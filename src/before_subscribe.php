<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Подтвердите подписку");

echo '<div class="content-main content-main-post-item col-md-8 no-float">
	<div class="content-main-wrapper">
		<div class="content-main-post-item-container">
			<div class="content-main-post-item-header clearfix">
				<div class="content-main-post-item-header-title-first">
					<h1>Подтвердите подписку</h1>
				</div>
			</div>
			<div class="content-main-post-item-body">
				<p>На Ваш почтовый ящик отправлено сообщение, содержащее ссылку для подтверждения правильности e-mail адреса. Пожалуйста, перейдите по ссылке для завершения подписки.</p>
				<p>Если письмо не пришло в течение 15 минут, проверьте папку «Спам». Если письмо вдруг попало в эту папку, откройте письмо, нажмите кнопку «Не спам» и перейдите по ссылке подтверждения. Если же письма нет и в папке «Спам», попробуйте подписаться ещё раз. Возможно, вы ошиблись при вводе адреса.</p>
				<p><a href="/">Вернуться на главную</a></p>
			</div>
		</div>
	</div>';

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>