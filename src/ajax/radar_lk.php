<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$added = new CrmAdder();
$added->checkRequired()->add("Регистрация Радар");	


require_once($_SERVER["DOCUMENT_ROOT"]."/api/mailchimp/MailChimp.php");

$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');

$list_id = 'b6050240b6';
$result = $MailChimp->post("lists/$list_id/members", [
	'email_address' => $_REQUEST["email"],
	'status'        => 'subscribed',
	"merge_fields" => [
		"EMAIL"=> $_REQUEST["email"],
		"FNAME"=> $_REQUEST["name"],
		"LNAME"=> "",
		"PHONE"=> $_REQUEST["phone_final"]
	],
	"interests"=> array(
		"c27112d78f"=> true
	)
]);