<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$_REQUEST["DATE"] = date("d-m-Y H:i:s");
file_put_contents($_SERVER["DOCUMENT_ROOT"]."/log.txt", print_r($_REQUEST, true), FILE_APPEND);

$data = array(
	"email" => isset($_REQUEST["Email"])?$_REQUEST["Email"]:$_REQUEST["email"],
	"name" => $_REQUEST["name"],
	"phone_final" => $_REQUEST["Phone"],
	"landing" => $_REQUEST['landing'],
	"utm_source" => $_REQUEST['utm_source'],
	"utm_campaign" => $_REQUEST['utm_campaign'],
	"utm_medium" => $_REQUEST['utm_medium'],
	"utm_personal" => $_REQUEST['utm_personal'],
	"utm_term" => $_REQUEST['utm_term'],
    "t_form" => $_REQUEST['t_form'],
	"mc_list" => $_REQUEST['mc_list'],
	"mc_interes" => $_REQUEST['mc_interes'],
);
unset($_REQUEST);
foreach ($data as $field=>$val){
	$_REQUEST[$field] = $val;
}


$_REQUEST["tilda"] = true;
$needAuthAfterRegister = false;
$added = new CrmAdder();
$added->checkRequired()->add($_REQUEST["landing"], $needAuthAfterRegister);


require_once($_SERVER["DOCUMENT_ROOT"]."/api/mailchimp/MailChimp.php");

$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');

$list_id = 'b6050240b6';
$interes_id = '0ac7b87356';

if($_REQUEST["mc_list"]){
	$list_id = $_REQUEST["mc_list"];
}
if($_REQUEST["mc_interes"]){
	$interes_id = $_REQUEST["mc_interes"];
}
//CLogger::Mailchimp_subscribe_before(print_r(array("request"=>$_REQUEST, "interes_id"=>$interes_id, "data"=>$data), true));

$existEmail = $MailChimp->clearGroups($_REQUEST["email"], $list_id, 10);//������� ������������ ������ ��� ���������� ������ �����
if($existEmail){
	$subscriber_hash = MailChimp::subscriberHash($_REQUEST["email"]);
	//��������� ������������� ������� �� ������ ��������. ���� ������� - �� ������ ������ pending � ��� ������ ������ �� �������������
    //��������.
    $res = $MailChimp->get("lists/{$list_id}/members/{$subscriber_hash}");
    if($_REQUEST["email"]=="alphaprogrammer@yandex.ru")
       CLogger::mailchimpPendingStatus("list_id=".$list_id." ".print_r($res, true));
    if(isset($res["status"]) && $res["status"]=="unsubscribed"){
        $result = $MailChimp->put("lists/{$list_id}/members/{$subscriber_hash}", [
          'status'        => 'pending',
          "interests"=> array(
            $interes_id => true
          )
        ]);
    } else { //����� ����������� ������� ���������� interes_id � ���������
        $result = $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
          'interests'    => array(
            $interes_id => true
          ),
          'status' => 'subscribed'
        ]);
    }
} else {
	$result = $MailChimp->post("lists/$list_id/members", [
		'email_address' => $_REQUEST["email"],
		'status'        => 'subscribed',
		"merge_fields" => [
			"EMAIL"=> $_REQUEST["email"],
			"FNAME"=> $_REQUEST["name"],
			"LNAME"=> "",
			"PHONE"=> $_REQUEST["phone_final"]
		],
		"interests"=> array(
			$interes_id => true
		)
	]);
}


CLogger::Mailchimp_subscribe(print_r(array("request"=>$_REQUEST, "interes_id"=>$interes_id, "data"=>$data, "result"=>$result), true));


?>