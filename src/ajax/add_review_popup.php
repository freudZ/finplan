<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$r = \Bitrix\Main\Context::getCurrent()->getRequest();
\Bitrix\Main\Loader::includeModule("iblock");

$req = array(
	"name",
	"email",
	"review_text"
);
foreach ($req as $re){
	if(!$r->get($re)){
		exit();
	}
}

if($r->get("confidencial_add_review")=="on"){
	$el = new CIBlockElement();

	$PROP = array();
	foreach($r->get("social") as $soc){
		if($soc) {
			$PROP["SOCIALS"][] = $soc;
		}
	}
	if($r->getFile("file")){
		$PROP["FILE"] = $r->getFile("file");
	}
	$PROP["EMAIL"] = $r->get("email");
	$PROP["FIO"] = $r->get("name");

	$arLoadProductArray = Array(
		"IBLOCK_ID"      => 25,
		"ACTIVE_FROM" => date("d.m.Y"),
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => $r->get("name"),
		"ACTIVE"         => "N",
		"DETAIL_TEXT"    => $r->get("review_text"),
	);

	if($id = $el->Add($arLoadProductArray)){
		CEvent::Send("NEW_REVIEW", SITE_ID, array(
			"LINK" => 'https://fin-plan.org/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=25&type=FINPLAN&ID='.$id.'&lang=ru&find_section_section=0&WF=Y'
		));
	}
	?>
	<b>Спасибо за ваш отзыв!</b>
	<p>Мы обязательно опубликуем его после проверки.</p>
<?
}