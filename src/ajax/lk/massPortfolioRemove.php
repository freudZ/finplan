<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
//Обработчик для массового удаления портфелей из ЛК
if ($_POST["ajax"] == 'y') {
	$portfolio = new CPortfolio();
	$arReturnPids = array();
	//Удаление всего портфеля
	if ($_POST['action'] == 'deleteMassPortfolio') {
		foreach($_POST['pids'] as $pid){
		 $arReturnPids[] = $pid;
		 $ret = $portfolio->deletePortfolio($pid);
		}
		echo json_encode($arReturnPids);
		unset($portfolio);
		die();
	}

} //if($_POST["action"]=='ajax')