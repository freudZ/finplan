<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!$USER->IsAuthorized()){
	exit();
}

$data = getFavoriteList();
$names = getFavoriteNames();

?>
<div class="userpage_favorites infinite_container green">
    <p class="title">Избранное:</p>

    <div class="userpage_favorites_list custom_collapse accordion">
        <?if($data):?>
            <?foreach ($data as $type=>$items):?>
                <div class="userpage_favorites_element custom_collapse_elem opened">
                    <div class="userpage_favorites_element_head custom_collapse_elem_head">
                        <p class="collapse_btn"><?=$names[$type]?></p>

                        <span class="icon icon-arr_down collapse_btn"></span>
                    </div>

                    <div class="userpage_favorites_element_body custom_collapse_elem_body">
                        <div class="userpage_favorites_element_body_inner">
                            <ul class="userpage_favorites_articles_list<?if(in_array($type, array("action", "obligation"))):?> userpage_favorites_shares_list<?endif?>">
                                <?foreach ($items as $item):?>
                                    <li>
										<span class="icon icon-close" data-id="<?=$item["ID"]?>"></span>
										<a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a>
										<span class="tick_outer">
											<?if(isset($item["PROPS"]["PROPERTY_LASTPRICE_VALUE"])):?>
												<span class="tick_price">
													<?=$item["PROPS"]["PROPERTY_LASTPRICE_VALUE"]?> руб.
												</span>
											<?endif?>
											<?if(isset($item["PROPS"]["PROPERTY_LASTCHANGEPRCNT_VALUE"])):?>
												<span class="tick_<?if($item["PROPS"]["PROPERTY_LASTCHANGEPRCNT_VALUE"]>=0):?>up<?else:?>down<?endif?> tick_percentage">
													<?=$item["PROPS"]["PROPERTY_LASTCHANGEPRCNT_VALUE"]?>%
												</span>
											<?endif?>
										</span>
									</li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?endforeach?>
        <?else:?>
            <p>У вас пока нет статей и материалов в избранном.</p>
        <?endif?>
    </div>
</div>