<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$req = array(
	"name",
	"email",
	"phone_final"
);
foreach($req as $r){
	if(!$_REQUEST[$r]){
		exit();
	} else {
		$fields[strtoupper($r)] = $_REQUEST[$r];
	}
}
CEvent::Send("ADD_TEXT_QUERY_FROM", SITE_ID, $fields);
?>
<b>Спасибо за вашу заявку!</b>
<p>Мы обязательно свяжемся с Вами, ожидайте.</p>