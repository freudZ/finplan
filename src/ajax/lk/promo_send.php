<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!$USER->IsAuthorized()){
    exit();
}

$arFields = array(
    "FIO" => "",
    "LINK" => "",
    "CODE" => "",
    "EMAIL" => $_REQUEST["email"]
);

//фио
$fields = array("LAST_NAME", "FIRST_NAME", "SECOND_NAME");
$fio = array();
foreach ($fields as $f){
	if($t = $USER->GetParam($f)){
		$fio[] = $t;
	}
}
$fio = $USER->GetParam("FIRST_NAME");
if(!$fio){
	$fio = $USER->GetParam("LOGIN");
}
$arFields["FIO"] = $fio;

if($_REQUEST["code"]){
    $arFields["CODE"] = $_REQUEST["code"];
}
if($_REQUEST["link"]){
    $arFields["LINK"] = $_REQUEST["link"];
}

if($arFields["LINK"]){
    $mesID = 14;
} else {
    $mesID = 15;
}
CEvent::Send("PROMO_SEND", SITE_ID, $arFields, "N", $mesID);

?>
<p style="color:green">Сообщение успешно отправлено.</p>