<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
/* Список портфелей для личного кабинета */
if(!$USER->IsAuthorized()){
	exit();
}
//Портфели
	$portfolio = new CPortfolio();
	$userId = $USER->GetID();
	$portfolioList = $portfolio->getPortfolioListForUser($userId, false);
	 $arTmpPortfolio = array();
	 $cntColumn = 0;
	foreach($portfolioList as $id=>$portfolio ){
		if($portfolio["owner_id"]!=$userId) continue;
		if(empty($portfolio['ver']) && !$GLOBALS["USER"]->IsAdmin()) continue;
		$arTmpPortfolio[$cntColumn][$id] = $portfolio;
		if($cntColumn<1){
			$cntColumn++;
		} else {
			$cntColumn = 0;
		}
	}

	$portfolioList = $arTmpPortfolio;
?>
<div class="userpage_favorites infinite_container green">
    <p class="title">Портфели:</p>

    <div class="userpage_favorites_list custom_collapse accordion">
        <?if($portfolioList):?>
                <div class="userpage_favorites_element custom_collapse_elem opened">
                    <div class="userpage_favorites_element_head custom_collapse_elem_head">
                        <p class="collapse_btn">Список портфелей:</p>

                        <span class="icon icon-arr_down collapse_btn"></span>
                    </div>

                    <div class="userpage_favorites_element_body custom_collapse_elem_body">
                        <div class="userpage_favorites_element_body_inner">
								   <div class="row">
								     <?foreach ($portfolioList as $col=>$portfolioItems):?>
									   <div class="col-xs-12 col-md-6">
										 <ul class="userpage_portfolio_articles_list">
                                <?foreach ($portfolioItems as $id=>$portfolio):?>
													  <?if($portfolio["owner_id"]!=$userId) continue;//Пропускаем расшаренные для текущего пользователя портфели?>
													  <?if(empty($portfolio['ver']) && !$GLOBALS["USER"]->IsAdmin()) continue;?>
			                              <li class="pidListElement_<?=str_replace('portfolio_','',$id)?>">
												 <span class="icon icon-close" data-id="<?=$id?>" data-toggle="modal" data-portfolio-name="<?=$portfolio["name"]?>" data-portfolio-id="<?=str_replace('portfolio_','',$id)?>" data-target="#popup_portfolio_remove_row"></span>

												<div class="checkbox">
													<input name="pidDelete[]" id="pidDelete_<?=str_replace('portfolio_','',$id)?>" class="massPidDel_input" type="checkbox" value="<?=str_replace('portfolio_','',$id)?>">
												   <label data-ver="<?=$portfolio['ver']?>" for="pidDelete_<?=str_replace('portfolio_','',$id)?>"><span><?=$portfolio["name"]?> <?=($GLOBALS["USER"]->IsAdmin()?(!empty($portfolio['ver'])?'(2.0)':''):'')?></span></label>
			 <a href="/portfolio/<?=$id?>/" class="dashed_link lk-portfolio-link" style="color:#ffb400;" id="portfolio_lk_<?=$id?>" target="_blank">Перейти</a>

													<span class="tick_outer">
														&nbsp;<span class="right-icon icon-lj_reverse" alt="Переименовать портфель" data-id="<?=$id?>" data-toggle="modal" data-portfolio-name="<?=$portfolio["name"]?>" data-portfolio-id="<?=str_replace('portfolio_','',$id)?>" data-target="#popup_portfolio_rename"></span>

													</span>
												</div>

												</li>
                                <?endforeach;?>
										</ul>
										</div>
			 						  <?endforeach;?>
								  </div>

									 <button disabled=""  data-toggle="modal" data-target="#popup_mass_portfolio_remove"  class="button delSelectedPortfolioPopup">Удалить выбранные портфели</button>
                        </div>
                    </div>
                </div>

        <?else:?>
            <p>У вас пока нет статей и материалов в избранном.</p>
        <?endif?>
    </div>
</div>