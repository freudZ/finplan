<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$r = \Bitrix\Main\Context::getCurrent()->getRequest();

global $USER;
if(!$USER->IsAuthorized()){
	exit();
}

if($r->get("password") != $r->get("confirm_password")){
	echo "Пароли не совпадают";
	exit();
}
if(strlen($r->get("password"))<6){
	echo "Минимальная длина пароля 6 символов";
	exit();
}

$user = new CUser;
$fields = Array(
	"PASSWORD" => $r->get("password")
);
$user->Update($USER->GetID(), $fields);

if($user->LAST_ERROR){
	echo $USER->LAST_ERROR;
}