<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$r = \Bitrix\Main\Context::getCurrent()->getRequest();

global $USER;
if(!$USER->IsAuthorized()){
	exit();
}

$user = new CUser;
$fields = Array(
	"NAME" => $r->get("name"),
	/*"LAST_NAME" => $r->get("surname"),
	"SECOND_NAME" => $r->get("patronymic")*/
);
$user->Update($USER->GetID(), $fields);