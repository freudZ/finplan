<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if($_REQUEST["secid"]){
	if($_REQUEST["type"]=="debstock_tab"){ // Облиги
		$class = new Obligations();
	} else if($_REQUEST["type"]=="shares_tab"){ //акции
		$class = new Actions();
	} else if($_REQUEST["type"]=="shares_usa_tab"){ //акции
		$class = new ActionsUsa();
	} else if($_REQUEST["type"]=="etf_tab"){ //etf
		$class = new ETF();
	}
	
	$item = $class->getItem($_REQUEST["secid"]);
	$havePayAccess = $class->havePayAccess;

	if($_REQUEST["type"]=="debstock_tab"){
		$obl_number = 15;
		$obl_number_from_cookie = $_COOKIE["obligations_number_cookie"];
		if ($obl_number_from_cookie != 0 && $obl_number_from_cookie != '') {
			$obl_number = $obl_number_from_cookie;
		}

		$item["PROPS"]["CURRENCYID"] = str_replace("SUR", "RUB", $item["PROPS"]["CURRENCYID"]);?>
		<tr data-id="<?=$item["PROPS"]["SECID"]?>" data-currency="<?=strtolower($item["PROPS"]["CURRENCYID"])?>">
		  <td class="obligation">
			<p class="elem_name "><a href="<?=$item["URL"]?>" class="tooltip_btn" title="<?=$item["NAME"]?> (<?= $item["PROPS"]["ISIN"] ?>)" portfolio-title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a></p>
			<?if($item["COMPANY"]):?>
				<p class="company_name line_green"><a class="tooltip_btn" title="<?=$item["COMPANY"]["NAME"]?>" href="<?=$item["COMPANY"]["URL"]?>"><?=$item["COMPANY"]["NAME"]?></a></p>
			<?endif?>
		  </td>
		  <td>
			<!-- цена выводится только в атрибут data-price -->
			<p class="elem_buy_price price_elem" data-price="<?=$item["DYNAM"]["Цена покупки"]?>"></p>
		  </td>
		  <td>
			<p class="date_off" data-date="<?=$item["PROPS"]["MATDATE"]?>"></p>
			<?if($item["PROPS"]["OFFERDATE"]):
				$t = new DateTime($item["PROPS"]["OFFERDATE"])?>
				<p class="line_green date_offerdate " data-offerdate="<?= $item["PROPS"]["OFFERDATE"] ?>">(<?=$t->format("d.m.Y")?>)</p>
			<?endif?>
		  </td>
		  <td>
			<!-- цена выводится только в атрибут data-price -->
			<p class="elem_sell_price price_elem" data-price="<?=$item["DYNAM"]["Цена погашения с учетом купонов"]?>"></p>
				<?if(array_key_exists("Цена погашения с учетом купонов в дату оферты",$item["DYNAM"]) && floatval($item["DYNAM"]["Цена погашения с учетом купонов в дату оферты"])>0):?>
				<p class="elem_sell_price_offer line_green price_elem" data-price="<?=$item["DYNAM"]["Цена погашения с учетом купонов в дату оферты"]?>"></p>
				<?endif;?>
		  </td>
		  <td>
			<p class="profit_year" data-profit_year="<?=$item["DYNAM"]["Доходность годовая"]?>"><?=$item["DYNAM"]["Доходность годовая"]?>%</p>
			<p class="profit_main" data-profit_main="<?=$item["DYNAM"]["Доходность общая"]?>"><?=$item["DYNAM"]["Доходность общая"]?>%</p>
			<?if($item["DYNAM"]["Доходность к офферте"]):?>
				<p class="line_green profit_offer" data-profit_offer="<?=$item["DYNAM"]["Доходность к офферте"]?>"><?=round($item["DYNAM"]["Доходность к офферте"], 1)?>%</p>
			<?endif?>
		  </td>
		  <td>
			<input class="numberof" type="text" value="<?=$obl_number?>"/>
		  </td>
		  <td>
			<div class="checkbox">
			  <input type="checkbox" id="<?=$item["PROPS"]["SECID"]?>"/>
			  <label for="<?=$item["PROPS"]["SECID"]?>"></label>
			</div>
		  </td>
		</tr>
		<?
	} else if($_REQUEST["type"]=="shares_tab"){
/*		$firephp = FirePHP::getInstance(true);
		$firephp->fb($item["PROPS"],FirePHP::LOG);*/
		?>
		<tr data-id="<?=$item["PROPS"]["SECID"]?>" data-lotsize="<?=$item["PROPS"]["LOTSIZE"]?>" data-currency="rub"<?if(!$havePayAccess && $item["PROPS"]["PROP_TARGET"]>=20):?> data-access="false"<?endif?>>
		  <td class="cshare">
			<p class="elem_name">
				<?if(!$havePayAccess && $item["DYNAM"]["Таргет"]>=20):?>
					<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть акцию</a>
				<?else:?>
					<a href="<?=$item["URL"]?>" class="tooltip_btn" title="<?=$item["NAME"]?> (<?= $item["PROPS"]["SECID"] ?>)" portfolio-title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a>
						  <?if(checkPaySeminar(13823, false, $USER->GetID())):?>
							  <?if($item["DYNAM"]["HAVE_GS"]=='Y'):?>
							     <?$gs_link = "По бумаге доступна аналитика в рамках годового сопровождения - <a class='' href='/lk/gs/".$item["CODE"]."/' target='_blank'>Смотреть</a>";?>
							     <span class="gs_tooltip_btn tooltip_btn p-tooltip-green" data-placement="top" title="" data-original-title="<?=$gs_link?>">A</span>
						  	  <?endif;?>
						  <?endif;?>
				<?endif?>
			</p>
			<?if($item["COMPANY"]):?>
				<p class="company_name line_green">
					<?if(!$havePayAccess && $item["DYNAM"]["Таргет"]>=20):?>
						<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть компанию</a>
					<?else:?>
						<a class="tooltip_btn" title="<?=$item["COMPANY"]["NAME"]?>" href="<?=$item["COMPANY"]["URL"]?>"><?=$item["COMPANY"]["NAME"]?></a>
					<?endif?>
				</p>
			<?endif?>
			<div class="no_display">
			  <p class="date_off" data-date=""></p>
			</div>
		  </td>
		  <td>
			<?if(!$havePayAccess && $item["DYNAM"]["Таргет"]>=20):?>
				<p class="elem_cshare_price price_elem" data-price="-"></p>
				<p class="elem_buy_price price_elem" data-price="-"></p>
				<p class="elem_company_price price_elem" data-price="-"></p>
			<?else:?>
				<p class="elem_cshare_price price_elem" data-price="<?=$item["PROPS"]["LASTPRICE"]?>"></p>
				<p class="elem_buy_price price_elem" data-price="<?=$item["DYNAM"]["Цена лота"]?>"></p>
				<p class="elem_company_price price_elem" data-price="<?=$item["PROPS"]["ISSUECAPITALIZATION"]?>"></p>
			<?endif?>

			<div class="no_display">
			  <p class="elem_sell_price sell_price_up" data-price=""></p>
			  <p class="elem_sell_price sell_price_down" data-price=""></p>
			</div>
		  </td>
		  <td>
			<p><?=$item["DYNAM"]["PE"]?></p>
			   <?if($item["PROPS"]["PROP_VID_AKTSIY"]!=='банковские'):?>
				<?$period = reset($item["PERIODS"])?>
				<p><?=round($period["Рентабельность собственного капитала"],2)?></p>
				<p><?=round($period["Темп роста прибыли"],2)?></p>
				<?endif;?>
		  </td>
		  <td>
			  	<?
				$divSum = $item["PROPS"]["PROP_DIVIDENDY_2018"];
				if($divSum>99){
					$divSum = round($divSum,0);
				}
				$divPrc = $item["DYNAM"]["Дивиденды %"];
				 ?>
				<p class="cshares_dividends_prc text-center" data-divprc="<?=$divPrc?>"><?= (floatval($divPrc)>0?$divPrc."%":'-'); ?></p>
				<p class="cshares_dividends text-center" data-div="<?=$divSum?>"><?=(floatval($divSum)>0?$divSum:'-')?></p>
		  </td>
		  <td>
				<?
				$dynam_target = 0;
				$dynam_sales = 0;

				$item["DYNAM"]["Таргет"] ? $dynam_target = $item["DYNAM"]["Таргет"] : $dynam_target = 0;

				$item["DYNAM"]["Просад"] ? $dynam_sales = $item["DYNAM"]["Просад"] : $dynam_sales = 0;
				?>
			<p class="cshare_up_p line_green"  data-value="<?=$dynam_target?>">+<span></span>%</p>
			<p class="cshare_down_p line_red"  data-value="<?=$dynam_sales?>">-<span></span>%</p>
			<p class=""  data-value="<?=$item["PROPS"]["BETTA"]?>"><?=round($item["PROPS"]["BETTA"],2)?></p>
			<div class="no_display">
			  <p class="profit_year_up"></p>
			  <p class="profit_year_down"></p>
			  <p class="profit_main_up"></p>
			  <p class="profit_main_down"></p>
			</div>
		  </td>
		  <td>
			<input class="numberof" type="text" value="5"/>
		  </td>
		  <td>
			<div class="checkbox">
			  <input type="checkbox" id="<?=$item["PROPS"]["SECID"]?>"/>
			  <label for="<?=$item["PROPS"]["SECID"]?>"></label>
			</div>
		  </td>
		</tr>
		<?
	}  else if($_REQUEST["type"]=="shares_usa_tab"){
		?>
		<tr data-id="<?=$item["PROPS"]["SECID"]?>" data-lotsize="1" data-currency="<?=(!empty($item["PROPS"]["CURRENCY"])?strtolower($item["PROPS"]["CURRENCY"]):"usd")?>"<?if(!$havePayAccess && $item["PROPS"]["TARGET"]>=20):?> data-access="false"<?endif?>>
		  <td class="cshare">
			<p class="elem_name trim">
				<?if(!$havePayAccess && $item["DYNAM"]["Таргет"]>=20):?>
					<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть акцию</a>
				<?else:?>
					<a href="<?=$item["URL"]?>" class="tooltip_btn" title="<?=$item["NAME"]?> (<?= $item["PROPS"]["SECID"] ?>)" portfolio-title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a>
						  <?if(checkPaySeminar(13823, false, $USER->GetID())):?>
							  <?if($item["DYNAM"]["HAVE_GS"]=='Y'):?>
							     <?$gs_link = "По бумаге доступна аналитика в рамках годового сопровождения - <a class='' href='/lk/gs_usa/".$item["CODE"]."/' target='_blank'>Смотреть</a>";?>
							     <a href="/lk/gs_usa/<?=$item["CODE"]?>/" class="gs_tooltip_btn tooltip_btn p-tooltip-green" data-placement="top" title="" data-original-title="<?=$gs_link?>">A</a>
						  	  <?endif;?>
						  <?endif;?>
				<?endif?>
			</p>
			<?if($item["COMPANY"]):?>
				<p class="company_name line_green">
					<?if(!$havePayAccess && $item["DYNAM"]["Таргет"]>=20):?>
						<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть компанию</a>
					<?else:?>
						<a class="tooltip_btn" title="<?=$item["COMPANY"]["NAME"]?>" href="<?=$item["COMPANY"]["URL"]?>"><?=$item["COMPANY"]["NAME"]?></a>
					<?endif?>
				</p>
			<?endif?>
			<div class="no_display">
			  <p class="date_off" data-date=""></p>
			</div>
		  </td>
		  <td>
			<?if(!$havePayAccess && $item["DYNAM"]["Таргет"]>=20):?>
				<p class="elem_cshare_price price_elem" data-price="-"></p>
				<p class="elem_buy_price price_elem" data-price="-"></p>
				<p class="elem_company_price price_elem" data-price="-"></p>
			<?else:?>
				<p class="elem_cshare_price price_elem" data-price="<?=$item["PROPS"]["LASTPRICE"]?>"></p>
				<p class="elem_buy_price price_elem no_display" data-price="<?=$item["PROPS"]["LASTPRICE"]?>"></p>
				<p class="elem_company_price price_elem" data-price="<?=$item["PROPS"]["ISSUECAPITALIZATION"]?>"></p>
			<?endif?>

			<div class="no_display">
			  <p class="elem_sell_price sell_price_up" data-price=""></p>
			  <p class="elem_sell_price sell_price_down" data-price=""></p>
			</div>
		  </td>
		  <td>

			<p><?=$item["DYNAM"]["PE"]?></p>
			   <?if($item["PROPS"]["PROP_VID_AKTSIY"]!=='банковские'):?>
				<?$period = reset($item["PERIODS"])?>
				<p><?=round($period["Рентабельность собственного капитала"],2)?></p>
				<p><?=round($period["Темп роста прибыли"],2)?></p>
				<?endif;?>
		  </td>
			  <td>
			  	<?
				$divSum = $item["PROPS"]["DIVIDENDS"];
				if($divSum>99){
					$divSum = round($divSum,0);
				}
				$divPrc = $item["DYNAM"]["Дивиденды %"];
				 ?>
				<p class="cshares_dividends_prc" data-divprc="<?=$divPrc?>"><?= (floatval($divPrc)>0?$divPrc."%":'-'); ?></p>
				<p class="cshares_dividends" data-div="<?=$divSum?>"><?=(floatval($divSum)>0?$divSum.getCurrencySign($item["PROPS"]["CURRENCY"]):'-')?></p>
			  </td>
		  <td>
				<?
				$dynam_target = 0;
				$dynam_sales = 0;

				if(intval($item["DYNAM"]["Таргет"])<=0){
					$item["DYNAM"]["Таргет"] = 0;
				}
				$item["DYNAM"]["Таргет"] ? $dynam_target = $item["DYNAM"]["Таргет"] : $dynam_target = 0;

				$item["DYNAM"]["Просад"] ? $dynam_sales = $item["DYNAM"]["Просад"] : $dynam_sales = 0;
				?>
			<p class="cshare_up_p line_green"  data-value="<?=$dynam_target?>">+<span></span>%</p>
			<p class="cshare_down_p line_red"  data-value="<?=$dynam_sales?>">-<span></span>%</p>
			<p class=""  data-value="<?=$item["PROPS"]["BETTA"]?>"><?=round($item["PROPS"]["BETTA"],2)?></p>
			<div class="no_display">
			  <p class="profit_year_up"></p>
			  <p class="profit_year_down"></p>
			  <p class="profit_main_up"></p>
			  <p class="profit_main_down"></p>
			</div>
		  </td>
		  <td>
			<input class="numberof" type="text" value="5"/>
		  </td>
		  <td>
			<div class="checkbox">
			  <input type="checkbox" id="<?=$item["PROPS"]["SECID"]?>"/>
			  <label for="<?=$item["PROPS"]["SECID"]?>"></label>
			</div>
		  </td>
		</tr>
		<?
	} else if($_REQUEST["type"]=="etf_tab"){
		?>
		<tr data-id="<?=$item["PROPS"]["SECID"]?>" data-lotsize="<?= $item["PROPS"]["LOTSIZE"] ?>" data-currency="<?= strtolower($item["PROPS"]["ETF_CURRENCY"]) ?>"<?if(!$arData["access"] && $item["PROPS"]["PROP_TARGET"]>=20):?> data-access="false"<?endif?>>
			  <td class="cshare">
				<p class="elem_name trim">
					<?if(!$arData["access"] && $item["DYNAM"]["Таргет"]>=20):?>
						<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть акцию</a>
					<?else:?>
						<a href="<?=$item["URL"]?>" class="tooltip_btn" title="<?=$item["NAME"]?> (<?= $item["PROPS"]["SECID"] ?>)" portfolio-title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a>
					<?endif?>
				</p>
				<?if($item["COMPANY"]):?>
					<p class="company_name line_green">
						<?if(!$arData["access"] && $item["DYNAM"]["Таргет"]>=20):?>
							<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть компанию</a>
						<?else:?>
							<a class="tooltip_btn" title="<?=$item["COMPANY"]["NAME"]?>" href="<?=$item["COMPANY"]["URL"]?>"><?=$item["COMPANY"]["NAME"]?></a>
						<?endif?>
					</p>
				<?endif?>
				<div class="no_display">
				  <p class="date_off" data-date=""></p>
				</div>
			  </td>
			  <td>

					<p class="elem_cshare_price price_elem" data-price="<?=$item["PROPS"]["LASTPRICE"]?>"><?=$item["PROPS"]["LASTPRICE"]?></p>
					<p class="elem_buy_price price_elem hide"  data-price="<?=$item["DYNAM"]["Цена лота"]?>"><?=$item["DYNAM"]["Цена лота"]?></p>

				<div class="no_display">
				  <p class="elem_sell_price sell_price_up" data-price=""></p>
				  <p class="elem_sell_price sell_price_down" data-price=""></p>
				</div>
			  </td>
			  <td>
				<p class="cshares_turnover" data-turn="<?=$item["PROPS"]["VALTODAY"]?>"></p>
			  </td>
			  <td>
					<?
					$dynam_target = 0;
					$dynam_sales = 0;

					$dynam_target = floatval($item["PROPS"]["ETF_AVG_ICNREASE"])>0 ?  floatval($item["PROPS"]["ETF_AVG_ICNREASE"]) : 0;
					$dynam_sales = floatval($item["DYNAM"]["Просад"])>0 ? floatval($item["DYNAM"]["Просад"]) : 0;

					?>
				<p class="cshare_up_p line_green"  data-value="<?=$dynam_target?>">+<span></span>%</p>
				<p class="cshare_down_p line_red"  data-value="<?=$dynam_sales?>">-<span></span>%</p>
				<p class=""  data-value="<?=$item["PROPS"]["BETTA"]?>"><?=round($item["PROPS"]["BETTA"],2)?></p>
				<div class="no_display">
				  <p class="profit_year_up"></p>
				  <p class="profit_year_down"></p>
				  <p class="profit_main_up"></p>
				  <p class="profit_main_down"></p>
				</div>
			  </td>
			  <td>
				<input class="numberof" type="text" value="5"/>
			  </td>
			  <td>
				<div class="checkbox">
				  <input type="checkbox" id="<?=$item["PROPS"]["SECID"]?>"/>
				  <label for="<?=$item["PROPS"]["SECID"]?>"></label>
				</div>
			  </td>
			</tr>
		<?
	}
}
