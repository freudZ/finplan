<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arReturn = [
    "suggestions" => [],
];
if($_REQUEST["query"]){
    $arFilter = Array("IBLOCK_ID"=>32, "PROPERTY_BOARDID" => [
        'TQBR',
        'TQDE',
        'TQDP',
        'TQTD',
        'TQTE',
		  'TQTF',
    ], [
        "LOGIC" => "OR",
        ["NAME" => "%".$_REQUEST["query"]."%"],
        ["PROPERTY_SECID" => "%".$_REQUEST["query"]."%"],
        ["PROPERTY_ISIN" => "%".$_REQUEST["query"]."%"],
    ], "ACTIVE"=>"Y");

    $res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10), array('ID','NAME','DETAIL_PAGE_URL','PROPERTY_ETF_TYPE_OF_ACTIVES'));
    while($ob = $res->GetNextElement()){
        $item = $ob->GetFields();

		$isETF = !empty($item["PROPERTY_ETF_TYPE_OF_ACTIVES_VALUE"])?true:false;

		if($isETF){
		  $item["DETAIL_PAGE_URL"]=str_replace("/actions/","/etf/",$item["DETAIL_PAGE_URL"]);
		}

        $arReturn["suggestions"][] = array(
            "value" => $item["NAME"] . ($isETF?" - ETF":" - акция"),
            "data" => $item["DETAIL_PAGE_URL"],
        );
    }

    $arFilter = Array("IBLOCK_ID"=>29, [
        "LOGIC" => "OR",
        ["NAME" => "%".$_REQUEST["query"]."%"],
    ], "ACTIVE"=>"Y");

    $res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10));
    while($ob = $res->GetNextElement()){
        $item = $ob->GetFields();

        $arReturn["suggestions"][] = array(
            "value" => $item["NAME"] . " - Компания",
            "data" => $item["DETAIL_PAGE_URL"],
        );
    }

    $arFilter = Array("IBLOCK_ID"=>27, [
        "LOGIC" => "OR",
        ["NAME" => "%".$_REQUEST["query"]."%"],
        ["PROPERTY_ISIN" => "%".$_REQUEST["query"]."%"],
    ], "ACTIVE"=>"Y");

    $res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10));
    while($ob = $res->GetNextElement()){
        $item = $ob->GetFields();

        $arReturn["suggestions"][] = array(
            "value" => $item["NAME"] . " - облигация",
            "data" => $item["DETAIL_PAGE_URL"],
        );
    }

	 //Индексы
    $arFilter = Array("IBLOCK_ID"=>53, [
        "LOGIC" => "OR",
        ["NAME" => "%".$_REQUEST["query"]."%"],
        ["CODE" => "%".$_REQUEST["query"]."%"],
    ], "ACTIVE"=>"Y");

    $res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10));
    while($ob = $res->GetNextElement()){
        $item = $ob->GetFields();

        $arReturn["suggestions"][] = array(
            "value" => $item["NAME"] . " - индекс",
            "data" => $item["DETAIL_PAGE_URL"],
        );
    }


//Ищем акции и компании США
    $arFilter = Array("IBLOCK_ID"=>55, [
        "LOGIC" => "OR",
        ["NAME" => "%".$_REQUEST["query"]."%"],
        ["PROPERTY_SECID" => "%".$_REQUEST["query"]."%"],
    ], "ACTIVE"=>"Y");

    $res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10), array('ID','NAME','DETAIL_PAGE_URL'));
    while($ob = $res->GetNextElement()){
        $item = $ob->GetFields();

/*		$isETF = !empty($item["PROPERTY_ETF_TYPE_OF_ACTIVES_VALUE"])?true:false;

		if($isETF){
		  $item["DETAIL_PAGE_URL"]=str_replace("/actions/","/etf/",$item["DETAIL_PAGE_URL"]);
		}*/

        $arReturn["suggestions"][] = array(
            "value" => $item["NAME"] . " - акция",
            "data" => $item["DETAIL_PAGE_URL"],
        );
    }

    $arFilter = Array("IBLOCK_ID"=>44, [
        "LOGIC" => "OR",
        ["NAME" => "%".$_REQUEST["query"]."%"],
    ], "ACTIVE"=>"Y");

    $res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10));
    while($ob = $res->GetNextElement()){
        $item = $ob->GetFields();

        $arReturn["suggestions"][] = array(
            "value" => $item["NAME"] . " - Компания США",
            "data" => $item["DETAIL_PAGE_URL"],
        );
    }

    //Ищем отрасли РФ
	$arFilter = Array("IBLOCK_ID"=>72, [
	  "LOGIC" => "OR",
	  ["NAME" => "%".$_REQUEST["query"]."%"],
	], "ACTIVE"=>"Y");

	$res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10));
	while($ob = $res->GetNextElement()){
		$item = $ob->GetFields();

		$arReturn["suggestions"][] = array(
		  "value" => $item["NAME"] . " - Отрасль РФ",
		  "data" => $item["DETAIL_PAGE_URL"],
		);
	}

}




echo json_encode($arReturn);
