<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arReturn = array(
    "suggestions" => array(

    )
);
if($_REQUEST["query"]){
    //$arFilter = Array("IBLOCK_ID"=>33, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
    $arFilter = Array("IBLOCK_ID"=>55, [
        "LOGIC" => "OR",
        ["NAME" => "%".$_REQUEST["query"]."%"],
        ["PROPERTY_SECID" => "%".$_REQUEST["query"]."%"],
    ], "ACTIVE"=>"Y", "=PROPERTY_HIDEN"=>false);

    $res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10));
    while($ob = $res->GetNextElement()){
        $item = $ob->GetFields();

        $arReturn["suggestions"][] = array(
            "value" => $item["NAME"],
            "data" => $item["DETAIL_PAGE_URL"],
        );
    }
}

echo json_encode($arReturn);
