<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arReturn = array(
	"suggestions" => array()
);
if($_REQUEST["query"]){
	
	$count = 10;
	
	if($_REQUEST["type"]=="debstock_tab"){ // Облиги
		$class = new Obligations();
	} else if($_REQUEST["type"]=="shares_tab"){ //акции
		$class = new Actions();
	} else if($_REQUEST["type"]=="shares_usa_tab"){ //акции США
		$class = new ActionsUsa();
	} else if($_REQUEST["type"]=="etf_tab"){ //etf
		$class = new ETF();
	}

	//отключить, если нет платного доступа
	if(!$class->havePayAccess){
		exit();
	}

	//выбранные
	$selected = $class->getSelectedItems();

	$items = $class->searchByName($_REQUEST["query"], $count, explode(",", $_REQUEST["inview"]));

	foreach($items as $item){
		//Пропускаем вышедшие из обращения
		if(!empty($item["PROPS"]["HIDEN"])){
					continue;
		}
		$secid = $item["SECID"];
		if($_REQUEST["type"]=="shares_tab"){
			$secid = $item["PROPS"]["SECID"];
		}

		if($selected[$secid]){
			continue;
		}

		$arReturn["suggestions"][] = array(
			"value" => $item["NAME"]."--".$item["SECID"]."--".$item["URL"],
		);
	}

	if(!$items){
		$items = $class->searchByName(switcher($_REQUEST["query"], 1), $count, explode(",", $_REQUEST["inview"]));
		foreach($items as $item){
			//Пропускаем вышедшие из обращения
			if(!empty($item["PROPS"]["HIDEN"])){
						continue;
			}
			if($selected[$item["SECID"]]){
				continue;
			}
			
			$arReturn["suggestions"][] = array(
				"value" => $item["NAME"]."--".$item["SECID"]."--".$item["URL"],
			);
		}
	}
}

echo json_encode($arReturn);