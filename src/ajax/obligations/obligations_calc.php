<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");

$arReturn = array(
	"suggestions" => array(

	)
);
$commonItem = array();

if($_REQUEST["query"]){

	$arFilter = Array("IBLOCK_ID"=>30, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10));

	while($ob = $res->GetNextElement()){

		$commonItem = $ob->GetFields();

		getCouponsInfo($commonItem);

		if (!$commonItem['COUPONS']) {
			continue;
		}

		if (!$commonItem["OBLIGATION"]["PROPS"]["LEGALCLOSE"]["VALUE"] && !$commonItem["OBLIGATION"]["PROPS"]["LASTPRICE"]["VALUE"]) {
			continue;
		}


		$arReturn["suggestions"][] = array(
			"value" => $commonItem["NAME"],
			"data" => $commonItem,
			"profit" => $commonItem['PROFIT'],
		);
	}
}

//если меняетяся цена
if ( $_POST['method'] === 'changePrice') {

	$commonItem["CODE"] = $_POST['code'];
	$data['LASTPRICE'] = (float)$_POST['price'] ?: 0;
	$data['LEGALCLOSE'] = (float)$_POST['price'] ?: 0;
	$data['userCouponPrice'] = (float)$_POST['coupon_price'] ?: 0;
	getCouponsInfo($commonItem, $data);

	$arReturn["suggestions"] = array(
		"data" => $commonItem,
		"profit" => $commonItem['PROFIT'],
	);
}

//если меняетяся цена купона
/*if ( $_POST['method'] === 'changeCouponPrice' && (int)$_POST['coupon_price'] ) {

	$commonItem["CODE"] = $_POST['code'];
	$data['userCouponPrice'] = $_POST['coupon_price'];
	getCouponsInfo($commonItem, $data);

	$arReturn["suggestions"] = array(
		"data" => $commonItem,
		"profit" => $commonItem['PROFIT'],
	);
}*/

if ($_POST['method'] === 'changeDetailedCouponPrice') {
	$commonItem["CODE"] = $_POST['code'];
	$data['detailedCouponPrice'] = $_POST['coupon_price'];
	$data['LASTPRICE'] = (float)$_POST['price'] ?: 0;
	$data['LEGALCLOSE'] = (float)$_POST['price'] ?: 0;

	getCouponsInfo($commonItem, $data);

	$arReturn["suggestions"] = array(
		"data" => $commonItem,
		"profit" => $commonItem['PROFIT'],
	);
}

echo json_encode($arReturn);

/**
*COUPONS INFO 
*/
function getCouponsInfo(&$commonItem, $qData = array()) {

	$propsFilter = Array("IBLOCK_ID"=>27, "CODE"=>$commonItem["CODE"]);
	$propsRes = CIBlockElement::GetList(Array(), $propsFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));

	/* PROPS */
	while ($propsItem = $propsRes->GetNextElement()) {			
		$commonItem["OBLIGATION"] = $propsItem->GetFields();
		$commonItem["OBLIGATION"]["PROPS"] = $propsItem->GetProperties();
		$commonItem["OBLIGATION"]["TITLE"] = 'Облигация '.$commonItem["OBLIGATION"]["NAME"].' ('.$commonItem["OBLIGATION"]["CODE"].')';
	}

	$data = array(
			'ACCRUEDINT' => $commonItem["OBLIGATION"]["PROPS"]["ACCRUEDINT"]["VALUE"] ?: 0,
			'FACEVALUE' => $commonItem["OBLIGATION"]["PROPS"]["FACEVALUE"]["VALUE"] ?: 0,
			'LASTPRICE' => $commonItem["OBLIGATION"]["PROPS"]["LASTPRICE"]["VALUE"] ?: 0,
			'LEGALCLOSE' => $commonItem["OBLIGATION"]["PROPS"]["LEGALCLOSE"]["VALUE"] ?: 0,
			'COUPONS_SUM' => 0,
	);

	/* COUPONS */

	CModule::IncludeModule("highloadblock");

	$hlblock   = HL\HighloadBlockTable::getById(15)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	$couponRes = $entityClass::getList(array(
		"filter" => array(
			"UF_ITEM" => $commonItem["CODE"],
			"!UF_DATA" => false,
		),
		"select" => array(
			"UF_DATA"
		),
	));

	if($item = $couponRes->fetch()){
		$arCoupons = json_decode($item["UF_DATA"], true);

		foreach($arCoupons as $item){

			if($item["Дата"] && $item["Сумма гашения"]){
				$arCouponsDates[$item["Дата"]] = $item["Сумма гашения"];
			}

			if(!$item["Дата купона"]){
				continue;
			}

			$dt = DateTime::createFromFormat('d.m.Y', $item["Дата купона"]);

			if($dt->getTimeStamp()>time()){
				
				if($commonItem["COUPONS"]){
					$t = $commonItem["COUPONS"][count($commonItem["COUPONS"])-1];
					$nominal = $t["Номинал"]-$t["Гашение"];
				} else {
					$nominal = $commonItem["OBLIGATION"]["PROPS"]["FACEVALUE"]["VALUE"];
				}
				
				$tmp = array(
					"Дата выплаты" => $dt->format("d.m.Y"),
					"Номинал" => $nominal,
					"Купоны" => round($item["Ставка купона"]/100*$nominal*$item["Длительность купона"]/365, 2),
					"Гашение" => round($arCouponsDates[$dt->format("d.m.Y")]?$arCouponsDates[$dt->format("d.m.Y")]:0, 2),
				);
				$tmp["Денежный поток"] = round($tmp["Купоны"]+$tmp["Гашение"], 2);
				
				
				$commonItem["COUPONS"][] = $tmp;
			}
		}

		if($commonItem["COUPONS"]){
			$showTotal = true;
			$beforeCancelationDays = 0;
			$unknownCouponsCount = 0;
            $nowDate = new DateTime(date('d.m.Y'));

			foreach($commonItem["COUPONS"] as $n=>$item){

				if (!floatval($item["Купоны"])){
					$unknownCouponsCount++;
				}

                if($n === 0){
                    $firstNominal = $item["Номинал"];
                }

				if ($qData['detailedCouponPrice']) {
					$data['COUPONS_SUM'] += $qData['detailedCouponPrice'][$n];
				}else{

					if (floatval($item["Купоны"])) {
						$data['COUPONS_SUM'] += round(floatval($item["Купоны"]),2);
					} elseif ( $qData['userCouponPrice'] ) {
						$data['COUPONS_SUM'] += round(floatval($qData['userCouponPrice']),2);
					}
				}

                if ($n === 0) {
                    $previousDate = $nowDate;
                } else {
                    $previousDate = $currentDate;
                }
                $currentDate = new DateTime($item["Дата выплаты"]);

                $difference = $currentDate->diff($previousDate)->days;

                $arResult["COUPONS"][$n]["Период, дней"] = $difference;

                $intervalToNow = $nowDate->diff($currentDate)->days;

                if ($intervalToNow < $difference) {
                    $beforeCancelationDays += $intervalToNow;
                } else {
                    $beforeCancelationDays += $difference * $item["Номинал"] / $firstNominal;
                }

				if(!$item["Купоны"]){
					$commonItem["COUPONS"][$n]["Купоны"] = "Купон пока не определен";
					$showTotal = false;
				}

				if(!$item["Денежный поток"]){
					$commonItem["COUPONS"][$n]["Денежный поток"] = "Зависит от купона";
					$showTotal = false;
				}
				
				if($showTotal){
					$commonItem["COUPONS_TOTAL"]["Купоны"] += $item["Купоны"];
					$commonItem["COUPONS_TOTAL"]["Гашение"] += $item["Гашение"];
					$commonItem["COUPONS_TOTAL"]["Денежный поток"] += $item["Денежный поток"];
				}
			}

			$data['CANCELATION_DAYS'] = $beforeCancelationDays;

			if(!$showTotal){
				unset($commonItem["COUPONS_TOTAL"]);
			}
		}
	}

	if($commonItem['COUPONS']){

		if ($qData) {
			foreach ($qData as $k => $v) {
				if ($v || $v === 0) {
					$data[$k] = $v;
				}
			}
		}
		$commonItem['UNKNOWN'] = $unknownCouponsCount;
		$commonItem['AAA'] = calculate($data);
		$commonItem['PROFIT'] = $commonItem['AAA']['year_profit'];
	}
}


function calculate ($data) {

		if (!$data) {
			return 0;
		}

		if(($data['LASTPRICE'] ?: $data['LEGALCLOSE']) <= 0) {
			return array(
				'year_profit' => 0,
			);
		}

		$currentPriceWithNkd = (($data['FACEVALUE'] * ($data['LASTPRICE'] ?: $data['LEGALCLOSE'])) / 100) + $data['ACCRUEDINT'];

		$cancelPrice = $data['FACEVALUE'] + $data['COUPONS_SUM'];

		$commonProfit = ($cancelPrice - $currentPriceWithNkd) * 100 / $currentPriceWithNkd;

		$yearProfit = $commonProfit / $data['CANCELATION_DAYS'] * 365;

		return array(
			'data' => $data,
			'current_price_with_nkd' => $currentPriceWithNkd,
			'cancel_price' => $cancelPrice,
			'common_profit' => $commonProfit,
			'year_profit' => round($yearProfit, 2),
		);
}


