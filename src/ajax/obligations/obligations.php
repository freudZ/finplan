<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arReturn = array(
	"suggestions" => array(

	)
);
if($_REQUEST["query"]){
	//$arFilter = Array("IBLOCK_ID"=>30, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
    $arFilter = Array("IBLOCK_ID"=>27, [
        "LOGIC" => "OR",
        ["NAME" => "%".$_REQUEST["query"]."%"],
        ["PROPERTY_ISIN" => "%".$_REQUEST["query"]."%"],
    ], "ACTIVE"=>"Y", "=PROPERTY_HIDEN"=>false);

	$res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10), array("ID","NAME","IBLOCK_ID", "PROPERTY_MATDATE", "DETAIL_PAGE_URL"));
	while($ob = $res->GetNextElement()){
		$item = $ob->GetFields();
		$arReturn["suggestions"][] = array(
			"value" => $item["NAME"],
			"data" => $item["DETAIL_PAGE_URL"],
		);
	}
}

echo json_encode($arReturn);
