<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arReturn = array(
  "suggestions" => array(

  )
);
if($_REQUEST["query"]){
	$arFilter = Array("IBLOCK_ID"=>72, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nPageSize"=>10));
	while($ob = $res->GetNextElement()){
		$item = $ob->GetFields();

		$arReturn["suggestions"][] = array(
		  "value" => $item["NAME"],
		  "data" => $item["DETAIL_PAGE_URL"],
		);
	}
}

echo json_encode($arReturn);
