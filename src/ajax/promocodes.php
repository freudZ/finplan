<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$code = strip_tags($_POST["code"]);
$summ = intval($_POST["summ"]);
$return = [];

if(!$code){
	$return["err"] = '<span style="color:red">Заполните поле</span>';
} else {
	global $USER;
	$res = Promocodes::getPromocode($code, $USER->GetID(), $summ);
	if($res!=true){
		if($res===false){
		$return["err"] = '<span style="color:red">Неверный промокод</span>';
		} else {
		$return["err"] = '<span style="color:green">Промокод активирован</span>'; //В случае когда пытаются использовать индивидуальный промокод - показываем что принят, но по факту не обрабатываем далее
		}
	} else {
		$return["success"] = '<span style="color:green">Промокод активирован</span>';
		$return = array_merge($return, $res);

		if($return["summ"]){
			if($return["type"]!="PERSONAL"){
				$return["final_summ"] = $summ-$return["summ"];
			}
			unset($return["summ"]);
		}
	}
}

echo json_encode($return);