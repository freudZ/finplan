<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
if(!$_POST['checkBannerActive']){
    print_r(false);
    die();
}
$res = CIBlockElement::GetProperty(40, $_POST['checkBannerActive'], ['sort' => 'asc'], ["CODE" => "BANNER_SHOW"]);

$result = false;

while ($obj = $res->Fetch()) {
    $result = $obj["VALUE"];
}

print_r(json_encode(['result' => $result]));