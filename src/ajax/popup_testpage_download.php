<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$added = new CrmAdder();
$added->checkRequired()->add("Тест Баффет");

require_once($_SERVER["DOCUMENT_ROOT"]."/api/mailchimp/MailChimp.php");

$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');

$list_id = 'b6050240b6';
$result = $MailChimp->post("lists/$list_id/members", [
	'email_address' => $_REQUEST["email"],
	'status'        => 'subscribed',
	"merge_fields" => [
		"EMAIL"=> $_REQUEST["email"],
		"FNAME"=> $_REQUEST["name"],
		"LNAME"=> "",
		"PHONE"=> $_REQUEST["phone_final"]
	],
	"interests"=> array(
		"c0892d8b8e"=> true
	)
]);

$arReturn = array(
	"name"=>'Спасибо за заявку!',
	"text"=>'<p>В ближайшее время Вам на почту придет наш отчет по портфелю Berkshire. Если будут вопросы, Вы всегда можете задать их в чате на нашем сайте (внизу справа).</p>'
);
echo json_encode($arReturn);

?>