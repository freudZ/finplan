<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!$_REQUEST["item"]){
	$item = "Попап закрытие";
} else {
	$item = $_REQUEST["item"]." - Попап закрытие";
}

$added = new CrmAdder();
$added->checkRequired()->add($item);

require_once($_SERVER["DOCUMENT_ROOT"]."/api/mailchimp/MailChimp.php");
if($_REQUEST["type"]=="yes"){
	$inter = [
		"65d4de1ddc"=> true
	];
} else {
	$inter = [
		"65d4de1ddc"=> true
	];
}

$name = $_REQUEST["name"];


$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');
$list_id = 'b6050240b6';
$result = $MailChimp->post("lists/$list_id/members", [
	'email_address' => $_REQUEST["email"],
	'status'        => 'subscribed',
	"merge_fields" => [
		"EMAIL"=> $_REQUEST["email"],
		"FNAME"=> $name,
		"LNAME"=> "",
		"PHONE"=> $_REQUEST["phone_final"]
	],
	"interests"=> $inter
]);
?>
<p>Поздравляем Вас! Вы успешно подписались на обучающую рассылку Fin-plan. Уже сегодня Вы начнете получать полезные материалы и советы по инвестированию от нас. Следите за почтой!</p>