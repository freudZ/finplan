<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
/**
 * Возвращает периоды по эмитенту или коду акции при выборе из выпадающих списков сравнения на страницах эмитентов
 *
 */
$arReturn = array();

$_POST["code"] = htmlspecialchars($_POST["code"]);
$_POST["country"] = htmlspecialchars($_POST["country"]);

//if(isset($_POST["code"]) && !empty($_POST["code"]))
if($_POST["country"]=="RUS"){
	$resA = new Actions();
	$arReturn = $resA->getItem($_POST["code"])["PERIODS"];
	unset($resA);
} else if($_POST["country"]=="USA"){
	$resU = new ActionsUsa();
	$arReturn = $resU->getItem($_POST["code"]);

	$arReturn = $arReturn["PERIODS"];
	unset($resU);
}
echo json_encode($arReturn);
exit;
