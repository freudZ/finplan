<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!$USER->IsAuthorized()){
	exit();
}

$data = getFavoriteList();
?>
<?if($data):
	$arNames = getFavoriteNames();?>
	<?foreach ($data as $type=>$items):?>
		<p class="strong">Мои избранные <?=strtolower($arNames[$type])?>:</p>
		<ul class="favorites_list<? if(strtolower($arNames[$type])=='акции'):?> favorites_debstock_list<?endif;?>">
			<?foreach ($items as $item):?>
				<li>
					<a href="<?=$item["DETAIL_PAGE_URL"]?>" title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a> 
					<span class="favorites_list_remove" data-id="<?=$item["ID"]?>">&times;</span> 
					<span class="tick_outer">
						<?if(isset($item["PROPS"]["PROPERTY_LASTPRICE_VALUE"])):?>
							<span class="tick_price">
								<?=$item["PROPS"]["PROPERTY_LASTPRICE_VALUE"]?> руб.
							</span>
						<?endif?>
						<?if(isset($item["PROPS"]["PROPERTY_LASTCHANGEPRCNT_VALUE"])):?>
							<span class="tick_<?if($item["PROPS"]["PROPERTY_LASTCHANGEPRCNT_VALUE"]>=0):?>up<?else:?>down<?endif?> tick_percentage">
								<?=$item["PROPS"]["PROPERTY_LASTCHANGEPRCNT_VALUE"]?>%
							</span>
						<?endif?>
					</span>
				</li>
			<?endforeach;?>
		</ul>
		<hr>
	<?endforeach?>
<?else:?>
    <p>У вас пока нет статей и материалов в избранном.</p>
<?endif?>
