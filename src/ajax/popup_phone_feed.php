<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$added = new CrmAdder();
$added->checkRequired()->add(isset($_REQUEST["landing"]) ? "Лендинг - телефон" : "Сайт - телефон");


require_once($_SERVER["DOCUMENT_ROOT"]."/api/mailchimp/MailChimp.php");

$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');

$list_id = 'b6050240b6';
$result = $MailChimp->post("lists/$list_id/members", [
    'email_address' => $_REQUEST["email"],
    'status'        => 'subscribed',
    "merge_fields" => [
        "EMAIL"=> $_REQUEST["email"],
        "FNAME"=> $_REQUEST["name"],
        "LNAME"=> "",
        "PHONE"=> $_REQUEST["phone_final"]
    ],
    "interests"=> array(
        "0ac7b87356"=> true
    )
]);

$arReturn = array(
    "name"=>'Поздравляем Вас!',
    "text"=>'<p>Вы успешно подписались на обучающую рассылку Fin-plan. Уже сегодня Вы начнете получать полезные материалы и советы по инвестированию от нас. Следите за почтой!</p>'
);

$arFields = array(
    "FIO" => $_REQUEST["name"],
    "EMAIL" => $_REQUEST["email"],
    "PHONE" => $_REQUEST["phone_final"],
);

CEvent::Send("CALLBACK", SITE_ID, $arFields, "N");

echo json_encode($arReturn);
?>