<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$added = new CrmAdder();
$added->checkRequired()->add("Семинар - всплывашка");

    require_once($_SERVER["DOCUMENT_ROOT"]."/api/mailchimp/MailChimp.php");

$CMailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');

$list_id = 'b6050240b6';
$interests = array("0ac7b87356"=> true);

if(!empty($_REQUEST["mclist"])){
  $list_id = $_REQUEST["mclist"];
}
if(!empty($_REQUEST["mcinterest"])){
  $interests = array();
  $interests[$_REQUEST["mcinterest"]] = true;
}

$existEmail = $CMailChimp->clearGroups($_REQUEST["email"], $list_id, 10);//Очищаем существующие группы для указанного адреса почты
if($existEmail){
	$subscriber_hash = MailChimp::subscriberHash($_REQUEST["email"]);
	$result = $CMailChimp->patch("lists/$list_id/members/$subscriber_hash", [
					'interests'    => $interests
				]);
} else {
	$result = $CMailChimp->post("lists/$list_id/members", [
		'email_address' => $_REQUEST["email"],
		'status'        => 'subscribed',
		"merge_fields" => [
			"EMAIL"=> $_REQUEST["email"],
			"FNAME"=> $_REQUEST["name"],
			"LNAME"=> "",
			"PHONE"=> $_REQUEST["phone_final"]
		],
		"interests"=> $interests
	]);
}
//mail('alphaprogrammer@gmail.com','Отладка mailchimp',print_r($_REQUEST, true).print_r($result, true));
$arReturn = array(
	"name"=>'Поздравляем Вас!',
	"text"=>'<p>Вы успешно подписались на обучающую рассылку Fin-plan. Уже сегодня Вы начнете получать полезные материалы и советы по инвестированию от нас. Следите за почтой!</p>'
);
unset($CMailChimp);
echo json_encode($arReturn);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>