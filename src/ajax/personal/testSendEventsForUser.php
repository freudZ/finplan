<? //Тестовая отправка сообщения
 /* св-ва пользователя
 	 UF_EVENTS_CHAT_ID - множество ID каналов коммуникации для рассылки (accountId из leeloo)
	 UF_EVENTS_ENABLE - Выключатель рассылки. 0-выкл./ 1-вкл.
	 UF_EVENTS_TYPES  - список выбранных событий для отслеживания
	 UF_EVENTS_PORTFOLIO  - Список портфелей для фильтрации событий
	 UF_EVENTS_USE_FAVORITES  - Учитывать избранное
  */

 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


//$LL = new CCrmLeeLoo();
//$result = $LL->sendEventMessagesLeeLoo($_REQUEST["userId"]);
//unset($LL);
  $smSender = new CSmartSender();
  $result = $smSender->sendEventMessagesSmarSender($_REQUEST["userId"]);
  unset($smSender);
  echo $result;
 ?>