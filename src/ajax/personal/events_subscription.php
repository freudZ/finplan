<?
  /*
   * Блок работы с подпиской на уведомления для ЛК
   */
 /* св-ва пользователя
 	 UF_EVENTS_CHAT_ID - множество ID каналов коммуникации для рассылки (accountId из leeloo)
	 UF_EVENTS_ENABLE - Выключатель рассылки. 0-выкл./ 1-вкл.
	 UF_EVENTS_TYPES  - список выбранных событий для отслеживания
	 UF_EVENTS_PORTFOLIO  - Список портфелей для фильтрации событий
	 UF_EVENTS_USE_FAVORITES  - Учитывать избранное

  */

$haveRadarAccess = checkPayRadar();
$CREvents = new CRadarEvents();
$smartSender = new CSmartSender();
$arUserEventParams = $smartSender->getUserEventParams($USER->GetID());
$eventsEnable = $arUserEventParams["UF_EVENTS_ENABLE"]==1;
$subscribed = !empty($arUserEventParams["UF_SMART_SENDER_UID"]);
 $selectedEventTypes = array();
 if(isset($_REQUEST["portfolioList"]) && !empty($_REQUEST["portfolioList"])){
	$selectedPortfolioList = explode(",",$_REQUEST["portfolioList"]);
 }

$portfolio = new CPortfolio();
$portfolioList = $portfolio->getPortfolioListForUser($USER->GetID(), false);

  ?>
<div id="events_subscribe" class="calculate_line infinite_container green">
<p class="calculate_step_title">Для того, чтобы получать уведомления, Вам необходимо выполнить быструю настройку событий, по которым Вы хотите получать ежедневные новости в 5 шагов.</p>

   <div class="row">
      <div class="col-xs-12">
		   <p class="title">Шаг 1</p>
         <p class="calculate_step_title">Выберите события, по которым хотели бы получать уведомления:</p>
      </div>
   </div>
	<div class="row usual_checkboxes">
	<div class="col col-xs-6 col-lg-4">
		<div class="checkbox">
			<input class="allEventsCheckbox personal" type="checkbox" checked name="eventTypeAll" value="ALL">
			<label><span>Все события</span></label>
		</div>
	</div>
		 <? foreach($CREvents->arEventsTypes as $evKey=>$evName): ?>
			 <div class="col col-xs-6 col-lg-4">
								<div class="checkbox">
								<?
								  $checked = false;
									if(!isset($_REQUEST["eventTypes"])){
									 $checked = true;
									} elseif(in_array($evKey,$selectedEventTypes)){
									  $checked = true;
									}
								?>
									<input class="eventTypeCheckbox personal" type="checkbox" name="eventType[]" <?=($checked?'checked="checked"':'')?> value="<?= $evKey ?>">
									<label><span><?= $evName ?></span></label>
								</div>
			 </div>
		 <? endforeach;?>
	</div>

	<!-- портфели -->
	<div class="row usual_checkboxes">
      <div class="col-xs-12">
		   <p class="title">Шаг 2</p>
         <p class="calculate_step_title">Выберите портфели, по которым хотели бы получать уведомления:</p>
      </div>
	<div class="col col-xs-6 col-lg-4">
	<?if(!$haveRadarAccess):?>
		<div class="dib relative black tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["события фильтр портфелей"]?>">
	<?endif?>
	<div class="checkbox">
			<? if((count($arUserEventParams["UF_EVENTS_PORTFOLIO"]) == count($portfolioList) && !empty($arUserEventParams["UF_EVENTS_PORTFOLIO"][0])) || $arUserEventParams["UF_EVENTS_PORTFOLIO"][0]=='all'){
				$checkAllPortfolio = true;
			} else {
				$checkAllPortfolio = false;
			}

  			?>

			<input class="allPortfolioCheckbox personal" type="checkbox" <?= count($portfolioList)==0?'disabled':'' ?> <?= !$haveRadarAccess?'':($checkAllPortfolio?'checked':'') ?> name="portfolioAll" value="all">
			<label><span>Все портфели</span></label>
		</div>
	<?if(!$haveRadarAccess):?>
		</div>
	<?endif?>
	</div>


		 <? foreach($portfolioList as $portf_id => $portf_val): ?>
			 <div class="col col-xs-6 col-lg-4">
			 	<?if(!$haveRadarAccess):?>
					<div class="dib relative black tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["события фильтр портфелей"]?>">
				<?endif?>
								<div class="checkbox">
								<?
								  $checked = false;
									if(count($arUserEventParams["UF_EVENTS_PORTFOLIO"])==0 ){
									 $checked = true;
									} elseif($arUserEventParams["UF_EVENTS_PORTFOLIO"][0]=='all'){
									 $checked = true;
									} elseif(in_array(str_replace("portfolio_","",$portf_id),$arUserEventParams["UF_EVENTS_PORTFOLIO"])){
									  $checked = true;
									}



									if(!$haveRadarAccess){
										$checked = false;
									}
								?>
									<input class="eventPortfolioCheckbox personal" type="checkbox" name="portfolioList[]" <?=($checked?'checked="checked"':'')?> value="<?= $portf_id ?>">
									<label><span><?= $portf_val["name"] ?></span></label>
								</div>
				<?if(!$haveRadarAccess):?>
					</div>
				<?endif?>
			 </div>
		 <? endforeach;?>

	</div>

	<!-- Избранное -->
   <div class="row">
      <div class="col-xs-12">
		   <p class="title">Шаг 3</p>
         <p class="calculate_step_title">Установите переключатель в положение включено, если хотите добавить в выборку уведомлений активы из «избранного»:</p>
      </div>
		<div class="col-xs-12">
			<div class="form_element">
				<div class="checkbox custom">
					<input type="checkbox" class="personal" name="eventFavorites" <?//= ($arUserEventParams["UF_EVENTS_ENABLE"]==0)?'disabled':'' ?> class="personal" <?= $arUserEventParams["UF_EVENTS_USE_FAVORITES"]==1?'checked':'' ?> value="Y" >
					<label >Учитывать избранное</label>
				</div>
			</div>
		</div>
	</div>

	<!-- подписка и управление -->
   <div class="row">
      <div class="col-xs-12">
		   <p class="title">Шаг 4</p>
         <p class="calculate_step_title unsubscribed_block <?=($subscribed?'hidden':'')?>">Для того, чтобы подписаться введите свой номер телефона в форму ниже и нажмите кнопку Telegram</p>
      </div>
		<div class="col-xs-12 unsubscribed_block <?=($subscribed?'hidden':'')?>">
					<?global $USER;
					$uid = $USER->GetID();
			  $rsUser = CUser::GetByID($uid);

			  $arUser = $rsUser->Fetch();

			  $phone = $arUser["PERSONAL_PHONE"];
			  if(strpos($phone, "8")==0 && strpos($phone, "8")!==false){
				  $phone = substr($phone, 1);
			  } else if(strpos($phone, "+7")!==false){
				  $phone = str_replace("+7", "", $phone);
			  }

			  ?>
			<?if(!$subscribed && $uid==7307):?>
			<div class='salebot_subscribe_block'>
					<div class=" text-center salebot-content-container">
					<div class="event_subscribe_form" novalidate="novalidate">
							<input type="hidden" name="bitrix_uid" value="<?= $uid ?>">
							<input type="hidden" name="bitrix_uphone" value="<?= $phone ?>">
							<input type="hidden" name="bitrix_uemail" value="<?= !empty($arUser["EMAIL"])?$arUser["EMAIL"]:$arUser["LOGIN"] ?>">
							<input type="hidden" name="action" value="subscribe">
							<input type="hidden" name="type_messenger" value="1">
						<div class="form_element">
							<input type="text" class="inline" placeholder="Ваш номер телефона" name="phone" value="<?= $phone;?>" autocomplete="off">
						</div>
                        <div class="widget-text-container"><p class="widget-description" style="color: rgb(0, 0, 0);">Подписка на уведомления RadarInfo<br></p></div>
						<div class="row">
							<div class="col-sm-12 text-center">
								<a class="button" target="_blank" href="" id="subscribe_events_telegram"><span class="icon icon-telegram"></span> Telegram</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?endif;?>

			<div id="UTetJhfh" class="ss-landing" data-target="UTetJhfh" data-domain="invfut"></div>



			<script>
			<?if(!$subscribed):?>
			 $(document).ready(function(){
			 	var intervalID;
				intervalID = setInterval(function(){checkSubscribe(intervalID);}, 5000);
          });

			 function checkSubscribe(intervalID){

			 		$.ajax({
			 		type: "POST",
			 		url: "/api/smartSender/smartSender.php",
			 		dataType: "json",
			 		data:{ajax: "y", action: 'checkSubscribe', 'bxUserId':'<?=$arUser["ID"]?>'},
			 		cashe: false,
			 		async: true,
			 		success: function(data){
			 			getResponse(data);
			 		}
			 		});

					function getResponse(data){
						if(data.SUBSCRIBED==true){
							clearInterval(intervalID);
							$('.subscribed_block').removeClass('hidden');
							$('.unsubscribed_block').addClass('hidden');
						}
						if(data.ENABLED==true){
							$('input[name=eventOn]').prop('checked', true);
							$('.testSendEventsForUser').removeClass('disabled');
							$('.testSentMessage').text('');
						}
					}

			 }
			 <?endif;?>
			</script>
			<script>
			    let ssContext = {
			        variables: { // Данные клиента
			            name: 'int|string',
							bxUserId: <?=$arUser["ID"]?>,
			            // bonuses: 1000
			            // ...
			        },
			        scope: { // Внешние переменные
			            name: 'int|string',
							bxUserId: <?=$arUser["ID"]?>,
			            // amount: 1000
			            // name: Hello
			            // orderId: '1111-2222-3333-4444',
			            // ...
			        },
			        form: { // Значения по умолчанию внутри формы
			            //phone: '<?= $phone ?>', // Номер телефона
			            email: '<?= $arUser["EMAIL"] ?>', // Email
			        },
			    };
			</script>
			<script src="https://customer.smartsender.eu/js/client/lp.min.js?v2.0.0"></script>
			<p class="calculate_step_title">Если у Вас нет программы телеграм, то установить можно по <a href="https://telegram.org/" rel="nofollow" target="_blank">ссылке</a> (это бесплатно).</p>
		</div>
		<div class="col-xs-12 subscribed_block <?=($subscribed?'':'hidden')?>">
			<div class="form_element">
				<div class="checkbox custom">
					<input type="checkbox" name="eventOn" <?//= ($arUserEventParams["UF_EVENTS_ENABLE"]==0)?'disabled':'' ?> class="personal" <?= $eventsEnable?'checked':'' ?> value="Y" >
					<label>Уведомелния включены</label>
				</div>
			</div>
			<p class="calculate_step_title">Вы подписаны на получение уведомлений в телеграм от нашего бота @RadarInfo_bot </p>
		</div>
	</div>


	<!-- Избранное -->
   <div class="row">
      <div class="col-xs-12">
		   <p class="title">Шаг 5</p>
         <p class="calculate_step_title subscribed_block <?=($subscribed?'':'hidden')?>">Отлично, Вы подписаны на уведомления. Они будут приходить ежедневно в 8-30 по мск (кроме субботы и воскресенья) и сообщать вам о плановых событиях на текущий день</p>
         <p class="calculate_step_title unsubscribed_block <?=($subscribed?'hidden':'')?>">Подпишитесь на получение уведомлений, для начала рассылки</p>
      </div>

	</div>

	 <? $enableTestSendButton = $eventsEnable; ?>
   <div class="row subscribed_block <?=($subscribed?'':'hidden')?>">
      <div class="col-xs-12">
			<div>
				<div class="testSendEventsForUser <?= (!$enableTestSendButton?'disabled':'') ?> button" data-uid="<?= $USER->GetID() ?>">Отправить рассылку</div>
				&nbsp;<span class="testSentMessage"><?= (!$enableTestSendButton?'Отправка будет доступна после подписки.':'') ?></span>
				<hr>
			</div>
		</div>
	</div>

</div>