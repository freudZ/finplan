<? //Сохранение пользовательских настроек рассылки уведомлений
 /* св-ва пользователя
 	 UF_EVENTS_CHAT_ID - множество ID каналов коммуникации для рассылки (accountId из leeloo)
	 UF_EVENTS_ENABLE - Выключатель рассылки. 0-выкл./ 1-вкл.
	 UF_EVENTS_TYPES  - список выбранных событий для отслеживания
	 UF_EVENTS_PORTFOLIO  - Список портфелей для фильтрации событий

  */

 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
 Global $USER;
 $haveRadarAccess = checkPayRadar();
 $haveUSARadarAccess = checkPayUSARadar();


  if(!isset($_REQUEST['eventPortfolio']) || empty($_REQUEST['eventPortfolio'])){
  	$_REQUEST['eventPortfolio'] = "";
  }


  $oUser = new CUser;

  $aFields = array(
      'UF_EVENTS_PORTFOLIO' => $_REQUEST['eventPortfolio'],
      'UF_EVENTS_TYPES' => $_REQUEST['eventTypes'],
      'UF_EVENTS_ENABLE' => $_REQUEST['eventOn'],
      'UF_EVENTS_USE_FAVORITES' => $_REQUEST['eventFavorites']=='Y'?1:0
  );
  $oUser->Update($USER->GetID(), $aFields); //$iUserID (int) ID of USER



  echo json_encode(array("result"=>"success", "message"=>"saved"));

 ?>