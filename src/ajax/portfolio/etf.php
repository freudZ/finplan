<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
CModule::IncludeModule("iblock");

$arReturn = array(
	"suggestions" => array(

	)
);

$pid = htmlspecialchars($_GET['pid']);
$date = htmlspecialchars($_GET['date']);
if(!empty($pid) && !empty($date)){
$CPortfolio = new CPortfolio();
$arReturn["cache"] = $CPortfolio->getCacheOnBeginDate($pid, $date);
unset($CPortfolio);
}

if ($_REQUEST["query"]) {
	//$arFilter = Array("IBLOCK_ID"=>33, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
	$arFilter = Array("IBLOCK_ID" => 32,
	"PROPERTY_BOARDID" => array(
		'TQTF',
		'TQTD',
		'TQTE',
	),
	//"PROPERTY_IS_ETF_ACTIVE_VALUE" => "Y", //������ �������� �������
	 array(
		"LOGIC" => "OR",
		array("NAME" => "%" . $_REQUEST["query"] . "%"),
		array("PROPERTY_SECID" => "%" . $_REQUEST["query"] . "%"),
	),
	"ACTIVE" => "Y",
	"=PROPERTY_HIDEN"=>false
	);

	$res = CIBlockElement::GetList(Array("sort" => "asc", "NAME" => "asc"), $arFilter, false, array("nPageSize" => 10), array("*", "IBLOCK_ID", "PROPERTY_EMITENT_ID", "PROPERTY_SECID", "PROPERTY_LOTSIZE", "PROPERTY_LASTPRICE", "PROPERTY_LEGALCLOSE", "PROPERTY_ETF_CURRENCY"));
	while ($ob = $res->GetNextElement()) {
		$item = $ob->GetFields();

		//�������� ��������
		$arSelectC = Array("ID", "NAME", "IBLOCK_ID", "CODE");
		$arFilterC = Array("IBLOCK_ID" => IntVal(26), "ID" => $item["PROPERTY_EMITENT_ID_VALUE"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
		$resC = CIBlockElement::GetList(Array(), $arFilterC, false, Array("nTopCount" => 1), $arSelectC);
		if ($obC = $resC->GetNextElement()) {
			$arFieldsC = $obC->GetFields();

			//������� �������� ��������
			$arSelectCP = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_VAL_ED_FIRST");
			$arFilterCP = Array("IBLOCK_ID" => IntVal(29), "=NAME" => $arFieldsC["NAME"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$resCP = CIBlockElement::GetList(Array(), $arFilterCP, false, Array("nTopCount" => 1), $arSelectCP);
			while ($obCP = $resCP->GetNextElement()) {
				$arFieldsCP = $obCP->GetFields();
			}

		}

		$currency_value = 1;
		if(strpos(strtolower($item['PROPERTY_ETF_CURRENCY_VALUE']),'rub')===false){
		  $currency_value = getCBPrice(strtoupper($item['PROPERTY_ETF_CURRENCY_VALUE']));
		}

		$actionPrice = (!empty($item['PROPERTY_LASTPRICE_VALUE']) ? $item['PROPERTY_LASTPRICE_VALUE'] : $item['PROPERTY_LEGALCLOSE_VALUE']);
		$actionPrice *= $currency_value;
		$actionPriceValute = 0;
		$actionPrice = round($actionPrice,4);
		$arSuggestion = array(
			"value" => $item["NAME"],
			"id" => $item["ID"],
			"code" => $item["CODE"],
			"url" => str_replace("/actions/","/etf/",$item["DETAIL_PAGE_URL"]),
			"lastprice" =>$actionPrice*intval($item['PROPERTY_LOTSIZE_VALUE']),
			"price" => $actionPrice,
			"inlot_cnt"=>intval($item['PROPERTY_LOTSIZE_VALUE']),
			"emitent_url" => "/lk/obligations/company/" . $arFieldsCP["CODE"] . "/",
			"emitent_name" => $arFieldsC["NAME"],
			"emitent_currency" => strtolower($item['PROPERTY_ETF_CURRENCY_VALUE']),
			"currencyId" => strtolower($item['PROPERTY_ETF_CURRENCY_VALUE']),
			"secid" => $item['PROPERTY_SECID_VALUE'],
			"cache" => $arReturn["cache"],
		);
		if(strpos(strtolower($item['PROPERTY_ETF_CURRENCY_VALUE']),'rub')===false){
		  $actionPriceValute = round($actionPrice/$currency_value, 4);
		  $arSuggestion["price_valute"] = floatval($actionPriceValute);
		  $arSuggestion["rate"] = floatval($currency_value);
		}


		$arReturn["suggestions"][] = $arSuggestion;
	}
}

echo json_encode($arReturn);
