<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
CModule::IncludeModule("iblock");

$arReturn = array(
	"suggestions" => array(

	)
);
if ($_REQUEST["query"]) {
	//$arFilter = Array("IBLOCK_ID"=>33, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
	$arFilter = Array("IBLOCK_ID" => 54, "!=PROPERTY_SECID"=>"RUB", "PROPERTY_BASE"=>"Y",[
		"LOGIC" => "OR",
		["NAME" => "%" . $_REQUEST["query"] . "%"],
		["CODE" => "%" . $_REQUEST["query"] . "%"],
		["PROPERTY_SECID" => "%" . $_REQUEST["query"] . "%"],
	], "ACTIVE" => "Y", "=PROPERTY_HIDEN"=>false);

	$res = CIBlockElement::GetList(Array("sort" => "asc", "NAME" => "asc"), $arFilter, false, array("nPageSize" => 10), array("ID", "CODE", "NAME", "DETAIL_PAGE_URL", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_LASTPRICE"));
	while ($ob = $res->GetNextElement()) {
		$item = $ob->GetFields();

		$actionPrice = $item['PROPERTY_LASTPRICE_VALUE'];

		$arReturn["suggestions"][] = array(
			"value" => $item["NAME"],
			"id" => $item["ID"],
			"code" => $item["CODE"],
			"url" => $item["DETAIL_PAGE_URL"],
			"lastprice" =>$actionPrice,
			"price" => $actionPrice,
			"inlot_cnt"=>1,
			"emitent_url" => "/lk/currency/" . $item["CODE"] . "/",
			"emitent_name" => $item["NAME"],
			"emitent_currency" => strtolower($item["CODE"]),//strpos($arFieldsC["PROPERTY_VAL_ED_FIRST_VALUE"], "����") !== false ? 'usd' : 'rub',
			"currencyId" => strtolower($item["CODE"]), //��� ������������� � ��������
			"secid" => $item['PROPERTY_SECID_VALUE'],
		);
	}
}

echo json_encode($arReturn);
