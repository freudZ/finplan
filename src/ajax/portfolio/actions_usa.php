<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
CModule::IncludeModule("iblock");

$arReturn = array(
	"suggestions" => array(

	)
);

$pid = htmlspecialchars($_GET['pid']);
$date = htmlspecialchars($_GET['date']);
if(!empty($pid) && !empty($date)){
$CPortfolio = new CPortfolio();
$arReturn["cache"] = $CPortfolio->getCacheOnBeginDate($pid, $date);
unset($CPortfolio);
}

if ($_REQUEST["query"]) {
	//$arFilter = Array("IBLOCK_ID"=>33, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
	$arFilter = Array("IBLOCK_ID" => 55, "PROPERTY_BOARDID" => [
		'TQBR',
		'TQDE',
		'TQDP',
	], [
		"LOGIC" => "OR",
		["NAME" => "%" . $_REQUEST["query"] . "%"],
		["PROPERTY_SECID" => "%" . $_REQUEST["query"] . "%"],
	], "ACTIVE" => "Y", );

	$res = CIBlockElement::GetList(Array("sort" => "asc", "NAME" => "asc"), $arFilter, false, array("nPageSize" => 10), array("*", "IBLOCK_ID", "PROPERTY_EMITENT_ID", "PROPERTY_SECID", "PROPERTY_LOTSIZE", "PROPERTY_LASTPRICE", "PROPERTY_LEGALCLOSE", "PROPERTY_CURRENCY"));
	while ($ob = $res->GetNextElement()) {
		$item = $ob->GetFields();
		//if(!isset($item['PROPERTY_LOTSIZE_VALUE']) || intval($item['PROPERTY_LOTSIZE_VALUE'])<=0){
		//	$item['PROPERTY_LOTSIZE_VALUE'] = 1;
		//}
		//�������� ��������
		$arSelectC = Array("ID", "NAME", "IBLOCK_ID", "CODE");
		$arFilterC = Array("IBLOCK_ID" => IntVal(43), "ID" => $item["PROPERTY_EMITENT_ID_VALUE"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
		$resC = CIBlockElement::GetList(Array(), $arFilterC, false, Array("nTopCount" => 1), $arSelectC);
		if ($obC = $resC->GetNextElement()) {
			$arFieldsC = $obC->GetFields();

			//������� �������� ��������
			$arSelectCP = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_VAL_ED_FIRST");
			$arFilterCP = Array("IBLOCK_ID" => IntVal(44), "=NAME" => $arFieldsC["NAME"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$resCP = CIBlockElement::GetList(Array(), $arFilterCP, false, Array("nTopCount" => 1), $arSelectCP);
			while ($obCP = $resCP->GetNextElement()) {
				$arFieldsCP = $obCP->GetFields();
			}

		}

		$currency_value = 1;

		if(strpos(strtolower($item['PROPERTY_CURRENCY_VALUE']),'rub')===false){
		  $currency_value = getCBPrice(strtoupper($item['PROPERTY_CURRENCY_VALUE']));
		}

		$actionPrice = (!empty($item['PROPERTY_LASTPRICE_VALUE']) ? $item['PROPERTY_LASTPRICE_VALUE'] : $item['PROPERTY_LEGALCLOSE_VALUE']);
		$actionPriceValute = $actionPrice;
		$actionPrice *= $currency_value;
		$actionPrice = round($actionPrice,3);

		$arReturn["suggestions"][] = array(
			"value" => $item["NAME"],
			"id" => $item["ID"],
			"code" => $item["CODE"],
			"url" => $item["DETAIL_PAGE_URL"],
			"lastprice" =>$actionPrice*1,
			"price" => $actionPrice,
			"price_valute" => floatval($actionPriceValute),
			"rate"=>floatval($currency_value),
			"date_now"=>(new DateTime())->format('d.m.Y'),
			"inlot_cnt"=>1,
			"emitent_url" => "/lk/obligations/company_usa/" . $arFieldsCP["CODE"] . "/",
			"emitent_name" => $arFieldsC["NAME"],
			"emitent_currency" => strtolower($item['PROPERTY_CURRENCY_VALUE']),
			"currencyId" => strtolower($item['PROPERTY_CURRENCY_VALUE']),
			"secid" => $item['PROPERTY_SECID_VALUE'],
			"cache" => $arReturn["cache"],
		);
	}
}

echo json_encode($arReturn);
