<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
CModule::IncludeModule("iblock");

$arReturn = array(
	"suggestions" => array(),
	//"cache" => array()
);

$pid = htmlspecialchars($_GET['pid']);
$date = htmlspecialchars($_GET['date']);
if(!empty($pid) && !empty($date)){
$CPortfolio = new CPortfolio();
$arReturn["cache"] = $CPortfolio->getCacheOnBeginDate($pid, $date);
unset($CPortfolio);
}

if ($_REQUEST["query"]) {
	//$arFilter = Array("IBLOCK_ID"=>33, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
	$arFilter = Array("IBLOCK_ID" => 32, "PROPERTY_BOARDID" => [
		'TQBR',
		'TQDE',
		'TQDP',
	], [
		"LOGIC" => "OR",
		["NAME" => "%" . $_REQUEST["query"] . "%"],
		["PROPERTY_SECID" => "%" . $_REQUEST["query"] . "%"],
	], "ACTIVE" => "Y", );

	$res = CIBlockElement::GetList(Array("sort" => "asc", "NAME" => "asc"), $arFilter, false, array("nPageSize" => 10), array("*", "IBLOCK_ID", "PROPERTY_EMITENT_ID", "PROPERTY_SECID", "PROPERTY_LOTSIZE", "PROPERTY_LASTPRICE", "PROPERTY_LEGALCLOSE", ));
	while ($ob = $res->GetNextElement()) {
		$item = $ob->GetFields();

		//�������� ��������
		$arSelectC = Array("ID", "NAME", "IBLOCK_ID", "CODE");
		$arFilterC = Array("IBLOCK_ID" => IntVal(26), "ID" => $item["PROPERTY_EMITENT_ID_VALUE"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
		$resC = CIBlockElement::GetList(Array(), $arFilterC, false, Array("nTopCount" => 1), $arSelectC);
		if ($obC = $resC->GetNextElement()) {
			$arFieldsC = $obC->GetFields();

			//������� �������� ��������
			$arSelectCP = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_VAL_ED_FIRST");
			$arFilterCP = Array("IBLOCK_ID" => IntVal(29), "=NAME" => $arFieldsC["NAME"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$resCP = CIBlockElement::GetList(Array(), $arFilterCP, false, Array("nTopCount" => 1), $arSelectCP);
			while ($obCP = $resCP->GetNextElement()) {
				$arFieldsCP = $obCP->GetFields();
			}

		}


		$actionPrice = (!empty($item['PROPERTY_LASTPRICE_VALUE']) ? $item['PROPERTY_LASTPRICE_VALUE'] : $item['PROPERTY_LEGALCLOSE_VALUE']);

		$arReturn["suggestions"][] = array(
			"value" => $item["NAME"],
			"id" => $item["ID"],
			"code" => $item["CODE"],
			"url" => $item["DETAIL_PAGE_URL"],                          
			"lastprice" =>$actionPrice*intval($item['PROPERTY_LOTSIZE_VALUE']),
			"price" => $actionPrice,
			"inlot_cnt"=>intval($item['PROPERTY_LOTSIZE_VALUE']),
			"emitent_url" => "/lk/obligations/company/" . $arFieldsCP["CODE"] . "/",
			"emitent_name" => $arFieldsC["NAME"],
			"emitent_currency" => strpos($arFieldsC["PROPERTY_VAL_ED_FIRST_VALUE"], "����") !== false ? 'usd' : 'rub',
			"currencyId" => strpos($arFieldsC["PROPERTY_VAL_ED_FIRST_VALUE"], "����") !== false ? 'usd' : 'rub', //��� ������������� � ��������
			"secid" => $item['PROPERTY_SECID_VALUE'],
			"cache" => $arReturn["cache"],
		);
	}
}



echo json_encode($arReturn);
