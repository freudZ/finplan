<?
 /**
  * Скрипт обработки загрузки брокерских отчетов
  */
 require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
 $CHistory = new CHistory(true);
 $arReport = array();
 //При включенной отладке FirePhp После загрузки портфеля не происходит редиректа и закрытия попапа загрузки!!!
if($_POST["step"]==1){ //Парсим
	if ( 0 < $_FILES['broker_report_file']['error'] ) {
		 unset($CHistory);
	    echo json_encode(array("result"=>'error', "message"=>$_FILES['broker_report_file']['error']));
		 die();
	} else {
	$uploadFilePath = $_SERVER["DOCUMENT_ROOT"].'/upload/brokersReports/' . $_FILES['broker_report_file']['name'];
    move_uploaded_file($_FILES['broker_report_file']['tmp_name'], $uploadFilePath);
	 $reportType = htmlspecialchars($_POST["report_type"]);
    if (file_exists($uploadFilePath)) {
	 $arReport = $CHistory->uploadAndParse($reportType ,$uploadFilePath, $_FILES['broker_report_file']['name']);

	 if(array_key_exists('result', $arReport) && $arReport['result']=='error'){
	  	   unset($CHistory);
     		echo json_encode($arReport);
	 		die();
	 }
	 }
	}

	if(count($arReport["deals_nofound"])==0){
	  $_POST["step"] = 2;
  	} else {
	  	   unset($CHistory);
			//Сохраняем распарсенный отчет во временный файл на сервере в папке /upload/broker_report_tmp/ и передаем имя и путь к файлу в ответе на аякс 1 шага
			$saveTmpFilePath = $_SERVER["DOCUMENT_ROOT"].'/upload/broker_report_tmp/' . $_FILES['broker_report_file']['name'];
		 	file_put_contents($saveTmpFilePath, json_encode($arReport));
			//Вместо полного отчета передаем только массив с ненайденными элементами
     	  	echo json_encode(array("report"=>$arReport["deals_nofound"], "report_type"=>$reportType, "tmpFilePath"=>$saveTmpFilePath, "uploadFilePath"=>$uploadFilePath, "result"=>"step2"));
     		//echo json_encode(array("report"=>$arReport, "report_type"=>$reportType, "tmpFilePath"=>$saveTmpFilePath, "uploadFilePath"=>$uploadFilePath, "result"=>"step2"));
			die();
	}


}

 //Шаг 2
 if(intval($_POST["step"])==2){ //Создаем и заполняем портфель
    //Если перешли ко 2 шагу через аякс (были ненайденные) то читаем распарсенный отчет из временного файла иначе в $arReport уже будет массив из первого шага в рамках текущего скрипта
	 if(count($arReport["deals"])<=0 && isset($_POST["tmpFilePath"])){
	 	   //Читаем распарсенные данные из временного файла и потом его удаляем
			$arReport =  json_decode(file_get_contents($_POST["tmpFilePath"]), true);
		  	unlink($_POST["tmpFilePath"]);
	 }

	 if(!isset($reportType) && isset($_POST["report_type"])){
	  $reportType = htmlspecialchars($_POST["report_type"]);
	 }
	 $arFindedStep2 = array();

	 //Находим определенные пользователем активы
	 if(isset($_POST["nofoundArray"]) && count($_POST["nofoundArray"])){

		$arNofoundArray = $_POST["nofoundArray"];
		foreach($arNofoundArray as $kNf => $nfItem){
			if(strpos($nfItem["found"], " - ") === false){  //Если юзер не выбрал указал ненайденный актив вручную - не ищем элемент.
				unset($arFindedStep2[$nfItem["isin"]]);
				continue;
				}
			$nfItem["found"] = explode(" - ", $nfItem["found"]);
			$nfItem["found"] = $nfItem["found"][0];
		 	$finded = $CHistory->findItem(array("isin"=>$nfItem["found"]));


			if($finded["item"]!=false){
			 $arFindedStep2[$nfItem["isin"]] = $finded;
			} else {
				unset($arFindedStep2[$nfItem["isin"]]);
			}
		}
	 }

  //Добавляем определенные пользователем активы к соответствующим сделкам
  foreach($arReport["deals"] as $k=>$arDeal){
  	if(array_key_exists($arDeal["isin"], $arFindedStep2)){
	 $arReport["deals"][$k]["finded"] = $arFindedStep2[$arDeal["isin"]];
  	}
  	if(array_key_exists($arDeal["ticker"], $arFindedStep2)){
	 $arReport["deals"][$k]["finded"] = $arFindedStep2[$arDeal["ticker"]];
  	}
  }
	 if(count($arReport["deals"])>0 || count($arReport["cashflow"])>0){ //Если есть или сделки или ден. поток
	  if(intval($_POST["portfolio_id"])>0){
//	  	CLogger::Br_loader_2_debug("exist portf=".$_POST["portfolio_id"]);
		$arPortfolio = array("result"=>"ok", "id"=>intval($_POST["portfolio_id"]));
	  } else {
	   $arPortfolio = $CHistory->createPortfolio($_POST["portfolio_name"], $_POST["report_type"], $arReport["deals"]["date_period"], $_POST["broker_report_file_name"]);
//		CLogger::Br_loader_2_debug("created portf=".print_r($arPortfolio, true));
	  }
	 } else if(count($arReport["deals"])==0 && count($arReport["cashflow"])==0){ //Если загружается пустой портфель
	  if(intval($_POST["portfolio_id"])>0){
	  	//CLogger::Br_loader_2_debug("exist portf=".$_POST["portfolio_id"]);
		$arPortfolio = array("result"=>"ok", "id"=>intval($_POST["portfolio_id"]));
	  } else {
	   $arPortfolio = $CHistory->createPortfolio($_POST["portfolio_name"], $_POST["report_type"], $arReport["deals"]["date_period"], $_POST["broker_report_file_name"]);
		}
	 }

	 //Если создан портфель то наполним его данными из отчета
	 if(count($arPortfolio)>0 && $arPortfolio["result"]=="ok"){

	 	$begin = microtime(true);
		 $resultFill = $CHistory->fillPortfolioFromReport2($arPortfolio["id"], $arReport, $arPortfolio, $reportType, $_POST["broker_report_file_name"]);
			if(count($resultFill)>0){
			  		$CHistory->CPortfolio->clearCacheComplex($pid); ($arPortfolio["id"]);
			  echo json_encode(array("result"=>$resultFill["result"], "message"=>$resultFill["message"], "id"=>$arPortfolio["id"], "fillTime"=>$resultFill["time"]));
			} else {
			  echo json_encode(array("result"=>"error", "message"=>"Заполнения портфеля сделками не случилось", "id"=>0, "fillTime"=>0));
			}
			unlink($uploadFilePath);
	 		die();
	 } else {
		   unlink($uploadFilePath);
     		echo json_encode(array("result"=>"error", "message"=>"portfolio are not created!"));
	 		die();
	 }
    unlink($uploadFilePath);
}