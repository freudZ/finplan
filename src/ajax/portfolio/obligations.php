<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
CModule::IncludeModule("iblock");

$arReturn = array(
	"suggestions" => array(

	)
);
$pid = htmlspecialchars($_GET['pid']);
$date = htmlspecialchars($_GET['date']);
if(!empty($pid) && !empty($date)){
$CPortfolio = new CPortfolio();
$arReturn["cache"] = $CPortfolio->getCacheOnBeginDate($pid, $date);
unset($CPortfolio);
}

if ($_REQUEST["query"]) {
	//$arFilter = Array("IBLOCK_ID"=>30, "NAME"=>"%".$_REQUEST["query"]."%", "ACTIVE"=>"Y");
	$arFilter = Array("IBLOCK_ID" => 27, [
		"LOGIC" => "OR",
		["NAME" => "%" . $_REQUEST["query"] . "%"],
		["PROPERTY_ISIN" => "%" . $_REQUEST["query"] . "%"],
	], "ACTIVE" => "Y", );

	$res = CIBlockElement::GetList(Array("sort" => "asc", "NAME" => "asc"), $arFilter, false, array("nPageSize" => 10), array("*", "IBLOCK_ID", "PROPERTY_EMITENT_ID", "PROPERTY_SECID", "PROPERTY_CURRENCYID", "PROPERTY_FACEVALUE", "PROPERTY_MATDATE", "PROPERTY_LASTPRICE", "PROPERTY_LEGALCLOSE", "PROPERTY_ACCRUEDINT"));
	$curDate = new DateTime(date());
	while ($ob = $res->GetNextElement()) {
		$item = $ob->GetFields();

		//���������� ���������, ������� ��������
		$matDate = new DateTime($item["PROPERTY_MATDATE_VALUE"]);
/*echo "<pre  style='color:black; font-size:11px;'>";
   print_r(array("PROPERTY_MATDATE_VALUE"=>$item["PROPERTY_MATDATE_VALUE"], "int"=>intval($matDate->format('Y')), "matDate"=>$matDate->format('d.m.Y')));
   echo "</pre>";*/
/*	 	if(!empty($item["PROPERTY_MATDATE_VALUE"]) && intval($matDate->format('Y'))>0)
		 if($matDate<$curDate) continue;*/


		//�������� ��������
		$arSelectC = Array("ID", "NAME", "IBLOCK_ID", "CODE");
		$arFilterC = Array("IBLOCK_ID" => IntVal(26), "ID" => $item["PROPERTY_EMITENT_ID_VALUE"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
		$resC = CIBlockElement::GetList(Array(), $arFilterC, false, Array("nTopCount" => 1), $arSelectC);
		if ($obC = $resC->GetNextElement()) {
			$arFieldsC = $obC->GetFields();

			//������� �������� ��������
			$arSelectCP = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_VAL_ED_FIRST");
			$arFilterCP = Array("IBLOCK_ID" => IntVal(29), "=NAME" => $arFieldsC["NAME"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$resCP = CIBlockElement::GetList(Array(), $arFilterCP, false, Array("nTopCount" => 1), $arSelectCP);
			while ($obCP = $resCP->GetNextElement()) {
				$arFieldsCP = $obCP->GetFields();
			}

		}

		$currencyId = $item['PROPERTY_CURRENCYID_VALUE'];
		$currencyId = $currencyId=='SUR'?'RUB':$currencyId;
		$portfolio = new CPortfolio();
		$lastWorkDate = $portfolio->getMonthEndWorkday(date());
		$currency_rate = getCBPrice( strtoupper($currencyId), $lastWorkDate);
		unset($portfolio);
		$price = ($item['PROPERTY_FACEVALUE_VALUE'] / 100) * (!empty($item['PROPERTY_LASTPRICE_VALUE']) ? $item['PROPERTY_LASTPRICE_VALUE'] : $item['PROPERTY_LEGALCLOSE_VALUE']) + $item['PROPERTY_ACCRUEDINT_VALUE'];
		$price_calc = $price;
		if(strpos(strtolower($currencyId),'rub')===false){
		  $price_calc = $price*$currency_rate;
		  $price_calc = round($price_calc,3);
		}

/*		global $USER;
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            if($arUser["LOGIN"]=="freud"){
echo "<pre  style='color:black; font-size:11px;'>";
   print_r($item);
   echo "</pre>";
            }*/
		$arSuggestion = array(
			"value" => $item["NAME"],
			"id" => $item["ID"],
			"code" => $item["CODE"],
			"url" => $item["DETAIL_PAGE_URL"],
			"lastprice" => $price_calc,
			"price" => $price_calc,
			"price_orig" => $price,
			"currency_rate" => $currency_rate,
			"inlot_cnt"=>1,
			"currencyId"=>$currencyId,
			"emitent_url" => "/lk/obligations/company/" . $arFieldsCP["CODE"] . "/",
			"emitent_name" => $arFieldsC["NAME"],
			"emitent_currency" => strpos($arFieldsC["PROPERTY_VAL_ED_FIRST_VALUE"], "����") !== false ? 'usd' : 'rub',
			"secid" => $item['PROPERTY_SECID_VALUE'],
			"cache" => $arReturn["cache"],
		);

		if(strpos(strtolower($currencyId),'rub')===false){
		  //$actionPriceValute = round($actionPrice/$currency_rate, 3);
		  $arSuggestion["price_valute"] = $price;
		  $arSuggestion["rate"] = floatval($currency_rate);
		}

		$arReturn["suggestions"][] = $arSuggestion;
	}
}

echo json_encode($arReturn);
