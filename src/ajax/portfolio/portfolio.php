<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
		$isTester = false;
		$arGroups = CUser::GetUserGroup($USER->GetID());
		if(in_array(9, $arGroups)){
		  $isTester = true;
		}

	if(CLOSE_PORTFOLIO==true && !$GLOBALS["USER"]->IsAdmin() && !$isTester){
        $ret = array("result"=>"error", "message"=>"Работа с портфелями приостановлена, ведутся технические работы по модернизации функционала.", "error_text"=>"Работа с портфелями приостановлена, ведутся технические работы по модернизации функционала." , "data"=>"");
        echo json_encode($ret);
		die();
	}

if ($_POST["ajax"] == 'y') {
	$portfolio = new CPortfolio();
	//Получение списка портфолио для текущего пользователя
	if ($_POST['action'] == 'getPortfolioListForUser') {
	    Global $USER;
		echo $portfolio->getPortfolioListForUser($USER->GetID());
		unset($portfolio);
		die();
	}

	//Получает курс валюты на текущий или предыдущий рабочий день, если запрашиваемая дата приходится на выходной
	if ($_POST['action'] == 'getCurrencyRate') {

		if (!empty($_POST['data']['currencyId'])) {
			$ret = $portfolio->getCurrencyRate($_POST['data']['currencyId'], $_POST['data']['date']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
	}

	//Возвращает набор коэффициентов по портфелю
	if ($_POST['action'] == 'getIblockPortfolioCoeff') {
		if (intval($_POST['portfolioId']) > 0) {
			$ret = $portfolio->getIblockPortfolioCoeff($_POST['portfolioId'], $_POST['recalculate']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
	}

	//Сохраняет изменения в выбранном портфеле
	if ($_POST['action'] == 'updatePortfolio') {
		if (intval($_POST['data']['id']) > 0) {
			$ret = $portfolio->updatePortfolio($_POST['data']['id'], $_POST['data']);
		}
		unset($portfolio);
		die();
	}

	//Сохраняет описание портфеля
	if ($_POST['action'] == 'editDescrPortfolio') {
		if (intval($_POST['data']['portfolioId']) > 0) {
			$ret = $portfolio->editDescrPortfolio($_POST['data']['portfolioId'], $_POST['data']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
	}

	//Получает описание портфеля
	if ($_POST['action'] == 'getPortfolioDescription') {
		if (intval($_POST['portfolioId']) > 0) {
			$ret = $portfolio->getPortfolioDescription($_POST['portfolioId']);
		}
		echo $ret;
		unset($portfolio);
		die();
	}

	//Изменяет дату, количество и цену актива в сделке, изменяет связанную запись в истории портфеля
	if ($_POST['action'] == 'updatePortfolioActiveByDeal') {
		if (intval($_POST['portfolioId']) > 0) {

			echo $portfolio->updatePortfolioActiveByDeal(intval($_POST['portfolioId']), $_POST['data']);

		}
	}

	if ($_POST['action'] == 'getBondAmortValueByDate') {
		if (!empty($_POST['secid'])) {

			echo $portfolio->getBondAmortValueByDate($_POST['secid'], $_POST['date']);

		}
	}

	//Добавляет актив в портфель
	if ($_POST['action'] == 'addPortfolioActive') {
		if (intval($_POST['portfolioId']) > 0) {
			$ret = $portfolio->addPortfolioActive($_POST['portfolioId'], $_POST['data'], array(), ($_POST['owner']=="popup"?$portfolio->arHistoryActions["ACT_PLUS"]:""));
		}	echo json_encode($ret);
		unset($portfolio);
		die();
	}

	//Удаление активов портфеля
	if ($_POST['action'] == 'deletePortfolioActive') {
		//$_POST['data'] = {'portfolioId':portfolioId,'activeId':activeId,'cnt':cnt, 'price':price};
		if (intval($_POST['data']['portfolioId']) > 0) {
			$ret = $portfolio->deletePortfolioActive($_POST['data']['portfolioId'], $_POST['data'], array(), ($_POST['owner']=="popup"?$portfolio->arHistoryActions["ACT_MINUS"]:""));
		}
		echo $ret;
		unset($portfolio);
		die();
	}

	//Внесение налога или комиссии на базе продажи валюты
	if ($_POST['action'] == 'insertTaxComission') {
		//$_POST['data'] = {'portfolioId':portfolioId,'activeId':activeId,'cnt':cnt, 'price':price};
		if (intval($_POST['portfolioId']) > 0) {
		  //	$ret = $portfolio->deletePortfolioActive($_POST['data']['portfolioId'], $_POST['data'], array(), ($_POST['owner']=="popup"?$portfolio->arHistoryActions["ACT_MINUS"]:""));
			$ret = $portfolio->deletePortfolioCurrency($_POST['portfolioId'], $_POST['data'], array(), $portfolio->arHistoryActions["CACHE_MINUS"], false, 'user');
		}
		echo $ret;
		unset($portfolio);
		die();
	}


 	//Удаляет сделку и всю ее историю из портфеля  //TODO удалить после тестирования
	if ($_POST['action'] == 'deleteDealWithHistory') {
		//$_POST['data'] = {'portfolioId':portfolioId,'activeId':activeId,'cnt':cnt, 'price':price};
		if (intval($_POST['data']['portfolioId']) > 0) {
			$ret = $portfolio->deleteDealWithHistory($_POST['data']['portfolioId'], $_POST['data']['dealId']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
	}


  //Определяет наличие мультисделок в портфеле
  if ($_POST['action'] == 'isSimpleDeals') {
    $ret = $portfolio->isSimpleDeals($_POST['data']['portfolioId']);
    echo json_encode($ret?'Y':'N');
		unset($portfolio);
		die();
  }

    //Сохраняет данные ребалансировки пакетно сразу для всех активов за раз
    if ($_POST['action'] == 'setAutobalance') {
        $ret = $portfolio->setAutobalance(intval($_POST['portfolioId']), $_POST['data']);
        echo json_encode($ret);
        unset($portfolio);
        die();
    }


    //Считает и возвращает среднее значение амортизации по сделкам облиги в портфеле
    if ($_POST['action'] == 'getMidRedemptionValues') {
        $ret = $portfolio->getMidRedemptionValues(intval($_POST['pid']), $_POST['activeId']);
        echo json_encode($ret);
        unset($portfolio);
        die();
    }
	//Создание нового портфеля
	if ($_POST['action'] == 'addPortfolio') {
		if (count($_POST['data']) > 0 && empty($_POST['data']['add_to_exist_portfolio'])) {
		  $ret = $portfolio->addPortfolio($_POST['data']);
		  $portfolio->setSubzeroCacheOn($ret['id'], 'Y');
		} else if(count($_POST['data']) > 0 && !empty($_POST['data']['add_to_exist_portfolio'])){
		  $ret = array('result'=>'ok', 'id'=>intval($_POST['data']['add_to_exist_portfolio']));
		}

	  //Если портфель добавлен из радара - то проверяем и добавляем в него выбранные активы
	  //if(count($_POST['data']['actives_list'])>0 && $ret['id']>0){
	  if(count($_POST['data']['actives_list'])>0){
				$idActive = 0;
				$arCodes = array();
		 foreach($_POST['data']['actives_list'] as $active){
		 	if($active['active_type']=='share' || $active['active_type']=='share_usa' || $active['active_type']=='share_usa' || $active['active_type']=='share_etf' || $active['active_type']=='currency'){
			 $arCodes[$active['active_code']] = 0;
		 	}  else {
			 $arCodes[$active['active_secid']] = 0;
		 	}

		 }

		 $arCodes = $portfolio->getActivesIdByCodeOrSECID($arCodes);

		 foreach($_POST['data']['actives_list'] as $active){

			if($active['active_type']=='share' || $active['active_type']=='share_usa' || $active['active_type']=='share_etf' || $active['active_type']=='currency'){
			 $idActive = $arCodes[$active['active_code']];
		 	}  else {
			 $idActive = $arCodes[$active['active_secid']];
		 	}

		 	$data = array('typeActive'=>$active['active_type'],
								'idActive'=>$idActive,
								'nameActive'=>$active['active_name'],
								'priceActive'=>$active['active_lastprice'],
								'cntActive'=>$active['active_cnt'],
								'dateActive'=>(new DateTime(date()))->format('d.m.Y'),
								'codeActive'=>$active['active_code'],
								'urlActive'=>$active['active_url'],
								'currencyEmitent'=>$active['active_emitent_currency'],
								'nameActiveEmitent'=>$active['active_emitent_name'],
								'urlActiveEmitent'=>$active['active_emitent_url'],
								'lastpriceActive'=>$active['active_lastprice'],
								'price_one'=>$active['active_price_one'],
								'lotsize'=>$active['active_lotsize'],
								'secid'=>$active['active_secid'],
								);
			if($active['active_type']=='currency'){
			  $data['insert']='Y';
			}
									//201690               $pid, $data = array(), $copy_data = array(), $cacheActivity = "", $auto = false, $no_recount_cache = false, $owner='user'
	 	  $ret1 =	$portfolio->addPortfolioActive($ret['id'], $data, array(), ($_POST['owner']=="popup"?$portfolio->arHistoryActions["ACT_PLUS"]:""), true, false, 'autobalance');


		 }

	  }


		echo json_encode($ret);
		unset($portfolio);
		die();
	}

	//Переименование портфеля
	if($_POST['action'] == 'renamePortfolio') {
		if (count($_POST['data']) > 0 && empty($_POST['data']['add_to_exist_portfolio'])) {
		  $ret = $portfolio->renamePortfolio($_POST['data']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
	}


	//Копирование нового портфеля
	if ($_POST['action'] == 'copyPortfolio') {
		$ret = $portfolio->copyPortfolio($_POST["data"]["portfolioId"], $_POST);
		echo json_encode($ret);
		unset($portfolio);
		die();
	}

	//Запись сумм портфеля при обновлении подвала таблицы активов
	if ($_POST['action'] == 'updatePortfolioStatSumm'){
	  $portfolio->updatePortfolioStatSumm($_POST['data']);
	}
	//Чтение сумм портфеля обновлении подвала таблицы активов
	if ($_POST['action'] == 'getPortfolioStatSumm'){
	  $ret = $portfolio->getPortfolioStatSumm($_POST['pid']);
		echo json_encode($ret);
		unset($portfolio);
		die();
	}
	//Удаление всего портфеля
	if ($_POST['action'] == 'deletePortfolio') {
		//$_POST['data'] = {'portfolioId':portfolioId,'activeId':activeId,'cnt':cnt, 'price':price};
		if (intval($_POST['data']['id']) > 0) {
			$ret = $portfolio->deletePortfolio($_POST['data']['id']);
		}
		echo $ret;
		unset($portfolio);
		die();
	}

	//Очистка содержимого портфеля
	if ($_POST['action'] == 'clearPortfolio') {
		//$_POST['data'] = {'portfolioId':portfolioId,'activeId':activeId,'cnt':cnt, 'price':price};
		if (intval($_POST['data']['id']) > 0) {
			$ret = $portfolio->clearPortfolio($_POST['data']['id']);
		}
		echo $ret;
		unset($portfolio);
		die();
	}

	//Получение массива для бек-теста портфеля
	if ($_POST['action'] == 'getBackTestData') {
		if (intval($_POST['portfolioId']) > 0) {
			$benchmark = '';
			$benchmarkType = '';
			$benchmarkCurrency = 'rub';
			if(isset($_POST['benchmark']) && !empty($_POST['benchmark'])){
				$benchmark = $_POST['benchmark'];
				$benchmarkType = $_POST['benchmarkType'];
				$benchmarkCurrency = $_POST['benchmarkCurrency'];
			}
			$portfolio_benchmark = '';
			if(isset($_POST['protfolio_benchmark']) && !empty($_POST['protfolio_benchmark'])){
				$portfolio_benchmark = $_POST['protfolio_benchmark'];
			}

			//Строим график за 4,5 года = 4 года 6 месяцев
			$ret = $portfolio->getBackTestData($_POST['portfolioId'], 4.6, $benchmark, $portfolio_benchmark, $benchmarkType, $benchmarkCurrency);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
	}

	//Получение значения из сессии
	if ($_POST['action'] == 'getPortfolioStatesFromSession') {
		if (intval($_POST['portfolioId']) > 0) {
			$ret = $portfolio->getPortfolioStatesFromSession($_POST['portfolioId'], $_POST['session_param']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
	}
	//Сохранение значения в сессии
	if ($_POST['action'] == 'setPortfolioStatesToSession') {
		if (intval($_POST['portfolioId']) > 0) {
			$ret = $portfolio->setPortfolioStatesToSession($_POST['portfolioId'], $_POST['session_param'], $_POST['session_value']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
	}

   if ($_POST['action'] == 'setSubzeroCacheOn'){
		if (intval($_POST['portfolioId']) > 0) {
			$ret = $portfolio->setSubzeroCacheOn($_POST['portfolioId'], $_POST['checked']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
   }

   if ($_POST['action'] == 'checkSubZeroCache'){
		if (intval($_POST['pid']) > 0) {
			$ret = $portfolio->checkSubZeroCache($_POST['pid']);
		}
		echo json_encode($ret);
		unset($portfolio);
		die();
   }

	//Обновить и пересчитать портфель
	if ($_POST['action'] == 'recalculatePortfolio') {

		if (intval($_POST['portfolioId']) > 0) {
			$ret = $portfolio->recountCacheOnPortfolio($_POST['portfolioId'], '');
			$portfolio->clearCacheComplex($_POST['portfolioId'], 'recalculatePortfolio ajax');//Очистка всего кеша по портфелю
		} else {
			$ret = array("result"=>"error", "message"=>"Не указан id портфеля");
		}
		echo json_encode($ret);
		unset($portfolio);
		die();

	}


	if ($_POST['action'] == 'clearcachePortfolio') {

		if (intval($_POST['portfolioId']) > 0) {
			$portfolio->clearCacheComplex($_POST['portfolioId'], 'clearcachePortfolio ajax');//Очистка всего кеша по портфелю
			$ret = array("result"=>"success", "message"=>"Кеш портфеля очищен");
		} else {
			$ret = array("result"=>"error", "message"=>"Не указан id портфеля");
		}
		echo json_encode($ret);
		unset($portfolio);
		die();

	}

	//Получение массива истории портфеля в json для графика и таблицы
    if ($_POST['action'] == 'getHistoryStartDate') {
		 $result = $portfolio->getHistoryStartDate($_POST['portfolioId']);
        echo json_encode($result);
        unset($portfolio);
        die();
	 }

	 //Возвращает количества активов по типам для подписей фильтра по типам активов на вкладке оценки портфелей
    if ($_POST['action'] == 'getPortfolioActivesCnt') {
		 $result = $portfolio->getPortfolioActivesCnt($_POST['pid']);
        echo json_encode($result);
        unset($portfolio);
        die();
	 }


	//Получение массива истории портфеля в json для графика и таблицы
    if ($_POST['action'] == 'getPortfolioHistory') {

        $ret = array("result"=>"error", "message"=>"Нет данных или не передан id портфеля", "data"=>"");
        if (intval($_POST['portfolioId']) > 0) {
        	   //if($_POST["clearCache"]=="Y")
        		   //$portfolio->cleanPortfolioHistory(intval($_POST['portfolioId']));
            $retHistory = $portfolio->getHistoryDataForGraph($_POST['portfolioId'], $_POST['filter']);
				if(count($retHistory)>0){
					$result = 'success';
					$message = 'Данные истории получены';
				} else {
					$result = 'empty';
					$message = 'Исторических данных не найдено';
				}
				$ret = array("result"=>$result, "message"=>$message, "data"=>$retHistory);
        }
        echo json_encode($ret);
        unset($portfolio);
        die();
    }

  //Удаление сделки из истории портфеля
	 if($_POST['action'] == 'deleteHistoryDeal'){
        $ret = json_encode(array("result"=>"error", "message"=>"Нет данных или не передан id портфеля"));
        if (intval($_POST['portfolioId']) > 0) {
            $ret = $portfolio->deleteHistoryDeal($_POST['portfolioId'], $_POST['data']['histRowId']);
			  //	$retdeleteHistoryDeal = json_decode($retdeleteHistoryDeal);
        }
        echo $ret;
        unset($portfolio);
        die();
    }

  //Редактирование сделки из истории портфеля.
	 if($_POST['action'] == 'editHistoryDeal'){
        $ret = array("result"=>"error", "message"=>"Нет данных или не передан id портфеля", "data"=>"");
        if (intval($_POST['portfolioId']) > 0) {
        	   $portfolio->cleanPortfolioHistory(intval($_POST['portfolioId']));
            $retEditHistoryDeal = $portfolio->editHistoryDeal($_POST['portfolioId'], $_POST['data']);

				$ret = array("result"=>$retEditHistoryDeal["result"], "message"=>"Редактирование сделки из истории завершено", "hist_start_date"=>$retEditHistoryDeal["hist_start_date"], "data"=>$retEditHistoryDeal);
        }
        echo json_encode($ret);
        unset($portfolio);
        die();
    }




} //if($_POST["action"]=='ajax')