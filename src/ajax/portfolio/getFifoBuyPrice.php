<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main;
CModule::IncludeModule("highloadblock");

/**
 * �������� ���� ������� ������ �� ������ ���� (����� ������ ������� ������)   � ������������ ���� �� ���� ������� ��������
 *
 */
$CPortfolio = new CPortfolio();

$activeType = $_POST["activeType"];
$activeCode = $_POST["activeCode"];
$pid = $_POST["pid"];
$arResult['fifo'] = $CPortfolio->getFifoBuyPrice($pid, $activeType, $activeCode);
$arResult['fifo']["Price"] = round($arResult['fifo']["price"],($activeType=='bond'?3:2));
//�������� ������������ ���� �� ����
$activeSecid = $_POST["activeSecid"];
$date = $_POST["date"];
$arResult['hist'] = $CPortfolio->getHistActionsPrices($activeType, $date, $activeSecid, $activeCode);

unset($CPortfolio);

echo json_encode($arResult);