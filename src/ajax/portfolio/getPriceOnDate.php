<?php require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main;
CModule::IncludeModule("highloadblock");

/**
 * Получает цену актива на конкретную дату
 *
 */


$activeType = htmlspecialchars($_POST["activeType"]);
$activeCode = htmlspecialchars($_POST["activeCode"]);
$activeSecid = htmlspecialchars($_POST["activeSecid"]);
$activeCurrency = htmlspecialchars($_POST["currency"]);
//$date = htmlspecialchars($_POST["date"]);
$date = $_POST["date"];
$arResult = array("price"=>0, "price_valute"=>0, "rate"=>0);
if(strlen($date)==10) {//Ограничение на обработку только полной даты
    
    $CPortfolio = new CPortfolio();
    $arResult = $CPortfolio->getHistActionsPrices($activeType, $date, $activeSecid, $activeCode, $activeCurrency);
    
    if (isset($_POST["getCache"]) && htmlspecialchars($_POST['getCache']) == 'Y') {
        $arResult["cache"] = $CPortfolio->getCacheOnBeginDate(htmlspecialchars($_POST['pid']), $date);
    }
    unset($CPortfolio);
}


echo json_encode($arResult);