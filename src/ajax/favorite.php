<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main;

$r = Main\Context::getCurrent()->getRequest();


CModule::IncludeModule("highloadblock");
CModule::IncludeModule("iblock");
global $USER;

if($USER->IsAuthorized()) {
	$arTypesIB = array(
		"blog" => 5,
		"obligation" => 30,
		"obligation_company" => 29,
		"usa_company" => 44,
		"action" => 33,
		"etf" => 33,
		"action_usa" => 56,
	);
	
	if($r->get("type")!="delete"){
		$res = CIBlockElement::GetByID($r->get("id"));
		if($item = $res->GetNext()){
			if($item["IBLOCK_ID"]!=$arTypesIB[$r->get("type")]){
				exit();
			}
		} else {
			exit();
		}
	}

	$hlblock = HL\HighloadBlockTable::getById(12)->fetch();
	$entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $entity->getDataClass();

	$main_query = new Entity\Query($entity);
	$main_query->setOrder(array("ID" => "desc"));
	$main_query->setSelect(array("ID"));
	
	if($r->get("type")=="delete"){
		$main_query->setFilter(array("UF_USER" => $USER->GetID(), "UF_ITEM"=>$r->get("id")));
	} else {
		$main_query->setFilter(array("UF_USER" => $USER->GetID(), "UF_ITEM"=>$r->get("id"), "UF_TYPE" => $r->get("type")));
	}
		
	$exec = $main_query->exec();
	$exec = new CDBResult($exec);
	if ($item = $exec->Fetch()) {
		$entity_data_class::delete($item["ID"]);

		echo json_encode(array(
			"added" => false
		));
	} elseif($r->get("type")!="delete") {
		$entity_data_class::add(array(
			"UF_USER" => $USER->GetID(),
			"UF_ITEM" => $r->get("id"),
			"UF_TYPE" => $r->get("type"),
		));

		echo json_encode(array(
			"added" => true
		));
	}
}