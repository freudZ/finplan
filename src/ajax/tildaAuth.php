<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$_REQUEST["DATE"] = date("d-m-Y H:i:s");
file_put_contents($_SERVER["DOCUMENT_ROOT"]."/log.txt", print_r($_REQUEST, true), FILE_APPEND);

$data = array(
	"email" => $_REQUEST["email"],
	"name" => $_REQUEST["name"],
	"phone_final" => $_REQUEST["phone_final"],
	"landing" => $_REQUEST['landing'],
	"utm_source" => $_REQUEST['utm_source'],
	"utm_campaign" => $_REQUEST['utm_campaign'],
	"utm_medium" => $_REQUEST['utm_medium'],
	"utm_personal" => $_REQUEST['utm_personal'],
	"utm_term" => $_REQUEST['utm_term'],
    "t_form" => $_REQUEST['t_form'],
	"mc_list" => $_REQUEST['mc_list'],
	"mc_interes" => $_REQUEST['mc_interes'],
);
unset($_REQUEST);
foreach ($data as $field=>$val){
	$_REQUEST[$field] = $val;
}


$_REQUEST["tilda"] = true;

$uid=0;
$added = new CrmAdder();
$added->checkRequired()->add($_REQUEST["landing"], true);
$uid = $added->checkRequired()->getNewUserId();

//echo json_encode("uid=".$added->getNewUserId());
/* global $USER;
   $rsUser = CUser::GetByID($added->NewUserId);
   $arUser = $rsUser->Fetch();*/


//$userId = $added;


require_once($_SERVER["DOCUMENT_ROOT"]."/api/mailchimp/MailChimp.php");

$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');

$list_id = 'b6050240b6';
$interes_id = '0ac7b87356';

if($_REQUEST["mc_list"]){
	$list_id = $_REQUEST["mc_list"];
}
if($_REQUEST["mc_interes"]){
	$interes_id = $_REQUEST["mc_interes"];
}
//CLogger::TildaAuth(print_r($_REQUEST, true));
//CLogger::TildaAuth("interes_id=".$interes_id);
//CLogger::TildaAuth("list_id=".$list_id);



$result = $MailChimp->post("lists/$list_id/members", [
	'email_address' => $_REQUEST["email"],
	'status'        => 'subscribed',
	"merge_fields" => [
		"EMAIL"=> $_REQUEST["email"],
		"FNAME"=> $_REQUEST["name"],
		"LNAME"=> "",
		"PHONE"=> $_REQUEST["phone_final"]
	],
	"interests"=> array(
		$interes_id => true
	)
]);
//CLogger::TildaAuth("result=".print_r($result, true));
/* global $USER;
   $rsUser = CUser::GetByID($userId);
   $arUser = $rsUser->Fetch();*/
echo json_encode($uid);
?>