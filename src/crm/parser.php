<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}
$countFounded = 0;
$arFields = array(
	"следующий звонок" => 5,
	"продажи" => 7,
	"приглашение" => 8,
	"вебинары" => 9,
    "соцсети" => 10,
);

CModule::IncludeModule("iblock");
if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$inputFileName = $_FILES["file"]["tmp_name"];
	$i = 0;
	
	CModule::IncludeModule("highloadblock");

	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i<2){
				continue;
			}
			
			$arFilter = Array("IBLOCK_ID"=>DataTable::$ibId, "=PROPERTY_EMAIL"=>$data[0]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
			if($item = $res->GetNext())
			{
				if($data[$arFields["следующий звонок"]]){
					CIBlockElement::SetPropertyValues($item["ID"], DataTable::$ibId, $data[$arFields["следующий звонок"]], "NEW_CALL");
				}
				
				if($data[$arFields["продажи"]]){
					$val = explode("//", $data[$arFields["продажи"]]);
					$prop_val = [];
					foreach($val as $v){
						$v = trim($v);
						if(!$v){
							continue;
						}
						$prop_val[] = array("VALUE"=> array("TEXT" => date("d.m.Y")."`".$v, "TYPE"=>"text"));
					}
					
					if($prop_val){
						$prop_val = array_reverse($prop_val);
						CIBlockElement::SetPropertyValues($item["ID"], DataTable::$ibId, $prop_val, "SALE");
					}
				}
				
				if($data[$arFields["приглашение"]]){
					CIBlockElement::SetPropertyValues($item["ID"], DataTable::$ibId, $data[$arFields["приглашение"]], "NEXT_CALL_COME");
				}
				
				if($data[$arFields["вебинары"]]){
					$val = explode("//", $data[$arFields["вебинары"]]);
					$prop_val = [];
					foreach($val as $v){
						$v = trim($v);
						if(!$v){
							continue;
						}
						$prop_val[] = array("VALUE"=> $v);
					}
					if($prop_val){
						CIBlockElement::SetPropertyValues($item["ID"], DataTable::$ibId, $prop_val, "WEBINARS");
					}
				}

				if($data[$arFields["соцсети"]]){
				    $val = explode(",", $data[$arFields["соцсети"]]);
				    if(!is_array($val)){
				        $val = array($val);
                    }

					CIBlockElement::SetPropertyValues($item["ID"], DataTable::$ibId, $val, "SOCIALS");
				}
				
				$countFounded++;
			} else {
				$arNotFound[] = $data[0];
			}
		}
		
		if($countFounded){
			?>
			<p>
				Обнолено <?=$countFounded?> шт.
			</p>
			<?
		}
		if($arNotFound){
			?>
			<p>
				<b>Не найдены:</b> <?=implode(", ", $arNotFound)?>
			</p>
			<?
		}
		
		exit();
	}
}
?>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Файл с данными</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>