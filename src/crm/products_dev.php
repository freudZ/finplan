<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("Тест пересчета продуктов в инфоблок");?>
<?

  $cdb = new CrmDataBase;
	  CModule::IncludeModule("iblock");

		$arProductsID = array();
		$arRadar      = array();
		$arRadarUSA   = array();
		$arRadarEnd   = array();
		$arReturn     = array();
		$arUsers      = array();
		$arUsersDates = array();

		//Получаем список пользователей по строкам из CRM
	  $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_USER", "PROPERTY_DATE_FIRST_BUY", "PROPERTY_PRODUCTS");
	  $arFilter = Array("IBLOCK_ID" => 19, "=PROPERTY_DATE_LAST_BUY"=>date('Y-m-d H:i:s',MakeTimeStamp("06.07.2020 11:07:59"))    );
	//  $arFilter = Array("IBLOCK_ID" => 19, ">=PROPERTY_DATE_LAST_BUY"=>date('Y-m-d H:i:s',MakeTimeStamp("01.01.1970 00:00:00")), "<=PROPERTY_DATE_LAST_BUY"=>date('Y-m-d H:i:s',MakeTimeStamp("01.01.1970 23:59:59"))    );
	  //$arFilter = Array("IBLOCK_ID" => 19, "=PROPERTY_EDIT"=>"Y"    );
	  $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);

	  while( $ob = $res->fetch()){
	  	$arUsers[$ob["PROPERTY_USER_VALUE"]] = $ob["ID"];
	  }

/*	  echo "<pre  style='color:black; font-size:11px;'>";
        print_r($arUsers);
        echo "</pre>";*/
		$arFilter = Array("IBLOCK_ID" => 11, "!PROPERTY_PAYED" => false, "PROPERTY_USER" => array_keys($arUsers));
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, false);

		while ($ob = $res->GetNextElement()) {
			$item          = $ob->GetFields();
			$item["PROPS"] = $ob->GetProperties();

			$arProductsID[] = $item["PROPS"]["ITEM"]["VALUE"];
			foreach ($item["PROPS"]["DOP_ITEMS"]["VALUE"] as $val) {
				$arProductsID[] = $val;
			}
			if ($item["PROPS"]["RADAR_END"]["VALUE"] && $item["PROPS"]["PAYED"]["VALUE"]) {
				$date = explode(" ", $item["NAME"]);
				try {
					$date = (new DateTime($date[0]))->getTimestamp();
				} catch (Exception $e) {
					unset($date);
				}

				$arRadar[$item["PROPS"]["USER"]["VALUE"]][]  = $item["PROPS"]["RADAR_END"]["VALUE"];
				$arRadarUSA[$item["PROPS"]["USER"]["VALUE"]] = $item["PROPS"]["ACCESS_USA"]["VALUE"] == 'Y' && empty($arRadarUSA[$item["PROPS"]["USER"]["VALUE"]]) ? 'Y' : '';

				if ($date && $date > $arRadarEnd[$item["PROPS"]["USER"]["VALUE"]] || !$arRadarEnd[$item["PROPS"]["USER"]["VALUE"]]) {
					$arRadarEnd[$item["PROPS"]["USER"]["VALUE"]] = $date;
				}
			}

			$arSales[] = $item;
		}


		$productsData = $cdb->getProductsForCronCalculate(array_unique($arProductsID));

		$curDt = new DateTime();

		//собираем данные
		foreach ($arSales as $item) {
			$ended = "";
			$endtext		  = "";
			$is_GS = strpos($productsData[$item["PROPS"]["ITEM"]["VALUE"]]["NAME"], "ГС") !== false ? true : false;

			$endStyle = "";
			if ($item["PROPS"]["ITEM_END"]["VALUE"] && $is_GS == true) {
				$dt    = new DateTime($item["PROPS"]["ITEM_END"]["VALUE"]);
				$ended = $dt->format("d.m.Y");
				if ($dt < $curDt) {//Пунктирная рамка для продуктов, которые закончились. Радары и ГС
					$endStyle .= "border: 2px dashed white";
					$endtext = " закончился";
				}
			}

			$date = explode(" ", $item["NAME"]);
			$date = $date[0];
			if(checkIsAValidDate($date)==true)
			  $arUsersDates[$item["PROPS"]["USER"]["VALUE"]][] = $date;
			$dopEnded = "(" . $date . ")";
			if(!empty($ended) && !$is_GS){
			  $ended = '';
			}

			$arrStyle = array();
			if (!empty($endStyle)) {
				$arrStyle[] = $endStyle;
			}

			$arStyles = array_merge($productsData[$item["PROPS"]["ITEM"]["VALUE"]]["STYLE"], $arrStyle);
			if(array_key_exists($item["PROPS"]["ITEM"]["VALUE"], $productsData))
			  $arReturn[$item["PROPS"]["USER"]["VALUE"]][] = array("NAME"=>$productsData[$item["PROPS"]["ITEM"]["VALUE"]]["NAME"].(!empty($ended)?$endtext:''), "DATE_START"=>$date, "DATE_END"=>$ended, "STYLE"=>$arStyles, "HTML"=>"");


			foreach ($item["PROPS"]["DOP_ITEMS"]["VALUE"] as $val) {
				$arReturn[$item["PROPS"]["USER"]["VALUE"]][] = str_replace("</div>", $dopEnded . "</div>", $productsData[$val]);
			}
		}

		//Радар
		foreach ($arRadar as $userId => $dates) {
			$arTmpRadar = array();
			$maxItem      = "";
			$radarUsaHTML = "";
			$radarBg      = "blue";
			$endtext		  = "";
			$arStyles = array();
			$prevDate = new DateTime();
			foreach ($dates as $date) {
				$dt = new DateTime($date);
				if ($prevDate <= $dt) {
					$prevDate = $dt;
				}
			}
			$maxItem = $prevDate;

			$date = FormatDate("d.m.Y", $arRadarEnd[$userId]);

			if ($arRadarUSA[$userId] == 'Y' && empty($radarUsaHTML)) {
				$radarUsaHTML = ' +<img src="' . SITE_TEMPLATE_PATH . '/img/flags/us.png\"/>';
				$radarBg      = '#3399FF';
			}
			if ($maxItem < new DateTime()) {
			$arStyles = array_merge($productsData[$item["PROPS"]["ITEM"]["VALUE"]]["STYLE"], array("background:$radarBg", "border: 2px dashed white"));
			$endtext = " закончился";
			}

			  if(checkIsAValidDate($date)==true)
			    $arUsersDates[$userId][] = $date;

			$arValueRadar = array("NAME"=>"Радар1".$endtext.(!empty($radarUsaHTML)?" США":""), "DATE_START"=>$date, "DATE_END"=>$maxItem->format("d.m.Y"), "STYLE"=>$arStyles, "HTML"=>$radarUsaHTML);
			$md5 = "Радар".$endtext.(!empty($radarUsaHTML)?" США":"").$date.$maxItem->format("d.m.Y");
			if(!array_key_exists($md5, $arTmpRadar)){
				$arTmpRadar[$md5] = $arValueRadar;
				$arReturn[$userId][] = $arValueRadar;
			}

		}


		function checkIsAValidDate($myDateString){
  			  return strpos($myDateString, ".");
			}

/*		      echo "<pre  style='color:black; font-size:11px;'>";
            print_r($arUsersDates);
            echo "</pre>";*/




			//Запись в инфоблок
			foreach($arUsers as $uid=>$rowId){
				if(array_key_exists($uid, $arReturn)){

				  		usort($arUsersDates[$uid], function ($item1, $item2) {
					    return (new DateTime(date($item2)))  <=> (new DateTime(date($item1)));
						});


					echo $uid."-".$rowId."<pre  style='color:black; font-size:11px;'>";
					   print_r(reset($arUsersDates[$uid]));
				   echo "</pre>";   // pollv@mail.ru
				    	CIBlockElement::SetPropertyValuesEx(intval($rowId), 19, array("DATE_LAST_BUY" => reset($arUsersDates[$uid])));
				  	  	CIBlockElement::SetPropertyValuesEx(intval($rowId), 19, array("EDIT" => "06.07.2020"));
				}


			}
		      echo "<pre  style='color:black; font-size:11px;'>";
            print_r($arUsersDates);
            echo "</pre>";


			foreach($arReturn as $uid=>$data){
				$arValues = array();
				$arValuesDescr = array();
				$value = '';
				$descr = '';

				if(is_array($data)){
					for($i=0; $i<count($data); $i++){
						$value = $data[$i]["NAME"];
						$descr = urlencode(json_encode($data[$i]));
					   $arValues[] = array("VALUE"=>$value, "DESCRIPTION"=>"");
					   $arValuesDescr[] = array("VALUE"=>$descr, "DESCRIPTION"=>"");

					}
				}

				if(intval($arUsers[$uid])>0){
/*					CIBlockElement::SetPropertyValuesEx(intval($arUsers[$uid]), 19, array("PRODUCTS" => Array ("VALUE" => array("del" => "Y"))));
					CIBlockElement::SetPropertyValuesEx(intval($arUsers[$uid]), 19, array("PRODUCTS_PROPS" => Array ("VALUE" => array("del" => "Y"))));
					CIBlockElement::SetPropertyValuesEx(intval($arUsers[$uid]), 19, array("PRODUCTS" => $arValues));
				  	CIBlockElement::SetPropertyValuesEx(intval($arUsers[$uid]), 19, array("PRODUCTS_PROPS" => $arValuesDescr));*/
					}
			};

 ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>