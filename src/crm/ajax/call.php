<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
function convertPhone($val){
	$val = str_replace(array("(", ")", "-", " ", "_"), "", $val);
	if(strlen($val)!=12){
		return false;
	}
	return $val;
}

if($_REQUEST["phone"]) {
    $phone = convertPhone($_REQUEST["phone"]);
    $user = CUser::GetByID($USER->GetID())->Fetch();
    if(!$user["WORK_PHONE"]){
        exit();
    }

	$apiKey = "4a91b8e381d9c40f66a4";
	$apiSecret = "bccb24edacad4b0cdb76";

	$zd = new Zadarma_API\Client($apiKey, $apiSecret);

	$params = [
		"from" => $user["WORK_PHONE"],
		"to" => $phone,
		//"sip" => "",
		//"predicted" => "",
	];
	$answer = $zd->call('/v1/request/callback/', $params, 'get');
	$answerObject = json_decode($answer, true);

	echo $answerObject["status"];
}