<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

$countFounded = 0;
$arFields = array(
	"дата" => 1,
);

CModule::IncludeModule("iblock");
if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$inputFileName = $_FILES["file"]["tmp_name"];
	$i = 0;

	CModule::IncludeModule("highloadblock");

	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i<2){
				continue;
			}

			$arFilter = Array("IBLOCK_ID"=>DataTable::$ibId, "=PROPERTY_EMAIL"=>$data[0]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
			if($item = $res->GetNext())
			{
				if($data[$arFields["дата"]]) {

					$vals = array();
					$res = CIBlockElement::GetProperty(DataTable::$ibId, $item["ID"], "sort", "asc", array("CODE" => "WEBINARS"));
					while ($ob = $res->GetNext())
					{
						$vals[] = $ob['VALUE'];
					}

					if(!in_array($data[$arFields["дата"]], $vals)) {
						$vals[] = $data[$arFields["дата"]];
						CIBlockElement::SetPropertyValues($item["ID"], DataTable::$ibId, $vals, "WEBINARS");

						$countFounded++;
					}
				}

				//$countFounded++;
			} else {
				$arNotFound[] = $data[0];
			}
		}

		if($countFounded){
			?>
			<p>
				Обнолено <?=$countFounded?> шт.
			</p>
			<?
		}
		if($arNotFound){
			?>
			<p>
				<b>Не найдены:</b> <?=implode(", ", $arNotFound)?>
			</p>
			<?
		}

		exit();
	}
}
?>
<form enctype="multipart/form-data" method="post">
	<div class="form-group">
		<label for="exampleInputFile">Файл с данными</label>
		<input type="file" name="file" id="exampleInputFile">
	</div>
	<button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>