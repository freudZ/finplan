<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Главная");

global $USER;
     if($USER->IsAuthorized())
	  if(!in_array(1, $USER->GetUserGroupArray()))
		if(!in_array(7, $USER->GetUserGroupArray())) {
			LocalRedirect("https://fin-plan.org");
		}


//CLogger::CRM_DDOS("host=".$_SERVER["HTTP_HOST"]." ip=".$_SERVER["REMOTE_ADDR"]);
$dt = new DataTable();
$columns = $dt->getColumn();

/*global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
        echo "admin=".($USER->IsAdmin()?'Y':'N')."<br>";
		  $ac = new CrmAccess();
		  $tv = $ac->getCurTableView();
		  echo $tv;
      }*/

global $crmAccess;
?>
<!--main content start-->
<section id="main-content">
	<div style="display:none" class="sb-toggle-right"></div>
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<div class="panel-body">
						<div class="r"></div>
                  <?$base = new CrmDataBase()?>

						<?if($crmAccess->getCurTableView()!="admin"):?>
							<div style="display:flex" class="btn-dates-wrap">
								<div style="margin-right:5px" class="btn-toolbar dates" role="toolbar" aria-label="Toolbar with button groups">
									<button type="button" class="btn btn-secondary btn-default" data-val="today">сегодня</button>
									<button type="button" class="btn btn-secondary" data-val="week">неделя</button>
									<button type="button" class="btn btn-secondary" data-val="date">На дату</button>
								</div>
								<input class="dateonlypicker-init dateval-input" type="text" value="<?=date("d.m.Y")?>">
							</div>
                        <?else:?>
                            <div id="dopExclude">
                                <div class="form-group">
                                    <label>Вебинары, даты - исключения</label>
                                    <input type="text" class="form-control" name="PROPERTY_WEBINARS">
                                </div>
                                <div class="form-group">
                                    <label>Продукты исключения</label>
                                    <br>
                                    <select multiple name="PRODUCTS[]" class="form-control select2_init" style="width: 100% !important;">
                                        <option value="">Выберите</option>
                                        <?$data = json_decode($base->getProducts("all"), true)?>
                                        <?foreach ($data as $item):?>
													     <?if(strpos($item["label"], "Радар")!==false):?>
														   <option value="<?=$item["value"]?>"><?=$item["label"]?></option>
														  <?else:?>
                                            	<option value="<?=$item["label"]?>"><?=$item["label"]?></option>
														  <?endif;?>
                                        <?endforeach;?>
                                    </select>
                                </div>
                                <div class="form-group">
										      <label>Учитывать&nbsp;</label>
												<span><input type="radio" class="" name="BONUS_ONLY" <?=($_REQUEST["BONUS_ONLY"]=="ALL" || !isset($_REQUEST["BONUS_ONLY"])?'checked=""':'')?> value="ALL"> всех</span> |
												<span><input type="radio" class="" name="BONUS_ONLY" <?=($_REQUEST["BONUS_ONLY"]=="Y"?'checked=""':'')?> value="Y"> только бонусных</span> |
												<span><input type="radio" class="" name="BONUS_ONLY" <?=($_REQUEST["BONUS_ONLY"]=="N"?'checked=""':'')?> value="N"> всех кроме бонусных</span>
                                </div>
                            </div>

						<?endif?>
						<div class="DTable_buttons"></div>
						<div class="adv-table">
                            <table class="display table table-bordered table-striped" id="dynamic-table"
                                   data-view="<?=$crmAccess->getCurTableView()?>"

                                   data-items='<?=DataTable::getItemTypes()?>'
                                   <? /*data-products='<?=$base->getProducts("all")?>'*/ ?>
                                   data-products-alt='<?=$base->getProducts("all", true)?>'
                                   data-managers='<?=$base->getManagers("json")?>'
                                   data-referals='<?//=$base->getReferals("json")?>'
                                   data-webinars='<?=$base->getAllWebinars("json")?>'
                                   data-mailchimp='<?=$base->getAllMainChimpLists("json")?>'
                                   <?/*data-timezone='<?=$base->getAllTimezones("json")?>'*/?>
                            >
								<thead>
									<tr>
										<?foreach($columns as $item):?>
											<th><?=$item["NAME"]?></th>
										<?endforeach?>
									</tr>
								</thead>
							</table>
						</div>
						
						<div class="clearfix"></div>
						<form id="complex_action">
							<span>Комплексное действие:</span><br>
                            <div style="display: flex">
                                <select class="form-control" style="max-width: 200px;margin-right: 10px">
                                    <option value="">Выберите</option>
                                    <option value="manager">Установить поле Менеджер</option>
                                    <option value="next_call">Установить поле Cледующий звонок</option>
                                    <option value="date_next_call">Установить поле Следующий звонок с приглашением</option>
                                    <option value="check_db_mailchimp">Сверить базу с MailChimp</option>
                                    <option value="make_segment_mailchimp">Создать рассылочный сегмент в MailChimp</option>
                                    <option value="leeloo_setTags">Установить тег в LeeLoo</option>
                                </select>

                                <button class="btn btn-success">Далее</button>
                            </div>
						</form>
						
						<div style="display:none" id="complex_action_html">
							<div data-item="manager">
								<?$data = new Card("system");
								$data = $data->getManagers();
								?>
								<select class="form-control" name="manager">
									<?foreach($data as $item):?>
										<option value="<?=$item["VALUE"]?>"><?=$item["TEXT"]?></option>
									<?endforeach?>
								</select>
							</div>
							<div data-item="next_call">
								<input class="form-control datepicker-init" type="text" name="date">
							</div>
							<div data-item="date_next_call">
								<input class="form-control datepicker-init" type="text" name="date">
							</div>
							<div data-item="check_db_mailchimp">
								Вы уверены?
							</div>
							<div data-item="make_segment_mailchimp">
								<p>Название сегмента:</p>
								<input class="form-control" type="text" name="name" value="Сегмент от <?=date("d.m.Y H:i:s")?>">
							</div>
							<div data-item="leeloo_setTags">
								<p>Выберите тег:</p>
								<select class="form-control" name="leeloo_tag">
								 <option value="" selected>Не выбран</option>
								 <? $CLeeLoo = new CCrmLeeLoo;
									 $arLLTags = $CLeeLoo->getTags();
								  ?>
								  <?foreach($arLLTags["data"]["tags"] as $k=>$v):?>
									<option value="<?= $v["id"] ?>"><?= $v["name"] ?></option>
								  <?endforeach;?>
								  <?unset($CLeeLoo, $arLLTags);?>
								</select>
							</div>

						</div>
						
						<div id="complex_popup" class="modal" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<form method="post" class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Комплексное действие</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary">Отправить</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	   
		<!-- page end-->
	</section>
</section>
<!--main content end-->

<!-- Right Slidebar start -->
<div class="sb-slidebar sb-right sb-style-overlay">
	<a href="#" class="close">
		<i class="fa fa-times-circle" aria-hidden="true"></i>
	</a>
	<br>
	<div id="ajax-info">
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
