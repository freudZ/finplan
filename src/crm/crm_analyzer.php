<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("Анализатор CRM");?>
<?

// D7
$cache = Bitrix\Main\Data\Cache::createInstance();
if ($cache->initCache(86400, "crm_doubles", "/crm"))
{
    $arNewCRM_items = $cache->getVars();
}
elseif ($cache->startDataCache())
{


/* кеш */
			$arCRMDataDbl_List = array();
 			$arFilter = Array("IBLOCK_ID"=>DataTable::$ibId);
			$res = CIBlockElement::GetList(Array(), $arFilter, array("PROPERTY_EMAIL"), false, array("*"));
			while($item = $res->GetNext())
			{
				if($item["CNT"]>1){
			 $arCRMDataDbl_List[$item["PROPERTY_EMAIL_VALUE"]] = $item;
			 }
			}
			unset($res);
		 // $arCRMDataDbl_List = array("vale-ricci@mail.ru"=>1);
echo "<br><br><br><br><br>";
echo "Кол-во дублирующихся email: ".count($arCRMDataDbl_List)."<br>";
         //Получаем сгруппированные по email записи с наборами свойств и метками MULTIPLE
			$arFilter = Array("IBLOCK_ID"=>DataTable::$ibId, "PROPERTY_EMAIL"=>array_keys($arCRMDataDbl_List));
			$arSelect = array("ID", "NAME", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*");
			//$res = CIBlockElement::GetList(Array("PROPERTY_EMAIL"=>"ASC", "DATE_CREATE"=>"ASC"), $arFilter, false, false, $arSelect);
			//$res = CIBlockElement::GetList(Array("PROPERTY_EMAIL"=>"ASC", "ID"=>"ASC"), $arFilter, false, array("nTopCount"=>100), $arSelect);
			$res = CIBlockElement::GetList(Array("PROPERTY_EMAIL"=>"ASC", "ID"=>"ASC"), $arFilter, false, false, $arSelect);
			while($row_item = $res->GetNextelement())
			{
			 $item = $row_item->GetFields();
			 $arProps = $row_item->GetProperties();
			 foreach($arProps as $k=>$v){
			   $propVal = array();
			   if($v["MULTIPLE"]=='Y'){

				  $propVal['VALUE'] = $v['VALUE'];
				  $propVal['PROPERTY_VALUE_ID'] = $v['PROPERTY_VALUE_ID'];
				  $propVal['MULTIPLE'] = 'Y';
				} else {
					$propVal = array();
					$propVal['VALUE'] = $v['VALUE'];
					$propVal['MULTIPLE'] = 'N';
				}
				$item['PROPERTIES'][$k] = $propVal;
			 }
			 //$item['PROPERTIES'] = $arProps;
			 $arCRM_items[strtolower($item["PROPERTY_EMAIL_VALUE"])][] = $item;
			}
			unset($res);


//Обходим и вычисляем склеиваемые поля, производим подклейку к первому элементу, остальные помещаем в список удаляемых
$arNewCRM_items = array();
$arDeleteCRM_items = array();
$arFirstCurrentItem = array();
$arFirstSales = array();
foreach($arCRM_items as $email=>$arItem){
  $arFirstCurrentItem = reset($arItem);

  //Получаем даты покупки для главной записи, если они есть
  $arFirstItemProps  = $arFirstCurrentItem["PROPERTIES"];
  if(!empty($arFirstItemProps["DATE_FIRST_BUY"]["VALUE"])){
	$minDateFirstBuy = new DateTime($arFirstItemProps["DATE_FIRST_BUY"]["VALUE"]);
  }else{
   $minDateFirstBuy = false;
  }

  if(!empty($arFirstItemProps["DATE_LAST_BUY"]["VALUE"])){
	 $maxDateLastBuy = new DateTime($arFirstItemProps["DATE_LAST_BUY"]["VALUE"]);
  }else{
	 $maxDateLastBuy = false;
  }

  //Сформируем массив продаж с ключом по дате
  $arFirstSales = array();
  foreach($arFirstItemProps["SALE"]["VALUE"] as $arSale){
	 $arExplSale = explode('`',$arSale["TEXT"]);
	 $arFirstSales[] = array("DATE"=>$arExplSale[0],"TEXT"=>$arSale["TEXT"]);
  }


  foreach($arItem as $k=>$v){
	if($k == 0 ) {continue; } //Получаем основную запись (самый ранний элемент по сортировке даты создания или ID и пропускаем его обработку, проверяем остальные)
	$arProps = $v["PROPERTIES"];

	 //Имя
	 foreach($arProps["NAME"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["NAME"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["NAME"]["VALUE"][] = $value;
		}
	 }

	 //Телефон
	 foreach($arProps["PHONE"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["PHONE"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["PHONE"]["VALUE"][] = $value;
		}
	 }

	 //Откуда  т.к. сейчас св-во не множественное - то преобразуем в массив
		$value = $arProps["ITEM"]["VALUE"];
		  if(!is_array($arFirstItemProps["ITEM"]["VALUE"])){
		  	$arFirstCurrentItem["PROPERTIES"]["ITEM"]["VALUE"] = array($arFirstCurrentItem["PROPERTIES"]["ITEM"]["VALUE"]);
			$arFirstItemProps = $arFirstCurrentItem["PROPERTIES"];
		  }
		if(!in_array($value, $arFirstItemProps["ITEM"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["ITEM"]["VALUE"][] = $value;
		}

	 //Вебинары, даты
	 foreach($arProps["WEBINARS"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["WEBINARS"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["WEBINARS"]["VALUE"][] = $value;
		}
	 }

    //Определение минимальной даты первой покупки и максимальной даты последней покупки
	 $currDateFirstBuy = new DateTime($arProps["DATE_FIRST_BUY"]["VALUE"]);
	 if($minDateFirstBuy==false){
	 	$minDateFirstBuy = $currDateFirstBuy;
	 }
	if($minDateFirstBuy>$currDateFirstBuy){
		$minDateFirstBuy = $currDateFirstBuy;
	}

	 $currDateLastBuy = new DateTime($arProps["DATE_LAST_BUY"]["VALUE"]);
	 if($maxDateLastBuy==false){
	 	$maxDateLastBuy = $currDateLastBuy;
	 }
	if($maxDateLastBuy<$currDateLastBuy){
		$maxDateLastBuy = $currDateLastBuy;
	}

  //Продажи
  foreach($arProps["SALE"]["VALUE"] as $value){
	if(!in_array($value["TEXT"], $arFirstSales)){
	 $arExplSale = explode('`',$arSale["TEXT"]);
	 $addToSales = true;
	 foreach($arFirstSales as $arFirstSalesItem){
	 	if($arFirstSalesItem["TEXT"]==$arSale["TEXT"]){
	 		$addToSales = false;
			break;
	 	}
	 }
/*	 echo "<pre  style='color:black; font-size:11px;'>";
	    print_r($arFirstSales);
	    echo "</pre>";*/
	 if($addToSales)
	   $arFirstSales[] = array("DATE"=>$arExplSale[0],"TEXT"=>$arSale["TEXT"]);
	 }
  }

  //UTM источник
  	 if(!is_array($arFirstCurrentItem["PROPERTIES"]["utm_source"]["VALUE"]) && !empty($arFirstCurrentItem["PROPERTIES"]["utm_source"]["VALUE"])){
  	 	$arFirstCurrentItem["PROPERTIES"]["utm_source"]["VALUE"] = array($arFirstCurrentItem["PROPERTIES"]["utm_source"]["VALUE"]);
  	 } else {
  	 	$arFirstCurrentItem["PROPERTIES"]["utm_source"]["VALUE"] = array();
  	 }
	   $arFirstItemProps = $arFirstCurrentItem["PROPERTIES"];
	   $value = $arProps["utm_source"]["VALUE"];
		if(!in_array($value, $arFirstItemProps["utm_source"]["VALUE"]) && !empty($value)){
		  $arFirstCurrentItem["PROPERTIES"]["utm_source"]["VALUE"][] = $value;
		}

  //UTM кампания
  	 if(!is_array($arFirstCurrentItem["PROPERTIES"]["utm_campaign"]["VALUE"]) && !empty($arFirstCurrentItem["PROPERTIES"]["utm_campaign"]["VALUE"])){
  	 	$arFirstCurrentItem["PROPERTIES"]["utm_campaign"]["VALUE"] = array($arFirstCurrentItem["PROPERTIES"]["utm_campaign"]["VALUE"]);
  	 } else {
  	 	$arFirstCurrentItem["PROPERTIES"]["utm_campaign"]["VALUE"] = array();
  	 }
	   $arFirstItemProps = $arFirstCurrentItem["PROPERTIES"];
	   $value = $arProps["utm_campaign"]["VALUE"];
		if(!in_array($value, $arFirstItemProps["utm_campaign"]["VALUE"]) && !empty($value)){
		  $arFirstCurrentItem["PROPERTIES"]["utm_campaign"]["VALUE"][] = $value;
		}

  //UTM medium
  	 if(!is_array($arFirstCurrentItem["PROPERTIES"]["utm_medium"]["VALUE"]) && !empty($arFirstCurrentItem["PROPERTIES"]["utm_medium"]["VALUE"])){
  	 	$arFirstCurrentItem["PROPERTIES"]["utm_medium"]["VALUE"] = array($arFirstCurrentItem["PROPERTIES"]["utm_medium"]["VALUE"]);
  	 } else {
  	 	$arFirstCurrentItem["PROPERTIES"]["utm_medium"]["VALUE"] = array();
  	 }
	   $arFirstItemProps = $arFirstCurrentItem["PROPERTIES"];
	   $value = $arProps["utm_medium"]["VALUE"];
		if(!in_array($value, $arFirstItemProps["utm_medium"]["VALUE"]) && !empty($value)){
		  $arFirstCurrentItem["PROPERTIES"]["utm_medium"]["VALUE"][] = $value;
		}

  //UTM term
  	 if(!is_array($arFirstCurrentItem["PROPERTIES"]["utm_term"]["VALUE"]) && !empty($arFirstCurrentItem["PROPERTIES"]["utm_term"]["VALUE"])){
  	 	$arFirstCurrentItem["PROPERTIES"]["utm_term"]["VALUE"] = array($arFirstCurrentItem["PROPERTIES"]["utm_term"]["VALUE"]);
  	 } else {
  	 	$arFirstCurrentItem["PROPERTIES"]["utm_term"]["VALUE"] = array();
  	 }
	   $arFirstItemProps = $arFirstCurrentItem["PROPERTIES"];
	   $value = $arProps["utm_term"]["VALUE"];
		if(!in_array($value, $arFirstItemProps["utm_term"]["VALUE"]) && !empty($value)){
		  $arFirstCurrentItem["PROPERTIES"]["utm_term"]["VALUE"][] = $value;
		}

  //IP
  	 if(!is_array($arFirstCurrentItem["PROPERTIES"]["IP"]["VALUE"]) && !empty($arFirstCurrentItem["PROPERTIES"]["IP"]["VALUE"])){
  	 	$arFirstCurrentItem["PROPERTIES"]["IP"]["VALUE"] = array($arFirstCurrentItem["PROPERTIES"]["IP"]["VALUE"]);
  	 } elseif(!is_array($arFirstCurrentItem["PROPERTIES"]["IP"]["VALUE"])) {
  	 	$arFirstCurrentItem["PROPERTIES"]["IP"]["VALUE"] = array();
  	 }
	   $arFirstItemProps = $arFirstCurrentItem["PROPERTIES"];
	   $value = $arProps["IP"]["VALUE"];

		if(!in_array($value, $arFirstItemProps["IP"]["VALUE"]) && !empty($value)){
		  $arFirstCurrentItem["PROPERTIES"]["IP"]["VALUE"][] = $value;
		}

	 //Соцсети
	 foreach($arProps["SOCIALS"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["SOCIALS"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["SOCIALS"]["VALUE"][] = $value;
		}
	 }

	 //Часовой пояс
	 foreach($arProps["HOURS"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["HOURS"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["HOURS"]["VALUE"][] = $value;
		}
	 }

	 //MAILCHIMP
	 foreach($arProps["MAILCHIMP"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["MAILCHIMP"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["MAILCHIMP"]["VALUE"][] = $value;
		}
	 }

	 //CITY
	 foreach($arProps["CITY"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["CITY"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["CITY"]["VALUE"][] = $value;
		}
	 }

	 //Дата изменения
		  $arFirstCurrentItem["PROPERTIES"]["DATE_UPDATE"]["VALUE"] = $arProps["DATE_UPDATE"]["VALUE"];

	 //Желаемые курсы
	 foreach($arProps["WANTS"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["WANTS"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["WANTS"]["VALUE"][] = $value;
		}
	 }

	 //Приглашения на вебинар
	 foreach($arProps["WEBINARS_COME"]["VALUE"] as $value){
		if(!in_array($value, $arFirstItemProps["WEBINARS_COME"]["VALUE"])){
		  $arFirstCurrentItem["PROPERTIES"]["WEBINARS_COME"]["VALUE"][] = $value;
		}
	 }
	  $arFirstCurrentItem["DELETED_ID"][] = $v["ID"];

	  //Добавляем в список на удаление
	  if($arFirstCurrentItem["ID"]!=$v["ID"]){
		  $arDeleteCRM_items[] = $v["ID"];
	  }

  }//Цикл по свойствам

  $arNewCRM_items[$arFirstCurrentItem["ID"]] = $arFirstCurrentItem;
  //Записываем даты покупки
  $arNewCRM_items[$arFirstCurrentItem["ID"]]["PROPERTIES"]["DATE_FIRST_BUY"]["VALUE"] = is_object($minDateFirstBuy)?$minDateFirstBuy->format("d.m.Y H:i:s"):'';
  $arNewCRM_items[$arFirstCurrentItem["ID"]]["PROPERTIES"]["DATE_LAST_BUY"]["VALUE"] = is_object($maxDateLastBuy)?$maxDateLastBuy->format("d.m.Y H:i:s"):'';

  //Сортируем массив продаж и заменяем в основной записи
  usort($arFirstSales, function ($a, $b) { return (strtotime($a["DATE"]) <=> strtotime($b["DATE"])); });
  $arNewCRM_items[$arFirstCurrentItem["ID"]]["PROPERTIES"]["SALE"]["VALUE"] = array_values($arFirstSales);
}

/* кеш */


    if ($isInvalid)
    {
        $cache->abortDataCache();
    }
    // ...
    $cache->endDataCache($arNewCRM_items);
}

CModule::IncludeModule("iblock");
$ibTmpId = 63;
$arOne = 0;
//$dontAdd = true;
foreach($arNewCRM_items as $id=>$row){
 if($dontAdd) break;
$el = new CIBlockElement;
$arProps = $row["PROPERTIES"];
$PROP = array();

$PROP["USER"] = $arProps["USER"]["VALUE"];
$PROP["NAME"] = $arProps["NAME"]["VALUE"];
$PROP["EMAIL"] = $arProps["EMAIL"]["VALUE"];
$PROP["PHONE"] = $arProps["PHONE"]["VALUE"];
$PROP["ITEM"] = $arProps["ITEM"]["VALUE"];
if(count($arProps["utm_source"]["VALUE"])>0){$PROP["utm_source"] = $arProps["utm_source"]["VALUE"];}
if(count($arProps["utm_campaign"]["VALUE"])>0){$PROP["utm_campaign"] = $arProps["utm_campaign"]["VALUE"];}
if(count($arProps["utm_medium"]["VALUE"])>0){$PROP["utm_medium"] = $arProps["utm_medium"]["VALUE"];}
if(count($arProps["utm_term"]["VALUE"])>0){$PROP["utm_term"] = $arProps["utm_term"]["VALUE"];}
$PROP["IP"] = $arProps["IP"]["VALUE"];
$PROP["CITY"] = $arProps["CITY"]["VALUE"];
$PROP["HOURS"] = $arProps["HOURS"]["VALUE"];
$PROP["DATE_CREATE"] = $arProps["DATE_CREATE"]["VALUE"];
$PROP["DATE_UPDATE"] = $arProps["DATE_UPDATE"]["VALUE"];
$PROP["SOCIALS"] = $arProps["SOCIALS"]["VALUE"];
$PROP["WEBINARS"] = $arProps["WEBINARS"]["VALUE"];
$PROP["SALE"] = $arProps["SALE"]["VALUE"];
$PROP["NEW_CALL"] = $arProps["NEW_CALL"]["VALUE"];
$PROP["WEBINARS_COME"] = $arProps["WEBINARS_COME"]["VALUE"];
$PROP["NEXT_CALL_COME"] = $arProps["NEXT_CALL_COME"]["VALUE"];
$PROP["MAILCHIMP"] = $arProps["MAILCHIMP"]["VALUE"];
$PROP["MAILCHIMP"] = $arProps["MAILCHIMP"]["VALUE"];
$PROP["DATE_FIRST_BUY"] = $arProps["DATE_FIRST_BUY"]["VALUE"];
$PROP["DATE_LAST_BUY"] = $arProps["DATE_LAST_BUY"]["VALUE"];
$PROP["WANTS"] = $arProps["WANTS"]["VALUE"];
$PROP["PROMOCODE"] = $arProps["PROMOCODE"]["VALUE"];
$PROP["CRM_ID"] = $row["ID"];
$PROP["DEL_ID"] = $row["DELETED_ID"];



$arLoadProductArray = Array(
  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  "IBLOCK_ID"      => $ibTmpId,
  "PROPERTY_VALUES"=> $PROP,
  "NAME"           => $PROP["EMAIL"],
  "ACTIVE"         => "Y",            // активен
  );

if($PRODUCT_ID = $el->Add($arLoadProductArray)){
  echo "New ID: ".$PRODUCT_ID."<br>";
  $arOne++;
  }
else
  echo "Error: ".$el->LAST_ERROR."<br>";

// if($arOne>3) break;

}






//echo "<pre  style='color:black; font-size:11px;'>";
  // print_r($arFirstItemProps);
  // print_r($arNewCRM_items);
  // "del: <br>".print_r($arDeleteCRM_items);
 //  echo "</pre>";
 ?>

<?
	$arViewProps = array(
	  "USER",
	  "NAME",
	  "EMAIL",
	  "PHONE",
	  "ITEM",
	  "IP",
	  "CITY",
	  "HOURS",
	  "DATE_CREATE",
	  "DATE_UPDATE",
	  "WEBINARS",
	  "SALE",
	  "NEW_CALL",
	  "WEBINARS_COME",
	  "NEXT_CALL_COME",
	  "MAILCHIMP",
	  "DATE_FIRST_BUY",
	  "DATE_LAST_BUY",
	  "WANTS",
	  "PROMOCODE",
	  "PROMOCODE_STATUS",
	  "REFERAL",
	);
	$arHideProps = array(
	  "LEELOO_ID",
	  "MANAGER",
	  "PROMOCODE",
	  "PROMOCODE_STATUS",
	  "REFERAL",
	);
	$deepArrayProp = array("SALE", "WEBINARS_COME");



	$demo = reset($arNewCRM_items);

	echo "<pre  style='color:black; font-size:11px;'>";
	   print_r($demo);
	   echo "</pre>";
?>
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
 <div style="width:100%; overflow-x: visible; overflow-y:hidden; overflow:scroll">
<!--
	<?//foreach($arCRM_items as $email=>$arData):?>
	<?foreach($arNewCRM_items as $id=>$row):?>
	<table class="table table-striped table-bordered table-condensed table-responsive" style="width: 100%; font-size: 12px;">
	<tr>
	 <td class="bg-info text-primary"><strong><?= $row["PROPERTIES"]["EMAIL"]["VALUE"] ?></strong></td>
	</tr>
	<tr>
	 <td>
	      <table class="table table-striped table-bordered table-condensed table-responsive" style="width: 100%;font-size: 12px;">
			    <?$header_showed = false;?>
					<?//foreach($arData as $k=>$row):?>

									 <?$arTable = array("td"=>'',"value"=>'');?>
									 <?foreach($row["PROPERTIES"] as $propCode=>$val){
									 	if(!in_array($propCode,$arHideProps)){
									  	$arTable["td"] .= "<th style=\"\">".$propCode."</th>";
										if(in_array($propCode, $deepArrayProp)){
											$value = '';
											foreach($val["VALUE"] as $k=>$v){
											 $value .= (strlen($value)>0?' / <br>':'').htmlspecialchars_decode($v["TEXT"]);
											}

										}  else {
											if(is_array($val["VALUE"])){
											  $value = implode(" / <br>",$val["VALUE"]);
											}  else {
											  $value = $val["VALUE"];
											}

										}
										$arTable["value"] .= "<td class=\"".(is_array($val["VALUE"])?"bg-warning":"")."\">".$value."</td>";
									 }
									 }
									 ?>
					<?if(!$header_showed):?>
									<tr>
									 <td>ID</td>
									 <td>ID удаление</td>
									 <?= $arTable["td"] ?>
									</tr>
					 <?$header_showed=true;?>
					<?endif;?>
									<tr>
									 <td><?=$row["ID"]?></td>
									 <td><?=implode("<br>",$row["DELETED_ID"])?></td>
									 <?= $arTable["value"] ?>
									</tr>

					<?//endforeach;?>
			 </table>
	 </td>
	</tr>
	</table>
	<?endforeach;?>
-->
	</div>
</div>
</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>