<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Главная");
if(!$USER->IsAdmin()){
	exit();
}
?>
<br><br><br><br><br><br>
<?
CModule::IncludeModule("iblock");
$ibID = 19;


function convertIB($ib){
	//множ поля
	$multiProps = array("NAME", "PHONE", "CITY", "HOURS");
	
	foreach($multiProps as $prop){
		$res = CIBlockProperty::GetByID($prop, $ib);
		if($ar_res = $res->GetNext()){
			if($ar_res["MULTIPLE"]=="N"){
				$ibp = new CIBlockProperty();
				$ibp->Update($ar_res["ID"], array(
					"MULTIPLE" => "Y"
				));
			}
		}
	}
	
	//добавление полей
	$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("CODE"=>"USER", "IBLOCK_ID"=>$ib));
	if (!$properties->GetNext())
	{
		 $arFields = Array(
			"NAME" => "Пользователь",
			"ACTIVE" => "Y",
			"SORT" => "5",
			"CODE" => "USER",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "UserID", 
			"IBLOCK_ID" => $ib,
		);

		$ibp = new CIBlockProperty;
		$PropID = $ibp->Add($arFields);
	}
	
	
	//даты
	$arDatesFields = array(
		"DATE_CREATE" => array(
			"Дата создания",
			"120",
		),
		"DATE_UPDATE" => array(
			"Дата изменения",
			"130",
		),
	);
	foreach($arDatesFields as $code=>$item){
		$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("CODE"=>$code, "IBLOCK_ID"=>$ib));
		if (!$properties->GetNext())
		{
			 $arFields = Array(
				"NAME" => $item[0],
				"ACTIVE" => "Y",
				"SORT" => $item[1],
				"CODE" => $code,
				"PROPERTY_TYPE" => "S",
				"USER_TYPE" => "DateTime", 
				"IBLOCK_ID" => $ib,
			);

			$ibp = new CIBlockProperty;
			$PropID = $ibp->Add($arFields);
		}
	}
	
	$arNewFields = array(
		"MANAGER" => array(
			"NAME" => "Менеджер",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "UserID",
			"MULTIPLE" => "N",
			"SORT" => 140
		),
		"SOCIALS" => array(
			"NAME" => "Соц сети",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "",
			"MULTIPLE" => "Y",
			"SORT" => 150
		),
		"WEBINARS" => array(
			"NAME" => "Вебинары",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "",
			"MULTIPLE" => "Y",
			"SORT" => 160
		),
		"SALE" => array(
			"NAME" => "Продажи",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "HTML",
			"MULTIPLE" => "Y",
			"SORT" => 170
		),
		"NEW_CALL" => array(
			"NAME" => "Следующий звонок",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "DateTime",
			"MULTIPLE" => "N",
			"SORT" => 180
		),
		"WEBINARS_COME" => array(
			"NAME" => "Приглашения на вебинар",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "HTML",
			"MULTIPLE" => "Y",
			"SORT" => 190
		),
		"NEXT_CALL_COME" => array(
			"NAME" => "Следующий звонок с приглашением",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "DateTime",
			"MULTIPLE" => "N",
			"SORT" => 200
		),
		"MAILCHIMP" => array(
			"NAME" => "Mailchimp листы",
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "",
			"MULTIPLE" => "Y",
			"SORT" => 210
		),
        "DATE_FIRST_BUY" => array(
            "NAME" => "Дата первой покупки",
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "DateTime",
            "MULTIPLE" => "N",
            "SORT" => 220
        ),
        "DATE_LAST_BUY" => array(
            "NAME" => "Дата последней покупки",
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "DateTime",
            "MULTIPLE" => "N",
            "SORT" => 230
        ),
	);	
	//добавление полей
	foreach($arNewFields as $code=>$data){
		$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("CODE"=>$code, "IBLOCK_ID"=>$ib));
		if (!$properties->GetNext())
		{
			 $arFields = Array(
				"NAME" => $data["NAME"],
				"ACTIVE" => "Y",
				"SORT" => $data["SORT"],
				"CODE" => $code,
				"PROPERTY_TYPE" => $data["PROPERTY_TYPE"],
				"USER_TYPE" => $data["USER_TYPE"], 
				"IBLOCK_ID" => $ib,
				"MULTIPLE" => $data["MULTIPLE"]
			);

			$ibp = new CIBlockProperty;
			$PropID = $ibp->Add($arFields);
		}
	}
	
}

convertIB($ibID);

BaseHelper::afterBuyAction(138036);
exit();

/*
конвертим по очереди, сначала больше двух, convertItem, потом меньше convertSimpleItem
*/
$arFilter = Array("IBLOCK_ID"=>$ibID, "PROPERTY_DATE_CREATE"=>false);
$res = CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_EMAIL"), false, array());
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();

	if($arFields["CNT"]<2){
		convertSimpleItem($arFields["PROPERTY_EMAIL_VALUE"], $ibID);		
		continue;
	}


	//convertItem($arFields["PROPERTY_EMAIL_VALUE"], $ibID);

	
	$l++;
	if($l>=10000){
		exit();
	}
}

$l = 0;
//выберем юзеров из продаж
$arFilter = Array("IBLOCK_ID"=>11);
$res = CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_USER"), false, array());
while($item = $res->GetNext())
{
	if($item["PROPERTY_USER_VALUE"]==1){
		continue;
	}

	$rsUser = CUser::GetByID($item["PROPERTY_USER_VALUE"]);
	$arUser = $rsUser->Fetch();

	$arFilter = Array(
		"IBLOCK_ID"=>$ibID,
		array(
			"LOGIC" => "OR",
			array("PROPERTY_USER" => $arUser["ID"]),
			array("PROPERTY_EMAIL" => $arUser["LOGIN"]),
		),
	);
	$res2 = CIBlockElement::GetList(array(), $arFilter, false, false, array());
	if(!$res2->SelectedRowsCount()){
		convertSaleItem($item["PROPERTY_USER_VALUE"], $arUser, $ibID);
	}
	$l++;
	if($l==10000){
		exit();
	}
}

//конверт для юзера из продажи
function convertSaleItem($userID, $arUser, $ibID){
	//множ поля объединения
	$combFelds = array("NAME", "PHONE", "CITY", "HOURS");

	//социальные поля
	$socFields = array("utm_source", "utm_campaign", "utm_medium", "utm_term");
	
	//последние данные с полей
	$lastFields = array("ITEM", "IP");
	
	//сгруппированные данные
	$arUserData = array(
		"EMAIL" => trim($arUser["LOGIN"]),
		"USER" => $arUser["ID"],
	);
	
	foreach($combFelds as $item){
		$arUserData[$item] = array();
	}
	foreach($socFields as $item){
		$arUserData[$item] = "";
	}	
	foreach($lastFields as $item){
		$arUserData[$item] = "";
	}
	$arUserData["DATE_CREATE"] = PHP_INT_MAX;
	$arUserData["DATE_UPDATE"] = false;
	
	
	$arFilter = Array("IBLOCK_ID"=>11, "PROPERTY_USER"=>$userID);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false);
	while($ob = $res->GetNextElement())
	{
		$fields = $ob->GetFields();
		$fields["PROPS"] = $ob->GetProperties();
		
		$arUserData["NAME"][] = $fields["PROPS"]["FIO"]["VALUE"];
		
		$val = convertPhone($fields["PROPS"]["PHONE"]["VALUE"]);
		
		if($val){
			$arUserData["PHONE"][] = $val;
		}
		
		if($fields["DATE_CREATE_UNIX"]<$arUserData["DATE_CREATE"]){
			$arUserData["DATE_CREATE"] = $fields["DATE_CREATE_UNIX"];
		}
		if($fields["TIMESTAMP_X_UNIX"]>$arUserData["DATE_UPDATE"]){
			$arUserData["DATE_UPDATE"] = $fields["TIMESTAMP_X_UNIX"];
		}
	}
	
	if($arUserData["DATE_CREATE"]){
		$arUserData["DATE_CREATE"] = date("d.m.Y H:i:s", $arUserData["DATE_CREATE"]);
	}
	if($arUserData["DATE_UPDATE"]){
		$arUserData["DATE_UPDATE"] = date("d.m.Y H:i:s", $arUserData["DATE_UPDATE"]);
	}
	
	foreach($combFelds as $item){
		if($arUserData[$item]){
			$arUserData[$item] = array_unique($arUserData[$item]);
		}
	}
	
	$el = new CIBlockElement;
	$arLoadProductArray = Array(
		"IBLOCK_ID" => $ibID,
		"NAME" => date("d.m.Y H:i:s"),
		"ACTIVE" => "N",
		"PROPERTY_VALUES" => $arUserData,
	);
	$el->Add($arLoadProductArray);
	if($el->LAST_ERROR){
		?><pre><?print_r($el->LAST_ERROR)?></pre><?
		exit();
	}
}


function convertSimpleItem($mail, $ibID){
	
	$arFilter = Array("IBLOCK_ID"=>$ibID, "=PROPERTY_EMAIL"=>$mail, "PROPERTY_DATE_CREATE"=>false);
	$res = CIBlockElement::GetList(array("ID"=>"asc"), $arFilter, false, false, array());
	if($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$props = $ob->GetProperties();
				
		$arUserData = array();
		
		//Есть ли пользователь
		$filter = Array("LOGIN_EQUAL" => trim($mail));
		$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
		if ($arUser = $rsUsers->Fetch()) {
			if($arUser["NAME"]){
				$arUserData["NAME"][] = $arUser["NAME"];
			}
			$arUserData["USER"] = $arUser["ID"];
		}
		
		$arUserData["EMAIL"] = trim($mail);
		
		
		$arUserData["DATE_CREATE"] = $arFields["DATE_CREATE"];
		$arUserData["DATE_UPDATE"] = $arFields["TIMESTAMP_X"];
	
		CIBlockElement::SetPropertyValuesEx($arFields["ID"], $ibID, $arUserData);
	}
}

function convertItem($mail, $ibID){
	if(!$mail){
		?>Нет мыла - <pre><?print_r($mail)?></pre><?
		exit();
	}
	
	//множ поля объединения
	$combFelds = array("NAME", "PHONE", "CITY", "HOURS");
	
	//социальные поля
	$socFields = array("utm_source", "utm_campaign", "utm_medium", "utm_term");
	
	//последние данные с полей
	$lastFields = array("ITEM", "IP");
	
	//сгруппированные данные
	$arUserData = array(
		"ID" => array(),
		"EMAIL" => trim($mail),
		"USER" => false,
	);
	
	foreach($combFelds as $item){
		$arUserData[$item] = array();
	}
	foreach($socFields as $item){
		$arUserData[$item] = "";
	}	
	foreach($lastFields as $item){
		$arUserData[$item] = "";
	}
	$arUserData["DATE_CREATE"] = false;
	$arUserData["DATE_UPDATE"] = false;
	
	//Есть ли пользователь
	$filter = Array("LOGIN_EQUAL" => trim($mail));
	$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
	if ($arUser = $rsUsers->Fetch()) {
		$arUserData["NAME"][] = $arUser["NAME"];
		$arUserData["USER"] = $arUser["ID"];
	}
	
	$i=1;
	$arFilter = Array("IBLOCK_ID"=>$ibID, "=PROPERTY_EMAIL"=>$mail);
	$res = CIBlockElement::GetList(array("ID"=>"asc"), $arFilter, false, false, array());
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$props = $ob->GetProperties();
		
		$arUserData["ID"][] = $arFields["ID"];
		
		foreach($combFelds as $item){
			foreach($props[$item]["VALUE"] as $val){
				$val = trim($val);
				if($item=="PHONE"){
					$val = convertPhone($val);
				}
				if(!$val){
					continue;
				}
				$arUserData[$item][] = $val;
			}
		}
		
		
		//социалки из самого первого тянем
		if($i==1){
			foreach($socFields as $item){
				if($props[$item]["VALUE"]){
					$arUserData[$item] = $props[$item]["VALUE"];
				}
			}
			
			$arUserData["DATE_CREATE"] = $arFields["DATE_CREATE"];
		}
		
		//актуальные данные
		foreach($lastFields as $item){
			if($props[$item]["VALUE"]){
				$arUserData[$item] = $props[$item]["VALUE"];
			}
		}
	
		$arUserData["DATE_UPDATE"] = $arFields["TIMESTAMP_X"];
		
		$i++;
	}
	
	foreach($combFelds as $item){
		if($arUserData[$item]){
			$arUserData[$item] = array_unique($arUserData[$item]);
		}
	}
	
	
	//обработка
	$lastItem = $arUserData["ID"][count($arUserData["ID"])-1];
	foreach($arUserData["ID"] as $id){
		if($id==$lastItem){
			continue;
		}
		
		//удалить элементы
		CIBlockElement::Delete($id);
	}
	
	CIBlockElement::SetPropertyValues($lastItem, $ibID, $arUserData);
}


function convertPhone($val){
	$val = str_replace(array("(", ")", "-", " ", "_"), "", $val);
	if(strlen($val)!=12){
		return false;
	}
	return $val;
}