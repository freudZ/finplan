<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//Загрузка макро-показателей в HL 39
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ADDED" =>array(),
		"UPDATED" =>array(),
		"ERR" => array(),
		"EMPTY_ISIN" => array()
	);

	$inputFileName = $_FILES["file"]["tmp_name"];

	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {


	$arFieldsNum = array();

		while (($item = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i==1){ //Собираем массив дат с ключами по номерам колонок
			}

			if($i>1){
				if($item[0]=='header'){
					continue;
				}
/*				      echo "<pre  style='color:black; font-size:11px;'>";
                  print_r($item);
                  echo "</pre>";*/
						if(empty($item[2])) $item[2] = array();
					 	CIBlockElement::SetPropertyValuesEx($item[0], 19, array("LEELOO_ID" => $item[2]));
			}

			if($i>100){
		 //		break;
			}

		}

		//

		fclose($handle);

		unset($arActionsFreefloat);

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		//$cache->clean("actions_data");

		?>
		<p><b>Успешно обработано (кол-во значений):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Добавлено (кол-во значений):</b> <?=count($arMes["ADDED"])?></p>
		<p><b>Обновлено (кол-во значений):</b> <?=count($arMes["UPDATED"])?></p>
		<?if(count($arMes["ERR"])>0):?>
		<p><b>Ошибки:</b> <?print_r($arMes["ERR"])?></p>
		<?endif;?>
		<?exit();
	}
}
?>

<h1>Загрузка LeeLoo ID из CSV</h1>
<?
/*
 $Act = new Actions();
	$arAct = $Act->GetItem('RU000A0JR4A1');
	 echo "<pre  style='color:black; font-size:11px;'>";
       print_r($arAct);
       echo "</pre>";*/
 ?>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Данные по показателям</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>