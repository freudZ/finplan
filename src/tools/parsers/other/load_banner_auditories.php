<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
// Загрузка аудиторий для индивидуального показа баннеров
// При перезаписи существующе аудитории полностью затирает список выбранных пользователей
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ADDED" =>0,
		"UPDATED" =>0,
		"ERR" => array(),
		"ERR_NOTFOUND_USERS" => array(),
	);

	$inputFileName = $_FILES["file"]["tmp_name"];
	
	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {

	$SectionId = 94; //Раздел индивидуальных
	$iblockId = 71; //Раздел индивидуальных

	//Получим из инфоблока Виды целей таргетирования баннеров все записи
	$arSelect = Array("ID", "NAME", "IBLOCK_ID");
	$arFilter = Array("IBLOCK_ID"=>IntVal($iblockId), "IBLOCK_SECTION_ID"=>$SectionId);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$arAuditories = array();
	while($ob = $res->fetch()){
	 $arAuditories[$ob["ID"]] = $ob["NAME"];
	}
	 $arTmpAu = array();
	 $arTmpEmails = array();
	while (($item = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i>1){

				//Определяем id существующего элемента, если передан. Если не передан пытаемся найти в $arAuditories. Если и там нет - то будем создавать новый элемент
				$elementId = 0;
				 if(!empty($item[0])){
					$elementId = intval($item[0]);
				 } else {
					$elementId = intval(array_search($item[1], $arAuditories));
				 }

				 $arTmpAu[$item[1]]["ID"] = $elementId;
				 $arTmpAu[$item[1]]["EMAILS"][] = $item[2];
				 $arTmpEmails[$item[2]] = 0;//Собираем почту

				// $arEventsLoad[$item[1]][$item[0]][] = array("EVENT_NAME"=>$item[2], "EVENT_DESCRIPTION"=>$item[3], "EVENT_URL"=>$item[4], "EVENT_EMITENT"=>$item[5], "EVENT_OPERATOR"=>$item[6]);

				 if(!empty($item[5])){
				 		$arEmitents[$item[5]] = $item[5];
				 }




			}

			if($i<=2){
				continue;
			}

		}
		fclose($handle);

		if(count($arTmpAu)){
		  //Получим id привязываемых пользователей
		  $arLogins = implode('|',array_keys($arTmpEmails));
		  $data = CUser::GetList(($by="ID"), ($order="ASC"),
                    array(
                        'LOGIN_EQUAL' => $arLogins,
                        'ACTIVE' => 'Y'
                    )
                );

        while($arUser = $data->Fetch()) {
        		$arTmpEmails[$arUser['LOGIN']] = $arUser['ID'];
        }

		  //Проверяем не найденных пользователей
		  $arLoginTmp = array();
		  foreach($arTmpEmails as $login=>$id){
			 if(intval($id)>0){
				$arLoginTmp[$login] = $id;
			 } else {
				$arMes["ERR_NOTFOUND_USERS"][] = $login;
			 }
		  }
		  if(count($arLoginTmp)>0){
			$arTmpEmails = $arLoginTmp;
			unset($arLoginTmp);
		  }

		  foreach($arTmpAu as $groupName=>$arVal){

	  	      $el = new CIBlockElement;

				$arUsers = array();

				foreach($arVal["EMAILS"] as $login){
					if(array_key_exists($login, $arTmpEmails) && !in_array($arTmpEmails[$login], $arUsers))
					 $arUsers[] = $arTmpEmails[$login];
				}

				$PROP = array();
				$PROP["USERS"] = $arUsers;

				$arLoadProductArray = Array(
				  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
				  "IBLOCK_SECTION_ID" => $SectionId,
				  "PROPERTY_VALUES"=> $PROP,
				  "NAME"           => $groupName,
				  "ACTIVE"         => "Y",            // активен
				  );

				if(intval($arVal["ID"])==0){  //Добавляем новую группу
				 $arLoadProductArray["IBLOCK_ID"] = $iblockId;
					if($PRODUCT_ID = $el->Add($arLoadProductArray)){
					  $arMes["ADDED"] += 1;
					  $arMes["SUCCESS"] += 1;
					} else {
					  echo $arMes["ERR"][] = $el->LAST_ERROR;
					  }

				} else { //Обновляем группу
					if($el->Update(intval($arVal["ID"]), $arLoadProductArray)){
					  $arMes["UPDATED"] += 1;
					  $arMes["SUCCESS"] += 1;
					} else {
					  echo $arMes["ERR"][] = $el->LAST_ERROR;
					  }
				}



		  }



		} //count($arTmpAu)


		//$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		//$cache->clean("radar_events");

		?>
		<p><b>Успешно обработано аудиторий:</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Добавлено аудиторий:</b> <?=$arMes["ADDED"]?></p>
		<p><b>Обновлено аудиторий:</b> <?=$arMes["UPDATED"]?></p>
		<?if(count($arMes["ERR"])>0):?>
		<p><b>Ошибки:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<?endif;?>
		<?if(count($arMes["ERR_NOTFOUND_USERS"])>0):?>
		<p><b>Не обнаружены пользователи с логинами:</b> <?=implode(", ", $arMes["ERR_NOTFOUND_USERS"])?></p>
		<?endif;?>
		<?exit();
	}
}
?>

<h1>Загрузка аудиторий таргетирования</h1>
  <p>Загрузка аудиторий для индивидуального показа баннеров</p>
<hr>
<?
/*
 $Act = new Actions();
	$arAct = $Act->GetItem('RU000A0JR4A1');
	 echo "<pre  style='color:black; font-size:11px;'>";
       print_r($arAct);
       echo "</pre>";*/
 ?>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Загружаемый файл</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>