<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ADDED" =>array(),
		"UPDATED" =>array(),
		"ERR" => array(),
		"EMPTY_ISIN" => array()
	);
	
	$inputFileName = $_FILES["file"]["tmp_name"];
	
	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {

	//Получим из HL 36 все записи событий
	$arExistsEvents = array();
	$arExistsEventsFlip = array();

	$hlblock   = HL\HighloadBlockTable::getById(36)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	$resHL = $entityClass::getList(array(
		"select" => array(
			"*",
		),
	));
	while($item = $resHL->fetch()){
	 if(!is_object($item["UF_DATE"])) continue;  //Если кто-то создал в хайлоаде запись без даты
	// $arExistsEvents[$item["ID"]] = md5($item["UF_TYPE"].($item["UF_DATE"])->format('d.m.Y').$item["UF_EMITENT"]);
	 $arExistsEvents[$item["ID"]] = md5($item["UF_TYPE"].($item["UF_DATE"])->format('d.m.Y').(!empty($item["UF_EMITENT"])?$item["UF_EMITENT"]:$item["UF_OPERATOR"]));

	}

	$arExistsEventsFlip = array_flip($arExistsEvents);

		$arEmitents = array();
		$arEventsLoad = array();
		while (($item = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i>1){
				//foreach($data as $n=>$item){
				 if(empty($item[0])) continue; //Если нет даты - то пропускаем
				 $arEventsLoad[$item[1]][$item[0]][] = array("EVENT_NAME"=>$item[2], "EVENT_DESCRIPTION"=>$item[3], "EVENT_URL"=>$item[4], "EVENT_EMITENT"=>$item[5], "EVENT_OPERATOR"=>$item[6]);

				 if(!empty($item[5])){
				 		$arEmitents[$item[5]] = $item[5];
				 }

/*				  $mode = 'add';
				  //Определяем режим обработки данных
				  if(array_key_exists($item[0], $arItemsInIndex)){
					  if(array_key_exists($item[2], $arItemsInIndex[$item[0]])){
						 $mode = 'update';
					  }else{
						 $mode = 'add';
					  }
				  } else {
				  		$mode = 'add';
				  }*/


				 // $arItem = array("UF_INDEX"=>trim($item[0]),"UF_SHARE_CODE"=>trim($item[2]),"UF_SHARE_OF_INDEX"=>$shareInIndex, "UF_FREEFLOAT"=>$freefloat);

/*				  if($mode=='add'){
				  //добавляем в массивы индексов для акций новый индекс
				  if(!in_array(trim($item[0]), $arActionsInIndex[trim($item[2])]))
	             $arActionsInIndex[trim($item[2])][] = trim($item[0]);

					$result = $entityClass::add($arItem);
						if ($result->isSuccess())
						{
						    $id = $result->getId();
							 $arMes["ADDED"][]=$id;
							 $arMes["SUCCESS"] = $arMes["SUCCESS"]+1;
						}elseif(!$result->isSuccess()){
							 $arMes["ERR"][]=$result->getErrorMessages();
						}
				  }else{
					 $result = $entityClass::update($arItemsInIndex[$item[0]][$item[2]]["ID"], $arItem);
					   if ($result->isSuccess())
						{
						    $id = $result->getId();
							 $arMes["UPDATED"][]=$id;
							 $arMes["SUCCESS"] = $arMes["SUCCESS"]+1;
						}elseif(!$result->isSuccess()){
							 $arMes["ERR"][]=$result->getErrorMessages();
						}
				  }*/


				//}
			}

			if($i<=2){
				continue;
			}

		}
		fclose($handle);

		$arEmitentsId = array("RUS"=>array(), "USA"=>array());
		$arActives = array();
		//Получаем ID эмитентов РФ по загружаемым названиям
		$arSelect = Array("ID", "NAME", "IBLOCK_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal(26), "NAME"=>$arEmitents, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->fetch()){
		  $arEmitentsId["RUS"][$ob["NAME"]] = $ob["ID"];
		}

		//Получаем ID эмитентов США по загружаемым названиям
		$arSelect = Array("ID", "NAME", "IBLOCK_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal(43), "NAME"=>$arEmitents, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->fetch()){
		  $arEmitentsId["USA"][$ob["NAME"]] = $ob["ID"];
		}

		//Получаем акции и облигации по ID эмитента РФ
		$flip_rus = array_flip($arEmitentsId["RUS"]);
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_EMITENT_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal(32), "PROPERTY_EMITENT_ID"=>array_keys($flip_rus), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

		while($ob = $res->fetch()){
			if(!in_array($ob["CODE"], $arActives[ $flip_rus[ $ob["PROPERTY_EMITENT_ID_VALUE"] ] ]))
		     $arActives[ $flip_rus[ $ob["PROPERTY_EMITENT_ID_VALUE"] ] ][] =$ob["CODE"];
		}
		//Облигации
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_EMITENT_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal(27), "PROPERTY_EMITENT_ID"=>array_keys($flip_rus), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

		while($ob = $res->fetch()){
			if(!in_array($ob["CODE"], $arActives[ $flip_rus[ $ob["PROPERTY_EMITENT_ID_VALUE"] ] ]))
		     $arActives[ $flip_rus[ $ob["PROPERTY_EMITENT_ID_VALUE"] ] ][] =$ob["CODE"];
		}

		//Получаем акции по ID эмитента США
		$flip_usa = array_flip($arEmitentsId["USA"]);
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_EMITENT_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal(55), "PROPERTY_EMITENT_ID"=>array_keys($flip_usa), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

		while($ob = $res->fetch()){
			if(!in_array($ob["CODE"], $arActives[ $flip_usa[ $ob["PROPERTY_EMITENT_ID_VALUE"] ] ]))
		 		$arActives[ $flip_usa[ $ob["PROPERTY_EMITENT_ID_VALUE"] ] ][] =$ob["CODE"];
		}



		//Добавим с загружаемые данные коды активов
		$CRadarEvens = new CRadarEvents;


		foreach($arEventsLoad as $eventType=>$arDates){
			if(in_array($eventType, $CRadarEvens->arPortfolioEventKeys)){
			  foreach($arDates as $eDate=>$event){
			  	 foreach($event as $k=>$eventDetail){

				  if(!empty($eventDetail["EVENT_EMITENT"])){
					 $arEventsLoad[$eventType][$eDate][$k]["EVENT_ACTIVES"] = $arActives[$eventDetail["EVENT_EMITENT"]];
				  }

				 }
			  }


			}
		}

		//Записываем в БД
		foreach($arEventsLoad as $eventType=>$arDates){
			  foreach($arDates as $eDate=>$event){
			  	 foreach($event as $k=>$eventDetail){

				  $mode = 'add';
				  //Определяем режим обработки данных
				  $md5Key = md5($eventType.$eDate.(!empty($eventDetail["EVENT_EMITENT"])?$eventDetail["EVENT_EMITENT"]:$eventDetail["EVENT_OPERATOR"]));
				  if(in_array($md5Key, $arExistsEvents)){
						 $mode = 'update';
				  } else {
				  		$mode = 'add';
				  }

					 echo $mode;

				  $arItem = array("UF_EMITENT"=>$eventDetail["EVENT_EMITENT"],
				  						"UF_LINK"=>$eventDetail["EVENT_URL"],
										"UF_CODE"=>implode(",",$eventDetail["EVENT_ACTIVES"]),
										"UF_DESCRIPTION"=>$eventDetail["EVENT_DESCRIPTION"],
										"UF_TITLE"=>$eventDetail["EVENT_NAME"],
										"UF_TYPE"=>$eventType,
										"UF_DATE"=>$eDate,
										"UF_OPERATOR"=>$eventDetail["EVENT_OPERATOR"],
										);

				  if($mode=='add'){
					$result = $entityClass::add($arItem);
						if ($result->isSuccess())
						{
						    $id = $result->getId();
							 $arMes["ADDED"][]=$id;
							 $arMes["SUCCESS"] = $arMes["SUCCESS"]+1;
						}elseif(!$result->isSuccess()){
							 $arMes["ERR"][]=$result->getErrorMessages();
						}
				  }else{
					 $result = $entityClass::update($arExistsEventsFlip[$md5Key], $arItem);
					   if ($result->isSuccess())
						{
						    $id = $result->getId();
							 $arMes["UPDATED"][]=$id;
							 $arMes["SUCCESS"] = $arMes["SUCCESS"]+1;
						}elseif(!$result->isSuccess()){
							 $arMes["ERR"][]=$result->getErrorMessages();
						}
				  }




				 }
				 }
				 }




		//echo "<pre  style='color:black; font-size:11px;'>";
         //print_r($arActives);
        // print_r($arEventsLoad);
        // print_r($arExistsEvents);
      //echo "</pre>";

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cache->clean("radar_events");

		$res = new CRadarEvents();
		unset($res);
		?>
		<p><b>Успешно обработано (кол-во событий):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Добавлено (кол-во событий):</b> <?=count($arMes["ADDED"])?></p>
		<p><b>Обновлено (кол-во событий):</b> <?=count($arMes["UPDATED"])?></p>
		<?if(count($arMes["ERR"])>0):?>
		<p><b>Ошибки:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<?endif;?>
		<?exit();
	}
}
?>

<h1>Загрузка событий радара</h1>
<hr>
<?
/*
 $Act = new Actions();
	$arAct = $Act->GetItem('RU000A0JR4A1');
	 echo "<pre  style='color:black; font-size:11px;'>";
       print_r($arAct);
       echo "</pre>";*/
 ?>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Загружаемый файл</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>