<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//Загрузка лидов из тильды в CRM
CModule::IncludeModule("iblock");

global $USER;
$crmIblockId = 19;
$arGroups = $USER->GetUserGroupArray();
$arFormNames = array(
							"form362737370"=>"мастер",
							"form362738515"=>"архитектор",
							"form362738877"=>"магнат",
);

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"] && !empty($_REQUEST["nameLead"])){
	$arMes = array(
		"SUCCESS" => 0,
		"ADDED" =>array(),
		"UPDATED" =>array(),
		"ERR" => array(),
		"EMPTY_ISIN" => array()
	);

	$inputFileName = $_FILES["file"]["tmp_name"];

	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {


	$arFieldsNum = array();
	$arLeadUsers = array();
		while (($item = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i==2){
			  $arFieldsNum = $item;
			  $arFieldsNum = array_flip($arFieldsNum);
			}
			if($i<3){
					continue;
				}
				      $arLeadUsers[$item[$arFieldsNum["email"]]] = array("formid" => $item[$arFieldsNum["formid"]], "date"=>(new DateTime($item[$arFieldsNum["created"]]))->format('d.m.Y'));
						$arMes["SUCCESS"]++;
			}

			if($i>100){
		 //		break;
			}

		}

		fclose($handle);

		//Достаем id записей из CRM по email адресам
		$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_EMAIL", "PROPERTY_ITEM", "PROPERTY_LEADS");
		$arFilter = Array("IBLOCK_ID"=>$crmIblockId, "PROPERTY_ITEM"=>"10 тату", "PROPERTY_EMAIL"=>array_keys($arLeadUsers), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>10), $arSelect);
		$arFindCrmRows = array();
		while($ob = $res->Fetch()){
			$arFindCrmRows[$ob["PROPERTY_EMAIL_VALUE"]] = $ob;
		}

		$baseName = $_REQUEST["nameLead"]." ";
		$baseDate = $_REQUEST["dateLead"];
		if(!empty($baseDate))
			$baseDate = (new DateTime($baseDate))->format('d.m.Y');

		foreach($arFindCrmRows as $crmEmail=>$crmData){
			if(empty($baseDate)){
				$baseDate = $arLeadUsers[$crmEmail]["date"];
			}
		 $checkLeadStr = $baseDate . " " . $baseName . $arFormNames[$arLeadUsers[$crmEmail]["formid"]];
		 if(strpos($crmData["PROPERTY_LEADS_VALUE"], $checkLeadStr) === false){
		 	if(!empty($crmData["PROPERTY_LEADS_VALUE"])){
		 		$crmData["PROPERTY_LEADS_VALUE"] = explode(";",$crmData["PROPERTY_LEADS_VALUE"]);

				$crmData["PROPERTY_LEADS_VALUE"][] = $checkLeadStr;
				$tmp = array();
				foreach($crmData["PROPERTY_LEADS_VALUE"] as $val){
				  if(!empty($val)){
				  	$tmp[] = $val;
				  }
				}
			  	$crmData["PROPERTY_LEADS_VALUE"] = $tmp;
				unset($tmp);

				$crmData["PROPERTY_LEADS_VALUE"] = implode(";", $crmData["PROPERTY_LEADS_VALUE"]);

				$arMes["UPDATED"][] = $crmEmail . $checkLeadStr;
		 	} else {
				$crmData["PROPERTY_LEADS_VALUE"] = $checkLeadStr . ";";
				$arMes["ADDED"][] = $crmEmail . $checkLeadStr;
		 	}
			CIBlockElement::SetPropertyValuesEx($crmData["ID"], $crmIblockId, array("LEADS" => $crmData["PROPERTY_LEADS_VALUE"]));
		 } else {
			$arMes["ERR"][] = $crmEmail . " повторный лид, не записано. ";
		 }

		}

		//$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		//$cache->clean("actions_data");

		?>
		<p><b>Успешно обработано (кол-во значений):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Добавлено (кол-во значений):</b> <?=count($arMes["ADDED"])?></p>
		<p><b>Обновлено (кол-во значений):</b> <?=count($arMes["UPDATED"])?></p>
		<?if(count($arMes["ERR"])>0):?>
		<p><b>Ошибки:</b> <?print_r($arMes["ERR"])?></p>
		<?endif;?>
		<?exit();
	}
?>

<h1>Загрузка лидов из тильды в CRM</h1>
<?
/*
 $Act = new Actions();
	$arAct = $Act->GetItem('RU000A0JR4A1');
	 echo "<pre  style='color:black; font-size:11px;'>";
       print_r($arAct);
       echo "</pre>";*/
	if(count($_REQUEST) && empty($_REQUEST["nameLead"])):?>
	  <p style="color:red">Не задано общее название лидов</p>
	<? endif; ?>
	<?if(count($_FILES) && empty($_FILES["file"]["tmp_name"])):?>
	  <p style="color:red">Не загружен файл</p>
	<? endif; ?>
<form enctype="multipart/form-data" method="post">
 <div class="form-group">
    <label for="nameLead">Общее название лидов из файла</label>
    <input type="text" name="nameLead" id="nameLead">
  </div>
 <div class="form-group">
    <label for="dateLead">Дата лидов в файле</label>
    <input type="date" name="dateLead" id="dateLead">
	 <i>Если не заполнено - дата будет браться из первой колонки в файле</i>
  </div>
  <div class="form-group">
    <label for="exampleInputFile">Файл csv (кодировка utf-8)</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>