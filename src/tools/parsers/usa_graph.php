<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("Фиксация граничной даты для вывода на графики в ценах акций США");?>
<? use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

	$hlblock   = HL\HighloadBlockTable::getById(29)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();
  $arPrices = array();
  $arPricesStop = array();
	$resHL = $entityClass::getList(array(
		"select" => array(
			"ID",
			"UF_ITEM",
			"UF_DATE",
			"UF_OPEN",
			"UF_CLOSE",
			"UF_HIGH",
			"UF_LOW",
		),
		"filter" => array("!=UF_SHOW_GRAPH"=>"N"),
		"group" => array("UF_ITEM"),
		"order" => array("UF_ITEM"=>"ASC", "UF_DATE"=>"ASC"),
		//"limit" => 1000,
	));
	//$arPrices = $resHL->fetchAll();
   while($item = $resHL->fetch()){
	 if(($item["UF_OPEN"]>0 && $item["UF_CLOSE"]>0 && $item["UF_HIGH"]>0 && $item["UF_LOW"]>0) || in_array($item["UF_ITEM"],$arPricesStop)){
	  if(!in_array($item["UF_ITEM"],$arPricesStop))
		$arPricesStop[] = $item["UF_ITEM"];
	 	continue;
	 }
	 //$arPrices[$item["UF_ITEM"]][] = $item;
	 $arPrices[] = $item;
	}



	foreach($arPrices as $arItem){
/*		echo "<pre  style='color:black; font-size:11px;'>";
		   print_r($arItem);
		   echo "</pre>";*/
	  $entityClass::update($arItem["ID"], array("UF_SHOW_GRAPH"=>"N"));
	}



/*	echo "<pre  style='color:black; font-size:11px;'>";
      print_r($arPricesStop);
      echo "</pre>";*/

 ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>