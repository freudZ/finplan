<?require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php";
	define("NO_KEEP_STATISTIC", true);
	define("NOT_CHECK_PERMISSIONS", true);
	@set_time_limit(0);

	CModule::IncludeModule("iblock");
	CModule::IncludeModule("highloadblock");
	use Bitrix\Highloadblock as HL;

	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	if (!$USER->IsAdmin() && !in_array(5, $arGroups)) {
		exit();
	}

	if ($_REQUEST["send"] && $_FILES["file"]["tmp_name"]) {
		$arMes = [
			"ADDED" => 0,
			"UPDATED" => 0,
			"UPDATED_VOLUMES" => 0,
			"ERR" => array(),
			"EMPTY_ISIN" => array()
		];

		$hlblock           = HL\HighloadBlockTable::getById(29)->fetch();
		$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();

		$inputFileName = $_FILES["file"]["tmp_name"];
		$arTickers     = array();
	//Получим список загружаемых активов из файла:
		$i = 0;
		if (($handle = fopen($inputFileName, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
				$i++;
				if ($i <= 1) {
					continue;
				}

				$arTickers[$data[0]] = 'Y';
			}
			fclose($handle);
		}

		/* удаление дублей по тикеру и заполненному UF_CLOSE_BETA */
/*			$hlblock = HL\HighloadBlockTable::getById(29)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();
			$arFilter = array("@UF_ITEM"=>array_keys($arTickers), "!UF_CLOSE_BETA"=>false);
					$res         = $entityClass::getList(array(
						"filter" => $arFilter,
						"select" => array(
							"ID",
							"UF_ITEM",
						),
						//"limit" => 1,
						"order" => array(
							"UF_ITEM" => "ASC"
						),
						//"group"=>array("UF_ITEM"),
					));
		$cnt = 0;
		while ($item = $res->fetch()) {
			$entityClass::delete($item["ID"]);
			$cnt++;
		}
		  echo "Удалено записей: ".$cnt;
		  die();*/
		/* удаление дублей по тикеру и заполненному UF_CLOSE_BETA */




		$arActionsUSADates = array(); // Получаем даты существующих записей о ценах в разрезе акций
		$arActionsNoGraph  = array(); // Получаем даты раньше которых нельзя выводить на график
		$res               = $entity_data_class::getList(array(
			"filter" => array("@UF_ITEM" => array_keys($arTickers)),
			"select" => array(
				"*",
			),
			"order" => array("UF_ITEM" => "asc", "UF_DATE" => "ASC"),
			"group" => array("UF_ITEM"),

		));

		while ($item = $res->fetch()) {
			$arActionsUSADates[$item["UF_ITEM"]][$item["UF_DATE"]->format('d-m-Y')] = $item;

			if (!array_key_exists($item["UF_ITEM"], $arActionsNoGraph) && $item["UF_SHOW_GRAPH"] != "N") {
				$arActionsNoGraph[$item["UF_ITEM"]] = $item["UF_DATE"];
			}

		}


		$i = 0;
		if (($handle = fopen($inputFileName, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
				$i++;
				if ($i <= 1) {
					continue;
				}

	/*            if (!$data[6]) {
	$arMes["EMPTY_ISIN"][] = $data[0] . " - " . $data[5];
	continue;
	}*/

				//$data[1] = str_replace("-", ".", $data[1]);

				//Собираем массив для строки цен на дату
				$arDataUpdate = array(
					"UF_ITEM" => $data[0],
					"UF_DATE" => (new DateTime($data[1]))->format('d.m.Y'),
					"UF_OPEN" => $data[2],
					"UF_HIGH" => $data[3],
					"UF_LOW" => $data[4],
					"UF_CLOSE" => $data[5],
					"UF_CLOSE_BETA" => $data[5],
					"UF_VOLUME" => $data[6], //Объем торгов
					"UF_MANUAL" => "Y",
				);
				$firstDate = $arActionsNoGraph[$data[0]];

				if (strtotime($data[1]) < strtotime($firstDate)) { //Если данные раньше чем можно показывать на графиках - добавим отметку закрытия использования
					$arDataUpdate["UF_SHOW_GRAPH"] = "N";
				}

				$confirmDate =  (new DateTime($data[1]))->format('d-m-Y');
				if (array_key_exists($confirmDate, $arActionsUSADates[$data[0]])) { //Updates
					//update
					$arItem = $arActionsUSADates[$data[0]][$confirmDate];
					//Если каких то цен для записи не хватает - обновляем ее с признаком ручного добавления
					if (floatval($arItem["UF_OPEN"]) <= 0 || floatval($arItem["UF_CLOSE"]) <= 0 || floatval($arItem["UF_HIGH"]) <= 0 || floatval($arItem["UF_LOW"]) <= 0) {
						$id = $arActionsUSADates[$data[0]][$confirmDate]["ID"];

						$result = $entity_data_class::update($id, $arDataUpdate);

/*					echo $data[1]." upd: <pre  style='color:black; font-size:11px;'>";
					print_r($arDataUpdate);
					echo "</pre>";*/

						if ($result->isSuccess()) {
							$arMes["UPDATED"] = $arMes["UPDATED"] + 1;
							//$ID = $result->getId(); // получаем ID созданного элемента хайлоадблока
						} else {
							$errors = $result->getErrorMessages(); // получаем сообщения об ошибках
							echo "<pre  style='color:black; font-size:11px;'>";
							print_r($errors);
							echo "</pre>";
						}

						//Если цены заполнены то обновляем только объем и Цену Close для Беты
					} else {

						$id = $arActionsUSADates[$data[0]][$confirmDate]["ID"];
						//  unset($arDataUpdate["UF_ITEM"]);
						$arVolume                  = array();
						if($_REQUEST["updateAllPrices"]=='Y'){
						$arVolume["UF_OPEN"]     = $arDataUpdate["UF_OPEN"];
						$arVolume["UF_CLOSE"]    = $arDataUpdate["UF_CLOSE"];
						$arVolume["UF_HIGH"]     = $arDataUpdate["UF_HIGH"];
						$arVolume["UF_LOW"]      = $arDataUpdate["UF_LOW"];
					  }
						$arVolume["UF_VOLUME"]     = $arDataUpdate["UF_VOLUME"];
						$arVolume["UF_CLOSE_BETA"] = $arDataUpdate["UF_CLOSE_BETA"];
						$result                    = $entity_data_class::update($id, $arVolume);

/*					echo $data[1]." upd val beta: <pre  style='color:black; font-size:11px;'>";
					print_r($arVolume);
					echo "</pre>";*/

						if ($result->isSuccess()) {
							$arMes["UPDATED_VOLUMES"] = $arMes["UPDATED_VOLUMES"] + 1;
							// $ID = $result->getId(); // получаем ID созданного элемента хайлоадблока
						} else {
							$errors = $result->getErrorMessages(); // получаем сообщения об ошибках
							echo "<pre  style='color:black; font-size:11px;'>";
							print_r($errors);
							echo "</pre>";
						}
					}
				} else { //ADD
					//add
/*					echo $data[1]." add: <pre  style='color:black; font-size:11px;'>";
					print_r($arDataUpdate);
					echo "</pre>";*/
					$result = $entity_data_class::add($arDataUpdate);

					if ($result->isSuccess()) {
						$arMes["ADDED"] = $arMes["ADDED"] + 1;
						//  $ID = $result->getId(); // получаем ID созданного элемента хайлоадблока
					} else {
						$errors = $result->getErrorMessages(); // получаем сообщения об ошибках
						echo "<pre  style='color:black; font-size:11px;'>";
						print_r($errors);
						echo "</pre>";
					}
				}

				foreach ($arFieldsNum as $n => $code) {
					if ($code == 'SECURITIES_QUANTITY') {
						$data[$n] = str_replace(" ", "", $data[$n]);
						$data[$n] = str_replace(",", ".", $data[$n]);
						$data[$n] = round(floatval($data[$n]));
					}
					$PROPS[$code] = $data[$n];
				}
			}
			fclose($handle);

			$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
			$cache->clean("actions_usa_data");
			$cache->clean("actions_usa_filters_data");
		?>
        <p><b>Был загружен файл:</b> <?=$_FILES["file"]["name"]?></p>
        <p><b>Обновлено записей цен:</b> <?=$arMes["UPDATED"]?></p>
        <p><b>Обновлено объемов:</b> <?=$arMes["UPDATED_VOLUMES"]?></p>
        <p><b>Добавлено записей цен:</b> <?=$arMes["ADDED"]?></p>
        <?
				  if($arMes["UPDATED"]+$arMes["UPDATED_VOLUMES"]+$arMes["ADDED"]>0){ //Взводим флаг загрузки беты, ночью на кроне пройдет пересчет беты
				  	$beta = new CBeta;
					$beta->setSheduleBetaUSA(true);
					unset($beta);
				  }

        			exit();
        		}
        	}

        ?>
<h1>Загрузка цен для акций США</h1>
<p>Пример структуры файла:<br>
<textarea disabled>Ticker;Date;Open;High;Low;Close;Volume
FTR;02-01-2014;69,750000;70,949997;69,300003;70,050003;1031400
FTR;03-01-2014;70,500000;71,400002;70,050003;70,500000;925900</textarea>
</p>
<p>Добавляет недостающие цены (убирает дыры в ценах базы) с пометкой ручного внесения цены. Для существующих цен обновляет только колонку цены закрытия для расчета беты и колонку с объемом торгов</p>
<p>Если требуется обновить все цены, включая введенные вручную и заполненные ранее - поставьте галочку "Обновить все цены".</p>
<p><strong>Будьте внимательны, отменить внесенные изменения в ценах будет не просто.</strong></p>
<p><i>Размер одного файла не должен превышать 10 мегабайт.</i></p>
<form enctype="multipart/form-data" method="post">
    <div class="form-group">
		  <input type="checkbox" name="updateAllPrices" value="Y">
		  <label for="updateAllPrices">Обновить все цены (включая существующие)</label>
		  <br>
        <label for="exampleInputFile">Данные по ценам</label>
        <input type="file" name="file" id="exampleInputFile">
    </div>
    <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php";?>
