<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
@set_time_limit(0);

/*
 * Парсер загрузки данных по периодам для американских компаний
 *
 * Внимание!!! В БД загружаются периоды как есть. Их смещение расчитывается и находится строго в кеше action_usa
 * Расчет смещения реализован в классе RadarBase
 */

use Bitrix\Highloadblock as HL;

global $USER, $APPLICATION, $DB;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}
function setNextMonday($date){
	$dt = new DateTime($date);

	if($dt->format("N")==6){
		$dt->modify("+2 days");
	} elseif($dt->format("N")==7){
		$dt->modify("+1 days");
	}

	if(isWeekEndDay($dt->format("d.m.Y"))){
		$finded = false;
		while(!$finded){
			$dt->modify("+1 days");
			if(!isWeekEndDay($dt->format("d.m.Y"))){
				$finded = true;
			}
		}
	}

	return $dt->format("Y-m-d");
}
$start = microtime(true);
CModule::IncludeModule("iblock");
//Формируем массив существующих компаний США с ключом по имени
$arFilter = Array("IBLOCK_ID"=>43);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME"));
$arCompanyUSA = array();
while($item = $res->Fetch())
{
	$arCompanyUSA[$item["NAME"]]=array("ID"=>$item["ID"], "NAME"=>$item["NAME"]);
}

//Формируем массив страниц компаний США
$arFilterPages = Array("IBLOCK_ID"=>44);
$resPages = CIBlockElement::GetList(Array(), $arFilterPages, false, false, array("ID", "NAME"));
$arCompanyUSA_Pages = array();
while($item = $resPages->Fetch())
{
	$arCompanyUSA_Pages[$item["NAME"]]=array("ID"=>$item["ID"], "NAME"=>$item["NAME"]);
}


if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){

	$arMes = array(
	  "SUCCESS" => 0,
	  "ERR" => array()
	);
	$inputFileName = $_FILES["file"]["tmp_name"];
	$i = 1;

	if (($handle = fopen($inputFileName, "r")) !== FALSE) {

		//историческая капитализация для компаний США
/*		$hlblock = HL\HighloadBlockTable::getById($APPLICATION->usaPricesHlId)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();*/

		if(!isset($_REQUEST['operate_year']) || empty($_REQUEST['operate_year']) || intval($_REQUEST['operate_year'])<=0){
			$year = date("Y");
		} else {
			$year = intval($_REQUEST['operate_year']);
		}

		if(!isset($_REQUEST['operate_kv']) || empty($_REQUEST['operate_kv']) || intval($_REQUEST['operate_kv'])<=0){
			$op_kv = 1;
		} else {
			$op_kv = intval($_REQUEST['operate_kv']);
		}


		$quarters = [
		  1 => "-03-31",
		  2 => "-06-30",
		  3 => "-09-30",
		  4 => "-12-31",
		];
		$arYears = array(intval($year));
		if(!empty($_REQUEST['operate_year_end'])){
			for($yrs = intval($year)+1; $yrs<=intval($_REQUEST['operate_year_end']); $yrs++){
				$arYears[] = $yrs;
			}
		}

		$el = new CIBlockElement;
		$ibID = 55;
		$actionsData = array();
		$arActionHidenData = array();//Массив ID акций с ключами по тикеру для простановки признака вышедших с обращения
		$arFilter = ["IBLOCK_ID" => $ibID];

		$res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "IBLOCK_ID", "CODE", "NAME", "PROPERTY_SECID", "PROPERTY_SECURITIES_QUANTITY", "PROPERTY_EMITENT_ID"]);

		while ($rowItem = $res->GetNextElement()) {
			$item = $rowItem->GetFields();
			$arProps = $rowItem->GetProperties();
			$arQuantity = array();                 //Получаем кол-ва для сплита акций
			foreach($arProps["SECURITIES_QUANTITY"]["VALUE"] as $k=>$v){
				$date = $arProps["SECURITIES_QUANTITY"]["DESCRIPTION"][$k];
				$arQuantity[(empty($date)?'01.01.2011':$date)] = $v;
			}
			$item["PROPERTY_SECURITIES_QUANTITY"] = $arQuantity;

			/*			            [PROPERTY_SECURITIES_QUANTITY] => Array
							(
								[01.01.2011] => 45238952
								[27.08.2020] => 25238952
							)*/
			$actionsData[$item["PROPERTY_EMITENT_ID_VALUE"]] = $item;
			$arActionHidenData[$item["PROPERTY_SECID_VALUE"]] = $item["ID"];
		}


		$UpdatedCounter = array();
		while (($data = fgetcsv($handle, 1000000, ";")) !== FALSE) {
			if($i==1){
				$arData["period"] = $data;
			}elseif($i==2){
				$arData["params"] = $data;
			}elseif($i===3){
				$i++;
				continue;
			} else {
				//формирование данных по компании
				$parentPeriod = "";
				$arLoad = array();

				foreach($arData["period"] as $n=>$item){
					if($item && $n > 13){
						if($item!=$parentPeriod){
							$parentPeriod = $item;
						}
						if($data[$n]) {
							$arLoad[$parentPeriod][$arData["params"][$n]] = str_replace(",", ".", str_replace(" ", "", trim($data[$n])));
						}
					}
				}

/*				echo "<pre  style='color:black; font-size:11px;'>";
				print_r($arLoad);
				print_r($data);
				echo "</pre>";*/
				//обработка
				$company = getCompany($data[0], $arCompanyUSA_Pages, $arCompanyUSA);

				if($company){

					setCompanyPageParams($data[0], $data[1], $data[3], $data[4], $data[5], $data[6], $data[9], $data[10], $data[11], $data[12]);
					//Устанавливаем признак вышедших с обращения для акции
					if(array_key_exists($data[1], $arActionHidenData)){
					CIBlockElement::SetPropertyValuesEx($arActionHidenData[$data[1]], 55, array("HIDEN" => !empty($data[13])?'Y':''));
					}
				}

				if(count($arLoad)<2){
					continue;
				}

				foreach($arLoad as $date => $vals){
					$date = explode("кв", $date);
					$date = str_replace(' год', '', $date);
					$date[0] = trim($date[0]);
					$date[1] = trim($date[1]);

					if($company){
						//	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/usa_company_log.txt");
						//	AddMessage2Log('company = '.$company["ID"].' '.$company["NAME"].' date='.$date.' vals='.$vals,'');
						$arCompanyCurrency = array("REPORT"=>trim($data[10]), "CIRCULATION"=>trim($data[11]), "DIVIDENDS"=>trim($data[12]));
						//if($company["ID"]==174740)
						  //setItem($date, $vals, $company["ID"], $actionsData, $capData, $arCompanyCurrency);
						  setItem($date, $vals, $company["ID"], $actionsData, $arCompanyCurrency);
						$UpdatedCounter[$company["ID"]] = $company["ID"];
					} else {
						$arMes["ERR"][] = $data[0];
					}
				}

				if($company["ACTIONS"]){
					exit();
				}
			}

			$i++;
		}
		$arMes["SUCCESS"] = count($UpdatedCounter);
		clearCompanyPageCache($arCompanyUSA_Pages); //Очищаем кеш страниц компаний после загрузки данных

		/*CModule::IncludeModule("highloadblock");
		$hlblock   = HL\HighloadBlockTable::getById(22)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				"!UF_CAPITAL" => false,
				"!UF_DATA" => false
			),
			"select" => array(
				"*"
			),
		));
		while($data = $res->fetch()){
			$vals = json_decode($data["UF_DATA"], true);
			$capital = json_decode($data["UF_CAPITAL"], true);

			$capitalVal = 0;
			foreach ($capital as $n=>$v){
				$capitalVal+=$v;
			}
			$vals["Прошлая капитализация"] = round(($capitalVal/1000000), 2);

			$entityClass::update($data["ID"], array(
				"UF_DATA" => json_encode($vals)
			));
		}


		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cache->clean("obligations_data");*/
        $end = microtime(true);
        $delta = round($end-$start, 2);
		?>
        <p><b>Обновлено/добавлено (кол-во компаний):</b> <?=$arMes["SUCCESS"]?></p>
        <p><b>Не найдено компаний:</b> <?=implode(", ", $arMes["ERR"])?></p>
        <p><b>Время работы скрипта:</b> <?=$delta?> сек.</p>
		<?exit();
	}
}

function getSlidingsQuartals($firstQuartalDate){
	$arReturn = array();
	for($i=1; $i<=4; $i++){
		$dt = (new DateTime($firstQuartalDate))->modify('+3 month');

	}

}

function setItem($date, $data, $company, $actionsData, $arCompanyCurrency=array()){
/*    if($company!=151831){
        return false;
			}*/
	CModule::IncludeModule("highloadblock");
    Global $DB;
	$year =  (is_array($date) && boolval($date[1])) ? $date[1] : $date[0];
	$quarter = (is_array($date) && boolval($date[1])) ? $date[0] : '';
	$arKvartalToMonth = array(
	  1 => "03-31",
	  2 => "06-30",
	  3 => "09-30",
	  4 => "12-31",
	);

	if(array_key_exists($company, $actionsData)) {
		$ad = $actionsData[$company];
        $d = DateTime::createFromFormat('Y-m-d', $year.'-'.$arKvartalToMonth[$quarter]);
        //CLogger::parser_period_corp_usa("date: " . print_r($date, true) . " company:" . $company. " " . $year.'-'.$arKvartalToMonth[$quarter]);
//return false;
        $dateSql = $d->format("Y-m-d");
        $sql = "SELECT * FROM `hl_polygon_actions_data` WHERE `UF_DATE` >= '$dateSql' AND `UF_ITEM` = '".$ad["PROPERTY_SECID_VALUE"]."' LIMIT 1";
        $resActionPrice = $DB->Query($sql);
        $actionPrice = 0;
        if($rowActionPrice = $resActionPrice->fetch()){
            $actionPrice = floatval($rowActionPrice['UF_CLOSE']);
        }
/*        if($company==151831) {
            CLogger::appleConpanyParser("Цена на ".$quarter." ".$year." = ".$actionPrice);
        }*/
		if($actionPrice>0) {
	
			$data["Прошлая капитализация"] = round(($actionPrice * intval($ad["PROPERTY_SECURITIES_QUANTITY_VALUE"])) / 1000000, 2);
			//$data["Прошлая капитализация"] = round(($cd[$year][$quarter] * intval($ad["PROPERTY_SECURITIES_QUANTITY_VALUE"])) / 1000000, 2);

			//Переводим прошлую капу, если требуется
/*			if($arCompanyCurrency["REPORT"]!==$arCompanyCurrency["CIRCULATION"] && !empty($arCompanyCurrency["REPORT"])){
				$cbr = new CCurrency((new DateTime($year."-".$arKvartalToMonth[$quarter]))->format('d.m.Y'));
				$arReportRate = $cbr->getRate($arCompanyCurrency["REPORT"], false);
				$arCurrencyRep = array("dt"=>(new DateTime($year."-".$arKvartalToMonth[$quarter]))->format('d.m.Y'), "cur"=>$arCompanyCurrency["REPORT"], "rate"=>$arReportRate);
				CLogger::ReportRate(print_r($arCurrencyRep, true));
				usleep(300);
				$arCirculationRate = $cbr->getRate($arCompanyCurrency["CIRCULATION"], false);
				if($cbr->isRareCurrency($arCompanyCurrency["REPORT"])){//Если редкая валюта то считаем через доллары напрямую
					//Для редких валют у ЦБ есть курс не на все даты, ищем в прошлое до получения курса в случае отсутствия курса на конец квартала
					while(empty($arReportRate)){
						$currClassDate  =  $cbr->getClassDate();
						$cbr->setClassDate((new DateTime($currClassDate))->modify('-1 days')->format('d.m.Y'));
						$arReportRate = $cbr->getRate($arCompanyCurrency["REPORT"], false);
						usleep(300);
					}

					$data["Прошлая капитализация"] = $data["Прошлая капитализация"]*$arReportRate;
				}else{ //Если не редкая валюта то считаем через рубли
					$data["Прошлая капитализация"] = ( $data["Прошлая капитализация"] * $arCirculationRate)/$arReportRate;
				}
			}*/
            
            $recalcCap = true; //включить блок пересчета капитализации
            //$recalcCap = false;
            
/*            if($company==151831) {
                CLogger::appleConpanyParser(print_r($data, true));
            }*/
            if ($arCompanyCurrency["REPORT"]!==$arCompanyCurrency["CIRCULATION"] && !empty($arCompanyCurrency["REPORT"]) && $recalcCap) {
                //Достаем курс валюты отчетности
                $dateSql = (new DateTime($year . "-" . $arKvartalToMonth[$quarter]))->format('Y-m-d');
                $codeCurr = $arCompanyCurrency["REPORT"];
                $CURRENCY_REPORT = array("RATE"=>1);
                $sql = "SELECT `UF_RATE` AS 'RATE', `UF_CODE` AS 'CODE', `UF_DATE` AS 'DATE', `UF_RARE` AS 'RARE' FROM `hl_currency_rates` WHERE `UF_DATE` >= '$dateSql' AND `UF_CODE` = '$codeCurr' LIMIT 1";
                $resCurr = $DB->Query($sql);
                if($rowCurr = $resCurr->fetch()){
                    $CURRENCY_REPORT = $rowCurr;
                }
                unset($rowCurr, $resCurr);
                
                //Достаем курс валюты обращения
                $CURRENCY_CIRCULATION = array("RATE"=>1);
                $codeCurr = $arCompanyCurrency["CIRCULATION"];
                $sql = "SELECT `UF_RATE` AS 'RATE', `UF_CODE` AS 'CODE', `UF_DATE` AS 'DATE', `UF_RARE` AS 'RARE' FROM `hl_currency_rates` WHERE `UF_DATE` >= '$dateSql' AND `UF_CODE` = '$codeCurr' LIMIT 1";
                $resCurr = $DB->Query($sql);
                if($rowCurr = $resCurr->fetch()){
                    $CURRENCY_CIRCULATION = $rowCurr;
                }
                unset($rowCurr, $resCurr);
                //$cbr               = new CCurrency((new DateTime($year . "-" . $arKvartalToMonth[$quarter]))->format('d.m.Y'));
                $arReportRate      = $CURRENCY_REPORT["RATE"];
                $arCirculationRate = $CURRENCY_CIRCULATION["RATE"];
                if ($CURRENCY_REPORT["RARE"]==1) { //Если редкая валюта то считаем через доллары напрямую
                    $data["Прошлая капитализация"] = $data["Прошлая капитализация"] * $arReportRate;
                } else { //Если не редкая валюта то считаем через рубли
                    $data["Прошлая капитализация"] = ( $data["Прошлая капитализация"] * $arCirculationRate)/ $arReportRate;
                }
            }
		}
	}

	$arItem = array(
	  "UF_KVARTAL" => $quarter,
	  "UF_YEAR" => $year,
	  "UF_COMPANY" => $company,
	  "UF_DATA" => $DB->ForSql(json_encode($data)),
	);
 
	$sql = "SELECT `ID` FROM `hl_obl_company_data_usa` WHERE ";
    $where = array();
	if((is_array($date) && boolval($date[1]))){
        $where[] = "`UF_KVARTAL` = '".$date[0]."'";
    }
    $sqlYear = (is_array($date) && boolval($date[1])) ? $date[1] : $date[0];
	$where[] = "`UF_YEAR` = '$sqlYear'";
    $where[] = "`UF_COMPANY` = '$company'";
    $where = implode(" AND ", $where);
    $sql .= $where;

    $res = $DB->Query($sql);
    //CLogger::periodCorpUsaFindSql($sql);
	if($item = $res->fetch()){
	    $id = $item['ID'];
        $updateValue = array();
	    foreach ($arItem as $sk=>$sv){
            $updateValue[] = "`".$sk."`='".$sv."'";
        }
	    $addUpdateSql = "UPDATE `hl_obl_company_data_usa` SET ".implode(', ', $updateValue)." WHERE `ID` = $id";
	} else {
            $addUpdateSql = "INSERT INTO `hl_obl_company_data_usa` (`" . implode("`, `",
                array_keys($arItem)) . "`) VALUES ('" . implode("', '", array_values($arItem)) . "')";
	}
    $DB->Query($addUpdateSql);
	//CLogger::periodCorpUsaSql($addUpdateSql);
}

function getCompany($name, $arCompanyUSA_Pages, $arCompanyUSA ){
	CModule::IncludeModule("iblock");
	$name = trim($name);
	if(array_key_exists($name, $arCompanyUSA))
	{
		return $arCompanyUSA[$name];

	} else {
		$el = new CIBlockElement;

		$arLoadProductArray = Array(
		  "IBLOCK_ID"      => 43,
		  "NAME"           => $name,
		  "ACTIVE"         => "Y",
		  //"CODE" => Cutil::translit($data[15],"ru"),
		);

		$newCompany = $el->Add($arLoadProductArray);
		if(!$newCompany){

			?><b>Есть ошибка!</b><br><?
			?><pre><?print_r($el->LAST_ERROR)?></pre><?
			?><pre><?print_r($arLoadProductArray)?></pre><?
			?><pre><?print_r($name)?></pre><?
			return false;
		} else {
			$arCompanyUSA[$name] = $newCompany; //Дописываем в общий массив компаний США
		}
	}

	//создаем страницу компании
	if(array_key_exists($name,$arCompanyUSA_Pages)==false)
	{
		$char = ord(substr($name, 0 ,1));
		if($char<=122){
			$sort = 20;
		} else {
			$sort = 10;
		}

		$el = new CIBlockElement;

		$arLoadProductArray = Array(
		  "IBLOCK_ID"      => 44,
		  "NAME"           => $name,
		  "ACTIVE"         => "Y",
		  "CODE" => Cutil::translit($name,"ru"),
		  "SORT" => $sort
		);

		$newCompanyPage = $el->Add($arLoadProductArray);
		if(!$newCompanyPage){
			?><b>Есть ошибка!</b><br><?
			?><pre><?print_r($el->LAST_ERROR)?></pre><?
			?><pre><?= $name ?><?print_r($arLoadProductArray)?></pre><?
			?><pre><?print_r($name)?></pre><?
			exit();
		} else {
			$arCompanyUSA_Pages[$name] = $newCompanyPage; //Дописываем в общий массив страниц компаний США
		}
	}

	if(array_key_exists($name, $arCompanyUSA))
	{
		return $arCompanyUSA[$name];

	}
}

function clearCompanyPageCache($arCompanyUSA_Pages){
	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	foreach($arCompanyUSA_Pages as $cp){
		$cache->clean("company_usa_data".$cp["ID"]);
	}
}

function setCompanyPageParams($company, $code, $reporDate, $finYearStartDate, $periodOffset, $halfYear, $currency, $curr_report, $curr_obr, $curr_div){


	CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>44, "NAME"=>$company);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	if($item = $res->GetNext())
	{

		if ($code) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $code, "CODE");
		}
		if ($reporDate) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $reporDate, "REPORT_DATE");
		}
		if ($finYearStartDate) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $finYearStartDate, "FIN_YEAR_START_DATE");
		}
		if ($periodOffset!==false) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $periodOffset, "PERIOD_OFFSET");
		}
		if ($halfYear!==false) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $halfYear, "HALF_YEAR");
		}

		if ($currency) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $currency, "CURRENCY");
		}
		if ($curr_report) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $curr_report, "CURRENCY_REPORT");
		}
		if ($curr_obr) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $curr_obr, "CURRENCY_CIRCULATION");
		}
		if ($curr_div) {
			CIBlockElement::SetPropertyValues($item["ID"], 44, $curr_div, "CURRENCY_DIVIDENDS");
		}

	}
}
?>
    <h1>Загрузка данных по компаниям США</h1>
    <p><i>Если загружается отчетность за 4 квартал предыдущего года в текущем, то следует установить год загружаемой отчетности. Пример: грузим 4кв.2019 года в январе 2020г. -Значит нужно выбрать 2019 год в списке "Загружаемые годы с" и указать 4 квартал.<br>
            Если требуется загрузить данные (обновить для компаний) за несколько лет - то нужно указать во втором поле "по" конечный (больший) год диапазона. Пример: <strong>с 2018 по 2020</strong><br>
            Если квартал не указан, то будут проверяться <strong>все 4 квартала</strong> для указанного года или диапазона лет.</i></p>
    <form enctype="multipart/form-data" method="post">
        <div class="form-group">

            <label for="operate_year">Загружаемые годы с </label>
            <select name="operate_year">
				<?$y=date('Y');
				while($y>date('Y')-10):?>
                    <option value="<?= $y ?>" <?=($_REQUEST["operate_year"]==$y?'selected':'')?>><?= $y ?></option>
					<?$y--;?>
				<?endwhile;?>
            </select>
            &nbsp;&nbsp;
            <label for="operate_year">по </label>
            <select name="operate_year_end">
                <option value="" <?=(empty($_REQUEST["operate_year_end"])?'selected':'')?>> без диапазона </option>
				<?$y=date('Y')+1;
				while($y>date('Y')-10):?>
                    <option value="<?= $y ?>" <?=($_REQUEST["operate_year_end"]==$y?'selected':'')?>><?= $y ?></option>
					<?$y--;?>
				<?endwhile;?>
            </select>
            <br><br>
            <label for="operate_year">Обрабатываемый квартал (указать, если нужно обработать конкретный квартал)</label>
            <input name="operate_kv" value=""><br><br>

            <label for="exampleInputFile">Отчет по компаниям</label>
            <input type="file" name="file" id="exampleInputFile">
        </div>
        <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
    </form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>