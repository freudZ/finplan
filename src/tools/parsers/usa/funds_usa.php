<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//setlocale(LC_ALL, 'en_US.utf8');
?>
<?use Bitrix\Highloadblock as HL;
use PolygonIO\Rest\Rest;
require $_SERVER["DOCUMENT_ROOT"].'/vendor/polygon-io/api/src/rest/Rest.php';
require $_SERVER["DOCUMENT_ROOT"].'/vendor/polygon-io/api/src/rest/common/Mappers.php';
global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

?>
<h1>Загрузка данных по фондам США</h1>
<?
Global $DB;
//Получаем тикер с полигона по cusip
function getTickerByCusip($rest, $cusip=0){
 $params = array("limit"=>1, "cusip"=>$cusip);
 $data = $rest->reference->tickers->get($params);
 $arResult = array();
 foreach($data["results"] as $arItem){
  $arResult = array("TYPE"=>$arItem["type"], "TICKER"=>$arItem["ticker"]);
 }
	return $arResult;
}

//Получаем уникальные названия фондов
$arFunds = array();
$query = "SELECT DISTINCT `UF_FUND` FROM `hl_usa_funds_data` ORDER BY `UF_FUND`";
$res = $DB->Query($query);
$arFunds = array();
while($row = $res->fetch()){
  $arFunds[] = $row["UF_FUND"];
}




if(isset($_FILES['fileXml']) && $_POST['submit']){

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

$rest = new Rest('ZmCXrWbo2hzVHWQtmyn9UJ3rlGWE3tKL');

//Получаем список тикеров и cusip ранее загруженных

$query = "SELECT DISTINCT `UF_CUSIP`, `UF_TICKER`, `UF_TYPE` FROM `hl_usa_funds_data` WHERE `UF_TICKER` != '' ORDER BY `UF_TICKER`";
$res = $DB->Query($query);
$arTickers = array();
while($row = $res->fetch()){
  $arTickers[$row["UF_CUSIP"]] = array("TYPE"=>$row["UF_TYPE"], "TICKER"=>$row["UF_TICKER"]);
}



$uploaddir = $_SERVER["DOCUMENT_ROOT"].'/upload/fundsXml/';
$uploadfile = $uploaddir . basename($_FILES['fileXml']['name']);

if (move_uploaded_file($_FILES['fileXml']['tmp_name'], $uploadfile)) {
    echo "<div class='bg-success'>Файл корректен и был успешно загружен.</div>";
} else {
    echo "<div class='bg-warning'>Возможная атака с помощью файловой загрузки!</div>";
}

 $response_xml_data = file_get_contents($uploadfile);
 if($response_xml_data){
 	 echo "<div class='bg-success'>Содержимое файла прочитано</div>";
 }

try {
	$infoTable = new SimpleXMLElement($response_xml_data);
		echo "<div class='bg-success'>Файл содержит xml данные</div>";
	}
catch (Exception $e) {
	 echo "<div class='bg-warning'>Ошибка: ".$e->getMessage()."</div>";
	 die();
} finally {

}

Global $DB;
$tableName = '';

$arFundData = array();
$arFundDataMySql = array();
$insertDate = (new DateTime($_POST["fundDate"]))->format('Y-m-d');
if(isset($_POST["fundNameList"]) && !empty($_POST["fundNameList"])){
  $insertFundName = htmlspecialchars($_POST["fundNameList"]);
}  else {
  $insertFundName = htmlspecialchars($_POST["fundName"]);
}


if(empty($insertFundName)){
	echo "<div class='bg-warning'>Не задано название фонда. Загрузка невозможна.</div>";
} else {
//$insertFundName = htmlspecialchars($_POST["fundName"]);
foreach ($infoTable as $item) {
	$cusip = (string)$item->cusip;
	if(!array_key_exists($cusip, $arTickers)){
		$arPolygonData = getTickerByCusip($rest, $cusip);
		$arTickers[$cusip] = $arPolygonData;
	} else {
		$arPolygonData = $arTickers[$cusip];
	}
	$arFields = array(
	 "UF_DATE"=>"'".$insertDate."'",
	 "UF_FUNDNAME"=>"'".$DB->ForSql($insertFundName)."'",
	 "UF_TICKER"=>"'".(!empty($arPolygonData["TICKER"])?$arPolygonData["TICKER"]:'')."'",
	 "UF_TYPE"=>"'".(!empty($arPolygonData["TYPE"])?$arPolygonData["TYPE"]:'')."'",
	 "nameOfIssuer"=> "'".$DB->ForSql((string)$item->nameOfIssuer)."'",
	 "titleOfClass"=> "'".$DB->ForSql((string)$item->titleOfClass)."'",
	 "cusip"=> "'".$cusip."'",
	 "value"=> (string)$item->value,
	 "sshPrnamt"=> "'".(string)$item->shrsOrPrnAmt->sshPrnamt."'",
	 "sshPrnamtType"=> "'".(string)$item->shrsOrPrnAmt->sshPrnamtType."'",
	 "investmentDiscretion"=> "'".(string)$item->investmentDiscretion."'",
	 "UF_PUT_CALL" =>"''",
	 "UF_OTHER_MANAGER" =>"''",
	 "Sole"=> (string)$item->votingAuthority->Sole,
	 "Shared"=> (string)$item->votingAuthority->Shared,
	 "None"=> (string)$item->votingAuthority->None,
	);

	$arFundData[] = $arFields;
  	$arFundDataMySql[] = "(".implode(",", $arFields).")";

} //if(empty($insertFundName))
  echo "<div class='bg-success'>Определен фонд: '".$DB->ForSql($insertFundName)."'</div><br>";
if(count($arFundDataMySql) && !empty($insertFundName)){
 $query = "INSERT INTO `hl_usa_funds_data`(`UF_DATE`, `UF_FUND`, `UF_TICKER`, `UF_TYPE`, `UF_NAME_OF_ISSUER`, `UF_TITLE_OF_CLASS`, `UF_CUSIP`,
 `UF_VALUE`, `UF_PRN_AMT`, `UF_PRN_TYPE`, `UF_INVESTMENT_DISCRETION`, `UF_PUT_CALL`, `UF_OTHER_MANAGER`, `UF_SOLE`, `UF_SHARED`, `UF_NONE`)
 VALUES ".implode(", ", $arFundDataMySql);

 $DB->Query($query);

 $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
 $cache->clean("actions_usa_filters_data");

	unlink($uploadfile);
	} else {
		echo "<div class='bg-error'>Ошибка Не задано название фонда. Загрузка невозможна.</div>";
	}
	echo "<hr>";
	}
}
?>


<form name="fundForm" action="" method="POST" enctype="multipart/form-data">
  <div>
  <p>Если в списке есть нужный фонд то название нужно выбрать из списка</p>
	<label for="fundNameList">Существующие фонды</label>
	 <select name="fundNameList">
		<option value="" <?=(!isset($_POST["fundNameList"]) || $_POST["fundNameList"]==""?'selected':'')?>>Новый</option>
		<?foreach($arFunds as $fundname):?>
			<option value="<?=$fundname?>" <?=(isset($_POST["fundNameList"]) && $_POST["fundNameList"]==$fundname?'selected':'')?>><?=$fundname?></option>
		<?endforeach;?>
	 </select>
  </div>
  <br>
  <div>
   <p>Если в списке нет нужного фонда (загружается для нового фонда) то название нужно ввести в это поле, а в списке выше выбрать "Новый"</p>
	<label for="fundName">Название нового фонда</label><input type="text" name="fundName" id="fundName" value="<?=$_POST["fundName"]?>"/>
  </div>
  <br>
  <div>
  	<p>Указывается дата публикации файла</p>
	<label for="fundDate">Дата</label><input type="date" name="fundDate" id="fundDate" value="<?=$_POST["fundDate"]?>"/>
  </div>
  <br>
  <div>
  	<p>Принимаются только файлы в формате xml скачанные с https://www.sec.gov</p>
	<label for="fileXml">Файл xml</label><input type="file" name="fileXml" id="fileXml" value=""/>
  </div>
  <br>
  <input name="submit" type="submit" value="Загрузить">
</form>
<hr>
<p><span style="color: #FF0000">!!! Не забывайте обновлять актуальный период загруженных данных</span> <a class="red" href="/bitrix/admin/gcustomsettings.php?lang=ru" target="_blank">Здесь</a> (вкладка "Дата для периодов"->"Актуальный период по данным фондов США")<br>
Образец значения свойства: <strong>Данные за 2 квартал 2021г. (обновлено 15.08.21)</strong></p>
<p>Загруженные данные сохраняются в highload инфоблоке <a href="/bitrix/admin/highloadblock_rows_list.php?ENTITY_ID=55&lang=ru" target="_blank">USAFundsData</a> - Отрывается в новой вкладке</p>
<p><strong><i>Кеш списка фондов в фильтре радара очищается после загрузки автоматически, но для обновления данных тикеров требуется ручной сброс кеша акций США!</i></strong></p>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>