<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
// Обновление цен заведеннных не вручную для акции США по тикеру и диапазону цен в формате d.m.Y
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_REQUEST["ticker"]){
   $dateFrom = new dateTime('23.12.2019');
	$postFrom = new dateTime($_REQUEST["from"]);
	$postTo = new dateTime($_REQUEST["to"]);

			if($postFrom<$dateFrom){
			 $postFrom = $dateFrom;
			}

	$url         = 'https://investcab.ru/api/chistory?symbol='.$_REQUEST["ticker"].'&resolution=D&from='.$postFrom->getTimeStamp().'&to='.$postTo->getTimeStamp();
	//$url         = 'https://investcab.ru/api/chistory/';
	$client      = new \GuzzleHttp\Client();
	$syncLogFile = $_SERVER["DOCUMENT_ROOT"] . "/log/priceUsaUpdater_log.txt";

	try {
	   $res     = $client->request("GET", $url);
	   //$res     = $client->get("GET", $url, ['query' => ['symbol' => 'A', 'resolution' => 'D', 'from'=>'1568624480', 'to'=>'1599728540']]);
	   //$res     = $client->get("GET", $url, ['query' => ['symbol' => 'A']]);
		//$res     = $client->request("GET", $url, ['query' => ['symbol=A&resolutionD&from=1568624480&to=1599728540'], 'http_errors' => true]);
		$strJson = array();
		$strJson = json_decode($res->getBody()->getContents(), true);
		$strJson = json_decode($strJson, true);

		 $arSpbPrice = array();
		foreach($strJson["t"] as $k=>$v){
			$jDate = date('d.m.Y', $v);
		  $strJson["t"][$k] = $jDate;
		  $arSpbPrice[$jDate] = array("UF_OPEN" => $strJson["o"][$k], "UF_CLOSE"=>$strJson["c"][$k], "UF_HIGH"=>$strJson["h"][$k], "UF_LOW"=>$strJson["l"][$k]);
		}

			$hlblock   = HL\HighloadBlockTable::getById(29)->fetch();
      	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
      	$entityClass = $entity->getDataClass();

			$arFilter = array("=UF_ITEM"=>$_REQUEST["ticker"], ">=UF_DATE"=>$postFrom->format('d.m.Y'));
			if($postTo<(new DateTime())){
				$arFilter["<=UF_DATE"] = $postTo->format('d.m.Y');
			}

			$resHL = $entityClass::getList(array(
      		"select" => array(
      			"*",
      		),
      		"filter" => $arFilter,
      		"order" => array("UF_DATE"=>"ASC"),
      	));

		$arDBPrice = array();
	  //	$arDBPrice = $resHL->fetchAll();
		while($row = $resHL->fetch()){
		   $arDBPrice[$row["UF_DATE"]->format('d.m.Y')] = array("ID"=> $row["ID"] ,"UF_MANUAL" => $row["UF_MANUAL"], "UF_OPEN" => $row["UF_OPEN"], "UF_CLOSE" => $row["UF_CLOSE"], "UF_HIGH"=>$row["UF_HIGH"], "UF_LOW" => $row["UF_LOW"]);
		}



		foreach($arDBPrice as $date=>$val){
			if(empty($val["UF_MANUAL"])){
				$arNewPrice = array();
				if(array_key_exists($date, $arSpbPrice)){
					echo $date." date exist<br>";
				  $arNewPrice = array(
					 "UF_OPEN"=>$arSpbPrice[$date]["UF_OPEN"],
					 "UF_CLOSE"=>$arSpbPrice[$date]["UF_CLOSE"],
					 "UF_HIGH"=>$arSpbPrice[$date]["UF_HIGH"],
					 "UF_LOW"=>$arSpbPrice[$date]["UF_LOW"],
				  );
				} else{
				  echo $date." date not exist<br>";
				}
				if(count($arNewPrice)>0){

				  $entityClass::update($val["ID"], $arNewPrice);
				}


			}


		}


	} catch (RequestException $e) {

		$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectMoex, request; url: ' . $dataUrl . ' exception: ' . $e->getRequest();
		file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);

		if ($e->hasResponse()) {
			$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectMoex, request; url: ' . $dataUrl . ' exception: ' . $e->getRequest();
			file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);
		}
	}


}

?>
<h1>Обновление цены акции США</h1>
  <p>Цены обновятся за исключением добавленных в ручном режиме</p>
<hr>
<?
/*
 $Act = new Actions();
	$arAct = $Act->GetItem('RU000A0JR4A1');
	 echo "<pre  style='color:black; font-size:11px;'>";
       print_r($arAct);
       echo "</pre>";*/
 ?>
<form enctype="multipart/form-data" method="get">
  <div class="form-group">
    <label for="ticker">Тикер </label>
    <input type="text" name="ticker" value="<?= $_REQUEST["ticker"]?>">
	 <br>
	 <label for="ticker">За период с:</label>
    <input type="datetime" name="from" value="<?= $_REQUEST["from"]?>">

	 <label for="ticker"> по:</label>
    <input type="datetime" name="to" value="<?= $_REQUEST["to"]?>">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Обновить цены</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>