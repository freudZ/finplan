<? define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
/* Данный файл обновляет цены всех акций США заведенные не вручную
	Для указания диапазона дат нужно изменить даты в ф-ции updateActionUsa()

 */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

if(isset($_POST["tickerName"]) && !empty($_POST["tickerName"]) && !empty($_POST["dateFrom"])){
//$sleep = 1;//in seconds
$sleep = 0;//in seconds

$start = microtime(true);
$arTickers = array();
if(strpos($_POST["tickerName"], ",")!==false){
	$arTickers = explode(", ", $_POST["tickerName"]);
} else {
	$arTickers = array($_POST["tickerName"]);
}


function updateActionUsa2($ticker, $from='', $to='', $IblockActionId=0, $addNewPrices=false, $itemQuantity){
	Global $DB, $APPLICATION;
	$formattedDateFrom = (new dateTime($from))->format("d.m.Y");
	$formattedDateTo = (new dateTime($to))->format("d.m.Y");
   $dateFrom = new dateTime($formattedDateFrom.' 00:00:00');
	$postFrom = clone $dateFrom;
	$postTo = new dateTime($formattedDateTo.' 23:59:59');

			if($postFrom<$dateFrom){
			 $postFrom = $dateFrom;
			}

	$url         = 'https://investcab.ru/api/chistory?symbol='.$ticker.'&resolution=D&from='.$postFrom->getTimeStamp().'&to='.$postTo->getTimeStamp();
  //	echo $url.PHP_EOL;
	//$url         = 'https://investcab.ru/api/chistory/';
	$client      = new \GuzzleHttp\Client();
	$syncLogFile = $_SERVER["DOCUMENT_ROOT"] . "/log/reaperPriceUsaUpdater_log.txt";
		$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: updateActionUsa; url: ' . $url;
		file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);
	try {
	   $res     = $client->request("GET", $url);
	   //$res     = $client->get("GET", $url, ['query' => ['symbol' => 'A', 'resolution' => 'D', 'from'=>'1568624480', 'to'=>'1599728540']]);
	   //$res     = $client->get("GET", $url, ['query' => ['symbol' => 'A']]);
		//$res     = $client->request("GET", $url, ['query' => ['symbol=A&resolutionD&from=1568624480&to=1599728540'], 'http_errors' => true]);
		$strJson = array();
		$strJson = json_decode($res->getBody()->getContents(), true);
		$strJson = json_decode($strJson, true);

		 $arSpbPrice = array();
		foreach($strJson["t"] as $k=>$v){
			$jDate =date ('d.m.Y', $v);
		  $strJson["t"][$k] = $jDate;
		  $arSpbPrice[$jDate] = array("UF_OPEN" => $strJson["o"][$k], "UF_CLOSE"=>$strJson["c"][$k], "UF_HIGH"=>$strJson["h"][$k], "UF_LOW"=>$strJson["l"][$k]);
		}

		foreach($arSpbPrice as $date=>$arPrice){
		//Удаляем цену за текущую дату в обеих таблицах
		$queryFormattedDate = (new dateTime($date))->format("Y-m-d");
		if(in_array($ticker, $APPLICATION->crossTickers)){
			$query = "DELETE FROM `hl_polygon_actions_data` WHERE `UF_DATE`='$queryFormattedDate' AND `UF_ITEM`='$ticker'";
			$DB->Query($query);
		}
		$query = "DELETE FROM `hl_spb_actions_data` WHERE `UF_DATE`='$queryFormattedDate' AND `UF_ITEM`='$ticker'";
		$DB->Query($query);

		$query = "INSERT INTO `hl_spb_actions_data` (`UF_LOW`, `UF_HIGH`, `UF_CLOSE`, `UF_OPEN`, `UF_DATE`, `UF_ITEM`, `UF_MANUAL`, `UF_SHOW_GRAPH`, `UF_VOLUME`, `UF_CLOSE_BETA`)
		VALUES ('".$arPrice['UF_LOW']."', '".$arPrice['UF_HIGH']."', '".$arPrice['UF_CLOSE']."', '".$arPrice['UF_OPEN']."', '$queryFormattedDate', '$ticker', 'N', 'Y', 0, 0)";
		$DB->Query($query);

		if(in_array($ticker, $APPLICATION->crossTickers)){
			$query = "INSERT INTO `hl_polygon_actions_data` (`UF_LOW`, `UF_HIGH`, `UF_CLOSE`, `UF_OPEN`, `UF_DATE`, `UF_ITEM`, `UF_MANUAL`, `UF_SHOW_GRAPH`, `UF_VOLUME`, `UF_CLOSE_BETA`)
			VALUES ('".$arPrice['UF_LOW']."', '".$arPrice['UF_HIGH']."', '".$arPrice['UF_CLOSE']."', '".$arPrice['UF_OPEN']."', '$queryFormattedDate', '$ticker', 'N', 'Y', 0, 0)";
			$DB->Query($query);
		 }

					if($_POST["updateLastprice"]=="Y" && $arPrice == end($arSpbPrice)){  //Если требуется обновлять цены и капитализацию в инфоблоке акций США и последняя дата в массиве
					  CIBlockElement::SetPropertyValues($IblockActionId, 55, str_replace(",", ".", $arPrice['UF_CLOSE']), "LASTPRICE");
					  CIBlockElement::SetPropertyValues($IblockActionId, 55, round((str_replace(",", ".", $arPrice['UF_CLOSE']) * $itemQuantity), 2), "ISSUECAPITALIZATION");
					}
			}
		} catch (RequestException $e) {

		$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: updateActionUsa; url: ' . $dataUrl . ' exception: ' . $e->getRequest();
		file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);

		if ($e->hasResponse()) {
			$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: updateActionUsa; url: ' . $dataUrl . ' exception: ' . $e->getRequest();
			file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);
		}
	}

}



$addNewPrices = ($_POST["addNewHLPrice"]=="Y"?true:false);  //Добавлять не существующие цены

$lastpriceDate = '';

$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_SECID", "PROPERTY_SECURITIES_QUANTITY");
//$arFilter = Array("IBLOCK_ID"=>IntVal(55), "=PROPERTY_SECID"=>"AAPL");   //Для обновления и добавления по тикеру конкретной компании
$arFilter = Array("IBLOCK_ID"=>IntVal(55)); //Для обновления и добавления по всем компаниям
$arFilter["=PROPERTY_SECID"] = $arTickers;

if(empty($_POST["dateTo"])){
	$_POST["dateTo"] = $_POST["dateFrom"];
}

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){
 if(!empty($ob["PROPERTY_SECID_VALUE"])){
 	//echo 'updated '.$ob["PROPERTY_SECID_VALUE"];
	updateActionUsa2($ob["PROPERTY_SECID_VALUE"], $_POST["dateFrom"], $_POST["dateTo"], $ob["ID"], $addNewPrices, $ob["PROPERTY_SECURITIES_QUANTITY_VALUE"]);
  	echo '<div class="bg-success">Заменена цена '.$ob["PROPERTY_SECID_VALUE"].'</div>';
	echo PHP_EOL;
	if($sleep>0)
	  sleep($sleep);
 }
}



function clearSpbexGraph(){
/*	$graphCacheFolder = $_SERVER["DOCUMENT_ROOT"]."/bitrix/cache/spbex_graph/";
if (is_dir($graphCacheFolder)) {
    rmdir($graphCacheFolder);
}*/
if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   $GLOBALS['CACHE_MANAGER']->ClearByTag('usa_graphdata');
}

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean('actions_usa_data');
clearSpbexGraph();  //Кэш графиков цен акций США очищен



$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";

}
?>

<div class="container">
 <div class="row">
 <div class="col-md-12">
<h1>Обновление/добавление цен на акции США с сайта investcab.ru (СПБ)</h1>
<div>* Цены для указанных тикеров и дат будут предварительно удалены и затем добавлены вновь</div> <br>
<div><i>Тикеры немецких акций, которые дублируются в таблицу полигона</i><br>
 <div><?=implode(", ", $APPLICATION->crossTickers);?></div>
</div>
<hr>
<form name="PriceForm" action="" method="POST" enctype="multipart/form-data">
  <div>
	<label for="tickerName">Тикер/тикеры через запятую</label>
	 <input type="text" name="tickerName" value="<?=$_POST["tickerName"]?>">
	 <br>
	 <br>
	 <div><i>Если требуется обновить цены за одну дату - правое поле "по" заполнять датой не обязательно</i></div>
	 <label for="date">Дата с&nbsp;</label><input type="date" name="dateFrom" value="<?=$_POST["dateFrom"]?>"> по <input type="date" name="dateTo" value="<?=$_POST["dateTo"]?>"><br>
	 <div><i>При установленной галке будет перезаписана последняя цена в инфоблок акции и пересчитана текущая капитализация</i></div>
	 <label for="updateLastprice">Обновить последнюю цену в инфоблоке акции&nbsp;</label><input type="checkbox" name="updateLastprice" value="Y" <?=(isset($_POST["updateLastprice"]) && $_POST["updateLastprice"]=="Y"?'checked':'')?>><br>
	 <input name="submit" type="submit" value="Обновить цены">
  </div>

</form>
</div>
</div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>