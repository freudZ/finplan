<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
 use Bitrix\Highloadblock as HL;
	CModule::IncludeModule("iblock");
	CModule::IncludeModule('highloadblock');
?>
<?$APPLICATION->SetTitle("Пересчитать дивиденды акций США");?>
<div class="wrapper gray_bg" itemscope itemtype="http://schema.org/Article">
    <div class="container">
        <div class="main_content white_bg">
            <div class="main_content_inner">
<?
  //$sync = new MoexSync();
  //$sync->updateYearDividends(60728, 32, 'RU0007288411', 20702);

/*  //Обновляет годовые дивиденды для акции США
  function updateYearDividends($ElementId, $IblockId, $code, $LastPrice){


		  CModule::IncludeModule('highloadblock');
   //     $hlblock = HL\HighloadBlockTable::getById(32)->fetch();
   //     $dataClass = HL\HighloadBlockTable::compileEntity($hlblock);

 $hlblock = HL\HighloadBlockTable::getById(32)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $dataClass= $entity->getDataClass();

      $res = $dataClass::getList(array(
          "filter" => array(
              "UF_ITEM" => $code,
          ),
			 "limit" => 1,
          "select" => array("ID", "UF_ITEM", "UF_DATA"),
			 //"cache" => array("ttl" => 3600)
      )
		);

	  if ($hlItem = $res->fetch()) {
	  	if(!empty($hlItem["UF_DATA"])){
		  $divData = json_decode($hlItem["UF_DATA"], true);
		  if(!count($divData)) return;
			$endDate = '';  // Дата окончания годового периода
			$startDate = '';  // Дата начала годового периода
			$now = new DateTime(date());

			//Поиск окончания диапазона
			foreach($divData as $coupon){
			  $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
			  if($now<$closeDate){
			  	$endDate = $closeDate;
			  } else if($now>=$closeDate && empty($endDate)){
				  $endDate = $closeDate;
				 break;
			  }
			}

			$startDate = (clone $endDate)->modify('-1 year');

			$YearSum = 0;
			$YearPrc = 0;
			$Description = '';
			foreach($divData as $coupon){
			  $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
			  if($closeDate<=$endDate && $closeDate>=$startDate){
				 $YearSum += $coupon["Дивиденды"];
				 $textDiv = explode(" (",$coupon["Дивиденды"]); //Выкидываем пересчитанную в рубли сумму в скобках
			 //	 $Description .= (strlen($Description)>0?'; ':'').$coupon["Дивиденды"].' руб. ('.$closeDate->format('d.m.Y').' - '.($now<$closeDate?'рекомендовано':'выплачено').')';
				 $Description .= (strlen($Description)>0?'; ':'').$textDiv[0].' от '.$closeDate->format('d.m.Y').' - '.($now<$closeDate?'рекомендовано':'выплачено');
			  }

			}

			$Description = ""."(".$Description.")";

			if(floatval($LastPrice)>0){
			$YearPrc = round($YearSum/$LastPrice*100,2);

			CIBlockElement::SetPropertyValues($ElementId, $IblockId, $YearSum, "DIVIDENDS");
			CIBlockElement::SetPropertyValues($ElementId, $IblockId, $Description, "SOURCE");

			}


		}
	  }

	 }//updateYearDividends*/
require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_tools.php" );
$fptTools = new fptTools();
$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_LASTPRICE");
$arFilter = Array("IBLOCK_ID"=>IntVal(55));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){

 $fptTools->updateYearDividends($ob["ID"], 55, $ob["CODE"], $ob["PROPERTY_LASTPRICE_VALUE"]);
}
$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean("actions_usa_data");

 ?>
 				<div class="success">Дивиденды пересчитаны</div>
            </div>
            </div>
            </div>
            </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>