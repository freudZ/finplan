<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//setlocale(LC_ALL, 'en_US.utf8');
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}


CModule::IncludeModule("iblock");

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){

	$inputFileName = $_FILES["file"]["tmp_name"];
	$i = 0;

	$items = explode("\n", file_get_contents($inputFileName));

	foreach ($items as $handle) {
		$i++;
		//$handle = iconv("CP1251", "UTF-8", $handle);
		$data = explode(";", $handle);
		if(count($data)>50){
			?>
			<p>Ошибка файла</p>
			<pre><?print_r($data)?></pre>
			<?exit();
		}
		if($i==1){
			continue;
		}

		if($data[1]){
			$parentName = $data[1];
		}


		$data[3] = str_replace(",", ".", $data[3]);
		$data[3] = str_replace(" ", "", $data[3]);
		$data[3] = trim($data[3]);
		 //TODO Переделать на множество валют если будут дивы в отличной от доллара и евро валютах
		$haveValute = "";
		if(strpos($data[3], "$")!==false){
			$haveValute = "$";
			$data[3] = str_replace("$", "", $data[3]);
		}
		if(strpos($data[3], "€")!==false){
			$haveValute = "€";
			$data[3] = str_replace("€", "", $data[3]);
		}


		if(ceil($data[3])){
			if(array_key_exists(trim($data[2]),$arData[$parentName])){  //Если такая дата уже есть - суммируем дивиденды

			 $existsValue = $arData[$parentName][trim($data[2])];  //Получаем текущее значение дивидендов
				$haveValute = "";
				if(strpos($existsValue, "$")!==false){  //Отрезаем знак валюты
					$haveValute = "$";
					$existsValue = str_replace("$", "", $existsValue);
				}
				if(strpos($existsValue, "€")!==false){  //Отрезаем знак валюты
					$haveValute = "€";
					$existsValue = str_replace("€", "", $existsValue);
				}
			 $arData[$parentName][trim($data[2])] = strval(floatval($existsValue)+floatval($data[3])).$haveValute;//Суммируем и возвращаем валюту
			} else {
			$arData[$parentName][trim($data[2])] = $data[3].$haveValute;
			}
		}
	}


	 $cnt = 0;
	if($arData){//Если собраны данные



//Получим список акций США
$arActionsCode = array();
$arActionsIsinCode = array();
$arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_ISIN", "PROPERTY_SECID");
$arFilter = Array("IBLOCK_ID"=>IntVal(55));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->Fetch()){
 $arActionsCode[$ob["PROPERTY_SECID_VALUE"]] = array("ID"=>$ob["ID"], "CODE"=>$ob["CODE"], "ISIN"=>$ob["PROPERTY_ISIN_VALUE"]);
 $arActionsIsinCode[$ob["PROPERTY_ISIN_VALUE"]] = array("ID"=>$ob["ID"], "CODE"=>$ob["CODE"], "SECID"=>$ob["PROPERTY_SECID_VALUE"]);
}



//Получим список страниц акций
$arActionsPages = array();
$arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE");
$arFilter = Array("IBLOCK_ID"=>IntVal(56));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->Fetch()){
 $arActionsPages[$ob["CODE"]] = $ob["ID"];
}

//TODO Проверить необходимость скользящих дивов для америки
//+ Посчитаем и запишем скользящие дивиденды
/*		include($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_actions_dividents.php");
		$cDiv = new fptActionsDividends(); //Получаем скользящие дивиденды
		$arSlideDivs = $cDiv->arActionsSlideDiv;
		$cntSlideDivs = 0;
		if(is_array($arSlideDivs) && count($arSlideDivs)>0){
			foreach($arSlideDivs as $isin=>$arSlideDivsData){
				$apCode = $arActionsIsinCode[$isin]["CODE"];
				if(!empty($apCode)){

					$apId = $arActionsPages[$apCode];
					if(intval($apId)>0){

	     			if(count($arSlideDivsData)>0){
					$prop["SLIDING_DIVS"] = array('VALUE'=>array('TYPE'=>'HTML', 'TEXT'=>serialize($arSlideDivsData)));
					} else {
					$prop["SLIDING_DIVS"] = array('VALUE'=>array('TYPE'=>'HTML', 'TEXT'=>''));
					}
					CIBlockElement::SetPropertyValuesEx($apId, 56, $prop);
					$cntSlideDivs++;
						}
				}
			}
		}*/
//- Посчитаем и запишем скользящие дивиденды

		//Запишем собранные дивиденды из загруженного файла
		foreach($arData as $code=>$vals){
			$apCode = $arActionsCode[$code]["CODE"];

			if(!empty($apCode))
			{
			 $apId = $arActionsPages[$apCode];

				if(intval($apId)>0)
				{
					unset($prop["SLIDING_DIVS"]);
					$prop["DIVIDENDS"] = array('VALUE'=>array('TYPE'=>'HTML', 'TEXT'=>serialize($vals)));
					CIBlockElement::SetPropertyValuesEx($apId, 56, $prop);
					$cnt++;
				} else {
					?><p>Не найдена страница акции - <?=$apCode?></p><?
				}
			} else {
				?><p>
				Не найдена акция по SECID - <?=$code?>
				</p><?
			}
		}

		?>
		<p>Готово.<br>Обновлено страниц акций: <?= $cnt ?><br>Обновлено страниц скольз. дивидендов: <?= $cntSlideDivs ?></p>
		<?exit();
	}
}
?>
<h1>Загрузка дивидендов по акциям США</h1>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Выберите файл</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>