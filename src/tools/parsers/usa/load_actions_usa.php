<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if (!$USER->IsAdmin() && !in_array(5, $arGroups)) {
    exit();
}

if ($_REQUEST["send"] && $_FILES["file"]["tmp_name"]) {
    $arMes = [
        "SUCCESS" => 0,
        "ERR" => array(),
        "EMPTY_ISIN" => array()
    ];

//подготовка данных для заполнения цены и динамики цен
$Yesturday = (new DateTime())->modify("-1 days");
$dates = array(
    'YESTURDAY' => $Yesturday->format('d.m.Y'),
    'MONTH' => (clone $Yesturday)->sub(new DateInterval('P1M')),
    'ONE_YEAR' => (clone $Yesturday)->sub(new DateInterval('P1Y')),
    'THREE_YEAR' => (clone $Yesturday)->sub(new DateInterval('P3Y')),
		);
 unset($Yesturday);
	foreach ($dates as $k => $v) {
	  if($k!='YESTURDAY'){
	  	if (isUsaWeekendDay($v->format("d.m.Y"))) {
	        $finded = false;
	        while (!$finded) {
	            $v->modify("+1 days");
	            if (!isUsaWeekendDay($v->format("d.m.Y"))) {
	                $finded = true;
	            }
	        }
	    }
	    $dates[$k] = $v->format("d.m.Y");
	  }
	}



$hlblock = HL\HighloadBlockTable::getById(29)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$arActionsUSAPrices = array(); // Последние цены на вчера
foreach ($dates as $k => $date) {
    $res = $entity_data_class::getList(array(
        "filter" => array("UF_DATE" => $date),
        "select" => array(
            "UF_CLOSE",
            "UF_ITEM",
        ),
		"order"=>array("UF_DATE"=>"desc"),
		"group"=>array("UF_ITEM"),

    ));

    while ($item = $res->fetch()) {
    	if($k!='YESTURDAY') {
        $quotationsData[$k][$item["UF_ITEM"]] = floatVal($item["UF_CLOSE"]);
		  } else {
		  $arActionsUSAPrices[$item["UF_ITEM"]] = floatVal($item["UF_CLOSE"]);
		  }
    }
}

/*	  echo "<pre  style='color:black; font-size:11px;'>";
        print_r($arActionsUSAPrices);
        echo "</pre>";*/

    $inputFileName = $_FILES["file"]["tmp_name"];
    $el = new CIBlockElement;

    $i = 0;
    if (($handle = fopen($inputFileName, "r")) !== FALSE) {

        $arProps = array();
        $properties = CIBlockProperty::GetList(["sort" => "asc", "name" => "asc"], ["ACTIVE" => "Y", "IBLOCK_ID" => 55]);
        while ($prop_fields = $properties->GetNext()) {
            $arProps[$prop_fields["CODE"]] = 1;
        }

        $arFieldsNum = [];

        while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
            $i++;

            if ($i === 1) {
                foreach ($data as $n => $v) {
                    if ($arProps[$v]) {
                        $arFieldsNum[$n] = $v;
                    }
                }
            }

            if ($i <= 2) {
                continue;
            }

            if (!$data[6]) {
                $arMes["EMPTY_ISIN"][] = $data[0] . " - " . $data[5];
                continue;
            }

            $PROPS = [];
            foreach ($arFieldsNum as $n => $code) {
            	if($code=='SECURITIES_QUANTITY'){
					  $data[$n] = str_replace(" ","",$data[$n]);
					  $data[$n] = str_replace(",",".",$data[$n]);
					  $data[$n] = round(floatval($data[$n]));
            	}
                $PROPS[$code] = $data[$n];
            }

		      if(array_key_exists($data[5],$arActionsUSAPrices)) //проверяем наличие цен по SECID (Код бумаги, тикер)
				$PROPS["LASTPRICE"] = $arActionsUSAPrices[$data[5]];

            //Динамика акций - добавляем в массив свойств для обновления или добавления элемента
            foreach ($dates as $k => $date) {
            	if($k!='YESTURDAY'){
                if (isset($quotationsData[$k][$data[5]])) {
                	  $PROPS["QUOTATIONS_$k"] = floatVal($quotationsData[$k][$data[5]]);
                }
               }
            }


            $newActionData = Array(
                "IBLOCK_ID" => 55,
                "PROPERTY_VALUES" => $PROPS,
                "NAME" => $data[0],
                "CODE" => $data[6],
                "ACTIVE" => "Y",
            );

            $arFilter = ["IBLOCK_ID" => 55, "CODE" => $data[6]];
            $res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "IBLOCK_ID", "NAME", "PROPERTY_EMITENT_ID"]);
            if ($item = $res->GetNext()) {
                unset($newActionData["PROPERTY_VALUES"]);
					 //Определяем эмитента по названию и добавляем в свойства элементов
                $PROPS["EMITENT_ID"] = setCompany($data, $item);

                $el->Update($item["ID"], $newActionData);
					 checkActionUsaPage($newActionData);//Создаем страницу акции США
                foreach ($PROPS as $propCode => $value) {
                    CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], str_replace(",", ".", $value), $propCode);
                }
                if ($PROPS) {
                    $arMes["UPDATED"]++;
                } else {
                    $arMes["ERR"][] = $data[6];
                }
            } else {
                if($res = $el->Add($newActionData)){ //Если создали новую акцию то проверим и присвоим ей эмитента
                 $newActionData["ID"] = $res;
					  $value = setCompany($data, $newActionData["ID"]);
					  CIBlockElement::SetPropertyValues($newActionData["ID"], $newActionData["IBLOCK_ID"], str_replace(",", ".", $value), "EMITENT_ID");
                }
					 checkActionUsaPage($newActionData);//Создаем страницу акции США
                if ($PROPS) {
                    $arMes["ADDED"]++;

                } else {
                    $arMes["ERR"][] = $data[6];
                }
            }
        }
        fclose($handle);

        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cache->clean("actions_usa_data");
        $cache->clean("actions_usa_filters_data");
        ?>
        <p><b>Обновлено (кол-во акций):</b> <?= $arMes["UPDATED"] ?></p>
        <p><b>Добавлено (кол-во акций):</b> <?= $arMes["ADDED"] ?></p>
        <p><b>Не найдено акций:</b> <?= implode(", ", $arMes["ERR"]) ?></p>
        <p><b>Не загружены (нет значения "ISIN-код бумаги"):</b> <?= implode(", ", $arMes["EMPTY_ISIN"]) ?></p>
        <?
        exit();
    }
}

//Проверяем и создаем страницу акции
function checkActionUsaPage($arActionData){
	$num = CIBlockElement::GetList(false,array("IBLOCK_ID"=>56, "ACTIVE"=>"Y", "CODE"=>$arActionData["CODE"]),array(), false, array("ID","NAME"));
	if(intval($num)<=0){
	  unset($arActionData["PROPERTY_VALUES"]);
	  $arActionData["IBLOCK_ID"] = 56;
	  $el = new CIBlockElement;
	  $el->Add($arActionData);
	}
}

//проверка и создание компании
function setCompany($data, $actionItem)
{
    $return = "";

    $arFilter = ["IBLOCK_ID" => 43, "NAME" => $data[9]];
    $res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "NAME", "PROPERTY_EMITENT_ID"]);
    if ($item = $res->GetNext()) {
        if (!$item["PROPERTY_EMITENT_ID_VALUE"]) {
            CIBlockElement::SetPropertyValues($actionItem["ID"], $actionItem["IBLOCK_ID"], $item["ID"], "EMITENT_ID");
        }

        $return = $item["ID"];
    } else {
        $el = new CIBlockElement;

        $arLoadProductArray = Array(
            "IBLOCK_ID" => 43,
            "NAME" => $data[9],
            "ACTIVE" => "Y",
        );

        $newCompany = $el->Add($arLoadProductArray);

        if (!$newCompany) {
            ?><b>Есть ошибка! (setCompany-1)</b><br><?
            ?>
            <pre><?
        print_r($el->LAST_ERROR) ?></pre><?
            ?>
            <pre><?
        print_r($arLoadProductArray) ?></pre><?
            ?>
            <pre><?
        print_r($data) ?></pre><?
            exit();
        }

        CIBlockElement::SetPropertyValues($actionItem["ID"], $actionItem["IBLOCK_ID"], $newCompany, "EMITENT_ID");

        $return = $newCompany;
    }


    //создаем страницу компании
	 $companyCode = Cutil::translit($data[9], "ru");
    $arFilter = Array("IBLOCK_ID" => 44, "NAME" => $data[9]);
    //$arFilter = Array("IBLOCK_ID" => 44, "CODE"=>$companyCode);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME", "PROPERTY_EMITENT_ID"));
    if (!$res->SelectedRowsCount()) {
        $char = ord(substr($data[9], 0, 1));
        if ($char <= 122) {
            $sort = 20;
        } else {
            $sort = 10;
        }

        $el = new CIBlockElement;

        $arLoadProductArray = Array(
            "IBLOCK_ID" => 44,
            "NAME" => $data[9],
            "ACTIVE" => "Y",
            "CODE" => $companyCode,
            "SORT" => $sort
        );

        $newCompany = $el->Add($arLoadProductArray);
        if (!$newCompany) {
            ?><b>Есть ошибка! (setCompany-2)</b><br><?
				echo $data[9];
            ?>
            <pre><?
        print_r($el->LAST_ERROR) ?></pre><?
            ?>
            <pre><?
        print_r($arLoadProductArray) ?></pre><?
            ?>
            <pre><?
        print_r($data) ?></pre><?
            exit();
        }
    }

    return $return;
}

?>
<h1>Загрузка данных по акциям США</h1>
<form enctype="multipart/form-data" method="post">
    <div class="form-group">
        <label for="exampleInputFile">Данные по акциям</label>
        <input type="file" name="file" id="exampleInputFile">
    </div>
    <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>