<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//setlocale(LC_ALL, 'en_US.utf8');
use Bitrix\Highloadblock as HL;

global $USER, $DB;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

$text_chunk_size = 50000;
if ($DB->type === "MYSQL")
  $text_chunk_size = 50000;
else
  $text_chunk_size = 1900;

if( !function_exists('chunked')){
	function chunked($text, $partSize) {
    //$partSize = floor(strlen($text)/$parts);
    //$parts = floor(strlen($text)%$partSize);
    $parts = ceil(strlen($text)/$partSize);
    $tailSize = strlen($text)%$parts;
    $arr = array();
    for ($i = 0; $i < $parts; $i++) {
       $arr[$i] = array("VALUE"=>array("TEXT"=>substr($text, $i*$partSize, $partSize), "TYPE"=>"HTML"),"DESCRIPTION"=>$i );
    }
    // Хвост
    if ($tailSize != 0) {
        $end = substr($text, $parts*$partSize, $tailSize);
		  $arr[$parts] = array("VALUE"=>array("TEXT"=>$end, "TYPE"=>"HTML"),"DESCRIPTION"=>$parts );
    }
    return $arr;
}
}


$arParentNames = array(
	"Отчет о прибылях и убытках",
	"Бухгалтерский баланс",
	"Cash Flow (Данные для DCF)",
);

CModule::IncludeModule("iblock");
if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	if(!$_REQUEST["company"]){
		?>
		<p>Обновите и выберите компанию</p>
		<?exit();
	}
	
	$inputFileName = $_FILES["file"]["tmp_name"];
	$i = 1;
	
	$items = explode("\n", file_get_contents($inputFileName));
	
	foreach ($items as $handle) {
		$data = explode(";", $handle);
		if(count($data)>50){
			?>
			<p>Ошибка файла</p>
			<pre><?print_r($data)?></pre>
			<?exit();
		}
		
		if(in_array($data[1], $arParentNames)){
			$arPeriods = array();
			foreach($data as $n=>$period){
				if($n<2){
					continue;
				}
				$arPeriods[$n] = trim($period);
			}
			$parentName = $data[1];
		} else {
			if(!$data[1]){
				continue;
			}
			
			$tmp = array(
				"name" => $data[1],
				"bold" => $data[0],
			);
			foreach($arPeriods as $n=>$period){
				if(trim($data[$n])){
					$tmp["items"][$period] = $data[$n];
				}
			}
			
			if($tmp["items"]){
				$arData[$parentName][$tmp["name"]] = $tmp;
			}
		}
	}

	if($arData){
		/*CModule::IncludeModule("highloadblock");

		$hlblock   = HL\HighloadBlockTable::getById(16)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();
		
		$arItem = array(
			"UF_ITEM" => $_REQUEST["company"],
			"UF_DATA" => json_encode($arData),
		);
		
		$res = $entityClass::getList(array(
			"filter" => array(
				"UF_ITEM" => $_REQUEST["company"],
			),
			"select" => array(
				"ID"
			),
		));
		if($item = $res->fetch()){
			$entityClass::update($item["ID"], $arItem);
		} else {
			$entityClass::add($arItem);
		}*/
		$arJsonData = json_encode($arData, JSON_UNESCAPED_UNICODE );

		$arData = chunked($arJsonData, $text_chunk_size);
		ksort($arData); //Сортируем для правильной последовательности фрагментов. Дополнительно в Description указываем порядковый номер фрагмента

		CIBlockElement::SetPropertyValues($_REQUEST["company"], 29, array(), "EXT_DATA_LONG");
	  	CIBlockElement::SetPropertyValues($_REQUEST["company"], 29, $arData, "EXT_DATA_LONG");

		//$t = CIBlockElement::SetPropertyValues($_REQUEST["company"], 29, json_encode($arData, JSON_UNESCAPED_UNICODE ), "EXPANDED_DATA");
		?><pre>Загружено частей данных: <?print_r(count($arData))?></pre><?
		?>
		<p>Готово</p>
		<?exit();
	}
}

CModule::IncludeModule("iblock");
$arCompanies = array();
$arFilter = Array("IBLOCK_ID"=>29, "!PROPERTY_EXPANDED"=>false);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "CODE", "NAME"));
while($item = $res->GetNext())
{
	$arCompanies[] = $item;
}
?>
<h1>Загрузка данных по компании (расширенная)</h1>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Расширенная инфа по компании</label>
    <input type="file" name="file" id="exampleInputFile">
  </div> 
  <div class="form-group">
    <label for="exampleInputFile">Компания</label>
    <select name="company">
		<?foreach($arCompanies as $item):?>
			<option value="<?=$item["ID"]?>"><?=$item["NAME"]?></option>
		<?endforeach?>
	</select>
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>