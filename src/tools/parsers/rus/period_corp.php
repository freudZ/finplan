<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//Заметки:
/*
 * Компания Инград, по ней було изменение тикера акций с OPIN на INGR. Раньше 2020 года биржа не отдает в историю данные по этим акциям.
 */
@set_time_limit(0);

use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

CModule::IncludeModule("eremin.finplantools");//Модуль инструментов для пересчета капы

function setNextMonday($date){
	$dt = new DateTime($date);
	
	if($dt->format("N")==6){
		$dt->modify("+2 days");
	} elseif($dt->format("N")==7){
		$dt->modify("+1 days");
	}
	
	if(isWeekEndDay($dt->format("d.m.Y"))){
		$finded = false;
		while(!$finded){
			$dt->modify("+1 days");
			if(!isWeekEndDay($dt->format("d.m.Y"))){
				$finded = true;
			}
		}
	}
	
	return $dt->format("Y-m-d");
}

function setPrevWorkday($date){
	$dt = new DateTime($date);

	if($dt->format("N")==6){
		$dt->modify("-1 days");
	} elseif($dt->format("N")==7){
		$dt->modify("-2 days");
	}

	if(isWeekEndDay($dt->format("d.m.Y"))){
		$finded = false;
		while(!$finded){
			$dt->modify("-1 days");
			if(!isWeekEndDay($dt->format("d.m.Y"))){
				$finded = true;
			}
		}
	}

	return $dt->format("Y-m-d");
}

CModule::IncludeModule("iblock");


if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	
	$arMes = array(
		"SUCCESS" => 0,
		"ERR" => array()
	);
	$inputFileName = $_FILES["file"]["tmp_name"];
	$i = 1;
	
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000000, ";")) !== FALSE) {
			if($i==1){
				$arData["period"] = $data;
			}elseif($i==2){
				$arData["params"] = $data;
			} else {

				//формирование данных по компании
				$parentPeriod = "";
				$arLoad = array();
				foreach($arData["period"] as $n=>$item){
					if($item){
						if($item!=$parentPeriod){
							$parentPeriod = $item;
						}
						if($data[$n]){
							$arLoad[$parentPeriod][$arData["params"][$n]] = str_replace(",", ".", str_replace(" ", "", trim($data[$n])));
						}
					}
				}
				
				//обработка
				$company = getCompany($data[0]);
				if($company){
					setItemMoneyValueAndType($data[0], $data[1], $data[2]);
				}
				
				if(count($arLoad)<2){
					continue;
				}
				
				
				foreach($arLoad as $date => $vals){
					$date = explode("кв", $date);
					$date[0] = trim($date[0]);
					$date[1] = trim($date[1]);
					if(!$date[0] || !$date[1]){
						continue;
					}

					if($company){
						setItem($date, $vals, $company["ID"]);
						$arMes["SUCCESS"]++;
					} else {
						$arMes["ERR"][] = $data[0];
					}
				}
				
				if($company["ACTIONS"]){
					exit();
				}
			}
			
			$i++;
		}

		CModule::IncludeModule("highloadblock");
		$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();

		$secColumn = 1;
		$monthlyColumn = 41;

		$arKvartalToMonth = array(
			1 => "03-31",
			2 => "06-30",
			3 => "09-30",
			4 => "12-31",
		);

		$boardIDs = array("TQBR", "TQDE");

		foreach($boardIDs as $board){
			//получаем акции
			$arEmitents = array();
			$arEmitentsDolls = array();

			$arFilter = Array("IBLOCK_ID"=>32, "!PROPERTY_EMITENT_ID"=>false, "!PROEPTY_SECID"=>false,"PROPERTY_BOARDID"=>$board);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "PROPERTY_SECID", "PROPERTY_EMITENT_ID"));
			while($v = $res->GetNext())
			{
				$arEmitents[$v["PROPERTY_SECID_VALUE"]] = $v["PROPERTY_EMITENT_ID_VALUE"];

				//получаем компанию
				$res2 = CIBlockElement::GetByID($v["PROPERTY_EMITENT_ID_VALUE"]);
				if($ar_res = $res2->GetNext()){
					$arFilter3 = Array("IBLOCK_ID"=>29, "NAME"=>$ar_res["NAME"]);
					$res3 = CIBlockElement::GetList(Array(), $arFilter3, false, false, array("PROPERTY_VAL_ED_FIRST"));
					if($ob = $res3->GetNext())
					{
						if(strpos($ob["PROPERTY_VAL_ED_FIRST_VALUE"], "долл.")!==false){
							$arEmitentsDolls[$v["PROPERTY_EMITENT_ID_VALUE"]] = true;
						}
					}
				}
				unset($res2, $res3, $arFilter3);
			}

			if(!isset($_REQUEST['operate_year']) || empty($_REQUEST['operate_year']) || intval($_REQUEST['operate_year'])<=0){
			 $year = date("Y");
			} else {
			 $year = intval($_REQUEST['operate_year']);
			}

			if(!isset($_REQUEST['operate_kv']) || empty($_REQUEST['operate_kv']) || intval($_REQUEST['operate_kv'])<=0){
			 $op_kv = 1;
			} else {
			 $op_kv = intval($_REQUEST['operate_kv']);
			}

            for($kv=$op_kv;$kv<=4;$kv++){
				  //	 CLogger::loadCompanyMMK("kv=".$kv);
                $res_cnt = $entityClass::getList(array(
                    "filter" => array(
                        "UF_KVARTAL" => $kv,
                        "UF_YEAR" => $year,
                    ),
                    "select" => array(
                        "ID"
                    ),
                ));
                if(!$res_cnt->getSelectedRowsCount()){
                    continue;
                }

					 unset($res_cnt);

                //$date = setNextMonday($year."-".$arKvartalToMonth[$kv]);
                $date = setPrevWorkday($year."-".$arKvartalToMonth[$kv]); //ищем предыдущий рабочий день если текущий выходной (Александра подтвердила о поиске в обратном направлении)

					// CLogger::loadCompanySeverstal("date=".$date);
				  	// CLogger::loadCompanySeverstal("board=".$board);

                $url = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/'.$board.'/securities.json?date='.$date.'&iss.meta=off';
                $data = ConnectMoex($url);

/*                echo "<pre>";
                print_r(array("kv"=>$kv, "year"=>$year, "date"=>$date, "data"=>$data));
                echo "</pre>";*/

                if(!$data["history"]["data"]){
                    continue;
                }

                $countPage = 1;
                if($data["history.cursor"]["data"][0][1]>$data["history.cursor"]["data"][0][2]){
                    $countPage = ceil($data["history.cursor"]["data"][0][1]/$data["history.cursor"]["data"][0][2]);
                }
                for($i=1;$i<=$countPage;$i++){
                    if($i>1){
                        //$date = setNextMonday($year."-".$arKvartalToMonth[$kv]);
                        $url = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/'.$board.'/securities.json?date='.$date.'&iss.meta=off&start='.(100*($i-1));
                        $data = ConnectMoex($url);
                        if(!$data["history"]["data"]){
                            continue;
                        }
                    }
                    foreach($data["history"]["data"] as $item){
                        if(!$item[$secColumn] || !$item[$monthlyColumn]){
                            continue;
                        }

                        $company = $arEmitents[$item[$secColumn]];



                        if(!$company){
                            continue;
                        }

								$arFilter4 = array(
                                "UF_KVARTAL" => $kv,
                                "UF_YEAR" => $year,
                                "UF_COMPANY" => $company
                            );
                        $res4 = $entityClass::getList(array(
                            "filter" => $arFilter4,
                            "select" => array(
                                "*"
                            ),
                        ));
                        	if($company==62691){  //MMK
                       // 	CLogger::loadCompanyMMK('filter4 '.print_r($arFilter4,true));
									}
                        if($data4 = $res4->fetch()){



                            if(empty($data4["UF_DATA"])){
                                continue;
                            }
                            $capital = [];
                            if(!empty($data4["UF_CAPITAL"])){
                                $capital = json_decode($data4["UF_CAPITAL"], true);
                            }

                            $val = $item[$monthlyColumn];
                            if($arEmitentsDolls[$company]) {
                                //$dt = new DateTime($date);
										  $cbr = new CCurrency((new DateTime($date))->format("d.m.Y"));
                                //$val = $item[$monthlyColumn]/getCBPrice("USD", $dt->format("d/m/Y"));
                                $val = $item[$monthlyColumn]/$cbr->getRate("USD", false);
										  unset($cbr);
                            }
                            $capital[$item[1]] = $val;

                            $entityClass::update($data4["ID"], array(
                                "UF_CAPITAL" => json_encode($capital)
                            ));
                            //}
                        }
								unset($res4, $data4);
                    }
                }
            }
		}

		$res5 = $entityClass::getList(array(
			"filter" => array(
				"!UF_CAPITAL" => false,
				"!UF_DATA" => false
			),
			"select" => array(
				"*"
			),
		));
		while($data5 = $res5->fetch()){
			$vals = json_decode($data5["UF_DATA"], true);
			$capital = json_decode($data5["UF_CAPITAL"], true);

			$capitalVal = 0;
			foreach ($capital as $n=>$v){
				$capitalVal+=$v;
			}
			$vals["Прошлая капитализация"] = round(($capitalVal/1000000), 2);


			$x = $entityClass::update($data5["ID"], array(
				"UF_DATA" => json_encode($vals)
			));
		}
		unset($res5, $data5);
		//Выборка со страниц компаний
		$fptTools = new fptTools();
		$arCompanyPages  = $fptTools->getCompanyList();
		$arCalculatedCompanyCap = array();
		foreach($arCompanyPages as $cname=>$arCompany){
			$fptTools->saveOneCompanyHistory(intval($arCompany["COMPANY_ID"]));
			$arCalculatedCompanyCap[$cname] = $arCompany;
		}

		
		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cache->clean("obligations_data");
		?>

		<p><b>Обновлено/добавлено (кол-во компаний):</b> <?=$arMes["SUCCESS"]?></p>
		<?if(count($arCalculatedCompanyCap)>0):?>
		 <p><b>Обновлена капитализация (кол-во компаний):</b> <?=count($arCalculatedCompanyCap)?></p>
		  <p>
		  	<? foreach($arCalculatedCompanyCap as $cname=>$arCompanyId): ?>
			 <?= $cname ?> [<?= $arCompanyId ?>]<br>
		  <?endforeach;?>
		  </p>
		<?endif;?>
		<p><b>Не найдено компаний:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<?exit();
	}
}

	//Вот тут переписываются данные
function setItem($date, $data, $company){
	$arItem = array();
	CModule::IncludeModule("highloadblock");

	$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();
	
	$arItem = array(
		"UF_KVARTAL" => $date[0],
		"UF_YEAR" => $date[1],
		"UF_COMPANY" => $company,
		"UF_DATA" => json_encode($data),
	);
	
	$res = $entityClass::getList(array(
		"filter" => array(
			"UF_KVARTAL" => $date[0],
			"UF_YEAR" => $date[1],
			"UF_COMPANY" => $company,
		),
		"select" => array(
			"ID"
		),
	));
	if($item = $res->fetch()){
		$entityClass::update($item["ID"], $arItem);
	} else {
		$entityClass::add($arItem);
	}
	unset($res, $item);
}

function getCompany($name){
	CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>26, "NAME"=>$name);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME"));
	if($item = $res->GetNext())
	{
		unset($res);
		return $item;
	}
	unset($res);
	return false;
}
function setItemMoneyValueAndType($company, $vid, $value){
	if(!$vid ||!$value){
		return false;
	}
	
	CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$company);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	if($item = $res->GetNext())
	{
		CIBlockElement::SetPropertyValues($item["ID"], 29, $value, "VAL_ED_FIRST");
		
		$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>29, "CODE"=>"TYPE_REPORT_FIRST", "VALUE"=>$vid));
		if($enum_fields = $property_enums->GetNext())
		{
			CIBlockElement::SetPropertyValues($item["ID"], 29, $enum_fields["ID"], "TYPE_REPORT_FIRST");
		} else {
			CIBlockElement::SetPropertyValues($item["ID"], 29, "", "TYPE_REPORT_FIRST");
		}
	}
	unset($res, $property_enums, $enum_fields, $item);
}
?>
<h1>Парсер компаний РФ</h1>
<div><i>Если загружается отчетность за 4 квартал предыдущего года в текущем, то следует установить год загружаемой отчетности. Пример: грузим 4кв.2019 года в январе 2020г. -Значит нужно выбрать 2019 год в списке. Для всех остальных случаев оставляем текущий год.</i></div>

<form enctype="multipart/form-data" method="post">
  <div class="form-group">
  	 <label for="operate_year">Обрабатываемый год</label>

<!--	 <select name="operate_year">
	 	<?$y=date('Y');
		while($y>date('Y')-10):?>
	 	<option value="<?= $y ?>"><?= $y ?></option>
		<?$y--;?>
		<?endwhile;?>
	 </select>-->

  	 <input name="operate_year" value="<?= date('Y')?>"><br>
	 <label for="operate_year">Обрабатываемый квартал (указать, если нужно обработать конкретный квартал)</label>
	 <input name="operate_kv" value=""><br>

    <label for="exampleInputFile">Отчет по компаниям</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>