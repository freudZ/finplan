<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//Загрузчик данных по ETF
CModule::IncludeModule("iblock");

$arNumberProps = array("ETF_COMISSION", "ETF_AVG_ICNREASE", "PROP_PROSAD");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ERR" => array(),
		"EMPTY_ISIN" => array()
	);
	
	$inputFileName = $_FILES["file"]["tmp_name"];

	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		
		$arProps = array();

		$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>32));
		while ($prop_fields = $properties->GetNext())
		{
			$arProps[$prop_fields["CODE"]] = $prop_fields["NAME"];
		}
		
		$arFieldsNum = array();
		$preview_text_colnum = 0; //Колонка с Preview_text
		while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;

			if($i==1){
				foreach($data as $n=>$item){
					if($item=="PREVIEW_TEXT"){
					 $preview_text_colnum = $n;
					}
					if(array_key_exists(trim($item),$arProps)){
						$arFieldsNum[$n] = $item;
					}
				}
			}

			if($i<=1){
				continue;
			}
			
			if(!$data[0]){
			   $arMes["EMPTY_SECID"][] = $data[0]." - ".$data[2];
				continue;
			}

			//акция
			$arFilter = Array("IBLOCK_ID"=>32, "PROPERTY_SECID"=>$data[0]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "NAME", "PROPERTY_EMITENT_ID"));
			if($item2 = $res->GetNext())
			{
				if(!$item2["PROPERTY_EMITENT_ID_VALUE"]){
		 		  	$item2["PROPERTY_EMITENT_ID_VALUE"] = setCompany($data, $item2);
				}

				if($preview_text_colnum>0 && !empty($data[$preview_text_colnum])){  //Если есть описание ETF - сохраним его в preview_picture
					$el = new CIBlockElement;
					$arLoadProductArray = Array(
					  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
					  "PREVIEW_TEXT"   => htmlspecialchars($data[$preview_text_colnum]),
					  );
					$resPreviewText = $el->Update($item2["ID"], $arLoadProductArray);
				}


				foreach($arFieldsNum as $n=>$propCode){
					if($data[$n]){
							if(in_array($propCode,$arNumberProps)!==false){
							 $data[$n] = str_replace(",", ".", $data[$n]);
							}
			  		 	CIBlockElement::SetPropertyValues($item2["ID"], $item2["IBLOCK_ID"], $data[$n], $propCode);
					}
				}

			  	  CIBlockElement::SetPropertyValues($item2["ID"], $item2["IBLOCK_ID"], 109, "IS_ETF_ACTIVE");



				if($arFieldsNum){
					$arMes["SUCCESS"]++;
				} else {
					$arMes["ERR"][] = $data[6];
				}
			} else {
				$arMes["ERR"][] = $data[6];
			}


		}

		fclose($handle);



		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cache->clean("actions_data");
		$cache->clean("etf_data");
		$cache->clean("etf_filters_data");
		?>
		<p><b>Обновлено/добавлено (кол-во ETF):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Не найдено ETF:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<p><b>Не загружены (нет значения "код бумаги"):</b> <?=implode(", ", $arMes["EMPTY_SECID"])?></p>
		<?exit();
	}
}

//проверка и создание компании
function setCompany($data, $item){
	$return = "";
	$emitent = $data[2]; //Счет от нуля
	$arFilter = Array("IBLOCK_ID"=>26, "NAME"=>$emitent);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME", "PROPERTY_EMITENT_ID"));
	if($item2 = $res->GetNext())
	{
		if(!$item2["PROPERTY_EMITENT_ID_VALUE"]){
			CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $item2["ID"], "EMITENT_ID");
		}
		
		$return = $item2["ID"];
	} else {
		$el = new CIBlockElement;

		$arLoadProductArray = Array(
		  "IBLOCK_ID"      => 26,
		  "NAME"           => $emitent,
		  "ACTIVE"         => "Y",
			//"CODE" => Cutil::translit($data[15],"ru"),
		);

		$newCompany = $el->Add($arLoadProductArray);
		if(!$newCompany){
			?><b>Есть ошибка!</b><br><?
			?><pre><?print_r($el->LAST_ERROR)?></pre><?
			?><pre><?print_r($arLoadProductArray)?></pre><?
			?><pre><?print_r($data)?></pre><?
			exit();
		}
		CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $newCompany, "EMITENT_ID");
		
		$return = $newCompany;
	}
	
	
	//создаем страницу компании
	$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$emitent);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME", "PROPERTY_EMITENT_ID"));
	if(!$res->SelectedRowsCount())
	{
		$char = ord(substr($emitent, 0 ,1));
		if($char<=122){
			$sort = 20;
		} else {
			$sort = 10;
		}
		
		$el = new CIBlockElement;
		$arParams = array("replace_space"=>"-","replace_other"=>"-");
		$arLoadProductArray = Array(
		  "IBLOCK_ID"      => 29,
		  "NAME"           => $emitent,
		  "ACTIVE"         => "Y",
		  "CODE" => Cutil::translit($emitent,"ru",$arParams),
		  "SORT" => $sort
		);

		$newCompany = $el->Add($arLoadProductArray);
		if(!$newCompany){
			?><b>Есть ошибка!</b><br><?
			?><pre><?print_r($el->LAST_ERROR)?></pre><?
			?><pre><?print_r($arLoadProductArray)?></pre><?
			?><pre><?print_r($data)?></pre><?
			exit();
		}
	}
	
	return $return;
}
?>
<h1>Загрузчик данных по ETF из csv файла</h1>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Данные по ETF</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>