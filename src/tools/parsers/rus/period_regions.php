<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
@set_time_limit(0);

use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
    exit();
}
function setNextMonday($date){
    $dt = new DateTime($date);

    if($dt->format("N")==6){
        $dt->modify("+2 days");
    } elseif($dt->format("N")==7){
        $dt->modify("+1 days");
    }

    if(isWeekEndDay($dt->format("d.m.Y"))){
        $finded = false;
        while(!$finded){
            $dt->modify("+1 days");
            if(!isWeekEndDay($dt->format("d.m.Y"))){
                $finded = true;
            }
        }
    }

    return $dt->format("Y-m-d");
}
CModule::IncludeModule("iblock");


if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){

    $arMes = array(
        "SUCCESS" => 0,
        "ERR" => array()
    );
    $inputFileName = $_FILES["file"]["tmp_name"];
    $i = 1;

    if (($handle = fopen($inputFileName, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000000, ";")) !== FALSE) {
            if($i==1){
                $arData["period"] = $data;
            }elseif($i==2){
                $arData["params"] = $data;
            } else {
                //формирование данных по компании
                $parentPeriod = "";
                $arLoad = array();
                foreach($arData["period"] as $n=>$item){
                    if($item && $n > 1){
                        if($item!=$parentPeriod){
                            $parentPeriod = $item;
                        }
                        if($data[$n]){
                            $arLoad[$parentPeriod][$arData["params"][$n]] = str_replace(",", ".", str_replace(" ", "", trim($data[$n])));
                        }
                    }
                }


                //обработка
                $company = getCompany($data[0]);


                if($company){
                    setItemMoneyValueAndType($data[0], $data[1]);
                }

                if(count($arLoad)<2){
                    continue;
                }


                foreach($arLoad as $date => $vals){
                    $date = str_replace(' год', '', $date);
                    if(!$date){
                        continue;
                    }

                    if($company){
                        setItem($date, $vals, $company["ID"]);
                        $arMes["SUCCESS"]++;
                    } else {
                        $arMes["ERR"][] = $data[0];
                    }
                }

                if($company["ACTIONS"]){
                    exit();
                }
            }

            $i++;
        }

        /*CModule::IncludeModule("highloadblock");
        $hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
        $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
        $entityClass = $entity->getDataClass();

        $secColumn = 1;
        $monthlyColumn = 41;

        $arKvartalToMonth = array(
            1 => "03-31",
            2 => "06-30",
            3 => "09-30",
            4 => "12-31",
        );

        $boardIDs = array("TQBR", "TQDE");
        foreach($boardIDs as $board){
            //получаем акции
            $arEmitents = array();
            $arEmitentsDolls = array();

            $arFilter = Array("IBLOCK_ID"=>32, "!PROPERTY_EMITENT_ID"=>false, "!PROEPTY_SECID"=>false,"PROPERTY_BOARDID"=>$board);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "PROPERTY_SECID", "PROPERTY_EMITENT_ID"));
            while($v = $res->GetNext())
            {
                $arEmitents[$v["PROPERTY_SECID_VALUE"]] = $v["PROPERTY_EMITENT_ID_VALUE"];

                //получаем компанию
                $res2 = CIBlockElement::GetByID($v["PROPERTY_EMITENT_ID_VALUE"]);
                if($ar_res = $res2->GetNext()){
                    $arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$ar_res["NAME"]);
                    $res3 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("PROPERTY_VAL_ED_FIRST"));
                    if($ob = $res3->GetNext())
                    {
                        if(strpos($ob["PROPERTY_VAL_ED_FIRST_VALUE"], "долл.")!==false){
                            $arEmitentsDolls[$v["PROPERTY_EMITENT_ID_VALUE"]] = true;
                        }
                    }
                }
            }

            for($year=2011;$year<=date("Y");$year++){
                for($kv=1;$kv<=4;$kv++){
                    if($kv==1 && $year==2011){
                        continue;
                    }

                    $res = $entityClass::getList(array(
                        "filter" => array(
                            "UF_KVARTAL" => $kv,
                            "UF_YEAR" => $year,
                        ),
                        "select" => array(
                            "ID"
                        ),
                    ));
                    if(!$res->getSelectedRowsCount()){
                        continue;
                    }

                    $date = setNextMonday($year."-".$arKvartalToMonth[$kv]);
                    $url = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/'.$board.'/securities.json?date='.$date.'&iss.meta=off';
                    $data = ConnectMoex($url);

                    if(!$data["history"]["data"]){
                        continue;
                    }

                    $countPage = 1;
                    if($data["history.cursor"]["data"][0][1]>$data["history.cursor"]["data"][0][2]){
                        $countPage = ceil($data["history.cursor"]["data"][0][1]/$data["history.cursor"]["data"][0][2]);
                    }

                    for($i=1;$i<=$countPage;$i++){
                        if($i>1){
                            $date = setNextMonday($year."-".$arKvartalToMonth[$kv]);
                            $url = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/'.$board.'/securities.json?date='.$date.'&iss.meta=off&start='.(100*($i-1));
                            $data = ConnectMoex($url);

                            if(!$data["history"]["data"]){
                                continue;
                            }
                        }
                        foreach($data["history"]["data"] as $item){
                            if(!$item[$secColumn] || !$item[$monthlyColumn]){
                                continue;
                            }
                            $company = $arEmitents[$item[$secColumn]];
                            if(!$company){
                                continue;
                            }


                            $res = $entityClass::getList(array(
                                "filter" => array(
                                    "UF_KVARTAL" => $kv,
                                    "UF_YEAR" => $year,
                                    "UF_COMPANY" => $company
                                ),
                                "select" => array(
                                    "*"
                                ),
                            ));
                            if($data = $res->fetch()){
                                if(!$data["UF_DATA"]){
                                    continue;
                                }
                                $capital = [];
                                if($data["UF_CAPITAL"]){
                                    $capital = json_decode($data["UF_CAPITAL"], true);
                                }

                                $val = $item[$monthlyColumn];
                                if($arEmitentsDolls[$company]) {
                                    $dt = new DateTime($date);
                                    $val = $item[$monthlyColumn]/getCBPrice("USD", $dt->format("d/m/Y"));
                                }
                                $capital[$item[1]] = $val;

                                //$vals = json_decode($data["UF_DATA"], true);
                                //if(!$vals["Прошлая капитализация"]){
                                //	$vals["Прошлая капитализация"] = round(($item[$monthlyColumn]/1000000), 2);

                                $entityClass::update($data["ID"], array(
                                    "UF_CAPITAL" => json_encode($capital)
                                ));
                                //}
                            }
                        }
                    }
                }
            }
        }
        $res = $entityClass::getList(array(
            "filter" => array(
                "!UF_CAPITAL" => false,
                "!UF_DATA" => false
            ),
            "select" => array(
                "*"
            ),
        ));
        while($data = $res->fetch()){
            $vals = json_decode($data["UF_DATA"], true);
            $capital = json_decode($data["UF_CAPITAL"], true);

            $capitalVal = 0;
            foreach ($capital as $n=>$v){
                $capitalVal+=$v;
            }
            $vals["Прошлая капитализация"] = round(($capitalVal/1000000), 2);

            $entityClass::update($data["ID"], array(
                "UF_DATA" => json_encode($vals)
            ));
        }


        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cache->clean("obligations_data");*/
        ?>
        <p><b>Обновлено/добавлено (кол-во компаний):</b> <?=$arMes["SUCCESS"]?></p>
        <p><b>Не найдено компаний:</b> <?=implode(", ", $arMes["ERR"])?></p>
        <?exit();
    }
}

function setItem($date, $data, $company){
    $arItem = array();
    CModule::IncludeModule("highloadblock");

    $hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
    $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
    $entityClass = $entity->getDataClass();

    $arItem = array(
        "UF_YEAR" => $date,
        "UF_COMPANY" => $company,
        "UF_DATA" => json_encode($data),
    );

    $res = $entityClass::getList(array(
        "filter" => array(
            "UF_YEAR" => $date,
            "UF_COMPANY" => $company,
        ),
        "select" => array(
            "ID"
        ),
    ));
    if($item = $res->fetch()){
        $entityClass::update($item["ID"], $arItem);
    } else {
        $entityClass::add($arItem);
    }
}

function getCompany($name){
    CModule::IncludeModule("iblock");
    $arFilter = Array("IBLOCK_ID"=>26, "NAME"=>$name);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME"));
    if($item = $res->GetNext())
    {
        return $item;
    }
    return false;
}
function setItemMoneyValueAndType($company, $value){
    if(!$value){
        return false;
    }

    CModule::IncludeModule("iblock");
    $arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$company);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
    if($item = $res->GetNext())
    {
        CIBlockElement::SetPropertyValues($item["ID"], 29, $value, "VAL_ED_FIRST");
        CIBlockElement::SetPropertyValues($item["ID"], 29, 1, "IS_REGION");
    }
}
?>
<h1>Загрузка регионов</h1>
<form enctype="multipart/form-data" method="post">
    <div class="form-group">
        <label for="exampleInputFile">Отчет по компаниям</label>
        <input type="file" name="file" id="exampleInputFile">
    </div>
    <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>