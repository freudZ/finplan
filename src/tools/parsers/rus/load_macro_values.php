<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//Загрузка макро-показателей в HL 39
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ADDED" =>array(),
		"UPDATED" =>array(),
		"ERR" => array(),
		"EMPTY_ISIN" => array()
	);

	$inputFileName = $_FILES["file"]["tmp_name"];

	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {


	//Получим массив созданных элементов инфоблока 69 "Макропоказатели"

	$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "PROPERTY_VAL_ED_FIRST");
	$arFilter = Array("IBLOCK_ID"=>69, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->fetch()){
	 $arMacroIbElement[$ob["XML_ID"]] = array("NAME"=>$ob["NAME"], "CODE"=>$ob["CODE"], "VAL_ED_FIRST"=>$ob["PROPERTY_VAL_ED_FIRST_VALUE"], "ID"=>$ob["ID"]);
	}

		//Получаем ID существуюего/созданного элемента инфоблока 69 "Макропоказатели"
		function checkMacroIbElement($name, $xmlId, $valEd, &$arMacroIbElement){
		 $elementId = 0;
		 $arParams = array("replace_space"=>"-","replace_other"=>"-");
       $trans = Cutil::translit($name,"ru",$arParams);

		 if(array_key_exists($xmlId, $arMacroIbElement)){
			  $elementId = $arMacroIbElement[$xmlId]["ID"];
			  if($arMacroIbElement[$xmlId]["VAL_ED_FIRST"]!=$valEd){ //Если ед.изм отличается - обновим ее
				 CIBlockElement::SetPropertyValues($arMacroIbElement[$xmlId]["ID"], 69, $valEd, "VAL_ED_FIRST");
			  }
		 } else {
			$el = new CIBlockElement;
			$PROP["VAL_ED_FIRST"] = $valEd;
			$arLoadProductArray = Array(
			  "IBLOCK_ID"      => 69,
			  "NAME"           => $name,
			  "CODE"           => $trans,
			  "PROPERTY"       => $PROP,
			  "XML_ID"         => $xmlId,
			  "ACTIVE"         => "Y",
			);

			if($newMacroElement = $el->Add($arLoadProductArray)){
			  $arMacroIbElement[$xmlId] = array("NAME"=>$name, "CODE"=>$trans, "VAL_ED_FIRST"=>$valEd, "ID"=>$newMacroElement);
			  $elementId = $newMacroElement;
			}
		 }
		 return $elementId;
		}

	//Получим из HL 39 все записи показателей
	$arHLMacroValues = array();

	$hlblock   = HL\HighloadBlockTable::getById(39)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	$resHL = $entityClass::getList(array(
		"select" => array(
			"ID",
			"UF_CODE",
			"UF_DATE",
			"UF_VALUE",
		),
		"order" => array("UF_DATE"=>"ASC", "UF_CODE"=>"ASC"),
	));
	while($item = $resHL->fetch()){
	 $date = (new DateTime($item["UF_DATE"]))->format("d.m.Y");
	 $arHLMacroValues[$item["UF_CODE"]][$date] = array("ID"=>$item["ID"], "VALUE"=>$item["UF_VALUE"]);
	 unset($date);
	}

		$arFieldsNum = array();

		while (($item = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i==1){ //Собираем массив дат с ключами по номерам колонок
			  foreach($item as $n=>$data){
			  	 if($n>3)
				   $arDatesColumns[$n] = (new DateTime($data))->format("d.m.Y");
			  }

			}

			if($i>1){
				if($item[0]=='header'){
					continue;
				}
				foreach($item as $n=>$data){
					if($n<4) continue;
				  if($n>3){  //Определяем режим добавления/обновления
				    $mode = 'add';
					  if(array_key_exists($item[1], $arHLMacroValues)){ //Если такой код показателя есть в БД - проверяем дальше
						  if(array_key_exists($arDatesColumns[$n], $arHLMacroValues[$item[1]])){//Если дата для этого показателя есть в БД - обновление иначе добавление
							 $mode = 'update';
						  }else{
							 $mode = 'add';
						  }
					  } else { //Если такого кода показателя нет в БД - добавление
					  		$mode = 'add';
					  }
				  }

				  if($n>3){
					 $value = floatval(str_replace(",",".",trim($data)));
					 $date = $arDatesColumns[$n];
					 $arItem = array("UF_TYPE"=>trim($item[0]),"UF_CODE"=>trim($item[1]),"UF_ITEM"=>checkMacroIbElement(trim($item[2]), trim($item[1]), trim($item[3]), $arMacroIbElement), "UF_DATE"=>$date, "UF_VALUE"=>$value);

					  if($mode=='add'){
							$result = $entityClass::add($arItem);
								if ($result->isSuccess())
								{
								    $id = $result->getId();
									 $arMes["ADDED"][]=$id;
									 $arMes["SUCCESS"] = $arMes["SUCCESS"]+1;
 						    //добавляем в массив макро показателей новую запись
			             $arHLMacroValues[trim($item[1])][$date] = array("ID"=>$id, "VALUE"=>$value);

								}elseif(!$result->isSuccess()){
									 $arMes["ERR"][]=$result->getErrorMessages();
								}
						  }else{
							 $result = $entityClass::update($arHLMacroValues[$item[1]][$date]["ID"], $arItem);
							   if ($result->isSuccess())
								{
								    $id = $result->getId();
									 $arMes["UPDATED"][]=$id;
									 $arMes["SUCCESS"] = $arMes["SUCCESS"]+1;
								}elseif(!$result->isSuccess()){
									 $arMes["ERR"][]=$result->getErrorMessages();
								}
						  }


				  }
 				}
			}

/*			if($i>2){
				break;
			}*/

		}
		fclose($handle);

		unset($arActionsFreefloat);

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		//$cache->clean("actions_data");

		?>
		<p><b>Успешно обработано (кол-во значений):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Добавлено (кол-во значений):</b> <?=count($arMes["ADDED"])?></p>
		<p><b>Обновлено (кол-во значений):</b> <?=count($arMes["UPDATED"])?></p>
		<?if(count($arMes["ERR"])>0):?>
		<p><b>Ошибки:</b> <?print_r($arMes["ERR"])?></p>
		<?endif;?>
		<?exit();
	}
}
?>

<h1>Загрузка макро-показателей</h1>
<hr>
<?
/*
 $Act = new Actions();
	$arAct = $Act->GetItem('RU000A0JR4A1');
	 echo "<pre  style='color:black; font-size:11px;'>";
       print_r($arAct);
       echo "</pre>";*/
 ?>
<form enctype="multipart/form-data" method="post">
  <p><i>Даты в заголовке файла должны быть в формате дд.мм.гггг</i></p>
  <div class="form-group">
    <label for="exampleInputFile">Данные по показателям</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>