<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ADDED" =>array(),
		"UPDATED" =>array(),
		"ERR" => array(),
		"EMPTY_ISIN" => array()
	);
	
	$inputFileName = $_FILES["file"]["tmp_name"];
	
	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {

	//Получим из HL 34 все записи акций и сгруппируем по коду индекса
	$arItemsInIndex = array();

	//Формируем массивы индексов по каждой акции для котоой задано вхождение
	$arActionsInIndex = array();

	$hlblock   = HL\HighloadBlockTable::getById(34)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	$resHL = $entityClass::getList(array(
		"select" => array(
			"ID",
			"UF_INDEX",
			"UF_SHARE_CODE",
			"UF_SHARE_OF_INDEX",
			"UF_FREEFLOAT",
		),
		"order" => array("UF_INDEX"=>"ASC", "UF_SHARE_CODE"=>"ASC"),
	));
	while($item = $resHL->fetch()){
	 $arItemsInIndex[$item["UF_INDEX"]][$item["UF_SHARE_CODE"]] = $item;
	//собираем массивы индексов для акций
   if(!in_array($item["UF_INDEX"], $arActionsInIndex[$item["UF_SHARE_CODE"]]))
	  $arActionsInIndex[$item["UF_SHARE_CODE"]][] = $item["UF_INDEX"];

	}

		$arFieldsNum = array();

		while (($item = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i>1){
				//foreach($data as $n=>$item){
				  $mode = 'add';
				  //Определяем режим обработки данных
				  if(array_key_exists($item[0], $arItemsInIndex)){
					  if(array_key_exists($item[2], $arItemsInIndex[$item[0]])){
						 $mode = 'update';
					  }else{
						 $mode = 'add';
					  }
				  } else {
				  		$mode = 'add';
				  }

				  $shareInIndex = floatval(str_replace(",",".",trim($item[3])));
				  $freefloat = floatval(str_replace(",",".",trim($item[4])));

				  $arItem = array("UF_INDEX"=>trim($item[0]),"UF_SHARE_CODE"=>trim($item[2]),"UF_SHARE_OF_INDEX"=>$shareInIndex, "UF_FREEFLOAT"=>$freefloat);

				  if($mode=='add'){
				  //добавляем в массивы индексов для акций новый индекс
				  if(!in_array(trim($item[0]), $arActionsInIndex[trim($item[2])]))
	             $arActionsInIndex[trim($item[2])][] = trim($item[0]);

					$result = $entityClass::add($arItem);
						if ($result->isSuccess())
						{
						    $id = $result->getId();
							 $arMes["ADDED"][]=$id;
							 $arMes["SUCCESS"] = $arMes["SUCCESS"]+1;
						}elseif(!$result->isSuccess()){
							 $arMes["ERR"][]=$result->getErrorMessages();
						}
				  }else{
					 $result = $entityClass::update($arItemsInIndex[$item[0]][$item[2]]["ID"], $arItem);
					   if ($result->isSuccess())
						{
						    $id = $result->getId();
							 $arMes["UPDATED"][]=$id;
							 $arMes["SUCCESS"] = $arMes["SUCCESS"]+1;
						}elseif(!$result->isSuccess()){
							 $arMes["ERR"][]=$result->getErrorMessages();
						}
				  }


				//}
			}

			if($i<=2){
				continue;
			}

		}
		fclose($handle);

		//Перезапись собранных индексов для акций
		if(count($arActionsInIndex)>0){

		$arSelect = Array("ID", "CODE", "IBLOCK_ID");
		$arFilter = Array("IBLOCK_ID"=>32, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arActionsId = array();
		while($ob = $res->Fetch()){
		 $arActionsId[$ob["CODE"]] = $ob["ID"];
		}

		 foreach($arActionsInIndex as $action_code=>$arInIndexes){

		  	 CIBlockElement::SetPropertyValues($arActionsId[$action_code], 32, implode("/",$arInIndexes), "IN_INDEXES");


		 }


		}

		//Перерасчет капы фрифлоат для свойства акции
		//Посчитать показатель капа х фрифлоат / 100  (капа фрифлоата)
		//--------------------------------------------------
		//Получим список акций входящих в индексы, вместе с ними и их фрифлоат и рассчитаем капу фрифлоата
		$arActionsFreefloat = array();

		$hlblock   = HL\HighloadBlockTable::getById(34)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$moexIndexData = $entity->getDataClass();
		$raw = $moexIndexData::getList([
		            "select" => [
		                "UF_SHARE_CODE",
		                "UF_FREEFLOAT"
		            ],
		        ]);
				while($row = $raw->fetch()){
				     $arActionsFreefloat[$row["UF_SHARE_CODE"]] = $row["UF_FREEFLOAT"];
				}
		    $arFilter = Array("IBLOCK_ID" => 32, "CODE"=>array_keys($arActionsFreefloat));
		    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "CODE", "IBLOCK_ID", "PROPERTY_ISSUECAPITALIZATION", "PROPERTY_ISIN"));

		    while ($v = $res->Fetch()) {
					if(array_key_exists($v["CODE"],$arActionsFreefloat)==true){ //Если в массиве с фрифлоатом есть даная акция - считаем ей капу фрифлоата
						CIBlockElement::SetPropertyValues($v["ID"], 32, floatval(floatval($v["PROPERTY_ISSUECAPITALIZATION_VALUE"])*$arActionsFreefloat[$v["CODE"]]/100), "CAP_X_FREEFLOAT"); //Считаем и записываем капу фрифлоата
					}
		    }

		  unset($arActionsFreefloat);


		
		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cache->clean("actions_data");

		$res = new Actions();
		$res->getTable(1);
		?>
		<p><b>Успешно обработано (кол-во акций):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Добавлено (кол-во акций):</b> <?=count($arMes["ADDED"])?></p>
		<p><b>Обновлено (кол-во акций):</b> <?=count($arMes["UPDATED"])?></p>
		<?if(count($arMes["ERR"])>0):?>
		<p><b>Ошибки:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<?endif;?>
		<?exit();
	}
}
?>

<h1>Загрузка дополнительных данных для акций в составе индексов</h1>
<hr>
<?
/*
 $Act = new Actions();
	$arAct = $Act->GetItem('RU000A0JR4A1');
	 echo "<pre  style='color:black; font-size:11px;'>";
       print_r($arAct);
       echo "</pre>";*/
 ?>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Данные по акциям в индексах</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>