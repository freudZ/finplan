<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//Загрузчик данных по ETF
CModule::IncludeModule("iblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ERR" => array(),
		"EMPTY_ISIN" => array()
	);
	
	$inputFileName = $_FILES["file"]["tmp_name"];

	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		
/*		$arProps = array();

		$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>32));
		while ($prop_fields = $properties->GetNext())
		{
			$arProps[$prop_fields["CODE"]] = $prop_fields["NAME"];
		}*/
		//Получим массив ID индексов с ключами по тикерам
			$arFilter = Array("IBLOCK_ID"=>53, "PROPERTY_SECID"=>$data[0]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "CODE", "NAME"));
		   $arIndexesId = array();
		   while($row = $res->fetch()){
			  $arIndexesId[$row["CODE"]] = $row["ID"];
		   }

		$arFieldsNum = array();
		$preview_text_colnum = 0; //Колонка с Preview_text
		while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;

			if($i==1){
				foreach($data as $n=>$item){
					if($item=="PREVIEW_TEXT"){
					 $preview_text_colnum = $n;
					}
/*					if(array_key_exists(trim($item),$arProps)){
						$arFieldsNum[$n] = $item;
					}*/
				}
			}

			if($i<=1){
				continue;
			}

			if($i>1){
				if(!$data[0]){
				   $arMes["EMPTY_SECID"][] = $data[0];
					continue;
				}

				if(array_key_exists($data[0], $arIndexesId)){

				if($preview_text_colnum>0 && !empty($data[$preview_text_colnum])){  //Если есть описание индекса - сохраним его в preview_text
					$el = new CIBlockElement;
					$arLoadProductArray = Array(
					  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
					  "PREVIEW_TEXT"   => html_entity_decode($data[$preview_text_colnum]),
					  "PREVIEW_TEXT_TYPE"   => "html",
					  );
					if($el->Update($arIndexesId[$data[0]], $arLoadProductArray)){
					  $arMes["SUCCESS"]++;
					} else {
					  $arMes["ERR"][] = "Индекс ".$data[0]." не обновлен";
					}

				}

				} else {
					 $arMes["ERR"][] = "Не найден ".$data[0];
				}

			}

		}
		fclose($handle);
/*		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cache->clean("actions_data");
		$cache->clean("etf_data");
		$cache->clean("etf_filters_data");*/
		?>
		<p><b>Обновлено/добавлено (кол-во индексов):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Не найдено/не обновлено индексов:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<p><b>Не загружены (нет тикера в базе):</b> <?=implode(", ", $arMes["EMPTY_SECID"])?></p>
		<?exit();
	}
}

?>

<h1>Загрузка описаний индексов из csv файла</h1>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Данные по индексам</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>