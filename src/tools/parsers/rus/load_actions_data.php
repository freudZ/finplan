<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

CModule::IncludeModule("iblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ERR" => array(),
		"EMPTY_ISIN" => array()
	);
	
	$inputFileName = $_FILES["file"]["tmp_name"];
	
	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		
		$arProps = array();
		$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>32,"CODE"=>"PROP_%"));
		while ($prop_fields = $properties->GetNext())
		{
			$arProps[$prop_fields["NAME"]] = $prop_fields["CODE"];
		}
		
		$arFieldsNum = array();
		while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;

			if($i==2){
				foreach($data as $n=>$item){
					if($arProps[$item]){
						$arFieldsNum[$n] = $arProps[$item];
					}
				}
			}

			if($i<=2){
				continue;
			}
			
			if(!$data[6]){
			   $arMes["EMPTY_ISIN"][] = $data[0]." - ".$data[5];
				continue;
			}


			//акция
			$arFilter = Array("IBLOCK_ID"=>32, "CODE"=>trim($data[6]));
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "NAME", "PROPERTY_EMITENT_ID", "PROPERTY_SECID"));
			if($item2 = $res->GetNext())
			{

				if(!$item2["PROPERTY_EMITENT_ID_VALUE"]){
					$item2["PROPERTY_EMITENT_ID_VALUE"] = setCompany($data, $item2);
				}

				foreach($arFieldsNum as $n=>$propCode){
					//if($data[$n]){
						CIBlockElement::SetPropertyValues($item2["ID"], $item2["IBLOCK_ID"], str_replace(",", ".", $data[$n]), $propCode);
					//}
				}
				if($arFieldsNum){
					$arMes["SUCCESS"]++;
				} else {
					$arMes["ERR"][] = $data[6];
				}
			} else {
				$arMes["ERR"][] = $data[6];
			}
		}
		fclose($handle);
	
		
		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cache->clean("actions_data");

		$res = new Actions();
		$res->getTable(1);
		?>
		<p><b>Обновлено/добавлено (кол-во акций):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Не найдено акций:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<p><b>Не загружены (нет значения "ISIN-код бумаги"):</b> <?=implode(", ", $arMes["EMPTY_ISIN"])?></p>
		<hr>
		<p>После загрузки данных запустите обновление SEO-данных для страниц акций по <a href="/tools/system/seo/seo.php?type=actionPages">ссылке</a></p>
		<?exit();
	}
}

//проверка и создание компании
function setCompany($data, $item){
	$return = "";
	
	$arFilter = Array("IBLOCK_ID"=>26, "NAME"=>$data[9]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME", "PROPERTY_EMITENT_ID"));
	if($item2 = $res->GetNext())
	{
		if(!$item2["PROPERTY_EMITENT_ID_VALUE"]){
			CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $item2["ID"], "EMITENT_ID");
		}
		
		$return = $item2["ID"];
	} else {
		$el = new CIBlockElement;

		$arLoadProductArray = Array(
		  "IBLOCK_ID"      => 26,
		  "NAME"           => $data[9],
		  "ACTIVE"         => "Y",
			//"CODE" => Cutil::translit($data[15],"ru"),
		);

		$newCompany = $el->Add($arLoadProductArray);
		if(!$newCompany){
			?><b>Есть ошибка!</b><br><?
			?><pre><?print_r($el->LAST_ERROR)?></pre><?
			?><pre><?print_r($arLoadProductArray)?></pre><?
			?><pre><?print_r($data)?></pre><?
			exit();
		}
		CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $newCompany, "EMITENT_ID");

		$return = $newCompany;
	}
	
	
	//создаем страницу компании
	$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$data[9]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME", "PROPERTY_EMITENT_ID"));
	if(!$res->SelectedRowsCount())
	{
		$char = ord(substr($data[9], 0 ,1));
		if($char<=122){
			$sort = 20;
		} else {
			$sort = 10;
		}
		
		$el = new CIBlockElement;

		$arProps["SEO_TITLE_FULL"] = str_replace(" ао", " обыкновенные", $data[9]);
		$arProps["SEO_TITLE_FULL"] = str_replace(" ап", " привилегированные", $data[9]);
		$arProps["SEO_TICKER"] = $item["PROPERTY_SECID_VALUE"];

		$arLoadProductArray = Array(
		  "IBLOCK_ID"      => 29,
		  "NAME"           => $data[9],
		  "ACTIVE"         => "Y",
		  "PROPERTY_VALUES" => $arProps,
		  "CODE" => Cutil::translit($data[9],"ru"),
		  "SORT" => $sort
		);

		$newCompany = $el->Add($arLoadProductArray);
		if(!$newCompany){
			?><b>Есть ошибка!</b><br><?
			?><pre><?print_r($el->LAST_ERROR)?></pre><?
			?><pre><?print_r($arLoadProductArray)?></pre><?
			?><pre><?print_r($data)?></pre><?
			exit();
		}
	}
	
	return $return;
}
?>
<h1>Парсер акций РФ</h1>
<div><i>если все ок - он пишет список ISIN кодов не найденных акций. Этих акции уже нет на бирже.</i></div>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Данные по акциям</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>