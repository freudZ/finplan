<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

$arMonthTable = array(
	"янв" => "01",
	"фев" => "02",
	"мар" => "03",
	"апр" => "04",
	"май" => "05",
	"июн" => "06",
	"июл" => "07",
	"авг" => "08",
	"сен" => "09",
	"окт" => "10",
	"ноя" => "11",
	"дек" => "12",
);

CModule::IncludeModule("iblock");
if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	
	$arMes = array(
		"SUCCESS" => 0,
		"ERR" => array()
	);
	$inputFileName = $_FILES["file"]["tmp_name"];
	$i = 1;
	
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000000, ";")) !== FALSE) {
			if($i==1){
				$arData["period"] = $data;
			}elseif($i==2){
				$arData["params"] = $data;
			} else {
				//формирование данных по компании
				$parentPeriod = "";
				$arLoad = array();
				foreach($arData["period"] as $n=>$item){
					if($item){
						if($item!=$parentPeriod){
							$parentPeriod = $item;
						}
						if($data[$n]){
							$arLoad[$parentPeriod][$arData["params"][$n]] = str_replace(",", ".", str_replace(" ", "", trim($data[$n])));
						}
					}
				}
				
				//обработка
				foreach($arLoad as $date => $vals){
					$date = explode(".", $date);
					
					//месяц с текста в номер
					$date[0] = $arMonthTable[$date[0]];

					if($company = getCompany($data[0])){
						setItem($date, $vals, $company);
						$arMes["SUCCESS"]++;
					} else {
						$arMes["ERR"][] = $data[0];
					}
				}
			}
			
			$i++;
		}
		
	 //	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	 //	$cache->clean("obligations_data");
		?>
		<p><b>Обновлено/добавлено (кол-во компаний):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Не найдено компаний:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<?exit();
	}
}

function setItem($date, $data, $company){
	$arItem = array();
	CModule::IncludeModule("highloadblock");

	$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();
	
	$arItem = array(
		"UF_MONTH" => $date[0],
		"UF_YEAR" => $date[1],
		"UF_COMPANY" => $company,
		"UF_DATA" => json_encode($data),
	);
	
	$res = $entityClass::getList(array(
		"filter" => array(
			"UF_MONTH" => $date[0],
			"UF_YEAR" => $date[1],
			"UF_COMPANY" => $company,
		),
		"select" => array(
			"ID"
		),
	));
	if($item = $res->fetch()){
		$entityClass::update($item["ID"], $arItem);
	} else {
		$entityClass::add($arItem);
	}
}

function getCompany($name){
	CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>26, "NAME"=>$name);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	if($item = $res->GetNext())
	{
		return $item["ID"];
	}
	return false;
}
?>
<h1>Парсер банков РФ</h1>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Отчет по банкам</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>