<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

CModule::IncludeModule("iblock");

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

/*
Array
(
    [0] => SHORTNAME
    [1] => MATDATE
    [2] => FACEVALUE
    [3] => CURRENCYID
    [4] => LISTLEVEL
    [5] => LAST
    [6] => VALUE
    [7] => ACCRUEDINT
    [8] => COUPONVALUE
    [9] => COUPONPERIOD
    [10] => NEXTCOUPON
    [11] => SECID
    [12] => OFFERDATE
    [13] => 
    [14] => 
    [15] => 
    [16] => 
    [17] => 
)
Array
(
    [0] => Бумага
    [1] => Погашение
    [2] => Номинал
    [3] => Валюта
    [4] => Листинг
    [5] => Цена послед.
    [6] => Оборот
    [7] => НКД
    [8] => Размер купона
    [9] => Длит. купона
    [10] => Дата выпл. куп.
    [11] => ISIN-код бумаги
    [12] => Дата оферты
    [13] => Номер лицензии
    [14] => Вид облигации
    [15] => Эмитент
    [16] => Голубые фишки
    [17] => Дефолты
)
*/

$arVid = array(
	"банковские" => 35,
	"корпоративные" => 36,
	"федеральные" => 37,
	"муниципальные" => 38,
);

$paymentOrderArr = array(
	"субординированная" => 65,
	"не субординированная" => 66,
);


if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$arMes = array(
		"SUCCESS" => 0,
		"ERR" => array()
	);
	
	$inputFileName = $_FILES["file"]["tmp_name"];
	
	$i=0;
	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
			$i++;
			if($i<=2){
				continue;
			}
			//облигация
			$arFilter = Array("IBLOCK_ID"=>27, "CODE"=>$data[11]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "NAME", "PROPERTY_EMITENT_ID"));
			if($item = $res->GetNext())
			{
				if(!$item["PROPERTY_EMITENT_ID_VALUE"]){
					$item["PROPERTY_EMITENT_ID_VALUE"] = setCompany($data, $item);
				}

				//вид облагации
				$arVidData = array();
				if($arVid[$data[14]]){
					$arVidData[] = $arVid[$data[14]];
				}
				
				//голубые фишки
				if($data[16]){
					$arVidData[] = 39;
				}

				//очередность выплаты
				$paymentOrder = $paymentOrderArr ? $paymentOrderArr[strtolower($data[18])] : '';
				
				//дефолтные
				if($data[17]){
					$arVidData[] = 40;
					CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $data[17], "CSV_VID_DEFOLT");
				} else {
					CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], "", "CSV_VID_DEFOLT");
				}
				CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $arVidData, "CSV_VID_OLB");
				CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $paymentOrder, "CSV_PAYMENT_ORDER");
				CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $data[19], "RATING_PROTECTION_TYPE");
				CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $data[20], "COUPON_INCOME");
				CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $data[21], "ADDITIONAL_INCOME");
				CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $data[22], "ADDITIONAL_INCOME_PERIOD");

				$arMes["SUCCESS"]++;
			} else {
				
				$arMes["ERR"][] = $data[11];
			}
		}
		fclose($handle);
		
	//	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	//	$cache->clean("obligations_data");
		?>
		<p><b>Обновлено/добавлено (кол-во облигаций):</b> <?=$arMes["SUCCESS"]?></p>
		<p><b>Не найдено облигаций:</b> <?=implode(", ", $arMes["ERR"])?></p>
		<?exit();
	}
}
//проверка и создание компании
function setCompany($data, $item){
	$return = "";
	
	$arFilter = Array("IBLOCK_ID"=>26, "NAME"=>$data[15]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME", "PROPERTY_EMITENT_ID"));
	if($item2 = $res->GetNext())
	{
		if(!$item2["PROPERTY_EMITENT_ID_VALUE"]){
			CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $item2["ID"], "EMITENT_ID");
		}
		
		$return = $item2["ID"];
	} else {
		$el = new CIBlockElement;

		$arLoadProductArray = Array(
		  "IBLOCK_ID"      => 26,
		  "NAME"           => $data[15],
		  "ACTIVE"         => "Y",
			//"CODE" => Cutil::translit($data[15],"ru"),
		);

		$newCompany = $el->Add($arLoadProductArray);
		if(!$newCompany){
			?><b>Есть ошибка! Компания уже существует.</b><br><?
			?><pre><?print_r($el->LAST_ERROR)?></pre><?
			?><pre><?print_r($arLoadProductArray)?></pre><?
			?><pre><?print_r($data)?></pre><?
			exit();
		}
		CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $newCompany, "EMITENT_ID");
		
		$return = $newCompany;
	}
	
	
	//создаем страницу компании
	$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$data[15]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME", "PROPERTY_EMITENT_ID"));
	if(!$res->SelectedRowsCount())
	{
		$char = ord(substr($data[15], 0 ,1));
		if($char<=122){
			$sort = 20;
		} else {
			$sort = 10;
		}
		
		$el = new CIBlockElement;

		$arLoadProductArray = Array(
		  "IBLOCK_ID"      => 29,
		  "NAME"           => $data[15],
		  "ACTIVE"         => "Y",
		  "CODE" => Cutil::translit($data[15],"ru"),
		  "SORT" => $sort
		);

		$newCompany = $el->Add($arLoadProductArray);
		if(!$newCompany){
			?><b>Есть ошибка!</b><br><?
			?><pre><?print_r($el->LAST_ERROR)?></pre><?
			?><pre><?print_r($arLoadProductArray)?></pre><?
			?><pre><?print_r($data)?></pre><?
			exit();
		}
	}
	
	return $return;
}
?>
<h1>Парсер облигаций</h1>
<div><i>(проверяем в "компаниях " и в "страницах компаний", если Ошибка - чаще всего запятые, ковычки и прочая ерунда.
То есть мы грузим страницу уже без запятой, например, а там она у нас записана с запятой). <br>Если все ок - он пишет список ISIN кодов не найденных облигаций. Эти облигации уже погашены и на бирже их уже нет.</i></div>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Данные по облигациям</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>