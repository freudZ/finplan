<?
$aMenuLinks = Array(
	Array(
		"Парсер акций РФ",
		"/tools/parsers/rus/load_actions_data.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Парсер компаний РФ",
		"/tools/parsers/rus/period_corp.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Парсер банков РФ",
		"/tools/parsers/rus/period_banks.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Загрузка данных по компании (расширенная)",
		"/tools/parsers/rus/expanded_company.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Загрузка дивидендов акций РФ",
		"/tools/parsers/rus/dividends.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Загрузка описаний индексов для акций РФ",
		"/tools/parsers/rus/load_indexes_description.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Загрузка долей в индексах для акций РФ",
		"/tools/parsers/rus/load_indexes_data.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Парсер ETF",
		"/tools/parsers/rus/load_etf_data.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Парсер облигаций",
		"/tools/parsers/rus/load_obligations_data.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Загрузка купонов по облигациям",
		"/tools/parsers/rus/coupons.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Загрузка регионов",
		"/tools/parsers/rus/period_regions.php",
		Array(),
		Array(),
		""
	),
	Array(
		"Загрузка макро-показателей",
		"/tools/parsers/rus/load_macro_values.php",
		Array(),
		Array(),
		""
	),
);

?>
