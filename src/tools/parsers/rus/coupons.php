<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	exit();
}

CModule::IncludeModule("iblock");
if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){
	$inputFileName = $_FILES["file"]["tmp_name"];
	$i = 1;
	
	CModule::IncludeModule("highloadblock");

	$hlblock   = HL\HighloadBlockTable::getById(15)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	if (($handle = fopen($inputFileName, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
			if($i==1){
				unset($data[0]);
				$arData["params"] = $data;
			} else {
				if($data[0]){
					$parentCode = $data[0];
				}
				
				$tmp = array();
				foreach($arData["params"] as $n=>$item){
					if(!$item){
						continue;
					}
					if(!$data[$n]){
						continue;
					}
					$tmp[$item] = str_replace(",", ".", $data[$n]);
				}
				
				$arResult[$parentCode][] = $tmp;
			}
			
			$i++;
		}
	}
	
	if($arResult){

		foreach($arResult as $code=>$data){
/*			if($code=='XS0114288789'){
			echo count($data)."<pre  style='color:black; font-size:11px;'>";
			   print_r($data);
			   echo "</pre>";
				}*/

			$arItem = array(
				"UF_ITEM" => $code,
				"UF_DATA" => json_encode($data),
			);
			
			$res = $entityClass::getList(array(
				"filter" => array(
					"UF_ITEM" => $code,
				),
				"select" => array(
					"ID"
				),
			));
			if($item = $res->fetch()){
				$entityClass::update($item["ID"], $arItem);
			} else {
				$entityClass::add($arItem);
			}
		}
		?>
		<p>Готово</p>
		<?
		exit();
	} else {
		?>
		<p>Нет данных для обновления</p>
		<?
		exit();
	}
}
?>
<h1>Загрузка купонов по облигациям</h1>
<form enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label for="exampleInputFile">Данные по купонам</label>
    <input type="file" name="file" id="exampleInputFile">
  </div>
  <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>