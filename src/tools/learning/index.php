<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle("Обучение"); ?>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Examples </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle"
                                                    data-toggle="dropdown" role="button" aria-haspopup="true"
                                                    aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">Добавить курс</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <!--                                <li class="remove">
                                                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                                            </li>-->
                        </ul>
                    </div>
                    <?$CLearning = new CLearning;
                        $arCourses = $CLearning->getResponse("https://fin-plan.org/api/learning/v1/getCoursesList/short/y/");
                    ?>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Активность</th>
                                    <th>Сложность</th>
                                    <th>Категория</th>
                                    <th>Уроки</th>
                                    <th>Тесты</th>
                                    <th>Стоимость</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Название</th>
                                    <th>Активность</th>
                                    <th>Сложность</th>
                                    <th>Категория</th>
                                    <th>Уроки</th>
                                    <th>Тесты</th>
                                    <th>Стоимость</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?foreach($arCourses as $cid=>$arCourse):?>
                                <tr>
                                    <td><a href="/tools/learning/edit_course?id=<?=$arCourse["ID"]?>"><?=$arCourse["UF_NAME"]?></a></td>
                                    <td><?=($arCourse["UF_ACTIVE"]?'Да':'Нет')?></td>
                                    <td><?=$arCourse["UF_DIFFICULTY"]["name"]?></td>
                                    <td><?=$arCourse["UF_CATEGORY"]["name"]?></td>
                                    <td><?=$arCourse["UF_PRICE"]?></td>
                                    <td><?=count($arCourse["UF_LESSONS"])?></td>
                                    <td><?=count($arCourse["UF_TESTS"])?></td>
                                </tr>
                                <?endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>