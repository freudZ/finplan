<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

$hlblock   = HL\HighloadBlockTable::getById(29)->fetch();
$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
$entityClass = $entity->getDataClass();

$arFilter = array("=UF_DATE"=>"13.05.2021");

$resHL = $entityClass::getList(array(
  "select" => array(
    "ID",
    "UF_ITEM"
  ),
  "filter" => $arFilter,
  "order" => array("UF_ITEM"=>"ASC", "ID"=>"ASC"),
));
$arPrices = array();

while($row = $resHL->fetch()){
        $arPrices[$row["UF_ITEM"]][]=  $row["ID"];
}

foreach($arPrices as $item=>$arId){
    if(count($arId)>1){
        $entityClass::delete($arId[1]);
    }
}

echo "<pre  style='color:black; font-size:11px;'>";
   print_r($arPrices);
echo "</pre>";
?>

