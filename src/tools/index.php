<?define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
 global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	LocalRedirect("/");
	exit();
}
?>
<?$APPLICATION->SetTitle("Инструменты администрирования");?>
 <div class="container">
 	<h1>Инструменты администрирования</h1>
 	<div class="row">
 		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
 		 <div class="bg-success center-block index_blocks">
		  <h2>Управление радаром</h2>
		  <p>В этом разделе можно оперативно отслеживать возможные проблемные места в наборах данных, так же достпны различные варианты очистки кэша</p>
		 </div>
 		</div>
 		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
 		 <div class="bg-success center-block index_blocks">
		  <h2>Парсеры</h2>
		  <p>Используются для автоматизации загрузки данных в формате csv в базу данных сайта, автоматического пересчета данных и ускорения прочих рутинных операций</p>
 		 </div>
 		</div>
 		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
 			<div class="bg-success center-block index_blocks">
			<h2>Прочее</h2>
			<p>В этом разделе будут появляться доплнительные инструменты по работе с данными</p>
 			</div>
 		</div>
 	</div>
 </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>