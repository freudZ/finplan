<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("highloadblock");
Global $APPLICATION;
?>
<?$APPLICATION->SetTitle("Тест расчета дивидендов по США");?>
<br><br><br><br><br><br><br><br><br>
<?

function updateYearDividends($ElementId, $IblockId, $code, $LastPrice=0){
    CModule::IncludeModule("iblock");
    $hlblock = HL\HighloadBlockTable::getById(32)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $dataClass= $entity->getDataClass();
    
    $res = $dataClass::getList(array(
        "filter" => array(
          "UF_ITEM" => $code,
        ),
        "limit" => 1,
        "select" => array("ID", "UF_ITEM", "UF_DATA"),
        //"cache" => array("ttl" => 3600)
      )
    );
    
    if ($hlItem = $res->fetch()) {
        
        
        if(!empty($hlItem["UF_DATA"])){
            $divData = json_decode($hlItem["UF_DATA"], true);
            if(!count($divData)) return;
            $endDate = '';  // Дата окончания годового периода
            $startDate = '';  // Дата начала годового периода
            $now = new DateTime(date());
            
            //Поиск окончания диапазона
            foreach($divData as $coupon){
                $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
                if($now<$closeDate){
                    $endDate = $closeDate;
                    echo 'end1='.$endDate->format('d.m.Y').PHP_EOL;
                } else if($now>=$closeDate && empty($endDate)){
                    $endDate = $closeDate;
                    echo 'end2='.$endDate->format('d.m.Y').PHP_EOL;
                    break;
                }
            }
            
            if(intval($endDate->format('Y'))<intval($now->format('Y'))){
                $endDate = $now;
            }
            
            //$startDate = (clone $endDate)->modify('-1 year');
            $startDate = (clone $endDate)->modify("-1 years")->modify("+1 month");
            echo "<pre  style='color:black; font-size:11px;'>";
               echo 'start='.$startDate->format('d.m.Y').PHP_EOL;
               echo 'end='.$endDate->format('d.m.Y').PHP_EOL;
            echo "</pre>";
            $YearSum = 0;
            $YearPrc = 0;
            $FutureYearSum = 0;
            $FutureYearPrc = 0;
            $Description = '';
            foreach($divData as $coupon){
                $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
                if($closeDate<=$endDate && $closeDate>=$startDate){
                    
                    $textDiv = explode(" (",$coupon["Дивиденды"]); //Выкидываем пересчитанную в рубли сумму в скобках
                    $YearSum += floatval($textDiv[0]);
                    //	 $Description .= (strlen($Description)>0?'; ':'').$coupon["Дивиденды"].' руб. ('.$closeDate->format('d.m.Y').' - '.($now<$closeDate?'рекомендовано':'выплачено').')';
                    $Description .= (strlen($Description)>0?'; ':'').$textDiv[0].' от '.$closeDate->format('d.m.Y').' - '.($now<$closeDate?'рекомендовано':'выплачено');
                }
                
            }
            //Считаем будущие дивиденды
            foreach($divData as $coupon){
                $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
                if($closeDate>$now ){
                    $textDiv = explode(" (",$coupon["Дивиденды"]); //Выкидываем пересчитанную в рубли сумму в скобках
                    $FutureYearSum += floatval($textDiv[0]);
                }
                
            }
            
            $Description = ""."(".$Description.")";
            
            if(floatval($LastPrice)>0){
                $YearPrc = round($YearSum/$LastPrice*100,2);
                $FutureYearPrc = round($FutureYearSum/$LastPrice*100,2);
            }
            try {
            echo "Description<pre  style='color:black; font-size:11px;'>";
               print_r($Description);
            echo "</pre>";
            //    CIBlockElement::SetPropertyValues($ElementId, $IblockId, $YearSum, "DIVIDENDS");
            //    CIBlockElement::SetPropertyValues($ElementId, $IblockId, $Description, "SOURCE");
                //Записываем будущие дивиденды
            //    CIBlockElement::SetPropertyValues($ElementId, $IblockId, $FutureYearSum, "FUTURE_DIVIDENDS");
            //    CIBlockElement::SetPropertyValues($ElementId, $IblockId, $FutureYearPrc, "FUTURE_DIVIDENDS_PRC");
            
            } catch (Exception $e) {
                $err = $e->getMessage();
                echo "<pre  style='color:black; font-size:11px;'>";
                print_r($err);
                echo "</pre>";
            }
        }
    }
    unset($res, $dataClass, $entity, $hlblock, $closeDate);
}//updateYearDividends

updateYearDividends(214255, 55, 'US37045V1008', 48.72);

 ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>