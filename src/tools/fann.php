<?define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
 global $USER;
$arGroups = $USER->GetUserGroupArray();

Global $APPLICATION, $USER;
if(!$USER->IsAuthorized()){
    LocalRedirect("/lk/radar_in/");
    exit();
} else {
    $uid = 7307;
    //CUsersStat::setStat($USER->GetID(), 'radar2', $APPLICATION->GetCurUri());
    $userStat = CUsersStat::getStat($uid , '/lk/obligations/');
    
    echo "<pre  style='color:black; font-size:11px;'>";
       print_r($userStat);
    echo "</pre>";
    
    $start = microtime(true);
    $res = CUsersStat::isNewRadarUser($uid);
    $end = microtime(true);
    $delta = $end-$start;
    echo "<pre  style='color:black; font-size:11px;'>";
       print_r($res?'Y':'N');
    echo "</pre>";
    
}

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	LocalRedirect("/");
	exit();
}
echo $delta;

/// <summary>
/// Возвращает результат нечеткого сравнения слов.
/// </summary>
/// <param name="firstToken">Первое слово.</param>
/// <param name="secondToken">Второе слово.</param>
/// <returns>Результат нечеткого сравения слов.</returns>
function IsTokensFuzzyEqual($firstToken, $secondToken) {
    $equalSubtokensCount = 0;
    $usedTokens = boolval(strlen($secondToken) - $SubtokenLength +1);
    for ($i = 0; $i < strlen($firstToken) - $SubtokenLength + 1; ++$i) {
        $subtokenFirst =  substr($firstToken, $i, $SubtokenLength);
        for ($j = 0; $j < strlen($secondToken) - $SubtokenLength + 1; ++$j) {
            if (!$usedTokens[$j]) {
                $subtokenSecond = substr($secondToken, $j, $SubtokenLength);
                if ($subtokenFirst === $subtokenSecond) {
                    $equalSubtokensCount++;
                    $usedTokens[$j] = true;
                    break;
                }
            }
        }
    }

    $subtokenFirstCount = strlen($firstToken) - $SubtokenLength + 1;
    $subtokenSecondCount = strlen($secondToken) - $SubtokenLength + 1;

    $tanimoto = (1.0 * $equalSubtokensCount) / ($subtokenFirstCount + $subtokenSecondCount - $equalSubtokensCount);

    return $ThresholdWord <= $tanimoto;
}

  $email1 = "test@mail.ru";
  $email2 = "t.estimon@mail.ru alphaprogrammer@";
?>
<?$APPLICATION->SetTitle("Инструменты администрирования");?>
 <div class="container">
 	<h1>Нечеткое сравнение слов</h1>
 	<div class="row">
 		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
 		 <div class="bg-success center-block index_blocks">
		  <?= $email1 ?> и <?= $email2 ?> одно и тоже? -> <?=((IsTokensFuzzyEqual($email1, $email2))?'Y':'N');?>
		 </div>
 		</div>
 	</div>
 </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>