<?define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
 global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	LocalRedirect("/");
	exit();
}
/* ----------------------- */

\Bitrix\Main\Loader::includeModule("iblock");
$data = ConnectMoex("https://iss.moex.com/iss/engines/stock/markets/bonds/securities.json");

$step = 0;
$datesArr = [];
$needLoop = true;
do{
    $dates = ConnectMoex("https://iss.moex.com/iss/history/engines/stock/markets/bonds/listing.json?status=traded&start=" . $step);
    foreach ($dates["securities"]["data"] as $v) {
        $datesArr[$v[0]] = $v[5];
    }
    $step += 100;
    $needLoop = (!$dates["securities"]["data"]) ? false : true;
}while($needLoop);

$ibID = 27;
$ibPageID = 30;
$arData = array();

//Получим имена всех эмитентов в массив
$arSelectEm = Array("ID", "NAME", "IBLOCK_ID");
$arFilterEm = Array("IBLOCK_ID"=>IntVal(26), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$resEm = CIBlockElement::GetList(Array(), $arFilterEm, false, false, $arSelectEm);
while($ob = $resEm->fetch()){
  $arEmitents[$ob["ID"]] = $ob["NAME"];
}
unset($resEm, $arFilterEm, $arSelectEm);



//соответствия колонок свойствам
$properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ibID));
while ($prop = $properties->GetNext())
{
	if(in_array($prop["CODE"], $data["securities"]["columns"])){
		$arData["PROPS"][array_search($prop["CODE"], $data["securities"]["columns"])] = $prop["CODE"];
	}
}

//ISIN
$arData["PROPS"][array_search("ISIN", $data["securities"]["columns"])] = "ISIN";

//Соберем существующие облигации в массив с именами их эмитентов с ключами по коду облигации
$arFilterObl = Array("IBLOCK_ID"=>$ibID);
$resObl = CIBlockElement::GetList(Array(), $arFilterObl, false, false, array("ID", "IBLOCK_ID", "PROPERTY_EMITENT_ID", "PROPERTY_ISIN", "PROPERTY_BOARDID", "PROPERTY_SECID", "PROPERTY_OFFER_NOUPDATE"));
$arAllObligations = array();
$arAllObligationsBySecid = array();
while($ob = $resObl->fetch())
{
    $arAllObligations[$ob["PROPERTY_ISIN_VALUE"]] = array("ID"=>$ob["ID"], "BOARDID"=>$ob["PROPERTY_BOARDID_VALUE"], "EMITENT_NAME"=>$arEmitents[$ob["PROPERTY_EMITENT_ID_VALUE"]], "SECID"=>$ob["PROPERTY_SECID_VALUE"], "OFFER_NOUPDATE"=>$ob["PROPERTY_OFFER_NOUPDATE_VALUE"]);
    $arAllObligationsBySecid[$ob["PROPERTY_SECID_VALUE"]] = array("ID"=>$ob["ID"], "BOARDID"=>$ob["PROPERTY_BOARDID_VALUE"], "EMITENT_NAME"=>$arEmitents[$ob["PROPERTY_EMITENT_ID_VALUE"]], "SECID"=>$ob["PROPERTY_SECID_VALUE"], "OFFER_NOUPDATE"=>$ob["PROPERTY_OFFER_NOUPDATE_VALUE"]);
}
unset($ob, $arFilterObl, $resObl);

/*$el = new CIBlockElement;

//обработка
$arHaveCodes = array();
foreach ($data["securities"]["data"] as $item){
	if($item[0]!='SU29018RMFS7') continue;
	$PROP = array();

	foreach($arData["PROPS"] as $num=>$code){
		$PROP[$code] = $item[$num];
	}

    if(array_key_exists($item[0], $datesArr)) {
        $PROP["STARTDATE"] = $datesArr[$item[0]];
    }

	if($PROP["ISIN"]){
		$item[0] = $PROP["ISIN"];
	}

	$arHaveCodes[] = $item[0];

	$arLoadProductArray = Array(
		"IBLOCK_ID"      => $ibID,
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => $item[2], //SHORTNAME
		"CODE" => $item[0], //SECID
		"ACTIVE"         => "Y",
	);

	//Получим имя эмитента для страницы облигации (для title через SEO вкладку)
	$emitentName = '';
	if(array_key_exists($item[0],$arAllObligations))
	{
        $arObligData = $arAllObligations[$item[0]];
        $emitentName = $arObligData["EMITENT_NAME"];
		unset($arLoadProductArray["PROPERTY_VALUES"]);

		$el->Update($arObligData["ID"], $arLoadProductArray);
		foreach($PROP as $code=>$value){
			if($code == "OFFERDATE" && $arObligData["OFFER_NOUPDATE"]=='Y') continue;
			//TODO Сделать апдейт свойств одним запросом
			CIBlockElement::SetPropertyValues($arObligData["ID"], $ibID, $value, $code);
		}
	} else {

		//+Получаем первоначальную номинальную стоимость и добавляем в список свойств
			$dataInitVal = ConnectMoex("https://iss.moex.com/iss/securities/".$PROP["ISIN"]."/.json");
		$moexval = false;
		foreach($dataInitVal["description"]["data"] as $n=>$val){
			if($val[0]=="INITIALFACEVALUE"){
				$moexval = $val[2];
				$arLoadProductArray["PROPERTY_VALUES"]["INITIALFACEVALUE"] = $val[2];
				break;
			}
		}
		//Если с биржи ничего не пришло - то возьмем текущий номинал как первоначальный
		if($moexval==false && array_key_exists("FACEVALUE",$arLoadProductArray["PROPERTY_VALUES"])){
		  $arLoadProductArray["PROPERTY_VALUES"]["INITIALFACEVALUE"] = $arLoadProductArray["PROPERTY_VALUES"]["FACEVALUE"];
		}

	  unset($dataInitVal);
		//-Получаем первоначальную номинальную стоимость и добавляем в список свойств

		$el->Add($arLoadProductArray);
	}

	$arFilter = Array("IBLOCK_ID"=>$ibPageID, "CODE"=>$item[0]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	if(!$res->SelectedRowsCount())
	{

		$char = ord(substr($item[2], 0 ,1));
		if($char<=122){
			$sort = 20;
		} else {
			$sort = 10;
		}
		$arProp["EMITENT_NAME"] = $emitentName;
		$el->Add(array(
			"IBLOCK_ID"      => $ibPageID,
			"NAME"           => $item[2], //SHORTNAME
			"CODE" => $item[0], //SECID
			"PROPERTY_VALUES" => $arProp,
			"SORT" => $sort,
		));
	} elseif($row = $res->fetch()) {
		CIBlockElement::SetPropertyValues($row["ID"], $ibPageID, $emitentName, "EMITENT_NAME");
	}

}*/

/* -------------------- */
?>
<?$APPLICATION->SetTitle("Инструменты администрирования");?>
 <div class="container-fluid">
 	<h1>Инструменты администрирования</h1>
 	<div class="row">
 		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
 		 <div class="bg-success center-block index_blocks">
		  <h2>Облиги</h2>
		 </div>
 		</div>
		</div>
			<div class="row">
 		<div class="col-md-6">
 		 <div class="bg-success center-block index_blocks">
<? echo "<pre  style='color:black; font-size:11px;'>";
   print_r(array_combine( $data["securities"]["columns"], $data["securities"]["data"][2100]) );
  // print_r($data["securities"]["data"][2100]);
   echo "</pre>"; ?>
 		 </div>
 		</div>
 		<div class="col-md-6">
 		 <div class="bg-success center-block index_blocks">
<? echo "<pre  style='color:black; font-size:11px;'>";
   print_r($data["securities"]["columns"]);
   echo "</pre>"; ?>
	<? echo "<pre  style='color:black; font-size:11px;'>";
   print_r($PROP);
   echo "</pre>"; ?>
 		 </div>
 		</div>


 	</div>
 </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>