<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
/**
 * Скрипт расчета данных свечного графика для акции, прототип
 */
         if(!$GLOBALS["USER"]->IsAdmin()){
           LocalRedirect("/");
         }
?>
<?
  use Bitrix\Highloadblock as HL;
$APPLICATION->SetTitle("Скрипт расчета данных свечного графика для акции, прототип");?>
<br><br><br><br><br><br><br><br><br>
<?
\Bitrix\Main\Loader::includeModule("highloadblock");

CModule::IncludeModule("iblock");
  $begin = microtime(true);
$code = 'GAZP';
$hlCandleTableName = "hl_candle_graph_data";
$hlTableName = "hl_moex_actions_data";
//$hlTableName = "hl_spb_actions_data";

 Global $DB;

  //Получим последние даты расчитанных свечей (по последним записям месячного диапазона) для всех акций
  $querySelectAllCandles = "SELECT `ID`, `UF_TICKER`, `UF_PERIOD`, `UF_DATE_FROM`, `UF_DATE_TO` FROM `$hlCandleTableName` WHERE `UF_CURRENT_PERIOD`='Y' ORDER BY 'UF_DATE_TO' DESC";
  $arAllCandles = array();
  $res = $DB->Query($querySelectAllCandles);
  while($row = $res->fetch()){
	 $arAllCandles[$row["UF_TICKER"]][$row["UF_PERIOD"]] = $row;
  }

  //Если в выборке свечей нет записи с текущим периодом - то на всякий случай затираем данные по текущему активу полностью и перезаписываем всю историю вместе с текущим периодом
  //Эта ситуация так же возникает, если была удалена запись для текущего месяца.
	if(!array_key_exists('M', $arAllCandles[$code]) || !array_key_exists('W', $arAllCandles[$code])){
	  $queryClear = "DELETE FROM `$hlCandleTableName` WHERE `UF_TICKER` = '$code'";
	  $DB->StartTransaction();
	  $DB->Query($queryClear);
	  $DB->Commit();
	  $queryClear = '';
	  $arAllCandles = array();
	}


  $firephp = FirePHP::getInstance(true);
  $firephp->fb($arAllCandles,FirePHP::LOG);

  $query = "SELECT * FROM `$hlTableName` WHERE `UF_ITEM` = '$code'";
  $hasDateFilter = false;
  if(array_key_exists($code, $arAllCandles)){
	 if(!empty($arAllCandles[$code]['M']['UF_DATE_FROM']) && !empty($arAllCandles[$code]['W']['UF_DATE_FROM'])){
		$query .= " AND `UF_DATE`>='".(new DateTime($arAllCandles[$code]['M']['UF_DATE_FROM']))->format('Y-m-d')."'";
		$hasDateFilter = true;
	 }
  }
  $query .= " ORDER BY `UF_DATE` ASC";
  $res = $DB->Query($query);

  $arMonths = array();
  $arWeeks = array();

  while($row = $res->fetch()){
  	$UF_CURRENT_PERIOD_MONTH = (new DateTime($row["UF_DATE"]))->getTimestamp()>=(new DateTime())->modify('first day of this month')->getTimestamp()?'Y':'';
  	$UF_CURRENT_PERIOD_WEEK = (new DateTime($row["UF_DATE"]))->getTimestamp()>=(new DateTime())->modify('this week monday')->getTimestamp()?'Y':'';

	//дата для месяцев
	$dateMonth = (new DateTime($row["UF_DATE"]))->format('m.Y');
	//дата для недель
	$dateWeek = (new DateTime($row["UF_DATE"]))->format('W.m.Y'); //Номер недели, месяц, год


	//Расчет свечей для диапазона "месяц"
	//Если начинаем новый месяц
	if(!array_key_exists($dateMonth, $arMonths)){
	 $arMonths[$dateMonth]['UF_TICKER'] = $code;
	 $arMonths[$dateMonth]['UF_DATE_FROM'] = $row["UF_DATE"];
	 $arMonths[$dateMonth]['UF_DATE_TO'] = $row["UF_DATE"];
	 $arMonths[$dateMonth]['UF_PERIOD'] = "M"; //Диапазон - месяц
	 $arMonths[$dateMonth]['UF_CURRENT_PERIOD'] = $UF_CURRENT_PERIOD_MONTH;
	 $arMonths[$dateMonth]['UF_OPEN'] = $row["UF_OPEN"];
	 $arMonths[$dateMonth]['UF_HIGH'] = $row["UF_HIGH"];
	 $arMonths[$dateMonth]['UF_LOW'] = $row["UF_LOW"];
	 $arMonths[$dateMonth]['UF_CLOSE'] = $row["UF_CLOSE"];
	} else { //Если считаем для существующего месяца
	 $arMonths[$dateMonth]['UF_DATE_TO'] = $row["UF_DATE"];
	 if($arMonths[$dateMonth]['UF_HIGH']<$row["UF_HIGH"]){
	   $arMonths[$dateMonth]['UF_HIGH'] = $row["UF_HIGH"];
		}
	 if($arMonths[$dateMonth]['UF_LOW']>$row["UF_LOW"]){
	  	$arMonths[$dateMonth]['UF_LOW'] = $row["UF_LOW"];
	  }
	 $arMonths[$dateMonth]['UF_CLOSE'] = $row["UF_CLOSE"];
	}

	//Расчет свечей для диапазона "неделя"
	//Если начинаем новый месяц
		$add = false;
     if($hasDateFilter && $UF_CURRENT_PERIOD_WEEK=='Y'){
     	$add = true;
     }else if($hasDateFilter && $UF_CURRENT_PERIOD_WEEK!='Y'){
     	$add = false;
     }else if(!$hasDateFilter){
		$add = true;
     }

	 if($add){
	if(!array_key_exists($dateWeek, $arWeeks)){
	 $arWeeks[$dateWeek]['UF_TICKER'] = $code;
	 $arWeeks[$dateWeek]['UF_DATE_FROM'] = $row["UF_DATE"];
	 $arWeeks[$dateWeek]['UF_DATE_TO'] = $row["UF_DATE"];
	 $arWeeks[$dateWeek]['UF_PERIOD'] = "W"; //Диапазон - неделя
	 $arWeeks[$dateWeek]['UF_CURRENT_PERIOD'] = $UF_CURRENT_PERIOD_WEEK;
	 $arWeeks[$dateWeek]['UF_OPEN'] = $row["UF_OPEN"];
	 $arWeeks[$dateWeek]['UF_HIGH'] = $row["UF_HIGH"];
	 $arWeeks[$dateWeek]['UF_LOW'] = $row["UF_LOW"];
	 $arWeeks[$dateWeek]['UF_CLOSE'] = $row["UF_CLOSE"];
	} else { //Если считаем для существующей недели
	 $arWeeks[$dateWeek]['UF_DATE_TO'] = $row["UF_DATE"];
	 if($arWeeks[$dateWeek]['UF_HIGH']<$row["UF_HIGH"]){
	   $arWeeks[$dateWeek]['UF_HIGH'] = $row["UF_HIGH"];
		}
	 if($arWeeks[$dateWeek]['UF_LOW']>$row["UF_LOW"]){
	  	$arWeeks[$dateWeek]['UF_LOW'] = $row["UF_LOW"];
	  }
	 $arWeeks[$dateWeek]['UF_CLOSE'] = $row["UF_CLOSE"];
	}
	} //if add == true for week

  } //while расчета свечей


  savePeriodsToDB($hlCandleTableName, $arAllCandles, $code, $arMonths, "M");
  savePeriodsToDB($hlCandleTableName, $arAllCandles, $code, $arWeeks, "W");


function savePeriodsToDB($hlCandleTableName, &$arAllCandles, $ticker=false, $arData=array(), $period=false){
  Global $DB;
  if(!$ticker || !$period || count($arData)<=0 || empty($hlCandleTableName)) return false; //Если переданы не все данные - выходим
  //Запись в БД рассчитанных данных для свечей месяцев

  foreach($arData as $k=>$value){

        $DB->PrepareFields($hlCandleTableName);
        $arFields = array(
            "UF_TICKER"                    => "'".trim($value['UF_TICKER'])."'",
            "UF_DATE_FROM"                 => "'".trim($value['UF_DATE_FROM'])."'",
            "UF_DATE_TO"                   => "'".trim($value['UF_DATE_TO'])."'",
            "UF_PERIOD"                    => "'".trim($value['UF_PERIOD'])."'",
            "UF_CURRENT_PERIOD"            => "'".trim($value['UF_CURRENT_PERIOD'])."'",
            "UF_OPEN"            			 => "'".floatval($value['UF_OPEN'])."'",
            "UF_HIGH"            			 => "'".floatval($value['UF_HIGH'])."'",
            "UF_LOW"            			    => "'".floatval($value['UF_LOW'])."'",
            "UF_CLOSE"            			 => "'".floatval($value['UF_CLOSE'])."'",
            );

        $DB->StartTransaction();
		  //Если запись существует то обновляем ее по ID
        if ($arAllCandles[$value['UF_TICKER']][$period]['UF_DATE_TO']==$value['UF_DATE_TO'])
        {
			  if($value['UF_CURRENT_PERIOD']=='Y'){
			  	$ID = $arAllCandles[$value['UF_TICKER']][$period]['ID'];
        	    $firephp = FirePHP::getInstance(true);
        	    $firephp->fb(array("key"=>$k." update ".$value['UF_CURRENT_PERIOD'], "ID"=>$ID, "per"=>$period),FirePHP::LOG);
             $DB->Update($hlCandleTableName, $arFields, "WHERE ID='".$ID."'", $err_mess.__LINE__);
			  }
        }
        else //Иначе добавляем новую запись
        {
        	  $firephp = FirePHP::getInstance(true);
        	  $firephp->fb(array("label"=>$k." insert ".$value['UF_CURRENT_PERIOD'], "per"=>$period),FirePHP::LOG);
           $ID = $DB->Insert($hlCandleTableName, $arFields, $err_mess.__LINE__);
        }
       // $ID = intval($ID);
        if (strlen($strError)<=0)
        {
            $DB->Commit();
        }
        else {
        	CLogger::CandleHLError($strError);
        	$DB->Rollback();
			}
  }
}




 echo $code."<br>";
 echo microtime(true) - $begin;
 echo PHP_EOL;



 ?>
	<table cellpadding="3" cellspacing="3">
   	<tr>
   		<th>Мес.</th>
   		<th>Нед.</th>
   	</tr>
   	<tr>
   		<td valign="top">
	<?  echo "<pre  style='color:black; font-size:11px;'>";
    print_r($arMonths);
    echo "</pre>"; ?>
	 </td>
   		<td valign="top">
			<?  echo "<pre  style='color:black; font-size:11px;'>";
    print_r($arWeeks);
    echo "</pre>"; ?>
			</td>
   	</tr>
   </table>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>