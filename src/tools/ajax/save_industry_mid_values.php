<?php
/**
 * Записывает правочные значения по среднеотраслевым показателям в хайлоад блок
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock as HL;
CModule::IncludeModule('highloadblock');

$hlblock     = HL\HighloadBlockTable::getById(43)->fetch();
$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
$entityClass = $entity->getDataClass();

/*$_POST['sector'] = urldecode($_POST['sector']);
$_POST['mark'] = urldecode($_POST['mark']);
$_POST['year'] = urldecode($_POST['year']);*/



$arPost = array();
foreach($_POST as $key=>$param){
    if(strpos($key, "UF_")!==false){
        $arPost[$key] = $param;
    }
}

$mode = 'cell';
if($_POST["MODE"]=='row'){
	$mode = 'row';
}

if($mode=='row') {
	//Делаем правку для строки актива
	$arFilter = array("PROPERTY_SECID"=>$arPost["UF_ACTIVE_CODE"]);
	$arSelect = array("ID", "CODE", "NAME", "IBLOCK_ID", "PROPERTY_SECID");
	if($arPost["UF_COUNTRY"]=="RUS"){
		$arFilter["IBLOCK_ID"]=32;
		$resRadar = new Actions();
	} elseif($arPost["UF_COUNTRY"]=="USA") {
		$arFilter["IBLOCK_ID"]=55;
		$resRadar = new ActionsUsa();
	}

	$res_action = CIBlockElement::GetList(array(),$arFilter,false,false,$arSelect);
	$arPeriods = array();//Периоды для которых делается правка
	if($row = $res_action->fetch()){
		$arPeriods = array_keys($resRadar->getItem($row["CODE"])["PERIODS"]);
		unset($resRadar, $res_action);
	}

//Получаем все правки для актива по сектору и показателю
	$arFilter = array("UF_COUNTRY"     => $arPost["UF_COUNTRY"],
					  "UF_SECTOR_NAME" => $arPost["UF_SECTOR_NAME"],
					  "UF_PARAM_NAME"  => $arPost["UF_PARAM_NAME"],
					  "UF_ACTIVE_CODE" => $arPost["UF_ACTIVE_CODE"],
	);
	$resHL = $entityClass::getList(array("select" => array("ID", "UF_KVARTAL", "UF_YEAR"),
										 "filter" => $arFilter,
										 "order"  => array("UF_YEAR" => "ASC", "UF_KVARTAL" => "ASC"),
	));
    $arFixPeriods = array();
	if(isset($_POST['delete']) && $_POST['delete'] == 'y') { //Для удаления используем упрощенный обход
		while($arExistFix = $resHL->fetch()) {
			$otvet = $entityClass::delete($arExistFix["ID"]);
		}
	} else {//Для обновления/добавления обходим с проверкой
		while($arExistFix = $resHL->fetch()) {

			$action = 'update';
			$changeId = $arExistFix["ID"];

			$HlPeriod = $arExistFix["UF_KVARTAL"] . '-' . $arExistFix["UF_YEAR"] . '-KVARTAL';

			if(in_array($HlPeriod, $arPeriods)) {
				$arFixPeriods[] = $HlPeriod;
				$action = 'update';
				$otvet = $entityClass::update($changeId, $arPost);
			}

		}//while $resHL

		//Добавим недостающие периоды в правки
		$arDiffPeriods = array_diff($arPeriods, array_values($arFixPeriods));

		if(count($arDiffPeriods)){
			foreach($arDiffPeriods as $dPeriod){
				$arPeriod = explode("-",$dPeriod);
				$arPost["UF_KVARTAL"] = $arPeriod[0];
				$arPost["UF_YEAR"] = $arPeriod[1];
				$otvet = $entityClass::add($arPost);
			}
		}

	}

	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	if($arPost["UF_COUNTRY"] == 'RUS') {
		$cache->clean('industries_rus_data');
		$CI = new CIndustriesRus();
		unset($CI);
	} elseif($arPost["UF_COUNTRY"] == 'USA'){
		$cache->clean('sectors_usa_data');
		$CI = new SectorsUsa();
		unset($CI);
	}

}// mode = row


if($mode=='cell') {
	//Делаем правку для ячейки
	$arFilter = array("UF_COUNTRY"     => $arPost["UF_COUNTRY"],
					  "UF_SECTOR_NAME" => $arPost["UF_SECTOR_NAME"],
					  "UF_PARAM_NAME"  => $arPost["UF_PARAM_NAME"],
					  "UF_ACTIVE_CODE" => $arPost["UF_ACTIVE_CODE"],
					  "UF_KVARTAL"     => $arPost["UF_KVARTAL"],
					  "UF_YEAR"        => $arPost["UF_YEAR"],
	);
	$resHL = $entityClass::getList(array("select" => array("ID",),
										 "filter" => $arFilter,
										 "order"  => array("UF_YEAR" => "ASC", "UF_KVARTAL" => "ASC"),
	));
	$action = '';
	$updateId = 0;
	if($arExistFix = $resHL->fetch()) {
		$action = 'update';
		$changeId = $arExistFix["ID"];
		if(isset($_POST['delete']) && $_POST['delete'] == 'y') {
			$action = 'delete';
		}
	} else {
		$action = 'add';
	}


	if(array_key_exists("UF_VALUE", $arPost)) {

		switch($action) {
			case 'add':
				$otvet = $entityClass::add($arPost);
				break;
			case 'update':
				$otvet = $entityClass::update($changeId, $arPost);
				break;
			case 'delete':
				$otvet = $entityClass::delete($changeId);
				break;
		}


	} else {
		$result = array("result" => "error", "message" => "Попытка записать пустое значение в качестве корректировки");
	}
	if($otvet && $otvet->isSuccess()) {
		if($action != 'delete') {
			$result = array("result"  => "success",
							"message" => "action=" . $action . " обработан id=" . $otvet->getId()
			);
		} else {
			$result = array("result" => "success", "message" => "Корректировка удалена");
		}
		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		if($arPost["UF_COUNTRY"] == 'RUS') {
			$cache->clean('industries_rus_data');
			$CI = new CIndustriesRus();
			unset($CI);
		} elseif($arPost["UF_COUNTRY"] == 'USA'){
			$cache->clean('sectors_usa_data');
			$CI = new SectorsUsa();
			unset($CI);
		}

	} else if($otvet) {
		$result = array("result" => "error", "message" => "Ошибка: " . implode(', ', $otvet->getErrors()));
	}

} // mode = cell


  echo json_encode($result);?>