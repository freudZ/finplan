<?php
/**
 * Записывает ручной таргет и сразу же обновляет основной таргет для инфоблока 55 - акции США
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


$IblockId = 55;
$ElementId = intval($_POST["element_id"]);
$target = floatval($_POST["target"]);

if($ElementId>0 && !empty($_POST["target"])){
	CModule::IncludeModule("iblock");
                    CIBlockElement::SetPropertyValues(
                        $ElementId,
                        $IblockId,
                        round($target, 2),
                        "TARGET"
                    );
                    CIBlockElement::SetPropertyValues(
                        $ElementId,
                        $IblockId,
                        round($target, 2),
                        "TARGET_CUSTOM"
                    );
							 echo json_encode(array("result"=>"ok", "id"=>$ElementId, "message"=>"Данные сохранены"));
							 die();
						  } else {
							 echo json_encode(array("result"=>"error", "id"=>$ElementId, "message"=>"Ошибка! Данные не записаны"));
							 die();
						  }