<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
//Синхронизация цен акций и облигаций
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
use Bitrix\Highloadblock as HL;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

$_SERVER["SERVER_NAME"]="fin-plan.org";

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 193960;
$cCronTools->changeStartTime($cronTaskId);
$start = microtime(true);

$sync = new MoexSync();
//set_time_limit(300);

//обнуление
/*$arFilter = Array("IBLOCK_ID"=>32, "!PROPERTY_MOEX_LOADED" => false);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, ["IBLOCK_ID", "ID", "CODE", "PROPERTY_SECID", "PROPERTY_MOEX_LOADED"]);
while($item = $res->GetNext()){
    CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], "", "MOEX_LOADED");
}*/

//синхрон акций
//Пишем константу времени обновления для показа на страницах акций
\Bitrix\Main\Config\Option::set("grain.customsettings","ACTION_PRICES_DATE", (new DateTime())->format('d.m.Y H:i'));
$arFilter = Array("IBLOCK_ID"=>32, "!PROPERTY_SECID" => false);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, ["IBLOCK_ID", "ID", "CODE", "PROPERTY_SECID", "PROPERTY_MOEX_LOADED"]);
while($item = $res->GetNext()){
    $sync->processAction(
        $item,
        $item["PROPERTY_SECID_VALUE"],
        $item["PROPERTY_MOEX_LOADED_VALUE"]?true:false
    );
	 $sync->updateActionPrice($item["ID"], 32, $item["PROPERTY_SECID_VALUE"]);
}

//обнуление
/*$arFilter = Array("IBLOCK_ID"=>27, "!PROPERTY_MOEX_LOADED" => false);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, ["IBLOCK_ID", "ID", "CODE", "PROPERTY_SECID", "PROPERTY_MOEX_LOADED"]);
while($item = $res->GetNext()){
    CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], "", "MOEX_LOADED");
}*/

//синхрон облигаций
$arFilter = Array("IBLOCK_ID"=>27, "!PROPERTY_SECID" => false);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, ["IBLOCK_ID", "ID", "CODE", "PROPERTY_SECID", "PROPERTY_MOEX_LOADED"]);
while($item = $res->GetNext()){
    $code = $item["CODE"];
    if($item["PROPERTY_SECID_VALUE"] != $code){
        $code = $item["PROPERTY_SECID_VALUE"];
    }

    $sync->processObligation(
        $item,
        $code,
        $item["PROPERTY_MOEX_LOADED_VALUE"]?true:false
    );
	$sync->updateObligationPrice($item["ID"], 27, $code);
}


$finish = microtime(true);
$delta = $finish - $start;

//mail("alphaprogrammer@gmail.com","CRON: syncMoexDaily", "Время работы скрипта:" . $delta . " сек.");
echo "Время работы скрипта:" . $delta . " сек.\n";
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));