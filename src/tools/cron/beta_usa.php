<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Проверка и перерасчет беты США */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 305632;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

$logDate = date('Y-m-d_H-i-s');
$logfile = $_SERVER["DOCUMENT_ROOT"] . '/log/beta_usa/beta_usa_log_'.$logDate.'.csv';
file_put_contents($logfile, 'ticker;beta' . PHP_EOL, FILE_APPEND);

$beta = new CBeta;
if($beta->initCalculateUsaData()==true){ // Если нужно пересчитывать бету США
  $arTickerList = $beta->getTickerUSAList();

  foreach($arTickerList as $secid=>$id){
	 $arReturn = $beta->calculateOneActionUSABeta($secid,false);
	 if(!is_nan($arReturn["BETA"])){
	 	CIBlockElement::SetPropertyValues(intval($id), 55, floatval($arReturn["BETA"]), "BETTA");
	 }

  	 file_put_contents($logfile, $secid.';'.$arReturn["BETA"] . PHP_EOL, FILE_APPEND);
	 unset($arReturn);
  }

  $beta->setSheduleBetaUSA(false);
  $endDate = (new DateTime(strtotime(microtime(true))))->format('d.m.Y H:i:s');
  $beta->setSheduleBetaUSADate($endDate);
}//если инициализация и обсчет требуется

$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
