<?php //Парсер для акций США с биржи СПБ

if (empty($_SERVER["DOCUMENT_ROOT"])) {
    $_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . "/../..");
    $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
    $_SERVER["SERVER_NAME"] = "fin-plan.org";
}
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);

//echo "> prolog include".PHP_EOL;
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock as HL;

//echo "> crontools include".PHP_EOL;
require($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/classes/fpt_crontools.php");

\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("highloadblock");
//CModule::IncludeModule("iblock");
//CModule::IncludeModule("highloadblock");



///\Bitrix\Main\Loader::includeModule("iblock");
//use \Bitrix\Main\Loader;

$cCronTools = new fptСrontools;
$cronTaskId = 255571;
$cCronTools->changeStartTime($cronTaskId);
$start = microtime(true);

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean("actions_usa_data");

$ActionsUsa = new ActionsUsa();

$dataUrl = "http://api-spb.fin-plan.org/daily";
$client = new \GuzzleHttp\Client();
$offset = 0;
$ibPageID = 56;
$ibID = 55;//Инфоблок акций США
$need = true;
$hlblock = HL\HighloadBlockTable::getById(29)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$quotationsData = [];

$dates = [
	 'DAY' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P2D')),//Для США lastprice всегда равно цене вчерашней (последней) даты, по этому берем разницу в 2 дня
    'MONTH' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1M')),
    'ONE_YEAR' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1Y')),
    'THREE_YEAR' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P3Y')),
	 'TWO_DAY' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P3D')),//Нужен для расчета роста/падения при переходе через выходные и определении цены за день назад
];

//echo "> Список акций с lastprice для расчета однодневного роста".PHP_EOL;
//Список акций с lastprice для расчета однодневного роста
$arSelectInrease = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_LASTPRICE", "PROPERTY_SECID");
$arFilterInrease = Array("IBLOCK_ID"=>$ibID,  "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$resInrease = CIBlockElement::GetList(Array(), $arFilterInrease, false, false, $arSelectInrease);
$arLastpriceBase = array();
while($ob = $resInrease->Fetch()){
 $arLastpriceBase[$ob["PROPERTY_SECID_VALUE"]] = $ob["PROPERTY_LASTPRICE_VALUE"];
}
//echo "> ok".PHP_EOL;

//echo "> work days".PHP_EOL;
foreach ($dates as $k => $v) {
    if (isUsaWeekendDay($v->format("d.m.Y"))) {
        $finded = false;
		  //задаем направление поиска ближайшего рабочего дня
		  $countDir = '+'; // по умолч. для длинных интервалов ищем вперед
		  if($k=='DAY' || $k=='TWO_DAY'){
		  	$countDir = '-'; // для интервала в день ищем назад
		  }
		  if($k!='TWO_DAY'){
	        while (!$finded) {
	            $v->modify($countDir."1 days");
	            if (!isUsaWeekendDay($v->format("d.m.Y"))) {
	                $finded = true;
	            }
	        }
		  }
		 if($k=='TWO_DAY'){
        while(!$finded){
            $v->modify($countDir."1 days");
            if(!isWeekEndDay($v->format("d.m.Y")) && $v!=(new DateTime($dates['DAY']))){
                $finded = true;
            	}
        		}

		 }

    }
    $dates[$k] = $v->format("d.m.Y");
}
//echo "> ok".PHP_EOL;

//echo "> get close prices on date quotations".PHP_EOL;
Global $DB;
foreach ($dates as $k => $date) {
	 $dt = (new DateTime($date))->format('Y-m-d');
	 $dt_2 = (new DateTime($date))->modify('-2 days')->format('Y-m-d');
	 $query = "SELECT `UF_ITEM`, `UF_CLOSE` FROM `hl_polygon_actions_data` WHERE `UF_DATE` BETWEEN '$dt_2' AND '$dt'";

	 $res = $DB->Query($query);
/*    $res = $entity_data_class::getList([
        "filter" => [
            //"UF_DATE" => $date,
				["<=UF_DATE" => $date], [">=UF_DATE" => (new DateTime($date))->modify('-2 days')->format('d.m.Y'),]
        ],
        "select" => [
            "UF_CLOSE",
            "UF_ITEM",
        ],
    ]);*/

    while ($row = $res->fetch()) {
				if(floatval($row['UF_CLOSE'])>0){
				  $quotationsData[$k][$row["UF_ITEM"]] = $row['UF_CLOSE'];
				  break;
				}
        //$quotationsData[$k][$item["UF_ITEM"]] = $item["UF_CLOSE"];
    }
}
//echo "> ok".PHP_EOL;

//Собираем страницы компаний США
$arUsaActionsPages = array();
$arSelectUAP = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_CODE");
$arFilterUAP = Array("IBLOCK_ID" => IntVal($ibPageID));
$res = CIBlockElement::GetList(Array(), $arFilterUAP, false, false, $arSelectUAP);

while ($ob = $res->Fetch()) {
    $arUsaActionsPages[$ob["CODE"]] = $ob;
}

//Собираем акции США
$arUsaActions = array();
$arSelectUA = Array("ID",
                "IBLOCK_ID",
                "CODE",
                "NAME",
                "PROPERTY_SECID",
                "PROPERTY_EMITENT_ID",
                "PROPERTY_SECTOR",
                "PROPERTY_LASTPRICE",
                "PROPERTY_SECURITIES_QUANTITY",
                "PROPERTY_SP500",
                "PROPERTY_UNDERSTIMATION",
                "PROPERTY_TARGET_CUSTOM");
$arFilterUA = Array("IBLOCK_ID" => IntVal($ibID));
$res = CIBlockElement::GetList(Array(), $arFilterUA, false, false, $arSelectUA);

while ($ob = $res->Fetch()) {
    $arUsaActions[$ob["PROPERTY_SECID_VALUE"]] = $ob;
}

//Собираем названия эмитентов с ключами по ID
//Собираем акции США
$arUsaCompany = array();
$arSelectUACompany = Array("ID",
                "IBLOCK_ID",
                "CODE",
                "NAME",
               );
$arFilterUACompany = Array("IBLOCK_ID" => IntVal(43));
$res = CIBlockElement::GetList(Array(), $arFilterUACompany, false, false, $arSelectUACompany);

while ($ob = $res->Fetch()) {
    $arUsaCompany[$ob["ID"]] = $ob["NAME"];
}


//получение данных с биржи
while ($need) {

    $body = ['offset' => $offset];
    $res = $client->request("POST", $dataUrl, [
        'json' => $body,
    ]);

    $data = json_decode($res->getBody()->getContents(), true);
    CLogger::actions_usa_POST(print_r($data, true));
    $need = count($data) ? true : false;

    usaActionsLoop($data, $arUsaActionsPages, $dates, $quotationsData, $arLastpriceBase, $arUsaActions, $arUsaCompany, $ActionsUsa);
    usleep(500);
    $offset += 100;
}

//Сбросим кеш списка отраслей
$cache->clean("actions_usa_filters_data");

midSectorPE($ActionsUsa);

$ActionsUsa->CacheCompanyGraphics();

//Пересчитать количества вхождений акций США в портфели
try {
  $CPortfolio = new Cportfolio();
  $CPortfolio->getActionsUsaCntInPortfolios(true);
  unset($CPortfolio);
}catch (Exception $e){
	mail("alphaprogrammer@gmail.com", "Ошибка пересчета кол-ва вхождений акций США в портфели", print_r($e->getMessage(), true));
    var_dump("Cportfolio->getActionsUsaCntInPortfolios: " . $e->getMessage());
}


$finish = microtime(true);
$delta = $finish - $start;

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta, 2));

echo "Время работы скрипта:" . $delta . " сек.\n";


$cache->clean("actions_usa_data");
global $CACHE_MANAGER;
$CACHE_MANAGER->ClearByTag("usa_graphdata");

function usaActionsLoop($data, $arUsaActionsPages, $dates, $quotationsData, $arLastpriceBase, $arUsaActions, $arUsaCompany, $ActionsUsa)
{
	 Global $APPLICATION;
    $hlblock = HL\HighloadBlockTable::getById(intval($APPLICATION->usaPricesHlId))->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    $el = new CIBlockElement;
    //$actionsObj = new ActionsUsa();
    $actionsObj = $ActionsUsa;

    //получаем средний PE секторов
    $ibID = 58;
    $sectorsData = [];
    $arFilter = ["IBLOCK_ID" => $ibID];
    $res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "IBLOCK_ID", "NAME", "PROPERTY_MID_PE"]);

    while ($item = $res->fetch()) {
        $sectorsData[$item["NAME"]] = $item["PROPERTY_MID_PE_VALUE"];
    }

    $ibID = 55;
    $i = 0;
    $cnt = 0;
    foreach ($data as $val) {
        $code = $val['ticker'];
		  $cnt++;
		  $item = $arUsaActions[$code];
        if ($item) {

            //добавление страницы акции, если ее нет
            if (!array_key_exists($item["CODE"], $arUsaActionsPages)) {
            	$PROPS["SEO_TICKER"] = $item["SECID"];
            	$PROPS["SEO_COMPANY"] = $arUsaCompany[$item["PROPERTY_EMITENT_ID_VALUE"]];
                $arLoadProductArray = [
                    "IBLOCK_ID" => 56,//страницы акций сша
                    "NAME" => $item["NAME"],
                    "CODE" => $item['CODE'],
                ];

                if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                    $arUsaActionsPages[$item['CODE']] = array("ID" => $PRODUCT_ID);
                } else {
                    echo "Error: " . $el->LAST_ERROR;
                }
            }

            //Динамика акций
            foreach ($dates as $k => $date) {
            	if($k=="TWO_DAY") continue;//Пропускаем дату два дня назад, нужна только для обсчета роста за 1 день при переходе через выходные
                if (isset($quotationsData[$k][$item["PROPERTY_SECID_VALUE"]])) {
                    CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $quotationsData[$k][$item["PROPERTY_SECID_VALUE"]], "QUOTATIONS_$k");
                    $i++;
                }

					  //Считаем рост/падение для одного дня
					  if($k=="DAY"){
						  	 $actualLastprice = 0;
							 if(isset($quotationsData["DAY"][$item["PROPERTY_SECID_VALUE"]])){
							   $qDay = $quotationsData["DAY"][$item["PROPERTY_SECID_VALUE"]];
							 } else {
								continue;
							 }

							 if( $qDay==floatval($arLastpriceBase[$item["PROPERTY_SECID_VALUE"]])){
								 $resultNowPrice = $entity_data_class::getList([
					            "filter" => [
					                "UF_DATE" => $dates["TWO_DAY"],
					                "UF_ITEM" => $item["PROPERTY_SECID_VALUE"],
					            ],
					            "select" => [
					                "UF_CLOSE"
					            ],
					        ])->fetch();
							  $actualLastprice = $resultNowPrice["UF_CLOSE"];//Берем цену за два дня назад
						   } else {
							  $actualLastprice = $qDay; //Берем текущую цену lastprice
						   }
						//Записываем однодневный прирост
						$oneDayDropIncrease = round(((floatval($arLastpriceBase[$item["PROPERTY_SECID_VALUE"]]) / floatval($actualLastprice) - 1) * 100), 2);
						if(is_infinite($oneDayDropIncrease) || is_nan($oneDayDropIncrease)){
							$oneDayDropIncrease = 0;
						}
						CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $oneDayDropIncrease, "ONE_DAY_DROP_INCREASE");
					  }

            }

            //капитализация
            CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], round(($item["PROPERTY_LASTPRICE_VALUE"] * $item["PROPERTY_SECURITIES_QUANTITY_VALUE"]), 2), "ISSUECAPITALIZATION");

            //недооценка
            $actionItem = $actionsObj->getItem($item["CODE"]);
            $understimation = null;
            $year = date("Y");
            $month = date("n");
            $yearQuarter = ceil($month / 3);


            if ($actionItem["DYNAM"]["PE"] && $item["PROPERTY_SECTOR_VALUE"]) {
                $understimation = $sectorsData[$item["PROPERTY_SECTOR_VALUE"]] - $actionItem["DYNAM"]["PE"];

                CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $understimation, "UNDERSTIMATION");
            }

            if (!$item['PROPERTY_SP500_VALUE'] || (!$item['PROPERTY_UNDERSTIMATION_VALUE'] && !$understimation)) {
                    CIBlockElement::SetPropertyValues(
                        $item["ID"],
                        $item["IBLOCK_ID"],
                        0,
                        "TARGET"
                    );
                continue;
            }

            //TARGET
            if($actionItem["PERIODS"]) {
                $periods = array_values($actionItem["PERIODS"]);

                if ($periods[0] && $periods[0]['Прибыль за год (скользящая)']) {
                    $lastProfit = $periods[0]['Прибыль за год (скользящая)'];
                    $firstProfit = $actionItem["PERIODS"]["$yearQuarter-" . ($year-3) . "-KVARTAL"]["Прибыль за год (скользящая)"]
                        ?: $actionItem["PERIODS"]["$yearQuarter-" . ($year-2) . "-KVARTAL"]["Прибыль за год (скользящая)"] ?: "";

                    if(!$firstProfit) {
                        continue;
                    }

                    $midProfit = ((($lastProfit / $firstProfit) - 1) / 3) * 100;

                    $understimation = $understimation ?: $item['PROPERTY_UNDERSTIMATION_VALUE'];
						  $calcTarget = $understimation + $midProfit;
						  //Запишем расчетный таргет для информации, если он будет заменен для основного поля ручным.
						  CIBlockElement::SetPropertyValues(
                        $item["ID"],
                        $item["IBLOCK_ID"],
                        round($calcTarget, 2),
                        "TARGET_CALCULATE"
                    );
						  if(floatval($item['PROPERTY_TARGET_CUSTOM_VALUE'])>0){
							$calcTarget = floatval($item['PROPERTY_TARGET_CUSTOM_VALUE']);
						  }
                    CIBlockElement::SetPropertyValues(
                        $item["ID"],
                        $item["IBLOCK_ID"],
                        round($calcTarget, 2),
                        "TARGET"
                    );
                }
            }
        }
    }
}

//средний PE секторов
function midSectorPE($ActionsUsa) {
    $ibID = 55;
    $arFilter = ["IBLOCK_ID" => $ibID];
    $result = [];
    $actionsObj = $ActionsUsa;
    //$actionsObj = new ActionsUsa();
    $caps = [];

    $res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "IBLOCK_ID", "CODE", "NAME", "PROPERTY_ISSUECAPITALIZATION", "PROPERTY_SECID", "PROPERTY_SECTOR", "PROPERTY_EMITENT_ID", "PROPERTY_SP500"]);

    while ($item = $res->fetch()) {
        if(!$item['PROPERTY_SP500_VALUE']) {
            continue;
        }

        if ($item['PROPERTY_SECTOR_VALUE'] && $item['PROPERTY_EMITENT_ID_VALUE'] && $item['PROPERTY_ISSUECAPITALIZATION_VALUE']) {
            $actionItem = $actionsObj->getItem($item["CODE"]);

            if($actionItem["PERIODS"]) {
                $periods = array_values($actionItem["PERIODS"]);
                if ($periods[0] && $periods[0]['Прибыль за год (скользящая)']) {
                    $result[$item['PROPERTY_SECTOR_VALUE']][$item['PROPERTY_EMITENT_ID_VALUE']] = $periods[0]['Прибыль за год (скользящая)'];
                    $caps[$item['PROPERTY_SECTOR_VALUE']][$item['PROPERTY_EMITENT_ID_VALUE']] += $item['PROPERTY_ISSUECAPITALIZATION_VALUE'];
                }
            }
        }
    }

    foreach ($result as $sector => $ids) {
        $sumProfit = 0;
        $sumCaps = 0;

        foreach ($ids as $compID => $profit) {
            $sumProfit += $profit;

            $sumCaps += $caps[$sector][$compID];
        }

        $ibID = 58;
        $arFilter = ["IBLOCK_ID" => $ibID, "NAME" => $sector];
        $res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "IBLOCK_ID", "NAME"]);

        while ($item = $res->fetch()) {
            CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], round($sumCaps / $sumProfit/1000000, 2), "MID_PE");
        }
    }
}
