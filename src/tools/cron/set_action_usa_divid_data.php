<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	use Bitrix\Highloadblock as HL;
	$start = microtime(true);
	define("NO_KEEP_STATISTIC", true);
	define("NOT_CHECK_PERMISSIONS",true);
	define('CHK_EVENT', true);

	$_SERVER["SERVER_NAME"]="fin-plan.org";

	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_tools.php" );
    Global $APPLICATION, $DB;
	$fptTools = new fptTools();
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("highloadblock");

	//Мониторинг крон
	require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
	$cCronTools = new fptСrontools;
	$cronTaskId = 277155;
	$cCronTools->changeStartTime($cronTaskId);
	//Мониторинг крон

	//OblActionUSADividData
	$hlblock   = HL\HighloadBlockTable::getById(32)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	//исторические данные для динамики акций    SpbActionsData
/*	$hlblock   = HL\HighloadBlockTable::getById(29)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$spbActionsData = $entity->getDataClass();*/

	//получение страниц акций США
	$arFilter = Array("IBLOCK_ID"=>56);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, ["ID", "CODE", "IBLOCK_ID", "PROPERTY_DIVIDENDS"]);

   //Получение массива акций США с ключом по SECID
	 $arActionsUSA = array();
    $arFilter = Array("IBLOCK_ID"=>55);
    $res2 = CIBlockElement::GetList(array(), $arFilter, false, false, ["ID", "CODE", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_LASTPRICE", "PROPERTY_EMITENT_ID"]);
    while($elem2 = $res2->GetNext()){
    	$arActionsUSA[$elem2["CODE"]] = $elem2;
    }
	 unset($res2, $elem2);

   //Получение массива страниц компаний США с ключом по тикеру - PROPERTY_CODE
	 $arEmitentsUSA_DivCurrency = array();
    $arFilterEmitents = Array("IBLOCK_ID"=>44);
    $resEmitents = CIBlockElement::GetList(array(), $arFilterEmitents, false, false, ["ID", "CODE", "IBLOCK_ID", "PROPERTY_CODE", "PROPERTY_CURRENCY_DIVIDENDS"]);
    while($elemEmitents = $resEmitents->GetNext()){
    	$arEmitentsUSA_DivCurrency[$elemEmitents["PROPERTY_CODE_VALUE"]] = $elemEmitents["PROPERTY_CURRENCY_DIVIDENDS_VALUE"];
    }
	 unset($elemEmitents, $resEmitents, $arFilterEmitents);

while($elem = $res->GetNext()){

    if(array_key_exists($elem["CODE"],$arActionsUSA)){
		  $elem2 = $arActionsUSA[$elem["CODE"]];
    } else {
        continue;
    }

	 $secid = $elem2["PROPERTY_SECID_VALUE"]; //Тикер акции

    $data = array();
    if(!empty($elem["~PROPERTY_DIVIDENDS_VALUE"]["TEXT"])){

        $tmp = unserialize($elem["~PROPERTY_DIVIDENDS_VALUE"]["TEXT"]);
		  //Получаем код валюты дивидендов
		  $currencyCode = '';
		  if(array_key_exists($secid,$arEmitentsUSA_DivCurrency)){
			 $currencyCode = $arEmitentsUSA_DivCurrency[$secid];
		  }

        foreach($tmp as $date=>$value){

        		$date = preg_replace('/[\x00-\x1F\x7F-\xA0\xAD]/u', '', $date);
		      $date = trim($date);
		      $date = str_replace(" ","",$date);
            $dt = new DateTime($date);



            if($dt->getTimestamp()<$APPLICATION->minGraphDate){
                continue;
            }



				//Смещаем дату назад до рабочего дня, если она попадает на выходные
				//делаем это на отдельном объекте, только для получения цены закрытия
				$dt_filter = new DateTime($dt->format("d.m.Y"));
				if ($dt_filter->format("N") == 7) {
					$dt_filter->modify("-2 days");
				} elseif ($dt_filter->format("N") == 6) {
					$dt_filter->modify("-1 days");
				}

				if($dt<new DateTime()){ //Если дата дивиденда раньше текущей даты (старые дивы)

				$dt_t2 = new DateTime($date);
            $dt_t2->modify("-2 days");
            if ($dt_t2->format("N") == 7) {
                $dt_t2->modify("-2 days");
            } elseif ($dt_t2->format("N") == 6) {
                $dt_t2->modify("-1 days");
            }

				while(isUsaWeekendDay($dt_t2->format('d.m.Y'))){
					$dt_t2->modify("-1 days");
				}

/*				$close = $spbActionsData::getList([
                "filter" => [
                    "UF_DATE" => $dt_t2->format("d.m.Y"),
                    "UF_ITEM" => $elem2["PROPERTY_SECID_VALUE"],
                ],
                "select" => [
                    "UF_CLOSE"
                ],
            ])->fetch();*/
            
            $sql = "SELECT `UF_CLOSE` FROM `hl_polygon_actions_data` WHERE `UF_DATE` = '".$dt_t2->format("Y-m-d")."' AND `UF_ITEM` = '".$elem2["PROPERTY_SECID_VALUE"]."'";
            $res = $DB->Query($sql);
            if($row = $res->fetch()){
                $close = $row;
            } else {
                continue;
            }
            if (!$close['UF_CLOSE']) {
                continue;
            }

            $price = round($close['UF_CLOSE'], 2);

            $tmp2 = array(
                "Дата закрытия реестра" => $dt->format("d.m.Y"),
                "Дивиденды" => $value,
                "Цена на дату закрытия" => $price,
					 "Дата закрытия реестра (T-2)" => $dt_t2->format("d.m.Y"),
            );
				if(floatval($tmp2["Цена на дату закрытия"])>0){
					if(!empty($currencyCode)){
						 $t = trim(str_replace($currencyCode, "", $tmp2["Дивиденды"]));
						 $roubles = round($t * getCBPrice($currencyCode, $dt->format("d/m/Y")), 2);

	                $tmp2["Дивиденды"] .= " (" . $roubles . " руб.)";
	                $tmp2["Дивиденды, в %"] = round($t / $tmp2["Цена на дату закрытия"] * 100, 2);
	               // $tmp2["Дивиденды, в %"] = round($roubles / $tmp2["Цена на дату закрытия"] * 100, 2);
	            } else {
	                $tmp2["Дивиденды, в %"] = round($tmp2["Дивиденды"] / $tmp2["Цена на дату закрытия"] * 100, 2);
	            }

				}


				unset($dt, $dt_t2);
            $data[] = $tmp2;
            } else { //Иначе если дата дивиденда позже текущей даты (будущие дивы)

				  $tmp2 = array(
                "Дата закрытия реестра" => $dt->format("d.m.Y"),
                "Дивиденды" => $value,
                "Цена на дату закрытия" => floatval($elem2["PROPERTY_LASTPRICE_VALUE"]),
            );
				if(floatval($tmp2["Цена на дату закрытия"])>0){
					if(!empty($currencyCode)){
						 $t = trim(str_replace($currencyCode, "", $tmp2["Дивиденды"]));
						 $roubles = round($t * getCBPrice($currencyCode, $dt->format("d/m/Y")), 2);
	/*					 if($elem["CODE"]=="US6687711084")
						   CLogger::USA_divid2(print_r($roubles, true));*/
	                $tmp2["Дивиденды"] .= " (" . $roubles . " руб.)";
	                $tmp2["Дивиденды, в %"] = round($t / $tmp2["Цена на дату закрытия"] * 100, 2);
	            } else {
	                $tmp2["Дивиденды, в %"] = round($tmp2["Дивиденды"] / $tmp2["Цена на дату закрытия"] * 100, 2);
	            }
					}

            $dt->modify("-2 days");
            if ($dt->format("N") == 7) {
                $dt->modify("-2 days");
            } elseif ($dt->format("N") == 6) {
                $dt->modify("-1 days");
            }
            $tmp2["Дата закрытия реестра (T-2)"] = $dt->format("d.m.Y");

				unset($dt);
            $data[] = $tmp2;

            }//if($dt<new DateTime())
        }


    }



    //запись / обновление
    $arItem = array(
        "UF_ITEM" => $elem["CODE"],
        "UF_DATA" => json_encode($data),
    );

    $resHL = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $elem["CODE"],
        ),
        "select" => array(
            "ID"
        ),
    )
	 );

    $result = [
        "added" => 0,
        "updated" => 0,
    ];


    if($elemHL = $resHL->fetch()){
        $entityClass::update($elemHL["ID"], $arItem);
    } else {
        $entityClass::add($arItem);
    }
	  try {
	 $iblock_action = $arActionsUSA[$elem["CODE"]];

  	 $fptTools->updateYearDividends($iblock_action["ID"], $iblock_action["IBLOCK_ID"], $elem["CODE"], $iblock_action["PROPERTY_LASTPRICE_VALUE"]);
	} catch (Exception $e) {
        $err = $e->getMessage();
		  echo "<pre  style='color:black; font-size:11px;'>";
		     print_r($err);
		     echo "</pre>";
    }

}
unset($fptTools);
$finish = microtime(true);
$delta = $finish - $start;
echo $delta.'сек.';

	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cache->clean("actions_usa_data"); //Убиваем кеш американских акций, что бы пересоздать показатели в динамических данных

	if(floatval($delta)==0){
	  mail("alphaprogrammer@gmail.com","set_action_usa_divid_data.php не отработал, время 0 сек. ",round($delta,2));
	}
	//  mail("alphaprogrammer@gmail.com","divs ",round($delta,2));
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));