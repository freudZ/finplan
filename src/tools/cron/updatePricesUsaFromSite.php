<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Данный файл обновляет цены всех акций США заведенные не вручную
	Для указания диапазона дат нужно изменить даты в ф-ции updateActionUsa()

 */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

//$sleep = 1;//in seconds
$sleep = 0;//in seconds

$start = microtime(true);

$addNewPrices = true;  //Добавлят не существующие цены

$laspriceDate = '';
$laspriceDate = '19.07.2021';//Если требуется обновлять цены в инфоблоке акций США то указываем дату за которую цены закрытия считать последними

$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_SECID", "PROPERTY_SECURITIES_QUANTITY");
//$arFilter = Array("IBLOCK_ID"=>IntVal(55), "=PROPERTY_SECID"=>"AAPL");   //Для обновления и добавления по тикеру конкретной компании
$arFilter = Array("IBLOCK_ID"=>IntVal(55)); //Для обновления и добавления по всем компаниям

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){
 if(!empty($ob["PROPERTY_SECID_VALUE"])){
 	//echo 'updated '.$ob["PROPERTY_SECID_VALUE"];
  	echo 'updated '.$ob["PROPERTY_SECID_VALUE"].' > '.updateActionUsa($ob["PROPERTY_SECID_VALUE"], $laspriceDate, $ob["ID"], $addNewPrices, $ob["PROPERTY_SECURITIES_QUANTITY_VALUE"]);
	echo PHP_EOL;
	if($sleep>0)
	  sleep($sleep);
 }
}





function updateActionUsa($ticker, $laspriceDate='', $IblockActionId=0, $addNewPrices=false, $itemQuantity){
   $dateFrom = new dateTime('19.07.2021 00:00:00');
	$postFrom = clone $dateFrom;
	$postTo = new dateTime('19.07.2021 23:59:59');

			if($postFrom<$dateFrom){
			 $postFrom = $dateFrom;
			}

	$url         = 'https://investcab.ru/api/chistory?symbol='.$ticker.'&resolution=D&from='.$postFrom->getTimeStamp().'&to='.$postTo->getTimeStamp();
  //	echo $url.PHP_EOL;
	//$url         = 'https://investcab.ru/api/chistory/';
	$client      = new \GuzzleHttp\Client();
	$syncLogFile = $_SERVER["DOCUMENT_ROOT"] . "/log/priceUsaUpdater_log.txt";
		$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: updateActionUsa; url: ' . $url;
		file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);
	try {
	   $res     = $client->request("GET", $url);
	   //$res     = $client->get("GET", $url, ['query' => ['symbol' => 'A', 'resolution' => 'D', 'from'=>'1568624480', 'to'=>'1599728540']]);
	   //$res     = $client->get("GET", $url, ['query' => ['symbol' => 'A']]);
		//$res     = $client->request("GET", $url, ['query' => ['symbol=A&resolutionD&from=1568624480&to=1599728540'], 'http_errors' => true]);
		$strJson = array();
		$strJson = json_decode($res->getBody()->getContents(), true);
		$strJson = json_decode($strJson, true);

		 $arSpbPrice = array();
		foreach($strJson["t"] as $k=>$v){
			$jDate =date ('d.m.Y', $v);
		  $strJson["t"][$k] = $jDate;
		  $arSpbPrice[$jDate] = array("UF_OPEN" => $strJson["o"][$k], "UF_CLOSE"=>$strJson["c"][$k], "UF_HIGH"=>$strJson["h"][$k], "UF_LOW"=>$strJson["l"][$k]);
		}

			$hlblock   = HL\HighloadBlockTable::getById(29)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

			$arFilter = array("=UF_ITEM"=>$ticker, ">=UF_DATE"=>$postFrom->format('d.m.Y'));
			if($postTo<(new DateTime())){
				$arFilter["<=UF_DATE"] = $postTo->format('d.m.Y');
			}

			$resHL = $entityClass::getList(array(
				"select" => array(
					"*",
				),
				"filter" => $arFilter,
				"order" => array("UF_DATE"=>"ASC"),
			));

		$arDBPrice = array();
	  //	$arDBPrice = $resHL->fetchAll();
		while($row = $resHL->fetch()){
		   $arDBPrice[$row["UF_DATE"]->format('d.m.Y')] = array("ID"=> $row["ID"] ,"UF_MANUAL" => $row["UF_MANUAL"], "UF_OPEN" => $row["UF_OPEN"], "UF_CLOSE" => $row["UF_CLOSE"], "UF_HIGH"=>$row["UF_HIGH"], "UF_LOW" => $row["UF_LOW"]);
		}

		$dtCycle = clone $postFrom;
		while($dtCycle<=$postTo){
			if(!in_array($dtCycle->format('d.m.Y'), $arDBPrice) && !isUsaWeekendDay($dtCycle->format('d.m.Y'))){
		   $arDBPrice[$dtCycle->format('d.m.Y')] = array();
			}
		  $dtCycle->modify('+1 days');
		}



	 	CLogger::updateActionUsa('dt='.($addNewPrices?'Y':'N').' ticker='.$ticker.' spb='.print_r($arSpbPrice, true) );
		foreach($arDBPrice as $date=>$val){
		  //	if(empty($val["UF_MANUAL"])){
				$arNewPrice = array();
				if(array_key_exists($date, $arSpbPrice) && count($val)>0){
					echo $date." date exist".PHP_EOL;
				  $arNewPrice = array(
					 "UF_OPEN"=>$arSpbPrice[$date]["UF_OPEN"],
					 "UF_CLOSE"=>$arSpbPrice[$date]["UF_CLOSE"],
					 "UF_HIGH"=>$arSpbPrice[$date]["UF_HIGH"],
					 "UF_LOW"=>$arSpbPrice[$date]["UF_LOW"],
				  );
				}
				elseif(count($val)==0 && $addNewPrices==true) {  //Этот кусок кода не срабатывает на добавление иногда, когда не находится ближайшая дата после выходных
				  $arNewPrice = array(
				    "UF_DATE"=>$date,
					 "UF_ITEM"=>$ticker,
					 "UF_OPEN"=>$arSpbPrice[$date]["UF_OPEN"],
					 "UF_CLOSE"=>$arSpbPrice[$date]["UF_CLOSE"],
					 "UF_HIGH"=>$arSpbPrice[$date]["UF_HIGH"],
					 "UF_LOW"=>$arSpbPrice[$date]["UF_LOW"],
				  );
				  if(count($arSpbPrice)>0){
				  $entityClass::add($arNewPrice);
				  $arDBPrice[$date] = array("ID"=> 0 ,"UF_MANUAL" => '', "UF_OPEN" => $arSpbPrice[$date]["UF_OPEN"], "UF_CLOSE" => $arSpbPrice[$date]["UF_CLOSE"], "UF_HIGH"=>$arSpbPrice[$date]["UF_HIGH"], "UF_LOW" => $arSpbPrice[$date]["UF_LOW"]);
				
					if($date==$laspriceDate){  //Если требуется обновлять цены и капитализацию в инфоблоке акций США
					  CIBlockElement::SetPropertyValues($IblockActionId, 55, str_replace(",", ".", $arNewPrice['UF_CLOSE']), "LASTPRICE");
					  CIBlockElement::SetPropertyValues($IblockActionId, 55, round((str_replace(",", ".", $arNewPrice['UF_CLOSE']) * $itemQuantity), 2), "ISSUECAPITALIZATION");
					}
					echo $date." date not exist, added".PHP_EOL;
					} else {
						echo $date." date not added, empty array".PHP_EOL;
					}

				}
				if(count($arNewPrice)>0 && array_key_exists($date, $arSpbPrice) && count($val)>0){   //Если обновляем цену

				  $entityClass::update($val["ID"], $arNewPrice);
					if($date==$laspriceDate){  //Если требуется обновлять цены в инфоблоке акций США
					  CIBlockElement::SetPropertyValues($IblockActionId, 55, str_replace(",", ".", $arNewPrice['UF_CLOSE']), "LASTPRICE");
					}

				}


			//}
		}
					$date =  $postTo->format('d.m.Y');
			if($addNewPrices==true && !array_key_exists($date, $arDBPrice)) {
				  $arNewPrice = array(
				    "UF_DATE"=>$date,
					 "UF_ITEM"=>$ticker,
					 "UF_OPEN"=>$arSpbPrice[$date]["UF_OPEN"],
					 "UF_CLOSE"=>$arSpbPrice[$date]["UF_CLOSE"],
					 "UF_HIGH"=>$arSpbPrice[$date]["UF_HIGH"],
					 "UF_LOW"=>$arSpbPrice[$date]["UF_LOW"],
				  );
				  if(count($arSpbPrice)>0){
				  $entityClass::add($arNewPrice);
				  $arDBPrice[$date] = array("ID"=> 0 ,"UF_MANUAL" => '', "UF_OPEN" => $arSpbPrice[$date]["UF_OPEN"], "UF_CLOSE" => $arSpbPrice[$date]["UF_CLOSE"], "UF_HIGH"=>$arSpbPrice[$date]["UF_HIGH"], "UF_LOW" => $arSpbPrice[$date]["UF_LOW"]);
					if($date==$laspriceDate){  //Если требуется обновлять цены в инфоблоке акций США
					  CIBlockElement::SetPropertyValues($IblockActionId, 55, str_replace(",", ".", $arNewPrice['UF_CLOSE']), "LASTPRICE");
					}
					echo $date." date not exist, added".PHP_EOL;
					} else {
						echo $date." date not added, empty array".PHP_EOL;
					}
			 }
	  return 'ok';
	} catch (RequestException $e) {

		$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: updateActionUsa; url: ' . $dataUrl . ' exception: ' . $e->getRequest();
		file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);

		if ($e->hasResponse()) {
			$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: updateActionUsa; url: ' . $dataUrl . ' exception: ' . $e->getRequest();
			file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);
		}
	}
}


function clearSpbexGraph(){
/*	$graphCacheFolder = $_SERVER["DOCUMENT_ROOT"]."/bitrix/cache/spbex_graph/";
if (is_dir($graphCacheFolder)) {
    rmdir($graphCacheFolder);
}*/
if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   $GLOBALS['CACHE_MANAGER']->ClearByTag('usa_graphdata');
}

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean('actions_usa_data');
clearSpbexGraph();  //Кэш графиков цен акций США очищен



$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";