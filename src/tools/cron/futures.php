<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
//Фьючерсы и опционы  добавление новых и различные вычисления
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
use Bitrix\Highloadblock as HL;

$ibID = 70;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
\Bitrix\Main\Loader::includeModule("iblock");

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 365046;
$cCronTools->changeStartTime($cronTaskId);

//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/indexes_cron_log.txt");
$start = microtime(true);

//Список режимов торгов со ссылками для запросов
$arBoardsId = array(
 "RFUD"=>"https://iss.moex.com/iss/engines/futures/markets/forts/boards/RFUD/securities.json?iss.only=securities", //ФОРТС
 "FIQS"=>"https://iss.moex.com/iss/engines/futures/markets/fortsiqs/boards/FIQS/securities.json?iss.only=securities", //Фьючерсы IQS
 );

//Получим из инфоблока акций все акции, их ID и Привязанных эмитентов для добавления в свойства фьючерсов
$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_EMITENT_ID", "PROPERTY_SECID");
$arFilter = Array("IBLOCK_ID"=>IntVal(32), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arIblockActionsBySECID = array();
while($ob = $res->Fetch()){
 $arIblockActionsBySECID[$ob["PROPERTY_SECID_VALUE"]] = array("ID"=>$ob["ID"], "EMITENT_ID"=>$ob["PROPERTY_EMITENT_ID_VALUE"]);
}

//Получим из инфоблока все загруженные фьючерсы
$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_BOARDID", "PROPERTY_SECID");
$arFilter = Array("IBLOCK_ID"=>IntVal($ibID), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arIblockFeaturesByCode = array();
while($ob = $res->Fetch()){
 $arIblockFeaturesByCode[$ob["CODE"]] = $ob;
}

//получаем списки существующих фьючерсов
foreach($arBoardsId as $boardId=>$link){
  $data = ConnectMoex($link);
  if(count($data['securities']['data'])>0){
	   //Определяем соответствие номеров колонок свойствам инфоблока
	   $arColumnsData = array();
		$properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ibID));
		while ($prop = $properties->GetNext())
		{
			if(in_array($prop["CODE"], $data["securities"]["columns"])){
				$arColumnsData["PROPS"][array_search($prop["CODE"], $data["securities"]["columns"])] = $prop["CODE"];
			}
		}

		$secnameColumn = array_search("SECNAME", $data["securities"]["columns"]);
		$boardIdColumn = array_search("BOARDID", $data["securities"]["columns"]);

	 foreach($data['securities']['data'] as $indexRow){

		if(!array_key_exists($indexRow[0],$arIblockFeaturesByCode)){ //если такого индекса еще нет - добавим в инфоблок и добавим ключ в массив индексов из инфоблока, для последующего обхода при получении истории

			$el = new CIBlockElement;

			$PROP = array();
			foreach($arColumnsData["PROPS"] as $num=>$code){
				if($code=="ASSETCODE") { //Если есть привязка к активу находим по secid этого актива его ID и если есть ID эмитента
				  if(!empty($indexRow[$num])){
					 if(array_key_exists($indexRow[$num], $arIblockActionsBySECID)){
					  $PROP["ASSETCODE"] = $indexRow[$num];
					  $PROP["ASSET_ID"] = $arIblockActionsBySECID[$indexRow[$num]]["ID"];
						if(!empty($arIblockActionsBySECID[$indexRow[$num]]["EMITENT_ID"])){
						  $PROP["EMITENT_ID"] = $arIblockActionsBySECID[$indexRow[$num]]["EMITENT_ID"];
						}
					 }
				  }
				} else {
				  $PROP[$code] = $indexRow[$num];
				}

			}

			$arLoadProductArray = Array(
			 // "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
			  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
			  "IBLOCK_ID"      => $ibID,
			  "PROPERTY_VALUES"=> $PROP,
			  "NAME"           => $indexRow[$secnameColumn],
			  "CODE"           => $indexRow[0],
			  "ACTIVE"         => "Y",            // активен
			  );

			if($PRODUCT_ID = $el->Add($arLoadProductArray)){
			  //echo "New ID: ".$PRODUCT_ID;
			  $arIblockFeaturesByCode[$indexRow[0]] = array("ID"=>$PRODUCT_ID, "PROPERTY_BOARDID_VALUE"=>$indexRow[$boardIdColumn], "NAME"=>$indexRow[$secnameColumn]);
			  }
			else{
			  echo "Error: ".$el->LAST_ERROR;
			  }
		}

	 }
	 unset($data, $arColumnsData);
  } //count($data['securities']['data'])>0

} //foreach $arBoardsId

unset($arIblockFeaturesByCode, $arIblockActionsBySECID);
 global $CACHE_MANAGER;
  $CACHE_MANAGER->ClearByTag("ru_futures_candle");

$finish = microtime(true);
$delta = $finish - $start;



$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
echo $delta . ' сек.';

