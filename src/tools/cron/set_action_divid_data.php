<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . "/../..");
$DOCUMENT_ROOT              = $_SERVER["DOCUMENT_ROOT"];
use Bitrix\Highloadblock as HL;
$start = microtime(true);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);

$_SERVER["SERVER_NAME"] = "fin-plan.org";

require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

//Мониторинг крон
require $_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/classes/fpt_crontools.php";
require $_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/classes/fpt_tools.php";
$fptTools = new fptTools();
$cCronTools = new fptСrontools;
$cronTaskId = 182055;
$cCronTools->changeStartTime($cronTaskId);
//Мониторинг крон

$hlblock     = HL\HighloadBlockTable::getById(21)->fetch();
$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
$entityClass = $entity->getDataClass();

//исторические данные для динамики акций
$hlblock         = HL\HighloadBlockTable::getById(24)->fetch();
$entity          = HL\HighloadBlockTable::compileEntity($hlblock);
$moexActionsData = $entity->getDataClass();

//получение страниц акций
$arFilter = Array("IBLOCK_ID" => 33);
$res      = CIBlockElement::GetList(array(), $arFilter, false, false, ["ID", "CODE", "PROPERTY_DIVIDENDS"]);

while ($elem = $res->GetNext()) {
	$arFilter2 = Array("IBLOCK_ID" => 32, "CODE" => $elem["CODE"]);
	$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, ["ID", "IBLOCK_ID", "CODE", "PROPERTY_SECID", "PROPERTY_LEGALCLOSE", "PROPERTY_LASTPRICE"]);
	if ($elem2 = $res2->GetNext()) {
		$secId = $elem2["PROPERTY_SECID_VALUE"];
	} else {
		continue;
	}

	$data = array();
	$nowDate = new DateTime();
	if (!empty($elem["~PROPERTY_DIVIDENDS_VALUE"]["TEXT"])) {
		$tmp = unserialize($elem["~PROPERTY_DIVIDENDS_VALUE"]["TEXT"]);
		//$boardIDs = array("TQBR", "TQDE");

		foreach ($tmp as $date => $value) {
			try {
				$date = preg_replace('/[\x00-\x1F\x7F-\xA0\xAD]/u', '', $date);
				$dt   = new DateTime($date);
			} catch (Exception $e) {
				$err = $e->getMessage();
				$cCronTools->addErrorDescription($cronTaskId, $err);
				echo "<pre  style='color:black; font-size:11px;'>";
				print_r($err);
				echo "</pre>";
		   mail('alphaprogrammer@gmail.com','set_action_divid_data err1',print_r($err, true));
				continue;
			}

			if ($dt->getTimestamp() < $APPLICATION->minGraphDate) {
				continue;
			}

			//Смещаем дату назад до рабочего дня, если она попадает на выходные
			//делаем это на отдельном объекте, только для получения цены закрытия
			$dt_filter = new DateTime($dt->format("d.m.Y"));
			if ($dt_filter->format("N") == 7) {
				$dt_filter->modify("-2 days");
			} elseif ($dt_filter->format("N") == 6) {
				$dt_filter->modify("-1 days");
			}

			if ($dt < $nowDate) { //Если дата дивиденда раньше текущей даты (старые дивы)

				$dt_t2 = new DateTime($date);
				$dt_t2->modify("-2 days");
				if ($dt_t2->format("N") == 7) {
					$dt_t2->modify("-2 days");
				} elseif ($dt_t2->format("N") == 6) {
					$dt_t2->modify("-1 days");
				}

				if(isWeekEndDay($dt_t2->format("d.m.Y"))){//Если выходной или праздник то ищем ближайшую рабочую дату в направлении в прошлое
				  $finded = false;
		        while(!$finded){
		            $dt_t2->modify("-1 days");
		            if(!isWeekEndDay($dt_t2->format("d.m.Y"))){
		                $finded = true;
		            	}
		        		}
				}

				$close = $moexActionsData::getList([
					"filter" => [
						"UF_DATE" => $dt_t2->format("d.m.Y"),
						"UF_ITEM" => $elem2["PROPERTY_SECID_VALUE"],
					],
					"select" => [
						"UF_CLOSE"
					],
				])->fetch();

				if (!$close['UF_CLOSE']) {
					continue;
				}

				$price = round($close['UF_CLOSE'], 2);

				$tmp2 = array(
					"Дата закрытия реестра" => $dt->format("d.m.Y"),
					"Дивиденды" => $value,
					"Цена на дату закрытия" => $price,
					"Дата закрытия реестра (T-2)" => $dt_t2->format("d.m.Y"),
				);

/*				if($elem2["PROPERTY_SECID_VALUE"]=="TGKA"){
					define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/!div_load_log.txt");
					AddMessage2Log('$tmp2 = '.print_r($tmp2, true),'');
				}*/

				if (strpos($value, "$") !== false) {
					$t       = trim(str_replace("$", "", $tmp2["Дивиденды"]));
					$roubles = round($t * getCBPrice("USD", $dt->format("d/m/Y")), 2);

					$tmp2["Дивиденды"] .= " (" . $roubles . " руб.)";
					$tmp2["Дивиденды, в %"] = round($roubles / $tmp2["Цена на дату закрытия"] * 100, 2);
				} else {
					$tmp2["Дивиденды, в %"] = round($tmp2["Дивиденды"] / $tmp2["Цена на дату закрытия"] * 100, 2);
				}

				unset($dt, $dt_t2);
				$data[] = $tmp2;
			} else { //Иначе если дата дивиденда позже текущей даты (будущие дивы)

				$tmp2 = array(
					"Дата закрытия реестра" => $dt->format("d.m.Y"),
					"Дивиденды" => $value,
					"Цена на дату закрытия" => floatval((!empty($elem2["PROPERTY_LASTPRICE_VALUE"]) ? $elem2["PROPERTY_LASTPRICE_VALUE"] : $elem2["PROPERTY_LEGALCLOSE_VALUE"])),
				);
				if (strpos($value, "$") !== false) {
					$t       = trim(str_replace("$", "", $tmp2["Дивиденды"]));
					$roubles = round($t * getCBPrice("USD", $nowDate->format("d/m/Y")), 2);
					$tmp2["Дивиденды"] .= " (" . $roubles . " руб.)";
					$tmp2["Дивиденды, в %"] = round($roubles / $tmp2["Цена на дату закрытия"] * 100, 2);
				} else {
					$tmp2["Дивиденды, в %"] = round($tmp2["Дивиденды"] / $tmp2["Цена на дату закрытия"] * 100, 2);
				}
				$dt->modify("-2 days");
				if ($dt->format("N") == 7) {
					$dt->modify("-2 days");
				} elseif ($dt->format("N") == 6) {
					$dt->modify("-1 days");
				}
				$tmp2["Дата закрытия реестра (T-2)"] = $dt->format("d.m.Y");

				unset($dt);
				$data[] = $tmp2;
			} //if($dt<new DateTime())
		}
	}

	//запись / обновление
	$arItem = array(
		"UF_ITEM" => $elem["CODE"],
		"UF_DATA" => json_encode($data, JSON_PARTIAL_OUTPUT_ON_ERROR),
	);


	//CLogger::log_save_divs($elem["CODE"].print_r($arItem, true));


	$res22 = $entityClass::getList(array(
		"filter" => array(
			"UF_ITEM" => $elem["CODE"],
		),
		"select" => array(
			"ID"
		),
	));

	$result = [
		"added" => 0,
		"updated" => 0,
	];

	if ($elem22 = $res22->fetch()) {
		$entityClass::update($elem22["ID"], $arItem);
	} else {
		$entityClass::add($arItem);
	}

	try {
		$fptTools->updateYearDividendsRus($elem2["ID"], $elem2["IBLOCK_ID"], $elem2["CODE"], $elem2["PROPERTY_LASTPRICE_VALUE"]);
	} catch (Exception $e) {
		$err = $e->getMessage();
		$cCronTools->addErrorDescription($cronTaskId, $err);
		echo "<pre  style='color:black; font-size:11px;'>";
		print_r($err);
mail('alphaprogrammer@gmail.com','set_action_divid_data err2',print_r($err, true));
		echo "</pre>";
	}
}

$finish = microtime(true);
$delta  = $finish - $start;
echo "Время работы скрипта:" . $delta . " сек.\n";
//mail("alphaprogrammer@gmail.com", "set_action_divid_data", round($delta, 2)."<br>".print_r($err,true));
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta, 2));
