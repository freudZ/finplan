<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Получает котировки акций США от провайдера polygon */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

Global $DB, $APPLICATION;
use PolygonIO\Rest\Rest;

//       /opt/php71/bin/php -f /var/www/bitrix/data/www/fin-plan.org/tools/cron/getPolygonPrices.php

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" );
require $_SERVER["DOCUMENT_ROOT"].'/vendor/polygon-io/api/src/rest/Rest.php';

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 509416;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

$actionUsaIblockId = 55;
$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_SECID");
$arFilter = Array("IBLOCK_ID"=>$actionUsaIblockId, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
$arTickersUsa = array();
while($ob = $res->fetch()){
	//если тикер еще не добавлен и если он не входит в список дублируемых из СПБ тикеров - добавим его в массив
 if(!in_array($ob["PROPERTY_SECID_VALUE"], $arTickersUsa) && !in_array($ob["PROPERTY_SECID_VALUE"], $APPLICATION->crossTickers)){
   $arTickersUsa[$ob["ID"]] = $ob["PROPERTY_SECID_VALUE"];
 }
}
//Перевернутый массив инфоблока акций США с ключами по тикеру и значениями = ID
$arTickerToIblockId = array_flip($arTickersUsa);

unset($res, $ob, $arFilter, $arSelect);

if(count($arTickersUsa)>0){
$rest = new Rest('ZmCXrWbo2hzVHWQtmyn9UJ3rlGWE3tKL');
$date = (new DateTime())->modify('- 1 days')->format('Y-m-d');

$arExistPricesOnDate = array();
//Получаем на текущую дату $date все существующие цены для определения insert|update
$query = "SELECT `ID`, `UF_ITEM` FROM `hl_polygon_actions_data` WHERE `UF_DATE` = '$date'";
$res = $DB->Query($query);
while($row = $res->fetch()){
 $arExistPricesOnDate[$row["UF_ITEM"]] = $row["ID"];
}
unset($query, $res, $row);



//$date = '2021-07-13';
   foreach($arTickersUsa as $ticker){
		$arResult = array();
     try {
     	$arResult = $rest->stocks->dailyOpenClose->get($ticker, $date);
     } catch (Exception $e) {
     	   CLogger::getPolygonPrices($date." ".$e->getMessage());
			$arResult = array();
         //echo 'Поймано исключение: ',  $e->getMessage(), "\n";
     } finally {
       /* code */
     }

     if(count($arResult)>0 && $arResult["status"]=="OK"){
      if(!array_key_exists($ticker, $arExistPricesOnDate)){
         $query = "INSERT INTO `hl_polygon_actions_data`(`UF_ITEM`, `UF_DATE`, `UF_OPEN`, `UF_CLOSE`, `UF_HIGH`, `UF_LOW`, `UF_VOLUME`, `UF_SHOW_GRAPH`, `UF_MANUAL`) VALUES ('$ticker', '".$arResult['from']."',
         '".$arResult['open']."', '".$arResult['close']."', '".$arResult['high']."', '".$arResult['low']."', '".$arResult['volume']."', 'Y', '')";
      } else {
         $query = "UPDATE `hl_polygon_actions_data` SET `UF_OPEN`=".$arResult['open'].",`UF_CLOSE`=".$arResult['close'].",`UF_HIGH`=".$arResult['high'].",`UF_LOW`=".$arResult['low'].",`UF_VOLUME`=".$arResult['volume']." WHERE `ID` = $arExistPricesOnDate[$ticker]";
      }

      $DB->Query($query);

    if ($arResult['close']) {
        try {
            if (array_key_exists($ticker, $arTickerToIblockId)) {
                CIBlockElement::SetPropertyValues($arTickerToIblockId[$ticker], $actionUsaIblockId, str_replace(",", ".", $arResult['close']), "LASTPRICE");
            }
        } catch (Exception $e) {
            $err = $e->getMessage();
        }
    }

     }

   }

  //Сброс кеша
  try {
   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
   $cache->clean("actions_usa_data"); //Убиваем кеш американских акций, что бы пересоздать показатели в динамических данных
   $cache->clean("radar_ratings"); //Очищаем кеш рейтингов акций
	if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   $GLOBALS['CACHE_MANAGER']->ClearByTag('usa_graphdata'); //Очищаем кеш графиков котировок сша
   } catch (Exception $e) {
      $err = $e->getMessage();
   }
}//count tickers





$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";
if(count($arResults)>0){
   foreach($arResults as $k=>$v){
      echo $k." капа=".$v["CAP"]." результат: ".$v["RESULT"]."\n";
   }
}
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));

