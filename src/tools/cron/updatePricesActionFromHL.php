<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Данный файл обновляет цены всех акций РФ и ETF из HL блока на указанную дату

 */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

$start = microtime(true);

$addNewPrices = false;  //Добавлят не существующие цены

$laspriceDate = '';
$laspriceDate = '10.06.2021';//Если требуется обновлять цены в инфоблоке акций РФ то указываем дату за которую цены закрытия считать последними

$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_SECID", "PROPERTY_ETF_CURRENCY", "PROPERTY_IS_ETF_ACTIVE");
//$arFilter = Array("IBLOCK_ID"=>IntVal(55), "=PROPERTY_SECID"=>"AAPL");   //Для обновления и добавления по тикеру конкретной компании
$arFilter = Array("IBLOCK_ID"=>IntVal(32)); //Для обновления и добавления по всем компаниям

			$hlblock   = HL\HighloadBlockTable::getById(24)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();
			$resHL = $entityClass::getList(array(
				"select" => array(
					"*",
				),
				"filter" => array("=UF_DATE"=>$laspriceDate),
				"order" => array("UF_DATE"=>"ASC"),
			));

 $arHLPrices = array();
 while($hl = $resHL->fetch()){
	$arHLPrices[$hl["UF_ITEM"]] = $hl["UF_CLOSE"];
 }

$currencyRate = 1;

$currencyRateUsd = getCBPrice(strtoupper($laspriceDate), (new DateTime())->format('d.m.Y'));

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){
 if(!empty($ob["PROPERTY_SECID_VALUE"])){
 	//echo 'updated '.$ob["PROPERTY_SECID_VALUE"];
	if(!empty($arHLPrices[$ob["PROPERTY_SECID_VALUE"]])){

	 if(($ob["PROPERTY_IS_ETF_ACTIVE_VALUE"]=="Y" && $ob["PROPERTY_ETF_CURRENCY_VALUE"]!='RUB') || ($ob["PROPERTY_IS_ETF_ACTIVE_VALUE"]=="Y" && $ob["PROPERTY_ETF_CURRENCY_VALUE"]!='SUR')){
		$closePrice = round($arHLPrices[$ob["PROPERTY_SECID_VALUE"]]*$currencyRateUsd, 2);
	 } else {
		$closePrice = round($arHLPrices[$ob["PROPERTY_SECID_VALUE"]], 2);
	 }

	 	CIBlockElement::SetPropertyValues($ob["ID"], 32, str_replace(",", ".", $closePrice), "LASTPRICE");
	}

 }
}




$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";