<?
//Парсер НКД для облигаций с биржи, по утрам в 10:15
$_SERVER["DOCUMENT_ROOT"] = "/var/www/bitrix/data/www/fin-plan.org";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 251686;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

\Bitrix\Main\Loader::includeModule("iblock");
$data = ConnectMoex("https://iss.moex.com/iss/engines/stock/markets/bonds/securities.json");

$ibID = 27;

//Получаем все облигации в массив "ISIN"=>array("ID","ACCRUEDINT")
$arBonds = array();
$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_ACCRUEDINT", "PROPERTY_ISIN");
$arFilter = Array("IBLOCK_ID"=>IntVal($ibID), "=PROPERTY_HIDEN"=>false);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch()){
 $arBonds[$arFields["PROPERTY_ISIN_VALUE"]] = array("ID"=>$arFields["ID"], "ACCRUEDINT"=>$arFields["PROPERTY_ACCRUEDINT_VALUE"]);
}

//обработка
$ACCRUEDINT_col = '';
foreach ($data["securities"]["data"] as $item){

  if(empty($ACCRUEDINT_col)){ //Единоразово определяем колонку ACCRUEDINT
  	foreach($data["securities"]["columns"] as $col_idx=>$col){
  		if($col == 'ACCRUEDINT'){
		  $ACCRUEDINT_col = $col_idx;
		  break;
  		}
  	}
  }

  if(array_key_exists($item[0],$arBonds)){
	 //Если новый НКД отличается от старого - обновим его в базе
	 if(floatval($arBonds[$item[0]]["ACCRUEDINT"])!=floatval($item[$ACCRUEDINT_col])){
		//CIBlockElement::SetPropertyValues($arBonds[$item[0]]["ID"], $ibID, $item[$ACCRUEDINT_col], 'ACCRUEDINT');
		CIBlockElement::SetPropertyValuesEx($arBonds[$item[0]]["ID"], $ibID, array('ACCRUEDINT' => $item[$ACCRUEDINT_col]));
	 }

  }
}

//--------------------------------------------------

$finish = microtime(true);

$delta = $finish - $start;

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean("obligations_data");

$res = new Obligations();
$res->getTable(1);

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
echo $delta . ' сек.';