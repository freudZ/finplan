<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");


//синхронизация CRM с юзерами
$arFilter = Array("IBLOCK_ID"=>19, "PROPERTY_USER" => false, "!PROPERTY_EMAIL" => false);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, ["ID", "PROPERTY_EMAIL"]);

while($item2 = $res->GetNext()){
	$rsUser = CUser::GetByLogin($item2["PROPERTY_EMAIL_VALUE"]);
	if($arUser = $rsUser->Fetch()) {
		CIBlockElement::SetPropertyValues($item2["ID"], 19, $arUser["ID"], "USER");
	}
}

//попытки оплат
$dt = new DateTime();
$dt->modify("-20minutes");

$arFilter = Array(
    "IBLOCK_ID"=>11,
    "<=DATE_CREATE" => $dt->format("d.m.Y H:i:s"),
    ">=DATE_CREATE" => $dt->format("d.m.Y")." 00:00:00",
    "PROPERTY_PAYED" => false,
    //"ID" => 138165
);

$res = CIBlockElement::GetList(array("ID"=>"desc"), $arFilter, false, false, ["ID", "DATE_CREATE", "PROPERTY_USER", "PROPERTY_PAY_NAME"]);

while($item2 = $res->GetNext()){

	$dt = new DateTime($item2["DATE_CREATE"]);
	$text = $dt->format("d.m.Y").'`Попытка купить "'.$item2["PROPERTY_PAY_NAME_VALUE"].'"';

    //получаем запись в CRM
	$arFilter = Array("IBLOCK_ID"=>19, "PROPERTY_USER" => $item2["PROPERTY_USER_VALUE"]);
	$res2 = CIBlockElement::GetList(array(), $arFilter, false, false);
	if($ob = $res2->GetNextElement()){
		$item = $ob->GetFields();
		$item["PROPS"] = $ob->GetProperties();

		$haveDate = false;
		foreach ($item["PROPS"]["SALE"]["~VALUE"] as $val){
			if($val["TEXT"] == $text){
				$haveDate = true;
				break;
			}
		}

		if(!$haveDate) {
			$vals = [];
			foreach ($item["PROPS"]["SALE"]["PROPERTY_VALUE_ID"] as $n=>$propValId){
                $vals[$propValId] = array("TEXT" => $item["PROPS"]["SALE"]["~VALUE"][$n]["TEXT"], "TYPE"=>"text");
			}
            $vals[0] = array("TEXT"=>$text, "TYPE"=>"text");

			CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $dt->format("d.m.Y"), "NEW_CALL");
			CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $vals, "SALE");
		}
	}
}