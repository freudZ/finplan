<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Образец файла для крон с записью статистики в БД */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
@set_time_limit(0);

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;


require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 458479;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

$arErrors = array();
$arSuccess = array();


//Далее закомментирован код для получения курсов за несколько дат
/*$pause = 1; //Пауза между обработками дат
$upause = 500; //Пауза d микросекундах между обработками дат
$startDate = new DateTime('01.01.2020');
$endDate = new DateTime();

while($startDate<$endDate){
	$ret = updateRates($startDate->format('d.m.Y'));
	echo "Обработаны курсы за ".$startDate->format('d.m.Y').' USD: '.$ret.PHP_EOL;
	//sleep($pause);
	usleep($upause);
	$startDate->modify('+ 1 days');
}*/

$date = date('d.m.Y');
updateRates($date);

function updateRates($date){
//$date = date('d.m.Y');
//$date = date('13.06.2021');
$cbr = new CCurrency($date);
//$cbr->getRate('USD');
$cbr->getRateToParser();
	// $cbr->getCurrencyCodes($cbr->classDate);
	//  $cbr->setRareCurrencyValues($cbr->classDate);
$arCbRates = $cbr->currencyCodes;

//Делаем выборку в HL с курсами для определения необходимости обновлять или добавлять полученные курсы от ЦБ
    Loader::includeModule("iblock");
    Loader::includeModule("highloadblock");

    $hlBlock0 = HL\HighloadBlockTable::getById(44)->fetch();
    $entity0 = HL\HighloadBlockTable::compileEntity($hlBlock0);
    $entityDataClass0 = $entity0->getDataClass();


	$resHL0 = $entityDataClass0::getList(array(
		"select" => array(
			"*",
		),
		"filter" => array("=UF_DATE"=>(new DateTime($date))->format('d.m.Y'), "UF_CODE"=>array_keys($arCbRates)),
	));
	$arExistRates = array();
	while($row = $resHL0->fetch()){
	 $arExistRates[$row["UF_CODE"]] = $row["UF_RATE"];
	}

	foreach($arCbRates as $code=>$data){
	 	if(array_key_exists($code, $arExistRates)) continue; //не добавляем если курс на дату уже существует
	  $arFields = array(
		  			"UF_DATE" => $date,
		  			"UF_RARE" => $cbr->isRareCurrency($code),
		  			"UF_RATE_CURRENCY" => $data["CURRENCY"],
		  			"UF_CODE" => $code,
		  			"UF_NAME" => $data["NAME"],
		  			"UF_RATE" => $data["CURS"],
		  			"UF_NOMINAL" => $data["NOM"],
		  			"UF_DIR" => $data["DIR"],
	  );
	 $otvet = $entityDataClass0::add($arFields);
	  if ($otvet->isSuccess()) {
	  	$arSuccess[] = 'успешно добавлен '.$code.' id='.$otvet->getId();
	  } else {
	  	$arErrors[] = 'Ошибка: ' . implode(', ', $otvet->getErrors());
	  }
	}

//Получим базовые валюты для портфелей и добавим им курсы на новую дату
  $arBaseCurrencies = array();
  $arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_BASE","PROPERTY_SECID");
  $arFilter = Array("IBLOCK_ID"=>54, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!=PROPERTY_SECID"=>"RUB", "PROPERTY_BASE"=>"Y");
  $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
  while($ob = $res->fetch()){
	 $arBaseCurrencies[$ob["PROPERTY_SECID_VALUE"]] = $ob["ID"];
  }


  //Если найдены базовые валюты - получим для проверки их котировки из MoexCurrencyData
    $hlBlock = HL\HighloadBlockTable::getById(31)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlBlock);
    $entityDataClass = $entity->getDataClass();


	$resHL = $entityDataClass::getList(array(
		"select" => array(
			"*",
		),
		"filter" => array("=UF_DATE"=>(new DateTime($date))->format('d.m.Y'), "UF_ITEM"=>array_keys($arBaseCurrencies)),
	));
	$arMoexCurrencyDataRates = array();
	while($row = $resHL->fetch()){
	 $arMoexCurrencyDataRates[$row["UF_ITEM"]] = $row["UF_CLOSE"];
	}

	foreach($arBaseCurrencies as $baseCode=>$baseId){
  	  if(array_key_exists($baseCode, $arMoexCurrencyDataRates)) continue; //не добавляем и не обновляем если котировки на дату уже существуют

	  if($date==date('d.m.Y')){  //Если дата обновления равна текущей - то обновим LASTPRICE для базовых валют в инфоблоке
		  CIBlockElement::SetPropertyValuesEx($baseId, 54, array("LASTPRICE" => $arCbRates[$baseCode]["CURS"]));
		  CIBlockElement::SetPropertyValuesEx($baseId, 54, array("QUOTATIONS_NOW" => $arCbRates[$baseCode]["CURS"]));
  	  }
	  $data = $arCbRates[$baseCode];
	  $arFields = array(
		  			"UF_DATE" => $date,
		  			"UF_ITEM" => $baseCode,
		  			"UF_OPEN" => $data["CURS"],
		  			"UF_CLOSE" => $data["CURS"],
		  			"UF_HIGH" => $data["CURS"],
		  			"UF_LOW" => $data["CURS"],
		  			"UF_CLOSE_USD" => $data["CURS"]/$arCbRates["USD"]["CURS"],
		  			"UF_CLOSE_EUR" => $data["CURS"]/$arCbRates["EUR"]["CURS"],
	  );
	 $otvet = $entityDataClass::add($arFields);
	  if ($otvet->isSuccess()) {
	  	$arSuccess[] = 'успешно добавлен '.$baseCode.' id='.$otvet->getId();
	  } else {
	  	$arErrors[] = 'Ошибка: ' . implode(', ', $otvet->getErrors());
	  }

	}
	 CIBlock::clearIblockTagCache(54);
	//Сброс кеша валют
	if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   $GLOBALS['CACHE_MANAGER']->ClearByTag('valute');

if(count($arErrors)>0){
   print_r($arErrors, true);
}
unset($cbr);
return $arCbRates["USD"]["CURS"];

}

$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";
if(count($arResults)>0){
	foreach($arResults as $k=>$v){
		echo $k." капа=".$v["CAP"]." результат: ".$v["RESULT"]."\n";
	}
}
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
