<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Пересчет кеша событий радара */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 322838;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean("radar_events");

$CREvents = new CRadarEvents();
unset($CREvents);

$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
