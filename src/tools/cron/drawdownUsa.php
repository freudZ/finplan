<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Расчет просада по акциям США */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

@set_time_limit(0);

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;

$cCronTools = new fptСrontools;
$cronTaskId = 504760;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);


$year = date("Y");
$hlblock = HL\HighloadBlockTable::getById(29)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$res = $entity_data_class::getList([
    "filter" => [
        //"UF_ITEM" => "AMZN",
        ">=UF_DATE" => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P5Y'))->format("d.m.Y"),
    ],
    "select" => [
        "UF_ITEM",
        "UF_CLOSE",
    ],
]);

while ($item = $res->fetch()) {
    $result[$item["UF_ITEM"]][] = $item["UF_CLOSE"];
}

//средняя сумма
$action = [];
foreach ($result as $k => $closeArr) {
    $action[$k]["averege"] = round((array_sum($closeArr) / count($closeArr)), 2);
}
//отклонение
$deviations = [];
foreach ($result as $k => $closeArr) {
    foreach ($closeArr as $close) {
        $deviations[$k][] = $close - $action[$k]['averege'];
    }

    //сумма отклонений
    $action[$k]['deviation'] = round(array_sum($deviations[$k]),2);
}

//квадраты отклонений
foreach ($deviations as $k => $deviationsArr) {
    $squared = [];
    foreach ($deviationsArr as $deviation) {
        $squared[$k][] = $deviation * $deviation;
    }

    //сумма квадратов отклонений
    $action[$k]['sqrDeviation'] = round((array_sum($squared[$k]) / count($squared[$k])),2);
    $action[$k]['sqrt'] = round(sqrt($action[$k]['sqrDeviation']), 2);
}

$ibID = 55;
$arFilter = ["IBLOCK_ID" => $ibID];
$res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "IBLOCK_ID", "PROPERTY_LASTPRICE", "PROPERTY_SECID"]);

while ($item = $res->fetch()) {
    if($item["PROPERTY_LASTPRICE_VALUE"] && $item["PROPERTY_LASTPRICE_VALUE"] > 0 && isset($action[$item["PROPERTY_SECID_VALUE"]]) && $action[$item["PROPERTY_SECID_VALUE"]]['sqrt'] > 0) {
        CIBlockElement::SetPropertyValues(
            $item["ID"], $item["IBLOCK_ID"],
            round(($action[$item["PROPERTY_SECID_VALUE"]]['sqrt'] / $item["PROPERTY_LASTPRICE_VALUE"]) * 100, 2),
            "DRAWDOWN"
        );
    }
}


$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";
if(count($arResults)>0){
	foreach($arResults as $k=>$v){
		echo $k." капа=".$v["CAP"]." результат: ".$v["RESULT"]."\n";
	}
}
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
