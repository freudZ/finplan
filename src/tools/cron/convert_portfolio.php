<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Конвертер портфелей для запуска из командной строки */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

$start = microtime(true);

@set_time_limit(0);
CModule::IncludeModule("iblock");

$CPortfolio = new CPortfolio();

function convertPortfolio($pid, $CPortfolio){
 Global $USER;
 $arPortfolio = $CPortfolio->getPortfolio($pid);
 //$arDeals = $CPortfolio->getPortfolioActivesList1($pid, false, false);
		if(!empty($arPortfolio["PROPERTIES"]["VERSION"]["VALUE"])) return true;
 			$hlblockHist_id        = 27; // портфели история
			$hlblockHist           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblockHist_id)->fetch();
			$entityHist            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblockHist);
			$entityHist_data_class = $entityHist->getDataClass();
			//Получаем сделки портфеля
			$rsData = $entityHist_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC'),
				'select' => array('*'),
				'filter' => array('UF_PORTFOLIO' => $pid)
			));
			$arDeals      = array();
			$arDeals      = $rsData->fetchAll();



			$pidCopy = 0;
			$el                      = new CIBlockElement;
			$PROP                    = array();
			foreach($arPortfolio["PROPERTIES"] as $propCode=>$propValue){
				$PROP[$propCode] = $propValue["VALUE"];
			}
			$PROP["VERSION"] = '2.0';

			$convertNote = "Портфель сконвертирован в формат 2.0";
			$arLoadProductArray = Array(
				"MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
				"IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
				"IBLOCK_ID" => 52, //Портфели
				"PROPERTY_VALUES" => $PROP,
				"NAME" => $arPortfolio["NAME"],
				"PREVIEW_TEXT" => strlen($arPortfolio["PREVIEW_TEXT"])>0?$arPortfolio["PREVIEW_TEXT"].PHP_EOL.$convertNote:$convertNote,
				"ACTIVE" => "Y", // активен
			);

			if ($pidCopy = $el->Add($arLoadProductArray)) {
				$result = array("result" => "ok", "id" => $pidCopy);
				CIBlockElement::SetPropertyValuesEx($pidCopy, false, array('SUBZERO_CACHE_ON' => 276));
			} else {
				$result = array("result" => "error", "error_text" => $el->LAST_ERROR);
			}

			$CPortfolio->clearCachePortfolioCntForUser($PROP['USER']);


	//Заполняем историю заново только на основе сделок
	if($pidCopy>0){
		$arAddedDeal = array();
	foreach($arDeals as $hid=>$arDeal){
			if(count($arDeal)>0){
				$lotsize = $arDeal["UF_INLOT_CNT"];
				if(intval($lotsize)==0 && $arDeal["UF_ACTIVE_TYPE"]=='bond'){
					$lotsize = 1;
				}
				$arFields = array(
				 //"UF_DEAL_ID"=>$arDeal["ID"],
				 "UF_HISTORY_ACT"=>$CPortfolio->arHistoryActions["ACT_PLUS"],
				 "portfolioId"=>$pidCopy,
				 "cntActive"=>$arDeal["UF_LOTCNT"],
				 "lotsize"=>$lotsize,
				 "idActive"=>$arDeal["UF_ACTIVE_ID"],
				 "codeActive"=>$arDeal["UF_ACTIVE_CODE"],
				 "typeActive"=>$arDeal["UF_ACTIVE_TYPE"],
				 "nameActive"=>$arDeal["UF_ACTIVE_NAME"],
				 "urlActive"=>$arDeal["UF_ACTIVE_URL"],
				 "currencyActive"=>$arDeal["UF_CURRENCY"],
				 "currencyEmitent"=>$arDeal["UF_CURRENCY"],//Нужно дополнительно запрашивать по эмитенту
				 "priceActive"=>$arDeal["UF_LOTPRICE"],
				 "secid"=>$arDeal["UF_SECID"],
				 "nameActiveEmitent"=>$arDeal["UF_EMITENT_NAME"],
				 "urlActiveEmitent"=>$arDeal["UF_EMITENT_URL"],
				 "dateActive"=>$arDeal["UF_ADD_DATE"],
				 "price_one"=>$arDeal["UF_LOTPRICE"]/$lotsize,
				);
				if(!in_array($arDeal["UF_CURRENCY"], $CPortfolio->arRoubleCodes)){
					$priceActiveinCurrency = $CPortfolio->getHistActionsPrices($arDeal["UF_ACTIVE_TYPE"], $arDeal["UF_ADD_DATE"], $arDeal["UF_SECID"]);
					$priceCurrency = $CPortfolio->getHistActionsPrices('currency', $arDeal["UF_ADD_DATE"], $arDeal["UF_CURRENCY"]);
					$alterLastPriceCurrency = round($arDeal["UF_LOTPRICE"]/$priceCurrency["price"], 4);
					$arFields["lastpriceActiveValute"] = $priceActiveinCurrency["price_valute"]==$alterLastPriceCurrency?$priceActiveinCurrency["price_valute"]:$alterLastPriceCurrency;
				}

			  //$CPortfolio->addToHistory("PLUS", $arFields);
			  $CPortfolio->addPortfolioActive($pidCopy, $arFields, array(), $CPortfolio->arHistoryActions["ACT_PLUS"]);
			  $arAddedDeal[] = $arDeal["UF_ACTIVE_NAME"].'; Дт= '.$arDeal["UF_ADD_DATE"].'; Кол-во= '.$arDeal["UF_LOTCNT"].'; Цена лота= '.$arDeal["UF_LOTPRICE"];
	 		}
  		}
	}

  $CPortfolio->clearPortfolioListForUserCache($arPortfolio["PROPERTIES"]["OWNER_USER"]["VALUE"]);
  return $pidCopy;
}




   $rsAll = CIBlockElement::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => 52, "PROPERTY_VERSION"=>false, ">ID"=>438469), false, array(), array("ID", "IBLOCK_ID", "PROPERTY_VERSION"));
	$allCnt = $rsAll->SelectedRowsCount();
	$cnt = 0;
	while ($arEl = $rsAll->Fetch())
	{
		$cnt++;
		if(empty($arEl["PROPERTY_VERSION_VALUE"])){
		  $convertedId = convertPortfolio($arEl["ID"], $CPortfolio);
		  CLogger::CronConvertedPortfolio($arEl["ID"].' -> '.$convertedId);
		  }
		  CLogger::CronConverterProgress();
		  CLogger::CronConverterProgress('Сконвертировано '.$cnt.' из '.$allCnt.' портфелей'.PHP_EOL.'<br>Текущий портфель: '.$arEl["ID"].' <br>Скопирован в <a href="/portfolio/portfolio_'.$convertedId.'/" target="_blank" >'.$convertedId.'</a>' );
		  $finish = microtime(true);
		  $delta = $finish - $start;
		  CLogger::CronConverterProgress('<br>Время конвертации '.round($delta/60, 3).' мин. ' );

	}
		  CLogger::CronConverterProgress();
		  $finish = microtime(true);
		  $delta = $finish - $start;
		  CLogger::CronConverterProgress('Сконвертировано '.$cnt.' из '.$allCnt.' портфелей'.PHP_EOL.'<br>Конвертация завершена!' );
		  CLogger::CronConverterProgress('<br>Время конвертации '.round($delta/60, 3).' мин. ' );




$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . round($delta/60, 3) . " мин.\n";

