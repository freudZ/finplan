<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Отправка сообщений в радар инфо */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 376991;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

$smSender = new CSmartSender();
$result = $smSender->sendEventMessagesSmarSender();

unset($smSender);

$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
