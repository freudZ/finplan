<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* заполнение НКД облигаций в истории цен */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

use Bitrix\Highloadblock as HL;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;


$start = microtime(true);

//Собираем список кодов облиг
    $hlblock = HL\HighloadBlockTable::getById(25)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

	 $res = $entity_data_class::getList([
					            "filter" => [
					                "UF_FACEVALUE" => false,
										 //"UF_ITEM" => 'RU000A0JWSQ7'
					            ],
					            "select" => [
					                "UF_ITEM",
										 "UF_DATE",
										 "ID"
					            ],
									"order" => ["UF_ITEM"=>"ASC", "UF_DATE"=>"ASC"]
					        ]);
	 while($ob = $res->fetch()){
	 $arCodes[$ob["UF_ITEM"]][(new DateTime($ob["UF_DATE"]))->format('Y-m-d')] = $ob["ID"];
}


//Обходим массив кодов
foreach($arCodes as $code=>$arDates){

$step = 0;
$datesArr = array();
$needLoop = true;

$url = "http://iss.moex.com/iss/history/engines/stock/markets/bonds/securities/".$code."/securities.json?from=2011-01-01&till=2020-12-02";


do{
	$dates = ConnectMoex($url."&start=" . $step);
	foreach ($dates["history"]["data"] as $v) {
		$datesArr[$v[1]] = array("FV"=>$v[30], "AC"=>floatval($v[10]));
	}
	$step += 100;
	$needLoop = (!$dates["history"]["data"]) ? false : true;
}while($needLoop);

//Обходим существующие записи цен и если в биржевых данных есть дата - обновляем номинал и нкд облиги
 foreach($arDates as $date=>$rowId){
	if(array_key_exists($date, $datesArr)){
		$arFields = array("UF_FACEVALUE"=>$datesArr[$date]["FV"], "UF_ACCRUEDINT"=>$datesArr[$date]["AC"]);
		$entity_data_class::update($rowId, $arFields);
		echo "upd ".$code.PHP_EOL;
	}

 }



}


$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";

