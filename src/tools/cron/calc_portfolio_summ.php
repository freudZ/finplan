<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Образец файла для крон с записью статистики в БД */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 371524;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

/* тело */

 $CPortfolio = new CPortfolio();
 CModule::IncludeModule("iblock");

 $arSelect = Array("ID", "NAME", "IBLOCK_ID");
 $arFilter = Array("IBLOCK_ID"=>IntVal(52));
 $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
 while($ob = $res->fetch()){
  $arAllPortfolio[] = $ob["ID"];
 }
 unset($res, $ob);
 //$arAllPortfolio = $CPortfolio->getPortfolioListAll();
   $cbr = new CCurrency();
	$resO  = new Obligations();
	$resA  = new Actions();
	$resE  = new ETF();
	$resAU = new ActionsUsa();
 foreach($arAllPortfolio as $k){
	$arResult = $CPortfolio->calcPortfolio($k, $cbr, $resA, $resO, $resE, $resAU);
	if(count($arResult)==2){
  	  CIBlockElement::SetPropertyValuesEx($k, 52, array("INVEST_SUMM" => $arResult["START_SUMM"]));
  	  CIBlockElement::SetPropertyValuesEx($k, 52, array("CURRENT_SUMM" => $arResult["CURR_SUMM"]));
	}
	unset($arResult);
 }
   unset($resO, $resA, $resE, $resAU);
	unset($cbr);
/* тело */

$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";
if(count($arResults)>0){
	foreach($arResults as $k=>$v){
		echo $k." капа=".$v["CAP"]." результат: ".$v["RESULT"]."\n";
	}
}
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));