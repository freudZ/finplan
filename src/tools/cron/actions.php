<? $_SERVER["DOCUMENT_ROOT"] = "/var/www/bitrix/data/www/fin-plan.org";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"] = "fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;
use Bitrix\Highloadblock as HL;


require($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/classes/fpt_crontools.php");
$cCronTools = new fptСrontools;
$cronTaskId = 201733;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

\Bitrix\Main\Loader::includeModule("iblock");

$data = ConnectMoex("https://iss.moex.com/iss/engines/stock/markets/shares/securities.json");

//Подключаем класс для работы с запросами к московской бирже
$MApi = new MoexApi();

//исторические данные для динамики акций
CModule::IncludeModule("highloadblock");
$hlblock = HL\HighloadBlockTable::getById(24)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$moexActionsData = $entity->getDataClass();

//даты для фильтра по росту
$dates = ['DAY' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1D')), 'MONTH' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1M')), 'ONE_YEAR' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1Y')), 'THREE_YEAR' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P3Y')),];

foreach ($dates as $k => $v) {
    if (isWeekEndDay($v->format("d.m.Y"))) {
        $finded = false;
        //задаем направление поиска ближайшего рабочего дня
        $countDir = '+'; // по умолч. для длинных интервалов ищем вперед
        if ($k == 'DAY') {
            $countDir = '-'; // для интервала в день ищем назад
        }
        while (!$finded) {
            $v->modify($countDir . "1 days");
            if (!isWeekEndDay($v->format("d.m.Y"))) {
                $finded = true;
            }
        }
    }
    //$dates[$k] = $v->format('Y-m-d');
    $dates[$k] = $v->format("d.m.Y");
}
/*$dataArr = [];

$step = 0;
$needLoop = true;

do{
    foreach ($dates as $k => $date) {
        $monthData = ConnectMoex("https://iss.moex.com/iss/history/engines/stock/markets/shares/securities.json?date=" . $date . "&start=" . $step);

        foreach ($monthData["history"]["data"] as $v) {

            $dataArr[$v[1]][$k] = $v[17];
        }
    }
    $step += 100;
    $needLoop = (!$monthData["history"]["data"]) ? false : true;
}while($needLoop);*/

$ibID = 32;
$ibPageID = 33;
$ibCompanyId = 29;
$arPifBoardid = array("TQPI", "TQDE");

$arCapKoeffActionPagesTmp = array();
$arCapKoeffActionPages = array();
$arSelect = array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_CAPITAL_KOEF");
$arFilter = array("IBLOCK_ID" => IntVal($ibCompanyId), "!PROPERTY_CAPITAL_KOEF" => false);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
while ($ob = $res->Fetch()) {
    $arCapKoeffActionPagesTmp[$ob["NAME"]] = $ob["PROPERTY_CAPITAL_KOEF_VALUE"];
}

$arSelect = array("ID", "NAME", "IBLOCK_ID");
$arFilter = array("IBLOCK_ID" => IntVal(26), "NAME" => array_keys($arCapKoeffActionPagesTmp));
$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
while ($ob = $res->Fetch()) {
    $arCapKoeffActionPages[$ob["ID"]] = $arCapKoeffActionPagesTmp[$ob["NAME"]];

}

unset($arCapKoeffActionPagesTmp);


$arData = array();

//Список акций с lastprice для расчета однодневного роста
$arSelectInrease = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_LASTPRICE", "PROPERTY_SECID");
$arFilterInrease = array("IBLOCK_ID" => $ibID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
$resInrease = CIBlockElement::GetList(array(), $arFilterInrease, false, false, $arSelectInrease);
$arLastpriceBase = array();
while ($ob = $resInrease->Fetch()) {
    $arLastpriceBase[$ob["PROPERTY_SECID_VALUE"]] = $ob["PROPERTY_LASTPRICE_VALUE"];

}

//соответствия колонок свойтсвам
$properties = CIBlockProperty::GetList(array("sort" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => $ibID));
while ($prop = $properties->GetNext()) {
    if (in_array($prop["CODE"], $data["securities"]["columns"])) {
        $arData["PROPS"][array_search($prop["CODE"], $data["securities"]["columns"])] = $prop["CODE"];
    }
}


$el = new CIBlockElement;

//обработка
$arHaveCodes = array();
foreach ($data["securities"]["data"] as $item) {
    $PROP = array();

    foreach ($arData["PROPS"] as $num => $code) {
        $PROP[$code] = $item[$num];
    }

    /*if(array_key_exists($item[0], $dataArr)) {
        foreach ($dataArr[$item[0]] as $k => $quotate) {
            $PROP["QUOTATIONS_" . $k] = $quotate;
        }
    }*/

    //динамика акций

    foreach ($dates as $k => $date) {
        /*		$res = $moexActionsData::getList([
                    "filter" => [
                            ["LOGIC"=>"AND",
                            ["<=UF_DATE" => $date], [">=UF_DATE" => (new DateTime($date))->modify('-2 days')->format('d.m.Y'),]
                             ],
                        "UF_ITEM" => $item[0],
                    ],
                    "select" => [
                        "UF_DATE",
                        "UF_CLOSE"
                    ],
                        "order" => ["UF_DATE"=>"DESC"]

                ]);*/

        //Получаем последнюю по дате цену в диапазоне дат от $date-30 дней до $date
        $res = $moexActionsData::getList(["filter" => [["LOGIC" => "AND", ["<=UF_DATE" => $date], [">=UF_DATE" => (new DateTime($date))->modify('-10 days')->format('d.m.Y'),]], "UF_ITEM" => $item[0],], "select" => ["UF_DATE", "UF_CLOSE"], "order" => ["UF_DATE" => "DESC"], "limit" => 2

        ]);
        $result['UF_CLOSE'] = 0;
        while ($row = $res->fetch()) {
            if (floatval($row['UF_CLOSE']) > 0) {
                $result['UF_CLOSE'] = $row['UF_CLOSE'];
                if ($k != "DAY") {
                    break;
                }
            }
        }


        $PROP["QUOTATIONS_" . $k] = $result['UF_CLOSE'];
        $PROP["LASTPRICE"] = $result['UF_CLOSE'];

        //Считаем рост/падение для одного дня
        if ($k == "DAY") {

            $actualLastprice = $result['UF_CLOSE']; //Берем текущую цену lastprice

            //Записываем однодневный прирост
            if (floatval($actualLastprice) != 0) {
                $PROP["ONE_DAY_DROP_INCREASE"] = round(((floatval($arLastpriceBase[$item[0]]) / floatval($actualLastprice) - 1) * 100), 2);

            } else {
                $PROP["ONE_DAY_DROP_INCREASE"] = 0;
            }
        }
    }


    if ($PROP["ISIN"]) {
        $item[0] = $PROP["ISIN"];
    }

    $arInstrumentType = $MApi->getInstrumentParam($PROP["SECID"], array("TYPENAME"));
    if ($arInstrumentType["TYPENAME"] == "Пай закрытого ПИФа") {
        $PROP["IS_PIF_ACTIVE"] = 264;
    } else {
        $PROP["IS_PIF_ACTIVE"] = false;
    }
    /*	if($PROP["ISIN"]==$PROP["SECID"] && in_array($PROP["BOARDID"], $arPifBoardid)){
            $PROP["IS_PIF_ACTIVE"] = 264;
        }  else {
          $PROP["IS_PIF_ACTIVE"] = false;
        }*/

    if ($PROP["SECID"]) { //Добавляем к свойствам акции - объем выпущенных бумаг
        $PROP["ISSUESIZE"] = $arInstrumentParams = $MApi->getInstrumentParam($PROP["SECID"], array("ISSUESIZE"));
    }

    $arHaveCodes[] = $item[0];

    $arLoadProductArray = array("IBLOCK_ID" => $ibID, "PROPERTY_VALUES" => $PROP, "NAME" => $item[2], //SHORTNAME
        "CODE" => $item[0], //SECID
        "ACTIVE" => "Y",);

    $arFilter = array("IBLOCK_ID" => $ibID, "CODE" => $item[0]);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID"));
    if ($v = $res->GetNext()) {
        unset($arLoadProductArray["PROPERTY_VALUES"]);

        $el->Update($v["ID"], $arLoadProductArray);
        unset($PROP["LASTPRICE"]); //Убираем цену для обновляемого элемента, т.к. она обновлется до этого из /cron/syncMoexDaily.php
        foreach ($PROP as $code => $value) {
            CIBlockElement::SetPropertyValues($v["ID"], $ibID, $value, $code);
        }
    } else {
        $el->Add($arLoadProductArray);
    }

    $arFilter = array("IBLOCK_ID" => $ibPageID, "CODE" => $item[0]);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID"));
    if (!$res->SelectedRowsCount()) {

        $char = ord(substr($item[2], 0, 1));
        if ($char <= 122) {
            $sort = 20;
        } else {
            $sort = 10;
        }
        $arProps["SEO_TITLE_FULL"] = str_replace(" ао", " обыкновенные", $item[2]);
        $arProps["SEO_TICKER"] = $PROP["SECID"];

        $el->Add(array("IBLOCK_ID" => $ibPageID, "NAME" => $item[2], //SHORTNAME
            "PROPERTY_VALUES" => $arProps, "CODE" => $item[0], //SECID
            "SORT" => $sort,));
    } elseif ($row = $res->fetch()) {
        CIBlockElement::SetPropertyValues($row["ID"], $ibPageID, str_replace(" ао", " обыкновенные", $item[2]), "SEO_TITLE_FULL");
        CIBlockElement::SetPropertyValues($row["ID"], $ibPageID, $PROP["SECID"], "SEO_TICKER");
    }

}


//Пометим акции, которые вышли с обращения
if ($arHaveCodes) {
    $arFilter = array("IBLOCK_ID" => $ibID, "!CODE" => $arHaveCodes);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID"));
    while ($v = $res->GetNext()) {
        CIBlockElement::SetPropertyValues($v["ID"], $ibID, "Да", "HIDEN");
    }
}

//--------------------------------------------------
try {
    $arData = array();

//соответствия колонок свойтсвам
    $properties = CIBlockProperty::GetList(array("sort" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => $ibID));
    while ($prop = $properties->GetNext()) {
        if (in_array($prop["CODE"], $data["marketdata"]["columns"])) {
            $arData["PROPS"][array_search($prop["CODE"], $data["marketdata"]["columns"])] = $prop["CODE"];
        }
    }
    /*    $boardsArr = [
            'TQDE',
            'TQDP',
            'TQBR',
        ];*/
    $boardsArr = ['TQDE', 'TQDP', 'TQBR', 'TQTD', 'TQTE', 'TQPI',];
//обработка доп
    foreach ($data["marketdata"]["data"] as $item) {

        foreach ($arData["PROPS"] as $num => $code) {
            $arFilter = array("IBLOCK_ID" => $ibID, "PROPERTY_SECID" => $item[0]);
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID"));
            if ($v = $res->GetNext()) {
                CIBlockElement::SetPropertyValues($v["ID"], $ibID, $item[$num], $code);
            }
        }
    }

    foreach ($boardsArr as $boardID) {
        foreach ($data["marketdata"]["data"] as $item) {

            if ($item[1] !== $boardID) {
                continue;
            }
            foreach ($arData["PROPS"] as $num => $code) {
                $arFilter = array("IBLOCK_ID" => $ibID, "PROPERTY_SECID" => $item[0]);
                $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID"));
                if ($v = $res->GetNext()) {
                    CIBlockElement::SetPropertyValues($v["ID"], $ibID, $item[$num], $code);
                }
            }
        }
    }
} catch (Exception $e) {
    mail("alphaprogrammer@gmail.com", "Error: Marketdata", print_r($e->getMessage(), true));
    var_dump("Marketdata: " . $e->getMessage());
}

//--------------------------------------------------
//Данные по LASTCHANGEPRCNT
try {
    $arFilter = array("IBLOCK_ID" => $ibID, "!PROPERTY_BOARDID" => false);
    $res = CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_BOARDID"), false, array("PROPERTY_BOARDID"));
    while ($v = $res->GetNext()) {
        $url = 'https://iss.moex.com/iss/engines/stock/markets/shares/boards/' . $v["PROPERTY_BOARDID_VALUE"] . '/securities.json';
        $data = ConnectMoex($url);

        $needColumn = "";
        foreach ($data["marketdata"]["columns"] as $n => $column) {
            if ($column == "LASTCHANGEPRCNT") {
                $needColumn = $n;
                //[14] => LASTCHANGEPRCNT
                break;
            }
        }
        if ($needColumn) {
            $arItems = array();
            $arFilter = array("IBLOCK_ID" => $ibID, "PROPERTY_BOARDID" => $v["PROPERTY_BOARDID_VALUE"]);
            $res2 = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "PROPERTY_SECID"));
            while ($v2 = $res2->GetNext()) {
                $arItems[$v2["PROPERTY_SECID_VALUE"]] = $v2["ID"];
            }

            foreach ($data["marketdata"]["data"] as $n => $item) {
                if ($arItems[$item[0]]) {
                    CIBlockElement::SetPropertyValues($arItems[$item[0]], $ibID, $item[$needColumn], "LASTCHANGEPRCNT");
                }
            }
        }
    }
} catch (Exception $e) {
    mail("alphaprogrammer@gmail.com", "Error: LASTCHANGEPRCNT", print_r($e->getMessage(), true));
    var_dump("LASTCHANGEPRCNT: " . $e->getMessage());
}
//--------------------------------------------------
/*for($i=0;$i<=30;$i++){
    $url = "https://iss.moex.com/iss/statistics/engines/stock/currentprices.json";
    $startPage = $i*1000;
    if($startPage){
        $url .= "?start=".$startPage;
    }

    //файл с ценами
    $data = ConnectMoex($url);

    $arData = array();

    //соответствия колонок свойтсвам
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ibID));
    while ($prop = $properties->GetNext())
    {
        if(in_array($prop["CODE"], $data["currentprices"]["columns"])){
            $arData["PROPS"][array_search($prop["CODE"], $data["currentprices"]["columns"])] = $prop["CODE"];
        }
    }

    //обработка доп
    foreach ($data["currentprices"]["data"] as $item){
        foreach($arData["PROPS"] as $num=>$code){
            $arFilter = Array("IBLOCK_ID"=>$ibID, "PROPERTY_SECID"=>$item[2]);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
            if($v = $res->GetNext())
            {
                CIBlockElement::SetPropertyValues($v["ID"], $ibID, $item[$num], $code);
            }
        }
    }

}*/

//Обновление цен перенесено в класс moex_sync и выполняется парсером /cron/syncMoexDaily.php

//Сложить капитализацию

try {
//Пересчитаем капу
    $arCapKoeffActionPagesTmp = array();
    $arCapKoeffActionPages = array();
    $arSelect = array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_CAPITAL_KOEF");
    $arFilter = array("IBLOCK_ID" => IntVal(29), "!PROPERTY_CAPITAL_KOEF" => false);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNext()) {
        $arCapKoeffActionPagesTmp[$ob["NAME"]] = $ob["PROPERTY_CAPITAL_KOEF_VALUE"];
    }

    $arSelect = array("ID", "NAME", "IBLOCK_ID");
    $arFilter = array("IBLOCK_ID" => IntVal(26), "NAME" => array_keys($arCapKoeffActionPagesTmp));
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNext()) {
        $arCapKoeffActionPages[$ob["ID"]] = $arCapKoeffActionPagesTmp[$ob["NAME"]];

    }


    $arEmitentsCapital = array();
    $arFilter = array("IBLOCK_ID" => 32, "!PROPERTY_EMITENT_ID" => false, "!PROPERTY_ISSUECAPITALIZATION" => false);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "CODE", "PROPERTY_EMITENT_ID", "PROPERTY_ISSUECAPITALIZATION", "PROPERTY_ISIN"));
    while ($v = $res->GetNext()) {
        $arEmitentsCapital[$v["PROPERTY_EMITENT_ID_VALUE"]] += $v["PROPERTY_ISSUECAPITALIZATION_VALUE"];
    }

    foreach ($arEmitentsCapital as $emitent => $summ) {
        $arFilter = array("IBLOCK_ID" => 32, "PROPERTY_EMITENT_ID" => $emitent, "PROPERTY_HIDEN"=>false);
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "CODE", "PROPERTY_EMITENT_ID"));
        while ($v = $res->GetNext()) {
            if (array_key_exists($v["PROPERTY_EMITENT_ID_VALUE"], $arCapKoeffActionPages)) { //Если на странице компании задан коэффициент - пересчитываем сумму
                $summ = $summ / floatval($arCapKoeffActionPages[$v["PROPERTY_EMITENT_ID_VALUE"]]);
            }
            CIBlockElement::SetPropertyValues($v["ID"], 32, $summ, "ISSUECAPITALIZATION");
        }
    }
} catch (Exception $e) {
    mail("alphaprogrammer@gmail.com", "actions_sum_capa_error", print_r($e->getMessage(), true));
    var_dump("Capitalization: " . $e->getMessage());
}

//Пересчитываем текущую капитализацию по компаниям в валюте
try {
    CModule::IncludeModule("eremin.finplantools");

    $fptTools = new fptTools();
    $arResults = array();
    $arCompanyPages = $fptTools->getCompanyList();//Инициализируем выборку и заполнение свойств класса
    if (count($fptTools->arCompanyList) > 0) {
        //	 mail('alphaprogrammer@gmail.com', 'My Subject2', 'test');
        foreach ($fptTools->arCompanyList as $company_key => $arCompany) {
            $secid = $fptTools->arActions[$arCompany["COMPANY_ID"]];
            $curCap = $fptTools->GetCurCap($secid);

            $ret = $fptTools->saveCurrentCap($arCompany["COMPANY_ID"], $curCap);

            if (!empty($ret["result"])) {
                $arResults[$company_key . "_" . $secid] = array("CAP" => $curCap, "RESULT" => $ret["result"]);
            }
        }

    }
    unset($fptTools);
} catch (Exception $e) {
    mail("alphaprogrammer@gmail.com", "actions_cur_capa_error", print_r($e->getMessage(), true));
    var_dump("Cur capitalization: " . $e->getMessage());
}


//Посчитать показатель капа х фрифлоат / 100  (капа фрифлоата)
//--------------------------------------------------
CModule::IncludeModule("highloadblock");
//Получим список акций входящих в индексы, вместе с ними и их фрифлоат и рассчитаем капу фрифлоата
$arActionsFreefloat = array();

$hlblock = HL\HighloadBlockTable::getById(34)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$moexIndexData = $entity->getDataClass();
$raw = $moexIndexData::getList(["select" => ["UF_SHARE_CODE", "UF_FREEFLOAT"],]);
while ($row = $raw->fetch()) {
    $arActionsFreefloat[$row["UF_SHARE_CODE"]] = $row["UF_FREEFLOAT"];
}
$arFilter = array("IBLOCK_ID" => 32, "CODE" => array_keys($arActionsFreefloat));
$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "CODE", "IBLOCK_ID", "PROPERTY_ISSUECAPITALIZATION", "PROPERTY_ISIN"));

while ($v = $res->Fetch()) {
    if (array_key_exists($v["CODE"], $arActionsFreefloat) == true) { //Если в массиве с фрифлоатом есть даная акция - считаем ей капу фрифлоата
        CIBlockElement::SetPropertyValues($v["ID"], 32, floatval(floatval($v["PROPERTY_ISSUECAPITALIZATION_VALUE"]) * $arActionsFreefloat[$v["CODE"]] / 100), "CAP_X_FREEFLOAT"); //Считаем и записываем капу фрифлоата
    }
}

unset($arActionsFreefloat);

//Пересчитать кандидатов в индексы
$CIndexes = new CIndexes;
$CIndexes->setIndexCandidate();
unset($CIndexes);

//Пересчитать количества вхождений акций и etf в портфели
try {
    $CPortfolio = new CPortfolio();
    $CPortfolio->getActionsCntInPortfolios(true);
    unset($CPortfolio);
} catch (Exception $e) {
    mail("alphaprogrammer@gmail.com", "Ошибка пересчета кол-ва вхождений акций и etf в портфели", print_r($e->getMessage(), true));
    //var_dump("Cportfolio->getActionsCntInPortfolios: " . $e->getMessage());
}


//средний PE секторов
function midSectorPERus()
{
    $arFilter = array("IBLOCK_ID" => 32, "!=PROPERTY_BOARDID" => $APPLICATION->ExcludeFromRadar["акции"], "!PROPERTY_IS_ETF_ACTIVE_VALUE" => "Y", "!PROPERTY_IS_PIF_ACTIVE_VALUE" => "Y");
    $arResult = array();
    $resA = new Actions();
    $arCaps = [];

    $res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "IBLOCK_ID", "CODE", "NAME", "PROPERTY_ISSUECAPITALIZATION", "PROPERTY_SECID", "PROPERTY_PROP_SEKTOR", "PROPERTY_EMITENT_ID"]);

    while ($item = $res->fetch()) {

        if ($item['PROPERTY_PROP_SEKTOR_VALUE'] && $item['PROPERTY_EMITENT_ID_VALUE'] && $item['PROPERTY_ISSUECAPITALIZATION_VALUE']) {
            $actionItem = $resA->getItem($item["CODE"]);

            if ($actionItem["PERIODS"]) {
                $periods = array_values($actionItem["PERIODS"]);
                if ($periods[0] && $periods[0]['Прибыль за год (скользящая)']) {
                    $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$item['PROPERTY_EMITENT_ID_VALUE']] = $periods[0]['Прибыль за год (скользящая)'];
                    $arCaps[$item['PROPERTY_PROP_SEKTOR_VALUE']][$item['PROPERTY_EMITENT_ID_VALUE']] += $item['PROPERTY_ISSUECAPITALIZATION_VALUE'];
                }
            }
        }
    }

    //Подготовим выборку из инфоблока отраслей РФ
    $ibID = 72;
    $arFilterInd = array("IBLOCK_ID" => $ibID);
    $arIndustries = array();
    $resInd = CIBlockElement::GetList([], $arFilterInd, false, false, ["ID", "IBLOCK_ID", "NAME"]);
    while ($itemInd = $resInd->fetch()) {
        $arIndustries[$itemInd["NAME"]] = $itemInd;
    }


    foreach ($arResult as $sector => $ids) {
        $sumProfit = 0;
        $sumCaps = 0;

        foreach ($ids as $compID => $profit) {
            $sumProfit += $profit;

            $sumCaps += $arCaps[$sector][$compID];
        }

        $item = $arIndustries[$sector];
        CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], round($sumCaps / $sumProfit / 1000000, 2), "MID_PE");
    }
}


//-----------------------------
$finish = microtime(true);


$delta = $finish - $start;

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean("actions_data");
$cache->clean("etf_data");
$cache->clean("radar_ratings"); //Очищаем кеш рейтингов акций

global $CACHE_MANAGER;
$CACHE_MANAGER->ClearByTag("ru_actions_candle");

midSectorPERus();
$cache->clean("показателей отраслей по РФ"); //Очищаем кеш рейтингов акций
$res = new CIndustriesRus();
unset($res);

$res = new Actions();
$res->getTable(1);
$res = new ETF();
$res->getTable(1);

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta, 2));

echo $delta . ' сек.';