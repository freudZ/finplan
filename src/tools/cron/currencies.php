<?//Загрузка валют и их цен, перестройка кеша графиков
$_SERVER["DOCUMENT_ROOT"] = "/var/www/bitrix/data/www/fin-plan.org";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
use Bitrix\Highloadblock as HL;


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
\Bitrix\Main\Loader::includeModule("iblock");

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 206694;
$cCronTools->changeStartTime($cronTaskId);

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/currencies_cron_log.txt");
$start = microtime(true);

//даты для фильтра по росту
$dates = [
    'NOW' => (new DateTime(date('Y-m-d')))->modify('-1 days'),
    'MONTH' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1M')),
    'ONE_YEAR' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1Y')),
    'THREE_YEAR' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P3Y')),
];

foreach ($dates as $k => $v) {
    if(isWeekEndDay($v->format("d.m.Y"))){
        $finded = false;
        while(!$finded){
            $v->modify("+1 days");
            if(!isWeekEndDay($v->format("d.m.Y"))){
                $finded = true;
            }
        }
    }
    $dates[$k] = $v->format("d.m.Y");
}

//получаем список существующих валют
$data = ConnectMoex("https://iss.moex.com/iss/engines/currency/markets/selt/boards/CETS/securities.json?iss.only=securities");

//Получим из инфоблока все загруженные валюты
$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_NAME", "PROPERTY_BOARDID", "PROPERTY_SECID");
$arFilter = Array("IBLOCK_ID"=>IntVal(54), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arIblockIndexesByCode = array();
while($ob = $res->Fetch()){
 $arIblockIndexesByCode[$ob["CODE"]] = $ob;
}

/*[columns] => Array
   (
       [0] => SECID
       [1] => BOARDID
       [2] => SHORTNAME
       [3] => LOTSIZE
       [4] => SETTLEDATE
       [5] => DECIMALS
       [6] => FACEVALUE
       [7] => MARKETCODE
       [8] => MINSTEP
       [9] => PREVDATE
       [10] => SECNAME
       [11] => REMARKS
       [12] => STATUS
       [13] => FACEUNIT
       [14] => PREVPRICE
       [15] => PREVWAPRICE
       [16] => CURRENCYID
       [17] => LATNAME
   )*/

if(count($data['securities']['data'])>0){
 foreach($data['securities']['data'] as $indexRow){
	if(!array_key_exists($indexRow[0],$arIblockIndexesByCode)){ //если такого индекса еще нет - добавим в инфоблок и добавим ключ в массив индексов из инфоблока, для последующего обхода при получении истории

		$el = new CIBlockElement;

		$PROP = array();
		$PROP["SECNAME"]    = $indexRow[10];
		$PROP["LATNAME"] 	  = $indexRow[17];
		$PROP["SECID"]      = $indexRow[0];
		$PROP["BOARDID"]    = $indexRow[1];
		$PROP["LOTSIZE"]    = $indexRow[3];
		$PROP["CURRENCYID"] = $indexRow[16];
		$PROP["FACEVALUE"]  = $indexRow[6];
		$PROP["MINSTEP"]    = $indexRow[8];
		$PROP["FACEUNIT"]   = $indexRow[13];




		$arLoadProductArray = Array(
		 // "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
		  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
		  "IBLOCK_ID"      => 54,
		  "PROPERTY_VALUES"=> $PROP,
		  "NAME"           => $indexRow[2],
		  "CODE"           => $indexRow[0],
		  "ACTIVE"         => "Y",            // активен
		  );

		if($PRODUCT_ID = $el->Add($arLoadProductArray)){
		  //echo "New ID: ".$PRODUCT_ID;
		  $arIblockIndexesByCode[$indexRow[0]] = array("ID"=>$PRODUCT_ID, "PROPERTY_BOARDID_VALUE"=>$indexRow[1], "NAME"=>$indexRow[2]);
		  }
		else{
		  echo "Error: ".$el->LAST_ERROR;
		  }
	}

 }
 unset($data);
}//count>0


//Строки для отдельных дозапросов по диапазону дат.
$dateStart = (new DateTime('03.05.2011'))->format('Y-m-d'); //Начало истории
$curDate = (new DateTime(date()))->format('Y-m-d');

//По умолчанию получаем данные за текущий день
//$dateStart = (new DateTime(date()))->format('Y-m-d');
//$curDate = $dateStart;




//Получаем исторические данные по индексам
if(count($arIblockIndexesByCode)>0){

//исторические данные для динамики индексов
CModule::IncludeModule("highloadblock");
$hlblock   = HL\HighloadBlockTable::getById(31)->fetch();
$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
$moexIndexData = $entity->getDataClass();


 //Очищаем кеш графиков индексов
// $initdir = '/graph_new_hiload_id_30/';
// BXClearCache(true, "/graph_new_hiload_id_30/");
// $moexGraph = new MoexGraph();

 foreach($arIblockIndexesByCode as $SECID=>$data){


 $baseUrl = 'https://iss.moex.com/iss/history/engines/currency/markets/selt/boards/'.$data['PROPERTY_BOARDID_VALUE'].'/securities/'.$SECID.'.json?from='.$dateStart.'&till='.$curDate;

 //Получим список существующих исторических данных для исключения дублей по коду валюты+boardid+дате
 			$rsData = $moexIndexData::getList(array(
				'order' => array('UF_DATE' => 'ASC'),
				'select' => array('ID', 'UF_ITEM', 'UF_DATE'),
				'filter' => array('UF_ITEM' => $SECID)
			));
	  $arHLData = array();

/*    "columns": [
     0 "BOARDID",
     1 "TRADEDATE",
     2 "SHORTNAME",
     3 "SECID",
     4 "OPEN",
     5 "LOW",
     6 "HIGH",
     7 "CLOSE",
     8 "NUMTRADES",
     9 "VOLRUR",
     10 "WAPRICE"
    ],*/

	  	while ($el = $rsData->fetch()) {
		  $arHLData[md5($el['UF_ITEM'].$el['UF_DATE'])] = $el['ID'];
	  	}

 try{
    $loop = true;
    $startPage = 0;
    do{
		  $url = $baseUrl;
	 //	  echo "start=".$startPage."<br>";
        if($startPage){
            $url = $baseUrl."&start=".$startPage;
        }
		 $startPage += 100;

	  	 $data = ConnectMoex($url);  //Стартовый запрос по каждому новому тикеру, кроме данных отдает общее кол-во записей и позицию курсора
/*		 echo "cursor:<pre  style='color:black; font-size:11px;'>";
		    print_r($data['history.cursor']['data']);
		    echo "</pre>";*/
	  if(count($data['history']['data'])>0){
		 foreach($data['history']['data'] as $kd=>$vd){
		 	$tradeDate = (new DateTime($vd['1']))->format('d.m.Y');
		 	if(!array_key_exists(md5($vd[3].$tradeDate),$arHLData)){ //Если запись с такой комбинацией отсутствует то запишем новые данные
		     $arFields = Array(
					'UF_ITEM' => $vd[3],
					'UF_DATE' => $tradeDate,
					'UF_OPEN' => $vd[4],
					'UF_CLOSE' => $vd[7],
					'UF_HIGH' => $vd[6],
					'UF_LOW' => $vd[5],
				);
			  $HLRes =	$moexIndexData::add($arFields);

				 if($HLRes->isSuccess()){
/*				  echo "Записано: <pre  style='color:black; font-size:11px;'>";
                 print_r($arFields);
                 echo "</pre>";*/
					//AddMessage2Log($el->LAST_ERROR);return true;
				  //	AddMessage2Log('Записано в HL_30 SECID='.$vd[1]);
				 } else {
					AddMessage2Log('Ошибка: '.$HLRes->getErrors());return true;
				 }

				}
		 }
		}


	  //	$loop = ($startPage<intval($data['history.cursor']['data'][0][1])) ? true : false;
	  $loop = (!$data["history"]["data"]) ? false : true;
    } while($loop);

  }catch (Exception $e){
	mail("alphaprogrammer@gmail.com", "Error: indexed history loop", print_r($e->getMessage(), true));
    var_dump("New price loop: " . $e->getMessage());
}



	 //динамика индексов и перестройка кеша графиков
	 $PROP = array();

    foreach ($dates as $k=>$date) {
        $result = $moexIndexData::getList([
            "filter" => [
                "UF_DATE" => $date,
                "UF_ITEM" => $SECID,
            ],
            "select" => [
                "UF_CLOSE"
            ],
        ])->fetch();

        $PROP["QUOTATIONS_" . $k] = $result['UF_CLOSE'];
    }

/*	 	 echo $SECID."<pre  style='color:black; font-size:11px;'>";
       print_r($PROP);
       echo "</pre>";  die();*/
	   foreach($PROP as $code=>$value){
			CIBlockElement::SetPropertyValuesEx($arIblockIndexesByCode[$SECID]["ID"], 54, array($code => $value));
		}


		//Кешируем графики индексов
	 	//$chrtdata = $moexGraph->getForIndex($SECID, 31, 'index', 86400*7);

} //foreach $arIblockIndexesByCode по кодам бумаг


}


$finish = microtime(true);
$delta = $finish - $start;



$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));

