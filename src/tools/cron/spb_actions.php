<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
@set_time_limit(0);

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
    exit();
}

if($_REQUEST["send"] && $_FILES["file"]["tmp_name"]){

    Loader::includeModule("iblock");
    Loader::includeModule("highloadblock");
    
    $hlBlock = HL\HighloadBlockTable::getById(29)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlBlock);
    $entityDataClass = $entity->getDataClass();

    $arMes = array(
        "SUCCESS" => 0,
        "ERR" => [],
    );

    $inputFileName = $_FILES["file"]["tmp_name"];
    $row = 0;
    $header = [];

    if (($handle = fopen($inputFileName, "r")) !== FALSE) {

        while (($data = fgetcsv($handle, 1000000, ";")) !== FALSE) {
            try{
                if ($row === 0) {
                    //в первой строке заголовки, флипаем их чтобы получить индекс для следующих строк
                    $header = array_flip($data);
                    $row++;
                    continue;
                }

                $hlData = [
                    "UF_DATE" => (new DateTime($data[$header['trade_date']]))->format("d.m.Y"),
                    "UF_ITEM" => (string)$data[$header['asset_code']],
                    "UF_OPEN" => (float)$data[$header['main_firstdeal_price']],
                    "UF_CLOSE" => (float)$data[$header['main_lastdeal_price']],
                    "UF_HIGH" => (float)$data[$header['main_max_deal_price']],
                    "UF_LOW" => (float)$data[$header['main_min_deal_price']],
                ];

                $result = $entityDataClass::add($hlData);
                $arMes["SUCCESS"]++;

            } catch (Exception $e) {
                $arMes["ERR"][$data[$header['asset_code']]] = $e->getMessage();
            }

            $row++;
        }
?>
        <p><b>Обновлено/добавлено (кол-во компаний):</b> <?=$arMes["SUCCESS"]?></p>
        <p><b>Не найдено компаний:</b> <?=implode(", ", $arMes["ERR"])?></p>
<?
        exit();
    }
}
?>

<form enctype="multipart/form-data" method="post">
    <div class="form-group">
        <label for="file">Отчет по компаниям</label>
        <input type="file" name="file" id="file">
    </div>
    <button type="submit" name="send" value="Y" class="btn btn-default">Отправить</button>
</form>
