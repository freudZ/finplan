<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Получает котировки валют к доллару США от провайдера polygon, запускается в 7 утра по МСК (у поставщика 0:00 нового дня)*/
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

Global $DB, $APPLICATION;
use PolygonIO\Rest\Rest;

//       /opt/php71/bin/php -f /var/www/bitrix/data/www/fin-plan.org/tools/cron/getPolygonPrices.php

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" );
require $_SERVER["DOCUMENT_ROOT"].'/vendor/polygon-io/api/src/rest/Rest.php';

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
//$cCronTools = new fptСrontools;
//$cronTaskId = 509416;
//$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

/*$actionUsaIblockId = 55;
$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_SECID");
$arFilter = Array("IBLOCK_ID"=>$actionUsaIblockId, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
$arTickersUsa = array();
while($ob = $res->fetch()){
	//если тикер еще не добавлен и если он не входит в список дублируемых из СПБ тикеров - добавим его в массив
 if(!in_array($ob["PROPERTY_SECID_VALUE"], $arTickersUsa) && !in_array($ob["PROPERTY_SECID_VALUE"], $APPLICATION->crossTickers)){
   $arTickersUsa[$ob["ID"]] = $ob["PROPERTY_SECID_VALUE"];
 }
}*/
//Перевернутый массив инфоблока акций США с ключами по тикеру и значениями = ID
//$arTickerToIblockId = array_flip($arTickersUsa);

//unset($res, $ob, $arFilter, $arSelect);

//if(count($arTickersUsa)>0){
$rest = new Rest('ZmCXrWbo2hzVHWQtmyn9UJ3rlGWE3tKL');
//$date = (new DateTime())->modify('- 1 days')->format('Y-m-d');



$arExistPricesOnDateRub = array();
$sql = "SELECT DISTINCT `UF_CODE`  FROM `hl_currency_rates` WHERE `UF_RATE_CURRENCY` = 'RUB'";

$resRub  = $DB->Query($sql);
while($rowRub = $resRub->fetch()){
	$arExistPricesOnDateRub[] = $rowRub["UF_CODE"];
}


//$dateStart = '2021-07-20';  //С конкретной даты, раньше текущей
$dateStart = (new DateTime()); //За текущее число

$now = (new DateTime()); //До какой даты запрашивать курсы

$date = $dateStart;//(new DateTime($dateStart));
while($date->getTimestamp() <= $now->getTimestamp()){
$arExistPricesOnDate = array();
//Получаем на текущую дату $date все существующие курсы для определения insert|update
$dateSql = $date->format('Y-m-d');
$query = "SELECT `ID`, `UF_CODE`, `UF_RATE_CURRENCY` FROM `hl_currency_rates` WHERE `UF_DATE` = '$dateSql' AND `UF_RATE_CURRENCY` != 'RUB'";
$res = $DB->Query($query);
while($row = $res->fetch()){
  $arExistPricesOnDate[$row["UF_CODE"]] = $row["ID"];
}
unset($query, $res, $row);

$locale = 'global';

//$date = '2021-07-13';

		$arResult = array();
		$arTmp = array();
		$params = array();


     try {
     	$arResult = $rest->forex->groupedDaily->get($dateSql, 'global', 'FX', $params);
     } catch (Exception $e) {
     	   CLogger::getPolygonRates($dateSql." ".$e->getMessage());
			$arResult = array();
         //echo 'Поймано исключение: ',  $e->getMessage(), "\n";
     } finally {
       /* code */
     }

     if(count($arResult)>0 && $arResult["status"]=="OK"){

		  foreach($arResult["results"] as $k=>$v){
			 $code = mb_str_replace("C:","", $v["T"]);
			 $arCode = array(mb_substr($code,0,3), mb_substr($code,3,3));
			 if($arCode[1]=="USD" && $arCode[0]!="RUB"){
			 	//USDRUB
				//1 USD стоит ["c"] RUB
			 $arTmp[$arCode[0]]["c"] = $v["c"];
			 $arTmp[$arCode[0]]["cursUSD"] = round(1/floatval($v["c"]), 5);
			 $arTmp[$arCode[0]]["code"] = $code;
			 $arTmp[$arCode[0]]["arCode"] = $arCode;
			 	}
		  }

		}

		$dateSql = $date->format('Y-m-d');
//  echo "<pre  style='color:black; font-size:11px;'>";
   // print_r($arExistPricesOnDateRub);
	 // echo "date: ". $dateSql ." count: ".count($arTmp)." count_on_date: ".count($arExistPricesOnDate).PHP_EOL;
   //  echo "</pre>";
	  //	  CLogger::PolygonRates(print_r($arTmp, true));


  		foreach($arTmp as $code=>$arCurs){
  	   if(!in_array($code, $arExistPricesOnDateRub)){  //Если валюта не получается из ЦБ в отношении к рублю то добавляем/обновляем курс на дату.

      if(!array_key_exists($code, $arExistPricesOnDate)){

         $query = "INSERT INTO `hl_currency_rates`(`UF_CODE`, `UF_DATE`, `UF_RARE`, `UF_RATE_CURRENCY`, `UF_RATE`, `UF_NOMINAL`, `UF_DIR`) VALUES ('$code', '$dateSql', 1,
         '".$arCurs['arCode'][1]."', '".$arCurs['cursUSD']."', 1, 0)";
      } else {
         $query = "UPDATE `hl_currency_rates` SET `UF_RATE`=".$arCurs['cursUSD']." WHERE `ID` = $arExistPricesOnDate[$code]";
      }

      $DB->Query($query);
		}
	 }

	 $date->modify('+1 days');
  }

/*    if ($arResult['close']) {
        try {
            if (array_key_exists($ticker, $arTickerToIblockId)) {
                CIBlockElement::SetPropertyValues($arTickerToIblockId[$ticker], $actionUsaIblockId, str_replace(",", ".", $arResult['close']), "LASTPRICE");
            }
        } catch (Exception $e) {
            $err = $e->getMessage();
        }
    }*/





  //Сброс кеша
  try {
   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
  // $cache->clean("actions_usa_data"); //Убиваем кеш американских акций, что бы пересоздать показатели в динамических данных
  // $cache->clean("radar_ratings"); //Очищаем кеш рейтингов акций
 //	if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
 //  $GLOBALS['CACHE_MANAGER']->ClearByTag('usa_graphdata'); //Очищаем кеш графиков котировок сша
   } catch (Exception $e) {
      $err = $e->getMessage();
   }
//}//count tickers





$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";

//$cCronTools->changeEndTime($cronTaskId);
//$cCronTools->changeWorkTime($cronTaskId, round($delta,2));

