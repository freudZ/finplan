<?$_SERVER["DOCUMENT_ROOT"] = "/var/www/bitrix/data/www/fin-plan.org";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 237037;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

\Bitrix\Main\Loader::includeModule("iblock");
$data = ConnectMoex("https://iss.moex.com/iss/engines/stock/markets/bonds/securities.json");

$step = 0;
$datesArr = [];
$needLoop = true;
do{
    $dates = ConnectMoex("https://iss.moex.com/iss/history/engines/stock/markets/bonds/listing.json?status=traded&start=" . $step);
    foreach ($dates["securities"]["data"] as $v) {
        $datesArr[$v[0]] = $v[5];
    }
    $step += 100;
    $needLoop = (!$dates["securities"]["data"]) ? false : true;
}while($needLoop);

$ibID = 27;
$ibPageID = 30;
$arData = array();

//Получим имена всех эмитентов в массив
$arSelectEm = Array("ID", "NAME", "IBLOCK_ID");
$arFilterEm = Array("IBLOCK_ID"=>IntVal(26), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$resEm = CIBlockElement::GetList(Array(), $arFilterEm, false, false, $arSelectEm);
while($ob = $resEm->fetch()){
  $arEmitents[$ob["ID"]] = $ob["NAME"];
}
unset($resEm, $arFilterEm, $arSelectEm);



//соответствия колонок свойствам
$properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ibID));
while ($prop = $properties->GetNext())
{
	if(in_array($prop["CODE"], $data["securities"]["columns"])){
		$arData["PROPS"][array_search($prop["CODE"], $data["securities"]["columns"])] = $prop["CODE"];
	}
}

//ISIN
$arData["PROPS"][array_search("ISIN", $data["securities"]["columns"])] = "ISIN";

/*
echo "<pre>";
print_r($arData);
echo "</pre>";
exit();
*/

//Соберем существующие облигации в массив с именами их эмитентов с ключами по коду облигации
$arFilterObl = Array("IBLOCK_ID"=>$ibID);
$resObl = CIBlockElement::GetList(Array(), $arFilterObl, false, false, array("ID", "IBLOCK_ID", "PROPERTY_EMITENT_ID", "PROPERTY_ISIN", "PROPERTY_BOARDID", "PROPERTY_SECID", "PROPERTY_OFFER_NOUPDATE"));
$arAllObligations = array();
$arAllObligationsBySecid = array();
while($ob = $resObl->fetch())
{
    $arAllObligations[$ob["PROPERTY_ISIN_VALUE"]] = array("ID"=>$ob["ID"], "BOARDID"=>$ob["PROPERTY_BOARDID_VALUE"], "EMITENT_NAME"=>$arEmitents[$ob["PROPERTY_EMITENT_ID_VALUE"]], "SECID"=>$ob["PROPERTY_SECID_VALUE"], "OFFER_NOUPDATE"=>$ob["PROPERTY_OFFER_NOUPDATE_VALUE"]);
    $arAllObligationsBySecid[$ob["PROPERTY_SECID_VALUE"]] = array("ID"=>$ob["ID"], "BOARDID"=>$ob["PROPERTY_BOARDID_VALUE"], "EMITENT_NAME"=>$arEmitents[$ob["PROPERTY_EMITENT_ID_VALUE"]], "SECID"=>$ob["PROPERTY_SECID_VALUE"], "OFFER_NOUPDATE"=>$ob["PROPERTY_OFFER_NOUPDATE_VALUE"]);
}
unset($ob, $arFilterObl, $resObl);

$el = new CIBlockElement;

//обработка
$arHaveCodes = array();
foreach ($data["securities"]["data"] as $item){
	$PROP = array();

	foreach($arData["PROPS"] as $num=>$code){
		$PROP[$code] = $item[$num];
	}

    if(array_key_exists($item[0], $datesArr)) {
        $PROP["STARTDATE"] = $datesArr[$item[0]];
    }
	
	if($PROP["ISIN"]){
		$item[0] = $PROP["ISIN"];
	}

	$arHaveCodes[] = $item[0];
	
	$arLoadProductArray = Array(
		"IBLOCK_ID"      => $ibID,
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => $item[2], //SHORTNAME
		"CODE" => $item[0], //SECID
		"ACTIVE"         => "Y",
	);

	//Получим имя эмитента для страницы облигации (для title через SEO вкладку)
	$emitentName = '';
	if(array_key_exists($item[0],$arAllObligations))
	{
        $arObligData = $arAllObligations[$item[0]];
        $emitentName = $arObligData["EMITENT_NAME"];
		unset($arLoadProductArray["PROPERTY_VALUES"]);
		
		$el->Update($arObligData["ID"], $arLoadProductArray);
		foreach($PROP as $code=>$value){
			if($code == "OFFERDATE" && $arObligData["OFFER_NOUPDATE"]=='Y') continue;
			//TODO Сделать апдейт свойств одним запросом
			CIBlockElement::SetPropertyValues($arObligData["ID"], $ibID, $value, $code);

			if($code == "COUPONPERIOD" && floatval($value)>0){
				//Расчет кол-ва купонных выплат в год
					$coupons_in_year = 0;
					$coupons_in_year = floor(365/floatval($value));  // 365/Длит. купона
					CIBlockElement::SetPropertyValues($arObligData["ID"], $ibID, $coupons_in_year, "COUPON_IN_YEARS");
			}
		}
	} else {

		//+Получаем первоначальную номинальную стоимость и добавляем в список свойств
			$dataInitVal = ConnectMoex("https://iss.moex.com/iss/securities/".$PROP["ISIN"]."/.json");
		$moexval = false;
		foreach($dataInitVal["description"]["data"] as $n=>$val){
			if($val[0]=="INITIALFACEVALUE"){
				$moexval = $val[2];
				$arLoadProductArray["PROPERTY_VALUES"]["INITIALFACEVALUE"] = $val[2];
				break;
			}
		}
		//Если с биржи ничего не пришло - то возьмем текущий номинал как первоначальный
		if($moexval==false && array_key_exists("FACEVALUE",$arLoadProductArray["PROPERTY_VALUES"])){
		  $arLoadProductArray["PROPERTY_VALUES"]["INITIALFACEVALUE"] = $arLoadProductArray["PROPERTY_VALUES"]["FACEVALUE"];
		}

	  unset($dataInitVal);
		//-Получаем первоначальную номинальную стоимость и добавляем в список свойств

		$el->Add($arLoadProductArray);
	}
	
	$arFilter = Array("IBLOCK_ID"=>$ibPageID, "CODE"=>$item[0]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	if(!$res->SelectedRowsCount())
	{
		
		$char = ord(substr($item[2], 0 ,1));
		if($char<=122){
			$sort = 20;
		} else {
			$sort = 10;
		}
		$arProp["EMITENT_NAME"] = $emitentName;
		$el->Add(array(
			"IBLOCK_ID"      => $ibPageID,
			"NAME"           => $item[2], //SHORTNAME
			"CODE" => $item[0], //SECID
			"PROPERTY_VALUES" => $arProp,
			"SORT" => $sort,
		));
	} elseif($row = $res->fetch()) {
		CIBlockElement::SetPropertyValues($row["ID"], $ibPageID, $emitentName, "EMITENT_NAME");
	}
	
}

//Пометим облигации, которые вышли с обращения
if($arHaveCodes){
	foreach($arAllObligations as $isin=>$arObligIsin){
	    if(!in_array($isin,$arHaveCodes)){//Если облига из БД по коду не найдена в прилетевших с биржи - значит вышла с обращения
            CIBlockElement::SetPropertyValues($arObligIsin["ID"], $ibID, "Да", "HIDEN");
        }
    }
	
}

//--------------------------------------------------

$arData = array();

//соответствия колонок свойтсвам
$properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ibID));
while ($prop = $properties->GetNext())
{
	if(in_array($prop["CODE"], $data["marketdata"]["columns"])){
		$arData["PROPS"][array_search($prop["CODE"], $data["marketdata"]["columns"])] = $prop["CODE"];
	}
}

//обработка доп
foreach ($data["marketdata"]["data"] as $item){
	
	foreach($arData["PROPS"] as $num=>$code){
	    
	    if(array_key_exists($item[0], $arAllObligations)){
	        $arObligData = $arAllObligations[$item[0]];
            CIBlockElement::SetPropertyValues($arObligData["ID"], $ibID, $item[$num], $code);
        }
	}
}



//--------------------------------------------------
//Данные по LASTCHANGEPRCNT 
/*$arFilter = Array("IBLOCK_ID"=>$ibID, "!PROPERTY_BOARDID"=>false);
$res = CIBlockElement::GetList(Array(), $arFilter, array("PROPERTY_BOARDID"), false, array("PROPERTY_BOARDID"));*/
foreach($arAllObligations as $isin=>$arObligationIsin)
{
    if(empty($arObligationIsin["BOARDID"])) continue;

	$url = 'https://iss.moex.com/iss/engines/stock/markets/bonds/boards/'.$arObligationIsin["BOARDID"].'/securities.json';
	$data = ConnectMoex($url);

	$needColumn = "";
	$COUPONPERIOD_column = "";
	foreach($data["marketdata"]["columns"] as $n=>$column){
		if($column=="LASTCHANGEPRCNT"){
			$needColumn = $n;
			//[13] => LASTCHANGEPRCNT
			break;
		}
	}

	if($needColumn){
	/*	$arItems = array();
		$arFilter = Array("IBLOCK_ID"=>$ibID, "PROPERTY_BOARDID"=>$v["PROPERTY_BOARDID_VALUE"]);
		$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "PROPERTY_SECID"));
		while($v2 = $res2->GetNext())
		{
			$arItems[$v2["PROPERTY_SECID_VALUE"]] = $v2["ID"];
		}*/

		foreach($data["marketdata"]["data"] as $n=>$item){
		    if(array_key_exists($item[0], $arAllObligations))
			{
			    $arObligData = $arAllObligations[$item[0]];
				CIBlockElement::SetPropertyValues($arObligData["ID"], $ibID, $item[$needColumn], "LASTCHANGEPRCNT");
			}
		}
	}

}



//--------------------------------------------------

try{
    $loop = true;
    $startPage = 0;
    do{
        $url = "https://iss.moex.com/iss/statistics/engines/stock/currentprices.json";

        if($startPage){
            $url .= "?start=".$startPage;
        }

        $startPage += 1000;

        //файл с ценами
        $data = ConnectMoex($url);

        $arData = array();

        //соответствия колонок свойтсвам
        $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ibID));
        while ($prop = $properties->GetNext())
        {
            if(in_array($prop["CODE"], $data["currentprices"]["columns"])){
                $arData["PROPS"][array_search($prop["CODE"], $data["currentprices"]["columns"])] = $prop["CODE"];
            }
        }

        //обработка доп
        foreach ($data["currentprices"]["data"] as $item){
            foreach($arData["PROPS"] as $num=>$code){
                if(array_key_exists($item[2], $arAllObligations)){
                    $arObligData = $arAllObligations[$item[2]];
                    CIBlockElement::SetPropertyValues($arObligData["ID"], $ibID, $item[$num], $code);
                }
            }
        }

        $loop = (!$data["currentprices"]["data"]) ? false : true;

    }while($loop);

}catch (Exception $e){
    var_dump("New price loop: " . $e->getMessage());
}

//Пересчитать количества вхождений облигаций в портфели
try {
  $CPortfolio = new CPortfolio();
  $CPortfolio->getObligationsCntInPortfolios(true);
  unset($CPortfolio);
}catch (Exception $e){
	mail("alphaprogrammer@gmail.com", "Ошибка пересчета кол-ва вхождений облигаций в портфели", print_r($e->getMessage(), true));
    var_dump("Cportfolio->getObligationsCntInPortfolios: " . $e->getMessage());
}

$finish = microtime(true);

$delta = $finish - $start;

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean("obligations_data");
global $CACHE_MANAGER;
$CACHE_MANAGER->ClearByTag("ru_obligations_candle");

$res = new Obligations();
$res->getTable(1);
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));		
echo $delta . ' сек.';