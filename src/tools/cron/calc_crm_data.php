<? //Пересчет значений кол-ва вебинаров и продуктов для CRM в 5 часов утра ежедневно
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 325283;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

//Авторизуемся под админом, иначе класс CRM не допускает к данным
$USER->Authorize(1);

CModule::IncludeModule("iblock");



$cdb = new CrmDataBase;

//Пересчет кол-ва вебинаров
//$cdb->setWebinarsCount();


 bitrix\Main\Loader::includeModule("iblock");
 $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_USER");
 //$arFilter = Array("IBLOCK_ID"=>IntVal(19), "!=PROPERTY_USER"=>false, "<=PROPERTY_USER"=>7300);
 $arFilter = Array("IBLOCK_ID"=>IntVal(19), "!=PROPERTY_USER"=>false,);
 //$arFilter = Array("IBLOCK_ID"=>IntVal(19), "PROPERTY_USER"=>array(7352, 7307), )
 $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
 //$res = CIBlockElement::GetList(Array(), $arFilter, false, array("nTopCount"=>1000), $arSelect);
 $arUserList = array();
 while($ob = $res->fetch()){
 	$arUserList[$ob["PROPERTY_USER_VALUE"]] = $ob["PROPERTY_USER_VALUE"];
	//$cdb->setUsersProducts(array("=PROPERTY_USER"=>$ob["PROPERTY_USER_VALUE"]), true);
 }
	$cnt = 0;
foreach($arUserList as $kUid=>$uid){
	  //	if($cnt<=1000)
		 $cdb->setUsersProducts(array("=PROPERTY_USER"=>$kUid), true);
	  	$cnt++;
}

unset($cdb, $res);



 \Bitrix\Main\Config\Option::set("grain.customsettings","ACTUAL_DATE", (new DateTime(date()))->format("d.m.Y H:i:s") );

$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));