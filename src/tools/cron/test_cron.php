<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Образец файла для крон с записью статистики в БД */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 182046;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);
$maxI = 49;

for($i=0; $i<=$maxI; $i++){
	sleep(1);
}


$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";
if(count($arResults)>0){
	foreach($arResults as $k=>$v){
		echo $k." капа=".$v["CAP"]." результат: ".$v["RESULT"]."\n";
	}
}
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
