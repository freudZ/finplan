<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
/* Пересчет коэффициентов портфелей */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
@set_time_limit(0);

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 308563;
$cCronTools->changeStartTime($cronTaskId);

$start = microtime(true);

if (CModule::IncludeModule("iblock")){

	//Подключаем класс портфелей
	$portfolio = new CPortfolio();
	$portfolio->setKoeffDatesList();//формируем список дат для расчета коэффициентов
	//Получаем все активные портфели
	$PortfolioIbId = 52;
	$arSelect = Array("ID", "NAME", "IBLOCK_ID");
   $arFilter = Array("IBLOCK_ID"=>$PortfolioIbId, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
   $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
   while($ob = $res->fetch()){
	  $portfolio->setIblockPortfolioCoeff($ob['ID']);

/*	  $portfolioKoeff = $portfolio->getKoeffPortfolio($ob['ID']);

	  //записываем рассчитанные коэффициенты в инфоблок портфеля
	  //Шарп
	  if(!empty($portfolioKoeff["RUS"]["SHARP"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["RUS"]["SHARP"]["PORTFOLIO"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_SHARP_RUS_P" => floatval($portfolioKoeff["RUS"]["SHARP"]["PORTFOLIO"])) );
	  }
	  if(!empty($portfolioKoeff["RUS"]["SHARP"]["INDEX"]) && !is_nan($portfolioKoeff["RUS"]["SHARP"]["INDEX"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_SHARP_RUS_I" => floatval($portfolioKoeff["RUS"]["SHARP"]["INDEX"])) );
	  }
	  if(!empty($portfolioKoeff["USA"]["SHARP"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["USA"]["SHARP"]["PORTFOLIO"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_SHARP_USA_P" => floatval($portfolioKoeff["USA"]["SHARP"]["PORTFOLIO"])) );
	  }
	  if(!empty($portfolioKoeff["USA"]["SHARP"]["INDEX"]) && !is_nan($portfolioKoeff["USA"]["SHARP"]["INDEX"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_SHARP_USA_I" => floatval($portfolioKoeff["USA"]["SHARP"]["INDEX"])) );
	  }

	  //Трейнор
	  if(!empty($portfolioKoeff["RUS"]["TREYNOR"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["RUS"]["TREYNOR"]["PORTFOLIO"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_TREYNOR_RUS_P" => floatval($portfolioKoeff["RUS"]["TREYNOR"]["PORTFOLIO"])) );
	  }
	  if(!empty($portfolioKoeff["RUS"]["TREYNOR"]["INDEX"]) && !is_nan($portfolioKoeff["RUS"]["TREYNOR"]["INDEX"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_TREYNOR_RUS_I" => floatval($portfolioKoeff["RUS"]["TREYNOR"]["INDEX"])) );
	  }
	  if(!empty($portfolioKoeff["USA"]["TREYNOR"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["USA"]["TREYNOR"]["PORTFOLIO"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_TREYNOR_USA_P" => floatval($portfolioKoeff["USA"]["TREYNOR"]["PORTFOLIO"])) );
	  }
	  if(!empty($portfolioKoeff["USA"]["TREYNOR"]["INDEX"]) && !is_nan($portfolioKoeff["USA"]["TREYNOR"]["INDEX"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_TREYNOR_USA_I" => floatval($portfolioKoeff["USA"]["TREYNOR"]["INDEX"])) );
	  }

	  //Сортино
	  if(!empty($portfolioKoeff["RUS"]["SORTINO"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["RUS"]["SORTINO"]["PORTFOLIO"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_SORTINO_RUS_P" => floatval($portfolioKoeff["RUS"]["SORTINO"]["PORTFOLIO"])) );
	  }
	  if(!empty($portfolioKoeff["RUS"]["SORTINO"]["INDEX"]) && !is_nan($portfolioKoeff["RUS"]["SORTINO"]["INDEX"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_SORTINO_RUS_I" => floatval($portfolioKoeff["RUS"]["SORTINO"]["INDEX"])) );
	  }
	  if(!empty($portfolioKoeff["USA"]["SORTINO"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["USA"]["SORTINO"]["PORTFOLIO"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_SORTINO_USA_P" => floatval($portfolioKoeff["USA"]["SORTINO"]["PORTFOLIO"])) );
	  }
	  if(!empty($portfolioKoeff["USA"]["SORTINO"]["INDEX"]) && !is_nan($portfolioKoeff["USA"]["SORTINO"]["INDEX"])){
		 CIBlockElement::SetPropertyValuesEx($ob['ID'], $PortfolioIbId, array("CF_SORTINO_USA_I" => floatval($portfolioKoeff["USA"]["SORTINO"]["INDEX"])) );
	  }*/

   }




}



$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));
