<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Данный файл обновляет цены всех акций США из HL блока на указанную дату

 */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

$start = microtime(true);

$addNewPrices = false;  //Добавлят не существующие цены

$laspriceDate = '';
$laspriceDate = '13.05.2021';//Если требуется обновлять цены в инфоблоке акций США то указываем дату за которую цены закрытия считать последними

$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_SECID");
//$arFilter = Array("IBLOCK_ID"=>IntVal(55), "=PROPERTY_SECID"=>"AAPL");   //Для обновления и добавления по тикеру конкретной компании
$arFilter = Array("IBLOCK_ID"=>IntVal(55)); //Для обновления и добавления по всем компаниям

			$hlblock   = HL\HighloadBlockTable::getById(29)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();
			$resHL = $entityClass::getList(array(
				"select" => array(
					"*",
				),
				"filter" => array("=UF_DATE"=>$laspriceDate),
				"order" => array("UF_DATE"=>"ASC"),
			));

 $arHLPrices = array();
 while($hl = $resHL->fetch()){
	$arHLPrices[$hl["UF_ITEM"]] = $hl["UF_CLOSE"];
 }

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){
 if(!empty($ob["PROPERTY_SECID_VALUE"])){
 	//echo 'updated '.$ob["PROPERTY_SECID_VALUE"];
	if(!empty($arHLPrices[$ob["PROPERTY_SECID_VALUE"]])){
		$closePrice = round($arHLPrices[$ob["PROPERTY_SECID_VALUE"]], 2);
	 	CIBlockElement::SetPropertyValues($ob["ID"], 55, str_replace(",", ".", $closePrice), "LASTPRICE");
	}

 }
}




$finish = microtime(true);
$delta = $finish - $start;

echo "Время работы скрипта:" . $delta . " сек.\n";