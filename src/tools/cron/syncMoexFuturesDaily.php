<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
//Синхронизация цен фьючерсов
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
use Bitrix\Highloadblock as HL;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

$_SERVER["SERVER_NAME"]="fin-plan.org";

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_crontools.php" ) ;
$cCronTools = new fptСrontools;
$cronTaskId = 365803;
$cCronTools->changeStartTime($cronTaskId);
$start = microtime(true);

$sync = new MoexSync();

//Пишем константу времени обновления для показа на страницах акций
\Bitrix\Main\Config\Option::set("grain.customsettings","FUTURES_PRICES_DATE", (new DateTime())->format('d.m.Y H:i'));
$arFilter = Array("IBLOCK_ID"=>70, "!PROPERTY_SECID" => false);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, ["IBLOCK_ID", "ID", "CODE", "PROPERTY_SECID", "PROPERTY_BOARDID", "PROPERTY_ASSETCODE", "PROPERTY_MOEX_LOADED"]);
while($item = $res->GetNext()){
    $sync->processFutures(
        $item,
        $item["PROPERTY_SECID_VALUE"],
        $item["PROPERTY_MOEX_LOADED_VALUE"]?true:false
    );
	 $sync->updateFuturesPrice($item["ID"], 70, $item["PROPERTY_SECID_VALUE"]);
}

$finish = microtime(true);
$delta = $finish - $start;

//mail("alphaprogrammer@gmail.com","CRON: syncMoexDaily", "Время работы скрипта:" . $delta . " сек.");
echo "Время работы скрипта:" . $delta . " сек.\n";
$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta,2));