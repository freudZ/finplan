<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
//Пересчитывает текущую капитализацию по компаниям в валюте
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

require($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_tools.php" ) ;

$start = microtime(true);

CModule::IncludeModule("eremin.finplantools");

$fptTools = new fptTools();
 $arResults = array();
 $arCompanyPages  = $fptTools->getCompanyList();//Инициализируем выборку и заполнение свойств класса
if(count($fptTools->arCompanyList)>0){
  //	 mail('alphaprogrammer@gmail.com', 'My Subject2', 'test');
  foreach($fptTools->arCompanyList as $company_key=>$arCompany){
	 $secid = $fptTools->arActions[$arCompany["COMPANY_ID"]];
	 $curCap = $fptTools->GetCurCap($secid);

	 $ret = $fptTools->saveCurrentCap($arCompany["COMPANY_ID"], $curCap);

	 if(!empty($ret["result"])){
		$arResults[$company_key."_".$secid] = array("CAP"=>$curCap,"RESULT"=>$ret["result"]);
	 }
  }

}


$finish = microtime(true);
$delta = $finish - $start;

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cache->clean("actions_data");

$res = new Actions();
$res->getTable(1);

echo "Время работы скрипта:" . $delta . " сек.\n";
$result_text = '';
if(count($arResults)>0){
	foreach($arResults as $k=>$v){
		$result_text.= $k." капа=".$v["CAP"]." результат: ".$v["RESULT"]."\n";
		echo $k." капа=".$v["CAP"]." результат: ".$v["RESULT"]."\n";
	}
}
mail("alphaprogrammer@gmail.com", "cap_current_formula end", "Время работы скрипта:" . $delta . " сек.\n Результаты:". $result_text);