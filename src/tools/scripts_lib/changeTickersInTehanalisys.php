<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?  //Замена тикера в сохранениях теханализа
use Bitrix\Highloadblock as HL;
CModule::IncludeModule('highloadblock');
$APPLICATION->SetTitle("Замена тикера в сохранениях теханализа");?>

<?
$formSended = isset($_REQUEST["sendForm"])?true:false;
$mode = isset($_REQUEST["sendForm"]);
$finded = 0;
if($formSended){
$hlblock = HL\HighloadBlockTable::getById(40)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entityClass = $entity->getDataClass();
$oldTicker = 'AAXN';
$newTicker = 'AXON';
$arUserlist = array();
 global $USER;
$arFilter = array('UF_DATA'=>'%'.$oldTicker.'_%');
		$res         = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array(
				"*",
			),
			//"limit" => 1,
			//"group"=>array("UF_ITEM"),
			//"cache" => array("ttl" => 3600)
		));

      while ($item = $res->fetch()) {

		 if(strpos($item["UF_DATA"], $oldTicker)!==false){
		 	$finded++;
			$saveTxt = $item["UF_DATA"];

  $rsUser = CUser::GetByID($item["UF_USER"]);
  $arUser = $rsUser->Fetch();
			$arUserlist[$arUser["LOGIN"]][] = $arUser["LOGIN"];
			if($_REQUEST["go_replace"]=="Y"){
		 	$saveTxt = str_replace($oldTicker.'_', $newTicker.'_', $saveTxt);
			 	$entityClass::update($item["ID"], array("UF_DATA"=>$saveTxt));
		 }

		 }
		}

		}
 ?>
<div class="container">
 <h1><? $APPLICATION->ShowTitle(false); ?></h1>
 <p><span  style="color: #FF0000">Не использовать без необходимости и бекапа хайлоад блока с сохранениями теханализа!!</span></p>
 <div class="row">
  <div class="col-md-6">
<form id="portfolio_repair" action="" name="portfolio_repair">
 <input type="hidden" name="sendForm" value="Y">
 <label>Старый тикер</label><input type="text" name="oldTicker" id="oldTicker" value="<?=(!empty($_REQUEST["oldTicker"])?$_REQUEST["oldTicker"]:'')?>"><br>
 <label>Новый тикер</label><input type="text" name="newTicker" id="newTicker" value="<?=(!empty($_REQUEST["newTicker"])?$_REQUEST["newTicker"]:'')?>"><br>
 <p>Найдено сохранений со старым тикером: <?=$finded?></p>
 <input type="checkbox" value="Y" name="go_replace"><label>&nbsp;Заменить тикеры&nbsp;</label>
 <br>
 <button type="submit" value="replace">Проверить/Заменить тикеры новыми</button>

</form>
 <?if($finded && isset($_REQUEST["go_replace"])):?>
	  <h2>Указанные тикеры заменены</h2>
 <?endif;?>
  </div>
  <div class="col-md-6">
   <ul>
	 <?foreach($arUserlist as $k=>$v):?>
	  <li><?= $k ?> (<?= count($v) ?>)</li>
	 <?endforeach;?>
	</ul>
  </div>
 </div>

</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>