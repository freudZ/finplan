<?php
register_shutdown_function('error_alert');

function error_alert()
{
	if(is_null($e = error_get_last()) === false)
	{
		if($e["type"] !== E_NOTICE && $e["type"] !== E_STRICT) // не будем отправлять предупреждения
			mail('alphaprogrammer@gmail.com', 'Error from auto_prepend', print_r($e, true));

	}
}

?>