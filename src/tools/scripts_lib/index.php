<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("Системные скрипты");?>
 <div class="container">
 	<h1><? $APPLICATION->ShowTitle(); ?></h1>
	<p><span style="color: #FF0000">Если Вы не уверены в своих действиях - обратитесь к администратору сайта!!</span></p>
	<div class="row">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "inpage_opened_menu", Array(
	"ROOT_MENU_TYPE" => "local",	// Тип меню для первого уровня
		"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MAX_LEVEL" => "2",	// Уровень вложенности меню
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"COMPONENT_TEMPLATE" => ".default",
		"CHILD_MENU_TYPE" => "local",	// Тип меню для остальных уровней
		"MENU_THEME" => "green"
	),
	false
);?>
 </div>
 </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>