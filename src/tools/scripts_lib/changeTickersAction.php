<? //Удаление таблицы бекапа через ajax
if($_POST["ajax"]=="y" && $_POST["action"]=="deleteBackupTable" && !empty($_POST["name"])){
  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
  Global $DB, $USER;
  $arGroups = $USER->GetUserGroupArray();
	if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
		exit();
	}
  $queryDrop = "DROP TABLE `".htmlspecialchars($_POST["name"])."`";
  $DB->query($queryDrop);
  echo json_encode(array("table_name"=>$_POST["name"]));
  die();
} ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
  global $USER;
  $arGroups = $USER->GetUserGroupArray();
	if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
		exit();
	}
@set_time_limit(0);
//Замена тикера акции в сохранениях ТА, портфелях и котировках по акциям РФ и США
use Bitrix\Highloadblock as HL;
CModule::IncludeModule('highloadblock');
Global $DB;
$APPLICATION->SetTitle("Замена тикера акций в сохранениях ТА, портфелях и котировках по акциям РФ и США");?>

<?



$formSended = isset($_REQUEST["sendForm"])?true:false;
$mode = isset($_REQUEST["sendForm"]);
$findedTA = 0;



$hlblock = HL\HighloadBlockTable::getById(40)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entityClass = $entity->getDataClass();

if($formSended){

$oldTicker = $_REQUEST["oldTicker"];
$newTicker = $_REQUEST["newTicker"];
 $arReplaced = array('portf_deals'=>'', 'portf_history'=>'', 'ta'=>'', 'prices_ru'=>'', 'prices_usa'=>'');

/* +portfolio */
	$querySelectH = "SELECT count(`ID`) as CNT FROM `portfolio_history` WHERE `UF_SECID` = '".$oldTicker."' ";
	$resPortfolioHistory = $DB->query($querySelectH);
	$PortfHistoryCnt = $resPortfolioHistory->fetch();
/* -portfolio */

/* +action prices */
	$querySelectRU = "SELECT count(`ID`) as CNT FROM `hl_moex_actions_data` WHERE `UF_ITEM` = '".$oldTicker."' ";
	$resActionsPricesRU = $DB->query($querySelectRU);
	$actPricesRU= $resActionsPricesRU->fetch();


	$tableNameUsaPrices = $APPLICATION->usaPricesHlId==29?'hl_spb_actions_data':'hl_polygon_actions_data';
	$tableNameUsaPrices_backup = $tableNameUsaPrices.'_backup_';
	$querySelectUSA = "SELECT count(`ID`) as CNT FROM `$tableNameUsaPrices` WHERE `UF_ITEM` = '".$oldTicker."' ";
	$resActionsPricesUSA = $DB->query($querySelectUSA);
	$actPricesUSA = $resActionsPricesUSA->fetch();
/* -action prices */

/* + backups */
 if($_REQUEST["go_replace"]=="Y" && !empty($newTicker)){
 	//Для ТА проверяем заранее дополнительно нужно ли делать бекап таблицы
	$arFilter = array('UF_DATA'=>'%'.$oldTicker.'_%');
		$res         = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array(
				"*",
			),
		));
      if ($item = $res->fetch()) {
			$querySelect = "CREATE TABLE `".$DB->DBName.".hl_ta_graph_data_backup_".$oldTicker."` SELECT * FROM ".$DB->DBName.".hl_ta_graph_data";
			$DB->query($querySelect);
		}
	 if($PortfHistoryCnt["CNT"]>0){
	 	   $querySelect = "CREATE TABLE `".$DB->DBName.".portfolio_history_backup_".$oldTicker."` SELECT * FROM ".$DB->DBName.".portfolio_history";
			$DB->query($querySelect);
	 }
	 if($actPricesRU["CNT"]>0){
	 		$querySelect = "CREATE TABLE `".$DB->DBName.".hl_moex_actions_data_backup_".$oldTicker."` SELECT * FROM ".$DB->DBName.".hl_moex_actions_data";
			$DB->query($querySelect);
	 }

	 if($actPricesUSA["CNT"]>0){
	 		$querySelect = "CREATE TABLE `".$DB->DBName.".$tableNameUsaPrices_backup".$oldTicker."` SELECT * FROM ".$DB->DBName.".$tableNameUsaPrices";
			$DB->query($querySelect);
	 }
 }
	//echo $DB->DBName;
	$backupQuery = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='".$DB->DBName."' and TABLE_NAME like '%backup_".$oldTicker."%' ";

	$resBackupTables = $DB->query($backupQuery);
	while($row = $resBackupTables->fetch()){
	  $arFindedBackups[] = $row["TABLE_NAME"];
	}


/* - backups */




//+TA
$arUserlist = array();
 global $USER;
$arFilter = array('UF_DATA'=>'%'.$oldTicker.'_%');
		$res         = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array(
				"*",
			),
		));

      while ($item = $res->fetch()) {

		 if(strpos($item["UF_DATA"], $oldTicker)!==false){
		 	$findedTA++;
			$saveTxt = $item["UF_DATA"];

  $rsUser = CUser::GetByID($item["UF_USER"]);
  $arUser = $rsUser->Fetch();
			$arUserlist[$arUser["LOGIN"]][] = $arUser["LOGIN"];
			if($_REQUEST["go_replace"]=="Y" && !empty($newTicker)){
		 	$saveTxt = str_replace($oldTicker.'_', $newTicker.'_', $saveTxt);
			$entityClass::update($item["ID"], array("UF_DATA"=>$saveTxt));
			$arReplaced['ta'] +=1;
		 }

		 }
		}
//-TA

//+Portfolio
		if($_REQUEST["go_replace"]=="Y" && !empty($newTicker) && $PortfHistoryCnt["CNT"]>0){
	   //История
		$DB->StartTransaction();
		$DB->Update("portfolio_history", array("UF_SECID"=>"'".$newTicker."'"), "WHERE UF_SECID='".$oldTicker."'", $err_messH.__LINE__);
		if (strlen($err_messH)<=0)
        {
            $DB->Commit();
				$arReplaced['portf_history'] = $PortfHistoryCnt["CNT"];
        }
        else {
        	$DB->Rollback();
			$arReplaced['portf_history'] = $err_messH;
			}

	}

//-Portfolio

//+prices
	if($_REQUEST["go_replace"]=="Y" && !empty($newTicker) && $actPricesRU["CNT"]>0){
		//Сделки  TODO После переноса убрать update для сделок, т.к. не будет использоваться этот инфоблок
		$DB->StartTransaction();
		$DB->Update("hl_moex_actions_data", array("UF_ITEM"=>"'".$newTicker."'"), "WHERE UF_ITEM='".$oldTicker."'", $err_messRU.__LINE__);
		if (strlen($err_messRU)<=0)
        {
            $DB->Commit();
				$arReplaced['prices_ru'] = $actPricesRU["CNT"];
        }
        else {
        	$DB->Rollback();
				$arReplaced['prices_ru'] = $err_messRU;
		  }
		}

		if($_REQUEST["go_replace"]=="Y" && !empty($newTicker) && $actPricesUSA["CNT"]>0){
	   //История
		$DB->StartTransaction();
		$DB->Update($tableNameUsaPrices, array("UF_ITEM"=>"'".$newTicker."'"), "WHERE UF_ITEM='".$oldTicker."'", $err_messUSA.__LINE__);
		if (strlen($err_messUSA)<=0)
        {
            $DB->Commit();
				$arReplaced['prices_usa'] = $actPricesUSA["CNT"];
        }
        else {
        	$DB->Rollback();
			$arReplaced['prices_usa'] = $err_messUSA;
			}

	}

//-prices


		}
 ?>
<div class="container">
 <h1><? $APPLICATION->ShowTitle(false); ?></h1>
 <p>
 	<span  style="color: #FF0000">Не использовать без необходимости и бекапа хайлоад блоков портфелей и теханализа!!</span>
	<br><br>
	Внимание!!! Копии таблиц со старым тикером будут созданы автоматически, после проверки замены их нужно удалить вручную используя кнопку [X] возле каждой копии в таблице бекапов ниже.
 </p>
 <p>
  Регламент работ:<br>
  <ol>
	<li>Ввести старый тикер в соответствующее поле</li>
	<li>Убедиться в том, что не нажата галка "Заменить тикер"</li>
	<li>Нажать кнопку "Проверить/Заменить тикер новым"</li>
	<li>Если требуется замена тикера ввести новый тикер в соответствующее поле</li>
	<li>Если требуется замена тикера установить галку "Заменить тикер"</li>
	<li>Нажать кнопку "Проверить/Заменить тикер новым" (с установленной галкой произойдет замена тикера с созданием резервных копий таблиц БД)</li>
	<li>После замены галка "Заменить тикер" будет не установлена, нажать "Проверить/Заменить тикер новым" для повторной проверки старого тикера</li>
	<li>Проверить в публичном разделе сайта результаты замены (сделать сброс соответствующего вида кеша ПРИ НЕОБХОДИМОСТИ)</li>
	<li>Если замена прошла успешно удалить резервные копии таблиц для старого тикера кнопкой [X] возле каждой копии в таблице бекапов</li>
  </ol><br>
  <span  style="color: #FF0000">Если что-то пошло не так - срочно обратиться к системному администратору</span>
 </p>
 <div class="row">
  <div class="col-md-6">
<form id="portfolio_repair" action="" name="portfolio_repair">
 <input type="hidden" name="sendForm" value="Y">
 <label>Старый тикер</label><input type="text" name="oldTicker" id="oldTicker" value="<?=(!empty($_REQUEST["oldTicker"])?$_REQUEST["oldTicker"]:'')?>"><br>
 <label>Новый тикер</label><input type="text" name="newTicker" id="newTicker" value="<?=(!empty($_REQUEST["newTicker"])?$_REQUEST["newTicker"]:'')?>"><br>
 <hr>
 <p>Текущая база цен акций США: <?=($APPLICATION->usaPricesHlId==29?'СПБ':'Polygon')?></p>
 <p>Найдено сохранений ТА со старым тикером: <?=$findedTA?></p>
 <p>Найдено сделок со старым тикером в портфелях: <?=$PortfDealsCnt['CNT']?></p>
 <p>Найдено записей со старым тикером в истории портфелей: <?=$PortfHistoryCnt['CNT']?></p>
 <p>Найдено котировок акций РФ со старым тикером: <?=$actPricesRU['CNT']?></p>
 <p>Найдено котировок акций США со старым тикером: <?=$actPricesUSA['CNT']?></p>
 <hr>
 <input type="checkbox" value="Y" name="go_replace"><label>&nbsp;Заменить тикер&nbsp;</label>
 <br>
 <button type="submit" value="replace">Проверить/Заменить тикер новым</button>

</form>
 <?
 if(isset($_REQUEST["go_replace"])):?>
	  <h2>Указанные тикеры заменены</h2>
	  <ul>
		<?foreach($arReplaced as $k=>$v):?>
		  <? $name = '';
		  switch ($k) {
		    case 'portf_deals':
		        $name = 'В сделках портфелей';
		        break;
		    case 'portf_history':
		        $name = 'В записях истории портфелей';
		        break;
		    case 'ta':
		        $name = 'В сохранениях ТА';
		        break;
		    case 'prices_ru':
		        $name = 'В Котировках акций РФ';
		        break;
		    case 'prices_usa':
		        $name = 'В Котировках акций США';
		        break;
			} ?>

		  <li><?=$name?>: <?=empty($v)?'-':$v?></li>
		<?endforeach;?>
	  </ul>
 <?endif;?>
  </div>
  <div class="col-md-6">
   <h3>Пользователи ТА</h3>
   <ul>
	 <?foreach($arUserlist as $k=>$v):?>
	  <li><?= $k ?> (<?= count($v) ?>)</li>
	 <?endforeach;?>
	</ul>
	<hr>
	<h3>Копии таблиц БД</h3>
   <ul>
	 <?foreach($arFindedBackups as $k=>$v):?>
	  <li class="db_backup db_backup_<?=$v?>"><?= $v ?> <button class="db_backup_del" data-table="<?=$v?>">X</button></li>
	 <?endforeach;?>
	</ul>

  </div>
 </div>
	<script>
	$(document).ready(function(){
		$('.db_backup_del').on('click', function(){
		  		$.ajax({
        		type: "POST",
        		url: location,
        		dataType: "json",
        		data:{ajax: "y", action: 'deleteBackupTable', 'name': $(this).attr('data-table')},
        		cashe: false,
        		async: false,
        		success: function(data){
        			$('li.db_backup_'+data.table_name).detach();
        		}
        		});

      });


   });
   </script>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>