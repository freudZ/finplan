<?//Поиск кривых записей в CRM

if($_POST["ajax"]=="y" && (isset($_POST["go_replace"]) && $_POST["go_replace"]=="Y") &&
($_POST["action"]=="remap_user" || $_POST["action"]=="unlink_user") && count($_POST["remapUser"])){
	define("NO_KEEP_STATISTIC", true);
	define("NOT_CHECK_PERMISSIONS",true);
	define('CHK_EVENT', true);
  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
  Global $USER;
  $arGroups = $USER->GetUserGroupArray();
	if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
		exit();
	}
  CModule::IncludeModule("iblock");

  foreach($_POST["remapUser"] as $k=>$v){

	  if(empty($v) && $_POST["action"]=="remap_user") continue;
  		CIBlockElement::SetPropertyValues($k, 19, false,"USER");
                //Очищаем продукты если ни одной продажи не найдено
                CIBlockElement::SetPropertyValues(intval($k), 19, "", "DATE_FIRST_BUY");
                CIBlockElement::SetPropertyValues(intval($k), 19, "", "DATE_LAST_BUY");
                CIBlockElement::SetPropertyValues(intval($k), 19, "", "BUY_IN_MONTH");
                CIBlockElement::SetPropertyValuesEx(intval($k), 19,
                  array("PRODUCTS" => array("VALUE" => array("del" => "Y"))));
                CIBlockElement::SetPropertyValuesEx(intval($k), 19, array("HAVE_ALL_PRODUCTS" => ''));
                CIBlockElement::SetPropertyValuesEx(intval($k), 19,
                  array("PRODUCTS_PROPS" => array("VALUE" => array("del" => "Y"))));

		if($_POST["action"]=="remap_user"){
			$cdb = new CrmDataBase(false);
			$result = $cdb->setUsersProducts(array("=PROPERTY_USER"=>$v));
			unset($cdb);
			}
  }

  echo json_encode(array("remapUser"=>print_r($_POST["remapUser"], true)));
  die();
}

//Удаление отмеченных
if($_POST["ajax"]=="y" && (isset($_POST["go_replace"]) && $_POST["go_replace"]=="Y") && $_POST["action"]=="delete" && count($_POST["toDelete"])){
	define("NO_KEEP_STATISTIC", true);
	define("NOT_CHECK_PERMISSIONS",true);
	define('CHK_EVENT', true);
  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
  Global $USER;
  $arGroups = $USER->GetUserGroupArray();
	if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
		exit();
	}
  CModule::IncludeModule("iblock");
	 Global $DB;
  foreach($_POST["toDelete"] as $v){
	  if(empty($v)) continue;

	 $DB->StartTransaction();
    if(!CIBlockElement::Delete($v))
    {
        $strWarning .= 'Error!';
        $DB->Rollback();
    }
    else
        $DB->Commit();

  }

  echo json_encode(array("toDelete"=>print_r($_POST["toDelete"], true)));
  die();
} ?>



<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
  Global $USER;
  $arGroups = $USER->GetUserGroupArray();
	if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
		exit();
	}

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
CModule::IncludeModule("iblock");
$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_EMAIL", "PROPERTY_USER", "PROPERTY_NAME");
$arFilter = Array("IBLOCK_ID"=>19, '!=PROPERTY_EMAIL'=>'voldr@yandex.ru');
$arFilter[] = array('LOGIC'=>'OR',
array('!?PROPERTY_EMAIL'=>'@'),
array('=PROPERTY_USER'=>""),
array('=PROPERTY_USER'=>4891),
);
//$res = CIBlockElement::GetList(Array(), $arFilter, false, array("nTopCount"=>10), $arSelect);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arResult["STAT"]["ERROR_USER_YASHUK"] = 0;
$arResult["STAT"]["ERROR_USER"] = 0;
$arResult["STAT"]["ERROR_EMAIL"] = 0;
$arResult["CRM"] = array();
$arErrorTypes = array(0=>"ok", 1=>"не привязан к польз.", 2=>"непрвильно привязан", 3=>"Ящук", 4=>"Некорректная почта");
global $USER;
while($ob = $res->fetch()){
	$errorType = 0;
  if(empty($ob["PROPERTY_EMAIL_VALUE"]) || mb_strpos($ob["PROPERTY_EMAIL_VALUE"], "@")){
	  $arResult["STAT"]["ERROR_EMAIL"] = $arResult["STAT"]["ERROR_EMAIL"] + 1;
	  $errorType = 4;
  }
  if(empty($ob["PROPERTY_USER_VALUE"])){
	  $arResult["STAT"]["ERROR_USER"] = $arResult["STAT"]["ERROR_USER"] + 1;
	  $ob["NEW_USER"] = getUserByLoginEmail($ob["PROPERTY_EMAIL_VALUE"]);
	  $errorType = 1;
  }
  if(!empty($ob["PROPERTY_USER_VALUE"]) && !empty($ob["PROPERTY_EMAIL_VALUE"])){
  	    $rsUser = CUser::GetByID($ob["PROPERTY_USER_VALUE"]);
  	    $arUser = $rsUser->Fetch();
		 $ob["USER"] = $arUser;

		 if($ob["PROPERTY_EMAIL_VALUE"]!==$arUser["LOGIN"]){
		 	$ob["NEW_USER"] = getUserByLoginEmail($ob["PROPERTY_EMAIL_VALUE"]);
			$arResult["STAT"]["ERROR_USER"] = $arResult["STAT"]["ERROR_USER"] + 1;
			$errorType = 2;
		 }

  }
  if(!empty($ob["PROPERTY_USER_VALUE"]) && $ob["PROPERTY_USER_VALUE"]==4891){
	  $arResult["STAT"]["ERROR_USER_YASHUK"] = $arResult["STAT"]["ERROR_USER_YASHUK"] + 1;
	  $ob["NEW_USER"] = getUserByLoginEmail($ob["PROPERTY_EMAIL_VALUE"]);
	  $errorType = 3;
  }
  $ob["ERROR_TYPE"] = $errorType;

  $arResult["CRM"][] = $ob;
}
//4891

function getUserByLoginEmail($email){
	$arResult = false;
	     $filter = array("LOGIN_EQUAL" => trim($email));
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
        if ($arUser = $rsUsers->Fetch()) {
            $arResult = $arUser;
        }
	return $arResult;
}

?>
<div class="container">
 <h1>Реестр ошибочных записей в CRM</h1>
 <p>
 	<span  style="color: #FF0000">Использовать с осторожностью!</span>
	<br><br>
  </p>
 <p>
  Регламент работ:<br>
  <i style="color:red">!Важно: отмечть галочками следует однотипные проблемы и потом применять конкретное действие к отмеченным. Будьте внимательны!</i>
  <ol>
	<li>Оценить проблему записи из таблицы</li>
	<li>Если в почте нет символа @ то открыть запись в инфоблоке по ссылке на ее ID, и если запись мусорная - отметить здесь галочкой, затем все подобные записи удаляются в режиме действий "Удалить из CRM"</li>
	<li>Если почта корректная но привязан не тот пользователь и система не предлагает правильного пользователя в правой колонке - отметить галочкой, затем все подобные записи отвязать в режиме действий "Отвязать текущего пользователя"</li>
	<li>Если почта корректная и система предложила привязать правильного пользователя - отметить галочкой, затем все подобные перепривязать в режиме действий "Привязать предложенного пользователя"</li>
  </ol>
  <br>
  <span  style="color: #FF0000">Если сомневаетесь в своих действиях - обратитесь к системному администратору</span>
 </p>
 <div class="row">
  <div class="col-md-6">
<form id="portfolio_repair" action="" name="portfolio_repair">
 <input type="hidden" name="sendForm" value="Y">
<!-- <label>Старый тикер</label><input type="text" name="oldTicker" id="oldTicker" value="<?=(!empty($_REQUEST["oldTicker"])?$_REQUEST["oldTicker"]:'')?>"><br>
 <label>Новый тикер</label><input type="text" name="newTicker" id="newTicker" value="<?=(!empty($_REQUEST["newTicker"])?$_REQUEST["newTicker"]:'')?>"><br>
--> <hr>
 <p>Найдено записей c привязкой к пользователю "Ящук": <?= $arResult["STAT"]["ERROR_USER_YASHUK"]?></p>
 <p>Найдено записей c неправильной привязкой к пользователю: <?= $arResult["STAT"]["ERROR_USER"]?></p>
 <p>Найдено записей с некорректной почтой: <?= $arResult["STAT"]["ERROR_EMAIL"]?></p>
 <hr>
 <label for="select_group_actions">Действия с отмеченными</label>
 <select name="select_group_actions" id="select_group_actions">
  <option value="remap_user">Привязать предложенного пользователя</option>
  <option value="unlink_user">Отвязать текущего пользователя</option>
  <option value="delete">Удалить из CRM</option>

 </select>
 <br><br><br>
 <input type="checkbox" value="Y" name="go_replace" id="go_replace"><label>&nbsp;Исправить если все верно&nbsp;</label>
 &nbsp;
 <button type="button" id="submit_button" value="replace">Проверить/Исправить</button>
 </form>

 <table class="table table-condensed table-bordered table-striped table-responsive">
 <thead>
  <tr>
	<th>ID</th>
	<th>Проблема</th>
	<th>Почта</th>
	<th>Имена</th>
	<th>Привязанный</th>
	<th>Можно привязать</th>
	<th>Обработать</th>
  </tr>
  </thead>
  <? usort($arResult["CRM"], function ($item1, $item2) {
return (mb_strpos($item1["PROPERTY_EMAIL_VALUE"], "@")===false) < (mb_strpos($item2["PROPERTY_EMAIL_VALUE"], "@")===false);
}); ?>
  <?foreach($arResult["CRM"] as $crmItem):?>

  <tr>
	<td><a href="https://fin-plan.org/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=19&type=FINPLAN&lang=ru&ID=<?= $crmItem["ID"] ?>&find_section_section=0&WF=Y" target="_blank"><?= $crmItem["ID"] ?></a></td>
	<td><?= $arErrorTypes[$crmItem["ERROR_TYPE"]] ?></td>
	<td><?= $crmItem["PROPERTY_EMAIL_VALUE"] ?></td>
	<td><?= implode("; ", $crmItem["PROPERTY_NAME_VALUE"]) ?></td>
	<td data-uid="<?= $crmItem["USER"]["ID"] ?>"><?= ("[".$crmItem["USER"]["ID"]."] ".$crmItem["USER"]["LOGIN"]." ".$crmItem["USER"]["NAME"]. " ".$crmItem["USER"]["LAST_NAME"]) ?></td>
	<td data-newUid="<?= $crmItem["NEW_USER"]["ID"] ?>"><?= ("[".$crmItem["NEW_USER"]["ID"]."] ".$crmItem["NEW_USER"]["LOGIN"]." ".$crmItem["NEW_USER"]["NAME"]. " ".$crmItem["NEW_USER"]["LAST_NAME"]) ?></td>
	<td>
	<input type="checkbox" data-crm-id = "<?= $crmItem["ID"] ?>" data-remap-user="<?= $crmItem["NEW_USER"]["ID"] ?>" name="doit[<?= $crmItem["ID"] ?>]" <?if(mb_strpos($crmItem["PROPERTY_EMAIL_VALUE"], "@")!==false):?>checked="checked"<?endif;?>/>
	</td>
	</tr>
  <?endforeach;?>
 </table>

</div>
</div>
</div>

<script>
  $(document).ready(function(){
	 $('#submit_button').on('click', function(){
	 	let action = $("#select_group_actions").val();
	 	let doit = [];
	 	let remapUser = {};
	 	let toDelete = [];
		let goReplace = $('#go_replace').prop('checked');
	 	$('input[name^=doit]:checked').each(function(indx, element){
	 	  doit.push($(element).attr('data-crm-id'));

		  if(action=='remap_user' && $(element).attr('data-remap-user').length>0){
	 	  	remapUser[$(element).attr('data-crm-id')] = $(element).attr('data-remap-user');
		  }
		  if(action=='unlink_user'){
	 	  	remapUser[$(element).attr('data-crm-id')] = $(element).attr('data-remap-user');
		  }
		  if(action=='delete' && $(element).attr('data-crm-id').length>0){
	 	  	toDelete.push($(element).attr('data-crm-id'));
		  }

	 	});

		$.ajax({
    		type: "POST",
    		url: location,
    		dataType: "json",
    		data:{'ajax': 'y',
			'action': action,
			'goReplace': goReplace?'Y':'N',
			'remapUser' : remapUser,
			'toDelete' : toDelete,
			'doit': doit
			},
    		cashe: false,
    		async: false,
    		success: function(data){
    			getResponse(data);
    		}
    		});

    	 function getResponse(data){
    	 	location.reload();
    	 }

		console.log(remapUser);
    });




  });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>