<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Пересчет капитализации акций США */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" ) ;

\Bitrix\Main\Loader::includeModule("iblock");

$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_LASTPRICE", "PROPERTY_SECURITIES_QUANTITY");
$arFilter = Array("IBLOCK_ID"=>IntVal(55));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($item = $res->fetch()){
		//капитализация

	CIBlockElement::SetPropertyValues($item["ID"], 55, round(($item["PROPERTY_LASTPRICE_VALUE"] * $item["PROPERTY_SECURITIES_QUANTITY_VALUE"]), 2), "ISSUECAPITALIZATION");
		echo "Капитализация ".$item["NAME"]." = ".round(($item["PROPERTY_LASTPRICE_VALUE"] * $item["PROPERTY_SECURITIES_QUANTITY_VALUE"]), 2).PHP_EOL;
}

echo "Капитализация акций США пересчитана";
