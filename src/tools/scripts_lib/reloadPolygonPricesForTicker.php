<?//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
/* Получает котировки акций США от провайдера polygon, можно передать определенный тикер предварительно удалив его цены - скрипт перезатянет историю */
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";
//echo $_SERVER["DOCUMENT_ROOT"];
//die();
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" );
require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
$start = microtime(true);
$insertHistory = true;
require $_SERVER["DOCUMENT_ROOT"].'/vendor/polygon-io/api/src/rest/Rest.php';
//require '/var/www/bitrix/data/www/fin-plan.org/vendor/polygon-io/api/src/rest/reference/Reference.php';
//require $_SERVER["DOCUMENT_ROOT"].'/vendor/polygon-io/api/src/rest/common/Mappers.php';
use PolygonIO\Rest\Rest;
\Bitrix\Main\Loader::includeModule("iblock");

$rest = new Rest('ZmCXrWbo2hzVHWQtmyn9UJ3rlGWE3tKL');
$need = true;
$limit = 100;
$ticker = 'SWI';  //Указать нужный тикер
//$ticker = 'INMD';  //Указать нужный тикер
//type = Тип акций CS, ADRC и т.д.
$params = array("market"=>"stocks", "limit"=>$limit, "type"=>"CS");
if(!empty($ticker)){
	$params["ticker"] = $ticker;
} else {
	echo "не указан тикер. скрипт остановлен."; die();
}



$cursor = '';
$count = 0;
$tickerCnt = 0;
$addedRows = 0;
$clearedCandles = 0;
$arTickers = array();
$arCandleTickers = array();
//Получаем существующие тикеры из котировок, что бы не грузить их снова
$query = "SELECT DISTINCT `UF_ITEM` FROM `hl_polygon_actions_data`";
$res = $DB->Query($query);
$arExistTickers = array();
while($row = $res->fetch()){
  $arExistTickers[] = $row["UF_ITEM"];
}


while ($need) {
  if(!empty($cursor)){ //Если передан хеш курсора для следующей страницы - добавляем его в парамтеры и отправляем запрос.
  	$params["cursor"] = $cursor;
  	//$params["type"] = "CS";
  }
  $data = $rest->reference->tickers->get($params);
/*	 echo "<pre  style='color:black; font-size:11px;'>";
       print_r($params);
       print_r($data);
       echo "</pre>";*/
  foreach($data["results"] as $arItem){
	//if(isset($arItem["ticker"]) && !empty($arItem["ticker"]) && !in_array($arItem["ticker"], $arExistTickers)){
	if(isset($arItem["ticker"]) && !empty($arItem["ticker"])){
	   if($insertHistory ){
	   	$tickerCnt++;
			//$arTickers[$arItem["ticker"]] = $arItem["type"];
			$arCandleTickers[] = "'".$arItem["ticker"]."'";   //Собираем тикеры для удаления из свечных графиков текущих записей
	  	 	$addedRows += insertHystoryDataToHL($rest, $arItem["ticker"]); //Добавление котировок. Существующие будут затерты для передаваемого тикера
  		}
	}

  }

	  if(isset($data["next_url"]) && !empty($data["next_url"]) && strpos($data["next_url"], "cursor")!==false){
	  	$cursor = $rest->getCursor($data["next_url"]);
		if(!empty($cursor)){
			$need = true;
		}
	  } else {
	  	$need = false;
	  }
  $count+=$limit;
/*  if($count>20){
	  	$need = false;
  }*/
}

//Перезаписываем в инфоблок акции цены за день, месяц, год и три года назад для корректного отображения динамики на странице
if(isset($params["ticker"]) && !empty($params["ticker"])){
  recalcShareDynamics($params["ticker"]);
}


if(count($arCandleTickers)>0){
	recalcCandleGraph($arCandleTickers);
}


/**
 * Перезаписываем в инфоблок акции цены за день, месяц, год и три года назад для корректного отображения динамики на странице
 *
 * @param  [add type]   $ticker [add description]
 *
 * @return [add type]  [add description]
 */
function recalcShareDynamics($ticker){
	Global $DB, $APPLICATION;
	$ibID = 55;//Инфоблок акций США
	$quotationsData = array();

	$dates = array(
		 'DAY' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P2D')),//Для США lastprice всегда равно цене вчерашней (последней) даты, по этому берем разницу в 2 дня
	    'MONTH' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1M')),
	    'ONE_YEAR' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P1Y')),
	    'THREE_YEAR' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P3Y')),
		 'TWO_DAY' => (new DateTime(date('Y-m-d')))->sub(new DateInterval('P3D')),//Нужен для расчета роста/падения при переходе через выходные и определении цены за день назад
	);
	foreach ($dates as $k => $date) {
		 $dt = $date->format('Y-m-d');
		 $dt_2 = $date->modify('-2 days')->format('Y-m-d');
		 $query = "SELECT `UF_ITEM`, `UF_CLOSE` FROM `hl_polygon_actions_data` WHERE `UF_ITEM` = '$ticker' AND (`UF_DATE` BETWEEN '$dt_2' AND '$dt')";

		 $res = $DB->Query($query);

	    while ($row = $res->fetch()) {

					if(floatval($row['UF_CLOSE'])>0){
					  $quotationsData[$k] = $row['UF_CLOSE'];
					  break;
					}
	    }
	}



//Собираем акции США
$arUsaActions = array();
$arSelectUA = Array("ID",
                "IBLOCK_ID",
                "CODE",
                "NAME",
                "PROPERTY_SECID",
                "PROPERTY_EMITENT_ID",
                "PROPERTY_SECTOR",
                "PROPERTY_LASTPRICE",
                "PROPERTY_SECURITIES_QUANTITY",
                "PROPERTY_SP500",
                "PROPERTY_UNDERSTIMATION",
                "PROPERTY_TARGET_CUSTOM");
$arFilterUA = Array("IBLOCK_ID" => IntVal($ibID), "PROPERTY_SECID"=>$ticker);
$res = CIBlockElement::GetList(Array(), $arFilterUA, false, false, $arSelectUA);

while ($ob = $res->Fetch()) {
    $arUsaActions[$ob["PROPERTY_SECID_VALUE"]] = $ob;
}


		//echo "> Список акций с lastprice для расчета однодневного роста".PHP_EOL;
		//Список акций с lastprice для расчета однодневного роста
		$arSelectInrease = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_LASTPRICE", "PROPERTY_SECID");
		$arFilterInrease = Array("IBLOCK_ID"=>$ibID, "PROPERTY_SECID"=>$ticker, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$resInrease = CIBlockElement::GetList(Array(), $arFilterInrease, false, false, $arSelectInrease);
		$arLastpriceBase = array();
		while($ob = $resInrease->Fetch()){
		 $arLastpriceBase[$ob["PROPERTY_SECID_VALUE"]] = $ob["PROPERTY_LASTPRICE_VALUE"];
		}



            //Динамика акций
            foreach ($dates as $k => $date) {
            	$item = $arUsaActions[$ticker];

            	if($k=="TWO_DAY") continue;//Пропускаем дату два дня назад, нужна только для обсчета роста за 1 день при переходе через выходные
                if (isset($quotationsData[$k])) {
                    CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $quotationsData[$k], "QUOTATIONS_$k");
                    $i++;
                }

					  //Считаем рост/падение для одного дня
					  if($k=="DAY"){
						  	 $actualLastprice = 0;
							 if(isset($quotationsData["DAY"])){
							   $qDay = $quotationsData["DAY"];
							 } else {
								continue;
							 }

							 if( $qDay==floatval($arLastpriceBase[$item["PROPERTY_SECID_VALUE"]])){
							 	$dt = $dates["TWO_DAY"]->format('Y-m-d');
							 	 $queryNowPrice = "SELECT `UF_ITEM`, `UF_CLOSE` FROM `hl_polygon_actions_data` WHERE `UF_ITEM` = '$ticker' AND `UF_DATE` = '$dt')";
								 $res = $DB->Query($queryNowPrice);
/*								 $resultNowPrice = $entity_data_class::getList([
					            "filter" => [
					                "UF_DATE" => $dates["TWO_DAY"],
					                "UF_ITEM" => $item["PROPERTY_SECID_VALUE"],
					            ],
					            "select" => [
					                "UF_CLOSE"
					            ],
					        ])->fetch();*/
							  if($row = $res->fetch()){
							  	$actualLastprice = $row["UF_CLOSE"];//Берем цену за два дня назад
							  }
						   } else {
							  $actualLastprice = $qDay; //Берем текущую цену lastprice
						   }
						//Записываем однодневный прирост
						$oneDayDropIncrease = round(((floatval($arLastpriceBase[$item["PROPERTY_SECID_VALUE"]]) / floatval($actualLastprice) - 1) * 100), 2);
						if(is_infinite($oneDayDropIncrease) || is_nan($oneDayDropIncrease)){
							$oneDayDropIncrease = 0;
						}
						CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $oneDayDropIncrease, "ONE_DAY_DROP_INCREASE");
					  }

            }

}

/**
 * Удаляет из свечных графиков США текущие записи по массиву тикеров, что приведет к пересчету и обновлению графиков 
 *
 * @param  [add type]   $arTickers [add description]
 *
 * @return [add type]  [add description]
 */
function recalcCandleGraph($arTickers){
	Global $DB;
	if(count($arTickers)>0){
		$query = "DELETE FROM `hl_candle_graph_data_usa` WHERE `UF_TICKER` IN (".(implode(", ", $arTickers)).") AND `UF_CURRENT_PERIOD`='Y'";
		$DB->Query($query);
	}
}

function insertHystoryDataToHL($rest, $ticker){
  Global $DB;
  $params = array("limit"=>50000);
  $from = '2015-01-01';
  $to = (new DateTime())->modify('- 1 days')->format('Y-m-d');
  $data = $rest->stocks->aggregates->get($ticker, 1, $from, $to, 'day', $params);
  $addedRowsTmp = 0;
  $query = "DELETE FROM `hl_polygon_actions_data` WHERE `UF_ITEM`='$ticker'";
  $DB->Query($query);

  $sql = "INSERT INTO `hl_polygon_actions_data`(`UF_ITEM`, `UF_DATE`, `UF_OPEN`, `UF_CLOSE`, `UF_HIGH`, `UF_LOW`, `UF_VOLUME`, `UF_SHOW_GRAPH`, `UF_MANUAL`) VALUES ";
  $arsqlvalues = array();
  foreach($data["results"] as $row){
	$date = (new DateTime(date("Y-m-d", intval($row["timestamp"])/1000)))->format('Y-m-d');
	$arsqlvalues[] = "('$ticker', '$date', '".$row['open']."', '".$row['close']."', '".$row['high']."', '".$row['low']."', '".$row['volume']."', 'Y', '')";
	$addedRowsTmp++;
  }

  if(count($arsqlvalues)>0){
  	$sql .= implode(", ", $arsqlvalues);
	$DB->query($sql);
  }
  //echo $sql;
  return $addedRowsTmp;
}
$finish = microtime(true);
$delta = $finish - $start;

echo "Тикеров:" . $tickerCnt . "\n";
echo "Строк котировок:" . $addedRows . "\n";
echo "Сброшено свечных графиков:" . count($arCandleTickers) . "\n";
echo "Время работы скрипта:" . round($delta/60, 2) . " мин.\n";
print_r($arTickers);

 ?>

<?//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>