<?define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
 global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	LocalRedirect("/");
	exit();
}
global $USER;

$CPortfolio = new CPortfolio();
$arAllPortfolioList = $CPortfolio->getPortfolioListAll();
$arNoClientsPortfolioList = array();//Массив портфелей, пользователей с недействующим радаром
$arAllPortfolioOwners = array();//Все владельца портфелей
foreach($arAllPortfolioList as $pid=>$val){
	//Собираем портфели по пользователям
	if(!in_array($pid, $arAllPortfolioOwners[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]))
     $arAllPortfolioOwners[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]][] = $pid;

 $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["PORTFOLIO_ITEMS"][$pid] = $val;
 if(!array_key_exists('ZERO', $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]])){
 	$arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["ZERO"] = 0;
 }
 if(floatval($val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"])<=0)
  $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["ZERO"] += 1;

 $arSummary["CNT_PORTFOLIO"] += 1;
    $portfolioDifferenceSumm = $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"]-$val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
	 if($portfolioDifferenceSumm==0){
	  $arSummary["CNT_ZERO_PORTFOLIO"] += 1;
	 }
}

 //Проверяем пользователей на действующий радар
 $arAllPortfolioOwnersNoRadar = array();
 foreach($arAllPortfolioOwners as $owner=>$pids){
   If(checkPayRadar(false, $owner)==false){
	  $arNoClientsPortfolioList = array_merge($arNoClientsPortfolioList, $pids);
	  $arAllPortfolioOwnersNoRadar[$owner] = $pids;
 }
 }



?>
<?$APPLICATION->SetTitle("Инструменты администрирования");?>
 <div class="container">
 	<h1>Инструменты администрирования</h1>
 	<div class="row">
 		<div class="col-12 ">
 		 <div class="bg-success center-block index_blocks">
		  <h2>Зачистка портфелей</h2>
			 Пустых портфелей: <?= $arSummary["CNT_ZERO_PORTFOLIO"] ?><br>
			 Портфели пользователей без радара: <?= count($arNoClientsPortfolioList); ?>
			 <table class="table table-condensed">
			 <thead>
			  <tr>
				<th>ID, логин, посл.авторизация</th>
				<th>ID портфелей</th>
			  </tr>
			 </thead>
				<?foreach($arAllPortfolioOwnersNoRadar as $ownerNoRadar=>$ownerPids):?>
				  <tr>
					<td>
					  <?

                    $rsUser = CUser::GetByID($ownerNoRadar);
                    $arUser = $rsUser->Fetch();
					  ?>
					  ID: <?=$ownerNoRadar?> <strong><?=$arUser["LOGIN"]?></strong>  <?=$arUser["LAST_LOGIN"]?>
					</td>
					<td>
					 <?foreach($ownerPids as $pid):?>
						 <a href="/portfolio/portfolio_<?=$pid?>" target="_blank"><?=$pid?></a>
					 <?endforeach;?>
					</td>
				  </tr>
				<?endforeach;?>
			 </table>
		 </div>
 		</div>
 	</div>
 </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>