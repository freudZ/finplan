<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	LocalRedirect("/");
	exit();
}

		$hlblock     = HL\HighloadBlockTable::getById(35)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
/*			"filter" => array(
				"UF_EMITENT" => $emitentName,
				"!UF_CODE" => false,
			),*/
			"select" => array("*"),
			"order" => array("UF_LOGIN_DATE"=>"ASC")
		));

	  $arThiefs = array();
	  $arIpTable = array();
	  while($row = $res->fetch()){
		$arThiefs[$row["UF_LOGIN"]][($row["UF_LOGIN_DATE"])->format('d.m.Y H:i:s')][] = $row;
		if(!in_array($row["UF_IP"], $arIpTable[$row["UF_LOGIN"]]))
		   $arIpTable[$row["UF_LOGIN"]][] = $row["UF_IP"];
	  }


?>

	<div class="container">
		<h1>Поиск жуликов</h1>
		<div class="main_content">
			<div class="main_content_inner">
				<div class="btn-group" role="group" aria-label="">

				</div>
				<br><br>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th colspan="4">Логин</th>
						</tr>
					</thead>
					<tbody>
<? ?>
						<?foreach($arThiefs as $login => $arDateList):?>
						  <?// if(count($arIpList)<2) continue; //Если число ip адресов на дату и время входа менее 2 ?>
						  <? $class="";
						     $cntIp = count($arIpTable[$login]);
							  if($cntIp>5 && $cntIp<10){
								 $class = 'bg-warning text-warning';
							  } else if ($cntIp>=10){
								 $class = 'bg-danger text-danger';
							  }
							  $loginCss = str_replace(array("@",".","-","_"),"",$login);
						   ?>
							<tr class="<?= $class?>">
							  <td colspan="4"><a href="javascript:void(0);" style="text-decoration:none;" class="login_roll" data-login="<?= $loginCss ?>" data-alt-text="-">+ <strong><?= $login ?></strong><?=($cntIp>1?' - <span class="red"> разные ip за весь период: <strong>'.$cntIp.'</strong></span>':'')?></a></td>
							</tr>
							<tr class="login-<?=$loginCss?> hidden <?= $class?>">
									<th>Дата входа</th>
									<th>Почта</th>
									<th>IP</th>
									<th>Больше 3 попыток войти</th>
							</tr>
								<?foreach($arDateList as $date=>$arRowList):?>
								  <?foreach($arRowList as $k=>$arRow):?>
										<tr class="login-<?=$loginCss?> hidden <?= $class?>">
											<td><?=$date?></td>
											<td><?=$arRow["UF_EMAIL"]?></td>
											<td><?=$arRow["UF_IP"]?></td>
											<td><?=!empty($arRow["UF_ERROR_AUTH_10"])?"Да":""?></td>
										</tr>
									<?endforeach?>
								<?endforeach?>

						<?endforeach?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
  <script>
  $(document).ready(function(){
	 $('.login_roll').on('click', function(){

		var rowId = $(this).data('login');
		var tmpText = '';
		if($(this).hasClass('showed')){
			$('.login-'+rowId).addClass('hidden');
			$(this).removeClass('showed');
			  //tmpText = $(this).text();
			  // $(this).text($(this).data('alt-text'));
			 //	$(this).data('alt-text', tmpText);
		} else {
			$('.login-'+rowId).removeClass('hidden');
			$(this).addClass('showed');
			 // tmpText = $(this).text();
			 //  $(this).text($(this).data('alt-text'));
			 //	$(this).data('alt-text', tmpText);
		}

    });


  });
  </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>