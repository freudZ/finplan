<?$_SERVER["DOCUMENT_ROOT"] = "/var/www/bitrix/data/www/fin-plan.org";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
 $arResult = array("result"=>"", "message"=>"");
 CModule::IncludeModule("iblock");
$arCachesType['CRMgetProducts'] = 'CRMgetProducts';
/*$arCachesType['actions_usa_data'] = 'actions_usa_data';
$arCachesType['obligations_data'] = 'obligations_data';
$arCachesType['actions_usa_filters_data'] = 'actions_usa_filters_data';
$arCachesType['etf_data'] = 'etf_data';
$arCachesType['etf_filters_data'] = 'etf_filters_data';*/



$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
if($_REQUEST['type']=='allCRM'){
	foreach($arCachesType as $cType){
	  $cache->clean($cType);
	}
	 $arResult["result"] = "clear1";
	 $arResult["message"] = "Весь кэш CRM очищен";
} elseif($_REQUEST['type']=='CRMgetProducts'){ //Очистка списка проукдуктов  $base->getProducts($ids)

  $arSelect = Array("ID", "NAME", "IBLOCK_ID");
  $arFilter = Array("IBLOCK_ID"=>9);
  $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
  while($ob = $res->Fetch()){
	 $cache->clean(htmlspecialchars('CRMgetProducts_'.$ob['ID']));
  }
  $cache->clean(htmlspecialchars('CRMgetProducts_all'));
  
	if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   $GLOBALS['CACHE_MANAGER']->ClearByTag('CRMgetProducts');

  	 $arResult["result"] = "clear1";
	 $arResult["message"] = "Кэш списка продуктовCRM очищен";
} elseif($_REQUEST['type']=='CRM_filters'){
  $cache = Bitrix\Main\Data\Cache::createInstance();
  $cache->cleanDir('/crm_product_filter/');
	 $arResult["result"] = "clear1";
	 $arResult["message"] = "Кэш фильтров CRM очищен";
} elseif($_REQUEST['type']=='getManagers'){
  $cache->clean(htmlspecialchars('getManagers'));
	 $arResult["result"] = "clear1";
	 $arResult["message"] = "Кэш менеджеров CRM очищен";
}
?>
 <div class="container">

 	<?if(!empty($arResult["result"])):?>
		<p><?= $arResult["message"] ?></p>
	<?endif;?>
 	<h1>Управление кешированием CRM</h1>

	<div class="row">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "inpage_level_menu", Array(
	"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
		"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MAX_LEVEL" => "2",	// Уровень вложенности меню
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"COMPONENT_TEMPLATE" => ".default",
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"MENU_THEME" => "green"
	),
	false
);?>
 </div>
 </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>