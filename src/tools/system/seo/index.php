<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<? global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(array(1,5), $arGroups)){
	exit();
} ?>
<?$APPLICATION->SetTitle("СЕО инструменты");?>
 <div class="container">
 	<h1>СЕО инструменты</h1>
	<div class="row">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "inpage_opened_menu", Array(
	"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
		"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"COMPONENT_TEMPLATE" => ".default",
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"MENU_THEME" => "green"
	),
	false
);?>
 </div>
 </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>