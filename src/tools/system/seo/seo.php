<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<? global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(array(1,5), $arGroups)){
	exit();
} ?>
<?$APPLICATION->SetTitle("Index");
  use Bitrix\Iblock\InheritedProperty;
?>
<?
 $actionIblockId = 32; //Акции
 $actionPagesIblockId = 33; //Страницы акций
 $obligIblockId = 27; //Облигации
 $obligPagesIblockId = 30; //Страницы облигаций
 $companyIblockId = 26; //Компании

if (CModule::IncludeModule("iblock")){

if($_REQUEST["type"]=="actionPages"){
$APPLICATION->SetTitle("СЕО инструменты - обновление SEO данных страниц акций и etf");
//Проставление текстового эмитента, расширенного названия и тикера на страницы акций
//Для страниц акций и Etf собираются разные шаблоны, их части лежат в SEO_1 и SEO_2 а между ними подставляется название актива и тикер.
$arCompanies = array();
$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID");
$arFilter = Array("IBLOCK_ID"=>IntVal($companyIblockId));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){
  $arCompanies[$ob["ID"]] = $ob["NAME"];
}

$arActions = array();
$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_IS_ETF_ACTIVE", "PROPERTY_SECID", "PROPERTY_EMITENT_ID");
$arFilter = Array("IBLOCK_ID"=>IntVal($actionIblockId));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){
  $emitent = $arCompanies[$ob["PROPERTY_EMITENT_ID_VALUE"]];
  $seoType = $ob["PROPERTY_IS_ETF_ACTIVE_VALUE"]==true?'Etf':'Акции';
  $seo1 = $ob["PROPERTY_IS_ETF_ACTIVE_VALUE"]==true?'Купить':'Цена '.strtolower($seoType);
  $seo2 = $ob["PROPERTY_IS_ETF_ACTIVE_VALUE"]==true?'на Московской бирже - Эмитент '.$emitent.' - цена на сегодня - график и дивиденды 2020':'- стоимость ценных бумаг на сегодня - график и дивиденды 2020';
  $seoName = $ob["NAME"];

  if(strpos($ob["NAME"]," ао")!==false){
    $seoName = str_replace(" ао", " обыкновенные", $ob["NAME"]);
  } else if(strpos($ob["NAME"]," ап")!==false){
    $seoName = str_replace(" ап", " привилегированные", $ob["NAME"]);
  }
  $arActions[$ob["CODE"]] = array("SEO_1"=>$seo1, "SEO_2"=>$seo2, "SEO_TYPE"=>$seoType, "SEO_TITLE_FULL"=>$seoName, "SEO_TICKER"=> $ob["PROPERTY_SECID_VALUE"]);
}


$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID");
$arFilter = Array("IBLOCK_ID"=>IntVal($actionPagesIblockId));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$cnt = 0;
while($ob = $res->fetch()){
	if(array_key_exists($ob["CODE"], $arActions)){
	CIBlockElement::SetPropertyValues($ob["ID"], $actionPagesIblockId, $arActions[$ob["CODE"]]["SEO_TYPE"], "SEO_TYPE");
	CIBlockElement::SetPropertyValues($ob["ID"], $actionPagesIblockId, $arActions[$ob["CODE"]]["SEO_TITLE_FULL"], "SEO_TITLE_FULL");
	CIBlockElement::SetPropertyValues($ob["ID"], $actionPagesIblockId, $arActions[$ob["CODE"]]["SEO_TICKER"], "SEO_TICKER");
	CIBlockElement::SetPropertyValues($ob["ID"], $actionPagesIblockId, $arActions[$ob["CODE"]]["SEO_1"], "SEO_1");
	CIBlockElement::SetPropertyValues($ob["ID"], $actionPagesIblockId, $arActions[$ob["CODE"]]["SEO_2"], "SEO_2");
	$ipropValues = new InheritedProperty\ElementValues($actionPagesIblockId, $ob["ID"]);  //Сброс кеша для SEO элемента
   $ipropValues->clearValues();
	$cnt++;
	}
}

echo "<p>Обновлены SEO данные страниц акций РФ и etf (кол-во элементов): ".$cnt."</p>";

} //actionPages


if($_REQUEST["type"]=="obligPages"){
$APPLICATION->SetTitle("СЕО инструменты - обновление SEO данных страниц облигаций");

//Проставление текстового эмитента на страницы облигаций
$arOblig = array();
$arCompanies = array();
$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID");
$arFilter = Array("IBLOCK_ID"=>IntVal($companyIblockId));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){
  $arCompanies[$ob["ID"]] = $ob["NAME"];
}


$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_EMITENT_ID");
$arFilter = Array("IBLOCK_ID"=>IntVal($obligIblockId));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->fetch()){
  $arOblig[$ob["CODE"]] = $arCompanies[$ob["PROPERTY_EMITENT_ID_VALUE"]];
}


$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID");
$arFilter = Array("IBLOCK_ID"=>IntVal($obligPagesIblockId));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$cnt = 0;
while($ob = $res->fetch()){
	CIBlockElement::SetPropertyValues($ob["ID"], $obligPagesIblockId, $arOblig[$ob["CODE"]], "EMITENT_NAME");
	$ipropValues = new InheritedProperty\ElementValues($obligPagesIblockId, $ob["ID"]);  //Сброс кеша для SEO элемента
   $ipropValues->clearValues();
	$cnt++;
}

echo "<p>Обновлены SEO данные страниц облигаций (кол-во элементов): ".$cnt."</p>";

}//obligPages


switch ($_REQUEST["type"]) {
    case "clearSeoActionsPageTitles": // очистка "Заголовок элемента" страниц акций
        clearSeoElementPararms($actionPagesIblockId, "ELEMENT_PAGE_TITLE");
        break;
    case "clearSeoActionsMetaTitles": // очистка "Шаблон META TITLE" страниц акций
        clearSeoElementPararms($actionPagesIblockId, "ELEMENT_META_TITLE");
        break;
    case "clearSeoObligPageTitles": // очистка "Заголовок элемента" страниц облигаций
        clearSeoElementPararms($obligPagesIblockId, "ELEMENT_PAGE_TITLE");
        break;
    case "clearSeoObligMetaTitles": // очистка "Шаблон META TITLE" страниц облигаций
        clearSeoElementPararms($obligPagesIblockId, "ELEMENT_META_TITLE");
        break;
}




}


//Очистка индивидуальных метатегов на вкладке SEO
function clearSeoElementPararms($IblockID, $clearParam){
$arSelect = Array("ID", "NAME", "IBLOCK_ID");
$arFilter = Array("IBLOCK_ID"=>IntVal($IblockID));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$cnt = 0;
while($ob = $res->fetch()){
   $el = new CIBlockElement;
	$arLoadProductArray = Array(
	  "IPROPERTY_TEMPLATES" => array([$clearParam]=>""),
	  //"IPROPERTY_TEMPLATES" => array("ELEMENT_PAGE_TITLE"=>""),
	);
	 $el->Update($ob["ID"], $arLoadProductArray);
	 	$ipropValues = new InheritedProperty\ElementValues($IblockID, $ob["ID"]);  //Сброс кеша для SEO элемента
      $ipropValues->clearValues();
		$cnt++;
	 }
	 $text = '';
	 if($IblockID==33){
	 	$text = 'акций';
	 }
	 if($IblockID==30){
	 	$text = 'облигаций';
	 }
 echo "<p>Очищены индивидуальные настройки ".$clearParam." страниц ".$text." (кол-во элементов): ".$cnt."</p>";
}
 ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>