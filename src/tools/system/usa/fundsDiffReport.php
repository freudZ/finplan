<?define("WIDE_PAGE","Y");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<?$APPLICATION->SetTitle("Отчет по движению активов в фондах США");?>

<?
Global $DB;
//Получаем уникальные названия фондов
$arFunds = array();
$query = "SELECT DISTINCT `UF_FUND` FROM `hl_usa_funds_data` ORDER BY `UF_FUND`";
$res = $DB->Query($query);
$arFunds = array();
while($row = $res->fetch()){
  $arFunds[] = $row["UF_FUND"];
}

$arFundsReportTickers = array();
$outTickers = array();
$inTickers = array();

if(isset($_POST["fundName"]) && !empty($_POST["fundName"])){
	$fundName = $_POST["fundName"];

$query = "SELECT DISTINCT `UF_DATE` FROM `hl_usa_funds_data` WHERE `UF_FUND` = '$fundName' ORDER BY `UF_DATE` DESC LIMIT 2";
$res = $DB->Query($query);
$arLastDatesFundsReports = array();
while($row = $res->fetch()){
  $arLastDatesFundsReports[] = "'".$row["UF_DATE"]."'";
}

$query = "SELECT `UF_DATE`, `UF_TICKER`, `UF_VALUE` FROM `hl_usa_funds_data` WHERE `UF_FUND` = '$fundName' AND `UF_TICKER` != '' AND `UF_DATE` IN (".implode(", ", $arLastDatesFundsReports).") GROUP BY `UF_DATE`, `UF_TICKER` ORDER BY `UF_DATE` DESC, `UF_VALUE` DESC";
$res = $DB->Query($query);
$allTickersInReport = array();
while($row = $res->fetch()){
  $arFundsReportTickers[$row["UF_DATE"]][$row["UF_TICKER"]] = $row["UF_VALUE"];
  $allTickersInReport[$row["UF_TICKER"]] = '';
}

 //Этих двух массивов хватит что бы использовать для фильтрации
 $outTickers = array_diff_key($arFundsReportTickers[str_replace("'", "", $arLastDatesFundsReports[1])], $arFundsReportTickers[str_replace("'", "", $arLastDatesFundsReports[0])]);
 $inTickers = array_diff_key($arFundsReportTickers[str_replace("'", "", $arLastDatesFundsReports[0])], $arFundsReportTickers[str_replace("'", "", $arLastDatesFundsReports[1])]);
}

//Достаем список отраслей для тикеров
 $resA = new ActionsUsa;
 foreach($allTickersInReport as $k=>$v){
 	$arItem = $resA->getItemBySecid($k);
	if(count($arItem) && isset($arItem["PROPS"]["INDUSTRY"])){
	  $allTickersInReport[$k]=array("INDUSTRY"=>$arItem["PROPS"]["INDUSTRY"], "SECTOR"=>$arItem["PROPS"]["SECTOR"]);
	}
 }

 $balanceTickersIndustry = $arFundsReportTickers[str_replace("'", "", $arLastDatesFundsReports[0])];
 $outTickersIndustry = $outTickers;
 $inTickersIndustry = $inTickers;

 foreach($balanceTickersIndustry as $k=>$v){
	$balanceTickersIndustry[$k]=$allTickersInReport[$k];
 }
 foreach($outTickersIndustry as $k=>$v){
	$outTickersIndustry[$k]=$allTickersInReport[$k];
 }
 foreach($inTickersIndustry as $k=>$v){
	$inTickersIndustry[$k]=$allTickersInReport[$k];
 }


?>
<div class="container">
 <div class="row">
 <div class="col-md-12">
<h1>Отчет по движению активов в фондах США</h1>
<form name="fundForm" action="" method="POST" enctype="multipart/form-data">
  <div>
	<label for="fundName">Фонд</label>
	 <select name="fundName">
		<option value="" <?=(!isset($_POST["fundName"]) || $_POST["fundName"]==""?'selected':'')?>>Не выбран</option>
		<?foreach($arFunds as $fundname):?>
			<option value="<?=$fundname?>" <?=(isset($_POST["fundName"]) && $_POST["fundName"]==$fundname?'selected':'')?>><?=$fundname?></option>
		<?endforeach;?>
	 </select>
	 <br>
	 <label for="showSectors">Показать секторы&nbsp;</label><input type="checkbox" name="showSectors" value="Y" <?=(isset($_POST["showSectors"]) && $_POST["showSectors"]=="Y"?'checked':'')?>><br>
	 <label for="showIndustry">Показать отрасли&nbsp;</label><input type="checkbox" name="showIndustry" value="Y" <?=(isset($_POST["showIndustry"]) && $_POST["showIndustry"]=="Y"?'checked':'')?>><br>
	 <input name="submit" type="submit" value="Сформировать отчет">
  </div>

</form>
</div>
</div>
</div>
<?if(count($arFundsReportTickers)):?>
<hr>
<div class="container-fluid">
 <div class="row">
 <div class="col-md-12">
 <p>
  Данные на основе отчетов за <?= implode(" и ",$arLastDatesFundsReports); ?>
 </p>
 <table class="table table-bordered table-condenced" style="font-size:11px">
	<thead>
	 <tr>
	  <td >На балансе</td>
	  <td >Вошли</td>
	  <td >Вышли</td>
	 </tr>
	</thead>
	<tbody>
	 <tr valign="top">
		<td  style="padding:0px;">
		 <table class="table table-bordered table-condensed">
		 <tr>
		  	<th>Тикер</th>
			<?if($_POST["showSectors"]=="Y"):?>
 				<th>Сектор</th>
			<?endif;?>
			<?if($_POST["showIndustry"]=="Y"):?>
 				<th>Отрасль</th>
			<?endif;?>
 			<th>VALUE (x$1000)</th>
		</tr>
		  <?foreach($arFundsReportTickers[str_replace("'", "", $arLastDatesFundsReports[0])] as $k=>$v):?>
		   <tr>
			 <td><?=$k?></td>
			 <?if($_POST["showSectors"]=="Y"):?>
			 	<td><?=$balanceTickersIndustry[$k]["SECTOR"]?></td>
			 <?endif;?>
			 <?if($_POST["showIndustry"]=="Y"):?>
			 	<td><?=$balanceTickersIndustry[$k]["INDUSTRY"]?></td>
			 <?endif;?>
			 <td><?=$v?></td>
			</tr>
		  <?endforeach;?>
		 </table>
		</td>

		<td style="padding:0px;">
		 <table class="table table-bordered table-condensed">
		 <tr>
		  	<th>Тикер</th>
			<?if($_POST["showSectors"]=="Y"):?>
 				<th>Сектор</th>
			<?endif;?>
			<?if($_POST["showIndustry"]=="Y"):?>
 				<th>Отрасль</th>
			<?endif;?>
 			<th>VALUE (x$1000)</th>
		</tr>
		  <?foreach($inTickers as $k=>$v):?>
		   <tr>
			 <td><?=$k?></td>
			 <?if($_POST["showSectors"]=="Y"):?>
			 	<td><?=$inTickersIndustry[$k]["SECTOR"]?></td>
			 <?endif;?>
			 <?if($_POST["showIndustry"]=="Y"):?>
			 	<td><?=$inTickersIndustry[$k]["INDUSTRY"]?></td>
			 <?endif;?>
			 <td><?=$v?></td>
			</tr>
		  <?endforeach;?>
		 </table>
		</td>

		<td style="padding:0px;">
		 <table class="table table-bordered table-condensed">
		 <tr>
		  	<th>Тикер</th>
			<?if($_POST["showSectors"]=="Y"):?>
 				<th>Сектор</th>
			<?endif;?>
			<?if($_POST["showIndustry"]=="Y"):?>
 				<th>Отрасль</th>
			<?endif;?>
 			<th>VALUE (x$1000)</th>
		</tr>
		  <?foreach($outTickers as $k=>$v):?>
		   <tr>
			 <td><?=$k?></td>
			 <?if($_POST["showSectors"]=="Y"):?>
			 	<td><?=$outTickersIndustry[$k]["SECTOR"]?></td>
			 <?endif;?>
			 <?if($_POST["showIndustry"]=="Y"):?>
			  <td><?=$outTickersIndustry[$k]["INDUSTRY"]?></td>
			 <?endif;?>
			 <td><?=$v?></td>
			</tr>
		  <?endforeach;?>
		 </table>
		</td>

<!--	  <td><?=implode("<br>", array_keys($arFundsReportTickers[str_replace("'", "", $arLastDatesFundsReports[0])]))?></td>
	  <td><?=implode("<br>", array_values($balanceTickersIndustry))?></td>
	  <td><?=implode("<br>", array_values($arFundsReportTickers[str_replace("'", "", $arLastDatesFundsReports[0])]))?></td>
	  <td><?=implode("<br>", array_keys($inTickers))?></td>
	  <td><?=implode("<br>", array_values($inTickersIndustry))?></td>
	  <td><?=implode("<br>", array_values($inTickers))?></td>
	  <td><?=implode("<br>", array_keys($outTickers))?></td>
	  <td><?=implode("<br>", array_values($outTickersIndustry))?></td>
	  <td><?=implode("<br>", array_values($outTickers))?></td>-->
	 </tr>
	</tbody>
 </table>
 </div>
 </div>
 </div>
 <?else:?>
	 <p>Нет данных или не выбран фонд для отчета.</p>
 <?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>