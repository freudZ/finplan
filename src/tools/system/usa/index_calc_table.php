<?define("NEED_AUTH", true);
define("WIDE_PAGE","Y");
define("NO_BX_PANEL","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("Таблица таргетов по США");?>
<?
CModule::IncludeModule("iblock");
$actUsa = new ActionsUsa();

//Сектора
$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_MID_PE");
$arFilter = Array("IBLOCK_ID"=>IntVal(58), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arSectorPE = array();
while($ob = $res->Fetch()){
 $arSectorPE[$ob["NAME"]] = $ob["PROPERTY_MID_PE_VALUE"];
}

$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>IntVal(55), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("PROPERTY_TARGET_CUSTOM"=>"DESC", "PROPERTY_TARGET"=>"DESC"), $arFilter, false, false, $arSelect);
?>
<div class="col-md-12">
<h1>Таблица таргетов по США</h1>
<div class="scrollX t12" >

 <table class="table table-striped table-bordered table-condensed">
	<tr class="">
		<th>Название</th>
		<th>Вал.</th>
		<th>Цена</th>
		<th>Ручной прогноз</th>
		<th>Прогноз <br>(Расчетный прогноз)</th>
		<th>Недооценка</th>
		<th>P/E</th>
		<th>Cр.отрсл. <br>P/E</th>
		<th>Прибыль тек</th>
		<th>Прибыль <br>-1 год</th>
		<th>Кв.Приб. <br>-1 год</th>
		<th>Прибыль <br>-2 года</th>
		<th>Кв.Приб. <br>-2 года</th>
		<th>Прибыль <br>-3 года</th>
		<th>Кв.Приб. <br>3 года</th>
	</tr>

<?
$arResultCustomTarget = array();
$arResult = array();
while($ob = $res->GetNextElement()){
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
 $bgRow = floatval($arProps["TARGET_CUSTOM"]["VALUE"])>0?'#dff0d8;':'';
 $radarData = $actUsa->getItem($arProps["ISIN"]["VALUE"]);
/* echo "<pre  style='color:black; font-size:11px;'>";
    print_r($radarData);
    echo "</pre>";*/
reset($radarData["PERIODS"]);
 $lastPeriod = key($radarData["PERIODS"]);
 $lastPeriod = explode("-",$lastPeriod);
 $periodYear = $lastPeriod[0].'-'.($lastPeriod[1]-1).'-'.$lastPeriod[2];
 $periodTwoYear = $lastPeriod[0].'-'.($lastPeriod[1]-2).'-'.$lastPeriod[2];
 $periodThreeYear = $lastPeriod[0].'-'.($lastPeriod[1]-3).'-'.$lastPeriod[2];

 $arPeriodYear = array();
 if(array_key_exists($periodYear, $radarData["PERIODS"])){
 $arPeriodYear = $radarData["PERIODS"][$periodYear];
 }
 $arPeriodTwoYear = array();
 if(array_key_exists($periodTwoYear, $radarData["PERIODS"])){
 $arPeriodTwoYear = $radarData["PERIODS"][$periodTwoYear];
 }
 $arPeriodThreeYear = array();
 if(array_key_exists($periodThreeYear, $radarData["PERIODS"])){
 $arPeriodThreeYear = $radarData["PERIODS"][$periodThreeYear];
 }

 if(floatval($arProps["TARGET_CUSTOM"]["VALUE"])>0){
	 $arResultCustomTarget[] = array("FIELDS"=>$arFields, "PROPS"=>$arProps, "RADAR_DATA"=>$radarData,
	 "CALC_VALUES"=>array("periodYear"=>$periodYear, "periodTwoYear"=>$periodTwoYear, "periodThreeYear"=>$periodThreeYear,
	 							 "arPeriodYear"=>$arPeriodYear, "arPeriodTwoYear"=>$arPeriodTwoYear, "arPeriodThreeYear"=>$arPeriodThreeYear),
								 "bgRow" => $bgRow
	 );
 } else {
	 $arResult[] = array("FIELDS"=>$arFields, "PROPS"=>$arProps, "RADAR_DATA"=>$radarData,
	 "CALC_VALUES"=>array("periodYear"=>$periodYear, "periodTwoYear"=>$periodTwoYear, "periodThreeYear"=>$periodThreeYear,
	 							 "arPeriodYear"=>$arPeriodYear, "arPeriodTwoYear"=>$arPeriodTwoYear, "arPeriodThreeYear"=>$arPeriodThreeYear),
								 "bgRow" => $bgRow
	 );
 }
 }
 	usort($arResultCustomTarget, function ($item1, $item2) {
					    	  return floatval($item2["PROPS"]["TARGET_CUSTOM"]["VALUE"]) <=> floatval($item1["PROPS"]["TARGET_CUSTOM"]["VALUE"]);
						});
 	usort($arResult, function ($item1, $item2) {
					    	  return floatval($item2["PROPS"]["TARGET"]["VALUE"]) <=> floatval($item1["PROPS"]["TARGET"]["VALUE"]);
						});  ?>
 <? foreach($arResultCustomTarget as $item):?>
 <tr style="background-color:<?= $item["bgRow"] ?>">
 	<td><?= $item["FIELDS"]["NAME"] ?> [<?=$item["PROPS"]["SECID"]["VALUE"]?>]</td>
 	<td><?= $item["PROPS"]["CURRENCY"]["VALUE"] ?></td>
 	<td><?= $item["PROPS"]["LASTPRICE"]["VALUE"] ?></td>
 	<td>
	<input class="tools_table_input" type="text" id="target_custom_<?=$item["FIELDS"]["ID"]?>" value="<?= floatval($item["PROPS"]["TARGET_CUSTOM"]["VALUE"])>0?$item["PROPS"]["TARGET_CUSTOM"]["VALUE"]:0; ?>"/>
	<a href="javascript:void(0);" class="custom_target_savebtn ctsbtn_<?=$item["FIELDS"]["ID"]?>" data-id="<?=$item["FIELDS"]["ID"]?>" title="Сохранить значение"><i class="fa fa-save"></i></a>&nbsp;<span id="save_status_<?=$item["FIELDS"]["ID"]?>" class="save_status"></span>
	</td>
 	<td><?= floatval($item["PROPS"]["TARGET"]["VALUE"])>0?$item["PROPS"]["TARGET"]["VALUE"]:0; ?> (<?=$item["PROPS"]["TARGET_CALCULATE"]["VALUE"]?>)</td>
 	<td><?= $item["PROPS"]["UNDERSTIMATION"]["VALUE"] ?></td>
 	<td><?= $item["RADAR_DATA"]["DYNAM"]["PE"] ?></td>
 	<td><?= $arSectorPE[$item["PROPS"]["SECTOR"]["VALUE"]] ?></td>
	<td><?= $item["RADAR_DATA"]["DYNAM"]["Прибыль"] ?></td>
 	<td><?= $item["CALC_VALUES"]["arPeriodYear"]["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$item["CALC_VALUES"]["periodYear"]) ?></td>
 	<td><?= $item["CALC_VALUES"]["arPeriodTwoYear"]["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$item["CALC_VALUES"]["periodTwoYear"]) ?></td>
 	<td><?= $item["CALC_VALUES"]["arPeriodThreeYear"]["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$item["CALC_VALUES"]["periodThreeYear"]) ?></td>
 </tr>
 <?endforeach;?>
<? foreach($arResult as $item):?>
 <tr style="background-color:<?= $item["bgRow"] ?>">
 	<td><?= $item["FIELDS"]["NAME"] ?> [<?=$item["PROPS"]["SECID"]["VALUE"]?>]</td>
 	<td><?= $item["PROPS"]["CURRENCY"]["VALUE"] ?></td>
 	<td><?= $item["PROPS"]["LASTPRICE"]["VALUE"] ?></td>
 	<td>
	<input class="tools_table_input" type="text" id="target_custom_<?=$item["FIELDS"]["ID"]?>" value="<?= floatval($item["PROPS"]["TARGET_CUSTOM"]["VALUE"])>0?$item["PROPS"]["TARGET_CUSTOM"]["VALUE"]:0; ?>"/>
	<a href="javascript:void(0);" class="custom_target_savebtn ctsbtn_<?=$item["FIELDS"]["ID"]?>" data-id="<?=$item["FIELDS"]["ID"]?>" title="Сохранить значение"><i class="fa fa-save"></i></a>&nbsp;<span id="save_status_<?=$item["FIELDS"]["ID"]?>" class="save_status"></span>
	</td>
 	<td><?= floatval($item["PROPS"]["TARGET"]["VALUE"])>0?$item["PROPS"]["TARGET"]["VALUE"]:0; ?> (<?=$item["PROPS"]["TARGET_CALCULATE"]["VALUE"]?>)</td>
 	<td><?= $item["PROPS"]["UNDERSTIMATION"]["VALUE"] ?></td>
 	<td><?= $item["RADAR_DATA"]["DYNAM"]["PE"] ?></td>
 	<td><?= $arSectorPE[$item["PROPS"]["SECTOR"]["VALUE"]] ?></td>
	<td><?= $item["RADAR_DATA"]["DYNAM"]["Прибыль"] ?></td>
 	<td><?= $item["CALC_VALUES"]["arPeriodYear"]["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$item["CALC_VALUES"]["periodYear"]) ?></td>
 	<td><?= $item["CALC_VALUES"]["arPeriodTwoYear"]["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$item["CALC_VALUES"]["periodTwoYear"]) ?></td>
 	<td><?= $item["CALC_VALUES"]["arPeriodThreeYear"]["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$item["CALC_VALUES"]["periodThreeYear"]) ?></td>
 </tr>
 <?endforeach;?>
 </table>
 </div>
 </div>
 <script>

  function save_custom_target_usa(id, value){
		$.ajax({
		type: "POST",
		url: "/tools/ajax/save_custom_target_usa.php",
		dataType: "json",
		data:{ajax: "y", element_id: id, target: value},
		cashe: false,
		async: false,
		success: function(data){
			getResponse(data);
		}
		});

	 function getResponse(data){
	 	arr = data;
	   if(arr["result"]=="ok"){
		  $("#save_status_"+arr["id"]).html('<i class="fa fa-check" style="color:#339900"></i>');
		  setTimeout(function(){$("#save_status_"+arr["id"]).html('');},2000);
	   }  else {
		  $("#save_status_"+arr["id"]).html('<i class="fa fa-cancel " style="color:#FF0000"></i>');
		  setTimeout(function(){$("#save_status_"+arr["id"]).html('');},2000);
	   }
	 }
  }

 $(document).ready(function(){

  $('.custom_target_savebtn').on('click', function(){
  	var id = $(this).attr('data-id');
	var value = $('#target_custom_'+id).val();
	 save_custom_target_usa(id, value);
  });

 });
 </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>