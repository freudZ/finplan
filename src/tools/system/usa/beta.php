<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
$APPLICATION->SetTitle("Расчет беты для акций США");?>
<?

$arReturn = array();
if(!empty($_POST["secid"])){
$beta = new CBeta;
$shareSecid = trim($_POST["secid"]);
$beta->initCommonUsaData();
$arReturn = $beta->calculateOneActionUSABeta($shareSecid,true);

}

 ?>
 <div class="container-fluid">
 <h2>Расчет беты акций США</h2>
 <div class="row">
 <div class="col-md-6">
 <form class="form form-inline" name="coeff_form" action="" method="POST" target="_self">
 <div class="form-group">
	 <input class="form-control" id="secid" name="secid" type="text" placeholder="Тикер акции" value="<?= $_POST["secid"] ?>">
 </div>
 	 <button type="submit" name="send" class="btn btn-default">Рассчитать бету</button>
 </form>
 <hr>
</div>
<?if(isset($arReturn["BETA"])):?>
 <div class="col-md-12">
<table class="table table-striped table-condensed table-bordered" style="font-size:0.7em">
 <tr>
  <th style="border-right:1px solid #ccc;" colspan="8">Бета = <?= $arReturn["BETA"] ?></th>
 </tr>
 <tr>
  <th style="border-right:1px solid #ccc;" colspan="8">Вычисления беты для акции <?= $shareSecid ?></th>
 </tr>
 <tr>
  <th>A<br>Дата</th>
  <th>B<br>Индекс S&P 500</th>
  <th>C<br>Актив</th>
  <th>D<br>Доходность индекса</th>
  <th>E<br>Доходность актива</th>
  <th>F<br>Допустимая дневная доходность</th>
  <th>G<br>Ковариация доходности актива и индекса</th>
  <th>H<br>Дисперсия доходности индекса</th>
 </tr>

	<?for($i=0; $i<count($arReturn["DEBUG"]["A"])-1; $i++ ):?>
 <tr>
  <td><?= $arReturn["DEBUG"]["A"][$i] ?></td>
  <td><?= $arReturn["DEBUG"]["B"][$i] ?></td>
  <td><?= $arReturn["DEBUG"]["C"][$i] ?></td>
  <td><?= $arReturn["DEBUG"]["D"][$i] ?></td>
  <td><?= $arReturn["DEBUG"]["E"][$i] ?></td>
  <td><?= $arReturn["DEBUG"]["F"][$i] ?></td>
  <td><?= $arReturn["DEBUG"]["G"][$i] ?></td>
  <td><?= $arReturn["DEBUG"]["H"][$i] ?></td>


 </tr>
	<?endfor;?>

</table>

 <hr>
 <h2>Ценовые ряды</h2>
<table class="table">
 <tr>
  <td>Дата</td>
  <td>Акция</td>
  <td>SP500</td>

 </tr>
 	<?foreach($arReturn["DEBUG"]["LIST_OF_DATA"] as $date=>$summ):?>
 <tr>
  <td><?= $summ["DATE"] ?></td>
  <td><?= str_replace(".", ",", $summ["SUM_USA"]) ?></td>
  <td><?= str_replace(".", ",", $summ["SP500"]) ?></td>


 </tr>
	<?endforeach;?>

</table>

</div>
<?endif;?>
</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>