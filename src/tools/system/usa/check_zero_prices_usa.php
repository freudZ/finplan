<? define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
  //Проверка существующих нулевых цен акций в базе данных по США
?>
<? use Bitrix\Highloadblock as HL;
	CModule::IncludeModule('highloadblock');
 ?>
<?$APPLICATION->SetTitle("Проверка существующих нулевых цен акций в базе данных по США");?>
<?

				//Получаем цены на акции
				$hlblock     = HL\HighloadBlockTable::getById(29)->fetch();
				$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
				$entityClass = $entity->getDataClass();
				$arFilter    = array(
					  //"UF_CLOSE" => 0,
					  "UF_CLOSE_BETA" => 0,
					  //"UF_CLOSE" => false,
					  "UF_CLOSE_BETA" => false,
				);

				if(isset($_REQUEST["UF_ITEM"]) && !empty($_REQUEST["UF_ITEM"])){
				  $arFilter["UF_ITEM"] = $_REQUEST["UF_ITEM"];
				}

				$res = $entityClass::getList(array(
					"filter" => $arFilter,
					"select" => array(
						"ID", "UF_ITEM", "UF_DATE", "UF_CLOSE", "UF_CLOSE_BETA"
					),
					//"limit" => 20,
					"order" => array(
						"UF_DATE" => "ASC"
					),
					//"group"=>"UF_ITEM"
					//"cache" => array("ttl" => 3600)
				));
			  $arResult = array();
				while ($item = $res->fetch()) {
					$item["UF_DATE"] = (new DateTime($item["UF_DATE"]))->format('d.m.Y');
					$arResult[$item["UF_ITEM"]][$item["UF_DATE"]] = $item;

				}


				//Если была команда удалить записи цен с выходными днями (читай пустые или с нулями)
				if(isset($_REQUEST["UF_DEL_WEEKEND"]) && !empty($_REQUEST["UF_DEL_WEEKEND"])){

				$arWeekendsDates = array();
						  foreach($arResult[$_REQUEST["UF_DEL_WEEKEND"]] as $date=>$arRow){
							 if(isUsaWeekendDay($date)){
							  $arWeekendsDates[]=$date;
							  $entityClass::delete($arRow["ID"]);
							 }
						  }
              LocalRedirect("check_zero_prices_usa.php");
				}



 ?>

<div class="container">
 <?if(isset($_REQUEST["UF_ITEM"]) && !empty($_REQUEST["UF_ITEM"])):?>
<h2>Записи о ценах актива <strong><?= $_REQUEST["UF_ITEM"] ?></strong> с нулевыми или пустыми ценами</h2>
<div class="row">
  <div class="col-md-12">
    <a href="/tools/system/usa/check_zero_prices_usa.php">Назад к списку</a>
	 <br>
	 <table class="table table-striped table-condensed table-bordered">
	  <tr>
		<th>Дата</th>
		<th>UF_CLOSE</th>
		<th>UF_CLOSE_BETA</th>
	  </tr>
	  <?foreach($arResult[$_REQUEST["UF_ITEM"]] as $date=>$arItem):?>
		 <tr class="<?=(isUsaWeekendDay($date)?'warning':'')?>">
		  <td><?= $date ?></td>
		  <td><?= $arItem["UF_CLOSE"] ?></td>
		  <td><?= $arItem["UF_CLOSE_BETA"] ?></td>
		 </tr>
	  <?endforeach;?>
	 </table>
	 <a href="/tools/system/usa/check_zero_prices_usa.php">Назад к списку</a>
  </div>
</div>
<?else:?>
 <h2>Список активов с нулевыми или пустыми ценами</h2>
<div class="row">
  <div class="col-md-12">
	 <table class="table table-striped table-condensed table-bordered">
	  <tr>
		<th>Актив</th>
		<th>Кол-во нулевых <br>или пустых цен</th>
		<th>Из них приходятся <br>на выходные и праздники</th>
	  </tr>
	  <? $allWeekendsDates = 0; ?>
	  <?foreach($arResult as $key=>$arItem):?>
	  <? $arWeekendsDates = array();
		  foreach($arItem as $date=>$arRow){
			 if(isUsaWeekendDay($date)){
			  $arWeekendsDates[]=$date;
			  $allWeekendsDates++;
			 }

		  }

	   ?>
		 <tr>
		  <td><a href="/tools/system/usa/check_zero_prices_usa.php?UF_ITEM=<?= $key ?>" target="_blank"><?= $key ?></a></td>
		  <td><?= count($arItem) ?></td>
		  <td><?= count($arWeekendsDates) ?>&nbsp;<? if(count($arWeekendsDates)): ?><a href="/tools/system/usa/check_zero_prices_usa.php?UF_DEL_WEEKEND=<?= $key ?>" target="_self">Удалить эти записи</a><?endif;?></td>
		 </tr>
	  <?endforeach;?>
	 </table>
	 <p>Всего выходных и праздников с нулевой ценой: <?=$allWeekendsDates?></p>
  </div>
</div>
<?endif;?>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>