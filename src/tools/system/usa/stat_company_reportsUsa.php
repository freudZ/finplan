<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

use Bitrix\Highloadblock as HL;

global $USER, $APPLICATION;
$arGroups = $USER->GetUserGroupArray();
if (!$USER->IsAdmin() && !in_array(array(1, 5), $arGroups)) {
    LocalRedirect("/");
    exit();
}
CModule::IncludeModule("iblock");
$arResult = array();
$currentYear = (new DateTime())->format("Y");
$year = isset($_REQUEST["year"]) ? $_REQUEST["year"] : $currentYear;

$resA = new ActionsUsa();
$arCompanyPeriods = array();
$arCompanyBestValues = array();
$arCompanyData = array();
$allCompanyCount = array();
$allCompanyCountBySector = array();
$arCompanyCountReports = array();

foreach ($resA->arOriginalsItems as $arItem) {
    if(isset($_REQUEST["sp500"]) && $_REQUEST["sp500"]=="Y"){
        if(empty($arItem["PROPS"]["SP500"]) || strpos($arItem["PROPS"]["SP500"],"SP500")===false) continue;
    }
    if (!in_array($arItem["COMPANY"]["ID"], $allCompanyCount)) {
        $allCompanyCount[$arItem["COMPANY"]["ID"]] = $arItem["COMPANY"]["ID"];
    }
    $sector = $arItem["PROPS"]["SECTOR"];
    $allCompanyCountBySector[$sector] += 1;

    
    foreach ($arItem["PERIODS"] as $periodName => $periodVal) {
        if (strpos($periodName, $year) === false) {
            continue;
        }//Не берем ненужные периоды
        

        
        $arPeriod = explode("-", $periodName);
        $arLastPeriod = ($arPeriod[0] == 1 ? 4 : $arPeriod[0] - 1) . "-" . ($arPeriod[0] == 1 ? $arPeriod[1] - 1 : $arPeriod[1]) . "-" . $arPeriod[2];

        $pBest = false;
        $vBest = false;
        if (!in_array($arItem["COMPANY"]["NAME"], $arCompanyCountReports[$periodName]["COMPANIES"])) {
            $arCompanyCountReports[$periodName]["COMPANIES"][] = $arItem["COMPANY"]["NAME"];
        }
        if (!in_array($arItem["COMPANY"]["NAME"], $arCompanyCountReports[$periodName][$sector]["COMPANIES"])) {
            $arCompanyCountReports[$periodName][$sector]["COMPANIES"][] = $arItem["COMPANY"]["NAME"];
        }
        // echo $arLastPeriod."<br>";
        //if(isset($arItem["PERIODS"][$arLastPeriod])){
        if ($periodVal["Прибыль за год (скользящая)"] > $arItem["PERIODS"][$arLastPeriod]["Прибыль за год (скользящая)"]) {
            $pBest = true;
            $arCompanyBestValues[$periodName]["Прибыль лучше прошлого периода"] += 1;
            $arCompanyBestValues[$periodName]["LAST_PERIOD"] = $arLastPeriod;
            $arCompanyBestValues[$periodName][$sector]["Прибыль лучше прошлого периода"] += 1;
            $arCompanyBestValues[$periodName][$sector]["LAST_PERIOD"] = $arLastPeriod;
        }
        if ($periodVal["Выручка за год (скользящая)"] > $arItem["PERIODS"][$arLastPeriod]["Выручка за год (скользящая)"]) {
            $pBest = true;
            $arCompanyBestValues[$periodName]["Выручка лучше прошлого периода"] += 1;
            $arCompanyBestValues[$periodName]["LAST_PERIOD"] = $arLastPeriod;
            $arCompanyBestValues[$periodName][$sector]["Выручка лучше прошлого периода"] += 1;
            $arCompanyBestValues[$periodName][$sector]["LAST_PERIOD"] = $arLastPeriod;
        }
        //}
        
        
        $arCompanyPeriods[$periodName]["COMPANIES"][$sector][$arItem["COMPANY"]["ID"]] = array(
          "Прибыль лучше прошлого периода"        => $pBest,
          "Выручка лучше прошлого периода"        => $vBest,
          "Прибыль за год (скользящая)"           => $periodVal["Прибыль за год (скользящая)"],
          "Выручка за год (скользящая)"           => $periodVal["Выручка за год (скользящая)"],
          "Прибыль за год (скользящая) Пр.период" => $arItem["PERIODS"][$arLastPeriod]["Прибыль за год (скользящая)"],
          "Выручка за год (скользящая) Пр.период" => $arItem["PERIODS"][$arLastPeriod]["Выручка за год (скользящая)"]
        );
        $arCompanyPeriods[$periodName]["PERIOD_TYPE"] = $arPeriod[2];
        $arCompanyPeriods[$periodName]["PERIOD_YEAR"] = $arPeriod[1];
        $arCompanyPeriods[$periodName]["PERIOD_VAL"] = $arPeriod[0];
        
        if ($arPeriod[0] == 1) {
        }
    }
    
    $arCompanyData[$arItem["COMPANY"]["ID"]] = $arItem["COMPANY"];
}

//Считаем общее кол-ва компаний
/*foreach ($arCompanyCountReports as $periodName=>$arPeriods){
    $arCompanyCountReports[$periodName]["COMPANIES"] = 0;
    foreach($arPeriods as $arSectors){
        $arCompanyCountReports[$periodName]["COMPANIES"] += count($arSectors);
    }
}*/


//Сортируем периоды
uasort($arCompanyPeriods, function ($item1, $item2) {
    $result = false;
    //Даты кварталов
    $quartDates = array(
      1 => "03-31",
      2 => "06-30",
      3 => "09-30",
      4 => "12-31",
    );
    if ($item1["PERIOD_TYPE"] == "KVARTAL" && $item1["PERIOD_YEAR"] > 2000 && $item2["PERIOD_YEAR"] > 2000) {
        $dstr1 = $item1["PERIOD_YEAR"] . "-" . $quartDates[$item1["PERIOD_VAL"]];
        $dt1 = new DateTime($dstr1);
        $dstr2 = $item2["PERIOD_YEAR"] . "-" . $quartDates[$item2["PERIOD_VAL"]];
        $dt2 = new DateTime($dstr2);
        $result = $dt1 < $dt2;
    }
    return $result;
});

?>
<div class="container">

    <h1>Статистика отчетности компаний США</h1>
    <div class="main_content">
        <div class="main_content_inner">
            <form class="inline-form sorting-form" enctype="multipart/form-data" method="post">
                <div class="form-group">
                    <label for="year">Получить статистику за год</label>
                    <? $yearCnt = $currentYear ?>
                    <select name="year">
                        <? while ($yearCnt > 2010): ?>
                            <option value="<?= $yearCnt ?>" <?= ($_REQUEST["year"] == $yearCnt ? 'selected' : '') ?>><?= $yearCnt ?></option>
                            <? $yearCnt--; ?>
                        <? endwhile; ?>
                    </select>
                    &nbsp;
                    <label for="sp500">Показывать только компании из SP500</label>
                    <input type="checkbox" name="sp500" value="Y" <?=($_REQUEST["sp500"]=="Y"?'checked':'')?>>
                    <!--                    <label for="sort">Сортировка</label>&nbsp;
                    <select name="sort">
                        <option value="cnt" <? /*=$_REQUEST["sort"]=="cnt"?'selected=""':''*/ ?>>По общему вхождению в портфели</option>
                        <option value="cnt_user" <? /*=$_REQUEST["sort"]=="cnt_user"?'selected=""':''*/ ?>>По вхождению в портфели пользователей</option>
                    </select>-->
                    
                    <!--                    <label for="user">Направление</label>&nbsp;
                    <select name="dir">
                        <option value="asc" <? /*=$_REQUEST["dir"]=="asc"?'selected=""':''*/ ?> >По возрастанию</option>
                        <option value="desc" <? /*=$_REQUEST["dir"]=="desc"?'selected=""':''*/ ?> >По убыванию</option>
                    </select>-->
                    <br>
                    <button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>
                </div>
            
            </form>
            
            <? if (count($arCompanyPeriods) > 0): ?>
                <h3>Всего компаний: <?= count($allCompanyCount); ?></h3>
                <table class="table table-condensed table-bordered table-striped stat_company_reports">
                    <thead class="bg-info">
                    <tr>
                        <th colspan="3">Период</th>
                    </tr>
                    <tr>
                        <th>Сектор/Компания (всего отчетов)</th>
                        <th>Прибыль скользящая тек./<br>прошл.</th>
                        <th>Выручка скользящая тек./<br>прошл.</th>
                    </tr>
                    
                    </thead>
                    
                    <? foreach ($arCompanyPeriods as $periodName => $periodVal): ?>
                        <tr class="bg-info">
                            <td class="period_header" data-rows="<?= $periodName ?>" data-companies="<?=implode(",", $arCompanyCountReports[$periodName]["COMPANIES"])?>"><i
                                  class="fas fa-angle-right"></i> <?= $periodName ?>
                                <?$perPrc = round(count($arCompanyCountReports[$periodName]["COMPANIES"])/count($allCompanyCount)*100,2);?>
                                (<?= count($arCompanyCountReports[$periodName]["COMPANIES"]) ?> из <?= count($allCompanyCount)?> или <?=$perPrc?>%)
                            </td>
                            <?$bestPeriodPPercent = round($arCompanyBestValues[$periodName]["Прибыль лучше прошлого периода"]/count($arCompanyCountReports[$periodName]["COMPANIES"])*100,2)?>
                            <?$bestPeriodVPercent = round($arCompanyBestValues[$periodName]["Выручка лучше прошлого периода"]/count($arCompanyCountReports[$periodName]["COMPANIES"])*100,2)?>
                            <td><?= $arCompanyBestValues[$periodName]["Прибыль лучше прошлого периода"] ?> (<?=$bestPeriodPPercent?>%)</td>
                            <td><?= $arCompanyBestValues[$periodName]["Выручка лучше прошлого периода"] ?> (<?=$bestPeriodVPercent?>%)</td>
                        </tr>
                        <? $cntSectors = 0;
                        foreach ($periodVal["COMPANIES"] as $sectorName => $arCompanies): ?>
                            <tr class="sector-row  sector_<?= $periodName ?> hidden" data-sector="<?= $cntSectors ?>"
                                data-rows="<?= $periodName ?>">
                            <td class="sector-header" data-sector="<?= $cntSectors ?>" data-rows="<?= $periodName ?>">
                                &nbsp;&nbsp;<i class="fas fa-angle-right"></i> <?= $sectorName ?>
                                <? $sectPrc = round(count($arCompanyCountReports[$periodName][$sectorName]["COMPANIES"])/$allCompanyCountBySector[$sectorName]*100, 2);?>
                                (<?= count($arCompanyCountReports[$periodName][$sectorName]["COMPANIES"]) ?> из <?= $allCompanyCountBySector[$sectorName]?> или <?=$sectPrc?>%)
                            </td>
                            <td>  <?$bestPPercent = round( $arCompanyBestValues[$periodName][$sectorName]["Прибыль лучше прошлого периода"]/count($arCompanyCountReports[$periodName][$sectorName]["COMPANIES"])*100,2);?>
                                &nbsp;&nbsp;<strong><?= $arCompanyBestValues[$periodName][$sectorName]["Прибыль лучше прошлого периода"] ?></strong> (<?=$bestPPercent?>%)</td>
                            <td><?$bestVPercent = round( $arCompanyBestValues[$periodName][$sectorName]["Выручка лучше прошлого периода"]/count($arCompanyCountReports[$periodName][$sectorName]["COMPANIES"])*100,2);?>
                                &nbsp;<strong><?= $arCompanyBestValues[$periodName][$sectorName]["Выручка лучше прошлого периода"] ?></strong> (<?=$bestVPercent?>%)</td>
                            </tr>
                            <? foreach ($arCompanies as $companyId => $companyVal): ?>
                                <?
                                $pStyle = '';
                                $vStyle = '';
                                if ($companyVal["Прибыль за год (скользящая) Пр.период"]) {
                                    if ($companyVal["Прибыль за год (скользящая)"] > $companyVal["Прибыль за год (скользящая) Пр.период"]) {
                                        $pStyle = 'background-color:#CCFFCC';
                                    } else {
                                        $pStyle = 'background-color:#FFCCCC';
                                    }
                                }
                                if ($companyVal["Выручка за год (скользящая) Пр.период"]) {
                                    if ($companyVal["Выручка за год (скользящая)"] > $companyVal["Выручка за год (скользящая) Пр.период"]) {
                                        $vStyle = 'background-color:#CCFFCC';
                                    } else {
                                        $vStyle = 'background-color:#FFCCCC';
                                    }
                                }
                                ?>
                                <tr class="row_<?= $periodName ?> sector_<?= $cntSectors ?> period_rows hidden">
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?= $arCompanyData[$companyId]["URL"] ?>"
                                                                   target="_blank"><?= $arCompanyData[$companyId]["NAME"] ?></a>
                                    </td>
                                    <td style="<?= $pStyle ?>">
                                        &nbsp;&nbsp;&nbsp;&nbsp;<?= $companyVal["Прибыль за год (скользящая)"] ?><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<i><?= $companyVal["Прибыль за год (скользящая) Пр.период"] ?></i>
                                    </td>
                                    <td style="<?= $vStyle ?>">
                                        &nbsp;&nbsp;&nbsp;&nbsp;<?= $companyVal["Выручка за год (скользящая)"] ?><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<i><?= $companyVal["Выручка за год (скользящая) Пр.период"] ?></i>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                            <? $cntSectors++; ?>
                        <? endforeach; ?>
                    <? endforeach; ?>
                </table>
            <? endif; ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.period_header').on('click', function () {
            if (!$(this).parents('tr').hasClass('active')) {
                $(this).parents('tr').addClass('active');
                var curPeriodRow = $(this).attr('data-rows');
                $(this).find('i').removeClass('fa-angle-right').addClass('fa-angle-down');
                $('.sector-row.sector_' + curPeriodRow).removeClass('hidden');
            } else {
                $(this).parents('tr').removeClass('active');
                var curPeriodRow = $(this).attr('data-rows');
                $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-right');
                $('.sector-row.sector_' + curPeriodRow).addClass('hidden');
            }
        });
        $('.sector-header').on('click', function () {
            if (!$(this).parents('tr').hasClass('active')) {
                $(this).parents('tr').addClass('active');
                var curSectorRow = $(this).attr('data-sector');
                var curPeriodRow = $(this).attr('data-rows');
                $(this).find('i').removeClass('fa-angle-right').addClass('fa-angle-down');
                $('.period_rows.sector_' + curSectorRow + '.row_' + curPeriodRow).removeClass('hidden');
            } else {
                $(this).parents('tr').removeClass('active');
                var curSectorRow = $(this).attr('data-sector');
                var curPeriodRow = $(this).attr('data-rows');
                $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-right');
                $('.period_rows.sector_' + curSectorRow + '.row_' + curPeriodRow).addClass('hidden');
            }
        });


    });
</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
