<?
define("WIDE_PAGE","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("Коэффициенты портфеля");?>

<?
$arKoeffData = array();
if(intval($_POST["portfolioId"])>0){
$portf= new CPortfolio;
$arKoeffData = $portf->getKoeffPortfolio(intval($_POST["portfolioId"]),true);
}
?>
<div class="container">
<h2>Коэффициенты портфеля</h2>
<div class="col-md-6">
 <form class="form form-inline" name="coeff_form" action="" method="POST" target="_self">
 <div class="form-group">
	 <input class="form-control" id="portfolioId" name="portfolioId" type="text" placeholder="ID портфеля (303277)" value="<?= $_POST["portfolioId"] ?>">
 </div>
 	 <button type="submit" name="send" class="btn btn-default">Рассчитать</button>
 </form>
 <hr>
</div>
</div>
<?if(count($arKoeffData)>0):?>
<div class="container">
 <div class="row">
 <div class="col-md-12">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#main_tab"  data-toggle="tab">Расчеты</a></li>
		<li class=""><a href="#summ_tab"  data-toggle="tab" >Ценовые ряды</a></li>
	</ul>
</div>
</div>
</div>

 <div class="container-fluid">
 <div class="row">
 <div class="col-md-12">
<div class="tab-content">
 <div id="main_tab" class="tab-pane active ">
<h2>Шарп, исходные данные:</h2>
<table class="table table-striped table-condensed table-bordered" >
<tr>
<td>
<? echo "По РФ:<pre  style='color:black; font-size:11px;'>";
  print_r($arKoeffData["RUS"]["SHARP_CALCULATE"]);
echo "</pre>"; ?>
</td>
<td>
<?
echo "По США:<pre  style='color:black; font-size:11px;'>";
  print_r($arKoeffData["USA"]["SHARP_CALCULATE"]);
echo "</pre>";
 ?></td>
</tr>
<tr>
<td>Коэфф. Шарпа (РФ):  <?= $arKoeffData["RUS"]["SHARP"]["PORTFOLIO"] ?> > <?= $arKoeffData["RUS"]["SHARP"]["INDEX"] ?> > 0</td>
<td>Коэфф. Шарпа (США):  <?= $arKoeffData["USA"]["SHARP"]["PORTFOLIO"] ?> > <?= $arKoeffData["USA"]["SHARP"]["INDEX"] ?> > 0</td>
</tr>
</table>

<h2>Трейнор, исходные данные:</h2>
<table class="table table-striped table-condensed table-bordered" >
<tr>
<td>
<? echo "По РФ:<pre  style='color:black; font-size:11px;'>";
  print_r($arKoeffData["RUS"]["TREYNOR_CALCULATE"]);
echo "</pre>"; ?>
</td>
<td>
<?
echo "По США:<pre  style='color:black; font-size:11px;'>";
  print_r($arKoeffData["USA"]["TREYNOR_CALCULATE"]);
echo "</pre>";
 ?></td>
</tr>
<tr>
<td>Коэфф. Трейнора (РФ):  <?= $arKoeffData["RUS"]["TREYNOR"]["PORTFOLIO"] ?> > <?= $arKoeffData["RUS"]["TREYNOR"]["INDEX"] ?> > 0</td>
<td>Коэфф. Трейнора (США):  <?= $arKoeffData["USA"]["TREYNOR"]["PORTFOLIO"] ?> > <?= $arKoeffData["USA"]["TREYNOR"]["INDEX"] ?> > 0</td>
</tr>

</table>

<h2>Сортино, исходные данные:</h2>
<table class="table table-striped table-condensed table-bordered" >
<tr>
<td>
<? echo "По РФ:<pre  style='color:black; font-size:11px;'>";
  print_r($arKoeffData["RUS"]["SORTINO_CALCULATE"]);
echo "</pre>"; ?>
</td>
<td>
<?
echo "По США:<pre  style='color:black; font-size:11px;'>";
  print_r($arKoeffData["USA"]["SORTINO_CALCULATE"]);
echo "</pre>";
 ?></td>
</tr>
<tr>
<td>Коэфф. Сортино (РФ):  <?= $arKoeffData["RUS"]["SORTINO"]["PORTFOLIO"] ?> > <?= $arKoeffData["RUS"]["SORTINO"]["INDEX"] ?> > 0</td>
<td>Коэфф. Сортино (США):  <?= $arKoeffData["USA"]["SORTINO"]["PORTFOLIO"] ?> > <?= $arKoeffData["USA"]["SORTINO"]["INDEX"] ?> > 0</td>
</tr>

</table>


<table class="table table-striped table-condensed table-bordered" style="font-size:0.7em">
 <tr>
  <th style="border-right:1px solid #ccc;" colspan="7">РФ</th>
  <th colspan="7">США</th>
 </tr>
 <tr>
  <th>A<br>Доходность индекса</th>
  <th>B<br>Доходность инвестиционного портфеля</th>
  <th>C<br>Допустимая дневная доходность</th>
  <th>D<br>Волатильность "вниз"</th>
  <th>E<br>Квадрат разницы</th>
  <th>G<br>Волатильность "вниз" индекса</th>
  <th style="border-right:1px solid #ccc;">H<br>Квадрат разницы индекса</th>

  <th>A<br>Доходность индекса</th>
  <th>B<br>Доходность инвестиционного портфеля</th>
  <th>C<br>Допустимая дневная доходность</th>
  <th>D<br>Волатильность "вниз"</th>
  <th>E<br>Квадрат разницы</th>
  <th>G<br>Волатильность "вниз" индекса</th>
  <th>H<br>Квадрат разницы индекса</th>
 </tr>

	<?for($i=0; $i<count($arKoeffData["RUS"]["A"])-1; $i++ ):?>
 <tr>
  <td><?= round($arKoeffData["RUS"]["A"][$i],5) ?></td>
  <td><?= round($arKoeffData["RUS"]["B"][$i],5) ?></td>
  <td><?= $arKoeffData["RUS"]["C"][$i] ?></td>
  <td><?= $arKoeffData["RUS"]["D"][$i] ?></td>
  <td><?= $arKoeffData["RUS"]["E"][$i] ?></td>
  <td><?= $arKoeffData["RUS"]["G"][$i] ?></td>
  <td style="border-right:2px solid #ccc;"><?= $arKoeffData["RUS"]["H"][$i] ?></td>

  <td><?= round($arKoeffData["USA"]["A"][$i],5) ?></td>
  <td><?= round($arKoeffData["USA"]["B"][$i],5) ?></td>
  <td><?= $arKoeffData["USA"]["C"][$i] ?></td>
  <td><?= $arKoeffData["USA"]["D"][$i] ?></td>
  <td><?= $arKoeffData["USA"]["E"][$i] ?></td>
  <td><?= $arKoeffData["USA"]["G"][$i] ?></td>
  <td><?= $arKoeffData["USA"]["H"][$i] ?></td>
 </tr>
	<?endfor;?>

</table>
</div>

 <div id="summ_tab" class="tab-pane">
 <h2>Суммовые ряды</h2>
<table class="table">
 <tr>
  <td>Дата</td>
  <td>IMOEX</td>
  <td>Акции РФ</td>
  <td>SP500</td>
  <td>Акции США</td>
 </tr>
 	<?foreach($arKoeffData["LIST_OF_DATA"] as $date=>$summ):?>
 <tr>
  <td><?= $summ["DATE"] ?></td>
  <td><?= str_replace(".", ",", $summ["IMOEX"]) ?></td>
  <td><?= str_replace(".", ",", $summ["SUM"]) ?></td>
  <td><?= str_replace(".", ",", $summ["SP500"]) ?></td>
  <td><?= str_replace(".", ",", $summ["SUM_USA"]) ?></td>

 </tr>
	<?endforeach;?>

</table>
  </div>

  </div>

</div>
</div>
</div>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>