<?/* Статистика авторизаций клиентов */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	LocalRedirect("/");
	exit();
}
	  CModule::IncludeModule("iblock");

$arSelect = Array("ID", "NAME","DATE_CREATE", "IBLOCK_ID","PROPERTY_USER","PROPERTY_RADAR_END");
$arFilter = Array("IBLOCK_ID"=>IntVal(11), ">=PROPERTY_RADAR_END"=>date('Y-m-d'), "!PROPERTY_RADAR_END"=>false, "PROPERTY_PAYED"=>10, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arRadarClients = array();
$arRadarBuy = array();
while($ob = $res->fetch()){
if(!in_array($ob["PROPERTY_USER_VALUE"], $arRadarClients))
 $arRadarClients[] = $ob["PROPERTY_USER_VALUE"];
 $arRadarBuy[$ob["PROPERTY_USER_VALUE"]] = array("DATE_BUY"=>(new DateTime($ob["DATE_CREATE"]))->format('d.m.Y'), "RADAR_END"=>(new DateTime($ob["PROPERTY_RADAR_END_VALUE"]))->format('d.m.Y'));
}


		$hlblock     = HL\HighloadBlockTable::getById(35)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				"UF_USER" => $arRadarClients,
			),
			"select" => array("*"),
			"order" => array("UF_LOGIN_DATE"=>"ASC")
		));

	  $arAuthStat = array();
	  $arIpTable = array();
	  $arDatesTable = array();
	  while($row = $res->fetch()){
	//  	$date = (new DateTime($row["UF_LOGIN_DATE"]))->format('01.m.Y');
$arParams["ACTIVE_DATE_FORMAT"]="M Y";
$date = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($row["UF_LOGIN_DATE"], CSite::GetDateFormat()));


		$arAuthStat[trim($row["UF_LOGIN"])]["dates"][$date][] = $row;
		$arAuthStat[trim($row["UF_LOGIN"])]["info"] = $arRadarBuy[$row["UF_USER"]];
/*		if(!in_array($row["UF_IP"], $arIpTable[$row["UF_LOGIN"]]))
		   $arIpTable[$row["UF_LOGIN"]][] = $row["UF_IP"];*/
			if(!in_array($date, $arDatesTable))
		     $arDatesTable[] = $date;
	  }

	  	ksort($arAuthStat);
?>

	<div class="container">
		<h1>Статистика авторизаций клиентов</h1>
		<div class="main_content">
			<div class="main_content_inner">
				<div class="btn-group" role="group" aria-label="">

				</div>
				<br><br>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>№</th>
							<th>Логин</th>
							<th>Оплата</th>
							<th>Дт.оконч.</th>
							<?foreach($arDatesTable as $k => $arDate):?>
							 <th><?=$arDate?></th>
							<?endforeach;?>
						</tr>
					</thead>
					<tbody>
<? ?>

						<?$cntLogin = 1; foreach($arAuthStat as $login => $arDateList):?>
						  <tr>
						   <td><?= $cntLogin ?></td>
							<td><?=$login?></td>
							<td><?=$arDateList["info"]["DATE_BUY"]?></td>
							<td><?=$arDateList["info"]["RADAR_END"]?></td>
							 <?foreach($arDatesTable as $k => $arDate):?>
								<?if(array_key_exists($arDate, $arDateList["dates"])):?>
								  <td><?= count($arDateList["dates"][$arDate])?></td>
								<?else:?>
								  <td></td>
								<?endif;?>
							<?endforeach?>
						  </tr>
						  <?// if(count($arIpList)<2) continue; //Если число ip адресов на дату и время входа менее 2 ?>

							<!--							<tr class="<?= $class?>">
							  <td colspan="4"><a href="javascript:void(0);" style="text-decoration:none;" class="login_roll" data-login="<?= $loginCss ?>" data-alt-text="-">+ <strong><?= $login ?></strong><?=($cntIp>1?' - <span class="red"> разные ip за весь период: <strong>'.$cntIp.'</strong></span>':'')?></a></td>
							</tr>
							<tr class="login-<?=$loginCss?> hidden <?= $class?>">
									<th>Дата входа</th>
									<th>Почта</th>
									<th>IP</th>
									<th>Больше 3 попыток войти</th>
							</tr>-->
<!--								<?foreach($arDateList as $date=>$arRowList):?>
								  <?foreach($arRowList as $k=>$arRow):?>
										<tr class="login-<?=$loginCss?> hidden <?= $class?>">
											<td><?=$date?></td>
											<td><?=$arRow["UF_EMAIL"]?></td>
											<td><?=$arRow["UF_IP"]?></td>
											<td><?=!empty($arRow["UF_ERROR_AUTH_10"])?"Да":""?></td>
										</tr>
									<?endforeach?>
								<?endforeach?>-->


					<?$cntLogin++;
					endforeach?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
  <script>
  $(document).ready(function(){
	 $('.login_roll').on('click', function(){

		var rowId = $(this).data('login');
		var tmpText = '';
		if($(this).hasClass('showed')){
			$('.login-'+rowId).addClass('hidden');
			$(this).removeClass('showed');
			  //tmpText = $(this).text();
			  // $(this).text($(this).data('alt-text'));
			 //	$(this).data('alt-text', tmpText);
		} else {
			$('.login-'+rowId).removeClass('hidden');
			$(this).addClass('showed');
			 // tmpText = $(this).text();
			 //  $(this).text($(this).data('alt-text'));
			 //	$(this).data('alt-text', tmpText);
		}

    });


  });
  </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>