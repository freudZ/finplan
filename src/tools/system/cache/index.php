<?$_SERVER["DOCUMENT_ROOT"] = "/var/www/bitrix/data/www/fin-plan.org";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"]="fin-plan.org";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$arResult = array("result"=>"", "message"=>"");
$arCachesType['actions_data'] = 'actions_data';
$arCachesType['actions_usa_data'] = 'actions_usa_data';
$arCachesType['obligations_data'] = 'obligations_data';
$arCachesType['actions_usa_filters_data'] = 'actions_usa_filters_data';
$arCachesType['etf_data'] = 'etf_data';
$arCachesType['etf_filters_data'] = 'etf_filters_data';
$arCachesType['radar_events'] = 'radar_events';
$arCachesType['radar_ratings'] = 'radar_ratings';



$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
if($_REQUEST['type']=='all'){
	foreach($arCachesType as $cType){
	  $cache->clean($cType);
	}
	clearMoexGraph();
	clearSpbexGraph();
	 $arResult["result"] = "clear1";
	 $arResult["message"] = "Весь кэш очищен";

} elseif($_REQUEST['type']!='actions_rplus_data'){
	if($_REQUEST['type']=="spbex_graph"){
	  clearSpbexGraph();
	  	 $arResult["result"] = "clear2";
	 	 $arResult["message"] = "Кэш графиков цен акций США очищен";

	} else if($_REQUEST['type']=="moex_graph"){
	  clearMoexGraph();
	  	 $arResult["result"] = "clear2";
	 	 $arResult["message"] = "Кэш графиков цен по РФ очищен";

	} elseif($_REQUEST['type']=="valute"){
	  clearValuteCache();
	  	 $arResult["result"] = "clear3";
	 	 $arResult["message"] = "Кэш курсов валют очищен";

	} elseif($_REQUEST['type']=="targeting_banners"){
	  $CBanners = new CBanners(true);
	  unset($CBanners);
	 $arResult["result"] = "clear4";
	 $arResult["message"] = "Кэш таргетированных банеров очищен";

	} elseif(!empty($_REQUEST['type'])){
  	  $cache->clean($_REQUEST['type']);

	  //Очень долго потом открываются страницы компаний США
/*		if($_REQUEST['type']=='actions_usa_data'){ //Сбрасываем кеш данных на страницах компаний США
			$arSelect = Array("ID", "NAME", "IBLOCK_ID");
			$arFilter = Array("IBLOCK_ID"=>44);
			CModule::IncludeModule("iblock");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->fetch()){
				$cacheId = "company_usa_data".$ob["ID"];
				$cache->clean($cacheId);
			}
		}*/
		 if($_REQUEST['type'])


	  	 $arResult["result"] = "clear5";
	    $arResult["message"] = "Кэш [".$_REQUEST["type"]."] очищен";
	}

} elseif($_REQUEST['type']=='actions_rplus_data'){
  $cache->clean($_REQUEST['type']);
  	 $arResult["result"] = "clear6";
	 $arResult["message"] = "Весь кэш DATA очищен";
}
function clearMoexGraph(){
	if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   $GLOBALS['CACHE_MANAGER']->ClearByTag('ru_graphdata');

}
function clearSpbexGraph(){
/*	$graphCacheFolder = $_SERVER["DOCUMENT_ROOT"]."/bitrix/cache/spbex_graph/";
if (is_dir($graphCacheFolder)) {
    rmdir($graphCacheFolder);
}*/
if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   $GLOBALS['CACHE_MANAGER']->ClearByTag('usa_graphdata');

}
function clearValuteCache(){
	//$cache->clean("valute");
	if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   $GLOBALS['CACHE_MANAGER']->ClearByTag('valute');
}
?>


 <div class="container">

 	<?if(!empty($arResult["result"])):?>
		<p><span><?=$arResult["result"]?>&nbsp;</span><?= $arResult["message"] ?></p>
	<?endif;?>
 	<h1>Управление кешированием</h1>

	<div class="row">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "inpage_level_menu", Array(
	"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
		"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MAX_LEVEL" => "2",	// Уровень вложенности меню
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"COMPONENT_TEMPLATE" => ".default",
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"MENU_THEME" => "green"
	),
	false
);?>
 </div>
 </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>