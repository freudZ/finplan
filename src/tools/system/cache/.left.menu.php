<?
$aMenuLinks = Array(
	Array(
		"Очистить кеш акций РФ",
		"/tools/system/cache/?type=actions_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш ETF",
		"/tools/system/cache/?type=etf_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш акций США",
		"/tools/system/cache/?type=actions_usa_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш облигаций",
		"/tools/system/cache/?type=obligations_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш фьючерсов",
		"/tools/system/cache/?type=futures_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш валют",
		"/tools/system/cache/?type=currencies_radar_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш графиков цен акций США",
		"/tools/system/cache/?type=spbex_graph",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш графиков цен по РФ",
		"/tools/system/cache/?type=moex_graph",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш графиков макро-показателей по РФ",
		"/tools/system/cache/?type=all_macro_chart_data",
		Array(),
		Array(),
		""
	),

	Array(
		"Очистить кеш секторов в фильтре по США",
		"/tools/system/cache/?type=actions_usa_filters_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш показателей секторов по США",
		"/tools/system/cache/?type=sectors_usa_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш показателей отраслей по РФ",
		"/tools/system/cache/?type=industries_rus_data",
		Array(),
		Array(),
		""
	),

	Array(
		"Очистить кеш значений фильтров по США",
		"/tools/system/cache/?type=actions_usa_filters_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш значений фильтров по ETF",
		"/tools/system/cache/?type=etf_filters_data",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш списка событий радара",
		"/tools/system/cache/?type=radar_events",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш рейтингов радара",
		"/tools/system/cache/?type=radar_ratings",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш таргетирования баннеров",
		"/tools/system/cache/?type=targeting_banners",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить весь кеш по радару",
		"/tools/system/cache/?type=all",
		Array(),
		Array(),
		""
	),
	Array(
		"Очистить кеш для Data-FinPlan",
		"/tools/system/cache/?type=actions_rplus_data",
		Array(),
		Array(),
		""
	),
);

?>
