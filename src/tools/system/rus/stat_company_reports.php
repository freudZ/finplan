<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

use Bitrix\Highloadblock as HL;

global $USER, $APPLICATION;
$arGroups = $USER->GetUserGroupArray();
if (!$USER->IsAdmin() && !in_array(array(1, 5), $arGroups)) {
    LocalRedirect("/");
    exit();
}
CModule::IncludeModule("iblock");
$arResult = array();
$currentYear = (new DateTime())->format("Y");
$year = isset($_REQUEST["year"]) ? $_REQUEST["year"] : $currentYear;

$resA = new Actions();
$arCompanyPeriods = array();
$arCompanyBestValues = array();
$arCompanyData = array();
$allCompanyCount = array();
$allCompanyCountBySector = array();
$arCompanyCountReports = array();

foreach ($resA->arOriginalsItems as $arItem) {
    if (in_array($arItem["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar)) {
        continue;
    }
    if (!in_array($arItem["COMPANY"]["NAME"], $allCompanyCount)) {
        $allCompanyCount[$arItem["COMPANY"]["NAME"]] = $arItem["COMPANY"]["NAME"];
    }
    foreach ($arItem["PERIODS"] as $periodName => $periodVal) {
        if (strpos($periodName, $year) === false) {
            continue;
        }//Не берем ненужные периоды
        if (strpos($periodName, "MONTH") !== false) {
            continue;
        }//Не берем ненужные периоды
        

        
        $arPeriod = explode("-", $periodName);
        $arLastPeriod = ($arPeriod[0] == 1 ? 4 : $arPeriod[0] - 1) . "-" . ($arPeriod[0] == 1 ? $arPeriod[1] - 1 : $arPeriod[1]) . "-" . $arPeriod[2];
        //Если компания отчитывается по полугодиям тогда предыдущий квартал будет равен минус два квартала от текущего
        
        if(!array_key_exists($arLastPeriod, $arItem["PERIODS"])){
            $kv = $arPeriod[0];
            $lYear = $arPeriod[1];
            if($kv==1){
                $kv = 3;
                $lYear -= 1;
            }else if($kv==2){
                $kv = 4;
                $lYear -= 1;
            } else {
                $kv -=2;
            }
        
            $arLastPeriod = $kv . "-" .  $lYear . "-" . $arPeriod[2];
        }

        
        $pBest = false;
        $vBest = false;
        $sector = $arItem["PROPS"]["PROP_SEKTOR"];
        $allCompanyCountBySector[$sector] += 1;
 
        if (!in_array($arItem["COMPANY"]["NAME"], $arCompanyCountReports[$periodName]["COMPANIES"])) {
            $arCompanyCountReports[$periodName]["COMPANIES"][] = $arItem["COMPANY"]["NAME"];
        }
        if (!in_array($arItem["COMPANY"]["NAME"], $arCompanyCountReports[$periodName][$sector]["COMPANIES"])) {
            $arCompanyCountReports[$periodName][$sector]["COMPANIES"][] = $arItem["COMPANY"]["NAME"];
        }
        
        $arCompanyCount[$periodName] = isset($arCompanyCount[$arLastPeriod]) ? $arCompanyCount[$arLastPeriod] + 1 : $arCompanyCount[$periodName] + 1;
        $arCompanySectorCount[$periodName][$sector] = isset($arCompanyCount[$arLastPeriod]) ? $arCompanyCount[$arLastPeriod] + 1 : $arCompanyCount[$periodName] + 1;

        // echo $arLastPeriod."<br>";
        //if(isset($arItem["PERIODS"][$arLastPeriod])){
        if ($periodVal["Прибыль за год (скользящая)"] > $arItem["PERIODS"][$arLastPeriod]["Прибыль за год (скользящая)"]) {
            $pBest = true;
            if(!in_array($arItem["COMPANY"]["NAME"],$arCompanyBestValues[$periodName]["Прибыль лучше прошлого периода"])) {
                $arCompanyBestValues[$periodName]["Прибыль лучше прошлого периода"][] = $arItem["COMPANY"]["NAME"];
                $arCompanyBestValues[$periodName]["LAST_PERIOD"] = $arLastPeriod;
                $arCompanyBestValues[$periodName][$sector]["Прибыль лучше прошлого периода"][] = $arItem["COMPANY"]["NAME"];
                $arCompanyBestValues[$periodName][$sector]["LAST_PERIOD"] = $arLastPeriod;
            }
        }
        if ($periodVal["Выручка за год (скользящая)"] > $arItem["PERIODS"][$arLastPeriod]["Выручка за год (скользящая)"]) {
            $pBest = true;
            if(!in_array($arItem["COMPANY"]["NAME"],$arCompanyBestValues[$periodName]["Выручка лучше прошлого периода"])) {
                $arCompanyBestValues[$periodName]["Выручка лучше прошлого периода"][] = $arItem["COMPANY"]["NAME"];
                $arCompanyBestValues[$periodName]["LAST_PERIOD"] = $arLastPeriod;
                $arCompanyBestValues[$periodName][$sector]["Выручка лучше прошлого периода"][] = $arItem["COMPANY"]["NAME"];
                $arCompanyBestValues[$periodName][$sector]["LAST_PERIOD"] = $arLastPeriod;
            }
        }
        //}
        
        
        $arCompanyPeriods[$periodName]["COMPANIES"][$sector][$arItem["COMPANY"]["ID"]] = array(
          "Прибыль лучше прошлого периода"        => $pBest,
          "Выручка лучше прошлого периода"        => $vBest,
          "Прибыль за год (скользящая)"           => $periodVal["Прибыль за год (скользящая)"],
          "Выручка за год (скользящая)"           => $periodVal["Выручка за год (скользящая)"],
          "Прибыль за год (скользящая) Пр.период" => $arItem["PERIODS"][$arLastPeriod]["Прибыль за год (скользящая)"],
          "Выручка за год (скользящая) Пр.период" => $arItem["PERIODS"][$arLastPeriod]["Выручка за год (скользящая)"]
        );
        $arCompanyPeriods[$periodName]["PERIOD_TYPE"] = $arPeriod[2];
        $arCompanyPeriods[$periodName]["PERIOD_YEAR"] = $arPeriod[1];
        $arCompanyPeriods[$periodName]["PERIOD_VAL"] = $arPeriod[0];
        
        if ($arPeriod[0] == 1) {
        }
    }
    $arCompanyData[$arItem["COMPANY"]["ID"]] = $arItem["COMPANY"];
}
//Сортируем периоды
uasort($arCompanyPeriods, function ($item1, $item2) {
    $result = false;
    //Даты кварталов
    $quartDates = array(
      1 => "03-31",
      2 => "06-30",
      3 => "09-30",
      4 => "12-31",
    );
    if ($item1["PERIOD_TYPE"] == "KVARTAL" && $item1["PERIOD_YEAR"] > 2000 && $item2["PERIOD_YEAR"] > 2000) {
        $dstr1 = $item1["PERIOD_YEAR"] . "-" . $quartDates[$item1["PERIOD_VAL"]];
        $dt1 = new DateTime($dstr1);
        $dstr2 = $item2["PERIOD_YEAR"] . "-" . $quartDates[$item2["PERIOD_VAL"]];
        $dt2 = new DateTime($dstr2);
        $result = $dt1 < $dt2;
    }
    return $result;
});

?>
<div class="container">
    <h1>Статистика отчетности компаний РФ</h1>
    <div class="main_content">
        <div class="main_content_inner">
            <form class="inline-form sorting-form" enctype="multipart/form-data" method="post">
                <div class="form-group">
                    <label for="year">Получить статистику за год</label>
                    <? $yearCnt = $currentYear ?>
                    <select name="year">
                        <? while ($yearCnt > 2010): ?>
                            <option value="<?= $yearCnt ?>" <?= ($_REQUEST["year"] == $yearCnt ? 'selected' : '') ?>><?= $yearCnt ?></option>
                            <? $yearCnt--; ?>
                        <? endwhile; ?>
                    </select>

                    <!--                    <label for="sort">Сортировка</label>&nbsp;
                    <select name="sort">
                        <option value="cnt" <? /*=$_REQUEST["sort"]=="cnt"?'selected=""':''*/ ?>>По общему вхождению в портфели</option>
                        <option value="cnt_user" <? /*=$_REQUEST["sort"]=="cnt_user"?'selected=""':''*/ ?>>По вхождению в портфели пользователей</option>
                    </select>-->

                    <!--                    <label for="user">Направление</label>&nbsp;
                    <select name="dir">
                        <option value="asc" <? /*=$_REQUEST["dir"]=="asc"?'selected=""':''*/ ?> >По возрастанию</option>
                        <option value="desc" <? /*=$_REQUEST["dir"]=="desc"?'selected=""':''*/ ?> >По убыванию</option>
                    </select>-->
                    <button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>
                </div>

            </form>
            
            <? if (count($arCompanyPeriods) > 0): ?>
                <h3>Всего компаний: <?= count($allCompanyCount); ?></h3>
                <table class="table table-condensed table-bordered table-striped stat_company_reports">
                    <thead class="bg-info">
                    <tr>
                        <th colspan="3">Период</th>
                    </tr>
                    <tr>
                        <th>Сектор/Компания (всего отчетов)</th>
                        <th>Прибыль скользящая тек./<br>прошл.</th>
                        <th>Выручка скользящая тек./<br>прошл.</th>
                    </tr>

                    </thead>
                    
                    <? foreach ($arCompanyPeriods as $periodName => $periodVal): ?>
                        <tr class="bg-info">
                            <td class="period_header" data-rows="<?= $periodName ?>"><i
                                        class="fas fa-angle-right"></i> <?= $periodName ?>
                                <?$perPrc = round(count($arCompanyCountReports[$periodName]["COMPANIES"])/count($allCompanyCount)*100,2);?>
                                (<?= count($arCompanyCountReports[$periodName]["COMPANIES"]) ?> из <?= count($allCompanyCount)?> или <?=$perPrc?>%)
                            </td>
                            <?$bestPeriodPPercent = round(count($arCompanyBestValues[$periodName]["Прибыль лучше прошлого периода"])/count($arCompanyCountReports[$periodName]["COMPANIES"])*100,2)?>
                            <?$bestPeriodVPercent = round(count($arCompanyBestValues[$periodName]["Выручка лучше прошлого периода"])/count($arCompanyCountReports[$periodName]["COMPANIES"])*100,2)?>
                            <td><?= count($arCompanyBestValues[$periodName]["Прибыль лучше прошлого периода"]) ?> (<?=$bestPeriodPPercent?>%)</td>
                            <td><?= count($arCompanyBestValues[$periodName]["Выручка лучше прошлого периода"]) ?> (<?=$bestPeriodVPercent?>%)</td>
                        </tr>
                        <? $cntSectors = 0;
                        foreach ($periodVal["COMPANIES"] as $sectorName => $arCompanies): ?>
                            <tr class="sector-row  sector_<?= $periodName ?> hidden" data-sector="<?= $cntSectors ?>"
                                data-rows="<?= $periodName ?>">

                            <td class="sector-header" data-sector="<?= $cntSectors ?>" data-rows="<?= $periodName ?>">
                                &nbsp;&nbsp;<i class="fas fa-angle-right"></i> <?= $sectorName ?>
                                <? $sectPrc = round(count($arCompanyCountReports[$periodName][$sectorName]["COMPANIES"])/$allCompanyCountBySector[$sectorName]*100, 2);?>
                                (<?= count($arCompanyCountReports[$periodName][$sectorName]["COMPANIES"]) ?> из <?= $allCompanyCountBySector[$sectorName]?> или <?= $sectPrc?>%)
                            </td>
                                <?$bestPPercent = round((count($arCompanyBestValues[$periodName][$sectorName]["Прибыль лучше прошлого периода"])/count($arCompanyCountReports[$periodName][$sectorName]["COMPANIES"]))*100,2);?>
                                <?$besVPercent = round((count($arCompanyBestValues[$periodName][$sectorName]["Выручка лучше прошлого периода"])/count($arCompanyCountReports[$periodName][$sectorName]["COMPANIES"]))*100,2);?>
                            <td>
                                &nbsp;&nbsp;<strong><?= count($arCompanyBestValues[$periodName][$sectorName]["Прибыль лучше прошлого периода"])?></strong> (<?= $bestPPercent?>%)</td>
                            <td>
                                <strong><?= count($arCompanyBestValues[$periodName][$sectorName]["Выручка лучше прошлого периода"] )?></strong> (<?=$besVPercent?>%)</td>
                            </tr>
                            <? foreach ($arCompanies as $companyId => $companyVal): ?>
                                <?
                                $pStyle = '';
                                $vStyle = '';
                                if ($companyVal["Прибыль за год (скользящая) Пр.период"]) {
                                    if ($companyVal["Прибыль за год (скользящая)"] > $companyVal["Прибыль за год (скользящая) Пр.период"]) {
                                        $pStyle = 'background-color:#CCFFCC';
                                    } else {
                                        $pStyle = 'background-color:#FFCCCC';
                                    }
                                }
                                if ($companyVal["Выручка за год (скользящая) Пр.период"]) {
                                    if ($companyVal["Выручка за год (скользящая)"] > $companyVal["Выручка за год (скользящая) Пр.период"]) {
                                        $vStyle = 'background-color:#CCFFCC';
                                    } else {
                                        $vStyle = 'background-color:#FFCCCC';
                                    }
                                }
                                ?>
                                <tr class="row_<?= $periodName ?> sector_<?= $cntSectors ?> period_rows hidden">
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?= $arCompanyData[$companyId]["URL"] ?>"
                                                                   target="_blank"><?= $arCompanyData[$companyId]["NAME"] ?></a>
                                    </td>
                                    <td style="<?= $pStyle ?>">
                                        &nbsp;&nbsp;&nbsp;&nbsp;<?= $companyVal["Прибыль за год (скользящая)"] ?><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<i><?= $companyVal["Прибыль за год (скользящая) Пр.период"] ?></i>
                                    </td>
                                    <td style="<?= $vStyle ?>">
                                        &nbsp;&nbsp;&nbsp;&nbsp;<?= $companyVal["Выручка за год (скользящая)"] ?><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<i><?= $companyVal["Выручка за год (скользящая) Пр.период"] ?></i>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                            <? $cntSectors++; ?>
                        <? endforeach; ?>
                    <? endforeach; ?>
                </table>
            <? endif; ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.period_header').on('click', function () {
            if (!$(this).parents('tr').hasClass('active')) {
                $(this).parents('tr').addClass('active');
                var curPeriodRow = $(this).attr('data-rows');
                $(this).find('i').removeClass('fa-angle-right').addClass('fa-angle-down');
                $('.sector-row.sector_' + curPeriodRow).removeClass('hidden');
            } else {
                $(this).parents('tr').removeClass('active');
                var curPeriodRow = $(this).attr('data-rows');
                $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-right');
                $('.sector-row.sector_' + curPeriodRow).addClass('hidden');
            }
        });
        $('.sector-header').on('click', function () {
            if (!$(this).parents('tr').hasClass('active')) {
                $(this).parents('tr').addClass('active');
                var curSectorRow = $(this).attr('data-sector');
                var curPeriodRow = $(this).attr('data-rows');
                $(this).find('i').removeClass('fa-angle-right').addClass('fa-angle-down');
                $('.period_rows.sector_' + curSectorRow + '.row_' + curPeriodRow).removeClass('hidden');
            } else {
                $(this).parents('tr').removeClass('active');
                var curSectorRow = $(this).attr('data-sector');
                var curPeriodRow = $(this).attr('data-rows');
                $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-right');
                $('.period_rows.sector_' + curSectorRow + '.row_' + curPeriodRow).addClass('hidden');
            }
        });


    });
</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
