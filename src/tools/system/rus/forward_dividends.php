<?define("NEED_AUTH", true); //Только для авторизованных
define("WIDE_PAGE","Y"); //Верстка по ширине экрана для больших таблиц
define("NO_BX_PANEL","Y");//НЕ показывать панель битрикс

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<? global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	LocalRedirect("/");
	exit();
} ?>

<?$APPLICATION->SetTitle("Форвардные дивиденды");?>
<?
CModule::IncludeModule("iblock");
$actUsa = new ActionsUsa();

//Сектора
$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_MID_PE");
$arFilter = Array("IBLOCK_ID"=>IntVal(58), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arSectorPE = array();
while($ob = $res->Fetch()){
 $arSectorPE[$ob["NAME"]] = $ob["PROPERTY_MID_PE_VALUE"];
}

$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>IntVal(55), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("PROPERTY_TARGET_CUSTOM"=>"DESC"), $arFilter, false, false, $arSelect);
?>
<div class="col-md-12">
<h1><? $APPLICATION->ShowTitle(); ?></h1>
<div class="scrollX t12" >

 <table class="table table-striped table-bordered table-condensed">
	<tr class="">
		<th>Название</th>
		<th>Вал.</th>
		<th>Цена</th>
		<th>Ручной прогноз</th>
		<th>Прогноз</th>
		<th>Недооценка</th>
		<th>P/E</th>
		<th>Cр.отрсл. <br>P/E</th>
		<th>Прибыль тек</th>
		<th>Прибыль <br>-1 год</th>
		<th>Кв.Приб. <br>-1 год</th>
		<th>Прибыль <br>-2 года</th>
		<th>Кв.Приб. <br>-2 года</th>
		<th>Прибыль <br>-3 года</th>
		<th>Кв.Приб. <br>3 года</th>
	</tr>

<?
while($ob = $res->GetNextElement()):
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
 $bgRow = floatval($arProps["TARGET_CUSTOM"]["VALUE"])>0?'#dff0d8;':'';
 $radarData = $actUsa->getItem($arProps["ISIN"]["VALUE"]);
/* echo "<pre  style='color:black; font-size:11px;'>";
    print_r($radarData);
    echo "</pre>";*/
reset($radarData["PERIODS"]);
 $lastPeriod = key($radarData["PERIODS"]);
 $lastPeriod = explode("-",$lastPeriod);
 $periodYear = $lastPeriod[0].'-'.($lastPeriod[1]-1).'-'.$lastPeriod[2];
 $periodTwoYear = $lastPeriod[0].'-'.($lastPeriod[1]-2).'-'.$lastPeriod[2];
 $periodThreeYear = $lastPeriod[0].'-'.($lastPeriod[1]-3).'-'.$lastPeriod[2];

 $arPeriodYear = array();
 if(array_key_exists($periodYear, $radarData["PERIODS"])){
 $arPeriodYear = $radarData["PERIODS"][$periodYear];
 }
 $arPeriodTwoYear = array();
 if(array_key_exists($periodTwoYear, $radarData["PERIODS"])){
 $arPeriodTwoYear = $radarData["PERIODS"][$periodTwoYear];
 }
 $arPeriodThreeYear = array();
 if(array_key_exists($periodThreeYear, $radarData["PERIODS"])){
 $arPeriodThreeYear = $radarData["PERIODS"][$periodThreeYear];
 }
 ?>
 <tr style="background-color:<?= $bgRow ?>">
 	<td><?= $arFields["NAME"] ?> [<?=$arProps["SECID"]["VALUE"]?>]</td>
 	<td><?= $arProps["CURRENCY"]["VALUE"] ?></td>
 	<td><?= $arProps["LASTPRICE"]["VALUE"] ?></td>
 	<td><?= floatval($arProps["TARGET_CUSTOM"]["VALUE"])>0?$arProps["TARGET_CUSTOM"]["VALUE"]:0; ?></td>
 	<td><?= floatval($arProps["TARGET"]["VALUE"])>0?$arProps["TARGET"]["VALUE"]:0; ?></td>
 	<td><?= $arProps["UNDERSTIMATION"]["VALUE"] ?></td>
 	<td><?= $radarData["DYNAM"]["PE"] ?></td>
 	<td><?= $arSectorPE[$arProps["SECTOR"]["VALUE"]] ?></td>
	<td><?= $radarData["DYNAM"]["Прибыль"] ?></td>
 	<td><?= $arPeriodYear["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$periodYear) ?></td>
 	<td><?= $arPeriodTwoYear["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$periodTwoYear) ?></td>
 	<td><?= $arPeriodThreeYear["Прибыль за год (скользящая)"] ?></td>
 	<td><?= str_replace('-KVARTAL','',$periodThreeYear) ?></td>
 </tr>

 <?endwhile;?>
 </table>
 </div>
 </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>