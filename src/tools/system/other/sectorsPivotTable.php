<?php
define("WIDE_PAGE", "Y");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)) {
	LocalRedirect("/");
	exit();
}
CModule::IncludeModule("iblock");
$clearTmpFix = false;
if(isset($_REQUEST['cleanTmp']) && $_REQUEST['cleanTmp'] == 'clean') {
	$clearTmpFix = true;
}

$CSectorsUsa = new SectorsUsa($clearTmpFix);
//получение сектора
$sectors = array();
$arFilter = array("IBLOCK_ID" => $CSectorsUsa->industriesUsaIblockId);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "NAME", "CODE"));
while($item = $res->GetNext()) {
	$sectors[$item['NAME']] = $item;
}
$actions = $CSectorsUsa->getSectorActionsData($_REQUEST['sector']);
$arFixParams = $CSectorsUsa->arFixParams;
$sector = $CSectorsUsa->getItem($sectors[$_REQUEST['sector']]['CODE'], true, !empty($_REQUEST['year']) ? $_REQUEST['year'] : false);
$res = array();
$marks = $CSectorsUsa->arParamsList;
unset($CSectorsUsa);

?>

    <style>
    </style>
    <div class="container-fluid">
        <h1>Сводная таблица секторов США</h1>
        <div class="row">
            <div class="col-md-12">
                <form enctype="multipart/form-data" id="filter_pivot" method="post">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="sector_usa">Отрасль</label>&nbsp;
                                <select id="sector_usa" name="sector" class="form-control">
									<? foreach($sectors as $sectorItem): ?>
                                        <option value="<?= $sectorItem['NAME']; ?>" <?= ($sectorItem['NAME'] == $_REQUEST['sector']) ? 'selected' : null ?>>
											<?= $sectorItem['NAME'] ?>
                                        </option>
									<? endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="mark">Показатель</label>&nbsp;
                                <select id="mark" name="mark" class="form-control">
									<? foreach($marks as $mark): ?>
                                        <option <?= ($mark == $_REQUEST['mark']) ? 'selected' : null ?>>
											<?= $mark ?>
                                        </option>
									<? endforeach; ?>
                                </select>

                            </div>
                            <div class="col-md-2">
                                <label for="sector_usa">Период</label>&nbsp;
                                <select id="sector_usa" name="year" class="form-control">
                                    <option value="" <?= (!isset($_REQUEST['year']) || empty($_REQUEST['year'])) ? 'selected' : null ?>>
                                        Последние 5 кварталов
                                    </option>
									<? for($yr = (new DateTime())->format('Y'); $yr > 2011; $yr--): ?>
                                        <option value="<?= $yr; ?>" <?= ($yr == $_REQUEST['year']) ? 'selected' : null ?>>
											<?= $yr ?>
                                        </option>
									<? endfor; ?>
                                    <option value="all" <?= (isset($_REQUEST['year']) && $_REQUEST['year'] == 'all') ? 'selected' : null ?>>
                                        Все периоды
                                    </option>
                                </select>

                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="checkbox" value="clean" name="cleanTmp"
                                       title="Очистить временные правки для появившихся реальных значений">
                                <label for="cleanTmp"
                                       title="Очистить временные правки для появившихся реальных значений">Очистить
                                    временные правки</label>
                            </div>
                            <div class="col-md-9">
                                <p><strong>Цветовая маркировка: </strong>&nbsp;<span
                                            class="bg-warning">Временная правка</span>&nbsp; <span class="bg-info">Постоянная правка</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>
                </form>
            </div>
        </div>

        <!--<form enctype="multipart/form-data" method="post">
    <div class="form-group">
        <label for="sector_usa">Выберите сектор</label>&nbsp;
        <select id="sector_usa" name="sector" class="form-control">
            <? /*foreach ($sectors as $sectorItem):*/ ?>
            <option <? /*=($sectorItem['NAME'] == $_REQUEST['sector'])?'selected': null*/ ?>>
                <? /*=$sectorItem['NAME']*/ ?>
            </option>
            <? /*endforeach;*/ ?>
        </select>
        <br>
        <label for="mark">Выберите Показатель</label>&nbsp;
        <select id="mark" name="mark" class="form-control">
            <? /*foreach ($marks as $mark):*/ ?>
            <option <? /*=($mark == $_REQUEST['mark'])?'selected': null*/ ?>>
                <? /*=$mark*/ ?>
            </option>
            <? /*endforeach;*/ ?>
        </select>
    </div>
    <button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>
</form>-->
        <br>
		<? if($_REQUEST['send'] && $_REQUEST['sector'] && $_REQUEST['mark']): ?>
            <div class="row">
                <div class="col-md-12">
                    <div style="overflow-x: scroll;">
                        <table class="table table-pivot table-striped table-condensed table-bordered bg-success"
                               style="font-size:0.8em">
                            <thead>
                            <tr>
                                <th>Акция</th>
								<?
								foreach($sector['EXPANDED_PERIODS']['PERIODS'] as $kp=>$period):
									?>
                                    <th><?= $period ?>
										  <span class="fillFromPastPeriod" data-id="act_col_id_<?= $kp ?>" style="display: block; float:right; text-decoration: none; cursor:pointer;" class="fa-xs fa-stack"><i class="fas fa-arrow-left"></i></span>
												</th>
												<? endforeach; ?>
                            </tr>
                            </thead>
                            <tbody>
							<? $actCnt = 0; ?>
							<? foreach($actions as $name => $action): ?>

                                <tr id="act_row_id_<?= $actCnt ?>" data-secid="<?= $action["SECID"] ?>">
                                    <td nowrap><?= $name ?>
                                        <span data-id="act_row_id_<?= $actCnt ?>" data-mode="row" data-target="#industries_popup"  data-toggle="modal"
                                              style="display: block; float:right; text-decoration: none; cursor:pointer;" class="fa-xs fa-stack"><i class="fas fa-edit"></i>
                                        </span>
                                    </td>
									<? foreach($sector['EXPANDED_PERIODS']['PERIODS'] as $sectorPeriod):
										if(!$res[$sectorPeriod]) {
											$res[$sectorPeriod] = 0;
										}
										$arPeriod = explode("-", $sectorPeriod);
										$class = '';
										$type = '';
										if(array_key_exists($action["SECID"], $arFixParams[$_REQUEST['sector']][$_REQUEST['mark']])) {

											$arFixAction = $arFixParams[$_REQUEST['sector']][$_REQUEST['mark']][$action["SECID"]];

											if(array_key_exists($arPeriod[1], $arFixAction) && array_key_exists($arPeriod[0], $arFixAction[$arPeriod[1]])) {


												/*  echo $arPeriod[1].' '.$arPeriod[0]."<pre  style='color:#000000; font-size:11px;'>";
																																print_r($arFixAction[$arPeriod[1]][$arPeriod[0]]);
																																echo "</pre>";*/
												$type = $arFixAction[$arPeriod[1]][$arPeriod[0]]["UF_TYPE"];
												if($type == "TMP") {
													$class = 'bg-warning';
												}
												if($type == "ALWAYS") {
													$class = 'bg-info';
												}
											}


										}
										$res[$sectorPeriod] += $action["PERIODS"][$sectorPeriod][$_REQUEST['mark']];
										?>
                                        <td class="<?= $class ?>"
                                            data-kv="<?= $arPeriod[0]; ?>"
                                            data-year="<?= $arPeriod[1]; ?>"
                                            data-type="<?= (!empty($type) ? $type : '') ?>"
                                            data-mark="<?= $_REQUEST['mark'] ?>"
                                            data-val="<?= $action["PERIODS"][$sectorPeriod][$_REQUEST['mark']] ?: 0 ?>"
                                        ><?= formatNumber($action["PERIODS"][$sectorPeriod][$_REQUEST['mark']]) ?>
                                            <span data-id="act_row_id_<?= $actCnt ?>" data-target="#industries_popup"
                                                  data-toggle="modal" data-mode="cell" style="display: block; float:right; text-decoration: none; cursor:pointer;"
                                                  class="fa-xs fa-stack">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </td>
									<? endforeach; ?>
                                </tr>
								<? $actCnt++; ?>
							<? endforeach; ?>
                            <tr>
                                <td>result</td>
								<? foreach($sector['EXPANDED_PERIODS']['PERIODS'] as $sectorPeriod): ?>
                                    <td><?= formatNumber($res[$sectorPeriod]) ?></td>
								<? endforeach; ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		<? endif; ?>

        <div class="modal modal_black fade" id="industries_popup" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="modal-title uppercase cell-title">Внести поправку значения</p>
                        <p class="modal-title uppercase row-title hidden">Правка для всей строки</p>
                        <button type="button" class="close"><i class="fa fa-close fa-xs"></i></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <input type="hidden" id="UF_COUNTRY" value="USA" name="UF_COUNTRY">
                            <input type="hidden" id="UF_SECTOR_NAME" name="UF_SECTOR_NAME">
                            <input type="hidden" id="UF_PARAM_NAME" name="UF_PARAM_NAME">
                            <input type="hidden" id="UF_ACTIVE_CODE" name="UF_ACTIVE_CODE">
                            <input type="hidden" id="UF_KVARTAL" name="UF_KVARTAL">
                            <input type="hidden" id="UF_YEAR" name="UF_YEAR">
                            <input type="hidden" id="MODE" name="MODE" value="">
                            <div class="form_element">
                                <label for="type">Тип значения</label>
                                <select name="UF_TYPE" id="UF_TYPE">
                                    <option value="TMP" selected>Временное</option>
                                    <option value="ALWAYS">Постоянное</option>
                                </select><br>
                                <i>Временную правку нельзя внести если существущее значение больше нуля!</i>
                            </div>
                            <div class="form_element">
                                <input type="text" id="UF_VALUE" name="UF_VALUE" placeholder="Корректировка"/>
                            </div>
                            <div class="form_element">
                                <label for="delete">Удалить корректировку</label>
                                <input type="checkbox" id="delete" name="delete" value="y"/>
                            </div>
                            <br/>
                            <div class="form_element text-center">
                            <span class="save_industry_values button" type="submit"
                                  data-dismiss="modal">Подтвердить</span>
                                <span class="button btn-warning" data-dismiss="modal">Отмена</span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>