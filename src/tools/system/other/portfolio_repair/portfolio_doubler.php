<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
  //Дублирует портфель только в историю (для перехода на новый формат хранения истории) на dev
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
?>
<?$APPLICATION->SetTitle("Конвертер в портфели 2.0");?>
<?
$subzeroIblockYValueId=276; //Перед запуском проверить id значения Y в инфоблоке портфелей на боевом сайте
$pid = intval($_REQUEST["pid"]);
$go_double = htmlspecialchars($_REQUEST["go_double"]);
?>
<?if($pid>0){

 $CPortfolio = new CPortfolio();
 $arPortfolio = $CPortfolio->getPortfolio($pid);
 //$arDeals = $CPortfolio->getPortfolioActivesList1($pid, false, false);

 			$hlblockHist_id        = 27; // портфели история
			$hlblockHist           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblockHist_id)->fetch();
			$entityHist            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblockHist);
			$entityHist_data_class = $entityHist->getDataClass();
			//Получаем сделки портфеля
			$rsData = $entityHist_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC'),
				'select' => array('*'),
				'filter' => array('UF_PORTFOLIO' => $pid)
			));
			$arDeals      = array();
			$arDeals      = $rsData->fetchAll();

 if($go_double=="Y"){
			$pidCopy = 0;
			$el                      = new CIBlockElement;
			$PROP                    = array();
			foreach($arPortfolio["PROPERTIES"] as $propCode=>$propValue){
				$PROP[$propCode] = $propValue["VALUE"];
			}
			$PROP["VERSION"] = "2.0";
			$arSelect = Array("ID", "NAME");
         $arFilter = Array("IBLOCK_ID"=>52, "NAME"=>$arPortfolio["NAME"]." (Вер: 2.0)", "PROPERTY_VERSION"=>"2.0", "PROPERTY_OWNER_USER"=>$PROP["OWNER_USER"]);
         $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$countExistName = 0;
			while($ob = $res->fetch()){
				$countExistName++;
         }



			$convertNote = "Портфель сконвертирован в формат 2.0";
			$arLoadProductArray = Array(
				"MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
				"IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
				"IBLOCK_ID" => 52, //Портфели
				"PROPERTY_VALUES" => $PROP,
				"NAME" => $arPortfolio["NAME"]."".($countExistName>0?(' ('.($countExistName+1).')'):''),
				"PREVIEW_TEXT" => strlen($arPortfolio["PREVIEW_TEXT"])>0?$arPortfolio["PREVIEW_TEXT"].PHP_EOL.$convertNote:$convertNote,
				"ACTIVE" => "Y", // активен
			);

			if ($pidCopy = $el->Add($arLoadProductArray)) {
				$result = array("result" => "ok", "id" => $pidCopy);
				CIBlockElement::SetPropertyValuesEx($pidCopy, 52, array("SUBZERO_CACHE_ON" => $subzeroIblockYValueId));
				CIBlockElement::SetPropertyValuesEx($pidCopy, 52, array("VERSION" => '2.0'));
			} else {
				$result = array("result" => "error", "error_text" => $el->LAST_ERROR);
			}

			$CPortfolio->clearCachePortfolioCntForUser($PROP['OWNER_USER']);
			$CPortfolio->clearPortfolioListForUserCache($PROP['OWNER_USER']);

	//Заполняем историю заново только на основе сделок
	if($pidCopy>0){
		$arAddedDeal = array();
	foreach($arDeals as $hid=>$arDeal){
			if(count($arDeal)>0){
				$lotsize = $arDeal["UF_INLOT_CNT"];
				if(intval($lotsize)==0 && $arDeal["UF_ACTIVE_TYPE"]=='bond'){
					$lotsize = 1;
				}
				$arFields = array(
				 //"UF_DEAL_ID"=>$arDeal["ID"],
				 "UF_HISTORY_ACT"=>$CPortfolio->arHistoryActions["ACT_PLUS"],
				 "portfolioId"=>$pidCopy,
				 "cntActive"=>$arDeal["UF_LOTCNT"],
				 "lotsize"=>$lotsize,
				 "idActive"=>$arDeal["UF_ACTIVE_ID"],
				 "codeActive"=>$arDeal["UF_ACTIVE_CODE"],
				 "typeActive"=>$arDeal["UF_ACTIVE_TYPE"],
				 "nameActive"=>$arDeal["UF_ACTIVE_NAME"],
				 "urlActive"=>$arDeal["UF_ACTIVE_URL"],
				 "currencyActive"=>$arDeal["UF_CURRENCY"],
				 "currencyEmitent"=>$arDeal["UF_CURRENCY"],//Нужно дополнительно запрашивать по эмитенту
				 "priceActive"=>$arDeal["UF_LOTPRICE"],
				 "secid"=>$arDeal["UF_SECID"],
				 "nameActiveEmitent"=>$arDeal["UF_EMITENT_NAME"],
				 "urlActiveEmitent"=>$arDeal["UF_EMITENT_URL"],
				 "dateActive"=>$arDeal["UF_ADD_DATE"],
				 "price_one"=>$arDeal["UF_LOTPRICE"]/$lotsize,
				);
				if(!in_array($arDeal["UF_CURRENCY"], $CPortfolio->arRoubleCodes)){
					$priceActiveinCurrency = $CPortfolio->getHistActionsPrices($arDeal["UF_ACTIVE_TYPE"], $arDeal["UF_ADD_DATE"], $arDeal["UF_SECID"]);
					$priceCurrency = $CPortfolio->getHistActionsPrices('currency', $arDeal["UF_ADD_DATE"], $arDeal["UF_CURRENCY"]);
					$alterLastPriceCurrency = round($arDeal["UF_LOTPRICE"]/$priceCurrency["price"], 4);
					$arFields["lastpriceActiveValute"] = $priceActiveinCurrency["price_valute"]==$alterLastPriceCurrency?$priceActiveinCurrency["price_valute"]:$alterLastPriceCurrency;
				}

			  //$CPortfolio->addToHistory("PLUS", $arFields);

													 // $pid, $data, $copy_data, $cacheActivity, $auto, $no_recount_cache, $owner

			  $CPortfolio->addPortfolioActive($pidCopy, $arFields, array(), $CPortfolio->arHistoryActions["ACT_PLUS"], true, true, 'user');
			  $arAddedDeal[] = $arDeal["UF_ACTIVE_NAME"].'; Дт= '.$arDeal["UF_ADD_DATE"].'; Кол-во= '.$arDeal["UF_LOTCNT"].'; Цена лота= '.$arDeal["UF_LOTPRICE"];
	 		}
  		}
		$CPortfolio->recountCacheOnPortfolio($pidCopy, "01.01.2011", false, 'autobalance');
	}
 }

}
?>


<script>
<?if(isset($_REQUEST["go_double"])):?>
  window.history.pushState(null, null, 	location.protocol + "//" +
                location.host +
                location.pathname +
                location.hash.split('?')[0]+'?pid='+'<?=$_REQUEST["pid"]?>');
	<?endif;?>
</script>


<div class="container">
 <h1><? $APPLICATION->ShowTitle(false); ?></h1>
 <div class="row">
  <div class="col-md-6">
  <p>Перед запуском проверить id значения Y в инфоблоке портфелей на боевом сайте. Текущее значение <?= $subzeroIblockYValueId ?></p>
<form id="portfolio_repair" action="" name="portfolio_repair">
 <label>ID портфеля</label><input type="text" name="pid" id="pid" value="<?=(!empty($_REQUEST["pid"])?$_REQUEST["pid"]:'')?>"><br>
 <input type="checkbox" value="Y" name="go_double"><label>&nbsp;Дублировать в портфель 2.0</label>
 <br>
 <button type="submit" value="check_double">Проверить/дублировать</button>
</form>
  </div>
 </div>
 <div class="row">
  <div class="col-md-12 check_report">
	  <h2>Результат конвертирования:</h2>
		 <?if($pidCopy>0):?>
		  Создана копия портфеля в стандарте 2.0: <a href="/portfolio/portfolio_<?=$pidCopy?>/" target="_blank"><?=$arLoadProductArray["NAME"];?></a>
		  <br>
		  <strong>Скопированы сделки:</strong> <br>
		  <?foreach($arAddedDeal as $deals):?>
		    <?=$deals?><br>
		  <?endforeach;?>
		 <?endif;?>
  </div>

 </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>