<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
?>
<?$APPLICATION->SetTitle("Ремонт портфелей");?>
<?

$pid = intval($_REQUEST["pid"]);
$go_repair = htmlspecialchars($_REQUEST["go_repair"]);
 $arProblems = array();
 $problemsCnt = 0;
 $arHistory = array();
?>
<?if($pid>0){

 $CPortfolio = new CPortfolio();

 $arHistory = $CPortfolio->getPortfolioHistory($pid);
 $arDeals = $CPortfolio->getPortfolioActivesList($pid, false, false);


 foreach($arHistory as $arItem){
 	if(empty($arItem["UF_CURRENCY"]) || intval($arItem["UF_ACTIVE_ID"])==0){
 	 $problemsCnt++;
	 $arProblems["HISTORY"][$arItem["ID"]] = $arItem["UF_DEAL_ID"];
 	}
	if(empty($arItem["UF_DEAL_ID"])){
		$problemsCnt++;
	 $arProblems["EMPTY_DEAL_ID"][$arItem["ID"]] = $arItem["UF_ACTIVE_ID"];
	}
 }
        $hlblock = HL\HighloadBlockTable::getById(28)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();

 if($go_repair=="Y" && count($arProblems["EMPTY_DEAL_ID"])>0){

	//Удаляем записи истории по данному портфелю
	foreach($arProblems["EMPTY_DEAL_ID"] as $delId=>$data){
		$entityClass::delete($delId);
	}

	//Заполняем историю заново только на основе сделок
	foreach($arDeals as $hid=>$arDeal){
		if(count($arDeal)>0){
			$arFields = array(
			 "UF_DEAL_ID"=>$arDeal["ID"],
			 "portfolioId"=>$arDeal["UF_PORTFOLIO"],
			 "cntActive"=>$arDeal["UF_LOTCNT"],
			 "lotsize"=>$arDeal["UF_INLOT_CNT"],
			 "idActive"=>$arDeal["UF_ACTIVE_ID"],
			 "codeActive"=>$arDeal["UF_ACTIVE_CODE"],
			 "typeActive"=>$arDeal["UF_ACTIVE_TYPE"],
			 "nameActive"=>$arDeal["UF_ACTIVE_NAME"],
			 "urlActive"=>$arDeal["UF_ACTIVE_URL"],
			 "currencyActive"=>$arDeal["UF_CURRENCY"],
			 "currencyEmitent"=>$arDeal["UF_CURRENCY"],//Нужно дополнительно запрашивать по эмитенту
			 "priceActive"=>$arDeal["UF_LOTPRICE"],
			 "secid"=>$arDeal["UF_SECID"],
			 "nameActiveEmitent"=>$arDeal["UF_EMITENT_NAME"],
			 "urlActiveEmitent"=>$arDeal["UF_EMITENT_URL"],
			 "dateActive"=>$arDeal["UF_ADD_DATE"],
			 "price_one"=>$arDeal["UF_PRICE_ONE"],
			);

		  $CPortfolio->addToHistory("PLUS", $arFields);

		}
	}
 }

 if($go_repair=="Y" && count($arProblems["HISTORY"])>0){
	foreach($arProblems["HISTORY"] as $hid=>$dealId){
		$arDeal = $arDeals[$dealId];

		if(count($arDeal)>0){
			$arFields = array();
			$arFields = array(
			 "UF_ACTIVE_ID"=>$arDeal["UF_ACTIVE_ID"],
			 "UF_ACTIVE_TYPE"=>$arDeal["UF_ACTIVE_TYPE"],
			 "UF_ACTIVE_NAME"=>$arDeal["UF_ACTIVE_NAME"],
			 "UF_ACTIVE_URL"=>$arDeal["UF_ACTIVE_URL"],
			 "UF_EMITENT_NAME"=>$arDeal["UF_EMITENT_NAME"],
			 "UF_EMITENT_URL"=>$arDeal["UF_EMITENT_URL"],
			 "UF_ACTIVE_CODE"=>$arDeal["UF_ACTIVE_CODE"],
			 "UF_CURRENCY"=>$arDeal["UF_CURRENCY"],
			 "UF_ACTIVE_LASTPRICE"=>$arDeal["UF_ACTIVE_LASTPRICE"],
			 "UF_SECID"=>$arDeal["UF_SECID"],
			 "UF_INLOT_CNT"=>$arDeal["UF_INLOT_CNT"],
			 "UF_PRICE_ONE"=>$arDeal["UF_PRICE_ONE"],
			 "UF_DATE_PRICE_ONE"=>$arDeal["UF_ADD_DATE"],
			);

        $hlblock = HL\HighloadBlockTable::getById(28)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();

		  $entityClass::update($hid, $arFields);

		}

	}
 }

 $arHistory = $CPortfolio->getPortfolioHistory($pid);
 $arDeals = $CPortfolio->getPortfolioActivesList($pid, false, false);

 $problemsCnt = 0;
 foreach($arHistory as $arItem){
 	if(empty($arItem["UF_CURRENCY"]) || intval($arItem["UF_ACTIVE_ID"])==0){
 	 $problemsCnt++;
	 $arProblems["HISTORY"][$arItem["ID"]] = $arItem["UF_DEAL_ID"];
 	}
	if(empty($arItem["UF_DEAL_ID"])){
		$problemsCnt++;
	 $arProblems["EMPTY_DEAL_ID"][$arItem["ID"]] = $arItem["UF_ACTIVE_ID"];
	}
 }

}
?>





<div class="container">
 <h1><? $APPLICATION->ShowTitle(false); ?></h1>
 <div class="row">
  <div class="col-md-6">
<form id="portfolio_repair" action="" name="portfolio_repair">
 <label>ID портфеля</label><input type="text" name="pid" id="pid" value="<?=(!empty($_REQUEST["pid"])?$_REQUEST["pid"]:'')?>"><br>
 <input type="checkbox" value="Y" name="go_repair"><label> Ремонтировать портфель</label>
 <br>
 <button type="submit" value="check_repair">Проверить/починить</button>
</form>
  </div>
 </div>
 <div class="row">
  <div class="col-md-12 check_report">
	  <h2>Найденные проблемы в портфеле:</h2>
	  <?
		 if(count($arProblems["EMPTY_DEAL_ID"])>0):?>
		 <p><span style="color: #FF0000">Нет привязки сделок истории к главной таблице (старый портфель)</span></p>
		 <?endif;?>

		 <?if(count($arProblems["HISTORY"])>0):?>
		 <p><span style="color: #FF0000">Не полные данные в записях истории</span></p>
		 <?endif;?>

		 <?if($problemsCnt==0):?>
			проблем не найдено
		 <?endif;?>
  </div>

 </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>