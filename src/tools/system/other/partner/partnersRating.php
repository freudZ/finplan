<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();
$notInGroups = true;
foreach($arGroups as $itemGroup){
	if(in_array($itemGroup, array(1,5,15))){
		$notInGroups = false;
		break;
	}
}

if(!$USER->IsAdmin() && $notInGroups){
  	LocalRedirect("/");
  	exit();
}
CModule::IncludeModule("iblock");

//Получаем список рефералов из CRM
$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_REFERAL");
$arFilter = Array("IBLOCK_ID"=>IntVal(19), "!PROPERTY_REFERAL"=>false, );
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arCRMReferals = array();
while($ob = $res->fetch()){
	global $USER;

		$rsUser = CUser::GetByLogin($ob["PROPERTY_REFERAL_VALUE"]);
		$arUser = $rsUser->Fetch();
 $arCRMReferals[$arUser["ID"]] = $arUser["ID"];
}



//Получаем список начисленных бонусов и группируем по владельцам
   $arPartnersBonuses = array();
	$hlblock   = HL\HighloadBlockTable::getById(18)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();
	$resHL = $entityClass::getList(array(
		"select" => array(
			"*",
		),
		//"filter" => array("=UF_TYPE"=>9),
		"group" => array("UF_USER"),
		"order" => array("UF_USER"=>"ASC", "UF_DATE"=>"ASC"),
		//"limit" => 1000,
	));
	while($row = $resHL->fetch()){
		//$arPartnersBonuses[$row["UF_USER"]]["ITEMS"][] = $row;
		if($row["UF_TYPE"]==9){ //Суммируем начисления
		$arPartnersBonuses[$row["UF_USER"]]["SUMM"] += $row["UF_VAL"];
		}
		if($row["UF_TYPE"]==10){ //Суммируем списания
		$arPartnersBonuses[$row["UF_USER"]]["SUMM_MINUS"] += $row["UF_VAL"];
		}


	  	//$arPartnersBonuses[$row["UF_USER"]]["REF_CNT"] = $payedReferals;
	}

	//Дополним массив из начислений рефералами из CRM с проверкой от дублей в массиве
	if(count($arCRMReferals)>0){
		foreach($arCRMReferals as $k=>$userRefId){
		  if(!array_key_exists($k, $arPartnersBonuses) && intval($k)>0){
		  	$arPartnersBonuses[$k] = array("SUMM"=>0, "SUMM_MINUS"=>0);
		  }
		}
	}

	$bonusGift = new CBonusGift();
	$arSummary = array();
	foreach($arPartnersBonuses as $uid=>$val){
		$items = Bonuses::getOperations($uid);
		$payedReferals = 0;
		$commonReferals = 0;
			  foreach($items as $item){
			  	if($item["UF_TYPE"]==9)
				  $payedReferals++;
			  }



		$userReferals = array();
		$userReferals = promocodes::getUserReferalsToLK($uid, true);


 			  if(count($userReferals)>0 ){
				  if(array_key_exists(88,$userReferals)){
				  	$commonReferals += intval(count($userReferals[88]));
					}
				  if(array_key_exists("",$userReferals)){
				  	$commonReferals += intval(count($userReferals[89]));
				  	$commonReferals += $payedReferals;
				  	//$payedReferals += intval(count($userReferals[89]));
					}

			  }
		$arPartnersBonuses[$uid]["REF_PAYED"] =  intval($payedReferals);
		$arPartnersBonuses[$uid]["REF_CNT"] = intval($commonReferals);
		$arPartnersBonuses[$uid]["CLIENT"] = $bonusGift->isOwnerClient($uid)?'Y':'';
		$arSummary["BALL_SUMM"] += $arPartnersBonuses[$uid]["SUMM"];
		$arSummary["BALL_SUMM_MINUS"] += $arPartnersBonuses[$uid]["SUMM_MINUS"];
		if($arPartnersBonuses[$uid]["REF_CNT"]>0){
			$arSummary["REF_PAYED"] += $arPartnersBonuses[$uid]["REF_PAYED"];
			$arSummary["REF_CNT"] += 1;
			$arSummary["REF_CLIENTS"] += 1;
			$arSummary["REF_CNT"] += $arPartnersBonuses[$uid]["REF_CNT"];
			$arSummary["REF_BALL_SUMM"] += $arPartnersBonuses[$uid]["SUMM"];
			$arSummary["REF_BALL_SUMM_MINUS"] += $arPartnersBonuses[$uid]["SUMM_MINUS"];
		}
		if($arPartnersBonuses[$uid]["CLIENT"]=='Y'){
			$arSummary["CLIENTS"] += 1;
		}

	}

  //Получим индивидуальные промокоды с количеством кликов по ссылкам
	$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_UTM_STAT_CLICK", "PROPERTY_USER");
	$arFilter = Array("IBLOCK_ID"=>IntVal(36),
	  array("LOGIC"=>"OR",
		 array("=PROPERTY_USER"=>array_keys($arPartnersBonuses)),
		 array(">PROPERTY_UTM_STAT_CLICK"=>0),
	  ),
	   );
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$arUtmPromoClick = array();

	while($ob = $res->fetch()){
	 $uid = intval($ob["PROPERTY_USER_VALUE"]);
	 $arUtmPromoClick[$uid] = array("PROMO_CODE"=>$ob["NAME"], "UTM_STAT_CLICK"=>intval($ob["PROPERTY_UTM_STAT_CLICK_VALUE"]));
	 $arPartnersBonuses[$uid]["UTM_STAT_CLICK"] = intval($ob["PROPERTY_UTM_STAT_CLICK_VALUE"]);
	 $arSummary["UTM_STAT_CLICK"] += intval($ob["PROPERTY_UTM_STAT_CLICK_VALUE"]);

     if($arPartnersBonuses[$uid]["REF_CNT"]>0){
		 $arSummary["REF_UTM_STAT_CLICK"] += intval($ob["PROPERTY_UTM_STAT_CLICK_VALUE"]);
	  }
	}

	$arSummary["REF_UTM_STAT_CLICK_USER_CNT"] = 0;
	foreach($arPartnersBonuses as $uid=>$val){
		if($val["UTM_STAT_CLICK"]>0){
			$arSummary["REF_UTM_STAT_CLICK_USER_CNT"]+=1;
		}
	}

	//$arSummary["REF_UTM_STAT_CLICK_USER_CNT"] = count($arSummary["REF_UTM_STAT_CLICK_USER_CNT"]);

  unset($bonusGift);
  //Получаем пользователей для показа в таблицах
  $data = CUser::GetList(($by="ID"), ($order="ASC"),
            array(
                'ID' => array_keys($arPartnersBonuses),
            )
        );
  $arUserList = array();


while($arUser = $data->Fetch()) {
    $arUserList[$arUser['ID']] = $arUser;
}




/* Сортировка */
 if(empty($_REQUEST["sort"])){
 	$_REQUEST["sort"] = 'ref_cnt';
 }
 if(empty($_REQUEST["dir"])){
 	$_REQUEST["dir"] = 'desc';
 }

  if($_REQUEST["dir"]=='asc'){
      uasort($arPartnersBonuses, function ($item1, $item2) {
      	switch ($_REQUEST["sort"]) {
      	    case 'bonus':
      	        return $item1["SUMM"] > $item2["SUMM"];
      	        break;
      	    case 'bonus_minus':
      	        return ($item1["SUMM"]-$item1["SUMM_MINUS"]) > ($item2["SUMM"]-$item2["SUMM_MINUS"]);
      	        break;
      	    case 'ref_cnt':
      	        return $item1["REF_CNT"] > $item2["REF_CNT"];
      	        break;
      	    case 'ref_payed':
      	        return $item1["REF_PAYED"] > $item2["REF_PAYED"];
      	        break;
      	    case 'client':
      	        return $item1["CLIENT"] == 'Y';
      	        break;
      	    case 'click_utm':
      	        return $item1["UTM_STAT_CLICK"] > $item2["UTM_STAT_CLICK"];
      	        break;
      	}

		});
  }   elseif($_REQUEST["dir"]=='desc'){
      uasort($arPartnersBonuses, function ($item1, $item2) {
      	switch ($_REQUEST["sort"]) {
      	    case 'bonus':
      	        return $item1["SUMM"] < $item2["SUMM"];
      	        break;
      	    case 'bonus_minus':
      	        return ($item1["SUMM"]-$item1["SUMM_MINUS"]) < ($item2["SUMM"]-$item2["SUMM_MINUS"]);
      	        break;
      	    case 'ref_cnt':
      	        return $item1["REF_CNT"] < $item2["REF_CNT"];
      	        break;
      	    case 'ref_payed':
      	        return $item1["REF_PAYED"] < $item2["REF_PAYED"];
      	        break;
      	    case 'client':
      	        return $item1["CLIENT"] !=='Y';
      	        break;
      	    case 'click_utm':
      	        return $item1["UTM_STAT_CLICK"] < $item2["UTM_STAT_CLICK"];
      	        break;
      	}

		});
  }



?>
 <div class="container">
		<h1>Рейтинг партнеров</h1>
		<div class="main_content">
			<div class="main_content_inner">
<?if(count($arPartnersBonuses)>0):?>

					<form class="inline-form sorting-form" enctype="multipart/form-data" method="post">
					  <div class="form-group">
					  	 <label for="user">Сортировка</label>&nbsp;

						 <select name="sort">
							<option value="bonus" <?=$_REQUEST["sort"]=="bonus"?'selected=""':''?>>По начисленным баллам</option>
							<option value="bonus_minus" <?=$_REQUEST["sort"]=="bonus_minus"?'selected=""':''?> >По оставшимся баллам</option>
							<option value="ref_cnt" <?=$_REQUEST["sort"]=="ref_cnt"?'selected=""':''?> >По рефералам (всего)</option>
							<option value="ref_payed" <?=$_REQUEST["sort"]=="ref_payed"?'selected=""':''?> >По прибыльным рефералам</option>
							<option value="click_utm" <?=$_REQUEST["sort"]=="click_utm"?'selected=""':''?> >По кол-ву кликов</option>
							<option value="client" <?=$_REQUEST["sort"]=="client"?'selected=""':''?> >Клиент/не клиент</option>
						 </select>

					  	 <label for="user">Направление сортировки</label>&nbsp;

						 <select name="dir">
							<option value="asc" <?=$_REQUEST["dir"]=="asc"?'selected=""':''?> >По возрастанию</option>
							<option value="desc" <?=$_REQUEST["dir"]=="desc"?'selected=""':''?> >По убыванию</option>
						 </select>
						 <!--<button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>-->
					  </div>

					</form>
				<h4 class="text-info">Итого поделившихся ссылками: <?= $arSummary["REF_UTM_STAT_CLICK_USER_CNT"]?>

				</h4>
				<table class="table table-condensed table-bordered table-striped">
				  <thead class="bg-info">
					<tr>
							<th rowspan="3">№ п/п</th>
							<th>Партнер</th>
							<th>Клиент</th>
							<th>Начислено<br> всего баллов</th>
							<th>Остаток баллов</th>
							<th>Клики<br>по ссылкам</th>
							<th>Рефералов<br> (из них прибыльных)</th>
<!--							<th>Код</th>
							<th>Тип</th>
							<th>Дает (тип)</th>
							<th>Дает (значение)</th>
							<th>Использован</th>-->
						</tr>
					   <tr class="bg-success">
							<th>Итого</th>
							<th><?//= $arSummary["CLIENTS"] ?></th>
							<th><?= $arSummary["BALL_SUMM"] ?></th>
							<th><?= $arSummary["BALL_SUMM"]-$arSummary["BALL_SUMM_MINUS"] ?></th>
							<th><?= $arSummary["UTM_STAT_CLICK"] ?></th>
							<th>Реф.: <?= $arSummary["REF_CNT"] ?></th>
						</tr>
					   <tr class="bg-success">
							<th>Итого (партнеры)</th>
							<th><?//= $arSummary["REF_CLIENTS"] ?></th>
							<th><?= $arSummary["REF_BALL_SUMM"] ?></th>
							<th><?= $arSummary["REF_BALL_SUMM"]-$arSummary["REF_BALL_SUMM_MINUS"] ?></th>
							<th><?= $arSummary["REF_UTM_STAT_CLICK"] ?></th>
							<th>Парт.: <?= $arSummary["REF_CLIENTS"] ?> ($ <?=$arSummary["REF_PAYED"]?>)</th>
						</tr>
					 </thead>
				  <? $cnt = 1; ?>
				  <?foreach($arPartnersBonuses as $pid=>$arVal):?>
				    <?if($arVal["REF_CNT"] == 0 && $arUtmPromoClick[$pid]["UTM_STAT_CLICK"]==0) continue;?>
 						<tr class="<?if(array_key_exists($pid, $arAdminPartners)):?>bg-success<?endif;?>">
						   <td><?= $cnt; $cnt++;?></td>
							<td title="<?= $arUtmPromoClick[$pid]["PROMO_CODE"]?>"><?if(array_key_exists($pid, $arAdminPartners)):?>[adm] <?endif;?><strong><?= $arUserList[$pid]["LOGIN"] ?></strong>
							 <a class="btn btn-info btn-xs" style="display: block; float:right; text-decoration: none;" href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $pid ?>" target="_blank">Профиль</a></td>
							<td><?= $arVal["CLIENT"]=='Y'?'Да':'' ?></td>
							<td><?= $arVal["SUMM"] ?></td>
							<td><?= $arVal["SUMM"]-$arVal["SUMM_MINUS"] ?></td>
							<td><?= $arUtmPromoClick[$pid]["UTM_STAT_CLICK"]?>
							 <span class="label label-warning " style="display: block; float:right; text-decoration: none;"><small><?= $arUtmPromoClick[$pid]["PROMO_CODE"]?></small></span>
							</td>
							<td><?= ($arVal["REF_CNT"]) ?>
							  <?if(intval($arVal["REF_PAYED"])>0 && intval($arVal["REF_CNT"])>0):?>
								 <span class="text-success small" alt="Прибыльные">($ <?= $arVal["REF_PAYED"] ?>)</span>
							  <?endif;?>
							  &nbsp;<a class="btn btn-info btn-xs" style="display: block; float:right; text-decoration: none;" href="/tools/system/other/partner/partnersList.php?user=<?= $pid ?>&send=Y" target="_blank">Подробнее</a>
							</td>
						</tr>
				  <?endforeach;?>
				</table>
				<?endif;?>

				<hr>


			</div>
		</div>
 </div>
 <script>
 $(document).ready(function(){
	 $('.sorting-form select').on('change', function(){
		var sort = $('select[name=sort]').val();
		var dir = $('select[name=dir]').val();

		window.top.location.href='<?=CUtil::JSEscape($APPLICATION->GetCurPage(true))?>?sort='+sort+'&dir='+dir;

    });

 });
 </script>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>