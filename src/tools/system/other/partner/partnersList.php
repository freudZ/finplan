<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

$notInGroups = true;
foreach($arGroups as $itemGroup){
	if(in_array($itemGroup, array(1,5,15))){
		$notInGroups = false;
		break;
	}
}

if(!$USER->IsAdmin() && $notInGroups){
  	LocalRedirect("/");
  	exit();
}


if($_REQUEST["send"] && $_REQUEST["user"]){

if(is_numeric(trim($_REQUEST["user"]))){
 $rsUser = CUser::GetById(trim($_REQUEST["user"]));
} else {
 $rsUser = CUser::GetByLogin($_REQUEST["user"]);
}

$arUser = $rsUser->Fetch();

if(intval($arUser["ID"])>0){
$_REQUEST["user"] = $arUser["LOGIN"];
$totalBonuses = Bonuses::getTotalBonuses($arUser["ID"]);
$items = Bonuses::getOperations($arUser["ID"]);
$userReferals = promocodes::getUserReferalsToLK($arUser["ID"], true);

$arReferalsLogin = array();
foreach($items as $item){
  if(!empty($item["UF_FROM_USER"])){
	 $arReferalsLogin[$item["UF_FROM_USER"]] = array("LOGIN"=>"","STATUS"=>"");
  }
}

$data = CUser::GetList(($by="ID"), ($order="ASC"),
            array(
                'ID' => implode(" | ", array_keys($arReferalsLogin)),
            )
        );

while($arUserRef = $data->Fetch()) {
	 $arReferalsLogin[$arUserRef['ID']]["LOGIN"] = $arUserRef['LOGIN'];
}

//Заполняем статусы промокодов рефералов
foreach($userReferals as $code=>$arReferals){
  foreach($arReferals as $refItem){
	  $arReferalsLogin[$refItem['USER_ID']]["STATUS"] = $refItem['PROMOCODE_STATUS'];
  }
}


//Получим список промокодов пользователя
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_TYPE", "PROPERTY_SET_TYPE", "PROPERTY_SET", "PROPERTY_DATE_START", "PROPERTY_DATE_END", "PROPERTY_USED");
		$arFilter = Array("IBLOCK_ID"=>IntVal(36), "PROPERTY_USER"=>$arUser["ID"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arPromocodes = array();
		while( $row = $res->fetch()){
		 $arPromocodes[$row["ID"]] = $row;
		}

}

}

?>

	<div class="container">
		<h1>Журнал партнера <?if($_REQUEST["user"]):?><?= $_REQUEST["user"] ?> [<?= $arUser["ID"] ?>]<?endif;?> </h1>
		<div class="main_content">
			<div class="main_content_inner">
				<div class="btn-group" role="group" aria-label="">
					<form class="inline-form" enctype="multipart/form-data" method="post">
					  <div class="form-group">
					  	 <label for="user">Введите логин или id партнера</label>&nbsp;

					<!--	 <select name="operate_year">
						 	<?$y=date('Y');
							while($y>date('Y')-10):?>
						 	<option value="<?= $y ?>"><?= $y ?></option>
							<?$y--;?>
							<?endwhile;?>
						 </select>-->

					  	 <input name="user" value="<?= $_REQUEST["user"] ?>">
						 <button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>
					  </div>

					</form>

				<hr>
		  	   <? $commonReferals = 0;
			     $payedReferals = 0;
				  $strReferals = '';

			  foreach($items as $item){
			  	if($item["UF_TYPE"]==9)
				  $payedReferals++;
			  }

			  if(count($userReferals)>0 ){
				  if(array_key_exists(88,$userReferals)){
				  	$commonReferals += intval(count($userReferals[88]));
					$strReferals = 'привлеченных реф.: '.$commonReferals;
					}
				  if(array_key_exists("",$userReferals)){
				  	$commonReferals += intval(count($userReferals[89]));
				  	$commonReferals += $payedReferals;
				  	//$payedReferals += intval(count($userReferals[89]));
					$strReferals = 'привлеченных реф.: '.$commonReferals.', из них прибыльных: '.$payedReferals;
					}

			  } else {
			  	$strReferals = 'Пользователь не привлек ни одного реферала';
			  }



			   ?>
				<h2>Промокоды партнера: <?= count($arPromocodes)>0?count($arPromocodes):'не созданы' ?></h2>
				<?if(count($arPromocodes)>0):?>
				<table class="table table-condensed table-bordered table-striped">
				  <thead class="bg-info">
					<tr>
							<th>ID</th>
							<th>Срок действия</th>
							<th>Код</th>
							<th>Тип</th>
							<th>Дает (тип)</th>
							<th>Дает (значение)</th>
							<th>Использован</th>
						</tr>
					 </thead>
				  <?foreach($arPromocodes as $pid=>$arPropmo):?>
 						<tr>
							<td ><?= $pid ?></td>
							<td><?= $arPropmo["PROPERTY_DATE_START_VALUE"] ?> - <?= $arPropmo["PROPERTY_DATE_END_VALUE"] ?></td>
							<th scope="row" class="text-info"><?= $arPropmo["NAME"] ?></th>
							<td><?= $arPropmo["PROPERTY_TYPE_VALUE"] ?></td>
							<td><?= $arPropmo["PROPERTY_SET_TYPE_VALUE"] ?></td>
							<td><?= $arPropmo["PROPERTY_SET_VALUE"] ?></td>
							<td><?= $arPropmo["PROPERTY_USED_VALUE"] ?></td>
						</tr>
				  <?endforeach;?>
				</table>
				<?endif;?>

				<hr>

				<h2>Реферальная программа: <?= $strReferals ?></h2>
				 <?if(count($userReferals)>0):?>
					<table class="table table-condensed table-bordered">
					<thead class="bg-info">
						<tr>
							<th>Дата добавления</th>
							<th>Реферал</th>
							<th>Код</th>
						</tr>
					</thead>
					<?// Значения для PROMOCODE_STATUS: 87 - disabled (Не рабочий), 88 - enabled (Рабочий), 89 - used (Использован) ?>
					<? $arStatus = array(87=>"Не рабочий", 88=>"Рабочий", 89=>"Не заполнен", ""=>"Не заполнен");
						$arStatusClass = array(87=>"bg-danger", 88=>"bg-success", 89=>"bg-warning", ""=>"bg-warning" );
					?>
					  <?foreach($userReferals as $code=>$arReferals):?>

						 <tr class="<?= $arStatusClass[$code] ?>">
						  <td colspan="3">Статус кода <strong><?= $arStatus[$code]; ?></strong>:</td>
						 </tr>
						  <?foreach($arReferals as $refItem):?>
						  <tr>
							<td><?= $refItem["DATE_CREATE"] ?></td>
							<td><?= $refItem["USER_EMAIL"] ?></td>
							<td><?= $refItem["PROMOCODE"] ?></td>
						  </tr>
					     <?endforeach;?>
					  <?endforeach;?>
					</table>
				 <?endif;?>
				<hr>

				<?if(count($userReferals)>0 || count($totalBonuses)>0):?>
   				<h2>Всего бонусов: <?= $totalBonuses ?></h2>
				  <table class="table table-condensed table-bordered table-striped">
					<thead class="bg-info">
						<tr>
							<th>Дата</th>
							<th>Событие</th>
							<th>Реферал</th>
							<th>Статус промокода</th>
							<th>Начислено бонусов</th>
						</tr>
					</thead>
					<?if($items):?>

						<tbody>
							<?foreach($items as $item):
								$class = "minus";
								$prefix = "-";
								$text = "Списание бонусов (оплата «".$item["UF_DESC"]."»)";

								if($item["UF_TYPE"]==9){
									$class = "plus";
									$prefix = "+";
									$text = "Начисление бонусов (".$item["UF_DESC"].")";
								}
								?>
								<tr class="<?=$class?>">
									<td class="date_col"><?=$item["UF_DATE"]->format("d.m.Y")?></td>
									<td class="event_col">
									<?=$text?>
									</td>
									<td><?=$arReferalsLogin[$item['UF_FROM_USER']]["LOGIN"]?></td>
									<td class="bonus_col"><?=$arReferalsLogin[$item['UF_FROM_USER']]["STATUS"]?></td>
									<td class="bonus_col"><?=$prefix?><?=$item["UF_VAL"]?></td>
								</tr>
							<?endforeach?>
						</tbody>
					<?else:?>
					<tr>
						<td  colspan="5">
						  В данный момент у пользователя нет операций по бонусам.
						</td>
					</tr>
					<?endif;?>
				  </table>
		   	<?endif;?>
				</div>
				<br><br>
			</div>
		</div>
  </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>