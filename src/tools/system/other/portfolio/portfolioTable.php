<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();
$portfolioIblockId = 52;
if(!$USER->IsAdmin() && !in_array(array(1,5), $arGroups)){
	LocalRedirect("/");
	exit();
}
CModule::IncludeModule("iblock");
$arSummary = array();
$CPortfolio = new CPortfolio();
//$arSummary = $CPortfolio->getSummarySatsForAllPortfolios();
/* TODO удалить, если некорректно формируется таблица
$arAllPortfolioList = $CPortfolio->getPortfolioListAll();

foreach($arAllPortfolioList as $pid=>$val){
 $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["PORTFOLIO_ITEMS"][$pid] = $val;
 $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["CURRENT_SUMM"][$pid] = $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"];
 $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["INVEST_SUMM"][$pid] = $val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
 if(!array_key_exists('ZERO', $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]])){
 	$arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["ZERO"] = 0;
 }
 if(floatval($val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"])<=0)
  $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["ZERO"] += 1;

 $arSummary["CNT_PORTFOLIO"] += 1;
    $portfolioDifferenceSumm = $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"]-$val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
	 if($portfolioDifferenceSumm<0){
	  $arSummary["CNT_MINUS_PORTFOLIO"] += 1;
	  $arSummary["MID_INVEST_SUMM"] += $val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
	  $arSummary["MID_CURRENT_SUMM"] += $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"];
	  $arSummary["MID_DELTA_SUMM"] += $portfolioDifferenceSumm;
	 }
	 if($portfolioDifferenceSumm>0){
	  $arSummary["CNT_PLUS_PORTFOLIO"] += 1;
	  $arSummary["MID_INVEST_SUMM"] += $val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
	  $arSummary["MID_CURRENT_SUMM"] += $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"];
	  $arSummary["MID_DELTA_SUMM"] += $portfolioDifferenceSumm;
	 }
	 if($portfolioDifferenceSumm==0){
	  $arSummary["CNT_ZERO_PORTFOLIO"] += 1;
	 }
}*/
// $arPortfolio = $arSummary["arPortfolio"];

$arSummary = array();

$CPortfolio = new CPortfolio();
$arAllPortfolioList = $CPortfolio->getPortfolioListAll();

foreach($arAllPortfolioList as $pid=>$val){
 $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["PORTFOLIO_ITEMS"][$pid] = $val;
 $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["CURRENT_SUMM"][$pid] = $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"];
 $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["INVEST_SUMM"][$pid] = $val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
 if(!array_key_exists('ZERO', $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]])){
 	$arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["ZERO"] = 0;
 }
 if(floatval($val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"])<=0)
  $arPortfolio[$val["PROPERTIES"]["OWNER_USER"]["VALUE"]]["ZERO"] += 1;

 $arSummary["CNT_PORTFOLIO"] += 1;
    $portfolioDifferenceSumm = $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"]-$val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
	 if($portfolioDifferenceSumm<0){
	  $arSummary["CNT_MINUS_PORTFOLIO"] += 1;
	  $arSummary["MID_INVEST_SUMM"] += $val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
	  $arSummary["MID_CURRENT_SUMM"] += $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"];
	  $arSummary["MID_DELTA_SUMM"] += $portfolioDifferenceSumm;
	 }
	 if($portfolioDifferenceSumm>0){
	  $arSummary["CNT_PLUS_PORTFOLIO"] += 1;
	  $arSummary["MID_INVEST_SUMM"] += $val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];
	  $arSummary["MID_CURRENT_SUMM"] += $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"];
	  $arSummary["MID_DELTA_SUMM"] += $portfolioDifferenceSumm;
	 }
	 if($portfolioDifferenceSumm==0){
	  $arSummary["CNT_ZERO_PORTFOLIO"] += 1;
	 }
}

 //Приводим средние значения
 $countActivePortfolio = $arSummary["CNT_MINUS_PORTFOLIO"]+$arSummary["CNT_PLUS_PORTFOLIO"];
$arSummary["MID_INVEST_SUMM"] = round($arSummary["MID_INVEST_SUMM"]/$countActivePortfolio, 2);
$arSummary["MID_CURRENT_SUMM"] = round($arSummary["MID_CURRENT_SUMM"]/$countActivePortfolio, 2);
$arSummary["MID_DELTA_SUMM"] = round($arSummary["MID_DELTA_SUMM"]/$countActivePortfolio, 2);


unset($arAllPortfolioList, $CPortfolio);

foreach($arPortfolio as $uid=>$val){
  $arPortfolio[$uid]["CURR_MIN"] = min($arPortfolio[$uid]["CURRENT_SUMM"]);
  $arPortfolio[$uid]["CURR_MAX"] = max($arPortfolio[$uid]["CURRENT_SUMM"]);
  $arPortfolio[$uid]["CURR_AVG"] = array_sum($arPortfolio[$uid]["CURRENT_SUMM"]) / count($arPortfolio[$uid]["CURRENT_SUMM"]);
  $arPortfolio[$uid]["INV_MIN"] = min($arPortfolio[$uid]["INVEST_SUMM"]);
  $arPortfolio[$uid]["INV_MAX"] = max($arPortfolio[$uid]["INVEST_SUMM"]);
  $arPortfolio[$uid]["INV_AVG"] = array_sum($arPortfolio[$uid]["INVEST_SUMM"]) / count($arPortfolio[$uid]["INVEST_SUMM"]);
  $arPortfolio[$uid]["INV_SUMM"] = array_sum($arPortfolio[$uid]["INVEST_SUMM"]);
  $arPortfolio[$uid]["DIFFERENCE"] = array_sum($arPortfolio[$uid]["CURRENT_SUMM"]) - array_sum($arPortfolio[$uid]["INVEST_SUMM"]);//Разница от сумм вложений и сумм тек. стоимости
  if($arPortfolio[$uid]["DIFFERENCE"]!=0){
    $arPortfolio[$uid]["DIFFERENCE_PRC"] = $arPortfolio[$uid]["DIFFERENCE"]/array_sum($arPortfolio[$uid]["INVEST_SUMM"])*100;
  } else {
    $arPortfolio[$uid]["DIFFERENCE_PRC"] = 0;
  }

}

  //Получаем пользователей для показа в таблицах
  $data = CUser::GetList(($by="ID"), ($order="ASC"),
            array(
                'ID' => array_keys($arPortfolio),
            )
        );
  $arUserList = array();


while($arUser = $data->Fetch()) {
    $arUserList[$arUser['ID']] = $arUser;
}



/* Сортировка */
 if(empty($_REQUEST["sort"])){
 	$_REQUEST["sort"] = 'cnt';
 }
 if(empty($_REQUEST["dir"])){
 	$_REQUEST["dir"] = 'desc';
 }

  if($_REQUEST["dir"]=='asc'){
      uasort($arPortfolio, function ($item1, $item2) {
      	switch ($_REQUEST["sort"]) {
      	    case 'cnt':
      	        return count($item1["PORTFOLIO_ITEMS"]) > count($item2["PORTFOLIO_ITEMS"]);
      	        break;
      	    case 'diff':
      	        return $item1["DIFFERENCE"] > $item2["DIFFERENCE"];
      	        break;
      	    case 'diff_prc':
      	        return $item1["DIFFERENCE_PRC"] > $item2["DIFFERENCE_PRC"];
      	        break;
      	    case 'min_sum':
      	        return $item1["CURR_MIN"] > $item2["CURR_MIN"];
      	        break;
      	    case 'max_sum':
      	        return $item1["CURR_MAX"] > $item2["CURR_MAX"];
      	        break;
      	    case 'avg_sum':
      	        return $item1["CURR_AVG"] > $item2["CURR_AVG"];
      	        break;
      	    case 'min_invsum':
      	        return $item1["INV_MIN"] > $item2["INV_MIN"];
      	        break;
      	    case 'max_invsum':
      	        return $item1["INV_MAX"] > $item2["INV_MAX"];
      	        break;
      	    case 'avg_invsum':
      	        return $item1["INV_AVG"] > $item2["INV_AVG"];
      	        break;
      	    case 'zero':
      	        return $item1["ZERO"] > $item2["ZERO"];
      	        break;

      	}

		});
  }   elseif($_REQUEST["dir"]=='desc'){
      uasort($arPortfolio, function ($item1, $item2) {
      	switch ($_REQUEST["sort"]) {
      	    case 'cnt':
      	        return count($item1["PORTFOLIO_ITEMS"]) < count($item2["PORTFOLIO_ITEMS"]);
      	        break;
      	    case 'diff':
      	        return $item1["DIFFERENCE"] < $item2["DIFFERENCE"];
      	        break;
      	    case 'diff_prc':
      	        return $item1["DIFFERENCE_PRC"] < $item2["DIFFERENCE_PRC"];
      	        break;
      	    case 'min_sum':
      	        return $item1["CURR_MIN"] < $item2["CURR_MIN"];
      	        break;
      	    case 'max_sum':
      	        return $item1["CURR_MAX"] < $item2["CURR_MAX"];
      	        break;
      	    case 'avg_sum':
      	        return $item1["CURR_AVG"] < $item2["CURR_AVG"];
      	        break;
      	    case 'min_invsum':
      	        return $item1["INV_MIN"] < $item2["INV_MIN"];
      	        break;
      	    case 'max_invsum':
      	        return $item1["INV_MAX"] < $item2["INV_MAX"];
      	        break;
      	    case 'avg_invsum':
      	        return $item1["INV_AVG"] < $item2["INV_AVG"];
      	        break;
      	    case 'zero':
      	        return $item1["ZERO"] < $item2["ZERO"];
      	        break;
      	}

		});
  }

?>
 <div class="container">
		<h1>Статистика по портфелям</h1>
		<div class="main_content">
			<div class="main_content_inner">
<?if(count($arPortfolio)>0):?>

					<form class="inline-form sorting-form" enctype="multipart/form-data" method="post">
					  <div class="form-group">
					  	 <label for="user">Сортировка</label>&nbsp;

						 <select name="sort">
							<option value="cnt" <?=$_REQUEST["sort"]=="cnt"?'selected=""':''?>>По количеству</option>
							<option value="diff" <?=$_REQUEST["sort"]=="diff"?'selected=""':''?>>По успешности</option>
							<option value="diff_prc" <?=$_REQUEST["sort"]=="diff"?'selected=""':''?>>По успешности %</option>
							<option value="min_sum" <?=$_REQUEST["sort"]=="min_sum"?'selected=""':''?> >По мин. текущей сумме</option>
							<option value="max_sum" <?=$_REQUEST["sort"]=="max_sum"?'selected=""':''?> >По макс. текущей сумме</option>
							<option value="avg_sum" <?=$_REQUEST["sort"]=="avg_sum"?'selected=""':''?> >По сред. текущей сумме</option>
							<option value="min_invsum" <?=$_REQUEST["sort"]=="min_invsum"?'selected=""':''?> >По мин. инвест. сумме</option>
							<option value="max_invsum" <?=$_REQUEST["sort"]=="max_invsum"?'selected=""':''?> >По макс. инвест. сумме</option>
							<option value="avg_invsum" <?=$_REQUEST["sort"]=="avg_invsum"?'selected=""':''?> >По сред. инвест. сумме</option>
							<option value="zero" <?=$_REQUEST["sort"]=="zero"?'selected=""':''?> >По кол-ву пустых</option>
						 </select>

					  	 <label for="user">Направление сортировки</label>&nbsp;

						 <select name="dir">
							<option value="asc" <?=$_REQUEST["dir"]=="asc"?'selected=""':''?> >По возрастанию</option>
							<option value="desc" <?=$_REQUEST["dir"]=="desc"?'selected=""':''?> >По убыванию</option>
						 </select>
						 <!--<button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>-->
					  </div>

					</form>
				<table class="table table-condensed table-bordered table-striped">
				<thead class="bg-info">
				  <tr>
				   <th>Всего портфелей</th>
				   <th>из них в +</th>
				   <th>из них в -</th>
				   <th>из них <br>пустых</th>
				   <th>∑ инв. сред.</th>
				   <th>∑ тек. сред.</th>
				   <th>Δ сред. <br>(% от ∑ инв.)</th>
				  </tr>
				</thead>
				  <tr>
				   <td><?= $arSummary["CNT_PORTFOLIO"] ?></td>
				   <td class="bg-success"><?= $arSummary["CNT_PLUS_PORTFOLIO"] ?></td>
				   <td class="bg-danger"><?= $arSummary["CNT_MINUS_PORTFOLIO"] ?></td>
				   <td><?= $arSummary["CNT_ZERO_PORTFOLIO"] ?></td>
				   <td><?= $arSummary["MID_INVEST_SUMM"] ?></td>
				   <td><?= $arSummary["MID_CURRENT_SUMM"] ?></td>
				   <td><?= $arSummary["MID_DELTA_SUMM"] ?> (<?= round($arSummary["MID_DELTA_SUMM"]/$arSummary["MID_INVEST_SUMM"]*100, 2) ?>%)</td>

				  </tr>



				</table>

				<table class="table table-condensed table-bordered table-striped">
				  <thead class="bg-info">
					<tr>
							<th>Пользователь</th>
							<th>Кол-во портфелей</th>
							<th>Пустых</th>
							<th>Инв. мин</th>
							<th>Инв. макс.</th>
							<th>Инв. сред.</th>
							<th rowspan="2" title="Суммарный прирост стоимости портфелей">Успешность</th>
						</tr>
					<tr>
							<th colspan="3"></th>
							<th>Тек. мин</th>
							<th>Тек. макс.</th>
							<th>Тек. сред.</th>
						</tr>

					 </thead>
				  <?foreach($arPortfolio as $uid=>$arVal):?>
 						<tr>
							<td rowspan="2"  class="v_middle"><strong><?= empty($arUserList[$uid]["LOGIN"])?'Потеряные портфели':$arUserList[$uid]["LOGIN"] ?></strong> <?if(intval($uid)>0):?>[<?= $uid ?>] <a class="btn btn-info btn-xs btn-right"  href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $uid ?>" target="_blank">Профиль</a><?endif;?></td>
							<td rowspan="2"  class="v_middle"><?= count($arVal["PORTFOLIO_ITEMS"]);?>&nbsp;
							<a class="btn btn-xs btn-info btn-right toggle_items" data-target="p_items_<?= $arUserList[$uid]["ID"] ?>" href="javascript:void(0);">Портфели</a>
							<div id="p_items_<?= $arUserList[$uid]["ID"] ?>" class="portfolio_items_block">
								<?foreach($arVal["PORTFOLIO_ITEMS"] as $p=>$pVal):?>
								 <a class="btn-xs" href="/portfolio/portfolio_<?= $p ?>/" target="_blank"><?=$p?> (<?= round($pVal["PROPERTIES"]["CURRENT_SUMM"]["VALUE"],2)?>р.)</a><br>
								<?endforeach;?>
							</div>
							</td>
							<td rowspan="2"  class="v_middle"><?= (array_key_exists('ZERO', $arVal) && intval($arVal["ZERO"])>0?$arVal["ZERO"]:'') ?></td>
							<td><?= round($arVal["INV_MIN"], 2) ?></td>
							<td><?= round($arVal["INV_AVG"], 2) ?></td>
							<td><?= round($arVal["INV_MAX"], 2) ?></td>
							<? $diffClass='';
							 if($arVal["DIFFERENCE"]<0){
								$diffClass = 'bg-danger';
							 }
							 if($arVal["DIFFERENCE"]==0){
								$diffClass = 'bg-warning';
							 }

							 ?>
							<td class="<?=$diffClass;?>" ><?= round($arVal["DIFFERENCE"], 2) ?></td>
						</tr>
 						<tr class="bottom-border-bold">
							<td><?= round($arVal["CURR_MIN"], 2) ?></td>
							<td><?= round($arVal["CURR_AVG"], 2) ?></td>
							<td><?= round($arVal["CURR_MAX"], 2) ?></td>
							<td class="<?=$diffClass;?>" data-investSumm="<?= round($arVal["INV_SUMM"], 2) ?>"><?= round($arVal["DIFFERENCE_PRC"], 2)."%" ?></td>
						</tr>
				  <?endforeach;?>
				</table>
				<?endif;?>

				<hr>


</div>
</div>
</div>

 <script>
 $(document).ready(function(){
	 $('.sorting-form select').on('change', function(){
		var sort = $('select[name=sort]').val();
		var dir = $('select[name=dir]').val();

		window.top.location.href='<?=CUtil::JSEscape($APPLICATION->GetCurPage(true))?>?sort='+sort+'&dir='+dir;
    });
  $('.toggle_items').on('click', function(){
    var target = $(this).data('target');
	 $('.portfolio_items_block').removeClass('active');
	 $('#'+target).addClass('active');


  });


 });
 </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>