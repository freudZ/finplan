<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(array(1,5), $arGroups)){
	LocalRedirect("/");
	exit();
}
CModule::IncludeModule("iblock");
$arResult = array();
 if(empty($_REQUEST["type"])){
 	$_REQUEST["type"] = 'shares';
 }
 $type = htmlspecialchars($_REQUEST["type"]);
switch ($type) {
    case 'shares':
    case 'etf':
	       if($type=='etf'){
				 $resA = new ETF();
			 } else {
				 $resA = new Actions();
			 }


			$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID","PROPERTY_SECID", "PROPERTY_IS_ETF_ACTIVE", "PROPERTY_CNT_IN_PORTFOLIO", "PROPERTY_CNT_IN_PORTFOLIO_USERS", "PROPERTY_PRC_IN_PORTFOLIO", "PROPERTY_PRC_OF_USERS");
			$arFilter = Array("IBLOCK_ID"=>IntVal(32), "!PROPERTY_CNT_IN_PORTFOLIO"=>false, "!CODE"=>false, "PROPERTY_HIDEN"=>false);
			if($type=='etf'){
			  $arFilter["!PROPERTY_IS_ETF_ACTIVE"]=false;
			} else {
			  $arFilter["PROPERTY_IS_ETF_ACTIVE"]=false;
			}
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->fetch()){

			 $arAction = $resA->getItem($ob["CODE"]);
			 $arResult[$ob["ID"]] = array(
													 "NAME" => $arAction["NAME"],
													 "SECID" => $arAction["NAME"],
													 "URL" => $arAction["URL"],
													 "CNT_IN_PORTFOLIO" => $ob["PROPERTY_CNT_IN_PORTFOLIO_VALUE"],
													 "CNT_IN_PORTFOLIO_USERS" => $ob["PROPERTY_CNT_IN_PORTFOLIO_USERS_VALUE"],
													 "PRC_IN_PORTFOLIO" => $ob["PROPERTY_PRC_IN_PORTFOLIO_VALUE"],
													 "PRC_OF_USERS" => $ob["PROPERTY_PRC_OF_USERS_VALUE"],
			 );

			}

			unset($resA);
        break;
    case "shares_usa":
	      $resA = new ActionsUsa();
			$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID","PROPERTY_SECID", "PROPERTY_IS_ETF_ACTIVE", "PROPERTY_CNT_IN_PORTFOLIO", "PROPERTY_CNT_IN_PORTFOLIO_USERS", "PROPERTY_PRC_IN_PORTFOLIO", "PROPERTY_PRC_OF_USERS");
			$arFilter = Array("IBLOCK_ID"=>IntVal(55), "!PROPERTY_CNT_IN_PORTFOLIO"=>false, "!CODE"=>false);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->fetch()){

			 $arAction = $resA->getItem($ob["CODE"]);
			 $arResult[$ob["ID"]] = array(
													 "NAME" => $arAction["NAME"],
													 "SECID" => $arAction["NAME"],
													 "URL" => $arAction["URL"],
													 "CNT_IN_PORTFOLIO" => $ob["PROPERTY_CNT_IN_PORTFOLIO_VALUE"],
													 "CNT_IN_PORTFOLIO_USERS" => $ob["PROPERTY_CNT_IN_PORTFOLIO_USERS_VALUE"],
													 "PRC_IN_PORTFOLIO" => $ob["PROPERTY_PRC_IN_PORTFOLIO_VALUE"],
													 "PRC_OF_USERS" => $ob["PROPERTY_PRC_OF_USERS_VALUE"],
			 );

			}

			unset($resA);
        break;
    case "bonds":
			$resA = new Obligations();
			$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID","PROPERTY_SECID", "PROPERTY_IS_ETF_ACTIVE", "PROPERTY_CNT_IN_PORTFOLIO", "PROPERTY_CNT_IN_PORTFOLIO_USERS", "PROPERTY_PRC_IN_PORTFOLIO", "PROPERTY_PRC_OF_USERS");
			$arFilter = Array("IBLOCK_ID"=>IntVal(27), "!PROPERTY_CNT_IN_PORTFOLIO"=>false, "!CODE"=>false, "PROPERTY_HIDEN"=>false);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->fetch()){

			 $arAction = $resA->getItem($ob["CODE"]);
			 $arResult[$ob["ID"]] = array(
													 "NAME" => $arAction["NAME"],
													 "SECID" => $arAction["NAME"],
													 "URL" => $arAction["URL"],
													 "CNT_IN_PORTFOLIO" => $ob["PROPERTY_CNT_IN_PORTFOLIO_VALUE"],
													 "CNT_IN_PORTFOLIO_USERS" => $ob["PROPERTY_CNT_IN_PORTFOLIO_USERS_VALUE"],
													 "PRC_IN_PORTFOLIO" => $ob["PROPERTY_PRC_IN_PORTFOLIO_VALUE"],
													 "PRC_OF_USERS" => $ob["PROPERTY_PRC_OF_USERS_VALUE"],
			 );

			}

			unset($resA);
        break;
}



/* Сортировка */
 if(empty($_REQUEST["sort"])){
 	$_REQUEST["sort"] = 'cnt';
 }
 if(empty($_REQUEST["dir"])){
 	$_REQUEST["dir"] = 'desc';
 }

  if($_REQUEST["dir"]=='asc'){
      uasort($arResult, function ($item1, $item2) {
      	switch ($_REQUEST["sort"]) {
      	    case 'cnt':
      	        return $item1["CNT_IN_PORTFOLIO"] > $item2["CNT_IN_PORTFOLIO"];
      	        break;
      	    case 'cnt_user':
      	        return $item1["CNT_IN_PORTFOLIO_USERS"] > $item2["CNT_IN_PORTFOLIO_USERS"];
      	        break;
      	}

		});
  }   elseif($_REQUEST["dir"]=='desc'){
      uasort($arResult, function ($item1, $item2) {
      	switch ($_REQUEST["sort"]) {
      	    case 'cnt':
      	        return $item1["CNT_IN_PORTFOLIO"] < $item2["CNT_IN_PORTFOLIO"];
      	        break;
      	    case 'cnt_user':
      	        return $item1["CNT_IN_PORTFOLIO_USERS"] < $item2["CNT_IN_PORTFOLIO_USERS"];
      	        break;
      	}

		});
  }

?>
 <div class="container">
		<h1>Вхождение бумаг в портфели</h1>
		<div class="main_content">
			<div class="main_content_inner">

					<form class="inline-form sorting-form" enctype="multipart/form-data" method="post">
					  <div class="form-group">
						 <label for="type">Тип активов</label>
						 <select name="type">
							<option value="shares" <?=($_REQUEST["type"]=="shares" || !isset($_REQUEST["type"]))?'selected=""':''?>>Акции РФ</option>
							<option value="etf" <?=$_REQUEST["type"]=="etf"?'selected=""':''?> >ETF</option>
							<option value="shares_usa" <?=$_REQUEST["type"]=="shares_usa"?'selected=""':''?> >Акции США</option>
							<option value="bonds" <?=$_REQUEST["type"]=="bonds"?'selected=""':''?> >Облигации</option>
						 </select>

					  	 <label for="sort">Сортировка</label>&nbsp;
						 <select name="sort">
							<option value="cnt" <?=$_REQUEST["sort"]=="cnt"?'selected=""':''?>>По общему вхождению в портфели</option>
							<option value="cnt_user" <?=$_REQUEST["sort"]=="cnt_user"?'selected=""':''?>>По вхождению в портфели пользователей</option>
						 </select>

					  	 <label for="user">Направление</label>&nbsp;
						 <select name="dir">
							<option value="asc" <?=$_REQUEST["dir"]=="asc"?'selected=""':''?> >По возрастанию</option>
							<option value="desc" <?=$_REQUEST["dir"]=="desc"?'selected=""':''?> >По убыванию</option>
						 </select>
						 <!--<button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>-->
					  </div>

					</form>

<?if(count($arResult)>0):?>
				<table class="table table-condensed table-bordered table-striped">
				  <thead class="bg-info">
					<tr>
							<th>Актив</th>
							<th>Входит в портфели</th>
							<th>Входит в <br>портфели пользователей</th>
						</tr>

					 </thead>
				  <?foreach($arResult as $id=>$arVal):?>
 						<tr>
							<td><a class="text-info" href="<?= $arVal["URL"] ?>" target="_blank"><?= $arVal["NAME"] ?> [<?= $arVal["SECID"] ?>]</a></td>
							<td><p style="text-align: center"><?= $arVal["CNT_IN_PORTFOLIO"];?> <?if(floatval($arVal["PRC_IN_PORTFOLIO"])>0):?>(<?= $arVal["PRC_IN_PORTFOLIO"];?>%)<?endif;?></p></td>
							<td><p style="text-align: center"><?= $arVal["CNT_IN_PORTFOLIO_USERS"];?> <?if(floatval($arVal["PRC_OF_USERS"])>0):?>(<?= $arVal["PRC_OF_USERS"];?>%)<?endif;?></p></td>
						</tr>
				  <?endforeach;?>
				</table>
<?endif;?>
			</div>
		</div>
 </div>
  <script>
 $(document).ready(function(){
	 $('.sorting-form select').on('change', function(){
		var type = $('select[name=type]').val();
		var sort = $('select[name=sort]').val();
		var dir = $('select[name=dir]').val();

		window.top.location.href='<?=CUtil::JSEscape($APPLICATION->GetCurPage(true))?>?type='+type+'&sort='+sort+'&dir='+dir;

    });

 });
 </script>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>