<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();
$portfolioIblockId = 52;
if(!$USER->IsAdmin() && !in_array(array(1,5), $arGroups)){
	LocalRedirect("/");
	exit();
}
CModule::IncludeModule("iblock");
$arSummary = array();
$CPortfolio = new CPortfolio();

$arSummary = array();

$CPortfolio = new CPortfolio();
$CACHE_MANAGER->ClearByTag("getPortfolioListAll_".false);
$arAllPortfolioList = $CPortfolio->getPortfolioListAll();
$arUsersId = array();
$arTmpPortfolioList = array();

//Получим даты начала историй по портфелям
Global $DB;
$arHistoryStartDates = array();
$query = "SELECT `UF_PORTFOLIO`, MIN(`UF_ADD_DATE`) AS `MIN_DATE` FROM `portfolio_history` WHERE `UF_PORTFOLIO` in (".implode(",", array_keys($arAllPortfolioList)).") GROUP BY `UF_PORTFOLIO`";
$res = $DB->Query($query);

while($row = $res->fetch()){
 $arHistoryStartDates[$row['UF_PORTFOLIO']] = $row['MIN_DATE'];
}


foreach($arAllPortfolioList as $pid=>$val){
if(empty($val["PROPERTIES"]["VERSION"]["VALUE"]) || count($val["PROPERTIES"]["LOADED_FILES"]["VALUE"])<=0) continue;
  $userId = $val["PROPERTIES"]["OWNER_USER"]["VALUE"];
  $arGroupsUser = CUser::GetUserGroup($userId);
  $arAdminGroups = array(1,5,7);
  $arrInAdminGrous = array_diff($arAdminGroups, $arGroupsUser);
  if(count($arrInAdminGrous)!=count($arAdminGroups)) continue;


  $arUsersId[] = $userId;
  $arAllPortfolioList[$pid]["DIFFERENCE"] = $val["PROPERTIES"]["CURRENT_SUMM"]["VALUE"] - $val["PROPERTIES"]["INVEST_SUMM"]["VALUE"];//Разница от сумм вложений и сумм тек. стоимости
  if($arAllPortfolioList[$pid]["DIFFERENCE"]!=0){
    $arAllPortfolioList[$pid]["DIFFERENCE_PRC"] = $arAllPortfolioList[$pid]["DIFFERENCE"]/$val["PROPERTIES"]["INVEST_SUMM"]["VALUE"]*100;
  } else {
    $arAllPortfolioList[$pid]["DIFFERENCE_PRC"] = 0;
  }
  if(isset($arHistoryStartDates[$pid])){
  		$arAllPortfolioList[$pid]["MIN_DATE"] = (new DateTime($arHistoryStartDates[$pid]))->format('d.m.Y');
 		$arTmpPortfolioList[$pid] = $arAllPortfolioList[$pid];
 }
 $arSummary["CNT_PORTFOLIO"] += 1;
}
$arAllPortfolioList = $arTmpPortfolioList;
unset($arTmpPortfolioList, $CPortfolio, $arHistoryStartDates, $res);



  //Получаем пользователей для показа в таблицах
  $data = CUser::GetList(($by="ID"), ($order="ASC"),
            array(
                'ID' => $arUsersId,
            )
        );
  $arUserList = array();
	while($arUser = $data->Fetch()) {
	    $arUserList[$arUser['ID']] = $arUser;
	}



/* Сортировка */
 if(empty($_REQUEST["sort"])){
 	$_REQUEST["sort"] = 'diff';
 }
 if(empty($_REQUEST["dir"])){
 	$_REQUEST["dir"] = 'desc';
 }

  if($_REQUEST["dir"]=='asc'){
      uasort($arAllPortfolioList, function ($item1, $item2) {
      	switch ($_REQUEST["sort"]) {
      	    case 'diff':
      	        return $item1["DIFFERENCE"] > $item2["DIFFERENCE"];
      	        break;
      	    case 'diff_prc':
      	        return $item1["DIFFERENCE_PRC"] > $item2["DIFFERENCE_PRC"];
      	        break;


      	}

		});
  }   elseif($_REQUEST["dir"]=='desc'){
      uasort($arAllPortfolioList, function ($item1, $item2) {
      	switch ($_REQUEST["sort"]) {
      	    case 'diff':
      	        return $item1["DIFFERENCE"] < $item2["DIFFERENCE"];
      	        break;
      	    case 'diff_prc':
      	        return $item1["DIFFERENCE_PRC"] < $item2["DIFFERENCE_PRC"];
      	        break;
      	}

		});
  }

?>
 <div class="container">
		<h1>Статистика по загруженным портфелям</h1>
		<div class="main_content">
			<div class="main_content_inner">
<?if(count($arAllPortfolioList)>0):?>

					<form class="inline-form sorting-form" enctype="multipart/form-data" method="post">
					  <div class="form-group">
					  	 <label for="user">Сортировка</label>&nbsp;

						 <select name="sort">
							<option value="diff" <?=$_REQUEST["sort"]=="diff"?'selected=""':''?>>По приросту</option>
							<option value="diff_prc" <?=$_REQUEST["sort"]=="diff_prc"?'selected=""':''?>>По приросту в %</option>
						 </select>

					  	 <label for="user">Направление сортировки</label>&nbsp;

						 <select name="dir">
							<option value="asc" <?=$_REQUEST["dir"]=="asc"?'selected=""':''?> >По возрастанию</option>
							<option value="desc" <?=$_REQUEST["dir"]=="desc"?'selected=""':''?> >По убыванию</option>
						 </select>
						 <!--<button type="submit" name="send" value="Y" class="btn btn-default">Показать</button>-->
					  </div>

					</form>

				<table class="table table-condensed table-bordered table-striped">
				  <thead class="bg-info">
				  		<tr>
							<th>Пользователь</th>
									<th>Портфель</th>
									<th>Дата создания</th>
									<th>Старт. сумма</th>
									<th>Тек. сумма</th>
									<th>Прирост</th>
									<th>Прирост в %</th>
									<th></th>
						</tr>
				  </thead>
				  <?foreach($arAllPortfolioList as $pid=>$arVal):?>
							<?if(empty($arUserList[$arVal["PROPERTIES"]["OWNER_USER"]["VALUE"]]["LOGIN"])) continue;?>
							<? $uid = $arVal["PROPERTIES"]["OWNER_USER"]["VALUE"]; ?>
							<tr class="pid_row_<?=$pid?>">
							<? $diffClass='';
							 if($arVal["DIFFERENCE"]<0){
								$diffClass = 'bg-danger';
							 }
							 if($arVal["DIFFERENCE"]==0){
								$diffClass = 'bg-warning';
							 }

							 ?>
							  <td class="v_middle">
							  <strong><?//= $arUserList[$uid]["LOGIN"] ?></strong>
							  <?if(intval($uid)>0):?>[<?= $uid ?>] <a class="btn btn-info btn-xs btn-right"  href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $uid ?>" target="_blank">Профиль</a><?endif;?>
							  </td>
							  <td ><a class="btn-xs" href="/portfolio/portfolio_<?= $pid ?>/" target="_blank"><?=$pid?> (<?=$arVal["PROPERTIES"]["BROKER"]["VALUE"]?>)</a></td>
							  <td><?=$arVal["MIN_DATE"];//(new DateTime($arVal["PROPERTIES"]["TRACKING_DATE"]["VALUE"]))->format("d.m.Y")?></td>
							  <td><?= round($arVal["PROPERTIES"]["INVEST_SUMM"]["VALUE"],2) ?>р.</td>
							  <td><?= round($arVal["PROPERTIES"]["CURRENT_SUMM"]["VALUE"],2) ?>р.</td>
							  <td class="<?=$diffClass;?>"><?= round($arVal["DIFFERENCE"],2) ?>р.</td>
							  <td class="<?=$diffClass;?>"><?= round($arVal["DIFFERENCE_PRC"],2) ?>%</td>
							  <td><span class="del_pid_row" style="cursor:pointer" data-pid="<?=$pid?>">(i)</span></td>
							  </tr>


				  <?endforeach;?>
				</table>
				<?endif;?>

				<hr>


</div>
</div>
</div>

 <script>
 $(document).ready(function(){
  $('.sorting-form select').on('change', function(){
	var sort = $('select[name=sort]').val();
	var dir = $('select[name=dir]').val();
	window.top.location.href='<?=CUtil::JSEscape($APPLICATION->GetCurPage(true))?>?sort='+sort+'&dir='+dir;
   });

  $('.toggle_items').on('click', function(){
    var target = $(this).data('target');
	 $('.portfolio_items_block').removeClass('active');
	 $('#'+target).addClass('active');
  });

  $('.del_pid_row').on('click', function(){
	 var pid = $(this).attr('data-pid');
	 $('.pid_row_'+pid).detach();
  });

 });
 </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>