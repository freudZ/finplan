<div class="modal modal_black fade" id="popup_autoballance" tabindex="-1" data-backdrop="static">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Автоматическая балансировка портфеля</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
	  <div class="popup_progress" >Балансировка...</div>
		<div class="form_element">
			<div class="row">
			   <div class="col-lg-11">
				 <input type="number" min="0" max="999999999" id="abl_invest_value" placeholder="Минимальный размер инвестиций"/>
				</div>
				<div class="col-lg-1">
				 <span class="btn btn-success abl_recount_button"><span class="icon icon-spinner4"></span></span>
			   </div>
				<div class="abl_info_list_block abl_top_info col-lg-12">
				  <div class="yellow hidden">Увеличьте сумму инвестиций, что бы она была достаточной для вхождения активов в балансировку</div>
				</div>
			</div>
		</div>
			<br/>
                   <div class="row">
                     <div class="abl_chart_block abl_round_chart_outer col-lg-12">
                       <canvas id="abl_round_chart"></canvas>
                     </div>
							<br>
							<!--<div class="maxPart">Максимальная доля: <span></span></div>-->
                     <div class="abl_list_block col-lg-12">
                     </div>
							<div class="abl_info_list_block abl_bottom_info col-lg-12">
							  <div class="red hidden">* Активы не участвующие в балансировке</div>
							  <div class="new_invest_summ">Сумма после балансировки: <span></span></div>
							</div>
                   </div>

		<div class="form_element text-center">
		  <span class="setAutobalance button" data-dismiss="modal">Подтвердить</span>
		</div>
	  </div>
	</div>
  </div>
</div>