<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("Отладка истории портфеля");?>
<br><br><br>
<?
$CPortfolio = new CPortfolio();
$arData = $CPortfolio->getHistoryDataForGraph(intval($_GET["pid"]));
//$arData = $CPortfolio->getHistoryDataForGraph(403614);
/*echo "<pre  style='color:black; font-size:11px;'>";
   print_r($arData);
echo "</pre>";*/

?>
<table class="table table-bordered table-condensed">
<tr>
 <td>Дата</td>
 <td>plus</td>
 <td>minus</td>
 <td>rates</td>
 <td>events</td>
</tr>
<?foreach($arData as $dt=>$arItem):?>
	<tr>
	 <td class="t15">
	 <strong><?= $dt ?></strong>
	  <div style="padding:1px 5px; background-color:#339900; color:#fff"><?= $arItem["ALL_DEALS_SUMM"] ?></div>
	  <div style="padding:1px 5px; background-color:#0033CC; color:#fff"><?= $arItem["ALL_CHISTORY_SUMM"] ?></div>
	  <div style="padding:1px 5px; background-color:#FF9933; color:#fff"><?= $arItem["ALL_INCOMES_SUMM"] ?></div>
	  <div style="padding:1px 5px; background-color:#FFFF66"><?= $arItem["ALL_CACHE_SUMM"] ?></div>
	 </td>
		<?
		$arPlus = array();
		$arOrange = array();
		$arYellow = array();
		$arYellowMinus = array();
		$arMinus = array();

		 ?>
		<?
		foreach($arItem["ACTIVES"] as $k=>$val){
			$str = '<span class="'.(!empty($val["UF_AUTO"])?'red':'').'">'.$val["UF_ADD_DATE"]." ".$val["UF_ACTIVE_TYPE"]." ".$val["UF_SECID"]." Сумма: ".$val["UF_LOTCNT"]." * ".$val["UF_LOTPRICE"]." = <strong>".round($val["UF_LOTCNT"]*$val["UF_LOTPRICE"], 2)."</strong>".(!empty($val["UF_HISTORY_ACT"])?" ".$val["UF_HISTORY_ACT"]:"");
			$str .= '</span>';
			if($val["UF_HISTORY_DIRECTION"]=="PLUS"){

				  	if($val["UF_ACTIVE_TYPE"]=="currency" && $val["UF_HISTORY_ACT"] == $CPortfolio->arHistoryActions["CACHE_PLUS"] && !empty($val["UF_ADD_DATE"]) && !empty($val["UF_AUTO"])){
					$arOrange[] = $val["UF_ADD_DATE"]." ".$val["UF_ACTIVE_TYPE"]." Курс на ".$val["UF_INCOMING_DATE"]." ".$val["UF_SECID"]." лотов: ".$val["UF_LOTCNT"]." Сумма: ".$val["UF_RUB_SUMM_INCOMING"];
					}
					if($val["UF_ACTIVE_TYPE"]=='currency'){
						$yellowIncomeSumm += $notRUB?($arHistActive["UF_LOTCNT"]*$arHistActive["UF_LOTPRICE_RATE"]):$arHistActive["UF_LOTPRICE"];
						$arYellow[] = $val["UF_ADD_DATE"]." ".$val["UF_ACTIVE_TYPE"]." Курс на ".$val["UF_INCOMING_DATE"]." ".$val["UF_SECID"]." лотов: ".$val["UF_LOTCNT"]." Сумма: ".$val["UF_RUB_SUMM_INCOMING"];
					}

				$arPlus[] = $str;
				}else if($val["UF_HISTORY_DIRECTION"]=="MINUS"){
			  $arMinus[] = $str;
			    $arYellowMinus[] = $val["UF_ADD_DATE"]." ".$val["UF_ACTIVE_TYPE"]." Курс на ".$val["UF_INCOMING_DATE"]." ".$val["UF_SECID"]." лотов: ".$val["UF_LOTCNT"]." Сумма: ".$val["UF_RUB_SUMM_INCOMING"];
			}

		}
		  $arOrange[] = "Пред. сумма ".$arItem["LAST_INCOMES_SUMM"];
		?>
		<td class="bg-success t14">
			<? echo implode("<br>", $arPlus) ?><br>
			<div style="padding:1px 5px; background-color:#FF9933; color:#fff"><? echo implode("<br>", $arOrange) ?></div>
			<div style="padding:1px 5px; background-color:#FFFF66;"><? echo implode("<br>", $arYellow) ?></div>
		</td>
		<td class="bg-warning t14">
		   <? echo implode("<br>", $arMinus) ?><br>
			<div style="padding:1px 5px; background-color:#FFFF66;"><? echo implode("<br>", $arYellowMinus) ?></div>
		</td>
		<td class="t14"> <!-- курсы, цены -->
		  <?$arPrices = array();
			 foreach($arItem["REAL_PRICES"] as $k=>$val){
				$str = '<strong>'.$val["UF_SECID"]."</strong> ".round($val["PRICE"]["price"], 2)."р.";
				if($val["UF_ACTIVE_TYPE"]!='currency'){
					$str.= ' '.$val["PRICE"]["price_valute"].getCurrencySign(strtoupper($val["PRICE"]["currencyId"]));
					$str.=' курс на '.$val["PRICE"]["rate_date"].' = '.$val["PRICE"]["rate"].'р.';
				}
				$arPrices[] = $str;
			 }

		  ?>
			<? echo implode("<br>", $arPrices) ?>

		</td>
		<td></td>
	</tr>
<?endforeach;?>
</table>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>