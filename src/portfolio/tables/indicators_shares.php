<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");   ?>
                <div class="portfolio_tabs_table__outer" style="<?=($view_table == 'pt_indicators_shares' ? 'display:block' : 'display:none');?>">
                <table class="radar_table smart_table portfolio_tabs_table pt_indicators_shares">
                  <thead>
                    <tr>
                      <th class="name_col">Акции:</th>
                      <th class="price_col">Цена акции, руб.&nbsp;/<br/>Цена компании, млн.руб.</th>
                      <th class="number_col">P/e&nbsp;/<br/>Рск, %&nbsp;/</br>ТР пр., %</th>
                      <th class="final_price_col">Дивид. за год, %&nbsp;/<br/>сумма&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title=" ">i</span></th>
                      <th class="profitability_col">Прирост за месяц&nbsp;/<br/>за год&nbsp;/<br/>за 3 года</th>
							 <th class="curr_price_col">Прогноз&nbsp;/<br/>Просад&nbsp;/<br/>Бета</th>
                    </tr>
                  </thead>
                  <tbody>

<?php if (count($portfolioActives) > 0): ?>
	  <?foreach ($portfolioActives as $aid => $aval): ?>
	  <? $activeType = $aval['UF_ACTIVE_TYPE'];  ?>
	   <?if($activeType=='share' || $activeType=='share_usa'):?>
		    <? $radar_data = $aval['RADAR_DATA'];
			  $currency_current_rate = getCBPrice(strtoupper($activeCurrencyId=='SUR'?'RUB':$activeCurrencyId),$currency_current_date);//Получаем курс на ближайший рабочий или сегодняшний день
			  $radar_data["PROPS"]["ISSUECAPITALIZATION"] = $radar_data["PROPS"]["ISSUECAPITALIZATION"]*$currency_current_rate;
			if($radar_data["PROPS"]["ISSUECAPITALIZATION"]>=1000000){
				$cap = number_format(round(($radar_data["PROPS"]["ISSUECAPITALIZATION"]/1000000), 0), 0, '.', ' ');
			} else {
				$cap = number_format(round(($radar_data["PROPS"]["ISSUECAPITALIZATION"]/1000000), 2), 2, '.', ' ');
			}
			 ?>
			 <tr data-elem="<?=$activeType;?>">
	                      <td class="obligation">                                          <!-- доходн                                                  таргет  -->
	                        <p class="elem_name trim"><a class="elem_name__link tooltip_btn" href="<?=$aval['UF_ACTIVE_URL'];?>" title="<?=$aval['UF_ACTIVE_NAME'];?>"><?=$aval['UF_ACTIVE_NAME'];?></a></p>
	                        <p class="company_name line_green"><a class="tooltip_btn" href="<?=$radar_data['COMPANY']['URL'];?>" title="<?=$aval['UF_EMITENT_NAME'];?>"><?=$aval['UF_EMITENT_NAME'];?></a></p>
	                      </td>
								 <td>
								   <p class="elem_cshare_price price_elem" data-price="<?=$radar_data["PROPS"]["LASTPRICE"]?>"><?=$radar_data["PROPS"]["LASTPRICE"]?></p>
									<p class="elem_company_price price_elem" data-price="<?=$cap?>"><?=$cap?></p>
								 </td>
								 <td>
									<p><?=$radar_data["DYNAM"]["PE"]?></p>
								   <?if($radar_data["PROPS"]["PROP_VID_AKTSIY"]!=='банковские'):?>
									<?$period = $radar_data["LAST_PERIOD"];?>
									<p><?=round($period["Рентабельность собственного капитала"],2)?></p>
									<p><?=round($period["Темп роста прибыли"],2)?></p>
									<?else:?>
									<p>-</p>
									<p>-</p>
									<?endif;?>
								 </td>
								 <td>
									 <p><?=$radar_data["DYNAM"]["Дивиденды %"]?></p>
									 <p><?=$radar_data["PROPS"]["PROP_DIVIDENDY_2018"]?></p>
								 </td>
								 <td>
									 <?$class=floatval($radar_data["DYNAM"]["MONTH_INCREASE"])>0?'line_green':(floatval($radar_data["DYNAM"]["MONTH_INCREASE"])<0?'line_red':'');?>
										  <p class="<?= $class ?>">
											 <?= !empty($radar_data["DYNAM"]["MONTH_INCREASE"])?round($radar_data["DYNAM"]["MONTH_INCREASE"],2)."%":'-'; ?>
										  </p>
										 <?$classY=floatval($radar_data["DYNAM"]["YEAR_INCREASE"])>0?'line_green':(floatval($radar_data["DYNAM"]["YEAR_INCREASE"])<0?'line_red':'');?>
										 <p class="<?= $classY ?>">
											<?= !empty($radar_data["DYNAM"]["YEAR_INCREASE"])?round($radar_data["DYNAM"]["YEAR_INCREASE"],2)."%":'-'; ?>
										 </p>
										 <?$classTY=floatval($radar_data["DYNAM"]["THREE_YEAR_INCREASE"])>0?'line_green':(floatval($radar_data["DYNAM"]["THREE_YEAR_INCREASE"])<0?'line_red':'');?>
										 <p class="<?= $classTY ?>">
											<?= !empty($radar_data["DYNAM"]["THREE_YEAR_INCREASE"])?round($radar_data["DYNAM"]["THREE_YEAR_INCREASE"],2)."%":'-'; ?>
										 </p>
								 </td>								 
								 <td>
					<?
					$dynam_target = 0;
					$dynam_sales = 0;

					$radar_data["DYNAM"]["Таргет"] ? $dynam_target = $radar_data["DYNAM"]["Таргет"] : $dynam_target = 0;

					$radar_data["DYNAM"]["Просад"] ? $dynam_sales = $radar_data["DYNAM"]["Просад"] : $dynam_sales = 0;
					?>
				<p class="cshare_up_p <?=($dynam_target>0?'line_green':'')?>"  data-value="<?=$dynam_target?>"><?=($dynam_target>0?'+':'')?><span><?=($dynam_target<0?0:($dynam_target.'%'))?></span></p>
				<p class="cshare_down_p line_red"  data-value="<?=$dynam_sales?>">-<span><?=$dynam_sales?></span>%</p>
				<p class="" data-value="<?=$radar_data["PROPS"]["BETTA"]?>"><?=round($radar_data["PROPS"]["BETTA"],2)?></p>
								 </td>

			 </tr>
		 <?endif;?>
	  <?endforeach;?>
<?endif;?>
						</tbody>
						</table>
					 </div>
