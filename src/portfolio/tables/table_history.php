<? //Таблица для показа на вкладке портфельного раздела "История" под графиком ?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>
<?


//$portfolio = $APPLICATION->portfolio;
if(!$portfolio){
   //$portfolio = new CPortfolio(true);
   $portfolio = new CPortfolio();
   }
                              $pid = str_replace("portfolio_", "", $_REQUEST["ELEMENT_ID"]);

										$isOwner = $portfolio->isPortfolioOwner($pid);
										  if(false==$isOwner && !$GLOBALS["USER"]->IsAdmin()){
											 $enabled_editor = false;
										  }
										  if(true==$isOwner || $GLOBALS["USER"]->IsAdmin()) {
											 $enabled_editor = true;
										  }



                              $historyDealsOnPage = 10;
                              if(count($_REQUEST['filter'])>0){
                                 foreach($_REQUEST['filter'] as $k=>$val){
                                   $key = key($val);
                                   $arFilter[$key] = $val[$key];
                                 }
                              } else {
                                 $arFilter = array();
                              }
										$arHistory = $portfolio->getPortfolioHistory($pid, $arFilter);
                         //     $firephp = FirePHP::getInstance(true);
                         //     $firephp->fb(array("filter"=>$arFilter),FirePHP::LOG);
                              //$arFilter = $_REQUEST['filter'];

                              if(count($arFilter)==0){
                                 if(!empty($portfolio->getPortfolioStatesFromSession($pid, 'showChildDeals'))){
                                   $arFilter["showChildDeals"] = $portfolio->getPortfolioStatesFromSession($pid, 'showChildDeals');
                                 } else{
                                   $arFilter["showChildDeals"] = 'N';
                                 }
                                 if(!empty($portfolio->getPortfolioStatesFromSession($pid, 'histAction'))){
                                   $arFilter["histAction"] = $portfolio->getPortfolioStatesFromSession($pid, 'histAction');
                                 } else{
                                   $arFilter["histAction"] = '';
                                 }
                              }

                              if(isset($arFilter["showChildDeals"])){
                                 $portfolio->setPortfolioStatesToSession($pid, 'showChildDeals', $arFilter["showChildDeals"]);
                              }
                              if(isset($arFilter["histAction"])){
                                 $portfolio->setPortfolioStatesToSession($pid, 'histAction', $arFilter["histAction"]);
                              }

                          //    $firephp = FirePHP::getInstance(true);
                          //    $firephp->fb(array("histFilter1"=>$_REQUEST['filter']),FirePHP::LOG);
                          //    $firephp->fb(array("histFilter2"=>$arFilter),FirePHP::LOG);


                              if(count($arFilter)>0){


                               $arHistoryTmp = array();
                                foreach($arHistory as $kr=>$item){
                                   if($arFilter["showChildDeals"]=='N' && $item['UF_CALCULATED']=="Y") continue;
                                   if(!empty($arFilter["histAction"]) && $item["UF_HISTORY_ACT"]!=$portfolio->arHistoryActions[$arFilter["histAction"]]) continue;
											  if(!empty($arFilter["active"]) && $item["UF_ACTIVE_ID"]!=$arFilter["active"]) continue;
											  if(!empty($arFilter["date_from"]) && !empty($arFilter["date_to"])){
												 if((New DateTime($item["UF_ADD_DATE"]))<(new DateTime($arFilter["date_from"]))) continue;
												 if((New DateTime($item["UF_ADD_DATE"]))>(new DateTime($arFilter["date_to"]))) continue;
											  }
                                   $arHistoryTmp[$item["ID"]] = $item;
                                }

                               //Если установлен фильтр по активу и включен показ сопутствующих сделок - добавим их в отфильтрованный массив
										 if(!empty($arFilter["active"]) && $arFilter["showChildDeals"]=='Y'){
										  foreach($arHistory as $kr=>$item){
											 if(!array_key_exists($item["UF_PARENT_ID"], $arHistoryTmp)) continue;
								 //			 $firephp = FirePHP::getInstance(true);
								 //			 $firephp->fb(array("item"=>$item),FirePHP::LOG);
											 $arHistoryTmp[$item["ID"]] = $item;
										  }
										 }


                                $arHistory = $arHistoryTmp;
                                unset($arHistoryTmp);
                              }

                              $HistoryPagenav = ceil(count($arHistory)/$historyDealsOnPage);

                               ?>
<div class="portfolioHistoryTable calculate_table" data-owner="<?=($isOwner?'Y':'N');?>" data-user="<?= $USER->GetID() ?>">

                              <?if(count($arHistory)>10):?>
                                <div class="portfolioHistoryPagenav">
                                       <ul class="pagination_custom">
                                     <?for($hpNav = 0; $hpNav<$HistoryPagenav; $hpNav++):?>
                                       <li class="<?=$hpNav == 0 ? 'active' : ''?>"><a href="javascript:void(0);" class="histPagenav page_<?=$hpNav?>" data-page="<?=$hpNav?>"><?=$hpNav + 1?></a></li>
                                     <?endfor;?>
                                       </ul>
                                </div>
                              <?endif;?>
                                <table class="smart_table history_table">
                                    <thead>
                                        <tr>
                                            <th>Актив</th>
                                            <th>Дата сделки</th>
                                            <th>Сделка</th>
                                            <th>Кол-во</th>
                                            <th>Цена</th>
                                            <th>Сумма</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <?$cntElements = 1;
                                      $page = 0;?>
                 <? /*     usort($arHistory, function ($item1, $item2) {
                   return $item2["UF_ADD_DATE"] <=> $item1["UF_ADD_DATE"];
                  });*/ ?>
                 <?    //  usort($arHistory, $portfolio->sortPortfolioHistory($item1, $item2)); ?>
                 <?     $arHistory = $portfolio->sortPortfolioHistory($arHistory, 'desc'); ?>
<?/*  echo "<pre  style='color:black; font-size:11px;'>";
   print_r($arHistory);
   echo "</pre>"; */?>
                                        <?foreach($arHistory as $kr=>$item):?>
                                        <?$dealView = '';
														$activeType = $item['UF_ACTIVE_TYPE'];
   	   											$radar_data = $item['RADAR_DATA'];
                                          //$childRow = !empty($item["UF_AUTO"]) && $item["UF_HISTORY_DIRECTION"]=='PLUS';
                                          $childRow = $item['UF_CALCULATED']=="Y";
														$calculated = $item['UF_CALCULATED']=="Y";
                                          if($item["UF_HISTORY_DIRECTION"]=='PLUS'){
                                             $dealView = $item["UF_HISTORY_ACT"];
                                             if(!empty($item['UF_DESCRIPTION'])){
                                               $dealView = $item['UF_DESCRIPTION'];
                                             }
                                           //   $dealView = $item["UF_ACTIVE_TYPE"]=='currency'?'Пополнение':'Покупка';
                                          }
                                          if($item["UF_HISTORY_DIRECTION"]=='MINUS'){
                                             $dealView = $item["UF_HISTORY_ACT"];
                                             if($item["UF_HISTORY_ACT"]==$portfolio->arHistoryActions["CACHE_MINUS"] && !empty($item["UF_CLEAR_CACHE"])){
                                              $dealView = "Изъятие";
                                             }
                                             if(!empty($item['UF_DESCRIPTION'])){
                                               $dealView = $item['UF_DESCRIPTION'];
                                             }
															if($item['UF_NO_CACHE_OPERATION']=='Y'){
															  $dealView .= " (без внесения кеша)";
															}
                                            //  $dealView = $item["UF_ACTIVE_TYPE"]=='currency'?'Расход':'Продажа';
                                          }
														$price = $item["UF_PRICE_ONE"]*$item["UF_INLOT_CNT"];
														$priceTotal = $item["UF_PRICE_ONE"]*$item["UF_INLOT_CNT"]*$item["UF_LOTCNT"];
														$price_real = isset($item["RADAR_DATA"]["DYNAM"]["Цена покупки для амортизации"])?$item["RADAR_DATA"]["DYNAM"]["Цена покупки для амортизации"]:0;
														$multiplier = 1;
														if(!empty($item["UF_CASHFLOW_TYPE"]) && (
															$item["UF_CASHFLOW_TYPE"]=="tax" ||
															$item["UF_CASHFLOW_TYPE"]=="comission"
														)){
															$multiplier = -1;
														}

														//$valutePairRate = 1;
														if(array_key_exists("UF_BUY_CURRENCY_RATE", $item)){
														  $price = round($price * $item["UF_BUY_CURRENCY_RATE"], 4);
														  $priceTotal = round($priceTotal * $item["UF_BUY_CURRENCY_RATE"], 4);
														}

														$activeCurrency = $item["UF_CURRENCY"];
														if($item["UF_CURRENCY"]!=$item["UF_ACTIVE_CURRENCY"] && !empty($item["UF_ACTIVE_CURRENCY"])){
															$activeCurrency = $item["UF_ACTIVE_CURRENCY"];
														}
						   ?>
                                            <tr class="portfolioHistoryTablePages <?=($childRow  ? 'child_history_row parent-id-' . $item["UF_PARENT_ID"] : '')?> phPage_<?=$page?> <?=$page > 0 ? 'hidden' : ''?>  row-id-<?=$item["ID"]?>"
                                              data-deal-id="<?=$item["UF_DEAL_ID"]?>"
                                              data-active-id="<?=$item["UF_ACTIVE_ID"]?>"
                                              data-type-active="<?=$item["UF_ACTIVE_TYPE"]?>"
                                              data-active-code="<?=$item["UF_ACTIVE_CODE"]?>"
                                              data-active-secid="<?=$item["UF_SECID"]?>"
                                              data-active-inlot-cnt="<?=$item["UF_INLOT_CNT"]?>"
                                              data-active-currency="<?=$activeCurrency;?>"
                                              data-valute-pair-rate="<?=$item["UF_BUY_CURRENCY_RATE"]?>"
                                              data-row-id="<?=$item["ID"]?>"
                                              data-parent-id="<?=$item["UF_PARENT_ID"]?>"
                                              data-row-direction="<?=$item["UF_HISTORY_DIRECTION"]?>"
                                              data-row-action="<?=$item["UF_HISTORY_ACT"]?>"
                                              data-ammort_summ="<?=$item['BOND_SUM_AMORT']?>"
                                              data-cnt="<?=$item["UF_HISTORY_CNT"]?>"
                                              data-price="<?=$price?>"
                                              data-price-one="<?=$item["UF_PRICE_ONE"]?>"
                                              data-price-summ="<?=$priceTotal?>"
                                              data-now-summ="<?=$price_real?>"
															 data-multiplier="<?=$multiplier?>"
															 <?
/*																$price_real = 0;
															  if($activeType == 'bond'){//Считаем текущую цену облигации принудительно
																 $price_real = ($radar_data['PROPS']['FACEVALUE']/100)*(floatval($radar_data['PROPS']['LASTPRICE'])>0?$radar_data['PROPS']['LASTPRICE']:$radar_data['PROPS']['LEGALCLOSE']);

																 if(floatval($radar_data['PROPS']['ACCRUEDINT'])>0) {  //НКД
																	$price_real = $price_real+floatval($radar_data['PROPS']['ACCRUEDINT']);
																 }
																  $price_real = $price_real * $currency_current_rate;


															  }*/
															  ?>

                                              >
                                                <td class="<?=($childRow ? 'td_child_hist_first' : '')?>">
                                                 <p class="elem_name trim"><a class="elem_name__link tooltip_btn" href="<?=$item["UF_ACTIVE_URL"]?>" title="" data-original-title="<?=$item["UF_ACTIVE_NAME"]?>"><?=$item["UF_ACTIVE_NAME"]?></a></p>
                                                 <?if(!$childRow):?>
                                                 <p class="company_name line_green"><a class="tooltip_btn" href="<?=$item["UF_EMITENT_URL"]?>" title="" data-original-title="<?=$item["UF_EMITENT_NAME"]?>"><?=$item["UF_EMITENT_NAME"]?></a></p>
                                                 <?endif;?>
                                                </td>
                                                <td><p class="history_operation_date"><?=$item["UF_ADD_DATE"]?></p></td>
                                                <td><p class="history_operation_name"><?=$dealView?></p></td>
                                                <td><p><?=round($item["UF_HISTORY_CNT"]*$multiplier,2)?></p></td>
                                                <td><p><?=$price?></p></td>
                                                <td><p><?=round($priceTotal*$multiplier,2)?></p></td>
                                                <td>

                                                   <?if(!$childRow && !$calculated && $enabled_editor==true && !$gsModelPortfolio):?>
                                                     <span data-hist-id="<?=$item["ID"]?>"
                                                           data-price="<?=$item["UF_HISTORY_PRICE"]?>"
																			  data-active-price-one="<?=$item["UF_PRICE_ONE"]?>"
                                                           data-cnt="<?=round($item["UF_HISTORY_CNT"], 2)?>"
                                                           data-operation-date="<?=$item["UF_ADD_DATE"]?>"
                                                           data-portfolio-id="<?=$pid?>"
                                                           class="popup_change_btn edit_history_btn icon icon-dots-horizontal-triple"
                                                           title="Редактировать запись">
                                                     </span>
                                                   <?endif;?>
                                                </td>
                                            </tr>
                                            <?
                                              if($cntElements==$historyDealsOnPage){
                                               $cntElements = 1;
                                               $page++;
                                              } else {
                                                $cntElements++;
                                              }
                                            ?>
                                        <?endforeach;?>
                                    </tbody>
                                </table>
                                <?if(count($arHistory)>10):?>
                                <div class="portfolioHistoryPagenav">
                                       <ul class="pagination_custom">
                                     <?for($hpNav = 0; $hpNav<$HistoryPagenav; $hpNav++):?>
                                       <li class="<?=$hpNav == 0 ? 'active' : ''?>"><a href="javascript:void(0);" class="histPagenav page_<?=$hpNav?>" data-page="<?=$hpNav?>"><?=$hpNav + 1?></a></li>
                                     <?endfor;?>
                                       </ul>
                                </div>
                                <?endif;?>
                             </div>
