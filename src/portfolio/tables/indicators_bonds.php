<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>
                <div class="portfolio_tabs_table__outer" style="<?=($view_table == 'pt_indicators_bonds' ? 'display:block' : 'display:none');?>">
                <table class="radar_table smart_table portfolio_tabs_table pt_indicators_bonds">
                  <thead>
                    <tr>
                      <th class="name_col">Облигация:</th>
                      <th class="price_col">Цена <br/>покупки</th>
                      <th class="">Купоны в % /<br/>в руб /<br/>период</th>
                      <th class="number_col">Срок гашения <br/><span class="line_green">(гашение по оферте)</span> /<br/>Ближайший купон</th>
                      <th class="final_price_col">Цена <br>погашения <br/>облигации <br/>с учетом <br/>купонов</th>
                      <th class="curr_price_col">Доходности: <br/>годовая /<br/>общая&nbsp;/<br/>по оферте</th>
                    </tr>
                  </thead>
                   <?php

                    //	$portf_id = $_REQUEST["ELEMENT_ID"];
                    //	$portfolioActives = $portfolio->getPortfolioActivesList(str_replace("portfolio_", "", $portf_id), false, true, $cur_filter);

                   ?>
                  <tbody>

<?php if (count($portfolioActives) > 0): ?>
	  <?foreach ($portfolioActives as $aid => $aval): ?>
	  <? $activeType = $aval['UF_ACTIVE_TYPE'];  ?>
	   <?if($activeType=='bond'):?>
		<?
		     $radar_data = $aval['RADAR_DATA'];
			  $currency_current_rate = getCBPrice(strtoupper($activeCurrencyId=='SUR'?'RUB':$activeCurrencyId),$currency_current_date);//Получаем курс на ближайший рабочий или сегодняшний день
		?>
		 <tr data-elem="<?=$activeType;?>">
                      <td class="obligation">                                          <!-- доходн                                                  таргет  -->
                        <p class="elem_name trim"><a class="elem_name__link tooltip_btn" href="<?=$aval['UF_ACTIVE_URL'];?>" title="<?=$aval['UF_ACTIVE_NAME'];?>"><?=$aval['UF_ACTIVE_NAME'];?></a></p>
                        <p class="company_name line_green"><a class="tooltip_btn" href="<?=$radar_data['COMPANY']['URL'];?>" title="<?=$aval['UF_EMITENT_NAME'];?>"><?=$aval['UF_EMITENT_NAME'];?></a></p>
                      </td>
							 <td><p><?=round($aval['UF_LOTPRICE'],2);?></p></td>
							 <td>
							 <p><?=round($radar_data['DYNAM']['Купоны в %'],2);?></p>
							 <p><?=$radar_data['PROPS']['COUPONVALUE'];?></p>
							 <p><?=$radar_data['PROPS']['COUPONPERIOD'];?></p>
							 </td>
							 <td>
								<p><?=(new DateTime($radar_data['PROPS']['MATDATE']))->format('d.m.Y');?>
								<?if(!empty($radar_data['PROPS']['OFFERDATE'])):?>
								 <br/><span class="line_green">(<?=(new DateTime($radar_data['PROPS']['OFFERDATE']))->format('d.m.Y');?>)</span>
								 <?else:?>
								 <br/><span class="line_green">-</span>
								<?endif;?>
								</p>
								<p>
								<?if(is_array($aval["LAST_COUPON"]) && count($aval["LAST_COUPON"])>0):?>
								<?= $aval["LAST_COUPON"]["Дата купона"]; ?>
								<?else:?>
								-
								<?endif;?>
								</p>
							 </td>
							 <td>
								<p><?= round($radar_data['DYNAM']['Цена погашения с учетом купонов'],2); ?></p>
							 </td>
							 <td>
							  <p><?= $radar_data['DYNAM']['Доходность годовая'] ?></p>
							  <p><?= $radar_data['DYNAM']['Доходность общая'] ?></p>
							  <p><?= $radar_data['DYNAM']['Доходность к офферте'] ?></p>
							 </td>
		 </tr>
		 <?endif;?>
	  <?endforeach;?>
<?endif;?>
						</tbody>
						</table>
					 </div>
