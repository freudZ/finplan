<? //Таблица для показа на вкладке портфельного раздела "Мои доходы" ?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>
<?

 if($_REQUEST["ajax"]=="y"){

 }
 $rubSign = getCurrencySign('RUB');
 $arEventsTmp = array();
 $portf_id = $_REQUEST["ELEMENT_ID"];
 if(!$portfolio){
 	$portfolio = new CPortfolio(true);
 }
  $arEventsTmp = $portfolio->getPortfolioEventList(str_replace("portfolio_", "", $portf_id), $_REQUEST);



?>
<?
		$allEventCount = 0;
		foreach($arEventsTmp as $arItem=>$arEventsList){
			$allEventCount += count($arEventsList);
		}

		$eventsDealsOnPage = 10;
		$eventsPagenav = round($allEventCount/$eventsDealsOnPage);

			uksort( $arEventsTmp, function($a,$b){
				$res = 0;

				// сравниваем даты добавления
				$addDateA=new DateTime($a); $addDateB=new DateTime($b);
				if( $addDateA != $addDateB ){
					  return ( $addDateA > $addDateB ) ? 1 : -1; //asc
				}


				return $res;
			} );

?>
						   <?if($allEventCount>10):?>
							  <div class="portfolioEventsPagenav">
										<ul class="pagination_custom">
									 <?for($evNav = 0; $evNav<=$eventsPagenav; $evNav++):?>
										<li class="<?=$evNav==0?'active':''?>"><a href="javascript:void(0);" class="eventPagenav page_<?= $evNav?>" data-page="<?= $evNav?>"><?= $evNav+1?></a></li>
									 <?endfor;?>
									   </ul>
							  </div>
						   <?endif;?>
 							<table class="smart_table history_table">
								<tr>
								     <th>Актив</th>
									  <th>Дата начисления</th>
									  <th>Купоны/Дивиденды<br>на 1 акц/обл, <?=$rubSign?></th>
									  <th>Количество</th>
									  <th>Сумма дохода, <?=$rubSign?></th>
								</tr>
								<?$allSumm = 0;?>
								<?$cntElements = 1;
								  $page = 0;?>
								<?foreach($arEventsTmp as $keyDate=>$event):?>

								   <?foreach($event as $ke=>$eventDetail):?>
									<? //$firephp = FirePHP::getInstance(true);
										//$firephp->fb(array("info"=>$eventDetail['INFO']),FirePHP::LOG); ?>
								    <tr class="portfolioEventsTablePages evPage_<?=$page?> <?= $page>0?'hidden':''?>">
								      <td>
										 <p class="elem_name trim"><a class="elem_name__link tooltip_btn" href="<?=$eventDetail['INFO']["UF_ACTIVE_URL"]?>" title="" data-original-title="<?=$eventDetail['INFO']["UF_ACTIVE_NAME"]?>"><?=$eventDetail['INFO']["UF_ACTIVE_NAME"]?></a></p>
										 <p class="company_name line_green"><a class="tooltip_btn" href="<?=$eventDetail['INFO']["UF_EMITENT_URL"]?>" title="" data-original-title="<?=$eventDetail['INFO']["UF_EMITENT_NAME"]?>"><?=$eventDetail['INFO']["UF_EMITENT_NAME"]?></a></p>
										</td>
										<td>
										  <p><?= $keyDate ?></p>
										</td>
										<td>
											<p><?=floatval($eventDetail['ITEMS']['SUM_ONE'])?></p>
										</td>
										<td>
											<p><?=$eventDetail['INFO']["COUNT"]?></p>
										</td>
									  <td>
										  <?= $eventDetail['ITEMS']['SUMM']; ?>
										  <? $allSumm += $eventDetail['ITEMS']["SUMM"];  ?>
									  </td>

									 </tr>
														  <?$cntElements++;
															 if($cntElements==$eventsDealsOnPage){
															  $cntElements = 1;
															  $page++;
															 }
														  ?>
									<? endforeach;?>
								<?endforeach;?>
								<tfoot>
								 <tr>
								  <td colspan="4">Итого:</td>
								  <td><?= $allSumm ?></td>
								 </tr>


								</tfoot>
							</table>
													   <?if($allEventCount>10):?>
							  <div class="portfolioEventsPagenav">
										<ul class="pagination_custom">
									 <?for($evNav = 0; $evNav<=$eventsPagenav; $evNav++):?>
										<li class="<?=$evNav==0?'active':''?>"><a href="javascript:void(0);" class="eventPagenav page_<?= $evNav?>" data-page="<?= $evNav?>"><?= $evNav+1?></a></li>
									 <?endfor;?>
									   </ul>
							  </div>
						   <?endif;?>
							<hr>
<?//unset($portfolioEv);?>
