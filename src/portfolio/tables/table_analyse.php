<? //Таблица для показа на вкладке портфельного раздела "Мои доходы" ?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>
<?

 if($_REQUEST["ajax"]=="y"){

 }
 $rubSign = getCurrencySign('RUB');
 $arAnalTmp = array();
 $pid = $_REQUEST["ELEMENT_ID"];
 if(!$portfolio){
 	$portfolio = new CPortfolio();
 }
 $arAnalyseLib = $portfolio->getAnalyseLib();
 $arAnalyseData = $portfolio->getAnalysePortfolio($pid);

?>
<div class="portfolioAnalysis">
    <h1 class="portfolioAnalysis__title">Анализ портфеля "<span><?=$arAnalyseData["NAME"]?></span>"</h1>

	<div class="hidden" style="display:none;">
	    
        <div class="sectors_colors">
		  <?foreach($portfolio->arSectorsColors as $k=>$color):?>
			  <input type="hidden" value="<?= $color ?>"/>
		  <?endforeach;?>
		 </div>
	  <? foreach($arAnalyseData["B1"] as $key=>$value): ?>
	    <?if($key=='arSectors') continue;?>
	    <?if($key=='arActivesAnalyse') continue;?>
		 <input type="hidden" class="an_<?=$key?>" value="<?=$value?>"/>
	  <? endforeach;?>
	  <? $cnt = 0;
	     foreach($arAnalyseData["B1"]["arSectors"] as $key=>$value): ?>
		 	<input type="hidden" class="an_sector_<?=$cnt?> an_sectors_lib" data-sector="<?=$key?>" data-summ="<?=$value["SUMM"]?>" data-prc="<?=$value["PRC"]?>" value="<?=$value["COUNT"]?>"/>
		 <?$cnt++;?>
	  <? endforeach;?>
<?/* echo "<pre  style='color:black; font-size:11px;'>";
   print_r($arAnalyseData['B2']['arActivesAnalyse']);
   echo "</pre>";*/ ?>
	</div>

    <ol class="portfolioAnalysis__menu">
        <li data-id="#b1_1" title="Структура портфеля" data-errors="" onclick="goToBlock('#b1_1')">Структура портфеля</li>
		<li data-id="#b1_2" title="Валютный баланс" data-errors="" onclick="goToBlock('#b1_2')">Валютный баланс</li>
		<li data-id="#b1_3" title="Количество активов" data-errors="" onclick="goToBlock('#b1_3')">Количество активов</li>
		<li data-id="#b1_4" title="Отраслевая диверсификация" data-errors="" onclick="goToBlock('#b1_4')">Отраслевая диверсификация</li>
        <?if(count($arAnalyseData['B2']['arActivesAnalyse']['bond'])>0):?>
		    <li data-id="#b2_1" title="Облигации" data-errors="" onclick="goToBlock('#b2_1')">Облигации</li>
        <?endif;?>
        <?if(count($arAnalyseData['B2']['arActivesAnalyse']['share'])>0):?>
		    <li data-id="#b2_2" title="Акции" data-errors="" onclick="goToBlock('#b2_2')">Акции</li>
        <?endif;?>
        <?if(count($arAnalyseData['B2']['arActivesAnalyse']['share_etf'])>0):?>
		    <li data-id="#b2_3" title="ETF" data-errors="" onclick="goToBlock('#b2_3')">ETF</li>
        <?endif;?>
		<li data-id="#b3_1" title="Общее резюме" data-errors="" onclick="goToBlock('#b3_1')">Общее резюме</li>
    </ol>

    <div id="b1_1" name="b1_1" class="scrollBlock infinite" data-errors="">
        <div class="infinite_container lightgray_bg">
            <h2 class="portfolioAnalysis__subtitle">1. Структура портфеля</h2>

            <div class="portfolioAnalysis__flex chart">
                <div class="flex-left">
                    <p class="anLegend_an_portfolio_common_squand"><span></span></p>
                    <p class="anLegend_an_portfolio_expected_return"><span></span></p>
                </div>

                <div class="flex-right round_chart_outer_b1">
                    <canvas id="an_b_1_1_chart"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div id="b1_2" name="b1_2" class="scrollBlock" data-errors="">
        <h2 class="portfolioAnalysis__subtitle">2. Валютный баланс</h2>

        <div class="portfolioAnalysis__flex chart">
            <div class="flex-left">
                <p class="anLegend_proportion_actives"><span></span></p>
            </div>

            <div class="flex-right round_chart_outer_b2">
                <canvas id="an_b_1_2_chart"></canvas>
            </div>
        </div>
    </div>

    <div id="b1_3" name="b1_3" class="scrollBlock infinite" data-errors="">
        <div class="infinite_container lightgray_bg">
            <h2 class="portfolioAnalysis__subtitle">3. Количество активов</h2>

            <ul class="portfolioAnalysis__activeNumber">
                <li class="anLegend_an_shares_usa_cnt">Американские акции: <span></span></li>
                <li class="anLegend_an_shares_cnt">Российские акции: <span></span></li>
                <li class="anLegend_an_bonds_cnt">Российские облигации: <span></span></li>
                <li class="anLegend_an_bonds_euro_cnt">Еврооблигации: <span></span></li>
            </ul>
        </div>
    </div>

    <div id="b1_4" name="b1_4" class="scrollBlock" data-errors="">
        <h2 class="portfolioAnalysis__subtitle">4. Отраслевая диверсификация</h2>

        <p class="anLegend_sector_divers"><span class="strong"></span></p><br/>

        <div class="portfolioAnalysis__flex chart">
            <div class="flex-left">
                <ul class="anLegend_sector_legend two_columns">
				  <? $cnt = 0;
				     foreach($arAnalyseData["B1"]["arSectors"] as $key=>$value): ?>
                     <li class="li_sector_<?=$cnt?>" data-sector="<?=$key?>" data-prc="<?=$value["PRC"]?>" data-value="<?=$value["COUNT"]?>"><span class="legend_sector_color" style="background-color:<?=$arAnalyseData["sectors_colors"][$cnt]?>;">&nbsp;</span>&nbsp;&nbsp;<?=$key?>: <span class="strong"><?=round($value["PRC"], 2)?>%</span></li>
					 <?$cnt++;?>
				  <? endforeach;?>
                </ul>
            </div>

            <div class="flex-right round_chart_outer_b4">
                <canvas id="an_b_1_4_chart"></canvas>
            </div>
        </div>
    </div>

    <div class="infinite">
        <div class="infinite_container lightgray_bg">
            <?if(count($arAnalyseData['B2']['arActivesAnalyse']["bond"])>0):?>
                <div id="b2_1" name="b2_1" class="scrollBlock portfolioAnalysis__papersList" data-errors="">
                    <h2 class="portfolioAnalysis__subtitle">Облигации</h2>

                    <ul class="two_columns">
                        <?foreach($arAnalyseData['B2']['arActivesAnalyse']['bond'] as $arItem):?>
                            <li class="<?=$arItem["WARNING_CLASS"]?>">
                                <a href="<?=$arItem["ACTIVE_URL"]?>" target="_blank" title="<?=$arItem["ACTIVE_NAME"]?>"><?=$arItem["ACTIVE_NAME"]?></a> - <span><?=$arItem["ACTIVE_MESSAGE"]?></span>
                            </li>
                        <?endforeach;?>
                    </ul>
                </div>
            <?endif;?>

            <?if(count($arAnalyseData['B2']['arActivesAnalyse']["share"])>0):?>
                <div id="b2_2" name="b2_2" class="scrollBlock portfolioAnalysis__papersList" data-errors="">
                    <h2 class="portfolioAnalysis__subtitle">Акции</h2>

                    <ul class="two_columns">
                        <?foreach($arAnalyseData['B2']['arActivesAnalyse']['share'] as $arItem):?>
                            <li class="<?=$arItem["WARNING_CLASS"]?>">
                                <a href="<?=$arItem["ACTIVE_URL"]?>" target="_blank" title="<?=$arItem["ACTIVE_NAME"]?>"><?=$arItem["ACTIVE_NAME"]?></a> - <span><?=$arItem["ACTIVE_MESSAGE"]?></span>
                            </li>
                        <?endforeach;?>
                    </ul>
                </div>
            <?endif;?>

            <?if(count($arAnalyseData['B2']['arActivesAnalyse']['share_etf'])>0):?>
                <div id="b2_3" name="b2_3" class="scrollBlock portfolioAnalysis__papersList" data-errors="">
                    <h2 class="portfolioAnalysis__subtitle">ETF</h2>

                    <ul class="two_columns">
                        <?foreach($arAnalyseData['B2']['arActivesAnalyse']['share_etf'] as $arItem):?>
                            <li class="<?=$arItem["WARNING_CLASS"]?>">
                                <a href="<?=$arItem["ACTIVE_URL"]?>" target="_blank" title="<?=$arItem["ACTIVE_NAME"]?>"><?=$arItem["ACTIVE_NAME"]?></a> - <span><?=$arItem["ACTIVE_MESSAGE"]?></span>
                            </li>
                        <?endforeach;?>
                    </ul>
                </div>
            <?endif;?>
        </div>
    </div>

    <div id="b3_1" name="b3_1" class="scrollBlock infinite summary" data-errors="">
        <div class="infinite_container green">
            <h2 class="portfolioAnalysis__subtitle">Общее резюме:</h2>

            <p>При анализе портфеля программа исходит из того, что портфель должен быть защищенным (то есть все риски в портфеле должны быть перекрыты) и сбалансированы. При этом мы понимаем, что существуют разные типы инвесторов и есть те, для кого избыточные риски приемлемы. Тем не менее, мы считаем важным сообщить Вам все подводные камни портфеля, которые мы заметили.</p>
            <p>Если Вы хотите привести свой портфель к более защищенному и сбалансированному состоянию, тогда изучите все красные замечания и по каждому из них примите индивидуальное решение соглашаться с ним или нет.</p>
            <p><strong>Указанная аналитика не является персональной инвестиционной рекомендацией.</strong></p>
        </div>
    </div>
</div>
<div id="an_lib" class="hidden">
  <??>
  <?foreach($arAnalyseLib as $arItem):?>
	<div class="anlib param_<?= $arItem["UF_PARAM"]?>" data-id="<?= $arItem["ID"]?>" data-param="<?= $arItem["UF_PARAM"]?>" data-condition="<?= $arItem["UF_CONDITION"]?>"><?= $arItem["UF_CONTENT"]?></div>
  <?endforeach;?>
</div>
<hr>
<?//unset($portfolioEv);?>
