<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

	if($_POST['ajax']=='y'){
		$cur_portfolio['id'] = $_POST['ELEMENT_ID'];
	}
if(!$portfolio){
   //$portfolio = new CPortfolio(true);
   $portfolio = new CPortfolio();
   }
if(!$startDate){
	 	$startDate = $portfolio->getHistoryStartDate($cur_portfolio['id']);
	}

	$dtStart = (new DateTime($startDate))->format('d.m.Y');
	$dtTo = date('d.m.Y');

	if(count($_REQUEST['filter'])>0){
		$arFilterTmp = array();
		foreach($_REQUEST['filter'] as $arTmpFltr){
			reset($arTmpFltr);
			$key = key($arTmpFltr);

		  $_REQUEST['filter'][$key] = $arTmpFltr[$key];
		}
		 //$_REQUEST['filter'] = $arFilterTmp;

		if(!empty($_REQUEST['filter']['date_from'])){
		 $dtStart = $_REQUEST['filter']['date_from'];
		}else{
		 $dtStart = $startDate;
		}
		if(!empty($_REQUEST['filter']['date_to'])){
		 $dtTo = $_REQUEST['filter']['date_to'];
		}
	}
			 $firephp = FirePHP::getInstance(true);
       $firephp->fb(array("_REQUEST"=>$_REQUEST, "startDate"=>$startDate, "dtStart"=>$dtStart),FirePHP::LOG);
	//$portfolio = $APPLICATION->portfolio;

/*	*/
			 //Отдельно получаем список активов в портфеле (без отбора для списка в фильтре по активам) и сохраняем в сессию
/*			 $_SESSION["PORTFOLIO_ACTIVES"][$cur_portfolio['id']] = array();
			 $arActivesList = $portfolio->getActivesForHistoryFilter($cur_portfolio['id']);
          $_SESSION["PORTFOLIO_ACTIVES"][$cur_portfolio['id']] = $arActivesList;*/

?>
 		<div class="flex_row row">
         <div class="col col_left col-xs-12">
                            <div class="form_element">
                                <label>Дата: с
                                    <span class="datepicker-inline__outer">
                                       <input type="text" name="phtf_date_from" id="phtf_date_from" placeholder="дата начала" class="datepicker month-delimetr-action" data-dt-start="<?=$dtStart?>" value="<?=$dtStart?>">
                                       <span class="icon icon-arr_down"></span>
                                    </span>

                                </label>
                                <label>по
                                    <span class="datepicker-inline__outer">
                                       <input type="text" name="phtf_date_to" id="phtf_date_to" placeholder="дата окончания" class="datepicker month-delimetr-action" value="<?=$dtTo?>">
                                       <span class="icon icon-arr_down"></span>
                                    </span>
										  </label>
                            </div>
         </div>
<!--         <div class="col col_left col-xs-7">
           <div class="form_element">
				  <label>Актив&nbsp;</label>
					 <select id="phtf_active" class="midval inline_select">
					    <option value="" data-code="" <?=(empty($_REQUEST['filter']['active'])?' selected=""':'')?> data-secid="">Все</option>
						<?foreach($_SESSION["PORTFOLIO_ACTIVES"][$cur_portfolio['id']] as $code=>$item):?>
						 <option value="<?= $code ?>" data-isin-code="<?=$item["CODE"]?>" <?=($_REQUEST['filter']['active']==strval($code)?' selected=""':'')?> ><?=$item["NAME"]?></option>
						<?endforeach;?>
					 </select>
           </div>
         </div>-->
<!--         <div class="col col_left col-xs-7">
           <div class="form_element">
				  <label>Вид движения&nbsp;</label>
					 <select id="phtf_historyActions" class="midval inline_select">
					    <option value="" data-code="" <?=($portfolio->getPortfolioStatesFromSession($cur_portfolio['id'], 'histAction')==""?' selected=""':'')?> data-secid="">Все</option>
						<?foreach($portfolio->arHistoryActions as $code=>$histAction):?>
						 <option value="<?= $code ?>" <?=($portfolio->getPortfolioStatesFromSession($cur_portfolio['id'], 'histAction')==$code?' selected=""':'')?>><?=$histAction?></option>
						<?endforeach;?>
					 </select>
           </div>
         </div>-->
         <div class="col col_left col-xs-7">
           <div class="form_element">
								<span class="checkbox custom phtf_Table_showAutoOperation">
                            <input type="checkbox" class="" id="phtf_Table_showAutoOperation" name="phtf_Table_showAutoOperation" value="Y" <?= ($portfolio->getPortfolioStatesFromSession($cur_portfolio['id'], 'showChildDeals')=='Y'?'checked=""':'')?> id="checkbox_0">
                            <label for="phtf_Table_showAutoOperation">Связанные операции</label>
                         </span>
           </div>
         </div>

       </div>
