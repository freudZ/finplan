<?  define("LK_PAGE", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
global $USER;
if(!$USER->IsAuthorized()){
    LocalRedirect("/");
    exit();
}

$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
$haveRadarAccess = checkPayRadar();

?>
<?$activeTab = isset($_REQUEST["PERSONAL_TAB"])?'lk'.$_REQUEST["PERSONAL_TAB"]:'lkmain'?>
<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner ">
				<div class="hidden-sm hidden-md hidden-lg">
					<div class=" no_separator-">
					<ul class="tabs_nav">
						<li class="<?= $activeTab=='lkmain'?'active':''?> lkTabBtn lk-right-tab-button" data-url="main"><a href="#lkmain"  data-toggle="tab" aria-expanded="false">Личный кабинет</a></li>
						<li class="<?= $activeTab=='lkbonus'?'active':''?> lkBonusTabBtn lk-right-tab-button" data-url="bonus"><a href="#lkbonus" data-toggle="tab" aria-expanded="false">Бонусная программа</a></li>
					   <li class="<?= $activeTab=='lknote'?'active':''?> lkNotificationTabBtn lk-right-tab-button" data-url="note"><a href="#lknote" data-toggle="tab" aria-expanded="false">Уведомления</a></li>
					</ul>
					</div>
					<hr>
				</div>
				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
				                <!-- *****/КАРКАС***** -->

                <div class="userpage_outer tab-content">
				  <div id="lkmain" class="tab-pane <?= $activeTab=='lkmain'?'active':''?>">
					 <?include "tabs/lkTab.php"?>
              </div>
					 <div id="lkbonus" class="tab-pane <?= $activeTab=='lkbonus'?'active':''?>">
						<?include "tabs/lkBonusTab.php"?>
					 </div>
					 <div id="lknote" class="tab-pane <?= $activeTab=='lknote'?'active':''?>">
						<?include "tabs/lkNotificationTab.php"?>
					 </div>
					 </div>

                <!-- *****КАРКАС***** -->
            </div>
        </div>

        <div class="sidebar">
	<div class="sidebar_inner">
        <form class="innerpage_search_form" method="GET">
            <div class="form_element">
                <input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Название / тикер / ISIN" name="q" id="autocomplete_all" />
                <button type="submit"><span class="icon icon-search"></span></button>
            </div>
        </form>
				<div class="hidden-xs">
					<div class=" no_separator-">
					<ul class="tabs_nav">
						<li class="<?= $activeTab=='lkmain'?'active':''?> lkTabBtn lk-right-tab-button" data-url="main"><a href="#lkmain"  data-toggle="tab" aria-expanded="false">Личный кабинет</a></li>
						<li class="<?= $activeTab=='lkbonus'?'active':''?> lkBonusTabBtn lk-right-tab-button" data-url="bonus"><a href="#lkbonus" data-toggle="tab" aria-expanded="false">Бонусная программа</a></li>
						<li class="<?= $activeTab=='lknote'?'active':''?> lkNotificationTabBtn lk-right-tab-button" data-url="note"><a href="#lknote" data-toggle="tab" aria-expanded="false">Уведомления</a></li>
					</ul>
					</div>
					<hr>
				</div>
		  <div class="sidebar_group">
			<div class="sidebar_group_inner">
					<div class="sidebar_element_ no_separator">
						<div class="sidebar_article_img_outer">
							<div class="sidebar_article_img">
							  <?if(!$haveRadarAccess):?>
							  <a href="#" data-toggle="modal" data-target="#buy_subscribe_popup"><img alt="" src="/upload/medialibrary/eaa/image_2020_09_15T05_14_08_074Z.png" title=""></a>
							  <?else:?>
			 					<a href="/upload/personal/Способы продвижения.pdf" target="_blank"><img alt="" src="/upload/medialibrary/eaa/image_2020_09_15T05_14_08_074Z.png" title=""></a>
							  <?endif;?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?$APPLICATION->IncludeComponent("bitrix:news.list", "right_sidebar_courses", array(
				"IBLOCK_TYPE" => "FINPLAN",
				"IBLOCK_ID" => "9",
				"NEWS_COUNT" => "999",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "LINK",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "N",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"AJAX_OPTION_ADDITIONAL" => ""
			),
				false
			);?>
        </div>
	   </div>
    </div>
</div>

<div class="modal fade" id="popup_lk_ok" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title uppercase">Сохранение</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <p class=" light text-center">Данные успешно сохранены</p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="popup_lk_err" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title uppercase">Ошибка</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <p class=" light text-center"></p>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="popup_add_interview" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header noborder">
                    <p class="modal-title">Оставить заявку на видеоинтервью</p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">

                    <form id="add_interview_form" class="add_review_form" method="post">
                        <div class="form_element">
                            <input type="text" name="name" placeholder="Ваши фамилия и имя" />
                        </div>

                        <div class="form_element">
                            <input type="text" name="email" placeholder="Ваша электронная почта" />
                        </div>

                        <div class="form_element">
                            <input type="text" name="phone" placeholder="Ваш телефон" />
                        </div>

                        <div class="submit_element text-center">
                            <input type="submit" class="button" value="Отправить"  />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="popup_add_text_query" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header noborder">
                    <p class="modal-title">Оставить заявку на сотрудничество</p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">

                    <form id="add_text_query" class="add_review_form" method="post">
                        <div class="form_element">
                            <input type="text" name="name" placeholder="Ваши фамилия и имя" />
                        </div>

                        <div class="form_element">
                            <input type="text" name="email" placeholder="Ваша электронная почта" />
                        </div>

                        <div class="form_element">
                            <input type="text" name="phone" placeholder="Ваш телефон" />
                        </div>

                        <div class="submit_element text-center">
                            <input type="submit" class="button" value="Получить задание"  />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>