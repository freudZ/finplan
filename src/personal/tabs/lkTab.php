                <div class="title_container">
                    <h1>Личный кабинет</h1>
                </div>

                    <div class="userpage_user_info custom_collapse accordion">
                        <div class="userpage_user_info_element custom_collapse_elem">
                            <div class="userpage_user_info_element_head custom_collapse_elem_head">
                                <p class="userpage_user_info_element_head_label">Логин:</p>

                                <div class="userpage_user_info_element_head_title">
                                    <p><?=$USER->GetLogin()?></p>
                                </div>
                            </div>
                        </div>

                        <div class="userpage_user_info_element custom_collapse_elem">
                            <div class="userpage_user_info_element_head custom_collapse_elem_head">
                                <p class="userpage_user_info_element_head_label">ФИО:</p>

                                <?$fields = array("LAST_NAME", "FIRST_NAME", "SECOND_NAME");
                                $fio = array();
                                foreach ($fields as $f){
                                    if($t = $USER->GetParam($f)){
                                        $fio[] = $t;
                                    }
                                }
								$fio = $USER->GetParam("FIRST_NAME");
                                if(!$fio){
                                    $fio = $USER->GetParam("LOGIN");
                                }
                                ?>
                                <div class="userpage_user_info_element_head_title">
                                    <p><?=$fio?></p>
                                </div>
                            </div>
                            <div class="userpage_user_info_element_body_outer">
                                <span class="collapse_btn" data-id="#userpage_user_info_element_2">Изменить</span>

                                <div id="userpage_user_info_element_2" class="userpage_user_info_element_body custom_collapse_elem_body">
                                    <div class="userpage_user_info_element_body_inner">
                                        <form>
                                            <div class="form_element">
                                                <input type="text" name="name" placeholder="Введите имя" value="<?=$USER->GetParam("FIRST_NAME")?>" />
                                            </div>

                                            <div class="submit_element">
                                                <input class="button button_green" type="submit" value="Сохранить" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="userpage_user_info_element custom_collapse_elem">
                            <div class="userpage_user_info_element_head custom_collapse_elem_head">
                                <p class="userpage_user_info_element_head_label">Пароль для входа:</p>

                                <div class="userpage_user_info_element_head_title">
                                    <p>*********</p>
                                </div>
                            </div>
                            <div class="userpage_user_info_element_body_outer">
                                <span class="collapse_btn" data-id="#userpage_user_info_element_2">Изменить</span>

                                <div id="userpage_user_info_element_3" class="userpage_user_info_element_body custom_collapse_elem_body">
                                    <div class="userpage_user_info_element_body_inner">
                                        <form>
                                            <div class="form_element">
                                                <input type="password" name="password" placeholder="Введите новый пароль" />
                                            </div>
                                            <div class="form_element">
                                                <input type="password" name="confirm_password" placeholder="Повторите пароль" />
                                            </div>

                                            <div class="submit_element">
                                                <input class="button button_green" type="submit" value="Сохранить" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="userpage_my_bonuses lkTab ">
					<?$showBonusProgram = \Bitrix\Main\Config\Option::get("grain.customsettings","ENABLE_BONUS_PROGRAM");
					if($showBonusProgram == "Y" || $GLOBALS["USER"]->IsAdmin()):?>
						<?
						$itemNumber = 1;
						$promoItems = Promocodes::getUserPromocodes($USER->GetID());
						if($promoItems["PERSONAL"][0]){
							$personal = $promoItems["PERSONAL"][0];
						}
						?>
					<?		$totalBonuses = Bonuses::getTotalBonuses($USER->GetID());
							$items = Bonuses::getOperations($USER->GetID());
							$userReferals = promocodes::getUserReferalsToLK($USER->GetID());
							?>
							<?if(intval($totalBonuses)<=0):?>
							  <p class="userpage_my_bonuses_title" >У Вас пока 0 бонусов</p>
							<?else:?>
							  <p class="userpage_my_bonuses_title" >Мои бонусы: <?=intval($totalBonuses)?></p>
							<?endif;?>

								  	   <? $commonReferals = 0;
									     $payedReferals = 0;
										  $strReferals = '';

										  foreach($items as $item){  //Считаем принесших бонусы
										  	if($item["UF_TYPE"]==9)
											  $payedReferals++;
										  }

									  if(count($userReferals)>0 ){
										  if(array_key_exists(88,$userReferals)){
										  	$commonReferals += intval($userReferals[88]);
											$strReferals = 'привлеченных рефералов: '.$commonReferals;
											}
										  if(array_key_exists("",$userReferals)){
										  	$commonReferals += intval($userReferals[89]);
											$commonReferals += $payedReferals;
										  	//$payedReferals += intval($userReferals[89]);
											$strReferals = 'привлеченных рефералов: <span class="yellow">'.$commonReferals.'</span>, из них прибыльных: <span class="yellow">'.$payedReferals.'</span>';
											}

									  } else {
									  	$strReferals = 'Вы не привлекли ни одного реферала';
									  }

										$strReferals = 'Кликов по ссылкам: <span class="yellow">'.intval($personal["PROPERTY_UTM_STAT_CLICK_VALUE"]).'</span>, '.$strReferals;

									   ?>

			            <p class="userpage_my_bonuses_inner_title t18 light yellow">Реферальная программа: <br><span class="white"><?echo $strReferals; ?></span></p>

							<button class="button lk_bonus_more_button">Подробнее о бонусной программе</button>
						<?endif //Включен показ бонусной программы?>
					</div>

                    <div class="lk_portfolio_ajax_data">
						<?$APPLICATION->IncludeFile("/ajax/lk/portfolio_list_lk.php")?>
                    </div>

                    <div class="lk_favorite_ajax_data">
						<?$APPLICATION->IncludeFile("/ajax/lk/favorite.php")?>
                    </div>

