                <div class="title_container">
                    <h1>Бонусная программа</h1>
                </div>
					 <div>
					 <p class="font_15 lightgray">Мы предлагаем Вам получать вознаграждение за участие в продвижении финансовой грамотности в СНГ, а также в развитии и популяризации сайта Fin-plan.org. Посмотрите презентацию нашей партнерской программы на видео ниже</p>
							<br/>
                  <div class="video_outer">
                        <span class="play"></span>
                        <div class="before"></div>
                        <?/*<div class="before" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/learn_page/video_02.jpg)"></div>*/?>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Y88TzFyNUwM?rel=0&amp;controls=1&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
          			</div>
					 <!--<iframe width="100%" height="315" src="https://www.youtube.com/embed/Y88TzFyNUwM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
					 </div>
					<div class="userpage_my_bonuses">
					<?$showBonusProgram = \Bitrix\Main\Config\Option::get("grain.customsettings","ENABLE_BONUS_PROGRAM");
					if($showBonusProgram == "Y" || $GLOBALS["USER"]->IsAdmin()):?>
						<?
						$itemNumber = 1;
						$promoItems = Promocodes::getUserPromocodes($USER->GetID());
						if($promoItems["PERSONAL"][0]){
							$personal = $promoItems["PERSONAL"][0];
						}
						?>
					<?		$totalBonuses = Bonuses::getTotalBonuses($USER->GetID());
							$items = Bonuses::getOperations($USER->GetID());
							$userReferals = promocodes::getUserReferalsToLK($USER->GetID());
							?>
							<?if(intval($totalBonuses)<=0):?>
							  <p class="userpage_my_bonuses_title" >У Вас пока 0 бонусов</p>
							<?else:?>
							  <p class="userpage_my_bonuses_title" >Мои бонусы: <?=intval($totalBonuses)?></p>
							<?endif;?>

								  	   <? $commonReferals = 0;
									     $payedReferals = 0;
										  $strReferals = '';

										  foreach($items as $item){  //Считаем принесших бонусы
										  	if($item["UF_TYPE"]==9)
											  $payedReferals++;
										  }

									  if(count($userReferals)>0 ){
										  if(array_key_exists(88,$userReferals)){
										  	$commonReferals += intval($userReferals[88]);
											$strReferals = 'привлеченных рефералов: '.$commonReferals;
											}
										  if(array_key_exists("",$userReferals)){
										  	$commonReferals += intval($userReferals[89]);
											$commonReferals += $payedReferals;
										  	//$payedReferals += intval($userReferals[89]);
											$strReferals = 'привлеченных рефералов: <span class="yellow">'.$commonReferals.'</span>, из них прибыльных: <span class="yellow">'.$payedReferals.'</span>';
											}

									  } else {
									  	$strReferals = 'Вы не привлекли ни одного реферала';
									  }

										$strReferals = 'Кликов по ссылкам: <span class="yellow">'.intval($personal["PROPERTY_UTM_STAT_CLICK_VALUE"]).'</span>, '.$strReferals;

									   ?>

			            <p class="userpage_my_bonuses_inner_title t18 light yellow">Реферальная программа: <br><span class="white"><?echo $strReferals; ?></span></p>


							<span class="userpage_my_bonuses_inner_toggle icon icon-arr_down collapsed" data-toggle="collapse" data-target="#userpage_my_bonuses_collapse"></span>

							<div id="userpage_my_bonuses_collapse" class="userpage_my_bonuses_collapse collapse">
								<div class="userpage_my_bonuses_inner">
									<p class="userpage_my_bonuses_inner_title t18 light white">История операций с бонусами:</p>

									<div class="userpage_my_bonuses_history customscroll">
										<?if($items):?>
											<table class="userpage_my_bonuses_history_table">
												<tbody>
													<?foreach($items as $item):
														$class = "minus";
														$prefix = "-";
														$text = "Списание бонусов (оплата «".$item["UF_DESC"]."»)";

														if($item["UF_TYPE"]==9){
															$class = "plus";
															$prefix = "+";
															$text = "Начисление бонусов (".$item["UF_DESC"].")";
														}
														?>
														<tr class="<?=$class?>">
															<td class="date_col"><?=$item["UF_DATE"]->format("d.m.Y")?></td>
															<td class="event_col">
																<p class="date"><?=$item["UF_DATE"]->format("d.m.Y")?></p><?=$text?>
															</td>
															<td class="bonus_col"><?=$prefix?><?=$item["UF_VAL"]?></td>
														</tr>
													<?endforeach?>
												</tbody>
											</table>
										<?else:?>
											<p class="white">В данный момент у вас нет операций по бонусам.</p>
										<?endif?>
									</div>
								</div>
							</div>
						<?endif //Включен показ бонусной программы?>
					</div>

					<?$showBonusProgram = \Bitrix\Main\Config\Option::get("grain.customsettings","ENABLE_BONUS_PROGRAM");
					if($showBonusProgram == "Y" || $GLOBALS["USER"]->IsAdmin()):?>

						<?if($promoItems["AFTER_BUY"][0]):
							$item = $promoItems["AFTER_BUY"][0];
							$dt = new DateTime($item["PROPERTY_DATE_END_VALUE"]);?>
							<div class="userpage_promocode infinite_container green">
								<p class="userpage_promocode_title">Ваш промокод на следующие курсы:
									<br/>
									<span class="strong"><?=$item["NAME"]?></span> <span class="promocode_btn buffer_copy_btn" data-clipboard-text="<?=$item["NAME"]?>">скопировать</span></p>

								<p class="userpage_promocode_description">Данный промокод дает скидку <?=$item["PROPERTY_SET_VALUE"]?><?=$item["PROPERTY_SET_TYPE_VALUE"]?> от цены сайта на любые дополнительные курсы и действителен до <?=FormatDate("j F Y", $dt->getTimestamp())?> года включительно. Вы можете использовать его сами для приобретения других курсов или дать друзьям.</p>
							</div>
						<?endif?>

						<div class="userpage_bonuses">
							<p class="userpage_bonuses_subtitle">Варианты участия в бонусной системе:</p>

							<div class="userpage_bonuses_list custom_collapse">
								<?//if($personal):?>
									<div class="userpage_bonuses_element custom_collapse_elem opened">
										<div class="userpage_bonuses_element_head custom_collapse_elem_head">
											<div class="col_left col">
												<span class="number collapse_btn"><?=$itemNumber?></span>
											</div>

											<div class="col_right col">
												<p class="collapse_btn">Приглашайте своих друзей на наши бесплатные вебинары с помощью <span class="personal_link_btn buffer_copy_btn" data-clipboard-text="http://mk.fin-plan.org/?utm_personal=<?=$personal["NAME"]?>">Вашей персональной ссылки</span> и получайте вознаграждение, когда они станут нашими клиентами.</p>

												<span class="icon icon-arr_down collapse_btn"></span>
											</div>
										</div>

										<div class="userpage_bonuses_element_body custom_collapse_elem_body">
											<div class="userpage_bonuses_element_body_inner">
												<div class="userpage_bonuses_element_card card_copy">
													<div class="col_left col">
														<p class="card_copy_link tooltip_btn" title="https://finplan.expert/10-investor-tattoos/?utm_personal=<?=$personal["NAME"]?>">https://finplan.expert/10-investor-tattoos/?utm_personal=<?=$personal["NAME"]?></p>

														<span class="personal_link_btn buffer_copy_btn button button_transparent" data-clipboard-text="https://finplan.expert/10-investor-tattoos/?utm_personal=<?=$personal["NAME"]?>">Копировать</span>

														<p class="card_copy_sys">ссылку в буфер</p>

														<span class="icon icon-linear_arr_down"></span>
														<span class="icon icon-linear_arr_right"></span>
													</div>

													<div class="col_right col">Ваша персональная ссылка для приглашения друзей на вебинар</div>
												</div>

												<div class="userpage_bonuses_element_card send_mail">
													<div class="col_left col">
														<p class="card_copy_link tooltip_btn">Узнайте как лучше оформить ссылку в соц.сетях</p>
                                 			<a class="button button_transparent" href="/upload/personal/Способы продвижения.pdf" target="_blank">Инструкция</a>

														<span class="icon icon-linear_arr_down"></span>
														<span class="icon icon-linear_arr_right"></span>
													</div>

													<div class="col_right col">Поделитесь своей партнерской ссылкой в соц.сетях и мессенджерах</div>
												</div>

												<div class="userpage_bonuses_element_card percents">
													<div class="col_left col">
														<p class="percents_first_line">от <span>10%</span></p>
														<p class="percents_second_line">к вашим бонусам</p>
													</div>

													<div class="col_right col">Получайте от 10% от стоимости покупки друзей!</div>
												</div>

												<a class="userpage_bonuses_element_more" href="#" target="_blank">Подробнее</a>
											</div>
										</div>
									</div>
									<?$itemNumber++?>
								<?//endif?>
							  <?if(true == false): // ПРосто скрываем блок?>
								<div class="userpage_bonuses_element custom_collapse_elem">
									<div class="userpage_bonuses_element_head custom_collapse_elem_head">
										<div class="col_left col">
											<span class="number collapse_btn"><?=$itemNumber?></span>
											<?$itemNumber++?>
										</div>

										<div class="col_right col">
											<p class="collapse_btn">Пригласите своих друзей на наши бесплатные вебинары с помощью своих соц. сетей и получайте от 10%, когда они станут нашими клиентами.</p>

											<span class="icon icon-arr_down collapse_btn"></span>
										</div>
									</div>

									<div class="userpage_bonuses_element_body custom_collapse_elem_body">
										<div class="userpage_bonuses_element_body_inner">
											<div class="userpage_bonuses_element_card post_preview">
												<div class="col_left col">
													<a href="<?=SITE_TEMPLATE_PATH?>/img/userpage/preview.jpg" class="fancybox"><img src="<?=SITE_TEMPLATE_PATH?>/img/userpage/preview_thumb.jpg" alt="" /></a>

													<span class="icon icon-linear_arr_down"></span>
													<span class="icon icon-linear_arr_right"></span>
												</div>

												<div class="col_right col">Ваш готовый пост с приглашением</div>
											</div>

											<div class="userpage_bonuses_element_card post_share">
												<div class="col_left col">
													<ul class="share_list">
														<?/*
														<li>
															<a href="#" target="_blank" class="icon icon-fb"></a>
														</li>
														<li>
															<a href="#" target="_blank" class="icon icon-vk"></a>
														</li>
														<li>
															<a href="#" target="_blank" class="icon icon-lj"></a>
														</li>
														<li>
															<a href="#" target="_blank" class="icon icon-tw"></a>
														</li>*/?>
														<?
														$shareName = "Запишись на бесплатный вебинар прямо сейчас";
														$shareUrl = "study.fin-plan.org/?utm_personal=".$personal["NAME"];
														$image_src_for_social = "/upload/medialibrary/da4/da47c45f722e4b246d5357c429d51616.jpg";
														$lj_event = htmlentities('<a href="http://'.$shareUrl.'">'.$shareName.'</a><img src="http://'.$_SERVER['HTTP_HOST'].$image_src_for_social.'">');
														?>
														<li>
															<a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$shareUrl?>&amp;title=<?=$shareName?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$shareUrl?>&amp;title=<?=$shareName?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
														</li>
														<li>
															<a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$shareUrl?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$shareUrl?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
														</li>
														<li>
															<a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$shareName?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$shareName?>"></a>
														</li>

														<li><a href="https://twitter.com/share?text=<?=$shareName?>" onclick="window.open('https://twitter.com/share?text=<?=$shareName?>&url=http://<?=$_SERVER["HTTP_HOST"]?><?=$_SERVER["REQUEST_URI"]?>','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a></li>

													</ul>

													<span class="icon icon-linear_arr_down"></span>
													<span class="icon icon-linear_arr_right"></span>
												</div>

												<div class="col_right col">Разместите пост со ссылкой у себя в соц. сетях</div>
											</div>

											<div class="userpage_bonuses_element_card percents">
												<div class="col_left col">
													<p class="percents_first_line">от <span>10%</span></p>
													<p class="percents_second_line">к вашим бонусам</p>
												</div>

												<div class="col_right col">Получайте от 10% от стоимости покупки друзей!</div>
											</div>

											<a class="userpage_bonuses_element_more" href="#" target="_blank">Подробнее</a>
										</div>
									</div>
								</div>
							<?endif;?>

								<?//if($personal):?>
									<div class="userpage_bonuses_element custom_collapse_elem">
										<div class="userpage_bonuses_element_head custom_collapse_elem_head">
											<div class="col_left col">
												<span class="number collapse_btn"><?=$itemNumber?></span>
											</div>

											<div class="col_right col">
												<p class="collapse_btn">Получайте от 10%, если у Вас есть друзья, которым интересны наши продукты и услуги и они уже готовы их приобрести.</p>

												<span class="icon icon-arr_down collapse_btn"></span>
											</div>
										</div>

										<div class="userpage_bonuses_element_body custom_collapse_elem_body">
											<div class="userpage_bonuses_element_body_inner">
												<div class="userpage_bonuses_element_card card_copy">
													<div class="col_left col">
														<p class="promocode_copy_link tooltip_btn" title="<?=$personal["NAME"]?>"><?=$personal["NAME"]?></p>

														<span class="promocode_btn button button_transparent buffer_copy_btn" data-clipboard-text="<?=$personal["NAME"]?>">Копировать</span>

														<p class="card_copy_sys">в буфер</p>

														<span class="icon icon-linear_arr_down"></span>
														<span class="icon icon-linear_arr_right"></span>
													</div>

													<div class="col_right col">Ваш промокод для друзей</div>
												</div>

												<div class="userpage_bonuses_element_card send_mail">
													<div class="col_left col">
														<p class="card_copy_link tooltip_btn">Узнайте как лучше предложить друзьям промокод</p>
                                 			<a class="button button_transparent" href="/upload/personal/Способы продвижения.pdf" target="_blank">Инструкция</a>

														<span class="icon icon-linear_arr_down"></span>
														<span class="icon icon-linear_arr_right"></span>
													</div>

													<div class="col_right col">Поделитесь своим промокодом с друзьями</div>
												</div>

												<div class="userpage_bonuses_element_card percents">
													<div class="col_left col">
														<p class="percents_first_line">от <span>10%</span></p>
														<p class="percents_second_line">к вашим бонусам</p>
													</div>

													<div class="col_right col">Получайте от 10% от стоимости покупки друзей!</div>
												</div>

												<a class="userpage_bonuses_element_more" href="#" target="_blank">Подробнее</a>
											</div>
										</div>
									</div>
									<?$itemNumber++?>
								<?//endif?>

								<div class="userpage_bonuses_element custom_collapse_elem">
									<div class="userpage_bonuses_element_head custom_collapse_elem_head">
										<div class="col_left col">
											<span class="number collapse_btn"><?=$itemNumber?></span>
										</div>

										<div class="col_right col">
											<p class="collapse_btn">Также баллы Вы можете получать за другие социальные активности:</p>

											<span class="icon icon-arr_down collapse_btn"></span>
										</div>
									</div>

									<div class="userpage_bonuses_element_body custom_collapse_elem_body">
										<div class="userpage_bonuses_element_body_inner">

<!--											<div class="userpage_bonuses_element_card long_card">
												<div class="col_left col col_green">
													<p class="userpage_bonuses_element_card_title">Написать отзыв и&nbsp;получить 50&nbsp;баллов</p>

													<span class="button" data-toggle="modal" data-target="#popup_add_review">Написать</span>
												</div>
											</div>-->

											<div class="userpage_bonuses_element_card long_card">
												<div class="col_left col col_blue">
													<p class="userpage_bonuses_element_card_title">Получайте баллы за написание текстов для Fin-plan</p>
													<p class="userpage_bonuses_element_card_description">(Получайте от 250 баллов за небольшие текстовые заметки о компаниях до 10000 баллов за статьи).</p>
													<span class="button" data-toggle="modal" data-target="#popup_add_text_query">Начать</span>
												</div>
											</div>

											<div class="userpage_bonuses_element_card long_card">
												<div class="col_left col col_black">
													<p class="userpage_bonuses_element_card_title">Участие в&nbsp;конкурсах и&nbsp;других активностях Fin&#8209;plan</p>

													<p class="userpage_bonuses_element_card_description">(следите за конкурсами в рассылке и&nbsp;в&nbsp;наших соц.&nbsp;сетях).</p>

													<div class="userpage_bonuses_card_social_tgl">
														<span class="button userpage_bonuses_card_social_tgl_btn">Подписаться</span>

														<ul class="share_list">
															<li>
																<a href="https://www.facebook.com/Fin-planorg-1588477478078872/" target="_blank" class="icon icon-fb"></a>
															</li>
															<li>
																<a href="https://vk.com/fin_plan_org" target="_blank" class="icon icon-vk"></a>
															</li>
															<li>
																<a href="https://t.me/finplanorg" target="_blank" class="icon icon-telegram"></a>
															</li>
                                                            <li>
																<a href="https://www.instagram.com/invest_finplan/" target="_blank" class="icon icon-inst"></a>
															</li>
                                                            <li>
																<a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?$itemNumber++?>
								</div>
						  <? //Генератор ссылок ?>
								<div class="userpage_bonuses_element custom_collapse_elem">
									<div class="userpage_bonuses_element_head custom_collapse_elem_head">
										<div class="col_left col">
											<span class="number collapse_btn"><?=$itemNumber?></span>
										</div>

										<div class="col_right col">
											<p class="collapse_btn">Генератор партнерских ссылок:</p>

											<span class="icon icon-arr_down collapse_btn"></span>
										</div>
									</div>

									<div class="userpage_bonuses_element_body custom_collapse_elem_body">
											<div class="userpage_bonuses_element_body_inner">
												<div class="userpage_bonuses_element_card card_copy">
													<div class="col_left col">
													  <div class="form_element">
															<input type="text" id="user_link" value="" data-code="<?=$personal["NAME"]?>">
													  </div>


														<span class="icon icon-linear_arr_down"></span>
														<span class="icon icon-linear_arr_right"></span>
													</div>
													<div class="col_right col">Вставьте ссылку на страницу сайта</div>
												</div>

												<div class="userpage_bonuses_element_card card_copy">
													<div class="col_left col">
														<p class="promocode_copy_gen_link tooltip_btn white" title=""></p>

														<span  class="personal_link_btn promocode_gen_copy_button button button_transparent buffer_copy_btn" data-clipboard-text="">Копировать</span>

														<? //<p class="card_copy_sys">в буфер</p> ?>

													</div>

													<div class="col_right col">Ваша ссылка для друзей</div>
												</div>

											</div>
									</div>
								</div>

							</div>
						</div>
					<?endif //Включить бонусную программу?>