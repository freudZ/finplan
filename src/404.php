<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Ошибка 404: Не найдено");

?>
    <div class="wrapper">
        <div class="container">
            <div class="main_content">
                <!-- *****/КАРКАС***** -->
            </div>
        </div>

        <div class="page_404 error_page_outer">
            <div class="container">
                <div class="error_page_inner">
                    <div class="error_page_round"></div>

                    <div class="error_page_text">
                        <p class="t100 lh0_8 medium white m0">Упс! 404!</p>
                        <p class="t60 light white mb30">Нет такой страницы</p>
                        <p class="t22 white">Вы можете сообщить нам об этой ошибке, написав на <a class="inherit no_decoration" href="mailto:koshin@fin-plan.org">koshin@fin-plan.org</a> <br>
						Также любые вопросы Вы можете задать нам в онлайн-чате. <br>Чат находится внизу справа и работает с 9 до 23-00 по мск.</p>
                        <p class="t18 light yellow">Вернуться на <a class="inherit" href="/">главную страницу</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="main_content">
                <!-- *****КАРКАС***** -->
            </div>
        </div>
    </div>
<?/*
<div class="content-main content-main-post-item col-md-8 no-float">
	<div class="content-main-banner" style="background: url('/upload/medialibrary/1a7/1a75098f54eddbfba3a0500946c99b7f.png') -15px 0px no-repeat; background-color: #e8eef0; background-position: right center;">
		<div class="content-main-banner-wrapper">
			<div class="content-main-banner-text">
				УЗНАЙ КАК ЗАРАБАТЫВАТЬ ДО 30% НА БЕЗРИСКОВЫХ ИНВЕСТИЦИЯХ 
			</div>
			<div class="content-main-banner-start">
				<a target="_blank" rel="nofollow" href="http://study.fin-plan.org/?utm_source=site&utm_medium=blog&utm_campaign=top_banner">Записаться!</a>
			</div>
		</div>
	</div>
	<div class="content-main-wrapper">
		<div class="content-main-post-item-container">
		    <h1 class="h1_404_page">404</h1>
		    <h2 class="h2_404_page">Запрашиваемая страница не найдена, извините</h2>
		    <p class="p_m_italic">Неправильно набран адрес, или такой страницы на сайте больше не существует.</p>
		</div>
	</div>
*/?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>