<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<? 	$title = "Технический анализ";
	$APPLICATION->SetTitle($title);
	$APPLICATION->SetPageProperty("title", $title); ?>
<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner">
				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
				<div class="title_container title_nav_outer">
					<h1><? $APPLICATION->ShowTitle(false); ?></h1>
				</div>
                <?require("./app/index.html");?>
            </div>
        </div>

		<div class="sidebar">
			<div class="sidebar_inner">
                <form class="innerpage_search_form" method="GET">
                    <div class="form_element">
                        <input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Название / тикер / ISIN" name="q" id="autocomplete_all" />
                        <button type="submit"><span class="icon icon-search"></span></button>
                    </div>
                </form>
            <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/right_sidebar_services.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?>
				<hr/>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "random_review_right", array(
					"IBLOCK_TYPE" => "FINPLAN",
					"IBLOCK_ID" => "25",
					"NEWS_COUNT" => "1",
					"SORT_BY1" => "RAND",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => ""
				),
					false
				);?>
			</div>
		</div>

    </div>
	   <div class="load_overlay">
			<p><span class="radar_animate"><span class="radar_round_inner"></span><span class="radar_round_track"></span><span class="radar_animate_element blue"></span><span class="radar_animate_element green"></span><span class="radar_animate_element orange"></span></span> <span class="radar_txt">Загрузка...</span></p>
		</div>
</div>
<?include("popups.php")?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>