<div class="modal modal_black fade" id="popup_refill" tabindex="-1" data-backdrop="static">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Укажите размер ежемесячного пополнения счёта</p>
		<button type="button" class="close"></button>
	  </div>
	  <div class="modal-body">
		<div class="form_element">
			<input type="text" id="refill_value" placeholder="Размер ежемесячного пополнения"/>
		</div>

			<br/>
		<div class="form_element text-center">
		  <span class="popup_refill_submit button" data-dismiss="modal">Подтвердить</span>
		</div>
	  </div>
	</div>
  </div>
</div>

<div class="modal modal_black fade" id="popup_money_error" tabindex="-1" data-backdrop="static">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Вы превысили значение суммы инвестиций!</p>
	  </div>
			<br/>
	  <div class="modal-body">
		<div class="form_element text-center">
		  <span class="popup_refill_submit button" data-dismiss="modal">ОК</span>
		  <br/>
		  <span class="popup_refill_change_summ" data-dismiss="modal">Изменить сумму инвестиций</span>
		</div>
	  </div>
	</div>
  </div>
</div>

<div class="modal modal_black fade" id="popup_investment_amount" tabindex="-1" data-backdrop="static">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Укажите сумму инвестирования</p>
	  </div>
	  <div class="modal-body">
		<p class="popup_calculate_description">Это сумма, в пределах которой вы будете составлять инвест-портфель. Сумму можно будет изменить в процессе при необходимости.</p>
		<p class="popup_calculate_origin">Минимальная сумма: <span></span></p>

		<div class="form_element text-center">
			<input id="popup_investment_summ" class="smart_txt_field" type="text" value="400 000" data-postfix=" руб." />
		</div>

		<div class="form_element text-center">
		  <span class="popup_refill_submit button">Подтвердить</span>
		</div>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="obligation_detail_popup">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<div class="modal-body_title"></div>
		<div class="modal-body_inner">
			Загрузка
		</div>
	  </div>
	  <div class="modal-footer">
		<p class="subtitle">Переходите на отдельную страницу <span class="obligation_detail_popup_type" data-obligations="облигации" data-shares="акции"></span></p>
		<p>Читайте комментарии, задавайте вопросы и оставляйте свое мнение по этой и другим ценным бумагам.</p>
		<p><a class="obligation_detail_more_btn button" href="#" target="_blank">Открыть</a></p>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="company_detail_popup">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<div class="modal-body_title"></div>
		<div class="modal-body_inner">
			Загрузка
		</div>
	  </div>
	  <div class="modal-footer">
		<p class="subtitle">Переходите на отдельную страницу компании</p>
		<p>Читайте комментарии, задавайте вопросы и оставляйте свое мнение по этой и другим компаниям</p>
		<p><a class="company_detail_more_btn button" href="#" target="_blank">Открыть</a></p>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="legal_popup" tabindex="-1">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <div class="modal-header">
      <p class="strong">Условия использования информации с сайта Fin-plan.org</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<div class="modal-body_title"></div>
		<div class="modal-body_inner">
      <p>Вся информация, размещенная на данном интернет-сайте, дизайн, подбор, группировка и расположение материалов являются интеллектуальной собственностью ИП Кошин В.В. и защищены российским законодательством об авторском праве и средствах массовой информации, а также международными договорами РФ по защите интеллектуальной собственности.</p>
      <p>Информация о торгах на рынке группы «московская биржа» предоставлена ПАО Московская биржа. Источником и владельцем биржевой информации является московская биржа.</p>
      <p>Без письменного согласия биржи нельзя осуществлять дальнейшее распространение и предоставление биржевой информации в любом виде и любыми средствами, включая электронные, механические, фотокопировальные, записывающие или другие (в том числе с использованием удаленного мобильного (беспроводного) доступа), её трансляцию, в том числе средствами теле- и радиовещания, её демонстрацию на интернет-сайтах, её использование в игровых, тренажерных и иных системах, предусматривающих демонстрацию и/или передачу биржевой информации, а также для расчёта производной информации (в том числе индексов и индикаторов), предназначенной для дальнейшего публичного распространения.</p>
      <p>ИП Кошин В.В. является дистрибьютором информации об итогах торгов на рынках группы московская биржа.</p>
      <p>Материалы данного интернет-сайта предназначены исключительно для персонального и некоммерческого использования. Запрещается использование автоматизированного извлечения информации данного интернет-сайта без письменного разрешения ИП Кошин В.В. Любое копирование, перепечатка и/или последующее распространение информации, представленной на данном сайте, или информации, полученной на основе этой информации в результате переработки, в том числе производимое путем кэширования, кадрирования или использованием аналогичных средств, строго запрещается без предварительного письменного разрешения ИП Кошин В.В. За нарушение настоящего правила наступает ответственность, предусмотренная законодательством и международными договорами РФ.</p>
      <p>ИП Кошин В.В. не несет ответственность за любую потерю или ущерб (потеря деловых доходов, потеря прибыли или любой прямой, косвенный, последующий, специальный или подобный ущерб вообще), являющийся результатом:</p>
      <ul>
        <li>любой погрешности (или неполноты в, или задержки, прерывания, ошибки или упущения в поставке) в информации;</li>
        <li>любого сбоя информационной системы;</li>
        <li>любого решения или действия, сделанного в уверенности относительно информации.</li>
      </ul>
      <p>ИП Кошин В.В. не несет ответственность за ущерб, причиненный вашему компьютеру, программному обеспечению, модему, телефону и другой собственности, который стал результатом использования информации, программного обеспечения или услуг.</p>
      <p>С уважением, компания Fin-plan.</p>
		</div>
	  </div>
	</div>
  </div>
</div>
