<div class="content-main-wrapper">
 <nav id="scrollsections-navigation"> <section id="home" class="scrollsections">
	<h1 class="content-main-h1 content-main-h1-first pull-left">Web-приложение для управления личными финансами</h1>
	<div class="content-main-counter pull-right">
		<div class="content-main-counter-text">
			 Нас уже выбрали
		</div>
		<div class="content-main-counter-count">
			 <span>3927</span>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<p>
		 Здесь создается сервис, который поможет вам самостоятельно создать личный финансовый план.
	</p>
	<div class="content-main-p-first">
		<div class="content-main-p-first-left">
 <img src="/bitrix/templates/finplan_copy/images/main-present.png">
		</div>
		<div class="content-main-p-first-right">
			 ВСЕМ УЧАСТНИКАМ ТЕСТИРОВАНИЯ ПЕРВЫЙ ГОД ИСПОЛЬЗОВАНИЯ ПРОГРАММЫ АБСОЛЮТНО БЕСПЛАТНО!
		</div>
	</div>
	<p>
		 Beta-версия приложения запускается с 15 марта 2017. Оставьте заявку на тестирование прямо сейчас!
	</p>
	<form class="content-main-subscribe-form" method="POST" action="https://cp.unisender.com/ru/subscribe?hash=5mxjwg3ha96qa1tsh6gx4mxumsmq79y4do97317e" name="subscribtion_form" onsubmit="return us_.onSubmit(this);" us_mode="embed" target="_blank">
		<div class="content-main-button-start">
 <button class="content-main-subscribe-button" data-toggle="modal" data-target="#loginModal" href="javascript:;" target="_blank">Подписаться</button>
		</div>
 <input name="charset" value="UTF-8" type="hidden"> <input name="default_list_id" value="5370914" type="hidden"> <input name="overwrite" value="2" type="hidden"> <input name="is_v5" value="1" type="hidden">
	</form>
	<div class="content-main-button-section pull-right">
 <a href="#about"><span class="glyphicon glyphicon-circle-arrow-down"></span></a>
	</div>
 </section> <section id="about" class="scrollsections">
	<h2 class="content-main-h1">Почему мы?</h2>
	<div class="content-main-whyus-item">
		<div class="content-main-whyus-item-left">
			<div>
 <img src="/bitrix/templates/finplan_copy/images/main-plane.png">
			</div>
		</div>
		<div class="content-main-whyus-item-right">
			<div class="content-main-whyus-item-right-title">
				 С НАМИ ЛЕГКО!
			</div>
			<div class="content-main-whyus-item-right-text">
				 Простой интерфейс поможет составить первый финансовый план без специальных знаний. Обучение встроено в процесс.
			</div>
		</div>
	</div>
	<div class="content-main-whyus-item">
		<div class="content-main-whyus-item-left">
			<div>
 <img src="/bitrix/templates/finplan_copy/images/main-line.png">
			</div>
		</div>
		<div class="content-main-whyus-item-right">
			<div class="content-main-whyus-item-right-title">
				 МЫ СДЕЛАЛИ СЛОЖНОЕ ПРОСТЫМ
			</div>
			<div class="content-main-whyus-item-right-text">
				 Мы проанализировали тысячи финансовых инструментов. Отсортировали и разложили по полочкам. Вам осталось выбрать то, что подходит именно Вам.
			</div>
		</div>
	</div>
	<div class="content-main-whyus-item">
		<div class="content-main-whyus-item-left">
			<div>
 <img src="/bitrix/templates/finplan_copy/images/main-bag.png">
			</div>
		</div>
		<div class="content-main-whyus-item-right">
			<div class="content-main-whyus-item-right-title">
				 ВСЕ В ОДНОМ МЕСТЕ
			</div>
			<div class="content-main-whyus-item-right-text">
				 Личный кабинет Fin-plan.org объединяет сервис для составления финансового плана с модулем учета доходов и расходов в одном web-приложении.
			</div>
		</div>
	</div>
	<form class="content-main-subscribe-form" method="POST" action="https://cp.unisender.com/ru/subscribe?hash=5mxjwg3ha96qa1tsh6gx4mxumsmq79y4do97317e" name="subscribtion_form" onsubmit="return us_.onSubmit(this);" us_mode="embed" target="_blank">
		<div class="content-main-button-start">
  <button class="content-main-subscribe-button" data-toggle="modal" data-target="#loginModal" href="javascript:;" target="_blank">Подписаться</button>
		</div>
 <input name="charset" value="UTF-8" type="hidden"> <input name="default_list_id" value="5370914" type="hidden"> <input name="overwrite" value="2" type="hidden"> <input name="is_v5" value="1" type="hidden">
	</form>
	<div class="content-main-button-section pull-right">
 <a href="#goals"><span class="glyphicon glyphicon-circle-arrow-down"></span></a>
	</div>
 </section> <section id="goals" class="scrollsections">
	<h2 class="content-main-h1">Ставьте финансовые цели<br>
	 и достигайте их</h2>
	<p>
 <img src="/bitrix/templates/finplan_copy/images/main-goals.png">
	</p>
	<p>
		 Ставьте финансовые цели. Оцените свое финансовое положение и реалистичность достижения финансовых целей. Планируйте этапы на пути к реализации своей мечты. Составьте финансовый план и бюджет и начинайте тренировать финансовую дисциплину. Спустя определенное время вы достигните своих целей. Делитесь своими достижениями с друзьями и двигайтесь к новым вершинам! Мечты стали ближе!
	</p>
	<form class="content-main-subscribe-form" method="POST" action="https://cp.unisender.com/ru/subscribe?hash=5mxjwg3ha96qa1tsh6gx4mxumsmq79y4do97317e" name="subscribtion_form" onsubmit="return us_.onSubmit(this);" us_mode="embed" target="_blank">
		<div class="content-main-button-start">
 <button class="content-main-subscribe-button" data-toggle="modal" data-target="#loginModal" href="javascript:;" target="_blank">Подписаться</button>
		</div>
 <input name="charset" value="UTF-8" type="hidden"> <input name="default_list_id" value="5370914" type="hidden"> <input name="overwrite" value="2" type="hidden"> <input name="is_v5" value="1" type="hidden">
	</form>
	<div class="content-main-button-section pull-right">
 <a href="#budget"><span class="glyphicon glyphicon-circle-arrow-down"></span></a>
	</div>
 </section> <section id="budget" class="scrollsections">
	<h2 class="content-main-h1">Управляйте семейным бюджетом эффективно</h2>
	<p>
 <img src="/bitrix/templates/finplan_copy/images/main-budget.png">
	</p>
	<p>
		 Составьте свой семейный финансовый план. Подкрепляйте его ежемесячным бюджетом и достигайте своих финансовых целей. Найдите дыру в своих расходах, начните ими управлять с помощью удобного бюджета, начинайте откладывать. Инвестируйте свои сбережения в надежные активы в соответствии с рекомендациями и получайте стабильный пассивный доход. Управлять семейным бюджетом теперь легко.
	</p>
	<form class="content-main-subscribe-form" method="POST" action="https://cp.unisender.com/ru/subscribe?hash=5mxjwg3ha96qa1tsh6gx4mxumsmq79y4do97317e" name="subscribtion_form" onsubmit="return us_.onSubmit(this);" us_mode="embed" target="_blank">
 		<div class="content-main-button-start">
 <button class="content-main-subscribe-button" data-toggle="modal" data-target="#loginModal" href="javascript:;" target="_blank">Подписаться</button>
		</div>
 <input name="charset" value="UTF-8" type="hidden"> <input name="default_list_id" value="5370914" type="hidden"> <input name="overwrite" value="2" type="hidden"> <input name="is_v5" value="1" type="hidden">
	</form>
	<div class="content-main-button-section pull-right">
 <a href="#investment"><span class="glyphicon glyphicon-circle-arrow-down"></span></a>
	</div>
 </section> <section id="investment" class="scrollsections">
	<h2 class="content-main-h1">Управляйте своим инвестиционным портфелем</h2>
	<p>
 <img src="/bitrix/templates/finplan_copy/images/main-invest.png">
	</p>
	<p>
		 Управляйте депозитами, пенсионными счетами, ценными бумагами, дебетовыми картами, и другими активами из одного приложения. Прогнозируйте доходность, сравнивайте активы, получайте рекомендации и ведите учет фактических операций. Составив инвестиционный план переходите на сайты наших партнеров и совершайте операции онлайн.
	</p>
	<form class="content-main-subscribe-form" method="POST" action="https://cp.unisender.com/ru/subscribe?hash=5mxjwg3ha96qa1tsh6gx4mxumsmq79y4do97317e" name="subscribtion_form" onsubmit="return us_.onSubmit(this);" us_mode="embed" target="_blank">

		<div class="content-main-button-start">
<button class="content-main-subscribe-button" data-toggle="modal" data-target="#loginModal" href="javascript:;" target="_blank">Подписаться</button>
		</div>
 <input name="charset" value="UTF-8" type="hidden"> <input name="default_list_id" value="5370914" type="hidden"> <input name="overwrite" value="2" type="hidden"> <input name="is_v5" value="1" type="hidden">
	</form>
	<div class="content-main-button-section pull-right">
 <a href="#pension"><span class="glyphicon glyphicon-circle-arrow-down"></span></a>
	</div>
 </section> <section id="pension" class="scrollsections">
	<h2 class="content-main-h1">Планируйте далеко вперед</h2>
	<p>
 <img src="/bitrix/templates/finplan_copy/images/main-pension.png">
	</p>
	<p>
		 Считайте сами, что выгоднее: переводить в НПФ или оставить пенсию в государственном фонде. Сравнивайте НПФ между собой и выбирайте самый выгодный и надежный. Просто задайте исходные параметры и оценивайте свою пенсию через 30 лет. Хотите копить на пенсию сами — отлично!
	</p>
	<form class="content-main-subscribe-form" method="POST" action="https://cp.unisender.com/ru/subscribe?hash=5mxjwg3ha96qa1tsh6gx4mxumsmq79y4do97317e" name="subscribtion_form" onsubmit="return us_.onSubmit(this);" us_mode="embed" target="_blank">

		<div class="content-main-button-start">
<button class="content-main-subscribe-button" data-toggle="modal" data-target="#loginModal" href="javascript:;" target="_blank">Подписаться</button>
		</div>
 <input name="charset" value="UTF-8" type="hidden"> <input name="default_list_id" value="5370914" type="hidden"> <input name="overwrite" value="2" type="hidden"> <input name="is_v5" value="1" type="hidden">
	</form>
	 <!--<div class="content-main-button-start">
				<a href="#">Начать</a>
			</div>-->
	<div class="content-main-button-section pull-right">
 <a href="#blog"><span class="glyphicon glyphicon-circle-arrow-down glyphicon-2x"></span></a>
	</div>
 </section> 




<section id="blog" class="scrollsections">
	<h2 class="content-main-h1">Лучшие статьи по управлению личным капиталом</h2>
	<p>
 <img src="/bitrix/templates/finplan_copy/images/main-blog.png">
	</p>
	<p>
		В нашем блоге в найдете ответы на все вопросы по финансовому планированию и инвестициям. Последние статьи в блоге: 
    <?
    $arrFilter = Array("!SECTION_ID" => 17);
    $APPLICATION->IncludeComponent("bitrix:news.list", "blog_list_short", array(
      "IBLOCK_TYPE" => "FINPLAN",
      "IBLOCK_ID" => "5",
      "NEWS_COUNT" => "3",
      "SORT_BY1" => "PROPERTY_ATTACH_POST",
      "SORT_ORDER1" => "DESC",
      "SORT_BY2" => "ACTIVE_FROM",
      "SORT_ORDER2" => "DESC",
      "FILTER_NAME" => "arrFilter",
      "FIELD_CODE" => array(
        0 => "",
        1 => "",
      ),
      "PROPERTY_CODE" => array(
        0 => "DOUBLE_POST",
        1 => "ATTACH_POST",
      ),
      "CHECK_DATES" => "Y",
      "DETAIL_URL" => "",
      "AJAX_MODE" => "N",
      "AJAX_OPTION_JUMP" => "N",
      "AJAX_OPTION_STYLE" => "Y",
      "AJAX_OPTION_HISTORY" => "N",
      "CACHE_TYPE" => "A",
      "CACHE_TIME" => "36000000",
      "CACHE_FILTER" => "N",
      "CACHE_GROUPS" => "Y",
      "PREVIEW_TRUNCATE_LEN" => "",
      "ACTIVE_DATE_FORMAT" => "",
      "SET_STATUS_404" => "N",
      "SET_TITLE" => "Y",
      "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
      "ADD_SECTIONS_CHAIN" => "Y",
      "HIDE_LINK_WHEN_NO_DETAIL" => "N",
      "PARENT_SECTION" => "",
      "PARENT_SECTION_CODE" => "",
      "INCLUDE_SUBSECTIONS" => "Y",
      "PAGER_TEMPLATE" => ".default",
      "DISPLAY_TOP_PAGER" => "N",
      "DISPLAY_BOTTOM_PAGER" => "Y",
      "PAGER_TITLE" => "Новости",
      "PAGER_SHOW_ALWAYS" => "Y",
      "PAGER_DESC_NUMBERING" => "N",
      "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
      "PAGER_SHOW_ALL" => "Y",
      "AJAX_OPTION_ADDITIONAL" => ""
      ),
      false
    );?>
	</p>
	 <!--<div class="content-main-button-start">
				<a href="#">Начать</a>
			</div>-->
	<div class="content-main-button-section pull-right">
 <a href="#home"><span class="glyphicon glyphicon-circle-arrow-up glyphicon-2x"></span></a>
	</div>
 </section></nav>
</div>
<script type='text/javascript'>var dateFormat="dd.mm.yyyy",us_msg={missing:'Не задано обязательное поле: "%s"',invalid:'Недопустимое значение поля: "%s"',email_or_phone:"Не задан ни email, ни телефон",email_absent:"Не указан электронный адрес",phone_absent:"Не указан телефон",no_list_ids:"Не выбрано ни одного списка рассылки"},us_emailRegexp=/^[a-zA-Z0-9_+=-]+[a-zA-Z0-9\._+=-]*@[a-zA-Z0-9][a-zA-Z0-9-]*(\.[a-zA-Z0-9]([a-zA-Z0-9-]*))*\.([a-zA-Z]{2,6})$/,us_phoneRegexp=/^\s*[\d +()-.]{7,32}\s*$/;if(typeof us_=="undefined")var us_=new function(){function h(a){var b=a.getElementsByTagName("input");for(var c=0;c<b.length;c++){var d=b[c];if(d.getAttribute("name")=="charset"){d.value==""&&(d.value=window.characterSet?window.characterSet:window.charset);return}}}function i(a){var b=document,c=b.createElement("div");c.style.position="absolute",c.style.width="auto",c=b.body.appendChild(c),c.appendChild(a),a.style.display="",d.push(c)}function j(){var a=window,b=document,c=a.innerWidth?a.innerWidth:b.body.clientWidth,e=a.innerHeight?a.innerHeight:b.body.clientHeight;for(var f=0;f<d.length;f++){var g=d[f],h=parseInt(g.offsetWidth+""),i=parseInt(g.offsetHeight+"");g.style.left=(c-h)/2+b.body.scrollLeft+f*10,g.style.top=(e-i)/2+b.body.scrollTop+f*10}}var a=!1,b=window.onload;window.onload=function(){us_.onLoad()};var c=null,d=[],e=!1,f=!1,g={};window.addEventListener("loadFormJavascript",function(){us_.onLoad()},!1),this.onLoad=function(){var d,e=document.getElementsByTagName("form"),f=[];for(d=0;d<e.length;d++)f.push(e[d]);for(d=0;d<f.length;d++){var g=f[d],k=g.getAttribute("us_mode");if(!k)continue;k=="popup"&&i(g),h(g)}j(),c=document.onresize,document.onresize=function(){us_.onResize()},a=!0,b&&b();var l=document.getElementsByClassName("formdatepicker");for(var d in l)var m=new Pikaday({field:l[d],format:dateFormat.toUpperCase()})},this.onResize=function(){j()},trim=function(a){return a==null?"":a.replace(/^\s\s*/,"").replace(/\s\s*$/,"")},this.onSubmit=function(b){return a?(_hideErrorMessages(b),!_validateTextInputs(b)||!_validateCheckboxes(b)||!_validateRadios(b)?!1:!0):(alert("us_.onLoad() has not been called"),!1)},_validateTextInputs=function(a){var b=a.querySelectorAll("input[type=text], textarea");if(b.length==0)return!0;for(var c=0;c<b.length;c++){var d=b[c];g[d.name]&&g[d.name].length>0?d.style["border-color"]=g[d.name]:g[d.name]=d.style["border-color"];var h=d.getAttribute("name");h==="email"&&(e=!0),h==="phone"&&(f=!0);var i=trim(d.value),j=i==="",k=d.getAttribute("_required")==="1";if(j){if(k){var l=us_msg.missing.replace("%s",d.getAttribute("_label"));return _showErrorMessage(l,d),d.style["border-color"]="#ff592d",d.focus(),!1}}else{function m(a){var b=us_msg.invalid.replace("%s",a.getAttribute("_label"));_showErrorMessage(b,a),a.style["border-color"]="#ff592d",a.focus()}var n=d.getAttribute("_validator"),o=null;switch(n){case null:case"":case"string":case"number":case"text":break;case"date":o=dateFormat.replace(/dd?/i,"([0-9]{1,2})"),o=o.replace(/mm?/i,"([0-9]{1,2})"),o=o.replace(/yy{1,3}/i,"([0-9]{2,4})"),o=new RegExp(o);var p=o.exec(i);if(!(p&&p[1]&&p[2]&&p[3]))return m(d),!1;var q=parseInt(p[1],10),r=parseInt(p[2],10),s=parseInt(p[3],10),t=new Date(s,r-1,q);if(t.getFullYear()!=s||t.getMonth()+1!=r||t.getDate()!=q)return m(d),!1;break;case"email":o=us_emailRegexp;break;case"phone":o=us_phoneRegexp;break;case"float":o=/^[+\-]?\d+(\.\d+)?$/;break;default:return alert('Internal error: unknown validator "'+n+'"'),d.focus(),!1}if(o&&!o.test(i)&&n!="date")return m(d),!1}}return!0},_validateCheckboxes=function(a){return _validateOptionsList(a,"checkbox")},_validateRadios=function(a){return _validateOptionsList(a,"radio")},_validateOptionsList=function(a,b){function f(a){return!a||a==document?null:a.parentNode.nodeName&&a.parentNode.nodeName.toLowerCase()==="ul"?a.parentNode:f(a.parentNode)}var c=a.querySelectorAll("input[type="+b+"]");if(c.length==0)return!0;var d=new Array,e="";for(var g=0;g<c.length;g++)c[g].getAttribute("_required")==="1"&&(e=c[g].getAttribute("name").replace(/(:|\.|\[|\])/g,"\\$1"),d.indexOf(e)===-1&&d.push(e));for(var h in d){var e=d[h],i=a.querySelectorAll("input[name="+e+"]:checked").length,j=a.querySelectorAll("input[name="+e+"]");if(i===0){var k=f(j[0]),l=us_msg.missing.replace("%s",j[0].getAttribute("_label"));return _showErrorMessage(l,k),!1}}return!0},_showErrorMessage=function(a,b){if(b){var c=b.parentNode.querySelector(".error-block");c.innerHTML=a,c.style.display="block"}},_hideErrorMessages=function(a){var b=a.querySelectorAll(".error-block");if(b.length==0)return;for(var c=0;c<b.length;c++){var d=b[c];d.innerHtml="",d.style.display="none"}}}</script><script type='text/javascript' src='https://cp.unisender.com/v5/template-editor-new/js/lib/moment/moment-with-langs.min.js'></script><script type='text/javascript' src='https://cp.unisender.com/v5/template-editor-new/js/lib/datepicker/pikaday.js'></script>