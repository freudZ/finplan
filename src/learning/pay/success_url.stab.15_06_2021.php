<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
	
include('config.php');
$hash = md5($_REQUEST['action'].';'.$_REQUEST['orderSumAmount'].';'.$_REQUEST['orderSumCurrencyPaycash'].';'.$_REQUEST['orderSumBankPaycash'].';'.$configs['shopId'].';'.$_REQUEST['invoiceId'].';'.$_REQUEST['customerNumber'].';'.$configs['ShopPassword']);

CLogger::success_url(print_r(array("request"=>$_REQUEST, "hash"=>$hash, "md5"=>strtolower($_REQUEST['md5'])), true));

if (strtolower($hash) != strtolower($_REQUEST['md5'])){
	
	$arFilter = Array("IBLOCK_ID"=>11, "ID"=>intval($_REQUEST["orderNumber"]), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
	if($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$props = $ob->GetProperties();
		
		//CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 10, "PAYED");
		
		if($props["CLOSE_ACCESS"]["VALUE"]){
			$hlbl = 11;
			$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch(); 
			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$entity_data_class = $entity->getDataClass(); 
			
			foreach($props["CLOSE_ACCESS"]["VALUE"] as $val){
				
				$main_query = new Entity\Query($entity);
				$main_query->setOrder(array("ID" => "desc"));
				$main_query->setFilter(array("UF_USER"=>$props["USER"]["VALUE"], "UF_SEMINAR"=>$val));
				$exec = $main_query->exec();
				$exec = new CDBResult($exec);
				if(!$exec->Fetch()){
					
					$entity_data_class::add(array(
						"UF_USER"=>$props["USER"]["VALUE"],
						"UF_SEMINAR"=>$val
					));
				}
			}
		}
		
		$res = CIBlockElement::GetByID($props["ITEM"]["VALUE"]);
		if($ar_res = $res->GetNext()){
			LocalRedirect($ar_res["DETAIL_PAGE_URL"]);
		}
	}
} else {
	//Если оплачен
	$arFilter = Array("IBLOCK_ID"=>11, "ID"=>intval($_REQUEST["orderNumber"]), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
	if($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$props = $ob->GetProperties();
		
		CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 10, "PAYED");
		

		if($props["CLOSE_ACCESS"]["VALUE"]){
			$hlbl = 11;
			$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch(); 
			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$entity_data_class = $entity->getDataClass(); 
			
			foreach($props["CLOSE_ACCESS"]["VALUE"] as $val){
				
				$main_query = new Entity\Query($entity);
				$main_query->setOrder(array("ID" => "desc"));
				$main_query->setFilter(array("UF_USER"=>$props["USER"]["VALUE"], "UF_SEMINAR"=>$val));
				$exec = $main_query->exec();
				$exec = new CDBResult($exec);
				if(!$exec->Fetch()){
					
					$entity_data_class::add(array(
						"UF_USER"=>$props["USER"]["VALUE"],
						"UF_SEMINAR"=>$val
					));
				}
			}
		}
		
		$res = CIBlockElement::GetByID($props["ITEM"]["VALUE"]);
		if($ar_res = $res->GetNext()){
			LocalRedirect($ar_res["DETAIL_PAGE_URL"]);
		}
	}
}
?>