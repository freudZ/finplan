<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include('config.php');
CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hash = md5($_POST['action'].';'.$_POST['orderSumAmount'].';'.$_POST['orderSumCurrencyPaycash'].';'.$_POST['orderSumBankPaycash'].';'.$configs['shopId'].';'.$_POST['invoiceId'].';'.$_POST['customerNumber'].';'.$configs['ShopPassword']);
if (strtolower($hash) != strtolower($_POST['md5'])){
	$code = 1;

}
else {
	$code = 0;

	CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>11, "ID"=>intval($_REQUEST["orderNumber"]), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
	if($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$props = $ob->GetProperties();

		CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 10, "PAYED");
		global $CACHE_MANAGER;
		$CACHE_MANAGER->ClearByTag("iblock_id_11");

		if($props["CLOSE_ACCESS"]["VALUE"]){
			$hlbl = 11;
			$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			foreach($props["CLOSE_ACCESS"]["VALUE"] as $val){

				$main_query = new Entity\Query($entity);
				$main_query->setOrder(array("ID" => "desc"));
				$main_query->setFilter(array("UF_USER"=>$props["USER"]["VALUE"], "UF_SEMINAR"=>$val));
				$exec = $main_query->exec();
				$exec = new CDBResult($exec);
				if(!$exec->Fetch()){

					$entity_data_class::add(array(
						"UF_USER"=>$props["USER"]["VALUE"],
						"UF_SEMINAR"=>$val
					));
				}
			}
		}

		//Промокоды
		PromoGenerator::afterBuyAction($props["USER"]["VALUE"], $props["PROMOCODE"]["VALUE"], $_POST['orderSumAmount'], $props["BONUSES_SUMM"]["VALUE"], $props["PAY_NAME"]["VALUE"]);

		//После покупки
		BaseHelper::afterBuyAction($item["ID"]);

		//если куплен радар
		if($props["RADAR_END"]["VALUE"]){
			$user = new CUser;
			$fields = Array(
				"UF_RADAR_ACCESS" => true,
			);
			$userUpdateResult = $user->Update($props["USER"]["VALUE"], $fields);
			//$checkCachedPayRadar = checkPayRadar(true, $props["USER"]["VALUE"]); //Перегенерация кеша проверки доступности радара для конкретного пользователя

			//Сбрасываем кеш проверки доступности радара для покупателя
			cleanCheckRadarsCache($props["USER"]["VALUE"]);

			CLogger::Pay_avoso_url("userUpdateResult: ".($userUpdateResult?'Y':'N')." props: ".print_r($props,true)." item: ".print_r($item,true)." checkPayRadar: ".($checkCachedPayRadar?'Y':'N'));
		}

		//подписываем в клиенты
        require_once($_SERVER["DOCUMENT_ROOT"]."/api/mailchimp/MailChimp.php");

		$rsUser = CUser::GetByID($props["USER"]["VALUE"]);
		$arUser = $rsUser->Fetch();

		$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');
		$list_id = "b6050240b6";
		$result = $MailChimp->get("search-members", array(
			"query" => $arUser["LOGIN"],
			"list_id" => $list_id
		));

		if ($MailChimp->success()) {
			if($result["exact_matches"]["members"]){
				foreach($result["exact_matches"]["members"] as $member){
					$MailChimp->delete("lists/".$list_id."/members/".$member["id"]);
				}
			}
		}


		//добавляем
		$tmp = [
			'email_address' => $arUser["LOGIN"],
			'status'        => 'subscribed',
			"merge_fields" => [
				"EMAIL"=> $arUser["LOGIN"],
				"FNAME"=> $arUser["NAME"],
				"LNAME"=> "",
			],
		];
		$list_id = "8733ecb52b";
		$result = $MailChimp->post("lists/$list_id/members", $tmp);
	}
}
	print '<?xml version="1.0" encoding="UTF-8"?>';
	print '<paymentAvisoResponse performedDatetime="'. $_POST['requestDatetime'] .'" code="'.$code.'" invoiceId="'. $_POST['invoiceId'] .'" shopId="'. $configs['shopId'] .'"/>';
?>