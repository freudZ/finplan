<?define("HIDE_LEFT_BLOCK", "Y");
define("SHOW_NEW_FOOTER", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Семинары по инвестициям, обучение инвестициям, Купить семинары по инвестициям, вебинары по инвестициям, семинары по инвестициям на фондовом рынке");
$APPLICATION->SetPageProperty("description", "Компания FIn-plan представляет уникальный цикл семинаров по инвестициям. За 3 недели обучения мы помогаем Вам выйти на профессиональный уровень инвестиций и зарабатывать 30% годовых с минимальными рисками и затратами времени.");
$APPLICATION->SetTitle("Семинары по инвестициям");

function ShowSection($id, $variant, $nopaddingbottom=false){
	CModule::IncludeModule("iblock");

	$arFilter = Array("IBLOCK_ID"=>20,"SECTION_ID"=>$id, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
	if(!$res->SelectedRowsCount()){
		return "";
	}

	$res2 = CIBlockSection::GetByID($id);
	$arSect = $res2->GetNext();

	?>
	<div class="services_<?if($variant==2):?>gray_<?endif?>container"<?if($nopaddingbottom):?> style="padding-bottom:0"<?endif?>>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<p class="subtitle"><?=$arSect["NAME"]?></p>
				</div>
			</div>
			<div class="row">
				<?
				while($ob = $res->GetNextElement())
				{
					$arFields = $ob->GetFields();
					$props = $ob->GetProperties();
					$i++;?>

					<?if($variant==1):?>
						<div class="col col-xs-6 col-lg-5<?if(is_float($i/2)):?> col-lg-offset-1<?endif?>">
							<div class="services_element">
								<div class="services_element_head">
									<?if($props["TOP_NAME"]["VALUE"]):?>
										<div class="services_element_head_inner"><?=$props["TOP_NAME"]["VALUE"]?></div>
									<?endif?>
								</div>

								<div class="services_element_img">
									<?if($arFields["CODE"]):?>
										<a href="#" data-toggle="modal" data-target="#<?=$arFields["CODE"]?>">
									<?else:?>
										<a href="<?=$props["LINK"]["VALUE"]?>">
									<?endif?>
										<span class="services_element_img_inner" style="background-image: url(<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>);">
										</span>
									</a>
									<?if($props["IMG_TEXT"]["VALUE"]):?>
										<div class="services_element_date"><?=$props["IMG_TEXT"]["VALUE"]?></div>
									<?endif?>
								</div>

								<div class="services_element_title_block">
									<div class="services_element_title_block_inner"><p class="services_element_title"><?=$arFields["NAME"]?></p></div>
								</div>

								<div class="services_element_body">
									<div class="services_element_body_inner">
										<p><?=$arFields["PREVIEW_TEXT"]?></p>

										<?if($arFields["CODE"]):?>
											<a class="button button_old" href="#" data-toggle="modal" data-target="#<?=$arFields["CODE"]?>"><?=$props["BTN"]["VALUE"]?></a>
										<?else:?>
											<a class="button button_old" href="<?=$props["LINK"]["VALUE"]?>"><?=$props["BTN"]["VALUE"]?></a>
										<?endif?>
									</div>
								</div>
							</div>
						</div>
					<?else:?>
						<div class="col-sm-12 col-lg-10 col-lg-offset-1">
							<div class="services_element"><p class="services_element_title"><?=$arFields["NAME"]?></p>
								<div class="services_element_img">
									<a href="<?=$props["LINK"]["VALUE"]?>">
										<span class="services_element_img_inner" style="background-image: url(<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>);">
										</span>
									</a>
								</div>

								<div class="services_element_body">
									<p class="services_element_title"><?=$arFields["NAME"]?></p>
									<p class="green"><?=$props["TOP_NAME"]["VALUE"]?></p>
									<p><?=$arFields["PREVIEW_TEXT"]?></p>

									<a class="button button_old" href="<?=$props["LINK"]["VALUE"]?>"><?=$props["BTN"]["VALUE"]?></a>
								</div>
							</div>
						</div>
					<?endif?>
				<?}?>
			</div>
		</div>
	</div>
<?}?>

<div class="jumb_green<?if ($USER->IsAuthorized()):?> jumb_green_sm<?endif?>">
	<div class="container">
		<div class="jumb_green_inner text-center">
			<p class="title white">Добро пожаловать в Школу <br/>разумного инвестирования!</p>
			<?if (!$USER->IsAuthorized()):?>
				<p class="white">Здесь собраны наши лучшие курсы, обучающие программы <br/>и материалы, чтобы помочь Вам начать инвестировать с нуля <br/>и добиться стабильных и высоких результатов.</p>
				<p class="lightgreen">Сделайте правильный выбор</p>
			<?endif?>
			<p><a class="font_12 white" href="https://drive.google.com/file/d/1mA7bpyLgIpQs3TejsVVhe1Ht7vP31qw5/view" target="_blank" rel="nofollow">Образовательная лицензия №12416</a></p>
		</div>
	</div>
</div>
<?if(!$USER->IsAuthorized()):?>
<?ShowSection(28, 1)?>
<?ShowSection(29, 2)?>
<?endif?>
<div class="services_container">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<p class="subtitle">Отдельные платные видеокурсы</p>
			</div>
		</div>

		<div class="row">
			<?CModule::IncludeModule("iblock");
			$arFilter = Array("IBLOCK_ID"=>9, "ACTIVE"=>"Y");
			if(!$USER->IsAuthorized()){
				$arFilter["!PROPERTY_HIDE_NOT_LOGIN"] = 19;
			}
			$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$arFields["PROPS"] = $ob->GetProperties();
				$arFields["PAYED"] = intval(checkPaySeminar($arFields["ID"]));

				$items[] = $arFields;
			}

			if($USER->IsAuthorized()){
				$orderBy=array('PAYED'=>'desc', 'SORT'=>'asc');

				function cmp($a, $b) {
				  global $orderBy;
				  $result= 0;
				  foreach( $orderBy as $key => $value ) {
					if( $a[$key] == $b[$key] ) continue;
					$result= ($a[$key] < $b[$key])? -1 : 1;
					if( $value=='desc' ) $result= -$result;
					break;
					}
				  return $result;
				  }
				usort($items, "cmp");
			}


			foreach($items as $arFields){

				$payed = checkPaySeminar($arFields["ID"]);
				$access = checkAccessSeminar($arFields["ID"]);
				
				$end = checkPayEndDateSeminar($arFields["ID"]);
				if($end){
					$end = new DateTime($end);
					$diff = $end->diff(new DateTime());
				}
				
				if($arFields["PROPS"]["LINK"]["VALUE"] && !$payed){
					$arFields["DETAIL_PAGE_URL"] = $arFields["PROPS"]["LINK"]["VALUE"];
				}
				$i++;
				?>
				<div class="col col-xs-6 col-lg-5<?if(is_float($i/2)):?> col-lg-offset-1<?endif?>">
					<div class="services_element<?if($payed):?> purchase<?endif?><?if($access):?> not_available<?endif?>">
						<div class="services_element_head">
							<div class="services_element_head_inner<?if($end):?> with_sys<?endif?>">
								Видеокурс
								<?if($end):?>
									<?if($diff->days>45):?>
										<span class="services_element_head_sys font_11 strong green">подписка до<br> <?=FormatDate("j F Y", $end->getTimestamp())?> года</span>
									<?else:?>
										<span class="services_element_head_sys font_11 strong red"><span class="icon icon-attention"></span> подписка до<br> <?=FormatDate("j F Y", $end->getTimestamp())?> года</span>
									<?endif?>
								<?endif?>
							</div>
						</div>

						<div class="services_element_img">
							<?if($payed && $access):?>
								<a href="#" data-toggle="modal" data-target="#popup_not_available">
							<?else:?>
								<a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
							<?endif?>
								<span class="services_element_img_inner" style="background-image: url(<?=CFile::GetPath($arFields['PREVIEW_PICTURE'])?>);">
								</span>
							</a>
						</div>

						<div class="services_element_title_block">
							<div class="services_element_title_block_inner"><p class="services_element_title"><?=$arFields["NAME"]?></p></div>
						</div>

						<div class="services_element_body">
							<div class="services_element_body_inner">
								<p><?=$arFields["PREVIEW_TEXT"]?></p>
								<?if($payed && $access):?>
									<a class="button button_old" href="#" data-toggle="modal" data-target="#popup_not_available">Недоступно</a>
								<?else:?>
									<a class="button button_old" href="<?=$arFields["DETAIL_PAGE_URL"]?>">Подробнее</a>
								<?endif?>
							</div>
						</div>
					</div>
				</div>
			<?}?>
		</div>
	</div>
</div>
<?if($USER->IsAuthorized()):?>
	<?ShowSection(29, 2, true)?>
<?endif?>
<?ShowSection(30, 2)?>
<div class="bottom_text">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p>Примеры достижений касаются личных результатов, являются последствием личных действий, знаний и опыта. Мы не несем ответственности за результаты, полученные другими людьми, поскольку они могут отличаться в зависимости от различных обстоятельств.</p>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="popup_not_available" tabindex="-1">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<p>Данный курс будет доступен после изучения предыдущих уроков и выполнения всех заданий</p>
	  </div>
	</div>
  </div>
</div>


<div class="modal fade" id="popup_download" tabindex="-1">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title">Скачать книгу "Темная сторона инвестирования: 7 инсайдов, которые знают только профи"</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<form id="webinar_reg_form" method="post">
			<?
			$metkas = array(
				"utm_source",
				"utm_campaign",
				"utm_medium",
				"utm_term"
			);
			foreach($metkas as $metka){
				if($_REQUEST[$metka]){
					?>
					<input type="hidden" name="<?=$metka?>" value="<?=$_REQUEST[$metka]?>">
					<?
				}
			}
			?>
			<div class="form_element">
				<input type="text" name="name" placeholder="Ваше имя" />
			</div>
			<div class="form_element">
				<input type="text" name="email" placeholder="Ваша электронная почта" />
			</div>
			<div class="form_element">
				<input type="text" name="phone" placeholder="Ваш телефон" />
			</div>
			<div class="confidencial_element form_element">
				<div class="checkbox">
					<input type="checkbox" id="confidencial_download_book" name="confidencial_download_book" checked />
					<label for="confidencial_download_book">Нажимая кнопку, я даю согласие на обработку персональных данных <br/>и соглашаюсь с </label><span data-toggle="modal" data-target="#footer356">пользовательским соглашением</span><label for="confidencial_download_book"> и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
				</div>
			</div>

			<div class="text-center">
				<input class="button button_green" type="submit" value="Скачать" />
				<p class="modal_comment">Мы против спама. Ваши данные<br/>не будут переданы 3-м лицам.</p>
			</div>
		</form>
	  </div>
	</div>
  </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
