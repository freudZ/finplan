<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;

if(!$_POST["fio"] || !$_POST["phone"]){
	exit();
}

if (!$USER->IsAuthorized() && $_POST["pay_type"] && intval($_POST["item"]) && $_POST["email"]){
	
	
}elseif (!$USER->IsAuthorized() || !$_POST["pay_type"] || !intval($_POST["item"])){
	exit();
}

if (!$USER->IsAuthorized()){
	function generate_password($number)
	{
		$arr = array('1','2','3','4','5','6','7','8','9','0');
		$pass = "";
		for($i = 0; $i < $number; $i++)
		{
		  // Вычисляем случайный индекс массива
		  $index = rand(0, count($arr) - 1);
		  $pass .= $arr[$index];
		}
		return $pass;
	}
	$pass = generate_password(6);
		
	$cUser = new CUser; 
	$sort_by = "ID";
	$sort_ord = "ASC";
	$arFilter = array(
	   "LOGIN_EQUAL" => $_REQUEST["email"],
	   "ACTIVE" => 'Y',
	);
	$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
	if ($arUser = $dbUsers->Fetch()) 
	{
	   exit();
	} else {
		global $USER;
		$arResult = $USER->Register(strip_tags($_POST["email"]), strip_tags($_POST["fio"]), "", $pass, $pass, strip_tags($_POST["email"]));
		if($arResult["TYPE"]=="ERROR"){
			exit();
		} else {
			$arEventFields = array(
				"EMAIL" => strip_tags($_POST["email"]),
				"LOGIN" => strip_tags($_POST["email"]),
				"PAS" => $pass
			);
			
			CEvent::Send("NEW_SEM_REG", SITE_ID, $arEventFields);

			$USER->Authorize($arResult["ID"]);
		}
	}
} else {
	$user = new CUser;
	$arName = explode(" ", strip_tags($_POST["fio"]));
	$user->Update($USER->GetID(), array(
		"NAME" => $arName[0],
        "LAST_NAME" => count($arName)>1?$arName[1]:""
	));
}

CModule::IncludeModule("iblock");
$preview_text = '';
$dop_items = array();
$arFilter = Array("IBLOCK_ID"=>14, "ID"=>intval($_POST["item"]), "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
if($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$props = $ob->GetProperties();
	$dop_items = $props["DOP_ITEMS"]["VALUE"];
	$close_items = $props["CLOSE_ACCESS"]["VALUE"];

	$_POST["item"] = $props["ITEM"]["VALUE"];

	$radarDays = $props["RADAR_DAYS"]["VALUE"];
	$itemDays = $props["ITEM_DAYS"]["VALUE"];
	$VIP = $props["VIP"]["VALUE"];
	$ACCESS_USA = $props["ACCESS_USA"]["VALUE"];
	$ACCESS_GS_USA = $props["ACCESS_GS_USA"]["VALUE"];
} else {
	$arFilter = Array("IBLOCK_ID"=>9, "ID"=>intval($_POST["item"]), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
	if($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$props = $ob->GetProperties();
		$dop_items = $props["DOP_ITEMS"]["VALUE"];
	} else {
		exit();
	}
}

if(strpos($_POST["pay_type"], "sber")===false){
	$arFilter = Array("IBLOCK_ID"=>12, "PROPERTY_YA_CODE"=>$_POST["pay_type"], "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
	if($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$buyID = $item["ID"];
	} else {
		exit();
	}
} else if($_POST["pay_type"]=="sber"){
	$buyID = 65682;
} else if($_POST["pay_type"]=="sber_credit"){
    $buyID = 523054;
    $preview_text = "Рассрочка Сбербанк онлайн";
}


$el = new CIBlockElement;

$PROP = array();
$PROP["FIO"] = $_POST["fio"];
$PROP["PHONE"] = $_POST["phone"];
$PROP["USER"] = $USER->GetID();
$PROP["ITEM"] = intval($_POST["item"]);
$PROP["BUY_TYPE"] = $buyID;
$PROP["DOP_ITEMS"] = $dop_items;
$PROP["CLOSE_ACCESS"] = $close_items;

//Выставляем Вип пакет если в скрытом семинаре отмечен вип-пакет
if($VIP=='Y'){
$PROP["VIP"] = Array("VALUE" => 284);
}

if($ACCESS_USA=='Y'){
$PROP["ACCESS_USA"] = Array("VALUE" => 111);
}
if($ACCESS_GS_USA=='Y'){
$PROP["ACCESS_GS_USA"] = Array("VALUE" => 282);
}

$PROP["PAY_NAME"] = $arFields["NAME"];
if(!$PROP["ITEM"]){
	unset($PROP["ITEM"]);
}

if($radarDays){
	$end = checkPayEndDateRadar();
	if($end){
		$end = new DateTime($end);
		$end->modify("+".$radarDays."days");
		
		$PROP["RADAR_END"] = $end->format("d.m.Y");
	} else {
		$dt = (new DateTime())->modify("+".$radarDays."days");
		$PROP["RADAR_END"] = $dt->format("d.m.Y");
	}
}
if($itemDays){
	$end = checkPayEndDateSeminar(intval($_POST["item"]));
	if($end){
		$end = new DateTime($end);
		$end->modify("+".$itemDays."days");
		$PROP["ITEM_END"] = $end->format("d.m.Y");
	} else {
		$dt = (new DateTime())->modify("+".$itemDays."days");
		$PROP["ITEM_END"] = $dt->format("d.m.Y");
	}
}

//промокод
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bonus_1_log.txt");
$promocode = strip_tags($_POST["promocode"]); //Применяемый промокод
				AddMessage2Log('promocode 1 (примененный на странице в поле ввода)= '.print_r($promocode, true),'');
if($props["SHOW_PROMO"]["VALUE"]){
	if($promocode){
		$promoVal = $promocode;
				AddMessage2Log('promoVal 2 = '.print_r($promoVal, true),'');
	} else {
		global $APPLICATION;
		//$promoVal = $APPLICATION->get_cookie("PERSONAL_PROMO");  //Проверка из куки, сделать проверку из пользовтельского свойства
		//$promoVal = Promocodes::getRefPersonalPromo($USER->GetID());  //Проверяем наличие промокода из пользовтельского свойства юзера
		$promoVal = Promocodes::getRefPersonalPromoCRM($USER->GetID());  //Проверяем наличие промокода из пользовтельского свойства элемнта 19 инфоблок

		AddMessage2Log('promoVal 3 = '.print_r($promoVal, true),'');

		if(Promocodes::checkPersonalPromoUses($promoVal, $USER->GetID())){
		  //	$promoVal = "";
			Promocodes::setRefPersonalPromoUsed($USER->GetID()); //Отмечаем, что промокод использован
			Promocodes::updateUserReferalStatusCRM('', $statusId, $USER->GetID()); //Отмечаем, что промокод использован
			$APPLICATION->set_cookie("PERSONAL_PROMO", "");
			AddMessage2Log('promoVal 4 = '.print_r($promoVal, true),'');

		}
	}


	if($promoVal){
		AddMessage2Log('promoVal 5 = '.print_r($promoVal, true),'');

		global $USER;
		$res = Promocodes::getPromocode($promoVal, $USER->GetID(), $props["PRICE"]["VALUE"]);
		if($res["have"]){
			$PROP["PROMOCODE"] = $promoVal;
			if($res["type"]!="PERSONAL" && $res["summ"]){
				$props["PRICE"]["VALUE"] = $props["PRICE"]["VALUE"] - $res["summ"].".00";
				$PROP["PROMOCODE_SUMM"] = $res["summ"];
			}
		}
	}
}

//бонусы
$bonuses = intval($_POST["bonuses"]);
$totalBonuses = Bonuses::getTotalBonuses($USER->GetID());
if($bonuses && $bonuses<=$totalBonuses && $totalBonuses){
	$PROP["BONUSES_SUMM"] = $bonuses;
}

$arLoadProductArray = Array(
  "IBLOCK_SECTION_ID" => false,
  "IBLOCK_ID"      => 11,
  "PROPERTY_VALUES"=> $PROP,
  "NAME"           => date("d.m.Y H:i:s"),
  "ACTIVE"         => "Y",
  "PREVIEW_TEXT"   => (!empty($preview_text)?$preview_text.'. ':'').$_POST["comment"]
);

if($order_id = $el->Add($arLoadProductArray)){
    BaseHelper::afterBuyAction($order_id);
		$amount = $props["PRICE"]["VALUE"];

		//Проверим бонусы и скидку по промокоду
		if(intval($PROP["BONUSES_SUMM"])>0){
		 $amount = floatVal($amount)-intval($PROP["BONUSES_SUMM"]);
		}



	if($_POST["pay_type"]=="sber"){
		include($_SERVER["DOCUMENT_ROOT"]."/learning/pay_sber/config.php");
        $amount = $amount*100;

		$sberParams = array(
			"userName" => $arSberConfig["userName"],
			"password" => $arSberConfig["password"],
			"orderNumber" => $order_id,
			"customerNumber" => $USER->GetLogin(),
			"amount" => str_replace(array(".", ","), "", $amount),
			"returnUrl" => "https://fin-plan.org/learning/pay_sber/process.php?pay_sber=success",
			"failUrl" => "https://fin-plan.org/learning/pay_sber/process.php?pay_sber=error",
			"description" => htmlspecialchars_decode($arFields["NAME"])
		);
		$postdata = http_build_query($sberParams);
		//https://securepayments.sberbank.ru/payment/rest/register.do?
		$sberData = json_decode(file_get_contents($arSberConfig["url"].'register.do?'.$postdata), true);
		//$sberData["orderId"] - SBER_ID
		//$sberData["formUrl"] - SBER_URL
		
		/*
		[orderId] => 0438d68d-f138-713f-0438-d68d04b11707
		[formUrl] => https://3dsec.sberbank.ru/payment/merchants/sbersafe/payment_ru.html?mdOrder=0438d68d-f138-713f-0438-d68d04b11707
		*/
		?>
		<a href="#" data-href="<?=$sberData["formUrl"]?>" class="hidden"></a>
		<?
	} else if($_POST["pay_type"]=="sber_credit"){
        include($_SERVER["DOCUMENT_ROOT"]."/learning/pay_sber_credit/config.php");
        $amount = $amount*100;
            global $USER;
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            $email = !empty($arUser["EMAIL"])?$arUser["EMAIL"]:$arUser["LOGIN"];
        
        $arOrderBundle["cartItems"]["items"][] = array(
          "positionId" => 0,
          "name" => htmlspecialchars_decode($arFields["NAME"]),
          "quantity" => array("value"=>1, "measure"=>"шт"),
          "itemAmount" => str_replace(array(".", ","), "", $amount),
          "itemPrice" => str_replace(array(".", ","), "", $amount),
          "itemCurrency" => 643,
          "itemCode" => $_POST["item"],
        );
        
        //rightTerms - Желаемый срок кредитования в месяцах. Возможна передача нескольких значений через запятую. Атрибут
        //передается, если выбран productType=INSTALLMENT
        //Если параметр заполнен, то клиенту будут доступны только указанные в параметре сроки оплаты. Например,
        //если "rightTerms":[3,6,9], то клиенту будут доступны для выбора сроки 3, 6 или 9 месяцев.
        //Если параметр не заполнен, то клиенту будут доступны все ранее утвержденные с банком сроки
        
        $arOrderBundle["installments"] = array(
                "productType" => "INSTALLMENT", //CREDIT - кредит, INSTALLMENT - рассрочка
                "productID" => 10, //Для функциональности интернет-кредитования укажите значение 10
                "rightTerms" => array(3,6,9,12)
        );
        
        $sberParams = array(
          "userName" => $arSberConfig["userName"],
          "password" => $arSberConfig["password"],
          "orderNumber" => $order_id,
          "clientId" => $USER->GetId(),
          "currency" => 643,
          "amount" => str_replace(array(".", ","), "", $amount),
          "returnUrl" => "https://fin-plan.org/learning/pay_sber_credit/process.php?pay_sber=success",
          "failUrl" => "https://fin-plan.org/learning/pay_sber_credit/process.php?pay_sber=error",
          "description" => htmlspecialchars_decode($arFields["NAME"]),
          "jsonParams" => json_encode(array("email"=>!empty($email)?$email:$_POST["email"])),
          "dummy" => true,
          "orderBundle" => json_encode($arOrderBundle)
        );
        //$postdata = htmlspecialchars(http_build_query($sberParams));
        //$postdata .= "&orderBundle=".json_encode($arOrderBundle);
        
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $arSberConfig["url"].'register.do', [
          'form_params' => $sberParams,
          'timeout'  => 10
        ]);
        if ( $response->getStatusCode() != 200 ) { return; }
        $response = json_decode($response->getBody()->getContents(), true);
       
        
        //https://securepayments.sberbank.ru/payment/rest/register.do?
      //  $sberData = json_decode(file_get_contents($arSberConfig["url"].'register.do?'.$postdata), true);
      //  echo "query: ".$postdata;
 /*       echo "<pre  style='color:black; font-size:11px;'>";
           print_r($sberData);
        echo "</pre>";*/
        //$sberData["orderId"] - SBER_ID
        //$sberData["formUrl"] - SBER_URL
        
   
        ?>
        <a href="#" data-href="<?=$response["formUrl"]?>" class="hidden"></a>
        <?
    } else {
		$ym_merchant_receipt = array(
			"customerContact" => str_replace(array(" ", "(", ")", "-"), "", $_POST["phone"]),
			"items" => array(
				"quantity" => 1,
				"price" => array(
					"amount" => str_replace(",", ".", $amount),
				),
				"tax" => 1,
				"text" =>  htmlspecialchars_decode($arFields["NAME"]),
                "paymentMethodType" => "full_payment",
                "paymentSubjectType" => "service",
			)
		);

		$data = json_encode($ym_merchant_receipt, JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE);
		$data = str_replace('"items":{', '"items":[{', $data);
		$data = str_replace('}}', '}]}', $data);
		?>
		<!--	<form action="https://money.yandex.ru/eshop.xml" method="post"> -->
			<form action="https://yoomoney.ru/eshop.xml" method="post">
				<input name="shopId" value="118806" type="hidden"/>
				<input name="scid" value="49350" type="hidden"/> 
				<input name="sum" value="<?=$amount?>" type="hidden">
				<input name="orderNumber" value="<?=$order_id?>" type="hidden">
				<input name="customerNumber" value="<?=$USER->GetLogin()?>" type="hidden"/>
				<input name="paymentType" value="<?=$_POST["pay_type"]?>" type="hidden">
				<input name="ym_merchant_receipt" value='<?=$data?>' type="hidden">
			</form>
	<?}?>
<?} else {
    echo "Error: ".$el->LAST_ERROR;
}?>
