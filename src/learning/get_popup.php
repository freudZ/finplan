<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
$arFilter = Array("IBLOCK_ID"=>9, "ID"=>intval($_REQUEST["id"]), "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$props = $ob->GetProperties();
	
	echo json_encode(array(
		"id"=>$arFields["ID"],
		"name"=>$arFields["NAME"],
		"price"=>$props["PRICE"]["VALUE"]." руб.",
	));
	
}
?>