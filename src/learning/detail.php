<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Семинары");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

define("IS_GS_SEMINAR",(intval($_REQUEST["SEMINAR_ID"])==13823?true:false)); //Определяем что открыт именно семинар ГС
$showGSUsa = checkPayGSUSA(13823, $USER->GetID()); //Определяем оплату с ГС США
$showTabsGs = true;//$GLOBALS["USER"]->IsAdmin(); //Показ табов в принципе. Можно отключать или прятать под админа
$buyGsLink = "https://finplan.expert/radar"; //Ссылка для редиректа на лендинг при неоплаченном ГС США и клике на табу США

CModule::IncludeModule("iblock");
$arFilter = Array("IBLOCK_ID"=>9, "ID"=>intval($_REQUEST["ELEMENT_ID"]), "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
if($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$props = $ob->GetProperties();

	if(!checkPaySeminar($arFields["ID"])){
		LocalRedirect("/learning/");
	}

	if(checkAccessSeminar($arFields["ID"])){
		LocalRedirect("/learning/");
	}
} else {
	LocalRedirect("/learning/");
}

$APPLICATION->SetPageProperty("og:title", $arFields["NAME"]);

if($props["WEBSERT_TEMPLATE"]["VALUE"]) {
	generateWebSertificate($arFields["ID"], CFile::GetPath($props["WEBSERT_TEMPLATE"]["VALUE"]));
}

$hlbl = 13;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$filt = array("UF_USER"=>$USER->GetID(), "UF_ITEM"=>$arFields["ID"]);

$main_query = new Entity\Query($entity);
$main_query->setSelect(array("*"));
$main_query->setFilter($filt);
$exec = $main_query->exec();
$exec = new CDBResult($exec);
if($item = $exec->Fetch()){
    $itemPay = $item;
}


CModule::IncludeModule("iblock");
$res = CIBlockSection::GetByID(21);
$arSect = $res->GetNext();

if($_REQUEST["SEMINAR_ID"]){
	$arFilter= array("IBLOCK_ID"=>13, "PROPERTY_ITEM"=>$_REQUEST["SEMINAR_ID"], "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
	if($ob = $res->GetNextElement())
	{
		$arFields2 = $ob->GetFields();
		$arSect["CODE"] = $arFields2["NAME"];
		$arSect["DESCRIPTION"] = $arFields2["PREVIEW_TEXT"];
	}
}

$end = checkPayEndDateSeminar($arFields["ID"]);
if($end){
	$end = new DateTime($end);
	$diff = $end->diff(new DateTime());
}
?>
    <div class="wrapper">
        <div class="container">
            <div class="main_content">
                <div class="main_content_inner">
					<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>

                    <div class="learn_page_top infinite_container green">
                        <div class="clearfix">
                            <a href="/learning/" class="link_with_icon"><span class="icon icon-doc_list"></span><span class="text">К&nbsp;списку всех <br />семинаров</span></a>
                        </div>
                    </div>

                    <div class="title_container">
                        <h1><?=$arFields["NAME"]?></h1>
					   <? $hide_pay_btn = \Bitrix\Main\Config\Option::get("grain.customsettings","HIDE_PAY_YEAR_DEMAND") && intval($_REQUEST["ELEMENT_ID"])==13823; ?>

						<?if($end):?>
							<?if($diff->days>45):?>
								<p class="font_13 strong green">подписка до <?=FormatDate("j F Y", $end->getTimestamp())?> года</p>
							<?else:?>
								<p data-hide="<?=$hide_pay_btn==true?'Y':'N'?>" class="font_13 strong red"><span class="icon icon-attention"></span> подписка до <?=FormatDate("j F Y", $end->getTimestamp())?> года</p>

								<?if($props["AGAIN_LINK"]["VALUE"] && $hide_pay_btn==false ):?>
									<a class="button button_gray_transparent" href="<?=$props["AGAIN_LINK"]["VALUE"]?>">Продлить по спец. цене</a>
								<?endif?>

							<?endif?>
						<?endif?>

                    </div>
                    <!-- *****/КАРКАС***** -->
                    <div class="learn_page_outer">

						<?for($i=1;$i<=10;$i++):?>
							<?if($props["PART_".$i."_NAME"]["VALUE"]):?>
                                <div class="learn_page_element">

										  <?if(IS_GS_SEMINAR && $showTabsGs && !empty($props["PART_".$i."_NAME_USA"]["VALUE"])):?>
							               <ul class="sections_list portfolio_tables_selector seminar_tab_selection" >
							                     <li class="tab_seminar_rus active" data-type="rus" data-value="tab_seminar_rus_<?=$i?>">РФ</li>
														<li class="tab_seminar_usa" data-type="usa" data-link="<?=($showGSUsa?'':$buyGsLink)?>" data-value="tab_seminar_usa_<?=$i?>">США</li>
												</ul>
										  <?endif;?>
										  <?if(IS_GS_SEMINAR && $showTabsGs && !empty($props["PART_".$i."_NAME_USA"]["VALUE"])):?>
												<div class="tab_seminar_rus_<?=$i?> tab_seminar_body tab_seminar_body_rus">
										  <?endif;?>
                                    <p class="green_subtitle">Часть <?=$i?>. <?=$props["PART_".$i."_NAME"]["VALUE"]?></p>
									<?if($props["PART_".$i."_DESC"]["VALUE"]):?>
                                        <p><?=$props["PART_".$i."_DESC"]["~VALUE"]["TEXT"]?></p>
									<?endif?>
									<?if($props["PART_".$i."_VIDEO"]["VALUE"]):?>
                                        <div class="video_outer">
                                            <span class="play"></span>
                                            <div class="before"></div>
                                            <?/*<div class="before" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/learn_page/video_02.jpg)"></div>*/?>
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$props["PART_".$i."_VIDEO"]["VALUE"]?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                        </div>
									<?endif?>
									<?if($props["PART_".$i."_DESC_AFTER"]["VALUE"]):?>
                                        <p><?=$props["PART_".$i."_DESC_AFTER"]["~VALUE"]["TEXT"]?></p>
									<?endif?>
										  <?if(IS_GS_SEMINAR && $showGSUsa && !empty($props["PART_".$i."_NAME_USA"]["VALUE"])):?>
												</div>
												<div class="tab_seminar_usa_<?=$i?> tab_seminar_body tab_seminar_body_usa" style="display:none;">
			                                     <p class="green_subtitle">Часть <?=$i?>. <?=$props["PART_".$i."_NAME_USA"]["VALUE"]?></p>
												<?if($props["PART_".$i."_DESC_USA"]["VALUE"]):?>
			                                        <p><?=$props["PART_".$i."_DESC_USA"]["~VALUE"]["TEXT"]?></p>
												<?endif?>
												<?if($props["PART_".$i."_VIDEO_USA"]["VALUE"]):?>
			                                        <div class="video_outer">
			                                            <span class="play"></span>
			                                            <div class="before"></div>
			                                            <?/*<div class="before" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/learn_page/video_02.jpg)"></div>*/?>
			                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$props["PART_".$i."_VIDEO_USA"]["VALUE"]?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			                                        </div>
												<?endif?>
												<?if($props["PART_".$i."_DESC_AFTER_USA"]["VALUE"]):?>
			                                        <p><?=$props["PART_".$i."_DESC_AFTER_USA"]["~VALUE"]["TEXT"]?></p>
												<?endif?>
												</div>
										  <?endif;//tab usa?>
                                </div>

								<?if($props["DOP_".$i."_INFO"]["VALUE"]):?>
									<?if(IS_GS_SEMINAR && $showTabsGs && !empty($props["DOP_".$i."_INFO_USA"]["VALUE"])):?>
					               <ul class="sections_list portfolio_tables_selector seminar_tab_selection" >
					                     <li class="tab_seminar_rus active" data-type="rus" data-value="tab_seminar_rus_<?=$i?>">РФ</li>
												<li class="tab_seminar_usa" data-type="usa" data-link="<?=($showGSUsa?'':$buyGsLink)?>" data-value="tab_seminar_usa_<?=$i?>">США</li>
										</ul>
									<?endif;?>
								   <?if(IS_GS_SEMINAR && $showTabsGs && !empty($props["PART_".$i."_NAME_USA"]["VALUE"])):?>
										<div class="tab_seminar_rus_<?=$i?> tab_seminar_body tab_seminar_body_rus">
								   <?endif;?>
                                    <?
									$arDopItems = [];
									$arDopItemsImportant = [];
                                    foreach($props["DOP_".$i."_INFO"]["VALUE"] as $n=>$val){
                                        $arFilter = Array("IBLOCK_ID"=>10, "ID"=>$val, "ACTIVE"=>"Y");
                                        $res = CIBlockElement::GetList(Array(), $arFilter, false, false);
                                        if($ob = $res->GetNextElement()) {
                                            $item = $ob->GetFields();
                                            $item["PROPS"] = $ob->GetProperties();

                                            if($item["PROPS"]["IMPORTANT"]["VALUE"] || $item["PROPS"]["HOMEWORK"]["VALUE"]){
												$arDopItemsImportant[] = $item;
                                            } else {
												$arDopItems[] = $item;
											}
                                        }
                                    }?>
                                    <?foreach($arDopItemsImportant as $item):
                                        if($item["PROPS"]["HOMEWORK"]["VALUE"]){
                                            continue;
										}
										$link = $item["PROPS"]["LINK"]["VALUE"];
                                        if(!$link){
                                            $link = CFile::GetPath($item["PROPS"]["FILE"]["VALUE"]);
                                        }
										?>
                                        <div class="learn_page_subsection download">
                                            <a href="<?=$link?>" class="icon icon-download2"></a>
                                            <a href="<?=$link?>" class="learn_page_subsection_subtitle"><?=$item["~NAME"]?></a>
                                        </div>
                                    <?endforeach;?>

                                    <?foreach($arDopItemsImportant as $item):
                                        if($item["PROPS"]["IMPORTANT"]["VALUE"]){
                                            continue;
                                        }
                                        ?>
                                        <div class="learn_page_subsection homework">
                                            <span class="icon icon-download2"></span>
                                            <p class="learn_page_subsection_subtitle">Домашнее задание</p>
                                            <p><?=$item["PREVIEW_TEXT"]?></p>
                                        </div>
									<?endforeach;?>
									<?if($arDopItems):?>
                                        <div class="learn_page_more">
                                            <p class="t20 m0 mb10">Дополнительные материалы</p>
                                            <ol class="learn_page_more_list">
                                                <?foreach($arDopItems as $item):?>
                                                    <?if($item["PROPS"]["LINK"]["VALUE"]):?>
                                                        <li class="read">
                                                            <a href="<?=$item["PROPS"]["LINK"]["VALUE"]?>"<?if($item["PROPS"]["BLANK"]["VALUE"]):?> target="_blank"<?endif?>>
                                                                <span class="text"><?=$item["~NAME"]?></span>
                                                                <span class="icon_outer"><span class="icon icon-eye_thin"></span>Читать</span>
                                                            </a>
                                                        </li>
                                                    <?else:?>
                                                    <li class="download">
                                                        <a href="<?=CFile::GetPath($item["PROPS"]["FILE"]["VALUE"])?>"<?if($item["PROPS"]["BLANK"]["VALUE"]):?> target="_blank"<?endif?>>
                                                            <span class="text"><?=$item["~NAME"]?></span>
                                                            <span class="icon_outer"><span class="icon icon-download"></span>Скачать</span>
                                                        </a>
                                                    </li>
                                                    <?endif?>
                                                <?endforeach?>
                                            </ol>
                                        </div>
                                    <?endif;?>
									<?if(IS_GS_SEMINAR && $showTabsGs && !empty($props["PART_".$i."_NAME_USA"]["VALUE"])):?>
												</div>
												<div class="tab_seminar_usa_<?=$i?> tab_seminar_body tab_seminar_body_usa" style="display:none;">
                                    <?
									$arDopItemsUsa = [];
									$arDopItemsImportantUsa = [];
                                    foreach($props["DOP_".$i."_INFO_USA"]["VALUE"] as $n=>$val){
                                        $arFilter = Array("IBLOCK_ID"=>10, "ID"=>$val, "ACTIVE"=>"Y");
                                        $res = CIBlockElement::GetList(Array(), $arFilter, false, false);
                                        if($ob = $res->GetNextElement()) {
                                            $item = $ob->GetFields();
                                            $item["PROPS"] = $ob->GetProperties();

                                            if($item["PROPS"]["IMPORTANT"]["VALUE"] || $item["PROPS"]["HOMEWORK"]["VALUE"]){
												$arDopItemsImportantUsa[] = $item;
                                            } else {
												$arDopItemsUsa[] = $item;
											}
                                        }
                                    }?>
                                    <?foreach($arDopItemsImportantUsa as $item):
                                        if($item["PROPS"]["HOMEWORK"]["VALUE"]){
                                            continue;
										}
										$link = $item["PROPS"]["LINK"]["VALUE"];
                                        if(!$link){
                                            $link = CFile::GetPath($item["PROPS"]["FILE"]["VALUE"]);
                                        }
										?>
                                        <div class="learn_page_subsection download">
                                            <a href="<?=$link?>" class="icon icon-download2"></a>
                                            <a href="<?=$link?>" class="learn_page_subsection_subtitle"><?=$item["~NAME"]?></a>
                                        </div>
                                    <?endforeach;?>

                                    <?foreach($arDopItemsImportantUsa as $item):
                                        if($item["PROPS"]["IMPORTANT"]["VALUE"]){
                                            continue;
                                        }
                                        ?>
                                        <div class="learn_page_subsection homework">
                                            <span class="icon icon-download2"></span>
                                            <p class="learn_page_subsection_subtitle">Домашнее задание</p>
                                            <p><?=$item["PREVIEW_TEXT"]?></p>
                                        </div>
									<?endforeach;?>

                                   <?if($arDopItemsUsa):?>
                                        <div class="learn_page_more">
                                            <p class="t20 m0 mb10">Дополнительные материалы</p>
                                            <ol class="learn_page_more_list">
                                                <?foreach($arDopItemsUsa as $item):?>
                                                    <?if($item["PROPS"]["LINK"]["VALUE"]):?>
                                                        <li class="read">
                                                            <a href="<?=$item["PROPS"]["LINK"]["VALUE"]?>"<?if($item["PROPS"]["BLANK"]["VALUE"]):?> target="_blank"<?endif?>>
                                                                <span class="text"><?=$item["~NAME"]?></span>
                                                                <span class="icon_outer"><span class="icon icon-eye_thin"></span>Читать</span>
                                                            </a>
                                                        </li>
                                                    <?else:?>
                                                    <li class="download">
                                                        <a href="<?=CFile::GetPath($item["PROPS"]["FILE"]["VALUE"])?>"<?if($item["PROPS"]["BLANK"]["VALUE"]):?> target="_blank"<?endif?>>
                                                            <span class="text"><?=$item["~NAME"]?></span>
                                                            <span class="icon_outer"><span class="icon icon-download"></span>Скачать</span>
                                                        </a>
                                                    </li>
                                                    <?endif?>
                                                <?endforeach?>
                                            </ol>
                                        </div>
                                    <?endif;?>

												</div>
									<?endif;//usa tab?>


								<?endif?>
							<?endif?>
						<?endfor?>

						<?if($props["OPEN_ACCESS"]["VALUE"] && checkAccessSeminar($props["OPEN_ACCESS"]["VALUE"])){
							$showFirstStep = true;
						}?>
                        <?if(!$props["HIDE_END_BLOCK"]["VALUE"]):?>
                            <div class="learn_page_steps infinite_container green"<?if(!$showFirstStep):?> style="margin-bottom: 50px"<?endif?>>
                                <p class="learn_page_steps_subtitle">Всё посмотрели?</p>
                                <p class="learn_page_steps_description">Поздравляем! Вы можете двигаться далее.</p>
                                <?if($showFirstStep):?>
                                    <div class="learn_page_step first_step">
                                        <span class="learn_page_step_before">►►►</span>

                                        <p>Напишите в форме ниже свои идеи, выводы, комментарии для перехода к следующему курсу и нажмите кнопку «Отправить».</p>

                                        <form class="learn_page_review_form" method="post">
                                            <div class="form_element">
                                                <textarea id="access_text" data-item="<?=$arFields["ID"]?>" name="message" placeholder="Изменилось ли Ваше представление об инвестициях? Был ли материал полезен и понятен? Если у Вас есть предложения и дополнения по улучшению курса, также напишите их нам. Спасибо!"></textarea>
                                            </div>

                                            <div class="submit_element">
                                                <button class="learn_page_second_step_open_btn button" type="submit">Отправить</button>
                                            </div>
                                        </form>
                                    </div>
                                <?endif;?>

                                <div class="learn_page_step second_step"<?if(!$showFirstStep):?> style="display: block"<?endif;?>>
                                    <span class="learn_page_step_before">►►►</span>

                                    <p>Вы закончили обучение по данному блоку. Получите Ваш электронный сертификат. Сохраните его в свой аккаунт социальной сети, чтобы не потерять:</p>

                                    <div class="share_sertificate">
                                        <?$APPLICATION->SetPageProperty("og:image", CFile::GetPath($itemPay["UF_FILE"]))?>
                                        <img class="mb30" src="<?=CFile::GetPath($itemPay["UF_FILE"])?>" alt="" /><br />

                                        <div class="share_sertificate_social_tgl">
                                            <span class="button share_sertificate_social_tgl_btn">Сохранить</span>

                                            <ul class="share_list share_cusom_links">
																								<li><span data-type="vk" target="_blank" class="icon icon-vk"></span></li>
                                                <? /* <li><span data-type="fb" target="_blank" class="icon icon-fb"></span></li>
                                                <li><span data-type="tw" target="_blank" class="icon icon-tw"></span></li> */ ?>
                                                <li><span data-type="lj" target="_blank" class="icon icon-lj"></span></li>
                                                <li><span data-type="ok" target="_blank" class="icon icon-ok"></span></li>
                                            </ul>

                                            <!-- <ul class="share_cusom_links">
    																						<li data-type="fb" class="icon icon-fb"></li>
    																						<li data-type="vk" class="icon icon-vk"></li>
    																						<li data-type="tg" class="icon icon-telegram"></li>
                                            </ul> -->
                                        </div>
                                    </div>

                                    <p class="t14 m0">Если Вы выполнили домашнее задание и посетили скайп-сессию, можете переходить к следующему блоку.</p>
                                </div>
                            </div>
                            <?if(!$props["HIDE_NEXT_COURSE"]["VALUE"]):?>
                                <div class="learn_page_bottom_more more_container"<?if(!$showFirstStep):?> style="display: block"<?endif;?>>
                                    <?if(checkPaySeminar($props["OPEN_ACCESS"]["VALUE"])):?>
                                        <p class="t24 strong">Перейти к&nbsp;следующему курсу</p>
                                        <a class="button" href="/learning/<?=$props["OPEN_ACCESS"]["VALUE"]?>/">Далее <span class="icon icon-arr_right"></span></a>
                                    <?else:?>
                                        <?
                                        $db_props = CIBlockElement::GetProperty(9, $props["OPEN_ACCESS"]["VALUE"], array("sort" => "asc"), Array("CODE"=>"LINK"));
                                        if($ar_props = $db_props->Fetch()):
                                            ?>
                                            <p class="t24 strong">Приобрести следующий курс</p>
                                            <a class="button" href="<?=$ar_props["VALUE"]?>">Далее <span class="icon icon-arr_right"></span></a>
                                        <?endif?>
                                    <?endif?>
                                </div>
                            <?endif;?>
                        <?endif;?>

                        <div class="confidencial_element form_element mt50 mb20">
                            <div class="oferta_gs" style="padding-left: 0;">
                            	  <?if($arFields["ID"]==13823):?>
										  <span data-toggle="modal" data-target="#footer21019">Работа в рамках годового сопровождения проводится согласно оферте на учебный процесс</span>
										  <?else:?>
										  <span data-toggle="modal" data-target="#footer21019">Работа в рамках данного курса проводится согласно оферте на учебный процесс</span>
										  <?endif;?>
                            </div>
                        </div>

                    </div>
                    <!-- *****КАРКАС***** -->
                </div>
            </div>

            <div class="sidebar">
                <div class="sidebar_inner">
                    <form class="innerpage_search_form" method="GET">
                        <div class="form_element">
                            <input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Название / тикер / ISIN" name="q" id="autocomplete_all" />
                            <button type="submit"><span class="icon icon-search"></span></button>
                        </div>
                    </form>
                    <p class="title">
						<?=$arSect["CODE"]?>
                    </p>
                    <p class="lightgray"><?=$arSect["DESCRIPTION"]?></p>

					<?if($_REQUEST["SEMINAR_ID"]){
						$arFilter = Array("IBLOCK_ID"=>6, "SECTION_ID"=>21, "PROPERTY_SEMINAR"=>$_REQUEST["SEMINAR_ID"], "INCLUDE_SUBSECTIONS"=>"Y", "ACTIVE"=>"Y");
					} else {
						$arFilter = Array("IBLOCK_ID"=>6, "SECTION_ID"=>21, "ACTIVE"=>"Y", "PROPERTY_SEMINAR"=>false);
					}
					$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
					while($ob = $res->GetNextElement())
					{
						$arFields = $ob->GetFields();
						$props = $ob->GetProperties();
						$img = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>242, 'height'=>162), BX_RESIZE_IMAGE_EXACT, true);
						?>

						<?if($props["VIDEO"]["VALUE"]):?>
                            <div class="sidebar_element no_separator">
                                <div class="sidebar_article_img_outer">
                                    <div class="sidebar_article_img">
                                        <a class="fancyb fancybox.iframe" href="https://www.youtube.com/embed/<?=$props["VIDEO"]["VALUE"]?>" rel="gallery"><img src="<?=$img["src"]?>" alt="" /></a>
                                    </div>

                                    <p class="sidebar_article_img_title"><a class="fancyb fancybox.iframe" href="https://www.youtube.com/embed/<?=$props["VIDEO"]["VALUE"]?>" rel="gallery"><?=$arFields["NAME"]?></a></p>
                                </div>
                            </div>
                        <?else:?>
                            <div class="sidebar_element no_separator">
                                <div class="sidebar_article_img_outer">
                                    <div class="sidebar_article_img">
                                        <a href="<?=$props["LINK"]["VALUE"]?>" target="_blank"><img src="<?=$img["src"]?>" alt="" /></a>
                                    </div>

                                    <p class="sidebar_article_img_title"><a href="<?=$props["LINK"]["VALUE"]?>" target="_blank"><?=$arFields["NAME"]?></a></p>
                                </div>
                            </div>
                        <?endif?>
						<?
					}
					?>
                </div>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
