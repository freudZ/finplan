<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
if (!$USER->IsAuthorized() || !$_POST["review"] || !intval($_POST["item"])){
	exit();
}

CModule::IncludeModule("iblock");

$arFilter = Array("IBLOCK_ID"=>9, "ID"=>intval($_POST["item"]), "!PROPERTY_BONUS"=>false, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
if($ob = $res->GetNextElement())
{
	$seminar = $ob->GetFields();
	$props = $ob->GetProperties();
} else {
	exit();
}

$arFilter = Array("IBLOCK_ID"=>11, "PROPERTY_ITEM"=>$seminar, "PROPERTY_USER"=>$USER->GetID(), "!PROPERTY_PAYED"=>false, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
if($ob = $res->GetNextElement())
{
	$item = $ob->GetFields();
	CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 11, "BONUS");
}


$arEventFields = array(
    "USER_EMAIL" => $USER->GetEmail(),
    "SEMINAR"  => $seminar["NAME"],
    "MES"  => strip_tags($_POST['review']),
    "FIO"  => $USER->GetFullName(),
);
CEvent::Send("BONUS_REVIEW", SITE_ID, $arEventFields);
?>

<span>ВАШ ПЕРСОНАЛЬНЫЙ БОНУС</span>
<p>
	<?=$props["BONUS"]["~VALUE"]["TEXT"]?>
</p>