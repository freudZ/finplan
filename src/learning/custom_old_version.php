<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Семинары по инвестициям, обучение инвестициям, Купить семинары по инвестициям, вебинары по инвестициям, семинары по инвестициям на фондовом рынке");
$APPLICATION->SetPageProperty("description", "Компания FIn-plan представляет уникальный цикл семинаров по инвестициям. За 3 недели обучения мы помогаем Вам выйти на профессиональный уровень инвестиций и зарабатывать 30% годовых с минимальными рисками и затратами времени.");
$APPLICATION->SetTitle("Семинары по инвестициям");
?>

<?CModule::IncludeModule("iblock");
$arFilter = Array("IBLOCK_ID"=>14, "CODE"=>$_REQUEST["ELEMENT_CODE"], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
if($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$arFields["PROPS"] = $ob->GetProperties();
	
	$res = CIBlockElement::GetByID($arFields["PROPS"]["ITEM"]["VALUE"]);
	$arItem = $res->GetNext();
} else {
	LocalRedirect("/learning/");
}
?>
<div class="content-main col-md-8 no-float">

	<? CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>8, "ACTIVE"=>"Y", "!PROPERTY_FOR_LEARNING"=>false);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
	if($ob = $res->GetNextElement())
	{
	$arFields2 = $ob->GetFields();
	$props2 = $ob->GetProperties();
	?>
	<div class="content-main-banner" style="background: url('<?= CFile::GetPath($props2["IMAGE"]["VALUE"]); ?>') -15px 0px no-repeat; background-color: #<?=$props2["COLOR"]["VALUE"]?>; background-position: right center;">
	  <div class="content-main-banner-wrapper">
		<div class="content-main-banner-text">
		<?if($props2["TEXT"]["VALUE"]):?>
			<?= $props2["TEXT"]["VALUE"]; ?>
		<?endif?>
		</div>
		<div class="content-main-banner-start">
			<a <? if(!empty($props2["LINK_BLANK"]["VALUE"])) { ?> target="_blank" <? } ?> <? if(!empty($props2["LINK_NOFOLLOW"]["VALUE"])) { ?> rel="nofollow" <? } ?>
			href="<?= $props2["LINK_URL"]["VALUE"]; ?>"><?= $props2["LINK_TEXT"]["VALUE"]; ?></a>
		</div>
	  </div>
	</div>
	<?
	}
	?>	
	
	<div class="content-main-wrapper not_set_height">
		<div style="margin-top:50px"></div>
		<div id="textid" style="width:521px;background:#fff;padding-left: 59px;padding-right: 59px;padding-top: 30px;padding-bottom:30px;margin:0 auto;">
		  <div class="supform">
			<div class="order-form">Форма заказа</div>
			<div class="form-sp">
				<?if($arFields["PROPS"]["SITE"]["VALUE"]):?>
					<?=$arFields["PROPS"]["SITE"]["VALUE"]?>
				<?else:?>
					<?=$arItem["NAME"]?>
				<?endif?>
			</div>
			<div class="form-price"><?=$arFields["PROPS"]["PRICE"]["VALUE"]?> руб.</div>
			<div class="fio">
				<p class="fio-p">ФИО *:
				 <input type="text" name="name" value="<?=$USER->GetFullName()?>">
				</p>
			</div>
			<div class="fio">
				<p class="fio-p">Телефон *:
				 <input type="text" name="phone">
				</p>
			</div>
			<?if(!$USER->IsAuthorized()):?>
				<div class="fio">
					<p class="fio-p">E-mail *:
					 <input type="text" name="email" class="check_mail" autocomplete="off">
					</p>
				</div>
				<div class="fio password" style="display:none">
					<p class="fio-p">Пароль *:
						<input type="text" name="pass" class="pass_val">
					</p>
					<p class="fio-p" style="text-align:center">
						<button class="payor login" style="border:0;">Войти</button>
					</p>
					<p style="text-align:center">Вы уже зарегистрированы на сайте Fin-plan.org, введите свой пароль для авторизации.</p>
					<p class="err" style="display:none;text-align:center;color:red">Не верный логин или пароль</p>
				</div>
			<?endif?>
			<input type="hidden" name="id" id="sem_id" value="<?=$arFields["ID"]?>">
			<div class="comnet">
			  <p>Комментарии к заказу:<br>
			  <textarea name="comment"></textarea></p>
			</div>
			<div class="ratio">
				<p>Выберите способ оплаты:</p>
				<?CModule::IncludeModule("iblock");
				$arFilter = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array("sort"=>"asc"), $arFilter, false, false);
				while($ob = $res->GetNextElement()):
					$i++;
					$item = $ob->GetFields();
					$props = $ob->GetProperties();?>
					<div>
					<input class="r_button" type="radio" value="<?=$props["YA_CODE"]["VALUE"]?>" name="pay_type" id="r_<?=$item["ID"]?>"<?if($i==1):?> checked<?endif?> />
					<label for="r_<?=$item["ID"]?>"><?=$item["NAME"]?></label>
					</div>
				<?endwhile?>
			</div>
			<p id="form_reg_error" style="display:none;color:red;margin-top:10px">Заполните корректно обязательные поля</p>
			<div class="payor" id="pay_me" style="cursor:pointer">ОПЛАТИТЬ</div>
		  </div>
		</div>
	</div>

<script>
$("input[name=phone]").mask("+7 (999) 999-99-99");
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>