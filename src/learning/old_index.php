<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Семинары по инвестициям, обучение инвестициям, Купить семинары по инвестициям, вебинары по инвестициям, семинары по инвестициям на фондовом рынке");
$APPLICATION->SetPageProperty("description", "Компания FIn-plan представляет уникальный цикл семинаров по инвестициям. За 3 недели обучения мы помогаем Вам выйти на профессиональный уровень инвестиций и зарабатывать 30% годовых с минимальными рисками и затратами времени.");
$APPLICATION->SetTitle("Семинары по инвестициям");
exit();
?>


<div class="content-main col-md-8 no-float">
	<? CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>8, "ACTIVE"=>"Y", "!PROPERTY_FOR_LEARNING"=>false);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
	if($ob = $res->GetNextElement())
	{
	$arFields = $ob->GetFields();
	$props = $ob->GetProperties();
	?>
	<div class="content-main-banner" style="background: url('<?= CFile::GetPath($props["IMAGE"]["VALUE"]); ?>') -15px 0px no-repeat; background-color: #<?=$props["COLOR"]["VALUE"]?>; background-position: right center;">
	  <div class="content-main-banner-wrapper">
		<div class="content-main-banner-text">
		<?if($props["TEXT"]["VALUE"]):?>
			<?= $props["TEXT"]["VALUE"]; ?>
		<?endif?>
		</div>
		<div class="content-main-banner-start">
			<a <? if(!empty($props["LINK_BLANK"]["VALUE"])) { ?> target="_blank" <? } ?> <? if(!empty($props["LINK_NOFOLLOW"]["VALUE"])) { ?> rel="nofollow" <? } ?>
			href="<?= $props["LINK_URL"]["VALUE"]; ?>"><?= $props["LINK_TEXT"]["VALUE"]; ?></a>
		</div>
	  </div>
	</div>
	<?
	}
	?>	
	<div class="">
		<div class="wrap1">
			<div class="content-main-h1-container">
				<h1 class="content-main-h1">СЕМИНАРЫ ПО ИНВЕСТИЦИЯМ ОТ КОМПАНИИ Fin-plan</h1>
            </div>
		</div>
		<div class="content-main-wrapper not_set_height">
			<div class="learning_text">
				<?include("top_text.php")?>
			</div>
			<?CModule::IncludeModule("iblock");
			$arFilter = Array("IBLOCK_ID"=>9, "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$props = $ob->GetProperties();
				
				$preview = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>60, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);
				
				$payed = checkPaySeminar($arFields["ID"]);
				?>
				<div class="seminar anbg">
                    <div class="wrap1">
						<div class="fan"> 
							<div class="tit-photo<?if($payed):?> payed<?endif?>">
								<div class="one-photo">
									<?if($payed):
										$payed_img = CFile::ResizeImageGet($props['IMG_AFTER_BUY']["VALUE"], array('width'=>60, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);?>
										<img src="<?=$payed_img["src"]?>">
									<?else:?>
										<img src="<?=$preview["src"]?>">
									<?endif?>
								</div>
								<div class="one-title">
									<a href="<?=$arFields["DETAIL_PAGE_URL"]?>"><?=$arFields["NAME"]?></a>
									<div class="gap"><span>Длительность:</span> <span class="spanb"><?=$props["DLIT"]["VALUE"]?></span></div>
								</div>
							</div>
							<div class="vid3<?if($payed):?> payed<?endif?>">
								<?/*if (!$USER->IsAuthorized()):?>
									<a href="#" data-backurl="/learning/" data-toggle="modal" data-target="#loginModal"><div class="one-reg">Регистрация</div></a>
								<?else:?>
									<?if($payed):?>
										<a href="<?=$arFields["DETAIL_PAGE_URL"]?>"><div class="one-reg">Смотреть</div></a>
									<?else:?>
										<a href="javascript:void(0)"><div class="one-reg one-f" data-id="<?=$arFields["ID"]?>">Подробнее</div></a>
									<?endif?>
								<?endif*/?>
								<?if($payed):?>
									<a href="<?=$arFields["DETAIL_PAGE_URL"]?>"><div class="one-reg">Смотреть</div></a>
								<?else:?>
									<a href="javascript:void(0)"><div class="one-reg">Подробнее</div></a>
								<?endif?>
							</div>
							<?/*
							<div class="one-price"><?=$props["PRICE"]["VALUE"]?> руб.</div>
							*/?>
						</div>
						<div class="one-video">
							<?if($props["VIDEO"]["VALUE"] && $props["VIDEO_PIC"]["VALUE"]):
								$video_img = CFile::ResizeImageGet($props["VIDEO_PIC"]["VALUE"], array('width'=>412, 'height'=>270), BX_RESIZE_IMAGE_EXACT, true);?>
								<div class="videoall">
									<div class="nopos">
										<div class="videopn"><img src="<?=$video_img["src"]?>"></div>
										<a class="fancyb fancybox.iframe" href="https://www.youtube.com/embed/<?=$props["VIDEO"]["VALUE"]?>" rel="gallery">
											<div class="playnew"><img src="<?=SITE_TEMPLATE_PATH?>/images/newl2.png"></div>
										</a>
										<div class="videosem"><img src="<?=SITE_TEMPLATE_PATH?>/images/Rectangle-3.png"><p>Посмотрите краткое<br>видео семинара</p></div>
									</div>
								</div>
							<?endif?>
							<div class="inp">
								<div class="videotext">
									<?=$arFields["PREVIEW_TEXT"]?>
									
									<div style="text-align:center"><img style="margin-bottom:20px" src="<?=CFile::GetPath($props["IMG_PRICE"]["VALUE"])?>"></div>
								</div>
							</div>
							<?/*if (!$USER->IsAuthorized()):?>
								<a href="#" data-toggle="modal" data-target="#loginModal"><div class="reg2">ПОЛУЧИТЬ ДОСТУП К ВИДЕО</div></a>
							<?else:?>
								<?if(!$payed):?>
									<a href="#"><div class="reg2 one-f" data-id="<?=$arFields["ID"]?>">ПОЛУЧИТЬ ДОСТУП К ВИДЕО</div></a>
								<?endif?>
							<?endif*/?>
							<?if(!$payed):?>
								<a href="#"><div class="reg2 one-f" data-id="<?=$arFields["ID"]?>">ОПЛАТИТЬ</div></a>
							<?endif?>
							<div class="arrow1">
								<div><img src="<?=SITE_TEMPLATE_PATH?>/images/aroow1.png"></div>
								<div class="arrowin"><a href="">СВЕРНУТЬ</a></div>
							</div>
						</div>  
                    </div>
                </div>
				<?
			}
			?>
			<div class="learning_text">
				<?include("bottom_text.php")?>
			</div>
		</div>
	</div>
	<div class="inform" id="textid">
	  <div class="form-in">
	  <div class="supform">
		<div class="order-form">Форма заказа</div>
		<div class="form-sp"></div>
		<div class="form-price"></div>
		<div class="fio">
			<p class="fio-p">ФИО *:
			 <input type="text" name="name" value="<?=$USER->GetFullName()?>">
			</p>
		</div>
		<div class="fio">
			<p class="fio-p">Телефон *:
			 <input type="text" name="phone">
			</p>
		</div>
		<?if(!$USER->IsAuthorized()):?>
			<div class="fio">
				<p class="fio-p">E-mail *:
				 <input type="text" name="email" class="check_mail" autocomplete="off">
				</p>
			</div>
			<div class="fio password" style="display:none">
				<p class="fio-p">Пароль *:
					<input type="text" name="pass" class="pass_val">
				</p>
				<p class="fio-p" style="text-align:center">
					<button class="payor login" style="border:0;">Войти</button>
				</p>
				<p style="text-align:center">Вы уже зарегистрированы на сайте Fin-plan.org, введите свой пароль для авторизации.</p>
				<p class="err" style="display:none;text-align:center;color:red">Не верный логин или пароль</p>
			</div>
		<?endif?>
		<input type="hidden" name="id" id="sem_id" value="">
		<div class="comnet">
		  <p>Комментарии к заказу:<br>
		  <textarea name="comment"></textarea></p>
		</div>
		<div class="ratio">
			<p>Выберите способ оплаты:</p>
			<?CModule::IncludeModule("iblock");
			$arFilter = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array("sort"=>"asc"), $arFilter, false, false);
			while($ob = $res->GetNextElement()):
				$i++;
				$item = $ob->GetFields();
				$props = $ob->GetProperties();?>
				<input class="r_button" type="radio" value="<?=$props["YA_CODE"]["VALUE"]?>" name="pay_type" id="r_<?=$item["ID"]?>"<?if($i==1):?> checked<?endif?> />
				<label for="r_<?=$item["ID"]?>"><?=$item["NAME"]?></label>
			<?endwhile?>
		</div>
		<p id="form_reg_error" style="display:none;color:red;margin-top:10px">Заполните корректно обязательные поля</p>
		<div class="payor" id="pay_me" style="cursor:pointer">ОПЛАТИТЬ</div>
	  </div>
		</div>
	  <div class="clos-but"><a href="javascript:void(0)" class="textid-show"><img src="<?=SITE_TEMPLATE_PATH?>/images/clos.png"></a></div>
	</div>

<script>
$("input[name=phone]").mask("+7 (999) 999-99-99");
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>