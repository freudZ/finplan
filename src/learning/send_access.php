<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
if (!$USER->IsAuthorized() || !$_POST["review"] || !intval($_POST["item"])){
	exit();
}

CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;


$arFilter = Array("IBLOCK_ID"=>9, "ID"=>intval($_POST["item"]), "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
if($ob = $res->GetNextElement())
{
	$seminar = $ob->GetFields();
	$props = $ob->GetProperties();
} else {
	exit();
}

$hlbl = 11;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch(); 
$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass(); 

$main_query = new Entity\Query($entity);
$main_query->setOrder(array("ID" => "desc"));
$main_query->setFilter(array("UF_USER"=>$USER->GetID(), "UF_SEMINAR"=>$props["OPEN_ACCESS"]["VALUE"]));
$main_query->setSelect(array("*"));
$exec = $main_query->exec();
$exec = new CDBResult($exec);
if($item = $exec->Fetch()){
	$entity_data_class::delete($item["ID"]);
}
			
$arEventFields = array(
    "USER_EMAIL" => $USER->GetEmail(),
    "SEMINAR"  => $seminar["NAME"],
    "MES"  => strip_tags($_POST['review']),
    "FIO"  => $USER->GetFullName(),
);
CEvent::Send("SEMINAR_ACCESS", SITE_ID, $arEventFields);
?>

<?if(checkPaySeminar($props["OPEN_ACCESS"]["VALUE"])):?>
	<a href="/learning/<?=$props["OPEN_ACCESS"]["VALUE"]?>/">Перейти к следующему курсу</a>
<?else:?>
	<?
	$db_props = CIBlockElement::GetProperty(9, $props["OPEN_ACCESS"]["VALUE"], array("sort" => "asc"), Array("CODE"=>"LINK"));
	if($ar_props = $db_props->Fetch()):
	?>
		<a href="<?=$ar_props["VALUE"]?>">Приобрести следующий курс</a>
	<?endif?>
<?endif?>