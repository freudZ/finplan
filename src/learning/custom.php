<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Семинары по инвестициям, обучение инвестициям, Купить семинары по инвестициям, вебинары по инвестициям, семинары по инвестициям на фондовом рынке");
$APPLICATION->SetPageProperty("description", "Компания FIn-plan представляет уникальный цикл семинаров по инвестициям. За 3 недели обучения мы помогаем Вам выйти на профессиональный уровень инвестиций и зарабатывать 30% годовых с минимальными рисками и затратами времени.");
$APPLICATION->SetTitle("Семинары по инвестициям");

global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
?>

<?CModule::IncludeModule("iblock");

//SIMPLE_PAY_PAGE - определяет упрощенную оплату для скрытия части элементов шаблона

/**
 * Получает список оферт по массиву их ID
 *
 * @param  [add type]   $arId
 *
 * @return [add type]
 */
function getOfertaList($arId){

}

$arFilter = Array("IBLOCK_ID"=>14, "CODE"=>$_REQUEST["ELEMENT_CODE"], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
if($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$arFields["PROPS"] = $ob->GetProperties();

	//todo - сделать кеширование запросов
	//Получаем массив ID семинаров и привязанные к ним ID оферт, выделаем текущий элемент
	$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_OFERTA"); //Получаем семинар с привязкой оферты
   $arFilter = Array("IBLOCK_ID"=>IntVal(9));
   $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$arSeminarOferts = array(); //массив семинаров и их оферт
   while($ob = $res->GetNextElement()){
    $arItemCommon = $ob->GetFields();
	 $arSeminarOferts[$arItemCommon["ID"]] = $arItemCommon["PROPERTY_OFERTA_VALUE"];
	 if($arItemCommon["ID"] == $arFields["PROPS"]["ITEM"]["VALUE"]){
		$arItem = $arItemCommon;
	 }
   }

	//Формируем массив Оферт
	$arOferts = array();
	$isRadar = intval($arFields["PROPS"]["RADAR_DAYS"]["VALUE"])>0?true:false;
	if($isRadar==true){ //Если продается Радар - добавим оферту радара
	  //Ищем оферту для радара
		$arSelectOferta = Array("ID", "NAME", "DETAIL_TEXT", "IBLOCK_ID","PROPERTY_IS_RADAR", "PROPERTY_DISPLAY_NAME"); //Оферту для радара
	   $arFilterOferta = Array("IBLOCK_ID"=>IntVal(47), "PROPERTY_IS_RADAR_VALUE"=>'Y');
	   $resOferta = CIBlockElement::GetList(Array(), $arFilterOferta, false, false, $arSelectOferta);
		$arRadarOferta = array();
		while($ob = $resOferta->GetNextElement()){
	    $arRadarOferta = $ob->GetFields();
	   }

		if(count($arRadarOferta)>0){
		  $arOferts[$arRadarOferta["ID"]] = array('NAME'=>$arRadarOferta["NAME"], 'DISPLAY_NAME'=>$arRadarOferta["PROPERTY_DISPLAY_NAME_VALUE"],'DETAIL_TEXT'=>$arRadarOferta["DETAIL_TEXT"]);
		}
	}

	if(!empty($arItem["PROPERTY_OFERTA_VALUE"])){
	  $arOferts[$arItem["PROPERTY_OFERTA_VALUE"]] = array('NAME'=>'', 'DISPLAY_NAME'=>'','DETAIL_TEXT'=>'');
	}

   //Проверяем дополнительные материалы и собираем их ID для получения списка оферт
	 if(!empty($arFields["PROPS"]["DOP_ITEMS"]["VALUE"])){
		foreach($arFields["PROPS"]["DOP_ITEMS"]["VALUE"] as $dopId){
	     $arOferts[$arSeminarOferts[$dopId]] = array('NAME'=>'', 'DISPLAY_NAME'=>'','DETAIL_TEXT'=>'');
		}

	 }



	//Дополняем массив данных по используемым офертам
		$arSelectOferta = Array("ID", "NAME", "DETAIL_TEXT", "IBLOCK_ID","PROPERTY_IS_RADAR", "PROPERTY_DISPLAY_NAME"); //Оферту для радара
	   $arFilterOferta = Array("IBLOCK_ID"=>IntVal(47), "ID"=>array_keys($arOferts));
	   $resOferta = CIBlockElement::GetList(Array(), $arFilterOferta, false, false, $arSelectOferta);
		$arRadarOferta = array();
		while($ob = $resOferta->GetNextElement()){
	    $arRadarOferta = $ob->GetFields();
		 if(empty($arOferts[$arRadarOferta["ID"]]['DISPLAY_NAME'])){
		 	 $arOferts[$arRadarOferta["ID"]] = array('NAME'=>$arRadarOferta["NAME"], 'DISPLAY_NAME'=>$arRadarOferta["PROPERTY_DISPLAY_NAME_VALUE"],'DETAIL_TEXT'=>$arRadarOferta["DETAIL_TEXT"]);
		 }
	   }


} else {
	LocalRedirect("/learning/");
}
?>
<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner">
				<?if(!SIMPLE_PAY_PAGE):?>
			   	<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
				<?endif;?>
                <div class="order_page_top infinite_container green">
                    <div class="clearfix">
                        <h1 class="white m0">Форма заказа</h1>

                        <img src="https://yoomoney.ru/i/html-letters/safe-kassa-logo-white.svg" alt="">
                    </div>
                </div>

                <div class="infinite_container black">
                    <div class="clearfix">
                        <p class="t24 uppercase green m0">Школа разумного инвестирования</p>
                        <p class="t18 light">
							<?if($arFields["PROPS"]["SITE"]["VALUE"]):?>
								<?=$arFields["PROPS"]["SITE"]["VALUE"]?>
							<?else:?>
								<?=$arItem["NAME"]?>
							<?endif?>
                        </p>

                        <p class="order_start_price t34 strong m0"><span></span> руб.</p>
                        <input id="item_id" type="hidden" name="item_id" value="<?=$arFields["ID"]?>" hidden />
                        <?
                        $addictionName = $arFields["PROPS"]["SITE"]["VALUE"]?$arFields["PROPS"]["SITE"]["VALUE"]:$arItem["NAME"];
                        
                        ?>
                        <input id="item_product_name" type="hidden" name="item_product_name" value="Школа разумного инвестирования<?=!empty($addictionName)?'. '.$addictionName:''?>" hidden />
                        <input id="start_price" type="text" value="<?=$arFields["PROPS"]["PRICE"]["VALUE"]?>" hidden disabled />
                    </div>
                </div>
                <!-- *****/КАРКАС***** -->
                <div class="order_page_outer">
                    <form class="order_page_form" method="post">
                        <div class="form_element">
                            <input type="text" name="name" placeholder="ФИО" value="<?=$USER->GetFullName()?>" />
                        </div>

                        <div class="row">
                            <div class="col col-md-<?if($USER->IsAuthorized()):?>12<?else:?>6<?endif?>">
                                <div class="form_element">
                                    <input type="text" name="phone" placeholder="Телефон" />
                                </div>
                            </div>
                            <?if(!$USER->IsAuthorized()):?>
                                <div class="col col-md-6">
                                    <div class="form_element">
                                        <input type="text" name="email" autocomplete="off" placeholder="Электронная почта" class="check_mail" />
                                    </div>
                                </div>
                            <?endif;?>
                        </div>
                        <?if(!$USER->IsAuthorized()):?>
                            <div class="row fio password" style="display:none">
                                <div class="col col-md-12">
                                    Пароль *:
                                    <input type="text" name="pass" class="pass_val">
                                </div>
                                <div class="col col-md-12 text-center" style="margin: 20px 0;">
                                    <button class="payor login button big" style="border:0;">Войти</button>
                                </div>
                                <div class="col col-md-12">
                                    <p style="text-align:center">Вы уже зарегистрированы на сайте Fin-plan.org, введите свой пароль для авторизации.</p>
                                    <p class="err" style="display:none;text-align:center;color:red">Не верный логин или пароль</p>
                                </div>
                            </div>
                        <?endif;?>

                        <div class="form_element textarea_element mt20 mb40" <?if(SIMPLE_PAY_PAGE):?>style="display:none"<?endif;?>>
                            <textarea name="message" placeholder="Комментарии к заказу"></textarea>
                        </div>

                        <hr />

      		<?$showBonusProgram = \Bitrix\Main\Config\Option::get("grain.customsettings","ENABLE_BONUS_PROGRAM");
					if($showBonusProgram == "Y"):?>
							<?if($arFields["PROPS"]["SHOW_PROMO"]["VALUE"] || $arFields["PROPS"]["SHOW_BONUS"]["VALUE"]):?>
								<div class="order_form_promocode_and_bonuses">
									<?if($arFields["PROPS"]["SHOW_PROMO"]["VALUE"]):?>
										<div class="order_form_promocode_outer">
											<div class="form_element">
												<div class="checkbox">
													<input type="checkbox" id="order_form_promocode_check" />
													<label for="order_form_promocode_check">У меня есть промокод</label>
												</div>
											</div>

											<div class="order_form_promocode_inner">
												<input type="text" name="promocode" />
												<button type="button" class="button">Подтвердить</button>
												<div class="mes"></div>
											</div>
										</div>
									<?endif?>

									<hr />

									<?if($arFields["PROPS"]["SHOW_BONUS"]["VALUE"] && $USER->IsAuthorized()):
										$totalBonuses = Bonuses::getTotalBonuses($USER->GetID());?>
										<?if($totalBonuses):?>
											<div class="order_form_bonuses_outer">
												<div class="form_element">
													<div class="checkbox">
														<input type="checkbox" id="order_form_bonuses_check" />
														<label for="order_form_bonuses_check">Использовать бонусные баллы</label>
													</div>
												</div>

												<div class="order_form_bonuses_inner">
													<div class="order_form_bonuses_inner_green">
														<div class="col col_left">
															<p class="t24 light white m0">Мои бонусы: <span class="all_bonuses">0</span></p>
															<input id="all_bonuses" type="text" value="<?=$totalBonuses?>" hidden disabled />
														</div>

														<div class="col col_mid">
															<div class="ui_slider bonuses_slider"></div>
														</div>

														<div class="col col_right">
															<span class="final_bonuses">0</span>
															<input id="bonuses_slider_input" name="bonuses" type="text" hidden disabled />
														</div>
													</div>
												</div>
											</div>
										<?endif?>
									<?endif?>
								</div>
								<hr />
							<?endif?>
						<?endif; //Включить бонусную программу?>
						<?/*
                        <div class="form_element mb50">
                            <div class="checkbox">
                                <input type="checkbox" id="order_form_year" />
                                <label for="order_form_year">Докупить курс «<a href="#" target="_blank">Годовое сопровождение</a>» <span class="strong">со скидкой 50%</span> за <span class="order_form_year_price"></span>&nbsp;руб.</label>
                            </div>
                            <input id="order_form_year_price_val" type="text" value="5000" hidden disabled />
                        </div>

                        */?>
                        <p class="final_price t28 strong">Итого к оплате: <span></span> руб.</p>
                        <input id="final_price_val" type="text" hidden disabled />
                        <input type="hidden" name="id" id="sem_id" value="<?=$arFields["ID"]?>">

                        <hr />

                        <div class="row mt40">
                            <?CModule::IncludeModule("iblock");
                            if($GLOBALS["USER"]->IsAdmin()){
                                $arFilter = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y");
                            } else {
                                $arFilter = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y", "!=PROPERTY_SHOW_FOR_ADMIN_VALUE"=>"Y");
                            }
                            if(isset($_REQUEST["pay_type"]) && $_REQUEST["pay_type"]=="installment"){
                                $arFilter["PROPERTY_INSTALLMENT_VALUE"] = "Y";
                            }
                            $res = CIBlockElement::GetList(Array("sort"=>"asc"), $arFilter, false, false);
                            while($ob = $res->GetNextElement()):
                                $i++;
                                $item = $ob->GetFields();
                                $props = $ob->GetProperties();?>

                                <?if($item["ID"]==65682):?>
                                    <div class="col col-md-6">
                                        <div class="form_element">
                                            <div class="radio">
                                                <input type="radio" id="order_payment_sber" value="sber" name="pay_type" checked />
                                                <label for="order_payment_sber"><?=$item["NAME"]?></label>
                                            </div>
                                        </div>
                                    </div>
                                <?elseif($item["ID"]==523054):?>
                                    <div class="col col-md-6">
                                        <div class="form_element">
                                            <div class="radio">
                                                <input type="radio" id="order_payment_sber" value="sber_credit" name="pay_type" checked />
                                                <label for="order_payment_sber"><?=$item["NAME"]?></label>
                                            </div>
                                        </div>
                                    </div>
                                <?else:?>
                                <div class="col col-md-6">
                                    <div class="form_element">
                                        <div class="radio">
                                            <input type="radio" id="order_payment_<?=$item["ID"]?>" value="<?=$props["YA_CODE"]["VALUE"]?>" name="pay_type"<?if($i==1 && !$haveActive):?> checked<?endif;?> />
                                            <label for="order_payment_<?=$item["ID"]?>"><?=$item["NAME"]?></label>
                                        </div>
                                    </div>
                                </div>
                                <?endif?>
                            <?endwhile;?>
                        </div>

                        <hr />

                        <div class="confidencial_element form_element mt40 mb40">
                            <div class="checkbox">
                                <input type="checkbox" id="confidencial_order_page" name="confidencial_order_page" checked />
                                <label for="confidencial_order_page">Нажимая кнопку, я даю согласие на обработку персональных данных и соглашаюсь <br />с </label><span data-toggle="modal" data-target="#footer356">пользовательским соглашением</span>
                                <label for="confidencial_order_page">, </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
										  <? if($GLOBALS["USER"]->IsAdmin()): ?>
											  <?if(count($arOferts)>0){
												 foreach($arOferts as $ofId=>$ofVal){ ?><label for="confidencial_order_page">, </label><span data-toggle="modal" data-target="#footer<?= $ofId ?>"><?= $ofVal["DISPLAY_NAME"] ?></span>
												<? }
											  } else {?>
											     <label for="confidencial_order_page"> и </label><span data-toggle="modal" data-target="#footer21019">офертой</span>
											  <?}?>
										  <?else:?>
										  <span data-toggle="modal" data-target="#footer21019">офертой</span>
										  <?endif;?>
                            </div>
                        </div>

                        <hr />

                        <div class="submit_element mt30 text-center">
                            <button type="button" id="pay_me" class="button big highlight">Оплатить</button>
                        </div>
                    </form>
                    <div id="hidden-div"></div>
                </div>
                <!-- *****КАРКАС***** -->
            </div>
        </div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
    </div>
</div>

	<div class="load_overlay" style="background: rgba(0,0,0,0.2)">
		<p><span class="radar_animate"><span class="radar_round_inner"></span><span class="radar_round_track"></span><span class="radar_animate_element blue"></span><span class="radar_animate_element green"></span><span class="radar_animate_element orange"></span></span> <span class="radar_txt">Загрузка...</span></p>
	</div>
	<?if(count($arOferts)>0)
	foreach($arOferts as $ofId=>$ofVal):?>
<div class="modal fade modal_lg" id="footer<?= $ofId ?>" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h2><?= $ofVal["NAME"] ?></h2>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		  </div>
		  <div class="modal-body">
			<div class="modal_shares container-fluid">
			  <?= $ofVal["DETAIL_TEXT"] ?>
		   </div>
		  </div>
		</div>
	  </div>
	</div>
	<?endforeach;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
