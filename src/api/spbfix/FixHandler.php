<?php
/*$_SERVER["DOCUMENT_ROOT"] = "/var/www/vh62156/data/www/fin-plan.org";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$_SERVER["SERVER_NAME"] = "fin-plan.org";
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");*/

if (empty($_SERVER["DOCUMENT_ROOT"])) {
    $_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . "/../..");
    $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
    $_SERVER["SERVER_NAME"] = "fin-plan.org";
}

use Bitrix\Highloadblock as HL;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/classes/fpt_crontools.php");
$cCronTools = new fptСrontools;
$cronTaskId = 423328;
$cCronTools->changeStartTime($cronTaskId);
$start = microtime(true);


use \Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

use Bitrix\Main\Entity;
Global $APPLICATION, $DB;
CLogger::FixHandlerErrorData("Getting json data..");
$data = json_decode(file_get_contents('php://input'), true);

$hlblock = HL\HighloadBlockTable::getById(29)->fetch();

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
CModule::IncludeModule("iblock");

$err = '';

if (!$data['data']) {
	mail("alphaprogrammer@gmail.com", (new DateTime())->format('d.m.Y H:i:s')." Не пришли котировки от СПБ", "Требуется вручную запустить выполнение задания из консоли");
	CLogger::FixHandlerErrorData(print_r($data, true));
	$err = 'recieved empty data';
}

//$fptTools = new fptTools();
// Массив полей для добавления
if(count($data['data'])>0)
foreach ($data['data'] as $val) {

    $date = DateTime::createFromFormat('Y-m-d', $val['date']);
    $date = $date->setTime(0, 0, 0);

    $arr = ["UF_ITEM" => $val['ticker'], "UF_OPEN" => $val['open'], "UF_CLOSE" => $val['close'], "UF_HIGH" => $val['high'], "UF_LOW" => $val['low'], "UF_DATE" => $date->format('d.m.Y'),];


    try {
        if (!isUsaWeekendDay($arr["UF_DATE"])) //Проверка на выходной или праздничный день для США
            $result = $entity_data_class::add($arr);
    } catch (Exception $e) {
        $err = $e->getMessage();
    }

	 //Дублируем в таблицу полигона цену для тикера из кросс-списка (те что есть на СПБ и нет на полигоне)
	 if(in_array($val['ticker'] ,$APPLICATION->crossTickers)){
		$query = "INSERT INTO `hl_polygon_actions_data`(`UF_ITEM`, `UF_DATE`, `UF_OPEN`, `UF_CLOSE`, `UF_HIGH`, `UF_LOW`, `UF_VOLUME`, `UF_SHOW_GRAPH`, `UF_MANUAL`) VALUES ('".$val['ticker']."', '".$date->format('Y-m-d')."',
         '".$val['open']."', '".$val['close']."', '".$val['high']."', '".$val['low']."', '', 'Y', '')";
		 $DB->Query($query);
	 }

	 //Прописываем для акции текущую цену, если акция в кросс-списке. Остальные пропишутся утром в 7.15 при парсинге полигона
    if ($val['close'] && in_array($val['ticker'] ,$APPLICATION->crossTickers)) {
        try {
            $arFilter = ["IBLOCK_ID" => 55, "PROPERTY_SECID" => $val['ticker']];
            $res = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "IBLOCK_ID"]);

            if ($item = $res->GetNext()) {
                CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], str_replace(",", ".", $val['close']), "LASTPRICE");
            }
        } catch (Exception $e) {
            $err = $e->getMessage();
        }
    }

}
$finish = microtime(true);
$delta = $finish - $start;

$cCronTools->changeEndTime($cronTaskId);
$cCronTools->changeWorkTime($cronTaskId, round($delta, 2));

if($err){
	mail("alphaprogrammer@gmail.com", "Ошибка FixHandler", implode(",",$err));
	CLogger::Error_FixHandler(print_r($err, true));
}

include("./FixClearCache.php");

/*try{
    require($_SERVER["DOCUMENT_ROOT"] . "/parsers/actions_usa.php");
    usaActionsLoop($data);
} catch (Exception $e){
    $err = $e->getMessage();
}*/

return print_r(['success' => $err ?: true,]);
