<?php
//Прием статуса nodefix с СПБ сервера и запись его в переменную в админке.

if (empty($_SERVER["DOCUMENT_ROOT"])) {
    $_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . "/../..");
    $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
    $_SERVER["SERVER_NAME"] = "fin-plan.org";
}

use Bitrix\Highloadblock as HL;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader;

$data = json_decode(file_get_contents('php://input'), true);


if($data['status']=='ok'){
\Bitrix\Main\Config\Option::set("grain.customsettings","FIX_STATUS",(new DateTime())->format('d.m.Y H:i:s').PHP_EOL.$data['data']);
}
if($data['status']=='restart'){
\Bitrix\Main\Config\Option::set("grain.customsettings","FIX_LAST_RESTART",(new DateTime())->format('d.m.Y H:i:s').PHP_EOL.$data['data']);
}

CLogger::nodefixStatus($data['data']);

return print_r(['success' => $err ?: true,]);