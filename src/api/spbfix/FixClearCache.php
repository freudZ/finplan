<?php
if (empty($_SERVER["DOCUMENT_ROOT"])) {
	$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . "/../..");
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	$_SERVER["SERVER_NAME"] = "fin-plan.org";
}

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);

require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
try {
	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cache->clean("actions_usa_data"); //Убиваем кеш американских акций, что бы пересоздать показатели в динамических данных
	$cache->clean("radar_ratings"); //Очищаем кеш рейтингов акций
	if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
   	$GLOBALS['CACHE_MANAGER']->ClearByTag('usa_graphdata');
} catch (Exception $e) {
	$err = $e->getMessage();
}

$graphCacheFolder = $_SERVER["DOCUMENT_ROOT"]."/bitrix/cache/spbex_graph/";
if (is_dir($graphCacheFolder)) {
    rmdir($graphCacheFolder);
}


return print_r([
	'success' => $err ?: true,
]);
