<?

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');

class CCommonApi extends CApiBase{
    protected $actionsIblockId = 32; //ID инфоблока акций РФ
    protected $actionsUsaIblockId = 55; //ID инфоблока акций США
    protected $IndexesIblockId = 55; //ID инфоблока индексов
    protected $actionsPricesHLblockId = 24; //ID HL инфоблока цен акций
    protected $actionsPricesTableName = "hl_moex_actions_data"; //имя таблицы цен акций
    protected $actionsCandlesTableName = "hl_candle_graph_data"; //имя таблицы цен акций
    protected $actionsUsaPricesHLblockId = 29; //ID HL инфоблока цен акций США

    public function __construct($request, $origin, $version)
    {
    	  Global $APPLICATION;
		  $this->actionsUsaPricesHLblockId = $APPLICATION->usaPricesHlId;
        parent::__construct($request);
        //CModule::IncludeModule("iblock");
        if (isset($this->nargs["filter"])) {
            $this->createFilterArray();
        }
    }

    protected function checkPayRadar()
    {
        $haveRadarAccess = checkPayRadar();
        return $haveRadarAccess;
    }

    protected function checkPayRadarUsa(){
        $haveUSARadarAccess = checkPayUSARadar();
        return $haveUSARadarAccess;
    }

	 protected function checkPayGS($userId = 0){
	  return checkPaySeminar(13823, false, $userId);
	 }

	 protected function checkPayGSUsa($userId = 0)
    {
		return checkPayGSUSA(13823, $userId);
    }
	 
    /**
     * Возвращает код сессии битрикс
     *  /api/v1/getBXSessid
     * @return string
     *
     * @access protected
     */
    protected function getBXSessid()
    {
        return bitrix_sessid();
    }

	 /**
	  * Возвращает список тикеров валют, индексов для динамического вывода над контентом страниц
	  *
	  * @return array массив
	  *
	  * @access public
	  */
	 public function getHeaderTickers(){
	 	 Global $APPLICATION;
		 return $APPLICATION->headerTickers;
	 }


	 /**
	  * Получение списка тикеров индексов для показа на страницах акций
	  *
	  * @return array
	  *
	  * @access public
	  */
	 public function getShowInIndexActions(){
	 	Global $APPLICATION;
		return $APPLICATION->showIndexForActions;
	 }

    //Возвращает текущего пользователя, который авторизован, проверяет его на админа и доступ в радар
    protected function getUser()
    {
        global $USER;
        $arReturn = array();
        if ($this->verb == 'work') {
            $arReturn["USER_ID"] = $USER->GetID();
            $arReturn["AUTHORIZED"] = $USER->IsAuthorized();
            if ($arReturn["AUTHORIZED"]) {
                $arReturn["IS_ADMIN"] = $GLOBALS["USER"]->IsAdmin();
                $arReturn["PAY_RADAR"] = $this->checkPayRadar();
                $arReturn["PAY_RADAR_USA"] = $this->checkPayRadarUsa();
                $arReturn["PAY_GS"] = $this->checkPayGS($USER->GetID());
                $arReturn["PAY_GS_USA"] = $this->checkPayGSUsa($USER->GetID());
            } else {
                $arReturn["IS_ADMIN"] = false;
                $arReturn["PAY_RADAR"] = false;
                $arReturn["PAY_RADAR_USA"] = false;
                $arReturn["PAY_GS"] = false;
            }
        } elseif ($this->verb == 'debug') {
            if (!empty($this->nargs["userId"])) {
                $arReturn["USER_ID"] = $this->nargs["userId"];
                $arReturn["AUTHORIZED"] = $this->nargs["authorized"] == "true" ? true : false;
                $arReturn["IS_ADMIN"] = $this->nargs["isAdmin"] == "true" ? true : false;
                $arReturn["PAY_RADAR"] = $this->nargs["payRadar"] == "true" ? true : false;
                $arReturn["PAY_RADAR_USA"] = $this->nargs["payRadarUsa"] == "true" ? true : false;
                $arReturn["PAY_GS"] = $this->nargs["payGS"] == "true" ? true : false;
            }
        }
        return $arReturn;
    }
    /**
     * Формирует массив фильтра для выборок
     *
     * @return array
     *
     * @access protected
     */
    protected function createFilterArray()
    {
        $arTmp = array();
        if (!is_array($this->nargs["filter"])) {
            $this->nargs["filter"] = array($this->nargs["filter"]);
        }
        if (array_key_exists("filter", $this->nargs) && count($this->nargs["filter"]) > 0) {
            foreach ($this->nargs["filter"] as $fval) {
                $delimiter = '';
                if (strpos($fval, "!=") !== false) {
                    $delimiter = "!=";
                } else {
                    if (strpos($fval, ">=") !== false) {
                        $delimiter = ">=";
                    } else {
                        if (strpos($fval, "<=") !== false) {
                            $delimiter = "<=";
                        } else {
                            if (strpos($fval, "!") !== false) {
                                $delimiter = "!";
                            } else {
                                if (strpos($fval, ">") !== false) {
                                    $delimiter = "<";
                                } else {
                                    $delimiter = "=";
                                }
                            }
                        }
                    }
                }
                $arFval = explode($delimiter, $fval);
                $arTmp[$delimiter . $arFval[0]] = $arFval[1]; //!='false'?:false;
            }
            if (count($arTmp) > 0) {
                $this->nargs["filter"] = $arTmp;
                unset($arTmp, $arFval, $delimiter);
            }
        }
    }

    public function clearProps($arProperties)
    {
        $arPropNeedFields = array(
          "ID",
          "NAME",
          "ACTIVE",
          "CODE",
          "PROPERTY_TYPE",
          "MULTIPLE",
          "USER_TYPE",
          "HINT",
          "PROPERTY_VALUE_ID",
          "VALUE",
          "DESCRIPTION",
          "VALUE_ENUM",
          "VALUE_XML_ID"
        );

        foreach ($arProperties as $pkey => $arProp) {
            $arrDiff = array_intersect_key($arProp, array_flip($arPropNeedFields));
            $arProperties[$pkey] = $arrDiff;
        }

        return $arProperties;
    }

    /**
     * Очищает результат от мусора
     *
     * @param array $arFields
     *
     * @return array
     *
     * @access public
     */
    public function clearResult($arFields)
    {

        foreach ($arFields as $key => $value) {
            if (is_nan($value)) {
                $arFields[$key] = "";
            }
            if (0 === strpos($key, '~')) {
                unset($arFields[$key]);
            } else {
                if (true == is_array($value)) {
                    $arFields[$key] = $this->clearResult($arFields[$key]);
                }
            }
        }

        return $arFields;
    }
    /**
     * выводит на экран отладку при наличии параметра debug:y в запросе
     *
     * @param  [add type]   $data
     *
     * @return [add type]
     *
     * @access protected
     */
    protected function showDebug($data, $description = '')
    {
        if (isset($this->nargs["debug"]) && $this->nargs["debug"] == 'y') {
            echo $description . "<pre  style='color:black; font-size:11px;'>";
            print_r($data);
            echo "</pre>";
        }
    }
}