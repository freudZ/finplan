<?

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');

class CApiActives extends CApiBase
{
    protected $DB;
    protected $actionsIblockId = 32; //ID инфоблока акций РФ
    protected $obligationsIblockId = 27; //ID инфоблока облигаций РФ
    protected $companyIblockId = 26; //ID инфоблока страниц компаний РФ
    protected $companyPagesIblockId = 29; //ID инфоблока страниц компаний РФ
    
    protected $actionsUsaIblockId = 55; //ID инфоблока акций США
    protected $companyUsaIblockId = 43; //ID инфоблока страниц компаний США
    protected $companyUsaPagesIblockId = 44; //ID инфоблока страниц компаний США
    
    protected $IndexesIblockId = 55; //ID инфоблока индексов
    protected $actionsPricesHLblockId = 24; //ID HL инфоблока цен акций
    protected $actionsPricesTableName = "hl_moex_actions_data"; //имя таблицы цен акций
    protected $actionsPricesUsaTableName; //имя таблицы цен акций США
    protected $obligationPricesTableName = "hl_moex_obligations_data"; //имя таблицы с ценами облигаций
    protected $obligationCouponsTableName = "hl_obl_coupons_data"; //имя таблицы с купонами облигаций
    protected $actionsCandlesTableName = "hl_candle_graph_data"; //имя таблицы свечей акций РФ
    protected $actionsUsaCandlesTableName = "hl_candle_graph_data_usa"; //имя таблицы свечей акций США
    protected $actionsDividendsTableName = "hl_obl_action_divid_data"; //имя таблицы с дивидендами
    protected $actionsUsaDividendsTableName = "hl_obl_action_usa_divid_data"; //имя таблицы с дивидендами США
    protected $actionsUsaPricesHLblockId = 29; //ID HL инфоблока цен акций США
    protected $arCurrencySigns = array(); //Массив со знаками валют (формируется в методах получения списков активов)
    public $arFinancialDataLang = array(
      "Оборотные активы"                     => "currentAssets",
      "Активы"                               => "actives",
      "Собственный капитал"                  => "equity",
      "Выручка  тек."                        => "currentRevenue",
      "Выручка прошл. Год"                   => "lastYearRevenue",
      "Прибыль"                              => "profit",
      "EBITDA"                               => "ebitda",
      "Прошлая капитализация"                => "capitalization",
      "PERIOD_YEAR"                          => "periodYear",
      "PERIOD_VAL"                           => "periodVal",
      "PERIOD_TYPE"                          => "periodType",
      "Доля собственного капитала в активах" => "shareOfEquityInAssets",
      "Темп роста активов"                   => "assetGrowthRate",
      "Выручка квартальная"                  => "quarterlyRevenue",
      "Прибыль квартальная"                  => "quarterlyProfit",
      "Выручка за год (скользящая)"          => "yearSlidingRevenue",
      "Прибыль за год (скользящая)"          => "yearSlidingProfit",
      "Рентабельность собственного капитала" => "returnOnEquity",
      "P/E"                                  => "PE",
      "P/B"                                  => "PB",
      "P/Equity"                             => "PEquity",
      "P/Sale"                               => "PSale",
      "Темп роста прибыли"                   => "profitGrowRate",
      "Темп прироста выручки"                => "revenueGrowRate",
      "PEG"                                  => "PEG",
      "AVEREGE_PROFIT"                       => "averageProfit",
      "EV/EBITDA"                            => "EVEBITDA",
      "DEBT/EBITDA"                          => "DEBTEBITDA",
      "CURRENT_PERIOD"                       => "currentPeriod",
    );
    public $arMonths = array(
      "Янв" => "01",
      "Фев" => "02",
      "Мар" => "03",
      "Апр" => "04",
      "Май" => "05",
      "Июн" => "06",
      "Июл" => "07",
      "Авг" => "08",
      "Сен" => "09",
      "Окт" => "10",
      "Ноя" => "11",
      "Дек" => "12"
    );
    
    public function __construct($request, $origin, $version)
    {
        global $APPLICATION, $DB;
        $this->DB = $DB;
        $this->actionsUsaPricesHLblockId = $APPLICATION->usaPricesHlId;
        $this->actionsPricesUsaTableName = $APPLICATION->usaPricesHlId == 53 ? 'hl_polygon_actions_data' : 'hl_spb_actions_data';
        parent::__construct($request);
        //CModule::IncludeModule("iblock");
        if (isset($this->nargs["filter"])) {
            $this->createFilterArray();
        }
    }
    
    protected function checkPayRadar()
    {
        $haveRadarAccess = checkPayRadar();
        return $haveRadarAccess;
    }
    
    protected function checkPayRadarUsa()
    {
        $haveUSARadarAccess = checkPayUSARadar();
        return $haveUSARadarAccess;
    }
    
    protected function checkPayGS($userId = 0)
    {
        return checkPaySeminar(13823, false, $userId);
    }
    
    protected function checkPayGSUsa($userId = 0)
    {
        return checkPayGSUSA(13823, $userId);
    }
    
    /**
     * Формирует массив фильтра для выборок
     *
     * @return array
     *
     * @access protected
     */
    protected function createFilterArray()
    {
        $arTmp = array();
        if (!is_array($this->nargs["filter"])) {
            $this->nargs["filter"] = array($this->nargs["filter"]);
        }
        if (array_key_exists("filter", $this->nargs) && count($this->nargs["filter"]) > 0) {
            foreach ($this->nargs["filter"] as $fval) {
                $delimiter = '';
                if (strpos($fval, "!=") !== false) {
                    $delimiter = "!=";
                } else {
                    if (strpos($fval, ">=") !== false) {
                        $delimiter = ">=";
                    } else {
                        if (strpos($fval, "<=") !== false) {
                            $delimiter = "<=";
                        } else {
                            if (strpos($fval, "!") !== false) {
                                $delimiter = "!";
                            } else {
                                if (strpos($fval, ">") !== false) {
                                    $delimiter = "<";
                                } else {
                                    $delimiter = "=";
                                }
                            }
                        }
                    }
                }
                $arFval = explode($delimiter, $fval);
                $arTmp[$delimiter . $arFval[0]] = $arFval[1]; //!='false'?:false;
            }
            if (count($arTmp) > 0) {
                $this->nargs["filter"] = $arTmp;
                unset($arTmp, $arFval, $delimiter);
            }
        }
    }
    
    
    protected function getCompanyDescription()
    {
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0])) {
            return array("error" => "id param is empty");
        }
        if (isset($this->args[0]) && !empty($this->args[0])) {
            if ($this->verb == "RUS") {
                $arResult = $this->getCompanyDescriptionRus();
            } else {
                if ($this->verb == "USA") {
                    $arResult = $this->getCompanyDescriptionUsa();
                }
            }
            
        }
        return $arResult;
    }
    
    protected function getCompanyDescriptionRus()
    {
        $arResult = array();
        //Получение самой компании
        $arSelect = array("ID", "NAME", "IBLOCK_ID");
        $arFilter = array(
          "IBLOCK_ID"   => IntVal($this->companyIblockId),
          "=ID"         => $this->args[0],
          "ACTIVE_DATE" => "Y",
          "ACTIVE"      => "Y"
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCnt" => 1), $arSelect);
        while ($ob = $res->fetch()) {
            $arResult["COMPANY_ID"] = $ob["ID"];
            $arResult["NAME"] = $ob["NAME"];
        }
        
        
        $arSelect = array("ID", "NAME", "CODE", "IBLOCK_ID", "PREVIEW_TEXT", "PREVIEW_PICTURE");
        $arFilter = array(
          "IBLOCK_ID"   => IntVal($this->companyPagesIblockId),
          "=NAME"       => $arResult["NAME"],
          "ACTIVE_DATE" => "Y",
          "ACTIVE"      => "Y"
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        if ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            foreach ($arProps as $propCode => $propVal) {
                $code = str_replace("PROPERTY_", "", $propCode);
                $code = str_replace("_VALUE", "", $code);
                $arResult[$code] = $propVal["VALUE"];
            }
            $arResult["ID"] = $arFields["ID"];
            $arResult["CODE"] = $arFields["CODE"];
            
            $arResult["PREVIEW_TEXT"] = $arFields["PREVIEW_TEXT"];
            if (!empty($arFields["PREVIEW_PICTURE"])) {
                $arResult["PREVIEW_PICTURE"] = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
            }
            
            
            //облигации компании и еврооблигации
            $arEmitents_id = array_merge(array($arResult["COMPANY_ID"]), $arResult["CHILD_COMPANY_RUS"]);
            $arFilter = array("IBLOCK_ID" => $this->obligationsIblockId, "PROPERTY_EMITENT_ID" => $arEmitents_id);
            $res = CIBlockElement::GetList(array(), $arFilter, false, false,
              array("NAME", "DETAIL_PAGE_URL", "PROPERTY_EMITENT_ID"));
            while ($item = $res->GetNext()) {
                if ($item["PROPERTY_EMITENT_ID_VALUE"] == $arResult["COMPANY_ID"]) {
                    $arResult["OBLIGATIONS"][] = $this->clearResult($item);
                } else {
                    if (in_array($item["PROPERTY_EMITENT_ID_VALUE"], $arResult["CHILD_COMPANY_RUS"])) {
                        $arResult["OBLIGATIONS_EURO"][] = $this->clearResult($item);
                    }
                }
            }
            
            //акции компании
            $arFilter = array("IBLOCK_ID" => $this->actionsIblockId, "PROPERTY_EMITENT_ID" => $arResult["COMPANY_ID"]);
            $res = CIBlockElement::GetList(array(), $arFilter, false, false,
              array("NAME", "DETAIL_PAGE_URL", "PROPERTY_IS_ETF_ACTIVE"));
            while ($item = $res->GetNext()) {
                $isETF = $item["PROPERTY_IS_ETF_ACTIVE_VALUE"] == "Y" || strpos($item["NAME"],
                  "ETF") !== false ? true : false;
                if ($isETF) {
                    $item["DETAIL_PAGE_URL"] = str_replace("/actions/", "/etf/", $item["DETAIL_PAGE_URL"]);
                }
                $arResult["ACTIONS"][] = $this->clearResult($item);
            }
            
            //акции США компании, если она назначена общим эмитентов
            $arResult["ACTIONS_USA"] = array();
            $arFilter = array(
              "IBLOCK_ID"                  => $this->actionsUsaIblockId,
              "PROPERTY_COMMON_EMITENT_ID" => $arResult["COMPANY_ID"]
            );
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
            while ($item = $res->GetNext()) {
                $item["DETAIL_PAGE_URL"] = $item["DETAIL_PAGE_URL"];
                $arResult["ACTIONS_USA"][] = $this->clearResult($item);
            }
            
            
        }
        return $arResult;
    }
    
    protected function getCompanyDescriptionUsa()
    {
        $arResult = array();
        //Получение самой компании
        $arSelect = array("ID", "NAME", "IBLOCK_ID");
        $arFilter = array(
          "IBLOCK_ID"   => IntVal($this->companyUsaIblockId),
          "=ID"         => $this->args[0],
          "ACTIVE_DATE" => "Y",
          "ACTIVE"      => "Y"
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCnt" => 1), $arSelect);
        while ($ob = $res->fetch()) {
            $arResult["COMPANY_ID"] = $ob["ID"];
            $arResult["NAME"] = $ob["NAME"];
        }
        
        
        $arSelect = array("ID", "NAME", "CODE", "IBLOCK_ID", "PREVIEW_TEXT", "PREVIEW_PICTURE");
        $arFilter = array(
          "IBLOCK_ID"   => IntVal($this->companyUsaPagesIblockId),
          "=NAME"       => $arResult["NAME"],
          "ACTIVE_DATE" => "Y",
          "ACTIVE"      => "Y"
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        if ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            foreach ($arProps as $propCode => $propVal) {
                $code = str_replace("PROPERTY_", "", $propCode);
                $code = str_replace("_VALUE", "", $code);
                $arResult[$code] = $propVal["VALUE"];
            }
            $arResult["ID"] = $arFields["ID"];
            $arResult["CODE"] = $arFields["CODE"];
            
            $arResult["PREVIEW_TEXT"] = $arFields["PREVIEW_TEXT"];
            if (!empty($arFields["PREVIEW_PICTURE"])) {
                $arResult["PREVIEW_PICTURE"] = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
            }
            
            //акции компании
            $arFilter = array(
              "IBLOCK_ID"           => $this->actionsUsaIblockId,
              "PROPERTY_EMITENT_ID" => $arResult["COMPANY_ID"]
            );
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
            while ($item = $res->GetNext()) {
                $arResult["ACTIONS"][] = $this->clearResult($item);
            }
            
        }
        return $arResult;
    }
    
    /**
     * Возвращает дивиденды по isin коду акции, маршрутизирует между РФ и Сша
     *
     * @return [add type]  [add description]
     *
     * @access protected
     */
    protected function getCoupons()
    {
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0])) {
            return array("error" => "isin param is empty");
        }
        
        if (!isset($this->args[1]) || empty($this->args[1])) {
            $resO = new Obligations();
            $arRadarData = $resO->getItemByIsin($this->args[0]);
            $initialFacevalue = $arRadarData["PROPS"]["INITIALFACEVALUE"];
        } else {
            $initialFacevalue = $this->args[1];
        }
        
        $isin = $this->args[0];
        $query = "SELECT * FROM `$this->obligationCouponsTableName` WHERE `UF_ITEM` = '$isin'";
        
        $res = $this->DB->Query($query);
        
        while ($item = $res->fetch()) {
            if (empty($item["UF_DATA"])) {
                continue;
            }
            $arCoupons = json_decode($item["UF_DATA"], true);
            
            foreach ($arCoupons as $item) {
                if (!empty($item["Дата"]) && floatval($item["Сумма гашения"]) > 0) {
                    $arCouponsDates[$item["Дата"]] = $item["Сумма гашения"];
                }
                if (trim($item["Примечание"])) {
                    $arResult["LIST_VALUES"][] = array(
                      "NAME"  => "Формула расчета купона",
                      "VALUE" => $item["Примечание"],
                    );
                }
            }
            
            $cup_cnt = 0;
            $cup_page_cnt = 0;
            
            foreach ($arCoupons as $item) {
                
                if (!$item["Дата купона"]) {
                    continue;
                }
                $dt = DateTime::createFromFormat('d.m.Y', trim($item["Дата купона"]));
                
                //  if($dt->getTimeStamp()>time()){
                
                if ($arResult["COUPONS"]) {
                    $t = $arResult["COUPONS"][count($arResult["COUPONS"]) - 1];
                    $nominal = $t["Номинал"] - $t["Гашение"];
                } else {
                    $nominal = floatval($initialFacevalue);
                }
                
                $dtString = $dt->format("d.m.Y");
                $tmp = array(
                  "Дата выплаты" => $dt->format("d.m.Y"),
                  "Номинал"      => $nominal,
                  "Купоны"       => $item["Ставка купона"] / 100 * $nominal * $item["Длительность купона"] / 365,
                  "Гашение"      => $arCouponsDates[$dt->format("d.m.Y")] ? $arCouponsDates[$dt->format("d.m.Y")] : 0,
                  "page"         => $cup_page_cnt,
                  "Неопределен"  => isset($item["Ставка купона"]) && $item["Ставка купона"] == '-' ? 'Y' : 'N'
                );
                
                $tmp["Денежный поток"] = $tmp["Купоны"] + $tmp["Гашение"];
                
                
                $arResult["COUPONS"][] = $tmp;
                
                $cup_cnt++;
                if ($cup_cnt == 20) {
                    $cup_cnt = 0;
                    $cup_page_cnt++;
                }
                
                //   } //if($dt->getTimeStamp()>time())
                
            }
            
            $arResult["COUPON_PAGES_CNT"] = $cup_page_cnt;
            
            if ($arResult["COUPONS"]) {
                $arResult["COUPONS_FIELDS"] = array(
                  "Дата выплаты"   => true,
                  "Номинал"        => true,
                  "Купоны"         => true,
                  "Гашение"        => true,
                  "Денежный поток" => true,
                );
                
                $showTotal = true;
                foreach ($arResult["COUPONS"] as $n => $item) {
                    if ($item["Неопределен"] == 'Y') {
                        $arResult["COUPONS"][$n]["Купоны"] = "Купон пока не определен";
                        $showTotal = false;
                    }
                    if (!$item["Денежный поток"]) {
                        $arResult["COUPONS"][$n]["Денежный поток"] = "Зависит от купона";
                        $showTotal = false;
                    }
                    
                    if ($showTotal) {
                        $arResult["COUPONS_TOTAL"]["Купоны"] += $item["Купоны"];
                        $arResult["COUPONS_TOTAL"]["Гашение"] += $item["Гашение"];
                        $arResult["COUPONS_TOTAL"]["Денежный поток"] += $item["Денежный поток"];
                    }
                }
                if (!$showTotal) {
                    unset($arResult["COUPONS_TOTAL"]);
                }
            }
        }
        
        return $arResult;
    }
    
    /**
     * Возвращает дивиденды по isin коду акции, маршрутизирует между РФ и Сша
     *
     * @return [add type]  [add description]
     *
     * @access protected
     */
    protected function getDividends()
    {
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0])) {
            return array("error" => "secid param is empty");
        }
        if (isset($this->args[0]) && !empty($this->args[0])) {
            if ($this->verb == "RUS") {
                $arResult = $this->getDividendsRus();
            } else {
                if ($this->verb == "USA") {
                    $arResult = $this->getDividendsUsa();
                }
            }
            
        }
        return $arResult;
    }
    
    protected function getDividendsRus()
    {
        $arResult = array();
        $code = $this->args[0];
        $query = "SELECT * FROM `$this->actionsDividendsTableName` WHERE `UF_ITEM` = '$code'";
        $res = $this->DB->Query($query);
        if ($elem = $res->fetch()) {
            $arResult["DIVIDENDS"] = json_decode($elem["UF_DATA"], true, JSON_UNESCAPED_UNICODE);
            
            $arResult["DIVIDENDS_FIELDS"] = array(
              "Дата закрытия реестра"       => true,
              "Дата закрытия реестра (T-2)" => true,
              "Цена на дату закрытия"       => true,
              "Дивиденды"                   => true,
              "Дивиденды, в %"              => true,
            
            );
        }
        
        return $arResult;
    }
    
    protected function getDividendsUsa()
    {
        $arResult = array();
        $code = $this->args[0];
        $query = "SELECT * FROM `$this->actionsUsaDividendsTableName` WHERE `UF_ITEM` = '$code'";
        $res = $this->DB->Query($query);
        if ($elem = $res->fetch()) {
            $arResult["DIVIDENDS"] = json_decode($elem["UF_DATA"], true, JSON_UNESCAPED_UNICODE);
            
            $arResult["DIVIDENDS_FIELDS"] = array(
              "Дата закрытия реестра"       => true,
              "Дата закрытия реестра (T-2)" => true,
              "Цена на дату закрытия"       => true,
              "Дивиденды"                   => true,
              "Дивиденды, в %"              => true,
            
            );
        }
        
        return $arResult;
    }
    
    protected function getObligationDetail()
    {
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0])) {
            return array("error" => "isin param is empty");
        }
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 0;//3600;
        $cacheId = $this->CacheTA_ID . 'getObligationDetail' . $this->args[0] . $this->nargs["modify"];
        $isin = $this->args[0];
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else { //проверяем и кешируем
            $arSelect = array("ID", "NAME", "CODE", "IBLOCK_ID", "PREVIEW_PICTURE", "PROPERTY_SECID");
            $arFilter = array(
              "IBLOCK_ID"     => $this->obligationsIblockId,
              "PROPERTY_ISIN" => $isin,
              "ACTIVE_DATE"   => "Y",
              "ACTIVE"        => "Y"
            );
            
            $res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCnt" => 1), $arSelect);
            if ($ob = $res->fetch()) {
                $arResult["OBLIGATION_PICTURE"] = (!empty($ob["PREVIEW_PICTURE"]) ? CFile::GetPath($ob["PREVIEW_PICTURE"]) : null);
                $resO = new Obligations();
                $arRadarData = $resO->getItemByIsin($isin);
                unset($arRadarData["PERIODS"]);
                $arResult["RADAR_DATA"] = $arRadarData;
                $arResult["GRAPH_CANDLE_DATA"] = array();
                unset($arRadarData);
                //Получаем свечи на облиги
                $query = "SELECT * FROM `$this->actionsCandlesTableName` WHERE `UF_TICKER` = '$isin' ORDER BY `UF_DATE_FROM` ASC";
                $res = $this->DB->Query($query);
                
                while ($item = $res->fetch()) {
                    $arResult["GRAPH_CANDLE_DATA"][$item["UF_PERIOD"]][$item["UF_DATE_FROM"]] = $item;
                }
                //Для облиг минфина получаем свечи по secid
                if (count($arResult["GRAPH_CANDLE_DATA"]) == 0) {
                    $secid = $ob["PROPERTY_SECID_VALUE"];
                    $query = "SELECT * FROM `$this->actionsCandlesTableName` WHERE `UF_TICKER` = '$secid' ORDER BY `UF_DATE_FROM` ASC";
                    $res = $this->DB->Query($query);
                    
                    while ($item = $res->fetch()) {
                        $arResult["GRAPH_CANDLE_DATA"][$item["UF_PERIOD"]][$item["UF_DATE_FROM"]] = $item;
                    }
                }
                
                //Max Min - для облиг нет отраслей
                /*                     if(!empty($ob["PROPERTY_PROP_SEKTOR_VALUE"])){
                                       $CIndusrtry = new CIndustriesRus;
                                       $arFields["MIN_MAX"] = $CIndusrtry->arIndustryMinMax[$ob["PROPERTY_PROP_SEKTOR_VALUE"]];
                
                                     }*/
                
            }
            
            
            $cache->set($cacheId, $arResult);
        }
        unset($cache);
        //$this->showDebug($arResult, 'arResult');
        return $this->clearResult($arResult);
    }
    
    
    protected function getActivesList()
    {
        $arResult = array();
        if (!isset($this->verb) || empty($this->verb)) {
            return array("error" => "country verb is empty");
        }
        
        $type = 'shares';
        if (isset($this->args[0]) || !empty($this->args[0])) {
            $type = $this->args[0];
        }
        
        if ($type == 'shares') {
            if ($this->verb == 'USA') {
                $arResult = $this->getActionsUsaList();
            }
            if ($this->verb == 'RUS') {
                $arResult = $this->getActionsList();
            }
        }
        if ($type == 'obligations') {
            $arResult = $this->getObligationsList();
        }
        if ($type == 'shares_etf') {
            //$res = new ETF;
        }
        if ($type == 'shares_pif') {
            //$res = new ETF;
        }
        if ($type == 'futures') {
            //$res = new CFutures;
        }
        if ($type == 'indexes') {
            //$res = new CIndexes;
        }
        if ($type == 'currencies') {
            //$res = new CCurrencyRadar;
        }
        return $arResult;
    }
    
    protected function getActionsList()
    {
        $arResult = array();
        $res = new Actions;
        $pageSize = 100;
        $pageNumber = 1;
        $currencySign = getCurrencySign("RUB");
        //Если передано - устанавливаем размер страницы
        if (isset($this->args[1]) || !empty($this->args[1])) {
            $pageSize = intval($this->args[1]);
        }
        //Если передано - устанавливаем номер страницы
        if (isset($this->args[2]) || !empty($this->args[2])) {
            $pageNumber = intval($this->args[2]);
        }
        
        $arItems = $res->arOriginalsItems;
        usort($arItems, function ($a, $b) {
            
            if ($a["NAME"] == $b["NAME"]) {
                return 0;
            }
            if ((preg_match('/[А-Яа-яЁё]/u', $a["NAME"]) && !preg_match('/[А-Яа-яЁё]/u',
                  $b["NAME"])) || (!preg_match('/[А-Яа-яЁё]/u', $a["NAME"]) && preg_match('/[А-Яа-яЁё]/u',
                  $b["NAME"]))) {
                return ($a["NAME"] > $b["NAME"]) ? -1 : 1;
            } else {
                return ($a["NAME"] < $b["NAME"]) ? -1 : 1;
            }
            
        });
        
        $i = 0;
        foreach ($arItems as $resItem) {
            $i++;
            if ($pageNumber > 1 && $i <= (($pageNumber - 1) * $pageSize)) {
                continue;
            }
            if ($i > ($pageSize * $pageNumber)) {
                break;
            }
            
            $arItem = array(
              "ID"               => $resItem["ID"],
              "NAME"             => $resItem["NAME"],
              "URL"              => $resItem["URL"],
              "ISIN"             => $resItem["SECID"],
              "SECID"            => $resItem["PROPS"]["SECID"],
              "LASTPRICE"        => $resItem["PROPS"]["LASTPRICE"],
              "CURRENCY"         => "RUB",
              "CURRENCY_SIGN"    => $currencySign,
              "QUOTATIONS_MONTH" => $resItem["PROPS"]["QUOTATIONS_MONTH"],
              "MONTH_INCREASE"   => $resItem["DYNAM"]["MONTH_INCREASE"],
              "HAVE_GS"          => $resItem["HAVE_GS"],
            );
            
            
            $arResult["ITEMS"][] = $arItem;
            $arResult["PAGENAV"] = array(
              "TOTAL"        => count($arItems),
              "PAGE_SIZE"    => $pageSize,
              "CURRENT_PAGE" => $pageNumber
            );
            //$arResult[] = $i;
            
        }
        
        return $arResult;
    }
    
    protected function getActionsUsaList()
    {
        $arResult = array();
        $res = new ActionsUsa;
        $pageSize = 100;
        $pageNumber = 1;
        
        //Если передано - устанавливаем размер страницы
        if (isset($this->args[1]) || !empty($this->args[1])) {
            $pageSize = intval($this->args[1]);
        }
        //Если передано - устанавливаем номер страницы
        if (isset($this->args[2]) || !empty($this->args[2])) {
            $pageNumber = intval($this->args[2]);
        }
        
        $arItems = $res->arOriginalsItems;
        setlocale(LC_ALL, '');
        usort($arItems, function ($a, $b) {
            if ($a["NAME"] == $b["NAME"]) {
                return 0;
            }
            if ((preg_match('/[A-Za-z]/u', $a["NAME"]) && !preg_match('/[A-Za-z]/u',
                  $b["NAME"])) || (!preg_match('/[A-Za-z]/u', $a["NAME"]) && preg_match('/[A-Za-z]/u', $b["NAME"]))) {
                return ($a["NAME"] > $b["NAME"]) ? -1 : 1;
            } else {
                return ($a["NAME"] < $b["NAME"]) ? -1 : 1;
            }
        });
        
        $i = 0;
        foreach ($arItems as $resItem) {
            $i++;
            if ($pageNumber > 1 && $i <= (($pageNumber - 1) * $pageSize)) {
                continue;
            }
            if ($i > ($pageSize * $pageNumber)) {
                break;
            }
            
            if (!in_array($resItem["PROPS"]["CURRENCY"], $this->arCurrencySigns)) {
                $this->arCurrencySigns[$resItem["PROPS"]["CURRENCY"]] = getCurrencySign($resItem["PROPS"]["CURRENCY"]);
            }
            $arItem = array(
              "ID"               => $resItem["ID"],
              "NAME"             => $resItem["NAME"],
              "URL"              => $resItem["URL"],
              "ISIN"             => $resItem["SECID"],
              "SECID"            => $resItem["PROPS"]["SECID"],
              "LASTPRICE"        => $resItem["PROPS"]["LASTPRICE"],
              "CURRENCY"         => $resItem["PROPS"]["CURRENCY"],
              "CURRENCY_SIGN"    => $this->arCurrencySigns[$resItem["PROPS"]["CURRENCY"]],
              "QUOTATIONS_MONTH" => $resItem["PROPS"]["QUOTATIONS_MONTH"],
              "MONTH_INCREASE"   => $resItem["DYNAM"]["MONTH_INCREASE"],
              "HAVE_GS"          => $resItem["DYNAM"]["HAVE_GS"],
            );
            
            
            $arResult["ITEMS"][] = $arItem;
            $arResult["PAGENAV"] = array(
              "TOTAL"        => count($arItems),
              "PAGE_SIZE"    => $pageSize,
              "CURRENT_PAGE" => $pageNumber
            );
            //$arResult[] = $i;
            
        }
        return $arResult;
    }
    
    protected function getObligationsList()
    {
        $arResult = array();
        $res = new Obligations;
        $pageSize = 100;
        $pageNumber = 1;
        
        //Если передано - устанавливаем размер страницы
        if (isset($this->args[1]) || !empty($this->args[1])) {
            $pageSize = intval($this->args[1]);
        }
        //Если передано - устанавливаем номер страницы
        if (isset($this->args[2]) || !empty($this->args[2])) {
            $pageNumber = intval($this->args[2]);
        }
        
        $arItems = $res->arOriginalsItems;
        setlocale(LC_ALL, '');
        usort($arItems, function ($a, $b) {
            if ($a["NAME"] == $b["NAME"]) {
                return 0;
            }
            if ((preg_match('/[A-Za-z]/u', $a["NAME"]) && !preg_match('/[A-Za-z]/u',
                  $b["NAME"])) || (!preg_match('/[A-Za-z]/u', $a["NAME"]) && preg_match('/[A-Za-z]/u', $b["NAME"]))) {
                return ($a["NAME"] > $b["NAME"]) ? -1 : 1;
            } else {
                return ($a["NAME"] < $b["NAME"]) ? -1 : 1;
            }
        });
        
        $i = 0;
        foreach ($arItems as $resItem) {
            $i++;
            if ($pageNumber > 1 && $i <= (($pageNumber - 1) * $pageSize)) {
                continue;
            }
            if ($i > ($pageSize * $pageNumber)) {
                break;
            }
            
            if (!in_array($resItem["PROPS"]["CURRENCY"], $this->arCurrencySigns)) {
                $this->arCurrencySigns[$resItem["PROPS"]["CURRENCY"]] = getCurrencySign($resItem["PROPS"]["CURRENCY"]);
            }
            
            $arItem = array(
              "ID"                 => $resItem["ID"],
              "NAME"               => $resItem["NAME"],
              "URL"                => $resItem["URL"],
              "ISIN"               => $resItem["SECID"],
              "SECID"              => $resItem["PROPS"]["SECID"],
              "MATDATE"            => $resItem["PROPS"]["MATDATE"],
              "Доходность годовая" => $resItem["DYNAM"]["Доходность годовая"],
              "HIDEN"              => $resItem["PROPS"]["HIDEN"],
            );
            
            
            $arResult["ITEMS"][] = $arItem;
            $arResult["PAGENAV"] = array(
              "TOTAL"        => count($arItems),
              "PAGE_SIZE"    => $pageSize,
              "CURRENT_PAGE" => $pageNumber
            );
            //$arResult[] = $i;
            
        }
        return $arResult;
    }
    
    /**
     * Возвращает фин.показатели по тикеру
     * @return array
     */
    protected function getFinancialData()
    {
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0])) {
            return array("error" => "secid param is empty");
        }
        
        if (isset($this->args[0]) && !empty($this->args[0])) {
            $activeCode = $this->args[0];
            $isBond = false;
            if ($this->verb == "RUS") {
                $res = new Actions();
            } else {
                if ($this->verb == "RUS_BOND") {
                    $res = new Obligations();
                    $isBond = true;
                } else {
                    if ($this->verb == "USA") {
                        $res = new ActionsUsa();
                    }
                }
            }
            if ($res) {
                if ($isBond) {
                    $arRadarData = $res->getItemByIsin($activeCode);
                } else {
                    $arRadarData = $res->getItemBySecid($activeCode);
                }
                $arResult = $arRadarData["PERIODS"];
                
                //Если передан фильтр конкретного периода или диапазона
                $isRange = false;
                $hasFilter = false;
                $arFilter = array("from" => "");
                if (isset($this->args[1]) && !empty($this->args[1])) {
                    $hasFilter = true;
                    $arFilter["from"] = $this->args[1];
                }
                if (((isset($this->args[1]) && !empty($this->args[1])) && (isset($this->args[2]) && !empty($this->args[2]))) && ($this->args[1] != $this->args[2])) {
                    $isRange = true;
                    $arFilter["from"] = $this->args[1];
                    $arFilter["to"] = $this->args[2];
                }
                
                if ($hasFilter) {
                    if (count($arFilter) == 1 && !$isRange) {
                        $arTmp[$arFilter["from"]] = $arResult[$arFilter["from"]];
                        $arResult = $arTmp;
                    }
                    if ($isRange) {
                        $arTmp = array();
                        $arFilter["from"] = explode("-", $arFilter["from"]);
                        $arFilter["to"] = explode("-", $arFilter["to"]);
                        $fromQuartal = $arFilter["from"][0];
                        $fromYear = $arFilter["from"][0];
                        
                        for ($year = $arFilter["from"][1]; $year <= $arFilter["to"][1]; $year++) {
                            $tmpQuartal = 1;
                            if ($year == $arFilter["from"][1]) {
                                $tmpQuartal = $arFilter["from"][0];
                            }
                            for ($quartal = $tmpQuartal; $quartal <= 4; $quartal++) {
                                $arTmpKey = implode("-", array($quartal, $year, "KVARTAL"));
                                $arTmp[$arTmpKey] = $arResult[$arTmpKey];
                                if ($quartal == 4 || ($year == $arFilter["to"][1] && $quartal == $arFilter["to"][0])) {
                                    break;
                                }
                            }
                        }
                        if (count($arTmp) > 0) {
                            $arResult = $arTmp;
                        }
                    }
                }
                $arTmp = array();
                $arResult = array_map(array($this, 'translateFinancialDataKeys'), $arResult);
                $arTmp["data"] = $arResult;
                $arTmp["columns"] = array_flip($this->arFinancialDataLang);
                $arResult = $arTmp;
                unset($arRadarData, $res, $arTmp, $arTmpKey);
            }
            
            
        }
        
        return $arResult;
    }
    
    protected function getCompanyChartPriceData()
    {
        if ($this->verb == 'RUS') {
            return $this->getCompanyChartPriceDataRus();
        }
        if ($this->verb == 'USA') {
            return $this->getCompanyChartPriceDataUsa();
        }
    }
    
    protected function getCompanyChartPriceDataRus()
    {
        $arResult = array();
        $graph = new MoexGraph();
        $arResult = $graph->getForActionByMonthWithFrom($this->args[0], $this->args[1], true);
        return $arResult;
    }
    
    protected function getCompanyChartPriceDataUsa()
    {
        $arResult = array();
        $graph = new SpbexGraph();
        $arResult = $graph->getForActionByMonthWithFrom($this->args[0], $this->args[1], false, true);
        return $arResult;
    }
    
    /**
     * переводит ключи финпоказателей на английский с camelCase
     * @param $arData
     * @return array
     */
    private function translateFinancialDataKeys($arData)
    {
        $arKeyMap = $this->arFinancialDataLang;
        $arDiffKeys = array_diff_key($arKeyMap, $arData);
        foreach ($arDiffKeys as $mk => $val) {
            unset($arKeyMap[$mk]);
        }
        $arReturn = array_combine(array_values($arKeyMap), array_values($arData));
        return $arReturn;
    }
    
    /**
     * Возвращает детальную информацию об акции, маршрутизирует между РФ и Сша
     *
     * @return [add type]  [add description]
     *
     * @access protected
     */
    protected function getActionDetail()
    {
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0])) {
            return array("error" => "secid param is empty");
        }
        if (isset($this->args[0]) && !empty($this->args[0])) {
            if ($this->verb == "RUS") {
                $arResult = $this->getActionDetailRus();
            } else {
                if ($this->verb == "USA") {
                    $arResult = $this->getActionDetailUsa();
                }
            }
            
        }
        return $arResult;
    }
    
    /**
     * Возвращает актуальную цену актива из базы данных
     * @return array|string[]
     */
    protected function getLastPriceFromDb()
    {
        $arResult = array();
        $tableName = '';
        if (!isset($this->args[1]) || empty($this->args[1])) {
            return array("error" => "secid param is empty");
        }
        if (isset($this->args[0]) && !empty($this->args[0])) {
            if ($this->verb == "RUS") {
                if ($this->args[0] == 'shares') {
                    $tableName = $this->actionsPricesTableName;
                } elseif ($this->args[0] == 'bonds') {
                    $tableName = $this->obligationPricesTableName;
                }
            } else {
                if ($this->verb == "USA") {
                    $tableName = $this->actionsPricesUsaTableName;
                }
            }
            
        } else {
            return array("error" => "typeActive param is empty");
        }
        
        $sql = "SELECT `UF_DATE`, `UF_CLOSE` FROM `" . $tableName . "` WHERE `UF_ITEM` = '" . stripcslashes($this->args[1]) . "' ORDER BY `UF_DATE` DESC LIMIT 1";
        $res = $this->DB->Query($sql);
        if ($row = $res->fetch()) {
            $row['UF_CLOSE'] = floatval($row['UF_CLOSE']);
            $arResult = $row;
        }
        
        return $arResult;
    }
    
    //Возвращает акцию Сша по SECID
    protected function getActionDetailUsa()
    {
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0])) {
            return array("error" => "secid param is empty");
        }
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 3600;
        $cacheId = $this->CacheTA_ID . 'getActionUsaDetail' . $this->args[0] . $this->nargs["modify"];
        $ticker = $this->args[0];
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else { //проверяем и кешируем
            
            $arSelect = array("ID", "NAME", "CODE", "IBLOCK_ID", "PREVIEW_PICTURE", "PROPERTY_SECTOR");
            $arFilter = array(
              "IBLOCK_ID"      => $this->actionsUsaIblockId,
              "PROPERTY_SECID" => $ticker,
              "ACTIVE_DATE"    => "Y",
              "ACTIVE"         => "Y"
            );
            
            $res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCnt" => 1), $arSelect);
            if ($ob = $res->fetch()) {
                $arFields["ACTION_PICTURE"] = (!empty($ob["PREVIEW_PICTURE"]) ? CFile::GetPath($ob["PREVIEW_PICTURE"]) : null);
                $arFields["TYPE"] = !empty($extType) ? $extType : 'share_usa';
                $arFields["TICKER"] = $ticker;
                
                $resAUsa = new ActionsUsa();
                
                $arFields["RADAR_DATA"] = array();
                
                //$this->showDebug($arFields, 'arFields');
                
                $arRadarData = $resAUsa->getItemBySecid($ticker);
                //$this->showDebug($arRadarData, 'RADAR_DATA');
                /*                    global $USER;
                                    $rsUser = CUser::GetByID($USER->GetID());
                                    $arUser = $rsUser->Fetch();
                                    if ($arUser["LOGIN"] == "freud") {
                                        unset($arRadarData["PERIODS"]);
                                        unset($arRadarData["PROPS"]);
                
                                    }*/
                //Получим текущие периоды за квартал и если есть за месяц (для банков)
                unset($arRadarData["PERIODS"]);
                if (array_key_exists("PERIODS", $arRadarData)) {
                    $currentKvartal = array();
                    $currentMonth = array();
                    foreach ($arRadarData["PERIODS"] as $pk => $period) {
                        if (strpos($pk, "KVARTAL") !== false && count($currentKvartal) == 0) {
                            $currentKvartal = $period;
                        }
                        //TODO Избавляемся от банковские периодов по месяцам
                        /*                            if (strpos($pk, "MONTH") !== false && count($currentMonth) == 0) {
                                                        $currentMonth = $period;
                                                    }*/
                        
                    }
                    
                    $arRadarData["CURRENT_PERIOD_KVARTAL"] = $currentKvartal;
                    //$arRadarData["CURRENT_PERIOD_MONTH"] = $currentMonth;
                }
                
                foreach ($arRadarData as $rdk => $rdv) {
                    if (!is_array($rdv)) {
                        $arFields["RADAR_DATA"][$rdk] = $rdv;
                    } else {
                        if (is_array($rdv) && count($rdv) > 0) {
                            $arFields["RADAR_DATA"][$rdk] = $rdv;
                        }
                    }
                }
                //$arFields["RADAR_DATA"] = $arRadarData;
                unset($arRadarData);
                
                //Получаем цены на акции
                $query = "SELECT * FROM `$this->actionsUsaCandlesTableName` WHERE `UF_TICKER` = '$ticker' ORDER BY `UF_DATE_FROM` ASC";
                $res = $this->DB->Query($query);
                
                while ($item = $res->fetch()) {
                    $arFields["GRAPH_CANDLE_DATA"][$item["UF_PERIOD"]][$item["UF_DATE_FROM"]] = $item;
                }
                
                //Max Min
                if (!empty($ob["PROPERTY_SECTOR_VALUE"])) {
                    $CIndusrtry = new SectorsUsa;
                    $arFields["MIN_MAX"] = $CIndusrtry->arIndustryMinMax[$ob["PROPERTY_SECTOR_VALUE"]];
                    
                }
                
            }
            
            
            if ($this->nargs["modify"] == 'full') {
                unset($resAUsa);
            }
            
            
            $arFields["REGION"] = "USA";
            $arResult = $arFields;
            
            $cache->set($cacheId, $arResult);
        }
        unset($cache);
        //$this->showDebug($arResult, 'arResult');
        return $this->clearResult($arResult);
    }
    
    
    //Возвращает акцию РФ по SECID
    protected function getActionDetailRus()
    {
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0])) {
            return array("error" => "secid param is empty");
        }
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 3600;
        $cacheId = $this->CacheTA_ID . 'getActionDetail' . $this->args[0] . $this->nargs["modify"];
        $ticker = $this->args[0];
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else { //проверяем и кешируем
            
            $arSelect = array(
              "ID",
              "NAME",
              "CODE",
              "IBLOCK_ID",
              "PREVIEW_PICTURE",
              "PROPERTY_PROP_SEKTOR",
              "PROPERTY_IS_ETF_ACTIVE",
              "PROPERTY_IS_PIF_ACTIVE"
            );
            $arFilter = array(
              "IBLOCK_ID"      => $this->actionsIblockId,
              "PROPERTY_SECID" => $ticker,
              "ACTIVE_DATE"    => "Y",
              "ACTIVE"         => "Y"
            );
            
            $res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCnt" => 1), $arSelect);
            if ($ob = $res->fetch()) {
                $arFields["ACTION_PICTURE"] = (!empty($ob["PREVIEW_PICTURE"]) ? CFile::GetPath($ob["PREVIEW_PICTURE"]) : null);
                $extType = $ob["PROPERTY_IS_ETF_ACTIVE_VALUE"] == "Y" ? 'etf' : '';
                $extType = $ob["PROPERTY_IS_PIF_ACTIVE_VALUE"] == "Y" ? 'pif' : '';
                $arFields["TYPE"] = !empty($extType) ? $extType : 'share';
                $arFields["TICKER"] = $ticker;
                
                if ($arFields["TYPE"] == "share") {
                    $resA = new Actions();
                } elseif ($arFields["TYPE"] == "etf") {
                    $resA = new ETF();
                } /*elseif ($arFields["TYPE"] == "pif") { //TODO
                        $resA = new PIF();
                    }*/
                
                $arFields["RADAR_DATA"] = array();
                
                //$this->showDebug($arFields, 'arFields');
                
                $arRadarData = $resA->getItemBySecid($ticker);
                //$this->showDebug($arRadarData, 'RADAR_DATA');
                /*                    global $USER;
                                    $rsUser = CUser::GetByID($USER->GetID());
                                    $arUser = $rsUser->Fetch();
                                    if ($arUser["LOGIN"] == "freud") {
                                        unset($arRadarData["PERIODS"]);
                                        unset($arRadarData["PROPS"]);
                
                                    }*/
                //Получим текущие периоды за квартал и если есть за месяц (для банков)
                unset($arRadarData["PERIODS"]);
                if (array_key_exists("PERIODS", $arRadarData)) {
                    $currentKvartal = array();
                    $currentMonth = array();
                    foreach ($arRadarData["PERIODS"] as $pk => $period) {
                        if (strpos($pk, "KVARTAL") !== false && count($currentKvartal) == 0) {
                            $currentKvartal = $period;
                        }
                        //TODO Избавляемся от банковские периодов по месяцам
                        /*                            if (strpos($pk, "MONTH") !== false && count($currentMonth) == 0) {
                                                        $currentMonth = $period;
                                                    }*/
                    }
                    
                    $arRadarData["CURRENT_PERIOD_KVARTAL"] = $currentKvartal;
                    //$arRadarData["CURRENT_PERIOD_MONTH"] = $currentMonth;
                }
                
                foreach ($arRadarData as $rdk => $rdv) {
                    if (!is_array($rdv)) {
                        $arFields["RADAR_DATA"][$rdk] = $rdv;
                    } else {
                        if (is_array($rdv) && count($rdv) > 0) {
                            $arFields["RADAR_DATA"][$rdk] = $rdv;
                        }
                    }
                }
                //$arFields["RADAR_DATA"] = $arRadarData;
                unset($arRadarData);
                
                //Получаем свечи на акции
                $query = "SELECT * FROM `$this->actionsCandlesTableName` WHERE `UF_TICKER` = '$ticker' ORDER BY `UF_DATE_FROM` ASC";
                $res = $this->DB->Query($query);
                
                while ($item = $res->fetch()) {
                    $arFields["GRAPH_CANDLE_DATA"][$item["UF_PERIOD"]][$item["UF_DATE_FROM"]] = $item;
                }
                
                //Max Min
                if (!empty($ob["PROPERTY_PROP_SEKTOR_VALUE"])) {
                    $CIndusrtry = new CIndustriesRus;
                    $arFields["MIN_MAX"] = $CIndusrtry->arIndustryMinMax[$ob["PROPERTY_PROP_SEKTOR_VALUE"]];
                    
                }
            }
            
            if ($this->nargs["modify"] == 'full') {
                unset($resA);
            }
            
            $arFields["REGION"] = "RUS";
            $arResult = $arFields;
            
            $cache->set($cacheId, $arResult);
        }
        unset($cache);
        //$this->showDebug($arResult, 'arResult');
        return $this->clearResult($arResult);
    }
    
    
}


