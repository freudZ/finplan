<?

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');

class CRadarApi extends CApiBase{
    protected $actionsIblockId = 32; //ID инфоблока акций РФ
    protected $actionsUsaIblockId = 55; //ID инфоблока акций США
    protected $IndexesIblockId = 55; //ID инфоблока индексов
    protected $actionsPricesHLblockId = 24; //ID HL инфоблока цен акций
    protected $actionsPricesTableName = "hl_moex_actions_data"; //имя таблицы цен акций
    protected $actionsCandlesTableName = "hl_candle_graph_data"; //имя таблицы цен акций
    protected $radarFilterValuesTableName = "hl_radar_filter_values"; //имя таблицы со списками значений для фильтров радара
    protected $actionsUsaPricesHLblockId = 29; //ID HL инфоблока цен акций США

    public function __construct($request, $origin, $version)
    {
    	  Global $APPLICATION;
		  $this->actionsUsaPricesHLblockId = $APPLICATION->usaPricesHlId;
        parent::__construct($request);
        //CModule::IncludeModule("iblock");

    }


	 /**
	  * Возвращает массив значений для контролов фильтра
	  *
	  * @return array
	  *
	  * @access protected
	  */
	 protected function getFilterValues(){
	  $arResult = array("bonds"=>array(), "shares"=>array(), "shares_usa"=>array(), "etf"=>array(), "pif"=>array(), "features"=>array());
	  Global $APPLICATION, $DB;

	  /* Акции */
	  $arResult["default"]["shares"]["pe"]=$APPLICATION->obligationDefaultForm["pe"];
	  $arResult["default"]["shares"]["profitability"]=$APPLICATION->obligationDefaultForm["profitability"];
		$CPresets = new RadarPresets;
	   $arPresetsList = array();
		$arPresetsList = $CPresets->getPresetSelectArray();
		unset($CPresets);
	  $arResult["presets"] = $arPresetsList;

	  /* Кешированные массивы для списочных значений фильтра */
	  $ResActionsRus = new Actions();//Получаем массив списочных свойств для вывода
	  $arFilterEnumsRus = $ResActionsRus->getRadarFiltersValues();
	  unset($ResActionsRus);
	  $arResult["shares"]["enum_values"] = $arFilterEnumsRus;

	  /* Получение статичных значений фильтров */
	  $query = "SELECT * FROM `$this->radarFilterValuesTableName` ORDER BY `UF_TYPE` ASC, `UF_PARAM_CODE` ASC, `UF_SORT` ASC";
	  $res = $DB->Query($query);

	  while($row = $res->fetch()){
		 $arResult[$row["UF_TYPE"]][$row["UF_PARAM_CODE"]][] = $row;
	  }


	  return $arResult;
	 }

	 protected function getActionsList(){
	 	$page = 1;
        if (isset($this->args[0]) && !empty($this->args[0])) {
            $page = $this->args[0];
        }
		$arResult = array();
		$res = new Actions();
		$arResult = $res->getTable($page);
		return  $arResult;
	 }


}