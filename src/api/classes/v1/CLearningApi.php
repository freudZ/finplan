<?

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');

class CLearningApi extends CApiBase
{
    protected $DB;
    protected $seminarIblockId = 9; //ID инфоблока семинаров (курсов)
    protected $actionsIblockId = 32; //ID инфоблока акций РФ
    protected $actionsUsaIblockId = 55; //ID инфоблока акций США
    protected $IndexesIblockId = 55; //ID инфоблока индексов
    protected $actionsPricesHLblockId = 24; //ID HL инфоблока цен акций
    protected $actionsPricesTableName = "hl_moex_actions_data"; //имя таблицы цен акций
    protected $actionsCandlesTableName = "hl_candle_graph_data"; //имя таблицы цен акций
    protected $actionsUsaPricesHLblockId = 29; //ID HL инфоблока цен акций США
    
    protected $coursesTableName = 'hl_cources';
    protected $tagsLibTableName = "hl_tags_lib";
    protected $lessonsTableName = 'hl_lessons';
    protected $testsTableName = 'hl_learning_tests';
    protected $questionsTableName = 'hl_learning_questions';
    protected $questionTypesTableName = 'hl_learning_question_types';
    protected $answersVariantsTableName = 'hl_learning_answers_variants';
    protected $userAnswersTableName = 'hl_learning_user_answers';
    protected $viewVideoStatTableName = 'hl_learning_video_stat';
    protected $videoTableName = 'hl_learning_video';
    protected $learningStaTableName = 'hl_learning_stat';
    
    public function __construct($request, $origin, $version)
    {
        global $APPLICATION, $DB;
        $this->DB = $DB;
        $this->actionsUsaPricesHLblockId = $APPLICATION->usaPricesHlId;
        parent::__construct($request);
        //CModule::IncludeModule("iblock");
        if (isset($this->nargs["filter"])) {
            $this->createFilterArray();
        }
    }
    
    protected function checkPayRadar()
    {
        $haveRadarAccess = checkPayRadar();
        return $haveRadarAccess;
    }
    
    protected function checkPayRadarUsa()
    {
        $haveUSARadarAccess = checkPayUSARadar();
        return $haveUSARadarAccess;
    }
    
    protected function checkPayGS($userId = 0)
    {
        return checkPaySeminar(13823, false, $userId);
    }
    
    protected function checkPayGSUsa($userId = 0)
    {
        return checkPayGSUSA(13823, $userId);
    }
    
    /**
     * Возвращает словарь тегов для использования в фильтрах курсов и материалов
     * @return array
     */
    protected function getTagsLibrary($byIdKeys = false)
    {
        $arResult = array();
        
        $query = "SELECT * FROM `$this->tagsLibTableName` ORDER BY `UF_TYPE` ASC, `UF_CODE` ASC";
        $res = $this->DB->Query($query);
        
        while ($row = $res->fetch()) {
            $code = $byIdKeys ? $row["ID"] : $row["UF_CODE"];
            $arResult[$row["UF_TYPE"]][$code] = array(
              "id"   => $row["ID"],
              "code" => $row["UF_CODE"],
              "name" => $row["UF_NAME"]
            );
        }
        
        return $arResult;
    }
    
    /**
     * api
     * Список курсов (семинары)
     *
     * @return [add type]  [add description]
     *
     * @access protected
     */
    protected function getCoursesList()
    {
        $arResult = array();
        if ($this->verb == full) {
            $sql = "SELECT * FROM `" . $this->coursesTableName . "` WHERE `UF_ACTIVE` = 1 ORDER BY `UF_SORT`";
        } else {
            $sql = "SELECT `ID`, `UF_NAME`, `UF_DESCRIPTION`, `UF_PICTURE`, `UF_PRICE`, `UF_SHORT_VIDEO`, `UF_SHORT_PICTURE`,
            `UF_LINK`, `UF_DURATION`, `UF_ACCESS_AFTER_COURSES`, `UF_ACCESS_GS_USA`, `UF_ACCESS_GS_RUS`, `UF_AVAILABLE_FOR_FREE`,
             `UF_AVAILABLE_FOR_AUTH`, `UF_HIDE_NO_AUTH`, `UF_CATEGORY`, `UF_DIFFICULTY` FROM `" . $this->coursesTableName . "` WHERE `UF_ACTIVE` = 1 ORDER BY `UF_SORT`";
        }
        $res = $this->DB->Query($sql);
        //Получим словарь
        $arTagsLib = $this->getTagsLibrary(true);
        
        //Если требуется список уроков
        $arLessons = array();
        if (isset($this->args[0]) && !empty($this->args[0])) {
            if ($this->args[0] == 'y') {
                $arLessons = $this->getLessons();
                /*                $sqlLessons = "SELECT * FROM `".$this->lessonsTableName."` WHERE `UF_ACTIVE` = 1 ORDER BY `UF_SORT`";
                                $resLessons = $this->DB->Query($sqlLessons);
                                while($rowLesson = $resLessons->fetch()){
                                    $arLessons[$rowLesson["UF_COURSE_ID"]][] = $rowLesson;
                                }*/
            }
            
        }
        
        while ($ob = $res->fetch()) {
            if (intval($ob["UF_PICTURE"])) {
                $ob["UF_PICTURE"] = CFile::GetPath($ob["UF_PICTURE"]);
            }
            if (intval($ob["UF_SHORT_PICTURE"])) {
                $ob["UF_SHORT_PICTURE"] = CFile::GetPath($ob["UF_SHORT_PICTURE"]);
            }
            if (intval($ob["UF_CATEGORY"])) {
                $ob["UF_CATEGORY"] = $arTagsLib["category"][intval($ob["UF_CATEGORY"])];
            }
            if (intval($ob["UF_DIFFICULTY"])) {
                $ob["UF_DIFFICULTY"] = $arTagsLib["difficulty"][intval($ob["UF_DIFFICULTY"])];
            }
            $ob["LESSONS"] = array();
            if (isset($this->args[0]) && !empty($this->args[0])) {
                if ($this->args[0] == 'y') {
                    if (count($arLessons) && array_key_exists(intval($ob["ID"]), $arLessons)) {
                        $ob["LESSONS"] = $arLessons[$ob["ID"]];
                    }
                }
            }
            /*            $arFieldsResult = array(
                          "name"              => $arFields["NAME"],
                          "description"       => $arFields["PREVIEW_TEXT"],
                          "img"               => $img,
                          "date"              => $arFields["DATE_CREATE"],
                          "price"             => (int)$arFields["PROPERTY_PRICE_VALUE"],
                          "difficulty"        => $arTagsLib["difficulty"][$arFields["PROPERTY_DIFFICULTY_VALUE"]],
                          "category"          => $arTagsLib["category"][$arFields["PROPERTY_CATEGORY_VALUE"]],
                          "isPopular"         => true,
                          "isPurchased"       => true,
                          "previewVideoUrl"   => $arFields["PROPERTY_VIDEO_VALUE"],
                          "previewVideoThumb" => $imgVideoThumb,
                        );*/
            
            $arResult[] = $ob;
        }
        return $arResult;
    }
    
    protected function getCourseDetail()
    {
        $arResult = array();
        $courseId = intval($this->args[0]);
        if ($courseId <= 0) {
            return array("status" => "error", "message" => "empty or wrong course id");
        }
        $sql = "SELECT * FROM `" . $this->coursesTableName . "` WHERE `UF_ACTIVE` = 1 AND `ID` = $courseId ORDER BY `UF_SORT`";
        $res = $this->DB->Query($sql);
        //Получим словарь
        $arTagsLib = $this->getTagsLibrary(true);
        
        //Если требуется список уроков
        $arLessons = array();
        if (isset($this->args[1]) && !empty($this->args[1])) {
            if ($this->args[1] == 'y') {
                $arLessons = $this->getLessons();
                /*                $sqlLessons = "SELECT * FROM `".$this->lessonsTableName."` WHERE `UF_ACTIVE` = 1 ORDER BY `UF_SORT`";
                                $resLessons = $this->DB->Query($sqlLessons);
                                while($rowLesson = $resLessons->fetch()){
                                    $arLessons[$rowLesson["UF_COURSE_ID"]][] = $rowLesson;
                                }*/
            }
            
        }
        
        while ($ob = $res->fetch()) {
            if (intval($ob["UF_PICTURE"])) {
                $ob["UF_PICTURE"] = CFile::GetPath($ob["UF_PICTURE"]);
            }
            if (intval($ob["UF_SHORT_PICTURE"])) {
                $ob["UF_SHORT_PICTURE"] = CFile::GetPath($ob["UF_SHORT_PICTURE"]);
            }
            if (intval($ob["UF_CATEGORY"])) {
                $ob["UF_CATEGORY"] = $arTagsLib["category"][intval($ob["UF_CATEGORY"])];
            }
            if (intval($ob["UF_DIFFICULTY"])) {
                $ob["UF_DIFFICULTY"] = $arTagsLib["difficulty"][intval($ob["UF_DIFFICULTY"])];
            }
            $ob["LESSONS"] = array();
            if (isset($this->args[1]) && !empty($this->args[1])) {
                if ($this->args[1] == 'y') {
                    if (count($arLessons) && array_key_exists(intval($ob["ID"]), $arLessons)) {
                        $ob["LESSONS"] = $arLessons[$ob["ID"]];
                    }
                }
            }
            /*            $arFieldsResult = array(
                          "name"              => $arFields["NAME"],
                          "description"       => $arFields["PREVIEW_TEXT"],
                          "img"               => $img,
                          "date"              => $arFields["DATE_CREATE"],
                          "price"             => (int)$arFields["PROPERTY_PRICE_VALUE"],
                          "difficulty"        => $arTagsLib["difficulty"][$arFields["PROPERTY_DIFFICULTY_VALUE"]],
                          "category"          => $arTagsLib["category"][$arFields["PROPERTY_CATEGORY_VALUE"]],
                          "isPopular"         => true,
                          "isPurchased"       => true,
                          "previewVideoUrl"   => $arFields["PROPERTY_VIDEO_VALUE"],
                          "previewVideoThumb" => $imgVideoThumb,
                        );*/
            
            $arResult = $ob;
        }
        return $arResult;
    }
    
    /**
     * api
     * Возвращает урок кратко или полностью со всеми видео и тестами
     * @return array
     */
    protected function getLessonElement()
    {
        $arResult = array();
        if (isset($this->args[0]) && !empty($this->args[0])) {
            $arResult = $this->getLessons(false, intval($this->args[0]));
        } else {
            return $arResult;
        }
        //Получаем список материалов к уроку
        if (isset($this->verb) && $this->verb == 'full') {
            $arResult["VIDEO"] = $this->getVideoForLesson(false, intval($this->args[0]), 0);
            $arResult["TEST"] = $this->getTestForLesson(false, intval($this->args[0]));
        }
        return $arResult;
    }
    
    /**
     * api
     * Возвращает ответы пользователя на тест
     * @return array
     */
    protected function getUserAnswersForTest()
    {
        $arResult = array();
        $testId = false;
        $uid = false;
        if (isset($this->args[0]) || intval($this->args[0]) > 0) {
            $uid = intval($this->args[0]);
        }
        if (isset($this->args[1]) || intval($this->args[1]) > 0) {
            $testId = intval($this->args[1]);
        }
        $arResult = $this->getUserAnswers($uid, $testId);
        
        return $arResult;
    }
    
    /**
     * Внутренняя функция
     * Возвращает сохраненные пользовательские ответы на вопросы теста
     * Если указать id теста на выходы будет массив ответов без группировки по id теста.
     * в противном случае в возвращаемом массиве элементы будут сгруппированы по ключу с id тестов
     * @param false $uid
     * @param false $testId
     * @return array
     */
    protected function getUserAnswers($uid = false, $testId = false)
    {
        $arResult = array();
        if ($uid == false) {
            return $arResult;
        }
        
        $sql = "SELECT * FROM `" . $this->userAnswersTableName . "` WHERE `UF_USER_ID` = $uid ";
        if ($testId != false) {
            $sql .= " AND `UF_TEST_ID` = " . $testId;
        }
        $sql .= " ORDER BY `UF_TEST_ID`";
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            if ($testId == false) {
                $arResult[$row["UF_TEST_ID"]][] = $row;
            } else {
                $arResult[] = $row;
            }
            
        }
        return $arResult;
    }
    
    /**
     * api
     * Возвращает статистику просмотров видеороликов
     * @return array
     */
    protected function getVideoStatisics()
    {
        $arResult = array();
        $uid = intval($this->args[0]) > 0 ? $this->args[0] : false;
        $lessonId = intval($this->args[1]) > 0 ? $this->args[1] : false;
        $videoId = intval($this->args[2]) > 0 ? $this->args[2] : false;
        
        $arResult = $this->getVideoStats($uid, $lessonId, $videoId);
        return $arResult;
    }
    
    /**
     * Добавляет / обновляет данные о просмотре видеороликов к урокам или faq
     * @return array|string[]
     */
    protected function setVideoStatistics()
    {
        $arResult = array();
        if (intval($this->args[0]) <= 0 || intval($this->args[1]) <= 0 || intval($this->args[2]) <= 0) {
            return array("status" => "error", "message" => "empty param for query");
        }
        $uid = intval($this->args[0]);
        $lessonId = intval($this->args[1]);
        $videoId = intval($this->args[2]);
        $time = "00:00:00";
        if (isset($this->args[3]) && !empty($this->args[3])) {
            $time = $this->args[3];
        }
        $viewed = 0;
        if (isset($this->args[4]) && !empty($this->args[4])) {
            $viewed = $this->args[4] == 'y' ? 1 : 0;
        }
        try {
            $sql = "INSERT INTO `" . $this->viewVideoStatTableName . "` SET `UF_USER_ID`=$uid, `UF_LESSON_ID`=$lessonId, `UF_VIDEO_ID`=$videoId, `UF_TIME`='$time', `UF_VIEWED`=$viewed  ON DUPLICATE KEY UPDATE `UF_TIME`='$time', `UF_VIEWED`=$viewed";
            $res = $this->DB->Query($sql);
            $arResult = array("status" => "ok", "rowId" => intval($this->DB->LastID()));
        } catch (Exception $e) {
            $arResult = array("status" => "error", "message" => $e->getMessage());
        }
        return $arResult;
    }
    
    /**
     * Внутренняя функция
     * Возвращает статистику просмотров видеороликов
     * @param false $uid
     * @param false $lessonId
     * @param false $videoId
     * @return array
     */
    protected function getVideoStats($uid = false, $lessonId = false, $videoId = false)
    {
        $arResult = array();
        if ($uid == false) {
            return $arResult;
        }
        
        $sql = "SELECT * FROM `" . $this->viewVideoStatTableName . "` WHERE `UF_USER_ID` = $uid ";
        
        if ($lessonId != false) {
            $sql .= " AND `UF_LESSON_ID` = " . $lessonId;
        }
        if ($videoId != false) {
            $sql .= " AND `UF_VIDEO_ID` = " . $videoId;
        }
        $sql .= " ORDER BY `UF_LESSON_ID` ASC, `UF_VIDEO_ID` ASC";
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            if ($lessonId == false) {
                $arResult[$row["UF_LESSON_ID"]][$row["UF_VIDEO_ID"]] = $row;
            } else {
                $arResult[$row["UF_VIDEO_ID"]] = $row;
            }
            
        }
        return $arResult;
    }
    
    protected function getLearningStatistics()
    {
        $arResult = array();
        $uid = intval($this->args[0]) > 0 ? $this->args[0] : false;
        $courseId = intval($this->args[1]) > 0 ? $this->args[1] : false;
        $arResult = $this->getLearningStats($uid, $courseId);
        
        return $arResult;
    }
    
    protected function getLearningStats($uid = false, $courseId = false)
    {
        $arResult = array();
        if ($uid == false) {
            return $arResult;
        }
        
        $sql = "SELECT * FROM `" . $this->learningStaTableName . "` WHERE `UF_USER_ID` = $uid ";
        if ($courseId != false) {
            $sql .= " AND `UF_COURSE_ID` = " . $courseId;
        }
        $sql .= " ORDER BY `UF_COURSE_ID` ASC, `UF_DATE` ASC";
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            if (!empty($row["UF_LESSONS"])) {
                $row["UF_LESSONS"] = unserialize($row["UF_LESSONS"]);
            }
            if (!empty($row["UF_TESTS"])) {
                $row["UF_TESTS"] = unserialize($row["UF_TESTS"]);
            }
            $row["UF_SUCCESS_END"] = ($row["UF_SUCCESS_END"] == "0" || empty($row["UF_SUCCESS_END"])) ? false : true;
            if ($courseId == false) {
                $arResult[$row["UF_COURSE_ID"]] = $row;
            } else {
                $arResult = $row;
            }
        }
        return $arResult;
    }
    
    /**
     * Внутренняя функция
     * Возвращает массив с описанием видеороликов согласно условиям
     * @param false $courseId
     * @param false $lessonId
     * @param false $forFaq
     * @return array
     */
    protected function getVideoForLesson($courseId = false, $lessonId = false, $forFaq = false)
    {
        $arResult = array();
        
        $sql = "SELECT * FROM `" . $this->videoTableName . "` WHERE `UF_ACTIVE` = 1 ";
        if ($courseId != false) {
            $sql .= " AND `UF_COURSE_ID` = " . $courseId;
        }
        if ($lessonId != false) {
            $sql .= " AND `UF_LESSON_ID` = " . $lessonId;
        }
        if ($forFaq != false) {
            $sql .= " AND `UF_FAQ_VIDEO` = 1";
        }
        $sql .= " ORDER BY `UF_SORT`";
        
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            $arResult[] = $row;
        }
        return $arResult;
    }
    
    /**
     * Внутренняя функция
     * Возвращает тест или массив тестов к уроку или курсу
     * @param false $courseId
     * @param false $lessonId
     * @return array
     */
    protected function getTestForLesson($courseId = false, $lessonId = false)
    {
        $arResult = array();
        
        $sql = "SELECT * FROM `" . $this->testsTableName . "` WHERE `UF_ACTIVE` = 1 ";
        if ($courseId != false) {
            $sql .= " AND `UF_COURSE_ID` = " . $courseId;
        }
        if ($lessonId != false) {
            $sql .= " AND `UF_LESSON_ID` = " . $lessonId;
        }
        $sql .= " ORDER BY `UF_SORT`";
        
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            $row["QUESTIONS"] = $this->getTestQuestions($row["ID"]);
            $arResult[] = $row;
        }
        return $arResult;
    }
    
    /**
     * Внутренняя функция
     * Возвращает список вопросов для теста $testId
     * @param false $testId
     * @return array
     */
    protected function getTestQuestions($testId = false)
    {
        $arResult = array();
        if ($testId == false) {
            return $arResult;
        }
        
        $arQuestionsType = array();
        $sql = "SELECT * FROM `" . $this->questionTypesTableName . "` ORDER BY `ID`";
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            $arQuestionsType[$row["ID"]] = $row;
        }
        unset($sql, $row, $res);
        
        $sql = "SELECT * FROM `" . $this->questionsTableName . "` WHERE `UF_ACTIVE` = 1 AND `UF_TEST_ID` = $testId ORDER BY `UF_SORT` ASC";
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            if (intval($row["UF_PICTURE"]) > 0) {
                $row["UF_PICTURE"] = CFile::GetPath($row["UF_PICTURE"]);
            }
            $row["UF_TYPE"] = $arQuestionsType[$row["UF_TYPE"]];
            $row["ANSWERS"] = $this->getAnswersForQuestions($row["ID"]);
            $arResult[] = $row;
        }
        
        
        return $arResult;
    }
    
    /**
     * Внутренняя функция
     * Возвращает список ответов для вопроса $questionId
     * @param false $questionId
     * @return array
     */
    protected function getAnswersForQuestions($questionId = false)
    {
        $arResult = array();
        if ($questionId == false) {
            return $arResult;
        }
        
        $sql = "SELECT * FROM `" . $this->answersVariantsTableName . "` WHERE `UF_ACTIVE` = 1 AND `UF_QUESTION_ID` = $questionId";
        $sql .= " ORDER BY `UF_SORT`";
        
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            $arResult[] = $row;
        }
        return $arResult;
    }
    
    /**
     * Внутренняя функция
     * Возвращает список уроков либо уточненный список по $courseId либо конкретный урок по $lessonId
     * @param false $courseId
     * @param false $lessonId
     * @return array
     */
    protected function getLessons($courseId = false, $lessonId = false)
    {
        
        $arResult = array();
        $sql = "SELECT * FROM `" . $this->lessonsTableName . "` WHERE `UF_ACTIVE` = 1 ";
        if ($courseId != false) {
            $sql .= " AND `UF_COURSE_ID` = " . $courseId;
        }
        if ($lessonId != false) {
            $sql .= " AND `ID` = " . $lessonId;
        }
        $sql .= " ORDER BY `UF_SORT`";
        
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            if (!empty($row["UF_MATERIALS"])) {
                $row["UF_MATERIALS"] = explode(";", $row["UF_MATERIALS"]);
            }
            if (!empty($row["UF_ARTICLES"])) {
                $row["UF_ARTICLES"] = explode(";", $row["UF_ARTICLES"]);
            }
            if (!empty($row["UF_COURCES"])) {
                $row["UF_COURCES"] = explode(";", $row["UF_COURCES"]);
            }
            if (!empty($row["UF_LINKS"])) {
                $row["UF_LINKS"] = explode(";", $row["UF_LINKS"]);
            }
            if (!empty($row["UF_FILES"])) {
                $row["UF_FILES"] = explode(";", $row["UF_FILES"]);
                $arTmpFiles = array();
                foreach ($row["UF_FILES"] as $fileId) {
                    $arTmpFiles[] = CFile::GetPath($fileId);
                }
            }
            if (intval($row["UF_PICTURE"]) > 0) {
                $row["UF_PICTURE"] = CFile::GetPath($row["UF_PICTURE"]);
            }
            if ($lessonId != false) {
                $arResult = $row;
            } else {
                $arResult[$row["UF_COURSE_ID"]][$row["ID"]] = $row;
            }
            
        }
        
        return $arResult;
    }
    
    protected function setUserTestAnswer()
    {
        $arResult = array();
        $uid = intval($this->args[0]);
        $testId = intval($this->args[1]);
        $questionId = intval($this->args[2]);
        $questionType = intval($this->args[3]);
        $answerId = intval($this->args[4]);
        $userAnswer = '';
        //UrlEncodedFormData
        if (isset($this->request['userAnswer']) && !empty($this->request['userAnswer'])) {
            $userAnswer = stripcslashes($this->request['userAnswer']);
        }
        
        $sql = "INSERT INTO `" . $this->userAnswersTableName . "` SET `UF_USER_ID`=$uid, `UF_TEST_ID`=$testId, `UF_QUESTION_ID`=$questionId, `UF_TYPE`='$questionType', `UF_ANSWER_ID`=$answerId, `UF_USER_ANSWER`='$userAnswer' ON DUPLICATE KEY UPDATE `UF_ANSWER_ID`=$answerId, `UF_USER_ANSWER`='$userAnswer'";
        
        try {
            $res = $this->DB->Query($sql);
            $arResult = array("status" => "ok", "rowId" => intval($this->DB->LastID()));
        } catch (Exception $e) {
            $arResult = array("status" => "error", "message" => $e->getMessage());
        }
        return $arResult;
    }
    
    /**
     * Формирует массив фильтра для выборок
     *
     * @return array
     *
     * @access protected
     */
    protected function createFilterArray()
    {
        $arTmp = array();
        if (!is_array($this->nargs["filter"])) {
            $this->nargs["filter"] = array($this->nargs["filter"]);
        }
        if (array_key_exists("filter", $this->nargs) && count($this->nargs["filter"]) > 0) {
            foreach ($this->nargs["filter"] as $fval) {
                $delimiter = '';
                if (strpos($fval, "!=") !== false) {
                    $delimiter = "!=";
                } else {
                    if (strpos($fval, ">=") !== false) {
                        $delimiter = ">=";
                    } else {
                        if (strpos($fval, "<=") !== false) {
                            $delimiter = "<=";
                        } else {
                            if (strpos($fval, "!") !== false) {
                                $delimiter = "!";
                            } else {
                                if (strpos($fval, ">") !== false) {
                                    $delimiter = "<";
                                } else {
                                    $delimiter = "=";
                                }
                            }
                        }
                    }
                }
                $arFval = explode($delimiter, $fval);
                $arTmp[$delimiter . $arFval[0]] = $arFval[1]; //!='false'?:false;
            }
            if (count($arTmp) > 0) {
                $this->nargs["filter"] = $arTmp;
                unset($arTmp, $arFval, $delimiter);
            }
        }
    }
    
    protected function getPayedItems()
    {
        global $APPLICATION, $DB;
        
        $arResult = array();
        $uid = $this->args[0];
        $sql = "SELECT `ID` FROM `b_iblock_element` WHERE `IBLOCK_ID` = $this->seminarIblockId AND `ACTIVE` = 'Y'";
        if (isset($this->args[1]) && !empty($this->args[1])) {
            $itemId = intval($this->args[1]);
            $sql .= " AND `ID` = '$itemId'";
        }
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            
            $payed = checkPaySeminar($row['ID'], false, $uid);
            $arResult[$row['ID']] = $payed;
        }
        return $arResult;
    }
    
    protected function getUserProducts()
    {
        
        $arResult = array();
        if (!isset($this->args[0]) || empty($this->args[0]) || intval($this->args[0]) <= 0) {
            return $arResult;
        }
        $uid = $this->args[0];
        $sql = "SELECT `ID`, `VALUE` FROM `b_iblock_element_prop_m19` WHERE `IBLOCK_PROPERTY_ID` = 707 AND `IBLOCK_ELEMENT_ID` = (SELECT `IBLOCK_ELEMENT_ID` FROM `b_iblock_element_prop_s19` WHERE `PROPERTY_339` = $uid)
ORDER BY `b_iblock_element_prop_m19`.`IBLOCK_ELEMENT_ID` ASC";
        //echo $sql;
        $res = $this->DB->Query($sql);
        while ($row = $res->fetch()) {
            $arResult[$row["ID"]] = $row["VALUE"];
        }
        
        return $arResult;
    }
    
    /**
     * выводит на экран отладку при наличии параметра debug:y в запросе
     *
     * @param  [add type]   $data
     *
     * @return [add type]
     *
     * @access protected
     */
    protected function showDebug($data, $description = '')
    {
        if (isset($this->nargs["debug"]) && $this->nargs["debug"] == 'y') {
            echo $description . "<pre  style='color:black; font-size:11px;'>";
            print_r($data);
            echo "</pre>";
        }
    }
}