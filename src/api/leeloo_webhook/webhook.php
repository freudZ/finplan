<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//Вебхук для leeloo, отрабатывает после подписки клиента и пытается найти его в базе CRM по номеру телефона, если находит то присваивает ему LEELOO_ID
CModule::IncludeModule("iblock");
$errors = array();
$input = array();
try {
	$input = \Bitrix\Main\Web\Json::decode($request->getInput());
} catch (Exception $e) {
	$errors[] = $e->getMessage();
}

 if(count($errors)==0){

 define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log/leelooHook_log.txt");
// AddMessage2Log('$input = '.print_r($input, true),'');
	CLogger::subscribe_Events_leeloo_webhook(print_r($input, true));


	foreach($input["events"] as $event=>$data){

	  $pid = $data["data"]["person_id"];
	  $arCrmPeople = findInCRMByPhone($data["data"]["phone"]); //Ищем по вариантам номера телефона в инфоблоке CRM

 		  if($data["data"]["tunnel_name"]=="Radar Info" && count($data["data"]["utm_marks"])>0){ //Если подписка на рассылку уведомлений - пропишем пользователю ID канала коммуникации
            if( array_key_exists("utm_content" ,$data["data"]["utm_marks"]) && intval($data["data"]["utm_marks"]["utm_content"])>0 )
              setUserSubscribeAccountId(intval($data["data"]["utm_marks"]["utm_content"]), $data["data"]["account_id"]);
          }

	  if(count($arCrmPeople)>0 && count($arCrmPeople<5)){
	  	$arLeeLooId["LEELOO_ID"] = '';
	  	foreach($arCrmPeople as $id=>$arLeeLooId){

	  	 	setLeelooIdtoCrm($id, $arLeeLooId["LEELOO_ID"], $pid, $arLeeLooId["USER_ID"]);
		}
	  }

	}

 }

//Поиск в базе CRM по вариантам номера телефона
function findInCRMByPhone($phone){
$arResult = array();

if(empty(trim($phone))) return array();//Если передан пустой телефон

if(strpos($phone,"+7")!==false){
$phone1 = str_replace("+7", "+8", $phone);
} else if(strpos($phone,"+8")!==false){
$phone1 = str_replace("+8", "+7", $phone);
}

	$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_PHONE" ,"PROPERTY_LEELOO_ID", "PROPERTY_USER");
	$arFilter = Array("IBLOCK_ID"=>19,
		array("LOGIC"=>"OR", array("PROPERTY_PHONE"=>$phone), array( "PROPERTY_PHONE"=>$phone1),
		array("PROPERTY_PHONE"=>str_replace("+","",$phone)), array( "PROPERTY_PHONE"=>str_replace("+","",$phone1))			),

 );
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->fetch()){
		if(!in_array($ob["PROPERTY_LEELOO_ID_VALUE"], $arResult[$ob["ID"]]["LEELOO_ID"])){
		  $arResult[$ob["ID"]] = array("USER_ID"=>$ob["PROPERTY_USER_VALUE"], "LEELOO_ID"=>array_unique($ob["PROPERTY_LEELOO_ID_VALUE"]));
		  }
	}

 return $arResult;
}

//Прописывает пользователю id канала коммуникации для рассылки уведомлений
/*
 * @param $uid int - id пользователя битрикс
 * @param $accountId string - id канала коммуникации leeloo
 */
function setUserSubscribeAccountId($uid, $accountId){

			global $USER;
			  $rsUser = CUser::GetByID($uid);
			  $arUser = $rsUser->Fetch();

			  if(count($arUser)){
				$arCommunicationsId = $arUser["UF_EVENTS_CHAT_ID"];
				if(!in_array($accountId, $arCommunicationsId)){
				  $arCommunicationsId[] = $accountId;
				}

            $oUser = new CUser;

            $aFields = array(
                'UF_EVENTS_CHAT_ID' => $arCommunicationsId,
                'UF_EVENTS_ENABLE' => 1,
                'UF_EVENTS_USE_FAVORITES' => 0,
					 'UF_EVENTS_TYPES' => 'ALL,DIVIDENDS,DIVIDENDS_USA,OFERTA,CANCELLATION,COUPONS,REPORT_RUS,REPORT_USA,IMPORTANT,SOVIET,GOSA,WEBINAR'
            );
            $oUser->Update($uid, $aFields); //$iUserID (int) ID of USER
				}
}

//Добавляет в CRM ID из leeloo
function setLeelooIdtoCrm($id, $arExistLeeLooId, $leelooId, $uid){
	if(intval($id)>0){
	if(!is_array($arExistLeeLooId)){
		$arExistLeeLooId = array();
	}
	 $arExistLeeLooId[] = $leelooId;

	 $arExistLeeLooId = array_unique($arExistLeeLooId);
 		CIBlockElement::SetPropertyValuesEx($id, 19, array("LEELOO_ID" => $arExistLeeLooId));
	}
}


//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log/leeloo/leeloo_webhook_log_".(new DateTime())->format('d_m_Y').".txt");
//AddMessage2Log($el->LAST_ERROR);return true;
AddMessage2Log('arr = '.print_r(array("input"=>$input, "finded_in_crm"=>$arCrmPeople), true),'');


header("HTTP/1.0 200 OK");