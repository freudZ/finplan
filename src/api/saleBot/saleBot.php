<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

/**
 * Роутер для smart sender и апи для сообщений на сайте
 */

if (count($_REQUEST) <= 0 && !isset($_REQUEST['action'])) {
    die();
}
$smSender = new CsaleBot();
if ($_SERVER["HTTP_AUTHORIZATION"] !== 'Bearer ' . $smSender->bearer_token) {
    die();
}
$action = '';
$bxUserId = 0;

$action = $_REQUEST['action'];
$bxUserId = $_REQUEST['bitrix_uid'];
//CLogger::apiSaleBot(print_r($_SERVER, true));
//CLogger::apiSaleBot(print_r($_POST, true));
CLogger::saleBotWebhook(print_r($_REQUEST, true));
if ($action == 'confirmation') {

	 if(empty($bxUserId) && isset($_REQUEST['client_email'])){ //Если подтверждение не содержит id пользователя битрикс (например из бота через тильду) - то ищем пользователя по переданному email
		 $data = CUser::GetList(($by="ID"), ($order="ASC"),
                   array(
                       'LOGIN' => $_REQUEST['client_email'],
                       'ACTIVE' => 'Y'
                   )
               );
       while($arUser = $data->Fetch()) {
         $bxUserId = $arUser['ID'];
       }
		 CLogger::saleBotWebhook("Поиск юзера по почте ".$_REQUEST['client_email']);
		 CLogger::saleBotWebhook("Найден юзер ".$bxUserId);
	 }

    setUserConfirmedData($bxUserId, $_POST);
}
if ($action == 'subscribeOn') {
    setUserSubscribeAccountId($bxUserId, $_POST);
}
if ($action == 'subscribeOff') {
    $smartSenderUid = $_REQUEST['smartSenderUid'];
    setUserSubscribeOff($bxUserId, $smartSenderUid);
}
if ($action == 'sendMessage') {

}

if ($action == 'checkSubscribe') {
    $res = $smSender->checkSubscribe($bxUserId);
    echo $res;
    
    unset($smSender);
    die();
}


function setUserConfirmedData($uid, $arPost)
{
    global $USER;
    $rsUser = CUser::GetByID($uid);
    $arUser = $rsUser->Fetch();
    if (count($arUser)) {
        $oUser = new CUser;
        /*
 [bitrix_uid] => 7307
[client_id] => 24978911
[platform_id] => 1641824483
[client_type] => 1
[messenger] => Telegram
[action] => confirmation
         */
        $messId = $arPost["client_type"] . ',' . $arPost["messenger"] . ',' . $arPost["platform_id"];
        $arExistMessId = $arUser["UF_PLATFORM_ID"];
        /*				$add = true;
                        foreach($arExistMessId as $arVal){
                            if($arVal['VALUE'] == $messId){
                              $add = false;
                              break;
                            }
                        }*/
        if (!in_array($messId, $arExistMessId)) {
            $arExistMessId[] = $messId;
        }
        
        $aFields = array(
          'UF_SALEBOT_ID'  => $arPost["client_id"],
          'UF_PLATFORM_ID' => $arExistMessId,
        );
        $oUser->Update($uid, $aFields); //$iUserID (int) ID of USER
        
        //Запись ID Sale bot в crm
        updateCrmSaleBotId($uid, $arPost["client_id"]);
        
    }
}

//Прописывает пользователю id канала коммуникации для рассылки уведомлений
/*
 * @param $uid int - id пользователя битрикс
 * @param $accountId string - id канала коммуникации leeloo
 */
function setUserSubscribeAccountId($uid, $arPost)
{
    
    global $USER;
    $rsUser = CUser::GetByID($uid);
    $arUser = $rsUser->Fetch();
    
    if (count($arUser)) {
        
        $oUser = new CUser;
        $messId = $arPost["client_type"] . ',' . $arPost["messenger"] . ',' . $arPost["platform_id"];
        $arExistMessId = $arUser["UF_PLATFORM_ID"];
        if (!in_array($messId, $arExistMessId)) {
            $arExistMessId[] = $messId;
        }
        
        $aFields = array(
          'UF_SALEBOT_ID'           => $arPost["client_id"],
          'UF_PLATFORM_ID'          => $arExistMessId,
          'UF_EVENTS_ENABLE'        => 1,
          'UF_EVENTS_USE_FAVORITES' => 0,
          'UF_EVENTS_TYPES'         => 'ALL,DIVIDENDS,DIVIDENDS_USA,OFERTA,CANCELLATION,COUPONS,REPORT_RUS,REPORT_USA,IMPORTANT,SOVIET,GOSA,WEBINAR'
        );
        
        if($arUser["PERSONAL_PHONE"]!=$arPost["phone"]){
            $aFields["PERSONAL_PHONE"] = $arPost["phone"];
        }
        $oUser->Update($uid, $aFields); //$iUserID (int) ID of USER
    
        //Запись ID Sale bot в crm
        updateCrmSaleBotId($uid, $arPost["client_id"]);
    }
}


function updateCrmSaleBotId($uid, $salebotUid)
{
    CModule::IncludeModule("iblock");
    $findedCrmId = 0;
    $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_USER");
    $arFilter = array("IBLOCK_ID" => 19, "=PROPERTY_USER" => $uid);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    if ($ob = $res->fetch()) {
        $findedCrmId = $ob["ID"];
    }
    
    if (intval($findedCrmId) > 0) {
        CIBlockElement::SetPropertyValuesEx($findedCrmId, 19, array("SALEBOT_ID" => $salebotUid));
    }
}

/**
 * [add description]
 *
 * @param  [add type]   $uid [add description]
 * @param  [add type]   $smartSenderUid [add description]
 *
 * @return [add type]  [add description]
 */
function setUserSubscribeOff($uid, $smartSenderUid)
{
    global $USER;
    $rsUser = CUser::GetByID($uid);
    $arUser = $rsUser->Fetch();
    
    if (count($arUser)) {
        
        $oUser = new CUser;
        
        $aFields = array(
            //'UF_SMART_SENDER_UID' => $smartSenderUid,
            'UF_EVENTS_ENABLE' => 0,
            //'UF_EVENTS_USE_FAVORITES' => 0,
            //'UF_EVENTS_TYPES' => 'ALL,DIVIDENDS,DIVIDENDS_USA,OFERTA,CANCELLATION,COUPONS,REPORT_RUS,REPORT_USA,IMPORTANT,SOVIET,GOSA,WEBINAR'
        );
        $oUser->Update($uid, $aFields); //$iUserID (int) ID of USER
    
        //Запись ID Sale bot в crm
        updateCrmSaleBotId($uid, "");
    }
}

header("HTTP/1.1 200 " . "webhook recived");
echo json_encode("ok");
die();
?>