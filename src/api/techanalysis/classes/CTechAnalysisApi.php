<?
/**
 * Класс для реализации restful api на базе платформы Битрикс
 *
 *
 * @category  class
 * @package   CTechAnalysisApi.php
 * @author    Alexandr Eremin <alphaprogrammer@gmail.com>
 * @copyright 2020
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     63 class CTechAnalysisApi extends CApiBase
 *     78   function __construct($request, $origin)
 *     97   function getBXSessid()
 *    102   function checkPayRadar()
 *    108   function checkPayRadarUsa()
 *    140   function checkPayGS($userId = 0)
 *    171   function checkPayGSUsa($userId = 0)
 *    176   function getEmbeddedTAData()
 *    211   function clearTACache()
 *    219   function getActionList()
 *    255   function getUser()
 *    290   function graphData()
 *    337   function getSaveList()
 *    374   function getSaveListExt()
 *    412   function getBackupList()
 *    458   function getGraphData()
 *    512   function getLastTimestamp()
 *    541   function getPublicListGraphData()
 *    596   function getMd5Graph()
 *    610   function setPublicTemplate()
 *    677   function setGraphData()
 *    782   function deleteGraphData()
 *    821   function getActionDetail()
 *    955   function getActionListUsa()
 *    984   function getActionDetailUsa()
 *   1086   function getIndexList()
 *   1112   function getDetailInfo()
 *   1138   function createFilterArray()
 *   1186   function getIndicatorsList()
 *   1271   function getIndicatorsPresets()
 *   1332   function getLineNamesHL()
 *   1362   function getLineParams()
 *   1426   function testParams()
 *   1451   function showDebug($data, $description = )
 *   1460   function clearProps($arProperties)
 *   1495   function clearResult($arFields)
 *
 * TOTAL FUNCTIONS: 35
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');

class CTechAnalysisApi extends CApiBase
{
    protected $actionsIblockId = 32; //ID инфоблока акций РФ
    protected $actionsUsaIblockId = 55; //ID инфоблока акций США
    protected $IndexesIblockId = 55; //ID инфоблока индексов
    protected $TAIndicatorsIblockId = 65; //ID инфоблока списка индикаторов
    protected $TAIndicatorLinesIblockId = 66; //ID инфоблока списка настроек линий индикаторов
    protected $TAIndicatorPresetsIblockId = 67; //ID инфоблока списка шаблонов индикаторов
    protected $TAGraphDataHLIblockId = 40; //ID HL инфоблока с данными пользовательских графиков
    protected $TALineTypesHLIblockId = 37; //ID HL инфоблока списка типов линий индикаторов
    protected $actionsPricesHLblockId = 24; //ID HL инфоблока цен акций
    protected $actionsUsaPricesHLblockId = 29; //ID HL инфоблока цен акций США
    protected $CacheTA_ID = 'Techanaliz_'; //Название кеша
    protected $backupCount = 5; //Кол-во хранимых резервных копий для одного именованного сохранения пользователя
    
    public function __construct($request, $origin)
    {
    	  Global $APPLICATION;
		  $this->actionsUsaPricesHLblockId = $APPLICATION->usaPricesHlId;
        parent::__construct($request);
        CModule::IncludeModule("iblock");

        if (isset($this->nargs["filter"])) {
            $this->createFilterArray();
        }
    }
    
    /**
     * Возвращает код сессии битрикс
     *  /api/v1/getBXSessid
     * @return string
     *
     * @access protected
     */
    protected function getBXSessid()
    {
        return bitrix_sessid();
    }
    
    protected function checkPayRadar()
    {
        $haveRadarAccess = checkPayRadar();
        return $haveRadarAccess;
    }
    
    protected function checkPayRadarUsa(){
        $haveUSARadarAccess = checkPayUSARadar();
        return $haveUSARadarAccess;
    }
    
    //Проверяет для пользователя наличие купленного ГС
/*    protected function checkPayGS_()
    {
        global $USER;
        $result = false;
        $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_PRODUCTS", "PROPERTY_USER");
        $arFilter = array("IBLOCK_ID" => IntVal(19), "PROPERTY_USER" => $USER->GetID(), "PROPERTY_PRODUCTS" => "ГС%");

        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        $arGs = array();
        while ($ob = $res->fetch()) {
            $arGs = array_merge($arGs, $ob["PROPERTY_PRODUCTS_VALUE"]);
        }

        $haveGs = false;
        foreach ($arGs as $val) {
            if (($val == "ГС" || $val == "ГС США") || (strpos($val, "ГС закончится") !== false || strpos($val, "ГС США закончится") !== false )) {
                $haveGs = true;
                break;
            }
        }

        unset($res, $arSelect, $arFilter, $arGs);
        $result = $haveGs;
        return $result;
    }*/

	 protected function checkPayGS($userId = 0){
	  return checkPaySeminar(13823, false, $userId);
	 }

    //Проверяет для пользователя наличие купленного ГС США
/*    protected function checkPayGSUsa()
    {
        global $USER;
        $result = false;
        $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_PRODUCTS", "PROPERTY_USER");
        $arFilter = array("IBLOCK_ID" => IntVal(19), "PROPERTY_USER" => $USER->GetID(), "PROPERTY_PRODUCTS" => "ГС США%");

        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        $arGs = array();
        while ($ob = $res->fetch()) {
            $arGs = array_merge($arGs, $ob["PROPERTY_PRODUCTS_VALUE"]);
        }

        $haveGs = false;
        foreach ($arGs as $val) {
            if ($val == "ГС США" || strpos($val, "ГС США закончится") !== false) {
                $haveGs = true;
                break;
            }
        }

        unset($res, $arSelect, $arFilter, $arGs);
        $result = $haveGs;
        return $result;
    }*/

	 protected function checkPayGSUsa($userId = 0)
    {
		return checkPayGSUSA(13823, $userId);
    }

	 protected function getEmbeddedTAData(){
		 $arResult = array();
		 $arAction = array();
		 $url = $this->args[0];

		 if(strpos($url, "gs")!==false){ //ГС РФ
		 	$arResult['simpleMode'] = true;
			$resA = new Actions();
			$arAction = $resA->getItemByCode($this->args[1]);
			if(is_array($arAction)){
				$arResult['actionList'][$arAction["PROPS"]["ISIN"]] = $arAction["PROPS"]["SECID"];
			}
			 $arResult['templateId'] = 113714;
			 $arResult['country'] = 'RUS';
		 }
		 if(strpos($url, "gs_usa")!==false){ //ГС США
		 	$arResult['simpleMode'] = true;
			$resAU = new ActionsUsa();
			$arAction = $resAU->getItemByCode($this->args[1]);
			if(is_array($arAction)){
				$arResult['actionList'][$arAction["PROPS"]["ISIN"]] = $arAction["PROPS"]["SECID"];
			}
			$arResult['templateId'] = 113412;
			$arResult['country'] = 'USA';
		 }
		/* {
templateId: идентификатор шаблона,
actionList: массив secid нужных бумаг,
simpleMode: true (по-умолчанию)
} */

		  return $arResult;
	 }

    //Очистищает указанныы кеш
    protected function clearTACache()
    {
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cache->clean($this->CacheTA_ID . $this->verb);
        return array("result" => "ok");
    }
    
    //Возвращает список акций РФ
    protected function getActionList()
    {
        global $APPLICATION;
        $arResult = array();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 86400;
        $cacheId = $this->CacheTA_ID . 'getActionList';
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else { //проверяем и кешируем
            $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_ISIN", "PROPERTY_EMITENT_ID");
            $arFilter = array(
              "IBLOCK_ID"          => IntVal($this->actionsIblockId),
              "!=PROPERTY_BOARDID" => $APPLICATION->ExcludeFromRadar["теханализ"],
              "!PROPERTY_BOARDID"  => false,
              "PROPERTY_HIDEN"     => false,
              "ACTIVE_DATE"        => "Y",
              "ACTIVE"             => "Y"
            );
            
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while ($ob = $res->Fetch()) {
                $arResult[$ob["PROPERTY_SECID_VALUE"]] = array(
                  "ID"         => $ob["ID"],
                  "REGION"     => "RUS",
                  "NAME"       => $ob["NAME"],
                  "EMITENT_ID" => $ob["PROPERTY_EMITENT_ID_VALUE"],
                  "ISIN" => $ob["PROPERTY_ISIN_VALUE"]
                );
            }
            $cache->set($cacheId, $arResult);
        }
        return $this->clearResult($arResult);
    }
    
    //Возвращает текущего пользователя, который авторизован, проверяет его на админа и доступ в радар
    protected function getUser()
    {
        global $USER;
        $arReturn = array();
        if ($this->verb == 'work') {
            $arReturn["USER_ID"] = $USER->GetID();
            $arReturn["AUTHORIZED"] = $USER->IsAuthorized();
            if ($arReturn["AUTHORIZED"]) {
                $arReturn["IS_ADMIN"] = $GLOBALS["USER"]->IsAdmin();
                $arReturn["PAY_RADAR"] = $this->checkPayRadar();
                $arReturn["PAY_RADAR_USA"] = $this->checkPayRadarUsa();
                $arReturn["PAY_GS"] = $this->checkPayGS($USER->GetID());
                $arReturn["PAY_GS_USA"] = $this->checkPayGSUsa($USER->GetID());
            } else {
                $arReturn["IS_ADMIN"] = false;
                $arReturn["PAY_RADAR"] = false;
                $arReturn["PAY_RADAR_USA"] = false;
                $arReturn["PAY_GS"] = false;
            }
        } elseif ($this->verb == 'debug') {
            if (!empty($this->nargs["userId"])) {
                $arReturn["USER_ID"] = $this->nargs["userId"];
                $arReturn["AUTHORIZED"] = $this->nargs["authorized"] == "true" ? true : false;
                $arReturn["IS_ADMIN"] = $this->nargs["isAdmin"] == "true" ? true : false;
                $arReturn["PAY_RADAR"] = $this->nargs["payRadar"] == "true" ? true : false;
                $arReturn["PAY_RADAR_USA"] = $this->nargs["payRadarUsa"] == "true" ? true : false;
                $arReturn["PAY_GS"] = $this->nargs["payGS"] == "true" ? true : false;
            }
        }
        return $arReturn;
    }
    
    //Чтение/запись данных пользовательского графика в теханализе
    //Работа с данными графика без резервных копий
    //deprecated 01.10.2020
    protected function graphData()
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        $arFilter = array('UF_USER' => $this->nargs["userId"]);
        $res = $entityClass::getList(array(
          "filter" => $arFilter,
          "select" => array(
            "ID",
            "UF_DATA"
          ),
        ));
        while ($item = $res->fetch()) {
            $arResult["data"] = $item["UF_DATA"];
            $arResult["id"] = $item["ID"];
        }
        if ($this->verb == 'get') {
            if (!array_key_exists("data", $arResult)) {
                $arResult["data"] = "";
                $arResult["error"] = "Данных для текущего пользователя нет";
            }
        } else {
            if ($this->verb == 'set') {
                $arBody = file_get_contents("php://input");
                $arBody = json_decode($arBody, true);
                //$arResult = $arBody;
                $arItem["UF_USER"] = $this->nargs["userId"];
                $arItem["UF_DATA"] = json_encode($arBody["data"]);
                if (!array_key_exists("data", $arResult)) { //Если еще нет данных - добавление
                    $result = $entityClass::add($arItem);
                } else { //Иначе перезапись
                    $result = $entityClass::update($arResult["id"], $arItem);
                    
                }
                
                
            }
        }
        
        return $arResult;
    }
    
    //Возвращает список сейвов для указанного пользователя
    //  /api/techanalysis/getSaveList/1/
    protected function getSaveList()
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();

        $arFilter = array('UF_USER' => $this->args[0]);
        if (count($this->args) > 1 && intval($this->args[1]) > 0) {
            if (is_numeric($this->args[1])) {
                $arFilter["ID"] = $this->args[1];
            } else {
                if (is_string($this->args[1])) {
                    $arFilter["UF_SAVENAME"] = $this->args[1];
                }
            }
        }

        $arParams = array(
          "filter" => $arFilter,
          "select" => array(
            "UF_SAVENAME"
          ),
          "order"  => array("UF_SAVENAME" => "ASC"),
          "group"  => array("UF_SAVENAME"),
        );

        $res = $entityClass::getList($arParams);
        while ($item = $res->fetch()) {
            $arResult[] = $item["UF_SAVENAME"];
        }

        return $arResult;
    }

    //Возвращает список сейвов для указанного пользователя c ID шаблона
    //  /api/techanalysis/getSaveListExt/1/
    protected function getSaveListExt()
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();

        $arFilter = array('UF_USER' => $this->args[0]);
        if (count($this->args) > 1 && intval($this->args[1]) > 0) {
            if (is_numeric($this->args[1])) {
                $arFilter["ID"] = $this->args[1];
            } else {
                if (is_string($this->args[1])) {
                    $arFilter["UF_SAVENAME"] = $this->args[1];
                }
            }
        }

        $arParams = array(
          "filter" => $arFilter,
          "select" => array(
            "UF_SAVENAME",
            "UF_TEMPLATE_ID"
          ),
          "order"  => array("UF_SAVENAME" => "ASC"),
          "group"  => array("UF_SAVENAME"),
        );

        $res = $entityClass::getList($arParams);
        while ($item = $res->fetch()) {
            $arResult[] = array("UF_SAVENAME"=>$item["UF_SAVENAME"], "UF_TEMPLATE_ID"=>$item["UF_TEMPLATE_ID"]);
        }

        return $arResult;
    }

    //Возвращает список резервных копий для указанного пользователя
    //  /api/techanalysis/getBackupList/1/
    protected function getBackupList()
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        $arFilter = array('UF_USER' => $this->args[0]);
        if (count($this->args) > 1 && intval($this->args[1]) > 0) {
            if (is_numeric($this->args[1])) {
                $arFilter["ID"] = $this->args[1];
            } else {
                if (is_string($this->args[1])) {
                    $arFilter["UF_SAVENAME"] = $this->args[1];
                }
            }
        }
        
        $arParams = array(
          "filter" => $arFilter,
          "select" => array(
            "ID",
            "UF_MD5",
            "UF_DATETIME",
            "UF_SAVENAME"
          ),
          "order"  => array("UF_SAVENAME" => "ASC"),
        );
        
        $res = $entityClass::getList($arParams);
        while ($item = $res->fetch()) {
            $arItem["savename"] = $item["UF_SAVENAME"];
            $arItem["md5"] = $item["UF_MD5"];
            $arItem["datetime"] = $item["UF_DATETIME"]->format('d.m.Y H:i:s');
            $arItem["id"] = $item["ID"];
            
            $arResult[] = $arItem;
        }
        
        return $arResult;
    }
    
    //Возвращает сохраненный сейв либо возвращает сейв с указанным id для указанного пользователя
    //  /api/techanalysis/getGraphData/1/
    //  /api/techanalysis/getGraphData/1/20/
    //  /api/techanalysis/getGraphData/1/Мой график 1/
    protected function getGraphData()
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        $arFilter = array('UF_USER' => $this->args[0]);
        if (count($this->args) > 1 && !empty($this->args[1])) {
            if (is_numeric($this->args[1])) {
                $arFilter["ID"] = $this->args[1];
            } else {
                if (is_string($this->args[1])) {
                    $arFilter["UF_SAVENAME"] = $this->args[1];
                }
            }
        }
        $arParams = array(
          "filter" => $arFilter,
          "select" => array(
            "ID",
            "UF_DATA",
            "UF_MD5",
            "UF_DATETIME",
            "UF_SAVENAME",
            "UF_PUBLIC_RADAR",
            "UF_PUBLIC_GS",
				"UF_TEMPLATE_ID"
          ),
          "order"  => array("UF_DATETIME" => "DESC"),
          "limit"  => 1
        );
        
        $res = $entityClass::getList($arParams);
        while ($item = $res->fetch()) {
            $arResult["savename"] = $item["UF_SAVENAME"];
            $arResult["public_radar"] = $item["UF_PUBLIC_RADAR"];
            $arResult["public_gs"] = $item["UF_PUBLIC_GS"];
            $arResult["data"] = $item["UF_DATA"];
            $arResult["md5"] = $item["UF_MD5"];
            $arResult["datetime"] = $item["UF_DATETIME"]->format('d.m.Y H:i:s');
            $arResult["id"] = $item["ID"];
            $arResult["template_id"] = $item["UF_TEMPLATE_ID"];
        }
        
        return $arResult;
    }
    
    /**
     * Возвращает timestamp последнего сейва с для заданного пользователя и названия шаблона
     * $this->args[0] - user id
     * $this->args[1] - Название шаблона
     * @return array|false
     */
    protected function getLastTimestamp()
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        $arFilter = array('UF_USER' => $this->args[0]);
        $arFilter["UF_SAVENAME"] = $this->args[1];
        
        $arParams = array(
          "filter" => $arFilter,
          "select" => array(
            "ID",
            "UF_DATETIME",
				"UF_TEMPLATE_ID"
          ),
          "order"  => array("UF_DATETIME" => "DESC"),
          "limit"  => 1
        );
        
        $res = $entityClass::getList($arParams);
        while ($item = $res->fetch()) {
            $item["UF_DATETIME"] = $item["UF_DATETIME"]->format('d.m.Y H:i:s');
            $arResult = $item;
        }
        return $arResult;
    }
    
    protected function getPublicListGraphData()
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        $arFilter = array(
          "LOGIC" => "OR",
          array('=UF_PUBLIC_RADAR' => 'Y'),
          array('=UF_PUBLIC_GS' => 'Y'),
          array('=UF_PUBLIC_RADAR' => 'Y', '=UF_PUBLIC_GS' => 'Y')
        );
        $arParams = array(
          "filter" => $arFilter,
          "select" => array(
            "ID",
            "UF_DATA",
            "UF_MD5",
            "UF_DATETIME",
            "UF_SAVENAME",
            "UF_PUBLIC_RADAR",
            "UF_PUBLIC_GS",
            "UF_USER",
            "UF_TEMPLATE_ID"
          ),
          "order"  => array("UF_DATETIME" => "DESC"),
        );
        $res = $entityClass::getList($arParams);
        
        while ($item = $res->fetch()) {
            $arTmp = array();
            if (array_key_exists(md5($item["UF_SAVENAME"]), $arResult)) {
                continue;
            }
            $arTmp["userId"] = intval($item["UF_USER"]);
            $arTmp["savename"] = $item["UF_SAVENAME"];
            $arTmp["public_radar"] = $item["UF_PUBLIC_RADAR"];
            $arTmp["public_gs"] = $item["UF_PUBLIC_GS"];
            //$arTmp["data"] = $item["UF_DATA"];
            $arTmp["md5"] = $item["UF_MD5"];
            $arTmp["datetime"] = $item["UF_DATETIME"]->format('d.m.Y H:i:s');
            $arTmp["id"] = intval($item["ID"]);
            $arTmp["template_id"] = intval($item["UF_TEMPLATE_ID"]);
            $arResult[md5($item["UF_SAVENAME"])] = $arTmp;
        }
        
        usort($arResult, function ($item1, $item2) {
            return $item1['savename'] < $item2['savename'];
        });
        
        return $arResult;
        
    }
    
    protected function getMd5Graph()
    {
        //Обработаем переданные данные, сравним md5
        $arData = json_decode($this->file);
        //$jsonGraph = serialize($arData->data);
        $jsonGraph = json_encode($arData->data);
        $md5Json = md5($jsonGraph);
        
        
        //return  $md5Json;
        return $arData->data;
    }
    
    //Устанавливает / снимает признак публичного шаблона, а так же очищает кеш данных публичных графиков
    protected function setPublicTemplate()
    {
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        $arIdGraphData = array();
        
        $arFilter = array('UF_USER' => $this->args[0]);
        if (!isset($this->args[1]) || empty($this->args[1])) {
            $arFilter["UF_SAVENAME"] = 'По-умолчанию';
        } else {
            $arFilter["UF_SAVENAME"] = htmlspecialchars($this->args[1]);
        }
        
        $res = $entityClass::getList(array(
          "filter" => $arFilter,
          "select" => array(
            "ID"
          ),
        ));
        $arIdGraphData = $res->fetchAll();
        
        if (count($arIdGraphData) == 0) {
            return array("result" => "error", "message" => "Шаблоны не найдены");
        }
        
        $publicFlag = array("RADAR" => "", "GS" => "");
        switch ($this->verb) {
            case "set"://Отмечаем шаблон и его РК как публичные
                //Обработаем переданные данные
                $arData = file_get_contents("php://input");
                $arData = json_decode($arData);
                $publicFlag["RADAR"] = $arData->publicGraphRadar;
                $publicFlag["GS"] = $arData->publicGraphGS;
                break;
            case "unset": //Снимаем отметку публичности с шаблона и его РК
                $publicFlag = array("RADAR" => "", "GS" => "");
                break;
        }
        
        $arResult = array("ERRORS" => array(), "SUCCESS" => array());
        foreach ($arIdGraphData as $id) {
            $res = $entityClass::update($id,
              array("UF_PUBLIC_RADAR" => $publicFlag["RADAR"], "UF_PUBLIC_GS" => $publicFlag["GS"]));
            if ($res->isSuccess()) {
                $arResult["SUCCESS"][] = $res->getId();
            } else {
                $arResult["ERRORS"][] = implode(', ', $res->getErrors());
            }
        }
        
        if (count($arResult["ERRORS"]) == 0) {
            return array(
              "result"  => "success",
              "message" => 'Обновлены записи с id: ' . implode(', ', $arResult["SUCCESS"])
            );
        } else {
            return array(
              "result"  => "error",
              "message" => 'Обновлены записи с id: ' . implode(', ',
                  $arResult["SUCCESS"]) . '; Ошибки: ' . implode(', ', $arResult["ERRORS"])
            );
        }
        
    }
    
    //Сверяем md5 и сохраняет данные графика для пользователя
    protected function setGraphData()
    {
        $arResult = array();
        $this->file = file_get_contents("php://input");
        //Получим данные о сейвах пользователя
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        if (intval($this->args[0]) <= 0) {
            return array("result" => "error", "message" => "uid not found, is guest probably");
        }
        $arFilter = array('UF_USER' => $this->args[0]);
        if (!isset($this->args[1]) || empty($this->args[1])) {
            $arFilter["UF_SAVENAME"] = 'По-умолчанию';
        } else {
            $arFilter["UF_SAVENAME"] = htmlspecialchars($this->args[1]);
        }
        $res = $entityClass::getList(array(
          "filter" => $arFilter,
          "select" => array(
            "ID",
            "UF_DATA",
            "UF_MD5",
            "UF_DATETIME",
            "UF_TEMPLATE_ID"
          ),
          "order"  => array("UF_DATETIME" => "ASC"),
        ));
        $saveDelId = false;  //id самого старого сейва
        $saveCnt = 0;  //Текущее кол-во сейвов
		  $saveTemplateId = 0;
        while ($item = $res->fetch()) {
            if ($saveDelId == false) {
                $saveDelId = $item["ID"];
            }
				$saveTemplateId = $item["UF_TEMPLATE_ID"];
            $saveCnt++;
        }
        //Обработаем переданные данные, сравним md5
        $arData = json_decode($this->file);
        //$jsonGraph = json_encode($arData["data"]);
        $jsonGraph = json_encode($arData->data, JSON_UNESCAPED_SLASHES);
        //$md5Graph = $arData->md5;
        //$md5Graph = $arData["md5"];
        $md5Json = md5($jsonGraph);
        
        /*global $USER;
              $rsUser = CUser::GetByID($USER->GetID());
              $arUser = $rsUser->Fetch();
              if($arUser["LOGIN"]=="freud"){
         echo $md5Json."<br>";
         echo $jsonGraph; exit;
              }*/
        
        // return array("is_obj"=>is_object($jsonGraph)?'Y':'N', "md5PUT"=>$md5Graph, "md5Json"=>$md5Json);
        
        /*		if($md5Graph!=$md5Json){ //Если md5 не совпадают - возвращаем ошибку
                  $arResult = array("result"=>"error", "message"=>"md5 is not mismatch");
                  return $arResult;
                } else {*/
        if ($saveCnt >= $this->backupCount && $saveDelId != false) { //Удалим самый старый сейв
            $entityClass::delete($saveDelId);
        }

		  if($saveTemplateId==0){
		  	$CEnum = new CEnumerators;
			$saveTemplateId = $CEnum->getNewEnum("TA");
		  }

        //Сформируем и запишем новый последний сейв.
        $arItem["UF_USER"] = $this->args[0];
        $arItem["UF_DATA"] = $jsonGraph;
        $arItem["UF_TEMPLATE_ID"] = $saveTemplateId;
        $arItem["UF_PUBLIC_RADAR"] = isset($arData->publicGraphRadar) ? $arData->publicGraphRadar : '';
        $arItem["UF_PUBLIC_GS"] = isset($arData->publicGraphGS) ? $arData->publicGraphGS : '';
        //$arItem["UF_MD5"] = $md5Json;
        $saveDateTime = (new DateTime())->format('d.m.Y H:i:s');
        $arItem["UF_DATETIME"] = $saveDateTime;
//	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log/setGraphData_log.txt");
//AddMessage2Log($el->LAST_ERROR);return true;
// AddMessage2Log(array("jsonGraph"=>$jsonGraph),'');
        
        if (!isset($this->args[1]) || empty($this->args[1])) {
            $arItem["UF_SAVENAME"] = 'По-умолчанию';
        } else {
            $arItem["UF_SAVENAME"] = htmlspecialchars($this->args[1]);
        }
        $otvet = $entityClass::add($arItem);
        
        if ($otvet->isSuccess()) {
            $arResult = array(
              "result"    => "success",
              "message"   => "added id=" . $otvet->getId(),
              "timestamp" => $saveDateTime
            );
        } else {
            $arResult = array("result" => "error", "message" => implode(', ', $otvet->getErrors()));
        }
        return $arResult;
        //}
        
        
    }
    
    protected function deleteGraphData()
    {
        //Получим данные о сейвах пользователя
        $hlblock = HL\HighloadBlockTable::getById($this->TAGraphDataHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        $arFilter = array('UF_USER' => $this->args[0]);
        if (!isset($this->args[1]) || empty($this->args[1])) {
            $arFilter["UF_SAVENAME"] = 'По-умолчанию';
            return array("result" => "error", "message" => "empty or default savename");
        } else {
            $arFilter["UF_SAVENAME"] = htmlspecialchars($this->args[1]);
        }
        $res = $entityClass::getList(array(
          "filter" => $arFilter,
          "select" => array(
            "ID",
            "UF_SAVENAME",
            "UF_MD5",
            "UF_DATETIME"
          ),
          "order"  => array("UF_DATETIME" => "ASC"),
        ));
        
        $rows = $res->fetchAll();
        
        foreach ($rows as $data) {
            if ($data["UF_SAVENAME"] == 'По-умолчанию') {
                continue;
            }
            $entityClass::delete($data["ID"]);
        }
        
        
        return array("result" => "success", "message" => "savename cleared");
    }
    
    //Возвращает акцию РФ по SECID
    protected function getActionDetail()
    {
        $arResult = array();
        if (!isset($this->verb) || empty($this->verb)) {
            return array("error" => "secid param is empty");
        }
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 3600;
        $cacheId = $this->CacheTA_ID . 'getActionDetail' . $this->verb . $this->nargs["modify"];
        
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else { //проверяем и кешируем
            
            $arSelect = array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_*");
            $arFilter = array(
              "IBLOCK_ID"      => $this->actionsIblockId,
              "PROPERTY_SECID" => $this->verb,
              "ACTIVE_DATE"    => "Y",
              "ACTIVE"         => "Y"
            );
            $res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCnt" => 1), $arSelect);
            if ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arFields["PROPS"] = $ob->GetProperties();
                $arFields["PROPS"] = $this->clearProps($arFields["PROPS"]);
                $arFields["TYPE"] = $arFields["PROPS"]["IS_ETF_ACTIVE"]["VALUE"] == "Y" ? 'etf' : 'share';
                
                if ($this->nargs["modify"] == 'full') { //Добавляем данные радара только для полного запроса данных, для цен не добавляем
                    if ($arFields["TYPE"] == "share") {
                        $resA = new Actions();
                    } elseif ($arFields["TYPE"] == "etf") {
                        $resA = new ETF();
                    }
                }
                
                $arFields["RADAR_DATA"] = array();
                
                //$this->showDebug($arFields, 'arFields');
                
                if ($this->nargs["modify"] == 'full') { //Добавляем данные радара только для полного запроса данных, для цен не добавляем
                    $arRadarData = $resA->getItem($arFields["CODE"]);
                    //$this->showDebug($arRadarData, 'RADAR_DATA');
                    
                    global $USER;
                    $rsUser = CUser::GetByID($USER->GetID());
                    $arUser = $rsUser->Fetch();
                    if ($arUser["LOGIN"] == "freud") {
                        unset($arRadarData["PERIODS"]);
                        unset($arRadarData["PROPS"]);
                        
                    }
                    //Получим текущие периоды за квартал и если есть за месяц (для банков)
                    
                    if (array_key_exists("PERIODS", $arRadarData)) {
                        $currentKvartal = array();
                        $currentMonth = array();
                        foreach ($arRadarData["PERIODS"] as $pk => $period) {
                            if (strpos($pk, "KVARTAL") !== false && count($currentKvartal) == 0) {
                                $currentKvartal = $period;
                            }
                            if (strpos($pk, "MONTH") !== false && count($currentMonth) == 0) {
                                $currentMonth = $period;
                            }
                            
                            if (count($currentKvartal) > 0 && count($currentMonth)) {
                                break;
                            }
                        }
                        
                        $arRadarData["CURRENT_PERIOD_KVARTAL"] = $currentKvartal;
                        $arRadarData["CURRENT_PERIOD_MONTH"] = $currentMonth;
                    }
                    
                    unset($arRadarData["DYNAM"]["Доля в индексах"]);
                    
                    
                    foreach ($arRadarData as $rdk => $rdv) {
                        if (!is_array($rdv)) {
                            $arFields["RADAR_DATA"][$rdk] = $rdv;
                        } else {
                            if (is_array($rdv) && count($rdv) > 0) {
                                $arFields["RADAR_DATA"][$rdk] = $rdv;
                            }
                        }
                    }
                    //$arFields["RADAR_DATA"] = $arRadarData;
                    unset($arRadarData);
                }
            }
            
            //Получаем цены на акции
            $hlblock = HL\HighloadBlockTable::getById($this->actionsPricesHLblockId)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entityClass = $entity->getDataClass();
            $arFilter = array('UF_ITEM' => $arFields["PROPS"]["SECID"]["VALUE"]);
            
            $res = $entityClass::getList(array(
              "filter" => $arFilter,
              "select" => array(
                "UF_ITEM",
                "UF_DATE",
                "UF_CLOSE",
                "UF_HIGH",
                "UF_LOW",
                "UF_OPEN"
              ),
              //"limit" => 20,
              "order"  => array(
                "UF_DATE" => "ASC"
              ),
              "cache"  => array("ttl" => 3600)
            ));
            while ($item = $res->fetch()) {
                $date = (new DateTime($item["UF_DATE"]))->format('Y-m-d');
                $item["UF_DATE"] = $date;
                $arFields["MOEX_ACTION_DATA"][$date] = $item;
            }
            if ($this->nargs["modify"] == 'full') {
                unset($resA);
            }
            
            
            $arFields["REGION"] = "RUS";
            $arResult = $arFields;
            
            $cache->set($cacheId, $arResult);
        }
        unset($cache);
        //$this->showDebug($arResult, 'arResult');
        return $this->clearResult($arResult);
    }
    
    //Возвращает список акций США
    protected function getActionListUsa()
    {
        $arResult = array();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 86400;
        $cacheId = $this->CacheTA_ID . 'getActionListUsa';
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else { //проверяем и кешируем
            $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_ISIN", "PROPERTY_EMITENT_ID");
            $arFilter = array("IBLOCK_ID" => IntVal($this->actionsUsaIblockId), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while ($ob = $res->Fetch()) {
                $arResult[$ob["PROPERTY_SECID_VALUE"]] = array(
                  "ID"         => $ob["ID"],
                  "REGION"     => "USA",
                  "NAME"       => $ob["NAME"],
                  "EMITENT_ID" => $ob["PROPERTY_EMITENT_ID_VALUE"],
                  "ISIN" => $ob["PROPERTY_ISIN_VALUE"]
                );
            }
            $cache->set($cacheId, $arResult);
        }
        unset($cache);
        return $this->clearResult($arResult);
    }
    
    
    //Возвращает акцию США по SECID
    protected function getActionDetailUsa()
    {
        $arResult = array();
        if (!isset($this->verb) || empty($this->verb)) {
            return array("error" => "secid param is empty");
        }
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 0;//3600;
        $cacheId = $this->CacheTA_ID . 'getActionDetail' . $this->verb . $this->nargs["modify"];
        
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else { //проверяем и кешируем
            if ($this->nargs["modify"] == 'full') { //Добавляем данные радара только для полного запроса данных, для цен не добавляем
                $resA = new ActionsUsa();
            }
            $arSelect = array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_*");
            $arFilter = array(
              "IBLOCK_ID"      => $this->actionsUsaIblockId,
              "PROPERTY_SECID" => $this->verb,
              "ACTIVE_DATE"    => "Y",
              "ACTIVE"         => "Y"
            );
            $res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCnt" => 1), $arSelect);
            if ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arFields["PROPS"] = $ob->GetProperties();
                $arFields["PROPS"] = $this->clearProps($arFields["PROPS"]);
                
                $arFields["TYPE"] = 'share_usa';
                if ($this->nargs["modify"] == 'full') { //Добавляем данные радара только для полного запроса данных, для цен не добавляем
                    $arFields["RADAR_DATA"] = $resA->getItem($arFields["CODE"]);
                    
                    //Получим текущие периоды за квартал и если есть за месяц (для банков)
                    if (array_key_exists("PERIODS", $arFields["RADAR_DATA"])) {
                        $currentKvartal = array();
                        $currentMonth = array();
                        foreach ($arFields["RADAR_DATA"]["PERIODS"] as $pk => $period) {
                            if (strpos($pk, "KVARTAL") !== false && count($currentKvartal) == 0) {
                                $currentKvartal = $period;
                            }
                            if (strpos($pk, "MONTH") !== false && count($currentMonth) == 0) {
                                $currentMonth = $period;
                            }
                            
                            if (count($currentKvartal) > 0 && count($currentMonth)) {
                                break;
                            }
                        }
                        
                        $arFields["RADAR_DATA"]["CURRENT_PERIOD_KVARTAL"] = $currentKvartal;
                        $arFields["RADAR_DATA"]["CURRENT_PERIOD_MONTH"] = $currentMonth;
                    }
                    unset($arFields["RADAR_DATA"]["DYNAM"]["Доля в индексах"]);
                }
            }
            
            //Получаем цены на акции
            $hlblock = HL\HighloadBlockTable::getById($this->actionsUsaPricesHLblockId)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entityClass = $entity->getDataClass();
            $arFilter = array('UF_ITEM' => $arFields["PROPS"]["SECID"]["VALUE"]);
            
            $res = $entityClass::getList(array(
              "filter" => $arFilter,
              "select" => array(
                "UF_ITEM",
                "UF_DATE",
                "UF_CLOSE",
                "UF_HIGH",
                "UF_LOW",
                "UF_OPEN",
                "UF_VOLUME"
              ),
              //"limit" => 20,
              "order"  => array(
                "UF_DATE" => "ASC"
              ),
              "cache"  => array("ttl" => 3600)
            ));
            
            while ($item = $res->fetch()) {
                $date = (new DateTime($item["UF_DATE"]))->format('Y-m-d');
                $item["UF_DATE"] = $date;
                $arFields["MOEX_ACTION_DATA"][$date] = $item;
            }
            
            
            if ($this->nargs["modify"] == 'full') {
                unset($resA);
            }
            
            $arFields["REGION"] = "USA";
            $arResult = $arFields;
            $cache->set($cacheId, $arResult);
        }
        unset($cache);
        
        return $this->clearResult($arResult);
    }
    
    //Возвращает список индексов Мосбиржи
    protected function getIndexList()
    {
        $arResult = array();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 86400;
        $cacheId = $this->CacheTA_ID . 'getIndexList';
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else { //проверяем и кешируем
            $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_EMITENT_ID");
            $arFilter = array("IBLOCK_ID" => IntVal($this->IndexesIblockId), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
            
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while ($ob = $res->Fetch()) {
                $arResult[$ob["PROPERTY_SECID_VALUE"]] = array(
                  "ID"         => $ob["ID"],
                  "NAME"       => $ob["NAME"],
                  "CURRENCYID" => $ob["PROPERTY_CURRENCYID_VALUE"]
                );
            }
            $cache->set($cacheId, $arResult);
        }
        return $this->clearResult($arResult);
    }
    
    
    protected function getDetailInfo()
    {
        $arResult = array();
        $arSecidRegion = explode("_", $this->verb);
        if (count($arSecidRegion) == 2) {
            $this->verb = $arSecidRegion[0];
            if ($arSecidRegion[1] == "RUS") {
                $arResult = $this->getActionDetail();
            }
            if ($arSecidRegion[1] == "USA") {
                $arResult = $this->getActionDetailUsa();
            }
        } else {
            $arResult = array("error" => "not set region");
        }
        
        return $arResult;
    }
    
    /**
     * Формирует массив фильтра для выборок
     *
     * @return array
     *
     * @access protected
     */
    protected function createFilterArray()
    {
        $arTmp = array();
        if (!is_array($this->nargs["filter"])) {
            $this->nargs["filter"] = array($this->nargs["filter"]);
        }
        if (array_key_exists("filter", $this->nargs) && count($this->nargs["filter"]) > 0) {
            foreach ($this->nargs["filter"] as $fval) {
                $delimiter = '';
                if (strpos($fval, "!=") !== false) {
                    $delimiter = "!=";
                } else {
                    if (strpos($fval, ">=") !== false) {
                        $delimiter = ">=";
                    } else {
                        if (strpos($fval, "<=") !== false) {
                            $delimiter = "<=";
                        } else {
                            if (strpos($fval, "!") !== false) {
                                $delimiter = "!";
                            } else {
                                if (strpos($fval, ">") !== false) {
                                    $delimiter = "<";
                                } else {
                                    $delimiter = "=";
                                }
                            }
                        }
                    }
                }
                $arFval = explode($delimiter, $fval);
                $arTmp[$delimiter . $arFval[0]] = $arFval[1]; //!='false'?:false;
            }
            if (count($arTmp) > 0) {
                $this->nargs["filter"] = $arTmp;
                unset($arTmp, $arFval, $delimiter);
            }
        }
    }
    
    
    /**
     * Возвращает список индикаторов для графика теханализа
     *
     * @return array
     *
     * @access protected
     */
    protected function getIndicatorsList()
    {
        //Сброс кеша по изменению инфоблока в
        $arResult = array();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheId = "TA_indicators_list_" . $this->verb;
        $cacheTtl = 86400 * 365;
        //$cache->clean("usa_actions_data");
        if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else {
            $arLineNames = $this->getLineNamesHL();
            
            $arLineParams = $this->getLineParams();
            $arIndicatorsPresets = $this->getIndicatorsPresets();
            $arSelect = array("ID", "NAME", "CODE");
            if ($this->verb == "full") {
                $arSelect = array_merge($arSelect, array("IBLOCK_ID", "PREVIEW_TEXT", "DETAIL_TEXT"));
            }
            $arFilter = array(
              "IBLOCK_ID"   => IntVal($this->TAIndicatorsIblockId),
              "ACTIVE_DATE" => "Y",
              "ACTIVE"      => "Y"
            );
            $res = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
            while ($row = $res->GetNextElement()) {
                $ob = $row->GetFields();
                if ($this->verb == "full") {
                    $arProps = $row->GetProperties();
                    //Обрабатываем свойства
                    foreach ($arProps as $code => $arPropItem) {
                        switch ($code) {
                            case "DATA_DIGIT":
                            case "DATA_CHECKBOX":
                                if (count($arPropItem["VALUE"])) {
                                    foreach ($arPropItem["VALUE"] as $valKey => $val) {
                                        $arVal = explode(":", $val);
                                        $ob["PROPS"][$code][] = array(
                                          "SORT"    => $arVal[0],
                                          "CODE"    => $arVal[1],
                                          "NAME"    => $arVal[2],
                                          "DEFAULT" => $arPropItem["DESCRIPTION"][$valKey]
                                        );
                                    }
                                }
                                break;
                            case "VIS_LINE":
                                if (count($arPropItem["VALUE"])) {
                                    foreach ($arPropItem["VALUE"] as $valKey => $val) {
                                        foreach ($arLineParams[$val]["PROPS"]["LIST"] as $k => $v) {
                                            $arLineParams[$val]["PROPS"]["LIST"][$k] = $arLineNames[$k];
                                        }
                                        $ob["PROPS"][$code][] = array(
                                          "SORT"   => $arLineParams[$val]["SORT"],
                                          "CODE"   => $arLineParams[$val]["CODE"],
                                          "NAME"   => $arLineParams[$val]["NAME"],
                                          "PARAMS" => $arLineParams[$val]["PROPS"]
                                        );
                                    }
                                }
                                break;
                        }
                        
                        
                    }
                    
                    //Получение шаблонов
                    if (array_key_exists($ob["ID"], $arIndicatorsPresets)) {
                        $ob["TEMPLATES"] = $arIndicatorsPresets[$ob["ID"]];
                    } else {
                        $ob["TEMPLATES"] = array();
                    }
                    
                }//full
                
                
                $arResult[] = $ob;
            }
            $cache->set($cacheId, $arResult);
        }
        
        return $this->clearResult($arResult);
    }
    
    
    private function getIndicatorsPresets()
    {
        $arResult = array();
        
        // выборка только активных разделов из инфоблока $IBLOCK_ID, в которых есть элементы
        // со значением свойства SRC, начинающееся с https://
        $arFilter = array(
          'IBLOCK_ID'     => $this->TAIndicatorPresetsIblockId,
          'GLOBAL_ACTIVE' => 'Y',
          'PROPERTY'      => array('!UF_INDICATOR' => false)
        );
        $db_list = CIBlockSection::GetList(array("SORT" => "ASC"), $arFilter, true,
          array("NAME", "ID", "UF_INDICATOR", "DEPTH_LEVEL", "IBLOCK_SECTION_ID"));
        
        $arPresetGroups = array();
        $arPresetTypes = array();
        while ($ar_result = $db_list->GetNext()) {
            $ar_result = $this->clearResult($ar_result);
            if ($ar_result["DEPTH_LEVEL"] == 1) {
                $arPresetTypes[$ar_result["ID"]] = $ar_result;
            } else {
                $arPresetGroups[$ar_result["ID"]] = $ar_result;
            }
            
        }
        
        //Получаем шаблоны и раскладываем по индикаторам
        $arSelect = array("ID", "NAME", "SORT", "IBLOCK_ID", "IBLOCK_SECTION_ID");
        $arFilter = array(
          "IBLOCK_ID"   => IntVal($this->TAIndicatorPresetsIblockId),
          "ACTIVE_DATE" => "Y",
          "ACTIVE"      => "Y"
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        while ($row = $res->GetNextElement()) {
            $arFields = $this->clearResult($row->GetFields());
            unset($arFields["IBLOCK_ID"]);
            $arProps = $this->clearResult($row->GetProperties());
            foreach ($arProps["PARAMS"]["VALUE"] as $k => $v) {
                $arFields["PARAMS"][$v] = $arProps["PARAMS"]["DESCRIPTION"][$k];
            }
            if ($arProps["DEFAULT"]["VALUE"] == "Y") {
                $arFields["DEFAULT"] = true;
            } else {
                $arFields["DEFAULT"] = false;
            }
            $arFieldsTmp = $arFields;
            unset($arFieldsTmp["IBLOCK_SECTION_ID"]);
            $arPresetGroups[$arFields["IBLOCK_SECTION_ID"]]["PRESETS"][] = $arFieldsTmp;
        }
        
        $arResult = array();
        foreach ($arPresetGroups as $arGroupPresets) {
            $arResult[$arGroupPresets["UF_INDICATOR"]] = $arGroupPresets["PRESETS"];
        }
        
        return $arResult;
    }
    
    
    //Возвращает все типы линий индикаторов для ТА
    private function getLineNamesHL()
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->TALineTypesHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        //$arFilter    = array('UF_ITEM' => $arFields["PROPS"]["SECID"]["VALUE"]);
        
        $res = $entityClass::getList(array(
            //"filter" => $arFilter,
            "select" => array(
              "UF_NAME",
              "UF_SORT",
              "ID",
              "UF_XML_ID"
            ),
            //"limit" => 20,
            "order"  => array(
              "UF_SORT" => "ASC"
            ),
            //"cache" => array("ttl" => 3600)
        ));
        while ($item = $res->fetch()) {
            $arResult[$item["UF_XML_ID"]] = $item["UF_NAME"];
            
        }
        return $arResult;
    }
    
    //Возвращает расширенные параметры для линий индикаторов
    private function getLineParams()
    {
        $arResult = array();
        
        $arSelect = array("ID", "NAME", "CODE", "SORT", "IBLOCK_ID");
        $arFilter = array(
          "IBLOCK_ID"   => IntVal($this->TAIndicatorLinesIblockId),
          "ACTIVE_DATE" => "Y",
          "ACTIVE"      => "Y"
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            
            $arProps = $ob->GetProperties();
            
            foreach ($arProps as $code => $arPropItem) {
                
                switch ($code) {
                    case "DEFAULT":
                        $arFields["PROPS"][$code] = $arPropItem["VALUE"];
                        break;
                    case "LIST":
                        //$arFields["PROPS"][$code] = $arPropItem["VALUE"];
                        $arFields["PROPS"][$code] = array_combine(array_keys(array_flip($arPropItem["VALUE"])),
                          array_values($arPropItem["VALUE"]));
                        break;
                    case "DEFAULT_COLOR":
                    case "FILL":
                    case "STROKE":
                        $arFields["PROPS"][$code] = $arPropItem["VALUE"];
                        break;
                    case "OTHER":
                        if (count($arPropItem["VALUE"])) {
                            $arOtherParams = array();
                            foreach ($arPropItem["VALUE"] as $valKey => $val) {
                                $arVal = explode(":", $val);
                                $arOtherParams[$arVal[0]][$arVal[1]] = $arPropItem["DESCRIPTION"][$valKey];
                                $arFields["PROPS"][$code][] = array(
                                  "SORT"    => $arVal[0],
                                  "CODE"    => $arVal[1],
                                  "NAME"    => $arVal[2],
                                  "DEFAULT" => $arPropItem["DESCRIPTION"][$valKey]
                                );
                            }
                            $arFields["PROPS"][$code] = $arOtherParams;
                        }
                        break;
                }
            }
            $arResult[$arFields["ID"]] = $arFields;
        }
        
        return $arResult;
    }
    
    /**
     * Выводит массивы полученных и разобранных параметров для контроля их формата
     * /api/v1/testParams/
     *
     * @return array
     *
     * @access protected
     */
    protected function testParams()
    {
        $arReturn = array(
          "method"   => $this->method,
          "endpoint" => $this->endpoint,
          "verb"     => $this->verb,
          "args"     => $this->args,
          "nargs"    => $this->nargs
        );
        if ($this->method == "PUT" || $this->method == "POST") {
            $arReturn["file"] = \Bitrix\Main\Web\Json::decode($this->file);
        }
        return $arReturn;
    }
    
    
    /**
     * выводит на экран отладку при наличии параметра debug:y в запросе
     *
     * @param  [add type]   $data
     *
     * @return [add type]
     *
     * @access protected
     */
    protected function showDebug($data, $description = '')
    {
        if (isset($this->nargs["debug"]) && $this->nargs["debug"] == 'y') {
            echo $description . "<pre  style='color:black; font-size:11px;'>";
            print_r($data);
            echo "</pre>";
        }
    }
    
    public function clearProps($arProperties)
    {
        $arPropNeedFields = array(
          "ID",
          "NAME",
          "ACTIVE",
          "CODE",
          "PROPERTY_TYPE",
          "MULTIPLE",
          "USER_TYPE",
          "HINT",
          "PROPERTY_VALUE_ID",
          "VALUE",
          "DESCRIPTION",
          "VALUE_ENUM",
          "VALUE_XML_ID"
        );
        
        foreach ($arProperties as $pkey => $arProp) {
            $arrDiff = array_intersect_key($arProp, array_flip($arPropNeedFields));
            $arProperties[$pkey] = $arrDiff;
        }
        
        return $arProperties;
    }
    
    /**
     * Очищает результат от мусора
     *
     * @param array $arFields
     *
     * @return array
     *
     * @access public
     */
    public function clearResult($arFields)
    {
        
        foreach ($arFields as $key => $value) {
            if (is_nan($value)) {
                $arFields[$key] = "";
            }
            if (0 === strpos($key, '~')) {
                unset($arFields[$key]);
            } else {
                if (true == is_array($value)) {
                    $arFields[$key] = $this->clearResult($arFields[$key]);
                }
            }
        }
        
        return $arFields;
    }
}
