<? // Requests from the same server don't have a HTTP_ORIGIN header
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once('classes/CApi.php');
require_once('classes/CTechAnalysisApi.php');

if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
$_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}
try {

$Api = new CTechAnalysisApi($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
echo $Api->processAPI();
} catch (Exception $e) {
echo json_encode(Array('error' => $e->getMessage()));
}