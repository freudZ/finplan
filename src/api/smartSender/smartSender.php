<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

	/**
	 * Роутер для smart sender и апи для сообщений на сайте
	 */
	 if(count($_REQUEST)<=0 && !isset($_REQUEST['action'])) die();
	 $smSender = new CSmartSender();

	 $action = '';
	 $bxUserId = 0;

	 $action = $_REQUEST['action'];
	 $bxUserId = $_REQUEST['bxUserId'];

	 if($action=='subscribeOn'){
	 	if($_SERVER["HTTP_USER_AGENT"]!=='Smart Sender') die();
	 	$smartSenderUid = $_REQUEST['smartSenderUid'];
	 	$smartSenderPhone = $_REQUEST['smartSenderPhone'];
		setUserSubscribeAccountId($bxUserId, $smartSenderUid, $smartSenderPhone);
	 }
	 if($action=='subscribeOff'){
	 	if($_SERVER["HTTP_USER_AGENT"]!=='Smart Sender') die();
	 	$smartSenderUid = $_REQUEST['smartSenderUid'];
		setUserSubscribeOff($bxUserId, $smartSenderUid);
	 }
	 if($action=='sendMessage'){

	 }

	 if($action=='checkSubscribe'){
	 	$res = $smSender->checkSubscribe($bxUserId);
	 	echo $res;

		unset($smSender);
		die();
	 }
//Прописывает пользователю id канала коммуникации для рассылки уведомлений
/*
 * @param $uid int - id пользователя битрикс
 * @param $accountId string - id канала коммуникации leeloo
 */
function setUserSubscribeAccountId($uid, $smartSenderUid, $phone=''){

			global $USER;
			  $rsUser = CUser::GetByID($uid);
			  $arUser = $rsUser->Fetch();

			  if(count($arUser)){

            $oUser = new CUser;

            $aFields = array(
					 'PERSONAL_PHONE' => $phone,
                'UF_SMART_SENDER_UID' => $smartSenderUid,
                'UF_EVENTS_ENABLE' => 1,
                'UF_EVENTS_USE_FAVORITES' => 0,
					 'UF_EVENTS_TYPES' => 'ALL,DIVIDENDS,DIVIDENDS_USA,OFERTA,CANCELLATION,COUPONS,REPORT_RUS,REPORT_USA,IMPORTANT,SOVIET,GOSA,WEBINAR'
            );
            $oUser->Update($uid, $aFields); //$iUserID (int) ID of USER

				//Запись ID Smart Sender в crm
				updateCrmSmartSenderId($uid, $smartSenderUid);

				}
}


function updateCrmSmartSenderId($uid, $smartSenderUid){
	CModule::IncludeModule("iblock");
	$findedCrmId = 0;
	$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_USER");
	$arFilter = Array("IBLOCK_ID"=>19, "=PROPERTY_USER"=>$uid);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($ob = $res->fetch()){
	  $findedCrmId = $ob["ID"];
	}

	if(intval($findedCrmId)>0){
		CIBlockElement::SetPropertyValuesEx($findedCrmId, 19, array("SMART_SENDER_ID" => $smartSenderUid));
	}
}

/**
 * [add description]
 *
 * @param  [add type]   $uid [add description]
 * @param  [add type]   $smartSenderUid [add description]
 *
 * @return [add type]  [add description]
 */
function setUserSubscribeOff($uid, $smartSenderUid){
			global $USER;
			  $rsUser = CUser::GetByID($uid);
			  $arUser = $rsUser->Fetch();

			  if(count($arUser)){

            $oUser = new CUser;

            $aFields = array(
                //'UF_SMART_SENDER_UID' => $smartSenderUid,
                'UF_EVENTS_ENABLE' => 0,
                //'UF_EVENTS_USE_FAVORITES' => 0,
					 //'UF_EVENTS_TYPES' => 'ALL,DIVIDENDS,DIVIDENDS_USA,OFERTA,CANCELLATION,COUPONS,REPORT_RUS,REPORT_USA,IMPORTANT,SOVIET,GOSA,WEBINAR'
            );
            $oUser->Update($uid, $aFields); //$iUserID (int) ID of USER

				//Запись пустого ID Smart Sender в crm
				updateCrmSmartSenderId($uid, "");
				}
}

   		header("HTTP/1.1 200 " ."webhook recived");
			echo json_encode("ok");
			die();
 ?>