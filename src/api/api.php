<? // Requests from the same server don't have a HTTP_ORIGIN header
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once('classes/CApi.php');

if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
$_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}
fwrite(fopen($_SERVER["DOCUMENT_ROOT"]."/api/log/apiLog.log","a"),date("c")." ".print_r($_REQUEST,true)."\n");

if(function_exists("processAPI")){
	echo "processAPI->ok";
}
/* Новый класс добавляется в require_once и подключается новым условием ниже */
try {
$version = $_REQUEST["version"];
$type = $_REQUEST["type"];
if($type=="actives"){ //Класс по работе с активами
	require_once('classes/'.$version.'/CApiActives.php');
	$class = new CApiActives($_REQUEST['request'], $_SERVER['HTTP_ORIGIN'], $version);
}elseif($type=="common"){ //Общие функции
	require_once('classes/'.$version.'/CCommonApi.php');
	$class = new CCommonApi($_REQUEST['request'], $_SERVER['HTTP_ORIGIN'], $version);
}elseif($type=="radar"){ //Класс для работы с радаром, его фильтрами
	require_once('classes/'.$version.'/CRadarApi.php');
	$class = new CRadarApi($_REQUEST['request'], $_SERVER['HTTP_ORIGIN'], $version);
}elseif($type=="learning"){ //Класс для работы с разделом обучения
	require_once('classes/'.$version.'/CLearningApi.php');
	$class = new CLearningApi($_REQUEST['request'], $_SERVER['HTTP_ORIGIN'], $version);
}else{
	echo json_encode(Array('error' => 'method "'.$type.'" are not exist'));
	exit();
}



echo $class->processAPI();
} catch (Exception $e) {
echo json_encode(Array('error' => $e->getMessage()));
}

