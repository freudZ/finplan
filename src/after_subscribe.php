<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Спасибо за подписку");

echo '<div class="content-main content-main-post-item col-md-8 no-float">
	<div class="content-main-wrapper">
		<div class="content-main-post-item-container">
			<div class="content-main-post-item-header clearfix">
				<div class="content-main-post-item-header-title-first">
					<h1>Спасибо за подписку</h1>
				</div>
			</div>
			<div class="content-main-post-item-body">
				<p>Ваш адрес подтверждён, и рассылка на него успешно активирована. </p>
				<p><a href="/">Вернуться на главную</a></p>
			</div>
		</div>
  </div>';

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>