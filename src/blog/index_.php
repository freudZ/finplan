<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
\Bitrix\Main\Loader::includeModule("iblock");
CModule::IncludeModule("iblock");


if($_REQUEST["tag"]){
	$_SESSION["blog"]["show_tag"] = $_REQUEST["tag"];
	LocalRedirect($APPLICATION->GetCurPage("", array("tag")));
}
if($_SESSION["blog"]["show_tag"]){
	?>
	<script>
		$(function () {
		    $(".sections_list_filter.sections_list li[data-value=<?=$_SESSION["blog"]["show_tag"]?>]").attr("data-show", "Y");
        });
	</script>
	<?
	unset($_SESSION["blog"]["show_tag"]);
}

$APPLICATION->SetPageProperty("title", "Блог | Сайт о финансовом планировании Fin-plan.org");
if($_GET['year']) {
	$year = $_GET['year'];
	$month = $_GET['month'] ? $_GET['month'] : '';
	$day = $_GET['day'] ? $_GET['day'] : '';

	if($day>0) {
		$day_new = $day + 1;
		$GLOBALS['arrFilter'] = Array(
		">=DATE_ACTIVE_FROM" => $day.".".$month.".".$year,
		"<=DATE_ACTIVE_FROM" => $day_new.".".$month.".".$year,
  "!SECTION_ID" => 17);
	} elseif($month>0) {
		$month_new = $month + 1;
		$GLOBALS['arrFilter'] = Array(
		">=DATE_ACTIVE_FROM" => "1.".$month.".".$year,
		"<=DATE_ACTIVE_FROM" => "1.".$month_new.".".$year,
  "!SECTION_ID" => 17);
	} elseif($year>0) {
		$year_new = $year + 1;
		$GLOBALS['arrFilter'] = Array(
		">=DATE_ACTIVE_FROM" => "1.1.".$year,
		"<=DATE_ACTIVE_FROM" => "1.1.".$year_new,
  "!SECTION_ID" => 17);
	} else {
		$GLOBALS['arrFilter'] = Array("!SECTION_ID" => 17);
	}
} else {
	$GLOBALS['arrFilter'] = Array("!SECTION_ID" => 17);
}
	
if($_REQUEST["q"]){
	$GLOBALS['arrFilter'][] = array(
        "LOGIC" => "OR",
        array("NAME" => "%".$_REQUEST["q"]."%"),
        array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
    );
}
if($_REQUEST["tags"]){
	$tmp["LOGIC"] = "OR";
	foreach ($_REQUEST["tags"] as $tag){
		$tmp[] = array("PROPERTY_TAGS" => $tag);
	}

	$GLOBALS['arrFilter'][] = $tmp;
}
if($_REQUEST["type"]){
    $items = array();
	$db_props = CIBlockElement::GetProperty(23, intval($_REQUEST["type"]), array("sort" => "asc"), Array("CODE"=>"ITEMS"));
	while($ar_props = $db_props->Fetch()) {
		$GLOBALS['arrFilter']["ID"][] = $ar_props["VALUE"];
	}
}

$newsCount = 18;
if($_REQUEST["SECTION_CODE"]=="slovar-finansovykh-terminov"){
    $newsCount = 9999;
    unset($GLOBALS['arrFilter']["!SECTION_ID"]);
}
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"blog_list_new",
	array(
		"IBLOCK_TYPE" => "FINPLAN",
		"IBLOCK_ID" => "5",
		"NEWS_COUNT" => $newsCount,
		"SORT_BY1" => "PROPERTY_ATTACH_POST",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "DOUBLE_POST",
			1 => "ATTACH_POST",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
		"INCLUDE_SUBSECTIONS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "blog_list",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>