<?   //Получение данных правого баннера в массив TOP и BOTTOM
     $CBanners = new CBanners();
     $arRightBanners = $CBanners->getRightBannerHtml();
	  unset($CBanners);
?>
<div class="sidebar">
	<div class="sidebar_inner">
		<?if(!SIMPLE_PAY_PAGE):?>
        <form class="innerpage_search_form" method="GET">
            <div class="form_element">
                <input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Название / тикер / ISIN" name="q" id="autocomplete_all" />
                <button type="submit"><span class="icon icon-search"></span></button>
            </div>
        </form>
		<?endif;?>
		<p class="title">Посмотреть записи по темам:</p>
		<div class="sidebar_element">
			<ul class="sidebar_menu">
				<?if($_REQUEST["BUY_LEARNING"]):?>
					<?$APPLICATION->IncludeComponent("bitrix:news.list", "menu-items", array(
						"IBLOCK_TYPE" => "FINPLAN",
						"IBLOCK_ID" => "5",
						"NEWS_COUNT" => "999",
						"SORT_BY1" => "SORT",
						"SORT_ORDER1" => "DESC",
						"SORT_BY2" => "ACTIVE_FROM",
						"SORT_ORDER2" => "DESC",
						"FILTER_NAME" => "arrFilter",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "DOUBLE_POST",
							1 => "ATTACH_POST",
						),
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"ADD_SECTIONS_CHAIN" => "N",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION" => 19,
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"PAGER_TITLE" => "Новости",
						"PAGER_SHOW_ALWAYS" => "Y",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "Y",
						"AJAX_OPTION_ADDITIONAL" => ""
						),
						false
					);?>
				<?else:?>
					<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "blog_right_list", Array(
						"IBLOCK_TYPE" => "FINPLAN",	// Тип инфоблока
							"IBLOCK_ID" => "5",	// Инфоблок
							"SECTION_ID" => "",	// ID раздела
							"SECTION_CODE" => "",	// Код раздела
							"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
							"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
							"SECTION_FIELDS" => array(	// Поля разделов
								0 => "",
								1 => "",
							),
							"SECTION_USER_FIELDS" => array(	// Свойства разделов
								0 => "",
								1 => "",
							),
							"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
							"CACHE_TYPE" => "A",	// Тип кеширования
							"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
							"CACHE_GROUPS" => "Y",	// Учитывать права доступа
							"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
							"VIEW_MODE" => "LINE",	// Вид списка подразделов
							"SHOW_PARENT_NAME" => "N",	// Показывать название раздела
						),
						false
					);?>
					<li><a href="/blog/">Все разделы</a></li>
				<?endif?>
			</ul>
		</div>
	  <?if(!SIMPLE_PAY_PAGE):?>
		<div class="sidebar_element">
			<p class="heading">Поиск по дате публикации</p>

			<?$APPLICATION->IncludeComponent("bitrix:news.calendar","blog.calendar",Array(
					"AJAX_MODE" => "Y",
					"IBLOCK_TYPE" => "news",
					"IBLOCK_ID" => "5",
					"MONTH_VAR_NAME" => "month",
					"YEAR_VAR_NAME" => "year",
					"WEEK_START" => "1",
					"DATE_FIELD" => "DATE_ACTIVE_FROM",
					"TYPE" => "EVENTS",
					"SECTION_ID" => "Y",
					"SHOW_YEAR" => "Y",
					"SHOW_TIME" => "Y",
					"TITLE_LEN" => "0",
					"SET_TITLE" => "N",
					"SHOW_CURRENT_DATE" => "N",
					"SHOW_MONTH_LIST" => "N",
					"NEWS_COUNT" => "0",
					"DETAIL_URL" => "",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
			);?>
		</div>

		<div class="sidebar_element text-center">
			<?//Подключение правого баннера фикс.
			   if(is_array($arRightBanners) && array_key_exists("TOP",$arRightBanners)){
				echo $arRightBanners["TOP"];
				}
			?>
        </div>
  <?
	  //Проверка на наличие радара что бы скрыть отзывы для клиентов
	  $userHaveRadar = false;
	  if($USER->IsAuthorized()){
		  $userHaveRadar = checkPayRadar(false, $USER->GetID());
	  }
  ?>
  	 <?if($userHaveRadar==false):?>
		<?$APPLICATION->IncludeComponent("bitrix:news.list", "random_review_right", array(
			"IBLOCK_TYPE" => "FINPLAN",
			"IBLOCK_ID" => "25",
			"CASE_IBLOCK" => "48",
			"NEWS_COUNT" => "1",
			"SORT_BY1" => "RAND",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "ACTIVE_FROM",
			"SORT_ORDER2" => "DESC",
			"FILTER_NAME" => "arrFilter",
			"FIELD_CODE" => array(
				0 => "",
				1 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "DOUBLE_POST",
				1 => "ATTACH_POST",
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "Y",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "Новости",
			"PAGER_SHOW_ALWAYS" => "Y",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "Y",
			"AJAX_OPTION_ADDITIONAL" => ""
		),
			false
		);?>
		 <?endif;?>
		<div class="sidebar_element">
			<p class="heading">Следуйте за нами:</p>

			<ul class="share_list">
				<li><a href="https://vk.com/fin_plan_org" target="_blank" class="icon icon-vk"></a></li>
				<li><a href="https://www.facebook.com/Fin-planorg-1588477478078872/" target="_blank" class="icon icon-fb"></a></li>
                <li><a href="https://www.instagram.com/invest_finplan/" target="_blank" class="icon icon-inst"></a></li>
				<li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li>
			</ul>

			<!-- VK Widget -->
			<div id="vk_groups"></div>

			<!-- fb Widget -->
<!--			<div id="fb-root"></div>
			<div class="fb_outer">
				<div class="fb-page" data-href="https://www.facebook.com/Fin-planorg-1588477478078872/" data-width="215" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Fin-planorg-1588477478078872/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Fin-planorg-1588477478078872/">Fin-plan.org</a></blockquote></div>
			</div>-->

			<!-- instagram Widget -->
			<div class="inst_widget_conetainer">

					<a href="https://www.instagram.com/invest_finplan/" target="_blank" class="instagram_link inst_widget_header_link"><span class="icon icon-inst"></span> Мы в инстаграм</a>

					<div class="zaiv-instagram-gallery-media and_now">
						<a href="https://www.instagram.com/invest_finplan/" target="_blank" class="instagram_link">
					  <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/instagram_section_picture.php"),Array(),Array("MODE"=>"html","NAME"=>"картинка инстаграм")) ?>
						</a>
					</div>

			</div>

		</div>

        <div class="sidebar_bnr_element sidebar_element fixed_element text-center">
		  <? //Подключение плавающей части правого баннера
			   if(is_array($arRightBanners) && array_key_exists("BOTTOM",$arRightBanners)){
				echo $arRightBanners["BOTTOM"];
				}
			 ?>
        </div>
	  <?endif;//!simple_pay?>	  
		<?if(strpos($APPLICATION->GetCurPage(), "/blog/")!==false && $APPLICATION->exclusiveMaterial["vk"] && $APPLICATION->exclusiveMaterial["telegram"]):?>
			<div class="sidebar_element">
				<p class="heading">Эксклюзивные материалы здесь:</p>
				<div class="button_group">
					<a class="button pull-left" target="_blank" rel="nofollow" href="<?=$APPLICATION->exclusiveMaterial["vk"]?>">Рассылка <span class="icon icon-vk"></span></a>
					<a class="button pull-right" target="_blank" rel="nofollow" href="<?=$APPLICATION->exclusiveMaterial["telegram"]?>">Канал <span class="icon icon-telegram"></span></a>
				</div>
			</div>
		<?endif?>
    </div>
</div>