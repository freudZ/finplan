<?
if(isset($_GET['demo'])) {echo phpinfo();exit; }
$show_7_slide = true;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if(isset($_GET['demo2'])) {echo 'TABKD';exit; }
//CLogger::INDEX_DDOS("host=".$_SERVER["HTTP_HOST"]." ip=".$_SERVER["REMOTE_ADDR"]);

CModule::IncludeModule("iblock");
?>
<?$APPLICATION->SetPageProperty("title", "Сайт о финансовом планировании Fin-plan.org");?>
		<div class="wrapper">
			<div class="container">
				<div class="row">
					<div class="main_content col-sm-12">
					</div>
				</div>
			</div>
			<div class="mainpage_wrapper fullpage_slider">
				<div class="section fs_elem_01" data-tooltip="Пошаговое<br/>обучение" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/index_bg_01.jpg);">
					<div class="container">
						<div class="fs_elem_text_01 fs_elem">
							<h1 style="color: #fff !important;">Разумное <br/>инвестирование</h1>

							<p class="yellow">Обучение и веб-сервис</p>

							<p class="description">для сохранения и<br/>приумножения капитала</p>

							<div class="button_list">
								<a class="button highlight" href="/lk/obligations/"<?/*if($USER->IsAuthorized()):?> href="/lk/obligations/"<?else:?> href="#" data-toggle="modal" data-target="#popup_login_back"<?endif*/?>>Сервис</a>
								<a class="button button_orange highlight" href="/learning/">Школа</a>
							</div>
						</div>
					</div>
				</div>

				<div class="section fs_elem_02" data-tooltip="О разумном<br/>инвестировании" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/index_bg_02.jpg);">
					<div class="container">
						<h2>Что такое разумное инвестирование?</h2>
						<div class="fs_elem_text_02 fs_elem">
							<div class="text_inner">
								<p class="big_percents"><span>30</span>%</p>

								<p class="description"><br/>годовых</p>

								<p class="yellow">Расскажем как</p>
							</div>

							<div class="video">
								<span class="video_prev_arr video_arr"></span>

								<div class="video_before"></div>
								<div class="video_inner">
									<div class="video_progress"></div>
									<a href="https://www.youtube.com/embed/NKTqPdISAMk?rel=0&vq=hd1080&modestbranding=1&autohide=1&controls=1" class="play fancybox fancybox.iframe"></a>
								</div>

								<span class="video_next_arr video_arr"></span>

								<a href="#" class="button highlight" data-toggle="modal" data-target="#popup_join">Присоединиться</a>
							</div>
						</div>
					</div>
				</div>
				<div class="section fs_elem_03" data-tooltip="Бесплатное<br/>обучение" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/index_bg_03.jpg);">
					<div class="container">
						<h2>Начните с бесплатного обучения</h2>
						<?$APPLICATION->IncludeComponent("bitrix:news.list", "slide3", Array(
							"IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
								"IBLOCK_ID" => "21",	// Код информационного блока
								"NEWS_COUNT" => "10",	// Количество новостей на странице
								"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
								"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
								"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
								"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
								"FILTER_NAME" => "",	// Фильтр
								"FIELD_CODE" => array(	// Поля
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(	// Свойства
									0 => "TOP_NAME",
									1 => "",
								),
								"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
								"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
								"AJAX_MODE" => "N",	// Включить режим AJAX
								"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
								"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
								"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
								"CACHE_TYPE" => "A",	// Тип кеширования
								"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
								"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
								"CACHE_GROUPS" => "Y",	// Учитывать права доступа
								"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
								"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
								"SET_TITLE" => "N",	// Устанавливать заголовок страницы
								"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
								"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
								"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
								"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
								"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
								"PARENT_SECTION" => "34",	// ID раздела
								"PARENT_SECTION_CODE" => "",	// Код раздела
								"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
								"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
								"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
								"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
								"PAGER_TITLE" => "Новости",	// Название категорий
								"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
								"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
								"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
								"DISPLAY_DATE" => "Y",	// Выводить дату элемента
								"DISPLAY_NAME" => "Y",	// Выводить название элемента
								"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
								"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
								"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
							),
							false
						);?>
					</div>
				</div>

				<div class="section fs_elem_04" data-tooltip="О компании,<br/>эксперты" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/index_bg_04.jpg);">
					<div class="container">
						<div class="fs_elem_text_04 fs_elem">
							<div class="fs_elem_text_04_inner">
								<h2>О компании</h2>

								<div class="mp_about_element">
									<p class="mp_about_big">31,2% годовых</p>
									<p class="mp_about_small">Средняя доходность наших портфелей</p>
								</div>

								<div class="mp_about_element">
									<p class="mp_about_big">Мы обучили инвесторов</p>
									<p class="mp_about_small">из 81 города РФ, из 8 стран</p>
								</div>

								<div class="mp_about_element">
									<p class="mp_about_big">Огромная база бесплатных материалов</p>
									<p class="mp_about_small">165 практических статей</p>

								</div>

								<div class="mp_about_element">
									<p class="mp_about_big">Профессиональная онлайн поддержка в чате</p>
									<p class="mp_about_small">с 9.00 до 23.00 по мск</p>
								</div>
							</div>
						</div>

						<div class="experts_outer fs_elem">
							<p class="experts_subtitle">Эксперты</p>

							<div class="experts_list">
								<div class="experts_element">
									<p class="experts_element_name">Шлячков Николай</p>
									<a class="experts_element_btn" href="#" data-toggle="modal" data-target="#about_shlyachkov">Познакомиться</a>
								</div>

								<div class="experts_element">
									<p class="experts_element_name">Кошин Виталий</p>
									<a class="experts_element_btn" href="#" data-toggle="modal" data-target="#about_koshin">Познакомиться</a>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="section fs_elem_05" data-tooltip="Новые обучающие<br/>материалы" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/index_bg_05.jpg);">
					<div class="container">
						<h2>Новые обучающие материалы на сайте</h2>
						<?
						$arFilter = Array('IBLOCK_ID'=>5, "ACTIVE"=>"Y");
						$db_list = CIBlockSection::GetList(Array("SORT"=>"asc"), $arFilter, false, array("UF_*"));
						while($ar_result = $db_list->GetNext())
						{
							if($ar_result["UF_SHOW_MAIN"]){
								$arSections[] = $ar_result;
							}
						}
						?>
						<div class="fs_elem_05_nav_outer fs_elem">
							<a href="/blog/" class="button button_transparent">Посмотреть все</a>

							<ul class="fs_elem_05_nav fs_elem">
								<li class="opened" data-type="all">все</li>
								<?foreach($arSections as $sect):?>
									<li data-type="section<?=$sect["ID"]?>"><?=$sect["NAME"]?></li>
								<?endforeach?>
							</ul>
						</div>

						<div class="matherials_slider_outer">
							<div class="matherials_slider fs_elem" data-type="all">
								<?$APPLICATION->IncludeComponent("bitrix:news.list", "slide5", Array(
									"IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
										"IBLOCK_ID" => "5",	// Код информационного блока
										"NEWS_COUNT" => "5",	// Количество новостей на странице
										"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
										"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
										"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
										"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
										"FILTER_NAME" => "",	// Фильтр
										"FIELD_CODE" => array(	// Поля
											0 => "",
											1 => "",
										),
										"PROPERTY_CODE" => array(	// Свойства
											0 => "TOP_NAME",
											1 => "",
										),
										"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
										"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
										"AJAX_MODE" => "N",	// Включить режим AJAX
										"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
										"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
										"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
										"CACHE_TYPE" => "A",	// Тип кеширования
										"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
										"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
										"CACHE_GROUPS" => "Y",	// Учитывать права доступа
										"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
										"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
										"SET_TITLE" => "N",	// Устанавливать заголовок страницы
										"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
										"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
										"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
										"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
										"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
										"PARENT_SECTION" => "",	// ID раздела
										"PARENT_SECTION_CODE" => "",	// Код раздела
										"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
										"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
										"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
										"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
										"PAGER_TITLE" => "Новости",	// Название категорий
										"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
										"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
										"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
										"DISPLAY_DATE" => "Y",	// Выводить дату элемента
										"DISPLAY_NAME" => "Y",	// Выводить название элемента
										"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
										"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
										"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
									),
									false
								);?>
							</div>
							<?foreach($arSections as $sect):?>
								<div class="matherials_slider fs_elem hdn" data-type="section<?=$sect["ID"]?>">
									<?$APPLICATION->IncludeComponent("bitrix:news.list", "slide5", Array(
										"IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
											"IBLOCK_ID" => "5",	// Код информационного блока
											"NEWS_COUNT" => "5",	// Количество новостей на странице
											"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
											"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
											"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
											"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
											"FILTER_NAME" => "",	// Фильтр
											"FIELD_CODE" => array(	// Поля
												0 => "",
												1 => "",
											),
											"PROPERTY_CODE" => array(	// Свойства
												0 => "TOP_NAME",
												1 => "",
											),
											"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
											"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
											"AJAX_MODE" => "N",	// Включить режим AJAX
											"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
											"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
											"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
											"CACHE_TYPE" => "A",	// Тип кеширования
											"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
											"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
											"CACHE_GROUPS" => "Y",	// Учитывать права доступа
											"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
											"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
											"SET_TITLE" => "N",	// Устанавливать заголовок страницы
											"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
											"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
											"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
											"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
											"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
											"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
											"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
											"PARENT_SECTION" => $sect["ID"],	// ID раздела
											"PARENT_SECTION_CODE" => "",	// Код раздела
											"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
											"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
											"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
											"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
											"PAGER_TITLE" => "Новости",	// Название категорий
											"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
											"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
											"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
											"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
											"DISPLAY_DATE" => "Y",	// Выводить дату элемента
											"DISPLAY_NAME" => "Y",	// Выводить название элемента
											"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
											"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
											"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
										),
										false
									);?>
								</div>
							<?endforeach?>
						</div>
					</div>
				</div>

				<div class="section fs_elem_06" data-tooltip="Отзывы<br/>учеников" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/index_bg_06.jpg);">
					<div class="container">
						<h2>Отзывы наших учеников</h2>

						<?$APPLICATION->IncludeComponent("bitrix:news.list", "slide6", Array(
							"IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
								"IBLOCK_ID" => "21",	// Код информационного блока
								"NEWS_COUNT" => "6",	// Количество новостей на странице
								"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
								"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
								"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
								"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
								"FILTER_NAME" => "",	// Фильтр
								"FIELD_CODE" => array(	// Поля
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(	// Свойства
									0 => "TOP_NAME",
									1 => "",
								),
								"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
								"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
								"AJAX_MODE" => "N",	// Включить режим AJAX
								"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
								"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
								"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
								"CACHE_TYPE" => "A",	// Тип кеширования
								"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
								"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
								"CACHE_GROUPS" => "Y",	// Учитывать права доступа
								"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
								"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
								"SET_TITLE" => "N",	// Устанавливать заголовок страницы
								"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
								"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
								"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
								"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
								"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
								"PARENT_SECTION" => "35",	// ID раздела
								"PARENT_SECTION_CODE" => "",	// Код раздела
								"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
								"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
								"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
								"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
								"PAGER_TITLE" => "Новости",	// Название категорий
								"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
								"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
								"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
								"DISPLAY_DATE" => "Y",	// Выводить дату элемента
								"DISPLAY_NAME" => "Y",	// Выводить название элемента
								"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
								"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
								"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
							),
							false
						);?>
					</div>
				</div>
				<?if($show_7_slide):?>
					<div class="section fs_elem_07" data-tooltip="Подарок<br/>от Fin-plan" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/index_bg_07.jpg);">
						<div class="container">
							<h2>Первый шаг для старта в инвестициях с нуля</h2>

							<form class="fs_subscribe_form fs_elem" method="post">
								<p class="fs_subscribe_form_text">Давайте перейдем от слов к делу. <br/> Запишитесь на 3-х часовой бесплатный семинар прямо сейчас.</p>

								<div class="row">
									<div class="col col-sm-4">
										<div class="form_element">
											<input type="text" name="name" placeholder="Представьтесь"/>
										</div>
									</div>

									<div class="col col-sm-4">
										<div class="form_element">
											<input type="text" name="phone" placeholder="Ваш телефон"/>
										</div>
									</div>

									<div class="col col-sm-4">
										<div class="form_element">
											<input type="text" name="email" placeholder="Электронная почта"/>
										</div>
									</div>

									<div class="col col-xs-12">
					          <div class="confidencial_element form_element">
					            <div class="checkbox">
					              <input type="checkbox" id="confidencial_mp_seminar" name="confidencial_mp_seminar" checked />
					              <label for="confidencial_mp_seminar">Нажимая кнопку, я даю согласие на обработку персональных данных <br/>и соглашаюсь с </label><span data-toggle="modal" data-target="#footer356">пользовательским соглашением</span><label for="confidencial_mp_seminar"> и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
					            </div>
					          </div>
									</div>
								</div>

								<button type="submit" class="button highlight">Получить доступ</button>
							</form>

							<span class="available fs_elem"></span>
						</div>
					</div>
				<?endif?>
			</div>

			<div class="bottom_elements">
				<div class="container">
					<div class="row">
						<div class="col col-sm-5">
							<p class="copywrite">@ Fin-Plan 2016-<script type="text/javascript">var mdate = new Date(); document.write(mdate.getFullYear());</script></p>
						</div>

						<div class="col col-sm-2 text-center">
							<span class="go_down"></span>
						</div>
 
						<div class="col col-sm-5">
							<a href="#" class="available_to_everyone" data-toggle="modal" data-target="#popup_available_to_everyone"><img src="<?=SITE_TEMPLATE_PATH?>/img/available_to_everyone.png" alt="Доступно кажлому"/></a>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="main_content col-sm-12">
<!-- *****КАРКАС***** -->
					</div>
				</div>
			</div>
		</div>

			<div class="modal fade" id="about_koshin" tabindex="-1">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				  </div>
				  <div class="modal-body">
					<div class="about_investor_table">
						<div class="col img_col">
							<div class="investor_img" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/koshin.jpg);"></div>
						</div>

						<div class="col name_col">
							<p class="modal-title">Кошин Виталий</p>
						</div>
					</div>

					<ul>
						<li>Профессиональный инвестор и финансист, основатель и директор (CEO) компании Fin-plan)</li>
						<li>В сфере финансов с 2002 года</li>
						<li>Опыт создания и управления инвестиционными проектами - 11 лет</li>
						<li>Опыт самостоятельного инвестирования на бирже - 7 лет</li>
						<li>Управлял финансами холдинга с оборотом 5 млрд. рублей в год</li>
						<li>Автор WEB-приложения по управлению личным капиталом Fin-plan.org (релиз в начале 2017 г.)</li>
					</ul>
				  </div>
				</div>
			  </div>
			</div>

			<div class="modal fade" id="about_shlyachkov" tabindex="-1">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				  </div>
				  <div class="modal-body">
					<div class="about_investor_table">
						<div class="col img_col">
							<div class="investor_img" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/shlyachkov.jpg);"></div>
						</div>

						<div class="col name_col">
							<p class="modal-title">Шлячков Николай</p>
						</div>
					</div>

					<ul>
						<li>Профессиональный инвестор и финансист, со-основатель и директор по инвестициям компании Fin-plan</li>
						<li>В сфере инвестиций с 2003 года, опыт самостоятельного инвестирования более 10 лет.</li>
						<li>С 2006 года работал трейдером и персональным финансовыми советником</li>
						<li>Консултационное управление активами в сумме 250 млн. руб.</li>
						<li>Со-автор WEB-приложения по управлению личным капиталом Fin-plan.org</li>
					</ul>
				  </div>
				</div>
			  </div>
			</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
