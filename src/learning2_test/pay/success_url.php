<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include('config.php');

$hash = md5($_REQUEST['action'].';'.$_REQUEST['orderSumAmount'].';'.$_REQUEST['orderSumCurrencyPaycash'].';'.$_REQUEST['orderSumBankPaycash'].';'.$configs['shopId'].';'.$_REQUEST['invoiceId'].';'.$_REQUEST['customerNumber'].';'.$configs['ShopPassword']);		
if (strtolower($hash) != strtolower($_REQUEST['md5'])){
	
	CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>11, "ID"=>intval($_REQUEST["orderNumber"]), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
	if($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$props = $ob->GetProperties();
		
		//CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 10, "PAYED");
		
		$res = CIBlockElement::GetByID($props["ITEM"]["VALUE"]);
		if($ar_res = $res->GetNext()){
			LocalRedirect($ar_res["DETAIL_PAGE_URL"]);
		}
	}
} else {
	CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>11, "ID"=>intval($_REQUEST["orderNumber"]), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
	if($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$props = $ob->GetProperties();
		
		CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 10, "PAYED");
		
		$res = CIBlockElement::GetByID($props["ITEM"]["VALUE"]);
		if($ar_res = $res->GetNext()){
			LocalRedirect($ar_res["DETAIL_PAGE_URL"]);
		}
	}
}
?>