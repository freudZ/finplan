<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Семинары");

CModule::IncludeModule("iblock");
$arFilter = Array("IBLOCK_ID"=>9, "ID"=>intval($_REQUEST["ELEMENT_ID"]), "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
if($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$props = $ob->GetProperties();
	
	if(!checkPaySeminar($arFields["ID"])){
		LocalRedirect("/learning/");
	}
} else {
	LocalRedirect("/learning/");
}
?>
<div class="content-main col-md-8 no-float">
	<? CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>8, "ACTIVE"=>"Y", "!PROPERTY_FOR_LEARNING"=>false);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
	if($ob = $res->GetNextElement())
	{
	$arFields2 = $ob->GetFields();
	$props2 = $ob->GetProperties();
	?>
	<div class="content-main-banner" style="background: url('<?= CFile::GetPath($props2["IMAGE"]["VALUE"]); ?>') -15px 0px no-repeat; background-color: #<?=$props2["COLOR"]["VALUE"]?>; background-position: right center;">
	  <div class="content-main-banner-wrapper">
		<div class="content-main-banner-text">
		<?if($props2["TEXT"]["VALUE"]):?>
			<?= $props2["TEXT"]["VALUE"]; ?>
		<?endif?>
		</div>
		<div class="content-main-banner-start">
			<a <? if(!empty($props2["LINK_BLANK"]["VALUE"])) { ?> target="_blank" <? } ?> <? if(!empty($props2["LINK_NOFOLLOW"]["VALUE"])) { ?> rel="nofollow" <? } ?>
			href="<?= $props2["LINK_URL"]["VALUE"]; ?>"><?= $props2["LINK_TEXT"]["VALUE"]; ?></a>
		</div>
	  </div>
	</div>
	<?
	}
	?>	
	<div class="newwrap">
      <div class="neww">
		<div class="content-main-h1-container new-con">
			<h1 class="content-main-h1"><?=$arFields["NAME"]?></h1>
        </div>
		</div>
      <div class="newwr">
      <div class="content-main-wrapper" style="padding-right:15px;">
        <div class="tolist"><a href="/learning/">К списку семинаров</a></div>
            <div class="row">
				<?for($i=1;$i<=10;$i++):?>
					<?if($props["PART_".$i."_NAME"]["VALUE"]):?>
						<div class="videostr">
							<span>Часть <?=$i?>. <?=$props["PART_".$i."_NAME"]["VALUE"]?></span>
							<?if($props["PART_".$i."_DESC"]["VALUE"]):?>
								<p><?=$props["PART_".$i."_DESC"]["~VALUE"]["TEXT"]?></p>
							<?endif?>
							<?if($props["PART_".$i."_VIDEO"]["VALUE"]):?>
								<div class="one-video2">
									<div class="myvideo">
										<iframe width="702" height="452" src="https://www.youtube.com/embed/<?=$props["PART_".$i."_VIDEO"]["VALUE"]?>" frameborder="0" allowfullscreen=""></iframe>
									</div>
								</div>
							<?endif?>
							<?if($props["PART_".$i."_DESC_AFTER"]["VALUE"]):?>
								<p><?=$props["PART_".$i."_DESC_AFTER"]["~VALUE"]["TEXT"]?></p>
							<?endif?>
						</div>
						<?if($props["DOP_".$i."_INFO"]["VALUE"]):?>
							<div class="dopmat">
								<span>ДОПОЛНИТЕЛЬНЫЕ МАТЕРИАЛЫ</span>
								<?foreach($props["DOP_".$i."_INFO"]["VALUE"] as $n=>$val):
									$arFilter = Array("IBLOCK_ID"=>10, "ID"=>$val, "ACTIVE"=>"Y");
									$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
									if($ob = $res->GetNextElement())
									{
										$item = $ob->GetFields();
										$item_prop = $ob->GetProperties();
									?>
										<div class="mat1 no-bor">
											<div class="lef"><p class="mat-num"><?=$n+1?>.</p></div>
											<div class="lef2"><p><?=$item["~NAME"]?></p></div>
											<?if($item_prop["LINK"]["VALUE"]):?>
												<div class="lef3"> <a href="<?=$item_prop["LINK"]["VALUE"]?>"<?if($item_prop["BLANK"]["VALUE"]):?> target="_blank"<?endif?>>ЧИТАТЬ</a></div>
											<?else:?>
												<div class="lef3"> <a href="<?=CFile::GetPath($item_prop["FILE"]["VALUE"])?>"<?if($item_prop["BLANK"]["VALUE"]):?> target="_blank"<?endif?>>СКАЧАТЬ</a></div>
											<?endif?>
										</div>
									<?}?>
								<?endforeach?>
							</div>
						<?endif?>
					<?endif?>
				<?endfor?>
				<?if($arFields["DETAIL_TEXT"]):?>
					<div class="mat-mail">
						<?=$arFields["DETAIL_TEXT"]?>
					</div>
				<?endif?>
				<?if($props["BONUS"]["VALUE"]["TEXT"]):?>
					<div class="dopmat review">
						<?if(checkBonusSeminar($arFields["ID"])):?>
							<span>ВАШ ПЕРСОНАЛЬНЫЙ БОНУС</span>
							<p>
								<?=$props["BONUS"]["~VALUE"]["TEXT"]?>
							</p>
						<?else:?>
							<span>ВСЕ ПОСМОТРЕЛИ?</span>
						<span>Поздравляю! Вы прошли первый шаг - получение знаний. Теперь практика! Получите бонусный видео-урок по работе с валютными активами и домашнее задание! Для этого просто оставьте свой комментарий по содержанию видео-уроков в форме ниже и нажмите кнопку "Отправить". Бонус и задание откроются автоматически. </span>
							<form method="post" id="send_review_bonus">
								<textarea style="width:100%;height:150px" id="bonus_text" data-item="<?=$arFields["ID"]?>" placeholder="Изменилось ли Ваше представление об инвестициях? Был ли материал понятен и полезен? Если у Вас есть предложения или дополнения по улучшению курса, также напишите их нам. Спасибо!"></textarea>
								<button type="submit" name="send_review" id="send_review" value="Y">Отправить</button>
							</form>
						<?endif?>
					</div>
				<?endif?> 
				
				<br>
				
				<?if($props["BOTTOM_TEXT"]["~VALUE"]["TEXT"]):?>
					<p><?=$props["BOTTOM_TEXT"]["~VALUE"]["TEXT"]?></p>
				<?endif?>
				<br>
				<br>
			</div>
		  </div>
	  </div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>