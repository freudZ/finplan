<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;

if(!$_POST["fio"] || !$_POST["phone"]){
	exit();
}

if (!$USER->IsAuthorized() && $_POST["pay_type"] && intval($_POST["item"]) && $_POST["email"]){
	
	
}elseif (!$USER->IsAuthorized() || !$_POST["pay_type"] || !intval($_POST["item"])){
	exit();
}
	
if (!$USER->IsAuthorized()){
	function generate_password($number)
	{
		$arr = array('1','2','3','4','5','6','7','8','9','0');
		$pass = "";
		for($i = 0; $i < $number; $i++)
		{
		  // Вычисляем случайный индекс массива
		  $index = rand(0, count($arr) - 1);
		  $pass .= $arr[$index];
		}
		return $pass;
	}
	$pass = generate_password(6);
		
	$cUser = new CUser; 
	$sort_by = "ID";
	$sort_ord = "ASC";
	$arFilter = array(
	   "LOGIN_EQUAL" => $_REQUEST["email"],
	   "ACTIVE" => 'Y',
	);
	$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
	if ($arUser = $dbUsers->Fetch()) 
	{
	   exit();
	} else {
		global $USER;
		$arResult = $USER->Register(strip_tags($_POST["email"]), "", "", $pass, $pass, strip_tags($_POST["email"]));
		if($arResult["TYPE"]=="ERROR"){
			exit();
		} else {
			$arEventFields = array(
				"EMAIL" => strip_tags($_POST["email"]),
				"LOGIN" => strip_tags($_POST["email"]),
				"PAS" => $pass
			);
			
			CEvent::Send("NEW_SEM_REG", SITE_ID, $arEventFields);

			$USER->Authorize($arResult["ID"]);
		}
	}
}

CModule::IncludeModule("iblock");

$dop_items = array();
$arFilter = Array("IBLOCK_ID"=>14, "ID"=>intval($_POST["item"]), "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
if($ob = $res->GetNextElement())
{
	$props = $ob->GetProperties();
	$dop_items = $props["DOP_ITEMS"]["VALUE"];
	
	$_POST["item"] = $props["ITEM"]["VALUE"];
	
} else {
	$arFilter = Array("IBLOCK_ID"=>9, "ID"=>intval($_POST["item"]), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
	if($ob = $res->GetNextElement())
	{
		$props = $ob->GetProperties();
		$dop_items = $props["DOP_ITEMS"]["VALUE"];
	} else {
		exit();
	}
}

$arFilter = Array("IBLOCK_ID"=>12, "PROPERTY_YA_CODE"=>$_POST["pay_type"], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
if($ob = $res->GetNextElement())
{
	$item = $ob->GetFields();
} else {
	exit();
}

$el = new CIBlockElement;

$PROP = array();
$PROP["FIO"] = $_POST["fio"];
$PROP["PHONE"] = $_POST["phone"];
$PROP["USER"] = $USER->GetID();
$PROP["ITEM"] = intval($_POST["item"]);
$PROP["BUY_TYPE"] = $item["ID"];
$PROP["DOP_ITEMS"] = $dop_items;

$arLoadProductArray = Array(
  "IBLOCK_SECTION_ID" => false,
  "IBLOCK_ID"      => 11,
  "PROPERTY_VALUES"=> $PROP,
  "NAME"           => date("d.m.Y H:i:s"),
  "ACTIVE"         => "Y",
  "PREVIEW_TEXT"   => $_POST["comment"]
);

if($order_id = $el->Add($arLoadProductArray)):
?>
	<form action="https://money.yandex.ru/eshop.xml" method="post"> 
		<input name="shopId" value="118806" type="hidden"/> 
		<input name="scid" value="49350" type="hidden"/> 
		<input name="sum" value="<?=$props["PRICE"]["VALUE"]?>" type="hidden">
		<input name="orderNumber" value="<?=$order_id?>" type="hidden"> 
		<input name="customerNumber" value="<?=$USER->GetLogin()?>" type="hidden"/> 
		<input name="paymentType" value="<?=$_POST["pay_type"]?>" type="hidden"> 
	</form>
<?endif?>
