<?define("HIDE_LEFT_BLOCK", "Y");
define("SHOW_NEW_FOOTER", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Семинары по инвестициям, обучение инвестициям, Купить семинары по инвестициям, вебинары по инвестициям, семинары по инвестициям на фондовом рынке");
$APPLICATION->SetPageProperty("description", "Компания FIn-plan представляет уникальный цикл семинаров по инвестициям. За 3 недели обучения мы помогаем Вам выйти на профессиональный уровень инвестиций и зарабатывать 30% годовых с минимальными рисками и затратами времени.");
$APPLICATION->SetTitle("Семинары по инвестициям");

function ShowSection($id, $variant, $nopaddingbottom=false){
	CModule::IncludeModule("iblock");
	
	$arFilter = Array("IBLOCK_ID"=>20,"SECTION_ID"=>$id, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
	if(!$res->SelectedRowsCount()){
		return "";
	}
	
	$res2 = CIBlockSection::GetByID($id);
	$arSect = $res2->GetNext();
	
	?>
	<div class="services_<?if($variant==2):?>gray_<?endif?>container"<?if($nopaddingbottom):?> style="padding-bottom:0"<?endif?>>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<p class="subtitle"><?=$arSect["NAME"]?></p>
				</div>
			</div>
			<div class="row">
				<?
				while($ob = $res->GetNextElement())
				{
					$arFields = $ob->GetFields();
					$props = $ob->GetProperties();
					$i++;?>
				
					<?if($variant==1):?>
						<div class="col col-xs-6 col-lg-5<?if(is_float($i/2)):?> col-lg-offset-1<?endif?>">
							<div class="services_element">
								<div class="services_element_head">
									<?if($props["TOP_NAME"]["VALUE"]):?>
										<div class="services_element_head_inner"><?=$props["TOP_NAME"]["VALUE"]?></div>
									<?endif?>
								</div>
								
								<div class="services_element_img">
									<a href="<?=$props["LINK"]["VALUE"]?>">
										<span class="services_element_img_inner" style="background-image: url(<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>);">
										</span>
									</a>
									<?if($props["IMG_TEXT"]["VALUE"]):?>
										<div class="services_element_date"><?=$props["IMG_TEXT"]["VALUE"]?></div>
									<?endif?>
								</div>
								
								<div class="services_element_title_block">
									<div class="services_element_title_block_inner"><p class="services_element_title"><?=$arFields["NAME"]?></p></div>
								</div>
								
								<div class="services_element_body">
									<div class="services_element_body_inner">
										<p><?=$arFields["PREVIEW_TEXT"]?></p>
										
										<a class="button" href="<?=$props["LINK"]["VALUE"]?>"><?=$props["BTN"]["VALUE"]?></a>
									</div>
								</div>
							</div>
						</div>
					<?else:?>
						<div class="col-sm-12 col-lg-10 col-lg-offset-1">
							<div class="services_element"><p class="services_element_title"><?=$arFields["NAME"]?></p>
								<div class="services_element_img">
									<a href="<?=$props["LINK"]["VALUE"]?>">
										<span class="services_element_img_inner" style="background-image: url(<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>);">
										</span>
									</a>
								</div>
								
								<div class="services_element_body">
									<p class="services_element_title"><?=$arFields["NAME"]?></p>
									<p class="green"><?=$props["TOP_NAME"]["VALUE"]?></p>
									<p><?=$arFields["PREVIEW_TEXT"]?></p>
									
									<a class="button" href="<?=$props["LINK"]["VALUE"]?>"><?=$props["BTN"]["VALUE"]?></a>
								</div>
							</div>
						</div>
					<?endif?>
				<?}?>
			</div>
		</div>
	</div>
<?}?>
<style>
@media (min-width: 1630px) {
	.container { width: 1600px; }
}
@media (max-width: 767px) {
	h1, .title { font-size: 26px; }
	.modal-dialog { width: 100%!important; }
	.modal:before { display: none; }
}
body {
	color: #87959a;
	font-family: 'Open Sans', sans-serif;
	font-size: 18px;
    font-weight: 400;
}

.button {
	display: inline-block;
	border: 2px solid #545d4f;
	border-radius: 20px;
	padding: 0 10px;
	width: auto;
	min-width: 146px;
	max-width: 100%;
	height: 40px;
	font-size: 14px;
	text-align: center;
	text-decoration: none!important; 
	text-transform: uppercase;
	color: #545d4f;
	cursor: pointer;
	
	transition: border-color 0.3s, background 0.3s, color 0.3s;
	-webkit-transition: border-color 0.3s, background 0.3s, color 0.3s;
	-moz-transition: border-color 0.3s, background 0.3s, color 0.3s;
}
input.button { background: none; }
.button:hover {
	border-color: #aed200;
	color: #333333;
}
.button:active,
.button:focus {
	border-color: #aed200;
	background: #aed200;
	color: #333333;
}
a.button,
span.button {
	padding-top: 10px;
	padding-bottom: 10px;
}
/* ---------------------------------------------------------------------- */
/* ----------------------------- BOTTOM TEXT BLOCK ---------------------- */
/* ---------------------------------------------------------------------- */

.bottom_text {
	padding-top: 20px;
	padding-bottom: 20px;
}
.bottom_text p {
	margin: 0;
	font-size: 14px;
}

h1, .title {
	font-size: 40px;
	font-weight: 800;
	text-transform: uppercase;
	color: #333333;
}
h2, .subtitle {
	font-size: 20px;
	font-weight: 600;
	text-transform: uppercase;
	color: #333333;
}

.white { color: white; }
.lightgreen { color: #ace616; }



/* ---------------------------------------------------------------------- */
/* ----------------------------- LEARN PAGE ----------------------------- */
/* ---------------------------------------------------------------------- */

.jumb_green {
	position: relative;
	background: url(<?=SITE_TEMPLATE_PATH?>/images/learn_jumb_lg.jpg) no-repeat center;
	background-size: cover;
	height: 505px;
}
.jumb_green.jumb_green_sm {
	height: 285px;
}
.jumb_green:before {
	content: '';
	display: block;
	position: absolute;
	top: 0;
	left: 0;
	z-index: 5;
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAH0lEQVQIW2NkQAU+jEh8HwYGhi0wATAHJAkSgHNAAgBV7gNRbmnIiQAAAABJRU5ErkJggg==) center rgba(1,75,27,0.8);
	width: 100%;
	height: 100%;
}
.jumb_green .container {
	position: relative;
	z-index: 10;
	height: 100%;
}
.jumb_green_inner {
	position: relative;
	padding-top: 140px;
	height: 100%;
}
.jumb_green.jumb_green_sm .jumb_green_inner { padding-top: 100px; }
.jumb_green_inner .title { margin-bottom: 40px; }

.jumb_green_inner .lightgreen {
	position: absolute;
	bottom: 40px;
	left: 0;
	margin: 0;
	width: 100%;
	font-size: 16px;
	font-weight: 800;
	letter-spacing: 3px;
	text-transform: uppercase;
}

@media (max-width: 767px) {
	.jumb_green { height: 320px; }
	.jumb_green_inner {
		padding-top: 50px;
		font-size: 16px;
	}
}
@media (max-width: 567px) {
	.jumb_green_inner {
		padding-top: 50px;
		font-size: 13px;
	}
	.jumb_green_inner .title { font-size: 16px; }
}

.services_container,
.services_gray_container { padding: 75px 0; }
.services_container .subtitle,
.services_gray_container .subtitle { margin-bottom: 45px; }
.services_element {
	border: 1px solid #dfe3e6;
	border-radius: 10px;
	background: white;
	overflow: hidden;
	margin-bottom: 50px;
	
	-webkit-transition: border 0.3s;
	-moz-transition: border 0.3s;
	transition: border 0.3s;
}
.services_element:hover { border-color: #aed200; }

.services_element_head,
.services_element_title_block,
.services_element_body {
	display: table;
	width: 100%;
}
.services_element_head_inner,
.services_element_title_block_inner,
.services_element_body_inner {
	display: table-cell;
	position: relative;
	padding-left: 20px;
	padding-right: 20px;
	width: 100%;
}

.services_element_head_inner {
	padding-bottom: 10px;
	height: 55px;
	vertical-align: bottom;
	font-size: 16px;
	font-weight: 700;
	color: #ffcc00;
}
.services_element.purchase .services_element_head_inner { padding-right: 130px; }
.services_element.purchase .services_element_head_inner:after {
	content: 'Оплачено';
	display: inline-block;
	position: absolute;
	bottom: 10px;
	right: 20px;
	font-size: 16px;
	font-weight: 700;
	text-transform: uppercase;
	color: #2cce00;
}
.services_element_img {
	position: relative;
	overflow: hidden;
	height: 230px;
}
.services_element.purchase .services_element_img a:before {
	content: '';
	display: inline-block;
	position: absolute;
	top: 50%;
	left: 50%;
	z-index: 10;
	border-radius: 50%;
	background: url(<?=SITE_TEMPLATE_PATH?>/images/icons/play_sm.png) no-repeat 43px center #ace616;
	margin-top: -52px;
	margin-left: -52px;
	width: 104px;
	height: 104px;
	
	-webkit-transition: background 0.3s;
	-moz-transition: background 0.3s;
	transition: background 0.3s;
}
.services_element.purchase.not_available .services_element_img a:before {
	background-image: url(<?=SITE_TEMPLATE_PATH?>/images/icons/stop.png);
	background-position: center;
}
.services_element.purchase:hover .services_element_img a:before {
	background-image: url(<?=SITE_TEMPLATE_PATH?>/images/icons/play_sm_hover.png);
	background-color: #069c49;
}
.services_element.purchase.not_available:hover .services_element_img a:before {
	background-image: url(<?=SITE_TEMPLATE_PATH?>/images/icons/stop_hover.png);
}
.services_element_img_inner {
	display: inline-block;
	background: no-repeat center;
	background-size: cover;
	width: 100%;
	height: 100%;
	
	-webkit-transition: opacity 0.3s, -webkit-transform 0.3s;
	-moz-transition: opacity 0.3s, -moz-transform 0.3s;
	transition: opacity 0.3s, transform 0.3s;
}
.services_element:hover .services_element_img_inner {
	opacity: 0.8;
	
	-webkit-transform: scale(1.02);
	-moz-transform: scale(1.02);
	transform: scale(1.02);
}
.services_element_date {
	position: absolute;
	bottom: 25px;
	left: 20px;
	border: 1px solid #dfe3e6;
	border-radius: 10px;
	background: white;
	padding: 12px;
	font-size: 16px;
	font-weight: 600;
	color: #00aa4f;
	
	-webkit-transition: border 0.3s, background 0.3s, color 0.3s;
	-moz-transition: border 0.3s, background 0.3s, color 0.3s;
	transition: border 0.3s, background 0.3s, color 0.3s;
}
.services_element:hover .services_element_date {
	background: #aed200;
	border-color: #aed200;
	color: #333333;
}
.services_element_title_block { padding: 0 20px; }
.services_element_title_block_inner {
	border-bottom: 1px solid #dfe3e6;
	padding: 0;
	height: 100px;
	vertical-align: middle;
}
.services_element_title {
	margin: 0;
	font-size: 20px;
	font-weight: 700;
	text-transform: uppercase;
	color: #333333;
}
.services_element_body_inner {
	padding-top: 20px;
	height: 250px;
	font-size: 18px;
	color: #545d4f;
}
.services_element_body_inner .button {
	position: absolute;
	bottom: 25px;
	left: 20px;
}

.services_gray_container { background: #f4f5f6; }
.services_gray_container .services_element {
	display: table;
	border: none;
	width: 100%;
}
.services_gray_container .services_element > .services_element_title {
	display: none;
	padding: 20px;
}
.services_gray_container .services_element_img,
.services_gray_container .services_element_body {
	display: table-cell;
	position: relative;
	padding: 20px;
	height: 280px;
	vertical-align: middle;
}
.services_gray_container .services_element_img { width: 33.333333%; }
.services_gray_container .services_element_img_inner {
	border-radius: 10px;
	width: 100%;
	height: 100%;
	
	-webkit-transform: none!important;
	-moz-transform: none!important;
	transform: none!important;
}

.services_gray_container .services_element_body {
	padding-bottom: 60px;
	width: 66.666666%;
}
.services_gray_container .services_element_body p {
	font-size: 18px;
	color: #9b9b9b;
}
.services_gray_container .services_element_body .services_element_title {
	margin-bottom: 20px;
	font-size: 20px;
	color: #333333;
}
.services_gray_container .services_element_body .green {
	margin-bottom: 15px;
	font-size: 18px;
	color: #00aa4f;
}
.services_gray_container .services_element_body .button {
	position: absolute;
	bottom: 20px;
	left: 20px;
}

@media (min-width: 768px) and (max-width: 991px) {
	.services_element_title { font-size: 18px; }
	.services_element_body_inner { font-size: 16px; }
	.services_gray_container .services_element_img,
	.services_gray_container .services_element_body { height: 240px; }
	.services_gray_container .services_element_body p { font-size: 16px; }
	.services_gray_container .services_element_body .services_element_title {
		margin-bottom: 10px;
		font-size: 18px;
	}
	.services_gray_container .services_element_body .green {
		margin-bottom: 10px;
		font-size: 16px;
	}
}
@media (min-width: 568px) and (max-width: 767px) {
	.services_element_head_inner { font-size: 14px; }
	.services_element_title_block { padding: 0 15px; }
	.services_element_head_inner,
	.services_element_body_inner {
		padding-left: 15px;
		padding-right: 15px;
	}
	.services_element_title_block_inner {
		padding-left: 0;
		padding-right: 0;
	}
	.services_element_img { height: 200px; }
	.services_element_date {
		bottom: 20px;
		left: 15px;
	}
	.services_element_date {
		padding: 8px 5px;
		font-size: 12px;
	}
	.services_element_title { font-size: 14px; }
	.services_element_body_inner { font-size: 14px; }
	
	.services_gray_container .services_element_img,
	.services_gray_container .services_element_body { height: 240px; }
	.services_gray_container .services_element_body p { font-size: 12px; }
	.services_gray_container .services_element_body .services_element_title {
		margin-bottom: 10px;
		font-size: 14px;
	}
	.services_gray_container .services_element_body .green {
		margin-bottom: 5px;
		font-size: 14px;
	}
}
@media (max-width: 567px) {
	.services_container .col { width: 100%; }
	.services_element_date {
		bottom: 20px;
		left: 15px;
		padding: 7px 5px;
		font-size: 14px;
	}
	.services_element_body_inner {
		height: auto;
		padding-bottom: 70px;
	}
	
	.services_gray_container .services_element,
	.services_gray_container .services_element_img,
	.services_gray_container .services_element_body { display: block; }
	.services_gray_container .services_element_img {
		margin: 0 auto;
		width: 270px;
		height: 250px;
	}
	.services_gray_container .services_element_body {
		padding-bottom: 70px;
		width: 100%;
		height: auto;
	}
	.services_gray_container .services_element > .services_element_title { display: inline-block; }
	.services_gray_container .services_element_body .services_element_title { display: none; }
}

/* ---------------------------------------------------------------------- */
/* --------------------------------- FOOTER ----------------------------- */
/* ---------------------------------------------------------------------- */

.footer_new .row {
	display:inline;
}
.footer_new {
	border-top: 1px solid #d0d5da;
	padding: 20px 0;
}
.footer_new p {
	margin: 0;
	font-size: 14px;
	font-weight: 300;
	color: #99a5a9;
}
.footer_new p span {
	font-size: 10px;
	font-weight: 400;
}
.footer_new a { font-weight: 400;
text-decoration: underline;
    color: #333333;
 }
.footer_black {
    display: inline-block;
	position: relative;
	line-height: 12px;
	font-size: 10px;
	color: #333333;
}
.footer_black a { text-decoration: none!important; }

@media (min-width: 480px) and (max-width: 767px) {
	.footer_new .col { height: 60px; }
}
@media (max-width: 479px) {
	.footer_new .col {
		margin-bottom: 20px;
		width: 100%;
	}
	.footer_new .col:last-child { margin-bottom: 0; }
}
.services_container .row {
	display:inline;
}
</style>
<div class="jumb_green<?if ($USER->IsAuthorized()):?> jumb_green_sm<?endif?>">
	<div class="container">
		<div class="jumb_green_inner text-center">
			<p class="title white">Добро пожаловать в Школу <br/>разумного инвестирования!</p>
			<?if (!$USER->IsAuthorized()):?>
				<p class="white">Здесь собраны наши лучшие курсы, обучающие программы <br/>и материалы, чтобы помочь Вам начать инвестировать с нуля <br/>и добиться стабильных и высоких результатов.</p>
				<p class="lightgreen">Делайте правильный выбор</p>
			<?endif?>
		</div>
	</div>
</div>
<?if(!$USER->IsAuthorized()):?>
<?ShowSection(28, 1)?>
<?ShowSection(29, 2)?>
<?endif?>
<div class="services_container">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<p class="subtitle">Отдельные платные видеокурсы</p>
			</div>
		</div>
		
		<div class="row">
			<?CModule::IncludeModule("iblock");
			$arFilter = Array("IBLOCK_ID"=>9, "ACTIVE"=>"Y");
			if(!$USER->IsAuthorized()){
				$arFilter["!PROPERTY_HIDE_NOT_LOGIN"] = 19;
			}
			$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$arFields["PROPS"] = $ob->GetProperties();
				$arFields["PAYED"] = intval(checkPaySeminar($arFields["ID"]));
				
				$items[] = $arFields;
			}
			
			if($USER->IsAuthorized()){
				$orderBy=array('PAYED'=>'desc', 'SORT'=>'asc');

				function cmp($a, $b) {
				  global $orderBy;
				  $result= 0;
				  foreach( $orderBy as $key => $value ) {
					if( $a[$key] == $b[$key] ) continue;
					$result= ($a[$key] < $b[$key])? -1 : 1;
					if( $value=='desc' ) $result= -$result;
					break;
					}
				  return $result;
				  }
				usort($items, "cmp");
			}

			
			foreach($items as $arFields){
				
				$payed = checkPaySeminar($arFields["ID"]);
				$i++;
				?>
				<div class="col col-xs-6 col-lg-5<?if(is_float($i/2)):?> col-lg-offset-1<?endif?>">
					<div class="services_element<?if($payed):?> purchase<?endif?>">
						<div class="services_element_head">
							<div class="services_element_head_inner">Видеокурс</div>
						</div>
						
						<div class="services_element_img">
							<a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
								<span class="services_element_img_inner" style="background-image: url(<?=CFile::GetPath($arFields['PREVIEW_PICTURE'])?>);">
								</span>
							</a>
						</div>
						
						<div class="services_element_title_block">
							<div class="services_element_title_block_inner"><p class="services_element_title"><?=$arFields["NAME"]?></p></div>
						</div>
						
						<div class="services_element_body">
							<div class="services_element_body_inner">
								<p><?=$arFields["PREVIEW_TEXT"]?></p>
								<a class="button" href="<?=$arFields["DETAIL_PAGE_URL"]?>">Подробнее</a>
							</div>
						</div>
					</div>
				</div>
			<?}?>
		</div>
	</div>
</div>
<?if($USER->IsAuthorized()):?>
	<?ShowSection(29, 2, true)?>
<?endif?>
<?ShowSection(30, 2)?>
<div class="bottom_text">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p>Примеры достижений касаются личных результатов, являются последствием личных действий, знаний и опыта. Мы не несем ответственности за результаты, полученные другими людьми, поскольку они могут отличаться в зависимости от различных обстоятельств.</p>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>