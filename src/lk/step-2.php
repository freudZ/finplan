<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<div class="content-main-lk-param">
    <div class="content-main-lk-param-wrapper">
        <div class="content-main-lk-param-step-counter pull-left">
            Шаг 2
        </div>
        <div class="content-main-lk-param-step-title">
            Заставьте свободные деньги работать
        </div>
    </div>
</div>
<div class="content-main-investment_type clearfix">
    <div class="content-main-investment_type_wrapper">
        <div class="content-main-investment_type-title pull-left">
            1. Выберите вид вложения: <span class="glyphicon glyphicon-question-sign"></span>
        </div>
        <div class="content-main-investment_type-buttons pull-right">
            <!--<a href="#"><span class="glyphicon glyphicon-remove"></span></a>
<a href="#"><span class="glyphicon glyphicon-lock"></span></a>
<a href="#"><span class="glyphicon glyphicon-eye-open"></span></a>-->
        </div>
        <div class="clearfix"></div> 
        <div class="content-main-postlist-sorting-container">
            <a href="#" class="content-main-postlist-sorting-container-active">Депозиты</a><a href="#">Облигации</a><a href="#">Акции</a><a href="#">Памм-счета</a>
        </div>
    </div>
</div>
<div class="content-main-investment_sum_time clearfix">
    <div class="content-main-investment_sum_time-wrapper">
        <div class="content-main-investment-container clearfix">
            <div class="content-main-investment_sum pull-left">
                <div class="content-main-investment_sum-title">
                    2. Задайте сумму вложений (в рублях): <span class="glyphicon glyphicon-question-sign"></span>
                </div>
                <div class="content-main-investment_sum-slider" id="sum-slider">
                </div>
                <input type="text" id="sum-slider-amount" maxlength="8">
            </div>
            <div class="content-main-investment_time pull-right">
                <div class="content-main-investment_time-title">
                    3. Задайте срок вложений (в месяцах): <span class="glyphicon glyphicon-question-sign"></span>
                </div>
                <div class="content-main-investment_time-slider" id="time-slider">
                </div>
                <input type="text" id="time-slider-amount" maxlength="2" disabled>
            </div>
        </div>
        <div class="content-main-filter-step-2">
            <div class="content-main-filter-step-2-control">
                <a class="content-main-filter-step-2-control-link" href="#">Дополнительные параметры поиска</a>
            </div>
            <div class="content-main-filter-step-2-inputs">
                <div class="content-main-filter-step-2-inputs-container unselectable">  
                    <div class="form-group">
                        <input type="text" id="f_bank_title" name="f_bank_title" class="form-control form-control-banktitle" placeholder="Название банка">
                    </div>
                    <div class="form-group">
                        <div class="content-main-filter-step-2-inputs-item">
                            <div>Капитализация процентов</div>
                            <label class="radio-inline">
                                <input type="radio" name="f_capital" id="f_capital_1" value="1"> да
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_capital" id="f_capital_0" value="0"> нет
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_capital" id="f_capital_2" value="2" checked> неважно
                            </label>
                        </div>
                        <div class="content-main-filter-step-2-inputs-item">
                            <div>Пополнение</div>
                            <label class="radio-inline">
                                <input type="radio" name="f_refill" id="f_refill_1" value="1"> да
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_refill" id="f_refill_0" value="0"> нет
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_refill" id="f_refill_2" value="2" checked> неважно
                            </label>
                        </div>
                        <div class="content-main-filter-step-2-inputs-item">
                            <div>Пополнение и частичное снятие</div>
                            <label class="radio-inline">
                                <input type="radio" name="f_refill_and" id="f_refill_and_1" value="1"> да
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_refill_and" id="f_refill_and_0" value="0"> нет
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_refill_and" id="f_refill_and_2" value="2" checked> неважно
                            </label>
                        </div>
                        <div class="content-main-filter-step-2-inputs-item">
                            <div>Рейтинг банка</div>
                            <label class="radio-inline">
                                <input type="radio" name="f_bank_rating" id="f_bank_rating_30" value="30"> ТОП-30
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_bank_rating" id="f_bank_rating_50" value="50"> ТОП-50
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_bank_rating" id="f_bank_rating_100" value="100"> ТОП-100
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="f_bank_rating" id="f_bank_rating" value="" checked> неважно
                            </label>
                        </div>
                        <div class="content-main-filter-step-2-inputs-item">
                            <div>Выплата процентов</div>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="pay_all" checked> выбрать все
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="f_pay_mon" name="f_pay_mon" class="pay_checkbox" checked> ежемесячно
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="f_pay_sec" name="f_pay_sec" class="pay_checkbox" checked> ежеквартально
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="f_pay_half" name="f_pay_half" class="pay_checkbox" checked> раз в полгода
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="f_pay_year" name="f_pay_year" class="pay_checkbox" checked> ежегодно
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="f_pay_end" name="f_pay_end" class="pay_checkbox" checked> в конце срока
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="f_pay_other" name="f_pay_other" class="pay_checkbox" checked> прочие
                            </label>
                        </div>
                        <div class="content-main-filter-step-2-inputs-item">
                            <label for="f_refill_graph"><input type="checkbox" name="f_refill_graph" id="f_refill_graph" checked> Учитывать пополнение на графике</label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="content-main-wrapper">
    <div class="content-main-lk-steplist-fixed">
        <div class="content-main-lk-steplist-item content-main-lk-steplist-item-filled">
            1 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            2 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            3 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            4 шаг
        </div>
        <div class="content-main-lk-steplist-item">
            5 шаг
        </div>
    </div>
    <div class="content-main-step2-charts">
        <div id="chart_results" class="clearfix"><p>Сумма вкладов с пополнением и процентами составит <span>300 000</span> рублей.</p></div>
        <div class="content-main-step2-charts-buttons pull-right">
            <!--<a href="#"><span class="glyphicon glyphicon-remove"></span></a>-->
            <a href="#"><span class="glyphicon glyphicon-lock"></span></a>
            <!--<a href="#"><span class="glyphicon glyphicon-eye-open"></span></a>-->
        </div>
        <div class="clearfix"></div> 
        <div id="chart_summary" class="pull-left">
            <div id="chart_summary_all" class="chart_summary_all">
                Сумма инвестиций <span>300 000 руб.</span>
            </div>
            <div id="chart_summary_dist" class="chart_summary_dist">
                Распределено <span>0 руб.</span>
            </div>
            <div id="chart_summary_left" class="chart_summary_left">
                Не распределено <span>300 000 р.</span>
            </div>
        </div>
        <div id="chart_pie"></div>
        <div id="chart_stacked"></div>
    </div>
    <? 
global $DB;
if($_GET['PAGEN_1']) {
    $pagenum = $_GET['PAGEN_1'];
} else {
    $pagenum = 1;
}

if($_GET['sortfield']) {
    $sortfield = $_GET['sortfield'];
} else {
    $sortfield = 'rate';
}

if($_GET['sorttable']) {
    $sorttable = $_GET['sorttable'];
} else {
    $sorttable = 'a_lk_deposits';
}

if($_GET['sort']) {
    $sort = $_GET['sort'];
} else {
    $sort = 'desc';
}

if($_GET['max_amount']) {
    $max_amount = $_GET['max_amount'];
} else {
    $max_amount = 150000;
}

if($_GET['max_period']) {
    $max_period = $_GET['max_period']*30;
} else {
    $max_period = 24*30;
}

$start_count = ($pagenum-1)*20;
$time_filter = 1449249537;
$results = $DB->Query("SELECT `a_lk_deposits`.*, `a_lk_banks`.`title` as `bank_title`, `a_lk_banks`.`rating` as `bank_rating` 
                FROM `a_lk_deposits` 
                INNER JOIN `a_lk_banks` ON `a_lk_deposits`.`id_bank` = `a_lk_banks`.`id` 
                WHERE 
                `a_lk_deposits`.`parse_date`>=".$time_filter." 
                AND `amount`<=".$max_amount." 
                AND `period` <= ".$max_period." 
                ORDER BY `".$sorttable."`.`".$sortfield."` ".$sort." 
                LIMIT ".$start_count.",20 ");
$deposits_array=array();

while ($row = $results->Fetch())
{
    array_push($deposits_array, $row);
}
    ?>
    <script>
        var curPage = 1;
        var totalPages = 10;
    </script>
    <div class="content-main-step2-table clearfix">
        <div class="content-main-step2-table-title pull-left">
            4. Выберите подходящий вклад для формирования консервативной части портфеля: <span class="glyphicon glyphicon-question-sign"></span>
        </div>
        <table class="table table-hover content-main-step2-table" id="step2-table">
            <thead>
                <tr>
                    <th data-table="a_lk_banks" data-field="title" class="content-main-step2-table-td-col1 header">Банк</th>
                    <th data-table="a_lk_deposits" data-field="title" class="content-main-step2-table-td-col2 header">Продукт</th>
                    <th data-table="a_lk_deposits" data-field="rate" class="content-main-step2-table-td-col3 header headerSortUp">Ставка, %</th>
                    <th data-table="a_lk_deposits" data-field="period" class="content-main-step2-table-td-col4 header">Срок, дней</th>
                    <th data-table="a_lk_banks" data-field="rating" class="content-main-step2-table-td-col5 header">Рейтинг</th>
                    <th data-table="a_lk_deposits" data-field="amount" class="content-main-step2-table-td-col6 header">Минимальная сумма вклада, руб.</th>
                    <th class="content-main-step2-table-td-col7">Выбрать банк</th>
                </tr>
            </thead>
            <tbody>
                <?
foreach($deposits_array as $deposit) {
    $refill_img = '';
    $capital_img = '';
    $pay_img = '';
    if($deposit['refill_and']=='1') {
        $refill_img = '<img title="Возможно пополнение и частичное снятие" src="'.SITE_TEMPLATE_PATH.'/images/dep_refill_and.png">';
    } elseif($deposit['refill']=='1') {
        $refill_img = '<img title="Возможно пополнение" src="'.SITE_TEMPLATE_PATH.'/images/dep_refill.png">';
    }

    if($deposit['capital']=='1') {
        $capital_img = '<img title="Капитализация процентов" src="'.SITE_TEMPLATE_PATH.'/images/dep_capital.png">';
    }

    if($deposit['pay_mon']=='1') {
        $pay_img = '<img title="Выплата процентов ежемесячно" src="'.SITE_TEMPLATE_PATH.'/images/dep_one.png">';
    } elseif($deposit['pay_sec']=='1') {
        $pay_img = '<img title="Выплата процентов ежеквартально" src="'.SITE_TEMPLATE_PATH.'/images/dep_sec.png">';
    } elseif($deposit['pay_half']=='1') {
        $pay_img = '<img title="Выплата процентов раз в полгода" src="'.SITE_TEMPLATE_PATH.'/images/dep_half.png">';
    } elseif($deposit['pay_year']=='1') {
        $pay_img = '<img title="Выплата процентов ежегодно" src="'.SITE_TEMPLATE_PATH.'/images/dep_year.png">';
    } elseif($deposit['pay_end']=='1') {
        $pay_img = '<img title="Выплата процентов в конце срока" src="'.SITE_TEMPLATE_PATH.'/images/dep_end.png">';
    }

                ?>
                <tr id="content-main-step2-table-tr-<?=$deposit['id']?>" onclick="">
                    <input type="hidden" class="content-main-step2-table-td-col1-pay_mon" name="content-main-step2-table-td-col1-pay_mon" value="<?=$deposit['pay_mon']?>">
                    <input type="hidden" class="content-main-step2-table-td-col1-pay_sec" name="content-main-step2-table-td-col1-pay_sec" value="<?=$deposit['pay_sec']?>">
                    <input type="hidden" class="content-main-step2-table-td-col1-pay_half" name="content-main-step2-table-td-col1-pay_half" value="<?=$deposit['pay_half']?>">
                    <input type="hidden" class="content-main-step2-table-td-col1-pay_year" name="content-main-step2-table-td-col1-pay_year" value="<?=$deposit['pay_year']?>">
                    <input type="hidden" class="content-main-step2-table-td-col1-pay_end" name="content-main-step2-table-td-col1-pay_end" value="<?=$deposit['pay_end']?>">
                    <input type="hidden" class="content-main-step2-table-td-col1-capital" name="content-main-step2-table-td-col1-capital" value="<?=$deposit['capital']?>">
                    <input type="hidden" class="content-main-step2-table-td-col1-refill" name="content-main-step2-table-td-col1-refill" value="<?=$deposit['refill']?>">
                    <input type="hidden" class="content-main-step2-table-td-col1-refill_and" name="content-main-step2-table-td-col1-refill_and" value="<?=$deposit['refill_and']?>">
                    <td class="content-main-step2-table-td-col1 content-main-step2-table-td-focus"><?=$deposit['bank_title']?></td>
                    <td class="content-main-step2-table-td-col2 content-main-step2-table-td-focus"><?=$deposit['title']?><div class="content-main-step2-table-td-col2-params"><?=$refill_img;?><?=$capital_img;?><?=$pay_img;?></div></td>
                    <td class="content-main-step2-table-td-col3 content-main-step2-table-td-focus"><?=$deposit['rate']?></td>
                    <td class="content-main-step2-table-td-col4 content-main-step2-table-td-focus"><?=$deposit['period']?></td>
                    <td class="content-main-step2-table-td-col5 content-main-step2-table-td-focus"><?=$deposit['bank_rating']?></td>
                    <td class="content-main-step2-table-td-col6 content-main-step2-table-td-focus"><?=$deposit['amount']?></td>
                    <td class="content-main-step2-table-td-col7"></td>
                </tr>
                <?
}
                ?>
            </tbody>
        </table>
    </div>
    <div class="content-main-postlist-more row">
        <a id="readmore_ajax" href="#">Показать ещё</a>
    </div>
    <div class="content-main-next-step-container clearfix">
        <div class="content-main-next-step-button pull-right">
            <a href="/lk/profile">Далее <span class="glyphicon glyphicon-menu-right" style="top: 3px; display: inline;"></span></a>
        </div>
        <div class="content-main-next-step-title pull-right">
            Спасибо! все графы первого шага заполнены. Идем дальше?
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>