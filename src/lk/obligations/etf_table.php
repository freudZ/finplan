<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$page = 1;
if($_REQUEST["page"]){
	$page = $_REQUEST["page"];
}

$resETF = new ETF();
$arData = $resETF->getTable($page);

?>

<div>
	<div class="calculate_table_head">
		<div class="row">
			<div class="col col_left col-sm-7">
				<p class="calculate_step_title">3. Выберите ETF (показано <?=$arData["shows"]?> из <?=$arData["total_items"]?>):</p>
			</div>
			<div class="col col_right col-sm-5"><span class="calculate_table_clear_btn">Сбросить портфель</span></div>
		</div>
	</div>

	<table class="smart_table">
	  <thead>
		<tr>
		  <th class="name_col">ETF:</th>

		  <th class="price_col">Цена</th>

		  <th class="turnover_col">База ETF&nbsp;/<br/>Комиссия</th>

		  <th class="profitability_col">Средний прирост&nbsp;/ <br/>Просад&nbsp;/ <br/>Бета</th>

		  <th class="inputs_col">Кол-во <br/>лотов</th>

		  <th class="check_col">Выбрать ETF
			<div class="checkbox">
			  <input id="calculate_table_all_etf" class="calculate_table_all" type="checkbox"/>
			  <label for="calculate_table_all_etf"></label>
			</div>
		  </th>
		</tr>
	  </thead>

	  <tbody>
	  	<?/* echo "<pre  style='color:black; font-size:11px;'>";
   print_r($arData["items"]);
   echo "</pre>"; */?>
		<?foreach($arData["items"] as $item):?>
			<tr data-id="<?=$item["PROPS"]["SECID"]?>" data-lotsize="<?= $item["PROPS"]["LOTSIZE"] ?>" data-currency="<?= strtolower($item["PROPS"]["ETF_CURRENCY"]) ?>"<?if(!$arData["access"] && $item["PROPS"]["PROP_TARGET"]>=20):?> data-access="false"<?endif?>>
			  <td class="cshare">
				<p class="elem_name">
					<?if(!$arData["access"] && $item["PROPS"]["ETF_AVG_ICNREASE"]>=20):?>
						<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть ETF</a>
					<?else:?>
						<a href="<?=$item["URL"]?>" class="elem_name__link tooltip_btn" title="<?=$item["NAME"]?> (<?=$item["PROPS"]["SECID"]?>)" portfolio-title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a>
					<?endif?>
				</p>
				<?if($item["COMPANY"]):?>
					<p class="company_name line_green">
						<?if(!$arData["access"] && $item["PROPS"]["ETF_AVG_ICNREASE"]>=20):?>
							<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть компанию</a>
						<?else:?>
							<a class="tooltip_btn" title="<?=$item["COMPANY"]["NAME"]?>" href="<?=$item["COMPANY"]["URL"]?>"><?=$item["COMPANY"]["NAME"]?></a>
						<?endif?>
					</p>
				<?endif?>
				<div class="no_display">
				  <p class="date_off" data-date=""></p>
				</div>
			  </td>
			  <td>

					<p class="elem_cshare_price price_elem" data-price="<?=$item["PROPS"]["LASTPRICE"]?>"><?=$item["PROPS"]["LASTPRICE"]?></p>
					<p class="elem_buy_price price_elem hide"  data-price="<?=$item["DYNAM"]["Цена лота"]?>"><?=$item["DYNAM"]["Цена лота"]?></p>

				<div class="no_display">
				  <p class="elem_sell_price sell_price_up" data-price=""></p>
				  <p class="elem_sell_price sell_price_down" data-price=""></p>
				</div>
			  </td>
			  <td>
				<!--<p class="cshares_turnover" data-turn="<?=$item["PROPS"]["VALTODAY"]?>"></p>-->
				<? $act_type = $item["PROPS"]["ETF_TYPE_OF_ACTIVES"];  //Замена длинных слов
					if(strpos($act_type,"Денежный" )!==false){
					  $act_type = "Денежн.";
					}
					if(strpos($act_type,"Смешанные" )!==false){
					  $act_type = "Cмешан.";
					}
				 ?>
				<p class="cshares_turnover" data-turn="<?=$act_type?>"></p>



				<p class="cshares_turnover" data-turn="<?=$item["PROPS"]["ETF_COMISSION"]."%"?>"></p>
			  </td>
			  <td>
					<?
					$dynam_target = 0;
					$dynam_sales = 0;

					$dynam_target = floatval($item["PROPS"]["ETF_AVG_ICNREASE"])>0 ?  floatval($item["PROPS"]["ETF_AVG_ICNREASE"]) : 0;
					$dynam_sales = floatval($item["DYNAM"]["Просад"])>0 ? floatval($item["DYNAM"]["Просад"]) : 0;

					?>
				<p class="cshare_up_p line_green"  data-value="<?=$dynam_target?>">+<span></span>%</p>
				<p class="cshare_down_p line_red"  data-value="<?=$dynam_sales?>">-<span></span>%</p>
				<p class=""  data-value="<?=$item["PROPS"]["BETTA"]?>"><?=round($item["PROPS"]["BETTA"],2)?></p>
				<div class="no_display">
				  <p class="profit_year_up"></p>
				  <p class="profit_year_down"></p>
				  <p class="profit_main_up"></p>
				  <p class="profit_main_down"></p>
				</div>
			  </td>
			  <td>
				<input class="numberof" type="text" value="5"/>
			  </td>
			  <td>
				<div class="checkbox">
				  <input type="checkbox" id="<?=$item["PROPS"]["SECID"]?>"/>
				  <label for="<?=$item["PROPS"]["SECID"]?>"></label>
				</div>
			  </td>
			</tr>
		<?endforeach?>
	  </tbody>
	</table>

	<div class="more-results-wrap">

	<? $portfolio = new CPortfolio(); ?>
	<?$checkPortfolio = $portfolio->checkPortfolioCntForUser($USER->GetID(), false);
	if($checkPortfolio["PORTFOLIO"]["LIMIT"]>$checkPortfolio["PORTFOLIO"]["OWNER"] || $checkPortfolio["ADMIN"]=="Y" ){
	$createNew = true;
	} else {
	$createNew = false;
	}
	unset($portfolio);
	?>
		  <div class=" text-right">
		  	<br>
	  	  <span class="<?if(!$haveRadarAccess):?>dib relative tooltip_custom_outer<?endif;?> calculate_table_add_portfolio_btn" <?if(!$haveRadarAccess):?>
	  data-content="<?=$APPLICATION->lkRadarPopups["заглушка портфеля"]?>"
	  <?else:?>
	   data-toggle="modal" data-target="#popup_add_portfolio"<?endif;?>><?= ($createNew?'Создать&nbsp;портфель':'Добавить&nbsp;в&nbsp;портфель') ?></span>
		  </div>

		<?if($arData["cur_page"]<$arData["count_page"] && $arData["count_page"]):?>
			<div class="more_results text-center">
				<button class="button" data-next="<?=$arData["cur_page"]+1?>">Загрузить ещё</button>
			</div>
		<?endif?>

	</div>
</div>
<? unset($resETF); ?>
