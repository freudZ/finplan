<?  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");  ?>
<? use \Bitrix\Main\Localization\Loc; ?>
 <?
	 $ResETF = new ETF();
	 $arFilterEnums = $ResETF->getRadarFiltersValues();
	 unset($ResETF);
	 ?>
      <div class="tab-pane fade" id="etf_tab">

         <div class="calculate_first">
            <div class="calculate_sliders_line calculate_line">
               <div class="row">
                  <div class="col col-xs-6">
                     <p class="calculate_step_title">1. Выберите средний прирост x2: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_AVG_ICNREASE")?>">i</span></p>
                     <div class="ETF_AVG_ICNREASE_slider_outer ui_slider_outer">
                         <div class="ui_slider_container">
                             <div class="us_slider_inner">
                                <div class="ETF_AVG_ICNREASE_slider ui_slider" data-value="0"></div>
                                <ul class="ui_slider_scale">
                                   <li>0</li>
                                   <li>10</li>
                                   <li>20</li>
                                   <li>30</li>
                                </ul>
                             </div>
                            <input id="ETF_AVG_ICNREASE_slider_input" name="ETF_AVG_ICNREASE" class="rubber inline" type="text" data-postfix="%" data-prefix=">" />
                         </div>
                     </div>
                  </div>
						<div class="col col-xs-6">
							<p class="calculate_step_title">2. Выберите комиссию: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_COMISSION")?>">i</span></p>
							<div class="ETF_COMISSION_slider_outer ui_slider_outer">
                                <div class="ui_slider_container">
                                    <div class="us_slider_inner">
                                        <div class="ETF_COMISSION_slider ui_slider" data-value="1.5"></div>
                                        <ul class="ui_slider_scale">
                                            <li>0,5</li>
                                            <li>1</li>
                                            <li>1,5</li>
                                            <li>2</li>
                                        </ul>
                                    </div>
								    <input id="ETF_COMISSION_slider_input" name="ETF_COMISSION" class="rubber inline" type="text" data-postfix="%" data-prefix="до" />
                                </div>
							</div>
						</div>
               </div>
					<div class="row">
					    <div class="col col-xs-6">
                     	<div class="form_element">
									<label>Биржа:</label>
									<select class="inline_select" name="ETF_EXCHANGE">
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["ETF_EXCHANGE"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_EXCHANGE")?>">i</span>
								</div>
                  </div>
											<? $CPresets = new RadarPresets;
											   $arPresetsList = array();
												$arPresetsList = $CPresets->getPresetSelectArray();
												unset($CPresets);
											?>
                     <div class="col col-xs-6">
                        <div class="form_element">
                        	<input id="apply_update_etf" type="hidden" value="">
                           <label>Стратегия: </label>
									<select id="preset_etf" name="preset_etf" class="inline_select preset preset_etf" freezed="N" cancel_update="N">
										<option value="">Не выбрана</option>
										<?foreach($arPresetsList['etf'] as $preset=>$query):?>
                               <option value="<?= $query ?>"><?= $preset ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_USA_TEMPLATE")?>">i</span>
                        </div>
                     </div>
					</div>
            </div>

            <div class="calculate_divider big"></div>
            <div><span class="calculate_more collapsed" data-toggle="collapse" data-target="#calculate_form_etf_more">Расширенный фильтр <span class="icon icon-arr_down"></span></span></div>
         </div>
         <div class="calculate_second">
            <div id="calculate_form_etf_more" class="collapse">
               <div class="calculate_line">
						<div class="row">
							<div class="col col-xs-6">
								<div class="form_element">
									<label>Тип активов:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="ETF_TYPE_OF_ACTIVES" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["ETF_TYPE_OF_ACTIVES"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_TYPE_OF_ACTIVES")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
							<div class="col col-xs-6">
								<div class="form_element">
									<label>Дивиденды:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="ETF_DIVIDENDS" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["ETF_DIVIDENDS"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_DIVIDENDS")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col col-xs-6">
								<div class="form_element">
									<label>Страна:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="ETF_COUNTRY" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["ETF_COUNTRY"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_COUNTRY")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
							<div class="col col-xs-6">
								<div class="form_element">
									<label>Отрасль:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="ETF_INDUSTRY" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["ETF_INDUSTRY"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_INDUSTRY")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
						</div>

			<div class="row">
						    <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Рейтинг радара: </label>
                           <select name="radar-rating" class="inline_select" >
							         <option value="all">Все</option>
                              <option value="0-1">до 1%</option>
                              <option value="0-5">до 5%</option>
                              <option value="0-10">до 10%</option>
                              <option value="0-20">до 20%</option>
                              <option value="0-30">до 30%</option>
										<option value="1-100">более 1%</option>
										<option value="5-100">более 5%</option>
										<option value="10-100">более 10%</option>
										<option value="20-100">более 20%</option>
										<option value="30-100">более 30%</option>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_RADAR_RATING")?>">i</span>
                        </div>
                     </div>
					  </div>

               </div>
                <div class="calculate_line">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="calculate_step_title black">Доп. параметры:</p>
                        </div>
                    </div>
						<div class="row">
							<div class="col col-xs-6">
								<div class="form_element">
									<label>Репликация:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="ETF_REPLICATION" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["ETF_REPLICATION"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_REPLICATION")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
							<div class="col col-xs-6">
								<div class="form_element">
									<label>Базовая валюта:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="ETF_BASE_CURRENCY" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
						            <option value="all_not_rub">Не рубль</option>
										<?foreach($arFilterEnums["ETF_BASE_CURRENCY"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_BASE_CURRENCY")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
						</div>
 						<div class="row">
							<div class="col col-xs-6">
								<div class="form_element">
									<label>Тип фонда:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="ETF_TYPE" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["ETF_TYPE"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_TYPE")?>">i</span>
 								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
							<div class="col col-xs-6">
								<div class="form_element">
									<label>Валюта расчета:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="ETF_CURRENCY" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["ETF_CURRENCY"] as $value):?>
										<option value="<?= $value ?>"><?= $value ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_CURRENCY")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
						</div>
						<div class="row">
						    <div class="col col-xs-6">
								<div class="form_element">
									<label>Оператор:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
									<select class="inline_select" name="EMITENT_ID" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						            <option value="all">Все</option>
										<?foreach($arFilterEnums["EMITENT_ID"] as $value):?>
										<option value="<?= $value ?>"><?= $arFilterEnums["EMITENT_NAME"][$value] ?></option>
										<?endforeach;?>
									</select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_EMITENT")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								</div>
							</div>
                     <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Бета: </label>
                           <select name="betta" class="inline_select" >
							         <option value="all">Все</option>
                              <option value="-999>-1">до -1</option>
                              <option value="-1>0">от -1 до 0</option>
                              <option value="0>0.5">от 0 до 0.5</option>
                              <option value="0.5>1">от 0.5 до 1</option>
                              <option value="1>999">больше 1</option>
                              <option value="-9991>1">меньше 1</option>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_BETTA")?>">i</span>
                        </div>
                     </div>
						</div>
                </div>
                <div class="calculate_line">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="calculate_step_title black">Динамика ETF: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ETF_DYNAMIC")?>">i</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>Прирост за месяц</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
                                <select name="month-increase" class="inline_select" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                    <option value="all">Все</option>
                                    <option value="down50">Падение более 50%</option>
                                    <option value="down25">Падение более 25%</option>
                                    <option value="down10">Падение более 10%</option>
                                    <option value="down">Падение</option>
                                    <option value="up">Рост</option>
                                    <option value="up10">Рост более 10%</option>
                                    <option value="up25">Рост более 25%</option>
                                    <option value="up50">Рост более 50%</option>
                                </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>Прирост за год</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
                                <select name="year-increase" class="inline_select" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                    <option value="all">Все</option>
                                    <option value="down50">Падение более 50%</option>
                                    <option value="down25">Падение более 25%</option>
                                    <option value="down10">Падение более 10%</option>
                                    <option value="down">Падение</option>
                                    <option value="up">Рост</option>
                                    <option value="up10">Рост более 10%</option>
                                    <option value="up25">Рост более 25%</option>
                                    <option value="up50">Рост более 50%</option>
                                </select>
 								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>Прирост за 3 года</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
                                <select name="three-year-increase" class="inline_select" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                    <option value="all">Все</option>
                                    <option value="down50">Падение более 50%</option>
                                    <option value="down25">Падение более 25%</option>
                                    <option value="down10">Падение более 10%</option>
                                    <option value="down">Падение</option>
                                    <option value="up">Рост</option>
                                    <option value="up10">Рост более 10%</option>
                                    <option value="up25">Рост более 25%</option>
                                    <option value="up50">Рост более 50%</option>
                                </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
      </div>