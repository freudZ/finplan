<?  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");  ?>
<?use \Bitrix\Main\Localization\Loc;?>
<? $haveRadarAccess = checkPayRadar(); ?>
      <div class="tab-pane fade" id="shares_tab">
         <div class="calculate_first">
            <div class="calculate_sliders_line calculate_line">
               <div class="row">
                  <div class="col col-xs-6">
                     <p class="calculate_step_title">1. Выберите коэффициент р/е акции: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_PE")?>">i</span></p>
                     <div class="coefficient_slider_outer ui_slider_outer">
                         <div class="ui_slider_container">
                             <div class="us_slider_inner">
                                <div class="coefficient_slider ui_slider" data-value="<?=(!empty($_GET['pe'])?$_GET['pe']:$APPLICATION->obligationDefaultForm["pe"])?>"></div>
                                <ul class="ui_slider_scale">
                                   <li>1</li>
                                   <li>5</li>
                                   <li>10</li>
                                   <li>15</li>
                                   <li>20</li>
                                </ul>
                             </div>
                            <input id="coefficient_slider_input" name="pe" class="rubber inline" type="text" data-postfix=" " data-prefix="до" />
                         </div>
                     </div>
                  </div>
                  <div class="col col-xs-6">
                     <p class="calculate_step_title"><span style="position:relative">2. Расчетный потенциал: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_CALC_POTENTIAL")?>">i</span></span></p>
                     <div class="profitability_slider_outer ui_slider_outer">
                         <div class="ui_slider_container">
                             <div class="us_slider_inner">
                                <div class="profitability_slider ui_slider" data-value="<?=(!empty($_GET['profitability'])?$_GET['profitability']:$APPLICATION->obligationDefaultForm["profitability"])?>"></div>
                                <ul class="ui_slider_scale">
                                   <li>10</li>
                                   <li>20</li>
                                   <li>30</li>
                                   <li>40</li>
                                   <li>50</li>
                                </ul>
                             </div>
                            <input id="profitability_slider_input" name="profitability" class="rubber inline" type="text" data-prefix=">" data-postfix="%" />
                         </div>
                     </div>

                  </div>
               </div>
            </div>
            <div class="calculate_checkboxes_line calculate_line">
               <div class="row">
                  <div class="col col-xs-6">
                     <div class="form_element">
                        <div class="checkbox custom">
                            <input type="checkbox" name="turnover_week" checked value="Y" />
                           <label>Убирать из выдачи акции, по которым нет оборотов <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_HIDE_LOWLIQUID")?>">i</span></label>
                        </div>
                     </div>
                  </div>
					 		 
											<? $CPresets = new RadarPresets;
											   $arPresetsList = array();
												$arPresetsList = $CPresets->getPresetSelectArray();
												unset($CPresets);
											?>
                     <div class="col col-xs-6">
                        <div class="form_element">
                        	<input id="apply_update_actions" type="hidden" value="">
                           <label>Стратегия: </label>
									<select id="preset_actions" name="preset_actions" class="inline_select preset preset_actions" freezed="N" cancel_update="N">
										<option value="">Не выбрана</option>
										<?foreach($arPresetsList['shares'] as $preset=>$query):?>
                               <option value="<?= $query ?>"><?= $preset ?></option>
										<?endforeach;?>
									</select>
								 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_TEMPLATE")?>">i</span>
                        </div>
                     </div>


 <!--                    <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Биржа: </label>
                           <select  class="inline_select exchange_id">
                              <option value="moex" selected>Мосбиржа</option>
                              <option value="spbx">СПб биржа</option>
                           </select>
								 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_CAPITALIZATION")?>">i</span>
                        </div>
                     </div>-->

               </div>
                <div class="row">
 <?if(checkPaySeminar(13823, false, $USER->GetID())==true):?>
     <div class="col col-xs-6">
         <div class="form_element">
             <div class="checkbox custom">
                 <input type="checkbox" id="into_gs_portfolio" name="into_gs_portfolio"  value="Y" />
                 <label for="into_gs_portfolio">Есть расширенная аналитика <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_INTO_GS_PORTFOLIO")?>">i</span></label>
             </div>
         </div>
     </div>
 <?endif;?>
                </div>
            </div>
            <div class="calculate_divider big"></div>
            <div><span class="calculate_more collapsed" data-toggle="collapse" data-target="#calculate_form_actions_more">Расширенный фильтр <span class="icon icon-arr_down"></span></span></div>
         </div>
         <div class="calculate_second">
            <div id="calculate_form_actions_more" class="collapse">
               <div class="calculate_line">
                  <div class="row">
                     <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Дивиденды более:</label>
                           <input class="percents_input" name="dividends" type="text" value="0%" />
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_DIVIDENDS")?>">i</span>
                        </div>
                     </div>
                     <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Капитализация более: </label>
                           <select name="capitalization" class="inline_select">
							  <option value="all">Все</option>
                              <option value="100000000">100 млн. руб.</option>
                              <option value="1000000000">1 млрд. руб.</option>
                              <option value="10000000000">10 млрд. руб.</option>
                              <option value="100000000000">100 млрд. руб.</option>
                              <option value="1000000000000">1 трлн. руб.</option>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_CAPITALIZATION")?>">i</span>
                        </div>
                     </div>
                  </div>
						<div class="row">
							<div class="col col-xs-6">

							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["аналитика - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="future_dividends" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Только будущие див-ды</label>
								<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_FUTURE_DIVIDENDS")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>

							</div>
                     <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Стоимость лота: </label>
                           <select name="lotprice" class="inline_select" >
							         <option value="all">Все</option>
                              <option value="0-1000">до 1000 руб.</option>
                              <option value="0-3000">до 3000 руб.</option>
                              <option value="0-5000">до 5000 руб.</option>
                              <option value="0-10000">до 10000 руб.</option>
                              <option value="0-20000">до 20000 руб.</option>

                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_LOT_PRICE")?>">i</span>
                        </div>
                     </div>
						</div>
						<div class="row">
							<div class="col col-xs-6">

							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["аналитика - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="no_dividends" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Нет дивидендов</label>
								<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_NO_DIVIDENDS")?>">i</span>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>

							</div>


						    <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Рейтинг радара: </label>
                           <select name="radar-rating" class="inline_select" >
							         <option value="all">Все</option>
                              <option value="0-1">до 1%</option>
                              <option value="0-5">до 5%</option>
                              <option value="0-10">до 10%</option>
                              <option value="0-20">до 20%</option>
                              <option value="0-30">до 30%</option>
										<option value="1-100">более 1%</option>
										<option value="5-100">более 5%</option>
										<option value="10-100">более 10%</option>
										<option value="20-100">более 20%</option>
										<option value="30-100">более 30%</option>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_RADAR_RATING")?>">i</span>
                        </div>
                     </div>


						</div>
               </div>
                <div class="calculate_line">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="calculate_step_title black">Мультипликаторы: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_MULTIPLICATORS")?>">i</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>P/Equity</label>
                                <select name="p-e" class="inline_select">
                                    <option value="all">Все</option>
                                    <option value="1-2">до 0,5</option>
                                    <option value="1">до 1</option>
                                    <option value="3-2">до 1,5</option>
                                    <option value="2">до 2</option>
                                </select>
										  <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_P_EQUITY")?>">i</span>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>PEG</label>
                                <select name="peg" class="inline_select">
                                    <option value="all">Все</option>
                                    <option value="1-2">до 0,5</option>
                                    <option value="1">до 1</option>
                                    <option value="3-2">до 1,5</option>
                                    <option value="2">до 2</option>
                                </select>
										  <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_PEG")?>">i</span>
                            </div>
                        </div>
                    </div>
 
						  <div class="row">
                     <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Бета: </label>
                           <select name="betta" class="inline_select" >
							         <option value="all">Все</option>
                              <option value="-999>-1">до -1</option>
                              <option value="-1>0">от -1 до 0</option>
                              <option value="0>0.5">от 0 до 0.5</option>
                              <option value="0.5>1">от 0.5 до 1</option>
                              <option value="1>999">больше 1</option>
                              <option value="-9991>1">меньше 1</option>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_BETTA")?>">i</span>
                        </div>
                     </div>
						  </div>

                </div>
                <div class="calculate_line">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="calculate_step_title black">Динамика акций: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_DYNAMIC")?>">i</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>Прирост за месяц</label>
                                <select name="month-increase" class="inline_select">
                                    <option value="all">Все</option>
                                    <option value="down50">Падение более 50%</option>
                                    <option value="down25">Падение более 25%</option>
                                    <option value="down10">Падение более 10%</option>
                                    <option value="down">Падение</option>
                                    <option value="up">Рост</option>
                                    <option value="up10">Рост более 10%</option>
                                    <option value="up25">Рост более 25%</option>
                                    <option value="up50">Рост более 50%</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>Прирост за год</label>
                                <select name="year-increase" class="inline_select">
                                    <option value="all">Все</option>
                                    <option value="down50">Падение более 50%</option>
                                    <option value="down25">Падение более 25%</option>
                                    <option value="down10">Падение более 10%</option>
                                    <option value="down">Падение</option>
                                    <option value="up">Рост</option>
                                    <option value="up10">Рост более 10%</option>
                                    <option value="up25">Рост более 25%</option>
                                    <option value="up50">Рост более 50%</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>Прирост за 3 года</label>
                                <select name="three-year-increase" class="inline_select">
                                    <option value="all">Все</option>
                                    <option value="down50">Падение более 50%</option>
                                    <option value="down25">Падение более 25%</option>
                                    <option value="down10">Падение более 10%</option>
                                    <option value="down">Падение</option>
                                    <option value="up">Рост</option>
                                    <option value="up10">Рост более 10%</option>
                                    <option value="up25">Рост более 25%</option>
                                    <option value="up50">Рост более 50%</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
			   <?$ResActionsRus = new Actions();//Получаем массив списочных свойств для вывода
	         $arFilterEnumsRus = $ResActionsRus->getRadarFiltersValues();
	         unset($ResActionsRus);
				?>
               <div class="calculate_line">
                  <div class="row">
                     <div class="col-xs-12">
                        <p class="calculate_step_title black">Отрасль:</p>
                     </div>
                  </div>
                  <div class="row usual_checkboxes">
					<? $n=0;
					foreach($arFilterEnumsRus["PROP_SEKTOR"] as $nkey=>$item):?>
						<div class="col col-xs-6 col-lg-4">
							<div class="checkbox">
								<input type="checkbox" id="industry_<?=$n?>" name="industry[]" value="<?=$item?>" />
								<label for="industry_<?=$n?>"><span><?=$item?></span></label>
							</div>
						</div>
					<?$n++; endforeach?>
						<div class="col col-xs-12 text-center">
							<div class="checkbox">
								<input type="checkbox" id="shares_all_sectors" />
								<label for="shares_all_sectors"><span class="strong">Все компании, кроме банков</span></label>
							</div>
						</div>
                  </div>
               </div>
               <div class="calculate_line tab_btn_outer">
                  <div class="row">
                     <div class="col col-xs-12">
                        <div class="form_element">
                           <label>Аналитика:</label>
                           <select id="shares_analysis_select" name="analitics" class="tab_btn inline_select">
                              <option value="shares_company_tab">Предприятие</option>
                              <option value="shares_bank_tab">Банк</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="calculate_line three_elements">
                  <div class="tab_container active" id="shares_company_tab">
					<div class="calculate_line_period">
					  <div class="text_element">
						<label>за период:</label>

						<select name="period_value" class="inline_select">
								<option value="last">Последний период</option>
							<?foreach($arKvartalPeriod as $val=>$name):?>
								<option value="<?=$val?>"><?=$name?></option>
							<?endforeach?>
						</select>
						<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_PERIOD")?>">i</span>
					  </div>
					</div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Темп прироста выручки более: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_GROWTH_TEMP")?>">i</span></label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
									<input class="percents_input" name="period[Темп прироста выручки][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="period[Темп прироста выручки][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Рентабельность собственного капитала, более: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_RENT_SK")?>">i</span></label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
                                <input class="percents_input" name="period[Рентабельность собственного капитала][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="period[Рентабельность собственного капитала][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Темп роста активов: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_ACTIVES_GROWTH_TEMP")?>">i</span></label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
                                <input class="percents_input" name="period[Темп роста активов][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="period[Темп роста активов][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Доля собственного капитала в активах: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_SHARE_OF_EQUITY")?>">i</span></label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
                                <input class="percents_input" name="period[Доля собственного капитала в активах][percent]" type="text" value="50%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="period[Доля собственного капитала в активах][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Темп роста прибыли: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_PROFIT_GROWTH_RATE")?>">i</span></label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
                                <input class="percents_input" name="period[Темп роста прибыли][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="period[Темп роста прибыли][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                  </div>
                  <div class="tab_container" id="shares_bank_tab">
                     <div class="calculate_line_period">
					  <div class="text_element">
						<label>за период:</label>
						<select name="month_value" class="inline_select">
								<option value="last">Последний месяц</option>
							<?foreach($arMonthPeriod as $val=>$name):?>
								<option value="<?=$val?>"><?=$name?></option>
							<?endforeach?>
						</select>
					  </div>
					</div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Рейтинг по активам:</label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
                                <select class="inline_select" name="month[Рейтинг по активам, номер][top]"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="20">топ 20</option>
                                   <option value="10">топ 10</option>
                                   <option value="5">топ 5</option>
                                </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
                        <div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="month[Рейтинг по активам, номер][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Рентабельность собственного капитала, более:</label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
                                <input class="percents_input" name="month[Рентабельность собственного капитала, %][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="month[Рентабельность собственного капитала, %][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Доля просроченной задолженности в кредитах, менее:</label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
                                <input class="percents_input" name="month[Доля просрочки, %][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
                        <div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="month[Доля просрочки, %][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Норматив достаточности капитала Н1, более</label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
                                <input class="percents_input" name="month[Н1,%][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - акции"]?>">
								<?endif?>
  								<input type="checkbox" name="month[Н1,%][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                  </div>
               </div>
               <div class="calculate_line">
                  <div class="row">

                     <div class="col col-xs-6">
                        <div class="form_element">
                          <label>Кандидаты на вхождение в индекс:</label>
                           <select name="index_candidate" class="inline_select">
                              <option value="default" selecter>Не учитывать</option>
										<?$n=0; foreach($arFilterEnumsRus["IN_INDEXES"] as $nkey=>$value):?>
										  <option value="<?= $nkey ?>"><?= $value ?></option>
										<?$n++; endforeach;?>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_INCLUDED_IN_INDICES")?>">i</span>
                        </div>
                     </div>

                     <div class="col col-xs-6">
                        <div class="form_element">
                          <label>Возможность маржинальных сделок:</label>
                           <select name="margin_deals" class="inline_select">
                              <option value="default" selecter>Не учитывать</option>
                              <option value="short">Операции «шорт»</option>
                              <option value="kredit">Кредит</option>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_MARGIN_OPPORTUNITY")?>">i</span>
                        </div>
                     </div>
                  </div>
						<div class="row">
                     <div class="col col-xs-6">
                        <div class="form_element">
                          <label>Входят в индексы:</label>
                           <select name="in_index" class="inline_select">
                              <option value="default" selecter>Не учитывать</option>
										<?$n=0; foreach($arFilterEnumsRus["IN_INDEXES"] as $nkey=>$value):?>
										  <option value="<?= $nkey ?>"><?= $value ?></option>
										<?$n++; endforeach;?>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_INCLUDED_IN_INDICES")?>">i</span>
                        </div>
                     </div>
						</div>
               </div>

					<div class="calculate_line">
					  <div class="row">
					     <div class="col col-xs-6">
								<div class="form_element">
                          <label>Доля экспорта:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
								<?endif?>
                           <select name="export_share" class="inline_select" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                              <option value="default" selecter>любая</option>
                              <option value="-1" selecter>без экспорта</option>
                              <option value="25">>25%</option>
                              <option value="50">>50%</option>
                              <option value="70">>70%</option>
                           </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                        </div>
                  </div>
					  </div>
					</div>

             </div>
         </div>
      </div>