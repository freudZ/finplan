<?  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");  ?>
<?use Bitrix\Highloadblock as HL;?>
<?use \Bitrix\Main\Localization\Loc;?>

      <div class="tab-pane fade in active" id="debstock_tab">

         <div class="calculate_first">
            <div class="calculate_sliders_line calculate_line">
               <div class="row">
                  <div class="col col-xs-6">
                     <p class="calculate_step_title">1. Выберите срок облигаций: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_BONDS_RANGE")?>">i</span></p>
                     <div class="time_slider_outer ui_slider_outer">
                        <div class="ui_slider_container">
                            <div class="us_slider_inner">
                                <div class="time_slider ui_slider" data-value="<?=$APPLICATION->obligationDefaultForm["time"]?>"></div>

                                <ul class="ui_slider_scale">
                                    <li>1</li>
                                    <li>5</li>
                                    <li>10</li>
                                    <li>15</li>
                                    <li>20</li>
                                </ul>
                            </div>

                            <input id="time_slider_input" name="duration" class="rubber inline" type="text" data-postfix="мес" data-prefix="до" />
                        </div>

                     </div>
                  </div>
                  <div class="col col-xs-6">
                     <p class="calculate_step_title">2. Выберите уровень доходности ценных бумаг: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_BONDS_PROFITABILITY")?>">i</span></p>
                     <div class="rate_slider_outer ui_slider_outer">
                         <div class="ui_slider_container">
                             <div class="us_slider_inner">
                                <div class="rate_slider ui_slider" data-value="<?=$APPLICATION->obligationDefaultForm["rentab"]?>"></div>
                                <ul class="ui_slider_scale">
                                   <li>1</li>
                                   <li>5</li>
                                   <li>10</li>
                                   <li>15</li>
                                   <li>20</li>
                                </ul>
                             </div>
                            <input id="rate_slider_input" name="rate" class="rubber inline" type="text" data-prefix="до" data-postfix="%" />
                         </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="calculate_checkboxes_line calculate_line">
               <div class="row">
                  <div class="col col-xs-6">
                     <div class="form_element">
                        <div class="checkbox custom">
                           <input type="checkbox" name="turnover_week" checked value="Y" />
                           <label>Убирать из выдачи облигации, по которым нет оборотов <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_HIDE_LOWLIQUID")?>">i</span></label>
                        </div>
                     </div>
                  </div>
							<? $CPresets = new RadarPresets;
							   $arPresetsList = array();
								$arPresetsList = $CPresets->getPresetSelectArray();
								unset($CPresets);
							?>
                     <div class="col col-xs-6">
                        <div class="form_element">
                        	<input id="apply_update_obligations" type="hidden" value="">
                           <label>Стратегия: </label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
									<select id="preset_obligations" name="preset_obligations" class="inline_select preset preset_obligations" freezed="N" cancel_update="N">
										<option value="">Не выбрана</option>
										<?foreach($arPresetsList['bonds'] as $preset=>$query):?>
                               <option value="<?= $query ?>"><?= $preset ?></option>
										<?endforeach;?>
									</select>
 								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
								   <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_TEMPLATE")?>">i</span>
                        </div>
                     </div>


               </div>
            </div>
            <div class="calculate_divider big"></div>
            <div><span class="calculate_more collapsed" data-toggle="collapse" data-target="#calculate_form_more">Расширенный фильтр <span class="icon icon-arr_down"></span></span></div>
         </div>

         <div class="calculate_second">
            <div id="calculate_form_more" class="collapse">
               <div class="calculate_line">
                  <div class="row">
                     <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Купоны более:</label>
                           <div class="dib relative">
                           <input class="percents_input" name="coupons_more" type="text" value="0%" />
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_HIGEST_COUPONS")?>">i</span>
                           </div>
                        </div>
                     </div>
                     <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Валюта облигации:</label>
                           <select class="inline_select" name="valute">
						      <option value="all">Все</option>
                              <option value="SUR">Руб</option>
                              <option value="USD">USD</option>
                              <option value="EUR">EUR</option>
                           </select>
                        </div>
                     </div>
                  </div>
                   <div class="row">
                      <div class="col col-xs-6">
                         <div class="form_element">
                           <div class="with_info_tooltip">
                              <label><span>Качество облигаций:
                                <span class="tooltip_btn" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_QUALITY")?>">i</span></span></label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
									<select class="inline_select" name="quality_bonds"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
									   <option value="all">Все облигации</option>
									   <option value="without_default">Без дефолтных</option>
									   <option value="only_blue_chips">Только голубые фишки</option>
									   <option value="no_risk">Без рисковых</option>
									   <option value="only_default">Только дефолтные</option>
									</select>

								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                         </div>
                      </div>

						    <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Рейтинг радара: </label>
                           <select name="radar-rating" class="inline_select" >
							         <option value="all">Все</option>
                              <option value="0-1">до 1%</option>
                              <option value="0-5">до 5%</option>
                              <option value="0-10">до 10%</option>
                              <option value="0-20">до 20%</option>
                              <option value="0-30">до 30%</option>
										<option value="1-100">более 1%</option>
										<option value="5-100">более 5%</option>
										<option value="10-100">более 10%</option>
										<option value="20-100">более 20%</option>
										<option value="30-100">более 30%</option>
                           </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_RADAR_RATING")?>">i</span>
                        </div>
                     </div>

                   </div>
               </div>
               <div class="calculate_line">
                   <div class="row">
                       <div class="col col-xs-6">
                           <div class="form_element">
                               <label>Оферта облигаций:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
                               <select class="inline_select" name="offerdate"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="all">Все</option>
                                   <option value="yes">Есть оферта</option>
                                   <option value="no">Нет оферты</option>
                               </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
										 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_OFERTA")?>">i</span>
                           </div>
                       </div>

                      <div class="col col-xs-6">
                         <div class="form_element">
                           <div class="with_info_tooltip">
                              <label><span>Цена облигаций:
                                <span class="tooltip_btn" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_PRICE")?>">i</span></span></label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
									<select class="inline_select" name="price_bonds"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
									   <option value="all">Все облигации</option>
									   <option value="higher">Больше номинала</option>
									   <option value="lower">Меньше номинала</option>
									   <option value="equal">Равна номиналу</option>
									   <option value="higher110">от 110% и выше</option>
									   <option value="higher120">от 120% и выше</option>
									   <option value="higher130">от 130% и выше</option>
										<option value="lower90">90% и ниже</option>
										<option value="lower80">80% и ниже</option>
										<option value="lower70">70% и ниже</option>
										<option value="lower50">50% и ниже</option>
									</select>

								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                         </div>
                      </div>

                   </div>
                   <div class="row">
                       <div class="col col-xs-6">
                           <div class="form_element">
                               <label>Листинг:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
                               <select class="inline_select" name="listlevel"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="all">Все</option>
                                   <option value="1">1 уровень</option>
                                   <option value="2">2 уровень</option>
                                   <option value="3">3 уровень</option>
                               </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
										 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_LISTING")?>">i</span>
                           </div>
                       </div>
                       <div class="col col-xs-6">
                           <div class="form_element">
                               <label>Тип купона:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
                               <select class="inline_select" name="coupon_type"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="all">Все</option>
                                   <option value="float">Плавающий</option>
                                   <option value="fix">Фиксированный</option>
                                   <option value="variable">Переменный</option>
                               </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
										 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_COUPON_TYPE")?>">i</span>
                           </div>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col col-xs-6">
                           <div class="form_element">
                               <label>Амортизация:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
                               <select class="inline_select" name="deprecation"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="all">Все</option>
                                   <option value="yes">Есть</option>
                                   <option value="no">Нет</option>
                               </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
										 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_AMORTIZATION")?>">i</span>
                           </div>
                       </div>
                       <div class="col col-xs-6">
                           <div class="form_element">
                               <label>Субординация:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
                               <select class="inline_select" name="payment_order"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="all">Все</option>
                                   <option value="yes">Субординированные</option>
                                   <option value="no">Не субординированные</option>
                               </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
										 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_SUBORDINATION")?>">i</span>
                           </div>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col col-xs-6">
                           <div class="form_element">
                               <label>Структурность:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
                               <select class="inline_select" name="structural"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="all">Все</option>
                                   <option value="yes">Структурные</option>
                                   <option value="no">Неструктурные</option>
                               </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
										 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_STRUCTURED")?>">i</span>
                           </div>
                       </div>
                       <div class="col col-xs-6">
                           <div class="form_element">
                               <label>Дюрация:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
                               <select class="inline_select" name="duration_period" <?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="all">Все</option>
                                   <option value="6m">До 6 мес.</option>
                                   <option value="12m">6 – 12 мес.</option>
                                   <option value="24m">1 – 2 года</option>
                                   <option value="36m">2 – 3 года</option>
                                   <option value="60m">3 – 5 лет</option>
                                   <option value="84m">5 – 7 лет</option>
                                   <option value="120m">7 – 10 лет</option>
                                   <option value="more">Боле 10 лет</option>
                               </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
										 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_DURATION")?>">i</span>
                           </div>
                       </div>
                   </div>

                   <div class="row">
                       <div class="col col-xs-6">
                           <div class="form_element">
                               <label>Количество купонных выплат в год:</label>
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["качество облигаций"]?>">
								<?endif?>
                               <select class="inline_select" name="coupons_in_year"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                                   <option value="all">Все</option>
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5-10">от 5 до 10</option>
                                   <option value="10-999">более 10</option>
                               </select>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
										 <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_COUPON_IN_YEARS")?>">i</span>
                           </div>
                       </div>
                       <div class="col col-xs-6">

                       </div>
                   </div>


               </div>
                <div class="calculate_line">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form_element">
                                <label>Дата выпуска: от
                                    <span class="datepicker-inline__outer">
                                       <input type="text" name="date_start_first" placeholder="любая дата" class="datepicker month-delimetr-action">
                                       <span class="icon icon-arr_down"></span>
                                    </span>

                                </label>
                                <label>до
                                    <span class="datepicker-inline__outer">
                                       <input type="text" name="date_start_last" placeholder="любая дата" class="datepicker month-delimetr-action">
                                       <span class="icon icon-arr_down"></span>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form_element">
                                <label>Дата гашения: от
                                    <span class="datepicker-inline__outer">
                                       <input type="text" name="date_cancel_first" placeholder="любая дата" class="datepicker without-delimetr-action">
                                       <span class="icon icon-arr_down"></span>
                                    </span>
                                </label>
                                <label>до
                                    <span class="datepicker-inline__outer">
                                       <input type="text" name="date_cancel_last" placeholder="любая дата" class="datepicker without-delimetr-action">
                                       <span class="icon icon-arr_down"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
               <div class="calculate_line">
                  <div class="row">
                     <div class="col-xs-12">
                        <p class="calculate_step_title black">Вид облигаций:</p>
                     </div>
                  </div>
                  <div class="row usual_checkboxes">
                     <div class="col col-xs-6">
                        <div class="checkbox">
                           <input type="checkbox" id="check_corporate" name="type[]" value="корпоративные" />
                           <label for="check_corporate"><span>корпоративные</span></label>
                        </div>
                     </div>
                     <div class="col col-xs-6">
                        <div class="checkbox">
                           <input type="checkbox" id="check_federal" name="type[]" value="федеральные" />
                           <label for="check_federal"><span>федеральные</span></label>
                        </div>
                     </div>
                     <?php /* <div class="col col-xs-6 col-md-4">
                        <div class="checkbox">
                           <input type="checkbox" id="check_bluechips" name="type[]" value="голубые фишки" />
                           <label for="check_bluechips">голубые фишки</label>
                        </div>
                     </div> */ ?>
                     <div class="col col-xs-6">
                        <div class="checkbox">
                           <input type="checkbox" id="check_banks" name="type[]" value="банковские" />
                           <label for="check_banks"><span>банковские</span></label>
                        </div>
                     </div>
					           <div class="col col-xs-6">
                        <div class="checkbox">
                           <input type="checkbox" id="check_municipal" name="type[]" value="муниципальные" />
                           <label for="check_municipal"><span>муниципальные</span></label>
                        </div>
                     </div>
                     <?php /* <div class="col col-xs-6 col-md-4">
                        <div class="checkbox">
                           <input type="checkbox" id="check_default" name="type[]" value="дефолтные" />
                           <label for="check_default">дефолтные</label>
                        </div>
                     </div> */ ?>
                  </div>
               </div>
               <div class="calculate_line tab_btn_outer">
                  <div class="row">
                     <div class="col col-xs-12">
                        <div class="form_element">
                           <label>Финансовый анализ:</label>

                           <select id="financial_analysis_select" name="fin_analysis" class="tab_btn inline_select">
                              <option value="corporate">Корпоративные облигации</option>
                              <option value="banks">Банковские облигации</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="calculate_line three_elements">
					<?
					$arMonthPeriod = array();
					$dt = (new DateTime())->modify("-1year");
					while($dt<(new DateTime())){
						$arMonthPeriod[$dt->format("m-Y")] = FormatDate("f Y", $dt->getTimeStamp());
						$dt->modify("+1month");
					}
					$arMonthPeriod = array_reverse($arMonthPeriod);
					?>
                  <div class="tab_container" id="banks">
					<div class="calculate_line_period">
					  <div class="text_element">
						<label>за период:</label>

						<select name="month_value" class="inline_select">
								<option value="last">Последний месяц</option>
							<?foreach($arMonthPeriod as $val=>$name):?>
								<option value="<?=$val?>"><?=$name?></option>
							<?endforeach?>
						</select>
						<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_PERIOD")?>">i</span>
					  </div>
					</div>
         <div class="row">
            <div class="col left_col col-xs-5 col-sm-4 col-md-5">
               <div class="form_element">
                  <label>Рейтинг по активам: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_ACTIVES_RATING")?>">i</span></label>
               </div>
            </div>
            <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
               <div class="form_element">
				<?if(!$haveRadarAccess):?>
					<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
				<?endif?>
					 <select class="inline_select" name="month[Рейтинг по активам, номер][top]"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						 <option value="20">топ 20</option>
						 <option value="10">топ 10</option>
						 <option value="5">топ 5</option>
					  </select>
				<?if(!$haveRadarAccess):?>
					</div>
				<?endif?>
               </div>
            </div>
            <div class="col right_col col-xs-5">
				<div class="checkbox custom">
					<?if(!$haveRadarAccess):?>
						<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
					<?endif?>
						<input type="checkbox" name="month[Рейтинг по активам, номер][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
						<label>Учитывать</label>
					<?if(!$haveRadarAccess):?>
						</div>
					<?endif?>
				</div>
			</div>
           </div>
           <div class="row">
            <div class="col left_col col-xs-5 col-sm-4 col-md-5">
               <div class="form_element">
                  <label>Рентабельность собственного капитала, более:</label>
               </div>
            </div>
            <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
               <div class="form_element">
				<?if(!$haveRadarAccess):?>
					<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
				<?endif?>
                  <input class="percents_input" name="month[Рентабельность собственного капитала, %][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
                <?if(!$haveRadarAccess):?>
					</div>
				<?endif?>
               </div>
            </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
								<?endif?>
  								<input type="checkbox" name="month[Рентабельность собственного капитала, %][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
           </div>
           <div class="row">
            <div class="col left_col col-xs-5 col-sm-4 col-md-5">
               <div class="form_element">
                  <label>Доля просроченной задолженности в кредитах, менее: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_SHARE_OF_OVERDUE_LOANS")?>">i</span></label>
               </div>
            </div>
            <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
               <div class="form_element">
					<?if(!$haveRadarAccess):?>
						<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
					<?endif?>
					<input class="percents_input" name="month[Доля просрочки, %][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
					<?if(!$haveRadarAccess):?>
						</div>
					<?endif?>
               </div>
            </div>
            <div class="col right_col col-xs-5">
  						<div class="checkbox custom">
							<?if(!$haveRadarAccess):?>
								<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
							<?endif?>
    							<input type="checkbox" name="month[Доля просрочки, %][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
    							<label>Учитывать</label>
    						<?if(!$haveRadarAccess):?>
								</div>
							<?endif?>
  						</div>
						</div>
           </div>
           <div class="row">
              <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                 <div class="form_element">
                    <label>Норматив достаточности капитала Н1, более <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_CAPITAL_ADEQUACY_RATIO")?>">i</span></label>
                 </div>
              </div>
              <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                 <div class="form_element">
					<?if(!$haveRadarAccess):?>
						<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
					<?endif?>
                    <input class="percents_input" name="month[Н1,%][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
					<?if(!$haveRadarAccess):?>
						</div>
					<?endif?>
                 </div>
              </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
								<?endif?>
  								<input type="checkbox" name="month[Н1,%][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?>>
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
           </div>
        </div>

        <div class="tab_container active" id="corporate">
					<?
					//текущий
					$kv = @intval( (date('n')-1) /4)+1;
					$year = date("Y");

					//$kv--;
					if($kv==0){
						$kv = 4;
						$year--;
					}

	 //В качестве подмены текущего квартала получаем жестко заданные в админке квартал и год
	 $curActQuart = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_QUARTAL");
  	 $curActYear = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_YEAR");

					$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
					$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
					$entityClass = $entity->getDataClass();

					$res = $entityClass::getList(array(
						"filter" => array(
							"UF_KVARTAL" => $kv,
							"UF_YEAR" => $year
						),
						"select" => array(
							"*"
						),
					));
					if(!$res->getSelectedRowsCount()){
						$kv--;
						if($kv==0){
							$kv = 4;
							$year--;
						}
					}

					if($res->getSelectedRowsCount()){
						$kv= $curActQuart;
						$year = $curActYear;
					}

					$arKvartalPeriod = array(
						$curActQuart."-".$curActYear => $curActQuart." квартал ".$curActYear,
					);
					while(count($arKvartalPeriod)<4){
						$kv--;
						if($kv==0){
							$kv = 4;
							$year--;
						}

						$arKvartalPeriod[$kv."-".$year] = $kv." квартал ".$year;
					}
					?>
					<div class="calculate_line_period">
					  <div class="text_element">
						<label>за период:</label>

						<select name="period_value" class="inline_select">
								<option value="last">Последний период</option>
							<?foreach($arKvartalPeriod as $val=>$name):?>
								<option value="<?=$val?>"><?=$name?></option>
							<?endforeach?>
						</select>
						<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_PERIOD")?>">i</span>
					  </div>
					</div>

         <div class="row">
            <div class="col left_col col-xs-5 col-sm-4 col-md-5">
               <div class="form_element">
                  <label>Темп прироста выручки более: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_REVENUE_GROWTH_PERIOD")?>">i</span></label>
               </div>
            </div>
            <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
               <div class="form_element">
					<?if(!$haveRadarAccess):?>
						<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
					<?endif?>
                    <input class="percents_input" name="period[Темп прироста выручки][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
					<?if(!$haveRadarAccess):?>
						</div>
					<?endif?>
               </div>
            </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
							<?if(!$haveRadarAccess):?>
								<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
							<?endif?>
  								<input type="checkbox" name="period[Темп прироста выручки][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
							<?if(!$haveRadarAccess):?>
								</div>
							<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Рентабельность собственного капитала, более: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_RETURN_ON_EQUITY")?>">i</span></label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
							<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
								<?endif?>
                              <input class="percents_input" name="period[Рентабельность собственного капитала][percent]" type="text" value="10%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
							<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
								<?endif?>
  								<input type="checkbox" name="period[Рентабельность собственного капитала][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
									<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                     <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                           <div class="form_element">
                              <label>Доля собственного капитала в активах более: <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_OBL_SHARE_OF_EQUITY")?>">i</span></label>
                           </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                           <div class="form_element">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
								<?endif?>
                              <input class="percents_input" name="period[Доля собственного капитала в активах][percent]"  type="text" value="50%"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
                           </div>
                        </div>
						<div class="col right_col col-xs-5">
							<div class="checkbox custom">
								<?if(!$haveRadarAccess):?>
									<div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["финансовый анализ - облигации"]?>">
								<?endif?>
  								<input type="checkbox" name="period[Доля собственного капитала в активах][use]" value="Y"<?if(!$haveRadarAccess):?> disabled="disabled"<?endif?> />
  								<label>Учитывать</label>
								<?if(!$haveRadarAccess):?>
									</div>
								<?endif?>
							</div>
						</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
