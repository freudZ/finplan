<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<? //Содержимое фильтра акций США ?>
<? use \Bitrix\Main\Localization\Loc; ?>
<div class="tab-pane fade" id="shares_usa_tab">
    <div class="calculate_first">
        <div class="calculate_sliders_line calculate_line">
            <div class="row">
                <div class="col col-xs-6">
                    <p class="calculate_step_title">1. Выберите коэффициент р/е акции: <span
                                class="tooltip_btn p-tooltip" title=""
                                data-original-title="<?= Loc::getMessage("RAD_ACT_PE") ?>">i</span></p>
                    <div class="coefficient_slider_usa_outer ui_slider_outer">
                        <div class="ui_slider_container">
                            <div class="us_slider_inner">
                                <div class="coefficient_slider_usa ui_slider"
                                     data-value="<?= $APPLICATION->actionUSADefaultForm["pe"] ?>"></div>
                                <ul class="ui_slider_scale">
                                    <li>10</li>
                                    <li>20</li>
                                    <li>30</li>
                                    <li>40</li>
                                    <li>50</li>
                                </ul>
                            </div>
                            <input id="coefficient_slider_usa_input" name="pe" class="rubber inline" type="text"
                                   data-postfix=" " data-prefix="до"/>
                        </div>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <p class="calculate_step_title"><span style="position:relative">2. Расчетный потенциал: <span
                                    class="tooltip_btn p-tooltip" title=""
                                    data-original-title="<?= Loc::getMessage("RAD_ACT_CALC_POTENTIAL") ?>">i</span></span>
                    </p>
                    <div class="profitability_slider_usa_outer ui_slider_outer">
                        <div class="ui_slider_container">
                            <div class="us_slider_inner">
                                <div class="profitability_slider_usa ui_slider"
                                     data-value="<?= $APPLICATION->actionUSADefaultForm["profitability"] ?>"></div>
                                <ul class="ui_slider_scale">
                                    <li>10</li>
                                    <li>20</li>
                                    <li>30</li>
                                    <li>40</li>
                                    <li>50</li>
                                </ul>
                            </div>
                            <input id="profitability_slider_usa_input" name="profitability" class="rubber inline"
                                   type="text" data-prefix=">" data-postfix="%"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <? $ResActionsUSA = new ActionsUSA();
        $arFilterEnums = $ResActionsUSA->getRadarFiltersValues();
        unset($ResActionsUSA);
        ?>
        <div class="calculate_checkboxes_line calculate_line ">
            <div class="row">
                <? $CPresets = new RadarPresets;
                $arPresetsList = array();
                $arPresetsList = $CPresets->getPresetSelectArray();
                unset($CPresets);
                ?>
                <div class="col col-xs-6">
                    <div class="form_element">
                        <input id="apply_update_actions_usa" type="hidden" value="">
                        <label class="onb_usa_strategy">Стратегия: </label>
                        <select id="preset_actions_usa"  name="preset_actions_usa"
                                class="inline_select preset preset_actions_usa " freezed="N" cancel_update="N">
                            <option value="">Не выбрана</option>
                            <? foreach ($arPresetsList['shares_usa'] as $preset => $query): ?>
                                <option value="<?= $query ?>"><?= $preset ?></option>
                            <? endforeach; ?>
                        </select>
                        <span class="tooltip_btn p-tooltip" title=""
                              data-original-title="<?= Loc::getMessage("RAD_ACT_USA_TEMPLATE") ?>">i</span>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <div class="form_element">
                        <label>Валюта:</label>
                        <?if(!$haveUSARadarAccess):?>
                        <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                            <?endif?>
                            <select name="currency" class="inline_select noClearPreset" <?if(!$haveUSARadarAccess):?> disabled="disabled" <?endif;?>>
                                <option value="all" selected>Не учитывать</option>
                                <?foreach($arFilterEnums["CURRENCY"] as $n=>$item):?>
                                    <option value="<?= $item ?>"><?= $item ?></option>
                                <?endforeach;?>
                                <!--      <option value="SP500">SP500</option>-->
                            </select>
                            <?if(!$haveUSARadarAccess):?>
                        </div>
                    <?endif?>
                        <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_INCLUDED_IN_INDICES")?>">i</span>
                    </div>
                </div>
                <!--                     <div class="col col-xs-6">
                        <div class="form_element">
                           <label>Биржа: </label>
                           <select class="inline_select exchange_id">
                              <option value="moex">Мосбиржа</option>
                              <option value="spbx" selected>СПб биржа</option>
                           </select>
								 	<span class="tooltip_btn p-tooltip" title="" data-original-title="<?= Loc::getMessage("RAD_ACT_CAPITALIZATION") ?>">i</span>
                        </div>
                     </div>-->

            </div>


        </div>


        <div class="calculate_line">
            <div class="row">
                <div class="col col-xs-6">
                    <div class="form_element">
                        <label>Входят в индекс:</label>
                        <?if(!$haveUSARadarAccess):?>
                        <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                            <?endif?>
                            <select name="in_index" class="inline_select noClearPreset" <?if(!$haveUSARadarAccess):?> disabled="disabled" <?endif;?>>
                                <option value="all" selected>Не учитывать</option>
                                <?foreach($arFilterEnums["SP500"] as $n=>$item):?>
                                    <option value="<?= $item ?>"><?= $item ?></option>
                                <?endforeach;?>
                                <!--      <option value="SP500">SP500</option>-->
                            </select>
                            <?if(!$haveUSARadarAccess):?>
                        </div>
                    <?endif?>
                        <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_INCLUDED_IN_INDICES")?>">i</span>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <div class="form_element">
                        <label>Страна:</label>
                        <?if(!$haveUSARadarAccess):?>
                        <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                            <?endif?>
                            <select name="country" class="inline_select noClearPreset" <?if(!$haveUSARadarAccess):?> disabled="disabled" <?endif;?>>
                                <option value="all" selected>Не учитывать</option>
                                <?foreach($arFilterEnums["COUNTRY"] as $n=>$item):?>
                                    <option value="<?= $item ?>"><?= $item ?></option>
                                <?endforeach;?>
                            </select>
                            <?if(!$haveUSARadarAccess):?>
                        </div>
                    <?endif?>
                        <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_USA_COUNTRY")?>">i</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <div class="form_element">
                        <label>Подборка от fin-plan:</label>
                        <?if(!$haveUSARadarAccess):?>
                        <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                            <?endif?>
                            <select name="top" class="inline_select noClearPreset" <?if(!$haveUSARadarAccess):?> disabled="disabled" <?endif;?>>
                                <option value="all" selected>Не учитывать</option>
                                <?foreach($arFilterEnums["TOP"] as $n=>$item):?>
                                    <option value="<?= $item ?>"><?= $item ?></option>
                                <?endforeach;?>
                            </select>
                            <?if(!$haveUSARadarAccess):?>
                        </div>
                    <?endif?>
                        <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_USA_TOP")?>">i</span>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <div class="form_element">
                        <label>Отрасль:</label>
                        <?if(!$haveUSARadarAccess):?>
                        <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                        <?endif?>
                            <select name="industry" class="inline_select noClearPreset" <?if(!$haveUSARadarAccess):?> disabled="disabled" <?endif;?>>
                                <option value="all" selected>Не учитывать</option>
                                <?foreach($arFilterEnums["INDUSTRY"] as $n=>$item):?>
                                    <option value="<?= $item ?>"><?= $item ?></option>
                                <?endforeach;?>
                            </select>
                            <?if(!$haveUSARadarAccess):?>
                        		</div>
                    		    <?endif?>
                        <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_USA_INDUSTRY")?>">i</span>
                    </div>
                </div>
            </div>
  <?//if($GLOBALS["USER"]->IsAdmin()):?>
            <div class="row">
                <div class="col col-xs-6">
                    <div class="form_element">
                        <label>Инвест-фонды:</label>
                        <?if(!$haveUSARadarAccess):?>
                        <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                            <?endif?>
                            <select name="fund" class="inline_select noClearPreset" <?if(!$haveUSARadarAccess):?> disabled="disabled" <?endif;?>>
                                <option value="all" selected>Не учитывать</option>
                                <?foreach($arFilterEnums["FUNDS"] as $n=>$item):?>
                                    <option value="BALANCE_<?= $item ?>"><?= $item ?> (на балансе)</option>
                                    <option value="IN_<?= $item ?>"><?= $item ?> (куплено)</option>
                                    <option value="OUT_<?= $item ?>"><?= $item ?> (продано)</option>
                                <?endforeach;?>
                            </select>
                            <?if(!$haveUSARadarAccess):?>
                        </div>
                    <?endif?>
                        <span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_USA_FUND")?>">i</span>
								<div class="radar-subtext"><?echo \Bitrix\Main\Config\Option::get("grain.customsettings", "ACTUAL_FUND_PERIOD");?></div>
                    </div>
                </div>
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>CAGR отрасли: </label>
                            <select name="cagr" class="inline_select">
                                <option value="all">Все</option>
                                <option value="10">Более 10%</option>
                                <option value="20">Более 20%</option>
                                <option value="30">Более 30%</option>
                                <option value="40">Более 40%</option>
                                <option value="50">Более 50%</option>
                            </select>
                            <span class="tooltip_btn p-tooltip" title=""
                                  data-original-title="<?= Loc::getMessage("RAD_ACT_USA_CAGR") ?>">i</span>
                        </div>
                    </div>
            </div>
  <?//endif;//if admin?>
            <div class="row">
                <div class="col col-xs-6">
                    <div class="form_element">
                        <div class="checkbox custom">
                        <?if(!$haveUSARadarAccess):?>
                        <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                        <?endif?>
                            <input class="noClearPreset" type="checkbox" name="use_esg" <?if(!$haveUSARadarAccess):?> disabled="disabled"<?endif?> value="Y"/>
                            <label>Учитывать ESG инвестиции <span class="tooltip_btn p-tooltip"
                                                                                          title=""
                                                                                          data-original-title="<?= Loc::getMessage("RAD_ACT_EGS_INVEST") ?>">i</span></label>
                         <?if(!$haveUSARadarAccess):?>
                     		</div>
                 		    <?endif?>
                        </div>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <div class="form_element">
                        <div class="checkbox custom">
                        <?if(!$haveUSARadarAccess):?>
                        <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                        <?endif?>
                            <input class="noClearPreset" type="checkbox" name="use_halal" <?if(!$haveUSARadarAccess):?> disabled="disabled"<?endif?> value="Y"/>
                            <label>Учитывать халяльные инвестиции <span class="tooltip_btn p-tooltip"
                                                                                          title=""
                                                                                          data-original-title="<?= Loc::getMessage("RAD_ACT_HALAL_INVEST") ?>">i</span></label>
	                      <?if(!$haveUSARadarAccess):?>
	                  		</div>
	              		    <?endif?>
                        </div>
                    </div>
                </div>
            </div>
<?if(checkPayGSUSA(13823, false, $USER->GetID())==true):?>
    <div class="row">
        <div class="col col-xs-6">
            <div class="form_element">
                <div class="checkbox custom">
                    <?if(!$haveUSARadarAccess):?>
                    <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["общая заглушка фильтра"]?>">
                        <?endif?>
                        <input class="noClearPreset" type="checkbox" id="into_gs_usa_portfolio" name="into_gs_usa_portfolio" <?if(!$haveUSARadarAccess):?> disabled="disabled"<?endif?> value="Y"/>
                        <label for="into_gs_usa_portfolio">Есть расширенная аналитика <span class="tooltip_btn p-tooltip"
                                                              title=""
                                                              data-original-title="<?= Loc::getMessage("RAD_ACT_INTO_GS_PORTFOLIO") ?>">i</span></label>
                        <?if(!$haveUSARadarAccess):?>
                    </div>
                <?endif?>
                </div>
            </div>
        </div>

    </div>
<?endif;?>
        </div>

        <div class="calculate_checkboxes_line calculate_line hidden">
            <div class="row">
                <div class="col col-xs-6">
                    <div class="form_element">
                        <div class="checkbox custom">
                            <input type="checkbox" name="turnover_week" checked value="Y"/>
                            <label>Убирать из выдачи акции, по которым нет оборотов <span class="tooltip_btn p-tooltip"
                                                                                          title=""
                                                                                          data-original-title="<?= Loc::getMessage("RAD_ACT_HIDE_LOWLIQUID") ?>">i</span></label>
                        </div>
                    </div>
                </div>
                <? global $USER;
                $rsUser = CUser::GetByID($USER->GetID());
                $arUser = $rsUser->Fetch();
                if ($arUser["LOGIN"] == "freud") {
                    if ($GLOBALS["USER"]->IsAdmin()) {
                        ?>

                        <div class="col col-xs-6">
                            <div class="form_element">
                                <label>Биржа: </label>
                                <select name="exchange_id" class="inline_select">
                                    <option value="moex">Мосбиржа</option>
                                    <option value="spbx">СПб биржа</option>
                                </select>
                                <!--	<span class="tooltip_btn p-tooltip" title="" data-original-title="<?= Loc::getMessage("RAD_ACT_CAPITALIZATION") ?>">i</span> -->
                            </div>
                        </div>
                    
                    
                    <? }
                } ?>
            </div>
        </div>
        <div class="calculate_divider big"></div>
        <div><span class="calculate_more collapsed" data-toggle="collapse"
                   data-target="#calculate_form_actions_usa_more">Расширенный фильтр <span
                        class="icon icon-arr_down"></span></span></div>
    </div>
    <div class="calculate_second">
        <div id="calculate_form_actions_usa_more" class="collapse">
            <div class="calculate_line">
                <div class="row">
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Дивиденды более:</label>
                            <input class="percents_input" name="dividends" type="text" value="0%"/>
                            <span class="tooltip_btn p-tooltip" title=""
                                  data-original-title="<?= Loc::getMessage("RAD_ACT_DIVIDENDS") ?>">i</span>
                        </div>
                    </div>
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Капитализация: </label>
                            <select name="capitalization" class="inline_select">
                                <option value="all">Все</option>
                                <option value="0-300000000">до 300 млн.$</option>
                                <option value="300000000-2000000000">от 300 млн.$ до 2 млрд.$</option>
                                <option value="2000000000-10000000000">от 2 до 10 млрд.$</option>
                                <option value="10000000000-100000000000">от 10 до 100 млрд.$</option>
                                <option value="1000000000-999000000000000">более 1 млрд.$</option>
                                <option value="10000000000-999000000000000">более 10 млрд.$</option>
                                <option value="100000000000-999000000000000">более 100 млрд.$</option>
                            </select>
                            <span class="tooltip_btn p-tooltip" title=""
                                  data-original-title="<?= Loc::getMessage("RAD_ACT_CAPITALIZATION") ?>">i</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col right_col col-xs-6">
                        <div class="checkbox custom">
                            <? if (!$haveUSARadarAccess): ?>
                            <div class="dib relative tooltip_custom_outer"
                                 data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                <? endif ?>
                                <input type="checkbox" name="future_dividends"
                                       value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                <label>Только будущие див-ды</label>
                                <span class="tooltip_btn p-tooltip" title=""
                                      data-original-title="<?= Loc::getMessage("RAD_ACT_USA_FUTURE_DIVIDENDS") ?>">i</span>
                                <? if (!$haveUSARadarAccess): ?>
                            </div>
                        <? endif ?>
                        </div>
                    </div>
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Цена: </label>
                            <select name="price" class="inline_select">
                                <option value="all">Все</option>
                                <option value="0-20">до 20$</option>
                                <option value="0-50">до 50$</option>
                                <option value="0-100">до 100$</option>
                                <option value="0-300">до 300$</option>
                                <option value="0-1000">до 1000$</option>
                                <option value="0-5000">до 5000$</option>
                            </select>
                            <span class="tooltip_btn p-tooltip" title=""
                                  data-original-title="<?= Loc::getMessage("RAD_ACT_USA_PRICE") ?>">i</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-6">

                        <div class="checkbox custom">
                            <? if (!$haveUSARadarAccess): ?>
                            <div class="dib relative tooltip_custom_outer"
                                 data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                <? endif ?>
                                <input type="checkbox" name="no_dividends"
                                       value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                <label>Нет дивидендов</label>
                                <span class="tooltip_btn p-tooltip" title=""
                                      data-original-title="<?= Loc::getMessage("RAD_ACT_NO_DIVIDENDS") ?>">i</span>
                                <? if (!$haveUSARadarAccess): ?>
                            </div>
                        <? endif ?>
                        </div>

                    </div>

						    <div class="col col-xs-6">
                <div class="form_element">
                  <label>Рейтинг радара: </label>
                  <select name="radar-rating" class="inline_select" >
							         <option value="all">Все</option>
                    <option value="0-1">до 1%</option>
                    <option value="0-5">до 5%</option>
                    <option value="0-10">до 10%</option>
                    <option value="0-20">до 20%</option>
                    <option value="0-30">до 30%</option>
										<option value="1-100">более 1%</option>
										<option value="5-100">более 5%</option>
										<option value="10-100">более 10%</option>
										<option value="20-100">более 20%</option>
										<option value="30-100">более 30%</option>
                  </select>
									<span class="tooltip_btn p-tooltip" title="" data-original-title="<?=Loc::getMessage("RAD_ACT_RADAR_RATING")?>">i</span>
                </div>
              </div>



                </div>
            </div>
            <div class="calculate_line">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="calculate_step_title black">Мультипликаторы: <span class="tooltip_btn p-tooltip"
                                                                                     title=""
                                                                                     data-original-title="<?= Loc::getMessage("RAD_ACT_MULTIPLICATORS") ?>">i</span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>P/Equity</label>
                            <select name="p-e" class="inline_select">
                                <option value="all">Все</option>
                                <option value="1-2">до 0,5</option>
                                <option value="1">до 1</option>
                                <option value="3-2">до 1,5</option>
                                <option value="2">до 2</option>
                            </select>
                            <span class="tooltip_btn p-tooltip" title=""
                                  data-original-title="<?= Loc::getMessage("RAD_ACT_P_EQUITY") ?>">i</span>
                        </div>
                    </div>
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>PEG</label>
                            <select name="peg" class="inline_select">
                                <option value="all">Все</option>
                                <option value="1-2">до 0,5</option>
                                <option value="1">до 1</option>
                                <option value="3-2">до 1,5</option>
                                <option value="2">до 2</option>
                            </select>
                            <span class="tooltip_btn p-tooltip" title=""
                                  data-original-title="<?= Loc::getMessage("RAD_ACT_PEG") ?>">i</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Бета: </label>
                            <select name="betta" class="inline_select">
                                <option value="all">Все</option>
                                <option value="-999>-1">до -1</option>
                                <option value="-1>0">от -1 до 0</option>
                                <option value="0>0.5">от 0 до 0.5</option>
                                <option value="0.5>1">от 0.5 до 1</option>
                                <option value="1>999">больше 1</option>
                                <option value="-9991>1">меньше 1</option>
                            </select>
                            <span class="tooltip_btn p-tooltip" title=""
                                  data-original-title="<?= Loc::getMessage("RAD_ACT_BETTA") ?>">i</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="calculate_line">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="calculate_step_title black">Динамика акций: <span class="tooltip_btn p-tooltip"
                                                                                    title=""
                                                                                    data-original-title="<?= Loc::getMessage("RAD_ACT_DYNAMIC") ?>">i</span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Прирост за месяц</label>
                            <select name="month-increase" class="inline_select">
                                <option value="all">Все</option>
                                <option value="down50">Падение более 50%</option>
                                <option value="down25">Падение более 25%</option>
                                <option value="down10">Падение более 10%</option>
                                <option value="down">Падение</option>
                                <option value="up">Рост</option>
                                <option value="up10">Рост более 10%</option>
                                <option value="up25">Рост более 25%</option>
                                <option value="up50">Рост более 50%</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Прирост за год</label>
                            <select name="year-increase" class="inline_select">
                                <option value="all">Все</option>
                                <option value="down50">Падение более 50%</option>
                                <option value="down25">Падение более 25%</option>
                                <option value="down10">Падение более 10%</option>
                                <option value="down">Падение</option>
                                <option value="up">Рост</option>
                                <option value="up10">Рост более 10%</option>
                                <option value="up25">Рост более 25%</option>
                                <option value="up50">Рост более 50%</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Прирост за 3 года</label>
                            <select name="three-year-increase" class="inline_select">
                                <option value="all">Все</option>
                                <option value="down50">Падение более 50%</option>
                                <option value="down25">Падение более 25%</option>
                                <option value="down10">Падение более 10%</option>
                                <option value="down">Падение</option>
                                <option value="up">Рост</option>
                                <option value="up10">Рост более 10%</option>
                                <option value="up25">Рост более 25%</option>
                                <option value="up50">Рост более 50%</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="calculate_line">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="calculate_step_title black">Сектор:</p>
                    </div>
                </div>
                <div class="row usual_checkboxes">
                    
                    <? foreach ($arFilterEnums["SECTOR"] as $n => $item): ?>
                        <div class="col col-xs-6 col-lg-4">
                            <div class="checkbox">
                                <input type="checkbox" id="sector_<?= $n ?>" name="sector[]" value="<?= $item ?>"/>
                                <label for="sector_<?= $n ?>"><span><?= $item ?></span></label>
                            </div>
                        </div>
                    <? endforeach ?>
                    <!--						<div class="col col-xs-12 text-center">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="shares_usa_all_sectors" />
                                                    <label for="shares_all_sectors"><span class="strong">Все компании, кроме банков</span></label>
                                                </div>
                                            </div>-->
                </div>
            </div>
            <div class="calculate_line tab_btn_outer">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="form_element">
                            <label>Аналитика:</label>
                            <select id="shares_analysis_select" name="analitics" class="tab_btn inline_select">
                                <option value="shares_company_tab">Предприятие</option>
                                <!--        <option value="shares_bank_tab">Банк</option>  -->
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="calculate_line three_elements">
                <div class="tab_container active" id="shares_company_tab">
                    <div class="calculate_line_period">
                        <div class="text_element">
                            <label>за период:</label>
                            <select name="period_value" class="inline_select">
                                <option value="last">Последний период</option>
                                <? foreach ($arKvartalPeriod as $val => $name): ?>
                                    <option value="<?= $val ?>"><?= $name ?></option>
                                <? endforeach ?>
                            </select>
                            <span class="tooltip_btn p-tooltip" title=""
                                  data-original-title="<?= Loc::getMessage("RAD_ACT_PERIOD") ?>">i</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Темп прироста выручки более: <span class="tooltip_btn p-tooltip" title=""
                                                                          data-original-title="<?= Loc::getMessage("RAD_ACT_GROWTH_TEMP") ?>">i</span></label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input class="percents_input" name="period[Темп прироста выручки][percent]"
                                           type="text"
                                           value="10%"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="period[Темп прироста выручки][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Рентабельность собственного капитала, более: <span class="tooltip_btn p-tooltip"
                                                                                          title=""
                                                                                          data-original-title="<?= Loc::getMessage("RAD_ACT_RENT_SK") ?>">i</span></label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input class="percents_input"
                                           name="period[Рентабельность собственного капитала][percent]" type="text"
                                           value="10%"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="period[Рентабельность собственного капитала][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Темп роста активов: <span class="tooltip_btn p-tooltip" title=""
                                                                 data-original-title="<?= Loc::getMessage("RAD_ACT_ACTIVES_GROWTH_TEMP") ?>">i</span></label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input class="percents_input" name="period[Темп роста активов][percent]" type="text"
                                           value="10%"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="period[Темп роста активов][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Доля собственного капитала в активах: <span class="tooltip_btn p-tooltip"
                                                                                   title=""
                                                                                   data-original-title="<?= Loc::getMessage("RAD_ACT_SHARE_OF_EQUITY") ?>">i</span></label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input class="percents_input"
                                           name="period[Доля собственного капитала в активах][percent]" type="text"
                                           value="50%"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="period[Доля собственного капитала в активах][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Темп роста прибыли: <span class="tooltip_btn p-tooltip" title=""
                                                                 data-original-title="<?= Loc::getMessage("RAD_ACT_PROFIT_GROWTH_RATE") ?>">i</span></label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input class="percents_input" name="period[Темп роста прибыли][percent]" type="text"
                                           value="10%"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["финансовый анализ - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="period[Темп роста прибыли][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab_container" id="shares_bank_tab">
                    <div class="calculate_line_period">
                        <div class="text_element">
                            <label>за период:</label>

                            <select name="month_value" class="inline_select">
                                
                                <? foreach ($arMonthPeriod as $val => $name): ?>
                                    <option value="<?= $val ?>"><?= $name ?></option>
                                <? endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Рейтинг по активам:</label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                    <? endif ?>
                                    <select class="inline_select"
                                            name="month[Рейтинг по активам, номер][top]"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?>>
                                        <option value="20">топ 20</option>
                                        <option value="10">топ 10</option>
                                        <option value="5">топ 5</option>
                                    </select>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="month[Рейтинг по активам, номер][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Рентабельность собственного капитала, более:</label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                    <? endif ?>
                                    <input class="percents_input"
                                           name="month[Рентабельность собственного капитала, %][percent]" type="text"
                                           value="10%"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="month[Рентабельность собственного капитала, %][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Доля просроченной задолженности в кредитах, менее:</label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                    <? endif ?>
                                    <input class="percents_input" name="month[Доля просрочки, %][percent]" type="text"
                                           value="10%"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="month[Доля просрочки, %][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col left_col col-xs-5 col-sm-4 col-md-5">
                            <div class="form_element">
                                <label>Норматив достаточности капитала Н1, более</label>
                            </div>
                        </div>
                        <div class="col mid_col col-xs-2 col-sm-3 col-md-2">
                            <div class="form_element">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                    <? endif ?>
                                    <input class="percents_input" name="month[Н1,%][percent]" type="text"
                                           value="10%"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                        <div class="col right_col col-xs-5">
                            <div class="checkbox custom">
                                <? if (!$haveUSARadarAccess): ?>
                                <div class="dib relative tooltip_custom_outer"
                                     data-content="<?= $APPLICATION->lkRadarPopups["аналитика - акции"] ?>">
                                    <? endif ?>
                                    <input type="checkbox" name="month[Н1,%][use]"
                                           value="Y"<? if (!$haveUSARadarAccess): ?> disabled="disabled"<? endif ?> />
                                    <label>Учитывать</label>
                                    <? if (!$haveUSARadarAccess): ?>
                                </div>
                            <? endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>