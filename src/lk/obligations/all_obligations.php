<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "top_banner",
						"EDIT_TEMPLATE" => "",
						"COMPONENT_TEMPLATE" => ".default",
						"AREA_FILE_RECURSIVE" => "Y"
					),
					false
				);?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
                <?
                //Формирование фильтра облигаций с перезагрузкой страниц и отдельными адресами
                $obligBaseUrl = '/lk/obligations/all/';
                $filter_code = htmlspecialchars($_REQUEST["FILTER_CODE"]);
                $title = "Список облигаций";
                if(!empty($filter_code))
                switch ($filter_code){
                    case "corp":
                        $title = str_replace("Список", "Список корпоративных", $title);
                        break;
                    case "fed":
                        $title = str_replace("Список", "Список федеральных", $title);
                        break;
                    case "mun":
                        $title = str_replace("Список", "Список муниципальных", $title);
                        break;
                    case "bank":
                        $title = str_replace("Список", "Список банковских", $title);
                        break;
                    case "def":
                        $title = str_replace("Список", "Список дефолтных", $title);
                        break;
                    case "euro":
                        $title = str_replace("Список облигаций", "Список еврооблигаций", $title);
                        break;
                    case "struct":
                        $title = str_replace("Список", "Список структурных", $title);
                        break;
                    case "sub":
                        $title = str_replace("Список", "Список субординированных", $title);
                        break;
                }
                $APPLICATION->SetTitle($title);
                ?>
				<div class="title_container">
					<h1><?=$APPLICATION->ShowTitle(false)?></h1>

					<form class="innerpage_search_form" method="GET">
						<div class="form_element">
							<input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете? Введите название или ISIN" name="q" id="autocomplete_obl_all" />
							<button type="submit"><span class="icon icon-search"></span></button>
						</div>
					</form>
				</div>

      <ul class="actives_list_filter sections_list">
          <li class="<?=(empty($filter_code)?'active':'')?>"><a href="<?=$obligBaseUrl?>">Все</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='corp'?'active':'')?>"><a href="<?=$obligBaseUrl?>corp/">Корпоративные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='fed'?'active':'')?>"><a href="<?=$obligBaseUrl?>fed/">Федеральные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='mun'?'active':'')?>"><a href="<?=$obligBaseUrl?>mun/">Муниципальные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='bank'?'active':'')?>"><a href="<?=$obligBaseUrl?>bank/">Банковские</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='def'?'active':'')?>"><a href="<?=$obligBaseUrl?>def/">Дефолтные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='euro'?'active':'')?>"><a href="<?=$obligBaseUrl?>euro/">Еврооблигации</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='struct'?'active':'')?>"><a href="<?=$obligBaseUrl?>struct/">Структурные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='sub'?'active':'')?>"><a href="<?=$obligBaseUrl?>sub/">Субординированные</a></li>
      </ul>
        <?
      $arrListFilter = array();
      $arFilteredItems = array();
              switch ($filter_code){
                  case "corp":
                      $arrListFilter = array("type", "корпоративные");
                  break;
                  case "fed":
                      $arrListFilter = array("type", "федеральные");
                      break;
                  case "mun":
                      $arrListFilter = array("type", "муниципальные");
                      break;
                  case "bank":
                      $arrListFilter = array("type", "банковские");
                      break;
                  case "def":
                      $arrListFilter = array("type", "дефолтные");
                      break;
                  case "euro":
                      $arrListFilter = array("valute", "RUB");
                      break;
                  case "struct":
                      $arrListFilter = array("structural", "y");
                      break;
                  case "sub":
                      $arrListFilter = array("payment_order", "y");
                      break;
                  default:
                      $arrListFilter = array("all", "y");
                      break;
              }
              $res = new Obligations();
              $arFilteredItems = $res->setFilterForList($arrListFilter);
              unset($res);
          


     
        ?>

                <?
				global $arrFilter;
                $arrFilter = array();
				if($_REQUEST["q"]){
					$arrFilter[] = array(
						"LOGIC" => "OR",
						array("NAME" => "%".$_REQUEST["q"]."%"),
						array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
					);
				} else {
				    if(count($arFilteredItems))
                      $arrFilter["CODE"]=array_keys($arFilteredItems);
				}
				
				?>

                    <p>В скобках указаны доходность и дата погашения облигаций</p>

				<?$APPLICATION->IncludeComponent("bitrix:news.list", "obligation_dist_list", array(
					"IBLOCK_TYPE" => "FINPLAN",
					"IBLOCK_ID" => "30",
					"NEWS_COUNT" => "9999",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "NAME",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
                    "ACTIVES_FILTER" => $arrListFilter,
					),
					false
				);?>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
