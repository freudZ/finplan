<?
$page = 1;
if($_REQUEST["page"]){
	$page = $_REQUEST["page"];
}

$res = new Obligations();
$arData = $res->getTable($page);

global $APPLICATION;
$obl_number = 15;
$obl_number_from_cookie = $_COOKIE["obligations_number_cookie"];
if ($obl_number_from_cookie != 0 && $obl_number_from_cookie != '') {
	$obl_number = $obl_number_from_cookie;
}
?>
<div>
	<div class="calculate_table_head">
		<div class="row">
			<div class="col col_left col-sm-7">
				<p class="calculate_step_title">3. Выберите облигацию (показано <?=$arData["shows"]?> из <?=$arData["total_items"]?>):</p>
			</div>
			<div class="col col_right col-sm-5"><span class="calculate_table_clear_btn">Сбросить портфель</span></div>
		</div>
	</div>

	<table class="smart_table">
	  <thead>
		<tr>
		  <th class="name_col">Облигация:</th>

		  <th class="price_col">Цена <br/>покупки:</th>

		  <th class="time_col">Срок гашения <br>(гашение по&nbsp;оферте):</th>

		  <th class="price_col">Цена <br/>погашения <br/>облигации <br/>с учетом <br/>купонов</th>

		  <th class="profitability_col">Доходности: <br/>годовая /<br/>общая / <br/>по оферте</th>

		  <th class="inputs_col">Кол-во <br/>лотов для <br>инвестиций</th>

		  <th class="check_col">Выбрать <br>облигацию
			<div class="checkbox">
			  <input id="calculate_table_all_debstock" class="calculate_table_all" type="checkbox"/>
			  <label for="calculate_table_all_debstock"></label>
			</div>
		  </th>
		</tr>
	  </thead>

	  <tbody>
		<?foreach($arData["items"] as $item):
			$item["PROPS"]["CURRENCYID"] = str_replace("SUR", "RUB", $item["PROPS"]["CURRENCYID"])?>
            <!--<? $item["PROPS"]["LISTLEVEL"] ?>-->
			<tr data-id="<?=$item["PROPS"]["SECID"]?>" data-currency="<?=strtolower($item["PROPS"]["CURRENCYID"])?>">
			  <td class="obligation">
				<p class="elem_name"><a href="<?=$item["URL"]?>" class="elem_name__link tooltip_btn" title="<?=$item["NAME"]?> (<?=$item["PROPS"]["ISIN"]?>)" portfolio-title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a></p>
				<?if($item["COMPANY"]):?>
					<p class="company_name line_green"><a class="tooltip_btn" title="<?=$item["COMPANY"]["NAME"]?>" href="<?=$item["COMPANY"]["URL"]?>"><?=$item["COMPANY"]["NAME"]?></a></p>
				<?endif?>
			  </td>
			  <td>
				<!-- цена выводится только в атрибут data-price -->
				<p class="elem_buy_price price_elem" data-price="<?=$item["DYNAM"]["Цена покупки"]?>"></p>
			  </td>
			  <td>
				<p class="date_off" data-date="<?=$item["PROPS"]["MATDATE"]?>"></p>
				<?if($item["PROPS"]["OFFERDATE"]):
					$t = new DateTime($item["PROPS"]["OFFERDATE"])?>
					<p class="line_green date_offerdate" data-offerdate="<?= $t->format("Y-m-d") ?>">(<?=$t->format("d.m.Y")?>)</p>
				<?endif?>
			  </td>
			  <td>
				<!-- цена выводится только в атрибут data-price -->
				<?$price = !empty($item["DYNAM"]["Цена погашения с учетом купонов в дату оферты"])?$item["DYNAM"]["Цена погашения с учетом купонов в дату оферты"]:$item["DYNAM"]["Цена погашения с учетом купонов"];?>
				<p class="elem_sell_price price_elem" data-price="<?=$item["DYNAM"]["Цена погашения с учетом купонов"]?>"></p>
				<?if(array_key_exists("Цена погашения с учетом купонов в дату оферты",$item["DYNAM"]) && floatval($item["DYNAM"]["Цена погашения с учетом купонов в дату оферты"])>0):?>
				<p class="elem_sell_price_offer line_green price_elem" data-price="<?=$item["DYNAM"]["Цена погашения с учетом купонов в дату оферты"]?>"></p>
				<?endif;?>
			  </td>
			  <td>
				<p class="profit_year" data-profit_year="<?=$item["DYNAM"]["Доходность годовая"]?>"><?=$item["DYNAM"]["Доходность годовая"]?>%</p>
				<p class="profit_main" data-profit_main="<?=$item["DYNAM"]["Доходность общая"]?>"><?=$item["DYNAM"]["Доходность общая"]?>%</p>
				<?if($item["DYNAM"]["Доходность к офферте"]):?>
					<p class="line_green profit_offer" data-profit_offer="<?=$item["DYNAM"]["Доходность к офферте"]?>"><?=round($item["DYNAM"]["Доходность к офферте"], 1)?>%</p>
				<?endif?>
			  </td>
			  <td>
				<input class="numberof" type="text" value="<?=$obl_number?>"/>
			  </td>
			  <td>
				<div class="checkbox">
				  <input type="checkbox" id="<?=$item["PROPS"]["SECID"]?>"/>
				  <label for="<?=$item["PROPS"]["SECID"]?>"></label>
				</div>
			  </td>
			</tr>
		<?endforeach?>

	  </tbody>
	</table>
	<div class="more-results-wrap">


 	<? $portfolio = new CPortfolio(); ?>
	<?$checkPortfolio = $portfolio->checkPortfolioCntForUser($USER->GetID(), false);
	if($checkPortfolio["PORTFOLIO"]["LIMIT"]>$checkPortfolio["PORTFOLIO"]["OWNER"] || $checkPortfolio["ADMIN"]=="Y"){
	$createNew = true;
	} else {
	$createNew = false;
	}
	unset($portfolio);
	?>
		  <div class="text-right">
		  	<br>
	  	  <span class="<?if(!$haveRadarAccess):?>dib relative tooltip_custom_outer<?endif;?> calculate_table_add_portfolio_btn" <?if(!$haveRadarAccess):?>
	  data-content="<?=$APPLICATION->lkRadarPopups["заглушка портфеля"]?>"
	  <?else:?>
	   data-toggle="modal" data-target="#popup_add_portfolio"<?endif;?>><?= ($createNew?'Создать&nbsp;портфель':'Добавить&nbsp;в&nbsp;портфель') ?></span>
		  </div>

		<?if($arData["cur_page"]<$arData["count_page"] && $arData["count_page"]):?>
			<div class="more_results text-center">
			  <button class="calc_table_more button" data-next="<?=$arData["cur_page"]+1?>">Загрузить ещё</button>
			</div>
		<?endif?>
	</div>
</div>
