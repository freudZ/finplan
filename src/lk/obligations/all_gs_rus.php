<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список акций с аналитикой по ГС РФ");
if(!checkPaySeminar(13823, false, $USER->GetID()) || !$USER->IsAuthorized()){
     LocalRedirect("http://Finplan.expert/club");
	}
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "top_banner",
						"EDIT_TEMPLATE" => "",
						"COMPONENT_TEMPLATE" => ".default",
						"AREA_FILE_RECURSIVE" => "Y"
					),
					false
				);?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
				<div class="title_container title_nav_outer">
					<h1><?=$APPLICATION->ShowTitle(false)?></h1>
                        <?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
                            <div class="title_nav">
                                <ul>
                                    <li><a href="/lk/gs/all/">Вся аналитика РФ</a></li>
                                    <li><a href="/lk/gs/all_usa/">Вся аналитика США</a></li>
                                    <li><a href="/learning/13823/">Личный кабинет "Клуб инвесторов"</a></li>
                                </ul>
                            </div>
                        <?endif?>
					<form class="innerpage_search_form hidden" method="GET">
						<div class="form_element">
							<input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете? Введите название или тикер" name="q" id="autocomplete_act_all" />
							<button type="submit"><span class="icon icon-search"></span></button>
						</div>
					</form>
				</div>

				<? $CGs = new CGs();

					 $arFilteredItems = $CGs->getAllGsIsins(array(), 'RUS');
				    if(count($arFilteredItems)){
                      $arrFilter["CODE"]=array_values($arFilteredItems);
					 }


					 $filter_code = '';
					 $PageBaseUrl = '/lk/gs/all/';
					 $filter_code = htmlspecialchars($_REQUEST["FILTER_CODE"]);

					 if(!empty($filter_code)){
					 	$arActivesInPortfolio = $CGs->getActivesInPortfolio($APPLICATION->modelPortfolioRusId);
						if($filter_code=="in_portfolio"){
							$arTmp = array_intersect($arrFilter["CODE"], $arActivesInPortfolio );
						  	$arrFilter["CODE"] = array_values($arTmp);
						}
						if($filter_code=="not_in_portfolio"){
						 	$arTmp = array_diff($arrFilter["CODE"], $arActivesInPortfolio);
						 	$arrFilter["CODE"] = array_values($arTmp);
							if(count($arrFilter["CODE"])==0){
								$arrFilter["CODE"] = array("0");
							}
						}
					 }


				//$arrFilter["!PROPERTY_BOARD_ID"] = $APPLICATION->ExcludeFromRadar["акции"];

				?>

         <ul class="actives_list_filter sections_list">
	          <li class="<?=(empty($filter_code)?'active':'')?>"><a href="<?=$PageBaseUrl?>">Все</a></li>
	          <li class="<?=(!empty($filter_code) && $filter_code=='in_portfolio'?'active':'')?>"><a href="<?=$PageBaseUrl?>in_portfolio/">Есть в учебном портфеле</a></li>
	          <li class="<?=(!empty($filter_code) && $filter_code=='not_in_portfolio'?'active':'')?>"><a href="<?=$PageBaseUrl?>not_in_portfolio/">Нет в учебном портфеле</a></li>
	      </ul>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "actions_gs_list", array(
					"IBLOCK_TYPE" => "FINPLAN",
					"IBLOCK_ID" => "33",
					"NEWS_COUNT" => "999999",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "NAME",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"ACTIVES_FILTER" => $arrListFilter,
					),
					false
				);?>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>
<?include("popups.php")?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
