<? define("COMPANY_DETAIL_PAGE", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список секторов США");
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "top_banner",
						"EDIT_TEMPLATE" => "",
						"COMPONENT_TEMPLATE" => ".default",
						"AREA_FILE_RECURSIVE" => "Y"
					),
					false
				);?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>

				<div class="title_container">
					<h1><?=$APPLICATION->ShowTitle(false)?></h1>

					<!--<form class="innerpage_search_form" method="GET">
						<div class="form_element">
							<input type="text" value="<?/*=$_REQUEST["q"]*/?>" placeholder="Что вы ищете? Введите название или тикер" name="q" id="autocomplete_act_all" />
							<button type="submit"><span class="icon icon-search"></span></button>
						</div>
					</form>-->
				</div>
				<? $APPLICATION->IncludeFile("/lk/obligations/include/all_sectors_usa_top.php",Array(),Array("MODE"=>"html","NAME"=>"верхний блок текста")) ?>

            <?//if($USER->IsAdmin()):?>
				  <? $CSectors = new SectorsUsa();
					  //$arIndPeriods = $CSectors->arAllIndustriesSumm;
					  $arIndCurPeriods = $CSectors->arSectorsCurrentPeriods;
					  $arDataCapGhistogramKvartals = array();
					  $arDataCapGhistogramYear = array();

					  foreach($arIndCurPeriods as $k=>$v){
						$arDataCapGhistogramKvartals[] = array($k,$v["Темп прироста капитализации с прошлого квартала"]);
						$arDataCapGhistogramYear[] = array($k,$v["Темп прироста капитализации с начала года"]);
						if($v["P/E"]<200 && $v["P/E"]>-200){ //Добавляем только сектора PE которых входит в диапазон от -200 до 200
							$arDataPEGhistogram[] = array($k,$v["P/E"]);
						}
					  }
					  unset($CSectors);
				   ?>
				  <? //1)	Гистограма с приростами капы ?>
              												<ul class="custom_tabs company_chart_tab industries_capa_chart_tab_01">
                                                        <li class="active">
                                                            <a href="#industries_capa_custom_tabs_01_quarter"
                                                               data-toggle="tab">
                                                                    по кварталам
                                                            </a>
                                                        </li>
                                                        <li><a href="#industries_capa_custom_tabs_01_year"
                                                               data-toggle="tab">по годам</a></li>
                                                    </ul>
																<div class="tab-content">
                                                    <div class="tab-pane active" data-currency-sign="%"
																	 		data-legend-name = 'Прирост капитализации относительно прошлого квартала'
                                                         id="industries_capa_custom_tabs_01_quarter">
                                                            <div class="company_chart_outer">
                                                                <canvas class="company_chart_capa"
                                                                        id="key_capa_quarters_chart"
                                                                        data-value='<?= json_encode($arDataCapGhistogramKvartals) ?>'></canvas>
                                                            </div>

																	 </div>
																	 <div class="tab-pane" data-currency-sign="%"
																	 		data-legend-name = 'Прирост капитализации относительно начала года'
                                                         id="industries_capa_custom_tabs_01_year">
                                                            <div class="company_chart_outer">
                                                                <canvas class="company_chart_capa"
                                                                        id="key_capa_years_chart"
                                                                        data-value='<?= json_encode($arDataCapGhistogramYear) ?>'></canvas>
                                                            </div>
																	 </div>

																</div>
						<!-- Гистограмма среднего PE по отраслям за тек. период -->
						<div class="tab-pane active" data-currency-sign=""
						data-legend-name = 'Средний P/E по отраслям'
						id="industries_capa_custom_tabs_01_quarter">
						<div class="company_chart_outer">
						    <canvas class="company_chart_capa"
						            id="key_capa_quarters_chart"
						            data-value='<?= json_encode($arDataPEGhistogram) ?>'></canvas>
						</div>

						</div>
						<script>
						$(document).ready(function(){
							chartIndustriesCapaRedraw();
                  });
                  </script>
            <?//endif;//admin?>

				<?
				global $arrFilter;
				if($_REQUEST["q"]){
					$arrFilter[] = array(
						"LOGIC" => "OR",
						array("NAME" => "%".$_REQUEST["q"]."%"),
						array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
					);
				}
				//$arrFilter["!PROPERTY_BOARD_ID"] = $APPLICATION->ExcludeFromRadar["акции"];
				?>
						  <?/* global $USER;
      if($GLOBALS["USER"]->IsAdmin()){*/
        $tmpl = "sectors_usa_list_table";
     /* } else {
        $tmpl = "sectors_usa_list";
      } */?>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", $tmpl, array(
					"IBLOCK_TYPE" => "action",
					"IBLOCK_ID" => "58",
					"NEWS_COUNT" => "999999",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "NAME",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => ""
					),
					false
				);?>
               <? $APPLICATION->IncludeFile("/lk/obligations/include/all_sectors_usa_bottom.php",Array(),Array("MODE"=>"html","NAME"=>"нижний блок текста")) ?>
                <?//if($USER->IsAdmin()):?>
                    <?$APPLICATION->IncludeComponent(
                      "finplan:PeriodsGraph",
                      "",
                      Array(
                        "CACHE_TIME" => "86400",
                        "CACHE_TYPE" => "A",
                        "COUNTRY" => "USA",
                        "LINE_CAPTION" => "Капитализация",
                        "LINE_CAPTION_СURRENCY" => "$",
                        "PERIODS_CNT" => "6",
                        "DEFAULT_GRAPH_MODE" => "KVARTAL",
                        "SHOW_CURRENT_PERIOD" => "Y",
                        "SHOW_CURRENT_PERIOD_ON_GRAPH" => "N",
                        "TITLE_BLOCK" => "Суммарные показатели по всем секторам США",
                        "TABLE_CURRENCY" => "$"
                      )
                    );?>
                <?//endif;?>
                
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>