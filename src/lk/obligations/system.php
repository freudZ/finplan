<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Highloadblock as HL;

global $USER;
$arGroups = $USER->GetUserGroupArray();

if(!$USER->IsAdmin() && !in_array(5, $arGroups)){
	LocalRedirect("/lk/obligation/");
	exit();
}

//Перенаправление в tools
LocalRedirect("/tools/system/rus/");

//Дальнейшее содержимое не используется, т.к. перенаправление в tools на аналогичный скрипт
$arTypes = array(
	"obl_no_emitent" => "Облигации без эмитентов",
	"action_no_emitent" => "Акции без эмитентов",
	"obl_coupons" => "Облигации купоны",
	"act_prosad_low" => "Акции с просадом менее 10%",
	"act_target_low" => "Акции с таргетом менее 10%",
	"obl_empty_initvalue" => "Облигации без нач.номинала"
);
if(!$_SESSION["radar_system"]["type"]){
	$_SESSION["radar_system"]["type"] = "obl_no_emitent";
}
if($_REQUEST["type"]){
	$_SESSION["radar_system"]["type"] = $_REQUEST["type"];
	LocalRedirect($_SERVER["SCRIPT_NAME"]);
}

function getLowSort($a, $b){
	if ($a[2] == $b[2]) {
		return 0;
	}
	return ($a[2] > $b[2]) ? -1 : 1;
}

CModule::IncludeModule("iblock");
if($_SESSION["radar_system"]["type"]=="obl_no_emitent"){
	$arData["colums"] = array(
		"ISIN",
		"Название",
	);
	
	$arFilter = Array("IBLOCK_ID"=>27, "PROPERTY_HIDEN"=>false, "PROPERTY_EMITENT_ID"=>false);
	$res = CIBlockElement::GetList(Array("NAME"=>"asc"), $arFilter, false, false, array("NAME", "PROPERTY_ISIN"));
	while($ob = $res->GetNext())
	{
		$arData["data"][] = array(
			$ob["PROPERTY_ISIN_VALUE"],
			$ob["NAME"],
		);
	}
}elseif($_SESSION["radar_system"]["type"]=="obl_empty_initvalue"){ // Обл. без номинала
	$arData["colums"] = array(
		"ISIN",
		"Название",
		//"Первоначальный номинал",
	);

	$arFilter = Array("IBLOCK_ID"=>27, "PROPERTY_INITIALFACEVALUE"=>false);
	//$arFilter = Array("IBLOCK_ID"=>27, array("LOGIC"=>"OR", array(">PROPERTY_INITIALFACEVALUE"=>1000), array("PROPERTY_INITIALFACEVALUE"=>false)));
	$res = CIBlockElement::GetList(Array("PROPERTY_INITIALFACEVALUE"=>"asc"), $arFilter, false, false, array("NAME", "PROPERTY_ISIN", "PROPERTY_INITIALFACEVALUE"));
	while($ob = $res->GetNext())
	{
		$arData["data"][] = array(
			$ob["PROPERTY_ISIN_VALUE"],
			$ob["NAME"],
			//$ob["PROPERTY_INITIALFACEVALUE_VALUE"],
		);
	}


}elseif($_SESSION["radar_system"]["type"]=="action_no_emitent"){
	$arData["colums"] = array(
		"SECID",
		"Название",
	);
	
	$arFilter = Array("IBLOCK_ID"=>32, "PROPERTY_EMITENT_ID"=>false, "!PROPERTY_BOARDID" => $APPLICATION->ExcludeFromRadar["акции"]);
	$res = CIBlockElement::GetList(Array("NAME"=>"asc"), $arFilter, false, false, array("NAME", "PROPERTY_SECID"));
	while($ob = $res->GetNext())
	{
		$arData["data"][] = array(
			$ob["PROPERTY_SECID_VALUE"],
			$ob["NAME"],
		);
	}
}elseif($_SESSION["radar_system"]["type"]=="obl_coupons"){
	CModule::IncludeModule("highloadblock");
	//отчеты
	$hlblock   = HL\HighloadBlockTable::getById(15)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	$res = $entityClass::getList(array(
		"filter" => array(
		),
		"select" => array(
			"*"
		),
	));
	while($item = $res->fetch()){
		$arCoupons[$item["UF_ITEM"]] = json_decode($item["UF_DATA"], true);
	}
		
	$arData["colums"] = array(
		"ISIN",
		"Название",
		"Дата",
	);
	
	$arFilter = Array("IBLOCK_ID"=>27, "PROPERTY_HIDEN"=>false);
	$res = CIBlockElement::GetList(Array("NAME"=>"asc"), $arFilter, false, false, array("ID", "NAME", "CODE", "PROPERTY_ISIN", "PROPERTY_MATDATE"));
	while($ob = $res->GetNext())
	{
		if($ob["PROPERTY_MATDATE_VALUE"]){
			$dt = new DateTime($ob["PROPERTY_MATDATE_VALUE"]);
			if($dt->getTimestamp()<time()){
				continue;
			}
		}
		
		$tmp = array(
			1 => $ob["PROPERTY_ISIN_VALUE"],
			2 => $ob["NAME"],
		);
		if(count($arCoupons[$ob["CODE"]])){
			$lastWithData = "";
			foreach($arCoupons[$ob["CODE"]] as $item){
				if($item["Дата купона"] && is_numeric(str_replace(",", ".", $item["Ставка купона"]))){
					$lastWithData = $item["Дата купона"];
				} else {
					break;
				}
			}
			if($lastWithData){
				$tmp[0] = $lastWithData;
			} else {
				$tmp[0] = "Нет купонов (дата)";
			}
		} else {
			$tmp[0] = "Ничего нет";
		}
		
		$arData["data"][] = $tmp;
	}

	usort($arData["data"], function($a, $b){
		if (($t = DateTime::createFromFormat('d.m.Y', $a[0])) !== FALSE) {
			$a[0] = $t->getTimestamp();
		} else {
			$a[0] = PHP_INT_MAX;
		}
		if (($t = DateTime::createFromFormat('d.m.Y', $b[0])) !== FALSE) {
			$b[0] = $t->getTimestamp();
		} else {
			$b[0] = PHP_INT_MAX;
		}
		
		if ($a[0] == $b[0]) {
			return 0;
		}
		return ($a[0] < $b[0]) ? -1 : 1;
	});
}elseif($_SESSION["radar_system"]["type"]=="act_prosad_low"){
	$res = new Actions();
	$arData["data"] = $res->getLowProsad();

	
	usort($arData["data"], "getLowSort");
	
	$arData["colums"] = array(
		"SECID",
		"Название",
		"Просад",
	);
	
}elseif($_SESSION["radar_system"]["type"]=="act_target_low"){
	$res = new Actions();
	$arData["data"] = $res->getLowTarget();
	
	usort($arData["data"], "getLowSort");
	
	$arData["colums"] = array(
		"SECID",
		"Название",
		"Таргет",
	);
}
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<div class="btn-group" role="group" aria-label="">
					<?foreach($arTypes as $type=>$name):?>
						<a href="<?=$_SERVER["SCRIPT_NAME"]?>?type=<?=$type?>" class="btn btn-<?if($_SESSION["radar_system"]["type"]==$type):?>success<?else:?>secondary<?endif?>"><?=$name?></a>
					<?endforeach?>
				</div>
				<br><br>
				<table class="table table-bordered">
					<thead>
						<tr>
							<?foreach($arData["colums"] as $item):?>
								<th><?=$item?></th>
							<?endforeach?>
						</tr>
					</thead>
					<tbody>
						<?foreach($arData["data"] as $item):?>
							<tr>
								<?foreach($item as $val):?>
									<td><?=$val?></td>
								<?endforeach?>
							</tr>
						<?endforeach?>
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>