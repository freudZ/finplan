<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список ETF мосбиржи");
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "top_banner",
						"EDIT_TEMPLATE" => "",
						"COMPONENT_TEMPLATE" => ".default",
						"AREA_FILE_RECURSIVE" => "Y"
					),
					false
				);?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
				<?  //Формирование фильтра ETF с перезагрузкой страниц и отдельными адресами
                $actionsBaseUrl = '/lk/etf/all/';
                $filter_code = htmlspecialchars($_REQUEST["FILTER_CODE"]);
                $title = "Список ETF мосбиржи";
					 $arFilteredItems = array(); //Отфильтрованные isin
					 $arPagesList = array(
					   "alletf" => array("menuName"=>"Все", "pageTitle"=>"Список ETF мосбиржи", "filter"=>array("IS_ETF_ACTIVE", "Y")),
						"rus" => array("menuName"=>"Акции РФ", "pageTitle"=>"ETF на российские акции", "filter"=>array("ETF_COUNTRY ETF_TYPE_OF_ACTIVES", "Россия", "Акции")),
						"usa" => array("menuName"=>"Акции США", "pageTitle"=>"ETF акций США", "filter"=>array("ETF_COUNTRY ETF_TYPE_OF_ACTIVES", "США", "Акции")),
						"chn" => array("menuName"=>"Акции Китая", "pageTitle"=>"ETF Китай", "filter"=>array("ETF_COUNTRY ETF_TYPE_OF_ACTIVES", "Китай", "Акции")),
						"deb" => array("menuName"=>"Облигации", "pageTitle"=>"ETF на облигации", "filter"=>array("ETF_TYPE_OF_ACTIVES", "Облигации")),
						"mny" => array("menuName"=>"Денежный рынок", "pageTitle"=>"ETF денежного рынка", "filter"=>array("ETF_TYPE_OF_ACTIVES", "Денежный рынок")),
						"rub" => array("menuName"=>"В рублях", "pageTitle"=>"ETF в рублях", "filter"=>array("ETF_CURRENCY", "RUB")),
						"usd" => array("menuName"=>"В долларах", "pageTitle"=>"ETF в долларах", "filter"=>array("ETF_CURRENCY", "USD")),
						"sp500" => array("menuName"=>"SP500", "pageTitle"=>"ETF на SP500", "filter"=>array("ETF_CALC_BASE", "Standard & Poor`s 500 Index (S&P 500)")),
						"ndq" => array("menuName"=>"NASDAQ", "pageTitle"=>"ETF на NASDAQ", "filter"=>array("ETF_CALC_BASE", "NASDAQ")),
						"gld" => array("menuName"=>"Золото", "pageTitle"=>"ETF на золото", "filter"=>array("ETF_TYPE_OF_ACTIVES", "Товар")),
						"fnx" => array("menuName"=>"Фонды Finex", "pageTitle"=>"ETF фонды Finex", "filter"=>array("EMITENT_ID", array(264267, 263877))),
						"tcf" => array("menuName"=>"Фонды Тинькофф", "pageTitle"=>"ETF фонды Тинькофф", "filter"=>array("EMITENT_ID", array(264277))),
						"sbr" => array("menuName"=>"Фонды Сбербанка", "pageTitle"=>"ETF фонды Сбербанка", "filter"=>array("EMITENT_ID", array(263881))),
						"vtb" => array("menuName"=>"Фонды ВТБ", "pageTitle"=>"ETF фонды ВТБ", "filter"=>array("EMITENT_ID", array(264265))),
					 );
                if(!empty($filter_code) && array_key_exists($filter_code, $arPagesList)){
						$title = $arPagesList[$filter_code]["pageTitle"];
                  $APPLICATION->SetTitle($title);
					 }
                ?>


				<div class="title_container">
					<h1><?=$APPLICATION->ShowTitle(false)?></h1>

					<form class="innerpage_search_form" method="GET">
						<div class="form_element">
							<input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете? Введите название или тикер" name="q" id="autocomplete_act_all" />
							<button type="submit"><span class="icon icon-search"></span></button>
						</div>
					</form>
				</div>


       <ul class="actives_list_filter sections_list">
			 <?foreach($arPagesList as $code=>$val):?>
			   <?if($code=="alletf"):?>
				<li class="<?=(empty($filter_code)?'active':'')?>"><a href="<?=$actionsBaseUrl?>"><?=$val["menuName"]?></a></li>
				<?else:?>
            <li class="<?=(!empty($filter_code) && $filter_code==$code?'active':'')?>"><a href="<?=$actionsBaseUrl.$code?>/"><?=$val["menuName"]?></a></li>
				<?endif;?>
			 <?endforeach;?>
      </ul>
		<?
				if(!empty($filter_code) && array_key_exists($filter_code, $arPagesList)){
              $res = new ETF();
				  $arrListFilter = $arPagesList[$filter_code]["filter"];
              $arFilteredItems = $res->setFilterForList($arrListFilter);

              unset($res);
				  }
				  if(empty($arrListFilter)){
				  	$arrListFilter = array("all", "y");
				  }
		?>

				<?
				global $arrFilter;
				if($_REQUEST["q"]){
					$arrFilter[] = array(
						"LOGIC" => "OR",
						array("NAME" => "%".$_REQUEST["q"]."%"),
						array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
					);
				}  else {
				    if(count($arFilteredItems))
                      $arrFilter["CODE"]=array_keys($arFilteredItems);
				}
				//$arrFilter["!PROPERTY_BOARD_ID"] = $APPLICATION->ExcludeFromRadar["акции"];
				?>

				<p>В скобках указаны последняя цена закрытия, прирост за месяц и комиссия</p>

				<?$APPLICATION->IncludeComponent("bitrix:news.list", "obligation_etf_list", array(
					"IBLOCK_TYPE" => "FINPLAN",
					"IBLOCK_ID" => "33",
					"NEWS_COUNT" => "999999",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "NAME",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"ACTIVES_FILTER" => $arrListFilter,
					),
					false
				);?>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>