<?
define("OBLIGATION_PAGE", "Y");
if(isset($_REQUEST["ajax_obligations"]) || isset($_REQUEST["ajax_actions"]) || isset($_REQUEST["ajax_actions_usa"]) || isset($_REQUEST["ajax_etf"]) || isset($_REQUEST["ajax_preset_filter"])){
 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
		$haveRadarAccess = checkPayRadar();
		$haveUSARadarAccess = checkPayUSARadar();
}
if($_REQUEST["ajax_preset_filter"]){
	if($_REQUEST["ajax_preset_type"]=='shares'){
	  include("filters/filter_actions.php");
	}
	exit();
}
if($_REQUEST["ajax_obligations"]){
	include("obligation_table.php");
	exit();
}
if($_REQUEST["ajax_actions"]){
	include("action_table.php");
	exit();
}

if($_REQUEST["ajax_etf"]){
	include("etf_table.php");
	exit();
}

if($_REQUEST["ajax_actions_usa"]){
	include("actions_usa_table.php");
	exit();
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use Bitrix\Main\Page\Asset;
use \Bitrix\Main\Localization\Loc;
\Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__); //языковой файл
Global $APPLICATION, $USER;
if(!$USER->IsAuthorized()){
	LocalRedirect("/lk/radar_in/");
	exit();
} else {
 //   CUsersStat::setStat($USER->GetID(), 'radar', $APPLICATION->GetCurPage(false));
}

// D7 костыльно подключаем скрипт, иначе ошибка в консоли
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.jqplot.min.js");



//Раздел правый блок
CModule::IncludeModule("iblock");
$arFilter = Array('IBLOCK_ID'=>31, 'ID'=>36);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, array("UF_*"));
$arSect = $db_list->GetNext();

$haveRadarAccess = checkPayRadar();
$haveUSARadarAccess = checkPayUSARadar();

if($haveRadarAccess){
    $radarEndDate = checkPayEndDateRadar();
    if($radarEndDate){
        $end = new DateTime($radarEndDate);
        $diff = $end->diff(new DateTime());
    }
}

//Если нет доступа скинуть данные о портфеле из кук
if(!$haveRadarAccess){
	$filter = Array("ID" => $USER->GetID());
	$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter, array("SELECT"=>array("UF_*")));
	if ($arUser = $rsUsers->Fetch()) {
		if($arUser["UF_RADAR_ACCESS"]){
			setcookie("id_arr_shares_cookie", null, -1);
			setcookie("id_arr_debstock_cookie", null, -1);
			setcookie("id_arr_etf_cookie", null, -1);
			setcookie("id_arr_market_map_cookie", null, -1);

			$user = new CUser;
			$fields = Array(
				"UF_RADAR_ACCESS" => false,
			);
			$user->Update($USER->GetID(), $fields);

			LocalRedirect("/lk/obligations/");
		}
	}
}
?>
<div class="wrapper" data-have-access="<?=($haveRadarAccess)?"true":"false"?>">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>

				<div class="calculate_outer">
<? global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
		 ?>
<?//if($GLOBALS["USER"]->IsAdmin() && $arUser["LOGIN"]=="freud"):?>
	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/top_mini_kurs.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	 <?//else:?>
<!--					<div class="calculate_top">
						<p class="calculate_top_title">Заставьте свободные деньги работать</p>

						<p class="calculate_top_description">Выберите <span>активы</span> для своего портфеля</p>
					</div>
-->
  <?//endif;?>





                    <? if($diff && $diff->days <= 45):?>
                        <div class="radar_check_alert">
                            <span class="icon icon-attention"></span><span style="color:red; padding-right: 10px;">Подписка до <?=FormatDate(array(
                                    "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                    "today" => "today",              // выведет "сегодня", если дата текущий день
                                    "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                    "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                    "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                ), MakeTimeStamp($radarEndDate), time()
                                );?></span>
                            <a class="dashed" target="_blank" href="https://fin-plan.org/buy_learning/informatsionno-analiticheskiy-servis-fin-plan-radar-usa-rf-dostup-na-12-mesyatsev-zhurnal/">Продлить подписку на спец. условиях</a>
                        </div>
                    <?endif;?>

					<!-- <p class="calculate_step_title">1. Выберите вид вложения:</p> -->

					<ul class="calculate_tabs tabs_nav radar_tabs">
						<li class="active"><a href="#debstock_tab" data-toggle="tab">Облигации<span class="tab_cnt_marker hidden"></span></a></li>
						<li><a href="#shares_tab" data-toggle="tab">Акции РФ<span class="tab_cnt_marker hidden"></span></a></li>
					  	<li><a href="#shares_usa_tab" data-toggle="tab">Акции США<span class="tab_cnt_marker hidden"></span></a></li>
						<li><a href="#etf_tab" data-toggle="tab">ETF<span class="tab_cnt_marker hidden"></span></a></li>
						<li><a href="/lk/market_map/" >Карта рынка</a></li>
						<li>             
							<a class="<?if(!$haveRadarAccess):?>dib relative tooltip_custom_outer<?endif;?>" <?if(!$haveRadarAccess):?>data-content="<?=$APPLICATION->lkRadarPopups["заглушка портфеля"]?>"<?else:?> href="https://fin-plan.org/portfolio/"<?endif;?> >Портфель</a>
						</li>
					</ul>


					<div class="relative">
						<?include("filter.php")?>

						<div class="calculate_third">
						  <div class="calculate_summ">
							<div class="row">
							  <div class="col col-xs-6 col-lg-4">
								<p class="calculate_summ_element">
								  <span class="calculate_summ_before">Сумма инвестиций:</span> <br/>
								  <span class="calculate_summ_price"><span id="money_main_text"></span>&nbsp;руб.<span class="icon icon-lj_reverse" data-toggle="modal" data-target="#popup_investment_amount"></span></span>

								  <input id="money_main" type="text" value="400000" disabled hidden />
								</p>
							  </div>

							  <div class="col col-xs-6 col-lg-4">
								<p class="calculate_summ_element distributed">
								  <span class="calculate_summ_before">Распределено:</span> <br/>
								  <span class="calculate_summ_price"><span id="money_in_work_text"></span>&nbsp;руб.</span>

								  <input id="money_in_work" type="text" value="0" disabled hidden />
								</p>
							  </div>

							  <div class="col col-xs-6 col-lg-4">
								<p class="calculate_summ_element undistributed">
								  <span class="calculate_summ_before">Не распределено:</span> <br/>
								  <span class="calculate_summ_price"><span id="money_free_text"></span>&nbsp;руб.</span>
								</p>
							  </div>

							  <div class="col col-xs-6 col-lg-4">
								<p class="calculate_summ_element all_income">
								  <span class="calculate_summ_before">Фиксированный доход: <span class="tooltip_btn p-tooltip-green" title="" data-original-title="<?=Loc::getMessage("RAD_PAGE_FIXED_INCOME")?>">i</span></span> <br/>
								  <span class="calculate_summ_price"><span id="all_income_text"></span>&nbsp;руб.</span>
								</p>
							  </div>

							  <div class="col col-xs-6 col-lg-4 no_display">
								<p class="calculate_summ_element shares_profit_max">
								  <span class="calculate_summ_before">MAX доход: <span class="tooltip_btn p-tooltip-green" title="" data-original-title="<?=Loc::getMessage("RAD_PAGE_MAX_INCOME")?>">i</span></span> <br/>
								  <span class="calculate_summ_price"><span id="shares_profit_max_text"></span>&nbsp;руб.</span>
								</p>
							  </div>

							  <div class="col col-xs-6 col-lg-4 no_display">
								<p class="calculate_summ_element shares_profit_min">
								  <span class="calculate_summ_before">MIN доход: <span class="tooltip_btn p-tooltip-green" title="" data-original-title="<?=Loc::getMessage("RAD_PAGE_MIN_INCOME")?>">i</span></span> <br/>
								  <span class="calculate_summ_price"><span id="shares_profit_min_text"></span>&nbsp;руб.</span>
								</p>
							  </div>
							</div>
						  </div>

						  <div class="calculate_graphics">
							<div class="row">
							  <div class="round_chart_outer col-lg-4">
								<canvas id="round_chart"></canvas>
							  </div>

							  <div class="linear_chart_outer col-lg-8">
								<canvas id="linear_chart"></canvas>
							  </div>
							</div>

							<div class="calculate_graphics_checkbox">
							  <div class="row">
								<div class="col-sm-12">
								  <div class="form_element text-center">
									<div class="checkbox">
									  <input id="iis" type="checkbox"/>
									  <label for="iis">Учитывать в расчете использование ИИС для увеличения доходности на 13% <span class="tooltip_btn p-tooltip-green" title="" data-original-title="<?=Loc::getMessage("RAD_PAGE_USE_IIS_FOR_INCREASE")?>">i</span></label>
									</div>
								  </div>
								</div>
							  </div>

							  <div class="row">
								<div class="col-sm-12">
								  <div class="form_element text-center">
									<div class="checkbox">
									  <input type="checkbox" name="replenishment" id="replenishment" />
									  <label for="replenishment">Добавить на график ежемесячное пополнение счёта</label>
									</div>
								  </div>
								</div>
							  </div>
							</div>
						  </div>
						</div>


						<div class="calculate_fourth">
							<div class="calculate_search_form_outer">
								<div class="col">
									<div class="search-form">
										<div class="form_element">
											<?if(!$haveRadarAccess):?>
												<div class="dib relative tooltip_custom_outer" style="width: 100%"  data-content="<?=$APPLICATION->lkRadarPopups["поиск"]?>">
											<?endif?>
												<input id="autocomplete_radar" class="search-form__input" type="text" value="" placeholder="Поиск ценных бумаг">

											<?if($haveRadarAccess):?>
												<div id="ajax_radar_search"></div>
											<?else:?>
												</div>
											<?endif?>
										</div>
									</div>
								</div>
							</div>

							<div class="debstock_table calculate_table obligations">
								<?include("obligation_table.php")?>
							</div>

						  <div class="shares_table calculate_table actions">
                        <?include("action_table.php")?>
						  </div>
						  <?//if($GLOBALS["USER"]->IsAdmin()):?>
						 	  <div class="shares_usa_table calculate_table actions_usa">
	                        <?include("actions_usa_table.php")?>
							  </div>
						  <?//endif;?>
						  <div class="etf_table calculate_table etf">
                        <?include("etf_table.php")?>
						  </div>
						</div>
					</div>


				<?
				if($APPLICATION->obligationMainShareData["img"]) {
					$APPLICATION->AddHeadString('<meta content="'.$APPLICATION->obligationMainShareData["img"].'" property="og:image">',true);
					$APPLICATION->AddHeadString('<link rel="image_src" href="'.$APPLICATION->obligationMainShareData["img"].'" />',true);
				}

				$lj_event = htmlentities('<a href="http://'.$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"].'">'.$APPLICATION->obligationMainShareData["title"].'</a><img src="http://'.$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"].'">');
                ?>
                <div class="article_share">
                    <p>Рассказать&nbsp;другим про&nbsp;данный сервис</p>
                    <ul class="share_list">
                        <li>
                            <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"]?>&amp;title=<?=$APPLICATION->obligationMainShareData["title"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"]?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"]?>&amp;title=<?=$APPLICATION->obligationMainShareData["title"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"]?>"></a>
                        </li>
                        <li>
                            <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"]?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"]?>"></a>
                        </li>
                        <li>
                            <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$APPLICATION->obligationMainShareData["title"]?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$APPLICATION->obligationMainShareData["title"]?>"></a>
                        </li>
                        <li><a href="https://twitter.com/share?text=<?=$APPLICATION->obligationMainShareData["title"]?>" onclick="window.open('https://twitter.com/share?text=<?=$APPLICATION->obligationMainShareData["title"]?>','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a></li>
                    </ul>
                </div>

								<div class="text-center mt-40 relative">
									<p class="legal_popup_btn"><a href="#" data-toggle="modal" data-target="#legal_popup">Условия использования информации, размещённой на данном сайте</a></p>
								</div>
			</div>

<!-- *****КАРКАС***** -->
			</div>
		</div>

		<div class="sidebar">
			<div class="sidebar_inner">
                <form class="innerpage_search_form" method="GET">
                    <div class="form_element">
                        <input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Название / тикер / ISIN" name="q" id="autocomplete_all" />
                        <button type="submit"><span class="icon icon-search"></span></button>
                    </div>
                </form>
<?//if($GLOBALS["USER"]->IsAdmin()):?>
      <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/right_sidebar_services.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?>
<?//else:?>
<!--				<p class="title"><?=$arSect["UF_NAME"]?></p>
				<p class="lightgray"><?=$arSect["DESCRIPTION"]?></p>

				<div class="sidebar_group">
					<div class="sidebar_group_inner">

						<?
						$arFilter = Array("IBLOCK_ID"=>31, "SECTION_ID"=>36, "ACTIVE"=>"Y");
						$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
						while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							$props = $ob->GetProperties();
							$img = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>242, 'height'=>162), BX_RESIZE_IMAGE_EXACT, true);
							?>
							<?if($props["VIDEO"]["VALUE"]):?>
								<div class="sidebar_element no_separator">
									<a class="sidebar_article_img_outer fancybox fancybox.iframe"  href="https://www.youtube.com/embed/<?=$props["VIDEO"]["VALUE"]?>" rel="gallery">
										<div class="sidebar_article_img">
											<img src="<?=$img["src"]?>" alt="" />
										</div>

										<p class="sidebar_article_img_title"><?=$arFields["NAME"]?></p>
									</a>
								</div>
							<?else:?>
								<div class="sidebar_element no_separator">
									<div class="sidebar_article_img_outer">
										<div class="sidebar_article_img">
											<a href="<?=$props["LINK"]["VALUE"]?>" target="_blank"><img src="<?=$img["src"]?>" alt="" /></a>
										</div>

										<p class="sidebar_article_img_title"><a href="<?=$props["LINK"]["VALUE"]?>" target="_blank"><?=$arFields["NAME"]?></a></p>
									</div>
								</div>
							<?endif?>
						<?
						}
					?>

					</div>
				</div>-->
<?//endif; // admin?>

				<hr/>
  <?
	  //Проверка на наличие радара что бы скрыть отзывы для клиентов
	  $userHaveRadar = false;
	  if($USER->IsAuthorized()){
		  $userHaveRadar = checkPayRadar(false, $USER->GetID());
	  }
  ?>
  <?if($userHaveRadar==false):?>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "random_review_right", array(
					"IBLOCK_TYPE" => "FINPLAN",
					"IBLOCK_ID" => "25",
					"NEWS_COUNT" => "1",
					"SORT_BY1" => "RAND",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => ""
				),
					false
				);?>
				<?endif;//$userHaveRadar?>
			</div>
		</div>
	</div>

		<div class="load_overlay">
			<p><span class="radar_animate"><span class="radar_round_inner"></span><span class="radar_round_track"></span><span class="radar_animate_element blue"></span><span class="radar_animate_element green"></span><span class="radar_animate_element orange"></span></span> <span class="radar_txt">Загрузка...</span></p>
		</div>
</div>


<?include("popups.php")?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
