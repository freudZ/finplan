<?
$aMenuLinks = Array(
	Array(
		"Список акций РФ", 
		"/lk/actions/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Список акций США", 
		"/lk/actions_usa/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Список облигаций РФ", 
		"/lk/obligations/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Список ETF РФ", 
		"/lk/etf/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Список Индексов РФ", 
		"/lk/index/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Список эмитентов РФ", 
		"/lk/obligations/company/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Список эмитентов США", 
		"/lk/obligations/company_usa/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Список отраслей РФ", 
		"/lk/industries/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Список секторов США",
		"/lk/sectors_usa/all/",
		Array(),
		Array(),
		""
	),

	Array(
		"Аналитика РФ",
		"/lk/gs/all/",
		Array(),
		Array(),
		""
	),
	Array(
		"Аналитика США",
		"/lk/gs/all_usa/",
		Array(),
		Array(),
		""
	),

);
if($GLOBALS["USER"]->IsAdmin()){



	$aMenuLinks[] =Array(
	  "Список ПИФов по РФ",
	  "/lk/pif/all/",
	  Array(),
	  Array(),
	  ""
	);

	$aMenuLinks[] =	Array(
	  "Список валют",
	  "/lk/currency/all/",
	  Array(),
	  Array(),
	  ""
	);

	$aMenuLinks[] =
	  Array(
		"Список фьючерсных контрактов",
		"/lk/futures/all/",
		Array(),
		Array(),
		""
	  );
	$aMenuLinks[] =Array(
	  "Список макро-показателей по РФ",
	  "/lk/macro_rus/all/",
	  Array(),
	  Array(),
	  ""
	);

}
?>