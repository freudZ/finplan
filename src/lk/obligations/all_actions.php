<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список акций мосбиржи");
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "top_banner",
						"EDIT_TEMPLATE" => "",
						"COMPONENT_TEMPLATE" => ".default",
						"AREA_FILE_RECURSIVE" => "Y"
					),
					false
				);?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
                <?
                //Формирование фильтра облигаций с перезагрузкой страниц и отдельными адресами
                $actionsBaseUrl = '/lk/actions/all/';
                $filter_code = htmlspecialchars($_REQUEST["FILTER_CODE"]);
                $title = "Список акций мосбиржи";
                if(!empty($filter_code))
                switch ($filter_code){
                    case "div":
                        $title = "Список дивидендных акций России";
                        break;
                    case "exp":
                        $title = "Компании экспортеры России";
                        break;
                    case "oil":
                        $title = "Список нефтяных компаний России";
                        break;
                    case "enr":
                        $title = "Акции энергетиков России";
                        break;
                    case "bnk":
                        $title = "Cписок публичных банков";
                        break;
                    case "sht":
                        $title = "Список акций которые можно шортить ";
                        break;
                    case "lvg":
                        $title = "Какие акции можно покупать с плечом (маржинальные акции список)";
                        break;
                    case "bld":
                        $title = "Строительные компании на мосбирже";
                        break;
                    case "met":
                        $title = "Акции металлургов России";
                        break;
                    case "subthsnd":
                        $title = "Список акций до 1000 рублей";
                        break;
                }
                $APPLICATION->SetTitle($title);
                ?>
				<div class="title_container">
					<h1><?=$APPLICATION->ShowTitle(false)?></h1>

					<form class="innerpage_search_form" method="GET">
						<div class="form_element">
							<input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете? Введите название или тикер" name="q" id="autocomplete_act_all" />
							<button type="submit"><span class="icon icon-search"></span></button>
						</div>
					</form>
				</div>

      <ul class="actives_list_filter sections_list">
          <li class="<?=(empty($filter_code)?'active':'')?>"><a href="<?=$actionsBaseUrl?>">Все</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='div'?'active':'')?>"><a href="<?=$actionsBaseUrl?>div/">Дивидендные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='exp'?'active':'')?>"><a href="<?=$actionsBaseUrl?>exp/">Экспортеры</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='oil'?'active':'')?>"><a href="<?=$actionsBaseUrl?>oil/">Нефтяные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='enr'?'active':'')?>"><a href="<?=$actionsBaseUrl?>enr/">Энергетика</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='bnk'?'active':'')?>"><a href="<?=$actionsBaseUrl?>bnk/">Банки</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='sht'?'active':'')?>"><a href="<?=$actionsBaseUrl?>sht/">Можно шортить</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='lvg'?'active':'')?>"><a href="<?=$actionsBaseUrl?>lvg/">Можно с плечом</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='bld'?'active':'')?>"><a href="<?=$actionsBaseUrl?>bld/">Строительные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='met'?'active':'')?>"><a href="<?=$actionsBaseUrl?>met/">Металлурги</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='subthsnd'?'active':'')?>"><a href="<?=$actionsBaseUrl?>subthsnd/">Акции до 1000</a></li>
      </ul>

        <?
      $arrListFilter = array();
      $arFilteredItems = array();
              switch ($filter_code){
                  case "dev":
                      $arrListFilter = array("dividends", "0.1%");
                  break;
                  case "exp":
                      $arrListFilter = array("export_share", 1);
                      break;
                  case "oil":
                      $arrListFilter = array("industry", array("Нефтегазовый сектор"));
                      break;
                  case "enr":
                      $arrListFilter = array("industry", array("Энергогенерирующий сектор", "Энергосбытовой сектор", "Электросетевой сектор"));
                      break;
                  case "bnk":
                      $arrListFilter = array("industry", array("Банковский сектор"));
                      break;
                  case "sht":
                      $arrListFilter = array("margin_deals", "short");
                      break;
                  case "lvg":
                      $arrListFilter = array("margin_deals", "kredit");
                      break;
                  case "bld":
                      $arrListFilter = array("industry", array("Строительный сектор"));
                      break;
                  case "met":
                      $arrListFilter = array("industry", array("Металлургический сектор"));
                      break;
                  case "subthsnd":
                      $arrListFilter = array("lotprice", "0-1000");
                      break;


                  default:
                      $arrListFilter = array("all", "y");
                      break;
              }
              $res = new Actions();
              $arFilteredItems = $res->setFilterForList($arrListFilter);
              unset($res);
/*					global $USER;
                     $rsUser = CUser::GetByID($USER->GetID());
                     $arUser = $rsUser->Fetch();
                     if($arUser["LOGIN"]=="freud"){
							  echo "<pre  style='color:black; font-size:11px;'>";
                          print_r($arrListFilter);
                          print_r($arFilteredItems);
                          echo "</pre>";
                     }*/


                  ?>
				<?
				global $arrFilter;
				if($_REQUEST["q"]){
					
					$arrFilter[] = array(
						"LOGIC" => "OR",
						array("NAME" => "%".$_REQUEST["q"]."%"),
						array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
					);
				} else {
				    if(count($arFilteredItems))
                      $arrFilter["CODE"]=array_keys($arFilteredItems);
				}
				//$arrFilter["!PROPERTY_BOARD_ID"] = $APPLICATION->ExcludeFromRadar["акции"];

				?>
				<p>В скобках указаны последняя цена закрытия и прирост за месяц</p>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "obligation_dist_list", array(
					"IBLOCK_TYPE" => "FINPLAN",
					"IBLOCK_ID" => "33",
					"NEWS_COUNT" => "999999",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "NAME",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"ACTIVES_FILTER" => $arrListFilter,
					),
					false
				);?>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>