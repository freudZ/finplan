<?
if($_SERVER["HTTP_X_REQUESTED_WITH"]=="XMLHttpRequest"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	Global $USER;
	$checkGs = checkPaySeminar(13823, false, $USER->GetID());
	$checkGsUsa = checkPayGSUSA(13823, false, $USER->GetID());
	if(!$checkGs || !$USER->IsAuthorized()){
	  exit;
	}

} else {
	define("GS_DETAIL_PAGE", "Y");
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$checkGs = checkPaySeminar(13823, false, $USER->GetID());
	$checkGsUsa = checkPayGSUSA(13823, false, $USER->GetID());

	if(!$checkGs || !$USER->IsAuthorized()){
	  LocalRedirect("http://Finplan.expert/club");
	}

}


$showGSUsa = checkPayGSUSA(13823, $USER->GetID()); //Определяем оплату с ГС США
$buyGsLink = "https://finplan.expert/radar"; //Ссылка для редиректа на лендинг при неоплаченном ГС США и клике на табу США


use \Bitrix\Main\Localization\Loc;
 \Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__); //языковой файл для фильтра

	$CGs = new CGs();
	$arData = $CGs->getList($_REQUEST["ELEMENT_CODE"]);
	$title = 'Аналитика';
	$arTableData = array();
	if(count($arData)>0){
	  $title = 	$arData[$_REQUEST["ELEMENT_CODE"]][count($arData[$_REQUEST["ELEMENT_CODE"]])-1]["UF_PAGE_TITLE"];
	  $arTableData = $CGs->getGsItemFromRadar($_REQUEST["ELEMENT_CODE"], 'RUS');
	}
	$APPLICATION->SetTitle($title);
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
                <div class="gsPage">
                    <?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            ".default",
                            array(
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "extended_banner",
                                "EDIT_TEMPLATE" => "",
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_RECURSIVE" => "Y"
                            ),
                            false
                        );?>

                        <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
                    <?endif?>

                    <div class="title_container title_nav_outer">
                        <h1><? $APPLICATION->ShowTitle(false); ?></h1>

                        <?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
                            <div class="title_nav">
                                <ul>
                                    <li><a href="/lk/gs/all/">Вся аналитика РФ</a></li>
                                    <li><a href="/lk/gs/all_usa/">Вся аналитика США</a></li>
                                    <li><a href="/learning/13823/">Личный кабинет "Клуб инвесторов"</a></li>
                                </ul>
                            </div>
                        <?endif?>
                    </div>
                    <?if(!$_GET['search_off']):?>
                    <br>
                        <form class="innerpage_search_form hidden" method="GET">
                            <div class="form_element">
                                <input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете? Введите название или тикер" name="q" id="autocomplete_act_all" />
                                <button type="submit"><span class="icon icon-search"></span></button>
                            </div>
                        </form>
                    <?endif?>
    <!-- *****/КАРКАС***** -->
                    <?if(count($arData)>0):?>
                        <?if(count($arTableData)>0):?>
                        <? $arLangTable = array("Рентабельность собственного капитала"=>"Р ск",
                                                        "Доля собственного капитала в активах"=>"Д ск",
                                                        "Темп прироста выручки"=>"Тр Вр",
                                                        "Темп роста прибыли"=>"Тр Пр",
																		  "Таргет"=>"Потенциал",
																		  "Просад"=>"Просад",
                                                        "GROW_HORIZON_PROGNOSE"=>"Горизонт",
                                                        );
                            $arILangTable = array("Рентабельность собственного капитала"=>"GS_TABLE_HEADING1",
                                                         "Доля собственного капитала в активах"=>"GS_TABLE_HEADING2",
                                                         "Темп прироста выручки"=>"GS_TABLE_HEADING3",
                                                         "Темп роста прибыли"=>"GS_TABLE_HEADING4",
                                                         "GROW_HORIZON_PROGNOSE"=>"GS_TABLE_HEADING5",
                                                         "P/E"=>"GS_TABLE_HEADING6",
                                                         "P/Sale"=>"GS_TABLE_HEADING7",
													 						"Таргет"=>"GS_TABLE_HEADING8",
													 						"Просад"=>"GS_TABLE_HEADING9",
                                                        );
                                                        ?>
                            <div class="scrollableTable">
                              <table>
                                <thead>
                                 <tr>
                                   <th>Показатели</th>
                                  <?foreach($arTableData["COMPANY"] as $k=>$v):?>
                                   <?if($k=="NAME") continue;?>
                                             <? $kl = $k; ?>
                                    <?if(array_key_exists($k, $arLangTable)){
                                        $k = $arLangTable[$k];
                                    }?>
                                    <th><?=$k?> <span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-key="<?= $kl ?>" data-original-title="<?=GetMessage($arILangTable[$kl])?>">i</span></th>
                                  <?endforeach;?>
                                 </tr>
                                </thead>
                                <tbody>
                                 <tr>
                                  <?foreach($arTableData["COMPANY"] as $k=>$v):?>
                                    <td><?=$v?></td>
                                  <?endforeach;?>
                                 </tr>
                                 <tr>
                                  <?foreach($arTableData["INDUSTRY"] as $k=>$v):?>
                                    <td><?=$v?></td>
                                  <?endforeach;?>
                                 </tr>
                                </tbody>
                              </table>
                            </div>
                        <?endif;?>
                        <ul>
                            <?foreach($arData[$_REQUEST["ELEMENT_CODE"]] as $arItem):?>
                              <div>
                                <h2><?=$arItem["UF_DATE"]?> <span><strong><?=$arItem["UF_TITLE"]?></strong></span></h2>

                                <?if(!empty($arItem["UF_BODY"])):?>
                                    <p><?=$arItem["UF_BODY"]?></p>
                                <?endif;?>

                                <?if(isset($arItem["UF_FILE"]) && intval($arItem["UF_FILE"]["ID"])>0):?>
                                    <div class="gsPage__files">
                                        <a class="gsPage__file" href="<?=$arItem["UF_FILE"]["SRC"]?>" target="_blank">
                                            <span class="icon icon-download"></span>
                                            <span class="text"><?= $arItem["UF_FILE"]["ORIGINAL_NAME"] ?></span>
                                        </a>
                                    </div>
                                <?endif;?>
                              </div>
                            <?endforeach?>
                        </ul>
                    <?else:?>
                     <p>По данной бумаге аналитика в рамках годового сопровождения не найдена.</p><br>
                    <?endif;?>
                    <div class="gsPage__bottomButtons">
								 <p><a href="/tehanaliz/?actionList=<?=$arTableData["ADDICTIONAL"]["SECID"]."_RUS"?>&templateId=<?=$APPLICATION->TA_template_GS_RUS?>" class="dashed" target="_blank">Открыть в ТА</a></p>
                         <p><a href="#" data-toggle="modal" data-target="#legal_popup" class="dashed">Условия
                                использования
                                информации, размещённой на данном сайте</a></p>
                    </div>

                    <?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
                        <div class="article_stats">
                            <div class="row">
                                <div class="col col-xs-6">
                                </div>

                                <div class="col col-xs-6">
                                    <ul class="article_stats_list">
                                        <li><span class="icon icon-comment"></span> <span id="commentsCount"></span></li>
                                        <li><span class="icon icon-eye"></span> <?=$arResult["SHOW_COUNTER"]?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?endif?>
                </div>
<!-- *****КАРКАС***** -->
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>


<div class="modal fade" id="legal_popup" tabindex="-1">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <div class="modal-header">
      <p class="strong">Условия использования информации с сайта Fin-plan.org</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<div class="modal-body_title"></div>
		<div class="modal-body_inner">
      <p>Вся информация, размещенная на данном интернет-сайте, дизайн, подбор, группировка и расположение материалов являются интеллектуальной собственностью ИП Кошин В.В. и защищены российским законодательством об авторском праве и средствах массовой информации, а также международными договорами РФ по защите интеллектуальной собственности.</p>
      <p>Информация о торгах на рынке группы «московская биржа» предоставлена ПАО Московская биржа. Источником и владельцем биржевой информации является московская биржа.</p>
      <p>Без письменного согласия биржи нельзя осуществлять дальнейшее распространение и предоставление биржевой информации в любом виде и любыми средствами, включая электронные, механические, фотокопировальные, записывающие или другие (в том числе с использованием удаленного мобильного (беспроводного) доступа), её трансляцию, в том числе средствами теле- и радиовещания, её демонстрацию на интернет-сайтах, её использование в игровых, тренажерных и иных системах, предусматривающих демонстрацию и/или передачу биржевой информации, а также для расчёта производной информации (в том числе индексов и индикаторов), предназначенной для дальнейшего публичного распространения.</p>
      <p>ИП Кошин В.В. является дистрибьютором информации об итогах торгов на рынках группы московская биржа.</p>
      <p>Материалы данного интернет-сайта предназначены исключительно для персонального и некоммерческого использования. Запрещается использование автоматизированного извлечения информации данного интернет-сайта без письменного разрешения ИП Кошин В.В. Любое копирование, перепечатка и/или последующее распространение информации, представленной на данном сайте, или информации, полученной на основе этой информации в результате переработки, в том числе производимое путем кэширования, кадрирования или использованием аналогичных средств, строго запрещается без предварительного письменного разрешения ИП Кошин В.В. За нарушение настоящего правила наступает ответственность, предусмотренная законодательством и международными договорами РФ.</p>
      <p>ИП Кошин В.В. не несет ответственность за любую потерю или ущерб (потеря деловых доходов, потеря прибыли или любой прямой, косвенный, последующий, специальный или подобный ущерб вообще), являющийся результатом:</p>
      <ul>
        <li>любой погрешности (или неполноты в, или задержки, прерывания, ошибки или упущения в поставке) в информации;</li>
        <li>любого сбоя информационной системы;</li>
        <li>любого решения или действия, сделанного в уверенности относительно информации.</li>
      </ul>
      <p>ИП Кошин В.В. не несет ответственность за ущерб, причиненный вашему компьютеру, программному обеспечению, модему, телефону и другой собственности, который стал результатом использования информации, программного обеспечения или услуг.</p>
      <p>С уважением, компания Fin-plan.</p>
		</div>
	  </div>
	</div>
  </div>
</div>


<?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
	<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?endif?>
