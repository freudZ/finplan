<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$page = 1;
if($_REQUEST["page"]){
	$page = $_REQUEST["page"];
}
use \Bitrix\Main\Localization\Loc;
 \Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__); //языковой файл для фильтра
$resActionsUsa = new ActionsUsa();
$arData = $resActionsUsa->getTable($page);
$userHaveGs = checkPaySeminar(13823, false, $USER->GetID());
/*echo "<pre  style='color:black; font-size:11px;'>";
	print_r($arData);
	echo "</pre>";*/
?>

<div>
	<div class="calculate_table_head">
		<div class="row">
			<div class="col col_left col-sm-7">
				<p class="calculate_step_title">3. Выберите акции США (показано <?=$arData["shows"]?> из <?=$arData["total_items"]?>):</p>
			</div>
			<div class="col col_right col-sm-5"><span class="calculate_table_clear_btn">Сбросить портфель</span></div>
		</div>
	</div>

	<table class="smart_table">
	  <thead>
		<tr>
		  <th class="name_col">Акции США:</th>

		  <th class="price_col">Цена акции США.&nbsp;/ <br/>Цена компании, млн.</th>

		  <th class="pe_col">P/e&nbsp;/ <br/>Рск,%&nbsp;/ <br/>ТР пр.,%&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_ACTION_TABLE_USA_PE_RSK_TR_PR")?>">i</span></th>

		  <th class="turnover_col">Дивид.<br/> за год,<br/> % / cумма </th>

		  <th class="profitability_col">Потенциал&nbsp;/ <br/>Просад&nbsp;/ <br/>Бета<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_ACTION_TABLE_USA_PROGNOSE_PRODAD")?>">i</span></th>

		  <th class="inputs_col">Кол-во <br/>лотов</th>

		  <th class="check_col">Выбрать <br>акцию
			<div class="checkbox">
			  <input id="calculate_table_all_share_usa" class="calculate_table_all" type="checkbox"/>
			  <label for="calculate_table_all_share_usa"></label>
			</div>
		  </th>
		</tr>
	  </thead>

	  <tbody>
		<? if(!is_array($arData["items"])){
			$arData["items"] = array();
		} ?>
		<?foreach($arData["items"] as $item):?>

		<tr data-id="<?=$item["PROPS"]["SECID"]?>" data-lotsize="1" data-currency="<?=(!empty($item["PROPS"]["CURRENCY"])?strtolower($item["PROPS"]["CURRENCY"]):"usd")?>"<?if(!$arData["access"] && $item["PROPS"]["PROP_TARGET"]>=20):?> data-access="false"<?endif?>>
			  <td class="cshare">
				<p class="elem_name trim">
					<?if(!$arData["access"] && $item["DYNAM"]["Таргет"]>=20):?>
						<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть акцию</a>
					<?else:?>
						<a href="<?=$item["URL"]?>" class="elem_name__link tooltip_btn" title="<?=$item["NAME"]?> (<?=$item["PROPS"]["SECID"]?>)" portfolio-title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a>
						  <?if($userHaveGs):?>
							  <?if($item["DYNAM"]["HAVE_GS"]=='Y'):?>
							     <?$gs_link = "По бумаге доступна аналитика в рамках годового сопровождения - <a class='' href='/lk/gs_usa/".$item["CODE"]."/' target='_blank'>Смотреть</a>";?>
							     <a class="gs_tooltip_btn tooltip_btn p-tooltip-green" data-placement="top" title="" data-original-title="<?=$gs_link?>"  href="/lk/gs_usa/<?=$item["CODE"]?>/">A</a>
						  	  <?endif;?>
						  <?endif;?>
					<?endif?>
				</p>
				<?if($item["COMPANY"]):?>
					<p class="company_name line_green">
						<?if(!$arData["access"] && $item["DYNAM"]["Таргет"]>=20):?>
							<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть компанию</a>
						<?else:?>
							<a class="tooltip_btn" title="<?=$item["COMPANY"]["NAME"]?>" href="<?=$item["COMPANY"]["URL"]?>"><?=$item["COMPANY"]["NAME"]?></a>
						<?endif?>
					</p>
				<?endif?>
				<div class="no_display">
				  <p class="date_off" data-date=""></p>
				</div>
			  </td>
			  <td>
				<?if(!$arData["access"] && $item["DYNAM"]["Таргет"]>=20):?>
					<p class="elem_cshare_price price_elem" data-price="-"></p>
					<p class="elem_buy_price price_elem  no_display" data-price="-"></p>
					<p class="elem_company_price price_elem" data-price="-"></p>
				<?else:?>
					<p class="elem_cshare_price price_elem" data-price="<?=$item["PROPS"]["LASTPRICE"]?>"></p>
					<p class="elem_buy_price price_elem no_display" data-price="<?=$item["DYNAM"]["Цена лота"]?>"></p>
					<p class="elem_company_price price_elem" data-price="<?=$item["PROPS"]["ISSUECAPITALIZATION"]?>"></p>
				<?endif?>

				<div class="no_display">
				  <p class="elem_sell_price sell_price_up" data-price=""></p>
				  <p class="elem_sell_price sell_price_down" data-price=""></p>
				</div>
			  </td>
			  <td>
				<p><?=$item["DYNAM"]["PE"]?></p>
				<?$period = reset($item["PERIODS"])?>
				<p><?=round($period["Рентабельность собственного капитала"],2)?></p>
				<p><?=round($period["Темп роста прибыли"],2)?></p>
			  </td>
			  <td>
			  	<?
				$divSum = $item["PROPS"]["DIVIDENDS"];
				if($divSum>99){
					$divSum = round($divSum,0);
				}
				$divPrc = $item["DYNAM"]["Дивиденды %"];
				 ?>
				<p class="cshares_dividends_prc" data-divprc="<?=$divPrc?>"><?= (floatval($divPrc)>0?$divPrc."%":'-'); ?></p>
				<p class="cshares_dividends" data-div="<?=$divSum?>"><?=(floatval($divSum)>0?$divSum.getCurrencySign($item["PROPS"]["CURRENCY"]):'-')?></p>
			  </td>
			  <td>
					<?
					$dynam_target = floatval($item["DYNAM"]["Таргет"])>0 ? $item["DYNAM"]["Таргет"] : 0;
					$dynam_sales = 0;

					;

					$item["DYNAM"]["Просад"] ? $dynam_sales = $item["DYNAM"]["Просад"] : $dynam_sales = 0;
					?>
				<p class="cshare_up_p line_green"  data-value="<?=$dynam_target?>">+<span></span>%</p>
				<p class="cshare_down_p line_red"  data-value="<?=$dynam_sales?>">-<span></span>%</p>
				<p class=""  data-value="<?=$item["PROPS"]["BETTA"]?>"><?=round($item["PROPS"]["BETTA"],2)?></p>
				<div class="no_display">
				  <p class="profit_year_up"></p>
				  <p class="profit_year_down"></p>
				  <p class="profit_main_up"></p>
				  <p class="profit_main_down"></p>
				</div>
			  </td>
			  <td>
				<input class="numberof" type="text" value="5"/>
			  </td>
			  <td>
				<div class="checkbox">
				  <input type="checkbox" id="<?=$item["PROPS"]["SECID"]?>"/>
				  <label for="<?=$item["PROPS"]["SECID"]?>"></label>
				</div>
			  </td>
			</tr>
		<?endforeach?>
	  </tbody>
	</table>

	<div class="more-results-wrap">

	<? $portfolio = new CPortfolio(); ?>
	<?$checkPortfolio = $portfolio->checkPortfolioCntForUser($USER->GetID(), false);
	if($checkPortfolio["PORTFOLIO"]["LIMIT"]>$checkPortfolio["PORTFOLIO"]["OWNER"] || $checkPortfolio["ADMIN"]=="Y" ){
	$createNew = true;
	} else {
	$createNew = false;
	}
	unset($portfolio);
	?>
		  <div class=" text-right">
		  	<br>
	  	  <span class="<?if(!$haveRadarAccess):?>dib relative tooltip_custom_outer<?endif;?> calculate_table_add_portfolio_btn" <?if(!$haveRadarAccess):?>
	  data-content="<?=$APPLICATION->lkRadarPopups["заглушка портфеля"]?>"
	  <?else:?>
	   data-toggle="modal" data-target="#popup_add_portfolio"<?endif;?>><?= ($createNew?'Создать&nbsp;портфель':'Добавить&nbsp;в&nbsp;портфель') ?></span>
		  </div>

		<?if($arData["cur_page"]<$arData["count_page"] && $arData["count_page"]):?>
			<div class="more_results text-center">
				<button class="button" data-next="<?=$arData["cur_page"]+1?>">Загрузить ещё</button>
			</div>
		<?endif?>

	</div>
</div>
<? unset($resActionsUsa); ?>
