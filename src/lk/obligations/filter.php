<?use Bitrix\Highloadblock as HL;?>
<?use \Bitrix\Main\Localization\Loc;?>
<? \Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__); //языковой файл для фильтра?>
<form class="calculate_form" method="post">
   <!-- курс валют по ЦБ -->
   <? $ResActionsUSA = new ActionsUSA();
   $arFilterEnums = $ResActionsUSA->getRadarFiltersValues();
   unset($ResActionsUSA);
   ?>
	<?foreach($arFilterEnums["CURRENCY"] as $n=>$item):?>
	<?if($item=="RUR" || $item=="SUR" || $item=="RUB"):?>
	 <input type="text" id="<?= strtolower($item) ?>_currency" data-sign="<?= getCurrencySign(''); ?>" value="1" hidden />
	<?else:?>
	 <input type="text" id="<?= strtolower($item) ?>_currency" data-sign="<?= getCurrencySign($item); ?>" value="<?=getCBPrice($item)?>" hidden />
	<?endif;?>
   <?endforeach;?>

   <!-- --- -->
   <div class="tab-content calculate_form_green">

	 <!-- obligations -->
	 <? include("filters/filter_obligations.php"); ?>

	 <!-- actions -->
	 <? include("filters/filter_actions.php"); ?>

	 <?//if($GLOBALS["USER"]->IsAdmin()):?>
	 <!-- actions_usa -->
	 <? include("filters/filter_actions_usa.php"); ?>
	 <?//endif;?>
	 
	 <!-- etf -->
	 <? include("filters/filter_actions_etf.php"); ?>

   </div>
</form>
