<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Список американских акций на Санкт-Петербургской бирже - Цена, котировки и графики на сегодня онлайн");
$APPLICATION->SetTitle("Список американских акций на Санкт-Петербургской бирже");
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "top_banner",
						"EDIT_TEMPLATE" => "",
						"COMPONENT_TEMPLATE" => ".default",
						"AREA_FILE_RECURSIVE" => "Y"
					),
					false
				);?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
<?
                //Формирование фильтра акций США с перезагрузкой страниц и отдельными адресами
                $actionsBaseUrl = '/lk/actions_usa/all/';
                $filter_code = htmlspecialchars($_REQUEST["FILTER_CODE"]);
                $title = "Список американских акций на Санкт-Петербургской бирже";

                if(!empty($filter_code)){
                switch ($filter_code){
                    case "chi":
                        $title = "Акции китайских компаний список";
                        break;
                    case "bio":
                        $title = "Биотехнологии список акций";
                        break;
                    case "eur":
                        $title = "Список европейских акций";
                        break;
                    case "div":
                        $title = "Список дивидендных акций США";
                        break;
                    case "ncl":
                        $title = "Нецикличные компании США";
                        break;
                    case "ccl":
                        $title = "Акции цикличных компаний";
                        break;
                    case "ind":
                        $title = "Акции промышленных компаний сша";
                        break;
                    case "raw":
                        $title = "Акции сырьевых компаний сша";
                        break;
                    case "tcn":
                        $title = "Акции технологических компаний сша";
                        break;
                    case "fin":
                        $title = "Акции финансовых компаний сша";
                        break;
                    case "hlt":
                        $title = "Акции здравоохранения сша";
                        break;
                    case "oil":
                        $title = "Акции нефтяных компаний сша";
                        break;
                    case "blu":
                        $title = "Голубые фишки США";
                        break;
                }
                $APPLICATION->SetTitle($title);
					 }
                ?>
				<div class="title_container">
					<h1><?=$APPLICATION->ShowTitle(false)?></h1>

					<form class="innerpage_search_form" method="GET">
						<div class="form_element">
							<input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете? Введите название или тикер" name="q" id="autocomplete_actions_usa" />
							<button type="submit"><span class="icon icon-search"></span></button>
						</div>
					</form>
				</div>
      
      <ul class="actives_list_filter sections_list">
          <li class="<?=(empty($filter_code)?'active':'')?>"><a href="<?=$actionsBaseUrl?>">Все</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='chi'?'active':'')?>"><a href="<?=$actionsBaseUrl?>chi/">Китайские</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='bio'?'active':'')?>"><a href="<?=$actionsBaseUrl?>bio/">Биотех</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='eur'?'active':'')?>"><a href="<?=$actionsBaseUrl?>eur/">Европейские</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='div'?'active':'')?>"><a href="<?=$actionsBaseUrl?>div/">Дивидендные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='ncl'?'active':'')?>"><a href="<?=$actionsBaseUrl?>ncl/">Нецикличные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='ccl'?'active':'')?>"><a href="<?=$actionsBaseUrl?>ccl/">Цикличные</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='ind'?'active':'')?>"><a href="<?=$actionsBaseUrl?>ind/">Промышленность</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='raw'?'active':'')?>"><a href="<?=$actionsBaseUrl?>raw/">Сырьевые</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='tcn'?'active':'')?>"><a href="<?=$actionsBaseUrl?>tcn/">Технологические</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='fin'?'active':'')?>"><a href="<?=$actionsBaseUrl?>fin/">Финансовые</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='hlt'?'active':'')?>"><a href="<?=$actionsBaseUrl?>hlt/">Здравоохранение</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='oil'?'active':'')?>"><a href="<?=$actionsBaseUrl?>oil/">Нефтяные (сланцевые)</a></li>
          <li class="<?=(!empty($filter_code) && $filter_code=='blu'?'active':'')?>"><a href="<?=$actionsBaseUrl?>blu/">Голубые фишки</a></li>
      </ul>

<?
      $arrListFilter = array();
      $arFilteredItems = array();
              switch ($filter_code){
                  case "chi":
                      $arrListFilter = array("country", "Китай");
                  break;
                  case "bio":
                      $arrListFilter = array("top", "Биотехнологии");
                      break;
                  case "oil":
                      $arrListFilter = array("top", "Сланцевики");
                      break;
                  case "eur":
                      $arrListFilter = array("country", array("Германия", "Италия", "Россия", "Казахстан", "Швейцария", "Великобритания", "Ирландия", "Кипр", "Люксембург", "Нидерланды", "Франция", "Швеция" ));
                      break;
                  case "div":
                      $arrListFilter = array("dividends", "0.1%");
                      break;
                  case "ncl":
                      $arrListFilter = array("sector", array("Нецикличные компании"));
                      break;
                  case "ccl":
                      $arrListFilter = array("sector", array("Цикличные компании"));
                      break;
                  case "ind":
                      $arrListFilter = array("sector", array("Промышленность"));
                      break;
                   case "raw":
                      $arrListFilter = array("sector", array("Сырье"));
                      break;
                  case "tcn":
                      $arrListFilter = array("sector", array("Технологии"));
                      break;
                  case "fin":
                      $arrListFilter = array("sector", array("Финансы"));
                      break;
                  case "hlt":
                      $arrListFilter = array("sector", array("Здравоохранение"));
                      break;
                  case "blu":
                      $arrListFilter = array("top", "Голубые фишки");
                      break;
                  default:
                      $arrListFilter = array("all", "y");
                      break;
              }


              $res = new ActionsUsa();
              $arFilteredItems = $res->setFilterForList($arrListFilter);
              unset($res);
/*					global $USER;
                     $rsUser = CUser::GetByID($USER->GetID());
                     $arUser = $rsUser->Fetch();
                     if($arUser["LOGIN"]=="freud"){
							  echo "<pre  style='color:black; font-size:11px;'>";
                          print_r($arrListFilter);
                          print_r($arFilteredItems);
                          echo "</pre>";
                     }*/

                  ?>
				<?
				global $arrFilter;
				if($_REQUEST["q"]){
					$arrFilter[] = array(
						"LOGIC" => "OR",
						array("NAME" => "%".$_REQUEST["q"]."%"),
						array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
					);
				} else {
				    if(count($arFilteredItems))
                      $arrFilter["CODE"]=array_keys($arFilteredItems);
				}
				//$arrFilter["!PROPERTY_BOARD_ID"] = $APPLICATION->ExcludeFromRadar["акции"];
				?>

				 <p>В скобках указаны последняя цена закрытия и прирост за месяц</p>

				<?$APPLICATION->IncludeComponent("bitrix:news.list", "actions_usa_list", array(
					"IBLOCK_TYPE" => "FINPLAN",
					"IBLOCK_ID" => "56",
					"NEWS_COUNT" => "999999",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "NAME",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",

					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"ACTIVES_FILTER" => $arrListFilter,
					),
					false
				);?>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>