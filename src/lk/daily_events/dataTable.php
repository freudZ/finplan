<?
 if($_POST["ajax"] && $_POST["ajax"]=="y"){
 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
 $haveRadarAccess = checkPayRadar();
 $haveUSARadarAccess = checkPayUSARadar();

 //Для неавторизованных и не имеющих доступа к радару (и к портфелям) сбрасываем фильтр по портфелям
 if((!$USER->IsAuthorized() || !$haveRadarAccess) && isset($_REQUEST["eventPortfolio"])){
	 unset($_REQUEST["eventPortfolio"]);
 }
 $CREvents = new CRadarEvents();

 $selectedEventTypes = array();
 if(isset($_REQUEST["eventTypes"]) && !empty($_REQUEST["eventTypes"])){
	$selectedEventTypes = explode(",",$_REQUEST["eventTypes"]);
 }
 elseif(!isset($_REQUEST["eventTypes"])){
  $selectedEventTypes = array_keys($CREvents->arEventsTypes);
 }

} else { //Берем класс из index.php
  $selectedEventTypes = array_keys($CREvents->arEventsTypes);
}
 ?>
<?$showedEvents = 0;?>

<div class="EventsCalendar">
    <div class="EventsCalendar__element">

		  <?foreach($CREvents->arEventsFiltered as $date=>$events):?>
		  <?
		    $hideDate = true;
			 foreach($selectedEventTypes as $eventSelDetail){
				if(count($events[$eventSelDetail])>0){
					$hideDate = false;
					break;
				}
			 }
		   ?>
				 <?

 ?>
		  <?if(count($selectedEventTypes)==0 || $hideDate) continue;?>
		  <h3 class="EventsCalendar__date">События на <?= $date ?></h3>
            <?foreach($CREvents->arEventsTypes as $event=>$eventName):?>
				  <? //Выводим шапки таблиц по видам событий ?>
                <?if(count($events[$event])<=0) continue;?>
						<?if($event=="DIVIDENDS" || $event=="DIVIDENDS_USA"):?>
				        <table class="EventsCalendar__table">
				            <thead>
				                <tr>
				                    <th><?=$CREvents->arEventsTypes[$event]?></th>
				                    <th>Текущая цена акции</th>
				                    <th>Дивиденды</th>
				                </tr>
				            </thead>
				            <tbody>
						<?elseif($event=="COUPONS"):?>
							<table class="EventsCalendar__table">
							            <thead>
							                <tr>
							                    <th><?=$CREvents->arEventsTypes[$event]?></th>
							                    <th>Купон</th>
							                    <th>Купон, в %</th>
							                </tr>
							            </thead>
							            <tbody>
						<?elseif($event=="CANCELLATION"  || $event=="OFERTA" || $event=="GOSA" || $event=="SOVIET" || $event=="WEBINARS"):?>
							<table class="EventsCalendar__table">
							            <thead>
							                <tr>
							                    <th><?=$CREvents->arEventsTypes[$event]?></th>
							                </tr>
							            </thead>
							            <tbody>
						    <?//echo "Облигация <a class=\"green\" href=\"".$detailEvent["URL"]."\" target=\"_blank\">".$detailEvent["NAME"]."</a><br>";?>
						<?elseif($event=="REPORT_USA" || $event=="REPORT_RUS"):?>
					        <table class="EventsCalendar__table cols-2">
					            <thead>
					                <tr>
					                    <th colspan="2"><?=$CREvents->arEventsTypes[$event]?></th>
					                </tr>
					            </thead>
					            <tbody>
						<?else:?>
								<table class="EventsCalendar__table">
								            <thead>
								                <tr>
								                    <th><?=$CREvents->arEventsTypes[$event]?></th>
								                </tr>
								            </thead>
								            <tbody>
						<?endif;?>

						<?//Цикл по событиям  ?>
                        <?foreach($events[$event] as $detailEvent):?>
                            <?if($event=="DIVIDENDS" || $event=="DIVIDENDS_USA"):?>
									 <?$showedEvents++;?>
                               <tr>
					                    <td><a href="<?= $detailEvent["URL"] ?>" target="_blank"><?= $detailEvent["NAME"] ?></a></td>
					                    <td>
					                        <p class="label">Текущая цена акции</p>
					                        <p class="value"><?= $detailEvent["Цена на дату закрытия"] ?><?=($event=="DIVIDENDS"?'&nbsp;руб.':'&nbsp;$')?></p>
					                    </td>
					                    <td>
					                        <p class="label">Дивиденды</p>
					                        <p class="value"><?= $detailEvent["Дивиденды"] ?><?=($event=="DIVIDENDS"?'&nbsp;руб.':'')?> (<?= $detailEvent["Дивиденды, в %"] ?>&nbsp;%)</p>
					                    </td>
					                </tr>
                            <?elseif($event=="COUPONS"):?>
									 <?$showedEvents++;?>
					                <tr>
					                    <td><a href="<?= $detailEvent["URL"] ?>" target="_blank" data-currency="<?=$detailEvent["CURRENCY"]?>"
											  data-value="<?=$detailEvent["-"]?>"><?= $detailEvent["NAME"] ?></a>
											   <?if($detailEvent["DEFAULT"]==true):?>
												  &nbsp;<span class="red">(дефолт)&nbsp;</span>
												<?endif;?>
											   <?if($detailEvent["STRUCTURED"]==true):?>
												  &nbsp;<span class="red">(структурная)</span>
												<?endif;?>
											  </td>
					                    <td>
					                        <p class="label">Купон</p>
					                        <p class="value"><?= $detailEvent["-"] ?><?if(is_numeric(str_replace(" ","",$detailEvent["-"]))):?>&nbsp;<?= getCurrencySign($detailEvent["CURRENCY"]) ?><?endif;?></p>
					                    </td>
					                    <td>
					                        <p class="label">Купон, в %</p>
					                        <p class="value"><?= $detailEvent["Ставка купона"] ?><?if(is_numeric($detailEvent["Ставка купона"])):?>&nbsp;%<?endif;?></p>
					                    </td>
					                </tr>
                            <?elseif($event=="CANCELLATION"  || $event=="OFERTA"):?>
									 <?$showedEvents++;?>
					                <tr>
					                    <td><a href="<?= $detailEvent["URL"] ?>" target="_blank"><?= $detailEvent["NAME"] ?></a>
											   <?if($detailEvent["DEFAULT"]==true):?>
												  &nbsp;<span class="red">(дефолт)&nbsp;</span>
												<?endif;?>
											   <?if($detailEvent["STRUCTURED"]==true):?>
												  &nbsp;<span class="red">(структурная)</span>
												<?endif;?>
											  </td>
					                </tr>
                            <?elseif($event=="REPORT_USA" || $event=="REPORT_RUS"):?>
									 <?$showedEvents++;?>
                                <?$arTitle = explode(" - ",$detailEvent["UF_TITLE"]);?>
					                <tr>
					                    <td><a href="<?= $detailEvent["UF_LINK"] ?>" target="_blank"><?= $detailEvent["UF_EMITENT"] ?></a></td>
					                    <td><?= count($arTitle)>1?$arTitle[1]:$detailEvent["UF_TITLE"] ?></td>
					                </tr>
                            <?elseif($event=="GOSA"):?>
									 <?$showedEvents++;?>
					                <tr>
					                    <td><?echo $detailEvent["UF_TITLE"].( !empty($detailEvent["UF_LINK"])?(" <a class=\"green\" href=\"".$detailEvent["UF_LINK"]."\" target=\"_blank\">Подробнее</a>"):"");?></td>
					                </tr>
                            <?else:?>
									 <?$showedEvents++;?>
					                <tr>
					                    <td><?echo $detailEvent["UF_TITLE"].( !empty($detailEvent["UF_DESCRIPTION"])?"<br>".$detailEvent["UF_DESCRIPTION"]."&nbsp;":"").( !empty($detailEvent["UF_LINK"])?(" <a class=\"green\" href=\"".$detailEvent["UF_LINK"]."\" target=\"_blank\">Подробнее</a>"):"");?></td>
					                </tr>
                            <?endif;?>
									 <?if($GLOBALS["USER"]->IsAdmin() && $_REQUEST["debug"]=='y'){
							        	echo "Под админом <pre  style='color:black; font-size:11px;'>";
							        	   print_r($detailEvent);
							        	   echo "</pre>";
							        }?>
                        <?endforeach;?>
				            </tbody>
				        </table>
				<?endforeach;?>
		  <?endforeach;?>
		  <?if($showedEvents==0):?>
		    <h3 class="EventsCalendar__date">За выбранный период дат нет ни одного события.</h3>
		  <?endif;?>
		  <?if(count($selectedEventTypes)==0):?>
		  	 <h3 class="EventsCalendar__date">Выберите одно или несколько событий для показа в календаре.</h3>
		  <?endif;?>
    </div>
</div>