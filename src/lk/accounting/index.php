<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>

<div class="content-main-lk-list-wrapper clearfix" data-page="<?=$_SESSION["accounting"]["cur_page"]?>">
  <div class="content-main-lk-list-left pull-left">
	<div class="content-main-lk-list-left-title">
	  Счёт:
	</div>
	<?$bills = getBillList()?>
	<div class="content-main-lk-list-left-summary content-main-lk-list-left-item<?if($_SESSION["accounting"]["bill"]=="all"):?> active<?endif?>" data-id="all">
		Итого по всем счетам <span><?=$bills["TOTAL_SUMM"]?> р</span>
	</div>
	<a class="btn_link" href="#" id="save_sort_btn">Сохранить</a>
	<?foreach($bills["ITEMS"] as $n=>$bill):?>
		<div class="content-main-lk-list-left-item<?if($_SESSION["accounting"]["bill"]==$bill["ID"]):?> active<?endif?>" data-id="<?=$bill["ID"]?>">
			<div class="up"<?if(!$n):?> style="display:none"<?endif?>></div>
			<div class="down"<?if(!$bills["ITEMS"][$n+1]):?> style="display:none"<?endif?>></div>
			<?=$bill["UF_NAME"]?> <span><?=$bill["SUMM"]?> <?=$bill["CURRENCY"]["UF_VIEW"]?></span>
			<a href="#" data-toggle="modal" data-target="#edit_bill" class="edit_bill_popup" data-id="<?=$bill["ID"]?>"><span class="content-main-lk-list-account-edit glyphicon glyphicon-edit"></span></a>
		</div>
	<?endforeach?>
	<div class="content-main-lk-list-left-item-button">
	  <div class="content-main-lk-list-right-header-button content-main-lk-list-right-header-button-left">
		<a href="#" data-toggle="modal" data-target="#new_bill">Добавить счет</a>
	  </div>
	</div>
  </div>
  <div class="content-main-lk-list-right">
	<div class="content-main-lk-list-right-top-menu">
	  <div class="content-main-lk-list-right-top-menu-item">
		<a href="/lk/accounting/?change_menu=1">
			<div class="content-main-lk-list-right-top-menu-item-icon content-main-lk-list-right-top-menu-item-icon-1<?if($_SESSION["accounting"]["cur_page"]==1):?>-active active<?endif?>" title="Ввод операций по счетам" rel="tipsy"> 
			</div>
		</a>
	  </div>
	  <div class="content-main-lk-list-right-top-menu-item">
		<a href="/lk/accounting/?change_menu=2">	
			<div class="content-main-lk-list-right-top-menu-item-icon content-main-lk-list-right-top-menu-item-icon-2<?if($_SESSION["accounting"]["cur_page"]==2):?>-active active<?endif?>" title="Динамика остатков по счетам" rel="tipsy"> 
			</div>
		</a>
	  </div>
	  <div class="content-main-lk-list-right-top-menu-item">
		<a href="/lk/accounting/?change_menu=3">	
			<div class="content-main-lk-list-right-top-menu-item-icon content-main-lk-list-right-top-menu-item-icon-3<?if($_SESSION["accounting"]["cur_page"]==3):?>-active active<?endif?>" title="Сравнение с лимитами" rel="tipsy"> 
			</div>
		</a>
	  </div>
	  <div class="content-main-lk-list-right-top-menu-item">
		<a href="/lk/accounting/?change_menu=4">	
			<div class="content-main-lk-list-right-top-menu-item-icon content-main-lk-list-right-top-menu-item-icon-4<?if($_SESSION["accounting"]["cur_page"]==4):?>-active active<?endif?>" title="Диаграмма расходов и доходов" rel="tipsy"> 
			</div>
		</a>
	  </div>
	</div>
	<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/core/menu/page".$_SESSION["accounting"]["cur_page"].".php");?>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>