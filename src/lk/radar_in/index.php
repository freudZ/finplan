<?
define("OUTER_CLASS", "loginpage");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Fin-plan Radar - иновационный сервис для анализа ценных бумаг");
$APPLICATION->SetPageProperty("og:image", "/local/templates/new/img/login_page/main_bg_sm-2.jpg");

$mode = "dev";
if($USER->IsAuthorized()){
	LocalRedirect("/lk/obligations/");
	exit();
}
?>

<div class="wrapper">
 <div class="main_content">
<!-- *****/КАРКАС***** -->
 </div>

  <div class="loginpage_inner">
	<div class="container">
	  <div class="loginpage_inner_first_line">
		<div class="col col_left">
		  <div class="radar_logo">
			<img class="radar_logo_img" src="<?=SITE_TEMPLATE_PATH?>/img/radar.svg" alt=" Fin-Plan RADAR" />
			<span class="radar_logo_text">Fin-Plan</span>
			<span class="radar_logo_radar">Radar</span>
		  </div>
		</div>

		<div class="col col_mid">
		  <p class="loginpage_description_yellow">Инновационный веб&#8209;сервис для&nbsp;инвесторов</p>
		</div>

		<div class="col col_right">
			<?if($mode=="reg"):?>
				<span class="loginpage_form_scroll_btn"><span class="text">Войти в <br>программу</span> <span class="icon icon-arr_down"></span></span>
			<?else:?>
				<span class="loginpage_form_scroll_btn button highlight" data-toggle="modal" data-target="#popup_login"><span class="text">Войти в <br />программу</span></span>
			<?endif?>
		</div>
	  </div>

	  <div class="loginpage_inner_video_line">
		<div class="loginpage_video_outer">
		 <!-- <span class="loginpage_video_img" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/login_page/main_bg_sm-2.jpg');"></span> -->
		  <span class="loginpage_video_img" style="background-image: url('https://img.youtube.com/vi/s7N27p86tAI/maxresdefault.jpg;"></span>
		  <span class="play"></span>
		  <!--<iframe width="560" height="315" src="https://www.youtube.com/embed/vstor3H-lnI?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>-->
		  <iframe width="560" height="315" src="https://www.youtube.com/embed/s7N27p86tAI?rel=0&vq=hd1080&modestbranding=1&autohide=1&controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	  </div>

	  <div class="loginpage_inner_second_line">
		<div class="col col_mid">
		  <p class="loginpage_title">Посмотрите презентацию сервиса</p>
		  <p class="loginpage_description">Fin-plan Radar - поиск результативных инвест-идей</p>
		</div>

		<div class="col col_left">
		  <p class="loginpage_logo_msk_outer">Наш&nbsp;партнер: <img class="loginpage_logo_msk" src="<?=SITE_TEMPLATE_PATH?>/img/logo_msk.png" alt="Московская биржа" /></p>
		</div>
	  </div>
	<?if($mode=="reg"):?>
	  <form class="loginpage_form loginpage_form_destination" method="post">
		<p class="loginpage_form_title">Войти в программу</p>

		<div class="row">
		  <div class="col col_left col-sm-4">
			<div class="form_element">
			  <input type="text" placeholder="Ваше имя" name="name" />
			</div>
		  </div>

		  <div class="col col_mid col-sm-4">
			<div class="form_element">
			  <input type="text" placeholder="Электронная почта" name="email" />
			</div>
		  </div>

		  <div class="col col_right col-sm-4">
			<div class="form_element">
			  <input type="text" placeholder="Мобильный телефон" name="phone" />
			</div>
		  </div>
		</div>

		<div class="loginpage_form_footer">
		  <div class="confidencial_element form_element">
			<div class="checkbox">
			  <input type="checkbox" id="confidencial_loginpage" name="confidencial_loginpage" checked="">
			  <label for="confidencial_loginpage">Нажимая кнопку, я даю согласие на обработку персональных данных <br>и соглашаюсь с </label><span data-toggle="modal" data-target="#footer356">пользовательским соглашением</span><label for="confidencial_loginpage"> и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
			</div>
		  </div>

		  <div class="submit_element">
			<input type="submit" class="button" value="<?if($mode=="reg"):?>Записаться<?else:?>Войти<?endif?>" />
		  </div>
		</div>
	  </form>
	<?endif?>
	</div>
  </div>

 <div class="main_content">
<!-- *****КАРКАС***** -->
 </div>

 <div class="sidebar">

 </div>
</div>

<?if($mode=="reg"):?>
	<div class="modal fade" id="success-login-radar" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<p class="modal-title uppercase">Спасибо!</p>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		  </div>
		  <div class="modal-body">
			<p>Вы зарегистрированы на презентацию приложения! В ближайшее время Вам придет письмо с дополнительными материалами!</p>
		  </div>
		</div>
	  </div>
	</div>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
