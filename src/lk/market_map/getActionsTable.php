<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Localization\Loc;
\Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__); //языковой файл

$exchange = 'moex';
if(isset($_REQUEST["exchange"]) && !empty(htmlspecialchars($_REQUEST["exchange"]))){
 $exchange = htmlspecialchars($_REQUEST["exchange"]);
}
?>
<?
$page = 1;
if($_REQUEST["page"]){
	$page = $_REQUEST["page"];
}
  //Фильтр по периоду
   $filter_period = '';
	$use_current_period = false;
  if(isset($_REQUEST["f_period"]) && !empty(htmlspecialchars($_REQUEST["f_period"]))){
	$filter_period = $_REQUEST["f_period"];

	if($filter_period == $_REQUEST["f_period_current"]){
	 $use_current_period = true;
/*	 $filter_period = explode('-',$filter_period);
	 	 if($filter_period[0]==1){
	 	 	$filter_period[1] = $filter_period[1]-1;
			$filter_period[0] = 4;
	 	 } else {
			$filter_period[0] = $filter_period[0]-1;
	 	 }

		 $filter_period = implode("-",$filter_period);*/
		 		 $curActQuart = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_QUARTAL");
  	    $curActYear = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_YEAR");
		 $filter_period = $curActQuart."-".$curActYear;
	}


  }

	  if($exchange=='moex'){
		$res = new ActionsMM();
		} else if($exchange=='spbx'){
		$res = new ActionsMMUSA();
		}
		$arData = $res->getTable($page);

// D7
/*$cache = Bitrix\Main\Data\Cache::createInstance();
if ($cache->initCache(86400, md5('mm_get_action_table'.serialize($_REQUEST)), '/market_map_getActionTable/'))
{
    $arData = $cache->getVars();
}
elseif ($cache->startDataCache())
{
    $result = array();

		$res = new ActionsMM($page);
		$arData = $res->getTable($page);
    if ($isInvalid)
    {
        $cache->abortDataCache();
    }

    $cache->endDataCache($arData);
}*/





?>
	<div class="calculate_table_head">
		<div class="row">
			<div class="col col_left col-sm-7">
				<p class="calculate_step_title">3. Выберите акцию (показано <?=$arData["shows"]?> из <?=$arData["total_items"]?>):</p>
			</div>
			<!--<div class="col col_right col-sm-5"><span class="calculate_table_clear_btn">Сбросить портфель</span></div> -->
		</div>
	</div>
	    <div class="smart_table_outer">
                  <table class="market_map_table calculate_table_ smart_table shares_table_">
                    <thead>
                      <tr>
                        <th class="col_name">Акция</th>
								<?if($_REQUEST["f_preset"]==0):?>
                        <th class="col_forecast">Прогноз&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_PROGNOSE")?>">i</span></th>
                        <th class="col_pe">P/E&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_PE")?>">i</span></th>
                        <th class="col_dividends">Дивиденды&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_DIVIDENDS")?>">i</span></th>
								<?endif;?>
 								<?if($_REQUEST["f_preset"]==1):?>
                        <th class="col_forecast">Рост&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_GROWTH")?>">i</span></th>
                        <th class="col_pe">Недооценка</th>
                        <th class="col_dividends">Дивиденды&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_DIVIDENDS")?>">i</span></th>
								<?endif;?>
 								<?if($_REQUEST["f_preset"]==2):?>
                        <th class="col_forecast">Прогноз&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_PROGNOSE")?>">i</span></th>
                        <th class="col_pe">EV/Ebitda</th>
                        <th class="col_dividends">Дивиденды&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_DIVIDENDS")?>">i</span></th>
								<?endif;?>
								<?if($_REQUEST["f_preset"]==3):?>
                        <th class="col_forecast">Рентабельность СК&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_RETURN_OF_EQUITY")?>">i</span></th>
                        <th class="col_pe">Рост&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_GROWTH")?>">i</span></th>
                        <th class="col_dividends">Капитализация, млн.&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_CAPITALIZATION")?>">i</span></th>
								<?endif;?>
								<?if($_REQUEST["f_preset"]==4):?>
                        <th class="col_forecast">EV/Ebitda</th>
                        <th class="col_pe">Net debt/Ebitda</th>
                        <th class="col_dividends">Дивиденды&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-original-title="<?=Loc::getMessage("RAD_MMAP_TBL_DIVIDENDS")?>">i</span></th>
								<?endif;?>
                        <th class="col_number">Кол-во <br/>лотов</th>
                        <th class="col_checkboxes">Выбрать <br>акцию
                          <div class="checkbox">
                            <input id="calculate_table_all" class="calculate_table_all" type="checkbox"/>
                            <label for="calculate_table_all"></label>
                          </div>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    	<?foreach($arData["items"] as $item):?>

							<? if(array_key_exists($filter_period."-KVARTAL",$item["PERIODS"])){
							$arSelectedPeriod = $item["PERIODS"][$filter_period."-KVARTAL"];
						  }
						  ?>
                      <tr data-id="<?=$item["PROPS"]["SECID"]?>" data-price="<?=$item["PROPS"]["LASTPRICE"]?>" data-elem_buy_price="<?=$item["DYNAM"]["Цена лота"]?>" data-company-price="<?=$item["PROPS"]["ISSUECAPITALIZATION"]?>" data-currency="rub"<?if(!$arData["access"] && $item["PROPS"]["PROP_TARGET"]>=20):?> data-access="false"<?endif?>>
                        <td >
                          <p class="elem_name trim">
									<?if(!$arData["access"] && $item["DYNAM"]["Таргет"]>=20):?>
										<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть акцию</a>
									<?else:?>
										<a href="<?=$item["URL"]?>" class="tooltip_btn" title="<?=$item["NAME"]?>"><?=$item["NAME"]?></a>
									<?endif?>
								  </p>
									<?if($item["COMPANY"]):?>
										<p class="company_name line_green">
											<?if(!$arData["access"] && $item["DYNAM"]["Таргет"]>=20):?>
												<a href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть компанию</a>
											<?else:?>
												<a class="tooltip_btn" title="<?=$item["COMPANY"]["NAME"]?>" href="<?=$item["COMPANY"]["URL"]?>"><?=$item["COMPANY"]["NAME"]?></a>
											<?endif?>
										</p>
									<?endif?>
								<div class="no_display">
								  <p class="date_off" data-date=""></p>
								  <p class="elem_sell_price sell_price_up" data-price=""></p>
								  <p class="elem_sell_price sell_price_down" data-price=""></p>
								</div>
                        </td>

								<?if($_REQUEST["f_preset"]==0):?>
                        <td class="col_forecast" data-forecast="<?=$item["DYNAM"]["Таргет"]?>"><?=$item["DYNAM"]["Таргет"]?></td>
								<?$curPE = ($use_current_period)?round($item["DYNAM"]["PE"],2):round($arSelectedPeriod["P/E"],2)?>
                        <td class="col_pe" data-pe="<?=$curPE?>"><?=$curPE?></td>
                        <td class="col_dividends" data-dividends="<?=$item["DYNAM"]["Дивиденды %"]?>"><?=$item["DYNAM"]["Дивиденды %"]?></td>
								<?endif;?>
								<?if($_REQUEST["f_preset"]==1):?>
								<td></td>
								<td></td>
								<td></td>
								<?endif;?>
								<?if($_REQUEST["f_preset"]==2):?>
								<td></td>
								<td></td>
								<td></td>
								<?endif;?>
								<?if($_REQUEST["f_preset"]==3):?>
								<?$curCap = ($use_current_period)?round($item["PROPS"]["ISSUECAPITALIZATION"]/1000000,2):round($arSelectedPeriod["Прошлая капитализация"],2);?>
                        <td class="col_forecast" data-forecast="<?=round($arSelectedPeriod["Рентабельность собственного капитала"],2)?>"><?=round($arSelectedPeriod["Рентабельность собственного капитала"],2)?></td>
                        <td class="col_pe" data-pe="<?=round($arSelectedPeriod["Темп роста прибыли"],2)?>"><?=round($arSelectedPeriod["Темп роста прибыли"],2)?></td>
                        <td class="col_dividends" data-dividends="<?=$curCap;?>"><?=$curCap;?><?= getCurrencySign($item["PROPS"]["CURRENCY"]) ?></td>
								<?endif;?>
								<?if($_REQUEST["f_preset"]==4):?>
								<td></td>
								<td></td>
								<td></td>
								<?endif;?>
                        <td>
                          <input class="numberof" type="text" value="5"/>
                        </td>
                        <td>
                          <div class="checkbox">
                            <input type="checkbox" id="<?=$item["PROPS"]["SECID"]?>"/>
                            <label for="<?=$item["PROPS"]["SECID"]?>"></label>
                          </div>
                        </td>
                      </tr>
							<?endforeach;?>
                    </tbody>
                  </table>
        </div>
	<div class="more-results-wrap">

		<?if($arData["cur_page"]<$arData["count_page"] && $arData["count_page"]):?>
			<div class="more_results text-center">
				<button class="button" data-next="<?=$arData["cur_page"]+1?>">Загрузить ещё</button>
			</div>
		<?endif?>
	</div>
	<? unset($res);
		unset($arData); ?>