<?define("MARKET_MAP_PAGE", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


if(!$USER->IsAuthorized()){
	LocalRedirect("/lk/radar_in/");
	exit();
}

//Раздел правый блок
CModule::IncludeModule("iblock");
$arFilter = Array('IBLOCK_ID'=>31, 'ID'=>36);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, array("UF_*"));
$arSect = $db_list->GetNext();

$haveRadarAccess = checkPayRadar();

if($haveRadarAccess){
    $radarEndDate = checkPayEndDateRadar();
    if($radarEndDate){
        $end = new DateTime($radarEndDate);
        $diff = $end->diff(new DateTime());
    }
}

//Если нет доступа скинуть данные о портфеле из кук
if(!$haveRadarAccess){
	$filter = Array("ID" => $USER->GetID());
	$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter, array("SELECT"=>array("UF_*")));
	if ($arUser = $rsUsers->Fetch()) {
		if($arUser["UF_RADAR_ACCESS"]){
			setcookie("id_arr_shares_cookie", null, -1);
			setcookie("id_arr_debstock_cookie", null, -1);
			setcookie("id_arr_market_map_cookie", null, -1);

			$user = new CUser;
			$fields = Array(
				"UF_RADAR_ACCESS" => false,
			);
			$user->Update($USER->GetID(), $fields);

			LocalRedirect("/lk/obligations/");
		}
	}
}

$exchange = 'moex';
if(isset($_REQUEST["exchange"]) && !empty(htmlspecialchars($_REQUEST["exchange"]))){
 $exchange = htmlspecialchars($_REQUEST["exchange"]);
}


?>
<div class="wrapper" data-have-access="<?=($haveRadarAccess)?"true":"false"?>">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>

				<div class="calculate_outer">
	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/top_mini_kurs.php"),
        Array(),
        Array("MODE"=>"php")
    );?>				
<!--					<div class="calculate_top">
						<p class="calculate_top_title">Заставьте свободные деньги работать</p>

						<p class="calculate_top_description">Выберите <span>активы</span> для своего портфеля</p>
					</div>-->
                    <? if($diff && $diff->days <= 45):?>
                        <div class="radar_check_alert">
                            <span class="icon icon-attention"></span><span style="color:red; padding-right: 10px;">Подписка до <?=FormatDate(array(
                                    "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                    "today" => "today",              // выведет "сегодня", если дата текущий день
                                    "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                    "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                    "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                ), MakeTimeStamp($radarEndDate), time()
                                );?></span>
                            <a class="dashed" target="_blank" href="http://fin-plan.org/buy_learning/informatsionno-analiticheskiy-servis-fin-plan-radar-12-1  ">Продлить подписку на спец. условиях</a>
                        </div>
                    <?endif;?>

					<!-- <p class="calculate_step_title">1. Выберите вид вложения:</p> -->

					<ul class="calculate_tabs tabs_nav">
						<li><a href="/lk/obligations/#debstock_tab_lnk" >Облигации</a></li>
						<li><a href="/lk/obligations/#shares_tab_lnk" >Акции</a></li>
						<li><a href="/lk/obligations/#shares_usa_tab_lnk" >Акции США</a></li>
						<li><a href="/lk/obligations/#etf_tab" data-toggle="tab">ETF</a></li>
						<li class="active"><a href="/lk/market_map/" data-toggle="tab">Карта рынка</a></li>
						<li><a href="/portfolio/" >Портфель</a></li>
					</ul>


					<div class="relative">
						<?include("filter.php")?>
						<div class="calculate_third">
						  <div class="calculate_summ">
							<div class="row">
							  <div class="col col-xs-6 col-lg-4">
								<p class="calculate_summ_element">
								  <span class="calculate_summ_before">Сумма инвестиций:</span> <br/>
								  <span class="calculate_summ_price"><span id="money_main_text"></span>&nbsp;руб.<span class="icon icon-lj_reverse" data-toggle="modal" data-target="#popup_investment_amount"></span></span>

								  <input id="money_main" type="text" value="0" disabled hidden />
								</p>
							  </div>

							  <div class="col col-xs-6 col-lg-4">
								<p class="calculate_summ_element distributed">
								  <span class="calculate_summ_before">Распределено:</span> <br/>
								  <span class="calculate_summ_price"><span id="money_in_work_text"></span>&nbsp;руб.</span>

								  <input id="money_in_work" type="text" value="0" disabled hidden />
								</p>
							  </div>

							  <div class="col col-xs-6 col-lg-4">
								<p class="calculate_summ_element undistributed">
								  <span class="calculate_summ_before">Не распределено:</span> <br/>
								  <span class="calculate_summ_price"><span id="money_free_text"></span>&nbsp;руб.</span>
								</p>
							  </div>
							</div>
						  </div>

						</div>


                <div class="market_map_chart_outer" data-array="">
                  <div id="market_map_chart"></div>
                  <div id="market_map_tooltip"></div>
                </div>
					<div class="calculate_table shares_table actionsMM">
					 <? if($_REQUEST["ajax_actions"]=="y") {
					 include("getActionsTable.php");
					 }
					 ?>
					</div>
					 <div id="post_table" style="display:none;"></div>
					</div><!-- relative -->


				<?
				if($APPLICATION->obligationMainShareData["img"]) {
				  	$APPLICATION->AddHeadString('<meta content="'.$APPLICATION->obligationMainShareData["img"].'" property="og:image">',true);
				  	$APPLICATION->AddHeadString('<link rel="image_src" href="'.$APPLICATION->obligationMainShareData["img"].'" />',true);
				}

			  	$lj_event = htmlentities('<a href="https://'.$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"].'">'.$APPLICATION->obligationMainShareData["title"].'</a><img src="http://'.$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"].'">');
                ?>
                <div class="article_share">
                    <p>Рассказать&nbsp;другим про&nbsp;данный сервис</p>
                    <ul class="share_list">
                        <li>
                            <a onclick="window.open('https://vkontakte.ru/share.php?url=https://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"]?>&amp;title=<?=$APPLICATION->obligationMainShareData["title"]?>&amp;image=https://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"]?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="https://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"]?>&amp;title=<?=$APPLICATION->obligationMainShareData["title"]?>&amp;image=https://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"]?>"></a>
                        </li>
                        <li>
                            <a onclick="window.open('https://www.facebook.com/sharer.php?u=https://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"]?>&amp;p[images][0]=https://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"]?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="https://www.facebook.com/sharer.php?u=https://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["url"]?>&amp;p[images][0]=https://<?=$_SERVER['HTTP_HOST'].$APPLICATION->obligationMainShareData["img"]?>"></a>
                        </li>
                        <li>
                            <a onclick="window.open('https://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$APPLICATION->obligationMainShareData["title"]?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="https://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$APPLICATION->obligationMainShareData["title"]?>"></a>
                        </li>
                        <li><a href="https://twitter.com/share?text=<?=$APPLICATION->obligationMainShareData["title"]?>" onclick="window.open('https://twitter.com/share?text=<?=$APPLICATION->obligationMainShareData["title"]?>','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a></li>
                    </ul>
                </div>

								<div class="text-center mt-40 relative">
									<p class="legal_popup_btn"><a href="#" data-toggle="modal" data-target="#legal_popup">Условия использования информации, размещённой на данном сайте</a></p>
								</div>
			</div>

<!-- *****КАРКАС***** -->
			</div>
		</div>

		<div class="sidebar">
			<div class="sidebar_inner">
                <form class="innerpage_search_form" method="GET">
                    <div class="form_element">
                        <input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Название / тикер / ISIN" name="q" id="autocomplete_all" />
                        <button type="submit"><span class="icon icon-search"></span></button>
                    </div>
                </form>
<? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/right_sidebar_services.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?>			
<!--				<p class="title"><?=$arSect["UF_NAME"]?></p>
				<p class="lightgray"><?=$arSect["DESCRIPTION"]?></p>

				<div class="sidebar_group">
					<div class="sidebar_group_inner">
						<?
						$arFilter = Array("IBLOCK_ID"=>31, "SECTION_ID"=>36, "ACTIVE"=>"Y");
						$res = CIBlockElement::GetList(Array("SORT"=>"asc"), $arFilter, false, false);
						while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							$props = $ob->GetProperties();
							$img = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>242, 'height'=>162), BX_RESIZE_IMAGE_EXACT, true);
							?>
							<?if($props["VIDEO"]["VALUE"]):?>
								<div class="sidebar_element no_separator">
									<a class="sidebar_article_img_outer fancybox fancybox.iframe"  href="https://www.youtube.com/embed/<?=$props["VIDEO"]["VALUE"]?>" rel="gallery">
										<div class="sidebar_article_img">
											<img src="<?=$img["src"]?>" alt="" />
										</div>

										<p class="sidebar_article_img_title"><?=$arFields["NAME"]?></p>
									</a>
								</div>
							<?else:?>
								<div class="sidebar_element no_separator">
									<div class="sidebar_article_img_outer">
										<div class="sidebar_article_img">
											<a href="<?=$props["LINK"]["VALUE"]?>" target="_blank"><img src="<?=$img["src"]?>" alt="" /></a>
										</div>

										<p class="sidebar_article_img_title"><a href="<?=$props["LINK"]["VALUE"]?>" target="_blank"><?=$arFields["NAME"]?></a></p>
									</div>
								</div>
							<?endif?>
						<?
						}
					?>
					</div>
				</div>-->
				<hr/>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "random_review_right", array(
					"IBLOCK_TYPE" => "FINPLAN",
					"IBLOCK_ID" => "25",
					"NEWS_COUNT" => "1",
					"SORT_BY1" => "RAND",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "DOUBLE_POST",
						1 => "ATTACH_POST",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_ADDITIONAL" => ""
				),
					false
				);?>
			</div>
		</div>
	</div>


<div class="modal modal_black fade in" id="popup_empty_actions" tabindex="-1" data-backdrop="static" style="display: none;">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">в этом секторе нет акций, удовлетворяющих условиям карты рынка!</p>

	  </div>
			<br>
	  <div class="modal-body">
		<div class="form_element text-center">
			<p class="modal-title"><small>Вам будут показаны все отрасли</small></p><br>
		  <span class="popup_refill_submit button" data-dismiss="modal">ОК</span>
		</div>
	  </div>
	</div>
  </div>
</div>

		<div class="load_overlay">
			<p><span class="radar_animate"><span class="radar_round_inner"></span><span class="radar_round_track"></span><span class="radar_animate_element blue"></span><span class="radar_animate_element green"></span><span class="radar_animate_element orange"></span></span> <span class="radar_txt">Загрузка...</span></p>
		</div>
</div>


<?include("popups.php")?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>