<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$page = 1;
if($_REQUEST["page"]){
	$page = $_REQUEST["page"];
}
  $arPresets = array(
		0=>array("NAME"=>"Прогноз, Р/Е, дивиденды", "ALL_MIN_X"=>0, "ALL_MIN_Y"=>"", "ALL_MAX_X"=>20, "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"p/e", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Прогноз", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>"P/E", "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		1=>array("NAME"=>"Рост, недооценка, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"Недооценка", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Рост", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		2=>array("NAME"=>"Прогноз, EV/Ebitda, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"EV/Ebitda", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Прогноз", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		3=>array("NAME"=>"Рентабельность СК, рост, капитализация", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>150, "ALL_MAX_Y"=>130, "AXIS_X_NAME"=>"Рост", "AXIS_X_POSTFIX"=>"%", "AXIS_Y_NAME"=>"Рентабельность СК", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Капитализация", "EXT_PARAM_POSTFIX"=>"млн.р.", "SORT_FIELD"=>'Темп роста прибыли', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		4=>array("NAME"=>"EV/Ebitda, Net debt/Ebitda, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"Net debt/Ebitda", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"EV/Ebitda", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
  );
/*if(empty($_POST)){ //Только для отладки из адресной строки
	$_POST = $_REQUEST;
}*/

//TODO заменить на json данные из post для идентичности данных
  $arCollationsSector = array(0=>array("name"=>"Банковский сектор", "code"=>"bank_sector"),
  										1=>array("name"=>"Высокотехнологический сектор", "code"=>"technology_sector"),
  										2=>array("name"=>"Горнодобывающий сектор", "code"=>"mining_sector"),
  										3=>array("name"=>"Машиностроительный сектор", "code"=>"machine_sector"),
  										4=>array("name"=>"Медийный сектор", "code"=>"media_sector"),
  										5=>array("name"=>"Металлургический сектор", "code"=>"metallurgical_sector"),
  										6=>array("name"=>"Нефтегазовый сектор", "code"=>"oil_sector"),
  										7=>array("name"=>"Потребительский сектор", "code"=>"consumer_sector"),
  										8=>array("name"=>"Ритейлерский сектор", "code"=>"retail_sector"),
  										9=>array("name"=>"Строительный сектор", "code"=>"construction_sector"),
  										10=>array("name"=>"Телекоммуникационный сектор", "code"=>"telecommunication_sector"),
  										11=>array("name"=>"Транспортный сектор", "code"=>"transport_sector"),
  										12=>array("name"=>"Финансовый сектор", "code"=>"financial_sector"),
  										13=>array("name"=>"Химический сектор", "code"=>"chemical_sector"),
  										14=>array("name"=>"Электросетевой сектор", "code"=>"electric_sector"),
  										15=>array("name"=>"Энергогенерирующий сектор", "code"=>"energy_gen_sector"),
  										16=>array("name"=>"Энергосбытовой сектор", "code"=>"energy_sell_sector"),
  										17=>array("name"=>"Все отрасли", "code"=>"all"),
										);

  $arCollationMirrorSector = array();
  for($i=0; $i<count($arCollationsSector); $i++){
	$arCollationMirrorSector[$arCollationsSector[$i]["name"]] = $arCollationsSector[$i]["code"];
  }


$show_current_cap = false;

  //Получаем информацию по акциям только с установленным эмитентом
	$arFilterActions_23 = Array("IBLOCK_ID"=>32, "ACTIVE"=>"Y", "!PROPERTY_EMITENT_ID"=>false, "=PROPERTY_HIDEN"=>false);

  //Расширяем фильтр из POST
  //Фильтр по отрасли
  if(isset($_POST["f_industry"]) && !empty(htmlspecialchars($_POST["f_industry"])) && $_POST["f_industry"]!=="all"){
	$arFilterActions_23["=PROPERTY_PROP_SEKTOR"] = htmlspecialchars($_POST["f_industry"]);
  }
  //Фильтр по рынку
  if(isset($_POST["f_market"]) && intval($_POST["f_market"])>0){
	$arFilterActions_23["IBLOCK_ID"] = intval($_POST["f_market"]);
  }

  //Фильтр по периоду
   $filter_period = '';
  if(isset($_POST["f_period"]) && !empty(htmlspecialchars($_POST["f_period"]))){
	$filter_period = $_POST["f_period"];

	if($filter_period == $_POST["f_period_current"]){
	 $show_current_cap = true;
	 $filter_period = explode('-',$filter_period);
	 	 if($filter_period[0]==1){
	 	 	$filter_period[1] = $filter_period[1]-1;
			$filter_period[0] = 4;
	 	 } else {
			$filter_period[0] = $filter_period[0]-1;
	 	 }

		 $filter_period = implode("-",$filter_period);
	}


  }

$cacheTime = 86400;
$cacheId = md5(serialize($arFilterActions_23).$filter_period.serialize($_POST) );
$cacheDir = "/market_map/";
// D7
$cache = Bitrix\Main\Data\Cache::createInstance();
if ($cache->initCache($cacheTime, $cacheId, $cacheDir))
{
    $result = $cache->getVars();
}
elseif ($cache->startDataCache())
{


    $result = array("arr_begin"=>array(), "arr_axis"=>array());
	 //start cache
	$res = CIBlockElement::GetList(Array(), $arFilterActions_23, false, false, array("NAME", "IBLOCK_ID", "CODE", "PROPERTY_EMITENT_ID", "PROPERTY_ISSUECAPITALIZATION","PROPERTY_PROP_TIP_AKTSII"));
	$arActions = array();
	while($ob = $res->Fetch())
	{
	   $arFields = $ob;
		$arActions[$arFields["PROPERTY_EMITENT_ID_VALUE"]][$arFields["PROPERTY_PROP_TIP_AKTSII_VALUE"]] = $arFields;
	}



	 $arActionsTmp = array();
	 $res = new RadarPlusActions();
	foreach($arActions as $emitentId=>$arActions){
	  $arOneAction = array();
	  if(array_key_exists("Обыкновенная",$arActions)){  //TODO Добавить сравнение див. доходности, взять большую
		 $arOneAction = $arActions["Обыкновенная"];
	  }else if(array_key_exists("Привилегированная",$arActions)){
		 $arOneAction = $arActions["Привилегированная"];
	  } else continue;

	  if(count($arOneAction)<=0) continue;

	  //$res = new ActionsMM();
	  //$res = new Actions();
		$arActionsTmp[$emitentId]["ACTION"] = $res->getItemBySECID($arOneAction["CODE"]);

 		$arActionsTmp[$emitentId]["ACTION"]["PERIODS"] = $arActionsTmp[$emitentId]["ACTION"]["PERIODS"];

		if($arActionsTmp[$emitentId]["ACTION"]["PERIODS"]){
			$i=1;
			foreach($arActionsTmp[$emitentId]["ACTION"]["PERIODS"] as $period=>$vals){
				if($vals["Темп прироста выручки"]){
					$arActionsTmp[$emitentId]["ACTION"]["Темп роста компании"] += $vals["Темп прироста выручки"];
				}
				if($vals["Темп роста прибыли"]){
					$arActionsTmp[$emitentId]["ACTION"]["Темп роста прибыли"] += $vals["Темп роста прибыли"];
				}

				$i++;
				if($i>=4){
					if($arActionsTmp[$emitentId]["ACTION"]["Темп роста компании"]){
						$arActionsTmp[$emitentId]["ACTION"]["Темп роста компании"] = $arActionsTmp[$emitentId]["ACTION"]["Темп роста компании"]/4;
						if($arActionsTmp[$emitentId]["ACTION"]["Темп роста компании"]>15){
							$arActionsTmp[$emitentId]["ACTION"]["Темп роста компании"] = 15;
						}
					}
					if($arActionsTmp[$emitentId]["ACTION"]["Темп роста прибыли"]){
						$arActionsTmp[$emitentId]["ACTION"]["Темп роста прибыли"] = $arActionsTmp[$emitentId]["ACTION"]["Темп роста прибыли"]/4;
						if($arActionsTmp[$emitentId]["ACTION"]["Темп роста прибыли"]>15){
							$arActionsTmp[$emitentId]["ACTION"]["Темп роста прибыли"] = 15;
						}
					}
					break;
				}
			}
		}
	}//foreach $arActions;

			$arr_begin = array();
        	$arSumm = array();
			$arr_bubbles_sizes = 0;
			$max_bubble = 0;
        	foreach($arActionsTmp as $k=>$item):

					  if(empty($item["ACTION"]["COMPANY"]["NAME"])) continue;
					  //Фильтруемый период
					  $arSelectedPeriod = array(); //Данные за выбранный период
					  $arPrevPeriod = array(); //Данные за этот же период год назад
					  if(array_key_exists($filter_period."-KVARTAL",$item["ACTION"]["PERIODS"])){
						$arSelectedPeriod = $item["ACTION"]["PERIODS"][$filter_period."-KVARTAL"];
					  }
					  $ar_filter_period = explode('-',$f_period);
					  //Получаем выбранный квартал за прошлый год
					  if(is_array($ar_filter_period)){
			 			$arYears = array(intval($ar_filter_period[1]),(intval($ar_filter_period[1])-1)); //TODO фильтруем по текущему выбранному году + год назад для расчетов итогов таблицы
						  if(array_key_exists($ar_filter_period[0]."-".end($arYears)."-KVARTAL",$item["ACTION"]["PERIODS"])){
							$arPrevPeriod = $item["ACTION"]["PERIODS"][$filter_period."-KVARTAL"];
						  }
			 			}

						if($show_current_cap==true){
							$cap = round($item["ACTION"]["PROPS"]["ISSUECAPITALIZATION"]/1000000,2);
							$curPE = round($item["ACTION"]["DYNAM"]["PE"],2);
						} else {
							$cap = round($arSelectedPeriod["Прошлая капитализация"],2);
							$curPE = round($arSelectedPeriod["P/E"],2);
						}

						//Назначим код сектора для работы с графиком
						$item["ACTION"]["PROPS"]["PROP_SEKTOR_CODE"] = $arCollationMirrorSector[$item["ACTION"]["PROPS"]["PROP_SEKTOR"]];

						if($_POST["f_preset"]==0){
						$dynam_target = floatval($item["ACTION"]["DYNAM"]["Таргет"])>0 ? $item["ACTION"]["DYNAM"]["Таргет"] : 0;
/*						 define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/getData_log.txt");
                   AddMessage2Log('$item = '.print_r(array_merge($item,array("curPE"=>$curPE)), true),'');*/
						if($dynam_target>0 && $item["ACTION"]["DYNAM"]["Дивиденды %"]>0 && $curPE>0 ) {
						 $add = true;
						 //Проверяем на ограничение диапазона, что бы не гонять лишние данные для всех отраслей
						 $gain = intval($arPresets[intval($_POST["f_preset"])]["ALL_MAX_X"]);
						 if($show_current_cap=true && ($gain>0 && $curPE>$gain)){
						 $add = false;
						 }


						 if($add){
						$arr_begin[] = array(
													 $curPE,
													 $dynam_target,
													 round($item["ACTION"]["DYNAM"]["Дивиденды %"],2),
													 $item["ACTION"]["NAME"],
													 $item["ACTION"]["PROPS"]["PROP_SEKTOR_CODE"]
													 );
						  //Получение максимального значения для кругов
						  if($max_bubble<round($item["ACTION"]["DYNAM"]["Дивиденды %"],2)) $max_bubble=round($item["ACTION"]["DYNAM"]["Дивиденды %"],2);
							}//if add
						}
						}

						if($_POST["f_preset"]==1){
						$dynam_target = floatval($item["ACTION"]["DYNAM"]["Таргет"])>0 ? $item["ACTION"]["DYNAM"]["Таргет"] : 0;

						if($arSelectedPeriod["Темп роста прибыли"]>0 && $item["ACTION"]["DYNAM"]["Дивиденды %"]>0 && 0>0) {
						$arr_begin[] = array(
													 0,//Недооценки пока не существует, сделаем позже
													 round($arSelectedPeriod["Темп роста прибыли"],2),
													 round($item["ACTION"]["DYNAM"]["Дивиденды %"],2),
													 $item["ACTION"]["NAME"],
													 $item["ACTION"]["PROPS"]["PROP_SEKTOR_CODE"]
													 );
						}
						}

						if($_POST["f_preset"]==2){
						$dynam_target = floatval($item["ACTION"]["DYNAM"]["Таргет"])>0 ? $item["ACTION"]["DYNAM"]["Таргет"] : 0;

						if($dynam_target>0 && $item["ACTION"]["DYNAM"]["Дивиденды %"]>0 ) {
						$arr_begin[] = array(
													 round($arSelectedPeriod["EV/Ebitda"],2),
													 $dynam_target,//Прогноз
													 round($item["ACTION"]["DYNAM"]["Дивиденды %"],2),
													 $item["ACTION"]["NAME"],
													 $item["ACTION"]["PROPS"]["PROP_SEKTOR_CODE"]
													 );
						}
						}
						if($_POST["f_preset"]==3){
						 $add = true;
						 //Проверяем на ограничение диапазона, что бы не гонять лишние данные для всех отраслей
						 $gain = intval($arPresets[intval($_POST["f_preset"])]["ALL_MAX_X"]);
						 if($show_current_cap=true && ($gain>0 && $arSelectedPeriod["Темп роста прибыли"]>$gain)){
						 $add = false;
						 }


						 if($add){

						if($cap && $arSelectedPeriod["Темп роста прибыли"]>0 && $arSelectedPeriod["Рентабельность собственного капитала"]>0) {
						$arr_begin[] = array(
													 round($arSelectedPeriod["Темп роста прибыли"],2),
													 round($arSelectedPeriod["Рентабельность собственного капитала"],2),//Рентабельность СК
													 $cap,
													 $item["ACTION"]["NAME"],
													 $item["ACTION"]["PROPS"]["PROP_SEKTOR_CODE"]
													 );
						  //Получение максимального значения для кругов
						  if($max_bubble<$cap) $max_bubble=$cap;
						  }//if add
						}
						}
			endforeach;

	 $prc = $arPresets[intval($_POST["f_preset"])]["LITTLE_PERCENT"];
	 $little_val = ($max_bubble/100)*$prc;


	 $result["arr_begin"]=$arr_begin;
	 //$result["arr_pages"]=$arr_begin;
	 $result["arr_bubbles_sizes"]=$little_val;
	 $result["arr_axis"]=$arPresets[intval($_POST["f_preset"])];
	 //end cache
    if ($isInvalid)
    {
        $cache->abortDataCache();
    }
    $cache->endDataCache($result);
}






			echo json_encode(array("status"=>"ok", "arr_begin"=>$result["arr_begin"], "arr_axis"=>$result["arr_axis"], "arr_bubbles_sizes"=>$result["arr_bubbles_sizes"]));

			//echo json_encode($arTest);