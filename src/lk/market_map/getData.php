<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$haveRadarAccess = checkPayRadar();
$haveUSARadarAccess = checkPayUSARadar();
$page = 1;
if($_REQUEST["page"]){
	$page = $_REQUEST["page"];
}

$exchange = 'moex';
if(isset($_REQUEST["exchange"]) && !empty(htmlspecialchars($_REQUEST["exchange"]))){
 $exchange = htmlspecialchars($_REQUEST["exchange"]);
}

if($exchange=='moex' || !$haveUSARadarAccess){
	$res = new ActionsMM();
	if(!$haveUSARadarAccess){
		$exchange = 'moex';
	}
} else if($exchange=='spbx'){
	$res = new ActionsMMUSA();
}

if($exchange=='moex'){
  $arPresets = array(
		0=>array("NAME"=>"Прогноз, Р/Е, дивиденды", "ALL_MIN_X"=>0, "ALL_MIN_Y"=>"", "ALL_MAX_X"=>20, "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"p/e", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Прогноз", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>"P/E", "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		1=>array("NAME"=>"Рост, недооценка, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"Недооценка", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Рост", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		2=>array("NAME"=>"Прогноз, EV/Ebitda, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"EV/Ebitda", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Прогноз", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		3=>array("NAME"=>"Рентабельность СК, рост, капитализация", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>150, "ALL_MAX_Y"=>130, "AXIS_X_NAME"=>"Рост", "AXIS_X_POSTFIX"=>"%", "AXIS_Y_NAME"=>"Рентабельность СК", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Капитализация", "EXT_PARAM_POSTFIX"=>" млн.р.", "SORT_FIELD"=>'Темп роста прибыли', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		4=>array("NAME"=>"EV/Ebitda, Net debt/Ebitda, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"Net debt/Ebitda", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"EV/Ebitda", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
  );
} else if($exchange=='spbx'){
  $arPresets = array(
		0=>array("NAME"=>"Прогноз, Р/Е, дивиденды", "ALL_MIN_X"=>0, "ALL_MIN_Y"=>"", "ALL_MAX_X"=>50, "ALL_MAX_Y"=>150, "AXIS_X_NAME"=>"p/e", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Прогноз", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>"P/E", "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		1=>array("NAME"=>"Рост, недооценка, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"Недооценка", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Рост", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		2=>array("NAME"=>"Прогноз, EV/Ebitda, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"EV/Ebitda", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"Прогноз", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		3=>array("NAME"=>"Рентабельность СК, рост, капитализация", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>250, "ALL_MAX_Y"=>150, "AXIS_X_NAME"=>"Рост", "AXIS_X_POSTFIX"=>"%", "AXIS_Y_NAME"=>"Рентабельность СК", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Капитализация", "EXT_PARAM_POSTFIX"=>" млн.$.", "SORT_FIELD"=>'Темп роста прибыли', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
		4=>array("NAME"=>"EV/Ebitda, Net debt/Ebitda, дивиденды", "ALL_MIN_X"=>"", "ALL_MIN_Y"=>"", "ALL_MAX_X"=>"", "ALL_MAX_Y"=>"", "AXIS_X_NAME"=>"Net debt/Ebitda", "AXIS_X_POSTFIX"=>"", "AXIS_Y_NAME"=>"EV/Ebitda", "AXIS_Y_POSTFIX"=>"%", "EXT_PARAM_NAME"=>"Дивиденды", "EXT_PARAM_POSTFIX"=>"%", "SORT_FIELD"=>'P/E', "SORT_ORDER"=>"ASC", "LITTLE_PERCENT"=>20 ),
  );
}


	 //Получаем массивы секторов для фильтрации
    $arSectors = $res->getSectorArrays();
	 $arCollationsSector  = $arSectors["SECTORS"];
	 $arCollationMirrorSector  = $arSectors["SECTORS_MIRROR"];
	 $arIndustryAxisNormalize = $res->getMarketMarNormalizeAxis();

   //Фильтр по периоду
   $filter_period = '';
	$use_current_period = false;
  if(isset($_REQUEST["f_period"]) && !empty(htmlspecialchars($_REQUEST["f_period"]))){
	$filter_period = $_REQUEST["f_period"];

	if($_REQUEST["f_period"] == $_REQUEST["f_period_current"]){
	    $use_current_period = true;
		 $curActQuart = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_QUARTAL");
  	    $curActYear = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_YEAR");
		 $filter_period = $curActQuart."-".$curActYear;
	}
  }

   $result = array("arr_begin"=>array(), "arr_axis"=>array(), "arr_inustry_axis"=>array());

	$arActionsTmp = array();

	 $arData = $res->getTableGraph();

        	$arSumm = array();
			$arr_bubbles_sizes = 0;

			 //Формируем массив для построения графика
          $arGraphData = $res->getBubbleGraphData($arData, $filter_period, $haveRadarAccess, $arCollationMirrorSector, $use_current_period);
			 $arr_begin = $arGraphData['arr_begin'];
			 $max_bubble = $arGraphData['max_bubble'];

	 $prc = $arPresets[intval($_POST["f_preset"])]["LITTLE_PERCENT"];
	 $little_val = ($max_bubble/100)*$prc;

	 $result["arr_begin"]=$arr_begin;
	 $result["arr_bubbles_sizes"]=$little_val;
	 $result["arr_axis"]=$arPresets[intval($_POST["f_preset"])];



	 $arIndustryAxis = array_key_exists(htmlspecialchars($_POST["f_industry"]),$arIndustryAxisNormalize[$exchange][intval($_POST["f_preset"])])?$arIndustryAxisNormalize[$exchange][intval($_POST["f_preset"])][htmlspecialchars($_POST["f_industry"])]:array();

	 $result["arr_inustry_axis"] = $arIndustryAxis;
	 unset($res, $arData, $arPresets, $arCollationsSector, $arCollationMirrorSector);


/*global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
$firephp = FirePHP::getInstance(true);
$firephp->fb($result,FirePHP::LOG);
      }*/

	 echo json_encode(array("status"=>"ok", "arr_begin"=>$result["arr_begin"], "arr_axis"=>$result["arr_axis"], "arr_industry_axis"=>$result["arr_inustry_axis"], "arr_bubbles_sizes"=>$result["arr_bubbles_sizes"]));