<?use Bitrix\Highloadblock as HL;?>
<?$arData = Actions::getIndustryList();

  $arCollationsSector = array(0=>array("name"=>"Банковский сектор", "code"=>"bank_sector"),
  										1=>array("name"=>"Высокотехнологический сектор", "code"=>"technology_sector"),
  										2=>array("name"=>"Горнодобывающий сектор", "code"=>"mining_sector"),
  										3=>array("name"=>"Машиностроительный сектор", "code"=>"machine_sector"),
  										4=>array("name"=>"Медийный сектор", "code"=>"media_sector"),
  										5=>array("name"=>"Металлургический сектор", "code"=>"metallurgical_sector"),
  										6=>array("name"=>"Нефтегазовый сектор", "code"=>"oil_sector"),
  										7=>array("name"=>"Потребительский сектор", "code"=>"consumer_sector"),
  										8=>array("name"=>"Ритейлерский сектор", "code"=>"retail_sector"),
  										9=>array("name"=>"Строительный сектор", "code"=>"construction_sector"),
  										10=>array("name"=>"Телекоммуникационный сектор", "code"=>"telecommunication_sector"),
  										11=>array("name"=>"Транспортный сектор", "code"=>"transport_sector"),
  										12=>array("name"=>"Финансовый сектор", "code"=>"financial_sector"),
  										13=>array("name"=>"Химический сектор", "code"=>"chemical_sector"),
  										14=>array("name"=>"Электросетевой сектор", "code"=>"electric_sector"),
  										15=>array("name"=>"Энергогенерирующий сектор", "code"=>"energy_gen_sector"),
  										16=>array("name"=>"Энергосбытовой сектор", "code"=>"energy_sell_sector"),
										);

//Формируем список периодов
	//текущий
	$kv = @intval( (date('n')-1) /4)+1;
	$year = date("Y");
	$show_kvartals = round(($year-2015)*4,0)-4;
	//$kv--;
	if($kv==0){
		$kv = 4;
		$year--;
	}
	$currKv = $kv;
	$currYear = $year;

	 //В качестве подмены текущего квартала получаем жестко заданные в админке квартал и год
	 $curActQuart = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_QUARTAL");
  	 $curActYear = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_YEAR");

	$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	$res = $entityClass::getList(array(
		"filter" => array(
			"UF_KVARTAL" => $kv,
			"UF_YEAR" => $year
		),
		"select" => array(
			"*"
		),
	));

/*	$res = $entityClass::getList(array(
		"filter" => array(
			"UF_KVARTAL" => $curActQuart,
			"UF_YEAR" => $curActYear
		),
		"select" => array(
			"*"
		),
	));*/


	if(!$res->getSelectedRowsCount()){
		$kv--;
		if($kv==0){
			$kv = 4;
			$year--;
		}
	}

/*	if($res->getSelectedRowsCount()){
		$kv= $curActQuart;
		$year = $curActYear;
	}*/

	$arKvartalPeriod = array(
		$currKv."-".$currYear => "Текущий",
	);
	 $arKvartalPeriod[$kv."-".$year] = $kv." квартал ".$year;

	while(count($arKvartalPeriod)<$show_kvartals){
		$kv--;
		if($kv==0){
			$kv = 4;
			$year--;
		}

/* 	$arQYOne = array($kv,$year);
	$arQYTwo = array($curActQuart,$curActYear);

	if($arQYOne[1]==$arQYTwo[1]){//если год одинаковый то сравниваем кварталы
	 if($arQYOne[0]==$arQYTwo[0]){$resQ=0;}
	 if($arQYOne[0]<$arQYTwo[0]){$resQ=1;}
	 if($arQYOne[0]>$arQYTwo[0]){$resQ=-1;}

	} else { //если годы разные - то сравниваем годы
	 if($arQYOne[1]<$arQYTwo[1]){$resQ=1;}
	 if($arQYOne[1]>$arQYTwo[1]){$resQ=-1;}
	}*/


	  //	if($resQ<=0){
		$arKvartalPeriod[$kv."-".$year] = $kv." квартал ".$year;
		//}
	}

?>
<form class="calculate_form" method="post">
   <!-- курс валют по ЦБ -->
   <input type="text" id="usd_currency" value="<?=getCBPrice("USD")?>" hidden />
   <input type="text" id="eur_currency" value="<?=getCBPrice("EUR")?>" hidden />
   <input type="text" id="arr_sectors" value="<?=json_encode($arCollationsSector)?>" hidden />
	<input id="table_sort_field" value="" type="hidden">
	<input id="table_sort_order" value="ASC" type="hidden">
   <!-- --- -->
  								<div class="infinite_container green">
                    <div class="calculate_line">
                      <div class="form_element">
                        <label>Выбор сектора для карты рынка:</label>

                        <select id="market_map_sector" class="inline_select">
                              <option value="all" data-code="all" <?=($_REQUEST["f_industry"]=='all'?'selected="selected"':'')?>>Все отрасли</option>
								<?foreach($arData as $n=>$item):?>
										<option value="<?=$arCollationsSector[$n]["name"]?>" data-code="<?=$arCollationsSector[$n]["code"]?>" <?=($_REQUEST["f_industry"]==$arCollationsSector[$n]["name"]?'selected="selected"':'')?>><?=$item?></option>
								<?endforeach?>
                            </select>
                      </div>
                      <div class="form_element">
                        <label>Выбрать тип карты рынка:</label>

                        <select id="market_map_preset" class="inline_select">
                              <option value="0">Прогноз, Р/Е, дивиденды</option>
                              <!--<option value="1">Рост, недооценка, дивиденды</option>-->  <!-- Недооценки пока не существует, сделаем позже -->
                              <!--<option value="2">Прогноз, EV/Ebitda, дивиденды</option>-->
                              <option value="3">Рентабельность СК, рост, капитализация</option>
                              <!--<option value="4">EV/Ebitda, Net debt/Ebitda, дивиденды</option>-->
                        </select>
                      </div>

                      <div class="form_element">
                        <label>Период:</label>
								<!--<input type="hidden" id="f_period_current" value="<?=$curActQuart."-".$curActYear?>"/>-->
								<input type="hidden" id="f_period_current" value="<?=$currKv."-".$currYear?>"/>
                        <select id="market_map_period" class="inline_select">

									<?foreach($arKvartalPeriod as $val=>$name):?>
										<option value="<?=$val?>" <?=($_REQUEST["f_period"]==$val?'selected="selected"':'')?>><?=$name?></option>
									<?endforeach?>
                        </select>
                      </div>
                    </div>
                  </div>
</form>

<div id="debug"></div>