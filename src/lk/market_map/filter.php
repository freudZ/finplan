<?use Bitrix\Highloadblock as HL;?>
<? if($_REQUEST["ajax"]=="y") {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	}
$haveUSARadarAccess = checkPayUSARadar();
$exchange = 'moex';
if(isset($_REQUEST["exchange"]) && !empty(htmlspecialchars($_REQUEST["exchange"]))){
 $exchange = htmlspecialchars($_REQUEST["exchange"]);
}

if($exchange=='moex' || !$haveUSARadarAccess){
	$res = new ActionsMM();
	$hlCompanyData = 14;
	if(!$haveUSARadarAccess){
		$exchange = 'moex';
	}
} else if($exchange=='spbx'){
	$res = new ActionsMMUSA();
	$hlCompanyData = 22;
}

	//Получаем массивы секторов для фильтрации
	$arSectors = $res->getSectorArrays();
	$arCollationsSector  = $arSectors["SECTORS"];
	$arCollationMirrorSector  = $arSectors["SECTORS_MIRROR"];
	$arData = $arCollationsSector;

   //Формируем список периодов
  	$getKvartalPeriod = $res->getPeriodList($hlCompanyData);
	$arKvartalPeriod = $getKvartalPeriod["LIST"];
  	$currKv = $getKvartalPeriod["CURR_KVARTAL"];
	$currYear = $getKvartalPeriod["CURR_YEAR"];
	unset($res, $hlCompanyData);
?>
<form class="calculate_form" method="post">
   <!-- курс валют по ЦБ -->
   <input type="text" id="usd_currency" value="<?=getCBPrice("USD")?>" hidden />
   <input type="text" id="eur_currency" value="<?=getCBPrice("EUR")?>" hidden />
   <input type="text" id="arr_sectors" value="<?=json_encode($arCollationsSector)?>" hidden />
	<input id="table_sort_field" value="" type="hidden">
	<input id="table_sort_order" value="ASC" type="hidden">
   <!-- --- -->
  	<div class="infinite_container green">
                    <div class="calculate_line">

                      <div class="form_element">
                        <label>Биржа:</label>
                        <select id="exchange" class="inline_select">
                              <option value="moex" data-code="moex" <?=($_REQUEST["exchange"]=='moex'?'selected="selected"':'')?>>Мосбиржа</option>

										<option value="spbx" data-code="spbx" <?if(!$haveUSARadarAccess):?>disabled<?endif;?> <?=($_REQUEST["exchange"]=='spbx'?'selected="selected"':'')?>>СПб биржа <?if(!$haveUSARadarAccess):?>- доступно на тарифе +США<?endif;?></option>

                        </select>
                      </div>

                      <div class="form_element">
                        <label>Выбор сектора для карты рынка:</label>
                        <select id="market_map_sector" class="inline_select">
                              <option value="all" data-code="all" <?=($_REQUEST["f_industry"]=='all'?'selected="selected"':'')?>>Все отрасли</option>
									<?foreach($arCollationsSector as $n=>$item):?>
											<option value="<?=$item["name"]?>" data-code="<?=$item["code"]?>" <?=($_REQUEST["f_industry"]==$item["name"]?'selected="selected"':'')?>><?=$item["name"]?></option>
									<?endforeach?>
                        </select>
                      </div>
                      <div class="form_element">
                        <label>Выбрать тип карты рынка:</label>
                        <select id="market_map_preset" class="inline_select">
                              <option value="0">Прогноз, Р/Е, дивиденды</option>
                              <!--<option value="1">Рост, недооценка, дивиденды</option>-->  <!-- Недооценки пока не существует, сделаем позже -->
                              <!--<option value="2">Прогноз, EV/Ebitda, дивиденды</option>-->
                              <option value="3">Рентабельность СК, рост, капитализация</option>
                              <!--<option value="4">EV/Ebitda, Net debt/Ebitda, дивиденды</option>-->
                        </select>
                      </div>

                      <div class="form_element">
                        <label>Период:</label>
								<input type="hidden" id="f_period_current" value="<?=$currKv."-".$currYear?>"/>
                        <select id="market_map_period" class="inline_select">
									<?foreach($arKvartalPeriod as $val=>$name):?>
										<option value="<?=$val?>" <?=($_REQUEST["f_period"]==$val?'selected="selected"':'')?>><?=$name?></option>
									<?endforeach?>
                        </select>
                      </div>
                    </div>
	</div>
</form>

<div id="debug"></div>