<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<div class="content-main-lk-param">
    <div class="content-main-lk-param-wrapper">
    </div>
</div>
<div class="content-main-wrapper">
    <div class="content-main-lk-steplist-fixed">
        <div class="content-main-lk-steplist-item content-main-lk-steplist-item-filled">
            1 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            2 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            3 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            4 шаг
        </div>
        <div class="content-main-lk-steplist-item">
            5 шаг
        </div>
    </div>
    <div class="content-main-lk-profile">
       <?
       // параметры
       $user_id=$USER->GetID();
       $iSize = 150;
       // логика
       if(isset($user_id) && $user_id!='')
        {
           // получаем объект класса CDbResult
          $rsUser = $USER->GetByID($user_id);
          // считаем кол-во выбранных записей
          $rows_q = $rsUser -> SelectedRowsCount();
           if($rows_q > 0)
          {
              $arUser = $rsUser->Fetch();
              // создаем тег изображения - фото пользователя
             if (intval($arUser["PERSONAL_PHOTO"]) > 0)
             {
               $imageFile = CFile::GetFileArray($arUser["PERSONAL_PHOTO"]);
                 if ($imageFile !== false)
                   {
                      $arFileTmp = CFile::ResizeImageGet(
                         $imageFile,
                         array("width" => $iSize, "height" => $iSize),
                         BX_RESIZE_IMAGE_PROPORTIONAL,
                         false
                      );
                      // в переменной хранится готовый тег изображения
                      $imageImg = CFile::ShowImage($arFileTmp["src"], $iSize, $iSize, "border='0'", "");
                   }
             }      
             //формируем $arResult
             $arResult=array();
             // выбираем Данные: имя, фамилия, e-mail, профессию, персональную страницу
             // Отчество, id фото
             $arResult["User"] = array(
               "NAME" => $arUser["NAME"],
               "LOGIN" => $arUser["LOGIN"],
              "SECOND_NAME" => $arUser["SECOND_NAME"],
              "LAST_NAME" => $arUser["LAST_NAME"],
              "EMAIL" => $arUser["EMAIL"],
              "PERSONAL_PROFESSION" => $arUser["PERSONAL_PROFESSION"],
              "PERSONAL_WWW" => $arUser["PERSONAL_WWW"],
              "PHOTO_IMGTAG" => $imageImg,
              );
            ?>
                <h1 class="blog-h1">Личный кабинет</h1>
                <div id="auth_info"> 
                    <div class="auth_photo"><?=$arResult["User"]["PHOTO_IMGTAG"];?></div> 
                    <h2><?=$arResult["User"]["NAME"]." ".$arResult["User"]["SECOND_NAME"]." ".$arResult["User"]["LAST_NAME"];?></h2> 
                    <p><?=$arResult["User"]["PERSONAL_PROFESSION"]?></p> 
                  <p>E-mail: <b><?=$arResult["User"]["LOGIN"]?></b></p> 
                    <p><?=$arResult["User"]["PERSONAL_WWW"]?></p>
                </div>
            <?

          }
          else
           { 
             ShowError("Профиля пользователя с указанным идентификатором не существует!");
           }
       }
       else
        {
          ?>
            <h1 class="blog-h1">Личный кабинет</h1>
                <div id="auth_info"> 
                    <p>Войдите на сайт, чтобы просмотреть информацию в вашем личном кабинете.</p>
                </div>
          <?
        }
     ?>      
</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>