<? 
  require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
  
  if($_GET['PAGEN_1']) {
    $pagenum = $_GET['PAGEN_1'];
    } else {
    $pagenum = 1;
  }  
  $start_count = ($pagenum-1)*20;
  
  if($_GET['sortfield']) {
    $sortfield = $_GET['sortfield'];
    } else {
    $sortfield = 'rate';
  }
  
  if($_GET['sorttable']) {
    $sorttable = $_GET['sorttable'];
    } else {
    $sorttable = 'a_lk_deposits';
  }
  
  if($_GET['sort']) {
    $sort = $_GET['sort'];
    } else {
    $sort = 'desc';
  }
  
  if($_GET['max_amount']) {
    $max_amount = $_GET['max_amount'];
    } else {
    $max_amount = 150000;
  }
  
  if($_GET['max_period']) {
    if($max_period==37) {
      $max_period = 1000000;
      } else {
      $max_period = $_GET['max_period']*30+10;
    }
    } else {
    $max_period = 24*30+10;
  }
  
  if($_GET['f_pay_mon'] || $_GET['f_pay_sec'] || $_GET['f_pay_half'] || $_GET['f_pay_year'] || $_GET['f_pay_end']) 
    $f_pay = ' AND (';
  elseif($_GET['f_pay_other']) 
    $f_pay = '';
  else 
    $f_pay = 'no-payments';
  
  $f_pay_and = '';
  
  if($_GET['f_pay_mon']) {
    $f_pay .= " `pay_mon`=".$_GET['f_pay_mon']." OR";
    } else {
    $f_pay_and .= " AND `pay_mon`=0 ";
  }
  
  if($_GET['f_pay_sec']) {
    $f_pay .= " `pay_sec`=".$_GET['f_pay_sec']." OR";
    } else {
    $f_pay_and .= " AND `pay_sec`=0 ";
  }
  
  if($_GET['f_pay_half']) {
    $f_pay .= " `pay_half`=".$_GET['f_pay_half']." OR";
    } else {
    $f_pay_and .= " AND `pay_half`=0 ";
  }
  
  if($_GET['f_pay_year']) {
    $f_pay .= " `pay_year`=".$_GET['f_pay_year']." OR";
    } else {
    $f_pay_and .= " AND `pay_year`=0 ";
  }
  
  if($_GET['f_pay_end']) {
    $f_pay .= " `pay_end`=".$_GET['f_pay_end']." OR";
    } else {
    $f_pay_and .= " AND `pay_end`=0 ";
  }
 
  if($_GET['f_pay_mon'] || $_GET['f_pay_sec'] || $_GET['f_pay_half'] || $_GET['f_pay_year'] || $_GET['f_pay_end']) {
    $f_pay = substr($f_pay, 0, -2); 
    $f_pay .= ') ';
  }

  if(isset($_GET['f_capital'])) {
    $f_capital = ($_GET['f_capital']=='2') ? "" : "AND `capital`=".$_GET['f_capital'] ;
    } else {
    $f_capital = '';
  }
  
  if(isset($_GET['f_refill'])) {
    $f_refill = ($_GET['f_refill']=='2') ? "" : "AND `refill`=".$_GET['f_refill'] ;
    } else {
    $f_refill = '';
  }
  
  if(isset($_GET['f_refill_and'])) {
    $f_refill_and = ($_GET['f_refill_and']=='2') ? "" : "AND `refill_and`=".$_GET['f_refill_and'] ;
    } else {
    $f_refill_and = '';
  }
  
  if(isset($_GET['f_refill_and']) && isset($_GET['f_refill']) && $_GET['f_refill_and']=='1' && $_GET['f_refill']=='1') {
    $f_refill = "AND (`refill`=".$_GET['f_refill']." OR ";
    $f_refill_and = " `refill_and`=".$_GET['f_refill_and']." ) ";
  }
  
  if(isset($_GET['f_bank_rating'])) {
    $f_bank_rating = ($_GET['f_bank_rating']=='') ? "" : "AND `a_lk_banks`.`rating`<=".$_GET['f_bank_rating'] ;
    } else {
    $f_bank_rating = '';
  }
  
  if(isset($_GET['f_bank_title'])) {
    $f_bank_title = ($_GET['f_bank_title']=='') ? "" : "AND `a_lk_banks`.`title` LIKE '%".urldecode($_GET['f_bank_title'])."%' ";
    } else {
    $f_bank_title = '';
  }
  
  if($f_pay!='no-payments') {
    global $DB;
    $time_filter = 1449249537;
    
    $query = "SELECT `a_lk_deposits`.*, `a_lk_banks`.`title` as `bank_title`, `a_lk_banks`.`rating` as `bank_rating` 
    FROM `a_lk_deposits` 
    INNER JOIN `a_lk_banks` 
    ON `a_lk_deposits`.`id_bank` = `a_lk_banks`.`id` 
    WHERE 
    `a_lk_deposits`.`parse_date`>=".$time_filter." 
    AND `amount`<=".$max_amount." 
    AND `period` <= ".$max_period." 
    ".$f_pay." ".$f_pay_and." ".$f_capital." ".$f_refill." ".$f_refill_and." ".$f_bank_rating." ".$f_bank_title." 
    ORDER BY `".$sorttable."`.`".$sortfield."` ".$sort." 
    LIMIT ".$start_count.",20 ";
    
    $results = $DB->Query($query);
    $deposits_array=array();
    
    while ($row = $results->Fetch())
    {
      array_push($deposits_array, $row);
    }
  }
?>

<?
  foreach($deposits_array as $deposit) {
    $refill_img = '';
    $capital_img = '';
    $pay_img = '';
    if($deposit['refill_and']=='1') {
      $refill_img = '<img title="Возможно пополнение и частичное снятие" src="'.SITE_TEMPLATE_PATH.'/images/dep_refill_and.png">';
      } elseif($deposit['refill']=='1') {
      $refill_img = '<img title="Возможно пополнение" src="'.SITE_TEMPLATE_PATH.'/images/dep_refill.png">';
    }
    
    if($deposit['capital']=='1') {
      $capital_img = '<img title="Капитализация процентов" src="'.SITE_TEMPLATE_PATH.'/images/dep_capital.png">';
    }
    
    if($deposit['pay_mon']=='1') {
      $pay_img = '<img title="Выплата процентов ежемесячно" src="'.SITE_TEMPLATE_PATH.'/images/dep_one.png">';
      } elseif($deposit['pay_sec']=='1') {
      $pay_img = '<img title="Выплата процентов ежеквартально" src="'.SITE_TEMPLATE_PATH.'/images/dep_sec.png">';
      } elseif($deposit['pay_half']=='1') {
      $pay_img = '<img title="Выплата процентов раз в полгода" src="'.SITE_TEMPLATE_PATH.'/images/dep_half.png">';
      } elseif($deposit['pay_year']=='1') {
      $pay_img = '<img title="Выплата процентов ежегодно" src="'.SITE_TEMPLATE_PATH.'/images/dep_year.png">';
      } elseif($deposit['pay_end']=='1') {
      $pay_img = '<img title="Выплата процентов в конце срока" src="'.SITE_TEMPLATE_PATH.'/images/dep_end.png">';
    }
    
  ?>
  
  <tr id="content-main-step2-table-tr-<?=$deposit['id']?>" onclick="">
    <input type="hidden" class="content-main-step2-table-td-col1-pay_mon" name="content-main-step2-table-td-col1-pay_mon" value="<?=$deposit['pay_mon']?>">
    <input type="hidden" class="content-main-step2-table-td-col1-pay_sec" name="content-main-step2-table-td-col1-pay_sec" value="<?=$deposit['pay_sec']?>">
    <input type="hidden" class="content-main-step2-table-td-col1-pay_half" name="content-main-step2-table-td-col1-pay_half" value="<?=$deposit['pay_half']?>">
    <input type="hidden" class="content-main-step2-table-td-col1-pay_year" name="content-main-step2-table-td-col1-pay_year" value="<?=$deposit['pay_year']?>">
    <input type="hidden" class="content-main-step2-table-td-col1-pay_end" name="content-main-step2-table-td-col1-pay_end" value="<?=$deposit['pay_end']?>">
    <input type="hidden" class="content-main-step2-table-td-col1-capital" name="content-main-step2-table-td-col1-capital" value="<?=$deposit['capital']?>">
    <input type="hidden" class="content-main-step2-table-td-col1-refill" name="content-main-step2-table-td-col1-refill" value="<?=$deposit['refill']?>">
    <input type="hidden" class="content-main-step2-table-td-col1-refill_and" name="content-main-step2-table-td-col1-refill_and" value="<?=$deposit['refill_and']?>">
    <td class="content-main-step2-table-td-col1 content-main-step2-table-td-focus"><?=$deposit['bank_title']?></td>
    <td class="content-main-step2-table-td-col2 content-main-step2-table-td-focus"><?=$deposit['title']?><div class="content-main-step2-table-td-col2-params"><?=$refill_img;?><?=$capital_img;?><?=$pay_img;?></div></td>
    <td class="content-main-step2-table-td-col3 content-main-step2-table-td-focus"><?=$deposit['rate']?></td>
    <td class="content-main-step2-table-td-col4 content-main-step2-table-td-focus"><?=$deposit['period']?></td>
    <td class="content-main-step2-table-td-col5 content-main-step2-table-td-focus"><?=$deposit['bank_rating']?></td>
    <td class="content-main-step2-table-td-col6 content-main-step2-table-td-focus"><?=$deposit['amount']?></td>
    <td class="content-main-step2-table-td-col7"></td>
  </tr>
  <?
  }
?>