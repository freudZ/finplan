<?
    define("CALCULATOR_PAGE", "Y");
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

    use \Bitrix\Main\Page\Asset;

    CModule::IncludeModule("iblock");
    $APPLICATION->SetTitle("Калькулятор ИИС");

    $image_src_for_social = SITE_TEMPLATE_PATH . '/img/iis_calc.png';
    $lj_event = htmlentities('<a href="http://' . $_SERVER['HTTP_HOST'] . $APPLICATION->GetCurUri() . '">Онлайн калькулятор доходности</a><img src="http://'.$_SERVER['HTTP_HOST'].$image_src_for_social.'">');
    Asset::getInstance()->addString('<meta content="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '" property="og:image">', true);
    Asset::getInstance()->addString('<link rel="image_src" href="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '" />', true);

    //статьи
    $obCache = new CPHPCache();
    $cacheLifetime = 86400*365*10;
    $cacheID = 'random_articles_for_calculator';
    $cachePath = '/random_articles/';
    $articles = [];

    if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
        $articles = $obCache->GetVars();
    }

    if(!count($articles)) {
        $obCache->StartDataCache();
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
        $arFilter = Array("IBLOCK_ID"=>5, "!SECTION_ID"=>array(5, 17), "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array("RAND"=>"asc"), $arFilter, false, array("nPageSize"=>3), $arSelect);

        while ($ob = $res->GetNext()){
            $articles[] = $ob;
        }

        $obCache->EndDataCache($articles);
    }
?>

<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner">
                <?$APPLICATION->IncludeFile("/blog/sect_extended_banner.php")?>
                <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
                <!-- *****/КАРКАС***** -->
                <div class="calc_iis__outer" style="margin-bottom: 0">
                    <div class="calc_iis__top">
                        <span class="icon icon-calculator"></span>
                        <h1 class="calc_iis__title">Калькулятор ИИС</h1>
                        <p class="description">Индивидуальные инвестиционные счета (ИИС) уже прочно вошли в жизнь инвесторов и позволяют получать дополнительный доход к инвестированию в виде государственных льгот по возврату НДФЛ.</p>
                    </div>
                    <div class="calc_iis__link">
                        <div class="calc_iis__link__inner">
                            <p>Подробнее узнать об этих счетах можно в нашей статье <br /><span class="big">«Что такое ИИС?»</span></p>
                        </div>
                        <a href="https://fin-plan.org/blog/investitsii/chto-takoe-iis/" target="_blank" class="button button_gray">Читать</a>
                    </div>
                    <p class="t20">Если коротко, то ИИС позволяет получать налоговый вычет по НДФЛ в размере 13% от суммы инвестиций.</p>
                    <p>К сожалению данный вычет не может быть больше суммы уплаченного Вами в расчетном году НДФЛ, поэтому для корректного расчета доходов от ИИС необходимо учитывать размер официальной заработной платы. Данный калькулятор покажет Вам все данные расчеты на 3 года вперед.</p>
                    <div class="calc_iis__fields">
                        <div class="line">
                            <div class="line__inner">
                                <span class="number">1</span>
                                <div class="text">
                                    <p class="text_inner">Введите размер пополнения ИИС в год <span class="iis_tooltip_tgl" title="Это сумма ежегодного пополнения ИИС. Если Вы не планируете пополнять ИИС каждый год, тогда нажмите «детализировано по годам» и скорректируйте суммы пополнения детализировано по годам.">?</span></p>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" id="detailInput">
                                    <label title="Снимите галочку детализации прежде чем вводить" for="detailInput"></label>
                                    <span id="iis_detail_btn" class="more" data-toggle="modal" data-target="#iis_detail_modal">Ввести детализировано по годам.</span>
                                </div>
                                <input id="input_iis" type="text" value="400000" />
                            </div>
                        </div>
                        <div class="line">
                            <div class="line__inner">
                                <span class="number">2</span>
                                <div class="text">
                                    <p class="text_inner">Официальная ежемесячная заработная плата, с которой платится НДФЛ <span class="iis_tooltip_tgl" title="Это сумма ежемесячной официальной заработной платы, с которой платится НДФЛ. Данная сумма нужна для корректно расчета налогового вычета по ИИС. Если у Вас несколько мест работы, то для корректного учета следует написать суммарный официальный заработок. Пенсии, стипендии, предпринимательский доход, а также дивиденды в данный расчет не включаются.">?</span></p>
                                </div>

                                <input id="input_salary" type="text" value="50000" />
                            </div>
                        </div>
                        <div class="line">
                            <div class="line__inner">
                                <span class="number">3</span>
                                <div class="text">
                                    <p class="text_inner">Введите ставку доходности инвестиций, в % <span class="iis_tooltip_tgl" title="Эта ставка позволит Вам увидеть полный результат инвестиционного дохода как от ИИС, так и от инвестиционных активов. При вводе ставки доходности следует ориентироваться на то, что чем выше доходность – тем выше риски. Безрисковой ставкой доходности на сегодняшний день можно считать доходность облигаций федерального займа на уровне 8%, 15% - может дать защищенный инвестиционный портфель из акций и облигаций. Более высокий уровень доходности конечно возможен, но риски такого инвестиционного портфеля будут уже более высоки, а стабильность доходности ниже">?</span></p>
                                </div>

                                <input id="input_invest_rate" type="text" value="10" />
                            </div>
                        </div>
                    </div>
                    <ul class="arr_list">
                        <li>
                            <span class="icon icon-linear_arr_down"></span>
                            <span class="text">
                            <span class="text_inner">При обозначенных исходных данных Ваш портфель за <span id="years_txt"></span> лет вырастет <br/><span class="bold">до <span id="val_max_output"></span> тыс. руб.</span></span></span>
                        </li>

                        <li>
                            <span class="icon icon-linear_arr_down"></span>
                            <span class="text">
                            <span class="text_inner"><span class="bold">Из которых <span id="iis_output"></span> тыс. руб.</span> <br/>Вы получите в качестве налогового вычета</span></span>
                        </li>

                        <li>
                            <span class="icon icon-linear_arr_right"></span>
                            <span class="text">
                            <span class="text_inner"><span class="bold">и <span id="passive_output"></span> тыс. руб.</span> <br/>будет Ваш инвестиционный доход.</span></span>
                        </li>
                    </ul>
                    <div class="chart__outer"></div>
                    <div class="custom_collapse">
                        <div class="custom_collapse_elem opened">
                            <div class="custom_collapse_elem_head collapse_btn">Дополнительные настройки <span class="icon icon-arr_down"></span></div>

                            <div class="custom_collapse_elem_body">
                                <div class="inner">
                                    <div class="chart_settings-iis">
                                        <div class="row">
                                            <div class="col-left col col-lg-6">
                                                <input id="input_period" type="text" value="30" />
                                                <label><span>Период <br />на графике (лет)</span></label>
                                            </div>

                                            <div class="col-right col col-lg-6">
                                                <input id="input_taxes" type="text" value="13" />
                                                <label><span>Учитывать налоги на прирост капитала в расчетах по ставке (%)</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="t20">Данный график лишь первый набросок Вашего финансового плана, для его реализации нужны конкретные финансовые инструменты и понимание инвестиционного процесса.</p><br />
                    <p class="t20">Чтобы узнать, во что инвестировать на ИИС, приглашаем вас на бесплатный вебинар по инвестированию:</p>

                    <div class="calc_iis__link">
                        <div class="calc_iis__link__inner">
                            <p>Записаться на вебинар <br /><span class="big">«Во что инвестировать на ИИС»</span></p>
                        </div>

                        <a href="javascript:void(0)" class="button button_light_green" data-toggle="modal" data-target="#popup_master_subscribe">Записаться</a>
                    </div>
                    <div class="company_accordion_outer" style="margin-bottom: 0">
                        <div class="custom_collapse">
                            <div class="custom_collapse_elem opened">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title collapse_btn" style="border-bottom:0">Данные по периодам (на конец периода)<span class="icon icon-arr_down"></span></h2>
                                        </div>
                                        <div class="custom_collapse_elem_body">
                                            <div class="custom_collapse_elem_body_inner">
                                                <div class="tab-content">
                                                    <table id="iisDataTable" class="footable-table"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="article_share">
                        <p>Рассказать другим</p>
                        <ul class="share_list">
                            <li>
                                <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST']?>/lk/calculator/&amp;title=Онлайн калькулятор доходности&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST']?>/lk/calculator/&amp;title=Онлайн калькулятор доходности&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST']?>/lk/calculator/&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST']?>/lk/calculator/&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=Онлайн калькулятор доходности', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=Онлайн калькулятор доходности"></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/share?text=Онлайн калькулятор доходности" onclick="window.open('https://twitter.com/share?text=Онлайн калькулятор доходности','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- *****КАРКАС***** -->

                <div class="other_articles_list">
                    <div class="other_articles_list_top">
                        <p>Рекомендуемые к прочтению статьи:</p>

                        <a class="link_arrow" href="/blog/">Все статьи</a>
                    </div>

                    <div class="articles_list row">
                        <? foreach($articles as $article): ?>
                            <div class="articles_list_element col-xs-6 col-lg-4">
                                <div class="articles_list_element_inner">
                                    <div class="articles_list_element_img">
                                        <div class="articles_list_element_img_inner">
                                            <a href="<?=$article['DETAIL_PAGE_URL']?>" style="background-image:url(<?=CFile::GetPath($article["PREVIEW_PICTURE"])?>);"></a>
                                        </div>
                                    </div>
                                    <div class="articles_list_element_text">
                                        <p class="articles_list_element_title"><a href="<?=$article['DETAIL_PAGE_URL']?>"><?=$article['NAME']?></a></p>
                                        <p  class="articles_list_element_description"><?=$article["PREVIEW_TEXT"]?></p>

                                        <p class="articles_list_element_date">
                                            <?=FormatDate(array(
                                                "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                "today" => "today",              // выведет "сегодня", если дата текущий день
                                                "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                            ), MakeTimeStamp($article["DATE_ACTIVE_FROM"]), time()
                                            );?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
                <div class="comment_outer">
                    <div class="content-main-post-item-comments">
                        <?$APPLICATION->IncludeComponent("cackle.comments", "cackle_default", Array(
                        ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "right_sidebar",
                "EDIT_TEMPLATE" => "",
                "COMPONENT_TEMPLATE" => ".default",
                "AREA_FILE_RECURSIVE" => "Y"
            ),
            false
        );?>
    </div>
</div>
<div class="modal modal_black fade" id="iis_detail_modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title uppercase">Введите размер пополнения ИИС в год</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="modal-body_inner">
                    <div id="iis_detail_modal_list"></div>

                    <div class="text-center mt30">
                        <span id="iis_detail_modal_confirm" class="button" data-dismiss="modal" aria-hidden="true">Подтвердить</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");