<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<div class="content-main-lk-param">
    <div class="content-main-lk-param-wrapper">
        <div class="content-main-lk-param-step-counter pull-left">
            Шаг 1
        </div>
        <div class="content-main-lk-param-step-title">
            Заставьте свободные деньги работать
        </div>
    </div>
</div>
<div class="content-main-step1-content-container clearfix">
    <div class="content-main-step1-content-wrapper">
        <div class="content-main-step1-h1-container">
            <h1 class="content-main-step1-h1">Составляем финансовый план</h1>
            <p>Для точного составления вашего плана, пройдите 5 шагов заполняя все графы. Это необходимо для более точного анализа и вывода результата, исходя из ваших данных.</p>
        </div>
        <div class="content-main-step1-goals-container">
            <div class="content-main-step1-goals-wrapper clearfix">
                <div class="content-main-step1-goals-buttons pull-left">
                    <div class="content-main-step1-goals-button-title">
                        ВЫБЕРИТЕ РАЗУМНЫЕ ФИНАНСОВЫЕ ЦЕЛИ
                    </div>
                    <div class="content-main-step1-goals-button">
                        <div class="loginModal-button-login">
                            <a href="#" data-toggle="modal" data-target="#addGoalModal">Добавить цель</a>
                        </div>
                    </div>
                </div>
                <div class="content-main-step1-goals-list JS-content-main-step1-goals-list pull-right">
                    <img class="content-main-step1-goals-list__demo" src="<?=SITE_TEMPLATE_PATH?>/images/step-1-goals.png">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-main-investment_sum_time content-main-step1-investment_sum_time clearfix">
    <div class="content-main-investment_sum_time-wrapper">
        <div class="content-main-investment_sum content-main-step1-investment_sum pull-left">
            <div class="content-main-investment_sum-title content-main-step1-investment_sum-title">
                Какую сумму Вы готовы инвестировать?
            </div>
            <div class="content-main-investment_sum-slider" id="step-1-sum-slider">
            </div>
            <span class="glyphicon glyphicon-circle-arrow-right"></span><input type="text" id="step-1-sum-slider-amount" maxlength="8">
        </div>
        <div class="content-main-investment_time content-main-step1-investment_time pull-right">
            <div class="content-main-investment_time-title content-main-step1-investment_time-title">
                Введите сумму ежемесячного пополнения
            </div>
            <div class="content-main-investment_time-slider" id="step-1-add-slider">
            </div>
            <span class="glyphicon glyphicon-circle-arrow-right"></span><input type="text" id="step-1-add-slider-amount" maxlength="8">
        </div>
    </div>
</div>
<div class="content-main-wrapper">
    <div class="content-main-lk-steplist-fixed">
        <div class="content-main-lk-steplist-item content-main-lk-steplist-item-filled">
            1 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            2 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            3 Шаг
        </div>
        <div class="content-main-lk-steplist-item">
            4 шаг
        </div>
        <div class="content-main-lk-steplist-item">
            5 шаг
        </div>
    </div>
    <div class="content-main-next-step-container clearfix">
        <div class="content-main-next-step-button pull-right">
            <a href="/lk/step-2">Далее <span class="glyphicon glyphicon-menu-right" style="top: 3px; display: inline;"></span></a>
        </div>
        <div class="content-main-next-step-title pull-right">
            Спасибо! все графы первого шага заполнены. Идем дальше?
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>