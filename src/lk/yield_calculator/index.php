<?
    define("CALCULATOR_PAGE", "Y");
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

    use \Bitrix\Main\Page\Asset;

    CModule::IncludeModule("iblock");
    $APPLICATION->SetTitle("Калькулятор доходности");
    $image_src_for_social = SITE_TEMPLATE_PATH . '/img/yield_calc.png';
    $lj_event = htmlentities('<a href="http://' . $_SERVER['HTTP_HOST'] . $APPLICATION->GetCurUri() . '">Онлайн калькулятор доходности</a><img src="http://'.$_SERVER['HTTP_HOST'].$image_src_for_social.'">');
    Asset::getInstance()->addString('<meta content="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '" property="og:image">', true);
    Asset::getInstance()->addString('<link rel="image_src" href="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '" />', true);
    
    //статьи
    $obCache = new CPHPCache();
    $cacheLifetime = 86400*365*10;
    $cacheID = 'random_articles_for_calculator';
    $cachePath = '/random_articles/';
    $articles = [];

    if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
        $articles = $obCache->GetVars();
    }

    if(!count($articles)) {
        $obCache->StartDataCache();
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
        $arFilter = Array("IBLOCK_ID"=>5, "!SECTION_ID"=>array(5, 17), "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array("RAND"=>"asc"), $arFilter, false, array("nPageSize"=>3), $arSelect);

        while ($ob = $res->GetNext()){
            $articles[] = $ob;
        }

        $obCache->EndDataCache($articles);
    }
?>

<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner">
                <?$APPLICATION->IncludeFile("/blog/sect_extended_banner.php")?>
                <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
<!--                --><?//$APPLICATION->SetViewTarget('blog_article_share');?>
                    <div class="main_sidemenu_element share_element active">
                        <span class="main_sidemenu_link icon icon-share" title="Поделиться статьей"></span>

                        <div class="main_sidemenu_inner">
                            <ul class="share_list">
                                <li>
                                    <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                                </li>
                                <li>
                                    <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                                </li>
                                <li>
                                    <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>"></a>
                                </li>
                                <!-- <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li> -->
                                <li><a href="https://twitter.com/share?text=<?=$arResult["NAME"]?>" onclick="window.open('https://twitter.com/share?text=<?=$arResult["NAME"]?>&url=http://<?=$_SERVER["HTTP_HOST"]?><?=$_SERVER["REQUEST_URI"]?>','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a></li>
                                <li>
                                    <?$inFavorite = checkInFavorite($arResult["ID"], "blog")?>
                                    <span<?if(!$USER->IsAuthorized()):?> data-toggle="modal" data-target="#popup_login"<?endif?> class="to_favorites button<?if($inFavorite):?> active<?endif;?>" data-id="<?=$arResult["ID"]?>">
                                            <span class="icon icon-star_empty"></span> <span class="text"><?if($inFavorite):?>В избранном<?else:?>В избранное<?endif;?></span>
                                        </span>
                                </li>
                            </ul>
                        </div>
                    </div>
<!--                --><?//$APPLICATION->EndViewTarget();?>
                <!-- *****/КАРКАС***** -->
                <div class="calc_iis__outer" style="margin-bottom: 0">
                    <div class="calc_iis__top">
                        <span class="icon icon-calculator"></span>
                        <h1 class="calc_iis__title">Онлайн калькулятор доходности</h1>
                        <p class="description">Данный калькулятор доходности покажет Вам чего можно добиться с помощью инвестирования.</p>
                        <p class="t14">Вы можете ввести размер стартового капитала, а также размер ежемесячных пополнений, чтобы моментально смоделировать как будут расти Ваши накопления с учетом той или иной ставки доходности. Данное моделирование поможет Вам понять, что стоимость денег сегодня и завтра совсем разная. Что сэкономленный в день 100 рублей при грамотном инвестировании и сбережении средств через несколько лет могут превратиться в солидный счет. Понимание данного процесса поможет Вам поставить яркие финансовые цели и начать действовать.</p>
                    </div>
                    <div class="calc_iis__fields">
                        <div class="line">
                            <div class="line__inner">
                                <span class="number">1</span>
                                <div class="text">
                                    <p class="text_inner">Введите размер стартового капитала <span class="iis_tooltip_tgl" title="Это стартовая сумма Ваших инвестиций, она также может быть равна 0, если у Вас еще нет накоплений.">?</span></p>
                                </div>

                                <input id="input_yield_start" type="text" value="0" />
                            </div>
                        </div>

                        <div class="line">
                            <div class="line__inner">
                                <span class="number">2</span>
                                <div class="text">
                                    <p class="text_inner">Ежемесячное пополнение счета <span class="iis_tooltip_tgl" title="Это сумма ежемесячного пополнения Ваших инвестиций, она также может быть равна 0, если Вы не планируете пополнять свой счет.">?</span></p>
                                </div>

                                <input type="text" id="input_refill" value="10000" />
                            </div>
                        </div>

                        <div class="line">
                            <div class="line__inner">
                                <span class="number">3</span>
                                <div class="text">
                                    <p class="text_inner">Введите ставку доходности, в % <span class="iis_tooltip_tgl" title="При вводе ставки доходности следует ориентироваться на то, что чем выше доходность – тем выше риски. Безрисковой ставкой доходности на сегодняшний день можно считать доходность облигаций федерального займа на уровне 8%, 15% - может дать защищенный инвестиционный портфель из акций и облигаций. Более высокий уровень доходности конечно возможен, но риски такого инвестиционного портфеля будут уже более высоки, а стабильность доходности ниже.">?</span></p>
                                </div>

                                <input type="text" id="input_yield_rate" value="10" />
                            </div>
                        </div>
                    </div>
                    <ul class="arr_list">
                        <li>
                            <span class="icon icon-linear_arr_right"></span>
                            <span class="text">
                                <span class="text_inner">При обозначенных исходных данных Ваш портфель <br/><span class="bold">за <span id="years_txt"></span> лет вырастет до <span id="val_max_output"></span> тыс. руб.</span></span>
                            </span>
                        </li>
                    </ul>
                    <div class="chart__outer"></div>
                    <div class="custom_collapse">
                        <div class="custom_collapse_elem opened">
                            <div class="custom_collapse_elem_head collapse_btn">Дополнительные настройки <span class="icon icon-arr_down"></span></div>

                            <div class="custom_collapse_elem_body">
                                <div class="inner">
                                    <div class="chart_settings-iis2">
                                        <div class="row">
                                            <div class="col-left col col-sm-6 col-md-4">
                                                <input type="text" id="input_yield_period" value="30" />
                                                <label><span>Период <br />на графике (лет)</span></label>
                                            </div>

                                            <div class="col-right col col-sm-6 col-md-4">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="iis_type_a" checked>
                                                    <label for="iis_type_a"><span>Учитывать ИИС типа А в расчетах</span></label>
                                                </div>
                                            </div>

                                            <div class="col-right col col-sm-12 col-md-4">
                                                <input type="text" id="input_yield_taxes" value="13" />
                                                <label><span>Учитывать налоги на прирост капитала в расчетах по ставке (%)</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="t20">Данный график лишь первый набросок Вашего финансового плана, для его реализации нужны конкретные финансовые инструменты и понимание инвестиционного процесса.</p><br />
                    <p class="t20">Чтобы узнать, во что инвестировать, чтобы реализовать данный финансовый план, приглашаем Вас на вебинар "Финансовый план: от задумки до реализации":</p>

                    <div class="calc_iis__link">
                        <div class="calc_iis__link__inner">
                            <p>Записаться на вебинар <br /><span class="big">«Во что инвестировать на ИИС»</span></p>
                        </div>

                        <a href="javascript:void(0)" class="button button_light_green" data-toggle="modal" data-target="#popup_master_subscribe">Записаться</a>
                    </div>
                    <div class="company_accordion_outer" style="margin-bottom: 0">
                        <div class="custom_collapse">
                            <div class="custom_collapse_elem opened">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title collapse_btn" style="border-bottom:0">Данные по периодам (на конец периода)<span class="icon icon-arr_down"></span></h2>
                                        </div>
                                        <div class="custom_collapse_elem_body">
                                            <div class="custom_collapse_elem_body_inner">
                                                <div class="tab-content">
                                                    <table id="iisDataTable" class="footable-table"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="article_share">
                        <p>Рассказать другим</p>
                        <ul class="share_list">
                            <li>
                                <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST']?>/lk/yield_calculator/&amp;title=Онлайн калькулятор доходности&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST']?>/lk/yield_calculator/&amp;title=Онлайн калькулятор доходности&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST']?>/lk/yield_calculator/&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST']?>/lk/yield_calculator/&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=Онлайн калькулятор доходности', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=Онлайн калькулятор доходности"></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/share?text=Онлайн калькулятор доходности" onclick="window.open('https://twitter.com/share?text=Онлайн калькулятор доходности','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- *****КАРКАС***** -->

                <div class="other_articles_list">
                    <div class="other_articles_list_top">
                        <p>Рекомендуемые к прочтению статьи:</p>

                        <a class="link_arrow" href="/blog/">Все статьи</a>
                    </div>

                    <div class="articles_list row">
                        <? foreach($articles as $article): ?>
                            <div class="articles_list_element col-xs-6 col-lg-4">
                                <div class="articles_list_element_inner">
                                    <div class="articles_list_element_img">
                                        <div class="articles_list_element_img_inner">
                                            <a href="<?=$article['DETAIL_PAGE_URL']?>" style="background-image:url(<?=CFile::GetPath($article["PREVIEW_PICTURE"])?>);"></a>
                                        </div>
                                    </div>
                                    <div class="articles_list_element_text">
                                        <p class="articles_list_element_title"><a href="<?=$article['DETAIL_PAGE_URL']?>"><?=$article['NAME']?></a></p>
                                        <p  class="articles_list_element_description"><?=$article["PREVIEW_TEXT"]?></p>

                                        <p class="articles_list_element_date">
                                            <?=FormatDate(array(
                                                "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                "today" => "today",              // выведет "сегодня", если дата текущий день
                                                "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                            ), MakeTimeStamp($article["DATE_ACTIVE_FROM"]), time()
                                            );?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
                <div class="comment_outer">
                    <div class="content-main-post-item-comments">
                        <?$APPLICATION->IncludeComponent("cackle.comments", "cackle_default", Array(
                        ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "right_sidebar",
                "EDIT_TEMPLATE" => "",
                "COMPONENT_TEMPLATE" => ".default",
                "AREA_FILE_RECURSIVE" => "Y"
            ),
            false
        );?>
    </div>
</div>
<div class="modal modal_black fade" id="iis_detail_modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title uppercase">Введите размер пополнения ИИС в год</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="modal-body_inner">
                    <div id="iis_detail_modal_list"></div>

                    <div class="text-center mt30">
                        <span id="iis_detail_modal_confirm" class="button" data-dismiss="modal" aria-hidden="true">Подтвердить</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");