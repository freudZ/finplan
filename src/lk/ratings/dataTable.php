<? if($_REQUEST["ajax"] && $_REQUEST["ajax"]=="y"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
		 $haveRadarAccess = checkPayRadar();
       $haveUSARadarAccess = checkPayUSARadar();

 //Для неавторизованных и не имеющих доступа к радару (и к портфелям) сбрасываем фильтр по портфелям
/* if((!$USER->IsAuthorized() || !$haveRadarAccess) && isset($_REQUEST["ratingType"])){
	 unset($_REQUEST["ratingType"]);
 }*/

/* $selectedEventTypes = array();
 if(isset($_REQUEST["ratingType"]) && !empty($_REQUEST["ratingType"])){
	$selectedEventTypes = explode(",",$_REQUEST["ratingType"]);
 }*/
 $CRadRatings = new CRadarRatings;

	}
 ?>
 <div class="EventsCalendar">
    <div class="EventsCalendar__element">
	 <?$notFinded = count($CRadRatings->arFilteredData)>0?"":"По выбранным условиям данных нет";?>
		<?if(in_array($_REQUEST["ratingType"], $CRadRatings->arTypesForPeriod)):?>
		 <h3 class="EventsCalendar__date"><?= $CRadRatings->arRatingsType[$_REQUEST["ratingType"]] ?><?= (array_key_exists($_REQUEST["ratingPeriod"], $CRadRatings->arRatingsPeriod)?"&nbsp;".$CRadRatings->arRatingsPeriod[$_REQUEST["ratingPeriod"]]:"")?></h3>
		<?else:?>
		 <h3 class="EventsCalendar__date"><?= $CRadRatings->arRatingsType[$_REQUEST["ratingType"]] ?></h3>
		<?endif;?>
		 <? $hideTable = false;
		    if(in_array($_REQUEST["ratingType"], $CRadRatings->arNeedAuthRatings) && !$USER->IsAuthorized()){
			 $hideTable = true;
		 }
		 ?>
		 <?if($hideTable):?>
			<a class="" href="#" data-toggle="modal" data-target="#popup_login">Войдите или зарегистрируйтесь</a>, что бы увидеть данный рейтинг
		 <?endif;?>


		 <?if(count($CRadRatings->arFilteredData)>0 && $hideTable==false):?>
		 <table class="EventsCalendar__table">
		 		<?if($_REQUEST["ratingType"]=="INCREASE" || $_REQUEST["ratingType"]=="INCREASE_ETF" || $_REQUEST["ratingType"]=="INCREASE_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Рост</th>
				                </tr>
				            </thead>
				            <tbody>
				 <?elseif($_REQUEST["ratingType"]=="DROP" || $_REQUEST["ratingType"]=="DROP_ETF" || $_REQUEST["ratingType"]=="DROP_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Падение</th>
				                </tr>
				            </thead>
				            <tbody>
				 <?elseif($_REQUEST["ratingType"]=="CAP_RUS" || $_REQUEST["ratingType"]=="CAP_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Капитализация, <?if($_REQUEST["ratingType"]=="CAP_RUS"):?>млн.р.<?else:?>млн.$<?endif;?></th>
				                </tr>
				            </thead>
				            <tbody>
				<?elseif($_REQUEST["ratingType"]=="DIV_RUS" || $_REQUEST["ratingType"]=="DIV_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Дивиденды, %</th>
				                </tr>
				            </thead>
				            <tbody>
				<?elseif($_REQUEST["ratingType"]=="PE_RUS" || $_REQUEST["ratingType"]=="PE_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Текущий P/E</th>
				                </tr>
				            </thead>
				            <tbody>
				<?elseif($_REQUEST["ratingType"]=="RSK_RUS" || $_REQUEST["ratingType"]=="RSK_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Рентабельность СК, %</th>
				                </tr>
				            </thead>
				            <tbody>
				<?elseif($_REQUEST["ratingType"]=="SLIDE_PROCEEDS_RUS" || $_REQUEST["ratingType"]=="SLIDE_PROCEEDS_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Скользящая выручка, <?if($_REQUEST["ratingType"]=="SLIDE_PROCEEDS_RUS"):?>млн.р.<?else:?>млн.$<?endif;?></th>
				                </tr>
				            </thead>
				            <tbody>
				<?elseif($_REQUEST["ratingType"]=="SLIDE_PROFIT_RUS" || $_REQUEST["ratingType"]=="SLIDE_PROFIT_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Скользящая прибыль, <?if($_REQUEST["ratingType"]=="SLIDE_PROFIT_RUS"):?>млн.р.<?else:?>млн.$<?endif;?></th>
				                </tr>
				            </thead>
				            <tbody>
				<?elseif($_REQUEST["ratingType"]=="TEMP_SLIDE_PROCEEDS_RUS" || $_REQUEST["ratingType"]=="TEMP_SLIDE_PROCEEDS_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Темп роста скользящей выручки, %</th>
				                </tr>
				            </thead>
				            <tbody>
				<?elseif($_REQUEST["ratingType"]=="TEMP_SLIDE_PROFIT_RUS" || $_REQUEST["ratingType"]=="TEMP_SLIDE_PROFIT_USA"):?>
				            <thead>
				                <tr>
				                    <th>Актив</th>
				                    <th>Темп роста скользящей прибыли, %</th>
				                </tr>
				            </thead>
				            <tbody>
				<?endif;?>

				<?foreach($CRadRatings->arFilteredData as $arItem):?>
				 <?if($_REQUEST["ratingType"]=="INCREASE" || $_REQUEST["ratingType"]=="INCREASE_ETF" || $_REQUEST["ratingType"]=="INCREASE_USA" || $_REQUEST["ratingType"]=="DROP" || $_REQUEST["ratingType"]=="DROP_ETF" || $_REQUEST["ratingType"]=="DROP_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a>
					  <? if(strpos($_REQUEST["ratingType"], "_ETF")!==false): ?>
						 <br><?= $arItem["PROPS"]["ETF_CALC_BASE"]; ?>
					  <?endif;?>
					 </td>
                      <?$val = floatval($arItem["DYNAM"][$_REQUEST["ratingPeriod"]."_INCREASE"]);?>
					 <td class="vam"><?= ($val<0?'':($val==0?'':'+')) ?><?= $val?>%</td>
				  </tr>
				 <?endif;?>
				 <?if($_REQUEST["ratingType"]=="CAP_RUS" || $_REQUEST["ratingType"]=="CAP_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a></td>
					 <td><?= round($arItem["PROPS"]["ISSUECAPITALIZATION"]/1000000,2)?></td>
				  </tr>
				 <?endif;?>
				 <?if($_REQUEST["ratingType"]=="DIV_RUS" || $_REQUEST["ratingType"]=="DIV_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a></td>
					 <td><?= round($arItem["DYNAM"]["Дивиденды %"],2)?></td>
				  </tr>
				 <?endif;?>
				 <?if($_REQUEST["ratingType"]=="PE_RUS" || $_REQUEST["ratingType"]=="PE_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a></td>
					 <td><?= $arItem["DYNAM"]["PE"]?></td>
				  </tr>
				 <?endif;?>
				 <?if($_REQUEST["ratingType"]=="RSK_RUS" || $_REQUEST["ratingType"]=="RSK_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a></td>
					 <td><?= round($arItem["PERIODS"]["Рентабельность собственного капитала"],2)?></td>
				  </tr>
				 <?endif;?>
				 <?if($_REQUEST["ratingType"]=="SLIDE_PROCEEDS_RUS" || $_REQUEST["ratingType"]=="SLIDE_PROCEEDS_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a></td>
					 <td><?= round($arItem["PERIODS"]["Выручка за год (скользящая)"],2)?></td>
				  </tr>
				 <?endif;?>
				 <?if($_REQUEST["ratingType"]=="SLIDE_PROFIT_RUS" || $_REQUEST["ratingType"]=="SLIDE_PROFIT_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a></td>
					 <td><?= round($arItem["PERIODS"]["Прибыль за год (скользящая)"],2)?></td>
				  </tr>
				 <?endif;?>
				 <?if($_REQUEST["ratingType"]=="TEMP_SLIDE_PROCEEDS_RUS" || $_REQUEST["ratingType"]=="TEMP_SLIDE_PROCEEDS_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a></td>
					 <td><?= round($arItem["PERIODS"]["Темп прироста выручки"],2)?></td>
				  </tr>
				 <?endif;?>
				 <?if($_REQUEST["ratingType"]=="TEMP_SLIDE_PROFIT_RUS" || $_REQUEST["ratingType"]=="TEMP_SLIDE_PROFIT_USA"):?>
				  <tr>
					 <td><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["NAME"] ?></a></td>
					 <td><?= round($arItem["PERIODS"]["Темп роста прибыли"],2)?></td>
				  </tr>
				 <?endif;?>
				<?endforeach;?>
				 </tbody>
		 </table>
		 <?else:?>
		  <h3 class="EventsCalendar__date"><?= $notFinded ?></h3>
		 <?endif;?>
	 </div>
 </div>
