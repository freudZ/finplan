<?
if($_SERVER["HTTP_X_REQUESTED_WITH"]=="XMLHttpRequest"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

} else {
	//define("OBLIGATION_DETAIL_PAGE", "Y");
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$title = "Рейтинг акций ".date('Y');
	$APPLICATION->SetTitle($title);
	$APPLICATION->SetPageProperty("title", $title);
}

?>
<?
 $haveRadarAccess = checkPayRadar();
 $haveUSARadarAccess = checkPayUSARadar();
  $CRadRatings = new CRadarRatings;
/* $selectedEventTypes = array();
 if(isset($_REQUEST["eventTypes"]) && !empty($_REQUEST["eventTypes"])){
	$selectedEventTypes = explode(",",$_REQUEST["eventTypes"]);
 }*/
		if(!isset($_REQUEST["ratingType"])){
			$_REQUEST["ratingType"] = $CRadRatings->defaultRatingType;
		}
		if(!isset($_REQUEST["ratingPeriod"])){
			$_REQUEST["ratingPeriod"] = $CRadRatings->defaultRatingPeriod;
		}



?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "extended_banner",
                            "EDIT_TEMPLATE" => "",
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_RECURSIVE" => "Y"
                        ),
                        false
                    );?>

					<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
				<?endif?>

				<div class="title_container title_nav_outer">
					<h1><? $APPLICATION->ShowTitle(false); ?></h1>

					<?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
<!--						<div class="title_nav">
							<ul>
								<li><a href="/lk/obligations/company/all/">Все эмитенты</a></li>
								<li><a href="/lk/obligations/all/">Все облигации</a></li>
								<li><a href="/lk/actions/all/">Все акции</a></li>
							</ul>
						</div>-->
					<?endif?>
				</div>
				<?if(!$_GET['search_off']):?>
			  <!--	<br> -->
<!--					<form class="innerpage_search_form" method="GET">
						<div class="form_element">
							<input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете? Введите название или тикер" name="q" id="autocomplete_act_all" />
							<button type="submit"><span class="icon icon-search"></span></button>
						</div>
					</form>-->
				<?endif?>
<!-- *****/КАРКАС***** -->

<div class="container-fluid">

<div class="relative">
               <div class="calculate_form">
                 <div class="infinite_container green">

<?//$data = getFavoriteList();// if($USER->IsAuthorized() && $haveRadarAccess): ?>


    <div class="calculate_line">
			<div class="row">
			   <div class="col col-xs-6">
					<div class="form_element">
					<label>Вид рейтинга: </label>
<!--					<?if(!$haveRadarAccess):?>
						<div class="dib relative black tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["события фильтр портфелей"]?>">
					<?endif?>-->
					<select name="ratingType" class="inline_select"  data-period-ratings="<?= implode(",",$CRadRatings->arTypesForPeriod)?>">
						<?foreach($CRadRatings->arRatingsType as $code => $name):?>
						 <option value="<?=$code;?>" <?=($name==$_REQUEST['ratingType'])?'selected':''?>><?=(in_array($code, $CRadRatings->arNeedAuthRatings) && !$USER->IsAuthorized()?"* ":"")?><?=$name;?></option>
						<?endforeach;?>
					</select>
					<?if(!$USER->IsAuthorized()):?>
					<p class="white font_11">* - для просмотра рейтига требуется авторизация</p>
					<?endif;?>
<!--					<?if(!$haveRadarAccess):?>
						</div>
					<?endif?>-->
					</div>
			</div>
			   <div class="col col-xs-6 period-container">
					<div class="form_element">
					<label>Период: </label>
<!--					<?if(!$haveRadarAccess):?>
						<div class="dib relative black tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["события фильтр портфелей"]?>">
					<?endif?>-->
					<select name="ratingPeriod" class="inline_select" >
						<?foreach($CRadRatings->arRatingsPeriod as $code => $name):?>
						<option value="<?=$code;?>" <?=($name==$_REQUEST['ratingPeriod'])?'selected':''?>><?=$name;?></option>
						<?endforeach;?>
					</select>
<!--					<?if(!$haveRadarAccess):?>
						</div>
					<?endif?>-->
					</div>
			</div>
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Выводить по: </label>
                            <select name="topDayCount" class="inline_select" >
                                <?foreach($CRadRatings->arPagerCount as $elemCount):?>
                                    <option value="<?=$elemCount;?>" <?=($elemCount==$_REQUEST['topDayCount'])?'selected':''?>><?=$elemCount;?></option>
                                <?endforeach;?>
                            </select>

                        </div>
                    </div>
                <?if($USER->IsAdmin()):?>
                    <div class="col col-xs-6">
                        <div class="form_element">
                            <label>Отрасль (адм.): </label>
                            <select name="industryRus" class="inline_select" disabled>
                                <?foreach($CRadRatings->arIndustryRus as $industryName => $elemCount):?>
                                    <option value="<?=$industryName;?>" <?=($industryName==$_REQUEST['industryRus'])?'selected':''?>><?=$industryName;?></option>
                                <?endforeach;?>
                            </select>

                        </div>
                    </div>
                <?endif;?>
		  </div>
    </div>

<?// endif;?>


</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
 <div class="ratingsListContainer" >
  <? include("dataTable.php"); ?>
 </div>
</div>
</div>



				<div class="text-center mt-40 mb-40">
					<p class="legal_popup_btn"><a href="#" data-toggle="modal" data-target="#legal_popup">Условия использования информации, размещённой на данном сайте</a></p>
				</div>
<!-- *****КАРКАС***** -->
			</div>
		</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
			<div class="load_overlay">
			<p><span class="radar_animate"><span class="radar_round_inner"></span><span class="radar_round_track"></span><span class="radar_animate_element blue"></span><span class="radar_animate_element green"></span><span class="radar_animate_element orange"></span></span> <span class="radar_txt">Загрузка...</span></p>
		</div>
</div>


<div class="modal fade" id="legal_popup" tabindex="-1">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <div class="modal-header">
      <p class="strong">Условия использования информации с сайта Fin-plan.org</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<div class="modal-body_title"></div>
		<div class="modal-body_inner">
      <p>Вся информация, размещенная на данном интернет-сайте, дизайн, подбор, группировка и расположение материалов являются интеллектуальной собственностью ИП Кошин В.В. и защищены российским законодательством об авторском праве и средствах массовой информации, а также международными договорами РФ по защите интеллектуальной собственности.</p>
      <p>Информация о торгах на рынке группы «московская биржа» предоставлена ПАО Московская биржа. Источником и владельцем биржевой информации является московская биржа.</p>
      <p>Без письменного согласия биржи нельзя осуществлять дальнейшее распространение и предоставление биржевой информации в любом виде и любыми средствами, включая электронные, механические, фотокопировальные, записывающие или другие (в том числе с использованием удаленного мобильного (беспроводного) доступа), её трансляцию, в том числе средствами теле- и радиовещания, её демонстрацию на интернет-сайтах, её использование в игровых, тренажерных и иных системах, предусматривающих демонстрацию и/или передачу биржевой информации, а также для расчёта производной информации (в том числе индексов и индикаторов), предназначенной для дальнейшего публичного распространения.</p>
      <p>ИП Кошин В.В. является дистрибьютором информации об итогах торгов на рынках группы московская биржа.</p>
      <p>Материалы данного интернет-сайта предназначены исключительно для персонального и некоммерческого использования. Запрещается использование автоматизированного извлечения информации данного интернет-сайта без письменного разрешения ИП Кошин В.В. Любое копирование, перепечатка и/или последующее распространение информации, представленной на данном сайте, или информации, полученной на основе этой информации в результате переработки, в том числе производимое путем кэширования, кадрирования или использованием аналогичных средств, строго запрещается без предварительного письменного разрешения ИП Кошин В.В. За нарушение настоящего правила наступает ответственность, предусмотренная законодательством и международными договорами РФ.</p>
      <p>ИП Кошин В.В. не несет ответственность за любую потерю или ущерб (потеря деловых доходов, потеря прибыли или любой прямой, косвенный, последующий, специальный или подобный ущерб вообще), являющийся результатом:</p>
      <ul>
        <li>любой погрешности (или неполноты в, или задержки, прерывания, ошибки или упущения в поставке) в информации;</li>
        <li>любого сбоя информационной системы;</li>
        <li>любого решения или действия, сделанного в уверенности относительно информации.</li>
      </ul>
      <p>ИП Кошин В.В. не несет ответственность за ущерб, причиненный вашему компьютеру, программному обеспечению, модему, телефону и другой собственности, который стал результатом использования информации, программного обеспечения или услуг.</p>
      <p>С уважением, компания Fin-plan.</p>
		</div>
	  </div>
	</div>
  </div>
</div>
<?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
	<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?endif?>