<?
define("TEST_PAGE", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тест");
$image_src_for_social = SITE_TEMPLATE_PATH . '/img/testpage_og.jpg';
$APPLICATION->AddHeadString('<meta content="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '" property="og:image">',true);
$APPLICATION->AddHeadString('<link rel="image_src" href="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '" />',true);
$lj_event = htmlentities('<a href="http://' . $_SERVER['HTTP_HOST'] . $APPLICATION->GetCurUri() . '">' . $arResult["NAME"].'</a><img src="http://'.$_SERVER['HTTP_HOST'].$image_src_for_social.'">');
global $USER;
?>

 <div class="testpage outer">
   <div class="wrapper">
     <div class="testpage__outer">
       <div class="container">
         <div class="row row__before">
           <div class="col__before col-left"></div>
           <div class="col__before col-right"></div>
         </div>

        <div class="testpage__inner">
          <div class="row row__title">
            <div class="col-sm-12">
              <p class="testpage__green_title" onclick="toFinalScreen(0)">Тест</p>
            </div>

            <div class="col col-left">
              <p class="testpage__title">Какой <br>ты инвестор</p>
            </div>

            <div class="col col-right">
              <p class="testpage__number"><span>Вопрос <span class="testpage__number_txt">1</span></span> / 5</p>
            </div>
          </div>

          <div class="row row__main">
            <div class="col col-left">
              <div class="testpage__baffet">
                <div class="testpage__baffet_lvl">
                  <div class="line"></div>
                </div>

                <img class="testpage__baffet_img" src="<?=SITE_TEMPLATE_PATH?>/img/banners/baffet_md.png" alt="">

                <p class="testpage__baffet_txt">Баффет одобрил&nbsp;бы Вашу&nbsp;стратегию</p>

                <div class="testpage__baffet__result">
                  <p class="testpage__baffet_txt_success">Поздравляем, Вы на правильном пути! Вашу стратегию одобрил бы сам Баффет</p>
                  <p class="testpage__baffet_txt_fail">Поздравляем, Вы сделали первые шаги, но пока Ваши инвестиционные решения далеки от идеальных. Инвестирование – это не просто, но и не сложно. Для начала важно научиться пониматься определенные принципы и методы работы. Баффет одобрил бы вашу стратегию</p>
                  <p class="testpage__baffet_percent">на <span>0</span>%</p>
                  <? /* <p class="testpage__baffet_txt_bottom">Хотите узнать, какие активы у самого Баффета в портфеле и почему он их выбрал?</p> */ ?>
                  <p class="testpage__baffet_txt_bottom"><a class="button highlight" href="#popup_testpage_download" data-toggle="modal">Прокачать до 100%</a></p>
                </div>
              </div>
              <div class="testpage__baffet_share">
                <p class="text">Поделитесь своими успехами с друзьями: </p>

                <ul class="share_list">
                    <li>
                        <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri()?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri()?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                    </li>
                    <li>
                        <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri()?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri()?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                    </li>
                    <li>
                        <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>"></a>
                    </li>
                    <!-- <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li> -->
                    <li><a href='https://connect.ok.ru/offer?url=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri()?>&amp;title=<?=$arResult["NAME"]?>&amp;imageUrl=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>' onclick="window.open('https://connect.ok.ru/offer?url=http://<?=$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri()?>&amp;title=<?=$arResult["NAME"]?>&amp;imageUrl=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>','Ok', 'width=626, height=626'); return false;" class="icon icon-ok" rel="nofollow"></a></li>
                </ul>
              </div>

              <? /* <div class="testpage__baffet_bnr">
                <p class="text">Тогда скачайте аналитический отчет от Fin-plan по текущему портфелю У. Баффета и его фонда Berkshire Hathway.</p>
                <a class="button" href="#popup_testpage_download" data-toggle="modal">Скачать</a>
              </div> */ ?>
            </div>

            <div class="col col-right">
              <div class="question_01">
                <div class="row">
                  <div class="quest-col_left col-xs-4 col-xs-push-8 col-sm-3 col-sm-push-9">
                    <img class="quest__img" src="<?=SITE_TEMPLATE_PATH?>/img/test_iphone.png" alt="">
                  </div>

                  <div class="quest-col_right col-xs-8 col-xs-pull-4 col-sm-9 col-sm-pull-3">
                    <p class="quest__text">На дворе 2007&nbsp;год, у Вас есть 23&nbsp;000 рублей, что бы Вы&nbsp;выбрали: купить iphone&nbsp;3G за 23&nbsp;000 или 76&nbsp;акций компании apple по&nbsp;10$.</p>
                  </div>
                </div>

                <div class="answers">
                  <span class="q01-iphone button">iPhone</span>
                  <span class="q01-shares button">76 акций</span>
                </div>
              </div>

              <div class="question_02">
                <p class="quest__text">У Вас есть 400 000 рублей и Вы хотите их инвестировать. <br />Как Вы их распределите между следующими видами активов?</p>

                <p class="q02-money yellow" data-money="400000">Сумма к распределению: <span>0</span> руб.</p>

                <div class="answers">
                  <div class="row">
                    <div class="test_slider_outer">
                      <p class="test_slider__before"><span>0</span> руб.</p>
                      <div class="test_slider" data-index="1"></div>
                      <p class="test_slider__after">Акции</p>
                    </div>

                    <div class="test_slider_outer">
                      <p class="test_slider__before"><span>0</span> руб.</p>
                      <div class="test_slider" data-index="2"></div>
                      <p class="test_slider__after">Облигации</p>
                    </div>

                    <div class="test_slider_outer">
                      <p class="test_slider__before"><span>0</span> руб.</p>
                      <div class="test_slider" data-index="3"></div>
                      <p class="test_slider__after">Депозиты</p>
                    </div>
                  </div>

                  <div class="next-btn__outer">
                    <span class="btn-next button button-white">Далее</span>
                  </div>
                </div>
              </div>

              <div class="question_03">
                <p class="quest__text">При выборе акции, что на самом деле важно для инвестора? <br /><span class="t14">Выберите 1 вариант ответа</span></p>

                <div class="answers">
                  <div class="row">
                    <div class="col-sm-6 col">
                      <div class="form_element">
                        <div class="radio">
                          <input type="radio" id="q3_0" name="q3">
                          <label for="q3_0"><span>Чтобы это была крупная известная компания</span></label>
                        </div>

                        <div class="radio">
                          <input type="radio" id="q3_1" name="q3">
                          <label for="q3_1"><span>Чтобы стоимость акций в последнее время падала</span></label>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6 col">
                      <div class="form_element">
                        <div class="radio">
                          <input type="radio" id="q3_2" name="q3">
                          <label for="q3_2"><span>Чтобы стоимость акций в последнее время росла</span></label>
                        </div>

                        <div class="radio">
                          <input type="radio" id="q3_3" name="q3">
                          <label for="q3_3"><span>Необходимо оценить перспективы развития бизнеса и его справедливую стоимость</span></label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="next-btn__outer">
                    <span class="btn-next button button-white">Далее</span>
                  </div>
                </div>
              </div>

              <div class="question_04">
                <p class="quest__text">На что важнее обращать внимание при выборе брокера для инвестора</p>

                <div class="answers">
                  <div class="q4_slider_outer test_slider_outer">
                    <p class="test_slider__after left">Комиссии</p>
                    <div class="test_slider" data-index="5"></div>
                    <p class="test_slider__after right">Надежность</p>
                  </div>

                  <div class="next-btn__outer">
                    <span class="btn-next button button-white">Далее</span>
                  </div>
                </div>
              </div>

              <div class="question_05">
                <p class="quest__text">При выборе облигации, что на самом деле важно для инвестора? <br /><span class="t14">Выберите 2 варианта ответа</span></p>

                <div class="answers">
                  <div class="row">
                    <div class="col-sm-6 col">
                      <div class="form_element">
                        <div class="checkbox">
                          <input type="checkbox" id="q5_0">
                          <label for="q5_0"><span>Облигация продается по цене менее 100% от номинала</span></label>
                        </div>

                        <div class="checkbox">
                          <input type="checkbox" id="q5_1">
                          <label for="q5_1"><span>Облигация имеет высокую доходность</span></label>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6 col">
                      <div class="form_element">
                        <div class="checkbox">
                          <input type="checkbox" id="q5_2">
                          <label for="q5_2"><span>Эмитент облигации имеет мало долгов и является финансово устойчивым</span></label>
                        </div>

                        <div class="checkbox">
                          <input type="checkbox" id="q5_3">
                          <label for="q5_3"><span>Облигация подходит Вам по срокам гашения</span></label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="next-btn__outer">
                    <span class="btn-next button button-white" data-toggle="modal">Далее</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       </div>
     </div>
   </div>
     <!--<div class="footer">
         <div class="container">
             <div class="row">
                 <div class="col col-xs-6 col-sm-3">
                     <p>©&nbsp;2016 ИП&nbsp;Кошин&nbsp;В.В.<br/><span>ОГРН&nbsp;316583500057741&nbsp;<br/>ИНН&nbsp;583708408904</span></p>
                 </div>

                 <div class="col col-xs-6 col-sm-3">
                     <p>Служба&nbsp;инвест-заботы о&nbsp;клиентах:<br/><a href="mailto:koshin@fin-plan.org">koshin@fin-plan.org</a></p>
                 </div>

                 <div class="col col-xs-6 col-sm-4">
                     <p>Написать лично Виталию:<br/><a href="http://www.facebook.com/vitaly.koshin" target="_blank">www.facebook.com/vitaly.koshin</a></p>
                 </div>

                 <div class="col col-xs-6 col-sm-2">
                     <span class="footer_black"><a href="#" data-toggle="modal" data-target="#footer354">Политика конфиденциальности</a><br/><a href="#" data-toggle="modal" data-target="#footer355">Согласие с рассылкой</a><br/><a href="#" data-toggle="modal" data-target="#footer356">Отказ от&nbsp;ответственности</a></span>
                 </div>
             </div>
         </div>
     </div>-->
     <div class="modal modal_black fade" id="popup_testpage_download" tabindex="-1">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <p class="modal-title">Получите на&nbsp;почту инвест-идеи от У.&nbsp;Баффета с&nbsp;аналитикой от&nbsp;Fin-plan</p>
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                 </div>
                 <div class="modal-body">

                     <form id="popup_testpage_download_form" class="popup_testpage_download_form" method="post">
                         <div class="form_element">
                             <input type="text" name="name" placeholder="Ваше имя" />
                         </div>
                         <div class="form_element">
                             <input type="text" name="email" placeholder="Ваша электронная почта" />
                         </div>
                         <div class="form_element">
                             <input type="text" name="phone" placeholder="Ваш телефон" />
                         </div>
                         <br/>
                         <div class="confidencial_element form_element">
                             <div class="checkbox">
                                 <input type="checkbox" id="confidencial_testpage_download" name="confidencial_testpage_download" checked="">
                                 <label for="confidencial_testpage_download">Нажимая кнопку, я даю согласие <br>на обработку персональных <br>данных и соглашаюсь с <br></label><span data-toggle="modal" data-target="#footer356">пользовательским соглашением</span>  <label for="confidencial_testpage_download"> <br>и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
                             </div>
                         </div>

                         <div class="text-center">
                             <input class="button button_green" type="submit" value="Скачать" />
                             <p class="modal_comment">Мы против спама. Ваши данные<br/>не будут переданы 3-м лицам.</p>
                         </div>
                     </form>
                 </div>
             </div>
         </div>
     </div>
     <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
 </div>
