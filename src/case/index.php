<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
\Bitrix\Main\Loader::includeModule("iblock");

if($_REQUEST["tag"]){
	$_SESSION["case"]["show_tag"] = $_REQUEST["tag"];
	LocalRedirect($APPLICATION->GetCurPage("", array("tag")));
}
if($_SESSION["case"]["show_tag"]){
	?>
	<script>
		$(function () {
		    $(".sections_list_filter.sections_list li[data-value=<?=$_SESSION["case"]["show_tag"]?>]").attr("data-show", "Y");
        });
	</script>
	<?
	unset($_SESSION["case"]["show_tag"]);
}

$APPLICATION->SetPageProperty("title", "Кейсы | Сайт о финансовом планировании Fin-plan.org");
if($_GET['year']) {
	$year = $_GET['year'];
	$month = $_GET['month'] ? $_GET['month'] : '';
	$day = $_GET['day'] ? $_GET['day'] : '';

	if($day>0) {
		$day_new = $day + 1;
		$arrFilter = Array(
		">=DATE_ACTIVE_FROM" => $day.".".$month.".".$year,
		"<=DATE_ACTIVE_FROM" => $day_new.".".$month.".".$year,
  "!SECTION_ID" => 17);
	} elseif($month>0) {
		$month_new = $month + 1;
		$arrFilter = Array(
		">=DATE_ACTIVE_FROM" => "1.".$month.".".$year,
		"<=DATE_ACTIVE_FROM" => "1.".$month_new.".".$year,
  "!SECTION_ID" => 17);
	} elseif($year>0) {
		$year_new = $year + 1;
		$arrFilter = Array(
		">=DATE_ACTIVE_FROM" => "1.1.".$year,
		"<=DATE_ACTIVE_FROM" => "1.1.".$year_new,
  "!SECTION_ID" => 17);
	} else {
		$arrFilter = Array("!SECTION_ID" => 17);
	}
} else {
	$arrFilter = Array("!SECTION_ID" => 17);
}
	
if($_REQUEST["q"]){
	$arrFilter[] = array(
        "LOGIC" => "OR",
        array("NAME" => "%".$_REQUEST["q"]."%"),
        array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
    );
}
if($_REQUEST["tags"]){
	$tmp["LOGIC"] = "OR";
	foreach ($_REQUEST["tags"] as $tag){
		$tmp[] = array("PROPERTY_TAGS" => $tag);
	}

	$arrFilter[] = $tmp;
}
if($_REQUEST["type"]){
    $items = array();
	$db_props = CIBlockElement::GetProperty(23, intval($_REQUEST["type"]), array("sort" => "asc"), Array("CODE"=>"ITEMS"));
	while($ar_props = $db_props->Fetch()) {
		$arrFilter["ID"][] = $ar_props["VALUE"];
	}
}

$newsCount = 18;

?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"case_list", 
	array(
		"IBLOCK_TYPE" => "FINPLAN",
		"IBLOCK_ID" => "48",
		"NEWS_COUNT" => $newsCount,
		"SORT_BY1" => "PROPERTY_ATTACH_POST",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "DOUBLE_POST",
			1 => "ATTACH_POST",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Кейсы",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>