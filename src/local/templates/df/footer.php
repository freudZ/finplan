<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true);?>
	<?if($_SESSION["haveAccessCrm"]):?>
		<!-- Right Slidebar end -->
        <!--footer start-->
        <footer class="site-footer">
            <div class="text-center">
                <?=date("Y")?> &copy; Fin-plan.org
                <a href="#" class="go-top">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>
        </footer>
        <!--footer end-->
    </section>
	<?endif?>

    <!-- js placed at the end of the document so the pages load faster -->

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.scrollTo.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.nicescroll.js" type="text/javascript"></script>
	 <script src="<?=SITE_TEMPLATE_PATH?>/js/select2.full.min.js"></script>
<!--    <script type="text/javascript" language="javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/data-tables/DT_bootstrap.js"></script>-->
<!--    <script src="<?=SITE_TEMPLATE_PATH?>/js/respond.min.js"></script>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/ru.js"></script>

    <script src="<?=SITE_TEMPLATE_PATH?>/js/moment.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/moment.ru.js"></script>-->

<!--	<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap-datepicker.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap-datepicker.ru.min.js"></script>-->

    <!--right slidebar-->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/slidebars.min.js"></script>

    <!--dynamic table initialization -->
    <!--<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/datatables/datatables.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/datatables/Buttons-1.5.6/js/buttons.bootstrap4.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/datatables/Buttons-1.5.6/js/buttons.print.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/datatables/JSZip-2.5.0/jszip.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/datatables/Select-1.3.0/js/select.bootstrap4.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/dt_filter/jquery.dataTables.yadcf.js"></script>

    <!--common script for all pages-->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/common-scripts.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/main_sctripts.js"></script>

</body>

</html>