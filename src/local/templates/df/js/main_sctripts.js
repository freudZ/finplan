$(document).ready(function(){
 $('.clear-data-cache').on('click', function(){
   clear_data_cache(pagetype);
 });

 if(pagetype=='actives') {
  filter_main_table_active();

  } else {
  $('.js-select2-single').select2();
  filter_main_table_periods();

  }
 if(pagetype=='actives'){
  $('#f_industry, #f_market, #f_period, #f_index, #f_delisting').on('change select', function(){
	filter_main_table_active();
  });
  $('.refresh-data-actives').on('click', function(){
	filter_main_table_active();
  });
 }
 if(pagetype=='periods'){
  $('#f_industry, #f_market, #f_period_start, #f_period_end, #f_company, #f_index, #f_delisting').on('change select', function(){
	filter_main_table_periods();
  });
  $('.refresh-data-periods').on('click', function(){
	filter_main_table_periods();
  });
 }
	DT_Init(pagetype);



  $(window).scroll(function(){
  	 var tableContainer = $(".dataTables_wrapper");
  	 if($('.fixedHeader-floating').scrollLeft()==0 && tableContainer.scrollLeft()>0){
			$('.fixedHeader-floating').css('left',(tableContainer.scrollLeft()*-1)+'px');
			}
  });


});

//Очищает кеш для даты
function clear_data_cache(pagetype){
		  $.post(
       "include/clear_data_cache.php",
       {
       },
       onAjaxSuccess
     );

     function onAjaxSuccess(data)
     {
       if(data==1){
			if(pagetype=='actives'){
			  filter_main_table_active();
			} else if(pagetype=='periods') {
			  filter_main_table_periods();
			}
       } else {
       	alert("Не удалось очистить кеш!");
       }
     }
}

function DT_Init(pagetype){
	// Disable search and ordering by default

	var table = $('#DTable').DataTable({
		  lengthMenu: [[10, 20, 30, 50, 100, -1], [10, 20, 30, 50, 100, "Все"]],
		  pageLength: 20,
		  filter: true,
	     paging:   (pagetype=='actives')?true:false,
        ordering: (pagetype=='actives')?true:false,
        info:     (pagetype=='actives')?true:false,
		  stateSave: true,
		  searching: true,
		  bAutoWidth: false,
		  //colReorder: true,
		  responsive: false,
		  fixedHeader: {
            header: true,
            footer: false
        },


		  select: {
            style: 'os',
            items: 'cell'
        },
		  dom: '<"top"<lBft><"clear">>rt<"bottom"<Bip><"clear">>',
		  initComplete: function () {

		  $("#ajax_loader").css('display','none');
		  topScrollX_init();
		  },
		  pagingType: "full_numbers",
		  "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull)
		   {
	 		 for($i=1;$i<aData.length;$i++){
				if(aData[$i].indexOf('E+') + 1) {
				  $('td', nRow).eq($i).css('background-color', '#FFE5E5');//Красим ячейку с кривым значением
				}
				if(Number(aData[$i])<0) {
				  $('td', nRow).eq($i).addClass('actionsTable_subzero').css('color', '#CC0000');//Красим текст ячейки с отрицательным значением
				}

	 		 }

			 if($('td',nRow).eq(0).data('cap_calc')=='y'){ //Если капитализация расчетная - красим фон компании в бледно-желтый цвет
				$(nRow).addClass('calcCapActionRow');
			 }
		   },
		  language: {
		  	 "search": "Поиск:",
		    "paginate": {
		      "first": "Первая",
		      "last": "Последняя",
		      "previous": "<",
		      "next": ">",
		    },
			 "lengthMenu": "Показывать _MENU_ записей на странице",
		    "zeroRecords": "Извините, ничего не найдено...",
		    "info": "Страница _PAGE_ из _PAGES_, всего строк: _TOTAL_",
		    "infoEmpty": "Нет доступных записей.",
		    "infoFiltered": "(filtered from _MAX_ total records)",
			 buttons: {
                copyTitle: 'Копирование в буфер обмена',
                copyKeys: 'Нажмите <i>ctrl</i> или <i>\u2318</i> + <i>C</i> чтобы скопировать данные из таблицы в буфер обмена. <br><br>Чтобы отменить, нажмите на это сообщение или нажмите Esc.',
                copySuccess: {
                    _: 'Скопировано строк: %d',
                    1: '1 строка скопирована'
                }
          }
		  },
			//dom: 'lBrtip',
			buttons: [{
                extend: 'copyHtml5',
					 text:      '<i class="fa fa-clipboard fa-1x" style="color:#663366"></i>',
					 titleAttr: 'Скопировать в буфер обмена',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },{
                extend:    'print',
                text:      '<i class="fa fa-print fa-1x" style="color:#000099"></i>',
                titleAttr: 'Распечатать'
            }, {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o fa-1x" style="color:#336600"></i>',
					 exportOptions: {
						orthogonal: 'export',
						columns: ':visible'
					 },
                titleAttr: 'Сохранить в excel'
            },
				{extend: 'colvis',
				text:      '<i class="fa fa-cog fa-1x" style="color:#555555"></i>',
            titleAttr: 'Показать/скрыть колонки'
					 }
				],
			processing: false,

	});
	$('#DTable_filter').addClass('form-group form-inline').css('width','30%').find('input').addClass('form-control').css('width','85%');
	$('#DTable_length').addClass('form-group form-inline').find('select').addClass('form-control');


    yadcf.init(table, [

    {column_number : 1, filter_type: "range_number"},
    {column_number : 2, filter_type: "range_number"},
    {column_number : 3, filter_type: "range_number"},
    {column_number : 4, filter_type: "range_number"},
    {column_number : 5, filter_type: "range_number"},
    {column_number : 6, filter_type: "range_number"},
    {column_number : 7, filter_type: "range_number"},
    {column_number : 8, filter_type: "range_number"},
    {column_number : 9, filter_type: "range_number"},
    {column_number : 10, filter_type: "range_number"},
    {column_number : 11, filter_type: "range_number"},
    {column_number : 12, filter_type: "range_number"},
    {column_number : 13, filter_type: "range_number"},
    {column_number : 14, filter_type: "range_number"},
    {column_number : 15, filter_type: "range_number"},
    {column_number : 16, filter_type: "range_number"},
    {column_number : 17, filter_type: "range_number"},
    {column_number : 18, filter_type: "range_number"},
    {column_number : 19, filter_type: "range_number"},
	 ]);
}

function topScrollX_init(){
  var tableContainer = $(".dataTables_wrapper");
  var tableS = $(".dataTables_wrapper #DTable");
  var fakeContainer = $(".large-table-fake-top-scroll-container-3");
  var fakeDiv = $(".large-table-fake-top-scroll-container-3 div");

  var tableWidth = tableS.width();
  fakeDiv.width(tableWidth);

  fakeContainer.scroll(function() {
    tableContainer.scrollLeft(fakeContainer.scrollLeft());
  });
  tableContainer.scroll(function() {
    fakeContainer.scrollLeft(tableContainer.scrollLeft());

  //Если шапка зафиксирована - двигаем ее
  if($('.fixedHeader-floating').length){
	$('.fixedHeader-floating').css('left',(tableContainer.scrollLeft()*-1)+'px');
  }
  });





}

function filter_main_table_active(){
	 $("#ajax_loader").css('display','block');
		  $.post(
       "include/body_active.php",
       {
         f_industry: $('#f_industry').val(), //Фильтр по отрасли
         f_market: $('#f_market').val(), //Фильтр по рынку (переключает инфобок акций)
         f_index: $('#f_index').val(), //Фильтр по индексу
         f_period: $('#f_period').val(), //Фильтр по периоду
         f_period_current: $('#f_period_current').val(), //указание текущего периода
         f_delisting: $('#f_delisting').val(), //Показывать вышедшие с обращения или нет
       },
       onAjaxSuccess
     );

     function onAjaxSuccess(data)
     {
       $('#table_body').html(data);
		 DT_Init(pagetype);
     }
}

function filter_main_table_periods(){
	 $("#ajax_loader").css('display','block');
		  $.post(
       "include/body_periods.php",
       {
         f_industry: $('#f_industry').val(), //Фильтр по отрасли
         f_company: $('#f_company').val(), //Фильтр по компании
         f_market: $('#f_market').val(), //Фильтр по рынку (переключает инфобок акций)
         f_index: $('#f_index').val(), //Фильтр по индексу
         f_period_start: $('#f_period_start').val(), //Фильтр по периоду - начало периода
         f_period_end: $('#f_period_end').val(), //Фильтр по периоду - конец периода
			f_delisting: $('#f_delisting').val(), //Показывать вышедшие с обращения или нет
       },
       onAjaxSuccess
     );

     function onAjaxSuccess(data)
     {
       $('#table_body').html(data);
		 DT_Init(pagetype);
     }
}


