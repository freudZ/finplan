$(document).ready(function () {
    $('#industries_popup').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var row = $('#' + button.attr('data-id'));
        var secid = row.attr('data-secid');
        var mode = button.attr('data-mode');
        var form = $(this).find('form');
        if(mode=='cell'){
            var cell = $(button).parents('td');
            var kvartal = cell.attr('data-kv');
            var year = cell.attr('data-year');
            var type = cell.attr('data-type');

            $(form).find('input#UF_KVARTAL').val(kvartal);
            $(form).find('input#UF_YEAR').val(year);
            $(form).find('input#MODE').val('cell');
            if (type.length) {
                $(form).find('select#UF_TYPE').val(type);
            } else {
                $(form).find('select#UF_TYPE').val('TMP');
            }

            var currValue = button.parents('td').attr('data-val');
            $(form).find('input#UF_VALUE').val(currValue);

            $(this).find('.row-title').addClass('hidden');
            $(this).find('.cell-title').removeClass('hidden');

        } else if(mode=='row'){
            $(form).find('input#UF_VALUE').val(0);
            $(form).find('input#MODE').val('row');
            $(this).find('.row-title').removeClass('hidden');
            $(this).find('.cell-title').addClass('hidden');

        }

        $(form).find('input#UF_ACTIVE_CODE').val(secid);
        $(form).find('input#UF_SECTOR_NAME').val($('select[name=sector]').val());
        $(form).find('input#UF_PARAM_NAME').val($('select[name=mark]').val());


    });


    $('#industries_popup .save_industry_values').on('click', function () {
        var form = $(this).parents('form');
        var mode = $(form).find('input#MODE').val();
       var params = {};
            params.ajax = "y";
            params["delete"] = ($(form).find('input#delete').prop('checked')?"y":"");
            params.MODE = mode;
            params.UF_ACTIVE_CODE = $(form).find('input#UF_ACTIVE_CODE').val();
            params.UF_SECTOR_NAME = $(form).find('input#UF_SECTOR_NAME').val();
            params.UF_PARAM_NAME = $(form).find('input#UF_PARAM_NAME').val();
            params.UF_VALUE = $(form).find('input#UF_VALUE').val();
            params.UF_TYPE = $(form).find('#UF_TYPE').val();
            params.UF_COUNTRY = $(form).find('input#UF_COUNTRY').val();


        if(mode=='cell'){
            params.UF_KVARTAL = $(form).find('input#UF_KVARTAL').val();
            params.UF_YEAR = $(form).find('input#UF_YEAR').val();
        } else if(mode=='row'){

        }


/*        var params = {
                ajax: "y",
                'delete': $(form).find('input#delete').prop('checked')?"y":"",
                MODE: $(form).find('input#MODE').val(),
                UF_ACTIVE_CODE: $(form).find('input#UF_ACTIVE_CODE').val(),
                UF_KVARTAL: $(form).find('input#UF_KVARTAL').val(),
                UF_YEAR: $(form).find('input#UF_YEAR').val(),
                UF_SECTOR_NAME: $(form).find('input#UF_SECTOR_NAME').val(),
                UF_PARAM_NAME: $(form).find('input#UF_PARAM_NAME').val(),
                UF_VALUE: $(form).find('input#UF_VALUE').val(),
                UF_TYPE: $(form).find('#UF_TYPE').val(),
                UF_COUNTRY: $(form).find('input#UF_COUNTRY').val(),
            }*/

        $.ajax({
            type: "POST",
            url: "/tools/ajax/save_industry_mid_values.php",
            dataType: "json",
            data: params,
            cashe: false,
            async: false,
            success: function (data) {
                //console.log(data);
                location.reload();
                //reloadCurPage();
                //$(form).find('button[name=send]').click();
               // window.location.href = location.href+'?sector='+$(form).find('input#UF_SECTOR_NAME').val()+'&mark='+$(form).find('input#UF_PARAM_NAME').val();
               // window.location.href = location.href;
            }


        });

    });

	 //���������� �������� �������� ������� ���������� �������� �� ������� ���������� ������� �������
	 $('.fillFromPastPeriod').on('click', function(){
	 	$(this).addClass('fa-spin');
		$(this).off('click');
		 var table = $(this).parents('table');
		 var columnCount = table.find('tr').length;
		 var currPeriodTd = $(this).parents('th');
		 var currIndex = currPeriodTd.index();
		 var form = $('#industries_popup form');
		 $(table.find('tbody tr')).each(function(indx, row){

			 if(Number($(row).find('td').eq(currIndex).attr('data-val'))==0){
				  //	console.log('row', $(row).find('td').eq(currIndex).attr('data-val'));
				for(PastCol = currIndex+1; PastCol<columnCount-1; PastCol++){
					var pastValue = Number($(row).find('td').eq(PastCol).attr('data-val'));
					if(isNaN(pastValue)){
						pastValue=0;
					}
					if(pastValue!=0){
						var params = {};
				            params.ajax = "y";
				            //params["delete"] = ($(form).find('input#delete').prop('checked')?"y":"");
				            params.MODE = 'cell';
				            params.UF_ACTIVE_CODE = $(row).attr('data-secid');
				            params.UF_SECTOR_NAME = $('select[name=sector]').val();
				            params.UF_PARAM_NAME = $('select[name=mark]').val();
				            params.UF_VALUE = pastValue;
				            params.UF_TYPE = "TMP";
				            params.UF_COUNTRY = $(form).find('input#UF_COUNTRY').val();
								params.UF_KVARTAL = $(row).find('td').eq(currIndex).attr('data-kv');
            				params.UF_YEAR = $(row).find('td').eq(currIndex).attr('data-year');
					        $.ajax({
					            type: "POST",
					            url: "/tools/ajax/save_industry_mid_values.php",
					            dataType: "json",
					            data: params,
					            cashe: false,
					            async: false,
					            success: function (data) {
					                //console.log(data);
					                //location.reload();
					                //reloadCurPage();
					                //$(form).find('button[name=send]').click();
					               // window.location.href = location.href+'?sector='+$(form).find('input#UF_SECTOR_NAME').val()+'&mark='+$(form).find('input#UF_PARAM_NAME').val();
					               // window.location.href = location.href;
					            }


					        });
						$(row).find('td').eq(currIndex).attr('data-val', pastValue).text(pastValue);
						$(row).find('td').eq(currIndex).addClass('bg-warning').append(' <span data-id="act_row_id_'+indx+'" data-mode="cell" data-target="#industries_popup"  data-toggle="modal" style="display: block; float:right; text-decoration: none; cursor:pointer;" class="fa-xs fa-stack"><i class="fas fa-edit"></i></span>');
						break;
					}
				}
			 }
       });
		 //location.reload();
		 $('#filter_pivot').submit();
	 });



});