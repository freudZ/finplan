<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true);?>
<?
if(WIDE_PAGE!="Y") define("WIDE_PAGE","N");

$currPage = $APPLICATION->GetCurPage(false);
 ?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?=$APPLICATION->ShowTitle()?></title>

	 <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery-ui.min.css" />
      <link href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" rel="stylesheet" type="text/css">

    <link href="<?=SITE_TEMPLATE_PATH?>/fonts/ubuntu/ubuntu.css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/fonts/iconmoon/style.css?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/fonts/iconmoon/style.css') ?>" rel="stylesheet" />

    <link href="<?=SITE_TEMPLATE_PATH?>/css/styles.css?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/css/styles.css') ?>" rel="stylesheet" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-2.2.2.min.js"></script>

    <?$APPLICATION->ShowHead()?>
 <script>

 var IS_AUTHORIZED = '<?= ($USER->IsAuthorized()?'Y':'N') ?>';
 var SITE_SERVER_NAME = '<?= SITE_SERVER_NAME ?>';
 var HTTPS = '<?= getServerProtocol(); ?>';
     if(HTTPS!='https'){
	  SITE_SERVER_NAME = 'www.'+SITE_SERVER_NAME;
     }
 </script>
  </head>
  <body data-auth="<?=($USER->IsAuthorized())?"true":"false"?>">
  <?if(NO_BX_PANEL!="Y"):?>
	<div class="container-fluid" style="position: fixed; bottom:0; left:0; right:0; z-index:1">
	       <div class="row">
	<?$APPLICATION->ShowPanel()?>
			 </div>
	</div>
	<?endif;?>
	<div class="outer<?if(OUTER_CLASS):?> <?=OUTER_CLASS?><?endif?>">
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col col-xs-6 col-md-2">
						<span class="hamburger"><span></span></span>
						<a class="logo" href="<?if($APPLICATION->GetCurPage()!="/"):?>/<?endif;?>"><span>Fin</span>-plan</a>
						<span class="logo_tools"><strong><i>tools</i></strong></span>
					</div>

					<div class="col main_menu_container col-md-8">
						<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"bootstrap_tools",
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "4",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "bootstrap_tools"
	),
	false
);?>
					</div>

					<div class="col col-xs-6 col-md-2 text-right">
					</div>
				</div>
			</div>
		</div>
 <div class="pagebody">
	 <div class="container<?=(WIDE_PAGE=="Y"?'-fluid':'')?>">
	 	<div class="row">