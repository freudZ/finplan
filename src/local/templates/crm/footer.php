<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true);?>
	<?if($_SESSION["haveAccessCrm"]):?>
		<!-- Right Slidebar end -->
        <!--footer start-->
        <footer class="site-footer">
            <div class="text-center">
                <?=date("Y")?> &copy; Fin-plan.org
                <a href="#" class="go-top">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>
        </footer>
        <!--footer end-->
    </section>
	<?endif?>

    <!-- js placed at the end of the document so the pages load faster -->

    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.scrollTo.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/advanced-datatable/media/js/JSZip-2.5.0/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/advanced-datatable/media/js/Buttons-1.5.6/js/dataTables.buttons.js"></script>
    <script type="text/javascript" language="javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/advanced-datatable/media/js/Buttons-1.5.6/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/advanced-datatable/media/js/Buttons-1.5.6/js/buttons.html5.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/data-tables/DT_bootstrap.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/respond.min.js"></script>  
	<script src="<?=SITE_TEMPLATE_PATH?>/js/select2.full.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/ru.js"></script>

    <script src="<?=SITE_TEMPLATE_PATH?>/js/moment.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/moment.ru.js"></script>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap-datepicker.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap-datepicker.ru.min.js"></script>


	<script src="<?=SITE_TEMPLATE_PATH?>/assets/datatable-filter/jquery.dataTables.yadcf.js"></script>

    <!--right slidebar-->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/slidebars.min.js"></script>

    <!--dynamic table initialization -->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/dynamic_table_init.js"></script>

    <!--common script for all pages-->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/common-scripts.js"></script>

</body>

</html>