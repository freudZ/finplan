<div class="modal modal_custom fade" id="popup_offer_01" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a class="logo" href="#"><span>Fin</span>-plan</a>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-close"></span></button>
			</div>
			<div class="modal-body">
				<p class="modal_custom_title m0">Представляет</p>
				<p class="modal_custom_text">Пошаговая инструкция (pdf  книга + видео)</p>

				<hr class="modal_custom_separator" />

				<p class="modal_custom_name">Как в <script type="text/javascript">var mdate = new Date(); document.write(mdate.getFullYear());</script> году инвестировать под <span class="green">20-25% годовых</span> в надежные активы?</p>

				<hr class="modal_custom_separator" />

				<div class="modal_custom_buttons">
					<div class="row">
						<div class="col_left col col-xs-4">
							<span id="popup_offer_01_btn" class="button button_green highlight">Узнать</span>
							<p class="modal_custom_text">как выгодно <br/>инвестировать</p>
						</div>

						<div class="col_mid col col-xs-4">
							<span class="button button_green_transparent" data-dismiss="modal">Уже знаю</span>
							<p class="modal_custom_text">и я уже успешный <br/>инвестор</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal modal_custom fade" id="popup_offer_01_form" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a class="logo" href="#"><span>Fin</span>-plan</a>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<p class="modal_custom_subtitle">Заполните графы для получения пошаговой инструкции</p>

				<hr class="modal_custom_separator" />

				<form class="form_offer_01">
					<div class="form_element">
						<input type="text" name="name" placeholder="Ваше имя" />
					</div>

					<div class="row">
						<div class="col col-xs-6">
							<div class="form_element m0">
								<input type="text" name="phone" placeholder="Телефон" />
							</div>
						</div>

						<div class="col col-xs-6">
							<div class="form_element m0">
								<input type="text" name="email" placeholder="Электронная почта" />
							</div>
						</div>
					</div>

					<hr class="modal_custom_separator" style="margin-top:10px" />

					<div class="confidencial_element form_element">
						<div class="checkbox">
							<input type="checkbox" id="confidencial_offer_01" name="confidencial_offer_01" checked />
							<label for="confidencial_offer_01">Нажимая кнопку, я даю согласие на обработку персональных <br />данных и соглашаюсь с </label><span data-toggle="modal" data-target="#footer354">пользовательским соглашением</span><label for="confidencial_offer_01"> <br />и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
						</div>
					</div>

					<hr class="modal_custom_separator" />

					<button class="button button_green highlight highlight_fat" type="submit">Отправить</button>
				</form>
			</div>
		</div>
	</div>
</div>
