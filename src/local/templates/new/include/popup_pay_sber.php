<?if($_SESSION["pay_sber_text"]):?>
	<div class="modal fade" id="popup_pay_sber" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<?if($_SESSION["pay_sber_text"]=="success"):?>
				<p class="modal-title uppercase">Спасибо!</p>
			<?else:?>
				<p class="modal-title uppercase">Ошибка</p>
			<?endif?>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		  </div>
		  <div class="modal-body">
			<?if($_SESSION["pay_sber_text"]=="success"):?>
				<p>Ваша оплата прошла успешно.</p>
			<?else:?>
				<p>Произошла ошибка при оплате. Пожалуйста свяжитесь с нами.</p>
			<?endif?>
		  </div>
		</div>
	  </div>
	</div>
	
	<script>
		$(function(){
			$("#popup_pay_sber").modal("show");
		});
	</script>
	<?unset($_SESSION["pay_sber_text"]);?>
<?endif?>