<?php
Global $DB;

$sql = "SELECT * FROM `hl_popups` WHERE `UF_TYPE` = 'commonPopup' AND `UF_ACTIVE` = 1 ORDER BY `UF_SORT` ASC LIMIT 1";
$res = $DB->Query($sql);
$arResult = array();
if($row = $res->fetch()){
 $arResult = $row;
 if(!empty($arResult["UF_PICTURE_1"])){
	$arResult["UF_PICTURE_1"] = CFile::GetPath($arResult["UF_PICTURE_1"]);
 } else {
	$arResult["UF_PICTURE_1"] = "";
 }
 if(!empty($arResult["UF_PICTURE_2"])){
	$arResult["UF_PICTURE_2"] = CFile::GetPath($arResult["UF_PICTURE_2"]);
 } else {
	$arResult["UF_PICTURE_2"] = "";
 }

global $USER;
					$uid = $USER->GetID();
			  $rsUser = CUser::GetByID($uid);

			  $arUser = $rsUser->Fetch();

			  $phone = $arUser["PERSONAL_PHONE"];
			  if(strpos($phone, "8")==0 && strpos($phone, "8")!==false){
				  $phone = substr($phone, 1);
			  } else if(strpos($phone, "+7")!==false){
				  $phone = str_replace("+7", "", $phone);
			  }

 // $detect = new Mobile_Detect;

}
?>

<?//if(count($arResult)>0 && ($USER->GetId()==7307 || $USER->GetId()==1)):?>
<?if(count($arResult)>0):?>
<div class="modal fade" id="commonPopup" tabindex="-1" data-coockie-days="<?=intval($arResult["UF_COOCKIE_DAYS"])>0?$arResult["UF_COOCKIE_DAYS"]:''?>">
  <div class="modal-dialog" >
	<div class="modal-content" style="background-image: url('<?=(isMobile()?$arResult["UF_PICTURE_2"]:$arResult["UF_PICTURE_1"])?>')">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body"  >
					<div class="salebot_subscribe_form" novalidate="novalidate">
							<input type="hidden" name="bitrix_uid" value="<?= $uid ?>">
							<input type="hidden" name="bitrix_uphone" value="<?= $phone ?>">
							<input type="hidden" name="bitrix_uemail" value="<?= !empty($arUser["EMAIL"])?$arUser["EMAIL"]:$arUser["LOGIN"] ?>">
							<input type="hidden" name="action" value="subscribe">
							<input type="hidden" name="type_messenger" value="1">

						<div class="row">
						<div class="form_element col-md-12 ">
							<input type="text" class="inline" placeholder="Ваш номер телефона" name="phone" value="<?= $phone;?>" autocomplete="off">
						</div>
						</div>
						<div class="row">
							<div class="col-sm-6 col-xs-6 text-center">
								<a class="button" target="_self" href="<?=$arResult["UF_BUTTON_TEXT"]?>"><span class="icon icon-vk"></span> Вконтакте</a>
							</div>
							<div class="col-sm-6 col-xs-6 text-center">
								<a class="button" target="_self" href="<?=$arResult["UF_BUTTON_LINK"]?>"><span class="icon icon-telegram"></span> Telegram</a>
							</div>
						</div>
					</div>
	  </div>
	</div>
  </div>
</div>
<?endif;?>