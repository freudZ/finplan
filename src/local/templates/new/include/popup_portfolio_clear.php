<? //$arActsLise = CPortfolio::getActionsList();
	//$arOblsLise = CPortfolio::getObligationsList();
 ?>
 <div class="modal modal_black fade" id="popup_portfolio_clear_row" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase">Очистка портфеля</p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">
       <div class="flex_row row">
         <div class="col col_center col-xs-12">
           <p class="t16 white m0">Очистка портфеля "<span id="clear_portfolio_name" data-portfolio-id=""></span>" приведет к полному удалению всех содержащихся в нем данных безвозвратно.<br>Вы уверены, что хотите очистить выбранный портфель?</p>
         </div>
       </div>
          <hr />
       <div class="submit_element text-center">
         <span class="popup_change_row_save button clear-portfolio-button">Очистить</span>
       </div>
     </div>
   </div>
   </div>
 </div>