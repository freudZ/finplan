<? //$arActsLise = CPortfolio::getActionsList();
	//$arOblsLise = CPortfolio::getObligationsList();
 ?>
 <div class="modal modal_black fade" id="popup_mass_portfolio_remove" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase">Удаление портфелей</p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">
	  <div class="popup_progress" ><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/loader_html.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?></div>
       <div class="flex_row row">
         <div class="col col_center col-xs-12">
           <p class="t16 white m0">Удаление отмеченных портфелей приведет к полному удалению всех содержащихся в них данных безвозвратно.<br>Вы уверены, что хотите удалить выбранные портфели?</p>
         </div>
       </div>
          <hr />
       <div class="submit_element text-center">
         <span class="popup_change_row_save button" id="delSelectedPortfolio">Удалить</span>
       </div>
     </div>
   </div>
   </div>
 </div>