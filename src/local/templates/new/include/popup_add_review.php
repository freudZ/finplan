<div class="modal fade" id="popup_add_review" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header noborder">
				<p class="modal-title">Оставить свой отзыв</p>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">

				<form id="add_review_form" class="add_review_form" method="post" enctype="multipart/form-data">
					<div class="form_element">
						<input type="text" name="name" placeholder="Ваши фамилия и имя" />
					</div>

					<div class="form_element">
						<input type="text" name="email" placeholder="Ваша электронная почта" />
					</div><br />

					<p class="gray_label">Ссылка на Ваш аккаунт в социальной сети</p>
					<div class="add_review_form_social_list_outer">
						<div class="add_review_form_social_list">
							<div class="form_element">
								<input type="text" name="social[]" placeholder="Ссылка на ваш профиль в соц. сети" />
							</div>
						</div>
						<div><span class="dashed_link">Добавить соц.сеть</span></div>
					</div>

					<div class="textarea_element form_element no_margin">
						<textarea name="review_text" placeholder="Ваш отзыв"></textarea>
					</div>

					<div class="form_element">
						<input type="file" name="file" data-placeholder="Прикрепить файл" />
					</div>

					<div class="confidencial_element form_element">
						<div class="checkbox">
							<input type="checkbox" id="confidencial_add_review" name="confidencial_add_review" checked />
							<label for="confidencial_add_review">Нажимая кнопку, я даю согласие <br />на обработку персональных <br />данных и соглашаюсь с <br /><span data-toggle="modal" data-target="#footer354">пользовательским соглашением</span> <br />и <span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span></label>
						</div>
					</div>

					<div class="submit_element text-center">
						<input type="submit" class="button" value="Отправить"  />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>