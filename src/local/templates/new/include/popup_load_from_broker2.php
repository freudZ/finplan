<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<? $portfolio = new CPortfolio();

	$arUserPortfolioList = array();
   $checkPortfolio = $portfolio->checkPortfolioCntForUser($USER->GetID(), false);
	if($checkPortfolio["PORTFOLIO"]["LIMIT"]>$checkPortfolio["PORTFOLIO"]["OWNER"] || $checkPortfolio["ADMIN"]=="Y" ){
	$createNew = true;
	} else {
	$createNew = false;
	}
	$arUserPortfolioList = $portfolio->getPortfolioListForUser($USER->GetID(), false);
	unset($portfolio);

	$CHistory = new CHistory(false);
	$arBrokers = $CHistory->getBrokersTypes();

	?>
 <div class="modal modal_black fade" id="popup_load_from_broker2" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase">Загрузить брокерский отчет</p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">
	    <div class="popup_progress preloader" ><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/loader_html.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?></div>
		<input type="hidden" class="parseData" id="parseData" value=""/>
		<input type="hidden" class="upploadFilePath" id="upploadFilePath" value=""/>
	   <div class="step_block step1">

	   <div class="row">
		  <div class="col-12 col-xs-12 yellow center">Сервис загрузки не учитывает маржинальные операции</div>
		 <?if($GLOBALS["USER"]->IsAdmin()):?>
		  <div class="col-12 col-xs-12 hidden"><?=\Bitrix\Main\Config\Option::get("grain.customsettings","BROKER_REPORT_SERVER");?></div>
		 <?endif;?>
		</div>
		<div class="hist_broker_types flex_row row " style="">
		  <div class="col-12 col_center col-xs-12">
          <div class="form_element">
			   <label>Выберите своего брокера</label>
          	<select id="popup_load_broker">
					<? $firstBrokerFiletypes =".".implode(", .",reset($arBrokers)["type"]);?>
					<?foreach($arBrokers as $k=>$v):?>
					  <?if(!in_array($v["name"], $APPLICATION->brokersList)) continue;?>
					  <option value="<?=$v["name"]?>" data-extensions="<?=".".implode(", .",$v["type"])?>"><?=$v["ru_name"]?></option>
					<?endforeach;?>

          	</select>
				<div class="load_broker_filetypes">Расширения файлов: <span><?=$firstBrokerFiletypes?></span></div>
          </div>
        </div>
      </div>
		<div class="hist_file_load flex_row row " style="">
        <div class="col-12 col_center col-xs-12">
          <div class="form_element">
			  <label>Файл отчета</label>
			  <input type="file" name="broker_report_file" id="broker_report_file" title="Выберите файл отчета" accept="<?=".".implode(",.",reset($arBrokers)["type"])?>" value="">
			 </div>
		  </div>

		</div>

        <p class="broker_report_file_error">Файл отчета не прикреплен</p>


		<div class="flex_row row "  <?= (!$createNew?'style="display:none;"':''); ?>>
        <div class="col-12 col_center col-xs-12">
          <div class="form_element">
			 <label>Выберите портфель</label>
          	<select id="popup_load_broker_portfolio">
              <?if($createNew):?>
          		<option selected="selected" value="" >Новый</option>
				  <?endif;?>
				  <?foreach($arUserPortfolioList as $pid=>$val):?>
				    <?if(empty($val["ver"])) continue;?>
					 <option value="<?=$val["id"]?>" ><?=$val["name"]?></option>
				  <?endforeach;?>
          	</select>
          </div>
        </div>
      </div>

		<div class="flex_row row new_portfolio_name hidden" >
        <div class="col-12 col_center col-xs-12">
          <div class="form_element">
            <input type="text" id="load_broker_portfolio_name" <?= (!$createNew?'disabled="disabled"':''); ?> placeholder="Укажите название портфеля"/>
			 </div>
		  </div>
		</div>
       <div class="submit_element text-center">
         <span class="button hist_broker_next_button" data-currstep="1">Далее</span>
       </div>
		</div>
		<div class="step_block step2 hidden">
		 <div>Не распознаны тикеры следующих бумаг:</div>
		 <div class=" nofoundActives">



		 </div>
		 <br>
		 
		 <div class="submit_element text-center">
         <span class="button hist_broker_step2_button" data-currstep="2">Далее</span>
       </div>
		</div>
          <hr />


     </div>
   </div>
   </div>
 </div>
