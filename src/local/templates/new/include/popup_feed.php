<div class="modal fade" id="popup_phone_feed" tabindex="-1">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Перезвонить</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		  <p>Оставьте Ваш номер и мы перезвоним Вам в течение 5 минут</p>
      <form class="popup_phone_feed_form">
        <div class="form_element">
          <input type="text" name="name" placeholder="Ваше имя" />
        </div>
        <div class="form_element">
          <input type="text" name="phone" placeholder="Ваш телефон" />
        </div>
        <div class="form_element">
          <input type="text" name="email" placeholder="Ваша электронная почта" />
        </div>
        <div class="confidencial_element form_element">
          <div class="checkbox">
            <input type="checkbox" id="confidencial_popup_phone_feed" name="confidencial_popup_phone_feed" checked required />
            <label for="confidencial_popup_phone_feed">Нажимая кнопку, я даю согласие <br />на обработку персональных <br />данных и соглашаюсь с <br /></label><span data-toggle="modal" data-target="#footer354">пользовательским соглашением</span><label for="confidencial_popup_phone_feed"> <br />и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
          </div>
        </div>
        <div class="submit_element text-center">
          <input type="submit" class="button" value="Отправить" />
        </div>
      </form>
	  </div>
	</div>
  </div>
</div>
