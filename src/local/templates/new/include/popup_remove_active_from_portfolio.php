 <div class="modal modal_black fade" id="popup_change_row" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase">Уменьшить долю актива <br />
		 <!--<span id="popup_change_row_name"></span>-->
		 <select id="popup_del_active_selector" class="popup_del_active_selector inline_select" data-current-popup="">

		 </select>
		 </p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">
	   <div class="popup_progress"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/loader_html.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?></div>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m2">Выберите действие</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
				<? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/selector_portfolio_popup.php"),Array("current"=>"popup_change_row", "selected"=>"popup_change_row"),Array("MODE"=>"html","NAME"=>"блок")) ?>
           </div>
         </div>
       </div>
     	 <input type="hidden" id="filled_from_active" value=""/>
     	 <input type="hidden" id="idActive" value="0"/>
     	 <input type="hidden" id="popup_remove_row_type" value=""/>
     	 <input type="hidden" id="popup_remove_row_code" value=""/>
     	 <input type="hidden" id="popup_remove_row_name" value=""/>
     	 <input type="hidden" id="popup_remove_row_url" value=""/>
     	 <input type="hidden" id="popup_remove_row_emitent_name" value=""/>
     	 <input type="hidden" id="popup_remove_row_emitent_url" value=""/>
     	 <input type="hidden" id="popup_remove_row_active_code" value=""/>
     	 <input type="hidden" id="popup_remove_row_currency_code" value=""/>
     	 <input type="hidden" id="popup_remove_row_currency_rate" value="0"/>
     	 <input type="hidden" id="popup_remove_row_active_secid" value=""/>
     	 <input type="hidden" id="popup_remove_row_active_inlot_cnt" value=""/>
     	 <input type="hidden" id="portfolioNumber" value="0"/>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Дата продажи</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="text" class="datepicker" id="popup_sale_row_date" placeholder="Укажите дату" value="<?=date('d.m.Y')?>"/>
           </div>
         </div>
       </div>
      <div class="flex_row row act_usd_price" style="display:none;">
        <div class="col col_left col-xs-5">
          <p class="t16 white m0 popup_change_row_fifo_price_currency_label">Цена выбытия <span></span></p>
        </div>
        <div class="col col_left col-xs-7">
          <div class="form_element">
            <input type="text" id="popup_change_row_price_currency" placeholder="Цена выбытия в валюте"/>
          </div>
        </div>
      </div>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0" id="activePriceTypeName">Цена выбытия </p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="hidden" id="popup_change_row_fifo_price" placeholder=""/>
             <input type="text" id="popup_change_row_price" placeholder="Укажите цену выбытия"/>
           </div>
         </div>
       </div>

      <div class="flex_row row obl_hidden">
        <div class="col col_left col-xs-5">
          <p class="t16 white m0 portfolio_remove_lot_price_label">Цена за 1 лот</p>
        </div>
        <div class="col col_left col-xs-7">
          <div class="form_element hidden popup_remove_row_fifo_price_lot"></div>
          <div class="form_element popup_remove_row_price_lot">
          </div>
        </div>
      </div>

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0 lineheight-1">На какое количество лотов уменьшать</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="number" id="popup_change_row_number" placeholder="Укажите количество лотов" min="1" max="0" />
           </div>
         </div>
       </div>
       <div class="flex_row row">
         <div class="col col_left col-xs-12">
				<span class="checkbox popup_change_row_no_cache_operation__checkbox mt30">
              <input type="checkbox" class="" id="popup_change_row_no_cache_operation" name="popup_change_row_no_cache_operation" value="N">
              <label for="popup_change_row_no_cache_operation">Продать без внесения кеша</label>
				</span>
         </div>
       </div>

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Итого сумма выбытия</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element popup_remove_row_summ">
           </div>
         </div>
       </div>

      <p class="t16 white mt20 m0 text-center">У вас осталось лотов: <span class="popup_change_row_number_current"></span></p>

       <div class="submit_element text-center">
         <span class="popup_del_active button remove-active-button">Сохранить</span>
       </div>
     </div>
   </div>
   </div>
 </div>