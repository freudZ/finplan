<? //регистрация для закрытых материалов в акциях и пр. с редиректом на текущую страницу
//Открывается скриптом в scripts.js клике на "регистрация" в окне авторизации popup_simple_auth.php?>
<div class="modal fade" id="popup_signup_simple" tabindex="-1">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Регистрация</p>
		<button type="button" class="gotoback gotoback_style" data-dismiss="" aria-hidden="true">Назад</button>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">


				<div class="popup_master_subscribe_inner">
					<div class="left_col col">
						<? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/simple_signup_left_col_popup.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?>
					</div>

					<div class="right_col col">

		<?
		$APPLICATION->IncludeComponent(
            "bitrix:main.register",
            "reg_new_simple",
            Array(
                "SHOW_FIELDS" => array("NAME"),
                "REQUIRED_FIELDS" => array("NAME"),
                "AUTH" => "Y",
                "USE_BACKURL" => "Y",
                "SUCCESS_PAGE" => "",
                "SET_TITLE" => "N",
                "USER_PROPERTY" => array(),
                "USER_PROPERTY_NAME" => "",
            )
        );?>
					</div>
				</div>
	  </div>
	</div>
  </div>
</div>