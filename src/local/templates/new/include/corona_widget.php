<?php
Global $DB;
$sql = "SELECT * FROM `hl_popups` WHERE `UF_TYPE` = 'rightBottomSlider' AND `UF_ACTIVE` = 1 ORDER BY `UF_SORT` ASC LIMIT 1";
$res = $DB->Query($sql);
$arResult = array();
if($row = $res->fetch()){
 $arResult = $row;
 if(!empty($arResult["UF_PICTURE_1"])){
	$arResult["UF_PICTURE_1"] = CFile::GetPath($arResult["UF_PICTURE_1"]);
 } else {
	$arResult["UF_PICTURE_1"] = "";
 }
 if(!empty($arResult["UF_PICTURE_2"])){
	$arResult["UF_PICTURE_2"] = CFile::GetPath($arResult["UF_PICTURE_2"]);
 } else {
	$arResult["UF_PICTURE_2"] = "";
 }
}
?>
<?if(count($arResult)>0):?>
<div class="crn-widget crn-gs-usa">
    <span class="icon icon-close"></span>
	 <?if(isset($arResult["UF_PICTURE_1"]) && !empty($arResult["UF_PICTURE_1"])):?>
	    <div class="crn-widget__img">
	        <img src="<?=$arResult["UF_PICTURE_1"]?>" class="usa" alt="">
	    </div>
	 <?endif;?>
    <div class="crn-widget__body">
		 <?if(isset($arResult["UF_NAME"]) && !empty($arResult["UF_NAME"])):?>
	        <p class="crn-widget__title"><?=$arResult["UF_NAME"]?></p>
		 <?endif;?>
		 <?if(isset($arResult["UF_BODY"]) && !empty($arResult["UF_BODY"])):?>
	        <p class="crn-widget__text"><?=htmlspecialchars_decode($arResult["UF_BODY"])?></p>
		 <?endif;?>
	 </div>
	 <?if(isset($arResult["UF_BUTTON_LINK"]) && !empty($arResult["UF_BUTTON_LINK"])):?>
	    <div class="crn-widget__footer">
	        <a href="<?= $arResult["UF_BUTTON_LINK"] ?>" target="_blank" class="button" ><?= $arResult["UF_BUTTON_TEXT"] ?></a>
	    </div>
	 <?endif;?>
</div>
<?endif;?>