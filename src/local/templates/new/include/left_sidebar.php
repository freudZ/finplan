<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $USER;?>
<div class="main_sidemenu">
	<div class="main_sidemenu_element">
		<span class="main_sidemenu_link icon icon-user"></span>

		<div class="main_sidemenu_inner">
            <?if($USER->IsAuthorized()):?>
                <div class="main_sidemenu_inner_top">
                    <p class="heading">Личный кабинет</p>
                    <?php /* <p><a href="<?=$APPLICATION->GetCurPageParam("logout=yes")?>">Выйти</a></p> */ ?>
                </div>

				<p><span class="dib">Войти в Личный кабинет:</span> <br/><a href="/personal/"><?=$USER->GetLogin()?></a>
				<br/><br/><span class="dib t18 strong"><?=$USER->GetFullName()?></span></p>
            <?else:?>
                <div class="main_sidemenu_inner_top">
                    <p class="heading">Вы не авторизованы</p>
                </div>

                <p>Нажмите <a href="#" data-toggle="modal" data-target="#popup_login">войти</a> или <a href="#" data-toggle="modal" data-target="#popup_signup">зарегистрироваться</a>, чтобы воспользоваться дополнительными возможностями сайта.</p>
		    <?endif;?>
        </div>
	</div>

	<?$APPLICATION->IncludeComponent("bitrix:news.list", "left_sidebar_courses", array(
		"IBLOCK_TYPE" => "FINPLAN",
		"IBLOCK_ID" => "9",
		"NEWS_COUNT" => "999",
		"SORT_BY1" => "PROPERTY_OPEN_ALL_REGISTERED",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "LINK",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
		false
	);?>


<? //Мои портфели ?>
<?//if($GLOBALS["USER"]->IsAdmin()):?>
    <div class="main_sidemenu_element">
        <span class="main_sidemenu_link icon icon-portfolio"></span>
        <div class="main_sidemenu_inner">
            <div class="main_sidemenu_inner_top">
                <p class="heading">Мои портфели</p>
            </div>
            <ul class="">
			<?if($USER->IsAuthorized()):?>
							<?$portfolio = new CPortfolio();?>
							<?$checkPortfolio = $portfolio->checkPortfolioCntForUser($USER->GetID(), false); ?>
							<?$portfolioListSidebar = $portfolio->getPortfolioListForUser($USER->GetID(), false);?>
							<?foreach ($portfolioListSidebar as $pid=>$arItem):
								//$payed = checkPaySeminar($arItem["ID"]);
								if(empty($arItem["ver"])) continue;
								?>
			                    <li class="<?//=($payed)?'purchased':'buy?>">
			                       <!-- <a<?if(!$payed):?> target="_blank"<?endif?> href="<?=($payed)?$arItem["DETAIL_PAGE_URL"]:$arItem["PROPERTIES"]["LINK"]["VALUE"]?>"><?=$arItem["NAME"]?></a> -->
			                        <a target="_blank" href="/portfolio/<?=$pid?>/"><?=$arItem["name"]?></a>
			                    </li>
							<?endforeach;?>

			<?endif;?>


		<?if($checkPortfolio["PORTFOLIO"]["LIMIT"]>$checkPortfolio["PORTFOLIO"]["OWNER"] || $checkPortfolio["ADMIN"]=="Y"):?>

		  <li>
		  	<hr>
		  <span class="calculate_table_add_portfolio_btn" data-toggle="modal" data-target="#popup_add_portfolio">Создать&nbsp;портфель</span>
		  </li>

		<?endif;?>
		<?if(!$USER->IsAuthorized()):?>
		  <li>
		  	Для работы с портфелями <a href="https://fin-plan.org/portfolio/?login=y" title="Войти как пользователь">войдите или зарегистрируйтесь</a>
		  	<hr>
		  <span class="calculate_table_add_portfolio_btn" data-toggle="modal" data-target="#popup_add_portfolio">Создать&nbsp;портфель</span>
		  </li>
		<?endif;?>
		<?unset($portfolio);?>
		            </ul>
		        </div>
		    </div>

<?//else:?>
	<?// $GLOBALS['arrUserPortfolio'] = array("PROPERTY_USER" => $USER->GetID()); ?>
	<?/*$APPLICATION->IncludeComponent("bitrix:news.list", "left_sidebar_portfolio", array(
		"IBLOCK_TYPE" => "FINPLAN",
		"IBLOCK_ID" => "52",
		"NEWS_COUNT" => "999",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrUserPortfolio",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "LINK",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
		false
	);*/?>
<?//endif;//if admin?>

	<?/*<div class="main_sidemenu_element">
		<span class="main_sidemenu_link icon icon-portfolio"></span>

		<div class="main_sidemenu_inner">
			<div class="main_sidemenu_inner_top">
				<p class="heading">Мои портфели</p>
			</div>

			<ul>
				<li><a href="#">Ссылка 1</a></li>
				<li><a href="#">Ссылка 2</a></li>
				<li><a href="#">Ссылка ссылка ссылка ссылка ссылка ссылка ссылка ссылка  ссылка ссылка ссылка 3</a></li>
				<li><a href="#">Ссылка 4</a></li>
			</ul>
		</div>
	</div>
	<div class="main_sidemenu_element">
		<span class="main_sidemenu_link icon icon-chart"></span>

		<div class="main_sidemenu_inner">
			<div class="main_sidemenu_inner_top">
				<p class="heading">Анализ портфеля</p>
			</div>

			<ul>
				<li><a href="#">Ссылка 1</a></li>
				<li><a href="#">Ссылка 2</a></li>
				<li><a href="#">Ссылка ссылка ссылка ссылка ссылка ссылка ссылка ссылка  ссылка ссылка ссылка 3</a></li>
				<li><a href="#">Ссылка 4</a></li>
			</ul>
		</div>
	</div>*/?>
	<div class="main_sidemenu_element">
		<span class="main_sidemenu_link icon icon-star_rounded"></span>

		<div class="main_sidemenu_inner">
			<div class="main_sidemenu_inner_top">
				<p class="heading">Избранное</p>
			</div>
            <?if($USER->IsAuthorized()):?>
                <div class="favorite_ajax_data">
                    <?$APPLICATION->IncludeFile("/ajax/left_sidebar/favorite.php")?>
                </div>
            <?else:?>
			    <p>Нажмите <a href="#" data-toggle="modal" data-target="#popup_login">войти</a> или <a href="#" data-toggle="modal" data-target="#popup_signup">зарегистрироваться</a>, чтобы воспользоваться дополнительными возможностями сайта.</p>
		    <?endif;?>
        </div>
	</div>

	<?$APPLICATION->IncludeComponent("bitrix:news.list", "left_sidebar_blog_types", array(
		"IBLOCK_TYPE" => "FINPLAN",
		"IBLOCK_ID" => "23",
		"NEWS_COUNT" => "999",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
		false
	);?>
	<?$APPLICATION->ShowViewContent("blog_article_share")?>
</div>
