<div class="modal fade" id="popup_signup" tabindex="-1">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Регистрация</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<?$APPLICATION->IncludeComponent(
            "bitrix:main.register",
            "reg_new",
            Array(
                "SHOW_FIELDS" => array("NAME","PERSONAL_PHONE"),
                "REQUIRED_FIELDS" => array("NAME", "PERSONAL_PHONE"),
                "AUTH" => "Y",
                "USE_BACKURL" => "Y",
                "SUCCESS_PAGE" => "/lk/obligation/",
                "SET_TITLE" => "N",
                "USER_PROPERTY" => array(),
                "USER_PROPERTY_NAME" => "",
            )
        );?>
	  </div>
	</div>
  </div>
</div>