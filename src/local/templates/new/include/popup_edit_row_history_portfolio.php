<?
 if(!$portfolio){
 	$portfolio = new CPortfolio();
	}
	$arCacheCurrencyList = $portfolio->getCacheCurrencyList();
 ?>
 <div class="modal modal_black fade" id="popup_edit_row_history_portfolio" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase">Редактирование истории сделки</p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">
     	<div class="popup_progress" ><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/loader_html.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?></div>
		 <input type="hidden" id="hist_edit_row_id" val=""/>
		 <input type="hidden" id="hist_edit_row_action" val=""/>
		 <input type="hidden" id="popup_hist_edit_init_date" val=""/>
		 <input type="hidden" id="popup_hist_edit_active_type" val=""/>
		 <input type="hidden" id="popup_hist_edit_active_code" val=""/>
		 <input type="hidden" id="popup_hist_edit_active_secid" val=""/>
		 <input type="hidden" id="popup_hist_edit_active_inlot_cnt" val=""/>
		 <input type="hidden" id="popup_hist_edit_active_price_one" value=""/>
		 <input type="hidden" id="popup_hist_edit_active_price_orig" value=""/>
		 <input type="hidden" id="popup_hist_edit_currency_rate" value=""/>
		 <input type="hidden" id="popup_hist_edit_currency_id" value=""/>
		 <input type="hidden" id="popup_hist_edit_active_lastprice_valute" val=""/>
		 <input type="hidden" id="popup_hist_edit_active_lastprice" val=""/>
		 <input type="hidden" id="popup_hist_edit_price" value=""/>
		 <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m2">Актив</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
				 <div id="popup_hist_edit_active_name" class="t17 m0 yellow hist_edit_active_name"></div>
				 <div id="popup_hist_edit_operation_name" class="t16 m0 white hist_edit_operation"></div>
           </div>
         </div>
       </div>
          <hr />

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Дата действия</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="text" class="datepicker" id="hist_edit_row_date" placeholder="Укажите дату" value="<?//=date('d.m.Y')?>"/>
           </div>
         </div>
       </div>

      <div class="flex_row row">
        <div class="col col_left col-xs-5">
          <p class="t16 white m0 portfolio_hist_edit_price_label">Цена за акцию</p>
        </div>
        <div class="col col_left col-xs-7">
          <div class="form_element">
            <input type="text" id="hist_edit_row_price" placeholder="Цена на дату"/>
          </div>
        </div>
      </div>
      <div class="flex_row row act_usd_price" style="display:none;">
        <div class="col col_left col-xs-5">
          <p class="t16 white m0 portfolio_hist_edit_price_usd_label">Цена за акцию <span></span></p>
        </div>
        <div class="col col_left col-xs-7">
          <div class="form_element">
            <input type="text" id="popup_hist_edit_row_price_one_usd" placeholder="Цена покупки в валюте"/>
          </div>
        </div>
      </div>
      <div class="flex_row row obl_hidden">
        <div class="col col_left col-xs-5">
          <p class="t16 white m0 portfolio_hist_edit_lot_price_label">Цена за 1 лот</p>
        </div>
        <div class="col col_left col-xs-7">
          <div class="form_element popup_hist_edit_row_price_lot">
          </div>
        </div>
      </div>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0 portfolio_hist_edit_lotcnt_label">Количество лотов</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="number" id="hist_edit_cnt_row_number" class="hist_edit_lotcnt_input" placeholder="Укажите количество" min="1" value="1" />
           </div>
         </div>
       </div>

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Итого сумма инвестиций</p>
         </div>
         <div class="col col_left col-xs-7">
           <div id="hist_edit_row_summ" class="form_element hist_edit_row_summ">

           </div>
         </div>
       </div>

       <div class="submit_element text-center">
		   <span class="hist_edit_row_delete button hist-edit-delete-button bg-red">Удалить сделку</span>
         <span class="hist_edit_row_save button hist-edit-apply-button">Записать</span>
       </div>
     </div>
   </div>
   </div>
 </div>