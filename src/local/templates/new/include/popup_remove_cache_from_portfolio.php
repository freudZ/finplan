 <div class="modal modal_black fade" id="popup_exclude_cache_row" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase">Изъять валюту из портфеля<br />
		 <!--<span id="popup_change_row_name"></span>-->
		 <select id="popup_exclude_cache_selector" class="popup_exclude_cache_selector inline_select">

		 </select>
		 </p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">
	   <div class="popup_progress">
		 <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/loader_html.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?>
		</div>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m2">Выберите действие</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
				<? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/selector_portfolio_popup.php"),Array("current"=>"popup_exclude_cache_row", "selected"=>"popup_exclude_cache_row"),Array("MODE"=>"html","NAME"=>"блок")) ?>
           </div>
         </div>
       </div>
     	 <input type="hidden" id="popup_exclude_filled_from_active" value=""/>
     	 <input type="hidden" id="popup_exclude_idActive" value="0"/>
     	 <input type="hidden" id="popup_exclude_row_type" value=""/>
     	 <input type="hidden" id="popup_exclude_row_active_code" value=""/>
     	 <input type="hidden" id="popup_exclude_row_name" value=""/>
     	 <input type="hidden" id="popup_exclude_row_url" value=""/>
     	 <input type="hidden" id="popup_exclude_row_emitent_name" value=""/>
     	 <input type="hidden" id="popup_exclude_row_emitent_url" value=""/>
     	 <input type="hidden" id="popup_exclude_row_currency_code" value=""/>
     	 <input type="hidden" id="popup_exclude_row_currency_rate" value="0"/>
     	 <input type="hidden" id="popup_exclude_row_active_secid" value=""/>
     	 <input type="hidden" id="popup_exclude_row_active_inlot_cnt" value=""/>
     	 <input type="hidden" id="excludePortfolioNumber" value="0"/>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Дата изъятия</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="text" class="datepicker" id="popup_exclude_row_date" placeholder="Укажите дату" value="<?=date('d.m.Y')?>"/>
           </div>
         </div>
       </div>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0" id="activePriceTypeName">Цена изъятия </p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="hidden" id="popup_exclude_row_fifo_price" placeholder=""/>
             <input type="text" id="popup_exclude_row_price" placeholder="Укажите цену выбытия"/>
           </div>
         </div>
       </div>

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0 lineheight-1">Какое количество изъять</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="number" id="popup_exclude_row_number" placeholder="Укажите изымаемое количество валюты" min="0" max="0" />
           </div>
         </div>
       </div>

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Итого сумма изъятия</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element popup_exclude_row_summ">
           </div>
         </div>
       </div>

      <p class="t16 white mt20 m0 text-center">Остаток валюты: <span class="popup_exclude_row_number_current"></span></p>

       <div class="submit_element text-center">
         <span class="popup_exclude_active button exclude-active-button hidden">Изъять</span>
       </div>
     </div>
   </div>
   </div>
 </div>