<div class="modal fade" id="popup_available_to_everyone_form"   tabindex="-1">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Записаться на вебинар</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
      <form class="available_to_everyone_form">
        <div class="form_element">
          <input type="text" name="name" placeholder="Ваше имя" />
        </div>
        <div class="form_element">
          <input type="text" name="phone" placeholder="Ваш телефон" />
        </div>
        <div class="form_element">
          <input type="text" name="email" placeholder="Ваша электронная почта" />
        </div>
        <div class="confidencial_element form_element">
          <div class="checkbox">
            <input type="checkbox" id="confidencial_available_to_everyone" name="confidencial_available_to_everyone" checked required />
            <label for="confidencial_available_to_everyone">Нажимая кнопку, я даю согласие <br />на обработку персональных <br />данных и соглашаюсь с <br /></label><span data-toggle="modal" data-target="#footer354">пользовательским соглашением</span><label for="confidencial_available_to_everyone"> <br />и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
          </div>
        </div>
        <div class="submit_element text-center">
          <input type="submit" class="button" value="Отправить" />
        </div>
      </form>
	  </div>
	</div>
  </div>
</div>
