<div class="crn-widget">
    <span class="icon icon-close"></span>

    <div class="crn-widget__img">
        <img src="<?=SITE_TEMPLATE_PATH?>/img/usa.png" class="usa" alt="">
    </div>

    <div class="crn-widget__body">
        <p class="crn-widget__title">У нас обновление!</p>
        <p class="crn-widget__text">Мы запустили веб-сервис по оценке акций США. Приглашаем Вас на вебинар "Инвестиции в технологии будущего на рынке США".</p>
    </div>

    <div class="crn-widget__footer">
        <button type="button" class="button" data-toggle="modal" data-target="#popup_master_subscribe">Записаться на вебинар</button>
    </div>
</div>
