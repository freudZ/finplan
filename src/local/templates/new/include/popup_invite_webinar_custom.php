<div class="modal fade" id="popup_master_subscribe_custom" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header noborder">
				<p class="modal-title">План вебинара и регистрация</p>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<div class="popup_master_subscribe_inner">
					<div class="left_col col">
						<p class="subtitle ib_NAME">"#NAME#"</p><br/><br/>
						<span class="ib_DETAIL_TEXT">#DETAIL_TEXT#</span>


						<p class="subtitle">Ближайшие даты:</p>
						<p class="font_14 green strong ib_PREVIEW_TEXT">#PREVIEW_TEXT#</p>
					</div>

					<div class="right_col col">
						<form id="master_subscribe_form_custom" class="master_subscribe_form" method="post">
						   <input type="hidden" class="ib_PROPERTY_MAILCHIMP_LIST_VALUE" name="mclist" value=""/>
						   <input type="hidden" class="ib_PROPERTY_MAILCHIMP_INTEREST_VALUE" name="mcinterest" value=""/>
							<p class="gray_label">Чтобы записаться, оставьте данные в форме ниже:</p><br/>

							<div class="form_element">
								<input type="text" name="name" placeholder="Ваше имя" />
							</div>

							<div class="form_element">
								<input type="text" name="email" placeholder="Ваша электронная почта" />
							</div>

							<div class="form_element">
								<input type="text" name="phone" placeholder="Ваш телефон" />
							</div>

							<div class="confidencial_element form_element">
								<div class="checkbox">
									<input type="checkbox" id="confidencial_master_subscribe" name="confidencial_master_subscribe" checked />
									<label for="confidencial_master_subscribe">Нажимая кнопку, я даю согласие <br />на обработку персональных <br />данных и соглашаюсь с <br /><span data-toggle="modal" data-target="#footer354">пользовательским соглашением</span> <br />и <span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span></label>
								</div>
							</div>

							<div class="submit_element text-center">
								<input type="submit" class="button" value="Отправить"  />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>