<? $portfolio = new CPortfolio(); ?>
	<?$checkPortfolio = $portfolio->checkPortfolioCntForUser($USER->GetID(), false);
	if($checkPortfolio["PORTFOLIO"]["LIMIT"]>$checkPortfolio["PORTFOLIO"]["OWNER"] || $checkPortfolio["ADMIN"]=="Y" ){
	$createNew = true;
	} else {
	$createNew = false;
	}
	unset($portfolio);
	?>
 <div class="modal modal_black fade" id="popup_add_portfolio" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase"><?= ($createNew?'Создать портфель':'Добавить в портфель') ?></p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">

<!--      <div class="flex_row row">
        <div class="col-12 col_ceter col-xs-12">
          <p class="t16 white m0">Укажите название портфеля</p>
        </div>
      </div>-->

		<div class="add_portfolio_from_radar flex_row row " style="display:none;">
        <div class="col-12 col_center col-xs-12">
          <div class="form_element">
          	<select id="popup_add_to_portfolio_radar_list">

              	<?if($createNew):?>
          		<option selected="selected" value="" >Новый</option>
				  <?endif;?>

          	</select>
          </div>
        </div>
      </div>

		<div class="flex_row row "  <?= (!$createNew?'style="display:none;"':''); ?>>
        <div class="col-12 col_center col-xs-12">
          <div class="form_element">
            <input type="text" id="popup_add_portfolio_name" <?= (!$createNew?'disabled="disabled"':''); ?> placeholder="Укажите название портфеля"/>
          </div>
        </div>
      </div>
		<div class="flex_row row hidden">
         <div class="col text-right col-xs-12">
           <div class="checkbox custom add_portfolio_checkbox">
             <input type="checkbox" id="portfolio_add_checkbox_watch" checked="checked"/>
             <label for="portfolio_add_checkbox_watch">Редактировать можно</label>
           </div>
         </div>
        </div>
		  <div class="flex_row row hidden">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Дата отслеживания</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="text" class="datepicker" id="popup_add_portfolio_date" disabled placeholder="Укажите дату" value="<?= (new DateTime($cur_portfolio['date_curr']))->format('d.m.Y') ?>"/>
           </div>
         </div>
       </div>

          <hr />

       <div class="flex_row_ row modal_actives_block" style="display:none;">
         <div class="col col_left col-xs-12 col-lg-12">
           <p class="t16 white m0">Выбранные активы:</p>
         </div>
         <div class="col modal_actives_rows_container_header col_left col-xs-12  col-lg-12">
         	<div class="modal_active_row">
			 	<div class="modal_active_name">Название</div>
			 	<div class="modal_active_count">Кол-во</div>
			 	<div class="modal_active_summ">Сумма</div>
			 </div>
         </div>
         <div class="col modal_actives_rows_container col_left col-xs-12  col-lg-12">

         </div>

		  <hr />

       </div>

       <div class="submit_element text-center">
         <span class="popup_change_row_save button add-portfolio-button">Создать</span>
       </div>
     </div>
   </div>
   </div>
 </div>