<?
if(!$portfolio){
    $portfolio = new CPortfolio();
}
$arCacheCurrencyList = $portfolio->getCacheCurrencyList();
?>
<div class="modal modal_black fade" id="popup_add_cache_row" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title uppercase">Внести валюту в портфель</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="popup_progress" ><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/loader_html.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?></div>
                <div class="flex_row row">
                    <div class="col col_left col-xs-5">
                        <p class="t16 white m2">Выберите действие</p>
                    </div>
                    <div class="col col_left col-xs-7">
                        <div class="form_element">
                            <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/selector_portfolio_popup.php"),Array("current"=>"popup_add_cache_row", "selected"=>"popup_add_cache_row"),Array("MODE"=>"html","NAME"=>"блок")) ?>
                        </div>
                    </div>
                </div>
                <div class="flex_row row">
                    <div class="col col_left col-xs-5">
                        <p class="t16 white m0">Выберите валюту</p>
                    </div>
                    <div class="col col_left col-xs-7">
                        <div class="form_element">
                            <input type="hidden" id="popup_insert_row_type" value="currency"/>
                            <input type="hidden" id="popup_insert_row_active_id" value=""/>
                            <input type="hidden" id="popup_insert_row_active_url" value=""/>
                            <input type="hidden" id="popup_insert_row_active_lastprice" value=""/>
                            <input type="hidden" id="popup_insert_row_active_lastprice_valute" value=""/>
                            <input type="hidden" id="popup_insert_row_active_price_one" value=""/>
                            <input type="hidden" id="popup_insert_row_currency_rate" value=""/>
                            <input type="hidden" id="popup_insert_row_active_price_orig" value=""/>
                            <input type="hidden" id="popup_insert_row_active_inlot_cnt" value=""/>
                            <input type="hidden" id="popup_insert_row_active_emitent_name" value=""/>
                            <input type="hidden" id="popup_insert_row_active_emitent_url" value=""/>
                            <input type="hidden" id="popup_insert_row_active_emitent_currency" value=""/>
                            <input type="hidden" id="popup_insert_row_active_currency_id" value=""/>
                            <input type="hidden" id="popup_insert_row_active_secid" value=""/>
                            <input type="hidden" id="popup_insert_row_price" value=""/>
                            <input type="hidden" id="insert_last_work_date" value=""/>
                            <select id="popup_add_cache_selector" class="popup_add_cache_selector inline_select" >
                                <option value="">Не выбрана</option>
                                <?foreach($arCacheCurrencyList as $listKey=>$listElement): ?>
                                    <option value="<?= $listKey ?>" <?=($listKey=="RUB"?'selected':'')?> data-inlot-cnt="<?=$listElement["LOTSIZE"]?>" data-lastprice="<?= $listElement["LASTPRICE"]?>" data-active-id="<?= $listElement["ID"] ?>" data-active-url="<?= $listElement["DETAIL_PAGE_URL"] ?>"><?= $listElement["NAME"] ?></option>
                                <?endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
                <hr />

                <div class="flex_row row">
                    <div class="col col_left col-xs-5">
                        <p class="t16 white m0">Дата добавления</p>
                    </div>
                    <div class="col col_left col-xs-7">
                        <div class="form_element">
                            <input type="text" class="datepicker" id="popup_insert_row_date" placeholder="Укажите дату" value="<?=date('d.m.Y')?>"/>
                        </div>
                    </div>
                </div>

                <div class="flex_row row">
                    <div class="col col_left col-xs-5">
                        <p class="t16 white m0 portfolio_insert_price_label">Курс</p>
                    </div>
                    <div class="col col_left col-xs-7">
                        <div class="form_element">
                            <input type="text" id="popup_insert_row_price_one" placeholder="Курс на дату"/>
                        </div>
                    </div>
                </div>


                <div class="flex_row row">
                    <div class="col col_left col-xs-5">
                        <p class="t16 white m0 portfolio_insert_lotcnt_label">Количество</p>
                    </div>
                    <div class="col col_left col-xs-7">
                        <div class="form_element">
                            <input type="text" id="popup_insert_row_number" class="portfolio_insert_lotcnt_input" placeholder="Укажите количество валюты" min="1" value="1" />
                        </div>
                    </div>
                </div>

                <div class="flex_row row">
                    <div class="col col_left col-xs-5">
                        <p class="t16 white m0">Итоговая сумма</p>
                    </div>
                    <div class="col col_left col-xs-7">
                        <div class="form_element popup_insert_row_summ">

                        </div>
                    </div>
                </div>

                <div class="submit_element text-center">
                    <span class="popup_insert_row_save button insert-currency-button">Внести</span>
                </div>
            </div>
        </div>
    </div>
</div>
