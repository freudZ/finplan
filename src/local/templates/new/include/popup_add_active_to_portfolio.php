<?
  use \Bitrix\Main\Localization\Loc;
 ?>
 <div class="modal modal_black fade" id="popup_add_row" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase">Добавить актив в портфель</p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">
     	<div class="popup_progress" ><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/loader_html.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?></div>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m2">Выберите действие</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
				<? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/selector_portfolio_popup.php"),Array("current"=>"popup_add_row", "selected"=>"popup_add_row"),Array("MODE"=>"html","NAME"=>"блок")) ?>
           </div>
         </div>
       </div>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Выберите тип актива</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <select id="popup_add_row_type" class="inline_select">
               <option value="bonds">Облигации</option>
               <option value="shares" selected="selected">Акции РФ</option>
					<option value="shares_usa">Акции США</option>

               <option value="shares_etf">ETF</option>
					<option value="currencies">Валюты</option>
             </select>
           </div>
         </div>
       </div>
       <div class="flex_row row">
         <div class="col col_left col-xs-12">
				<span class="checkbox popup_add_subzero_cache_on__checkbox ">
              <input type="checkbox" class="" id="popup_add_subzero_cache_on" name="popup_add_subzero_cache_on" value="Y">
              <label for="popup_add_subzero_cache_on">Автопополнение счета при нехватке средств <span class="tooltip_btn p-tooltip" title="" data-original-title="<?= Loc::getMessage("PORTF_POPUP_AUTOCACHE") ?>">i</span></label>
				</span>
         </div>
       </div>
          <hr />
         <div class="search-form">
           <div class="form_element">
           	 <input type="hidden" id="popup_add_row_active_id" value=""/>
           	 <input type="hidden" id="popup_add_row_active_code" value=""/>
           	 <input type="hidden" id="popup_add_row_active_url" value=""/>
           	 <input type="hidden" id="popup_add_row_active_lastprice" value=""/>
           	 <input type="hidden" id="popup_add_row_active_lastprice_valute" value=""/>
           	 <input type="hidden" id="popup_add_row_active_price_one" value=""/>
           	 <input type="hidden" id="popup_add_row_currency_rate" value=""/>
           	 <input type="hidden" id="popup_add_row_active_price_orig" value=""/>
           	 <input type="hidden" id="popup_add_row_active_inlot_cnt" value=""/>
           	 <input type="hidden" id="popup_add_row_active_emitent_name" value=""/>
           	 <input type="hidden" id="popup_add_row_active_emitent_url" value=""/>
           	 <input type="hidden" id="popup_add_row_active_emitent_currency" value=""/>
           	 <input type="hidden" id="popup_add_row_active_currency_id" value=""/>
           	 <input type="hidden" id="popup_add_row_active_secid" value=""/>
           	 <input type="hidden" id="popup_add_row_price" value=""/>
           	 <input type="hidden" id="last_work_date" value=""/>

             <input class="search-form__input" id="autocomplete_obl_act_all" type="text" name="q" autocomplete="off" value="" placeholder="Начните вводить название актива">
             <!-- <button class="search-form__button" type="button"> <span class="icon icon-search"></span></button> -->

<!--             <div class="search-form__list customscroll">
               <ul>
                 <?if(count($arActsLise)>0):?>
					   <?foreach($arActsLise as $act_id=>$ac_name):?>
                 		<li data-id="<?=$act_id?>"><?=$ac_name?> <button type="button" class="search-form__list_plus icon icon-plus"></button></li>
					   <?endforeach;?>
					  <?endif;?>
               </ul>
             </div>-->
           </div>
         </div>
          <hr />

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Дата добавления</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="text" class="datepicker" id="popup_add_row_date" placeholder="Укажите дату" value="<?=date('d.m.Y')?>"/>
           </div>
         </div>
       </div>
       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Кеш на дату</p>
         </div>
         <div class="col col_left col-xs-7">
				<ul id="popup_add_row_free_cache">

				</ul>
         </div>
       </div>
      <div class="flex_row row">
        <div class="col col_left col-xs-5">
          <p class="t16 white m0 portfolio_add_price_label">Цена за акцию</p>
        </div>
        <div class="col col_left col-xs-7">
          <div class="form_element">
            <input type="text" id="popup_add_row_price_one" placeholder="Укажите цену покупки"/>
          </div>
        </div>
      </div>
      <div class="flex_row row act_usd_price" style="display:none;">
        <div class="col col_left col-xs-5">
          <p class="t16 white m0 portfolio_add_price_usd_label">Цена за акцию <span></span></p>
        </div>
        <div class="col col_left col-xs-7">
          <div class="form_element">
            <input type="text" id="popup_add_row_price_one_usd" placeholder="Цена покупки в валюте"/>
          </div>
        </div>
      </div>


      <div class="flex_row row obl_hidden">
        <div class="col col_left col-xs-5">
          <p class="t16 white m0 portfolio_add_lot_price_label">Цена за 1 лот</p>
        </div>
        <div class="col col_left col-xs-7">
          <div class="form_element popup_add_row_price_lot">
          </div>
        </div>
      </div>

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0 portfolio_add_lotcnt_label">Количество лотов</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element">
             <input type="number" id="popup_add_row_number" class="portfolio_add_lotcnt_input" placeholder="Укажите количество лотов" min="1" value="1" />
           </div>
         </div>
       </div>

       <div class="flex_row row">
         <div class="col col_left col-xs-5">
           <p class="t16 white m0">Итого сумма инвестиций</p>
         </div>
         <div class="col col_left col-xs-7">
           <div class="form_element popup_add_row_summ">

           </div>
         </div>
       </div>
         
         <p class="popup_add_row__summ_error error text-center">Недостаточно кэша для добавления актива в портфель. Уменьшите количество лотов либо активируйте автопополнение счета</p>

       <div class="submit_element text-center">
         <span class="popup_change_row_save button add-active-button">Сохранить</span>
       </div>
     </div>
   </div>
   </div>
 </div>
