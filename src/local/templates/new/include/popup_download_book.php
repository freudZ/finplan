<div class="modal fade" id="popup_download" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<p class="modal-title">Скачать книгу</p>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<div class="popup_download_name">
					<div class="col img_col">
						<div class="popup_download_img" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/learning_slider/slide_02.jpg);"></div>
					</div>

					<div class="col text_col">Темная сторона инвестирования: 7 инсайдов, которые знают только профи</div>
				</div>

				<form id="webinar_reg_form" class="webinar_reg_form" method="post">
					<div class="form_element">
						<input type="text" placeholder="Ваше имя" name="name"/>
					</div>

					<div class="form_element">
						<input type="text" name="email" placeholder="Ваша электронная почта" />
					</div>

					<div class="form_element">
						<input type="text" placeholder="Ваш телефон" name="phone"/>
					</div>

					<br /> 

          <div class="confidencial_element form_element">
            <div class="checkbox">
              <input type="checkbox" id="confidencial_download_book" name="confidencial_download_book" checked />
              <label for="confidencial_download_book">Нажимая кнопку, я даю согласие <br />на обработку персональных <br />данных и соглашаюсь с <br /></label><span data-toggle="modal" data-target="#footer356">пользовательским соглашением</span>  <label for="confidencial_download_book"> <br />и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
            </div>
          </div>

					<div class="text-center">
						<input class="button button_green" type="submit" value="Скачать" />
						<p class="modal_comment">Мы против спама. Ваши данные<br/>не будут переданы 3-м лицам.</p>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
