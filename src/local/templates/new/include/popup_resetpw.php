<div class="modal fade" id="popup_reset_password" tabindex="-1">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<p class="modal-title uppercase">Восстановление пароля</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
	  <div class="modal-body">
		<?
		$APPLICATION->IncludeComponent(
			"bitrix:system.auth.forgotpasswd",
			"new_popup",
			Array(
			   
			)
		);
		?>
	  </div>
	</div>
  </div>
</div>