
<div class="modal fade" id="popup_login" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<p class="modal-title uppercase">Вход в личный кабинет</p>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		  </div>
		  <div class="modal-body">
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:system.auth.form",
				"auth",
				Array(
					"REGISTER_URL" => "",
					"FORGOT_PASSWORD_URL" => "",
					"PROFILE_URL" => "",
					"SHOW_ERRORS" => "Y"
				)
			);
			?>
		  </div>
		</div>
	  </div>
	</div>
	<div class="modal fade" id="popup_login_back" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<p class="modal-title uppercase">Вход в личный кабинет</p>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		  </div>
		  <div class="modal-body">
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:system.auth.form",
				"auth",
				Array(
					"REGISTER_URL" => "",
					"FORGOT_PASSWORD_URL" => "",
					"PROFILE_URL" => "/lk/accounting/",
					"SHOW_ERRORS" => "Y"
				)
			);
			?>
			
		  </div>
		</div>
	  </div>
	</div>