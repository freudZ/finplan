<?	?>
 <div class="modal modal_black fade" id="popup_portfolio_editdescr" tabindex="-1">
   <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-header">
       <p class="modal-title uppercase">Описание портфеля</p>
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     </div>
     <div class="modal-body">

		<div class="flex_row row ">
        <div class="col-12 col_center col-xs-12">
          <div class="form_element">
          	<textarea id="popup_portfolio_descr"></textarea>
          </div>
        </div>
      </div>

          <hr />

       <div class="submit_element text-center">
         <span class="popup_change_row_save button modal-savedescr-portfolio-button">Сохранить</span>
       </div>
     </div>
   </div>
   </div>
 </div>