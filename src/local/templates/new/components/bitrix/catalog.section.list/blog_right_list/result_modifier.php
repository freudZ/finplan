<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	  //Сортировка разделов
		if($arParams["SORT_ORDER"]=="DESC")
	 	usort($arResult["SECTIONS"], function ($item1, $item2) {
	    return $item2["SORT"] > $item1["SORT"];
		});

		if($arParams["SORT_ORDER"]=="ASC")
	 	usort($arResult["SECTIONS"], function ($item1, $item2) {
	    return $item2["SORT"] < $item1["SORT"];
		});

?>