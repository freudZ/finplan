<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["ITEMS"]):?>

        <p class="title green">Оплаченные курсы и сервисы:</p>
		<div class="sidebar_element">
			<?
			$end = checkPayEndDateRadar();
			$hasUsaRadar = checkPayUSARadar();
			if($end){
				$end = new DateTime($end);
				$diff = $end->diff(new DateTime());
			}
			?>
			<div class="sidebar_courses_element">
				<p>
					<a href="/lk/obligations/">
						Фин-План "Радар"
						<?if($end):
							?>
							<?if($diff->days>45):?>
								<br/><span class="font_13 strong green">подписка до <?=FormatDate("j F Y", $end->getTimestamp())?> года</span>
							<?else:?>
								<br/><span class="font_13 strong red"><span class="icon icon-attention"></span> подписка до <?=FormatDate("j F Y", $end->getTimestamp())?> года</span>
							<?endif?>
						<?endif?>
					</a>
				</p>
				<?if($end && $diff->days<=45):?>
					  <a class="button button_gray_transparent" href="<?=$APPLICATION->lkRadar["Продлить"]?>">Продлить</a>
				<?elseif(!$end):?>
					<a class="button" href="<?=$APPLICATION->lkRadarUsa["Купить"]?>">Купить</a>
				<?endif?>
			</div>
		</div>
		
        <?foreach ($arResult["ITEMS"] as $arItem):
            $payed = checkPaySeminar($arItem["ID"]);?>
            <?if($payed):
				$end = checkPayEndDateSeminar($arItem["ID"]);
				if($end){
					$end = new DateTime($end);
					$diff = $end->diff(new DateTime());
				}
				?>
                <div class="sidebar_element">
                    <div class="sidebar_courses_element">
                        <p>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
								<?=$arItem["NAME"]?>
								<?if($end):
									?>
									<?if($diff->days>45):?>
										<br/><span class="font_13 strong green">подписка до <?=FormatDate("j F Y", $end->getTimestamp())?> года</span>
									<?else:?>
										<br/><span class="font_13 strong red"><span class="icon icon-attention"></span> подписка до <?=FormatDate("j F Y", $end->getTimestamp())?> года</span>
									<?endif?>
								<?endif?>
							</a>
						</p>
						<?if($end && $diff->days<=45):?>
							<a class="button button_gray_transparent" href="<?=$arItem["PROPERTIES"]["AGAIN_LINK"]["VALUE"]?>">Продлить</a>
						<?endif?>
                    </div>
                </div>
            <?else:?>
                <div class="sidebar_element">
                    <div class="sidebar_courses_element">
                        <p><a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>"><?=$arItem["NAME"]?></a></p>

                        <a class="button" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">Купить</a>
                    </div>
                </div>
            <?endif;?>
        <?endforeach;?>

<?endif;?>