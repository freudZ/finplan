<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	$curPage = $arResult["NAV_RESULT"]->NavPageNomer;
	$totalPages = $arResult["NAV_RESULT"]->NavPageCount;
	if (is_array($arResult["SECTION"]))
        $section = $arResult["SECTION"]["PATH"][0]["CODE"];
	else
        $section = "0";

$monthes_1 = array(
1 => 'январь', 2 => 'февраль', 3 => 'март', 4 => 'апрель',
5 => 'май', 6 => 'июнь', 7 => 'июль', 8 => 'август',
9 => 'сентябрь', 10 => 'октябрь', 11 => 'ноябрь', 12 => 'декабрь'
);

$monthes_2 = array(
1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля',
5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа',
9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря'
);

$h1_title = "Блог о финансовом планировании";
if($_GET['year']) {
    $year = $_GET['year'];
    $month = $_GET['month'] ? $_GET['month'] : '';
    $day = $_GET['day'] ? $_GET['day'] : '';

    if($day>0) {
        $h1_title = "Статьи за ".$day." ".$monthes_2[$month]." ".$year." года";
    } elseif($month>0) {
        $h1_title = $h1_title = "Статьи за ".$monthes_1[$month]." ".$year." года";
    } elseif($year>0) {
        $h1_title = "Статьи за ".$year." год";
    }
    $APPLICATION->SetPageProperty("title", $h1_title." | Блог | Сайт о финансовом планировании Fin-plan.org");
} elseif (is_array($arResult["SECTION"])) {
    $h1_title = $arResult["SECTION"]["PATH"][0]["NAME"];
    $APPLICATION->SetPageProperty("title", $h1_title." | Блог | Сайт о финансовом планировании Fin-plan.org");
} elseif($_REQUEST["type"]){
	$elem = CIBlockElement::GetByID($_REQUEST["type"]);
	if($item = $elem->GetNext()){
		$h1_title = $item["NAME"];
		$APPLICATION->SetPageProperty("title", $h1_title." | Блог | Сайт о финансовом планировании Fin-plan.org");
	}
} else {
    $APPLICATION->SetPageProperty("title", "Блог | Сайт о финансовом планировании Fin-plan.org");
}

?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>

				<div class="title_container">
					<h1><?=$h1_title?></h1>
					<form class="innerpage_search_form" method="GET">
						<div class="form_element">
							<input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете?" name="q" id="autocomplete_blog" />
							<button type="submit"><span class="icon icon-search"></span></button>
						</div>
					</form>
				</div>

                <?if($arResult["TAGS"]):?>
					 <? ?>
                    <ul class="sections_list_filter sections_list">
                        <?foreach ($arResult["TAGS"] as $tag):?>
                            <li data-value="<?=$tag["ID"]?>"><?=$tag["NAME"]?> [<?=$tag["CNT"]?>]</li>
                        <?endforeach;?>
                    </ul>
				<?endif;?>

                <?if($arResult["SECTION"]["PATH"][0]["ID"]==17):
                    $mid = ceil(count($arResult["ITEMS_ABS"])/2);?>
                    <div class="dictionary_list">
                        <div class="row">
                            <div class="col col-xs-6">
                                <?foreach ($arResult["ITEMS_ABS"] as $letter=>$data):
                                    $i++?>
                                    <div class="dictionary_group">
                                        <p class="dictionary_group_character"><?=$letter?></p>

                                        <ul>
                                            <?foreach ($data as $item):?>
                                                <li><a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a></li>
                                            <?endforeach;?>
                                        </ul>
                                    </div>
                                    <?if($mid==$i):?>
                                        </div>
                                        <div class="col col-xs-6">
                                    <?endif;?>
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>
				<?else:?>
                    <div class="other_articles_list">
                        <div class="articles_list colored row">
                            <?if($_REQUEST["is_ajax"]){
                               $APPLICATION->RestartBuffer();
                                ob_start();
                            }?>
                            <? foreach($arResult["ITEMS"] as $arItem): ?>
                                <div class="articles_list_element col-xs-6 col-lg-4">
                                    <div class="articles_list_element_inner">
                                        <div class="articles_list_element_img">
                                            <div class="articles_list_element_img_inner">
                                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
                                            </div>
                                        </div>

                                        <div class="articles_list_element_text">
                                            <p class="articles_list_element_title">
                                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                            </p>
                                            <p  class="articles_list_element_description">
                                                <?=$arItem["PREVIEW_TEXT"]?>
                                            </p>

                                            <p class="articles_list_element_date">
                                                <?=FormatDate(array(
                                                    "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                    "today" => "today",              // выведет "сегодня", если дата текущий день
                                                    "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                    "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                    "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                                    ), MakeTimeStamp($arItem["DISPLAY_ACTIVE_FROM"]), time()
                                                );?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?endforeach?>

                            <?php
                            if($_REQUEST["is_ajax"]) {
                                $data = ob_get_contents();
                                ob_clean();

                                echo json_encode([
                                    "content" => $data,
                                    "next" => $_REQUEST["PAGEN_1"]+1,
                                    "last" => $arResult["LAST_PAGE"]
                                ]);
                                exit();
                            }
                            ?>
                        </div>

                        <?if($arResult["TOTAL_COUNT"]>$arParams["NEWS_COUNT"]):?>
                            <div class="other_articles_list_bottom text-center">
                                <a class="more_articles button button_green" data-next="2" href="#">Загрузить ещё <span class="icon icon-arr_down"></span></a>
                            </div>
                        <?endif;?>
                    </div>
                <?endif?>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>
