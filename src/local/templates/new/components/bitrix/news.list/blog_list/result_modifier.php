<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["SECTION"]["PATH"][0]["ID"]==17) {
	function record_sort($records, $field, $reverse=false) {
		$hash = array();
		foreach($records as $record) {
			$hash[$record[$field]] = $record;
		}
		($reverse)? krsort($hash) : ksort($hash);

		$records = array();
		foreach($hash as $record) {
			$records []= $record;
		}
		return $records;
	}
	function abcdef($a, $b) {
		if ($a == $b) {
			return 0;
		}
		if ((preg_match('/[А-Яа-яЁё]/u', $a) && !preg_match('/[А-Яа-яЁё]/u', $b)) || (!preg_match('/[А-Яа-яЁё]/u', $a) && preg_match('/[А-Яа-яЁё]/u', $b))) {
			return ($a > $b) ? -1 : 1;
		} else {
			return ($a < $b) ? -1 : 1;
		}

	}

	$arResultABC = array();

	foreach($arResult["ITEMS"] as $arItem) {
		$first = mb_strtoupper(mb_substr($arItem["NAME"], 0, 1));
		if(isset($arResultABC[$first]))
			array_push($arResultABC[$first], $arItem);
		else
			$arResultABC[$first] = array($arItem);

		$arResultABC[$first] = record_sort($arResultABC[$first], "NAME");
	}

	setlocale(LC_ALL, '');
	uksort($arResultABC, 'abcdef');

	$arResult["ITEMS_ABS"] = $arResultABC;
}


$arTags = [];

//список тегов
$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "!PROPERTY_TAGS" => false, "ACTIVE"=>"Y");
if($arParams["PARENT_SECTION_CODE"]){
	$arFilter["SECTION_CODE"] = $arParams["PARENT_SECTION_CODE"];
}

/*$res = CIBlockElement::GetList(Array(), $arFilter, array("PROPERTY_TAGS"), false);
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();

	$arTags[] = $arFields["PROPERTY_TAGS_VALUE"];
}*/

	//Считаем кол-во матириалов с тегами и кешируем
	$arTagsCount = array();
   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cacheId = "blog_tags_count";
	$cacheTtl = 86400 * 365;
       //$cache->clean("usa_actions_data");
	if ($cache->read($cacheTtl, $cacheId)) {
            $arItems = $cache->get($cacheId);
        } else {

			 $res = CIBlockElement::GetList(
			   array('CNT' => 'DESC'),
			   $arFilter,
			   array("PROPERTY_TAGS")
				);
				while ($item = $res->GetNext()) {
				  $arTags[$item["PROPERTY_TAGS_VALUE"]] = $item["CNT"];
				}

         $cache->set($cacheId, $arItems);
        }


//получение тегов
if($arTags) {
	$arFilter = Array("IBLOCK_ID" => 22, "ID" => array_keys($arTags), "ACTIVE" => "Y");
	$res = CIBlockElement::GetList(Array("SORT" => "asc", "NAME" => "asc"), $arFilter, false, false, array("NAME", "ID"));
	while ($item = $res->GetNext()) {
		$item["CNT"] = $arTags[$item["ID"]];
		$arResult["TAGS"][] = $item;
	}
}

/*global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
		  echo "<pre  style='color:black; font-size:11px;'>";
		     print_r($arResult["TAGS"]);
		     echo "</pre>";
      }*/

//кол-во элементов
unset($arFilter);

//даты
if($_GET['year']) {
	$year = $_GET['year'];
	$month = $_GET['month'] ? $_GET['month'] : '';
	$day = $_GET['day'] ? $_GET['day'] : '';

	if($day>0) {
		$day_new = $day + 1;
		$arFilter = Array(
			">=DATE_ACTIVE_FROM" => $day.".".$month.".".$year,
			"<=DATE_ACTIVE_FROM" => $day_new.".".$month.".".$year,
			"!SECTION_ID" => 17);
	} elseif($month>0) {
		$month_new = $month + 1;
		$arFilter = Array(
			">=DATE_ACTIVE_FROM" => "1.".$month.".".$year,
			"<=DATE_ACTIVE_FROM" => "1.".$month_new.".".$year,
			"!SECTION_ID" => 17);
	} elseif($year>0) {
		$year_new = $year + 1;
		$arFilter = Array(
			">=DATE_ACTIVE_FROM" => "1.1.".$year,
			"<=DATE_ACTIVE_FROM" => "1.1.".$year_new,
			"!SECTION_ID" => 17);
	} else {
		$arFilter = Array("!SECTION_ID" => 17);
	}
} else {
	$arFilter = Array("!SECTION_ID" => 17);
}

$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
$arFilter["ACTIVE"] = "Y";
$arFilter["ACTIVE_DATE"] = "Y";

if($arParams["PARENT_SECTION_CODE"]){
	$arFilter["SECTION_CODE"] = $arParams["PARENT_SECTION_CODE"];
}
if($_REQUEST["q"]){
	$arFilter[] = array(
		"LOGIC" => "OR",
		array("NAME" => "%".$_REQUEST["q"]."%"),
		array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
	);
}
$arFilter["!SECTION_ID"] = 17;

if($_REQUEST["tags"]){
	$tmp["LOGIC"] = "OR";
	foreach ($_REQUEST["tags"] as $tag){
		$tmp[] = array("PROPERTY_TAGS" => $tag);
	}

	$arFilter[] = $tmp;
}

if($_REQUEST["type"]){
	$items = array();
	$db_props = CIBlockElement::GetProperty(23, intval($_REQUEST["type"]), array("sort" => "asc"), Array("CODE"=>"ITEMS"));
	while($ar_props = $db_props->Fetch()) {
		$arFilter["ID"][] = $ar_props["VALUE"];
	}
}

$res = CIBlockElement::GetList(Array(), $arFilter, false, false);

$arResult["TOTAL_COUNT"] = $res->SelectedRowsCount();
$arResult["LAST_PAGE"] = ceil($arResult["TOTAL_COUNT"]/$arParams["NEWS_COUNT"]);
