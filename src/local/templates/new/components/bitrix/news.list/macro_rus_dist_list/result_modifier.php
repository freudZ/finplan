<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function record_sort($records, $field, $reverse=false) {
	$hash = array();
	foreach($records as $record) {
		$hash[$record[$field]] = $record;
	}
	($reverse)? krsort($hash) : ksort($hash);

	$records = array();
	foreach($hash as $record) {
		$records []= $record;
	}
	return $records;
}
function abcdef($a, $b) {
	if ($a == $b) {
		return 0;
	}
	if ((preg_match('/[А-Яа-яЁё]/u', $a) && !preg_match('/[А-Яа-яЁё]/u', $b)) || (!preg_match('/[А-Яа-яЁё]/u', $a) && preg_match('/[А-Яа-яЁё]/u', $b))) {
		return ($a > $b) ? -1 : 1;
	} else {
		return ($a < $b) ? -1 : 1;
	}

}

$arResultABC = array();

$mid = ceil(count($arResult["ITEMS"])/2);

foreach($arResult["ITEMS"] as $n=>$arItem) {
	$first = mb_strtoupper(mb_substr($arItem["NAME"], 0, 1));
	if(isset($arResultABC[$first]))
		array_push($arResultABC[$first], $arItem);
	else
		$arResultABC[$first] = array($arItem);

	$arResultABC[$first] = record_sort($arResultABC[$first], "NAME");

	if($mid==($n+1)){
		$arResult["MID_LETTER"] = $first;
	}
}

setlocale(LC_ALL, '');
uksort($arResultABC, 'abcdef');


$arResult["ITEMS_ABS"] = $arResultABC;