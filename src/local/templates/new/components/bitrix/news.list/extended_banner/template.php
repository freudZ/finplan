<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	 <? $havePopup = !empty($arItem["PROPERTIES"]["POPUP"]["VALUE"])?true:false;
		 $modal_data='';
		 if($havePopup){
		 	$modal_data='data-content-id="'.$arItem["PROPERTIES"]["POPUP"]["VALUE"].'" data-toggle="modal" data-target="#popup_master_subscribe_custom"';
		 }

	 ?>
	 <?if($arItem["PROPERTIES"]["EXT_BANNER"]["VALUE"]=="Y"):?>
    <div class="second_offer_outer testpage_bnr">
        <div class="offer_round_outer">
            <div class="col col_left with_round green" style="background-color: #e5e6e6;">
                <div class="col_inner">
                    <img class="baffet" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                    <p class="offer_round_title"
                        <?if($arItem["PROPERTIES"]["HEADER_COLOR"]["VALUE"]):?>
                            style="color: #<?=$arItem["PROPERTIES"]["HEADER_COLOR"]["VALUE"]?>"
                        <?endif?>
                    >
                        <?= htmlspecialchars_decode($arItem["PROPERTIES"]["HEADER_TEXT"]["VALUE"])?>
                    </p>
                    <p class="offer_round_subtitle"
                        <?if($arItem["PROPERTIES"]["BODY_COLOR"]["VALUE"]):?>
                            style="color: #<?=$arItem["PROPERTIES"]["BODY_COLOR"]["VALUE"]?>"
                        <?endif?>
                    >
                        <?=$arItem["PROPERTIES"]["BODY_TEXT"]["VALUE"]?>
                    </p>
                </div>
            </div>

            <div class="col col_right" style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>); background-position: left center">
                <div class="col_inner">
                    <a
                        <?if($arItem["PROPERTIES"]["LINK_BLANK"]["VALUE"]):?>
                            target="_blank"
                        <?endif?>
                        href="<?=($havePopup?'#':$arItem["PROPERTIES"]["LINK_URL"]["VALUE"])?>"
                        class="button button_rounded_green highlight <?if($havePopup):?>custom_popup_link<?endif;?>"
								<?= $modal_data ?>

                    >
                        <?=$arItem["PROPERTIES"]["LINK_TEXT"]["VALUE"]?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <style>
        <?if($arItem["PROPERTIES"]["CIRCLE_COLOR"]["VALUE"]):?>
            .testpage_bnr .offer_round_outer .col.with_round.green::before{
                background-color: #<?=$arItem["PROPERTIES"]["CIRCLE_COLOR"]["VALUE"]?>;
            }
					.button.button_rounded_green {
						border: 2px solid #<?=$arItem["PROPERTIES"]["BUTTON_COLOR"]["VALUE"]?>;
						background: #<?=$arItem["PROPERTIES"]["BUTTON_COLOR"]["VALUE"]?>;
						color: #<?=!empty($arItem["PROPERTIES"]["BUTTON_TEXT_COLOR"]["VALUE"])?:'000000';?>;
					}

					.button.button_rounded_green:hover {
						border-color: #<?=$arItem["PROPERTIES"]["BUTTON_HOVER_COLOR"]["VALUE"]?>;
						background: #<?=$arItem["PROPERTIES"]["BUTTON_HOVER_COLOR"]["VALUE"]?>;
						color: #<?=!empty($arItem["PROPERTIES"]["BUTTON_TEXT_COLOR"]["VALUE"])?:'000000';?>;
					}
        <?endif?>
    </style>
	 <?else:  //Шаблон простого баннера?>

    <div class="second_offer_outer header_bnr">
        <?if(!$arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] || $arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="IMG_FULL"):?>
            <p class="second_offer_text"><?=$arItem["PROPERTIES"]["HEADER_TEXT"]["VALUE"]?></p>

            <div class="second_offer" style="background: url('<?=$arItem["DETAIL_PICTURE"]["SRC"]?>') -15px 0px no-repeat; background-color: #e8eef0; background-position: right top;">
                <a class="button <?if($havePopup):?>custom_popup_link<?endif;?>" <?= $modal_data ?> <?if($arItem["PROPERTIES"]["LINK_BLANK"]["VALUE"]):?> target="_blank"<?endif?><?if($arItem["PROPERTIES"]["LINK_NOFOLLOW"]["VALUE"]):?> rel="nofollow"<?endif?> href="<?=($havePopup?'#':$arItem["PROPERTIES"]["LINK_URL"]["VALUE"])?>">
                    <?=$arItem["PROPERTIES"]["LINK_TEXT"]["VALUE"]?>
                </a>
            </div>
        <?elseif($arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="IMG_RIGHT" || $arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="IMG_BACKGROUND"):?>
				<?if($arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="IMG_BACKGROUND"){
				  $bgStyle = 'background-image: url('.$arItem["DETAIL_PICTURE"]["SRC"].'); background-position: left center';
				} else {
				  $bgStyle = '';
				}?>
		      <div class="offer_round_outer" style="<?= $bgStyle ?>">
					 <?if($arItem["PROPERTIES"]["BG_COLOR"]["VALUE"]){
					  $bg_color = '#'.$arItem["PROPERTIES"]["BG_COLOR"]["VALUE"].';';
					 }else{
					  $bg_color = 'transparent;';
					 }?>

                <div class="col col_left with_round green" style="background-color: <?= $bg_color ?>">
                    <div class="col_inner">
                        <p
                            class="offer_round_title"
                            <?if($arItem["PROPERTIES"]["HEADER_COLOR"]["VALUE"]):?>
                                style="color: #<?=$arItem["PROPERTIES"]["HEADER_COLOR"]["VALUE"]?>"
                            <?endif?>
                        >
                        	<?if(!empty($arItem["PROPERTIES"]["HEADER_TEXT"]["VALUE"])):?>
									  <?= htmlspecialchars_decode($arItem["PROPERTIES"]["HEADER_TEXT"]["VALUE"])?>
									  <?else:?>
                            <?=$arItem["NAME"]?>
									 <?endif;?>
                        </p>
                        <p
                            class="offer_round_subtitle"
                            <?if($arItem["PROPERTIES"]["BODY_COLOR"]["VALUE"]):?>
                                style="color: #<?=$arItem["PROPERTIES"]["BODY_COLOR"]["VALUE"]?>"
                            <?endif?>
                        >
                            <?=$arItem["PREVIEW_TEXT"]?>
                        </p>
                    </div>
                </div>

				<?if($arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="IMG_RIGHT"){
				  $bgStyle = 'background-image: url('.$arItem["DETAIL_PICTURE"]["SRC"].'); background-position: left center';
				} else {
				  $bgStyle = '';
				}?>
                <div class="col col_right <?if($arItem["PROPERTIES"]["SHOW_CHART"]["VALUE"]=="Y"):?>with_chart<?endif;?>" style="<?= $bgStyle ?>">
                    <div class="col_inner">
                        <a <?= $modal_data ?> <?if($arItem["PROPERTIES"]["LINK_BLANK"]["VALUE"]):?> target="_blank"<?endif?><?if($arItem["PROPERTIES"]["LINK_NOFOLLOW"]["VALUE"]):?> rel="nofollow"<?endif?> href="<?=($havePopup?'#':$arItem["PROPERTIES"]["LINK_URL"]["VALUE"])?>" class="button button_rounded_green <?if($havePopup):?>custom_popup_link<?endif;?>"><?=!empty($arItem["PROPERTIES"]["LINK_TEXT"]["VALUE"])?$arItem["PROPERTIES"]["LINK_TEXT"]["VALUE"]:'Подробнее'?></a>
                    </div>
                </div>
            </div>
            <style>
                <?if($arItem["PROPERTIES"]["CIRCLE_COLOR"]["VALUE"]):?>
                .header_bnr .offer_round_outer .col.with_round.green::before{
                    background-color: #<?=$arItem["PROPERTIES"]["CIRCLE_COLOR"]["VALUE"]?>;
                }
					.button.button_rounded_green {
						border: 2px solid #<?=$arItem["PROPERTIES"]["BUTTON_COLOR"]["VALUE"]?>;
						background: #<?=$arItem["PROPERTIES"]["BUTTON_COLOR"]["VALUE"]?>;
						color: #<?=!empty($arItem["PROPERTIES"]["BUTTON_TEXT_COLOR"]["VALUE"])?$arItem["PROPERTIES"]["BUTTON_TEXT_COLOR"]["VALUE"]:'000000';?>;
					}

					.button.button_rounded_green:hover {
						border-color: #<?=$arItem["PROPERTIES"]["BUTTON_HOVER_COLOR"]["VALUE"]?>;
						background: #<?=$arItem["PROPERTIES"]["BUTTON_HOVER_COLOR"]["VALUE"]?>;
						color: #<?=!empty($arItem["PROPERTIES"]["BUTTON_TEXT_COLOR"]["VALUE"])?$arItem["PROPERTIES"]["BUTTON_TEXT_COLOR"]["VALUE"]:'000000';?>;
					}
                <?endif?>
            </style>
        <?endif;?>
    </div>
	 <?endif;?>
<?endforeach;?>
