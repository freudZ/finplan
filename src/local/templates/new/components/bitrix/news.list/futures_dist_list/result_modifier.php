<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;

//Получаем список уникальных тикеров индексов из HL блока "Акции в индексах"
//$CFutures = new CFutures;

//$arSharesCntInIndexes = $CIndexes->getIndexesHL_tickers();


/* $arTmp = array("MAIN"=>array(), "OTHER"=>array());
 foreach($arResult["ITEMS"] as $item){
	if(array_key_exists($item["CODE"],$arSharesCntInIndexes)){
	 $arTmp["MAIN"][] = $item;
	}  else {
	 $arTmp["OTHER"][] = $item;
	}
 }
	if(count($arTmp["MAIN"])>0){
		$arResult["ITEMS"] = $arTmp;
		unset($arTmp);
	}*/



function record_sort($records, $prop, $reverse=false) {
	$hash = array();
	foreach($records as $record) {
		$hash[$record["PROPERTIES"][$prop]["VALUE"]] = $record;
	}
	($reverse)? krsort($hash) : ksort($hash);

	$records = array();
	foreach($hash as $record) {
		$records []= $record;
	}
	return $records;
}
function abcdef($a, $b) {
	if ($a == $b) {
		return 0;
	}
	if ((preg_match('/[А-Яа-яЁё0-9]/u', $a) && !preg_match('/[А-Яа-яЁё0-9]/u', $b)) || (!preg_match('/[А-Яа-яЁё0-9]/u', $a) && preg_match('/[А-Яа-яЁё0-9]/u', $b))) {
		return ($a > $b) ? -1 : 1;
	} else {
		return ($a < $b) ? -1 : 1;
	}

}

/*$arResultABC["MAIN"] = array();
$arResultABC["OTHER"] = array();*/

$arResultABC = array();

$mid = ceil(count($arResult["ITEMS"])/2);

foreach($arResult["ITEMS"] as $n=>$arItem) {

	$first = mb_strtoupper(mb_substr($arItem["PROPERTIES"]["LATNAME"]["VALUE"], 0, 1));
	if(isset($arResultABC[$first]))
		array_push($arResultABC[$first], $arItem);
	else
		$arResultABC[$first] = array($arItem);

	$arResultABC[$first] = record_sort($arResultABC[$first], "LATNAME");

	if($mid==($n+1)){
		$arResult["MID_LETTER"] = $first;
	}
}

setlocale(LC_ALL, '');
uksort($arResultABC, 'abcdef');
/*if(count($arResultABC["OTHER"])){
  uksort($arResultABC["OTHER"], 'abcdef');
}*/




$arResult["ITEMS_ABC"] = $arResultABC;
unset($CIndexes);