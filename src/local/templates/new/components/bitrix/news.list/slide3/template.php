<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="learning_slider fs_elem_text_03 fs_elem">
	<?foreach($arResult["ITEMS"] as $n=>$arItem):?>
		<div class="learning_slide">
			<div class="learning_slide_inner">
				<p class="learning_slide_type"><?=$arItem["PROPERTIES"]["TOP_NAME"]["~VALUE"]?></p>
			
				<a class="img_block" href="#" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
				
				<div class="text_block">
					<div class="learning_slide_name">
						<?if($n):?>
							<a href="#popup_download" data-toggle="modal">
						<?else:?>
							<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">
						<?endif?>
							<?=$arItem["NAME"]?>
						</a>
					</div>
					
					<div class="learning_slide_description"><?=$arItem["PREVIEW_TEXT"]?></div>
					
					<?if($n):?>
						<a href="#popup_download" class="button button_gray_transparent" data-toggle="modal">Скачать</a>
					<?else:?>
						<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="button button_gray_transparent">Подробнее</a>
					<?endif?>
				</div>
			</div>
		</div>
	<?endforeach?>
</div>