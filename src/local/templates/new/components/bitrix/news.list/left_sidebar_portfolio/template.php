<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <div class="main_sidemenu_element">
        <span class="main_sidemenu_link icon icon-portfolio"></span>

        <div class="main_sidemenu_inner">
            <div class="main_sidemenu_inner_top">
                <p class="heading">Мои портфели</p>
            </div>

            <ul class="">
<?if($arResult["ITEMS"] && $USER->IsAuthorized()):?>

				<?foreach ($arResult["ITEMS"] as $arItem):
					//$payed = checkPaySeminar($arItem["ID"]);?>
                    <li class="<?//=($payed)?'purchased':'buy'?>">
                       <!-- <a<?if(!$payed):?> target="_blank"<?endif?> href="<?=($payed)?$arItem["DETAIL_PAGE_URL"]:$arItem["PROPERTIES"]["LINK"]["VALUE"]?>"><?=$arItem["NAME"]?></a> -->
                        <a target="_blank" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                    </li>
				<?endforeach;?>

<?endif;?>
<?$portfolio = new CPortfolio();?>
<?$checkPortfolio = $portfolio->checkPortfolioCntForUser($USER->GetID(), false); ?>
<?if($checkPortfolio["PORTFOLIO"]["LIMIT"]>$checkPortfolio["PORTFOLIO"]["OWNER"] || $checkPortfolio["ADMIN"]=="Y"):?>

  <li>
  	<hr>
  <span class="calculate_table_add_portfolio_btn" data-toggle="modal" data-target="#popup_add_portfolio">Создать&nbsp;портфель</span>
  </li>

<?endif;?>
<?if(!$USER->IsAuthorized()):?>
  <li>
  	Для работы с портфелями <a href="https://fin-plan.org/portfolio/?login=y" title="Войти как пользователь">войдите или зарегистрируйтесь</a>
  	<hr>
  <span class="calculate_table_add_portfolio_btn" data-toggle="modal" data-target="#popup_add_portfolio">Создать&nbsp;портфель</span>
  </li>
<?endif;?>
<?unset($portfolio);?>
<?/*
                <li class="purchased"><a href="#">Облигации - основа профессиональных инвестиций</a></li>
                <li class="purchased"><a href="#">Долгосрочные инвестиции в акции для всех</a></li>
                <li class="buy"><a href="#">Секретное оружие портфельных инвесторов</a></li>
                <li class="buy"><a href="#">Самый полных по программе Quik</a></li>
                <li class="buy"><a href="#">Годовое сопровождение инвест-идеями</a></li>
                <li class="buy"><a href="#">Секреты фундаментального анализа</a></li>
                <li class="buy"><a href="#">База данных аналитических досье по компаниям от Fin-plan</a></li>
                */?>
            </ul>
        </div>
    </div>