<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/*if($arParams["IBLOCK_ID"]==33){
	$arFilter = Array("IBLOCK_ID"=>32, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("CODE", "PROPERTY_BOARDID"));
	while($ob = $res->GetNext())
	{
		$arActions[$ob["CODE"]] = $ob["PROPERTY_BOARDID_VALUE"];
	}

	foreach ($arResult["ITEMS"] as $n=>$item){
		$code = $arActions[$item["CODE"]];
		if(in_array($code, $APPLICATION->ExcludeFromRadar["акции"]) || !$code){
			unset($arResult["ITEMS"][$n]);
		}
	}
}*/


if($arParams["IBLOCK_ID"]==72){
	$CIndustries = new CIndustriesRus();
	//$arIndPeriods = $CIndustries->arAllIndustriesSumm;
	$arResult["INDUSTRY_CURRENT_DATA"] = array();
	$arResult["INDUSTRY_CURRENT_DATA"] = $CIndustries->arIndustryCurrentPeriods;
	$currencyTable = ', млн.руб.';
} else if($arParams["IBLOCK_ID"]==58){
	$CSectors = new SectorsUsa();
	//$arIndPeriods = $CIndustries->arAllIndustriesSumm;
	$arResult["INDUSTRY_CURRENT_DATA"] = array();
	$arResult["INDUSTRY_CURRENT_DATA"] = $CSectors->arSectorsCurrentPeriods;
	$currencyTable = ', млн.$';
}

$arTableColumns = array(
 0=>array("Прошлая капитализация"=>"Капитализация".$currencyTable,
 			 "Прибыль за год (скользящая)"=>"Прибыль скользящая кв.".$currencyTable,
 			 "Выручка за год (скользящая)"=>"Выручка скользящая кв.".$currencyTable,
 			 "Оборотные активы"=>"Оборотные активы".$currencyTable,
 			 "Активы"=>"Активы".$currencyTable,
 			 "Собственный капитал"=>"Собственный капитал".$currencyTable,
			 ),
 1=>array("Темп роста прибыли"=>"Темп прироста прибыли, %",
 			 "Темп прироста выручки"=>"Темп прироста выручки, %",
 			 "Темп роста активов"=>"Темп прироста активов, %",
 			 "Рентабельность собственного капитала"=>"Рск, %",
 			 "Доля собственного капитала в активах"=>"Доля СК в активах, %",
 			 "P/E"=>"P/E",
 			 "P/B"=>"P/B",
 			 "P/Equity"=>"P/Equity",
 			 "P/Sale"=>"P/S",
 			 "Дивидендная доходность"=>"Див. дох-ть, %",
			 ),
);

$arResult["TABLE_COLUMNS"] = $arTableColumns;

function record_sort($records, $field, $reverse=false) {
	$hash = array();
	foreach($records as $record) {
		$hash[$record[$field]] = $record;
	}
	($reverse)? krsort($hash) : ksort($hash);

	$records = array();
	foreach($hash as $record) {
		$records []= $record;
	}
	return $records;
}
function abcdef($a, $b) {
	if ($a == $b) {
		return 0;
	}
	if ((preg_match('/[А-Яа-яЁё]/u', $a) && !preg_match('/[А-Яа-яЁё]/u', $b)) || (!preg_match('/[А-Яа-яЁё]/u', $a) && preg_match('/[А-Яа-яЁё]/u', $b))) {
		return ($a > $b) ? -1 : 1;
	} else {
		return ($a < $b) ? -1 : 1;
	}

}
