<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="dictionary_list">
    <div class="dictionary_group">
        <div class="sector_table_outer company_accordion_outer">
            <table class="sector_table transform_table footable footable-1 breakpoint-xl" style="display: table;">
                <thead>
                <tr class="footable-header active">
                <th style="display: table-cell;" class="footable-first-visible"><?=($arParams["IBLOCK_ID"]==72?'Отрасль рынка': 'Сектор рынка')?></th>
                <? $arCellBreakpoints = array(0=>"", 1=>"xxs", 2=>"xxs xs", 3=>"xxs xs sm", 4=>"xxs xs sm md", 5=>"xxs xs sm md lg");?>
                <?$cnt = 0;
                foreach($arResult["TABLE_COLUMNS"][0] as $k=>$v):?>
                     <th class="<?=($cnt==count($arResult["TABLE_COLUMNS"][0])-1?'footable-last-visible':'')?>" data-breakpoints="<?=$arCellBreakpoints[$cnt]?>" style="display: table-cell;">
                        <?=$v?>
                     </th>
                <?$cnt++;
                endforeach;?>
                </tr>
                </thead>
                <tbody>
                     <?foreach ($arResult["ITEMS"] as $letter=>$data):?>
                      <tr>
                         <td  style="display: table-cell;" class="footable-first-visible">
                             <a class="sector_table__sectorName" href="<?=$data["DETAIL_PAGE_URL"]?>" target="_blank" title="<?=$data["NAME"]?>"><?=$data["NAME"]?></a>
                         </td>
                        <?$cnt = 0;
                        foreach($arResult["TABLE_COLUMNS"][0] as $k=>$v):?>
                             <td class="<?=($cnt==count($arResult["TABLE_COLUMNS"][0])-1?'footable-last-visible':'')?>" data-breakpoints="<?=$arCellBreakpoints[$cnt]?>" style="display: table-cell;">
                                <?=round($arResult["INDUSTRY_CURRENT_DATA"][$data["NAME"]][$k], 2);?>
                             </td>
                        <?$cnt++;
                        endforeach;?>
                      </tr>
                     <?endforeach;?>
              </tbody>
            </table>
            <br>
            <table class="sector_table transform_table footable footable-1 breakpoint-xl" style="display: table;">
                    <thead>
                    <tr class="footable-header active">
                    <th style="display: table-cell;" class="footable-first-visible"><?=($arParams["IBLOCK_ID"]==72?'Отрасль рынка': 'Сектор рынка')?></th>
                    <? $arCellBreakpoints = array(0=>"",
                                                          1=>"xxs",
                                                            2=>"xxs xs",
                                                            3=>"xxs xs sm",
                                                            4=>"xxs xs sm md",
                                                            5=>"xxs xs sm md lg",
                                                            6=>"xxs xs sm md lg",
                                                            7=>"xxs xs sm md lg",
                                                            8=>"xxs xs sm md lg",
                                                            9=>"xxs xs sm md lg"
                                                            );?>
                    <?$cnt = 0;
                    foreach($arResult["TABLE_COLUMNS"][1] as $k=>$v):?>
                         <th class="<?=($cnt==count($arResult["TABLE_COLUMNS"][1])-1?'footable-last-visible':'')?>" data-breakpoints="<?=$arCellBreakpoints[$cnt]?>" style="display: table-cell;">
                            <?=$v?>
                         </th>
                    <?$cnt++;
                    endforeach;?>
                    </tr>
                    </thead>
                    <tbody>
                         <?foreach ($arResult["ITEMS"] as $letter=>$data):?>
                          <tr>
                             <td  style="display: table-cell;" class="footable-first-visible"><a class="sector_table__sectorName" href="<?=$data["DETAIL_PAGE_URL"]?>" target="_blank" title="<?=$data["NAME"]?>"><?=$data["NAME"]?></a></td>
                            <?$cnt = 0;
                            foreach($arResult["TABLE_COLUMNS"][1] as $k=>$v):?>
                                 <td class="<?=($cnt==count($arResult["TABLE_COLUMNS"][1])-1?'footable-last-visible':'')?>" data-breakpoints="<?=$arCellBreakpoints[$cnt]?>" style="display: table-cell;">
                                    <?=round($arResult["INDUSTRY_CURRENT_DATA"][$data["NAME"]][$k], 2);?>
                                 </td>
                            <?$cnt++;
                            endforeach;?>
                          </tr>
                         <?endforeach;?>
                  </tbody>
                </table>
        </div>
    </div>
	
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
