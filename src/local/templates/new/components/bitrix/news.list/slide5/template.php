<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $n=>$arItem):
	$img = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']["ID"], array('width'=>166, 'height'=>166), BX_RESIZE_IMAGE_EXACT, true);?>
	<div class="matherials_slide">
		<div class="matherials_slide_inner">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="img_block" style="background-image: url(<?=$img["src"]?>);"></a>
			
			<div class="text_block">
				<p class="matherials_slide_name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></p>
				
				<div class="matherials_slide_description"><?=$arItem["PREVIEW_TEXT"]?></div>
				
				<p class="matherials_slide_date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></p>
			</div>
		</div>
	</div>
<?endforeach?>