<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetPageProperty("title", "Отзывы клиентов и кейсы Fin-plan | Сайт о финансовом планировании Fin-plan.org");
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>

				<div class="title_container">
					<h1>Отзывы клиентов и кейсы Fin-plan</h1>
				</div>

                <div class="other_articles_list">
                    <div class="articles_list colored row">
                        <?if($_REQUEST["is_ajax"]){
                           $APPLICATION->RestartBuffer();

                            ob_start();
                        }?>
                        <? foreach($arResult["ITEMS"] as $arItem): ?>
                            <div class="articles_list_element col-xs-6 col-lg-4">
                                <div class="articles_list_element_inner">
                                    <div class="articles_list_element_img">
                                        <div class="articles_list_element_img_inner">
                                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
                                        </div>
                                    </div>

                                    <div class="articles_list_element_text">
                                        <p class="articles_list_element_title">
                                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                        </p>
                                        <p  class="articles_list_element_description">
                                            <?=$arItem["PREVIEW_TEXT"]?>
                                        </p>

                                        <p class="articles_list_element_date">
                                            <?=FormatDate(array(
                                                "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                "today" => "today",              // выведет "сегодня", если дата текущий день
                                                "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                                ), MakeTimeStamp($arItem["DISPLAY_ACTIVE_FROM"]), time()
                                            );?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?endforeach?>

                        <?php
                        if($_REQUEST["is_ajax"]) {
                            $data = ob_get_contents();
                            ob_clean();

                            echo json_encode([
                                "content" => $data,
                                "next" => $_REQUEST["PAGEN_1"]+1,
                                "last" => $arResult["LAST_PAGE"]
                            ]);
                            exit();
                        }
                        ?>
                    </div>

                    <?if($arResult["TOTAL_COUNT"]>$arParams["NEWS_COUNT"]):?>
                        <div class="other_articles_list_bottom text-center">
                            <a class="more_articles button button_green" data-next="2" href="#">Загрузить ещё <span class="icon icon-arr_down"></span></a>
                        </div>
                    <?endif;?>
                </div>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
	</div>
</div>
