<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
$arFilter["ACTIVE"] = "Y";
$arFilter["ACTIVE_DATE"] = "Y";

if($arParams["PARENT_SECTION_CODE"]){
	$arFilter["SECTION_CODE"] = $arParams["PARENT_SECTION_CODE"];
}
if($_REQUEST["q"]){
	$arFilter[] = array(
		"LOGIC" => "OR",
		array("NAME" => "%".$_REQUEST["q"]."%"),
		array("PREVIEW_TEXT" => "%".$_REQUEST["q"]."%")
	);
}

$res = CIBlockElement::GetList(Array(), $arFilter, false, false);

$arResult["TOTAL_COUNT"] = $res->SelectedRowsCount();
$arResult["LAST_PAGE"] = ceil($arResult["TOTAL_COUNT"]/$arParams["NEWS_COUNT"]);
