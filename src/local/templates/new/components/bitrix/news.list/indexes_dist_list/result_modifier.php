<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;

//Получаем список уникальных тикеров индексов из HL блока "Акции в индексах"
$CIndexes = new CIndexes;
$arSharesCntInIndexes = $CIndexes->getIndexesHL_tickers();


 $arTmp = array("MAIN"=>array(), "OTHER"=>array());
 foreach($arResult["ITEMS"] as $item){
	if(array_key_exists($item["CODE"],$arSharesCntInIndexes)){
	 $arTmp["MAIN"][] = $item;
	}  else {
	 $arTmp["OTHER"][] = $item;
	}
 }
	if(count($arTmp["MAIN"])>0){
		$arResult["ITEMS"] = $arTmp;
		unset($arTmp);
	}



function record_sort($records, $field, $reverse=false) {
	$hash = array();
	foreach($records as $record) {
		$hash[$record[$field]] = $record;
	}
	($reverse)? krsort($hash) : ksort($hash);

	$records = array();
	foreach($hash as $record) {
		$records []= $record;
	}
	return $records;
}
function abcdef($a, $b) {
	if ($a == $b) {
		return 0;
	}
	if ((preg_match('/[А-Яа-яЁё]/u', $a) && !preg_match('/[А-Яа-яЁё]/u', $b)) || (!preg_match('/[А-Яа-яЁё]/u', $a) && preg_match('/[А-Яа-яЁё]/u', $b))) {
		return ($a > $b) ? -1 : 1;
	} else {
		return ($a < $b) ? -1 : 1;
	}

}

$arResultABC["MAIN"] = array();
$arResultABC["OTHER"] = array();

foreach($arResult["ITEMS"] as $key=>$arItems) {
$mid = ceil(count($arItems)/2);

foreach($arItems as $n=>$arItem) {
	$first = mb_strtoupper(mb_substr($arItem["NAME"], 0, 1));
	if(isset($arResultABC[$key][$first]))
		array_push($arResultABC[$key][$first], $arItem);
	else
		$arResultABC[$key][$first] = array($arItem);

	$arResultABC[$key][$first] = record_sort($arResultABC[$key][$first], "NAME");
	
	if($mid==($n+1)){
		$arResult[$key]["MID_LETTER"] = $first;
	}
}

}

setlocale(LC_ALL, '');
uksort($arResultABC["MAIN"], 'abcdef');
if(count($arResultABC["OTHER"])){
  uksort($arResultABC["OTHER"], 'abcdef');
}


$arResult["ITEMS_ABS"] = $arResultABC;
unset($CIndexes);