<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$showFull = false;
?>
<?
if($arResult["UF_ADMIN_ONLY"]==1 && $GLOBALS["USER"]->IsAdmin()){
  $showFull = true;
}
if($arResult["UF_ADMIN_ONLY"]==1 && !$GLOBALS["USER"]->IsAdmin()){
  $showFull = false;
}
if($arResult["UF_ADMIN_ONLY"]==0){
  $showFull = true;
}
if($arResult["SECTION"]["PATH"][0]["ACTIVE"]!="Y"){
  $showFull = false;
}
?>


<?if($showFull):?>
<?$picture = CFile::GetPath($arResult["SECTION"]["PATH"][0]["PICTURE"]);?>
<div class="calculate_top stories" style="background-image:url(<?=$picture;?>); background-color:<?=$arResult["UF_BG_COLOR"]?>">
<p class="calculate_top_title"><?= htmlspecialchars_decode($arResult["SECTION"]["PATH"][0]["NAME"])?></p>
  <button class="button" style="border-color:<?=$arResult["UF_BUTTON_BG_COLOR"]?>; background-color:<?=$arResult["UF_BUTTON_BG_COLOR"]?>; color:<?=$arResult["UF_BUTTON_TEXT_COLOR"];?>">Смотреть</button>
</div>

<div id="playlistPopup" class="playlistPopup">
	<div class="before"></div>

	<div class="playlist playlistPopup__container">
		<button class="playlistPopup__close icon icon-close"></button>
		<div class="col col-left">
			<ul class="playlist__list">
			 <?foreach($arResult["ITEMS"] as $k=>$arItem):?>
				<li class="playlist__elem <?=($k>0?'':'active')?>" data-block-id="<?=$arItem["ID"]?>" data-id="<?=$arItem["PROPERTIES"]["VIDEO"]["VALUE"]?>">
					<span class="number"><?= $arItem["NAME"]?></span>
					<span class="name"><?= $arItem["PROPERTIES"]["SUBHEADER"]["VALUE"]?></span>
					<span class="img">
						<img src="" alt="">
					</span>
				</li>
			 <?endforeach;?>
			</ul>
		</div>

		<div class="col col-right">
				<div class="playlist__videoOuter">
				<?if($GLOBALS["USER"]->IsAdmin() && !isset($_COOKIE["subscribe_mini_kurs"])):?>
				  <div class="playlist_overlay_subscribe">
				  <!-- leeloo init code -->
    <script>
    window.LEELOO = function(){
        window.LEELOO_INIT = { id: '5dbad5c3b2f45f000ccc6366' };
        var js = document.createElement('script');
        js.src = 'https://app.leeloo.ai/init.js';
        js.async = true;
        document.getElementsByTagName('head')[0].appendChild(js);
    }; LEELOO();
    </script>
<!-- end leeloo init code -->
					 <div class="subscribe_form_leeloo"><div class="wepster-hash-sz9wfn"></div>
					 <br>
					 <a class="button" id="subscribe_mini_kurs" href="javascript:void(0);">Далее</a>
					 </div>
				  </div>
				<?endif;?>
					<div id="playlist__video" class="playlist__videoInner" data-params="<?=$arResult["UF_PLAYER_PARAMS"]?>"></div>
				</div>

				<div class="playlist__text">
				<?foreach($arResult["ITEMS"] as $k=>$arItem):?>
				  <div data-block-id="<?=$arItem["ID"]?>" class="playlist-content-block block_<?=$arItem["ID"]?> <?=($k>0?'hidden':'')?>">
				  		<div class="playlist-undervideo-btn-block">
							<?= htmlspecialchars_decode($arItem["PREVIEW_TEXT"])?> &nbsp;
							<a class="button" href="<?= $arItem["PROPERTIES"]["BUTTON_BUY_LINK"]["VALUE"]?>" target="_blank" data-link="<?= $arItem["PROPERTIES"]["BUTTON_BUY_LINK"]["VALUE"]?>"><?= $arItem["PROPERTIES"]["BUTTON_BUY_TEXT"]["VALUE"]?></a>
						</div>
				  		<div class="playlist-undervideo-descr-block mb10">
							<?= htmlspecialchars_decode($arItem["DETAIL_TEXT"])?>
						</div>
						<ul>
							<?= htmlspecialchars_decode($arItem["PROPERTIES"]["LEFT_LINKS"]["VALUE"]["TEXT"])?>
						</ul>
				  </div>
				<?endforeach;?>
				</div>
			</div>
	</div>
</div>
<?else:?>
					<div class="calculate_top">
						<p class="calculate_top_title">Заставьте свободные деньги работать</p>

						<p class="calculate_top_description">Выберите <span>активы</span> для своего портфеля</p>
					</div>
<?endif;?>