<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
 $section_props = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ID' => $arParams["PARENT_SECTION"]),	true, array("UF_PLAYER_PARAMS", "UF_ADMIN_ONLY", "UF_BG_COLOR", "UF_BUTTON_BG_COLOR", "UF_BUTTON_TEXT_COLOR"));
$props_array = $section_props->GetNext();
$arResult["UF_ADMIN_ONLY"] = $props_array["UF_ADMIN_ONLY"];
$arResult["UF_BG_COLOR"] = $props_array["UF_BG_COLOR"];
$arResult["UF_BUTTON_BG_COLOR"] = $props_array["UF_BUTTON_BG_COLOR"];
$arResult["UF_BUTTON_TEXT_COLOR"] = $props_array["UF_BUTTON_TEXT_COLOR"];
$arResult["UF_PLAYER_PARAMS"] = $props_array["UF_PLAYER_PARAMS"];
unset($props_array, $section_props);
?>