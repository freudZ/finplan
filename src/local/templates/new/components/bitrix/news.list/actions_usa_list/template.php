<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="dictionary_list">
	<div class="row">
		<?foreach ($arResult["ITEMS_ABS"] as $letter=>$data):?>
			<div class="col col-xs-12">
				<div class="dictionary_group">
					<div class="row">
						<div class="col col-xs-12">
							<p class="dictionary_group_character"><?=$letter?></p>
						</div>
						<div class="col col-xs-6">
							<ul>
								<?$i=0;foreach ($data as $item):$i++;?>
									<?/*global $USER;//+login if
                                 $rsUser = CUser::GetByID($USER->GetID());
                                 $arUser = $rsUser->Fetch();
                                 if($arUser["LOGIN"]=="freud"):
												 echo "<pre  style='color:black; font-size:11px;'>";
                                        print_r($item["RADAR"]);
                                        echo "</pre>";
											endif;*///-login if?>
									<li><a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a>
									<?if($item["LASTPRICE"]>0):?>
										<span class="oblig_helper green">(<span class="tick_price" data-quotations-month="<?= $item["QUOTATIONS_MONTH"] ?>"><?= $item["LASTPRICE"].$item["CURRENCY_SIGN"].($item["LASTPRICE"]>0?",":"") ?>
													</span>
													<span class="<?= ($item["MONTH_INCREASE"]>0?'green':($item["MONTH_INCREASE"]<0?'red':'black')) ?> tick_percentage">
														<?= floatval($item["MONTH_INCREASE"]) ?>%</span>)
										</span>
									<?endif;?>
									</li>
									<?if(ceil(count($data)/2)==$i):?>
											</ul>
										</div>
										<div class="col col-xs-6">
											<ul>
									<?endif?>
								<?endforeach;?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<?endforeach;?>
	</div>
	
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
			