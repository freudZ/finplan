<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="buy_subscribe_list">
	<?foreach($arResult["ITEMS"] as $item):?>
		<div class="buy_subscribe_element">
			<div class="buy_subscribe_list_inner">
				<div class="buy_subscribe_element_head">
					<p class="buy_subscribe_element_title"><?=$item["NAME"]?></p>
					<p class="buy_subscribe_element_description"><?=$item["PREVIEW_TEXT"]?></p>
				</div>

				<div class="buy_subscribe_element_body">
					<?if($item["PROPERTIES"]["PROPS"]["VALUE"]):?>
						<p class="font_13 strong">Сервис «Fin-plan Radar» <br />
							<span class="dib font_11 green">(подписка на 1 год)</span>:</p>
						<ul>
							<?foreach($item["PROPERTIES"]["PROPS"]["VALUE"] as $val):?>
								<li><?=$val?></li>
							<?endforeach?>
						</ul>
					<?endif?>
					<?=$item["PROPERTIES"]["MORE_INFO"]["~VALUE"]["TEXT"]?>
				</div>

				<div class="buy_subscribe_element_price">
					<p>
						<?if($item["PROPERTIES"]["PRICE"]["VALUE"]):?>
							<span class="curr_price"><?=$item["PROPERTIES"]["PRICE"]["VALUE"]?></span> руб<br/>
						<?endif?>
						<?if($item["PROPERTIES"]["PRICE_YEAR"]["VALUE"]):?>
							<span class="old_price"><span class="strong"><?=$item["PROPERTIES"]["PRICE_YEAR"]["VALUE"]?> руб./мес.</span> при оплате за 1 год</span>
						<?endif?>
					</p>
				</div>

				<div class="buy_subscribe_element_footer">
					<a href="<?=$item["PROPERTIES"]["PAY_LINK"]["VALUE"]?>" class="button">Купить</a>
				</div>
			</div>
		</div>
	<?endforeach?>
</div>
