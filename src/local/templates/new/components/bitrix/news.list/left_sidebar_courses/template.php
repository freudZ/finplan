<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["ITEMS"]):?>
    <div class="main_sidemenu_element">
        <span class="main_sidemenu_link icon icon-presentation"></span>

        <div class="main_sidemenu_inner">
            <div class="main_sidemenu_inner_top">
                <p class="heading">Курсы Fin-plan</p>
            </div>

            <ul class="cources_list">
				<?foreach ($arResult["ITEMS"] as $arItem):
					$payed = checkPaySeminar($arItem["ID"]);?>
                    <li class="<?=($payed)?'purchased':'buy'?>">
                        <a<?if(!$payed):?> target="_blank"<?endif?> href="<?=($payed)?$arItem["DETAIL_PAGE_URL"]:$arItem["PROPERTIES"]["LINK"]["VALUE"]?>"><?=$arItem["NAME"]?></a>
                    </li>
				<?endforeach;?>
                <?/*
                <li class="purchased"><a href="#">Облигации - основа профессиональных инвестиций</a></li>
                <li class="purchased"><a href="#">Долгосрочные инвестиции в акции для всех</a></li>
                <li class="buy"><a href="#">Секретное оружие портфельных инвесторов</a></li>
                <li class="buy"><a href="#">Самый полных по программе Quik</a></li>
                <li class="buy"><a href="#">Годовое сопровождение инвест-идеями</a></li>
                <li class="buy"><a href="#">Секреты фундаментального анализа</a></li>
                <li class="buy"><a href="#">База данных аналитических досье по компаниям от Fin-plan</a></li>
                */?>
            </ul>
        </div>
    </div>
<?endif;?>