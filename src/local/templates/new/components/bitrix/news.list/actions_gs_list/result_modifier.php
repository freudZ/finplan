<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	$res = new Actions();
	$resIndRus = new CIndustriesRus();

	$arIndustryGroupsList = array();
   foreach ($arResult["ITEMS"] as $n=>$item){
        $resItem = $res->getItemByCode($item["CODE"]);
			 $arIndRus = $resIndRus->GetSectorByEmitentId($resItem['PROPS']['EMITENT_ID']);
			 $arIndRus = $resIndRus->getItem($arIndRus['CODE'], true);
			 $arIndRus["LAST_PERIOD"] = reset($arIndRus["PERIODS"]);
			 unset($arIndRus["PERIODS"], $arIndRus["EXPANDED_PERIODS"]);
		  $arIndustryGroupsList[$arIndRus["NAME"]]["INDUSTRY"] = $arIndRus;
		  $resItem["LAST_PERIOD"] = reset($resItem["PERIODS"]);
		  unset($resItem["PERIODS"], $item["PROPERTIES"]);
		  $item["LAST_PERIOD"] = $resItem["LAST_PERIOD"];
		  $item["LAST_PERIOD"]["Таргет"] = $resItem["DYNAM"]["Таргет"];
		  $item["LAST_PERIOD"]["Просад"] = $resItem["DYNAM"]["Просад"];
		  $item["LAST_PERIOD"]["GROW_HORIZON_PROGNOSE"] = $resItem["PROPS"]["GROW_HORIZON_PROGNOSE"];
		  $arIndustryGroupsList[$arIndRus["NAME"]]["ITEMS"][] = $item;
    }
  $arResult["GS_LIST"] = $arIndustryGroupsList;
  unset($arResult["ITEMS"]);
/*	echo "<pre  style='color:black; font-size:11px;'>";
      print_r($arIndustryGroupsList);
      echo "</pre>";*/

function record_sort($records, $field, $reverse=false) {
	$hash = array();
	foreach($records as $record) {
		$hash[$record[$field]] = $record;
	}
	($reverse)? krsort($hash) : ksort($hash);

	$records = array();
	foreach($hash as $record) {
		$records []= $record;
	}
	return $records;
}
function abcdef($a, $b) {
	if ($a == $b) {
		return 0;
	}
	if ((preg_match('/[А-Яа-яЁё]/u', $a) && !preg_match('/[А-Яа-яЁё]/u', $b)) || (!preg_match('/[А-Яа-яЁё]/u', $a) && preg_match('/[А-Яа-яЁё]/u', $b))) {
		return ($a > $b) ? -1 : 1;
	} else {
		return ($a < $b) ? -1 : 1;
	}

}




/*$arResultABC = array();

$mid = ceil(count($arResult["ITEMS"])/2);

foreach($arResult["ITEMS"] as $n=>$arItem) {
	$first = mb_strtoupper(substr($arItem["NAME"], 0, 1));
	if(isset($arResultABC[$first]))
		array_push($arResultABC[$first], $arItem);
	else
		$arResultABC[$first] = array($arItem);

	$arResultABC[$first] = record_sort($arResultABC[$first], "NAME");

	if($mid==($n+1)){
		$arResult["MID_LETTER"] = $first;
	}
}*/

setlocale(LC_ALL, '');
uksort($arResult["GS_LIST"], 'abcdef');

foreach($arResult["GS_LIST"] as $k=>$v){
 usort($v["ITEMS"], function ($item1, $item2) {
 return $item1["NAME"] <=> $item2["NAME"];
});
}


//$arResult["ITEMS_ABS"] = $arResultABC;