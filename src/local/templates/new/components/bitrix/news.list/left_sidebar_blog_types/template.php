<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["ITEMS"]):?>
<div class="main_sidemenu_element">
    <span class="main_sidemenu_link icon icon-foulder"></span>

    <div class="main_sidemenu_inner">
        <div class="main_sidemenu_inner_top">
            <p class="heading">Подборки статей</p>
        </div>

        <ul>
            <?foreach ($arResult["ITEMS"] as $arItem):?>
                <li>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</div>
<?endif;?>