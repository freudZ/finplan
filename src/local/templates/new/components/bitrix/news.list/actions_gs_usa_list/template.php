<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="dictionary_list">
	<div class="row">
				<? $arLangTable = array(   "P/E"=>"P/E",
													"P/Sale"=>"P/Sale",
													"Рентабельность собственного капитала"=>"Р ск",
													"Доля собственного капитала в активах"=>"Д ск",
													"Темп прироста выручки"=>"Тр Вр",
													"Темп роста прибыли"=>"Тр Пр",
													"Таргет"=>"Потенциал",
													"Просад"=>"Просад",
													"GROW_HORIZON_PROGNOSE"=>"Горизонт",
													);
						$arILangTable = array("Рентабельность собственного капитала"=>"GS_TABLE_HEADING1",
													 "Доля собственного капитала в активах"=>"GS_TABLE_HEADING2",
													 "Темп прироста выручки"=>"GS_TABLE_HEADING3",
													 "Темп роста прибыли"=>"GS_TABLE_HEADING4",
													 "GROW_HORIZON_PROGNOSE"=>"GS_TABLE_HEADING5",
													 "P/E"=>"GS_TABLE_HEADING6",
													 "P/Sale"=>"GS_TABLE_HEADING7",
													 "Таргет"=>"GS_TABLE_HEADING8",
													 "Просад"=>"GS_TABLE_HEADING9",
													);
													?>
                        <div class="scrollableTable">
                          <table>
                            <thead>
                             <tr>
                               <th>Показатели</th>
                              <?foreach($arLangTable as $k=>$v):?>
										 <? $kl = $k; ?>
                                <th><?=$v?> <span class="tooltip_btn p-tooltip-green" data-placement="bottom" title="" data-key="<?= $kl ?>" data-original-title="<?=GetMessage($arILangTable[$kl])?>">i</span></th>
                              <?endforeach;?>
                             </tr>
                            </thead>
                            <tbody>
									 <?foreach($arResult["GS_LIST"] as $codeIndustry=>$arIndustry):?>
                             <tr class="industry_headind">
									     <td><?=$arIndustry["INDUSTRY"]["NAME"]?></td>
                              <?foreach($arLangTable as $k=>$v):?>
		                                <td>
												  <? if(array_key_exists($k, $arIndustry["INDUSTRY"]["LAST_PERIOD"])) {
														 $val = round($arIndustry["INDUSTRY"]["LAST_PERIOD"][$k], 3);
													 } else {
														 $val = '';
													 } ?>
												  	<?=$val?>
												  </td>
                              <?endforeach;?>
                             </tr>
									    <?foreach($arIndustry["ITEMS"] as $codeItem=>$arItem):?>
										 	<tr>
											  <td>
                                                  <a href="/lk/gs_usa/<?=$arItem["CODE"]?>/" target="_blank"><?=$arItem["NAME"]?></a>
                                                  <a href="/lk/gs_usa/<?=$arItem["CODE"]?>/" class="gs_tooltip_btn tooltip_btn p-tooltip-green" data-placement="top" title="" data-original-title="Быстрый просмотр">A</a>
                                              </td>
		                              <?foreach($arLangTable as $k=>$v):?>
		                                <td>
												  <?if($k!="GROW_HORIZON_PROGNOSE"){
														 	$val = round($arItem["LAST_PERIOD"][$k], 3);
														 } else {
															$val = $arItem["LAST_PERIOD"][$k];
														 } ?>
												  	<?=$val?>
												  </td>
		                              <?endforeach;?>
											</tr>
									    <?endforeach;?>
									  <?endforeach;?>
                            </tbody>
                          </table>
                        </div>
	</div>
	
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?//=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
