<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="reviews_slider fs_elem">
	<?foreach($arResult["ITEMS"] as $n=>$arItem):?>
		<div class="reviews_slide">
			<div class="img_block">
				<a href="https://www.youtube.com/embed/<?=$arItem["PROPERTIES"]["VIDEO"]["VALUE"]?>?rel=0&vq=hd1080&modestbranding=1&autohide=1&controls=1" class="img_block_inner fancybox fancybox.iframe" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
				<a href="https://www.youtube.com/embed/<?=$arItem["PROPERTIES"]["VIDEO"]["VALUE"]?>?rel=0&vq=hd1080&modestbranding=1&autohide=1&controls=1" class="play fancybox fancybox.iframe"></a>
			</div>
			
			<div class="text_block">
				<p class="reviews_slide_name"><?=$arItem["NAME"]?></p>
				<p class="reviews_slide_post"><?=$arItem["PROPERTIES"]["DOLZH"]["VALUE"]?></p>
				
				<div class="reviews_slide_description"><?=$arItem["PREVIEW_TEXT"]?></div>
				
				<a href="https://www.youtube.com/embed/<?=$arItem["PROPERTIES"]["VIDEO"]["VALUE"]?>?rel=0&vq=hd1080&modestbranding=1&autohide=1&controls=1" class="more fancybox fancybox.iframe">Смотреть отзыв</a>
			</div>
		</div>
	<?endforeach?>
</div>