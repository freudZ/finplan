<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $n=>$arItem):?>
	<div class="modal fade modal_lg" id="footer<?=$arItem["ID"]?>" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h2><?=$arItem["NAME"]?></h2>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		  </div>
		  <div class="modal-body">
			<div class="modal_shares container-fluid">
				<?=$arItem["PREVIEW_TEXT"]?>
			</div>
		  </div>
		</div>
	  </div>
	</div>
<?endforeach?>