<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


//Добавляем для облиг доходность и дату погашения
if($arParams["IBLOCK_ID"]==30 && count($arParams["ACTIVES_FILTER"])){
$res = new Obligations();
    foreach ($arResult["ITEMS"] as $n=>$item){
        $resItem = $res->getItem($item["CODE"]);
        $arResult["ITEMS"][$n]["MATDATE"] = $resItem["PROPS"]["MATDATE"];
        $arResult["ITEMS"][$n]["Доходность годовая"] = $resItem["DYNAM"]["Доходность годовая"];
        $arResult["ITEMS"][$n]["HIDEN"] = $resItem["PROPS"]["HIDEN"];
    }

}

if($arParams["IBLOCK_ID"]==33 && count($arParams["ACTIVES_FILTER"])){
$res = new Actions();
    foreach ($arResult["ITEMS"] as $n=>$item){
        $resItem = $res->getItemByCode($item["CODE"]);
        //$arResult["ITEMS"][$n]["RADAR"] = $resItem;
        $arResult["ITEMS"][$n]["LASTPRICE"] = $resItem["PROPS"]["LASTPRICE"];
        $arResult["ITEMS"][$n]["CURRENCY"] = $resItem["PROPS"]["CURRENCY"];
        $arResult["ITEMS"][$n]["CURRENCY_SIGN"] = getCurrencySign("RUB");
        $arResult["ITEMS"][$n]["QUOTATIONS_MONTH"] = $resItem["PROPS"]["QUOTATIONS_MONTH"];
        $arResult["ITEMS"][$n]["MONTH_INCREASE"] = $resItem['DYNAM']['MONTH_INCREASE'];
    }

}

if($arParams["IBLOCK_ID"]==33){
	$arFilter = Array("IBLOCK_ID"=>32, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("CODE", "PROPERTY_BOARDID", "PROPERTY_IS_PIF_ACTIVE"));
	while($ob = $res->GetNext())
	{
		$arActions[$ob["CODE"]] = array("BOARDID"=>$ob["PROPERTY_BOARDID_VALUE"], "IS_PIF_ACTIVE"=>$ob["PROPERTY_IS_PIF_ACTIVE_VALUE"]);
	}



	foreach ($arResult["ITEMS"] as $n=>$item){
		$code = $arActions[$item["CODE"]]["BOARDID"];
		$is_pif = $arActions[$item["CODE"]]["IS_PIF_ACTIVE"];
	if(in_array($code, $APPLICATION->ExcludeFromRadar["акции"]) || !$code || $is_pif=='Y'){
			unset($arResult["ITEMS"][$n]);
		}
	
	
	}
}

function record_sort($records, $field, $reverse=false) {
	$hash = array();
	foreach($records as $record) {
		$hash[$record[$field]] = $record;
	}
	($reverse)? krsort($hash) : ksort($hash);

	$records = array();
	foreach($hash as $record) {
		$records []= $record;
	}
	return $records;
}
function abcdef($a, $b) {
	if ($a == $b) {
		return 0;
	}
	if ((preg_match('/[А-Яа-яЁё]/u', $a) && !preg_match('/[А-Яа-яЁё]/u', $b)) || (!preg_match('/[А-Яа-яЁё]/u', $a) && preg_match('/[А-Яа-яЁё]/u', $b))) {
		return ($a > $b) ? -1 : 1;
	} else {
		return ($a < $b) ? -1 : 1;
	}

}

$arResultABC = array();

$mid = ceil(count($arResult["ITEMS"])/2);

foreach($arResult["ITEMS"] as $n=>$arItem) {
	$first = mb_strtoupper(mb_substr($arItem["NAME"], 0, 1));
	if(isset($arResultABC[$first]))
		array_push($arResultABC[$first], $arItem);
	else
		$arResultABC[$first] = array($arItem);

	$arResultABC[$first] = record_sort($arResultABC[$first], "NAME");
	
	if($mid==($n+1)){
		$arResult["MID_LETTER"] = $first;
	}
}

setlocale(LC_ALL, '');
uksort($arResultABC, 'abcdef');


$arResult["ITEMS_ABS"] = $arResultABC;