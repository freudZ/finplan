<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="dictionary_list">
	<div class="row">
		<?foreach ($arResult["ITEMS_ABS"] as $letter=>$data):?>
			<div class="col col-xs-12">
				<div class="dictionary_group">
					<div class="row">
						<div class="col col-xs-12">
							<p class="dictionary_group_character"><?=$letter?></p>
						</div>
						<div class="col col-xs-6">
							<ul>
								<?$i=0;foreach ($data as $item):$i++;?>
									<li><a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a>&nbsp;
                                        <?//if($arParams["IBLOCK_ID"]==30 && $USER->IsAdmin()):?>
                                        <?if($arParams["IBLOCK_ID"]==30):?>
                                            <?$matdate = new DateTime($item["MATDATE"]);
                                            $textOnly = false;
                                            if($matdate<new DateTime()){
                                                $matdateText = "Погашена";
                                                $textOnly = true;
                                            } else if($item["HIDEN"]=="Да"){
                                                $matdateText = "Не обращается";
                                                $textOnly = true;
                                            }else {
                                                $textOnly = false;
                                                $matdateText =  $matdate->format("d.m.Y");
                                            }
                                            if(!empty($item["Доходность годовая"])){
                                                $item["Доходность годовая"] = $item["Доходность годовая"]."% ";
                                            }
                                            
                                            ?>
                                            <span class="oblig_helper <?=($textOnly?'grey':'green')?>">(<?=(!$textOnly)?$item["Доходность годовая"].$matdateText:$matdateText?>)</span>
                                        <?endif;?>
													 <?if($arParams["IBLOCK_ID"]==33):?>

													 <?if($item["LASTPRICE"]>0):?>
 										<span class="oblig_helper green">(<span class="tick_price" data-quotations-month="<?= $item["QUOTATIONS_MONTH"] ?>"><?= $item["LASTPRICE"].$item["CURRENCY_SIGN"].($item["LASTPRICE"]>0?",":"") ?>
													</span>

													<span class="<?= ($item["MONTH_INCREASE"]>0?'green':($item["MONTH_INCREASE"]<0?'red':'black')) ?> tick_percentage">
														<?= (floatval($item["MONTH_INCREASE"])) ?>%</span>)
										</span><?endif;?>

													 <?endif;?>
                                    </li>
									<?if(ceil(count($data)/2)==$i):?>
											</ul>
										</div>
										<div class="col col-xs-6">
											<ul>
									<?endif?>
								<?endforeach;?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<?endforeach;?>
	</div>
	
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
			