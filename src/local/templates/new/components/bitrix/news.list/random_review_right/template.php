<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?foreach ($arResult["ITEMS"] as $arItem):?>
<div class="sidebar_review_outer sidebar_group">
    <div class="sidebar_group_inner">
        <div class="sidebar_element">
            <p class="heading"><?= ($arItem["IBLOCK_ID"]==$arParams["IBLOCK_ID"]?'Отзыв:':'Кейс:') ?></p>

            <div class="sidebar_review_img">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <?if($arItem["IBLOCK_ID"]==$arParams["IBLOCK_ID"]) {
						  $img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>130, 'height'=>130), BX_RESIZE_IMAGE_EXACT, true);
						  }else {
							$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_EXACT, true);
						  }?>
                    <img src="<?=$img["src"]?>" alt="<?=$arItem["NAME"]?>">
                </a>
            </div>

            <div class="sidebar_review_text">
                <p class="sidebar_review_name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?if($arItem["IBLOCK_ID"]==$arParams["IBLOCK_ID"]): ?><?=$arItem["PROPERTIES"]["FIO"]["VALUE"]?><?else:?><?=$arItem["NAME"]?><?endif;?></a></p>

                <p class="sidebar_review_description"><?=$arItem["PREVIEW_TEXT"]?></p>

                <?php /* <p><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="threedots">...</a></p> */ ?>
            </div>
        </div>

        <div class="sidebar_group_more">
            <a class="link_arrow" href="<?= ($arItem["IBLOCK_ID"]==$arParams["IBLOCK_ID"]?'/reviews/':$arItem["LIST_PAGE_URL"]) ?>">Все <?= ($arItem["IBLOCK_ID"]==$arParams["IBLOCK_ID"]?'отзывы':'кейсы') ?></a>
        </div>
    </div>
</div>
<?endforeach;?>
