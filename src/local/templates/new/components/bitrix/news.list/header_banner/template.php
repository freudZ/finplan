<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);  
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="second_offer_outer header_bnr">
        <?if(!$arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] || $arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="IMG_FULL"):?>
            <p class="second_offer_text"><?=$arItem["PROPERTIES"]["TEXT"]["VALUE"]?></p>

            <div class="second_offer" style="background: url('<?=$arItem["PROPERTIES"]["IMAGE"]["VALUE"]?>') -15px 0px no-repeat; background-color: #e8eef0; background-position: right top;">
                <a class="button"<?if($arItem["PROPERTIES"]["LINK_BLANK"]["VALUE"]):?> target="_blank"<?endif?><?if($arItem["PROPERTIES"]["LINK_NOFOLLOW"]["VALUE"]):?> rel="nofollow"<?endif?> href="<?=$arItem["PROPERTIES"]["LINK_URL"]["VALUE"]?>">
                    <?=$arItem["PROPERTIES"]["LINK_TEXT"]["VALUE"]?>
                </a>
            </div>
        <?elseif($arItem["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="IMG_RIGHT"):?>
            <div class="offer_round_outer">
                <div class="col col_left with_round green" style="background-color: #d6dadb;">
                    <div class="col_inner">
                        <p
                            class="offer_round_title"
                            <?if($arItem["PROPERTIES"]["HEADER_COLOR"]["VALUE"]):?>
                                style="color: <?=$arItem["PROPERTIES"]["HEADER_COLOR"]["VALUE"]?>"
                            <?endif?>
                        >
                            <?=$arItem["NAME"]?>
                        </p>
                        <p
                            class="offer_round_subtitle"
                            <?if($arItem["PROPERTIES"]["BODY_COLOR"]["VALUE"]):?>
                                style="color: <?=$arItem["PROPERTIES"]["BODY_COLOR"]["VALUE"]?>"
                            <?endif?>
                        >
                            <?=$arItem["PREVIEW_TEXT"]?>
                        </p>
                    </div>
                </div>

                <div class="col col_right with_chart" style="background-image: url(<?=$arItem["PROPERTIES"]["IMAGE"]["VALUE"]?>); background-position: left center">
                    <div class="col_inner">
                        <a <?if($arItem["PROPERTIES"]["LINK_BLANK"]["VALUE"]):?> target="_blank"<?endif?><?if($arItem["PROPERTIES"]["LINK_NOFOLLOW"]["VALUE"]):?> rel="nofollow"<?endif?> href="<?=$arItem["PROPERTIES"]["LINK_URL"]["VALUE"]?>" class="button button_rounded_green">Подробнее</a>
                    </div>
                </div>
            </div>
            <style>
                <?if($arItem["PROPERTIES"]["CIRCLE_COLOR"]["VALUE"]):?>
                .header_bnr .offer_round_outer .col.with_round.green::before{
                    background-color: <?=$arItem["PROPERTIES"]["CIRCLE_COLOR"]["VALUE"]?>;
                }
                <?endif?>
            </style>
        <?endif;?>
    </div>
<?endforeach;?>
