<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="main_menu">

<?
$previousLevel = 0;

foreach($arResult as $arItem):
	if($arItem["PARAMS"]["URL"]){
		$arItem["LINK"] = $arItem["PARAMS"]["URL"];
	}
	?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>
		<li><a <?if($arItem["PARAMS"]["NEED_AUTH"]):?>href="#" data-toggle="modal" data-target="#popup_login_back"<?else:?>href="<?=$arItem["LINK"]?>"<?endif?>><?=$arItem["TEXT"]?></a>
			<ul class="main_menu_toggle">
	<?else:?>
		<li><a <?if($arItem["PARAMS"]["NEED_AUTH"]):?>href="#" data-toggle="modal" data-target="#popup_login_back"<?else:?>href="<?=$arItem["LINK"]?>"<?endif?>><?=$arItem["TEXT"]?></a></li>
	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<?endif?>