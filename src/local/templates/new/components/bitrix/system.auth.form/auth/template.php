<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult["FORM_TYPE"] == "login"):?>

<?if($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']):?>
    <div style="color:red"><?ShowMessage($arResult['ERROR_MESSAGE']);?></div>
<?endif?>
<form class="header_login" method="post" action="/local/templates/new/ajax/auth.php?login=yes">
	<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?endif?>
	<?if($arParams["PROFILE_URL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arParams["PROFILE_URL"]?>" />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />

	<div class="form_element">
		<input type="text" placeholder="Ваша электронная почта" name="USER_LOGIN" />
	</div>

	<div class="form_element password_element">
		<input type="password" placeholder="Пароль" name="USER_PASSWORD" />

		<span class="show_password">Показать пароль</span>
	</div><br/>

	<div class="form_element">
		<div class="checkbox">
			<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y">

			<label for="USER_REMEMBER_frm">Запомнить меня на этом сайте</label>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-5">
			<input type="submit" class="button" name="Login" value="Войти">
		</div>

		<div class="col-sm-7">
			<a href="#" id="popup_reset_password_toggle" class="font_12 light" data-toggle="modal" data-target="#popup_reset_password">Забыли пароль?</a>

			<a href="javascript:void(0);" id="popup_signup_toggle" class="font_12 green" style="text-transform: uppercase"><strong>Регистрация</strong></a>
		</div>
	</div>
</form>
<?endif?>
