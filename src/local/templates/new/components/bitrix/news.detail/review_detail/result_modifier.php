<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arFilter1 = Array(
	"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
	">ID"=>$arResult["ID"],
);

$res1 = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"ASC"), $arFilter1, false);
if ($ar_fields1 = $res1->GetNext()) {
	$arResult["NEXT"] = array(
		"URL" => $ar_fields1["DETAIL_PAGE_URL"],
		"NAME" => $ar_fields1["NAME"],
	);
}

$arFilter2 = Array(
	"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
	"<ID"=>$arResult["ID"],
);

$res2 = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter2, false);
if ($ar_fields2 = $res2->GetNext()) {
	$arResult["PREV"] = array(
		"URL" => $ar_fields2["DETAIL_PAGE_URL"],
		"NAME" => $ar_fields2["NAME"],
	);
}

//похожие статьи
if(is_array($arResult["DISPLAY_PROPERTIES"]["RELATED_POSTS"]["LINK_ELEMENT_VALUE"])) {
	foreach ($arResult["DISPLAY_PROPERTIES"]["RELATED_POSTS"]["LINK_ELEMENT_VALUE"] as $arItem) {
		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
		$arFilter = Array("IBLOCK_ID" => $arItem["IBLOCK_ID"], "ID" => $arItem["ID"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if ($ob = $res->GetNextElement()) {
			$arResult["OTHER"][] = $ob->GetFields();
		}
	}
}
