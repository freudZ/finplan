<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$APPLICATION->SetPageProperty("title", $arResult["NAME"]." | Отзывы | Сайт о финансовом планировании Fin-plan.org");?>

<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>

				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>

                <div class="title_container title_nav_outer">
                    <h1><?=$arResult["NAME"]?></h1>

                    <div class="title_nav">
                    	       <a class="title_nav_prev" href="/reviews/"><span class="icon icon-arr_left"></span> Все отзывы</a>&nbsp;&nbsp;&nbsp;
                    	       <a class="title_nav_prev" href="/case/">Все кейсы <span class="icon icon-arr_right"></span></a>
<!--                        <?if($arResult["PREV"]):?>
                            <a class="title_nav_prev" href="<?=$arResult["PREV"]["URL"]?>"><span class="icon icon-arr_left"></span> Предыдущая</a>
                        <?endif;?>
						<?if($arResult["NEXT"]):?>
                            <a class="title_nav_next" href="<?=$arResult["NEXT"]["URL"]?>">Следующая <span class="icon icon-arr_right"></span></a>
						<?endif;?>-->
                    </div>
                </div>

                <div class="article_inner">
                    <div class="review_card">
                        <div class="img_col col">
                            <div class="img_col_inner">
                                <?$img = CFile::ResizeImageGet($arResult["PREVIEW_PICTURE"]["ID"], array("width"=>130, "height"=>130), BX_RESIZE_IMAGE_EXACT)?>
                                <img src="<?=$img["src"]?>" alt="<?=$arResult["NAME"]?>"/>
                            </div>
                        </div>
                        <?if($arResult["PROPERTIES"]["QUOTE"]["~VALUE"]["TEXT"]):?>
                        <div class="text_col col">
                            <p class="review_card_title">Избранные цитаты:</p>
                            <p class="review_card_text"><?=$arResult["PROPERTIES"]["QUOTE"]["~VALUE"]["TEXT"]?></p>
                        </div>
                        <?endif;?>
                    </div>

                    <?if($arResult["PROPERTIES"]["VIDEO"]["VALUE"]):?>
                        <div class="review_video">
                            <span class="play"></span>
                            <div class="review_video_splash"></div>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$arResult["PROPERTIES"]["VIDEO"]["VALUE"]?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    <?endif;?>
                    <p>
					    <?=$arResult["DETAIL_TEXT"]?>
                    </p>
                </div>

                <div class="article_stats review_stats">
                    <div class="row">
                        <div class="col col-xs-6">
                            <p class="article_date">
								<?=FormatDate(array(
									"tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
									"today" => "today",              // выведет "сегодня", если дата текущий день
									"yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
									"d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
									"" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
								), MakeTimeStamp($arResult["DISPLAY_ACTIVE_FROM"]), time()
								);?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="article_share review_share">
                    <?if($arResult["PROPERTIES"]["SOCIALS"]["VALUE"]):?>
						<?if($arResult["PROPERTIES"]["CALL_TEXT"]["~VALUE"]):?>
                            <p><?=$arResult["PROPERTIES"]["CALL_TEXT"]["~VALUE"]?>:</p>
						<?endif;?>
                        <?foreach ($arResult["PROPERTIES"]["SOCIALS"]["VALUE"] as $val){
                            $img = false;
                            if(strpos($val, "vk.com")!==false){
                                $img = "vk";
                            } elseif(strpos($val, "facebook.com")!==false){
								$img = "fb";
                            }
                            if($img){
                                $arLinks[] = array(
                                    "link" => $val,
                                    "img" => $img,
                                );
                            }
                        }
                        ?>
                        <?if($arLinks):?>
                            <ul class="share_list">
                                <?foreach ($arLinks as $link):?>
                                    <li>
                                        <a rel="nofollow" target="_blank" class="icon icon-<?=$link["img"]?>" href="<?=$link["link"]?>"></a>
                                    </li>
                                <?endforeach;?>
                            </ul>
                        <?endif;?>
                    <?endif;?>
                    <span class="add_review_btn button" data-toggle="modal" data-target="#popup_add_review">Оставить свой <br/>отзыв</span>
                </div>

                <?if($arResult["PROPERTIES"]["BOTTOM_BANNER"]["VALUE"]):?>
					<?$APPLICATION->IncludeComponent("bitrix:news.detail", "banner_bottom", Array(
						"IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
						"IBLOCK_ID" => "24",	// Код информационного блока
						"ELEMENT_ID" => $arResult["PROPERTIES"]["BOTTOM_BANNER"]["VALUE"],	// ID новости
						"ELEMENT_CODE" => "",	// Код новости
						"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
						"FIELD_CODE" => array(	// Поля
							0 => "SHOW_COUNTER",
							1 => "PREVIEW_PICTURE",
						),
						"PROPERTY_CODE" => array(	// Свойства
							0 => "RELATED_POSTS",
							1 => "",
						),
						"IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"CACHE_TYPE" => "A",	// Тип кеширования
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"SET_STATUS_404" => "Y",	// Устанавливать статус 404, если не найдены элемент или раздел
						"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
						"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
						"ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
						"ACTIVE_DATE_FORMAT" => "",	// Формат показа даты
						"USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
						"DISPLAY_DATE" => "Y",	// Выводить дату элемента
						"DISPLAY_NAME" => "Y",	// Выводить название элемента
						"DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
						"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
						"USE_SHARE" => "N",	// Отображать панель соц. закладок
						"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"PAGER_TITLE" => "Страница",	// Название категорий
						"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
					),
						false
					);?>
                <?endif;?>

				<?if($arResult["OTHER"]):?>
                    <div class="other_articles_list">
                        <div class="other_articles_list_top">
                            <p>Рекомендуемые к прочтению статьи:</p>

                            <a class="link_arrow" href="/blog/">Все статьи</a>
                        </div>
                        <div class="articles_list row">
                            <?foreach ($arResult["OTHER"] as $item):?>
                                <div class="articles_list_element col-xs-6 col-lg-4">
                                    <div class="articles_list_element_inner">
                                        <div class="articles_list_element_img">
                                            <div class="articles_list_element_img_inner">
                                                <a href="<?=$item["DETAIL_PAGE_URL"]?>" style="background-image:url(<?=CFile::GetPath($item["PREVIEW_PICTURE"])?>);"></a>
                                            </div>
                                        </div>

                                        <div class="articles_list_element_text">
                                            <p class="articles_list_element_title">
                                                <a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a>
                                            </p>
                                            <p  class="articles_list_element_description"><?=$item["PREVIEW_TEXT"]?></p>

                                            <p class="articles_list_element_date">
                                                <?=FormatDate(array(
                                                    "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                    "today" => "today",              // выведет "сегодня", если дата текущий день
                                                    "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                    "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                    "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                                ), MakeTimeStamp($item["DATE_ACTIVE_FROM"]), time()
                                                );?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
				<?endif;?>

                <div class="comment_outer">
                    <div class="content-main-post-item-comments">
						<?$APPLICATION->IncludeComponent("cackle.comments", "cackle_default", Array(
						),
							false
						);?>
                    </div>
                </div>
            </div>
        </div>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
    </div>
</div>