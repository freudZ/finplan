<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$APPLICATION->SetPageProperty("title", $arResult["NAME"]." | Блог | Сайт о финансовом планировании Fin-plan.org");?>

<?
  $res = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
  if($ar_res = $res->GetNext()) {
    $section_code = $ar_res["CODE"];
  }
	if ($section_code!=$_REQUEST["SECTION_CODE"]) {
		$APPLICATION->RestartBuffer();
		CHTTP::SetStatus("404 Not Found");
		LocalRedirect("/404.php");
  }
?>
<div class="wrapper gray_bg" itemscope itemtype="http://schema.org/Article">
    <div class="container">
        <div class="main_content white_bg">
            <div class="main_content_inner">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "extended_banner",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                );?>
				<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"))?>
                <div class="title_container title_nav_outer">
                    
					<meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="https://google.com/article"/>

					<h1 itemprop="name"><?=$arResult["NAME"]?></h1>
					<span itemprop="author" style="display: none;">Кошин В.В.</span>
					
					<span itemprop="publisher" itemscope itemtype="https://schema.org/Organization" style="display: none;">Fin-Plan</span>
				
					<meta itemprop="datePublished" content="<?=date(Y."-".m."-".d);?>">
					<meta itemprop="dateModified"  content="<?=date(Y."-".m."-".d);?>">
					<span style="display:none;" itemprop="headline"><?=$arResult["NAME"]?></span>					
					<img  itemprop="image" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" style="display: none;"> 

                    <div class="title_nav">
                    	       <a class="title_nav_prev" href="/reviews/"><span class="icon icon-arr_left"></span> Все отзывы</a>&nbsp;&nbsp;&nbsp;
                    	       <a class="title_nav_prev" href="/case/">Все кейсы <span class="icon icon-arr_right"></span></a>
<!--                        <?if($arResult["PREV"]):?>
                            <a class="title_nav_prev" href="<?=$arResult["PREV"]["URL"]?>"><span class="icon icon-arr_left"></span> Предыдущая</a>
                        <?endif;?>
						<?if($arResult["NEXT"]):?>
                            <a class="title_nav_next" href="<?=$arResult["NEXT"]["URL"]?>">Следующая <span class="icon icon-arr_right"></span></a>
						<?endif;?>-->
                    </div>
                </div>

                <?if($arResult["TAGS"]):?>
                    <ul class="sections_list">
                        <?foreach ($arResult["TAGS"] as $item):?>
                            <li><a href="/blog/?tag=<?=$item["ID"]?>"><?=$item["NAME"]?></a></li>
                        <?endforeach;?>
                    </ul>
                <?endif;?>

                <div class="article_inner">
					<?=$arResult["DETAIL_TEXT"]?>
				<?if(!empty($arResult["LINKED_COMPANIES"])):?>
					<h2>Акции и компании, рассмотренные в кейсе:</h2>
					<ul>
					<?foreach($arResult["LINKED_COMPANIES"] as $kcomp=>$company):?>
					 <li>Компания <a href="<?=$company["DETAIL_PAGE_URL"]?>" target="_self"><?=$company["NAME"]?></a></li>
					 <ul>
						<?foreach($arResult["LINKED_ACTIONS"][$kcomp] as $kaction=>$action):?>
						 <li>Акция <a href="<?=$action["DETAIL_PAGE_URL"]?>" target="_self"><?=$action["NAME"]?></a></li>
						<?endforeach;?>
					 </ul>
					<?endforeach;?>
					</ul>
				<?endif;?>
                </div>

                <div class="article_stats">
                    <div class="row">
                        <div class="col col-xs-6">
                            <p class="article_date">
								<?=FormatDate(array(
									"tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
									"today" => "today",              // выведет "сегодня", если дата текущий день
									"yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
									"d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
									"" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
								), MakeTimeStamp($arResult["DISPLAY_ACTIVE_FROM"]), time()
								);?>
                            </p>
                        </div>

                        <div class="col col-xs-6">
                            <ul class="article_stats_list">
                                <li><span class="icon icon-comment"></span> <span id="commentsCount"></span></li>
                                <li><span class="icon icon-eye"></span> <?=$arResult["SHOW_COUNTER"]?></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <?
				$image_src_for_social = '';
				$first_image = get_first_image_from_post($arResult["DETAIL_TEXT"]);
				if($first_image!='') {
					$image_src_for_social = $first_image;
					$APPLICATION->AddHeadString('<meta content="'.$image_src_for_social.'" property="og:image">',true);
					$APPLICATION->AddHeadString('<link rel="image_src" href="'.$image_src_for_social.'" />',true);
				} elseif($arResult["DETAIL_PICTURE"]["SRC"]!='') {
					$image_src_for_social = $arResult["DETAIL_PICTURE"]["SRC"];
					$APPLICATION->AddHeadString('<meta content="'.$image_src_for_social.'" property="og:image">',true);
					$APPLICATION->AddHeadString('<link rel="image_src" href="'.$image_src_for_social.'" />',true);
				}

				$lj_event = htmlentities('<a href="http://'.$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"].'">'.$arResult["NAME"].'</a><img src="http://'.$_SERVER['HTTP_HOST'].$image_src_for_social.'">');
                ?>
                <div class="article_share">
                    <p>Рассказать другим про интересную статью</p>
                    <ul class="share_list">
                        <li>
                            <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                        </li>
                        <li>
                            <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                        </li>
                        <li>
                            <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>"></a>
                        </li>
                        <!-- <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li> -->
                        <li><a href="https://twitter.com/share?text=<?=$arResult["NAME"]?>" onclick="window.open('https://twitter.com/share?text=<?=$arResult["NAME"]?>&url=http://<?=$_SERVER["HTTP_HOST"]?><?=$_SERVER["REQUEST_URI"]?>','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a></li>
                        <li>
                            <?$inFavorite = checkInFavorite($arResult["ID"], "blog")?>
                            <span<?if(!$USER->IsAuthorized()):?> data-toggle="modal" data-target="#popup_login"<?endif?> class="to_favorites button<?if($inFavorite):?> active<?endif;?>" data-id="<?=$arResult["ID"]?>">
                                <span class="icon icon-star_empty"></span> <span class="text"><?if($inFavorite):?>В избранном<?else:?>В избранное<?endif;?></span>
                            </span>
                        </li>
                    </ul>
                </div>

				<?$this->SetViewTarget('blog_article_share');?>
					<div class="main_sidemenu_element share_element active">
						<span class="main_sidemenu_link icon icon-share" title="Поделиться статьей"></span>

						<div class="main_sidemenu_inner">
							<ul class="share_list">
								<li>
									<a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
								</li>
								<li>
									<a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
								</li>
								<li>
									<a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>"></a>
								</li>
								<!-- <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li> -->
								<li><a href="https://twitter.com/share?text=<?=$arResult["NAME"]?>" onclick="window.open('https://twitter.com/share?text=<?=$arResult["NAME"]?>&url=http://<?=$_SERVER["HTTP_HOST"]?><?=$_SERVER["REQUEST_URI"]?>','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a></li>
								<li>
									<?$inFavorite = checkInFavorite($arResult["ID"], "blog")?>
									<span<?if(!$USER->IsAuthorized()):?> data-toggle="modal" data-target="#popup_login"<?endif?> class="to_favorites button<?if($inFavorite):?> active<?endif;?>" data-id="<?=$arResult["ID"]?>">
										<span class="icon icon-star_empty"></span> <span class="text"><?if($inFavorite):?>В избранном<?else:?>В избранное<?endif;?></span>
									</span>
								</li>
							</ul>
						</div>
					</div>
				<?$this->EndViewTarget();?>

                <?if($arResult["PROPERTIES"]["BOTTOM_BANNER"]["VALUE"]):?>
					<?$APPLICATION->IncludeComponent("bitrix:news.detail", "banner_bottom", Array(
						"IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
						"IBLOCK_ID" => "24",	// Код информационного блока
						"ELEMENT_ID" => $arResult["PROPERTIES"]["BOTTOM_BANNER"]["VALUE"],	// ID новости
						"ELEMENT_CODE" => "",	// Код новости
						"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
						"FIELD_CODE" => array(	// Поля
							0 => "SHOW_COUNTER",
							1 => "PREVIEW_PICTURE",
						),
						"PROPERTY_CODE" => array(	// Свойства
							0 => "RELATED_POSTS",
							1 => "",
						),
						"IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"CACHE_TYPE" => "A",	// Тип кеширования
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"SET_STATUS_404" => "Y",	// Устанавливать статус 404, если не найдены элемент или раздел
						"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
						"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
						"ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
						"ACTIVE_DATE_FORMAT" => "",	// Формат показа даты
						"USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
						"DISPLAY_DATE" => "Y",	// Выводить дату элемента
						"DISPLAY_NAME" => "Y",	// Выводить название элемента
						"DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
						"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
						"USE_SHARE" => "N",	// Отображать панель соц. закладок
						"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"PAGER_TITLE" => "Страница",	// Название категорий
						"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
					),
						false
					);?>
                <?endif;?>

				<?if($arResult["OTHER"]):?>
                    <div class="other_articles_list">
                        <div class="other_articles_list_top">
                            <p>Рекомендуемые к прочтению статьи:</p>

                            <a class="link_arrow" href="/blog/">Все статьи</a>
                        </div>
                        <div class="articles_list colored row">
                            <?foreach ($arResult["OTHER"] as $item):?>
                                <div class="articles_list_element col-xs-6 col-lg-4">
                                    <div class="articles_list_element_inner">
                                        <div class="articles_list_element_img">
                                            <div class="articles_list_element_img_inner">
                                                <a href="<?=$item["DETAIL_PAGE_URL"]?>" style="background-image:url(<?=CFile::GetPath($item["PREVIEW_PICTURE"])?>);"></a>
                                            </div>
                                        </div>

                                        <div class="articles_list_element_text">
                                            <p class="articles_list_element_title">
                                                <a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a>
                                            </p>
                                            <p  class="articles_list_element_description"><?=$item["PREVIEW_TEXT"]?></p>

                                            <p class="articles_list_element_date">
                                                <?=FormatDate(array(
                                                    "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                    "today" => "today",              // выведет "сегодня", если дата текущий день
                                                    "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                    "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                    "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                                ), MakeTimeStamp($item["DATE_ACTIVE_FROM"]), time()
                                                );?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
				<?endif;?>

                <div class="comment_outer">
                    <div class="content-main-post-item-comments">
						<?$APPLICATION->IncludeComponent("cackle.comments", "cackle_default", Array(
						),
							false
						);?>
                    </div>
                </div>
            </div>
        </div>
		<?$t = "123";?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "right_sidebar",
				"EDIT_TEMPLATE" => "",
				"COMPONENT_TEMPLATE" => ".default",
				"AREA_FILE_RECURSIVE" => "Y"
			),
			false
		);?>
    </div>
</div>
<?if($arResult["FLY_BANNER"]):?>
<?
$needOpen = true;
if($_COOKIE["fly_right_banner"]){
	$needOpen = false;
}
$linkData = 'href="'.$arResult["FLY_BANNER"]["PROPS"]["LINK"]["VALUE"].'" target="_blank"';

if($arResult["FLY_BANNER"]["PROPS"]["OPEN"]["VALUE_ENUM_ID"]!=41){
	$linkData = 'href="#" data-toggle="modal" data-target="#popup_'.$arResult["FLY_BANNER"]["PROPS"]["OPEN"]["VALUE_XML_ID"].'"';
}
?>
<div class="popup_message" data-open="<?=$APPLICATION->flyRightBannerConf["open"]?>" data-life="<?=$APPLICATION->flyRightBannerConf["life"]?>" data-needopen="<?=$needOpen?>">
    <span class="icon icon-close"></span>
      <span class="icon icon-arr_left"></span>
    <a class="popup_message_inner" <?=$linkData?>>
      <span class="col col_left">
			<img src="<?=CFile::GetPath($arResult["FLY_BANNER"]["PREVIEW_PICTURE"])?>" alt="<?=$arResult["FLY_BANNER"]["NAME"]?>" />
        <span class="play"></span>
        <span class="popup_message_mobile_title t9 strong uppercase green mb10">Новое видео</span>
      </span>

      <span class="col col_right">
        <span class="popup_message_label t9 strong uppercase green mb10"><?=$arResult["FLY_BANNER"]["PROPS"]["TOP_NAME"]["VALUE"]?></span>
        <span class="popup_message_title t12 light uppercase white mb5"><?=$arResult["FLY_BANNER"]["NAME"]?></span>

        <span class="popup_message_description t10 light white m0"><?=$arResult["FLY_BANNER"]["PREVIEW_TEXT"]?></span>
      </span>
    </a>
</div>
<?endif?>
