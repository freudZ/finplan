<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arFilter1 = Array(
	"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
	">ID"=>$arResult["ID"],
);

$res1 = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"ASC"), $arFilter1, false);
if ($ar_fields1 = $res1->GetNext()) {
	$arResult["NEXT"] = array(
		"URL" => $ar_fields1["DETAIL_PAGE_URL"],
		"NAME" => $ar_fields1["NAME"],
	);
}

$arFilter2 = Array(
	"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
	"<ID"=>$arResult["ID"],
);

$res2 = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter2, false);
if ($ar_fields2 = $res2->GetNext()) {
	$arResult["PREV"] = array(
		"URL" => $ar_fields2["DETAIL_PAGE_URL"],
		"NAME" => $ar_fields2["NAME"],
	);
}

function get_first_image_from_post($post_html) {
	$patt = ' /<\s*img[^>]*src=[\"|\'](.*?)[\"|\'][^>]*\/*>/i'; // регулярное выражение
	preg_match($patt, $post_html, $result); // вызов обработчика с выводом в переменную $result
	if (!empty($result[1])) {  // проверяем есть ли результат
		return $result[1]; //результат есть - возвращаем результат
	} else {
		return ''; // результата нет - возвращаем пустую строку
	}
}

//похожие статьи
if(is_array($arResult["DISPLAY_PROPERTIES"]["RELATED_POSTS"]["LINK_ELEMENT_VALUE"])) {
	foreach($arResult["DISPLAY_PROPERTIES"]["RELATED_POSTS"]["LINK_ELEMENT_VALUE"] as $arItem) {
		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
		$arFilter = Array("IBLOCK_ID"=>$arItem["IBLOCK_ID"], "ID"=>$arItem["ID"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if ($ob = $res->GetNextElement()){
			$arResult["OTHER"][] = $ob->GetFields();
		}
	}
}

//теги
if($arResult["PROPERTIES"]["TAGS"]["VALUE"]) {
	$arFilter = Array("IBLOCK_ID" => 22, "ID" => $arResult["PROPERTIES"]["TAGS"]["VALUE"], "ACTIVE" => "Y");
	$res = CIBlockElement::GetList(Array("SORT" => "asc", "NAME" => "asc"), $arFilter, false, false, array("NAME", "ID"));
	while ($item = $res->GetNext()) {
		$arResult["TAGS"][] = $item;
	}
}

//всплывающий баннер
if($arResult["PROPERTIES"]["FLY_RIGHT_BANNER"]["VALUE"]) {
	$arFilter = Array("IBLOCK_ID" => 28, "ID" => $arResult["PROPERTIES"]["FLY_RIGHT_BANNER"]["VALUE"], "ACTIVE" => "Y");
	$res = CIBlockElement::GetList(Array("SORT" => "asc", "NAME" => "asc"), $arFilter, false, false);
	if ($ob = $res->GetNextElement()) {
		$item = $ob->GetFields();
		$item["PROPS"] = $ob->GetProperties();
		$arResult["FLY_BANNER"] = $item;
	}
}

$arResult["LINKED_ACTIONS"] = array();
$arResult["LINKED_COMPANIES"] = array();
//Ссылка на акции и компании (Не зарубежные)
if(is_array($arResult["DISPLAY_PROPERTIES"]["LINKED_ACTIONS"]["VALUE"])) {

      $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "DETAIL_PAGE_URL", "PROPERTY_EMITENT_ID", "SHOW_COUNTER");
		$arFilter = Array("IBLOCK_ID"=>$arResult["DISPLAY_PROPERTIES"]["LINKED_ACTIONS"]["LINK_IBLOCK_ID"], "ID"=>$arResult["DISPLAY_PROPERTIES"]["LINKED_ACTIONS"]["VALUE"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if ($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arResult["LINKED_ACTIONS"][$arFields["PROPERTY_EMITENT_ID_VALUE"]][$arFields["ID"]] = array("NAME"=>$arFields["NAME"], "DETAIL_PAGE_URL"=>$arFields["DETAIL_PAGE_URL"]);
			$arResult["LINKED_COMPANIES"][$arFields["PROPERTY_EMITENT_ID_VALUE"]] = array();
		}

  //Получим компании по ID Акций (Привязка к эмитенту)
$arFilterC = Array("IBLOCK_ID"=>26, "ID"=>array_keys($arResult["LINKED_COMPANIES"]));
$resC = CIBlockElement::GetList(Array(), $arFilterC, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($itemC = $resC->GetNext())
{
	//$arResult["LINKED_COMPANIES"][$itemC["ID"]] = $itemC;

	$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$itemC["NAME"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
	if($item = $res->GetNext())
	{
		$arResult["LINKED_COMPANIES"][$itemC["ID"]]["NAME"] = $item["NAME"];
		$arResult["LINKED_COMPANIES"][$itemC["ID"]]["DETAIL_PAGE_URL"] = $item["DETAIL_PAGE_URL"];
	}
}


}