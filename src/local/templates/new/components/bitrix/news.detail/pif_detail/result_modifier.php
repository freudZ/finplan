<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;
use Bitrix\Iblock\InheritedProperty;
$arResult["POINTS"] = array(
	"",
	"xxs",
	"xxs xs",
	"xxs xs sm",
	"xxs xs sm md",
	"xxs xs sm md lg"
);

//Доступ
$arResult["HAVE_ACCESS"] = checkPayRadar();
$arResult["CLOSED_NO_ACCESS_PROPS"] = array(
	"Просад",
    "Методика расчета дивидендов",
    "Средний прирост x2",
);

/*global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
		 echo "<pre  style='color:black; font-size:11px;'>";
          print_r($arResult["PROPERTIES"]);
          echo "</pre>";
      }*/


//акция
$arFilter = Array("IBLOCK_ID"=>32, "CODE"=>$arResult["CODE"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "ID", "DETAIL_PAGE_URL", "PREVIEW_TEXT"));
while($item = $res->GetNextElement())
{
	$arResult["ACTION"] = $item->GetFields();
	$arResult["ACTION"]["PROPS"] = $item->GetProperties();
}

/*global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
echo "<pre  style='color:black; font-size:11px;'>";
   print_r($arResult["ACTION"]);
   echo "</pre>";
      }*/

//Кейсы
$arResult["CASE"] = array();
$arFilter = Array("IBLOCK_ID"=>48, "ACTIVE"=>"Y", "PROPERTY_LINKED_ACTIONS"=>$arResult["ACTION"]["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($item = $res->GetNextElement())
{
	$arResult["CASE"][] = $item->GetFields();
}

//компания
$arFilter = Array("IBLOCK_ID"=>26, "ID"=>$arResult["ACTION"]["PROPS"]["EMITENT_ID"]["VALUE"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($item = $res->GetNext())
{
	$arResult["COMPANY"] = $item;
	
	$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$item["NAME"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL","PROPERTY_CAPITAL_KOEF"));
	if($item = $res->GetNext())
	{
		$arResult["COMPANY"]["DETAIL_PAGE_URL"] = $item["DETAIL_PAGE_URL"];
		$arResult["COMPANY"]["CAPITAL_KOEF"] = $item["PROPERTY_CAPITAL_KOEF_VALUE"];
	}
}



//список данных
if($arResult["ACTION"]["PROPS"]){

	$res = new ETF();
	$actionItem = $res->getItem($arResult["CODE"]);

	if(!empty($arResult["ACTION"]["PROPS"]["ETF_TOP_ACTIVES"]["VALUE"])){
		$arTop = explode(",",$arResult["ACTION"]["PROPS"]["ETF_TOP_ACTIVES"]["VALUE"]);
		$arFilterTop = array("IBLOCK_ID"=>32, "PROPERTY_SECID"=>$arTop);
		$arSelectTop = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_SECID");
      $resTop = CIBlockElement::GetList(Array(), $arFilterTop, false, false, $arSelectTop);
      while($obTop = $resTop->GetNextElement()){
       $arFieldsTop = $obTop->GetFields();
		 $key=array_search($arFieldsTop["PROPERTY_SECID_VALUE"], $arTop);
		 $arTop[$key] = "<a href=".$arFieldsTop["DETAIL_PAGE_URL"]." target=\"_blank\">".$arFieldsTop["NAME"]."</a>";
		}

		//Поиск среди буржуйских бумаг
		$arFilterTop = array("IBLOCK_ID"=>55, "PROPERTY_SECID"=>$arTop);
		$arSelectTop = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_SECID");
      $resTop = CIBlockElement::GetList(Array(), $arFilterTop, false, false, $arSelectTop);
      while($obTop = $resTop->GetNextElement()){
       $arFieldsTop = $obTop->GetFields();
		 $key=array_search($arFieldsTop["PROPERTY_SECID_VALUE"], $arTop);
		 $arTop[$key] = "<a href=".$arFieldsTop["DETAIL_PAGE_URL"]." target=\"_blank\">".$arFieldsTop["NAME"]."</a>";
		}

		//Поиск среди облигаций
		$arFilterTop = array("IBLOCK_ID"=>27, "PROPERTY_ISIN"=>$arTop);
		$arSelectTop = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_ISIN");
      $resTop = CIBlockElement::GetList(Array(), $arFilterTop, false, false, $arSelectTop);
      while($obTop = $resTop->GetNextElement()){
       $arFieldsTop = $obTop->GetFields();
		 $key=array_search($arFieldsTop["PROPERTY_ISIN_VALUE"], $arTop);
		 $arTop[$key] = "<a href=".$arFieldsTop["DETAIL_PAGE_URL"]." target=\"_blank\">".$arFieldsTop["NAME"]."</a>";
		}



		$arResult["ACTION"]["PROPS"]["ETF_TOP_ACTIVES"]["VALUE"] = $arTop;
	}



  				$currency_text = ' руб.';

				if($actionItem["PROPS"]["ETF_CURRENCY"]=='USD'){
					$currency_text = ' $';
				}else if($actionItem["PROPS"]["ETF_CURRENCY"]=='EUR'){
					$currency_text = ' €';
				}

	foreach(array("ISIN", "PROP_TIP_AKTSII", "PROP_SEKTOR",
   "IN_INDEX", "PRICE", "ISSUECAPITALIZATION", "PROP_TARGET",
	"PROP_PROSAD", "PROP_DIVIDENDY_2018", 'PROP_DIVIDEND_DESCRIPTION', "PE",
	"PEQUITY", "PEG", "ACTION_DYNAMIC", "ETF_TYPE_OF_ACTIVES", "ETF_CALC_BASE",
	"ETF_DIVIDENDS", "ETF_REPLICATION", "ETF_COMISSION", "ETF_TYPE", "ETF_EXCHANGE", "ETF_TOP_ACTIVES",
	"ETF_AVG_ICNREASE", "ETF_COUNTRY", "ETF_INDUSTRY", "ETF_BASE_CURRENCY", "ETF_CURRENCY", "BETTA") as $propCode){


	   if($propCode == "ETF_AVG_ICNREASE"){
			if(floatval($arResult["ACTION"]["PROPS"][$propCode]["VALUE"])<=0){
				continue;
			} else {
			  $arResult["ACTION"]["PROPS"][$propCode]["VALUE"] = $arResult["ACTION"]["PROPS"][$propCode]["VALUE"]."%";
			  $arResult["ACTION"]["PROPS"][$propCode]["NAME"] = "Средний прирост x2";
			}
		}
	   if($propCode == "ETF_COMISSION"){
			if(floatval($arResult["ACTION"]["PROPS"][$propCode]["VALUE"])<=0){
				continue;
			} else {
			  $arResult["ACTION"]["PROPS"][$propCode]["VALUE"] = $arResult["ACTION"]["PROPS"][$propCode]["VALUE"]."%";
			}
		}
		if($propCode == "ISIN"){
			$val = $arResult["ACTION"]["PROPS"][$propCode]["VALUE"];
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "ISIN-код",
				"VALUE" => $val,
			);
		} elseif($propCode == "IN_INDEX"){
			$val = array();
			
			if($arResult["ACTION"]["PROPS"]["PROP_MSCI"]["VALUE"]){
				$val[] = "MSCI";
			}
			if($arResult["ACTION"]["PROPS"]["PROP_MMVB"]["VALUE"]){
				$val[] = "ММВБ";
			}
			
			if($val){
				$arResult["LIST_VALUES"][] = array(
					"NAME" => "Бумага состоит в индексах",
					"VALUE" => implode(", ", $val),
				);
			}
		} elseif($propCode == "ISSUECAPITALIZATION"){
			if(!$actionItem["PROPS"][$propCode]){
				continue;
			}
			
			$t = array(
				"NAME" => "Капитализация",
				"VALUE" => number_format(round(($actionItem["PROPS"][$propCode]/1000000), 2), 2, '.', ' ') ." млн.р.",
			);




			if($actionItem["PROPS"][$propCode."_DOL"]){
				$t["VALUE"] .= " (".round($actionItem["PROPS"][$propCode."_DOL"]/1000000, 2)." млн. долл.)";
			}





			$arResult["LIST_VALUES"][] = $t;
		} elseif($propCode == "PRICE") {
            if (!$actionItem["PROPS"]["LASTPRICE"]) {
                continue;
            }
				$priceDate = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTION_PRICES_DATE");
				$arResult["PRICE_DATE"] = $priceDate;
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Цена ПИФ".(!empty($priceDate)?' на сегодня ('.$priceDate.')':''),
					 "CLASS" => "green",
                "VALUE" => $actionItem["PROPS"]["LASTPRICE"] . $currency_text,
            );

        } elseif($propCode == "PROP_DIVIDEND_DESCRIPTION" && $actionItem["PROPS"][$propCode]){
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Методика расчета дивидендов",
                "VALUE" => $actionItem["PROPS"][$propCode],
            );
		} elseif($propCode == "PE"){
            if(!$actionItem["DYNAM"]["PE"]){
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "P/E",
                "VALUE" => $actionItem["DYNAM"]["PE"],
            );
        } elseif($propCode == "PEQUITY"){
            if(!$actionItem["DYNAM"]["P/Equity"]){
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "P/Equity",
                "VALUE" => $actionItem["DYNAM"]["P/Equity"],
            );
        } elseif($propCode == "PEG"){
            if(!$actionItem["DYNAM"]["PEG"]){
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "PEG",
                "VALUE" => $actionItem["DYNAM"]["PEG"],
            );
        } elseif ($propCode == "BETTA") {
            if (!$arResult["ACTION"]["PROPS"][$propCode]["VALUE"]) {
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Бета",
                "VALUE" => round($arResult["ACTION"]["PROPS"][$propCode]["VALUE"], 2),
            );
        } elseif($propCode == "ACTION_DYNAMIC"){
            $valueArr = [];
            $color = '';
            if($actionItem["DYNAM"]["MONTH_INCREASE"]){
                $valueArr[] = " прирост за  месяц = <span style='color:" . ($actionItem['DYNAM']['MONTH_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["MONTH_INCREASE"] . "%</span>";
            }
            if($actionItem["DYNAM"]["YEAR_INCREASE"]){
                $valueArr[] = " прирост за  год = <span style='color:" . ($actionItem['DYNAM']['YEAR_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["YEAR_INCREASE"] . "%</span>";
            }
            if($actionItem["DYNAM"]["THREE_YEAR_INCREASE"]){
                $valueArr[] = " прирост за  три года = <span style='color:" . ($actionItem['DYNAM']['THREE_YEAR_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["THREE_YEAR_INCREASE"] . "%</span>";
            }

            if ($valueArr) {
                $arResult["LIST_VALUES"][] = array(
                    "NAME" => "Динамика ETF",
                    "VALUE" => $valueArr,
                );
            }
        } else {
			if($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]){
				$tmp = array(
					"NAME" => $arResult["ACTION"]["PROPS"][$propCode]["NAME"],
					"VALUE" => $arResult["ACTION"]["PROPS"][$propCode]["VALUE"],
				);
				
				
				
				if(is_array($tmp["VALUE"])){
					$tmp["VALUE"] = implode(", ", $tmp["VALUE"]);
				}
				if($propCode=="PROP_TARGET" && $arResult["ACTION"]["PROPS"]["PROP_DATA_TARGETA"]["VALUE"] && isset($actionItem["DYNAM"]["Таргет"])){
					$tmp["VALUE"] = $actionItem["DYNAM"]["Таргет"]."% (".$tmp["VALUE"].$currency_text." от ".$arResult["ACTION"]["PROPS"]["PROP_DATA_TARGETA"]["VALUE"].")";
				}
				if($propCode=="PROP_PROSAD" && $arResult["ACTION"]["PROPS"]["PROP_DATA_PROSADA"]["VALUE"]  && isset($actionItem["DYNAM"]["Просад"])){
					$tmp["VALUE"] = $actionItem["DYNAM"]["Просад"]."% (".$tmp["VALUE"].$currency_text." от ".$arResult["ACTION"]["PROPS"]["PROP_DATA_PROSADA"]["VALUE"].")";
				}
				if($propCode=="PROP_DIVIDENDY_2018"){
					$currency = 'руб.';
					if(!empty($arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"]) && strpos($arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"],"$")!==false){
					  $currency = 'долл.';
					 }

					$tmp["VALUE"] .= " ".$currency." или <b>".$actionItem["DYNAM"]["Дивиденды %"]."%</b>";
					if(!empty($arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"]) ){
						$tmp["VALUE"] .= " ".$arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"];
					}


				}
				
				$arResult["LIST_VALUES"][] = $tmp;
			}
		}
	}
}

//Дивиденды
if($arResult["PROPERTIES"]["DIVIDENDS"]["~VALUE"]["TEXT"]){

    $hlblock   = HL\HighloadBlockTable::getById(21)->fetch();
    $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
    $entityClass = $entity->getDataClass();

    $res2 = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $arResult["CODE"],
        ),
        "select" => array(
            "UF_DATA"
        ),
    ));
    if($elem = $res2->fetch()) {
        $arResult["DIVIDENDS"] = json_decode($elem["UF_DATA"], true);

        $arResult["DIVIDENDS_FIELDS"] = array(
            "Дата закрытия реестра" => true,
				"Дата закрытия реестра (T-2)" => true,
            "Цена на дату закрытия" => true,
				"Дивиденды" => true,
            "Дивиденды, в %" => true,

        );
    }
}

//статьи
$obCache = new CPHPCache();

$cacheLifetime = 86400*365*10;
$cacheID = 'random_articles_for'.$arResult["ID"]; 
$cachePath = '/random_articles/';

if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
{
	$arResult["ARTICLES"] = $obCache->GetVars();
} elseif($obCache->StartDataCache()) {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
	$arFilter = Array("IBLOCK_ID"=>5, "!SECTION_ID"=>array(5, 17), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND"=>"asc"), $arFilter, false, array("nPageSize"=>3), $arSelect);
	while ($ob = $res->GetNext()){
		$arResult["ARTICLES"][] = $ob;
	}

   $obCache->EndDataCache($arResult["ARTICLES"]);
}

//график
//global $USER;
//if($USER->IsAdmin()) {
    $graph = new MoexGraph();
    $arResult["CHARTS_DATA"] = $graph->getForAction($arResult["ACTION"]["PROPS"]["SECID"]["VALUE"]);

/*} else {
    $hlblock = HL\HighloadBlockTable::getById(20)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityClass = $entity->getDataClass();

    $res2 = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $arResult["CODE"],
        ),
        "select" => array(
            "UF_DATA"
        ),
    ));
    if ($elem = $res2->fetch()) {
        $arResult["CHARTS_DATA"] = json_decode($elem["UF_DATA"], true);
        $arResult["CHARTS_DATA"]["real_from"] = new DateTime($arResult["CHARTS_DATA"]["real_from"]["date"]);
        $arResult["CHARTS_DATA"]["from"] = new DateTime($arResult["CHARTS_DATA"]["from"]["date"]);
    }
}*/

//Текст акции
if($arResult["ACTION"] && $arResult["CHARTS_DATA"]){
	if(!$arResult["ACTION"]["PROPS"]["HIDEN"]["VALUE"]){
		if($arResult["CHARTS_DATA"]){
			$arResult["ACTION_TEXT"] = $arResult["NAME"]." (тикер - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].") находится в обращении с ".FormatDate("j F Y", $arResult["CHARTS_DATA"]["real_from"]->getTimestamp())." г. Эмитентом ETF является <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
		}
	} else {
		$arResult["ACTION_TEXT"] = $arResult["NAME"]." (тикер - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].") больше не обращается на московской бирже. Эмитент - <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
	}
	
	$APPLICATION->SetPageProperty("description", $arResult["ACTION_TEXT"]);
}

//Ключевики
$arKeywords = array();
if($arResult["LIST_VALUES"]){
	$arKeywords[] = "Параметры ПИФ ".$arResult["NAME"];
}
if($arResult["CHARTS_DATA"]["items"]){
	$arKeywords[] = "График ПИФ ".$arResult["NAME"];
}
if($arResult["DIVIDENDS"]){
	$arKeywords[] = "Дивиденды по ПИФ ".$arResult["NAME"];
}

if($arKeywords){
	$APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}