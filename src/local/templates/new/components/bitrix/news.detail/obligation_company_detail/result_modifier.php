<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	
	use Bitrix\Highloadblock as HL;

	$arResult["POINTS"] = array("",
															"xxs",
															"xxs xs",
															"xxs xs sm",
															"xxs xs sm md",
															"xxs xs sm md lg"
	);
	$arResult["KVARTAL_MONTH"] = array(1 => "Мар.",
																		 2 => "Июн.",
																		 3 => "Сен.",
																		 4 => "Дек.",
	);
 $arMonthToKvartal = array(1  => 1,
                                 2  => 1,
                                 3  => 1,
                                 4  => 2,
                                 5  => 2,
                                 6  => 2,
                                 7  => 3,
                                 8  => 3,
                                 9  => 3,
                                 10 => 4,
                                 11 => 4,
                                 12 => 4
);
	$isETF = $arResult["PROPERTIES"]["IS_ETF_COMPANY"]["VALUE"] == 'Y' ? true : false;
	$arResult["IS_ETF"] = $isETF;
//Доступ
	$arResult["HAVE_ACCESS"] = checkPayRadar();
   if(!empty($_GET["from_model_portfolio"]) && $_GET["from_model_portfolio"]==true){
	  $arResult["HAVE_ACCESS"] = true;
   }


	CModule::IncludeModule("highloadblock");

//Выбираем всех эмитентов в массив для списка выбора сравнения
$arFilterEmitents = array("IBLOCK_ID"=>26);
$resEmitents = CIBlockElement::GetList(array(),$arFilterEmitents, false, false, array("ID", "NAME"));
$arRusEmitents = array();
while($rowEmitent = $resEmitents->fetch() ){
	$arRusEmitents[$rowEmitent["ID"]] = $rowEmitent["NAME"];
}



$arFilterEm = array("IBLOCK_ID"=>32, "!=PROPERTY_BOARDID"=>$APPLICATION->ExcludeFromRadar["Акции"], "PROPERTY_IS_ETF_ACTIVE"=>false, "PROPERTY_IS_PIF_ACTIVE"=>false);
$arSelectEm = array("NAME", "CODE", "PROPERTY_EMITENT_ID");
$arEmitentsList = array();
$arEmitentsListHTML = "";
$resEm = CIBlockElement::GetList(array("NAME"=>"ASC"),$arFilterEm, false, false, $arSelectEm);
while($rowEm = $resEm->fetch()){
	$emitentName = $arRusEmitents[$rowEm["PROPERTY_EMITENT_ID_VALUE"]];
	if(!in_array($emitentName, $arEmitentsList)){
		$arEmitentsList[$rowEm["CODE"]] = $arRusEmitents[$rowEm["PROPERTY_EMITENT_ID_VALUE"]];
		$arEmitentsListHTML .= '<option value="'.$rowEm["CODE"].'">'.$arRusEmitents[$rowEm["PROPERTY_EMITENT_ID_VALUE"]].'</option>';
	}
}

$arResult["EMITENT_LIST"] = $arEmitentsList;
$arResult["EMITENT_LIST_HTML"] = $arEmitentsListHTML;
unset($rowEmitent, $resEmitents, $arRusEmitents, $rowEm, $resEm);

	if($arResult["PREVIEW_TEXT"]) {
		$obParser = new CTextParser;
		$APPLICATION->SetPageProperty("DESCRIPTION", trim(strip_tags($obParser->html_cut($arResult["PREVIEW_TEXT"], 200))));
	}


//получение компании
	$arFilter = array("IBLOCK_ID" => 26, "NAME" => $arResult["NAME"]);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "NAME"));
	if($item = $res->GetNext()) {
		$arResult["COMPANY"] = $item;
        $arResult["COMPANY"]["CHILD_COMPANY_RUS"] = $arResult["PROPERTIES"]["CHILD_COMPANY_RUS"]["VALUE"];
        $arResult["COMPANY"]["CHILD_COMPANY_USA"] = $arResult["PROPERTIES"]["CHILD_COMPANY_USA"]["VALUE"];
        

        
		//облигации компании и еврооблигации
        $arEmitents_id = array_merge(array($arResult["COMPANY"]["ID"]), $arResult["COMPANY"]["CHILD_COMPANY_RUS"]);
		$arFilter = array("IBLOCK_ID" => 27, "PROPERTY_EMITENT_ID" => $arEmitents_id);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL", "PROPERTY_EMITENT_ID"));
		while($item = $res->GetNext()) {
		    if($item["PROPERTY_EMITENT_ID_VALUE"]==$arResult["COMPANY"]["ID"]) {
                $arResult["COMPANY"]["OBLIGATIONS"][] = $item;
            } else if(in_array($item["PROPERTY_EMITENT_ID_VALUE"], $arResult["COMPANY"]["CHILD_COMPANY_RUS"])) {
                $arResult["COMPANY"]["OBLIGATIONS_EURO"][] = $item;
            }
		}
		

		
		//акции компании
		$arFilter = array("IBLOCK_ID" => 32, "PROPERTY_EMITENT_ID" => $arResult["COMPANY"]["ID"]);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
		while($item = $res->GetNext()) {
			if($isETF) {
				$item["DETAIL_PAGE_URL"] = str_replace("/actions/", "/etf/", $item["DETAIL_PAGE_URL"]);
			}
			$arResult["COMPANY"]["ACTIONS"][] = $item;
		}

        //акции США компании, если она назначена общим эмитентов
        $arResult["COMPANY"]["ACTIONS_USA"] = array();
        $arFilter = array("IBLOCK_ID" => 55, "PROPERTY_COMMON_EMITENT_ID" => $arResult["COMPANY"]["ID"]);
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
        while($item = $res->GetNext()) {
            $item["DETAIL_PAGE_URL"] = $item["DETAIL_PAGE_URL"];
            $arResult["COMPANY"]["ACTIONS_USA"][] = $item;
        }
		
		$arDividendsAction = array();
		
		//Выбираем обыкновенную акцию для компании
		$arFilter = array("IBLOCK_ID"                => 32,
											"PROPERTY_EMITENT_ID"      => $arResult["COMPANY"]["ID"],
											"PROPERTY_HIDEN"           => false,
											"PROPERTY_PROP_TIP_AKTSII" => "Обыкновенная"
		);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("CODE",
																									  "NAME",
																									  "DETAIL_PAGE_URL",
																									  "PROPERTY_"
		));
		$item = false;
		if($ob = $res->GetNext()) {
			$item = $ob;
		} else {
			$arFilter = array("IBLOCK_ID"                => 32,
												"PROPERTY_EMITENT_ID"      => $arResult["COMPANY"]["ID"],
												"PROPERTY_PROP_TIP_AKTSII" => "Привилегированная"
			);
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("CODE", "NAME", "DETAIL_PAGE_URL"));
			if($ob = $res->GetNext()) {
				$item = $ob;
			}
		}
		
		//Соберем все акции для вывода дивидендов
		$arFilterd = array("IBLOCK_ID" => 32, "PROPERTY_EMITENT_ID" => $arResult["COMPANY"]["ID"]);
		$resd = CIBlockElement::GetList(array(), $arFilterd, false, false, array("CODE",
																										 "NAME",
																										 "DETAIL_PAGE_URL",
																										 "PROPERTY_PROP_TIP_AKTSII",
                                                                               "PROPERTY_LASTPRICE",
                                                                               "PROPERTY_LEGALCLOSE",
                                                                               "PROPERTY_SECID",
                                                                               "PROPERTY_PROP_DIVIDENDY_2018"
		));
		while($obd = $resd->GetNext()) {
			$arDividendsAction[$obd["NAME"]] = array("TYPE" => $obd["PROPERTY_PROP_TIP_AKTSII_VALUE"],
																							 "CODE" => $obd["CODE"],
																							 "NAME" => $obd["NAME"],
                                                     "LASTPRICE" =>$obd["PROPERTY_LASTPRICE_VALUE"],
                                                     "LEGALCLOSE" =>$obd["PROPERTY_LEGALCLOSE_VALUE"],
                                                     "SECID" =>$obd["PROPERTY_SECID_VALUE"],
                                                     "DIVIDENDY" =>$obd["PROPERTY_PROP_DIVIDENDY_2018_VALUE"],
			);
		}
		
		
		if(count($arDividendsAction) > 0) {
			$arTmpDiv = array();
			$arFilterdPages = array("IBLOCK_ID" => 33, "NAME" => array_keys($arDividendsAction));
			$resdp = CIBlockElement::GetList(array(), $arFilterdPages, false, false, array("IBLOCK_ID",
																																										 "ID",
																																										 "CODE",
																																										 "NAME",
																																										 "PROPERTY_*"
			));
			while($obdp = $resdp->GetNextElement()) {
				$arFields = $obdp->GetFields();
				$arProps = $obdp->GetProperties();
				if(!empty($arProps["SLIDING_DIVS"]["VALUE"]["TEXT"])) {
					$arTmpDiv[$arFields["NAME"]]["PERIODS"] = unserialize(htmlspecialchars_decode($arProps["SLIDING_DIVS"]["VALUE"]["TEXT"]));
					$arTmpDiv[$arFields["NAME"]]["TYPE"] = $arDividendsAction[$arFields["NAME"]]["TYPE"];
				}
			}
			
			//Добавляем дивы со страницы акции в текущий период если текущего еще нет в массиве с дивами
			$curPeriodName = $arMonthToKvartal[date('m')]."-".date('Y');
            foreach($arDividendsAction as $name=>$actionVal) {
			if(!array_key_exists($curPeriodName,$arTmpDiv[$name]["PERIODS"])) {
                $priceAction = floatval($arDividendsAction[$name]["LASTPRICE"]) > 0 ? floatval($arDividendsAction[$name]["LASTPRICE"]) : floatval($arDividendsAction[$name]["LEGALCLOSE"]);
                $arTmpDiv[$name]["PERIODS"][$curPeriodName] = array(
                  "Скользящие дивиденды"           => $arDividendsAction[$name]["DIVIDENDY"],
                  "Цена акции на начало диапазона" => $priceAction,
                  "Цена акции на конец диапазона"  => $priceAction,
                  "Див. доходность %"              => round($arDividendsAction[$name]["DIVIDENDY"] / $priceAction * 100,
                    2)
                );
            }
            }
			$arResult["DIVIDENDS"] = $arTmpDiv;
			
		}
		unset($arTmpDiv, $arDividendsAction);

		$maxPeriodsCount = 6;

		if($item) {
			$res = new Actions();
			$arResult["ACTION"] = $res->getItem($item["CODE"]);
			$arResult["ACTION"]["PERIODS"] = $arResult["ACTION"]["PERIODS"];

			if($arResult["ACTION"]["PERIODS"]) {
				$i = 1;
				foreach($arResult["ACTION"]["PERIODS"] as $period => $vals) {
					if($vals["Темп прироста выручки"]) {
						$arResult["ACTION"]["Темп роста компании"] += $vals["Темп прироста выручки"];
					}
					if($vals["Темп роста прибыли"]) {
						$arResult["ACTION"]["Темп роста прибыли"] += $vals["Темп роста прибыли"];
					}
					
					$i++;
					if($i >= 4) {
						if($arResult["ACTION"]["Темп роста компании"]) {
							$arResult["ACTION"]["Темп роста компании"] = $arResult["ACTION"]["Темп роста компании"] / 4;
							if($arResult["ACTION"]["Темп роста компании"] > 15) {
								$arResult["ACTION"]["Темп роста компании"] = 15;
							}
						}
						if($arResult["ACTION"]["Темп роста прибыли"]) {
							$arResult["ACTION"]["Темп роста прибыли"] = $arResult["ACTION"]["Темп роста прибыли"] / 4;
							if($arResult["ACTION"]["Темп роста прибыли"] > 15) {
								$arResult["ACTION"]["Темп роста прибыли"] = 15;
							}
						}
						break;
					}
				}
			}
		} else {
			//если нет акции, данные берем с облигации
			if($arResult["COMPANY"]["OBLIGATIONS"][0]) {
				$res = new Obligations();
				$obligationItem = $res->getItem($arResult["COMPANY"]["OBLIGATIONS"][0]["CODE"]);

				$arResult["ACTION"]["PERIODS"] = $obligationItem["PERIODS"];
				
				//Есть ли банковская облигация?
				$arFilter = array("IBLOCK_ID"            => 27,
													"PROPERTY_CSV_VID_OLB" => 35,
													"PROPERTY_EMITENT_ID"  => $arResult["COMPANY"]["ID"]
				);
				$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
				if($res->SelectedRowsCount()) {
					$arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] = "банковские";
				}
			}
		}
	}

  //Получим отраслевые показатели в массив для компании с акциями
$arResult["HIDDEN_INDUSTRY_DATA"] = true;
$arResult["INDUSTRY_MID_VAL"] = array();
if(count($arResult["COMPANY"]["ACTIONS"])>0) {
	$CIndustriesRus = new CIndustriesRus();
	$currSector = $CIndustriesRus->GetSectorByEmitentId($arResult["COMPANY"]["ID"]);
	$x = $CIndustriesRus->getItem($currSector['CODE'], true);

	$arResult["HIDDEN_INDUSTRY_DATA"] = $x["PROPERTY_HIDDEN_VALUE"] != "Y" ? false : true;
	if(count($arResult["COMPANY"]["ACTIONS"]) <= 0 && $arResult["HIDDEN_INDUSTRY_DATA"] == false) {
		$arResult["HIDDEN_INDUSTRY_DATA"] = true;
	}

	$periods = $x['PERIODS'];
	$arResult["INDUSTRY_MID_VAL"] = $periods;
    $arResult["INDUSTRY_MID_VAL_CURRENT_PERIOD"] = reset($periods);
	unset($CIndustriesRus, $periods);
}


//расширенные данные
	$haveExtDataCunked = count($arResult["PROPERTIES"]["EXT_DATA_LONG"]["~VALUE"]) && !empty($arResult["PROPERTIES"]["EXT_DATA_LONG"]["~VALUE"][0]["TEXT"]);
	if($arResult["PROPERTIES"]["EXPANDED"]["VALUE"] || $haveExtDataCunked) {
		
		if(!$haveExtDataCunked) {
			$tmp = json_decode($arResult["PROPERTIES"]["EXPANDED_DATA"]["~VALUE"]["TEXT"], true);
		} else {
			$tmp = '';
			foreach($arResult["PROPERTIES"]["EXT_DATA_LONG"]["~VALUE"] as $value) {
				$tmp .= $value["TEXT"];
			}
			if(!empty($tmp)) {
				$tmp = json_decode($tmp, true);
			}
		}

//БАЛАНС
		foreach($tmp["Бухгалтерский баланс"] as $n => $item) {
			foreach($item["items"] as $period => $v) {
				$arResult["BALANCE"]["PERIODS"][$period] = true;
			}
		}
		if($arResult["BALANCE"]["PERIODS"]) {
			$arResult["BALANCE"]["PERIODS"] = array_reverse($arResult["BALANCE"]["PERIODS"]);
			
			$i = 0;
			foreach($arResult["BALANCE"]["PERIODS"] as $n => $v) {
				if(strpos($n, "4 кв") !== false) {
					$arResult["BALANCE"]["YEAR_PERIODS"][] = $n;
					$i++;
					if($i >= $maxPeriodsCount) {
						break;
					}
				}
			}
			
			//отрезать максимум 5 периодов
			$i = 0;
			foreach($arResult["BALANCE"]["PERIODS"] as $n => $v) {
				$i++;
				if($i > $maxPeriodsCount) {
					unset($arResult["BALANCE"]["PERIODS"][$n]);
				}
			}
		}
		$arResult["BALANCE"]["PERIODS_VALUES"] = $tmp["Бухгалтерский баланс"];
		
		//Отчет о прибылях и убытках
		foreach($tmp["Отчет о прибылях и убытках"] as $n => $item) {
			foreach($item["items"] as $period => $v) {
				$arResult["PLUSMINUS"]["PERIODS"][$period] = true;
			}
		}
		if($arResult["PLUSMINUS"]["PERIODS"]) {
			$arResult["PLUSMINUS"]["PERIODS"] = array_reverse($arResult["PLUSMINUS"]["PERIODS"]);
			
			$i = 0;
			foreach($arResult["PLUSMINUS"]["PERIODS"] as $n => $v) {
				if(strpos($n, "4 кв") !== false) {
					$arResult["PLUSMINUS"]["YEAR_PERIODS"][] = $n;
					$i++;
					if($i >= $maxPeriodsCount) {
						break;
					}
				}
			}
			
			//отрезать максимум 5 периодов
			$i = 0;
			foreach($arResult["PLUSMINUS"]["PERIODS"] as $n => $v) {
				$i++;
				if($i > $maxPeriodsCount) {
					unset($arResult["PLUSMINUS"]["PERIODS"][$n]);
				}
			}
		}
		$arResult["PLUSMINUS"]["PERIODS_VALUES"] = $tmp["Отчет о прибылях и убытках"];
		
		//Отчет о движении денежных средств
		foreach($tmp["Cash Flow (Данные для DCF)"] as $n => $item) {
			foreach($item["items"] as $period => $v) {
				$arResult["CASHFLOW"]["PERIODS"][$period] = true;
			}
		}
		if($arResult["CASHFLOW"]["PERIODS"]) {
			$arResult["CASHFLOW"]["PERIODS"] = array_reverse($arResult["CASHFLOW"]["PERIODS"]);
			
			$i = 0;
			foreach($arResult["CASHFLOW"]["PERIODS"] as $n => $v) {
				if(strpos($n, "4 кв") !== false) {
					$arResult["CASHFLOW"]["YEAR_PERIODS"][] = $n;
					$i++;
					if($i >= $maxPeriodsCount) {
						break;
					}
				}
			}
			
			//отрезать максимум $maxPeriodsCount периодов
			$i = 0;
			foreach($arResult["CASHFLOW"]["PERIODS"] as $n => $v) {
				$i++;
				if($i > $maxPeriodsCount) {
					unset($arResult["CASHFLOW"]["PERIODS"][$n]);
				}
			}
		}
		$arResult["CASHFLOW"]["PERIODS_VALUES"] = $tmp["Cash Flow (Данные для DCF)"];
		
		
		//Темп прироста основных средств - считаем для Анализ денежных потоков
		if($arResult["BALANCE"]) {
			foreach($arResult["BALANCE"]["PERIODS"] as $period => $v) {
				$period = explode(" кв ", $period);
				$prevYear = $period[1] - 1;
				
				$cur = str_replace(" ", "", $arResult["BALANCE"]["PERIODS_VALUES"]["Основные средства"]["items"][$period[0] . " кв " . $period[1]]);
				$prev = str_replace(" ", "", $arResult["BALANCE"]["PERIODS_VALUES"]["Основные средства"]["items"][$period[0] . " кв " . $prevYear]);
				
				if($cur && $prev) {
					//Темп прироста основных средств = (Текущий период / аналогичный период прошлого года - 1 ) х 100
					$arResult["BALANCE"]["Темп прироста основных средств"] = ($cur / $prev - 1) * 100;
				}
				break;
			}
		}
	}




//Ключеные показатели
	if($arResult["ACTION"]["PERIODS"]) {



		foreach($arResult["ACTION"]["PERIODS"] as $period => $vals) {
			if($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
				if($vals["PERIOD_TYPE"] == "MONTH") {
					continue;
				}
			}

			$arResult["EXPANDED_PERIODS"]["PERIODS"][] = $period;
			if(count($arResult["EXPANDED_PERIODS"]["PERIODS"]) >= $maxPeriodsCount) {
 			break;
			}
		}
		foreach($arResult["EXPANDED_PERIODS"]["PERIODS"] as $period) {
			foreach($arResult["ACTION"]["PERIODS"][$period] as $f => $v) {
				if($f == "PERIOD_YEAR" || $f == "PERIOD_VAL" || $f == "PERIOD_TYPE") {
					continue;
				}
				$arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"][$f] = true;
			}
		}
		$arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"]);
		
		$needPeriod = 4;
/*		if($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
			$needPeriod = 12;
		}*/

		foreach($arResult["ACTION"]["PERIODS"] as $period => $vals) {
			if($vals["PERIOD_VAL"] == $needPeriod) {

				$arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"][] = $period;
				
				if(count($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) >= $maxPeriodsCount) {
					break;
				}
			}
		}
		
		foreach($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $period) {
			foreach($arResult["ACTION"]["PERIODS"][$period] as $f => $v) {
				if($f == "PERIOD_YEAR" || $f == "PERIOD_VAL" || $f == "PERIOD_TYPE") {
					continue;
				}
				$arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"][$f] = true;
			}
		}
		$arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"]);
		
		if($arResult["ACTION"]["PROPS"]["SECID"]) {
			$graph = new MoexGraph();
			
			//для графика квартала, берем последний - 1 квартал
			if($arResult["EXPANDED_PERIODS"]["PERIODS"]) {
				$arKvartalToMonth = array(1 => "03-31",
																	2 => "06-30",
																	3 => "09-30",
																	4 => "12-31",
				);
				
				$t = array_reverse($arResult["EXPANDED_PERIODS"]["PERIODS"]);
				$last = explode("-", $t[0]);

					if($last[0] == 1) {
						$last[0] = 4;
						$last[1] -= 1;
					} else {
						$last[0] -= 1;
					}
					$from = $last[1] . "-" . $arKvartalToMonth[$last[0]];

				/*
				1 квартал с 1 января по 31 марта
				2 квартал с 1 апреля по 30 июня
				3 квартал с 1 июля по 30 сентября
				4 квартал с 1 октбяря по 31 декабря
				*/
				
				//график
				//if($USER->IsAdmin()) {
				$arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"] = $graph->getForActionByMonthWithFrom($arResult["ACTION"]["PROPS"]["SECID"], $from);

				/*} else {
						$obCache = new CPHPCache();
						$cacheLifetime = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")) - strtotime("now");
						//$cacheLifetime = 0;
						$cacheID = 'item' . $arResult["ID"] . "-" . intval($arResult["HAVE_ACCESS"]);
						$cachePath = '/company_kvartal_chart/';

						if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
								$arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"] = $obCache->GetVars();
						} elseif ($obCache->StartDataCache()) {
								$url = "http://iss.moex.com/iss/engines/stock/markets/shares/securities/" . $arResult["ACTION"]["PROPS"]["SECID"] . "/candles.json?from=" . $from . "&till=" . date("Y-m-d") . "&interval=31&start=0";

								$chartData = ConnectMoex($url);

								if ($chartData["candles"]["data"]) {
										foreach ($chartData["candles"]["data"] as $item) {
												$dt = new DateTime($item[7]);
												$arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"][] = array(
														FormatDate("M. Y", $dt->getTimestamp()),
														$item[1],
												);
										}
								}
								$obCache->EndDataCache($arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"]);
						}
				}*/
			}
			
			//для года берем начала последнего года
			if($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) {
				$t = array_reverse($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]);
				$last = explode("-", $t[0]);

					$from = $last[1] . "-01-01";

				
				//график
				
				//if($USER->IsAdmin()) {
				$arResult["EXPANDED_PERIODS"]["YEAR_CHART"] = $graph->getForActionByMonthWithFrom($arResult["ACTION"]["PROPS"]["SECID"], $from);
				
				
				/*} else {
						$obCache = new CPHPCache();
						$cacheLifetime = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")) - strtotime("now");
						//$cacheLifetime = 0;
						$cacheID = 'item' . $arResult["ID"];
						$cachePath = '/company_year_chart/';

						if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
								$arResult["EXPANDED_PERIODS"]["YEAR_CHART"] = $obCache->GetVars();
						} elseif ($obCache->StartDataCache()) {
								$url = "http://iss.moex.com/iss/engines/stock/markets/shares/securities/" . $arResult["ACTION"]["PROPS"]["SECID"] . "/candles.json?from=" . $from . "&till=" . date("Y-m-d") . "&interval=31&start=0";
								$chartData = ConnectMoex($url);

								if ($chartData["candles"]["data"]) {
										foreach ($chartData["candles"]["data"] as $item) {
												$dt = new DateTime($item[7]);
												$arResult["EXPANDED_PERIODS"]["YEAR_CHART"][] = array(
														FormatDate("M. Y", $dt->getTimestamp()),
														$item[1],
												);
										}
								}
								$obCache->EndDataCache($arResult["EXPANDED_PERIODS"]["YEAR_CHART"]);
						}
				}*/
			}
		}
		
		//для года берем начала последнего года
		//http://iss.moex.com/iss/engines/stock/markets/shares/securities/LKOH/candles.json?from=2015-01-01&till=2018-03-29&interval=31&start=0
	}

//Анализ денежных потоков - Года
	if($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) {
		foreach($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $period) {
            if($arResult["ACTION"]["PERIODS"][$period]["CURRENT_PERIOD"]==true) continue;
			$t = explode("-", $period);
			for($i = $t[1]; $i <= $t[1] + 5; $i++) {
				$arResult["ANALIZ_MONEY"]["YEARS"][] = $i;
			}
			break;
		}
	}


//статьи
	$obCache = new CPHPCache();
	
	$cacheLifetime = 86400 * 365 * 10;
	$cacheID = 'random_articles_for' . $arResult["ID"];
	$cachePath = '/random_articles/';
	
	if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
		$arResult["ARTICLES"] = $obCache->GetVars();
	} elseif($obCache->StartDataCache()) {
		$arSelect = array("ID",
											"IBLOCK_ID",
											"NAME",
											"DATE_ACTIVE_FROM",
											"PREVIEW_TEXT",
											"PREVIEW_PICTURE",
											"DETAIL_PAGE_URL",
											"SHOW_COUNTER"
		);
		$arFilter = array("IBLOCK_ID" => 5, "!SECTION_ID" => array(5, 17), "ACTIVE" => "Y");
		$res = CIBlockElement::GetList(array("RAND" => "asc"), $arFilter, false, array("nPageSize" => 3), $arSelect);
		while($ob = $res->GetNext()) {
			$arResult["ARTICLES"][] = $ob;
		}
		
		$obCache->EndDataCache($arResult["ARTICLES"]);
	}

//Ключевики
	$arKeywords = array($arResult["NAME"]);
	
	if($arResult["PROPERTIES"]["EXPANDED"]["VALUE"]) {
		if($arResult["EXPANDED_PERIODS"]) {
			$arKeywords[] = "Ключевые показатели";
		}
		if($arResult["BALANCE"]) {
			$t = "Баланс";
			if($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
				$t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
			}
			
			$arKeywords[] = $t;
		}
		if($arResult["PLUSMINUS"]) {
			$t = "Отчет о прибылях и убытках";
			if($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
				$t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
			}
			
			$arKeywords[] = $t;
		}
		if($arResult["CASHFLOW"]) {
			$t = "Отчет о движении денежных средств";
			if($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
				$t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
			}
			
			$arKeywords[] = $t;
		}
		if($arResult["PROPERTIES"]["BUSANALIZ"]["~VALUE"]["TEXT"]) {
			$arKeywords[] = "Бизнес-анализ";
		}
		if($arResult["ACTION"]) {
			$arKeywords[] = "Инвестиционный анализ";
		}
		if($arResult["ANALIZ_MONEY"]["YEARS"]) {
			$arKeywords[] = "Анализ денежных потоков";
		}
	} else {
		if($arResult["COMPANY"]["REPORT_FIELDS"]) {
			$arKeywords[] = "Финансовые показатели компании " . $arResult["COMPANY"]["NAME"];
		}
		if($arResult["COMPANY"]["ACTIONS"]) {
			if($isETF == true) {
				$labelText = 'ETF ';
			} else {
				$labelText = 'акций ';
			}
			
			$arKeywords[] = "Список " . $labelText . $arResult["COMPANY"]["NAME"];
		}
		if($arResult["COMPANY"]["OBLIGATIONS"]) {
			$arKeywords[] = "Список облигаций " . $arResult["COMPANY"]["NAME"];
		}
	}
	
	if($arKeywords) {
		$APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
	}
	
	
	/*var_dump(json_encode($arResult["ACTION"]["PERIODS"]));
	die();*/