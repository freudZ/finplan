<?
$MESS["COMPANY_PROP_TBL_ACTIVES"] = "это все принадлежащие компании материальные и нематериалые ценности, запасы, денежные средства и долговые обязательства.";
$MESS["COMPANY_PROP_TBL_CURRENT_ASSETS"] = "это наиболее ликвидные активы компании, которые потребляются в процессе производства (к примеру, запасы, денежные средства и т.п.).";
$MESS["COMPANY_PROP_TBL_EQUITY"] = "это объем собственных средств акционеров компании, инвестированных в бизнес.";
$MESS["COMPANY_PROP_TBL_REVENUE_REPORT"] = "это суммарные доходы от продажи основных видов продукции предприятия или услуг. Данные взяты из отчета, т.е. нарастающим итогом с начала года";
$MESS["COMPANY_PROP_TBL_REVENUE_LAST_YEAR"] = "это суммарные доходы от продажи основных видов продукции предприятия или услуг. Данные взяты из отчета, т.е. нарастающим итогом за аналогичный период прошлого года";
$MESS["COMPANY_PROP_TBL_PROFIT_REPORT"] = "это финансовый результат компании по итогам периода. Данные взяты из отчета, т.е. нарастающим итогом с начала года";
$MESS["COMPANY_PROP_TBL_REVENUE_QUARTAL"] = "Если компания отчитывается поквартально, то выручка за последний квартал, рассчитана как разница между показателями 'Выручка (Отчет)' за текущий и предыдущий период.";
$MESS["COMPANY_PROP_TBL_PROFIT_QUARTAL"] = "Если компания отчитывается поквартально, то прибыль за последний квартал, рассчитана как разница между показателями 'Прибыль (Отчет)' за текущий и предыдущий период.";
$MESS["COMPANY_PROP_TBL_REVENUE_SEMI_ANNUAL"] = "Если компания отчитывается по полугодиям, то выручка за полугодие, рассчитана как разница между показателями 'Выручка (Отчет)' за текущий период и предыдущее полугодие.";
$MESS["COMPANY_PROP_TBL_PROFIT_SEMI_ANNUAL"] = "Если компания отчитывается по полугодиям, то выручка за полугодие, рассчитана как разница между показателями 'Выручка (Отчет)' за текущий период и предыдущее полугодие.";
$MESS["COMPANY_PROP_TBL_EBITDA"] = "это прибыль компании до уплаты % по кредитам, налогов и амортизационных отчислений. Показатель EBITDA отражает результат от операционной деятельности компании. <a href='https://fin-plan.org/blog/investitsii/raschet-ebitda/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["COMPANY_PROP_TBL_REVENUE_YEAR_ROLLING"] = "это расчетный показатель, который рассчитывает выручку ровно за 1 год до даты отчета (сумма поквартальных или полугодовых данных). Сравнение по этому показателю позволяет адекватно оценивать показатели без учета сезонности и прочих некорректно влияющих факторов.";
$MESS["COMPANY_PROP_TBL_PROFIT_YEAR_ROLLING"] = "это расчетный показатель, который рассчитывает прибыль ровно за 1 год до даты отчета (сумма поквартальных или полугодовых данных). Сравнение по этому показателю позволяет адекватно оценивать показатели без учета сезонности и прочих некорректно влияющих факторов.";
$MESS["COMPANY_PROP_TBL_PROFIT_GROWTH_RATE"] = "это отношение скользящей прибыли за указанный период к данным за аналогичный период прошлого года - 1 *100%";
$MESS["COMPANY_PROP_TBL_REVENUE_GROWTH_RATE"] = "это отношение скользящей выручки за указанный период к данным за аналогичный период прошлого года - 1 *100%";
$MESS["COMPANY_PROP_TBL_ACTIVES_GROWTH_RATE"] = "это отношение активов компании за указанный период к данным за аналогичный период прошлого года - 1 *100%";
$MESS["COMPANY_PROP_TBL_RETURN_OF_EQUITY"] = "показатель эффективности вложенных собственных средств, рассчитывается как отношение скользящей чистой прибыли к собственному капиталу";
$MESS["COMPANY_PROP_TBL_SHARE_OF_EQUITY_IN_ASSETS"] = "показатель финансовой устойчивости компании, рассчитанный как отношение собственного капитала к общей сумме активов";
$MESS["COMPANY_PROP_TBL_DEBT_EBITDA"] = "соотношение обязательств и EBITDA. Отражает способность предприятия исполнять свои финансовые обязательства. Приемлемым считается уровень данного коэффициента ниже 3";
$MESS["COMPANY_PROP_TBL_PAST_CAPITALIZATION"] = "это капитализация на момент выхода отчета. Капитализация - это рыночная оценка стоимости компании, рассчитанная на основе текущих цен выпущенных акций. Показатель позволяет оценить размеры компании-эмитента";
$MESS["COMPANY_PROP_TBL_PE"] = "отношение капитализации компании к скользящей чистой прибыли. Данный мультипликатор показывает уровень недооценки актива. Чем ниже мультипликатор, тем выше недооценка.";
$MESS["COMPANY_PROP_TBL_PB"] = "отношение капитализации компании к активам компании. Значение показателя менее 1 означает, что актив стоит меньше, чем размер активов. Часто применяется в банковском секторе.";
$MESS["COMPANY_PROP_TBL_P_EQUITY"] = "отношение капитализации компании к собственному капиталу. Значение показателя менее 1 означает, что актив стоит меньше, чем размер собственного капитала.";
$MESS["COMPANY_PROP_TBL_P_SALE"] = "отношение капитализации компании к скользящей выручке. Значение показателя менее 1 означает, что актив стоит меньше, чем размер выручки.";
$MESS["COMPANY_PROP_TBL_EV_EBITDA"] = "отношение суммы капитализации компании и долгов к Ebitda. Данный мультипликатор показывает уровень недооценки актива. Чем ниже мультипликатор, тем выше недооценка.";
$MESS["COMPANY_PROP_TBL_PEG"] = "PEG = это отношение P/E к средним темпам прироста прибыли за 5 лет. Чем меньше значение показателя, тем выше недооценка актива.";
$MESS["COMPANY_PROP_TBL_DIVIDENDS_OB_RUR"] = "это расчетный показатель в рублях, который рассчитывает дивиденды ровно за 1 год до даты отчета.";
$MESS["COMPANY_PROP_TBL_DIVIDENDS_OB_PRC"] = "это расчетный показатель в %, который рассчитывает дивиденды ровно за 1 год до даты отчета.";
 ?>