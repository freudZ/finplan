<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;

$arResult["POINTS"] = array(
	"",
	"xxs",
	"xxs xs",
	"xxs xs sm",
	"xxs xs sm md",
	"xxs xs sm md lg"
);

//Доступ
$arResult["HAVE_ACCESS"] = checkPayRadar();
$arResult["CLOSED_NO_ACCESS_PROPS"] = array(
	"Target",
	"Просад",
    "Методика расчета дивидендов",
    "P/E",
);


//Кейсы
/*$arResult["CASE"] = array();
$arFilter = Array("IBLOCK_ID"=>48, "ACTIVE"=>"Y", "PROPERTY_LINKED_ACTIONS"=>$arResult["ACTION"]["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($item = $res->GetNextElement())
{
	$arResult["CASE"][] = $item->GetFields();
}*/


//список данных
	$res = new CFutures();

	$futureItem = $res->getItem($arResult["CODE"]);

	foreach(array("LATNAME", "ASSETCODE", "SECID", "SECTYPE", "LOTVOLUME", "PREVPRICE", "LASTSETTLEPRICE", "PREVSETTLEPRICE", "MINSTEP", "FIRSTTRADEDATE", "LASTTRADEDATE", "PREVOPENPOSITION", "INITIALMARGIN", "") as $propCode){
		if($propCode == "LOTVOLUME") {
            $arResult["LIST_VALUES"][] = array(
                "NAME" => $arResult["PROPERTIES"][$propCode]["NAME"],
                "VALUE" => $arResult["PROPERTIES"][$propCode]["VALUE"] . "",
            );

        } elseif($propCode == "ASSETCODE"){
            $arResult["LIST_VALUES"][] = array(
                "NAME" => $arResult["PROPERTIES"][$propCode]["NAME"],
                "VALUE" => "<a class=\"green\" href=\"".$futureItem["ASSET"]["URL"]."\" target=\"_blank\">".$arResult["PROPERTIES"][$propCode]["VALUE"]."</a>",
            );
		} elseif($propCode == "PREVPRICE"){
            $arResult["LIST_VALUES"][] = array(
                "NAME" => $arResult["PROPERTIES"][$propCode]["NAME"],
                "VALUE" => number_format($arResult["PROPERTIES"][$propCode]["VALUE"] , 2, '.', ' '). " руб.",
            );
		} elseif($propCode == "LASTSETTLEPRICE"){
            $arResult["LIST_VALUES"][] = array(
                "NAME" => $arResult["PROPERTIES"][$propCode]["NAME"],
                "VALUE" => number_format($arResult["PROPERTIES"][$propCode]["VALUE"] , 2, '.', ' '). " руб.",
					 "COLOR" => "green",
            );
        } elseif($propCode == "PREVSETTLEPRICE"){
            $arResult["LIST_VALUES"][] = array(
                "NAME" => $arResult["PROPERTIES"][$propCode]["NAME"],
                "VALUE" => number_format($arResult["PROPERTIES"][$propCode]["VALUE"] , 2, '.', ' '). " руб.",
            );
        } elseif($propCode == "PEG"){
            if(!$actionItem["DYNAM"]["PEG"]){
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "PEG",
                "VALUE" => $actionItem["DYNAM"]["PEG"],
            );
        } elseif($propCode == "ACTION_DYNAMIC"){

        } else {
			if($arResult["PROPERTIES"][$propCode]["VALUE"]){
				$tmp = array(
					"NAME" => $arResult["PROPERTIES"][$propCode]["NAME"],
					"VALUE" => $arResult["PROPERTIES"][$propCode]["VALUE"],
				);
				$arResult["LIST_VALUES"][] = $tmp;
			}
		}
	}

//Получаем компанию


//статьи
$obCache = new CPHPCache();

$cacheLifetime = 0;//86400*365*10;
$cacheID = 'random_articles_for'.$arResult["ID"];
$cachePath = '/random_articles/';

if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
{
	$arResult["ARTICLES"] = $obCache->GetVars();
} elseif($obCache->StartDataCache()) {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
	$arFilter = Array("IBLOCK_ID"=>5, "!SECTION_ID"=>array(5, 17), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND"=>"asc"), $arFilter, false, array("nPageSize"=>3), $arSelect);
	while ($ob = $res->GetNext()){
		$arResult["ARTICLES"][] = $ob;
	}

   $obCache->EndDataCache($arResult["ARTICLES"]);
}

//график
//global $USER;
//if($USER->IsAdmin()) {
    $FuturesGraph = new MoexGraph();
    $arResult["CHARTS_DATA"] = $FuturesGraph->getForFutures($arResult["CODE"], 38, 'futures', $arResult["PROPERTIES"]["ASSETCODE"]["VALUE"], 0);
	 //Готовим данные для линий таргета и просада на графике котировок
	 $target = $arResult["ACTION"]["PROPS"]["PROP_TARGET"]["VALUE"];
	 $targetPrc = $actionItem["DYNAM"]["Таргет"];
	 $arResult["CHARTS_DATA"]["target"] = array(array(str_replace("-","/",$arResult["CHARTS_DATA"]["min"]).' 00:00:00', $target, $targetPrc), array(str_replace("-","/",$arResult["CHARTS_DATA"]["max"]).' 00:00:00', $target, $targetPrc));
	 $arResult["CHARTS_DATA"]["target"] = json_encode($arResult["CHARTS_DATA"]["target"]);
	 $drawdown = $arResult["ACTION"]["PROPS"]["PROP_PROSAD"]["VALUE"];
	 $drawdownPrc = $actionItem["DYNAM"]["Просад"];
	 $arResult["CHARTS_DATA"]["drawdown"] = array(array(str_replace("-","/",$arResult["CHARTS_DATA"]["min"]).' 00:00:00', $drawdown, $drawdownPrc), array(str_replace("-","/",$arResult["CHARTS_DATA"]["max"]).' 00:00:00', $drawdown, $drawdownPrc));
	 $arResult["CHARTS_DATA"]["drawdown"] = json_encode($arResult["CHARTS_DATA"]["drawdown"]);

    //$arResult["CHARTS_DATA"] = $indexGraph->getForAction('ALRS');


 /*	 echo $arResult["CODE"]."<pre  style='color:black; font-size:11px;'>";
       print_r($arResult["CHARTS_DATA"]);
       echo "</pre>";*/

/*} else {
    $hlblock = HL\HighloadBlockTable::getById(20)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityClass = $entity->getDataClass();

    $res2 = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $arResult["CODE"],
        ),
        "select" => array(
            "UF_DATA"
        ),
    ));
    if ($elem = $res2->fetch()) {
        $arResult["CHARTS_DATA"] = json_decode($elem["UF_DATA"], true);
        $arResult["CHARTS_DATA"]["real_from"] = new DateTime($arResult["CHARTS_DATA"]["real_from"]["date"]);
        $arResult["CHARTS_DATA"]["from"] = new DateTime($arResult["CHARTS_DATA"]["from"]["date"]);
    }
}*/
//Текст акции
	if(new DateTime($arResult["PROPERTIES"]["LASTTRADEDATE"]["VALUE"])>=new DateTime()){
	   $arResult["FUTURE_TEXT"] = $arResult["NAME"]." (тикер - ".$arResult["PROPERTIES"]["SECID"]["VALUE"].") находится в обращении с ".FormatDate("j F Y\г\.", (new DateTime($arResult["PROPERTIES"]["FIRSTTRADEDATE"]["VALUE"]))->getTimestamp());
		if($futureItem["COMPANY"]){
			$arResult["FUTURE_TEXT"] .= " Эмитентом является <a href='".$futureItem["COMPANY"]["URL"]."'  target='_blank'>".$futureItem["COMPANY"]["NAME"]."</a>.";
		}
	} else {
		$arResult["FUTURE_TEXT"] = $arResult["NAME"]." (тикер - ".$arResult["PROPERTIES"]["SECID"]["VALUE"].") больше не обращается на московской бирже.";
		if($futureItem["COMPANY"]){
			$arResult["FUTURE_TEXT"] .= " Эмитент - <a href='".$futureItem["COMPANY"]["URL"]."' target='_blank'>".$futureItem["COMPANY"]["NAME"]."</a>.";
		}
	}

	$APPLICATION->SetPageProperty("description", $arResult["FUTURE_TEXT"]);


//Получаем привязанные акции
/* $arResult["INDEX_STAFF"] = array();

//Если это IMOEX - для IMOEX - с признаком ММВБ = Да
if($arResult["CODE"]=="IMOEX" && empty($arResult["PROPERTIES"]["ACTIVES"]["VALUE"])){
	$arrFilter["=PROPERTY_PROP_MMVB"] = "Да";
} else {
	$arrFilter["ID"] = $arResult["PROPERTIES"]["ACTIVES"]["VALUE"];
}



if(count($arrFilter["ID"])>0 || $arrFilter["=PROPERTY_PROP_MMVB"]=="Да"){

 $arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID");
 $arFilter = Array("IBLOCK_ID"=>IntVal(32), "=PROPERTY_HIDEN"=>false, "ACTIVE"=>"Y");
  $arFilter = array_merge($arFilter, $arrFilter);
 $resA = new Actions();
 $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

 while($ob = $res->GetNextElement()){
  $arFields = $ob->GetFields();
  $arResult["INDEX_STAFF"][] = $resA->getItem($arFields["CODE"]);
 }
  unset($resA);

}*/
 //Текущая цена индекса
 $v = (new DateTime(date()))->modify("-1 days");
    if(isWeekEndDay($v->format("d.m.Y"))){
        $finded = false;
        while(!$finded){
            $v->modify("-1 days");
            if(!isWeekEndDay($v->format("d.m.Y"))){
                $finded = true;
            }
        }
    }

 CModule::IncludeModule("highloadblock");
$hlblock   = HL\HighloadBlockTable::getById(38)->fetch();
$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
$moexIndexData = $entity->getDataClass();

 $result = $moexIndexData::getList([
            "filter" => [
                "UF_DATE" => $v->format("d.m.Y"),
                "UF_ITEM" => $arResult["CODE"],
            ],
            "select" => [
                "UF_CLOSE"
            ],
        ])->fetch();
 $arResult["DYNAM"]["CLOSEPRICE"] = $result['UF_CLOSE'];


 //Расчет приростов
/* if ($arResult["DYNAM"]["CLOSEPRICE"] && $arResult["PROPERTIES"]["QUOTATIONS_MONTH"]["VALUE"]) {
         $arResult["DYNAM"]["MONTH_INCREASE"] = round(((floatval($arResult["DYNAM"]["CLOSEPRICE"]) / floatval($arResult["PROPERTIES"]["QUOTATIONS_MONTH"]["VALUE"]) - 1) * 100), 2);
 }
 if ($arResult["DYNAM"]["CLOSEPRICE"] && $arResult["PROPERTIES"]["QUOTATIONS_ONE_YEAR"]["VALUE"]) {
         $arResult["DYNAM"]["ONE_YEAR_INCREASE"] = round(((floatval($arResult["DYNAM"]["CLOSEPRICE"]) / floatval($arResult["PROPERTIES"]["QUOTATIONS_ONE_YEAR"]["VALUE"]) - 1) * 100), 2);
 }
 if ($arResult["DYNAM"]["CLOSEPRICE"] && $arResult["PROPERTIES"]["QUOTATIONS_THREE_YEAR"]["VALUE"]) {
         $arResult["DYNAM"]["THREE_YEAR_INCREASE"] = round(((floatval($arResult["DYNAM"]["CLOSEPRICE"]) / floatval($arResult["PROPERTIES"]["QUOTATIONS_THREE_YEAR"]["VALUE"]) - 1) * 100), 2);
 }*/

//Ключевики
$arKeywords = array();
//if($arResult["LIST_VALUES"]){
	$arKeywords[] = "Параметры контракта ".$arResult["NAME"];
//}
if($arResult["CHARTS_DATA"]["items"]){
	$arKeywords[] = "График контракта ".$arResult["NAME"];
}


if($arKeywords){
	$APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}