<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Highloadblock as HL;

global $APPLICATION;

$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cacheId = "company_usa_data".$arResult["ID"];
$cacheTtl = 86400 * 7;

$arResult["POINTS"] = array(
    "",
    "xxs",
    "xxs xs",
    "xxs xs sm",
    "xxs xs sm md",
    "xxs xs sm md lg"
);
$arResult["KVARTAL_MONTH"] = array(
    1 => "Мар.",
    2 => "Июн.",
    3 => "Сен.",
    4 => "Дек.",
);

//Доступ
$arResult["HAVE_ACCESS"] = checkPayUSARadar();
//$arResult["HAVE_ACCESS"] = checkPayRadar();

  if(!empty($_GET["from_model_portfolio"]) && $_GET["from_model_portfolio"]==true){
	  $arResult["HAVE_ACCESS"] = true;
  }

CModule::IncludeModule("highloadblock");

//Выбираем всех эмитентов в массив для списка выбора сравнения
$arFilterEm = array("IBLOCK_ID"=>55);
$arSelectEm = array("NAME", "CODE");
$arEmitentsList = array();
$arEmitentsListHTML = "";
$resEm = CIBlockElement::GetList(array("NAME"=>"ASC"),$arFilterEm, false, false, $arSelectEm);
while($rowEm = $resEm->fetch()){
	$arEmitentsList[$rowEm["CODE"]] = $rowEm["NAME"];
	$arEmitentsListHTML .= '<option value="'.$rowEm["CODE"].'">'.$rowEm["NAME"].'</option>';
}

$arResult["EMITENT_LIST"] = $arEmitentsList;
$arResult["EMITENT_LIST_HTML"] = $arEmitentsListHTML;

if ($arResult["PREVIEW_TEXT"]) {
    $obParser = new CTextParser;
    $APPLICATION->SetPageProperty("DESCRIPTION", trim(strip_tags($obParser->html_cut($arResult["PREVIEW_TEXT"], 200))));
}


/*if ($cache->read($cacheTtl, $cacheId)) {
            $arResultCompany = $cache->get($cacheId);
        } else {*/

//получение компании
$arFilter = Array("IBLOCK_ID" => 43, "NAME" => $arResult["NAME"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME"));
if ($item = $res->GetNext()) {
    $arResultCompany["COMPANY"] = $item;

    /*//облигации компании
    $arFilter = Array("IBLOCK_ID"=>27, "PROPERTY_EMITENT_ID"=>$arResult["COMPANY"]["ID"]);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
    while($item = $res->GetNext())
    {
        $arResult["COMPANY"]["OBLIGATIONS"][] = $item;
    }*/

    //акции компании
    $arFilter = Array("IBLOCK_ID" => 55, "PROPERTY_EMITENT_ID" => $arResultCompany["COMPANY"]["ID"]);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
    while ($item = $res->GetNext()) {
        $arResultCompany["COMPANY"]["ACTIONS"][] = $item;
    }

    //Выбираем обыкновенную акцию для компании
    $arFilter = Array("IBLOCK_ID" => 55, "PROPERTY_EMITENT_ID" => $arResultCompany["COMPANY"]["ID"], "PROPERTY_SPECIES" => "Обыкновенная");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("CODE", "NAME", "DETAIL_PAGE_URL"));
    if ($ob = $res->GetNext()) {
        $item = $ob;
    } else {
        $arFilter = Array("IBLOCK_ID" => 55, "PROPERTY_EMITENT_ID" => $arResultCompany["COMPANY"]["ID"], "!PROPERTY_PROP_TIP_AKTSII");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("CODE", "NAME", "DETAIL_PAGE_URL"));
        if ($ob = $res->GetNext()) {
            $item = $ob;
        }
    }

    if ($item) {
        $arResultCompany["ACTION"] = (new ActionsUsa())->getItem($item["CODE"]);
    }

    //отчеты

    /*$hlblock = HL\HighloadBlockTable::getById(22)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityClass = $entity->getDataClass();

    $res = $entityClass::getList(array(
        "filter" => array(
            "UF_COMPANY" => $arResult["COMPANY"]["ID"],
            "!UF_YEAR" => false,
        ),
        "limit" => 15,
        "order" => array(
            "ID" => "desc",
        ),
        "select" => array(
            "*"
        ),
    ));

    while ($item = $res->fetch()) {
        $item["UF_DATA"] = json_decode($item["UF_DATA"], true);

        $t = $item["UF_MONTH"];
        $type = "MONTH";

        if (!$t) {
            $t = $item["UF_KVARTAL"];
            $type = "KVARTAL";
        }
        if ($t && $item["UF_YEAR"]) {
            $item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
            $item["UF_DATA"]["PERIOD_VAL"] = $t;
            $item["UF_DATA"]["PERIOD_TYPE"] = $type;
            $arResultCompany["ACTION"]["PERIODS"][$t . "-" . $item["UF_YEAR"] . "-" . $type] = $item["UF_DATA"];
        }
        if (!$t && $item["UF_YEAR"]) {
            $item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
            $item["UF_DATA"]["PERIOD_TYPE"] = "YEAR";
            $arResultCompany["ACTION"]["PERIODS"][$item["UF_YEAR"] . "-" . $type] = $item["UF_DATA"];
        }
    }

    $arResultCompany["ACTION"]["PERIODS"] = (new RadarBase())->calculatePeriodData($arResult["ACTION"]);*/

    foreach ($arResultCompany["ACTION"]["PERIODS"] as $k => $v) {
        $x[] = $k;
        unset($arResultCompany["ACTION"]["PERIODS"][$k]['PEG']);
    }

}

/*$cache->set($cacheId, $arResultCompany);
}*/

$arResult = array_merge($arResult, $arResultCompany);

//Получим отраслевые показатели в массив для компании с акциями
$arResult["HIDDEN_INDUSTRY_DATA"] = true;
$arResult["INDUSTRY_MID_VAL"] = array();
if(count($arResult["COMPANY"]["ACTIONS"])>0) {
	$CIndustriesUsa = new SectorsUsa();
	$currSector = $CIndustriesUsa->GetSectorByName($arResult["ACTION"]["PROPS"]["SECTOR"]);
	$x = $CIndustriesUsa->getItem($currSector['CODE'], true);

	global $USER;
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();

	$arResult["HIDDEN_INDUSTRY_DATA"] = $x["PROPERTY_HIDDEN_VALUE"] != "Y" ? false : true;
	if(count($arResult["COMPANY"]["ACTIONS"]) <= 0 && $arResult["HIDDEN_INDUSTRY_DATA"] == false) {
		$arResult["HIDDEN_INDUSTRY_DATA"] = true;
	}

	$periods = $x['PERIODS'];
	$arResult["INDUSTRY_MID_VAL"] = $periods;
	unset($CIndustriesUsa, $periods);
}


$maxPeriodsCount = 6;
//расширенные данные
if ($arResult["PROPERTIES"]["EXPANDED"]["VALUE"]) {

    $tmp = json_decode($arResult["PROPERTIES"]["EXPANDED_DATA"]["~VALUE"]["TEXT"], true);

    //БАЛАНС
    foreach ($tmp["Бухгалтерский баланс"] as $n => $item) {
        foreach ($item["items"] as $period => $v) {
            $arResult["BALANCE"]["PERIODS"][$period] = true;
        }
    }
    if ($arResult["BALANCE"]["PERIODS"]) {
        $arResult["BALANCE"]["PERIODS"] = array_reverse($arResult["BALANCE"]["PERIODS"]);

        $i = 0;
        foreach ($arResult["BALANCE"]["PERIODS"] as $n => $v) {
            if (strpos($n, "4 кв") !== false) {
                $arResult["BALANCE"]["YEAR_PERIODS"][] = $n;
                $i++;
                if ($i >= $maxPeriodsCount) {
                    break;
                }
            }
        }

        //отрезать максимум $maxPeriodsCount периодов
        $i = 0;
        foreach ($arResult["BALANCE"]["PERIODS"] as $n => $v) {
            $i++;
            if ($i > $maxPeriodsCount) {
                unset($arResult["BALANCE"]["PERIODS"][$n]);
            }
        }
    }
    $arResult["BALANCE"]["PERIODS_VALUES"] = $tmp["Бухгалтерский баланс"];

    //Отчет о прибылях и убытках
    foreach ($tmp["Отчет о прибылях и убытках"] as $n => $item) {
        foreach ($item["items"] as $period => $v) {
            $arResult["PLUSMINUS"]["PERIODS"][$period] = true;
        }
    }
    if ($arResult["PLUSMINUS"]["PERIODS"]) {
        $arResult["PLUSMINUS"]["PERIODS"] = array_reverse($arResult["PLUSMINUS"]["PERIODS"]);

        $i = 0;
        foreach ($arResult["PLUSMINUS"]["PERIODS"] as $n => $v) {
            if (strpos($n, "4 кв") !== false) {
                $arResult["PLUSMINUS"]["YEAR_PERIODS"][] = $n;
                $i++;
                if ($i >= $maxPeriodsCount) {
                    break;
                }
            }
        }

        //отрезать максимум 5 периодов
        $i = 0;
        foreach ($arResult["PLUSMINUS"]["PERIODS"] as $n => $v) {
            $i++;
            if ($i > $maxPeriodsCount) {
                unset($arResult["PLUSMINUS"]["PERIODS"][$n]);
            }
        }
    }
    $arResult["PLUSMINUS"]["PERIODS_VALUES"] = $tmp["Отчет о прибылях и убытках"];

    //Отчет о движении денежных средств
    foreach ($tmp["Cash Flow (Данные для DCF)"] as $n => $item) {
        foreach ($item["items"] as $period => $v) {
            $arResult["CASHFLOW"]["PERIODS"][$period] = true;
        }
    }
    if ($arResult["CASHFLOW"]["PERIODS"]) {
        $arResult["CASHFLOW"]["PERIODS"] = array_reverse($arResult["CASHFLOW"]["PERIODS"]);

        $i = 0;
        foreach ($arResult["CASHFLOW"]["PERIODS"] as $n => $v) {
            if (strpos($n, "4 кв") !== false) {
                $arResult["CASHFLOW"]["YEAR_PERIODS"][] = $n;
                $i++;
                if ($i >= $maxPeriodsCount) {
                    break;
                }
            }
        }

        //отрезать максимум $maxPeriodsCount периодов
        $i = 0;
        foreach ($arResult["CASHFLOW"]["PERIODS"] as $n => $v) {
            $i++;
            if ($i > $maxPeriodsCount) {
                unset($arResult["CASHFLOW"]["PERIODS"][$n]);
            }
        }
    }
    $arResult["CASHFLOW"]["PERIODS_VALUES"] = $tmp["Cash Flow (Данные для DCF)"];


    //Темп прироста основных средств - считаем для Анализ денежных потоков
    if ($arResult["BALANCE"]) {
        foreach ($arResult["BALANCE"]["PERIODS"] as $period => $v) {
            $period = explode(" кв ", $period);
            $prevYear = $period[1] - 1;

            $cur = str_replace(" ", "", $arResult["BALANCE"]["PERIODS_VALUES"]["Основные средства"]["items"][$period[0] . " кв " . $period[1]]);
            $prev = str_replace(" ", "", $arResult["BALANCE"]["PERIODS_VALUES"]["Основные средства"]["items"][$period[0] . " кв " . $prevYear]);

            if ($cur && $prev) {
                //Темп прироста основных средств = (Текущий период / аналогичный период прошлого года - 1 ) х 100
                $arResult["BALANCE"]["Темп прироста основных средств"] = ($cur / $prev - 1) * 100;
            }
            break;
        }
    }
}

//Ключевые показатели

if ($arResultCompany["ACTION"]["PERIODS"]) {
    foreach ($arResultCompany["ACTION"]["PERIODS"] as $period => $vals) {
        if ($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
            if ($vals["PERIOD_TYPE"] == "KVARTAL") {
                continue;
            }
        }
        $arResult["EXPANDED_PERIODS"]["PERIODS"][] = $period;
        if (count($arResult["EXPANDED_PERIODS"]["PERIODS"]) >= $maxPeriodsCount) {
            break;
        }
    }
    foreach ($arResult["EXPANDED_PERIODS"]["PERIODS"] as $period) {
        foreach ($arResultCompany["ACTION"]["PERIODS"][$period] as $f => $v) {
            if ($f == "PERIOD_YEAR" || $f == "PERIOD_VAL" || $f == "PERIOD_TYPE") {
                continue;
            }
            $arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"][$f] = true;
        }
    }
    $arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"]);

    $needPeriod = 4;
    if ($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
        $needPeriod = 12;
    }

    foreach ($arResultCompany["ACTION"]["PERIODS"] as $period => $vals) {
        $x[] = $vals;
        if ($vals["PERIOD_VAL"] == $needPeriod || !$vals["PERIOD_VAL"]) {

            $arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"][] = $period;

            if (count($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) >= $maxPeriodsCount) {
                break;
            }
        }
    }

    foreach ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $period) {
        foreach ($arResultCompany["ACTION"]["PERIODS"][$period] as $f => $v) {
            if ($f == "PERIOD_YEAR" || $f == "PERIOD_VAL" || $f == "PERIOD_TYPE") {
                continue;
            }
            $arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"][$f] = true;
        }
    }
    $arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"]);

    if ($arResult["ACTION"]["PROPS"]["SECID"]) {
        $graph = new SpbexGraph();
        //для графика квартала, берем последний - 1 квартал
        if ($arResult["EXPANDED_PERIODS"]["PERIODS"]) {
            $arKvartalToMonth = array(
                1 => "03-31",
                2 => "06-30",
                3 => "09-30",
                4 => "12-31",
            );

            $t = array_reverse($arResult["EXPANDED_PERIODS"]["PERIODS"]);
            $last = explode("-", $t[0]);

            if ($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
                $dt = DateTime::createFromFormat("m-y", $last[0] . "-" . $last[1]);
                $from = FormatDate("Y-m", $dt->getTimestamp()) . "-01";
            } else {
                if ($last[0] == 1) {
                    $last[0] = 4;
                    $last[1] -= 1;
                } else {
                    $last[0] -= 1;
                }
                $from = $last[1] . "-" . $arKvartalToMonth[$last[0]];
            }

            /*
            1 квартал с 1 января по 31 марта
            2 квартал с 1 апреля по 30 июня
            3 квартал с 1 июля по 30 сентября
            4 квартал с 1 октбяря по 31 декабря
            */



            //график
            $arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"] = $graph->getForActionByMonthWithFrom($arResult["ACTION"]["PROPS"]["SECID"], $from);

        }

        //для года берем начала последнего года
        if ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) {
            $t = array_reverse($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]);
            $last = explode("-", $t[0]);
            if ($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
                $dt = DateTime::createFromFormat("y", $last[1]);
                $from = FormatDate("Y", $dt->getTimestamp()) . "-01-01";
            } else {
                $from = $last[1] . "-01-01";
            }
		              //график
            $arResult["EXPANDED_PERIODS"]["YEAR_CHART"] = $graph->getForActionByMonthWithFrom($arResult["ACTION"]["PROPS"]["SECID"], $from);
				
        }

    }
}

//статьи
$obCache = new CPHPCache();

$cacheLifetime = 86400 * 365 * 10;
$cacheID = 'random_articles_for' . $arResult["ID"];
$cachePath = '/random_articles/';

if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
    $arResult["ARTICLES"] = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
    $arFilter = Array("IBLOCK_ID" => 5, "!SECTION_ID" => array(5, 17), "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array("RAND" => "asc"), $arFilter, false, array("nPageSize" => 3), $arSelect);
    while ($ob = $res->GetNext()) {
        $arResult["ARTICLES"][] = $ob;
    }

    $obCache->EndDataCache($arResult["ARTICLES"]);
}

//Ключевики
$arKeywords = array($arResult["NAME"]);

if ($arResult["PROPERTIES"]["EXPANDED"]["VALUE"]) {
    if ($arResult["EXPANDED_PERIODS"]) {
        $arKeywords[] = "Ключевые показатели";
    }
    if ($arResult["BALANCE"]) {
        $t = "Баланс";
        if ($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
            $t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
        }

        $arKeywords[] = $t;
    }
    if ($arResult["PLUSMINUS"]) {
        $t = "Отчет о прибылях и убытках";
        if ($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
            $t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
        }

        $arKeywords[] = $t;
    }
    if ($arResult["CASHFLOW"]) {
        $t = "Отчет о движении денежных средств";
        if ($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
            $t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
        }

        $arKeywords[] = $t;
    }
    if ($arResult["PROPERTIES"]["BUSANALIZ"]["~VALUE"]["TEXT"]) {
        $arKeywords[] = "Бизнес-анализ";
    }
    if ($arResult["ACTION"]) {
        $arKeywords[] = "Инвестиционный анализ";
    }
    if ($arResult["ANALIZ_MONEY"]["YEARS"]) {
        $arKeywords[] = "Анализ денежных потоков";
    }
} else {
    if ($arResult["COMPANY"]["REPORT_FIELDS"]) {
        $arKeywords[] = "Финансовые показатели компании " . $arResult["COMPANY"]["NAME"];
    }
    if ($arResult["COMPANY"]["ACTIONS"]) {
        $arKeywords[] = "Список акций " . $arResult["COMPANY"]["NAME"];
    }
    if ($arResult["COMPANY"]["OBLIGATIONS"]) {
        $arKeywords[] = "Список облигаций " . $arResult["COMPANY"]["NAME"];
    }
}

if ($arKeywords) {
    $APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}

