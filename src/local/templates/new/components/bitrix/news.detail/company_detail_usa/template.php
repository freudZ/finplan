<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner">
                <?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "extended_banner",
                            "EDIT_TEMPLATE" => "",
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_RECURSIVE" => "Y"
                        ),
                        false
                    );?>

                    <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"),Array(),Array("MODE"=>"html","NAME"=>"сайдбар"));?>
                <?endif?>

                <div class="title_container title_nav_outer">
                    <h1><?=$arResult["NAME"]?></h1>

                    <?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
                        <div class="title_nav">
                            <ul>
                                <li><a href="/lk/obligations/company_usa/all/">Все эмитенты США</a></li>
                            </ul>
                        </div>
                    <?endif?>
                </div>
                <?if(!$_GET['search_off']):?>
                    <br>
                    <form class="innerpage_search_form" method="GET">
                        <div class="form_element">
                            <input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Что вы ищете?" name="q" id="autocomplete_company_usa" />
                            <button type="submit"><span class="icon icon-search"></span></button>
                        </div>
                    </form>
                <?endif?>
                <?/*Последние 4 месяца или квартала*/?>
                <div class="article_inner">
                    <?
                    $imgStr = "";
                    if($arResult["PREVIEW_PICTURE"]["SRC"]){
                        $imgStr = '<div class="article_inner_image"> <img src="'.$arResult["PREVIEW_PICTURE"]["SRC"].'" alt="'.$arResult["NAME"].'" /></div>';
                    }
                    $arResult["PREVIEW_TEXT"] = str_replace("#IMG#", $imgStr, $arResult["PREVIEW_TEXT"]);
                    ?>
                    <p><?=$arResult["PREVIEW_TEXT"]?></p><br/>
                </div>
				<? /*$firephp = FirePHP::getInstance(true);
$firephp->fb($arResult["EXPANDED_PERIODS"],FirePHP::LOG); */?>
                <?//if($arResult["PROPERTIES"]["EXPANDED"]["VALUE"]):?>

                <div class="company_accordion_outer">
                    <div class="custom_collapse">
                        <?if($arResult["EXPANDED_PERIODS"]):?>
                            <div class="custom_collapse_elem opened">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
				   <?$heading1 = "Финансовые показатели ".$arResult["COMPANY"]["NAME"].($arResult["PROPERTIES"]["TYPE_REPORT_FIRST"]["VALUE"]?' ('.$arResult["PROPERTIES"]["TYPE_REPORT_FIRST"]["VALUE"].') ':' ');
					  if(!empty($arResult["PROPERTIES"]["SEO_SUBHEADER_1"]["VALUE"])){
						  $heading1 = $arResult["PROPERTIES"]["SEO_SUBHEADER_1"]["VALUE"];
					  }
					?>
                                            <h2 class="custom_collapse_elem_title collapse_btn"><?=$heading1?><span class="icon icon-arr_down"></span></h2>
                                        </div>
                                        <div class="custom_collapse_elem_body">
                                            <div class="custom_collapse_elem_body_inner">
                                                <?if(!$arResult["PROPERTIES"]["IS_REGION"]["VALUE"]):?>
                                                    <ul class="custom_tabs company_chart_tab company_chart_tab_01">
                                                        <li class="active">
                                                            <a href="#company_usa_custom_tabs_01_quarter" data-toggle="tab">
                                                                <?if($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"]=="банковские"):?>
                                                                    по месяцам
                                                                <?else:?>
                                                                    по кварталам
                                                                <?endif?>
                                                            </a>
                                                        </li>
                                                        <li><a href="#company_usa_custom_tabs_01_year" data-toggle="tab">по годам</a></li>
                                                        <?if(isset($arResult["ACTION"]["PROPS"]["SECID"]) && !empty($arResult["ACTION"]["PROPS"]["SECID"])):?>
                                                            <li>
                                                                <a href="/tehanaliz/?actionList=<?=$arResult["ACTION"]["PROPS"]["SECID"]."_USA"?>&templateId=<?=$APPLICATION->TA_template_GS_for_action_pages?>" class="dashed company-page-ta-link" target="_blank">Перейти в теханализ</a>
                                                            </li>
                                                        <?endif;?>
                                                    </ul>
                                                <?endif?>

                                                <div class="tab-content">
                                                    <div class="tab-pane active" data-currency-sign="<?= getCurrencySign($arResult["ACTION"]["PROPS"]["CURRENCY"]) ?>" id="company_usa_custom_tabs_01_quarter">
                                                        <?if($arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"]):?>
                                                            <div class="company_chart_outer">
                                                                <canvas class="company_chart"  id="key_indicators_quarters_chart" data-value='<?=json_encode($arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"])?>'></canvas>
                                                            </div>
                                                        <?endif?>
        <span class="overtable-span">
         <?if(!$arResult["HAVE_ACCESS"]):?>
           <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["аналитика - акции"]?>">
         <?endif?>
         
			<?//if($USER->IsAdmin() ||  $_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
      
			   <?if(!$arResult["HIDDEN_INDUSTRY_DATA"]):?>
                   <label for="midval">Сравнить показатели:&nbsp;</label>
               <select id="midval_1" name="midval" data-country="USA" class="midval1 midval inline_select" >
                   <option value="" selected>Не выбрано</option>
                   <option value="sector" >По отрасли</option>
                    <?=$arResult["EMITENT_LIST_HTML"]?>
               </select>

                       <span class="checkbox custom compareTypeMoney">
                            <input type="checkbox" id="compareTypeMoney_0" class="" name="compareTypeMoney" value="Y">
                            <label for="compareTypeMoney_0">В абсолютном выражении</label>
                         </span>
			   <?endif;?>
                         <span class="checkbox custom hideCurrentPeriodFinpoks">
                            <input type="checkbox" id="hideCurrentPeriodFinpoks" class="" name="hideCurrentPeriodFinpoks" value="Y">
                            <label for="hideCurrentPeriodFinpoks">Скрывать текущий период</label>
                         </span>
            <?//endif;//if not XMLHttpRequest?>
			   <?if(!$arResult["HAVE_ACCESS"]):?>
				</div>
			   <?endif?>
									<?if(isset($arResult["ACTION"]["COMPANY"]["NEXT_REPORT_DATE"]) && !empty($arResult["ACTION"]["COMPANY"]["NEXT_REPORT_DATE"])):?>
									<div>
									<p class="font_13 text-left strong green">Дата следующей отчтености <?= $arResult["ACTION"]["COMPANY"]["NEXT_REPORT_DATE"] ?></p>
									</div>
									<?endif;?>
       </span>
                                                        <?if($arResult["PROPERTIES"]["CURRENCY"]["VALUE"]):?>
                                                            <span class="overtable-span" ><p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["CURRENCY"]["VALUE"]?></p></span>
                                                        <?endif?>
                                                        <?if($arResult["PROPERTIES"]["REPORT_DATE"]["VALUE"]):?>
                                                            <p class="font_13 text-right strong green">дата начала финансового года -  <?=$arResult["PROPERTIES"]["REPORT_DATE"]["VALUE"]?></p>
                                                        <?endif?>
                                                        <?if(!$arResult["HAVE_ACCESS"]):?>
                                                        <div style="position:relative">
                                                            <?endif?>
                                                            <table class="key_indicators_table transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <?$i=0;foreach($arResult["EXPANDED_PERIODS"]["PERIODS"] as $item):
                                                                        $itemMarker = $item;
                                                                        if($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"]=="банковские"){
                                                                            $item = explode("-", $item);
                                                                            $dt = DateTime::createFromFormat("m-y", $item[0]."-".$item[1]);
                                                                            $item = FormatDate("M. Y", $dt->getTimestamp());
                                                                        } else {
                                                                            $item = explode("-", $item);

                                                                            if ($item[0]) {
                                                                                $item = $item[0]." кв. ".$item[1];
                                                                            } else {
                                                                                $item = $item[1] . " год";
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <?if(isset($arResult["ACTION"]["PERIODS"][$itemMarker]["CURRENT_PERIOD"]) && $arResult["ACTION"]["PERIODS"][$itemMarker]["CURRENT_PERIOD"]==1):?>
                                                                        <th class="tableCurrentPeriodCell" data-breakpoints="<?=$arResult["POINTS"][$i]?>">Текущий</th>
                                                                    <?else:?>
																						  	 <? if($i>=5){
																										 $currentPeriodClass = 'tableReservedPeriodCell hidden';
																							 } ?>
                                                                        <th class="<?= $currentPeriodClass ?>" data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=$item?></th>
                                                                    <?endif;?>
                                                                        <?$i++;endforeach?>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?$i=0;foreach($arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"] as $field=>$v):
                                                                    if($field=="CURRENT_PERIOD") { continue; }
                                                                    if ($field == "Темп прироста капитализации с прошлого квартала") {  continue; }
                                                                    if ($field == "Темп прироста капитализации с начала года") {  continue; }
                                                                    ?>
                                                                    <?if($field != 'AVEREGE_PROFIT'):?>
                                                                    <tr<?if(!$i):?> class="active"<?endif?> data-postfix="" data-number="<?=$i?>">
                                                                        <td data-propkey="<?=$field?>"><span class="name"><?=$v?></span>
																			<?if(array_key_exists($field, reset($arResult["INDUSTRY_MID_VAL"])) && $arResult["HAVE_ACCESS"]):?>
                                                                                <div class="mid_vals_table sector mid_vals_table_label <?=($showIndustryMidVals?'':'hidden')?>"
                                                                                     data-summ=" в абс." data-prc=" в %" data-noTogglePrc="<?= (in_array($v,$APPLICATION->sectorsPercentNames) ? 'N' : 'Y') ?>">
                                                                                    <i>По отрасли<span><?=(in_array($v,$APPLICATION->sectorsPercentNames)?' в %':'')?></span></i></div>
                                                                                <div class="emitent_vals hidden title"></div>
                                                                            <?endif;?>
                                                                        </td>
                                                                        <?$l=1;foreach($arResult["EXPANDED_PERIODS"]["PERIODS"] as $item):
                                                                            $t = explode("-", $item);
                                                                            if($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"]=="банковские"){
                                                                                $dt = DateTime::createFromFormat("m-y", $t[0]."-".$t[1]);
                                                                                $date = FormatDate("M. Y", $dt->getTimestamp());
                                                                            } else {
                                                                                $date = $arResult["KVARTAL_MONTH"][$t[0]]." ".$t[1];
                                                                            }
                                                                            ?>
																									 <?
																										if(isset($arResult["ACTION"]["PERIODS"][$item]["CURRENT_PERIOD"]) && $arResult["ACTION"]["PERIODS"][$item]["CURRENT_PERIOD"] == 1){
																										 $currentPeriodClass = 'tableCurrentPeriodCell';
																										} else {
																										 $currentPeriodClass = '';
																										}

																										if(end($arResult["EXPANDED_PERIODS"]["PERIODS"])==$item){
																										 $currentPeriodClass = 'tableReservedPeriodCell hidden';
																										}
																									 ?>
                                                                            <td class="<?=$currentPeriodClass?>" data-date="<?=$date?>" data-val="<?=$arResult["ACTION"]["PERIODS"][$item][$field]?>">
                                                                                <?if((!$arResult["HAVE_ACCESS"] ) && $l<=3):?>

                                                                                <?else:?>
                                                                                    <?$fieldVal = floatval($arResult["ACTION"]["PERIODS"][$item][$field]);?>
                                                                                    <?=formatNumber($fieldVal)?>
                                                                                    <?$midFieldVal = $arResult["INDUSTRY_MID_VAL"][$item][$field];
                                                                                      if(in_array($v,$APPLICATION->sectorsPercentNames)){
                                                                                          $midFieldVal = abs(($fieldVal/$midFieldVal)*100);
                                                                                      }

                                                                                    ?>
																					<?if(array_key_exists($field, $arResult["INDUSTRY_MID_VAL"][$item]) && $arResult["HAVE_ACCESS"]):?>
                                                                                        <div class="mid_vals_table sector <?=($showIndustryMidVals?'':'hidden')?>" data-dig-value="<?=$arResult["INDUSTRY_MID_VAL"][$item][$field]?>"
                                                                                             data-v="<?= $field ?>" data-summ="<?=formatNumber($arResult["INDUSTRY_MID_VAL"][$item][$field])?>"
                                                                                             data-prc="<?=formatNumber($midFieldVal)?>"><i><?= formatNumber($midFieldVal)?></i></div>
                                                                                        <div class="emitent_vals hidden per-<?= $item ?>" data-origVal="<?=$fieldVal?>" data-summ="" data-prc="" data-period="<?= $item ?>"><i></i></div>
                                                                                    <?endif;?>
                                                                                <?endif?>
                                                                            </td>
                                                                            <?$l++;endforeach?>
                                                                    </tr>
                                                                <?endif?>
                                                                    <?$i++;endforeach?>
                                                                </tbody>
                                                            </table>
                                                            <?if(!$arResult["HAVE_ACCESS"]):?>
                                                            <?if ($USER->IsAuthorized()):?>
                                                                <span class="buy_subscribe_popup_btn button" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть ключевые показатели по компании</span>
                                                            <?else:?>
																				    <a href="#" class="buy_subscribe_popup_btn button" data-toggle="modal" data-target="#popup_simple_auth">Получить демо-доступ к Fin-plan Radar</a>
                                                            <?endif?>
                                                        </div>
                                                    <?endif?>

                                                    </div>

                                                    <div class="tab-pane" data-currency-sign="<?= getCurrencySign($arResult["ACTION"]["PROPS"]["CURRENCY"]) ?>" id="company_usa_custom_tabs_01_year">
                                                        <?if($arResult["EXPANDED_PERIODS"]["YEAR_CHART"]):?>
                                                            <div class="company_chart_outer">
                                                                <canvas class="company_chart" id="key_indicators_years_chart"   data-value='<?=json_encode($arResult["EXPANDED_PERIODS"]["YEAR_CHART"])?>'></canvas>
                                                            </div>
                                                        <?endif?>
       <span class="overtable-span">
         <?if(!$arResult["HAVE_ACCESS"]):?>
           <div class="dib relative tooltip_custom_outer"  data-content="<?=$APPLICATION->lkRadarPopups["аналитика - акции"]?>">
         <?endif?>
			   <?//if($USER->IsAdmin() || $_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
		
				   <?if(!$arResult["HIDDEN_INDUSTRY_DATA"]):?>
                       <label for="midval">Сравнить показатели:&nbsp;</label>
                       <select id="midval_2" name="midval" data-country="USA" class="midval2 midval inline_select" >
                   <option value="" selected>Не выбрано</option>
                   <option value="sector" >По отрасли</option>
                    <?=$arResult["EMITENT_LIST_HTML"]?>
               </select>

                           <span class="checkbox custom compareTypeMoney">
                            <input type="checkbox" id="compareTypeMoney_1" class="" name="compareTypeMoney" value="Y">
                            <label for="compareTypeMoney_1">В абсолютном выражении</label>
                         </span>

				   <?endif;?>
	
			   <?//endif;//if not XMLHttpRequest?>
               <?if(!$arResult["HAVE_ACCESS"]):?>
		    </div>
            <?endif?>
       </span>

                                                        <?if(!$arResult["HAVE_ACCESS"]):?>
                                                        <div style="position:relative">
                                                            <?endif?>
                                                            <?if($arResult["PROPERTIES"]["CURRENCY"]["VALUE"]):?>
                                                                <span class="overtable-span"><p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["CURRENCY"]["VALUE"]?></p></span>
                                                            <?endif?>
                                                            <table class="key_indicators_years_table transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <?$i=0;foreach($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $item):
                                                                        $item = explode("-", $item);
                                                                        if($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"]=="банковские"){
                                                                            $dt = DateTime::createFromFormat("y", $item[1]);
                                                                            $item = FormatDate("Y", $dt->getTimestamp())." год";
                                                                        } else {
                                                                            $item = $item[1]." год";
                                                                        }?>
                                                                        <th data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=$item?></th>
                                                                        <?$i++;endforeach?>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <?$i=0;foreach($arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"] as $field=>$v):?>
																					 <?if ($field == "CURRENT_PERIOD") { continue; }
                                                                  if ($field == "Темп прироста капитализации с прошлого квартала") {  continue; }
                                                                  if ($field == "Темп прироста капитализации с начала года") {  continue; }
																					 ?>
                                                                    <?if($field != 'AVEREGE_PROFIT'):?>
                                                                        <tr<?if(!$i):?> class="active"<?endif?> data-postfix="" data-number="<?=$i?>">
                                                                            <td data-propkey="<?=$field?>"><span class="name"><?=$v?></span>
																				<?if(array_key_exists($field, reset($arResult["INDUSTRY_MID_VAL"])) && $arResult["HAVE_ACCESS"]):?>
                                                                                    <div class="mid_vals_table sector mid_vals_table_label <?=($showIndustryMidVals?'':'hidden')?>"
                                                                                         data-summ=" в абс." data-prc=" в %" data-noTogglePrc="<?= (in_array($v,$APPLICATION->sectorsPercentNames) ? 'N' : 'Y') ?>">
                                                                                        <i>По отрасли<span><?=(in_array($v,$APPLICATION->sectorsPercentNames)?' в %':'')?></span></i></div>
                                                                                    <div class="emitent_vals hidden title"><i></i></div>
                                                                                <?endif;?>
                                                                            </td>
                                                                            <?$l=1;foreach($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $item):
                                                                                $t = explode("-", $item);
                                                                                if($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"]=="банковские"){
                                                                                    $dt = DateTime::createFromFormat("m-y", $t[0]."-".$t[1]);
                                                                                    $date = FormatDate("M. Y", $dt->getTimestamp());
                                                                                } else {
                                                                                    $date = $arResult["KVARTAL_MONTH"][$t[0]]." ".$t[1];
                                                                                }
                                                                                ?>
                                                                                <td data-date="<?=$date?>" data-val="<?=$arResult["ACTION"]["PERIODS"][$item][$field]?>">
                                                                                    <?if(!$arResult["HAVE_ACCESS"] && $l<=2):?>

                                                                                    <?else:?>
																						<?$fieldVal = floatval($arResult["ACTION"]["PERIODS"][$item][$field]);?>
																						<?=formatNumber($fieldVal)?>
																						<?$midFieldVal = $arResult["INDUSTRY_MID_VAL"][$item][$field];
																						if(in_array($v,$APPLICATION->sectorsPercentNames)){
																							$midFieldVal = abs(($fieldVal/$midFieldVal)*100);
																						}

																						?>
																						<?if(array_key_exists($field, $arResult["INDUSTRY_MID_VAL"][$item]) && $arResult["HAVE_ACCESS"]):?>
                                                                                            <div class="mid_vals_table sector <?=($showIndustryMidVals?'':'hidden')?>"
                                                                                                 data-v="<?= $field ?>" data-summ="<?=formatNumber($arResult["INDUSTRY_MID_VAL"][$item][$field])?>"
                                                                                                 data-prc="<?=formatNumber($midFieldVal)?>"><i><?=formatNumber($midFieldVal)?></i></div>
                                                                                            <div class="emitent_vals hidden per-<?= $item ?>" data-origVal="<?=$fieldVal?>" data-summ="" data-prc="" data-period="<?= $item ?>"><i></i></div>
                                                                                        <?endif;?>

                                                                                    <?endif?>
                                                                                </td>
                                                                                <?$l++;endforeach?>
                                                                        </tr>
                                                                    <?endif;?>
                                                                    <?$i++;endforeach?>
                                                                </tbody>
                                                            </table>
                                                            <?if(!$arResult["HAVE_ACCESS"]):?>
                                                            <?if ($USER->IsAuthorized()):?>
                                                                <span class="buy_subscribe_popup_btn button" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть ключевые показатели по компании</span>
                                                            <?else:?>
                                                                <!--<a href="/lk/radar_in/" class="buy_subscribe_popup_btn button">Получить демо-доступ к Fin-plan Radar</a>   -->
																					 <a class="buy_subscribe_popup_btn button" href="#" data-toggle="modal" data-target="#popup_simple_auth">Получить демо-доступ к Fin-plan Radar</a>
                                                            <?endif?>
                                                        </div>
                                                    <?endif?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?endif?>

                        <?if($arResult["BALANCE"] && $arResult["PROPERTIES"]["EXPANDED"]["VALUE"]):?>
                            <div class="custom_collapse_elem">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title<?if($arResult["HAVE_ACCESS"]):?> collapse_btn<?endif?>"<?if(!$arResult["HAVE_ACCESS"] && $USER->IsAuthorized()):?> data-toggle="modal" data-target="#buy_subscribe_popup"<?endif?>>
                                                <?if(!$USER->IsAuthorized()):?>
																<a href="#" data-toggle="modal" data-target="#popup_simple_auth">
                                                <!--<a href="/lk/radar_in/">-->
                                                    <?endif?>
                                                    Баланс<?if($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]):?> (<?=$arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]?>)<?endif?> <span class="icon icon-arr_down"></span>
                                                    <?if(!$USER->IsAuthorized()):?>
                                                </a>
                                            <?endif?>
                                            </h2>
                                        </div>
                                        <?if($arResult["HAVE_ACCESS"]):?>
                                            <div class="custom_collapse_elem_body">
                                                <div class="custom_collapse_elem_body_inner">
                                                    <ul class="custom_tabs">
                                                        <li class="active"><a href="#custom_tabs_02_quarter" data-toggle="tab">по кварталам</a></li>
                                                        <li><a href="#custom_tabs_02_year" data-toggle="tab">по годам</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="custom_tabs_02_quarter">
                                                            <?if($arResult["PROPERTIES"]["VAL_ED"]["VALUE"]):?>
                                                                <p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["VAL_ED"]["VALUE"]?></p>
                                                            <?endif?>
                                                            <table class="transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <?$i=0;foreach($arResult["BALANCE"]["PERIODS"] as $item=>$v):?>
                                                                        <th data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=$item?></th>
                                                                        <?$i++;endforeach?>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <?foreach($arResult["BALANCE"]["PERIODS_VALUES"] as $item):?>
                                                                    <tr>
                                                                        <td>
                                                                            <?if($item["bold"]):?>
                                                                            <b>
                                                                                <?endif?>
                                                                                <?=$item["name"]?>
                                                                                <?if($item["bold"]):?>
                                                                            </b>
                                                                        <?endif?>
                                                                        </td>
                                                                        <?foreach($arResult["BALANCE"]["PERIODS"] as $period=>$v):?>
                                                                            <td><?=formatNumber($item["items"][$period])?></td>
                                                                        <?endforeach?>
                                                                    </tr>
                                                                <?endforeach?>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="tab-pane" id="custom_tabs_02_year">
                                                            <?if($arResult["PROPERTIES"]["VAL_ED"]["VALUE"]):?>
                                                                <p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["VAL_ED"]["VALUE"]?></p>
                                                            <?endif?>
                                                            <table class="transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <?$i=0;foreach($arResult["BALANCE"]["YEAR_PERIODS"] as $item):?>
                                                                        <th data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=str_replace("4 кв ", "" , $item)?></th>
                                                                        <?$i++;endforeach?>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <?foreach($arResult["BALANCE"]["PERIODS_VALUES"] as $item):?>
                                                                    <tr>
                                                                        <td>
                                                                            <?if($item["bold"]):?>
                                                                            <b>
                                                                                <?endif?>
                                                                                <?=$item["name"]?>
                                                                                <?if($item["bold"]):?>
                                                                            </b>
                                                                        <?endif?>
                                                                        </td>
                                                                        <?foreach($arResult["BALANCE"]["YEAR_PERIODS"] as $period):?>
                                                                            <td><?=formatNumber($item["items"][$period])?></td>
                                                                        <?endforeach?>
                                                                    </tr>
                                                                <?endforeach?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?endif?>
                                    </div>
                                </div>
                            </div>
                        <?endif?>

                        <?if($arResult["PLUSMINUS"] && $arResult["PROPERTIES"]["EXPANDED"]["VALUE"]):?>
                            <div class="custom_collapse_elem">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title<?if($arResult["HAVE_ACCESS"]):?> collapse_btn<?endif?>"<?if(!$arResult["HAVE_ACCESS"] && $USER->IsAuthorized()):?> data-toggle="modal" data-target="#buy_subscribe_popup"<?endif?>>
                                                <?if(!$USER->IsAuthorized()):?>
																<a href="#" data-toggle="modal" data-target="#popup_simple_auth">
                                                <!--<a href="/lk/radar_in/"> -->
                                                    <?endif?>
                                                    Отчет о прибылях и убытках<?if($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]):?> (<?=$arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]?>)<?endif?> <span class="icon icon-arr_down"></span>
                                                    <?if(!$USER->IsAuthorized()):?>
                                                </a>
                                            <?endif?>
                                            </h2>
                                        </div>
                                        <?if($arResult["HAVE_ACCESS"]):?>
                                            <div class="custom_collapse_elem_body">
                                                <div class="custom_collapse_elem_body_inner">
                                                    <ul class="custom_tabs">
                                                        <li class="active"><a href="#custom_tabs_03_quarter" data-toggle="tab">по кварталам</a></li>
                                                        <li><a href="#custom_tabs_03_year" data-toggle="tab">по годам</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="custom_tabs_03_quarter">
                                                            <?if($arResult["PROPERTIES"]["VAL_ED"]["VALUE"]):?>
                                                                <p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["VAL_ED"]["VALUE"]?></p>
                                                            <?endif?>
                                                            <table class="transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <?$i=0;foreach($arResult["PLUSMINUS"]["PERIODS"] as $item=>$v):?>
                                                                        <th data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=$item?></th>
                                                                        <?$i++;endforeach?>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <?foreach($arResult["PLUSMINUS"]["PERIODS_VALUES"] as $item):?>
                                                                    <tr>
                                                                        <td>
                                                                            <?if($item["bold"]):?>
                                                                            <b>
                                                                                <?endif?>
                                                                                <?=$item["name"]?>
                                                                                <?if($item["bold"]):?>
                                                                            </b>
                                                                        <?endif?>
                                                                        </td>
                                                                        <?foreach($arResult["PLUSMINUS"]["PERIODS"] as $period=>$v):?>
                                                                            <td><?=formatNumber($item["items"][$period])?></td>
                                                                        <?endforeach?>
                                                                    </tr>
                                                                <?endforeach?>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="tab-pane" id="custom_tabs_03_year">
                                                            <?if($arResult["PROPERTIES"]["VAL_ED"]["VALUE"]):?>
                                                                <p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["VAL_ED"]["VALUE"]?></p>
                                                            <?endif?>
                                                            <table class="transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <?$i=0;foreach($arResult["PLUSMINUS"]["YEAR_PERIODS"] as $item):?>
                                                                        <th data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=str_replace("4 кв ", "" , $item)?></th>
                                                                        <?$i++;endforeach?>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <?foreach($arResult["PLUSMINUS"]["PERIODS_VALUES"] as $item):?>
                                                                    <tr>
                                                                        <td>
                                                                            <?if($item["bold"]):?>
                                                                            <b>
                                                                                <?endif?>
                                                                                <?=$item["name"]?>
                                                                                <?if($item["bold"]):?>
                                                                            </b>
                                                                        <?endif?>
                                                                        </td>
                                                                        <?foreach($arResult["PLUSMINUS"]["YEAR_PERIODS"] as $period):?>
                                                                            <td><?=formatNumber($item["items"][$period])?></td>
                                                                        <?endforeach?>
                                                                    </tr>
                                                                <?endforeach?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?endif?>
                                    </div>
                                </div>
                            </div>
                        <?endif?>

                        <?if($arResult["CASHFLOW"] && $arResult["PROPERTIES"]["EXPANDED"]["VALUE"]):?>
                            <div class="custom_collapse_elem">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title<?if($arResult["HAVE_ACCESS"]):?> collapse_btn<?endif?>"<?if(!$arResult["HAVE_ACCESS"] && $USER->IsAuthorized()):?> data-toggle="modal" data-target="#buy_subscribe_popup"<?endif?>>
                                                <?if(!$USER->IsAuthorized()):?>
																<a href="#" data-toggle="modal" data-target="#popup_simple_auth">
                                                <!--<a href="/lk/radar_in/"> -->
                                                    <?endif?>
                                                    Отчет о движении денежных средств<?if($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]):?> (<?=$arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]?>)<?endif?> <span class="icon icon-arr_down"></span>
                                                    <?if(!$USER->IsAuthorized()):?>
                                                </a>
                                            <?endif?>
                                            </h2>
                                        </div>
                                        <?if($arResult["HAVE_ACCESS"]):?>
                                            <div class="custom_collapse_elem_body">
                                                <div class="custom_collapse_elem_body_inner">
                                                    <ul class="custom_tabs">
                                                        <li class="active"><a href="#custom_tabs_04_quarter" data-toggle="tab">по кварталам</a></li>
                                                        <li><a href="#custom_tabs_04_year" data-toggle="tab">по годам</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="custom_tabs_04_quarter">
                                                            <?if($arResult["PROPERTIES"]["VAL_ED"]["VALUE"]):?>
                                                                <p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["VAL_ED"]["VALUE"]?></p>
                                                            <?endif?>
                                                            <table class="transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <?$i=0;foreach($arResult["CASHFLOW"]["PERIODS"] as $item=>$v):?>
                                                                        <th data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=$item?></th>
                                                                        <?$i++;endforeach?>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <?foreach($arResult["CASHFLOW"]["PERIODS_VALUES"] as $item):?>
                                                                    <tr>
                                                                        <td>
                                                                            <?if($item["bold"]):?>
                                                                            <b>
                                                                                <?endif?>
                                                                                <?=$item["name"]?>
                                                                                <?if($item["bold"]):?>
                                                                            </b>
                                                                        <?endif?>
                                                                        </td>
                                                                        <?foreach($arResult["CASHFLOW"]["PERIODS"] as $period=>$v):?>
                                                                            <td><?=formatNumber($item["items"][$period])?></td>
                                                                        <?endforeach?>
                                                                    </tr>
                                                                <?endforeach?>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="tab-pane" id="custom_tabs_04_year">
                                                            <?if($arResult["PROPERTIES"]["VAL_ED"]["VALUE"]):?>
                                                                <p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["VAL_ED"]["VALUE"]?></p>
                                                            <?endif?>
                                                            <table class="transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <?$i=0;foreach($arResult["CASHFLOW"]["YEAR_PERIODS"] as $item):?>
                                                                        <th data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=str_replace("4 кв ", "" , $item)?></th>
                                                                        <?$i++;endforeach?>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <?foreach($arResult["CASHFLOW"]["PERIODS_VALUES"] as $item):?>
                                                                    <tr>
                                                                        <td>
                                                                            <?if($item["bold"]):?>
                                                                            <b>
                                                                                <?endif?>
                                                                                <?=$item["name"]?>
                                                                                <?if($item["bold"]):?>
                                                                            </b>
                                                                        <?endif?>
                                                                        </td>
                                                                        <?foreach($arResult["CASHFLOW"]["YEAR_PERIODS"] as $period):?>
                                                                            <td><?=formatNumber($item["items"][$period])?></td>
                                                                        <?endforeach?>
                                                                    </tr>
                                                                <?endforeach?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?endif?>
                                    </div>
                                </div>
                            </div>
                        <?endif?>

                        <?if($arResult["PROPERTIES"]["BUSANALIZ"]["~VALUE"]["TEXT"] && $arResult["PROPERTIES"]["EXPANDED"]["VALUE"]):?>
                            <div class="custom_collapse_elem">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title<?if($arResult["HAVE_ACCESS"]):?> collapse_btn<?endif?>"<?if(!$arResult["HAVE_ACCESS"] && $USER->IsAuthorized()):?> data-toggle="modal" data-target="#buy_subscribe_popup"<?endif?>>
                                                <?if(!$USER->IsAuthorized()):?>
																<a href="#" data-toggle="modal" data-target="#popup_simple_auth">
                                                <!--<a href="/lk/radar_in/">-->
                                                    <?endif?>
                                                    Бизнес-анализ <span class="icon icon-arr_down"></span>
                                                    <?if(!$USER->IsAuthorized()):?>
                                                </a>
                                            <?endif?>
                                            </h2>
                                        </div>
                                        <?if($arResult["HAVE_ACCESS"]):?>
                                            <div class="custom_collapse_elem_body">
                                                <div class="custom_collapse_elem_body_inner">
                                                    <p><?=$arResult["PROPERTIES"]["BUSANALIZ"]["~VALUE"]["TEXT"]?></p>
                                                </div>
                                            </div>
                                        <?endif?>
                                    </div>
                                </div>
                            </div>
                        <?endif?>

                        <?if($arResult["ACTION"] && $arResult["PROPERTIES"]["EXPANDED"]["VALUE"]):?>
                            <div class="custom_collapse_elem">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title<?if($arResult["HAVE_ACCESS"]):?> collapse_btn<?endif?>"<?if(!$arResult["HAVE_ACCESS"] && $USER->IsAuthorized()):?> data-toggle="modal" data-target="#buy_subscribe_popup"<?endif?>>
                                                <?if(!$USER->IsAuthorized()):?>
																<a href="#" data-toggle="modal" data-target="#popup_simple_auth">
                                                <!--<a href="/lk/radar_in/">-->
                                                    <?endif?>
                                                    Инвестиционный анализ <span class="icon icon-arr_down"></span>
                                                    <?if(!$USER->IsAuthorized()):?>
                                                </a>
                                            <?endif?>
                                            </h2>
                                        </div>
                                        <?if($arResult["HAVE_ACCESS"]):?>
                                            <div class="custom_collapse_elem_body">
                                                <div class="custom_collapse_elem_body_inner">
                                                    <p class="strong">Вы можете ввести в таблицу свои данные для возможного анализа</p>

                                                    <div class="company_tables_outer">
                                                        <table class="company_two_cols_table">
                                                            <tbody>
                                                            <tr>
                                                                <td>Текущий Р/Е</td>
                                                                <td><input id="ia_current_re" name="ia_current_re" type="text" value="<?=$arResult["ACTION"]["DYNAM"]["PE"]?$arResult["ACTION"]["DYNAM"]["PE"]:0?>" class="ia_re" /> <span class="tooltip_btn" title="Мультипликатор P/E, рассчитанный исходя из текущей капитализации компании и скользящего расчета прибыли ">i</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Справедливый Р/Е</td>
                                                                <td><input id="ia_equitable_re" name="ia_equitable_re" type="text" value="<?=$APPLICATION->companyPage["Инвестиционный анализ"]["Справедливый Р/Е"]?>" class="ia_re" /> <span class="tooltip_btn" title="В качестве справедливого P/E для компании берется расчетный справедливый п/е, либо средний п/е по рынку">i</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Прирост за счет недооценки</td>
                                                                <td><span class="ia_underestimation_increase"></span> <span class="tooltip_btn" title="Прирост за счет= (Справедливый P/E / Текущий P/E -1) х 100%">i</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Темп роста компании</td>
                                                                <td><input id="ia_company_growth_rate" name="ia_company_growth_rate" type="text" value="<?=$arResult["ACTION"]["Темп роста компании"]?$arResult["ACTION"]["Темп роста компании"]:0?>%" /> <span class="tooltip_btn" title="Темп роста компании = Средний темп роста выручки">i</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Прогнозные дивиденды</td>
                                                                <td><input id="ia_projected_dividends" name="ia_projected_dividends" type="text" value="<?=$arResult["ACTION"]["DYNAM"]["Дивиденды %"]?$arResult["ACTION"]["DYNAM"]["Дивиденды %"]:0?>" /> <span class="tooltip_btn" title="Прогнозные дивиденды = ожидаемые дивиденды по компании за год">i</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Консенсус-прогноз</td>
                                                                <td><span class="ia_consensus_forecast"></span> <span class="tooltip_btn" title="Консенсус прогноз = Прирост за счет движения к справедливой цене + Темп роста компании + Прогнозные дивиденды">i</span></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        <?endif?>
                                    </div>
                                </div>
                            </div>
                        <?endif?>

                        <?if($arResult["ANALIZ_MONEY"]["YEARS"] && $arResult["PROPERTIES"]["EXPANDED"]["VALUE"]):?>
                            <div class="custom_collapse_elem">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title<?if($arResult["HAVE_ACCESS"]):?> collapse_btn<?endif?>"<?if(!$arResult["HAVE_ACCESS"] && $USER->IsAuthorized()):?> data-toggle="modal" data-target="#buy_subscribe_popup"<?endif?>>
                                                <?if(!$USER->IsAuthorized()):?>
																<a href="#" data-toggle="modal" data-target="#popup_simple_auth">
                                                <!--<a href="/lk/radar_in/">-->
                                                    <?endif?>
                                                    Анализ денежных потоков <span class="icon icon-arr_down"></span>
                                                    <?if(!$USER->IsAuthorized()):?>
                                                </a>
                                            <?endif?>
                                            </h2>
                                        </div>
                                        <?if($arResult["HAVE_ACCESS"]):?>
                                            <div class="custom_collapse_elem_body">
                                                <div class="custom_collapse_elem_body_inner">
                                                    <p class="strong">Вы можете ввести в таблицу свои данные для возможного анализа</p>

                                                    <div class="company_tables_outer">
                                                        <table class="company_two_cols_table mb50">
                                                            <tbody>
                                                            <tr>
                                                                <td>Ставка дисконтирования</td>
                                                                <td><input id="tc_discount_rate_1" name="tc_discount_rate_1" type="text" value="<?=$APPLICATION->companyPage["Анализ денежных потоков"]["Ставка дисконтирования"]?>" /> <span class="tooltip_btn" title="Ставка дисконтирования – определяется экспертами компании Fin-plan.">i</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Темп прироста основных средств</td>
                                                                <td><input id="tc_fixed_assets_rate_1" name="tc_fixed_assets_rate_1" type="text" value="<?=$arResult["BALANCE"]["Темп прироста основных средств"]?round($arResult["BALANCE"]["Темп прироста основных средств"], 2):0?>%" /> <span class="tooltip_btn" title="Темп прироста основных средств - средний темп роста основных средств. ">i</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Темп прироста чистого денежного потока через 5 лет</td>
                                                                <td><input id="tc_net_cash_rate_1" name="tc_net_cash_rate_1" type="text" value="<?=$APPLICATION->companyPage["Анализ денежных потоков"]["Темп прироста чистого денежного потока через 5 лет"]?>" /> <span class="tooltip_btn" title="Темп прироста ЧДП - определяется экспертами компании Fin-plan. ">i</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Темп прироста чистой прибыли</td>
                                                                <td><input id="tc_net_profit_rate_1" name="tc_net_profit_rate_1" type="text" value="<?=$arResult["ACTION"]["Темп роста прибыли"]?$arResult["ACTION"]["Темп роста прибыли"]:0?>%" /> <span class="tooltip_btn" title="Темп прироста чистой прибыли – средний темп роста прибыли компании.">i</span></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <?if($arResult["PROPERTIES"]["VAL_ED"]["VALUE"]):?>
                                                            <p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["VAL_ED"]["VALUE"]?></p>
                                                        <?endif?>
                                                        <table class="tc_cash_flow_analisys_table transform_table mb50">
                                                            <thead>
                                                            <tr>
                                                                <th>Показатель</th>
                                                                <?$i=0;foreach($arResult["ANALIZ_MONEY"]["YEARS"] as $item):
                                                                    $item = $item." год";?>
                                                                    <th data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=$item?></th>
                                                                    <?$i++;endforeach?>
                                                            </tr>
                                                            </thead>

                                                            <tbody>
                                                            <tr class="tc_clear_profit">
                                                                <td>Чистая прибыль</td>
                                                                <?$i=0;foreach($arResult["ANALIZ_MONEY"]["YEARS"] as $item):?>
                                                                    <?if(!$i):
                                                                        $v = $arResult["PLUSMINUS"]["PERIODS_VALUES"]["Прибыль/убыток за отчетный период"]["items"]["4 кв ".$item];
                                                                        $v = explode(",", $v);
                                                                        $v = preg_replace("/[^x\d|*\.]/", "", $v[0]);
                                                                        ?>
                                                                        <td data-value="<?=$v?$v:0?>"> </td>
                                                                    <?else:?>
                                                                        <td> </td>
                                                                    <?endif?>
                                                                    <?$i++;endforeach?>
                                                            </tr>

                                                            <tr class="tc_depreciation">
                                                                <td>+ Амортизация</td>
                                                                <?$i=0;foreach($arResult["ANALIZ_MONEY"]["YEARS"] as $item):?>
                                                                    <?if(!$i):
                                                                        $v = $arResult["CASHFLOW"]["PERIODS_VALUES"]["Амортизация"]["items"]["4 кв ".$item];
                                                                        $v = explode(",", $v);
                                                                        $v = preg_replace("/[^x\d|*\.]/", "", $v[0]);?>
                                                                        <td data-value="<?=$v?$v:0?>"> </td>
                                                                    <?else:?>
                                                                        <td> </td>
                                                                    <?endif?>
                                                                    <?$i++;endforeach?>
                                                            </tr>

                                                            <tr class="tc_investments">
                                                                <td>- Инвестиции</td>
                                                                <?$i=0;foreach($arResult["ANALIZ_MONEY"]["YEARS"] as $item):?>
                                                                    <?if(!$i):
                                                                        $v = $arResult["CASHFLOW"]["PERIODS_VALUES"]["Инвестиции"]["items"]["4 кв ".$item];
                                                                        $v = explode(",", $v);
                                                                        $v = preg_replace("/[^x\d|*\.]/", "", $v[0]);?>
                                                                        <td data-value="<?=$v?$v:0?>"> </td>
                                                                    <?else:?>
                                                                        <td><input class="tc_investments_input_<?=$i-1?> tc_investments_input" type="text" /></td>
                                                                    <?endif?>
                                                                    <?$i++;endforeach?>
                                                            </tr>

                                                            <tr class="tc_net_cash_flow">
                                                                <td>Чистый денежный поток</td>
                                                                <?$i=0;foreach($arResult["ANALIZ_MONEY"]["YEARS"] as $item):?>
                                                                    <td></td>
                                                                    <?$i++;endforeach?>
                                                            </tr>

                                                            <tr class="tc_discount_coefficient">
                                                                <td>Коэффициент дисконтирования</td>
                                                                <?$i=0;foreach($arResult["ANALIZ_MONEY"]["YEARS"] as $item):?>
                                                                    <td></td>
                                                                    <?$i++;endforeach?>
                                                            </tr>

                                                            <tr class="tc_discounted_cash_flow">
                                                                <td>Дисконтированный денежный поток</td>
                                                                <?$i=0;foreach($arResult["ANALIZ_MONEY"]["YEARS"] as $item):?>
                                                                    <td></td>
                                                                    <?$i++;endforeach?>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <table class="company_two_cols_table">
                                                            <tbody>
                                                            <tr>
                                                                <td>Терминальная стоимость, млн.&nbsp;руб.</td>
                                                                <td><span class="tc_terminal_value">15,0%</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Справедливая стоимость, млн.&nbsp;руб.</td>
                                                                <td><span class="tc_fair_value">10,0%</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Текущая капитализация, млн.&nbsp;руб.</td>
                                                                <?$val = $arResult["ACTION"]["PROPS"]["ISSUECAPITALIZATION"];
                                                                if($arResult["ACTION"]["PROPS"]["ISSUECAPITALIZATION_DOL"]){
                                                                    $val = $arResult["ACTION"]["PROPS"]["ISSUECAPITALIZATION_DOL"];
                                                                }?>
                                                                <td><span class="tc_current_capitalization" data-value="<?=$val?>"></span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Потенциал прироста</td>
                                                                <td><span class="tc_growth_potential">10,0%</span></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        <?endif?>
                                    </div>
                                </div>
                            </div>
                        <?endif?>
                    </div>
                </div>
                <?/*else:?>
						<?if($arResult["COMPANY"]["REPORT_FIELDS"]):?>
							<div class="company_accordion_outer">
							  <div class="accordion custom_collapse">
								<div class="custom_collapse_elem opened">
								  <div class="infinite">
									<div class="infinite_container">
									  <div class="custom_collapse_elem_head">
										<h2 class="custom_collapse_elem_title collapse_btn" style="border-bottom:0">Финансовые показатели компании <?=$arResult["COMPANY"]["NAME"]?> <span class="icon icon-arr_down"></span></h2>
									  </div>
									  <div class="custom_collapse_elem_body">
										<div class="custom_collapse_elem_body_inner">

										  <div class="tab-content">
											<div class="tab-pane active" id="company_usa_custom_tabs_01_quarter">
												<?if($arResult["PROPERTIES"]["VAL_ED"]["VALUE"]):?>
													<p class="font_13 text-right strong green">данные в таблице указаны в <?=$arResult["PROPERTIES"]["VAL_ED"]["VALUE"]?></p>
												<?endif?>
											  <table class="transform_table">
												<thead>
												  <tr>
													<th>Показатель</th>
													<?$i=1;foreach($arResult["COMPANY"]["REPORT"] as $period=>$vals):
														$val = explode("-", $period);
														?>

														<th data-breakpoints="<?=$arResult["POINTS"][$i]?>">
															<?if($arResult["COMPANY"]["REPORT_TYPE"]=="KVARTAL"):?>
																<?=$val[0]?> квартал <?=$val[1]?>
															<?else:
																$str = "01.".$val[0].".".$val[1];
																$dt = new DateTime("01.".$val[0].".20".$val[1]);?>
																<?=FormatDate("f Y", $dt->getTimeStamp())?>
															<?endif?>
														</th>
													<?$i++;endforeach?>
												  </tr>
												</thead>

												<tbody>
													<?foreach($arResult["COMPANY"]["REPORT_FIELDS"] as $name=>$v):?>
														<tr>
															<td><?=$name?></td>
															<?$i=1;foreach($arResult["COMPANY"]["REPORT"] as $period=>$vals):?>
																<td>
																	<?if(!$arResult["HAVE_ACCESS"] && $i<=3):?>

																	<?else:?>
																		<?=$vals[$name]?>
																	<?endif?>
																</td>
															<?$i++;endforeach?>
														</tr>
													<?endforeach?>
												</tbody>
											  </table>
											  <?if(!$arResult["HAVE_ACCESS"]):?>
												<?if ($USER->IsAuthorized()):?>
													<span class="buy_subscribe_popup_btn button" data-toggle="modal" data-target="#buy_subscribe_popup">Открыть финансовые показатели по компании</span>
												<?else:?>
													<a href="/lk/radar_in/" class="buy_subscribe_popup_btn button">Получить демо-доступ к радару</a>
												<?endif?>
											  <?endif?>
											</div>

										  </div>
										</div>
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							</div>
							<br/><br/>
						<?endif?>
					<?endif*/?>

                <?if($arResult["COMPANY"]["ACTIONS"]):?>
				   <?$heading2 = "Список акций ".$arResult["COMPANY"]["NAME"];
					  if(!empty($arResult["PROPERTIES"]["SEO_SUBHEADER_2"]["VALUE"])){
						  $heading2 = $arResult["PROPERTIES"]["SEO_SUBHEADER_2"]["VALUE"];
					  }
					?>
                    <h2><?=$heading2?></h2>
                    <ul>
                        <?foreach($arResult["COMPANY"]["ACTIONS"] as $item):?>
                            <li><a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a></li>
                        <?endforeach?>
                    </ul>
                <?endif?>

                <?if($arResult["COMPANY"]["OBLIGATIONS"] && false):?>
                    <h2>Список облигаций <?=$arResult["COMPANY"]["NAME"]?></h2>
                    <ul>
                        <?foreach($arResult["COMPANY"]["OBLIGATIONS"] as $item):?>
                            <li><a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a></li>
                        <?endforeach?>
                    </ul>
                <?elseif(false):?>
                    <h2>Список облигаций <?=$arResult["COMPANY"]["NAME"]?></h2>
                    <p>В данный момент нет облигаций в обращении на бирже</p>
                <?endif?>

                <div class="text-center mt50 mb-40">
                    <p class="legal_popup_btn"><a href="#" data-toggle="modal" data-target="#legal_popup">Условия использования информации, размещённой на данном сайте</a></p>
                </div>

                <?if($_SERVER["HTTP_X_REQUESTED_WITH"]!="XMLHttpRequest"):?>
                    <div class="article_stats">
                        <div class="row">
                            <div class="col col-xs-6">
                            </div>

                            <div class="col col-xs-6">
                                <ul class="article_stats_list">
                                    <li><span class="icon icon-comment"></span> <span id="commentsCount"></span></li>
                                    <li><span class="icon icon-eye"></span> <?=$arResult["SHOW_COUNTER"]?></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?
                    $lj_event = htmlentities('<a href="http://'.$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"].'">'.$arResult["NAME"].'</a><img src="http://'.$_SERVER['HTTP_HOST'].$image_src_for_social.'">');

                    if($arResult["PREVIEW_PICTURE"]["SRC"]!='') {
                        $image_src_for_social = $arResult["PREVIEW_PICTURE"]["SRC"];
                        $APPLICATION->AddHeadString('<meta content="'.$image_src_for_social.'" property="og:image">',true);
                        $APPLICATION->AddHeadString('<link rel="image_src" href="'.$image_src_for_social.'" />',true);
                    }
                    ?>
                    <div class="article_share">
                        <p>Рассказать другим</p>
                        <ul class="share_list">
                            <li>
                                <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?=$arResult["NAME"]?>&amp;image=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'].$arResult["DETAIL_PAGE_URL"]?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'].$image_src_for_social?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event?>&amp;subject=<?=$arResult["NAME"]?>"></a>
                            </li>
                            <!-- <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li> -->
                            <li><a href="https://twitter.com/share?text=<?=$arResult["NAME"]?>" onclick="window.open('https://twitter.com/share?text=<?=$arResult["NAME"]?>','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a></li>
                            <li>
                                <?$inFavorite = checkInFavorite($arResult["ID"], "usa_company")?>
                                <span<?if(!$USER->IsAuthorized()):?> data-toggle="modal" data-target="#popup_login"<?endif?> class="to_favorites button<?if($inFavorite):?> active<?endif;?>" data-type="usa_company" data-id="<?=$arResult["ID"]?>">
	                                <span class="icon icon-star_empty"></span> <span class="text"><?if($inFavorite):?>В избранном<?else:?>В избранное<?endif;?></span>
	                            </span>
                            </li>
                        </ul>
                    </div>

                    <?$APPLICATION->IncludeComponent("bitrix:news.detail", "banner_bottom", Array(
                        "IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
                        "IBLOCK_ID" => "24",	// Код информационного блока
                        "ELEMENT_ID" => 18470,	// ID новости
                        "ELEMENT_CODE" => "",	// Код новости
                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                        "FIELD_CODE" => array(	// Поля
                            0 => "SHOW_COUNTER",
                            1 => "PREVIEW_PICTURE",
                        ),
                        "PROPERTY_CODE" => array(	// Свойства
                            0 => "RELATED_POSTS",
                            1 => "",
                        ),
                        "IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
                        "AJAX_MODE" => "N",	// Включить режим AJAX
                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        "CACHE_TYPE" => "A",	// Тип кеширования
                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                        "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
                        "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
                        "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
                        "SET_STATUS_404" => "Y",	// Устанавливать статус 404, если не найдены элемент или раздел
                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                        "ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
                        "ACTIVE_DATE_FORMAT" => "",	// Формат показа даты
                        "USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
                        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                        "DISPLAY_NAME" => "Y",	// Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
                        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                        "USE_SHARE" => "N",	// Отображать панель соц. закладок
                        "PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                        "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                        "PAGER_TITLE" => "Страница",	// Название категорий
                        "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                    ),
                        false
                    );?>

                    <?if($arResult["ARTICLES"]):?>
                        <div class="other_articles_list">
                            <div class="other_articles_list_top">
                                <p>Рекомендуемые к прочтению статьи:</p>

                                <a class="link_arrow" href="/blog/">Все статьи</a>
                            </div>
                            <div class="articles_list colored row">
                                <?foreach ($arResult["ARTICLES"] as $item):?>
                                    <div class="articles_list_element col-xs-6 col-lg-4">
                                        <div class="articles_list_element_inner">
                                            <div class="articles_list_element_img">
                                                <div class="articles_list_element_img_inner">
                                                    <a href="<?=$item["DETAIL_PAGE_URL"]?>" style="background-image:url(<?=CFile::GetPath($item["PREVIEW_PICTURE"])?>);"></a>
                                                </div>
                                            </div>

                                            <div class="articles_list_element_text">
                                                <p class="articles_list_element_title">
                                                    <a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a>
                                                </p>
                                                <p  class="articles_list_element_description"><?=$item["PREVIEW_TEXT"]?></p>

                                                <p class="articles_list_element_date">
                                                    <?=FormatDate(array(
                                                        "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                        "today" => "today",              // выведет "сегодня", если дата текущий день
                                                        "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                        "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                        "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                                    ), MakeTimeStamp($item["DATE_ACTIVE_FROM"]), time()
                                                    );?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?endforeach;?>
                            </div>
                        </div>
                    <?endif;?>

                    <hr/>
                    <p class="subtitle">Обсуждение <?=$arResult["NAME"]?></p>

                    <div class="comment_outer">
                        <div class="content-main-post-item-comments">
                            <?$APPLICATION->IncludeComponent("cackle.comments", "cackle_default", Array(
                            ),
                                false
                            );?>
                        </div>
                    </div>
                <?endif?>
                <!-- *****КАРКАС***** -->
            </div>
        </div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "right_sidebar",
                "EDIT_TEMPLATE" => "",
                "COMPONENT_TEMPLATE" => ".default",
                "AREA_FILE_RECURSIVE" => "Y"
            ),
            false
        );?>
    </div>
    <div class="load_overlay">
        <p><span class="radar_animate"><span class="radar_round_inner"></span><span class="radar_round_track"></span><span class="radar_animate_element blue"></span><span class="radar_animate_element green"></span><span class="radar_animate_element orange"></span></span> <span class="radar_txt">Загрузка...</span></p>
    </div>
</div>



<div class="modal fade" id="legal_popup" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <p class="strong">Условия использования информации с сайта Fin-plan.org</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="modal-body_title"></div>
                <div class="modal-body_inner">
                    <p>Вся информация, размещенная на данном интернет-сайте, дизайн, подбор, группировка и расположение материалов являются интеллектуальной собственностью ИП Кошин В.В. и защищены российским законодательством об авторском праве и средствах массовой информации, а также международными договорами РФ по защите интеллектуальной собственности.</p>
                    <p>Информация о торгах на рынке группы «московская биржа» предоставлена ПАО Московская биржа. Источником и владельцем биржевой информации является московская биржа.</p>
                    <p>Без письменного согласия биржи нельзя осуществлять дальнейшее распространение и предоставление биржевой информации в любом виде и любыми средствами, включая электронные, механические, фотокопировальные, записывающие или другие (в том числе с использованием удаленного мобильного (беспроводного) доступа), её трансляцию, в том числе средствами теле- и радиовещания, её демонстрацию на интернет-сайтах, её использование в игровых, тренажерных и иных системах, предусматривающих демонстрацию и/или передачу биржевой информации, а также для расчёта производной информации (в том числе индексов и индикаторов), предназначенной для дальнейшего публичного распространения.</p>
                    <p>ИП Кошин В.В. является дистрибьютором информации об итогах торгов на рынках группы московская биржа.</p>
                    <p>Материалы данного интернет-сайта предназначены исключительно для персонального и некоммерческого использования. Запрещается использование автоматизированного извлечения информации данного интернет-сайта без письменного разрешения ИП Кошин В.В. Любое копирование, перепечатка и/или последующее распространение информации, представленной на данном сайте, или информации, полученной на основе этой информации в результате переработки, в том числе производимое путем кэширования, кадрирования или использованием аналогичных средств, строго запрещается без предварительного письменного разрешения ИП Кошин В.В. За нарушение настоящего правила наступает ответственность, предусмотренная законодательством и международными договорами РФ.</p>
                    <p>ИП Кошин В.В. не несет ответственность за любую потерю или ущерб (потеря деловых доходов, потеря прибыли или любой прямой, косвенный, последующий, специальный или подобный ущерб вообще), являющийся результатом:</p>
                    <ul>
                        <li>любой погрешности (или неполноты в, или задержки, прерывания, ошибки или упущения в поставке) в информации;</li>
                        <li>любого сбоя информационной системы;</li>
                        <li>любого решения или действия, сделанного в уверенности относительно информации.</li>
                    </ul>
                    <p>ИП Кошин В.В. не несет ответственность за ущерб, причиненный вашему компьютеру, программному обеспечению, модему, телефону и другой собственности, который стал результатом использования информации, программного обеспечения или услуг.</p>
                    <p>С уважением, компания Fin-plan.</p>
                </div>
            </div>
        </div>
    </div>
</div>
