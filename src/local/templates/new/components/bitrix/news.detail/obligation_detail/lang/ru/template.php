<?
$MESS["OBL_PAGE_PARAM_ISIN"] = "уникальный код бумаги, который однозначно идентифицирует ценную бумагу";
$MESS["OBL_PAGE_PARAM_SHARE_TYPE"] = "по принадлежности эмитента, а также наличие признака 'голубые фишки' - бумаги отобранные экспертами компании Fin-Plan";
$MESS["OBL_PAGE_PARAM_DEFAULTS"] = "наличие дефолтов по данной облигации и срок исполнения обязательств. <a href='https://fin-plan.org/blog/investitsii/defolt-obligatsiy/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["OBL_PAGE_PARAM_NKD"] = "Накопленный купонный доход - это часть купонного дохода по облигации, которая рассчитывается пропорционально длительности купона. Данная сумма учитывается при покупке и продаже облигаций.";
$MESS["OBL_PAGE_PARAM_CURRENT_PRICE_WITH_NKD"] = "рассчитана как произведение 'Номинала' и  'Текущей цены' + Накопленный купонный доход (НКД).";
$MESS["OBL_PAGE_PARAM_LISTING_LEVEL"] = "уровень листинга означает соответствие требованиям и условиям биржи. Чем выше уровень (1, 2), тем лучше.";
$MESS["OBL_PAGE_PARAM_DURATION"] = "период времени, через который нам вернется инвестируемая сумма. Облигации с более длинной дюрацией более волатильны. <a href='https://fin-plan.org/blog/investitsii/chto-takoe-dyuratsiya-obligatsiy/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["OBL_PAGE_PARAM_ESTIMATED_ANNUAL_RETURN"] = "Годовая доходность = Общая доходность / Количество дней до погашения * 365 * 100%. При сроке погашения облигации меньше года, следует ориентироваться на общую доходность!";
$MESS["OBL_PAGE_PARAM_TOTAL_RETURN"] = "Общая доходность к погашению облигации = (Цена погашения с учетом купонов - Текущая цена с учетом НКД) /  Текущая цена с учетом НКД * 100%.";
$MESS["OBL_PAGE_PARAM_YIELD_TO_OFFER_ANNUAL"] = "Оферта - это возможность досрочного погашения облигаций. Доходность к оферте рассчитывается также как годовая доходность, только к дате оферты.";
$MESS["OBL_PAGE_PARAM_OFFER_DATE"] = "В зависимости от вида оферты наступают различные права и обязанности эмитента и владельца облигации. <a href='https://fin-plan.org/blog/investitsii/oferta-obligatsiy/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["OBL_PAGE_PARAM_PRIORITY_PAYMENT"] = "Субординированные облигации - это облигации без гарантийного покрытия собственным капиталом, т.е. займ под репутацию банка. <a href='https://fin-plan.org/blog/investitsii/subordinirovannye-obligatsii/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["OBL_PAGE_PARAM_COUPON_CALC_FORMULA"] = "Размер купона облигации рассчитывается по данной формуле и неизвестен до конца срока гашения. Рассчитать доходность, исходя из различных гипотез можно в 'Калькуляторе облигаций' на странице ниже. <a href='https://fin-plan.org/blog/investitsii/obligatsii-s-peremennym-kuponom/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["OBL_PAGE_PARAM_"] = "";
$MESS["OBL_PAGE_PARAM_"] = "";
$MESS["OBL_PAGE_PARAM_"] = "";
$MESS["OBL_PAGE_PARAM_"] = "";
$MESS["OBL_PAGE_PARAM_"] = "";

 ?>