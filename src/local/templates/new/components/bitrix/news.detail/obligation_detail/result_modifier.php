<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;

$arResult["POINTS"] = array(
	"",
	"xxs",
	"xxs xs",
	"xxs xs sm",
	"xxs xs sm md",
	"xxs xs sm md lg"
);

//облигация
$arResult["OBLIGATION"]["TITLE"] = $arResult["NAME"];
//Доступ
$arResult["HAVE_ACCESS"] = checkPayRadar();
$arResult["CLOSED_NO_ACCESS_PROPS"] = array(
"Очередность выплат",
"Дополнительный доход"
);
$arFilter = Array("IBLOCK_ID"=>27, "CODE"=>$arResult["CODE"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($item = $res->GetNextElement())
{
	$arResult["OBLIGATION"] = $item->GetFields();
	$arResult["OBLIGATION"]["PROPS"] = $item->GetProperties();
	$arResult["OBLIGATION"]["TITLE"] = 'Облигация '.$arResult["OBLIGATION"]["NAME"].' ('.$arResult["OBLIGATION"]["CODE"].')';
	
	$APPLICATION->SetTitle($arResult["OBLIGATION"]["TITLE"]);
}

//компания
if($arResult["OBLIGATION"]["PROPS"]["EMITENT_ID"]["VALUE"]){
	$arFilter = Array("IBLOCK_ID"=>26, "ID"=>$arResult["OBLIGATION"]["PROPS"]["EMITENT_ID"]["VALUE"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
	if($item = $res->GetNext())
	{
		$arResult["COMPANY"] = $item;

		$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$item["NAME"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL"));
		if($item = $res->GetNext())
		{
			$arResult["COMPANY"]["DETAIL_PAGE_URL"] = $item["DETAIL_PAGE_URL"];
		}
	}
}

$propsArray = [
    "ISIN",
    "CSV_VID_OLB",
    "DEFAULT",
    "FACEVALUE",
    "LASTPRICE",
    "ACCRUEDINT",
    "CUR_PRICE_NKD_RUB",
    "LISTLEVEL",
    "COUPONVALUE",
    "COUPONPERIOD",
    "NEXTCOUPON",
    "MATDATE",
    "DURATION",
    "DOXOD_GOD",
    "DOHOD_TOTAL",
    "DOXOD_OFFERTA",
    "OFFERDATE",
    "CSV_PAYMENT_ORDER",
    "ADDITIONAL_INCOME",
    "RATING_PROTECTION_TYPE",
    "COUPON_INCOME",
    "ADDITIONAL_INCOME_PERIOD",
	 "PRC_OF_USERS",
	 "LASTPRICE_PLUS_COUPONS"
];

//список данных
if($arResult["OBLIGATION"]["PROPS"]){
	$res = new Obligations();
	$obligationItem = $res->getItem($arResult["CODE"]);
	$arResult["OBLIGATION"]["DYNAM"] = $obligationItem["DYNAM"];
	foreach($propsArray as $propCode){
		if($propCode == "DEFAULT"){
			$val = "нет";
			$isRed = false;
			if(in_array("дефолтные", $arResult["OBLIGATION"]["PROPS"]["CSV_VID_OLB"]["VALUE"])){
				$val = "да";
				$isRed = true;
			}
			if($arResult["OBLIGATION"]["PROPS"]["CSV_VID_DEFOLT"]["VALUE"]){
				$val = $arResult["OBLIGATION"]["PROPS"]["CSV_VID_DEFOLT"]["VALUE"];
				$isRed = true;
			}
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Дефолты",
				"VALUE" => $val,
				"IS_RED" => $isRed
			);
		} elseif($propCode == "LASTPRICE_PLUS_COUPONS"){
			if($obligationItem["DYNAM"]["Цена погашения с учетом купонов"]){
				$val = round($obligationItem["DYNAM"]["Цена погашения с учетом купонов"], 2);
			} else {
				continue;
			}
		  $arResult["LIST_VALUES"][] = array(
				"NAME" => "Цена погашения с учетом купонов",
				"VALUE" => $val,
				"IS_RED" => false
			);


		} elseif($propCode == "PRC_OF_USERS"){
			if(floatval($arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"])>0){
				$val = "облигация есть у ".$arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"]."% пользователей";
			} else {
			  	$val = "эта облигация встречается менее чем у 1% пользователей радар";
			}
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Рейтинг радара",
				"VALUE" => $val,
			);
		  } elseif($propCode == "DOXOD_GOD") {
			$val = "нет цены для расчета";

			if ($obligationItem["DYNAM"]["Доходность годовая"]) {
				$val = $obligationItem["DYNAM"]["Доходность годовая"];
			}


			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Расчетная годовая доходность",
				"VALUE" => $val."%",
			);
		} elseif($propCode == "CUR_PRICE_NKD_RUB"){
			if($obligationItem["DYNAM"]["Цена покупки"]){
				$val = $obligationItem["DYNAM"]["Цена покупки"];
			} else {
				continue;
			}


			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Текущая цена с учетом НКД",
				"VALUE" => $val.convertOblCurrency($arResult["OBLIGATION"]["PROPS"]["CURRENCYID"]["VALUE"]),
			);
		} elseif($propCode == "DOHOD_TOTAL"){
			$val = "нет цены для расчета";

			if($obligationItem["DYNAM"]["Доходность общая"]){
				$val = $obligationItem["DYNAM"]["Доходность общая"];
			}

			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Общая доходность",
				"VALUE" => $val."%",
			);
		} elseif($propCode == "DOXOD_OFFERTA") {
			$val = "нет оферты";

			if ($obligationItem["DYNAM"]["Доходность к офферте"]) {
				$val = $obligationItem["DYNAM"]["Доходность к офферте"]."%";
			}

			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Доходность к оферте (годовая)",
				"VALUE" => $val,
			);
		}elseif($propCode == "LASTPRICE"){
			$val = "";

			if($arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"]){
				$val = $arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"];
			} elseif($arResult["OBLIGATION"]["PROPS"]["LEGALCLOSE"]["VALUE"]){
				$val = $arResult["OBLIGATION"]["PROPS"]["LEGALCLOSE"]["VALUE"];
			} else {
				continue;
			}

			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Текущая цена",
				"VALUE" => $val."%",
			);
		}elseif($propCode === "CSV_PAYMENT_ORDER" && $arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"][0]){

			//$val = "Облигация " . $arResult["OBLIGATION"]["NAME"] . " - " . $arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"][0];

			$arResult["LIST_VALUES"][] = [
				"NAME" => "Очередность выплат",
				"VALUE" => $arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"][0],
			];
		} else {
			if($arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"]){
				$tmp = array(
					"NAME" => $arResult["OBLIGATION"]["PROPS"][$propCode]["NAME"],
					"VALUE" => $arResult["OBLIGATION"]["PROPS"][$propCode]["VALUE"],
				);

				if($propCode=="OFFERDATE"){
					$dt = new DateTime($tmp["VALUE"]);
					$tmp["VALUE"] = $dt->format("d.m.Y");

					$i = 0;
					while($i<5){
						$dt->modify("-1 day");
						if(!isWeekEndDay($dt->format("d.m.Y"))){
							$i++;
						}
					}

					$offerText = '';
					//$offerText .= !empty($arResult["OBLIGATION"]["PROPS"]["OFFERTYPE"]["VALUE"])?$arResult["OBLIGATION"]["PROPS"]["OFFERTYPE"]["VALUE"].'. ':'';
					$offerText .= $arResult["OBLIGATION"]["PROPS"]["OFFERTYPE"]["VALUE"]=='Call-оферта'?' Установлена возможность досрочного погашения биржевых облигаций по усмотрению эмитента':' Для погашения облигации по оферте необходимо подать заявление своему брокеру до '.$dt->format("d.m.Y");


					$tmp["VALUE"].= " (".(!empty($arResult["OBLIGATION"]["PROPS"]["OFFERTYPE"]["VALUE"])?$arResult["OBLIGATION"]["PROPS"]["OFFERTYPE"]["VALUE"].'. ':'').$offerText.")";
				}


				if(is_array($tmp["VALUE"])){
					$tmp["VALUE"] = implode(", ", $tmp["VALUE"]);
				}
				
				if($propCode=="FACEVALUE" || $propCode=="COUPONVALUE" || $propCode=="ACCRUEDINT"){
					$tmp["VALUE"] .= convertOblCurrency($arResult["OBLIGATION"]["PROPS"]["CURRENCYID"]["VALUE"]);
				}

				if($propCode=="MATDATE"){
					$tmp["NAME"] = "Дата погашения облигации";
				}/*elseif($propCode=="LASTPRICE"){
					$tmp["NAME"] = "Текущая цена в %";
				}*/elseif($propCode=="ACCRUEDINT"){
					$tmp["NAME"] = "НКД";
				}
				
				if(in_array($propCode, ["NEXTCOUPON", "MATDATE"]) && $tmp["VALUE"]){
					if($tmp["VALUE"]=="0000-00-00"){
						continue;
					}
					$dt = new DateTime($tmp["VALUE"]);
					$tmp["VALUE"] = $dt->format("d.m.Y");
				}

				if($propCode=="COUPONVALUE" && $obligationItem["DYNAM"]["Купоны в %"]){
					$tmp["VALUE"].= " (".round($obligationItem["DYNAM"]["Купоны в %"], 2)."%)";
				}
				
				$arResult["LIST_VALUES"][] = $tmp;
				
				//Добавляем кол-во дней до погашения
                if($propCode=="MATDATE"){
                    $origin = new DateTime();
                    $target = new DateTime($tmp["VALUE"]);
                    if($target>$origin) {
                        $interval = $origin->diff($target);
                        $tmpDays["NAME"] = "Кол-во дней до погашения облигации";
                        $tmpDays["VALUE"] = $interval->format('%a') + 1;
                        $arResult["LIST_VALUES"][] = $tmpDays;
                    }
                }
                
                //Добавляем кол-во дней до оферты
                if($propCode=="OFFERDATE"){
                    $origin = new DateTime();
                    $target = new DateTime($arResult["OBLIGATION"]["PROPS"]["OFFERDATE"]["VALUE"]);
                    if($target>$origin) {
                        $interval = $origin->diff($target);
                            $tmpDays["NAME"] = "Кол-во дней до оферты";
                            $tmpDays["VALUE"] = $interval->format('%a')+1;
                            $arResult["LIST_VALUES"][] = $tmpDays;
                    }
                }
                
			}
		}
	}
}


//купоны
CModule::IncludeModule("highloadblock");

$hlblock   = HL\HighloadBlockTable::getById(15)->fetch();
$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
$entityClass = $entity->getDataClass();

$res = $entityClass::getList(array(
	"filter" => array(
		"=UF_ITEM" => $arResult["CODE"],
		//"!UF_DATA" => false,
	),
	"select" => array(
		"UF_DATA"
	),
));
while($item = $res->fetch()){
	if(empty($item["UF_DATA"])) continue;
	$arCoupons = json_decode($item["UF_DATA"], true);



	foreach($arCoupons as $item){
		if(!empty($item["Дата"]) && floatval($item["Сумма гашения"])>0){
			$arCouponsDates[$item["Дата"]] = $item["Сумма гашения"];
		}
		if(trim($item["Примечание"])){
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Формула расчета купона",
				"VALUE" => $item["Примечание"],
			);
		}
	}




	$cup_cnt = 0;
	$cup_page_cnt = 0;

	foreach($arCoupons as $item){

		if(!$item["Дата купона"]){
			continue;
		}
		$dt = DateTime::createFromFormat('d.m.Y', trim($item["Дата купона"]));

	  //	if($dt->getTimeStamp()>time()){

			if($arResult["COUPONS"]){
				$t = $arResult["COUPONS"][count($arResult["COUPONS"])-1];
				$nominal = $t["Номинал"]-$t["Гашение"];
			} else {
				$nominal = $arResult["OBLIGATION"]["PROPS"]["INITIALFACEVALUE"]["VALUE"];
			}
			
/*			$tmp = array(
				"Дата выплаты" => $dt->format("d.m.Y"),
				"Номинал" => $nominal,
				"Купоны" => round($item["Ставка купона"]/100*$nominal*$item["Длительность купона"]/365, 2),
				"Гашение" => round($arCouponsDates[$dt->format("d.m.Y")]?$arCouponsDates[$dt->format("d.m.Y")]:0, 2),
			);*/
			$dtString = $dt->format("d.m.Y");
			$tmp = array(
				"Дата выплаты" => $dt->format("d.m.Y"),
				"Номинал" => $nominal,
				"Купоны" => $item["Ставка купона"]/100*$nominal*$item["Длительность купона"]/365,
				"Гашение" => $arCouponsDates[$dt->format("d.m.Y")]?$arCouponsDates[$dt->format("d.m.Y")]:0,
				"page" => $cup_page_cnt,
				"Неопределен" => isset($item["Ставка купона"]) && $item["Ставка купона"]=='-'?'Y':'N'
			);

			$tmp["Денежный поток"] = $tmp["Купоны"]+$tmp["Гашение"];
			
			
			$arResult["COUPONS"][] = $tmp;

		  $cup_cnt++;
		  if($cup_cnt==20){
		  	$cup_cnt = 0;
			$cup_page_cnt++;
		  }

	 //	} //if($dt->getTimeStamp()>time())

	}

	$arResult["COUPON_PAGES_CNT"] = $cup_page_cnt;

	if($arResult["COUPONS"]){
		$arResult["COUPONS_FIELDS"] = array(
			"Дата выплаты" => true,
			"Номинал" => true,
			"Купоны" => true,
			"Гашение" => true,
			"Денежный поток" => true,
		);	
		
		$showTotal = true;
		foreach($arResult["COUPONS"] as $n=>$item){
			if($item["Неопределен"]=='Y'){
				$arResult["COUPONS"][$n]["Купоны"] = "Купон пока не определен";
				$showTotal = false;
			}
			if(!$item["Денежный поток"]){
				$arResult["COUPONS"][$n]["Денежный поток"] = "Зависит от купона";
				$showTotal = false;
			}
			
			if($showTotal){
				$arResult["COUPONS_TOTAL"]["Купоны"] += $item["Купоны"];
				$arResult["COUPONS_TOTAL"]["Гашение"] += $item["Гашение"];
				$arResult["COUPONS_TOTAL"]["Денежный поток"] += $item["Денежный поток"];
			}
		}
		if(!$showTotal){
			unset($arResult["COUPONS_TOTAL"]);
		}
	}
}

//статьи
$obCache = new CPHPCache();

$cacheLifetime = 86400*365*10; 
$cacheID = 'random_articles_for'.$arResult["ID"]; 
$cachePath = '/random_articles/';

if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
{
	$arResult["ARTICLES"] = $obCache->GetVars();
} elseif($obCache->StartDataCache()) {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
	$arFilter = Array("IBLOCK_ID"=>5, "!SECTION_ID"=>array(5, 17), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND"=>"asc"), $arFilter, false, array("nPageSize"=>3), $arSelect);
	while ($ob = $res->GetNext()){
		$arResult["ARTICLES"][] = $ob;
	}

   $obCache->EndDataCache($arResult["ARTICLES"]);
}


//график
//if($USER->IsAdmin()) {
    $code = $arResult["CODE"];
    if ($arResult["OBLIGATION"]["PROPS"]["SECID"]["VALUE"] != $code) {
        $code = $arResult["OBLIGATION"]["PROPS"]["SECID"]["VALUE"];
    }
    $graph = new MoexGraph();
    $arResult["CHARTS_DATA"] = $graph->getForObligation($code);
	 //Готовим данные для линий таргета и просада на графике котировок
/*	 $target = 0;//$arResult["ACTION"]["PROPS"]["PROP_TARGET"]["VALUE"];
	 $targetPrc = 0;//$actionItem["DYNAM"]["Таргет"];
	 $arResult["CHARTS_DATA"]["target"] = array(array(str_replace("-","/",$arResult["CHARTS_DATA"]["min"]).' 00:00:00', $target, $targetPrc), array(str_replace("-","/",$arResult["CHARTS_DATA"]["max"]).' 00:00:00', $target, $targetPrc));
	 $arResult["CHARTS_DATA"]["target"] = json_encode($arResult["CHARTS_DATA"]["target"]);
	 $drawdown = 0;//$arResult["ACTION"]["PROPS"]["PROP_PROSAD"]["VALUE"];
	 $drawdownPrc = 0;//$actionItem["DYNAM"]["Просад"];
	 $arResult["CHARTS_DATA"]["drawdown"] = array(array(str_replace("-","/",$arResult["CHARTS_DATA"]["min"]).' 00:00:00', $drawdown, $drawdownPrc), array(str_replace("-","/",$arResult["CHARTS_DATA"]["max"]).' 00:00:00', $drawdown, $drawdownPrc));
	 $arResult["CHARTS_DATA"]["drawdown"] = json_encode($arResult["CHARTS_DATA"]["drawdown"]);
*/
//Текст

if($arResult["OBLIGATION"]["PROPS"]["MATDATE"]["VALUE"] ){
	$dt = new DateTime($arResult["OBLIGATION"]["PROPS"]["MATDATE"]["VALUE"]);



	if($dt->getTimestamp()>time()){
		if(!$arResult["OBLIGATION"]["PROPS"]["HIDEN"]["VALUE"] ){
			$arResult["OBLIGATION_TEXT"] = "Облигация ".$arResult["NAME"];
			if($arResult["CHARTS_DATA"]["real_from"]){
			 $arResult["OBLIGATION_TEXT"] .= " находится в обращении с ".FormatDate("j F Y", $arResult["CHARTS_DATA"]["real_from"]->getTimestamp())." г.";
			}
		} else {
			$arResult["OBLIGATION_TEXT"] = "Облигация ".$arResult["NAME"]." <span class='red'>больше не обращается</span> на Московской бирже.";
		}
	}else{
		if($arResult["OBLIGATION"]["PROPS"]["MATDATE"]["VALUE"]=='0000-00-00'){
		  $arResult["OBLIGATION_TEXT"] = "Облигация ".$arResult["NAME"];
		  if($arResult["CHARTS_DATA"]["real_from"]){
		  $arResult["OBLIGATION_TEXT"] .=" находится в обращении с ".FormatDate("j F Y", $arResult["CHARTS_DATA"]["real_from"]->getTimestamp())." г.";
		  }
		} else {
		  $arResult["OBLIGATION_TEXT"] = "Облигация ".$arResult["NAME"]." <span class='red'>погашена</span>.";
		}

	}

	$APPLICATION->SetPageProperty("description", strip_tags($arResult["OBLIGATION_TEXT"]));
}

if($arResult["OBLIGATION"]["PROPS"]["MATDATE"]["VALUE"]){
  	if($arResult["COMPANY"]){
		$arResult["OBLIGATION_TEXT"] .= " Эмитент облигации - <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
		$APPLICATION->SetPageProperty("description", strip_tags($arResult["OBLIGATION_TEXT"]));
	}

}

//Ключевики
$arKeywords = array();
if($arResult["LIST_VALUES"]){
	$arKeywords[] = "Параметры облигации ".$arResult["NAME"];
}
if($arResult["CHARTS_DATA"]["items"]){
	$arKeywords[] = "График облигации ".$arResult["NAME"];
}
if($arResult["COUPONS"]){
	$arKeywords[] = "Купоны и амортизация по облигации ".$arResult["NAME"];
}

if($arKeywords){
	$APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}