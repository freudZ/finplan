<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Highloadblock as HL;

global $APPLICATION;

/*$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cacheId = "company_usa_data".$arResult["ID"];
$cacheTtl = 86400 * 7;*/

$arResult["POINTS"] = array(
    "",
    "xxs",
    "xxs xs",
    "xxs xs sm",
    "xxs xs sm md",
    "xxs xs sm md lg"
);
$arResult["KVARTAL_MONTH"] = array(
    1 => "Мар.",
    2 => "Июн.",
    3 => "Сен.",
    4 => "Дек.",
);

//Доступ
$arResult["HAVE_ACCESS"] = checkPayRadar();

CModule::IncludeModule("highloadblock");


if ($arResult["PREVIEW_TEXT"]) {
    $obParser = new CTextParser;
    $APPLICATION->SetPageProperty("DESCRIPTION", trim(strip_tags($obParser->html_cut($arResult["PREVIEW_TEXT"], 200))));
}

//получение сектора
$arFilter = Array("IBLOCK_ID" => 58, "NAME" => $arResult["NAME"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID", "NAME", "CODE"));
if ($item = $res->GetNext()) {
    $arResultCompany["COMPANY"] = $item;
}
$CSectors = new SectorsUsa();
$x = $CSectors->getItem($arResultCompany["COMPANY"]['CODE']);
$periods = $x['PERIODS'];
$arResultCompany['PERIODS'] = $periods;


$arResult = array_merge($arResult, $arResultCompany);

//Ключеные показатели
if ($periods) {
    foreach ($periods as $period => $vals) {
        $arResult["EXPANDED_PERIODS"]["PERIODS"][] = $period;
        if (count($arResult["EXPANDED_PERIODS"]["PERIODS"]) >= 6) {
            break;
        }
    }
    foreach ($arResult["EXPANDED_PERIODS"]["PERIODS"] as $period) {
        foreach ($periods[$period] as $f => $v) {
            if (in_array($f, ['profit_temp', 'proceeds_temp', 'PERIOD_VAL', 'PERIOD_YEAR', 'PERIOD_TYPE'])) {
                continue;
            }
            $arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"][$f] = true;
        }
    }

    $arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"]);

    $needPeriod = 4;

    foreach ($periods as $period => $vals) {
        $x[] = $vals;
        if ($vals["PERIOD_VAL"] == $needPeriod ) {

            $arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"][] = $period;

            if (count($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) >= 5) {
                break;
            }
        }
    }


    foreach ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $period) {
        foreach ($periods[$period] as $f => $v) {
            if (in_array($f, ['profit_temp', 'proceeds_temp', 'PERIOD_VAL', 'PERIOD_YEAR', 'PERIOD_TYPE'])) {
                continue;
            }
            $arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"][$f] = true;
        }
    }



    $arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"]);

    //для графика квартала, берем последний - 1 квартал
    if ($arResult["EXPANDED_PERIODS"]["PERIODS"]) {
        $arKvartalToMonth = array(
            1 => "03-31",
            2 => "06-30",
            3 => "09-30",
            4 => "12-31",
        );

        $t = array_reverse($arResult["EXPANDED_PERIODS"]["PERIODS"]);
        $last = explode("-", $t[0]);

        if ($last[0] == 1) {
            $last[0] = 4;
            $last[1] -= 1;
        } else {
            $last[0] -= 1;
        }
        $from = $last[1] . "-" . $arKvartalToMonth[$last[0]];

        /*
        1 квартал с 1 января по 31 марта
        2 квартал с 1 апреля по 30 июня
        3 квартал с 1 июля по 30 сентября
        4 квартал с 1 октбяря по 31 декабря
        */

        $arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"] = getDatesArray($from, $periods);
		  //Заполняем недостающую капу на пустых периодах
		  $arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"] = $CSectors->fillEmptyPeriodsCapa($arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"], $arResult["PROPERTIES"]["ISSUECAPITALIZATION"]["VALUE"]);

    }

    //для года берем начала последнего года
    if ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) {
        $t = array_reverse($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]);
        $last = explode("-", $t[0]);
        $from = $last[1] . "-01-01";
        //график
        $arResult["EXPANDED_PERIODS"]["YEAR_CHART"] = getDatesArray($from, $periods);
		  //Заполняем недостающую капу на пустых периодах
		  $arResult["EXPANDED_PERIODS"]["YEAR_CHART"] = $CSectors->fillEmptyPeriodsCapa($arResult["EXPANDED_PERIODS"]["YEAR_CHART"], $arResult["PROPERTIES"]["ISSUECAPITALIZATION"]["VALUE"]);

    }
}

function getDatesArray($startDate, $periods) {
	$arQuartMonth = array(1 => 1, 2 => 1, 3 => 1, 4 => 2, 5 => 2, 6 => 2, 7 => 3, 8 => 3, 9 => 3, 10 => 4, 11 => 4, 12 => 4,);
	$startDate = new DateTime($startDate);
	$lastPeriod = '';
		  foreach($periods as $k=>$v){
		  	if($v["CURRENT_PERIOD"]==true){
		  		unset($periods[$k]);
				break;
		  	}
		  }
		  reset($periods);
		  $t = key(array_keys($periods));
		  $last = explode("-", $t);
		  $lastPeriod = $arResult["KVARTAL_TO_MONTH"][$last[0]]."-".$last[1];
		  $lastPeriod = str_replace("-", ".", $lastPeriod);

        $to = (new DateTime(date($lastPeriod)));//->add(new DateInterval('P2M'));

    //$to = (new DateTime(date('d.m.Y')))->add(new DateInterval('P2M'));

    $interval_val = 'P1M';
    $startDate = $startDate->modify('first day of this month');
    $period = new DatePeriod(
        $startDate,
        //new DateInterval('P' . $interval . 'D'),
        new DateInterval($interval_val),
        $to
    );
    $getDates = [];
/*    foreach ($period as $key => $value) {
        $getDates[] = [
            FormatDate("M. Y", $value->getTimestamp()),
        ];
    }*/

	foreach ($period as $key => $value) {
		$quartNum = $arQuartMonth[intval($value->format('n'))];
		$periodStr = $quartNum . "-" . $value->format('Y') . "-KVARTAL";
		if($value->format('Y')>=2015){//Отрезаем график слева до 4 квартала 2015 года
			if(($quartNum>3 && $value->format('Y')==2015) || $value->format('Y')>2015){
				$blueValue = $periods[$periodStr]["Прошлая капитализация"];
			}
		}

		//После обновления битрикс месяц стал по символу M выводиться с маленькой буквы, из-за чего не рисуются желтые столбики на графиках.
		//принудительно приводим первый символ строки в верхний регистр. mb_ucfirst лежит в init.php
		$date = FormatDate("M. Y", $value->getTimestamp());
		$date = mb_ucfirst($date);
		$getDates[] = [$date, round($blueValue,2) ?: '', $periodStr,];
	}

    return $getDates;
}

//статьи
$obCache = new CPHPCache();

$cacheLifetime = 86400 * 365 * 10;
$cacheID = 'random_articles_for' . $arResult["ID"];
$cachePath = '/random_articles/';

if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
    $arResult["ARTICLES"] = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
    $arFilter = Array("IBLOCK_ID" => 5, "!SECTION_ID" => array(5, 17), "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array("RAND" => "asc"), $arFilter, false, array("nPageSize" => 3), $arSelect);
    while ($ob = $res->GetNext()) {
        $arResult["ARTICLES"][] = $ob;
    }

    $obCache->EndDataCache($arResult["ARTICLES"]);
}

//Ключевики
$arKeywords = array($arResult["NAME"]);

/*if ($arResult["PROPERTIES"]["EXPANDED"]["VALUE"]) {
    if ($arResult["EXPANDED_PERIODS"]) {
        $arKeywords[] = "Ключевые показатели";
    }
    if ($arResult["BALANCE"]) {
        $t = "Баланс";
        if ($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
            $t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
        }

        $arKeywords[] = $t;
    }
    if ($arResult["PLUSMINUS"]) {
        $t = "Отчет о прибылях и убытках";
        if ($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
            $t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
        }

        $arKeywords[] = $t;
    }
    if ($arResult["CASHFLOW"]) {
        $t = "Отчет о движении денежных средств";
        if ($arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"]) {
            $t .= " (" . $arResult["PROPERTIES"]["TYPE_REPORT"]["VALUE"] . ")";
        }

        $arKeywords[] = $t;
    }
    if ($arResult["PROPERTIES"]["BUSANALIZ"]["~VALUE"]["TEXT"]) {
        $arKeywords[] = "Бизнес-анализ";
    }
    if ($arResult["ACTION"]) {
        $arKeywords[] = "Инвестиционный анализ";
    }
    if ($arResult["ANALIZ_MONEY"]["YEARS"]) {
        $arKeywords[] = "Анализ денежных потоков";
    }
} else {
    if ($arResult["COMPANY"]["REPORT_FIELDS"]) {
        $arKeywords[] = "Финансовые показатели компании " . $arResult["COMPANY"]["NAME"];
    }
    if ($arResult["COMPANY"]["ACTIONS"]) {
        $arKeywords[] = "Список акций " . $arResult["COMPANY"]["NAME"];
    }
    if ($arResult["COMPANY"]["OBLIGATIONS"]) {
        $arKeywords[] = "Список облигаций " . $arResult["COMPANY"]["NAME"];
    }
}*/

if ($arKeywords) {
    $APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}

