<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner">
                <? if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "extended_banner",
                            "EDIT_TEMPLATE" => "",
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_RECURSIVE" => "Y"
                        ),
                        false
                    ); ?>

                    <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"), Array(), Array("MODE" => "html", "NAME" => "сайдбар")); ?>
                <? endif ?>

                <div class="title_container title_nav_outer">
                    <h1><? $APPLICATION->ShowTitle(false); ?></h1>

                    <? if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
                        <div class="title_nav">
                            <ul>
                                <li><a href="/lk/sectors_usa/all/">Все сектора США</a></li>
                            </ul>
                        </div>
                    <? endif ?>
                </div>
                <? if (!$_GET['search_off']): ?>
                    <br>
                    <form class="innerpage_search_form" method="GET">
                        <div class="form_element">
                            <input type="text" value="<?= $_REQUEST["q"] ?>" placeholder="Что вы ищете?" name="q"
                                   id="autocomplete_company_usa"/>
                            <button type="submit"><span class="icon icon-search"></span></button>
                        </div>
                    </form>
                <? endif ?>
                <? /*Последние 4 месяца или квартала*/ ?>
                <div class="article_inner">
                    <?
                    $imgStr = "";
                    if ($arResult["PREVIEW_PICTURE"]["SRC"]) {
                        $imgStr = '<div class="article_inner_image"> <img src="' . $arResult["PREVIEW_PICTURE"]["SRC"] . '" alt="' . $arResult["NAME"] . '" /></div>';
                    }
                    $arResult["PREVIEW_TEXT"] = str_replace("#IMG#", $imgStr, $arResult["PREVIEW_TEXT"]);
                    ?>
                    <p><?= $arResult["PREVIEW_TEXT"] ?></p><br/>
                </div>
                <div class="company_accordion_outer">
                    <div class="custom_collapse">
                        <? if ($arResult["EXPANDED_PERIODS"]): ?>
                            <div class="custom_collapse_elem opened">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <? $heading1 = "Финансовые показатели " . $arResult["COMPANY"]["NAME"] . ($arResult["PROPERTIES"]["TYPE_REPORT_FIRST"]["VALUE"] ? ' (' . $arResult["PROPERTIES"]["TYPE_REPORT_FIRST"]["VALUE"] . ') ' : ' ');
                                            if (!empty($arResult["PROPERTIES"]["SEO_SUBHEADER_1"]["VALUE"])) {
                                                $heading1 = $arResult["PROPERTIES"]["SEO_SUBHEADER_1"]["VALUE"];
                                            }
                                            ?>
                                            <h2 class="custom_collapse_elem_title collapse_btn"><?= $heading1 ?><span
                                                    class="icon icon-arr_down"></span></h2>
                                        </div>
                                        <div class="custom_collapse_elem_body">
                                            <div class="custom_collapse_elem_body_inner">
                                                <? if (!$arResult["PROPERTIES"]["IS_REGION"]["VALUE"]): ?>
                                                    <ul class="custom_tabs company_chart_tab company_chart_tab_01">
                                                        <li class="active">
                                                            <a href="#company_usa_custom_tabs_01_quarter" data-toggle="tab">по кварталам</a>
                                                        </li>
                                                        <li>
                                                            <a href="#company_usa_custom_tabs_01_year" data-toggle="tab">по годам</a>
                                                        </li>
                                                    </ul>
                                                <? endif ?>

                                                <div class="tab-content">
                                                    <div class="tab-pane active"
                                                         data-blue-name="Прошлая капитализация"
                                                         data-currency-sign="<?= "$"//getCurrencySign($arResult["ACTION"]["PROPS"]["CURRENCY"]) ?>"
                                                         id="company_usa_custom_tabs_01_quarter">
                                                        <? if ($arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"]): ?>
                                                            <div class="company_chart_outer">
                                                                <canvas class="company_chart"
                                                                        id="key_indicators_quarters_chart"
                                                                        data-value='<?= json_encode($arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"]) ?>'></canvas>
                                                            </div>
                                                        <? endif ?>

                       <span class="checkbox custom hideCurrentPeriodFinpoks">
                            <input type="checkbox" id="hideCurrentPeriodFinpoks" class="" name="hideCurrentPeriodFinpoks" value="Y">
                            <label for="hideCurrentPeriodFinpoks">Скрывать текущий период</label>
                         </span>
                                                        <?// if ($arResult["PROPERTIES"]["CURRENCY"]["VALUE"]): ?>
                                                            <p class="font_13 text-right strong green">данные в таблице
                                                                указаны
                                                                в $<?//= $arResult["PROPERTIES"]["CURRENCY"]["VALUE"] ?></p>
                                                        <?// endif ?>
                                                        <? if ($arResult["PROPERTIES"]["REPORT_DATE"]["VALUE"]): ?>
                                                            <p class="font_13 text-right strong green">дата начала
                                                                финансового года
                                                                - <?= $arResult["PROPERTIES"]["REPORT_DATE"]["VALUE"] ?></p>
                                                        <? endif ?>
                                                        <? if (!$arResult["HAVE_ACCESS"]): ?>
                                                        <div style="position:relative">
                                                            <? endif ?>
                                                            <table class="key_indicators_table transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <? $i = 0;
                                                                    foreach ($arResult["EXPANDED_PERIODS"]["PERIODS"] as $item):
																								$itemMarker = $item;
                                                                        $item = explode("-", $item);

                                                                        if ($item[0]) {
                                                                            $item = $item[0] . " кв. " . $item[1];
                                                                        } else {
                                                                            $item = $item[1] . " год";
                                                                        }
                                                                        ?>
                                                                    <?if(isset($arResult["PERIODS"][$itemMarker]["CURRENT_PERIOD"]) && $arResult["PERIODS"][$itemMarker]["CURRENT_PERIOD"]==1):?>
                                                                        <th class="tableCurrentPeriodCell" data-breakpoints="<?=$arResult["POINTS"][$i]?>">Текущий</th>
                                                                    <?else:?>
																						  <? if($i>=5){
																								  $currentPeriodClass = 'tableReservedPeriodCell hidden';
																							  } ?>
                                                                        <th class="<?= $currentPeriodClass ?>" data-breakpoints="<?=$arResult["POINTS"][$i]?>"><?=$item?></th>
                                                                    <?endif;?>
                                                                        <? $i++;endforeach ?>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <? $i = 0;
                                                                foreach ($arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"] as $field => $v):
		                                                                    if ($field == "CURRENT_PERIOD") { continue; }
																								  if ($field == "Темп прироста капитализации с прошлого квартала") {  continue; }
                                                                    		  if ($field == "Темп прироста капитализации с начала года") {  continue; }
                                                                    		  if ($field == "Дивидендная доходность") {  continue; }
                                                                    ?>
                                                                    <tr<? if (!$i):?> class="active"<? endif ?>
                                                                        data-postfix="" data-number="<?= $i ?>">
                                                                        <td><?= $v ?></td>
                                                                        <? $l = 1;
                                                                        foreach ($arResult["EXPANDED_PERIODS"]["PERIODS"] as $item):
                                                                            $t = explode("-", $item);
                                                                            $date = $arResult["KVARTAL_MONTH"][$t[0]] . " " . $t[1];
                                                                        ?>
																								  <?
																										if(isset($arResult["PERIODS"][$item]["CURRENT_PERIOD"]) && $arResult["PERIODS"][$item]["CURRENT_PERIOD"]==1){
																										 $currentPeriodClass = 'tableCurrentPeriodCell';
																										} else {
																										 $currentPeriodClass = '';
																										}

																										if(end($arResult["EXPANDED_PERIODS"]["PERIODS"])==$item){
																									  		 $currentPeriodClass = 'tableReservedPeriodCell hidden';
																										}
																									 ?>

                                                                            <td class="<?= $currentPeriodClass ?>" data-date="<?= $date ?>"
                                                                                data-val="<?= $arResult["PERIODS"][$item][$field] ?>">
                                                                                <? if (!$arResult["HAVE_ACCESS"] && $l <= 3):?>

                                                                                <? else:?>
                                                                                    <?= formatNumber($arResult["PERIODS"][$item][$field]) ?>
                                                                                <? endif ?>
                                                                            </td>
                                                                        <? $l++;endforeach ?>
                                                                    </tr>
                                                                    <? $i++;endforeach ?>
                                                                </tbody>
                                                            </table>
                                                            <? if (!$arResult["HAVE_ACCESS"]): ?>
                                                            <? if ($USER->IsAuthorized()): ?>
                                                                <span class="buy_subscribe_popup_btn button"
                                                                      data-toggle="modal"
                                                                      data-target="#buy_subscribe_popup">Открыть ключевые показатели по компании</span>
                                                            <? else: ?>
                                                                <a href="#" class="buy_subscribe_popup_btn button"
                                                                   data-toggle="modal" data-target="#popup_simple_auth">Получить
                                                                    демо-доступ к Fin-plan Radar</a>
                                                            <? endif ?>
                                                        </div>
                                                    <? endif ?>

                                                    </div>

                                                    <div class="tab-pane"
                                                         data-blue-name="Прошлая капитализация"
                                                         data-currency-sign="<?= "$"//getCurrencySign($arResult["ACTION"]["PROPS"]["CURRENCY"]) ?>"
                                                         id="company_usa_custom_tabs_01_year">
                                                        <? if ($arResult["EXPANDED_PERIODS"]["YEAR_CHART"]): ?>
                                                            <div class="company_chart_outer">
                                                                <canvas class="company_chart"
                                                                        id="key_indicators_years_chart"
                                                                        data-value='<?= json_encode($arResult["EXPANDED_PERIODS"]["YEAR_CHART"]) ?>'></canvas>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!$arResult["HAVE_ACCESS"]): ?>
                                                        <div style="position:relative">
                                                            <? endif ?>
                                                            <? //if ($arResult["PROPERTIES"]["CURRENCY"]["VALUE"]): ?>
                                                                <p class="font_13 text-right strong green">данные в
                                                                    таблице указаны
                                                                    в $<?//= $arResult["PROPERTIES"]["CURRENCY"]["VALUE"] ?></p>
                                                            <? //endif ?>
                                                            <table class="key_indicators_years_table transform_table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Показатель</th>
                                                                    <? $i = 0;
                                                                    foreach ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $item):
                                                                        $item = explode("-", $item);
                                                                        $item = $item[1] . " год";
                                                                    ?>
                                                                        <th data-breakpoints="<?= $arResult["POINTS"][$i] ?>"><?= $item ?></th>
                                                                    <? $i++;endforeach ?>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <? $i = 0;
                                                                foreach ($arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"] as $field => $v): ?>
                                                                        <tr<? if (!$i): ?> class="active"<? endif ?>
                                                                            data-postfix="" data-number="<?= $i ?>">
                                                                            <td><?= $v ?></td>
                                                                            <? $l = 1;
                                                                                foreach ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $item):
                                                                                    $t = explode("-", $item);
                                                                                    $date = $arResult["KVARTAL_MONTH"][$t[0]] . " " . $t[1];
                                                                                ?>
                                                                                    <td data-date="<?= $date ?>"
                                                                                        data-val="<?= $arResult["PERIODS"][$item][$field] ?>">
                                                                                        <? if (!$arResult["HAVE_ACCESS"] && $l <= 2):?>

                                                                                        <? else:?>
                                                                                            <?= formatNumber($arResult["PERIODS"][$item][$field]) ?>
                                                                                        <? endif ?>
                                                                                    </td>
                                                                            <? $l++;endforeach ?>
                                                                        </tr>
                                                                    <? $i++;endforeach ?>
                                                                </tbody>
                                                            </table>
                                                            <? if (!$arResult["HAVE_ACCESS"]): ?>
                                                            <? if ($USER->IsAuthorized()): ?>
                                                                <span class="buy_subscribe_popup_btn button"
                                                                      data-toggle="modal"
                                                                      data-target="#buy_subscribe_popup">Открыть ключевые показатели по компании</span>
                                                            <? else: ?>
                                                                <!--<a href="/lk/radar_in/" class="buy_subscribe_popup_btn button">Получить демо-доступ к Fin-plan Radar</a>   -->
                                                                <a class="buy_subscribe_popup_btn button" href="#"
                                                                   data-toggle="modal" data-target="#popup_simple_auth">Получить
                                                                    демо-доступ к Fin-plan Radar</a>
                                                            <? endif ?>
                                                        </div>
                                                    <? endif ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endif ?>
                    </div>
                </div>

                <div class="text-center mt50 mb-40">
                    <p class="legal_popup_btn"><a href="#" data-toggle="modal" data-target="#legal_popup">Условия
                            использования информации, размещённой на данном сайте</a></p>
                </div>

                <? if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
                    <div class="article_stats">
                        <div class="row">
                            <div class="col col-xs-6">
                            </div>

                            <div class="col col-xs-6">
                                <ul class="article_stats_list">
                                    <li><span class="icon icon-comment"></span> <span id="commentsCount"></span></li>
                                    <li><span class="icon icon-eye"></span> <?= $arResult["SHOW_COUNTER"] ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?
                    $lj_event = htmlentities('<a href="http://' . $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] . '">' . $arResult["NAME"] . '</a><img src="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '">');

                    if ($arResult["PREVIEW_PICTURE"]["SRC"] != '') {
                        $image_src_for_social = $arResult["PREVIEW_PICTURE"]["SRC"];
                        $APPLICATION->AddHeadString('<meta content="' . $image_src_for_social . '" property="og:image">', true);
                        $APPLICATION->AddHeadString('<link rel="image_src" href="' . $image_src_for_social . '" />', true);
                    }
                    ?>
                    <div class="article_share">
                        <p>Рассказать другим</p>
                        <ul class="share_list">
                            <li>
                                <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?= $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] ?>&amp;title=<?= $arResult["NAME"] ?>&amp;image=http://<?= $_SERVER['HTTP_HOST'] . $image_src_for_social ?>', 'vk', 'width=626, height=626'); return false;"
                                   title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk"
                                   href="http://vkontakte.ru/share.php?url=http://<?= $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] ?>&amp;title=<?= $arResult["NAME"] ?>&amp;image=http://<?= $_SERVER['HTTP_HOST'] . $image_src_for_social ?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?= $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] ?>&amp;p[images][0]=http://<?= $_SERVER['HTTP_HOST'] . $image_src_for_social ?>', 'facebook', 'width=626, height=626'); return false;"
                                   title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb"
                                   href="http://www.facebook.com/sharer.php?u=http://<?= $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] ?>&amp;p[images][0]=http://<?= $_SERVER['HTTP_HOST'] . $image_src_for_social ?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?= $lj_event ?>&amp;subject=<?= $arResult["NAME"] ?>', 'livejournal', 'width=626, height=626'); return false;"
                                   title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj"
                                   href="http://www.livejournal.com/update.bml?event=<?= $lj_event ?>&amp;subject=<?= $arResult["NAME"] ?>"></a>
                            </li>
                            <!-- <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li> -->
                            <li><a href="https://twitter.com/share?text=<?= $arResult["NAME"] ?>"
                                   onclick="window.open('https://twitter.com/share?text=<?= $arResult["NAME"] ?>','Twitter', 'width=626, height=626'); return false;"
                                   class="icon icon-tw" rel="nofollow"></a></li>
                            <li>
                                <? $inFavorite = checkInFavorite($arResult["ID"], "usa_company") ?>
                                <span<? if (!$USER->IsAuthorized()): ?> data-toggle="modal" data-target="#popup_login"<? endif ?> class="to_favorites button<? if ($inFavorite): ?> active<? endif; ?>"
                                                                                                                                  data-type="usa_company"
                                                                                                                                  data-id="<?= $arResult["ID"] ?>">
	                                <span class="icon icon-star_empty"></span> <span
                                        class="text"><? if ($inFavorite): ?>В избранном<? else: ?>В избранное<? endif; ?></span>
	                            </span>
                            </li>
                        </ul>
                    </div>

                    <? $APPLICATION->IncludeComponent("bitrix:news.detail", "banner_bottom", Array(
                        "IBLOCK_TYPE" => "FINPLAN",    // Тип информационного блока (используется только для проверки)
                        "IBLOCK_ID" => "24",    // Код информационного блока
                        "ELEMENT_ID" => 18470,    // ID новости
                        "ELEMENT_CODE" => "",    // Код новости
                        "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                        "FIELD_CODE" => array(    // Поля
                            0 => "SHOW_COUNTER",
                            1 => "PREVIEW_PICTURE",
                        ),
                        "PROPERTY_CODE" => array(    // Свойства
                            0 => "RELATED_POSTS",
                            1 => "",
                        ),
                        "IBLOCK_URL" => "",    // URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
                        "AJAX_MODE" => "N",    // Включить режим AJAX
                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "META_KEYWORDS" => "-",    // Установить ключевые слова страницы из свойства
                        "META_DESCRIPTION" => "-",    // Установить описание страницы из свойства
                        "BROWSER_TITLE" => "-",    // Установить заголовок окна браузера из свойства
                        "SET_STATUS_404" => "Y",    // Устанавливать статус 404, если не найдены элемент или раздел
                        "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "ADD_ELEMENT_CHAIN" => "N",    // Включать название элемента в цепочку навигации
                        "ACTIVE_DATE_FORMAT" => "",    // Формат показа даты
                        "USE_PERMISSIONS" => "N",    // Использовать дополнительное ограничение доступа
                        "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",    // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",    // Выводить детальное изображение
                        "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                        "USE_SHARE" => "N",    // Отображать панель соц. закладок
                        "PAGER_TEMPLATE" => "",    // Шаблон постраничной навигации
                        "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                        "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                        "PAGER_TITLE" => "Страница",    // Название категорий
                        "PAGER_SHOW_ALL" => "Y",    // Показывать ссылку "Все"
                    ),
                        false
                    ); ?>

                    <? if ($arResult["ARTICLES"]): ?>
                        <div class="other_articles_list">
                            <div class="other_articles_list_top">
                                <p>Рекомендуемые к прочтению статьи:</p>

                                <a class="link_arrow" href="/blog/">Все статьи</a>
                            </div>
                            <div class="articles_list colored row">
                                <? foreach ($arResult["ARTICLES"] as $item): ?>
                                    <div class="articles_list_element col-xs-6 col-lg-4">
                                        <div class="articles_list_element_inner">
                                            <div class="articles_list_element_img">
                                                <div class="articles_list_element_img_inner">
                                                    <a href="<?= $item["DETAIL_PAGE_URL"] ?>"
                                                       style="background-image:url(<?= CFile::GetPath($item["PREVIEW_PICTURE"]) ?>);"></a>
                                                </div>
                                            </div>

                                            <div class="articles_list_element_text">
                                                <p class="articles_list_element_title">
                                                    <a href="<?= $item["DETAIL_PAGE_URL"] ?>"><?= $item["NAME"] ?></a>
                                                </p>
                                                <p class="articles_list_element_description"><?= $item["PREVIEW_TEXT"] ?></p>

                                                <p class="articles_list_element_date">
                                                    <?= FormatDate(array(
                                                        "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                        "today" => "today",              // выведет "сегодня", если дата текущий день
                                                        "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                        "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                        "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                                    ), MakeTimeStamp($item["DATE_ACTIVE_FROM"]), time()
                                                    ); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>

                    <hr/>
                    <p class="subtitle">Обсуждение <?= $arResult["NAME"] ?></p>

                    <div class="comment_outer">
                        <div class="content-main-post-item-comments">
                            <? $APPLICATION->IncludeComponent("cackle.comments", "cackle_default", Array(),
                                false
                            ); ?>
                        </div>
                    </div>
                <? endif ?>
                <!-- *****КАРКАС***** -->
            </div>
        </div>

        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "right_sidebar",
                "EDIT_TEMPLATE" => "",
                "COMPONENT_TEMPLATE" => ".default",
                "AREA_FILE_RECURSIVE" => "Y"
            ),
            false
        ); ?>
    </div>
</div>


<div class="modal fade" id="legal_popup" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <p class="strong">Условия использования информации с сайта Fin-plan.org</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="modal-body_title"></div>
                <div class="modal-body_inner">
                    <p>Вся информация, размещенная на данном интернет-сайте, дизайн, подбор, группировка и расположение
                        материалов являются интеллектуальной собственностью ИП Кошин В.В. и защищены российским
                        законодательством об авторском праве и средствах массовой информации, а также международными
                        договорами РФ по защите интеллектуальной собственности.</p>
                    <p>Информация о торгах на рынке группы «московская биржа» предоставлена ПАО Московская биржа.
                        Источником и владельцем биржевой информации является московская биржа.</p>
                    <p>Без письменного согласия биржи нельзя осуществлять дальнейшее распространение и предоставление
                        биржевой информации в любом виде и любыми средствами, включая электронные, механические,
                        фотокопировальные, записывающие или другие (в том числе с использованием удаленного мобильного
                        (беспроводного) доступа), её трансляцию, в том числе средствами теле- и радиовещания, её
                        демонстрацию на интернет-сайтах, её использование в игровых, тренажерных и иных системах,
                        предусматривающих демонстрацию и/или передачу биржевой информации, а также для расчёта
                        производной информации (в том числе индексов и индикаторов), предназначенной для дальнейшего
                        публичного распространения.</p>
                    <p>ИП Кошин В.В. является дистрибьютором информации об итогах торгов на рынках группы московская
                        биржа.</p>
                    <p>Материалы данного интернет-сайта предназначены исключительно для персонального и некоммерческого
                        использования. Запрещается использование автоматизированного извлечения информации данного
                        интернет-сайта без письменного разрешения ИП Кошин В.В. Любое копирование, перепечатка и/или
                        последующее распространение информации, представленной на данном сайте, или информации,
                        полученной на основе этой информации в результате переработки, в том числе производимое путем
                        кэширования, кадрирования или использованием аналогичных средств, строго запрещается без
                        предварительного письменного разрешения ИП Кошин В.В. За нарушение настоящего правила наступает
                        ответственность, предусмотренная законодательством и международными договорами РФ.</p>
                    <p>ИП Кошин В.В. не несет ответственность за любую потерю или ущерб (потеря деловых доходов, потеря
                        прибыли или любой прямой, косвенный, последующий, специальный или подобный ущерб вообще),
                        являющийся результатом:</p>
                    <ul>
                        <li>любой погрешности (или неполноты в, или задержки, прерывания, ошибки или упущения в
                            поставке) в информации;
                        </li>
                        <li>любого сбоя информационной системы;</li>
                        <li>любого решения или действия, сделанного в уверенности относительно информации.</li>
                    </ul>
                    <p>ИП Кошин В.В. не несет ответственность за ущерб, причиненный вашему компьютеру, программному
                        обеспечению, модему, телефону и другой собственности, который стал результатом использования
                        информации, программного обеспечения или услуг.</p>
                    <p>С уважением, компания Fin-plan.</p>
                </div>
            </div>
        </div>
    </div>
</div>
