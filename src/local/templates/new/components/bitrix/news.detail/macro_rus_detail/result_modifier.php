<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;
use Bitrix\Iblock\InheritedProperty;
$arResult["POINTS"] = array(
	"",
	"xxs",
	"xxs xs",
	"xxs xs sm",
	"xxs xs sm md",
	"xxs xs sm md lg"
);

//Доступ
$arResult["HAVE_ACCESS"] = checkPayRadar();
$arResult["CLOSED_NO_ACCESS_PROPS"] = array(
	"Просад",
    "Методика расчета дивидендов",
    "Средний прирост x2",
);

/*global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
		 echo "<pre  style='color:black; font-size:11px;'>";
          print_r($arResult["PROPERTIES"]);
          echo "</pre>";
      }*/


//акция
/*$arFilter = Array("IBLOCK_ID"=>32, "CODE"=>$arResult["CODE"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "ID", "DETAIL_PAGE_URL", "PREVIEW_TEXT"));
while($item = $res->GetNextElement())
{
	$arResult["ACTION"] = $item->GetFields();
	$arResult["ACTION"]["PROPS"] = $item->GetProperties();
}*/

/*global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
echo "<pre  style='color:black; font-size:11px;'>";
   print_r($arResult["ACTION"]);
   echo "</pre>";
      }*/

//Кейсы
$arResult["CASE"] = array();
$arFilter = Array("IBLOCK_ID"=>48, "ACTIVE"=>"Y", "PROPERTY_LINKED_ACTIONS"=>$arResult["ACTION"]["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($item = $res->GetNextElement())
{
	$arResult["CASE"][] = $item->GetFields();
}



//статьи
$obCache = new CPHPCache();

$cacheLifetime = 86400*365*10;
$cacheID = 'random_articles_for'.$arResult["ID"]; 
$cachePath = '/random_articles/';

if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
{
	$arResult["ARTICLES"] = $obCache->GetVars();
} elseif($obCache->StartDataCache()) {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
	$arFilter = Array("IBLOCK_ID"=>5, "!SECTION_ID"=>array(5, 17), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND"=>"asc"), $arFilter, false, array("nPageSize"=>3), $arSelect);
	while ($ob = $res->GetNext()){
		$arResult["ARTICLES"][] = $ob;
	}

   $obCache->EndDataCache($arResult["ARTICLES"]);
}

//график
	$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_VAL_ED_FIRST");
   $arFilter = Array("IBLOCK_ID"=>IntVal($arParams["IBLOCK_ID"]), "!=ID"=>$arResult["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
   $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelect);
   while($ob = $res->fetch()){
	  $arResult["ALL_MACRO"][$ob["ID"]] = array("NAME"=>$ob["NAME"], "VAL_ED_FIRST"=>$ob["PROPERTY_VAL_ED_FIRST_VALUE"]);
   }

//global $USER;
//if($USER->IsAdmin()) {
$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cacheId = "all_macro_chart_data";
	$cacheTtl = 86400 * 30;
       //$cache->clean("usa_actions_data");
	if ($cache->read($cacheTtl, $cacheId)) {
       $arResult["CHARTS_DATA"] = $cache->get($cacheId);
        } else {
	    $graph = new MoexGraph();
	    $arResult["CHARTS_DATA"] = $graph->getForMacroValue($arResult["ID"]);
		 $cache->set($cacheId, $arResult["CHARTS_DATA"]);
   }

/*} else {
    $hlblock = HL\HighloadBlockTable::getById(20)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityClass = $entity->getDataClass();

    $res2 = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $arResult["CODE"],
        ),
        "select" => array(
            "UF_DATA"
        ),
    ));
    if ($elem = $res2->fetch()) {
        $arResult["CHARTS_DATA"] = json_decode($elem["UF_DATA"], true);
        $arResult["CHARTS_DATA"]["real_from"] = new DateTime($arResult["CHARTS_DATA"]["real_from"]["date"]);
        $arResult["CHARTS_DATA"]["from"] = new DateTime($arResult["CHARTS_DATA"]["from"]["date"]);
    }
}*/


//Текст акции
if($arResult["ACTION"] && $arResult["CHARTS_DATA"]){
	if(!$arResult["ACTION"]["PROPS"]["HIDEN"]["VALUE"]){
		if($arResult["CHARTS_DATA"]){
			$arResult["ACTION_TEXT"] = $arResult["NAME"]." (тикер - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].") находится в обращении с ".FormatDate("j F Y", $arResult["CHARTS_DATA"]["real_from"]->getTimestamp())." г. Эмитентом ETF является <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
		}
	} else {
		$arResult["ACTION_TEXT"] = $arResult["NAME"]." (тикер - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].") больше не обращается на московской бирже. Эмитент - <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
	}
	
	$APPLICATION->SetPageProperty("description", $arResult["ACTION_TEXT"]);
}

//Ключевики
$arKeywords = array();
if($arResult["LIST_VALUES"]){
	$arKeywords[] = "Параметры ETF ".$arResult["NAME"];
}
if($arResult["CHARTS_DATA"]["items"]){
	$arKeywords[] = "График ETF ".$arResult["NAME"];
}
if($arResult["DIVIDENDS"]){
	$arKeywords[] = "Дивиденды по ETF ".$arResult["NAME"];
}

if($arKeywords){
	$APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}