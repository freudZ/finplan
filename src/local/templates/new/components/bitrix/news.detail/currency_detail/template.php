<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
      die();
   }

   /** @var array $arParams */
   /** @var array $arResult */
   /** @global CMain $APPLICATION */
   /** @global CUser $USER */
   /** @global CDatabase $DB */
   /** @var CBitrixComponentTemplate $this */
   /** @var string $templateName */
   /** @var string $templateFile */
   /** @var string $templateFolder */
   /** @var string $componentPath */
   /** @var CBitrixComponent $component */
   $this->setFrameMode(true);
?>
<div class="wrapper">
	<div class="container">
		<div class="main_content">
			<div class="main_content_inner">
				<?php if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
				<?php $APPLICATION->IncludeComponent(
      "bitrix:main.include",
      ".default",
      array(
         "AREA_FILE_SHOW" => "sect",
         "AREA_FILE_SUFFIX" => "extended_banner",
         "EDIT_TEMPLATE" => "",
         "COMPONENT_TEMPLATE" => ".default",
         "AREA_FILE_RECURSIVE" => "Y"
      ),
      false
);?>
				<?php $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"), Array(), Array("MODE" => "html", "NAME" => "сайдбар"));?>
				<?php endif;?>
				<div class="title_container title_nav_outer">
					<h1>
						<?=$arResult["NAME"];?>
					</h1>
					<?php if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
					<div class="title_nav">
						<ul>
							<li><a href="/lk/obligations/company/all/">Все эмитенты</a></li>
							<li><a href="/lk/obligations/all/">Все облигации</a></li>
							<li><a href="/lk/actions/all/">Все акции</a></li>
							<li><a href="/lk/currency/all/">Все валюты</a></li>
						</ul>
					</div>
					<?php endif;?>
				</div>
				<?php if (!$_GET['search_off']): ?>
				<br>
				<form class="innerpage_search_form" method="GET">
					<div class="form_element">
						<input type="text" value="<?=$_REQUEST[" q "];?>" placeholder="Что вы ищете? Введите название или тикер" name="q" id="autocomplete_act_all" />
						<button type="submit"><span class="icon icon-search"></span></button>
					</div>
				</form>
				<?php endif;?>
				<!-- *****/КАРКАС***** -->
				<?php if ($arResult["PREVIEW_TEXT"]): ?>
				<p>
					<?=$arResult["PREVIEW_TEXT"];?>
				</p>
				<?php endif;?>
				<h2>Параметры валюты
					<?=$arResult["NAME"];?>:</h2>

				<ul>
					<li>
						<p>Полное название:
							<?=$arResult["PROPERTIES"]["NAME"]["VALUE"]?>
						</p>
					</li>
					<li>
						<p>Тикер:
							<?=$arResult["PROPERTIES"]["SECID"]["VALUE"]?>
						</p>
					</li>
					<li>
						<p>Валюта:
							<?=$arResult["PROPERTIES"]["CURRENCYID"]["VALUE"]?>
						</p>
					</li>
					<?if(!empty($arResult["PROPERTIES"]["QUOTATIONS_NOW"]["VALUE"])):?>
					<li>
						<p>Текущее значение:
							<?=round($arResult["PROPERTIES"]["QUOTATIONS_NOW"]["VALUE"],2)?>
						</p>
					</li>
					<?endif;?>
					 <?if(!empty($arResult["DYNAM"]["MONTH_INCREASE"]) || !empty($arResult["DYNAM"]["ONE_YEAR_INCREASE"]) || !empty($arResult["DYNAM"]["THREE_YEAR_INCREASE"])):?>
					<li>
						<p>Динамика индекса:
						   <?if(!empty($arResult["DYNAM"]["MONTH_INCREASE"])):?>
						   прирост за месяц = <span style='color:<?= ($arResult["DYNAM"]["MONTH_INCREASE"] < 0 ? 'red' : 'green') ?>'><?= $arResult["DYNAM"]["MONTH_INCREASE"] ?>% </span>
							<?endif;?>
							<?if(!empty($arResult["DYNAM"]["ONE_YEAR_INCREASE"])):?>
						прирост за год = <span style='color:<?= ($arResult["DYNAM"]["ONE_YEAR_INCREASE"] < 0 ? 'red' : 'green') ?>'><?= $arResult["DYNAM"]["ONE_YEAR_INCREASE"] ?>%  </span>
							<?endif;?>
							<?if(!empty($arResult["DYNAM"]["THREE_YEAR_INCREASE"])):?>
						прирост за три года = <span style='color:<?= ($arResult["DYNAM"]["THREE_YEAR_INCREASE"] < 0 ? 'red' : 'green') ?>'><?= $arResult["DYNAM"]["THREE_YEAR_INCREASE"] ?>% </span>
							<?endif;?>
						</p>
					</li>
					<?endif;?>
				</ul>
				<div class="article_inner">
					<p>
						<?//=$arResult["PREVIEW_TEXT"];?>
					</p>
				</div>
				<!--               <?php if (!empty($arResult["CASE"])): ?>
               <h2>Кейсы по акции <?=$arResult["NAME"];?>:</h2>
               <ul>
               <?php foreach ($arResult["CASE"] as $kcase => $case): ?>
                <li><a href="<?=$case["DETAIL_PAGE_URL"];?>" target="_self"><?=$case["NAME"];?></a></li>
               <?php endforeach;?>
               </ul>
            <?php endif;?>-->
				<?php if ($arResult["CHARTS_DATA"]["items"]): ?>
				<hr/>
				<h2>График валюты <?=$arResult["NAME"];?>:</h2>
				<div class="article_bar_chart_outer">
					<div class="article_bar_chart" id="chart2" data-items='<?=$arResult["CHARTS_DATA"]["items"];?>' data-max="<?=$arResult["CHARTS_DATA"]["max"];?>" data-min="<?=$arResult["CHARTS_DATA"]["min"];?>" data-tick="<?=$arResult["CHARTS_DATA"]["tick"];?>" data-currency="rub"></div>
				</div>
				<?php endif;?>

				<?if($arResult["INDEX_STAFF"]):?>
				<h2>Состав индекса</h2>
				<div class="company_accordion_outer indexes_actives_table">
				<table class="transform_table footable footable-3 breakpoint-xl">
					<thead>
						<tr class="footable-header">
							<th class="footable-first-visible">Акции:</th>
							<th class="text-center price_col">Цена акции, руб.&nbsp;/ <br>Цена лота, руб.&nbsp;</th>
							<th class="text-center">Прирост <br>за месяц</th>
							<th class="text-center">Прирост <br>за год</th>
							<th class="text-center footable-first-visible">Прирост <br>за 3 года</th>
						</tr>
					</thead>
					<tbody>
						<?foreach($arResult["INDEX_STAFF"] as $arAction):?>
						<tr >
							<td class="footable-first-visible">
									<a href="<?=$arAction["URL"]?>" class="tooltip_btn" title="" data-original-title="<?=$arAction["NAME"]?>"><?= $arAction["NAME"] ?></a><br>
									<a class="tooltip_btn line_green" title="" href="<?= $arAction["COMPANY"]["URL"] ?>" data-original-title="<?= $arAction["COMPANY"]["NAME"] ?>"><?= $arAction["COMPANY"]["NAME"] ?></a>
							</td>
							<td class="text-center">
								<?=!empty($arAction["PROPS"]["LASTPRICE"])?$arAction["PROPS"]["LASTPRICE"]:$arAction["PROPS"]["LEGALCLOSE"];?><br>
							   <?=$arAction["DYNAM"]["Цена лота"]?>
							</td>
							<?$class=floatval($arAction["DYNAM"]["MONTH_INCREASE"])>0?'line_green':(floatval($arAction["DYNAM"]["MONTH_INCREASE"])<0?'line_red':'');?>
							<td class="text-center <?= $class ?>">
                       <?= !empty($arAction["DYNAM"]["MONTH_INCREASE"])?round($arAction["DYNAM"]["MONTH_INCREASE"],2)."%":'-'; ?>
							</td>
							<?$class=floatval($arAction["DYNAM"]["YEAR_INCREASE"])>0?'line_green':(floatval($arAction["DYNAM"]["YEAR_INCREASE"])<0?'line_red':'');?>
							<td class="text-center <?= $class ?>">

								<?= !empty($arAction["DYNAM"]["YEAR_INCREASE"])?round($arAction["DYNAM"]["YEAR_INCREASE"],2)."%":'-'; ?>
							</td>
							<?$class=floatval($arAction["DYNAM"]["THREE_YEAR_INCREASE"])>0?'line_green':(floatval($arAction["DYNAM"]["THREE_YEAR_INCREASE"])<0?'line_red':'');?>
							<td class="text-center <?= $class ?> footable-last-visible">
								<?= !empty($arAction["DYNAM"]["THREE_YEAR_INCREASE"])?round($arAction["DYNAM"]["THREE_YEAR_INCREASE"],2)."%":'-'; ?>
							</td>
						</tr>
						<?endforeach;?>
					</tbody>
				</table>
				</div>
				<?endif;?>
				<?php if ($arResult["DETAIL_TEXT"]): ?>
				<p>
					<?=$arResult["DETAIL_TEXT"];?>
				</p>
				<?php endif;?>
				<!--               <?php if ($arResult["DIVIDENDS"]): ?>
               <div class="company_accordion_outer">
                 <div class="accordion custom_collapse">
                  <div class="custom_collapse_elem opened">
                    <div class="infinite">
                     <div class="infinite_container">
                       <div class="custom_collapse_elem_head">
                        <h2 class="custom_collapse_elem_title collapse_btn" style="border-bottom:0">Дивиденды по акции <?=$arResult["NAME"];?> <span class="icon icon-arr_down"></span></h2>
                       </div>
                       <div class="custom_collapse_elem_body">
                        <div class="custom_collapse_elem_body_inner">

                          <div class="tab-content">
                           <div class="tab-pane active" id="custom_tabs_01_quarter">
                             <table class="transform_table">
                              <thead>
                                <tr>
                                 <?php $i = 0;foreach ($arResult["DIVIDENDS_FIELDS"] as $name => $v): ?>
                                    <th data-breakpoints="<?=$arResult["POINTS"][$i];?>">
                                       <?=$name;?>
                                    </th>
                                 <?php $i++;endforeach;?>
                                </tr>
                              </thead>

                              <tbody>
                                 <?php foreach ($arResult["DIVIDENDS"] as $coupon): ?>
                                    <tr>
                                       <?php foreach ($arResult["DIVIDENDS_FIELDS"] as $name => $v): ?>
                                          <td><?=$coupon[$name];?></td>
                                       <?php endforeach;?>
                                    </tr>
                                 <?php endforeach;?>
                              </tbody>
                             </table>
                           </div>

                          </div>
                        </div>
                       </div>
                     </div>
                    </div>
                  </div>
                 </div>
               </div>
               <br/><br/>
            <?php endif;?>-->
				<br/><br/>
				<div class="text-center mb-40">
					<p class="legal_popup_btn"><a href="#" data-toggle="modal" data-target="#legal_popup">Условия использования информации, размещённой на данном сайте</a></p>
				</div>
				<?php if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
				<div class="article_stats">
					<div class="row">
						<div class="col col-xs-6">
						</div>
						<div class="col col-xs-6">
							<ul class="article_stats_list">
								<li><span class="icon icon-comment"></span> <span id="commentsCount"></span></li>
								<li><span class="icon icon-eye"></span>
									<?=$arResult["SHOW_COUNTER"];?>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<?php

                  $lj_event = htmlentities('<a href="http://' . $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] . '">' . $arResult["NAME"] . '</a><img src="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '">');
               ?>
					<div class="article_share">
						<p>Рассказать другим</p>
						<ul class="share_list">
							<li>
								<a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'] . $arResult[" DETAIL_PAGE_URL "];?>&amp;title=<?=$arResult["NAME
								 "];?>&amp;image=http://<?=$_SERVER['HTTP_HOST'] . $image_src_for_social;?>', 'vk', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk" href="http://vkontakte.ru/share.php?url=http://<?=$_SERVER['HTTP_HOST'] . $arResult[" DETAIL_PAGE_URL "];?>&amp;title=<?=$arResult["NAME
								 "];?>&amp;image=http://<?=$_SERVER['HTTP_HOST'] . $image_src_for_social;?>"></a>
							</li>
							<li>
								<a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'] . $arResult[" DETAIL_PAGE_URL
								 "];?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'] . $image_src_for_social;?>', 'facebook', 'width=626, height=626'); return false;" title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb" href="http://www.facebook.com/sharer.php?u=http://<?=$_SERVER['HTTP_HOST'] . $arResult[" DETAIL_PAGE_URL
								 "];?>&amp;p[images][0]=http://<?=$_SERVER['HTTP_HOST'] . $image_src_for_social;?>"></a>
							</li>
							<li>
								<a onclick="window.open('http://www.livejournal.com/update.bml?event=<?=$lj_event;?>&amp;subject=<?=$arResult[" NAME "];?>', 'livejournal', 'width=626, height=626'); return false;" title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj" href="http://www.livejournal.com/update.bml?event=<?=$lj_event;?>&amp;subject=<?=$arResult["
								 NAME "];?>"></a>
							</li>
							<!-- <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li> -->
							<li><a href="https://twitter.com/share?text=<?=$arResult[" NAME "];?>" onclick="window.open('https://twitter.com/share?text=<?=$arResult[" NAME "];?>','Twitter', 'width=626, height=626'); return false;" class="icon icon-tw" rel="nofollow"></a></li>
							<li>
								<?php $inFavorite = checkInFavorite($arResult["ID"], "action");?>
								<span<?php if (!$USER->IsAuthorized()): ?> data-toggle="modal" data-target="#popup_login"
									<?php endif;?> class="to_favorites button
									<?php if ($inFavorite): ?> active
									<?php endif;?>" data-type="action" data-id="
									<?=$arResult["ID"];?>">
										<span class="icon icon-star_empty"></span> <span class="text"><?php if ($inFavorite): ?>В избранном<?php else: ?>В избранное<?php endif;?></span>
										</span>
							</li>
						</ul>
					</div>
					<?php $APPLICATION->IncludeComponent("bitrix:news.detail", "banner_bottom", Array(
                     "IBLOCK_TYPE" => "FINPLAN", // Тип информационного блока (используется только для проверки)
                     "IBLOCK_ID" => "24", // Код информационного блока
                     "ELEMENT_ID" => 18470, // ID новости
                     "ELEMENT_CODE" => "", // Код новости
                     "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
                     "FIELD_CODE" => array( // Поля
                        0 => "SHOW_COUNTER",
                        1 => "PREVIEW_PICTURE",
                     ),
                     "PROPERTY_CODE" => array( // Свойства
                        0 => "RELATED_POSTS",
                        1 => "",
                     ),
                     "IBLOCK_URL" => "", // URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
                     "AJAX_MODE" => "N", // Включить режим AJAX
                     "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
                     "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                     "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
                     "CACHE_TYPE" => "A", // Тип кеширования
                     "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                     "CACHE_GROUPS" => "Y", // Учитывать права доступа
                     "META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
                     "META_DESCRIPTION" => "-", // Установить описание страницы из свойства
                     "BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
                     "SET_STATUS_404" => "Y", // Устанавливать статус 404, если не найдены элемент или раздел
                     "SET_TITLE" => "Y", // Устанавливать заголовок страницы
                     "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
                     "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
                     "ADD_ELEMENT_CHAIN" => "N", // Включать название элемента в цепочку навигации
                     "ACTIVE_DATE_FORMAT" => "", // Формат показа даты
                     "USE_PERMISSIONS" => "N", // Использовать дополнительное ограничение доступа
                     "DISPLAY_DATE" => "Y", // Выводить дату элемента
                     "DISPLAY_NAME" => "Y", // Выводить название элемента
                     "DISPLAY_PICTURE" => "Y", // Выводить детальное изображение
                     "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
                     "USE_SHARE" => "N", // Отображать панель соц. закладок
                     "PAGER_TEMPLATE" => "", // Шаблон постраничной навигации
                     "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                     "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
                     "PAGER_TITLE" => "Страница", // Название категорий
                     "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
                  ),
                     false
               );?>
					<?php if ($arResult["ARTICLES"]): ?>
					<div class="other_articles_list">
						<div class="other_articles_list_top">
							<p>Рекомендуемые к прочтению статьи:</p>
							<a class="link_arrow" href="/blog/">Все статьи</a>
						</div>
						<div class="articles_list colored row">
							<?php foreach ($arResult["ARTICLES"] as $item): ?>
							<div class="articles_list_element col-xs-6 col-lg-4">
								<div class="articles_list_element_inner">
									<div class="articles_list_element_img">
										<div class="articles_list_element_img_inner">
											<a href="<?=$item[" DETAIL_PAGE_URL "];?>" style="background-image:url(<?=CFile::GetPath($item[" PREVIEW_PICTURE "]);?>);"></a>
										</div>
									</div>
									<div class="articles_list_element_text">
										<p class="articles_list_element_title">
											<a href="<?=$item[" DETAIL_PAGE_URL "];?>">
												<?=$item["NAME"];?>
											</a>
										</p>
										<p class="articles_list_element_description">
											<?=$item["PREVIEW_TEXT"];?>
										</p>
										<p class="articles_list_element_date">
											<?=FormatDate(array(
										   "tommorow" => "tommorow", // выведет "завтра", если дата завтрашний день
										   "today" => "today", // выведет "сегодня", если дата текущий день
										   "yesterday" => "yesterday", // выведет "вчера", если дата прошлый день
										   "d" => 'j F Y', // выведет "9 июля", если месяц прошел
										   "" => 'j F Y', // выведет "9 июля 2012", если год прошел
										), MakeTimeStamp($item["DATE_ACTIVE_FROM"]), time()
										);?>
										</p>
									</div>
								</div>
							</div>
							<?php endforeach;?>
						</div>
					</div>
					<?php endif;?>
					<div class="comment_outer">
						<div class="content-main-post-item-comments">
							<?php $APPLICATION->IncludeComponent("cackle.comments", "cackle_default", Array(
                        ),
                           false
                     );?>
						</div>
					</div>
					<?php endif;?>
					<!-- *****КАРКАС***** -->
			</div>
		</div>
		<?php $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
               "AREA_FILE_SHOW" => "sect",
               "AREA_FILE_SUFFIX" => "right_sidebar",
               "EDIT_TEMPLATE" => "",
               "COMPONENT_TEMPLATE" => ".default",
               "AREA_FILE_RECURSIVE" => "Y"
            ),
            false
      );?>
	</div>
</div>
<div class="modal fade" id="legal_popup" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<p class="strong">Условия использования информации с сайта Fin-plan.org</p>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<div class="modal-body_title"></div>
				<div class="modal-body_inner">
					<p>Вся информация, размещенная на данном интернет-сайте, дизайн, подбор, группировка и расположение материалов являются интеллектуальной собственностью ИП Кошин В.В. и защищены российским законодательством об авторском праве и средствах массовой информации,
						а также международными договорами РФ по защите интеллектуальной собственности.</p>
					<p>Информация о торгах на рынке группы «московская биржа» предоставлена ПАО Московская биржа. Источником и владельцем биржевой информации является московская биржа.</p>
					<p>Без письменного согласия биржи нельзя осуществлять дальнейшее распространение и предоставление биржевой информации в любом виде и любыми средствами, включая электронные, механические, фотокопировальные, записывающие или другие (в том числе с использованием
						удаленного мобильного (беспроводного) доступа), её трансляцию, в том числе средствами теле- и радиовещания, её демонстрацию на интернет-сайтах, её использование в игровых, тренажерных и иных системах, предусматривающих демонстрацию и/или передачу
						биржевой информации, а также для расчёта производной информации (в том числе индексов и индикаторов), предназначенной для дальнейшего публичного распространения.</p>
					<p>ИП Кошин В.В. является дистрибьютором информации об итогах торгов на рынках группы московская биржа.</p>
					<p>Материалы данного интернет-сайта предназначены исключительно для персонального и некоммерческого использования. Запрещается использование автоматизированного извлечения информации данного интернет-сайта без письменного разрешения ИП Кошин В.В. Любое
						копирование, перепечатка и/или последующее распространение информации, представленной на данном сайте, или информации, полученной на основе этой информации в результате переработки, в том числе производимое путем кэширования, кадрирования или использованием
						аналогичных средств, строго запрещается без предварительного письменного разрешения ИП Кошин В.В. За нарушение настоящего правила наступает ответственность, предусмотренная законодательством и международными договорами РФ.</p>
					<p>ИП Кошин В.В. не несет ответственность за любую потерю или ущерб (потеря деловых доходов, потеря прибыли или любой прямой, косвенный, последующий, специальный или подобный ущерб вообще), являющийся результатом:</p>
					<ul>
						<li>любой погрешности (или неполноты в, или задержки, прерывания, ошибки или упущения в поставке) в информации;</li>
						<li>любого сбоя информационной системы;</li>
						<li>любого решения или действия, сделанного в уверенности относительно информации.</li>
					</ul>
					<p>ИП Кошин В.В. не несет ответственность за ущерб, причиненный вашему компьютеру, программному обеспечению, модему, телефону и другой собственности, который стал результатом использования информации, программного обеспечения или услуг.</p>
					<p>С уважением, компания Fin-plan.</p>
				</div>
			</div>
		</div>
	</div>
</div>