<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;

$arResult["POINTS"] = array(
	"",
	"xxs",
	"xxs xs",
	"xxs xs sm",
	"xxs xs sm md",
	"xxs xs sm md lg"
);

//Доступ
$arResult["HAVE_ACCESS"] = checkPayRadar();
$arResult["CLOSED_NO_ACCESS_PROPS"] = array(
	"Target",
	"Просад",
    "Методика расчета дивидендов",
    "P/E",
);


//Кейсы
/*$arResult["CASE"] = array();
$arFilter = Array("IBLOCK_ID"=>48, "ACTIVE"=>"Y", "PROPERTY_LINKED_ACTIONS"=>$arResult["ACTION"]["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($item = $res->GetNextElement())
{
	$arResult["CASE"][] = $item->GetFields();
}*/


//список данных
/*if($arResult["ACTION"]["PROPS"]){
	$res = new Actions();
	$actionItem = $res->getItem($arResult["CODE"]);

	foreach(array("ISIN", "PROP_TIP_AKTSII", "PROP_SEKTOR", "LOTSIZE", "PROP_SHORT", "PROP_KREDIT", "IN_INDEX", "PRICE", "ISSUECAPITALIZATION", "PROP_TARGET", "PROP_PROSAD", "PROP_DIVIDENDY_2018", 'PROP_DIVIDEND_DESCRIPTION', "PE", "PEQUITY", "PEG", "ACTION_DYNAMIC") as $propCode){
		if($propCode == "PROP_SHORT"){
			$val = "невозможны";
			if($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]){
				$val = "возможны";
			}
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Операции Short",
				"VALUE" => $val,
			);
		} elseif($propCode == "PROP_KREDIT"){
			$val = "невозможны";
			if($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]){
				$val = "возможны";
			}
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Операции с кредитным плечом",
				"VALUE" => $val,
			);
		} elseif($propCode == "IN_INDEX"){
			$val = array();

			if($arResult["ACTION"]["PROPS"]["PROP_MSCI"]["VALUE"]){
				$val[] = "MSCI";
			}
			if($arResult["ACTION"]["PROPS"]["PROP_MMVB"]["VALUE"]){
				$val[] = "ММВБ";
			}

			if($val){
				$arResult["LIST_VALUES"][] = array(
					"NAME" => "Бумага состоит в индексах",
					"VALUE" => implode(", ", $val),
				);
			}
		} elseif($propCode == "ISSUECAPITALIZATION"){
			if(!$actionItem["PROPS"][$propCode]){
				continue;
			}

			$t = array(
				"NAME" => "Капитализация",
				"VALUE" => number_format(round(($actionItem["PROPS"][$propCode]/1000000), 2), 2, '.', ' ') ." млн.р.",
			);




			if($actionItem["PROPS"][$propCode."_DOL"]){
				$t["VALUE"] .= " (".round($actionItem["PROPS"][$propCode."_DOL"]/1000000, 2)." млн. долл.)";
			}





			$arResult["LIST_VALUES"][] = $t;
		} elseif($propCode == "PRICE") {
            if (!$actionItem["PROPS"]["LASTPRICE"]) {
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Цена акции",
                "VALUE" => $actionItem["PROPS"]["LASTPRICE"] . " руб.",
            );

        } elseif($propCode == "PROP_DIVIDEND_DESCRIPTION" && $actionItem["PROPS"][$propCode]){
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Методика расчета дивидендов",
                "VALUE" => $actionItem["PROPS"][$propCode],
            );
		} elseif($propCode == "PE"){
            if(!$actionItem["DYNAM"]["PE"]){
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "P/E",
                "VALUE" => $actionItem["DYNAM"]["PE"],
            );
        } elseif($propCode == "PEQUITY"){
            if(!$actionItem["DYNAM"]["P/Equity"]){
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "P/Equity",
                "VALUE" => $actionItem["DYNAM"]["P/Equity"],
            );
        } elseif($propCode == "PEG"){
            if(!$actionItem["DYNAM"]["PEG"]){
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "PEG",
                "VALUE" => $actionItem["DYNAM"]["PEG"],
            );
        } elseif($propCode == "ACTION_DYNAMIC"){
            $valueArr = [];
            $color = '';
            if($actionItem["DYNAM"]["MONTH_INCREASE"]){
                $valueArr[] = " прирост за  месяц = <span style='color:" . ($actionItem['DYNAM']['MONTH_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["MONTH_INCREASE"] . "%</span>";
            }
            if($actionItem["DYNAM"]["YEAR_INCREASE"]){
                $valueArr[] = " прирост за  год = <span style='color:" . ($actionItem['DYNAM']['YEAR_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["YEAR_INCREASE"] . "%</span>";
            }
            if($actionItem["DYNAM"]["THREE_YEAR_INCREASE"]){
                $valueArr[] = " прирост за  три года = <span style='color:" . ($actionItem['DYNAM']['THREE_YEAR_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["THREE_YEAR_INCREASE"] . "%</span>";
            }

            if ($valueArr) {
                $arResult["LIST_VALUES"][] = array(
                    "NAME" => "Динамика акции",
                    "VALUE" => $valueArr,
                );
            }
        } else {
			if($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]){
				$tmp = array(
					"NAME" => $arResult["ACTION"]["PROPS"][$propCode]["NAME"],
					"VALUE" => $arResult["ACTION"]["PROPS"][$propCode]["VALUE"],
				);



				if(is_array($tmp["VALUE"])){
					$tmp["VALUE"] = implode(", ", $tmp["VALUE"]);
				}
				if($propCode=="PROP_TARGET" && $arResult["ACTION"]["PROPS"]["PROP_DATA_TARGETA"]["VALUE"] && isset($actionItem["DYNAM"]["Таргет"])){
					$tmp["VALUE"] = $actionItem["DYNAM"]["Таргет"]."% (".$tmp["VALUE"]." руб. от ".$arResult["ACTION"]["PROPS"]["PROP_DATA_TARGETA"]["VALUE"].")";
				}
				if($propCode=="PROP_PROSAD" && $arResult["ACTION"]["PROPS"]["PROP_DATA_PROSADA"]["VALUE"]  && isset($actionItem["DYNAM"]["Просад"])){
					$tmp["VALUE"] = $actionItem["DYNAM"]["Просад"]."% (".$tmp["VALUE"]." руб. от ".$arResult["ACTION"]["PROPS"]["PROP_DATA_PROSADA"]["VALUE"].")";
				}
				if($propCode=="PROP_DIVIDENDY_2018"){
					$tmp["VALUE"] .= " руб. или <b>".$actionItem["DYNAM"]["Дивиденды %"]."%</b>";
					if($arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"]){
						$tmp["VALUE"] .= " ".$arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"];
					}
				}

				$arResult["LIST_VALUES"][] = $tmp;
			}
		}
	}
}*/


//статьи
$obCache = new CPHPCache();

$cacheLifetime = 0;//86400*365*10;
$cacheID = 'random_articles_for'.$arResult["ID"];
$cachePath = '/random_articles/';

if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
{
	$arResult["ARTICLES"] = $obCache->GetVars();
} elseif($obCache->StartDataCache()) {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
	$arFilter = Array("IBLOCK_ID"=>5, "!SECTION_ID"=>array(5, 17), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND"=>"asc"), $arFilter, false, array("nPageSize"=>3), $arSelect);
	while ($ob = $res->GetNext()){
		$arResult["ARTICLES"][] = $ob;
	}

   $obCache->EndDataCache($arResult["ARTICLES"]);
}

//график
//global $USER;
//if($USER->IsAdmin()) {
    $indexGraph = new MoexGraph();
    $arResult["CHARTS_DATA"] = $indexGraph->getForIndex($arResult["CODE"], 31, 'currency', 0);
    //$arResult["CHARTS_DATA"] = $indexGraph->getForAction('ALRS');


/*} else {
    $hlblock = HL\HighloadBlockTable::getById(20)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityClass = $entity->getDataClass();

    $res2 = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $arResult["CODE"],
        ),
        "select" => array(
            "UF_DATA"
        ),
    ));
    if ($elem = $res2->fetch()) {
        $arResult["CHARTS_DATA"] = json_decode($elem["UF_DATA"], true);
        $arResult["CHARTS_DATA"]["real_from"] = new DateTime($arResult["CHARTS_DATA"]["real_from"]["date"]);
        $arResult["CHARTS_DATA"]["from"] = new DateTime($arResult["CHARTS_DATA"]["from"]["date"]);
    }
}*/

//Текст акции
/*if($arResult["ACTION"] && $arResult["CHARTS_DATA"]){
	if(!$arResult["ACTION"]["PROPS"]["HIDEN"]["VALUE"]){
		if($arResult["CHARTS_DATA"]){
			$arResult["ACTION_TEXT"] = "Обыкновенная Акция ".$arResult["NAME"]." (тикер акции - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].") находится в обращении с ".FormatDate("j F Y", $arResult["CHARTS_DATA"]["real_from"]->getTimestamp())." г. Эмитентом акции является <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
		}
	} else {
		$arResult["ACTION_TEXT"] = "Акция ".$arResult["NAME"]." (тикер акции - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].") больше не обращается на московской бирже. Эмитент облигации - <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
	}

	$APPLICATION->SetPageProperty("description", $arResult["ACTION_TEXT"]);
}*/

//Получаем привязанные акции
/* $arResult["INDEX_STAFF"] = array();

//Если это IMOEX - для IMOEX - с признаком ММВБ = Да
if($arResult["CODE"]=="IMOEX" && empty($arResult["PROPERTIES"]["ACTIVES"]["VALUE"])){
	$arrFilter["=PROPERTY_PROP_MMVB"] = "Да";
} else {
	$arrFilter["ID"] = $arResult["PROPERTIES"]["ACTIVES"]["VALUE"];
}



if(count($arrFilter["ID"])>0 || $arrFilter["=PROPERTY_PROP_MMVB"]=="Да"){

 $arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID");
 $arFilter = Array("IBLOCK_ID"=>IntVal(32), "=PROPERTY_HIDEN"=>false, "ACTIVE"=>"Y");
  $arFilter = array_merge($arFilter, $arrFilter);
 $resA = new Actions();
 $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

 while($ob = $res->GetNextElement()){
  $arFields = $ob->GetFields();
  $arResult["INDEX_STAFF"][] = $resA->getItem($arFields["CODE"]);
 }
  unset($resA);

}*/
 //Текущая цена индекса
 $v = (new DateTime(date()))->modify("-1 days");
    if(isWeekEndDay($v->format("d.m.Y"))){
        $finded = false;
        while(!$finded){
            $v->modify("-1 days");
            if(!isWeekEndDay($v->format("d.m.Y"))){
                $finded = true;
            }
        }
    }

 CModule::IncludeModule("highloadblock");
$hlblock   = HL\HighloadBlockTable::getById(31)->fetch();
$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
$moexIndexData = $entity->getDataClass();

 $result = $moexIndexData::getList([
            "filter" => [
                "UF_DATE" => $v->format("d.m.Y"),
                "UF_ITEM" => $arResult["CODE"],
            ],
            "select" => [
                "UF_CLOSE"
            ],
        ])->fetch();
 $arResult["DYNAM"]["CLOSEPRICE"] = $result['UF_CLOSE'];


 //Расчет приростов
 if ($arResult["DYNAM"]["CLOSEPRICE"] && $arResult["PROPERTIES"]["QUOTATIONS_MONTH"]["VALUE"]) {
         $arResult["DYNAM"]["MONTH_INCREASE"] = round(((floatval($arResult["DYNAM"]["CLOSEPRICE"]) / floatval($arResult["PROPERTIES"]["QUOTATIONS_MONTH"]["VALUE"]) - 1) * 100), 2);
 }
 if ($arResult["DYNAM"]["CLOSEPRICE"] && $arResult["PROPERTIES"]["QUOTATIONS_ONE_YEAR"]["VALUE"]) {
         $arResult["DYNAM"]["ONE_YEAR_INCREASE"] = round(((floatval($arResult["DYNAM"]["CLOSEPRICE"]) / floatval($arResult["PROPERTIES"]["QUOTATIONS_ONE_YEAR"]["VALUE"]) - 1) * 100), 2);
 }
 if ($arResult["DYNAM"]["CLOSEPRICE"] && $arResult["PROPERTIES"]["QUOTATIONS_THREE_YEAR"]["VALUE"]) {
         $arResult["DYNAM"]["THREE_YEAR_INCREASE"] = round(((floatval($arResult["DYNAM"]["CLOSEPRICE"]) / floatval($arResult["PROPERTIES"]["QUOTATIONS_THREE_YEAR"]["VALUE"]) - 1) * 100), 2);
 }

//Ключевики
$arKeywords = array();
//if($arResult["LIST_VALUES"]){
	$arKeywords[] = "Параметры индекса ".$arResult["NAME"];
//}
if($arResult["CHARTS_DATA"]["items"]){
	$arKeywords[] = "График индекса ".$arResult["NAME"];
}


if($arKeywords){
	$APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}