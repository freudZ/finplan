<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["PROPERTIES"]["LINK_TYPE"]["VALUE_XML_ID"]=="BOOK"):?>
	<?if($arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE"]):?>
		<div class="other_article" style="cursor:pointer" data-toggle="modal" data-target="#popup_<?=$arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE_XML_ID"]?>">
	<?else:?>
		<a href="<?=$arResult["PROPERTIES"]["LINK"]["~VALUE"]?>" class="other_article" >
	<?endif?>
			<p class="other_article_top"><?=$arResult["PROPERTIES"]["TOP_TEXT"]["~VALUE"]?></p>

			<p class="subtitle"><?=$arResult["NAME"]?></p>

			<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt=""/>

			<div class="other_article_bottom" >
				<div class="col_left col">
					<p><?=$arResult["PROPERTIES"]["BOTTOM_TEXT"]["~VALUE"]?></p>
				</div>

				<div class="col_right col" style="">
					<span class="button button_green highlight"><?=$arResult["PROPERTIES"]["LINK_BTN"]["~VALUE"]?> <span class="icon icon-arr_right"></span></span>
				</div>
			</div>
	 <?if($arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE"]):?>
		 </div>
	<?else:?>
		</a>
	<?endif?>
<?elseif($arResult["PROPERTIES"]["LINK_TYPE"]["VALUE_XML_ID"]=="LINK"):?>
	<?if($arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE"]):?>
		<div class="other_article dark" style="background: url(<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>) no-repeat  130% center black;cursor:pointer" data-toggle="modal" data-target="#popup_<?=$arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE_XML_ID"]?>">
	<?else:?>
		<a href="<?=$arResult["PROPERTIES"]["LINK"]["~VALUE"]?>" class="other_article dark" style="background: url(<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>) no-repeat  130% center black;">
	<?endif?>
			<p class="other_article_top"><?=$arResult["PROPERTIES"]["TOP_TEXT"]["~VALUE"]?></p>

			<p class="other_article_title"><?=$arResult["~NAME"]?></p>

			<p class="other_article_description"><?=$arResult["PREVIEW_TEXT"]?></p>

			<div class="other_article_bottom">
				<div class="col_left col">
					<p><?=$arResult["PROPERTIES"]["BOTTOM_TEXT"]["~VALUE"]?></p>
				</div>

				<div class="col_right col">
					<span class="button button_green highlight"><?=$arResult["PROPERTIES"]["LINK_BTN"]["~VALUE"]?> <span class="icon icon-arr_right"></span></span>
				</div>
			</div>
	 <?if($arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE"]):?>
		 </div>
	<?else:?>
		</a>
	<?endif?>
<?elseif($arResult["PROPERTIES"]["LINK_TYPE"]["VALUE_XML_ID"]=="SEMINAR"):?>
	<?if($arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE"]):?>
		<div class="review_more" data-toggle="modal" data-target="#popup_<?=$arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE_XML_ID"]?>" style="cursor:pointer;background-image: url(<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>)">
	<?else:?>
		<a href="<?=$arResult["PROPERTIES"]["LINK"]["~VALUE"]?>" class="review_more">
	<?endif?>
			<div class="review_more_body">
				<p class="review_more_top"><?=$arResult["PROPERTIES"]["TOP_TEXT"]["~VALUE"]?></p>

				<p class="review_more_title"><?=$arResult["~NAME"]?></p>
			</div>

			<div class="review_more_bottom">
				<p class="review_more_description"><?=$arResult["PROPERTIES"]["BOTTOM_TEXT"]["~VALUE"]?></p>

				<span class="button button_white"><?=$arResult["PROPERTIES"]["LINK_BTN"]["~VALUE"]?> <span class="icon icon-arr_right"></span></span>
			</div>
     <?if($arResult["PROPERTIES"]["OPEN_POPUP"]["VALUE"]):?>
		 </div>
	<?else:?>
		</a>
	<?endif?>
<?endif;?>
