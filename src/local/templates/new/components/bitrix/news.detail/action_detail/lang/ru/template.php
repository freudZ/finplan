<?
$MESS["ACT_PAGE_PARAM_ISIN"] = "уникальный код бумаги, который однозначно идентифицирует ценную бумагу";
$MESS["ACT_PAGE_PARAM_SHARE_TYPE"] = "обыкновенные акции - дают право в управлении компании и подвержены влиянию роста и развития компании, привилегированные - имеют преимущественные права по получению дивидендов. <a href='https://fin-plan.org/blog/investitsii/obyknovennye-i-privilegirovannye-aktsii/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["ACT_PAGE_PARAM_SHARE_SECTOR"] = "Секторальная принадлежность компании включает в себя ряд особенностей, которые присущи данной отрасли.";
$MESS["ACT_PAGE_PARAM_INLOT_CNT"] = "Акции торгуются лотами, т.е. это минимальное количество акций при покупке.";
$MESS["ACT_PAGE_PARAM_SHORT_OPERATIONS"] = "Возможность открытия короткой позиции по данному активу, т.е. возможность заработка с понижения котировок бумаги. <a href='https://fin-plan.org/blog/investitsii/kak-shortit-aktsii/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["ACT_PAGE_PARAM_LEVERAGE_TRANSACTIONS"] = "Возможность покупки бумаг с использованием заемных средств. <a href='https://fin-plan.org/blog/investitsii/marzhinalnaya-torgovlya/' target='_blank'>Смотреть&nbsp;статью</a>";
$MESS["ACT_PAGE_PARAM_INDEXES"] = "Индексы позволяют отслеживать состояние и динамику определенных групп активов, объединенных общим признаком. В основные индексы входят наиболее крупные и ликвидные активы.";
$MESS["ACT_PAGE_PARAM_PRICE"] = "Цена 1 акции компании. Минимальное количество бумаг для покупки в графе 'Количество акций в лоте'";
$MESS["ACT_PAGE_PARAM_CAPITALIZATION"] = "это рыночная оценка стоимости компании, рассчитанная на основе текущих цен выпущенных акций. Показатель позволяет оценить размеры компании-эмитента";
$MESS["ACT_PAGE_PARAM_TARGET"] = "Расчетный потенциал = сумма фундаментальной оценки разных источников роста акции: расчет недооценки, оценка роста компании, оценка дивидендной доходности. Не является прогнозом. Не является персональной инвестиционной рекомендацией";
$MESS["ACT_PAGE_PARAM_DRAWDOWN"] = "это показатель оценки риска (максимальная просадка за последние 3 года с учетом текущей технической картины)";
$MESS["ACT_PAGE_PARAM_DIVIDENDS_YEAR"] = "Дивиденды за год с учетом объявленных и выплаченных дивидендов. В расшифровке указаны дивидендные платежи и состояние выплаты (рекомендовано / утверждено / выплачено)";
$MESS["ACT_PAGE_PARAM_DIVIDENTS_METHOD_CALC"] = "Методика расчета дивидендов публикуется во внутренней документации компании - дивидендная политика и устав организации.";
$MESS["ACT_PAGE_PARAM_PE"] = "отношение капитализации компании к скользящей чистой прибыли. Данный мультипликатор показывает уровень недооценки актива. Чем ниже мультипликатор, тем выше недооценка.";
$MESS["ACT_PAGE_PARAM_P_EQUITY"] = "отношение капитализации компании к собственному капиталу. Значение показателя менее 1 означает, что актив стоит меньше, чем размер собственного капитала.";
$MESS["ACT_PAGE_PARAM_"] = "";
$MESS["ACT_PAGE_PARAM_BETTA"] = "Это статистический коэффициент, который характеризует движение отдельной акции относительно всего рынка в целом. Если бета > 1, то акция в статистике обгоняет рынок, от 0 до 1 - движется в одну сторону с рынком, но менее волатильна, менее 0 - акция движется противоположно рынку. Подробнее в статье. Бета расчитана за 5 лет. <a href='https://fin-plan.org/blog/investitsii/koeffitsient-beta-primery-rascheta-i-ispolzovaniya/' target='_blank'> Смотреть&nbsp;статью. </a>";
$MESS["ACT_PAGE_PARAM_"] = "";
$MESS["ACT_PAGE_PARAM_"] = "";
$MESS["ACT_PAGE_PARAM_"] = "";
$MESS["ACT_PAGE_PARAM_"] = "";
$MESS["ACT_PAGE_PARAM_"] = "";
$MESS["ACT_PAGE_PARAM_SHARE_DYNAMICS"] = "это отношение текущих цен в ценам различного периода (месяц / год / 3года)";
$MESS["ACT_PAGE_PARAM_"] = "";
 ?>