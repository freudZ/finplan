<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;
use Bitrix\Iblock\InheritedProperty;
$arResult["POINTS"] = array(
	"",
	"xxs",
	"xxs xs",
	"xxs xs sm",
	"xxs xs sm md",
	"xxs xs sm md lg"
);

//Доступ
$arResult["HAVE_ACCESS"] = checkPayRadar();

   if(!empty($_GET["from_model_portfolio"]) && $_GET["from_model_portfolio"]==true){
	  $arResult["HAVE_ACCESS"] = true;
   }

$arResult["CLOSED_NO_ACCESS_PROPS"] = array(
	"Target",
	"Просад",
    "Методика расчета дивидендов",
    "P/E",
    "P/Equity",
    "PEG",
);

/*global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
		 echo "<pre  style='color:black; font-size:11px;'>";
          print_r($arResult);
          echo "</pre>";
      }*/




//акция
$arFilter = Array("IBLOCK_ID"=>32, "CODE"=>$arResult["CODE"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "ID", "DETAIL_PAGE_URL"));
while($item = $res->GetNextElement())
{
	$arResult["ACTION"] = $item->GetFields();
	$arResult["ACTION"]["PROPS"] = $item->GetProperties();
}

//Кейсы
$arResult["CASE"] = array();
$arFilter = Array("IBLOCK_ID"=>48, "ACTIVE"=>"Y", "PROPERTY_LINKED_ACTIONS"=>$arResult["ACTION"]["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($item = $res->GetNextElement())
{
	$arResult["CASE"][] = $item->GetFields();
}


//компания
$arFilter = Array("IBLOCK_ID"=>26, "ID"=>$arResult["ACTION"]["PROPS"]["EMITENT_ID"]["VALUE"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while($item = $res->GetNext())
{
	$arResult["COMPANY"] = $item;
	
	$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$item["NAME"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL","PROPERTY_CAPITAL_KOEF"));
	if($item = $res->GetNext())
	{
		$arResult["COMPANY"]["DETAIL_PAGE_URL"] = $item["DETAIL_PAGE_URL"];
		$arResult["COMPANY"]["CAPITAL_KOEF"] = $item["PROPERTY_CAPITAL_KOEF_VALUE"];
	}
}



//список данных
if($arResult["ACTION"]["PROPS"]){

	$res = new Actions();
	$actionItem = $res->getItem($arResult["CODE"]);
	
        //Вытаскиваем отраслевые мультипликаторы за текущий период на страницу акции
        $CIndustriesRus = new CIndustriesRus();
        $propSector = $arResult["ACTION"]["PROPS"]["PROP_SEKTOR"]["VALUE"];
        $arIndustryCurrentPeriod = $CIndustriesRus->arIndustryCurrentPeriods[$propSector];
         unset($CIndustriesRus);
    
/*    global $USER;
    $rsUser = CUser::GetByID($USER->GetID());
    $arUser = $rsUser->Fetch();
    if($arUser["LOGIN"]=="freud"){
        echo "<pre  style='color:black; font-size:11px;'>";
        print_r($arIndustryCurrentPeriod);
        echo "</pre>";
    }*/
        
        
    

	foreach(array("ISIN", "PROP_TIP_AKTSII", "PROP_SEKTOR", "LOTSIZE", "ISSUESIZE", "FREEFLOAT",
	"PROP_SHORT", "PROP_KREDIT", "IN_INDEX", "PRICE", "ISSUECAPITALIZATION", "CAP_X_FREEFLOAT",
	"PROP_TARGET", "PROP_PROSAD", "PROP_DIVIDENDY_2018", 'PROP_DIVIDEND_DESCRIPTION',
	"PE", "PB", "PEQUITY", "P/Sale", "PEG", "ACTION_DYNAMIC", "PROP_EXPORT_SHARE", "BETTA", "PRC_OF_USERS") as $propCode){

		if($propCode == "PROP_EXPORT_SHARE"){
			if(floatval($arResult["ACTION"]["PROPS"][$propCode]["VALUE"])<=0){
				continue;
			} else {
			  $arResult["ACTION"]["PROPS"][$propCode]["VALUE"] = $arResult["ACTION"]["PROPS"][$propCode]["VALUE"]."%";
			}
		}

		if($propCode == "PROP_SHORT"){
			$val = "невозможны";
			if($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]){
				$val = "возможны";
			}
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Операции Short",
				"VALUE" => $val,
			);
		} elseif($propCode == "PROP_KREDIT"){
			$val = "невозможны";
			if($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]){
				$val = "возможны";
			}
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Операции с кредитным плечом",
				"VALUE" => $val,
			);
		} elseif($propCode == "PRC_OF_USERS"){
			if(floatval($arResult["ACTION"]["PROPS"][$propCode]["VALUE"])>0){
				$val = "акция есть у ".$arResult["ACTION"]["PROPS"][$propCode]["VALUE"]."% пользователей";
			} else {
			  	$val = "эта акция встречается менее чем у 1% пользователей радар";
			}
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Рейтинг радара",
				"VALUE" => $val,
			);
		} elseif($propCode == "IN_INDEX"){
			$val = array();
			
			if($arResult["ACTION"]["PROPS"]["PROP_MSCI"]["VALUE"]){
				$val[] = "MSCI";
			}
			if($arResult["ACTION"]["PROPS"]["PROP_MMVB"]["VALUE"]){
				$val[] = "ММВБ";
			}
			
			if($val){
				$arResult["LIST_VALUES"][] = array(
					"NAME" => "Бумага состоит в индексах",
					"VALUE" => implode(", ", $val),
				);
			}
		} elseif($propCode == "ISSUECAPITALIZATION"){
			if(!$actionItem["PROPS"][$propCode]){
				continue;
			}
			$t = array(
				"NAME" => "Капитализация",
				"VALUE" => number_format(round(($actionItem["PROPS"][$propCode]/1000000), 2), 2, '.', ' ') ." млн.р.",
			);
			if($actionItem["PROPS"][$propCode."_DOL"]){
				$t["VALUE"] .= " (".round($actionItem["PROPS"][$propCode."_DOL"]/1000000, 2)." млн. долл.)";
			}
			$arResult["LIST_VALUES"][] = $t;
		} elseif($propCode == "PRICE") {
            if (!$actionItem["PROPS"]["LASTPRICE"]) {
                continue;
            }
				$priceDate = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTION_PRICES_DATE");
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Цена акции".(!empty($priceDate)?' на сегодня ('.$priceDate.')':''),
					 "CLASS" => "green",
                "VALUE" => $actionItem["PROPS"]["LASTPRICE"] . " руб.",
            );

        } elseif($propCode == "PROP_DIVIDEND_DESCRIPTION" && $actionItem["PROPS"][$propCode]){
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Методика расчета дивидендов",
                "VALUE" => $actionItem["PROPS"][$propCode],
            );
		} elseif($propCode == "PE"){
            if(!$actionItem["DYNAM"]["PE"]){
                continue;
            }
            $arTmpValue = array(
              "NAME" => "P/E",
              "VALUE" => $actionItem["DYNAM"]["PE"],
            );
            
            //Добавляем отраслевое значение, если есть
            if(array_key_exists($arTmpValue["NAME"],$arIndustryCurrentPeriod) && !empty($arIndustryCurrentPeriod[$arTmpValue["NAME"]])){
                $arTmpValue["VALUE"] .= ' (средний P/E отрасли = '.round($arIndustryCurrentPeriod[$arTmpValue["NAME"]],2).')';
            }
            $arResult["LIST_VALUES"][] = $arTmpValue;
            
        } elseif($propCode == "PB"){
            if(!$actionItem["DYNAM"]["PB"]){
                continue;
            }
            $arTmpValue = array(
              "NAME" => "P/B",
              "VALUE" => $actionItem["DYNAM"]["PB"],
            );
            
            //Добавляем отраслевое значение, если есть
            if(array_key_exists($arTmpValue["NAME"],$arIndustryCurrentPeriod) && !empty($arIndustryCurrentPeriod[$arTmpValue["NAME"]])){
                $arTmpValue["VALUE"] .= ' (средний P/B отрасли = '.round($arIndustryCurrentPeriod[$arTmpValue["NAME"]],2).')';
            }
            $arResult["LIST_VALUES"][] = $arTmpValue;
            
        } elseif($propCode == "PEQUITY"){
            if(!$actionItem["DYNAM"]["P/Equity"]){
                continue;
            }
            
            $arTmpValue = array(
                "NAME" => "P/Equity",
                "VALUE" => $actionItem["DYNAM"]["P/Equity"],
            );
            
            //Добавляем отраслевое значение, если есть
            if(array_key_exists($arTmpValue["NAME"],$arIndustryCurrentPeriod) && !empty($arIndustryCurrentPeriod[$arTmpValue["NAME"]])){
                $arTmpValue["VALUE"] .= ' (средний P/Equity отрасли = '.round($arIndustryCurrentPeriod[$arTmpValue["NAME"]],2).')';
            }
            $arResult["LIST_VALUES"][] = $arTmpValue;
        } elseif($propCode == "P/Sale"){
            if(!$actionItem["DYNAM"]["P/Sale"]){
                continue;
            }
            
            $arTmpValue = array(
              "NAME" => "P/Sale",
              "VALUE" => $actionItem["DYNAM"]["P/Sale"],
            );
            
            //Добавляем отраслевое значение, если есть
            if(array_key_exists($arTmpValue["NAME"],$arIndustryCurrentPeriod) && !empty($arIndustryCurrentPeriod[$arTmpValue["NAME"]])){
                $arTmpValue["VALUE"] .= ' (средний P/Sale отрасли = '.round($arIndustryCurrentPeriod[$arTmpValue["NAME"]],2).')';
            }
            $arResult["LIST_VALUES"][] = $arTmpValue;
        }  elseif($propCode == "PEG"){
            if(!$actionItem["DYNAM"]["PEG"]){
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "PEG",
                "VALUE" => $actionItem["DYNAM"]["PEG"],
            );
        } elseif ($propCode == "BETTA") {
            if (!$actionItem["PROPS"]["BETTA"]) {
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Бета",
                "VALUE" => round($actionItem["PROPS"]["BETTA"], 2),
            );
        } elseif($propCode == "ACTION_DYNAMIC"){
            $valueArr = [];
            $color = '';
            if($actionItem["DYNAM"]["MONTH_INCREASE"]){
                $valueArr[] = " прирост за  месяц = <span style='color:" . ($actionItem['DYNAM']['MONTH_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["MONTH_INCREASE"] . "%</span>";
            }
            if($actionItem["DYNAM"]["YEAR_INCREASE"]){
                $valueArr[] = " прирост за  год = <span style='color:" . ($actionItem['DYNAM']['YEAR_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["YEAR_INCREASE"] . "%</span>";
            }
            if($actionItem["DYNAM"]["THREE_YEAR_INCREASE"]){
                $valueArr[] = " прирост за  три года = <span style='color:" . ($actionItem['DYNAM']['THREE_YEAR_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["THREE_YEAR_INCREASE"] . "%</span>";
            }

            if ($valueArr) {
                $arResult["LIST_VALUES"][] = array(
                    "NAME" => "Динамика акции",
                    "VALUE" => $valueArr,
                );
            }
        } elseif($propCode == "FREEFLOAT"){
			 if(array_key_exists('Доля в индексах',$actionItem["DYNAM"]) && count($actionItem["DYNAM"]["Доля в индексах"])>0){
				$arindexPart = reset($actionItem["DYNAM"]["Доля в индексах"]);
                $arResult["LIST_VALUES"][] = array(
                    "NAME" => "Фрифлоат",
                    "VALUE" => $arindexPart["FREEFLOAT"]."%",
                );

			 }

        } elseif($propCode == "CAP_X_FREEFLOAT"){
				if(floatval($arResult["ACTION"]["PROPS"][$propCode]["VALUE"])>0){
				$arResult["LIST_VALUES"][] =  array(
				"NAME" => "Капитализация фрифлоат",
				"VALUE" => number_format(round(($actionItem["PROPS"][$propCode]/1000000), 2), 2, '.', ' ') ." млн.р.",
			);
				}

        	} else {
			if($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]){
				$tmp = array(
					"NAME" => $arResult["ACTION"]["PROPS"][$propCode]["NAME"],
					"VALUE" => $arResult["ACTION"]["PROPS"][$propCode]["VALUE"],
				);
				
				
				
				if(is_array($tmp["VALUE"])){
					$tmp["VALUE"] = implode(", ", $tmp["VALUE"]);
				}
				if($propCode=="PROP_TARGET" && $arResult["ACTION"]["PROPS"]["PROP_DATA_TARGETA"]["VALUE"] && isset($actionItem["DYNAM"]["Таргет"])){
					$tmp["VALUE"] = $actionItem["DYNAM"]["Таргет"]."% (".$tmp["VALUE"]." руб. от ".$arResult["ACTION"]["PROPS"]["PROP_DATA_TARGETA"]["VALUE"].")";
				}
				if($propCode=="PROP_PROSAD" && $arResult["ACTION"]["PROPS"]["PROP_DATA_PROSADA"]["VALUE"]  && isset($actionItem["DYNAM"]["Просад"])){
					$tmp["VALUE"] = $actionItem["DYNAM"]["Просад"]."% (".$tmp["VALUE"]." руб. от ".$arResult["ACTION"]["PROPS"]["PROP_DATA_PROSADA"]["VALUE"].")";
				}
				if($propCode=="PROP_DIVIDENDY_2018"){
					$currency = 'руб.';
					if(!empty($arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"]) && strpos($arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"],"$")!==false){
					  $currency = 'долл.';
					 }

					$tmp["VALUE"] .= " ".$currency." или <b>".$actionItem["DYNAM"]["Дивиденды %"]."%</b>";
					if(!empty($arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"]) ){
						$tmp["VALUE"] .= " ".$arResult["ACTION"]["PROPS"]["PROP_ISTOCHNIK"]["VALUE"];
					}


				}
				
				$arResult["LIST_VALUES"][] = $tmp;
			}
		}
	}
}



//Дивиденды
$obCache = new CPHPCache();

$cacheLifetime = mktime(0,0,0,date("m"),date("d")+1,date("Y"))-strtotime("now"); 
//$cacheLifetime = 0;
$cacheID = 'dividends'.$arResult["ID"]; 
$cachePath = '/action_dividends/';

if($arResult["PROPERTIES"]["DIVIDENDS"]["~VALUE"]["TEXT"]){
	
	/*if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
	{
		$arResult["DIVIDENDS"] = $obCache->GetVars();
	} elseif($obCache->StartDataCache()) {
		
		$tmp = unserialize($arResult["PROPERTIES"]["DIVIDENDS"]["~VALUE"]["TEXT"]);
		$boardIDs = array("TQBR", "TQDE");

		foreach($tmp as $date=>$value){
			$dt = new DateTime(trim($date));
			if($dt->getTimestamp()<$APPLICATION->minGraphDate){
				continue;
			}

			$url = "http://iss.moex.com/iss/history/engines/stock/markets/shares/securities/".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"]."/candleborders.json?from=".$dt->format("Y-m-d")."&till=".$dt->format("Y-m-d");
			$chartData = ConnectMoex($url);

			foreach ($chartData["history"]["data"] as $item) {
				if(!in_array($item[3], $boardIDs)){
					continue;
				}
				if (!$item[49] && !$item[21]) {
					continue;
				}
				$price = $item[49];
				if(!$price){
					$price = $item[21];
				}

				$tmp2 = array(
					"Дата закрытия реестра" => $dt->format("d.m.Y"),
					"Дивиденды" => $value,
					"Цена на дату закрытия" => $price,
				);
				if (strpos($value, "$") !== false) {
					$t = trim(str_replace("$", "", $tmp2["Дивиденды"]));
					$roubles = round($t * getCBPrice("USD", $dt->format("d/m/Y")), 2);

					$tmp2["Дивиденды"] .= " (" . $roubles . " руб.)";
					$tmp2["Дивиденды, в %"] = round($roubles / $tmp2["Цена на дату закрытия"] * 100, 2);
				} else {
					$tmp2["Дивиденды, в %"] = round($tmp2["Дивиденды"] / $tmp2["Цена на дату закрытия"] * 100, 2);
				}

				$dt->modify("-2 days");
				if ($dt->format("N") == 7) {
					$dt->modify("-2 days");
				} elseif ($dt->format("N") == 6) {
					$dt->modify("-1 days");
				}
				$tmp2["Последняя дата покупки акции под дивиденды"] = $dt->format("d.m.Y");


				$arResult["DIVIDENDS"][] = $tmp2;
			}
		}
		
		$obCache->EndDataCache($arResult["DIVIDENDS"]);
	}*/

    $hlblock   = HL\HighloadBlockTable::getById(21)->fetch();
    $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
    $entityClass = $entity->getDataClass();

    $res2 = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $arResult["CODE"],
        ),
        "select" => array(
            "UF_DATA"
        ),
    ));
    if($elem = $res2->fetch()) {
        $arResult["DIVIDENDS"] = json_decode($elem["UF_DATA"], true);

        $arResult["DIVIDENDS_FIELDS"] = array(
            "Дата закрытия реестра" => true,
				"Дата закрытия реестра (T-2)" => true,
            "Цена на дату закрытия" => true,
				"Дивиденды" => true,
            "Дивиденды, в %" => true,

        );
    }
}

//статьи
$obCache = new CPHPCache();

$cacheLifetime = 86400*365*10;
$cacheID = 'random_articles_for'.$arResult["ID"]; 
$cachePath = '/random_articles/';

if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
{
	$arResult["ARTICLES"] = $obCache->GetVars();
} elseif($obCache->StartDataCache()) {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
	$arFilter = Array("IBLOCK_ID"=>5, "!SECTION_ID"=>array(5, 17), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND"=>"asc"), $arFilter, false, array("nPageSize"=>3), $arSelect);
	while ($ob = $res->GetNext()){
		$arResult["ARTICLES"][] = $ob;
	}

   $obCache->EndDataCache($arResult["ARTICLES"]);
}

//график
//global $USER;
//if($USER->IsAdmin()) {
    $graph = new MoexGraph();
    $arResult["CHARTS_DATA"] = $graph->getForAction($arResult["ACTION"]["PROPS"]["SECID"]["VALUE"]);

	 //Готовим данные для линий таргета и просада на графике котировок
	 $arChartItems = array();
	 $ChartYMAx = 0;
	 $ChartYMin = 0;
	 //Вычисляем максимальное значение для шкалы Y по данным графика и далее с учетом таргета и просада
	 if($arResult["CHARTS_DATA"]["items"]){
	  $arChartItems = json_decode($arResult["CHARTS_DATA"]["items"]);
	  foreach($arChartItems as $chartItem){
			$tmpItem = $chartItem;
			//т.к. в элементе графика первой идет дата в виде строки, то определяем максимальную цену исключая дату
			unset($tmpItem[0]);
			$imax = max($tmpItem);


   	if($imax>$ChartYMAx){
	  		$ChartYMAx = $imax;
	  	}

	  }

	 };
	 $target = floatval($arResult["ACTION"]["PROPS"]["PROP_TARGET"]["VALUE"]);
	 if($target>0){
	 if($target>$ChartYMAx){
	  $ChartYMAx = $target;
	 }
	 $targetPrc = $actionItem["DYNAM"]["Таргет"];
	 $arResult["CHARTS_DATA"]["target"] = array(array(str_replace("-","/",$arResult["CHARTS_DATA"]["min"]).' 00:00:00', $target, $targetPrc), array(str_replace("-","/",$arResult["CHARTS_DATA"]["max"]).' 00:00:00', $target, $targetPrc));
	 $arResult["CHARTS_DATA"]["target"] = json_encode($arResult["CHARTS_DATA"]["target"]);
		}


	 $drawdown = $arResult["ACTION"]["PROPS"]["PROP_PROSAD"]["VALUE"];
	 if($target>$ChartYMAx){
	  $ChartYMAx = $drawdown;
	 }
	 $drawdownPrc = $actionItem["DYNAM"]["Просад"];
	 $arResult["CHARTS_DATA"]["drawdown"] = array(array(str_replace("-","/",$arResult["CHARTS_DATA"]["min"]).' 00:00:00', $drawdown, $drawdownPrc), array(str_replace("-","/",$arResult["CHARTS_DATA"]["max"]).' 00:00:00', $drawdown, $drawdownPrc));
	 $arResult["CHARTS_DATA"]["drawdown"] = json_encode($arResult["CHARTS_DATA"]["drawdown"]);
	 $arResult["CHARTS_DATA"]["maxY"] = $ChartYMAx+($ChartYMAx*0.1);

/*} else {
    $hlblock = HL\HighloadBlockTable::getById(20)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityClass = $entity->getDataClass();

    $res2 = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $arResult["CODE"],
        ),
        "select" => array(
            "UF_DATA"
        ),
    ));
    if ($elem = $res2->fetch()) {
        $arResult["CHARTS_DATA"] = json_decode($elem["UF_DATA"], true);
        $arResult["CHARTS_DATA"]["real_from"] = new DateTime($arResult["CHARTS_DATA"]["real_from"]["date"]);
        $arResult["CHARTS_DATA"]["from"] = new DateTime($arResult["CHARTS_DATA"]["from"]["date"]);
    }
}*/



//Текст акции
if($arResult["ACTION"]){

	try { //Обход ошибки определения начала обращения при отсутствии котировок
		if(isset($arResult["CHARTS_DATA"]["real_from"]) && is_object($arResult["CHARTS_DATA"]["real_from"])){
		 $dateStart = " находится в обращении с ".FormatDate("j F Y", $arResult["CHARTS_DATA"]["real_from"]->getTimestamp())."г.";
		 } else {
		 	 $dateStart = "";
		 }
   } catch (Exception $e) {
       $dateStart = "";
   } finally {

   }
	  $type = !empty($arResult["ACTION"]["PROPS"]["PROP_TIP_AKTSII"]["VALUE"])?$arResult["ACTION"]["PROPS"]["PROP_TIP_AKTSII"]["VALUE"].' ':' ';
	if(!$arResult["ACTION"]["PROPS"]["HIDEN"]["VALUE"]){
		 $arResult["ACTION_TEXT"] = $type."акция ".$arResult["NAME"]." (тикер акции - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].")";
		if($arResult["CHARTS_DATA"]){
		  $arResult["ACTION_TEXT"] .= $dateStart;
		}
		 $arResult["ACTION_TEXT"] .= " Эмитентом акции является <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
	} else {
		$arResult["ACTION_TEXT"] = "Акция ".$arResult["NAME"]." (тикер акции - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].") больше не обращается на московской бирже. Эмитент - <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>."; 
	}
	
	$APPLICATION->SetPageProperty("description", $arResult["ACTION_TEXT"]);
}

//Ключевики
$arKeywords = array();
if($arResult["LIST_VALUES"]){
	$arKeywords[] = "Параметры акции ".$arResult["NAME"];
}
if($arResult["CHARTS_DATA"]["items"]){
	$arKeywords[] = "График акции ".$arResult["NAME"];
}
if($arResult["DIVIDENDS"]){
	$arKeywords[] = "Дивиденды по акции ".$arResult["NAME"];
}

if($arKeywords){
	$APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}