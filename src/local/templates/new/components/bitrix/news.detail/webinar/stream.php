<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if($_REQUEST["checkBannerActive"]){
    $APPLICATION->RestartBuffer();
    echo $arResult["PROPERTIES"]["BANNER_SHOW"]["VALUE"];
    exit();
}
if($USER->IsAuthorized()){
    $adder = new CrmAdder();
    $adder->addWebinarDate($arResult["NAME"], $arResult["PROPERTIES"]["WEB_DATE_NAME"]["VALUE"]);
}?>


    <div class="stream_video">
        <iframe width="560" height="315" src="<?=$arResult["PROPERTIES"]["YOUTUBE"]["VALUE"]?>" frameborder="0" allowfullscreen></iframe>
    </div>

    <?if($arResult["PROPERTIES"]["BANNER_IMG"]["VALUE"]):?>
        <div class="simple_offer_outer text-center" style="background-image: url(<?=CFile::GetPath($arResult["PROPERTIES"]["BANNER_IMG"]["VALUE"])?>);<?if(!$arResult["PROPERTIES"]["BANNER_SHOW"]["VALUE"]):?>display:none<?endif?>">
            <p class="simple_offer_title big"><?=$arResult["PROPERTIES"]["BANNER_TEXT"]["~VALUE"]?></p>
            <a href="<?=$arResult["PROPERTIES"]["BANNER_BTN_LINK"]["VALUE"]?>" class="button big"><?=$arResult["PROPERTIES"]["BANNER_BTN_TEXT"]["VALUE"]?></a>
        </div>
        <?if(!$arResult["PROPERTIES"]["BANNER_SHOW"]["VALUE"]):?>
            <script>
                $(function () {
                    var interVal;
                    function checkBannerActive() {
                        $.get("", {"checkBannerActive":true}, function (data) {
                            if(data){
                                $(".simple_offer_outer").show();
                                clearInterval(interVal);
                            }
                        });
                    }
                    interVal = setInterval(checkBannerActive, 30000);
                });
            </script>
        <?endif;?>
    <?endif?>

    <?
    $arComments = [];
    if($arResult["PROPERTIES"]["CACKLE"]["VALUE"]){
        $arComments[] = "cackle";
    }
    if($arResult["PROPERTIES"]["VK"]["VALUE"]){
        $arComments[] = "vk";
    }
    ?>
    <?if($arComments):?>
        <div class="comment_outer">
            <p class="title">Комментарии</p>
            <?if(count($arComments)==2):?>
                <div class="row">
                    <div class="col col-lg-6">
                        <div id="mc-container">
                        </div>
                        <script type="text/javascript">
                            cackle_widget = window.cackle_widget || [];
                            cackle_widget.push({widget: 'Comment', id: 35690});
                            (function() {
                                var mc = document.createElement('script');
                                mc.type = 'text/javascript';
                                mc.async = true;
                                mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
                            })();
                        </script>
                        <a id="mc-link" href="http://cackle.me">Комментарии для сайта <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a>

                    </div>

                    <div class="col col-lg-6">
                        <div id="vk_comments"></div>
                        <script type="text/javascript">
                            VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"});
                        </script>
                    </div>
                </div>
            <?else:?>
                <div class="row">
                    <div class="col col-sm-12">
                        <?if($arComments[0]=="cackle"):?>
                            <div id="mc-container">
                            </div>
                            <script type="text/javascript">
                                cackle_widget = window.cackle_widget || [];
                                cackle_widget.push({widget: 'Comment', id: 35690});
                                (function() {
                                    var mc = document.createElement('script');
                                    mc.type = 'text/javascript';
                                    mc.async = true;
                                    mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
                                })();
                            </script> <a id="mc-link" href="http://cackle.me">Комментарии для сайта <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a>
                        <?else:?>
                            <div id="vk_comments"></div>
                            <script type="text/javascript">
                                VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"});
                            </script>
                        <?endif;?>
                    </div>
                </div>
            <?endif;?>
        </div>
    <?endif;?>

    <p>
        <!--                            Для просмотра страницы <a href="#" data-toggle="modal" data-target="#popup_login">необходимо авторизоваться</a>-->
        Для просмотра страницы <a href="#" data-toggle="modal" data-target="#popup_login">необходимо авторизоваться</a>
    </p>

