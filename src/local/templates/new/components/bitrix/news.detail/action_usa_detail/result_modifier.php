<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Highloadblock as HL;

$arResult["POINTS"] = array(
    "",
    "xxs",
    "xxs xs",
    "xxs xs sm",
    "xxs xs sm md",
    "xxs xs sm md lg"
);

//Доступ
$arResult["HAVE_ACCESS"] = checkPayRadar();
$arResult["HAVE_USA_ACCESS"] = checkPayUSARadar();

   if(!empty($_GET["from_model_portfolio"]) && $_GET["from_model_portfolio"]==true){
	  $arResult["HAVE_ACCESS"] = true;
	  $arResult["HAVE_USA_ACCESS"] = true;
   }


$arResult["CLOSED_NO_ACCESS_PROPS"] = array(
    "Target",
    "Просад",
    "Методика расчета дивидендов",
    "P/E",
    "P/Equity",
    "PEG",
);

//акция
$arFilter = [
    "IBLOCK_ID" => 55,
    "CODE" => $arResult["CODE"]
];
$res = CIBlockElement::GetList([], $arFilter, false, false, ["NAME", "ID", "DETAIL_PAGE_URL"]);
while ($item = $res->GetNextElement()) {
    $arResult["ACTION"] = $item->GetFields();
    $arResult["ACTION"]["PROPS"] = $item->GetProperties();
}

//Кейсы
$arResult["CASE"] = [];
$arFilter = [
    "IBLOCK_ID" => 48,
    "ACTIVE" => "Y",
    "PROPERTY_LINKED_ACTIONS" => $arResult["ACTION"]["ID"]
];
$res = CIBlockElement::GetList([], $arFilter, false, false, ["NAME", "DETAIL_PAGE_URL"]);
while ($item = $res->GetNextElement()) {
    $arResult["CASE"][] = $item->GetFields();
}

//компания
if(intval($arResult["ACTION"]["PROPS"]["COMMON_EMITENT_ID"]["VALUE"])>0){
   $companyIblockId = 26;
   $companyPagesIblockId = 29;
   $emitentId = $arResult["ACTION"]["PROPS"]["COMMON_EMITENT_ID"]["VALUE"];
} else {
    $companyIblockId = 43;
    $companyPagesIblockId = 44;
    $emitentId = $arResult["ACTION"]["PROPS"]["EMITENT_ID"]["VALUE"];
}
 if($emitentId>0){
    $arFilter = [
      "IBLOCK_ID" => $companyIblockId,
      "ID"        => $emitentId
    ];
    $res = CIBlockElement::GetList([], $arFilter, false, false, ["NAME", "DETAIL_PAGE_URL"]);
    while ($item = $res->Fetch()) {
        $arResult["COMPANY"] = $item;

        $arFilter1 = array("IBLOCK_ID" => $companyPagesIblockId, "NAME" => $item["NAME"]);
        $res1 = CIBlockElement::GetList(array(), $arFilter1, false, false, array("DETAIL_PAGE_URL"));
        if ($item1 = $res1->GetNext()) {
            $arResult["COMPANY"]["DETAIL_PAGE_URL"] = $item1["DETAIL_PAGE_URL"];
        }
    }
} else {
  $arResult["COMPANY"] = array();
}

//список данных
if ($arResult["ACTION"]["PROPS"]) {
    $res = new ActionsUsa();
    $actionItem = $res->getItem($arResult["CODE"]);
    $currency = getCurrencySign($actionItem["PROPS"]['CURRENCY']);
	 $currency_cap = $currency;
	 $currency_cap =  getCurrencySign($actionItem["COMPANY"]["CURRENCY_REPORT"]);

    $currencyVal = getCBPrice($actionItem["PROPS"]['CURRENCY']);
    
    //Вытаскиваем отраслевые мультипликаторы за текущий период на страницу акции
    $CSectorsUsa = new SectorsUsa();
    $propSector = $arResult["ACTION"]["PROPS"]["SECTOR"]["VALUE"];
    $arSectorCurrentPeriod = $CSectorsUsa->arSectorsCurrentPeriods[$propSector];
    unset($CSectorsUsa);

    
/*    global $USER;
     $rsUser = CUser::GetByID($USER->GetID());
     $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
        echo "<pre  style='color:black; font-size:11px;'>";
          print_r($arSectorCurrentPeriod);
        echo "</pre>";
      }*/
    
/*	  $firephp = FirePHP::getInstance(true);
     $firephp->fb($actionItem,FirePHP::LOG);*/
	 $arResult["CURRENCY_VAL"] = $currencyVal;
    foreach (array("EMITENT_ID", "SECID", "CURRENCY", "ISIN", "SPECIES", "TYPE", "PRICE", "VOLUME", "TOP", "TARGET", "DRAWDOWN", "SECTOR",
                 "INDUSTRY", "SP500", "COUNTRY", "SECURITIES_QUANTITY", "ISSUECAPITALIZATION", "DIVIDENDS",
                 "PE", "PB", "PEQUITY", "P/Sale", "-UNDERSTIMATION",  "PEG", "ACTION_DYNAMIC", "PROP_EXPORT_SHARE", "BETTA", "PRC_OF_USERS") as $propCode) {

        if ($propCode == "EMITENT_ID") {
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Эмитент",
                "VALUE" => $arResult["COMPANY"]["NAME"],
            );
        } elseif($propCode == "PRC_OF_USERS"){
			if(floatval($arResult["ACTION"]["PROPS"][$propCode]["VALUE"])>0){
				$val = "акция есть у ".$arResult["ACTION"]["PROPS"][$propCode]["VALUE"]."% пользователей";
			} else {
			  	$val = "эта акция встречается менее чем у 1% пользователей радар";
			}
			$arResult["LIST_VALUES"][] = array(
				"NAME" => "Рейтинг радара",
				"VALUE" => $val,
			);
		  }  elseif ($propCode == "TOP" && !empty($arResult["ACTION"]["PROPS"][$propCode]["VALUE"])) {
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Группировка",
                "VALUE" => $arResult["ACTION"]["PROPS"][$propCode]["VALUE"],
            );
        } elseif ($propCode == "UNDERSTIMATION") {
            $val = "нет";
            if ($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]) {
                $val = $arResult["ACTION"]["PROPS"][$propCode]["VALUE"];
            }
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Недооценка",
                "VALUE" => $val,
            );
        } elseif ($propCode == "VOLUME") {
            /*			$val = "нет";
                        if($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]){
                            $val = $arResult["ACTION"]["PROPS"][$propCode]["VALUE"];
                        }
                        $arResult["LIST_VALUES"][] = array(
                            "NAME" => "Недооценка",
                            "VALUE" => $val,
                        );*/
        } elseif ($propCode == "INDUSTRY") {
            if ($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]) {
                $val = $arResult["ACTION"]["PROPS"][$propCode]["VALUE"];
            }
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Отрасль",
                "VALUE" => $val,
            );
        } elseif ($propCode == "SHORT") {
            $val = "невозможны";
            if ($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]) {
                $val = "возможны";
            }
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Операции Short",
                "VALUE" => $val,
            );
        } elseif ($propCode == "SP500") {
            if ($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]) {
                //$val = "SP500";
                $arResult["LIST_VALUES"][] = array(
                    "NAME" => "Входит в индексы",
                    "VALUE" => $arResult["ACTION"]["PROPS"][$propCode]["VALUE"],
                );
            }
        } elseif ($propCode == "CREDIT") {
            $val = "невозможны";
            if ($arResult["ACTION"]["PROPS"][$propCode]["VALUE"]) {
                $val = "возможны";
            }
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Операции с кредитным плечом",
                "VALUE" => $val,
            );
        } elseif ($propCode == "ISSUECAPITALIZATION") {
            if (!$actionItem["PROPS"][$propCode]) {
                continue;
            }

            $capVal = $actionItem["PROPS"][$propCode] / 1000000;
            $roundSigns = 2;
            if ($capVal - intval($capVal) == 0) {
                $roundSigns = 0;
            }

            $t = array(
                "NAME" => "Капитализация",
                "VALUE" => number_format(round(($capVal), $roundSigns), $roundSigns, '.', ' ') . " млн." . $currency,
            );
				if($actionItem["PROPS"][$propCode."_CURRENCY"]){

					$t["VALUE"] .= " (".round($actionItem["PROPS"][$propCode."_CURRENCY"]/1000000, 2)." млн. ".convertOblCurrency($actionItem["COMPANY"]["CURRENCY_REPORT"]).")";
				}
            $arResult["LIST_VALUES"][] = $t;
        } elseif ($propCode == "PRICE") {
            if (!$actionItem["PROPS"]["LASTPRICE"]) {
                continue;
            }
            //$priceDate = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTION_PRICES_USA_DATE");
            $arResult["LIST_VALUES"][] = array(
                //"NAME" => "Цена акции".(!empty($priceDate)?' на сегодня ('.$priceDate.')':''),
                "NAME" => "Цена акции",
                "CLASS" => "green",
                "VALUE" => $actionItem["PROPS"]["LASTPRICE"] . $currency . " (" . round($actionItem["PROPS"]["LASTPRICE"] * $currencyVal, 2) . "р.)",
            );

        } elseif ($propCode == "PE") {
            if (!$actionItem["DYNAM"]["PE"]) {
                continue;
            }
            $tmpVal = $actionItem["DYNAM"]["PE"];
            //Получим средний PE по отрасли
            if (array_key_exists("P/E", $arSectorCurrentPeriod) && !empty($arSectorCurrentPeriod["P/E"])) {
                $tmpVal = $tmpVal . ' (средний P/E сектора = ' . round($arSectorCurrentPeriod["P/E"], 2) . ')';
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "P/E",
                "VALUE" => $tmpVal,
            );
        } elseif($propCode == "PB"){
            if(!$actionItem["DYNAM"]["PB"]){
                continue;
            }
            $tmpVal = $actionItem["DYNAM"]["PB"];
    
            //Добавляем отраслевое значение, если есть
            if(array_key_exists("P/B",$arSectorCurrentPeriod) && !empty($arSectorCurrentPeriod["P/B"])){
                $tmpVal = $tmpVal . ' (средний P/B сектора = '.round($arSectorCurrentPeriod["P/B"],2).')';
            }
            $arResult["LIST_VALUES"][] = array(
              "NAME" => "P/B",
              "VALUE" => $tmpVal,
            );;
    
        } elseif($propCode == "P/Sale"){
            if(!$actionItem["DYNAM"]["P/Sale"]){
                continue;
            }
            $tmpVal = $actionItem["DYNAM"]["P/Sale"];
    
            //Добавляем отраслевое значение, если есть
            if(array_key_exists("P/Sale",$arSectorCurrentPeriod) && !empty($arSectorCurrentPeriod["P/Sale"])){
                $tmpVal = $tmpVal . ' (средний P/Sale сектора = '.round($arSectorCurrentPeriod["P/Sale"],2).')';
            }
            $arResult["LIST_VALUES"][] = array(
              "NAME" => "P/Sale",
              "VALUE" => $tmpVal,
            );;
    
        } elseif ($propCode == "PEQUITY") {
            if (!$actionItem["DYNAM"]["P/Equity"]) {
                continue;
            }

            $tmpVal = $actionItem["DYNAM"]["P/Equity"];
            if (array_key_exists("P/Equity", $arSectorCurrentPeriod) && !empty($arSectorCurrentPeriod["P/Equity"])) {
                $tmpVal = $tmpVal . ' (средний P/Equity сектора = ' . round($arSectorCurrentPeriod["P/Equity"], 2) . ')';
            }
            $arResult["LIST_VALUES"][] = array(
                "NAME" => "P/Equity",
                "VALUE" => $tmpVal,
            );
        } elseif ($propCode == "PEG") {
            if (!$actionItem["DYNAM"]["PEG"]) {
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "PEG",
                "VALUE" => $actionItem["DYNAM"]["PEG"],
            );
        } elseif ($propCode == "BETTA") {
            if (!$actionItem["PROPS"]["BETTA"]) {
                continue;
            }

            $arResult["LIST_VALUES"][] = array(
                "NAME" => "Бета",
                "VALUE" => round($actionItem["PROPS"]["BETTA"], 2),
            );
        } elseif ($propCode == "ACTION_DYNAMIC") {
            $valueArr = [];
            $color = '';
            if ($actionItem["DYNAM"]["MONTH_INCREASE"] && $actionItem["DYNAM"]["MONTH_INCREASE"]!=INF) {
                $valueArr[] = " прирост за  месяц = <span style='color:" . ($actionItem['DYNAM']['MONTH_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["MONTH_INCREASE"] . "%</span>";
            }
            if ($actionItem["DYNAM"]["YEAR_INCREASE"] && $actionItem['DYNAM']['YEAR_INCREASE']!=INF) {
                $valueArr[] = " прирост за  год = <span style='color:" . ($actionItem['DYNAM']['YEAR_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["YEAR_INCREASE"] . "%</span>";
            }
            if ($actionItem["DYNAM"]["THREE_YEAR_INCREASE"] && $actionItem["DYNAM"]["THREE_YEAR_INCREASE"]!=INF) {
                $valueArr[] = " прирост за  три года = <span style='color:" . ($actionItem['DYNAM']['THREE_YEAR_INCREASE'] < 0 ? 'red' : 'green') . "'>" . $actionItem["DYNAM"]["THREE_YEAR_INCREASE"] . "%</span>";
            }

            if ($valueArr) {
                $arResult["LIST_VALUES"][] = array(
                    "NAME" => "Динамика акции",
                    "VALUE" => $valueArr,
                );
            }
        } else {


            if (strlen($arResult["ACTION"]["PROPS"][$propCode]["VALUE"])) {
                $tmp = array(
                    "NAME" => $arResult["ACTION"]["PROPS"][$propCode]["NAME"],
                    "VALUE" => $arResult["ACTION"]["PROPS"][$propCode]["VALUE"],
                );


                if (is_array($tmp["VALUE"])) {
                    $tmp["VALUE"] = implode(", ", $tmp["VALUE"]);
                }
                /*				if($propCode=="TARGET" && $arResult["ACTION"]["PROPS"]["TARGET_DATE"]["VALUE"] && isset($actionItem["DYNAM"]["Таргет"])){
                                    $tmp["VALUE"] = $actionItem["DYNAM"]["Таргет"]."% (".$tmp["VALUE"]." руб. от ".$arResult["ACTION"]["PROPS"]["TARGET_DATE"]["VALUE"].")";
                                }*/

                if ($propCode == "TARGET" && isset($actionItem["DYNAM"]["Таргет"])) {
                	  //Если штатный таргет равен 0 но динамический расчетный больше нуля - заменяем штатный динамическим
						  if(floatval($tmp["VALUE"]) <= 0 && floatval($actionItem["DYNAM"]["Таргет"])>0){
						  	 $tmp["VALUE"] = floatval($actionItem["DYNAM"]["Таргет"]);
						  }

                    if (floatval($tmp["VALUE"]) <= 0) {
                        $tmp["VALUE"] = 0;
                    } else {
                        $target_summ = $actionItem["PROPS"]["LASTPRICE"] * (1 + $actionItem["DYNAM"]["Таргет"] / 100);
                        $tmp["VALUE"] = $actionItem["DYNAM"]["Таргет"] . "% (" . round($target_summ, 2) . $currency . ', ' . round($target_summ * $currencyVal, 2) . " руб.)";
                    }

                }
                if ($propCode == "DRAWDOWN" && isset($actionItem["DYNAM"]["Просад"])) {
                    $dropdown_summ = $actionItem["PROPS"]["LASTPRICE"] * (1 - $actionItem["DYNAM"]["Просад"] / 100);
                    $tmp["VALUE"] = $actionItem["DYNAM"]["Просад"] . "% (" . $dropdown_summ . $currency . ", " . round($dropdown_summ * $currencyVal, 2) . " руб.)";
                }
                if ($propCode == "DIVIDENDS") {
                    if (isset($actionItem["DYNAM"]["Дивиденды %"]) && !empty($actionItem["DYNAM"]["Дивиденды %"])) {
								 $currencyDiv = getCurrencySign($actionItem["COMPANY"]["CURRENCY_DIVIDENDS"]);

						  		//Пересчитаем эквивалент дивов за год в валюту обращения
								$arRussianCurrency = array("RUB", "RUR", "SUR");
								$showEqDivSumm = false;
								if($actionItem["COMPANY"]["CURRENCY_DIVIDENDS"]!=$actionItem["COMPANY"]["CURRENCY_CIRCULATION"]){
									$divSumm = $tmp["VALUE"];
									$showEqDivSumm = true;
								  if(!in_array($actionItem["COMPANY"]["CURRENCY_CIRCULATION"], $arRussianCurrency)){
									  $divRubRate = getCBPrice($actionItem["COMPANY"]["CURRENCY_DIVIDENDS"]);
									  $circRubRate = getCBPrice($actionItem["COMPANY"]["CURRENCY_CIRCULATION"]);
									  $divSumm = $divSumm*$divRubRate;
									  $divSumm = $divSumm/$circRubRate;
								  } else {
									  $circRubRate = getCBPrice($actionItem["COMPANY"]["CURRENCY_CIRCULATION"]);
									  $divSumm = $divSumm*$circRubRate;
								  }
									$divSumm = round($divSumm, 3);
								}


                        $tmp["VALUE"] .= " " . $currencyDiv .($showEqDivSumm?' ('.$divSumm.' '.getCurrencySign($actionItem["COMPANY"]["CURRENCY_CIRCULATION"]).')':''). " или <b>" . $actionItem["DYNAM"]["Дивиденды %"] . "%</b>";
                        if (!empty($arResult["ACTION"]["PROPS"]["SOURCE"]["VALUE"])) {
                            $tmp["VALUE"] .= " " . $arResult["ACTION"]["PROPS"]["SOURCE"]["VALUE"];
                        }
                    } else {

                        $tmp["VALUE"] = "Не выплачивает";
                        $tmp["NAME"] = "Дивиденды за год";
                    }

                }

                $arResult["LIST_VALUES"][] = $tmp;
            }
        }

    }//foreach


}

//Дивиденды

if ($arResult["PROPERTIES"]["DIVIDENDS"]["~VALUE"]["TEXT"]) {
    $hlblock = HL\HighloadBlockTable::getById(32)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityClass = $entity->getDataClass();

    $res2 = $entityClass::getList(array(
        "filter" => array(
            "UF_ITEM" => $arResult["CODE"],
        ),
        "select" => array(
            "UF_DATA"
        ),
    ));
    if ($elem = $res2->fetch()) {
        $arResult["DIVIDENDS"] = json_decode($elem["UF_DATA"], true);

        $arResult["DIVIDENDS_FIELDS"] = array(
            "Дата закрытия реестра" => true,
            "Дата закрытия реестра (T-2)" => false,
            "Цена на дату закрытия" => true,
            "Дивиденды" => true,
            "Дивиденды, в %" => true,

        );
    }
}


//статьи
$obCache = new CPHPCache();

$cacheLifetime = 86400 * 365 * 10;
$cacheID = 'random_articles_for' . $arResult["ID"];
$cachePath = '/random_articles/';

if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
    $arResult["ARTICLES"] = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SHOW_COUNTER");
    $arFilter = Array("IBLOCK_ID" => 5, "!SECTION_ID" => array(5, 17), "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array("RAND" => "asc"), $arFilter, false, array("nPageSize" => 3), $arSelect);
    while ($ob = $res->GetNext()) {
        $arResult["ARTICLES"][] = $ob;
    }

    $obCache->EndDataCache($arResult["ARTICLES"]);
}

$graph = new SpbexGraph();
$arResult["CHARTS_DATA"] = $graph->getForAction($arResult["ACTION"]["PROPS"]["SECID"]["VALUE"]);



	 //Готовим данные для линий таргета и просада на графике котировок
	 $arChartItems = array();
	 $ChartYMAx = 0;
	 //Вычисляем максимальное значение для шкалы Y по данным графика и далее с учетом таргета и просада
	 if($arResult["CHARTS_DATA"]["items"]){
	  $arChartItems = json_decode($arResult["CHARTS_DATA"]["items"]);


	  foreach($arChartItems as $chartItem){
			$tmpItem = $chartItem;
			//т.к. в элементе графика первой идет дата в виде строки, то определяем максимальную цену исключая дату
			unset($tmpItem[0]);
			$imax = max($tmpItem);
			
  	if($imax>$ChartYMAx){
	  		$ChartYMAx = $imax;
	  	}
	  }
	 };

//	  $firephp = FirePHP::getInstance(true);
//	  $firephp->fb(array("ChartYMAx"=>$ChartYMAx),FirePHP::LOG);


	 $target = $actionItem["PROPS"]["LASTPRICE"] * (1 + $actionItem["DYNAM"]["Таргет"] / 100);
	 if($target>$ChartYMAx){
	  $ChartYMAx = $target;
	 }
	 if(floatval($actionItem["DYNAM"]["Таргет"])>0){
	 $targetPrc = $actionItem["DYNAM"]["Таргет"];
	 $arResult["CHARTS_DATA"]["target"] = array(array(str_replace("-","/",$arResult["CHARTS_DATA"]["min"]).' 00:00:00', $target, $targetPrc), array(str_replace("-","/",$arResult["CHARTS_DATA"]["max"]).' 00:00:00', $target, $targetPrc));
	 $arResult["CHARTS_DATA"]["target"] = json_encode($arResult["CHARTS_DATA"]["target"]);
	 }

	 $drawdown = $actionItem["PROPS"]["LASTPRICE"] * (1 - $actionItem["DYNAM"]["Просад"] / 100);
	 $arResult["CHARTS_DATA"]["drawdownSumm"] = $actionItem["DYNAM"]["Просад"]>0?$drawdown:0;
	 if($target>$ChartYMAx){
	  $ChartYMAx = $drawdown;
	 }
	 $drawdownPrc = $actionItem["DYNAM"]["Просад"];
	 $arResult["CHARTS_DATA"]["drawdown"] = array(array(str_replace("-","/",$arResult["CHARTS_DATA"]["min"]).' 00:00:00', $drawdown, $drawdownPrc), array(str_replace("-","/",$arResult["CHARTS_DATA"]["max"]).' 00:00:00', $drawdown, $drawdownPrc));
	 $arResult["CHARTS_DATA"]["drawdown"] = json_encode($arResult["CHARTS_DATA"]["drawdown"]);
	 $arResult["CHARTS_DATA"]["maxY"] = $ChartYMAx+($ChartYMAx*0.1);
//Текст акции
if ($arResult["ACTION"]) {

	try { //Обход ошибки определения начала обращения при отсутствии котировок
		if(isset($arResult["CHARTS_DATA"]["real_from"]) && is_object($arResult["CHARTS_DATA"]["real_from"])){
		 $dateStart = " находится в обращении на Санкт-Петербургской бирже с ".FormatDate("j F Y", $arResult["CHARTS_DATA"]["real_from"]->getTimestamp())."г.";
		 } else {
		 	 $dateStart = "";
		 }
   } catch (Exception $e) {
       $dateStart = "";
   } finally {

   }


    $arResult["ACTION_TEXT"] = "Обыкновенная Акция " . $arResult["NAME"] . " (тикер акции - " . $arResult["ACTION"]["PROPS"]["SECID"]["VALUE"] . ")";
    if($arResult["CHARTS_DATA"]){
        $arResult["ACTION_TEXT"] .= $dateStart;
    }
    
    $arResult["ACTION_TEXT"] .= " Эмитентом акции является <a href='" . $arResult["COMPANY"]["DETAIL_PAGE_URL"] . "'>" . $arResult["COMPANY"]["NAME"] . "</a>.";
    // $arResult["ACTION_TEXT"] = "Обыкновенная Акция ".$arResult["NAME"]." (тикер акции - ".$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"].") находится в обращении с ".(new DateTime($arResult["CHARTS_DATA"]["real_from"]->getDateTime()))->format("j F Y")." г. Эмитентом акции является <a href='".$arResult["COMPANY"]["DETAIL_PAGE_URL"]."'>".$arResult["COMPANY"]["NAME"]."</a>.";
    $APPLICATION->SetPageProperty("description", $arResult["ACTION_TEXT"]);
}

//Ключевики
$arKeywords = array();
if ($arResult["LIST_VALUES"]) {
    $arKeywords[] = "Параметры акции " . $arResult["NAME"];
}
if ($arResult["CHARTS_DATA"]["items"]) {
    $arKeywords[] = "График акции " . $arResult["NAME"];
}
if ($arResult["DIVIDENDS"]) {
    $arKeywords[] = "Дивиденды по акции " . $arResult["NAME"];
}

if ($arKeywords) {
    $APPLICATION->SetPageProperty("keywords", implode(", ", $arKeywords));
}