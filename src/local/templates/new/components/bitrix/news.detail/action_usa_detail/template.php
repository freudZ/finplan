<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="wrapper">
    <div class="container">
        <div class="main_content">
            <div class="main_content_inner">
                <? if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "extended_banner",
                            "EDIT_TEMPLATE" => "",
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_RECURSIVE" => "Y"
                        ),
                        false
                    ); ?>

                    <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/left_sidebar.php"), Array(), Array("MODE" => "html", "NAME" => "сайдбар")) ?>
                <? endif ?>

				<div class="title_container title_nav_outer">
					<h1><? $APPLICATION->ShowTitle(false); ?></h1>

                    <? if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
                        <div class="title_nav">
                            <ul>
                                <li><a href="/lk/obligations/company_usa/all/">Все эмитенты</a></li>
                                <li><a href="/lk/actions_usa/all/">Все акции</a></li>
                            </ul>
                        </div>
                    <? endif ?>
                </div>
                <? if (!$_GET['search_off']): ?>
                    <br>
                    <form class="innerpage_search_form" method="GET">
                        <div class="form_element">
                            <input type="text" value="<?= $_REQUEST["q"] ?>"
                                   placeholder="Что вы ищете? Введите название или тикер" name="q"
                                   id="autocomplete_actions_usa"/>
                            <button type="submit"><span class="icon icon-search"></span></button>
                        </div>
                    </form>
                <? endif ?>
                <!-- *****/КАРКАС***** -->

                <? if ($arResult["ACTION_TEXT"]): ?>
                    <p>
                        <?= $arResult["ACTION_TEXT"] ?>
                    </p>
                <? endif ?>
                <? if ($arResult["LIST_VALUES"]): ?>
				   <?$heading1 = "Параметры акции ".$arResult["NAME"];
					  if(!empty($arResult["PROPERTIES"]["SEO_SUBHEADER_1"]["VALUE"])){
						  $heading1 = $arResult["PROPERTIES"]["SEO_SUBHEADER_1"]["VALUE"];
					  }
					?>
					<h2><?=$heading1?>:</h2>
                    <ul currency_rate="<?= $arResult["CURRENCY_VAL"] ?>">
						<? $arLangNames = array(
														 "Эмитент"=>"EMITENT",
														 "ISIN-код бумаги"=>"ISIN",
														 "Код бумаги"=>"SECID",
														 "Валюта"=>"CURRENCY",
														 "Вид акций"=>"SHARE_VID",
														 "Тип акции"=>"SHARE_TYPE",
														 "Группировка"=>"GROUP",
														 "Сектор"=>"SHARE_SECTOR",
														 "Отрасль"=>"INDUSTRY",
														 "Количество акций в лоте"=>"INLOT_CNT",
														 "Операции Short"=>"SHORT_OPERATIONS",
														 "Операции с кредитным плечом"=>"LEVERAGE_TRANSACTIONS",
														 "Входит в индексы"=>"INDEXES",
														 "PRICE"=>"PRICE",
														 "Страна"=>"COUNTRY",
														 "Количество ценных бумаг в обращении"=>"ACTIVES_CNT",
														 "Недооценка"=>"UNDERESTIMATION",
														 "Капитализация"=>"CAPITALIZATION",
														 "Расчетный потенциал"=>"TARGET",
														 "Просад"=>"DRAWDOWN",
														 "Дивиденды за год"=>"DIVIDENDS_YEAR",
														 "Методика расчета дивидендов"=>"DIVIDENTS_METHOD_CALC",
														 "P/E"=>"PE",
														 "P/Equity"=>"P_EQUITY",
														 "PEG"=>"PEG",
														 "Динамика акции"=>"DYNAMICS",
														 "Бета"=>"BETTA",
						); ?>
                        <? foreach ($arResult["LIST_VALUES"] as $item): ?>
                            <? if ($item["NAME"] == "Капитализация" && !empty($arResult["ACTION"]["PROPS"]["HIDEN"]["VALUE"])) continue; ?>
                            <li>
                                <p <? if ($item["COLOR"]): ?> style="color:<?=$item["COLOR"]?>"<? endif ?> class="<? if ($item["CLASS"]): ?> <?=$item["CLASS"]?><? endif ?>">
                                    <? if (in_array($item["NAME"], $arResult["CLOSED_NO_ACCESS_PROPS"]) && !$arResult["HAVE_USA_ACCESS"]): ?>
                                        <?= $item["NAME"] ?>:<?if(array_key_exists($item["NAME"],$arLangNames)):?>&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="top" title="" data-original-title="<?=GetMessage("ACT_USA_PAGE_PARAM_".$arLangNames[$item["NAME"]])?>">i</span><?endif;?> Этот показатель доступен в платной подписке.
                                        <? if ($USER->IsAuthorized()): ?>
                                            <a class="green" href="#" data-toggle="modal" data-target="#buy_subscribe_popup">Выбрать
                                                вариант подписки</a>
                                        <? else: ?>
                                            <a class="green" href="#" data-toggle="modal" data-target="#popup_simple_auth">Получить демо-доступ.</a>
                                        <? endif ?>
                                    <? else: ?>
                                        <? if ($item["NAME"] === "Динамика акции"): ?>
                                            <?
                                            $actionDynamicValue = '';
                                            foreach ($item["VALUE"] as $period) {
                                                $actionDynamicValue .= $period . " ";
                                            }
                                            ?>
                                            <?= $item["NAME"] ?>: <?= $actionDynamicValue ?><?if(array_key_exists($item["NAME"],$arLangNames)):?>&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="top" title="" data-original-title="<?=GetMessage("ACT_USA_PAGE_PARAM_".$arLangNames[$item["NAME"]])?>">i</span><?endif;?>
                                        <? else: ?>
                                            <? if (!empty($arResult["ACTION"]["PROPS"]["HIDEN"]["VALUE"])) {
                                                switch ($item["NAME"]) {
                                                    case "P/E":
                                                    case "P/Equity":
                                                    case "PEG":
                                                        $item["VALUE"] = '-';
                                                        break;
                                                }

                                            } ?>
													 <?if(strpos($item["NAME"],"Цена акции")!==false):?>
										            <?=$item["NAME"]?>: <?=$item["VALUE"]?><?if(array_key_exists("PRICE",$arLangNames)):?>&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="top" title="" data-original-title="<?=GetMessage("ACT_USA_PAGE_PARAM_".$arLangNames["PRICE"])?>">i</span><?endif;?>
													 <?else:?>
											         <?=$item["NAME"]?>: <?=$item["VALUE"]?><?if(array_key_exists($item["NAME"],$arLangNames)):?>&nbsp;<span class="tooltip_btn p-tooltip-green" data-placement="top" title="" data-original-title="<?=GetMessage("ACT_USA_PAGE_PARAM_".$arLangNames[$item["NAME"]])?>">i</span><?endif;?>
											 		 <?endif?>
                                        <? endif ?>
                                    <? endif ?>
                                </p>
                            </li>
                        <? endforeach ?>
                    </ul>
                <? endif ?>
                <div class="article_inner">
                    <p><?= $arResult["PREVIEW_TEXT"] ?></p>
                </div>

				<div style="display:none;">
					             	   <!-- курс валют по ЦБ -->
				   <input type="text" id="usd_currency" value="<?=round(getCBPrice("USD"),2)?>" hidden />
				   <input type="text" id="eur_currency" value="<?=round(getCBPrice("EUR"),2)?>" hidden />
					<table class="smart_table">
					  <tr data-id="<?= $arResult["ACTION"]["PROPS"]["SECID"]["VALUE"] ?>" data-lotsize="1" data-currency="<?= strtolower($arResult["ACTION"]["PROPS"]["CURRENCY"]["VALUE"]) ?>" class="checked">
						  <td class="cshare">
							   <p class="elem_name">
								  <a href="<?= $arResult["DETAIL_PAGE_URL"] ?>" class="tooltip_btn" title="" data-original-title="<?= $arResult["ACTION"]["NAME"] ?>"></a>
							   </p>
								<p class="company_name line_green">
								  <a class="tooltip_btn" title="" href="<?= $arResult["COMPANY"]["DETAIL_PAGE_URL"] ?>" data-original-title="<?= $arResult["COMPANY"]["NAME"] ?>"></a>
								</p>
						  </td>
						  <td>
								<p class="elem_buy_price price_elem" data-price="<?= $arResult["ACTION"]["PROPS"]["LASTPRICE"]["VALUE"] ?>"></p>
								<p class="elem_cshare_price " data-price="<?= $arResult["ACTION"]["PROPS"]["LASTPRICE"]["VALUE"] ?>"></p>
						  </td>
						  <td>
							<input class="numberof" type="text" value="1" data-working_value="1">
						  </td>
						</tr>
					</table>
				</div>
                <? if (!empty($arResult["CASE"])): ?>

                    <h2>Кейсы по акции <?= $arResult["NAME"] ?>:</h2>
                    <ul>
                        <? foreach ($arResult["CASE"] as $kcase => $case): ?>
                            <li><a href="<?= $case["DETAIL_PAGE_URL"] ?>" target="_self"><?= $case["NAME"] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                <? endif; ?>

                <? if ($arResult["CHARTS_DATA"]["items"]): ?>
                    <hr/>
				<?$heading2 = "График акции ".$arResult["NAME"];
					  if(!empty($arResult["PROPERTIES"]["SEO_SUBHEADER_2"]["VALUE"])){
						  $heading2 = $arResult["PROPERTIES"]["SEO_SUBHEADER_2"]["VALUE"];
					  }
					?>
					<h2><?=$heading2?>:
                        <?//if($USER->IsAdmin()):?>
                            <a href="/tehanaliz/?actionList=<?=$arResult["ACTION"]["PROPS"]["SECID"]["VALUE"]."_USA"?>&templateId=<?=$APPLICATION->TA_template_GS_for_action_pages?>" class="dashed detail-page-ta-link" target="_blank">Перейти в теханализ</a>
                        <?//endif;?>
                    </h2>
                    <div class="article_bar_chart_outer">
                        <div class="article_bar_chart" id="chart2"
                             data-items='<?= $arResult["CHARTS_DATA"]["items"] ?>'
                             data-max="<?= $arResult["CHARTS_DATA"]["max"] ?>"
                             data-min="<?= $arResult["CHARTS_DATA"]["min"] ?>"
 									  data-target='<?=$arResult["CHARTS_DATA"]["target"]?>'
									  data-maxy='<?=$arResult["CHARTS_DATA"]["maxY"]?>'
									  data-drd-summ='<?=$arResult["CHARTS_DATA"]["drawdownSumm"]?>'
										<?if(floatval($arResult["CHARTS_DATA"]["drawdownSumm"])>0):?>
										data-drawdown='<?=$arResult["CHARTS_DATA"]["drawdown"]?>'
										<?endif;?>
										data-target-label='Расчетный потенциал'
                             data-tick="<?= $arResult["CHARTS_DATA"]["tick"] ?>"
                             data-currency="<?= !empty($arResult["ACTION"]["PROPS"]["CURRENCY"]["VALUE"])?strtolower($arResult["ACTION"]["PROPS"]["CURRENCY"]["VALUE"]):'usd' ?>"></div>
                    </div>
                <? endif ?>

                <? if ($arResult["DIVIDENDS"]): ?>
				<?$heading3 = "Дивиденды по акции ".$arResult["NAME"];
					  if(!empty($arResult["PROPERTIES"]["SEO_SUBHEADER_3"]["VALUE"])){
						  $heading3 = $arResult["PROPERTIES"]["SEO_SUBHEADER_3"]["VALUE"];
					  }
					?>
                    <div class="company_accordion_outer">
                        <div class="accordion custom_collapse">
                            <div class="custom_collapse_elem opened">
                                <div class="infinite">
                                    <div class="infinite_container">
                                        <div class="custom_collapse_elem_head">
                                            <h2 class="custom_collapse_elem_title collapse_btn" style="border-bottom:0"> <?=$heading3?> <span class="icon icon-arr_down"></span></h2>
                                        </div>
                                        <div class="custom_collapse_elem_body">
                                            <div class="custom_collapse_elem_body_inner">

                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="action_usa_custom_tabs_01_quarter">
                                                        <table class="transform_table">
                                                            <thead>
                                                            <tr>
                                                                <? $i = 0;
                                                                foreach ($arResult["DIVIDENDS_FIELDS"] as $name => $v): ?>
																					     <?if($v==false){ continue; }?>
                                                                    <th data-breakpoints="<?= $arResult["POINTS"][$i] ?>">
                                                                        <?= $name ?>
                                                                    </th>
                                                                    <? $i++;endforeach ?>
                                                            </tr>
                                                            </thead>

                                                            <tbody>
                                                            <? foreach ($arResult["DIVIDENDS"] as $coupon): ?>
                                                                <tr class="<?if((new DateTime())<=(new DateTime($coupon["Дата закрытия реестра"])) && $onegreen==false): $onegreen = true;?> green<?endif;?>">
                                                                    <? foreach ($arResult["DIVIDENDS_FIELDS"] as $name => $v): ?>
																						  <?if($v==false){ continue; }?>
                                                                        <td><?= $coupon[$name] ?></td>
                                                                    <? endforeach ?>
                                                                </tr>
                                                            <? endforeach ?>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                <? endif ?>


                <div class="text-center mt-40 mb-40">
                    <p class="legal_popup_btn"><a href="#" data-toggle="modal" data-target="#legal_popup">Условия
                            использования информации, размещённой на данном сайте</a></p>
                </div>

                <? if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest"): ?>
                    <div class="article_stats">
                        <div class="row">
                            <div class="col col-xs-6">
                            </div>

                            <div class="col col-xs-6">
                                <ul class="article_stats_list">
                                    <li><span class="icon icon-comment"></span> <span id="commentsCount"></span></li>
                                    <li><span class="icon icon-eye"></span> <?= $arResult["SHOW_COUNTER"] ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?
                    $lj_event = htmlentities('<a href="http://' . $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] . '">' . $arResult["NAME"] . '</a><img src="http://' . $_SERVER['HTTP_HOST'] . $image_src_for_social . '">');
                    ?>
                    <div class="article_share">
                        <p>Рассказать другим</p>
                        <ul class="share_list">
                            <li>
                                <a onclick="window.open('http://vkontakte.ru/share.php?url=http://<?= $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] ?>&amp;title=<?= $arResult["NAME"] ?>&amp;image=http://<?= $_SERVER['HTTP_HOST'] . $image_src_for_social ?>', 'vk', 'width=626, height=626'); return false;"
                                   title="Поделиться с друзьями в Вконтакте" rel="nofollow" class="icon icon-vk"
                                   href="http://vkontakte.ru/share.php?url=http://<?= $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] ?>&amp;title=<?= $arResult["NAME"] ?>&amp;image=http://<?= $_SERVER['HTTP_HOST'] . $image_src_for_social ?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.facebook.com/sharer.php?u=http://<?= $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] ?>&amp;p[images][0]=http://<?= $_SERVER['HTTP_HOST'] . $image_src_for_social ?>', 'facebook', 'width=626, height=626'); return false;"
                                   title="Поделиться с друзьями в Facebook" rel="nofollow" class="icon icon-fb"
                                   href="http://www.facebook.com/sharer.php?u=http://<?= $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PAGE_URL"] ?>&amp;p[images][0]=http://<?= $_SERVER['HTTP_HOST'] . $image_src_for_social ?>"></a>
                            </li>
                            <li>
                                <a onclick="window.open('http://www.livejournal.com/update.bml?event=<?= $lj_event ?>&amp;subject=<?= $arResult["NAME"] ?>', 'livejournal', 'width=626, height=626'); return false;"
                                   title="Опубликовать в своем ЖЖ" rel="nofollow" class="icon icon-lj"
                                   href="http://www.livejournal.com/update.bml?event=<?= $lj_event ?>&amp;subject=<?= $arResult["NAME"] ?>"></a>
                            </li>
                            <!-- <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li> -->
                            <li><a href="https://twitter.com/share?text=<?= $arResult["NAME"] ?>"
                                   onclick="window.open('https://twitter.com/share?text=<?= $arResult["NAME"] ?>','Twitter', 'width=626, height=626'); return false;"
                                   class="icon icon-tw" rel="nofollow"></a></li>
                            <li>
                                <? $inFavorite = checkInFavorite($arResult["ID"], "action_usa") ?>
                                <span<? if (!$USER->IsAuthorized()): ?> data-toggle="modal" data-target="#popup_login"<? endif ?> class="to_favorites button<? if ($inFavorite): ?> active<? endif; ?>"
                                                                                                                                  data-type="action_usa"
                                                                                                                                  data-id="<?= $arResult["ID"] ?>">
	                                <span class="icon icon-star_empty"></span> <span
                                        class="text"><? if ($inFavorite): ?>В избранном<? else: ?>В избранное<? endif; ?></span>
	                            </span>
                            </li>
	                        <li>
									<?if(!$arResult["HAVE_USA_ACCESS"]):?>
									   <span  data-content="<?=$APPLICATION->lkRadarPopups["заглушка портфеля"]?>" class="dib relative tooltip_custom_outer" style="border-radius:0 !important">
										<a   href="javascript:void(0);" class="icon icon-portfolio add_to_portfolio " rel="nofollow"></a>
										</span>
									<?else:?>
	                        	<a  data-toggle="modal"<?if(!$USER->IsAuthorized()):?> data-target="#popup_login"<?else:?> data-target="#popup_add_portfolio"<?endif?>href="javascript:void(0);" class="icon icon-portfolio add_to_portfolio" rel="nofollow"></a>
									<?endif;?>
									</li>
                        </ul>
                    </div>

                    <? $APPLICATION->IncludeComponent("bitrix:news.detail", "banner_bottom", Array(
                        "IBLOCK_TYPE" => "FINPLAN",    // Тип информационного блока (используется только для проверки)
                        "IBLOCK_ID" => "24",    // Код информационного блока
                        "ELEMENT_ID" => 18471,    // ID новости
                        "ELEMENT_CODE" => "",    // Код новости
                        "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                        "FIELD_CODE" => array(    // Поля
                            0 => "SHOW_COUNTER",
                            1 => "PREVIEW_PICTURE",
                        ),
                        "PROPERTY_CODE" => array(    // Свойства
                            0 => "RELATED_POSTS",
                            1 => "",
                        ),
                        "IBLOCK_URL" => "",    // URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
                        "AJAX_MODE" => "N",    // Включить режим AJAX
                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "META_KEYWORDS" => "-",    // Установить ключевые слова страницы из свойства
                        "META_DESCRIPTION" => "-",    // Установить описание страницы из свойства
                        "BROWSER_TITLE" => "-",    // Установить заголовок окна браузера из свойства
                        "SET_STATUS_404" => "Y",    // Устанавливать статус 404, если не найдены элемент или раздел
                        "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "ADD_ELEMENT_CHAIN" => "N",    // Включать название элемента в цепочку навигации
                        "ACTIVE_DATE_FORMAT" => "",    // Формат показа даты
                        "USE_PERMISSIONS" => "N",    // Использовать дополнительное ограничение доступа
                        "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",    // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",    // Выводить детальное изображение
                        "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                        "USE_SHARE" => "N",    // Отображать панель соц. закладок
                        "PAGER_TEMPLATE" => "",    // Шаблон постраничной навигации
                        "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                        "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                        "PAGER_TITLE" => "Страница",    // Название категорий
                        "PAGER_SHOW_ALL" => "Y",    // Показывать ссылку "Все"
                    ),
                        false
                    ); ?>

                    <? if ($arResult["ARTICLES"]): ?>
                        <div class="other_articles_list">
                            <div class="other_articles_list_top">
                                <p>Рекомендуемые к прочтению статьи:</p>

                                <a class="link_arrow" href="/blog/">Все статьи</a>
                            </div>
                            <div class="articles_list colored row">
                                <? foreach ($arResult["ARTICLES"] as $item): ?>
                                    <div class="articles_list_element col-xs-6 col-lg-4">
                                        <div class="articles_list_element_inner">
                                            <div class="articles_list_element_img">
                                                <div class="articles_list_element_img_inner">
                                                    <a href="<?= $item["DETAIL_PAGE_URL"] ?>"
                                                       style="background-image:url(<?= CFile::GetPath($item["PREVIEW_PICTURE"]) ?>);"></a>
                                                </div>
                                            </div>

                                            <div class="articles_list_element_text">
                                                <p class="articles_list_element_title">
                                                    <a href="<?= $item["DETAIL_PAGE_URL"] ?>"><?= $item["NAME"] ?></a>
                                                </p>
                                                <p class="articles_list_element_description"><?= $item["PREVIEW_TEXT"] ?></p>

                                                <p class="articles_list_element_date">
                                                    <?= FormatDate(array(
                                                        "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
                                                        "today" => "today",              // выведет "сегодня", если дата текущий день
                                                        "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
                                                        "d" => 'j F Y',                   // выведет "9 июля", если месяц прошел
                                                        "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
                                                    ), MakeTimeStamp($item["DATE_ACTIVE_FROM"]), time()
                                                    ); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>

                    <div class="comment_outer">
                        <div class="content-main-post-item-comments">
                            <? $APPLICATION->IncludeComponent("cackle.comments", "cackle_default", Array(),
                                false
                            ); ?>
                        </div>
                    </div>
                <? endif ?>
                <!-- *****КАРКАС***** -->
            </div>
        </div>

        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "right_sidebar",
                "EDIT_TEMPLATE" => "",
                "COMPONENT_TEMPLATE" => ".default",
                "AREA_FILE_RECURSIVE" => "Y"
            ),
            false
        ); ?>
    </div>
</div>


<div class="modal fade" id="legal_popup" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <p class="strong">Условия использования информации с сайта Fin-plan.org</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="modal-body_title"></div>
                <div class="modal-body_inner">
                    <p>Вся информация, размещенная на данном интернет-сайте, дизайн, подбор, группировка и расположение
                        материалов являются интеллектуальной собственностью ИП Кошин В.В. и защищены российским
                        законодательством об авторском праве и средствах массовой информации, а также международными
                        договорами РФ по защите интеллектуальной собственности.</p>
                    <p>Информация о торгах на рынке группы «московская биржа» предоставлена ПАО Московская биржа.
                        Источником и владельцем биржевой информации является московская биржа.</p>
                    <p>Без письменного согласия биржи нельзя осуществлять дальнейшее распространение и предоставление
                        биржевой информации в любом виде и любыми средствами, включая электронные, механические,
                        фотокопировальные, записывающие или другие (в том числе с использованием удаленного мобильного
                        (беспроводного) доступа), её трансляцию, в том числе средствами теле- и радиовещания, её
                        демонстрацию на интернет-сайтах, её использование в игровых, тренажерных и иных системах,
                        предусматривающих демонстрацию и/или передачу биржевой информации, а также для расчёта
                        производной информации (в том числе индексов и индикаторов), предназначенной для дальнейшего
                        публичного распространения.</p>
                    <p>ИП Кошин В.В. является дистрибьютором информации об итогах торгов на рынках группы московская
                        биржа.</p>
                    <p>Материалы данного интернет-сайта предназначены исключительно для персонального и некоммерческого
                        использования. Запрещается использование автоматизированного извлечения информации данного
                        интернет-сайта без письменного разрешения ИП Кошин В.В. Любое копирование, перепечатка и/или
                        последующее распространение информации, представленной на данном сайте, или информации,
                        полученной на основе этой информации в результате переработки, в том числе производимое путем
                        кэширования, кадрирования или использованием аналогичных средств, строго запрещается без
                        предварительного письменного разрешения ИП Кошин В.В. За нарушение настоящего правила наступает
                        ответственность, предусмотренная законодательством и международными договорами РФ.</p>
                    <p>ИП Кошин В.В. не несет ответственность за любую потерю или ущерб (потеря деловых доходов, потеря
                        прибыли или любой прямой, косвенный, последующий, специальный или подобный ущерб вообще),
                        являющийся результатом:</p>
                    <ul>
                        <li>любой погрешности (или неполноты в, или задержки, прерывания, ошибки или упущения в
                            поставке) в информации;
                        </li>
                        <li>любого сбоя информационной системы;</li>
                        <li>любого решения или действия, сделанного в уверенности относительно информации.</li>
                    </ul>
                    <p>ИП Кошин В.В. не несет ответственность за ущерб, причиненный вашему компьютеру, программному
                        обеспечению, модему, телефону и другой собственности, который стал результатом использования
                        информации, программного обеспечения или услуг.</p>
                    <p>С уважением, компания Fin-plan.</p>
                </div>
            </div>
        </div>
    </div>
</div>
