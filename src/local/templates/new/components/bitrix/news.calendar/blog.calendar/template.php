<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
  /** @var array $arParams */
  /** @var array $arResult */
  /** @global CMain $APPLICATION */
  /** @global CUser $USER */
  /** @global CDatabase $DB */
  /** @var CBitrixComponentTemplate $this */
  /** @var string $templateName */
  /** @var string $templateFile */
  /** @var string $templateFolder */
  /** @var string $componentPath */
  /** @var CBitrixComponent $component */
  $this->setFrameMode(true);
  $monthes = array(
  1 => 'Январь', 2 => 'Февраль', 3 => 'Март', 4 => 'Апрель',
  5 => 'Май', 6 => 'Июнь', 7 => 'Июль', 8 => 'Август',
  9 => 'Сентябрь', 10 => 'Октябрь', 11 => 'Ноябрь', 12 => 'Декабрь'
  );
?>
<div class="news-calendar">
	<div class="content-sidebar-calendar-month">
		<?if($arResult["PREV_MONTH_URL"]):?>
    <a rel="nofollow" href="<?=$arResult["PREV_MONTH_URL"]?>" title="<?=$arResult["PREV_MONTH_URL_TITLE"]?>"><span class="icon icon-arr_left"></span></a>
		<?endif?>
		<a rel="nofollow" href="/blog/<?=$arResult["currentYear"]?>/<?=$arResult["currentMonth"]?>/"><?=$monthes[$arResult["currentMonth"]]?></a>&nbsp;<a rel="nofollow" href="/blog/<?=$arResult["currentYear"]?>/"><?=$arResult["currentYear"]?></a>
		<?if($arResult["NEXT_MONTH_URL"]):?>
    <a rel="nofollow" href="<?=$arResult["NEXT_MONTH_URL"]?>" title="<?=$arResult["NEXT_MONTH_URL_TITLE"]?>"><span class="icon icon-arr_right"></span></a>
      <?endif?>
    </div>
    <!--<table>
      <tr>
			<td class="NewsCalMonthNav" align="left">
      <?if($arResult["PREV_MONTH_URL"]):?>
      <a href="<?=$arResult["PREV_MONTH_URL"]?>" title="<?=$arResult["PREV_MONTH_URL_TITLE"]?>"><?=GetMessage("IBL_NEWS_CAL_PR_M")?></a>
      <?endif?>
      <?if($arResult["PREV_MONTH_URL"] && $arResult["NEXT_MONTH_URL"] && !$arParams["SHOW_MONTH_LIST"]):?>
      &nbsp;&nbsp;|&nbsp;&nbsp;
      <?endif?>
      <?if($arResult["SHOW_MONTH_LIST"]):?>
      &nbsp;&nbsp;
      <select onChange="b_result()" name="MONTH_SELECT" id="month_sel">
      <?foreach($arResult["SHOW_MONTH_LIST"] as $month => $arOption):?>
      <option value="<?=$arOption["VALUE"]?>" <?if($arResult["currentMonth"] == $month) echo "selected";?>><?=$arOption["DISPLAY"]?></option>
      <?endforeach?>
      </select>
      &nbsp;&nbsp;
      <?endif?>
      <?if($arResult["NEXT_MONTH_URL"]):?>
      <a href="<?=$arResult["NEXT_MONTH_URL"]?>" title="<?=$arResult["NEXT_MONTH_URL_TITLE"]?>"><?=GetMessage("IBL_NEWS_CAL_N_M")?></a>
      <?endif?>
			</td>
			<?if($arParams["SHOW_YEAR"]):?>
			<td class="NewsCalMonthNav" align="right">
      <?if($arResult["PREV_YEAR_URL"]):?>
      <a href="<?=$arResult["PREV_YEAR_URL"]?>" title="<?=$arResult["PREV_YEAR_URL_TITLE"]?>"><?=GetMessage("IBL_NEWS_CAL_PR_Y")?></a>
      <?endif?>
      <?if($arResult["PREV_YEAR_URL"] && $arResult["NEXT_YEAR_URL"]):?>
      &nbsp;&nbsp;|&nbsp;&nbsp;
      <?endif?>
      <?if($arResult["NEXT_YEAR_URL"]):?>
      <a href="<?=$arResult["NEXT_YEAR_URL"]?>" title="<?=$arResult["NEXT_YEAR_URL_TITLE"]?>"><?=GetMessage("IBL_NEWS_CAL_N_Y")?></a>
      <?endif?>
			</td>
			<?endif?>
      </tr>
    </table>-->
    <div class="content-sidebar-calendar-days">
      <?foreach($arResult["WEEK_DAYS"] as $WDay):?>
      <div><span class="content-sidebar-calendar-days-day"><?=$WDay["SHORT"]?></span></div>
      <?endforeach?>
      <?foreach($arResult["MONTH"] as $arWeek):?>
      <?foreach($arWeek as $arDay):?>
			<?
				$new_day_class = '';
				switch($arDay["tday_class"]) {
					case 'NewsCalDayOther':
          $new_day_class = 'content-sidebar-calendar-days-notthismonth';
          break;
					case 'NewsCalDay':
          $new_day_class = 'content-sidebar-calendar-days-thismonth';
          break;
        }
      ?>
			<? if(is_array($arDay["events"]) && count($arDay["events"])>0) {?>
        <?$abc = true;?>
        <?foreach($arDay["events"] as $event):?>
          <?
            $pos = strpos($event["url"],'slovar-finansovykh-terminov');
            if ($pos === false) {
            } else {
              $abc=false;
            }
          ?>
        <?endforeach?>
        <? if($abc == true) {?>
          <div><a rel="nofollow" class="content-sidebar-calendar-days-link" href="/blog/<?=$arResult["currentYear"]?>/<?=$arResult["currentMonth"]?>/<?=$arDay["day"]?>/"><span><?=$arDay["day"]?></span></a></div>
        <? } else {?>
          <div><span class="<?=$new_day_class?>"><?=$arDay["day"]?></span></div>
        <? } ?>
      <? } else {?>
				<div><span class="<?=$new_day_class?>"><?=$arDay["day"]?></span></div>
      <? } ?>


      <?endforeach?>
      <?endforeach?>
      </div>
    </div>
    <script language="JavaScript" type="text/javascript">
      <!--
      function b_result()
      {
        var idx=document.getElementById("month_sel").selectedIndex;
        <?if($arParams["AJAX_ID"]):?>
				BX.ajax.insertToNode(document.getElementById("month_sel").options[idx].value, 'comp_<?echo CUtil::JSEscape($arParams['AJAX_ID'])?>', <?echo $arParams["AJAX_OPTION_SHADOW"]=="Y"? "true": "false"?>);
        <?else:?>
				window.document.location.href=document.getElementById("month_sel").options[idx].value;
        <?endif?>
      }
      -->
    </script>
