<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true);
Global $USER, $APPLICATION;

if($_GET["utm_personal"]){
	Promocodes::setCookiePromo($_GET["utm_personal"]);
  	LocalRedirect($APPLICATION->GetCurPageParam("", array("utm_personal")));
}
  $curPage = $APPLICATION->GetCurPage(true);
  //Определяем страницу упрощенной оплаты
  define("SIMPLE_PAY_PAGE",checkSimplePay());
  if($USER->IsAuthorized()){
      $key = $APPLICATION->GetCurPage();
          CUsersStat::setStat($USER->GetID(), $key, $APPLICATION->GetCurUri(false));
  }
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?=$APPLICATION->ShowTitle()?></title>
	
			<meta name="yandex-verification" content="4e996e8d7c4c5027" />

	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/intltelinput.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/intlTelInput.css" />

	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.fullpage.min.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/slick.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap-datepicker3.standalone.min.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.mCustomScrollbar.min.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/footable.core.standalone.min.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/select2.min.css" />

	<?if(OBLIGATION_PAGE=="Y"):?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery-ui.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/select2.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/chartist.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/chartist-plugin-tooltip.css" />
		<link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.jqplot.min.css" />
		<link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.countdown.css" />
	<?endif?>
	<?if(MARKET_MAP_PAGE=="Y"):?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery-ui.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/select2.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/chartist.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/chartist-plugin-tooltip.css" />
		<link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.jqplot.min.css" />

	<?endif?>
	<?if(PORTFOLIO_PAGE=="Y"):?>
	   <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery-ui.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/select2.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/chartist.min.css" />
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/chartist-plugin-tooltip.css" />
		<link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.jqplot.min.css" />
	<?endif;?>
    <?if(TEST_PAGE=="Y"):?>
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/testpage.css" />
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery-ui.min.css" />
    <?endif?>
	 <?if(OBLIGATION_DETAIL_PAGE=="Y" || ETF_RADAR_PAGE=="Y"):?>
	     <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/select2.min.css" />
	 <?endif?>
    <link href="<?=SITE_TEMPLATE_PATH?>/fonts/ubuntu/ubuntu.css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/fonts/iconmoon/style.css?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/fonts/iconmoon/style.css') ?>" rel="stylesheet" />

    <link href="<?=SITE_TEMPLATE_PATH?>/css/styles.css?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/css/styles.css') ?>" rel="stylesheet" />
	<link href="<?=SITE_TEMPLATE_PATH?>/more.css" rel="stylesheet" />
	<!-- в custom css пишутся правила с указанием раздела/страницы для которых они предназначены. Далее при проверке верстальщик переносит правила в свои исходные css, обновляет их на сайте, а custom.css очищает-->
	<link href="<?=SITE_TEMPLATE_PATH?>/css/custom.css?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/css/custom.css') ?>" rel="stylesheet" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-2.2.2.min.js"></script>

    <?$APPLICATION->ShowHead()?>
 <script>

 var IS_AUTHORIZED = '<?= ($USER->IsAuthorized()?'Y':'N') ?>';
 var IS_ADMIN = '<?= ($GLOBALS["USER"]->IsAdmin()?'Y':'N') ?>';
 var SITE_SERVER_NAME = '<?= SITE_SERVER_NAME ?>';
 var HTTPS = '<?= getServerProtocol(); ?>';
     if(HTTPS!='https'){
	  //SITE_SERVER_NAME = 'www.'+SITE_SERVER_NAME;
	  SITE_SERVER_NAME = SITE_SERVER_NAME;
     }

 var CURRENCIES = <?=CUtil::PhpToJSObject($APPLICATION->currencies, false, true)?>;//Валюты с символами для скриптов

 </script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '721559197981009');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="//www.facebook.com/tr?id=721559197981009&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=DYWIwFy/*WUWZmro21c8f*49kyVF2sDc6QwBmzgv6Whi2zJ0HzC7FelUykuhLEw76hgHcZzeLCK8w672BtMaeI*yygtDFy1WPxp8gdRlUwc*c208xC6x*PB4i5Mk4PyFRkPSSOhviv0ZEsgPg/M9iVxm2BDhGuSrL*NLS3Ncu6k-&pixel_id=1000025683';</script>
<!--<script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/d94a697f077bf0be438608a1a6bdaa59_1.js" async></script> -->

<!-- Put this script tag to the <head> of your page -->
<?echo \Bitrix\Main\Config\Option::get("grain.customsettings","VK_OPENAPI_SCRIPT"); ?>
<!--<script type="text/javascript" src="https://vk.com/js/api/openapi.js?162"></script>-->

	<script type="text/javascript">
	   <?if(strpos($curPage, "portfolio")===false):?>
		<?echo \Bitrix\Main\Config\Option::get("grain.customsettings","VK_WIDGET_INIT"); ?>
		<?endif;?>
		//VK.init({apiId: 7124243, onlyWidgets: true});
	</script>

  </head>
  <body data-auth="<?=($USER->IsAuthorized())?"true":"false"?>">
<!-- Yandex.Metrika counter -->
<!-- 						  userParams: {
						      pay_radar: "<?=(checkPayRadar(false, $USER->GetID())?'y':'n')?>",
								pay_radar_usa: "<?=(checkPayUSARadar(false, $USER->GetID())?'y':'n')?>",
								user_auth: "<?=($USER->IsAuthorized()?'y':'n')?>",
						      UserID: "<?= ($USER->IsAuthorized()?$USER->GetID():'') ?>"
					    } -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter29896484 = new Ya.Metrika({
                    id:29896484,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/29896484" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
$(window).load(function () {
/*ym(29896484, 'userParams', {
      pay_radar: "<?=(checkPayRadar(false, $USER->GetID())?'y':'n')?>",
		pay_radar_usa: "<?=(checkPayUSARadar(false, $USER->GetID())?'y':'n')?>",
		user_auth: "<?=($USER->IsAuthorized()?'y':'n')?>",
    UserID: "<?= ($USER->IsAuthorized()?$USER->GetID():'') ?>"
});*/

});
</script>

<!-- User segmentation start -->
<script>
window.usetifulTags = {
    segment: "trial users"
};</script>
<!-- User segmentation end -->
<!-- Usetiful script start -->
<script>
  (function (w, d, s) {
    var a = d.getElementsByTagName('head')[0];
    var r = d.createElement('script');
    r.async = 1;
    r.src = s;
    r.setAttribute('id', 'usetifulScript');
    r.dataset.token = "50472816f661497f7651328f74d1ef1d";
            a.appendChild(r);
  })(window, document, "https://www.usetiful.com/dist/usetiful.js");
</script>
<!-- Usetiful script end -->

<?if(($USER->IsAuthorized())):?>
<? global $USER;
  $rsUser = CUser::GetByID($USER->GetID());
  $arUser = $rsUser->Fetch();
  $newUserDate = \Bitrix\Main\Config\Option::get("grain.customsettings","NEW_USERS_DATE");
  $isNewRadarUser = CUsersStat::isNewRadarUser($USER->GetID()); //Первый ли раз после покупки пользователь зашел на страницу радара?
//
/*	global $USER;
         $rsUser = CUser::GetByID($USER->GetID());
         $arUser = $rsUser->Fetch();
         if($arUser["LOGIN"]=="freud"){
			  echo "<pre  style='color:black; font-size:11px;'>";
              print_r($newUserDate);
              print_r( (new DateTime($arUser["DATE_REGISTER"]))->getTimestamp()>(new DateTime($newUserDate))->getTimestamp() ? 'Y':'N');
              echo "</pre>";
         }*/
  ?>
  <?if((new DateTime($arUser["DATE_REGISTER"]))->getTimestamp()<(new DateTime($newUserDate))->getTimestamp()):?>
<script>
	window.usetifulTags = {
	status: "newuser",
    role: "<?=($isNewRadarUser?'newcustomer':'oldcustomer')?>"
	};
</script>
 <?endif;?>
<?endif;?>

	<?$APPLICATION->ShowPanel()?>
	<div class="outer<?if(OUTER_CLASS):?> <?=OUTER_CLASS?><?endif?>">
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col col-xs-6 col-md-2">
						<span class="hamburger"><span></span></span>
						<a class="logo" href="<?if($APPLICATION->GetCurPage()!="/"):?>/<?endif;?>"><span>Fin</span>-plan</a>
					</div>

					<div class="col main_menu_container col-md-8">

						<?$APPLICATION->IncludeComponent("bitrix:menu", SIMPLE_PAY_PAGE?"simple_top":"top", Array(
							"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
								"MENU_CACHE_TYPE" => "N",	// Тип кеширования
								"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
								"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
								"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
								"MAX_LEVEL" => "2",	// Уровень вложенности меню
								"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
								"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
								"DELAY" => "N",	// Откладывать выполнение шаблона меню
								"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							),
							false
						);?>
					</div>

					<div class="col col-xs-6 col-md-2 text-right">

						<?if ($USER->IsAuthorized()):?>
                        <?global $USER;
							$arGroups = $USER->GetUserGroupArray();
							?>
                        <?if($USER->IsAdmin() || in_array(5, $arGroups)):?>
                                <a class="header_tools icon icon-tools" href="/tools/" target="_blank"></a>
                        <?endif;?>


                        <span class="header_user">
                            <a class="header_user__icon icon icon-user lk-top-button" href="/personal/main/?utm_content=<?= Cuser::GetID(); ?>"></a>

                            <ul class="header_user__submenu main_menu_toggle">
                                <li><a href="/personal/main/?utm_content=<?= Cuser::GetID(); ?>">Личный кабинет</a></li>
                                <li><a href="/personal/bonus/?utm_content=<?= Cuser::GetID(); ?>">Бонусная программа</a></li>
                                <li><a href="/personal/note/?utm_content=<?= Cuser::GetID(); ?>">Уведомления</a></li>
                            </ul>
                        </span>

							<a class="login_btn button button_transparent" href="<?=$APPLICATION->GetCurPageParam("logout=yes")?>">Выйти</a>
						<?else:?>
							<?if($APPLICATION->GetCurPage()!="/lk/radar_in/"):?>
								<a class="login_btn button button_transparent" href="#" data-toggle="modal" data-target="#popup_login">Войти</a>
							<?endif?>
						<?endif?>
					</div>
				</div>
			</div>
		</div>
