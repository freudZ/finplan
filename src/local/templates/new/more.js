$(function(){
	//Массовое удаление портфелей в ЛК пользователя
	$('#delSelectedPortfolio').off('click');
	$('#delSelectedPortfolio').on('click', function(){
		var arr;
		var pids = [];
		var currentPopup = $("#popup_mass_portfolio_remove");
		currentPopup.find('.popup_progress').addClass('active');
		$(".massPidDel_input:checked").each(function(indx, element){
        pids.push($(element).val());
      });
		  //	console.log(pids);
	  		$.ajax({
	  		type: "POST",
	  		url: "/ajax/lk/massPortfolioRemove.php",
	  		dataType: "json",
	  		data:{ajax: "y", action: 'deleteMassPortfolio', 'pids':pids},
	  		cashe: false,
	  		async: false,
	  		success: function(data){
	  			getResponse(data);
	  		}
	  		});

	  	 function getResponse(data){
	  	 	arr = data;
			arr.forEach(function(item, i, arr) {
			 // console.log('remove pid', item);
			  $('.pidListElement_'+item).detach();
			});
			currentPopup.find('.popup_progress').removeClass('active');
			currentPopup.modal('hide');
	  	 }
	});

	$('.massPidDel_input').off('change');
	$('.massPidDel_input').on('change', function(){
		console.log('pid_del_cnt',$(".massPidDel_input:checked").length);
     if($(".massPidDel_input:checked").length>0){
		 $('.delSelectedPortfolioPopup').removeAttr('disabled');
     } else {
		 $('.delSelectedPortfolioPopup').attr('disabled','disabled');
     }
   });


	//+Загрузка отчета брокера 2
		//Событие показа модального окна загрузки отчета брокера
    $('#popup_load_from_broker2').off('show.bs.modal');
    $('#popup_load_from_broker2').on('show.bs.modal', function(e)
      {
        var loadPopup = $('#popup_load_from_broker2');
        loadPopup.find('#load_broker_portfolio_name').val('');
		  loadPopup.find('#broker_report_file+.file_text span').text('Выберите файл');
		  loadPopup.find('#popup_load_broker option:first').prop('selected', true).change();
		  loadPopup.find('#popup_load_broker_portfolio option:first').prop('selected', true).change();
      }
    );

		//Выбор брокера
		$('#popup_load_from_broker2 #popup_load_broker').on('change', function(){
		  var loadPopup = $('#popup_load_from_broker2');
		  var loadFiletypes = loadPopup.find('.load_broker_filetypes span');
		  var extensions = $(this).find('option:selected').attr('data-extensions');
		  var fileInput = loadPopup.find('#broker_report_file');
		  //Назначаем допустимые расширения файлов отчета
		  loadFiletypes.text(extensions);
		  fileInput.attr('accept',extensions);

		 });

		//Выбор портфеля
		$('#popup_load_from_broker2 #popup_load_broker_portfolio').on('change', function(){
		 var loadPopup = $('#popup_load_from_broker2');
		 var new_portfolio_name_block = loadPopup.find('.new_portfolio_name');
		 if($("#popup_load_from_broker2 #popup_load_broker_portfolio").val().length>0){
			new_portfolio_name_block.addClass('hidden');
		 } else {
			new_portfolio_name_block.removeClass('hidden');
		 }


		});

	   //При выборе файла отчета, если не задано название портфеля - берем его из имени файла
		$('#popup_load_from_broker2 #broker_report_file').on('change', function(){
		  var loadPopup = $('#popup_load_from_broker2');
		  var portfolio_name = loadPopup.find('#load_broker_portfolio_name');

		  var file_data = loadPopup.find('#broker_report_file').prop('files')[0];

		  if(!portfolio_name.val().length){
			  portfolio_name.val(file_data["name"]);
		  }

		  return false;
		});

		$('#popup_load_from_broker2').on('show.bs.modal', function() {
			$('#popup_load_from_broker2').find('.broker_report_file_error').css('display', 'none');
		});

		$('#popup_load_from_broker2 .hist_broker_next_button').on('click', function(e){
			var loadPopup = $('#popup_load_from_broker2');
			var fileField = loadPopup.find('#broker_report_file');
			var fileError = loadPopup.find('.broker_report_file_error');

			fileField.on('change', function(event) {
				fileError.css('display', fileField.prop('files').length > 0 ? 'none' : 'block');
			});

			if (fileField.prop('files').length > 0) {
				fileError.css('display', 'none');

				var currstep = Number($(this).attr('data-currstep'));
				var minstep = 1;
				var maxstep = 2;

				if(currstep+1<=maxstep){
					loadPopup.find('.popup_progress').addClass('active');

					setTimeout(function() {
						var newStep = currstep+1;
						var form_data = new FormData();
						var file_data = loadPopup.find('#broker_report_file').prop('files')[0];
						var report_type = loadPopup.find('#popup_load_broker').val();
						var portfolio_name = loadPopup.find('#load_broker_portfolio_name').val();
						var portfolio_id = loadPopup.find('#popup_load_broker_portfolio').val();

						form_data.append('broker_report_file', file_data);
						console.log('@@@@@@@', loadPopup.find('#broker_report_file').prop('files'), file_data)

						form_data.append('broker_report_file_name', file_data.name);
						form_data.append('report_type', report_type);
						form_data.append('portfolio_name', portfolio_name);
						form_data.append('portfolio_id', portfolio_id);
						form_data.append('ajax', 'Y');
						form_data.append('step', currstep);
						$.ajax({
							type: "POST",
							url: "/ajax/portfolio/broker_report_load2.php",
							dataType: 'json',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,
							error: function (xhr, ajaxOptions, thrownError) {
								console.log(xhr.status);
								console.log(thrownError);
							},
							success: function(data){
								console.log('load_result', data);
								if(data["result"]=='success') {
									//window.open('/portfolio/portfolio_'+data["id"], "_blank");
									window.top.location.href = '/portfolio/portfolio_'+data["id"]+'/';
									loadPopup.find('.popup_progress').removeClass('active');
									$('#popup_load_from_broker').modal('hide');
								}
								else if(data["result"]=="step2"){

									$(this).attr('data-currstep', newStep);
									$('#popup_load_from_broker2 .step_block').addClass('hidden');
									$('#popup_load_from_broker2 .step_block.step'+newStep).removeClass('hidden');

									//	$('#popup_load_from_broker2 .parseData').val(data["report"]);
									$('#popup_load_from_broker2 .parseData').attr('data-tmpFile', data["tmpFilePath"]);
									$('#popup_load_from_broker2 .parseData').val(JSON.stringify(data["report"]));
									$('#popup_load_from_broker2 .uploadFilePath').val(data["uploadFilePath"]);
									$('#popup_load_from_broker2').find('.popup_progress').removeClass('active');
									var nofoundCnt = 0;
									//Object.keys(data["report"]["deals_nofound"]).forEach(function(key)
									Object.keys(data["report"]).forEach(function(key)
									{
										var ticker_isin = !this[key]['ticker']?this[key]['isin']:this[key]['ticker'];
										var tickerField = '<input type="text" value="" placeholder="Название / тикер / ISIN" name="q" data-isin="'+ticker_isin+'" class="autocomplete_load_broker" id="autocomplete_all'+key+'" />';

										$('#popup_load_from_broker2 .nofoundActives').append('<label>'+ticker_isin+'</label>'+tickerField+'<br>');



									//}, data["report"]["deals_nofound"]);
									}, data["report"]);

									//автокомплит для нераспознанных тикеров при загрузке отчета от брокера
									if($("#popup_load_from_broker2 .autocomplete_load_broker").length) {
										$('#popup_load_from_broker2 .autocomplete_load_broker').autocomplete({
											serviceUrl: '/ajax/obligations/broker_load_search.php',
											onSelect: function (suggestion) {
											}
										});
									}


								} else {
									// var messText = String(data["status"])+' '+data["message"];
									var messText = data["message"];
									// alert('Ошибка создания портфеля: '+messText);
									loadPopup.find('.popup_progress').text(messText);
									setTimeout(function(){
										document.location.reload();
										//window.top.location.href = '/portfolio/portfolio_'+data["id"];
										//window.open('/portfolio/portfolio_'+data["id"], "_blank");
										$('#popup_load_from_broker2').find('.popup_progress').removeClass('active');
										$('#popup_load_from_broker2').modal('hide');
									},2000);

								}
							}
						});
					}, 200);
				}
			} else {
				e.preventDefault();
				fileError.css('display', 'block');
			}

		});

		//Второй шаг
		$('#popup_load_from_broker2 .hist_broker_step2_button').on('click', function(e) {
			 var loadPopup = $('#popup_load_from_broker2');
			 loadPopup.find('.popup_progress').addClass('active');
		    var report_data = JSON.parse(loadPopup.find('.parseData').val());
		    var uploadFilePath = loadPopup.find('.uploadFilePath').val();
		    var report_type = loadPopup.find('#popup_load_broker').val();
		    var portfolio_name = loadPopup.find('#load_broker_portfolio_name').val();
		    var portfolio_id = loadPopup.find('#popup_load_broker_portfolio').val();
			 var file_data = loadPopup.find('#broker_report_file').prop('files')[0];
			 var nofoundArray = [];

			 loadPopup.find('.nofoundActives input').each(function(indx, element){
			 	let nofoundData = {'isin' : $(element).attr('data-isin'), 'found' : $(element).val()};
			   nofoundArray.push(nofoundData);
			 });

		    var data_fields = {
				'report': report_data,
				'uploadFilePath': uploadFilePath,
				'nofoundArray': nofoundArray,
				'report_type': report_type,
				'portfolio_name': portfolio_name,
				'portfolio_id': portfolio_id,
				'tmpFilePath': loadPopup.find('.parseData').attr('data-tmpfile'),
				'broker_report_file_name': file_data.name,
				'ajax': 'Y',
				'step': 2
		    };
			  console.log('load_result2 start step2');
		    $.ajax({
		    				 type: 'POST',
		                url: '/ajax/portfolio/broker_report_load2.php', // point to server-side PHP script
		                dataType: 'json',
		                cache: false,
    					processData: true,
 		                data: data_fields,
		                success: function(data){
		                	console.log('load_result2', data);
					         if(data["result"]=="success") {
					          //window.open('/portfolio/portfolio_'+data["id"], "_blank");
								 window.top.location.href = '/portfolio/portfolio_'+data["id"]+'/';
								 loadPopup.find('.popup_progress').removeClass('active');
					          $('#popup_load_from_broker2').modal('hide');
					        }
					        else {
					        	 var messText = String(data["status"])+' '+data["message"];
					          console.log('Ошибка заполнения или создания портфеля: ', messText);
								 setTimeout(function(loadPopup){
								 	 $('#popup_load_from_broker2').modal('hide');
									 $('#popup_load_from_broker2').find('.popup_progress').removeClass('active');
								 },2000);

					        }

							   $('#popup_load_from_broker2 .step_block').addClass('hidden');
								$('#popup_load_from_broker2 .step_block.step2').removeClass('hidden');

		                	//console.log(data);

		                }
		     });
		return false;
		});


	//-Загрузка отчета брокера 2

	/* + Табы для семинара ГС */
	if($('.seminar_tab_selection').length){
	  $('.seminar_tab_selection li').on('click', function(){
       var target_tab = $(this).attr('data-value');
       var type_tab = $(this).attr('data-type');
		 var usa_link = $(this).attr('data-link');
		 if(type_tab=='usa'){
			if(usa_link.length>0){
			  window.top.location.href = usa_link;
			  return false;
			}
		 }
		 $('.seminar_tab_selection li').removeClass('active');
		 $('.seminar_tab_selection li.tab_seminar_'+type_tab).addClass('active');
		 $('.tab_seminar_body').hide();
		 $('.tab_seminar_body_'+type_tab).show();
		 if(type_tab=='usa'){
		 	setLocationUrl(window.location.pathname+'#usa');
		 } else {
			setLocationUrl(window.location.pathname);
		 }

     });



	}

	/* - Табы для семинара ГС */

	/*+ Активация таба в радаре при переходе по ссылке */
	if($('.radar_tabs').length){
		var hash = location.hash;
		if(hash.length){
			$('.radar_tabs li').removeClass('active');
			$('.radar_tabs li a[href=\\'+hash+']').click();
		}
	}
	/*- Активация таба в радаре при переходе по ссылке */


	if($('#subscribe_mini_kurs').length){
		$('#subscribe_mini_kurs').on('click', function(){
		  	$.cookie('subscribe_mini_kurs', 'y', {expires: 365, path: '/'});
			$('.playlist_overlay_subscribe').detach();
		});

	}


	$("#popup_login").on('submit', "form", function(){
		 var form_action = $(this).attr('action');
		var form_backurl = $(this).find('input[name="backurl"]').val();

		 $.ajax({
            type: "POST",
            url: '/local/templates/new/ajax/auth.php',
            data: $(this).serialize(),
            timeout: 3000,
            error: function(request,error) {
                if (error == "timeout") {
                    alert('timeout');
                }
                else {
                    alert('Error! Please try again!');
                }
            },
            success: function(data) {
                $('#popup_login .modal-body').html(data);

                $('#popup_login form').attr('action',form_action);
                $('#popup_login input[name=backurl]').val(form_backurl);
            }
        });

		return false;
	});


	$('#popup_signup').on('submit','form',function(){

		var form_action = $(this).attr('action');
		var form_backurl = $(this).find('input[name="backurl"]').val();

		$.ajax({
		  type: "POST",
		  url: '/local/templates/new/ajax/auth.php',
		  data: $(this).serialize(),
		  timeout: 3000,
		  error: function(request,error) {
			if (error == "timeout") {
			  alert('timeout');
			}
			else {
			  alert('Error! Please try again!');
			}
		  },
		  success: function(data) {
			$('#popup_signup .modal-body').html(data);
			$('#popup_signup form').attr('action',form_action);
			$('#popup_signup input[name=backurl]').val(form_backurl);
		  }
		});

		return false;
	});

	$('#popup_reset_password').on('submit','form',function(){

		var form_action = $(this).attr('action');
		var form_backurl = $(this).find('input[name="backurl"]').val();
		$.ajax({
			type: "POST",
			url: '/local/templates/new/ajax/auth.php',
			data: $(this).serialize(),
			timeout: 3000,
			error: function(request,error) {
				if (error == "timeout") {
					alert('timeout');
				}else{
					alert('Error! Please try again!');
				}
			},
			success: function(data) {
				$('#popup_reset_password .modal-body form').remove();
				$('#popup_reset_password .modal-body').html(data);
				$('#popup_reset_password form').attr('action',form_action);
				$('#popup_reset_password input[name=backurl]').val(form_backurl);
			}
		});

		return false;
	});

	//блок показать еще
	$(".more_articles.button").click(function () {
        updateBlogPagen();

		return false;
    });

	function updateBlogPagen() {
        var cur = $(".more_articles.button"),
			tags = [];

        $('.sections_list_filter li.active').each(function () {
			tags.push($(this).data("value"));
        });

        $.get("", {"is_ajax":true, "PAGEN_1": cur.data("next"), "tags": tags}, function (data) {
            $(".articles_list.row").append(data.content);

            if(data.next>data.last){
                $(".more_articles.button").parent().hide();
            } else {
                $(".more_articles.button").parent().show();
                cur.data("next", data.next);
            }
        }, "json");
    }


	//Событие показа кастомного модального окна
	$('#popup_master_subscribe_custom').off('show.bs.modal').on('show.bs.modal', function(e) {
		 var button = $(e.relatedTarget);
       var elementId = '';

		 elementId = button.data('content-id');
	   //получим контент в json для заполнения формы
		$.ajax({
		type: "POST",
		url: "/ajax/get_custom_popup_data.php",
		dataType: "json",
		data:{"ELEMENT_ID": elementId},
		cashe: false,
		async: false,
		success: function(data){
			  for (key in data) {
			  	   if(key=='PROPERTY_MAILCHIMP_LIST_VALUE'){
					 $('#popup_master_subscribe_custom .ib_'+key).val(data[key]);
			  	   } else if(key=='PROPERTY_MAILCHIMP_INTEREST_VALUE'){
					 $('#popup_master_subscribe_custom .ib_'+key).val(data[key]);
			  	   } else {
					$('#popup_master_subscribe_custom .ib_'+key).html(data[key]);
					}
			  }

		}
		});
	});
	//Очищаем текст в кастомном модальном окне при его закрытии
	$('#popup_master_subscribe_custom').off('hide.bs.modal').on('hide.bs.modal', function(e) {
		 $('#popup_master_subscribe_custom [class^=ib]').html('');

	});

    //фильтр статей
	if ($('.sections_list_filter').length != 0) {

		$('.sections_list_filter li').click(function() {
			if ($(this).hasClass('active')) {
				$(this).removeClass('active');
				$(this).find('.close').remove();
			} else {
				$(this).addClass('active');
				if ($(this).find('.icon').length == 0) {
					$(this).append('<span class="close">&times;</span>');
				}
			}

            $(".articles_list.row").empty();
            $(".more_articles.button").data("next", 1);

			updateBlogPagen();
		});

        if($('.sections_list_filter li[data-show]').length){
            $('.sections_list_filter li[data-show]').click();
        }
	}

	//автокомплит
	if($("#autocomplete_blog").length) {
        $('#autocomplete_blog').autocomplete({
            serviceUrl: '/ajax/blog_autocomplete.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }

	//автокомплит компании облигаций
	if($("#autocomplete_company_obl").length) {
        $('#autocomplete_company_obl').autocomplete({
            serviceUrl: '/ajax/obligations/companies.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }

	//автокомплит компании облигаций
	if($("#autocomplete_company_usa").length) {
        $('#autocomplete_company_usa').autocomplete({
            serviceUrl: '/ajax/obligations/companies_usa.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }

	//автокомплит акций США
	if($("#autocomplete_actions_usa").length) {
        $('#autocomplete_actions_usa').autocomplete({
            serviceUrl: '/ajax/obligations/actions_usa.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }

	//автокомплит облигации
	if($("#autocomplete_obl_all").length) {
        $('#autocomplete_obl_all').autocomplete({
            serviceUrl: '/ajax/obligations/obligations.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }

	//автокомплит акции
	if($("#autocomplete_act_all").length) {
        $('#autocomplete_act_all').autocomplete({
            serviceUrl: '/ajax/obligations/actions.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }

	//автокомплит индекса
	if($("#autocomplete_ind_all").length) {
        $('#autocomplete_ind_all').autocomplete({
            serviceUrl: '/ajax/obligations/indexes.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }

	//автокомплит общий
	if($("#autocomplete_all").length) {
        $('#autocomplete_all').autocomplete({
            serviceUrl: '/ajax/obligations/general_search.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }
	//автокомплит общий для попапа внесения дивов/налогов
	if($("#popup_cashflow_debstock_selector").length) {
        $('#popup_cashflow_debstock_selector').autocomplete({
            serviceUrl: '/ajax/obligations/general_search_popup_tax.php',
            onSelect: function (suggestion) {
                $(this).val(suggestion.name);
            }
        });
    }


    //автокомплит отраслей РФ
    if($("#autocomplete_sectors_rus").length) {
        $('#autocomplete_sectors_rus').autocomplete({
            serviceUrl: '/ajax/obligations/sectorsRus.php',
            onSelect: function (suggestion) {
                location.href = suggestion.data;
            }
        });
    }

    //добавление статьи в избранное
	$('.to_favorites').click(function(e) {
		if(!$(this).data("toggle")) {
            var cur = this,
				type = "blog";
			if($(this).data("type")){
				type = $(this).data("type");
			}

            $.post("/ajax/favorite.php", {"type": type, "id": $(this).data("id")}, function (data) {
                if (data.added) {
                    $(cur).addClass('active');
                    $(cur).find('.text').text('В избранном');
                    $(cur).find('.icon').removeClass('icon-star_empty');
                    $(cur).find('.icon').addClass('icon-star');
                } else {
                    $(cur).removeClass('active');
                    $(cur).find('.text').text('В избранное');
                    $(cur).find('.icon').removeClass('icon-star');
                    $(cur).find('.icon').addClass('icon-star_empty');
                }

                updateFavoriteData();
            }, "json");
        }
	});

	//удаление из избранного
	$('body').on("click", ".favorites_list_remove", function() {
		var cur = this;
		$.post("/ajax/favorite.php", {"type": "delete", "id": $(this).data("id")}, function (data) {
			var li = $(cur).closest('li');

			li.css('opacity', 0);
			setTimeout(function () {
				li.slideUp(300);
			}, 300);
			setTimeout(function () {
				li.remove();
			}, 600);

            if($('.to_favorites[data-id='+$(cur).data("id")+']').length){
            	var item = $('.to_favorites[data-id='+$(cur).data("id")+']');

                item.removeClass('active');
                item.find('.text').text('В избранное');
                item.find('.icon').removeClass('icon-star');
                item.find('.icon').addClass('icon-star_empty');
        	}

            updateFavoriteData();
		});
	});

	function updateFavoriteData(){
        $.post("/ajax/left_sidebar/favorite.php", function (data) {
			$(".favorite_ajax_data").html(data);
        });
	}

    if ($('#add_review_form').length != 0) {

        var validator = $('#add_review_form').validate({
            rules: {
                name: {required: true, lettersonly: true},
                email: {required: true, truemail: true},
                review_text: {required: true},
                confidencial_add_review: {required: true}
            },
            messages: {
                name: {
                    required: "Данное поле не заполнено",
                    lettersonly: "Неверный формат данных"
                },
                email: {
                    required: "Данное поле не заполнено",
                    truemail: "Неверный формат данных"
                },
                review_text: {
                    required: "Данное поле не заполнено",
                },
                confidencial_add_review: {
                    required: "Необходимо поставить галочку",
                }
            },

            errorElement: "span",
            errorPlacement: function(error, element) {
                error.prependTo( element.parent("div") );
            },
            submitHandler: function(form){
                var formData = new FormData($("#add_review_form")[0]);
                $.ajax({
                    url         : '/ajax/add_review_popup.php',
                    data        : formData,
                    cache       : false,
                    contentType : false,
                    processData : false,
                    type        : 'POST',
                    success     : function(data, textStatus, jqXHR){
						$("#popup_add_review .modal-body").html(data);
                    }
                });
                return false;
            }
        });

        $('#add_review_form .dashed_link').click(function() {
            var form_elem_first = $(this).closest('.add_review_form_social_list_outer').find('.form_element').eq(0).clone();

            $(this).closest('.add_review_form_social_list_outer').find('.add_review_form_social_list').append(form_elem_first);

            var form_elem = $(this).closest('.add_review_form_social_list_outer').find('.add_review_form_social_list').find('.form_element');

            for (var i = 0; i < form_elem.length; i++) {
                if (i == form_elem.length - 1) {
                    form_elem.eq(i).prepend('<span class="remove">&times;</span>');
                    form_elem.eq(i).find('input').val('');
                }
            }
        })
        $('.add_review_form_social_list').on('click', '.form_element .remove', function() {
            var ths = $(this).closest('.form_element');
            ths.css('opacity', 0);
            setTimeout(function() {
                ths.remove();
            }, 300);
        });
    }

    //лк смены пароля + фио
    $("#userpage_user_info_element_2 form").submit(function () {
        $.post("/ajax/lk/change_fio.php", $(this).serialize(), function (data) {
            $('#popup_lk_ok').modal('show');
        });
        return false;
    });
	$("#userpage_user_info_element_3 form").submit(function () {
        $.post("/ajax/lk/change_pw.php", $(this).serialize(), function (data) {
            if(data){
                $("#popup_lk_err").modal("show").find(".text-center").html(data);
            } else {
                $('#popup_lk_ok').modal('show');
            }
        });
        return false;
    });

    //удаление из избранного в лк
    $('.userpage_favorites_articles_list .icon-close').click(function() {
        var cur = this;
        $.post("/ajax/favorite.php", {"type": "delete", "id": $(cur).data("id")}, function (data) {
            var elem = $(cur).closest('li');

            elem.css('opacity', 0);
            setTimeout(function () {
                elem.slideUp(300);
            }, 300);
            setTimeout(function () {
                elem.remove();
            }, 600);

            updateFavoriteData();
        });
    });

    //открытие второго шага на странице обучения
    var stepsOpening = function() {
        $('.learn_page_steps').css('margin-bottom', 50);
        $('.learn_page_step.second_step').slideDown(300);
        $('.learn_page_bottom_more').slideDown(300);
    }

    if ($('.learn_page_review_form').length != 0) {
        $('.learn_page_review_form').validate({
            rules: {
                message: {required: true}
            },
            messages: {
                message: {
                    required: "Данное поле не заполнено"
                },
                email: {
                    required: "Данное поле не заполнено",
                    truemail: "Неверный формат данных"
                },
                phone: {
                    required: "Данное поле не заполнено",
                }
            },

            submitHandler: function(form) {
                $.post("/learning/send_access.php", {
                    "review": $("#access_text").val(),
                    "item": $("#access_text").data("item")
                }, function(data) {
                    $(".first_step").hide();
                    stepsOpening();
                });
            },

            errorElement: "span",
            errorPlacement: function(error, element) {
                error.prependTo( element.parent("div") );
            },
        });
    }

    $("#pay_me").click(function(){
        if($('.order_page_form').valid()) {
            if (!$(this).hasClass("disabled")) {
								overlayShow();
				let itemName = '';
				if($(".order_page_form input[name=item_name]").length){
					itemName = $(".order_page_form input[name=item_name]").val();
				} else if($("input[name=item_product_name]").length){
					itemName = $("input[name=item_product_name]").val();
				}
                $.post("/learning/get_pay_form.php", {
                    "item_name": itemName,
                    "item": $("#sem_id").val(),
                    "email": $(".check_mail").val(),
                    "pay_type": $(".order_page_form input[name=pay_type]:checked").val(),
                    "fio": $(".order_page_form input[name=name]").val(),
                    "phone": $(".order_page_form .selected-dial-code").text()+" "+$(".order_page_form input[name=phone]").val(),
                    "comment": $(".order_page_form textarea[name=message]").val(),
					"promocode": $(".order_page_form input[name=promocode]").val(),
					"bonuses": $(".order_page_form input[name=bonuses]").val()
                }, function (data) {
                    if (data) {
                        $("#hidden-div").append(data);
						if($("#hidden-div form").length){
						  	$("#hidden-div form").submit();
						}
						if($("#hidden-div a.hidden").length){
						 	location.href=$("#hidden-div a.hidden").data("href");
						}
                    }
                });
            }
        } else {
					if ($(this).siblings('.error_validation').length == 0) {
						$(this).before('<p class="error_validation red t12 mb30 text-left">Одно или несколько полей не заполнены, либо заполнены с ошибкой</p>');
					}
				}
        return false;
    });
    function chechMail(){
        $.post("/learning/check_mail.php", {email: $(".check_mail").val()}, function(data) {
            if(data=="Y"){
                $(".fio.password").slideDown();
            } else {
                $(".fio.password").hide();
            }
        });
    }

    $(".check_mail").keyup(function(){
        chechMail();
    });
    $(".check_mail").change(function(){
        chechMail();
    });
    $(".check_mail").click(function(){
        chechMail();
    });
    $(".check_mail").mouseleave(function(){
        chechMail();
    });
    $(".payor.login").click(function(){
        $(".fio.password .err").hide();
        $.post("/learning/auth.php", {email: $(".check_mail").val(), pas: $(".pass_val").val()}, function(data) {
            if(data=="Y"){
                $(".check_mail").parents(".fio").hide();
                $(".fio.password").hide();
            } else {
                $(".fio.password .err").show();
            }
        });
        return false;
    });

	//облагиции
	$(".calculate_table.obligations").on("click", ".more-results-wrap button", function(){
		updateObligations("page="+$(this).data("next"));
	});


	function initObligationsActivities(){
	$(".calculate_form #debstock_tab input, .calculate_form #debstock_tab select:not(.preset), .calculate_form #debstock_tab textarea").change(function(){
	 if($(this).attr('cancel_update')!="Y"){
		if($(this).attr("name") == "fin_analysis"){
			setTimeout(updateObligations, 500);
		} else {
			updateObligations();
		}
	  }
	});

	$(".calculate_form #debstock_tab .calculate_more").click(function(){
		$('#preset_obligations').attr('freezed','Y');
		setTimeout(function () {updateObligations(); $('#preset_obligations').attr('freezed','N');}, 500);

	});

	$('select#preset_obligations').on('change', function(){
		if($(this).attr('cancel_update')!="Y"){
     		  var radarObligationsPresets = new CRadarPreset(this);
			  var select = document.getElementById("preset_obligations"); // Выбираем  select по id
    		  var sel_value = select.options[select.selectedIndex].value; // Значение value для выбранного option
    		  var sel_text = select.options[select.selectedIndex].text; // Текстовое значение для выбранного option

			  radarObligationsPresets.doPreset();

			  if(sel_text=='Сбросить все'){ //После сброса сбрасываем выбор шаблона
				 resetPreset('preset_obligations');
			  }

			  }
   });

	$( ".rate_slider.ui_slider" ).on( "slidechange", function( event, ui ) {
		if($(this).attr('cancel_update')!="Y"){
		$(this).parents('.ui_slider_container').find('.rubber_outer input').val(ui.value);
		updateObligations("rate="+ui.value);
		}
	});
	$( ".time_slider.ui_slider" ).on( "slidechange", function( event, ui ) {
		if($(this).attr('cancel_update')!="Y"){
		$(this).parents('.ui_slider_container').find('.rubber_outer input').val(ui.value);
		updateObligations("duration="+ui.value);
		}
	});

 		  //Чекбокс на странице компаний над таблицей финпоков "В абсолютном выражении"
        $("body").on("change", '#compareTypeMoney_0, #compareTypeMoney_1', function( event, ui ) {
      		var checked = $(this).prop('checked');
      		console.log('!!!', checked);

			if(checked==true){
                toggleComparsionTablePercentSummValues('key_indicators_table', 'summ');
                toggleComparsionTablePercentSummValues('key_indicators_years_table', 'summ');
            } else {
                toggleComparsionTablePercentSummValues('key_indicators_table', 'prc');
                toggleComparsionTablePercentSummValues('key_indicators_years_table', 'prc');
            }
        });

		  //Чекбокс на странице компаний над таблицей финпоков "Скрывать текущий период"
        $("body").on("change", '#hideCurrentPeriodFinpoks', function( event, ui ) {
      		var checked = $(this).prop('checked');
			if(checked==true){
					$('.key_indicators_table .tableCurrentPeriodCell').addClass('hidden');
					$('.key_indicators_table .tableReservedPeriodCell').removeClass('hidden');
					if($('.key_indicators_industries_quarters_table').length>0){
						$('.key_indicators_industries_quarters_table .tableCurrentPeriodCell').addClass('hidden');
						$('.key_indicators_industries_quarters_table .tableReservedPeriodCell').removeClass('hidden');
					}

            } else {
					$('.key_indicators_table .tableReservedPeriodCell').addClass('hidden');
					$('.key_indicators_table .tableCurrentPeriodCell').removeClass('hidden');
					if($('.key_indicators_industries_quarters_table').length>0){
						$('.key_indicators_industries_quarters_table .tableReservedPeriodCell').addClass('hidden');
						$('.key_indicators_industries_quarters_table .tableCurrentPeriodCell').removeClass('hidden');
					}
            }
				$('body .key_indicators_table tr.active').click();
				if($('.key_indicators_industries_quarters_table').length>0){
					$('body .key_indicators_industries_quarters_table tr.active').click();
				}
        });

        $('body').on('select2:select', 'select.midval', function(e){
            //console.log('!!!!!!')
            var selected_val = $(this).val();
            var country = $(this).attr('data-country');
            var currency = $(this).attr('data-currency');
             if(selected_val=='sector'){
                $('.mid_vals_table.sector').removeClass('hidden');
                 $('.emitent_vals').addClass('hidden');
            } else if(selected_val=='') {
                $('.mid_vals_table.sector').addClass('hidden');
                $('.emitent_vals').addClass('hidden');
            } else {//ajax call to db industries quartals date
                 $('.mid_vals_table.sector').addClass('hidden');
                 $('.emitent_vals i').text('');
                 overlayShow();
                 $.ajax({
                     type: "POST",
                     url: "/ajax/sectors_industry/get_emitent_periods_data.php",
                     dataType: "json",
                     data:{'ajax': 'y', 'code': selected_val, 'country': country, 'currency': currency},
                     cashe: false,
                     async: false,
                     success: function(data){
                         getResponse(data);
                     }
                 });

                 function getResponse(data){
                     var arr = data;
                     fillEmitentValsTable('key_indicators_table', arr);
                     fillEmitentValsTable('key_indicators_years_table', arr);
                     $('.emitent_vals').removeClass('hidden');
                 }
                 overlayHide();

             }

            $('select.midval option[value="' + selected_val + '"]').not(this).prop('selected', true);
            $('select.midval').trigger('change.select2');
        });

        //TODO Убрать после проверки сравнения через выпадающие списки на стр. эмитентов
/*        $('input[name=show_industry_data]').on('change', function(){
            //var checked = $(this).find('input').prop('checked');
            var checked = $(this).is(':checked')?true:false;
            console.log('checked', checked);
            if(checked==true){
                $('.mid_vals_table').removeClass('hidden');
            } else {
                $('.mid_vals_table').addClass('hidden');
            }
            // alert('checkbox = '+checked);
            $( ".checkbox.midval-show input").prop('checked', checked);
        });*/

	}

	//Переключает денежное и процентные значения в таблицах при показе сравнения
    function toggleComparsionTablePercentSummValues(classTable, typeCompare){
        $('body').find('.'+classTable+' tbody tr ').each(function (){
			var tdCnt = 0;
            var name = '';
            $(this).find('td').each(function (){
                if(tdCnt==0){
                    //if($(this).find('.title').length==0){return false;}
                    let block = $(this).find('.mid_vals_table');
                    if(typeCompare=="summ"){
                        if(block.attr('data-noTogglePrc')=='N') {
                            block.find('i span').text(block.attr('data-summ'));
                        }
                    }
                    if(typeCompare=="prc"){
                        if(block.attr('data-noTogglePrc')=='N') {
                        block.find('i span').text(block.attr('data-prc'));
                        }
                    }
                    name = $(this).attr('data-propkey');
                } else {
                    let block = $(this).find('.mid_vals_table');
                    let blockEmitentCompare = $(this).find('.emitent_vals');
                    if(typeCompare=="summ"){
                        block.find('i').text(block.attr('data-summ'));
                        blockEmitentCompare.find('i').text(blockEmitentCompare.attr('data-summ'));
                    }
                    if(typeCompare=="prc"){
                        block.find('i').text(block.attr('data-prc'));
                        blockEmitentCompare.find('i').text(blockEmitentCompare.attr('data-prc'));
                    }

                }
                tdCnt++;
            });
        });
    }

	//Универсально заполняет таблицы периодов по кварталам и годам
	function fillEmitentValsTable(classTable, arr ){
        var period = '';
        var showSumm = true;
        if($( "body .checkbox.compareTypeMoney" ).length) {//TODO Убрать проверку после открытия из под админа
            var showSumm = $("body .checkbox.compareTypeMoney").find('input').prop('checked');
        }

        $('.'+classTable+' tbody tr ').each(function (){

            var tdCnt = 0;
            var name = '';
            var noTogglePrc = false; //НЕ переключать из процентного вида
            $(this).find('td').each(function (){

                if(tdCnt==0){
                    //if($(this).find('.title').length==0){return false;}
                    if($(this).find('.mid_vals_table').attr('data-noTogglePrc')=='Y'){
                        noTogglePrc = true;
                    }
                    name = $(this).attr('data-propkey');
                } else {
                    let emitent_vals_block = $(this).find('.emitent_vals');

                    let period = emitent_vals_block.attr('data-period');

                    if(period != undefined && arr[period] != undefined) {

                        if(arr[period][name]!=undefined) {

                            var cellValue = arr[period][name];
                            let valPercent = (Number(cellValue)/Number(emitent_vals_block.attr('data-origVal'))*100).toFixed(2);
                            if(noTogglePrc == true){
                                let valPercent = (Number(cellValue)/Number(emitent_vals_block.attr('data-origVal'))*100).toFixed(2);
                            }

                            if(showSumm==true ) {
                                    emitent_vals_block.find('i').text(Number(cellValue).toFixed(2));
                            } else if(showSumm==false && noTogglePrc == true) {
                                emitent_vals_block.find('i').text(Number(cellValue).toFixed(2));
                            } else if(showSumm==false &&noTogglePrc == false ){
                                emitent_vals_block.find('i').text(valPercent);
                            }
                            emitent_vals_block.attr('data-summ',Number(cellValue).toFixed(2));
                            emitent_vals_block.attr('data-prc', (noTogglePrc==true?Number(cellValue).toFixed(2):valPercent));


                        }
                        //emitent_vals_block.text(arr.period[name]);
                    }
                }
                tdCnt++;
            });

        });
        return false;
    }

	initObligationsActivities();

	// function updateObligations(dopParams = false){
	function updateObligations(dopParams){
		overlayShow();
		var table = $(".calculate_table.obligations"),
			dopParamsExp;

		if(dopParams){
			dopParamsExp = dopParams.split("=");
		}

		var params = [];
		//$(".calculate_form #debstock_tab input:visible, .calculate_form #debstock_tab textarea:visible, .calculate_form #debstock_tab select:visible").each(function(){
		$(".calculate_form #debstock_tab input:visible, .calculate_form #debstock_tab textarea:visible, .calculate_form #debstock_tab select:visible, #debstock_tab [name=period_value], #debstock_tab [name=month_value], .calculate_form #debstock_tab:visible .three_elements input, .calculate_form #debstock_tab:visible .three_elements select").each(function(){
			var val = $(this).val();
			if($(this).attr("type")=="checkbox"){
				val = "";
				if($(this).is(":checked")){
					if($(this).attr("name")=="type[]"){
						val = $(this).val();
					} else {
						val = "y";
					}
				}
			}
			if(dopParamsExp){
				if($(this).attr("name")!=dopParamsExp[0]){
					params.push($(this).attr("name")+"="+val);
				}
			} else {
				params.push($(this).attr("name")+"="+val);
			}
		});
		$(".calculate_form #debstock_tab .tab_container:not(.active) select").each(function(){
			if($(this).prev().attr("name")){
				params.push($(this).prev().attr("name")+"="+$(this).prev().val());
			}
		});
		if(dopParams){
			params.push(dopParams);
		}

		$.get("", params.join("&")+"&ajax_obligations=y", function(data){
			if(dopParamsExp){
				if(dopParamsExp[0]!="page"){
					$(table).find(".smart_table tbody").empty();
				}
			} else {
				$(table).find(".smart_table tbody").empty();
			}
			$(table).find(".calculate_step_title").html($(data).find(".calculate_step_title").html());
			$(table).find(".smart_table tbody").append($(data).find(".smart_table tbody").html());
			$(table).find(".more-results-wrap").html($(data).find(".more-results-wrap").html());

			calcMain();
			checkRadio();
			calculateTableActiveName();
			calculateTableCompanyName();
			overlayHide();
			rebuildTooltips();
		});

		if(dopParamsExp){
			if(dopParamsExp[0]!="page") //Если переданы параметры для обновления но это не кнопка "Показать еще"
	   	  resetPreset('preset_obligations');//Если изменили фильтр вручную сбросим выбранный шаблон, т.к. его параметры изменены вручную.
		} else {
			  resetPreset('preset_obligations');//Если изменили фильтр вручную сбросим выбранный шаблон, т.к. его параметры изменены вручную.
		}
	}

	//акции
	$(".calculate_table.actions").on("click", ".more-results-wrap button", function(){
		updateActions("page="+$(this).data("next"));
	});

	function resetPreset(id){
	  if($('#'+id).attr('freezed')!='Y'){  //Если изменения вносятся шаблоном - то селект заморожен от сброса, пока шаблон полностью не применится
	  $('#'+id).val('').change();
	  }
	}

  	function initActionsActivities(){
	  $(".calculate_form #shares_tab input, .calculate_form #shares_tab select:not(.exchange_id, .preset), .calculate_form #shares_tab textarea, .calculate_form #shares_tab .three_elements input").on('change', function(){
	   if($(this).attr('cancel_update')!="Y"){
	   if($(this).attr("name") == "analitics"){
			setTimeout(updateActions, 500);
		} else {
			updateActions();
		}
		}
     });

	$('select#preset_actions').on('change', function(){
		if($(this).attr('cancel_update')!="Y"){
     		  var radarActionsPresets = new CRadarPreset(this);
			  var select = document.getElementById("preset_actions"); // Выбираем  select по id
    		  var sel_value = select.options[select.selectedIndex].value; // Значение value для выбранного option
    		  var sel_text = select.options[select.selectedIndex].text; // Текстовое значение для выбранного option

			  radarActionsPresets.doPreset();

			  if(sel_text=='Сбросить все'){ //После сброса сбрасываем выбор шаблона
				 resetPreset('preset_actions');
			  }

			  }
   });

	$(".calculate_form #shares_tab .calculate_more").on('click', function(){
	  $('#preset_actions').attr('freezed','Y');
	  setTimeout(function(){updateActions(); $('#preset_actions').attr('freezed','N');}, 1000);

	});

	$( ".coefficient_slider.ui_slider" ).on( "slidechange", function( event, ui ) {
		if($(this).attr('cancel_update')!="Y"){
		$(this).parents('.ui_slider_container').find('.rubber_outer input').val(ui.value);
		//$('#coefficient_slider_input').val(ui.value);
		updateActions("pe="+ui.value);
		}
	});

	$( ".profitability_slider.ui_slider" ).on( "slidechange", function( event, ui ) {
		if($(this).attr('cancel_update')!="Y"){
		//$('#profitability_slider_input').val(ui.value);
		$(this).parents('.ui_slider_container').find('.rubber_outer input').val(ui.value);
		updateActions("profitability="+ui.value);
		}
	});

	}



	initActionsActivities();


	function updateActions(dopParams){
	  	console.log('caller: ', updateActions.caller);
		//compatible with safari5 - it not support default params setting into function arguments..
		console.log(dopParams);
		if(dopParams == undefined){
			dopParams = false;
		}
		overlayShow();
		var table = $(".calculate_table.actions"),
			dopParamsExp;

		if(dopParams){
			dopParamsExp = dopParams.split("=");
		}

	  var params = [];
		//$(".calculate_form #shares_tab input:visible, .calculate_form #shares_tab textarea:visible, .calculate_form #shares_tab select:visible").each(function(){
		$(".calculate_form #shares_tab input:visible, .calculate_form #shares_tab textarea:visible, .calculate_form #shares_tab select:visible:not(.preset_actions), #shares_tab [name=period_value], #shares_tab [name=month_value], .calculate_form #shares_tab:visible .three_elements input, .calculate_form #shares_tab:visible .three_elements select").each(function(){
			var val = $(this).val();
			if($(this).attr("type")=="checkbox"){
				val = "";
				if($(this).is(":checked")){
					if($(this).attr("name")=="industry[]"){
						val = $(this).val();
					} else {
						val = "y";
					}
				}
			}

			if(dopParamsExp){
 				if($(this).attr("name")!=dopParamsExp[0]){
					params.push($(this).attr("name")+"="+val);
				}
			} else {
				params.push($(this).attr("name")+"="+val);
			}
		});

		$(".calculate_form #shares_tab .tab_container:not(.active) select").each(function(){
			if($(this).prev().attr("name")){
				params.push($(this).prev().attr("name")+"="+$(this).prev().val());
			}
		});
		if(dopParams){
			console.log(dopParams);
		 	params.push(dopParams);
		}
		  params = params.join("&");
		  console.log(params);
		 //	console.log('dopParamsExp', dopParamsExp);
		$.get("", params+"&ajax_actions=y", function(data){
			if(dopParamsExp){
				if(dopParamsExp[0]!="page"){
					$(table).find(".smart_table tbody").empty();
				}
			} else {
				$(table).find(".smart_table tbody").empty();
			}
			$(table).find(".calculate_step_title").html($(data).find(".calculate_step_title").html());
			$(table).find(".smart_table tbody").append($(data).find(".smart_table tbody").html());
			$(table).find(".more-results-wrap").html($(data).find(".more-results-wrap").html());

			calcMain();
			checkRadio();
			calculateTableActiveName();
			calculateTableCompanyName();
			overlayHide();
			rebuildTooltips();
		});
	
		if(dopParamsExp){
			if(dopParamsExp[0]!="page") //Если переданы параметры для обновления но это не кнопка "Показать еще"
	   	  resetPreset('preset_actions');//Если изменили фильтр вручную сбросим выбранный шаблон, т.к. его параметры изменены вручную.
		} else {
			  resetPreset('preset_actions');//Если изменили фильтр вручную сбросим выбранный шаблон, т.к. его параметры изменены вручную.
		}
	}


	//Акции США
	$(".calculate_table.actions_usa").on("click", ".more-results-wrap button", function(){
		updateActionsUSA("page="+$(this).data("next"));
	});

	function initActionsUSAActivities(){

	$(".calculate_form #shares_usa_tab input, .calculate_form #shares_usa_tab select:not(.exchange_id, .preset), .calculate_form #shares_usa_tab textarea, .calculate_form #shares_usa_tab .three_elements input").change(function(){
    if($(this).attr('cancel_update')!="Y"){
   	if($(this).attr("name") == "analitics"){
			setTimeout(updateActionsUSA, 500);
		} else {
        var  noClearPreset = 'N';
        if($(this).hasClass('noClearPreset')){
            noClearPreset = 'Y';
        }

        updateActionsUSA('',noClearPreset);
		}
	  }
	});

	$('select#preset_actions_usa').on('change', function(){
		if($(this).attr('cancel_update')!="Y"){
     		  var radarActionsUSAPresets = new CRadarPreset(this);
			  var select = document.getElementById("preset_actions_usa"); // Выбираем  select по id
    		  var sel_value = select.options[select.selectedIndex].value; // Значение value для выбранного option
    		  var sel_text = select.options[select.selectedIndex].text; // Текстовое значение для выбранного option

			  radarActionsUSAPresets.doPreset();

			  if(sel_text=='Сбросить все'){ //После сброса сбрасываем выбор шаблона
				 resetPreset('preset_actions_usa');
			  }

			  }
   });

	$(".calculate_form #shares_usa_tab .calculate_more").click(function(){
		$('#preset_actions_usa').attr('freezed','Y');
		setTimeout(function(){updateActionsUSA(); $('#preset_actions_usa').attr('freezed','N');}, 1000);

	});

	$( ".coefficient_slider_usa.ui_slider" ).on( "slidechange", function( event, ui ) {
		if($(this).attr('cancel_update')!="Y"){
		$(this).parents('.ui_slider_container').find('.rubber_outer input').val(ui.value);
		updateActionsUSA("pe="+ui.value);
		}
	});

	$( ".profitability_slider_usa.ui_slider" ).on( "slidechange", function( event, ui ) {
		if($(this).attr('cancel_update')!="Y"){
		//console.log('uiVal=',ui.value);
		$(this).parents('.ui_slider_container').find('.rubber_outer input').val(ui.value);
		updateActionsUSA("profitability="+ui.value);
		}
	});

	}
	initActionsUSAActivities();

	function updateActionsUSA(dopParams, noClearPreset='N'){
		//compatible with safari5 - it not support default params setting into function arguments..
		if(dopParams == undefined){
			dopParams = false;
		}
		overlayShow();
		var table = $(".calculate_table.actions_usa"),
			dopParamsExp;

		if(dopParams){
			dopParamsExp = dopParams.split("=");
		}
	   console.log('prof=',$('#profitability_slider_usa_input').val());
		var params = [];
		//$(".calculate_form #shares_tab input:visible, .calculate_form #shares_tab textarea:visible, .calculate_form #shares_tab select:visible").each(function(){
		$(".calculate_form #shares_usa_tab input:visible, .calculate_form #shares_usa_tab textarea:visible, .calculate_form #shares_usa_tab select:visible:not(.preset), #shares_usa_tab [name=period_value], #shares_usa_tab [name=month_value], .calculate_form #shares_usa_tab:visible .three_elements input, .calculate_form #shares_usa_tab:visible .three_elements select").each(function(){
		  //	console.log($(this).attr("name"));
			var val = $(this).val();
/*			if($(this).attr('id')=='profitability_slider_usa_input'){
			 console.log('slider_param=',$('#profitability_slider_usa_input').slider().val());
			}*/
			if($(this).attr("type")=="checkbox"){
				val = "";
				if($(this).is(":checked")){
					if($(this).attr("name")=="sector[]"){
						val = $(this).val();
					} else {
						val = "y";
					}
				}
			}

			if(dopParamsExp){
				if($(this).attr("name")!=dopParamsExp[0]){
					params.push($(this).attr("name")+"="+val);
				}
			} else {
				params.push($(this).attr("name")+"="+val);
			}
		});
		$(".calculate_form #shares_usa_tab .tab_container:not(.active) select").each(function(){
			if($(this).prev().attr("name")){
				params.push($(this).prev().attr("name")+"="+$(this).prev().val());
			}
		});
		if(dopParams){
			params.push(dopParams);
		}

		$.get("", params.join("&")+"&ajax_actions_usa=y", function(data){
			if(dopParamsExp){
				if(dopParamsExp[0]!="page"){
					$(table).find(".smart_table tbody").empty();
				}
			} else {
				$(table).find(".smart_table tbody").empty();
			}
			$(table).find(".calculate_step_title").html($(data).find(".calculate_step_title").html());
			$(table).find(".smart_table tbody").append($(data).find(".smart_table tbody").html());
			$(table).find(".more-results-wrap").html($(data).find(".more-results-wrap").html());

			calcMain();
			checkRadio();
			calculateTableActiveName();
			calculateTableCompanyName();
			overlayHide();
			rebuildTooltips();
		});

		if(dopParamsExp){
			if(dopParamsExp[0]!="page") //Если переданы параметры для обновления но это не кнопка "Показать еще"
	   	  resetPreset('preset_actions_usa');//Если изменили фильтр вручную сбросим выбранный шаблон, т.к. его параметры изменены вручную.
		} else {
            if(noClearPreset!='Y')
			  resetPreset('preset_actions_usa');//Если изменили фильтр вручную сбросим выбранный шаблон, т.к. его параметры изменены вручную.
		}

	}

	//etf
	$(".calculate_table.etf").on("click", ".more-results-wrap button", function(){
		updateETF("page="+$(this).data("next"));
	});


  function initETFActivities(){
	$(".calculate_form #etf_tab input, .calculate_form #etf_tab select:not(.exchange_id, .preset), .calculate_form #etf_tab textarea, .calculate_form #etf_tab .three_elements input").on('change', function(){
   if($(this).attr('cancel_update')!="Y"){
	 if($(this).attr("name") == "analitics"){
			setTimeout(updateETF, 500);
		} else {
			updateETF();
		}
	 }
	});

	$('select#preset_etf').on('change', function(){
		if($(this).attr('cancel_update')!="Y"){
     		  var radarETFPresets = new CRadarPreset(this);
			  var select = document.getElementById("preset_etf"); // Выбираем  select по id
    		  var sel_value = select.options[select.selectedIndex].value; // Значение value для выбранного option
    		  var sel_text = select.options[select.selectedIndex].text; // Текстовое значение для выбранного option

			  radarETFPresets.doPreset();

			  if(sel_text=='Сбросить все'){ //После сброса сбрасываем выбор шаблона
				 resetPreset('preset_etf');
			  }

			  }
   });

	$(".calculate_form #etf_tab .calculate_more").click(function(){
		$('#preset_etf').attr('freezed','Y');
		setTimeout(function (){updateETF(); $('#preset_etf').attr('freezed','N');}, 1000);
	});


	$( ".ETF_AVG_ICNREASE_slider.ui_slider" ).on( "slidechange", function( event, ui ) {
	  if($(this).attr('cancel_update')!="Y"){
	  	$(this).parents('.ui_slider_container').find('.rubber_outer input').val(ui.value);
		updateETF("ETF_AVG_ICNREASE="+ui.value);
	  }
	});
	$( ".ETF_COMISSION_slider.ui_slider" ).on( "slidechange", function( event, ui ) {
	  if($(this).attr('cancel_update')!="Y"){
	  	$(this).parents('.ui_slider_container').find('.rubber_outer input').val(ui.value);
		updateETF("ETF_COMISSION="+ui.value);
	  }
	});

  }
	initETFActivities();

	function updateETF(dopParams){
		//compatible with safari5 - it not support default params setting into function arguments..
		if(dopParams == undefined){
			dopParams = false;
		}
		overlayShow();
		var table = $(".calculate_table.etf"),
			dopParamsExp;

		if(dopParams){
			dopParamsExp = dopParams.split("=");
		}

		var params = [];
		//$(".calculate_form #shares_tab input:visible, .calculate_form #shares_tab textarea:visible, .calculate_form #shares_tab select:visible").each(function(){
		$(".calculate_form #etf_tab input:visible, .calculate_form #etf_tab textarea:visible, .calculate_form #etf_tab select:visible, #etf_tab [name=period_value], #etf_tab [name=month_value], .calculate_form #etf_tab:visible .three_elements input, .calculate_form #etf_tab:visible .three_elements select").each(function(){
			//console.log($(this).attr("name"));
			var val = $(this).val();
			if($(this).attr("type")=="checkbox"){
				val = "";
				if($(this).is(":checked")){
					if($(this).attr("name")=="industry[]"){
						val = $(this).val();
					} else {
						val = "y";
					}
				}
			}

			if(dopParamsExp){
				if($(this).attr("name")!=dopParamsExp[0]){
					params.push($(this).attr("name")+"="+val);
				}
			} else {
				params.push($(this).attr("name")+"="+val);
			}
		});
		$(".calculate_form #etf_tab .tab_container:not(.active) select").each(function(){
			if($(this).prev().attr("name")){
				params.push($(this).prev().attr("name")+"="+$(this).prev().val());
			}
		});
		if(dopParams){
			params.push(dopParams);
		}

		$.get("", params.join("&")+"&ajax_etf=y", function(data){
			if(dopParamsExp){
				if(dopParamsExp[0]!="page"){
					$(table).find(".smart_table tbody").empty();
				}
			} else {
				$(table).find(".smart_table tbody").empty();
			}
			$(table).find(".calculate_step_title").html($(data).find(".calculate_step_title").html());
			$(table).find(".smart_table tbody").append($(data).find(".smart_table tbody").html());
			$(table).find(".more-results-wrap").html($(data).find(".more-results-wrap").html());

			calcMain();
			checkRadio();
			calculateTableActiveName();
			calculateTableCompanyName();
			overlayHide();

            rebuildTooltips();
		});
		if(dopParamsExp){
			if(dopParamsExp[0]!="page") //Если переданы параметры для обновления но это не кнопка "Показать еще"
	   	  resetPreset('preset_etf');//Если изменили фильтр вручную сбросим выбранный шаблон, т.к. его параметры изменены вручную.
		} else {
			  resetPreset('preset_etf');//Если изменили фильтр вручную сбросим выбранный шаблон, т.к. его параметры изменены вручную.
		}

	}//etf


	//всплывающий попап блок правый блок
	if($(".popup_message").length){
		var item = $(".popup_message");
		if(item.data("needopen")){
			setTimeout(function(){
				$(".popup_message").addClass("opened");
				$.cookie('fly_right_banner', 'Y', { expires: $(".popup_message").data("life") });
			}, item.data("open")*1000);
		}
	}

	//форма входа в радар
	$('.loginpage_form').submit(function(){
		if($(this).valid()){
			$(this).remove();

			$.post("/ajax/radar_lk.php", $(this).serialize(), function(data) {
				$("#success-login-radar").modal("show");
			});

			return false;
		}
	});

	//промокоды
	if($(".order_form_promocode_inner").length){
		$(".order_form_promocode_inner button").click(function(){
			var parent = $(".order_form_promocode_inner"),
				final_price = $(".final_price span");

			parent.find(".mes").empty();

			$.post("/ajax/promocodes.php", {
					"code": parent.find("input").val(),
					"summ": $(".order_start_price span").text()
				}, function(data){
				if(data.err){
					parent.find(".mes").html(data.err);
				} else {
					parent.find(".mes").html(data.success);
					parent.find("button").remove();
					parent.find("input").prop("disabled", true);

					if(!isNaN(data.final_summ)){
					$('#start_price').val(data.final_summ);
					}
					finalPriceSumm();
				}
			}, "json");
			return false;
		});
	}

    //автокомплит акций/облигаций
    if($("#autocomplete_radar").length && $('#ajax_radar_search').length) {
        $('#autocomplete_radar').autocomplete({
            serviceUrl: '/ajax/obligations/radar.php',
            onSelect: function (suggestion) {
                console.log(suggestion);
            },
            minChars: 2,
            params: {
                type: function () {
                    //return $(".tab-pane.fade.active").attr("id");
                    return $(".tab-pane.active").attr("id");
                },
				inview: function () {
					var items = [];
					$(".smart_table:visible tr").each(function(){
						if($(this).data("id")){
							items.push($(this).data("id"));
						}
					});
                    return items;
                }
            },
            appendTo: '#ajax_radar_search',
            onSearchStart: function (params) {
                $("#ajax_radar_search").find(".search-form__list").remove();
            },
            beforeRender: function (container) {
                $("#ajax_radar_search").find(".search-form__list").remove();

                var items = [],
                    html = '<div class="search-form__list customscroll opened"><ul>';

                $(container).css({"visibility": "hidden"}).find(".autocomplete-suggestion").each(function () {
                    items.push($(this).html().replace(new RegExp("<strong>", 'g'), "<span>").replace(new RegExp("</strong>", 'g'), "</span>"));
                });


                $(items).each(function () {
                    var data = this.split("--");
                    data[1] = data[1].replace(/<\/?[^>]+(>|$)/g, "");
                    data[2] = data[2].replace(/<\/?[^>]+(>|$)/g, "");

                    html += '<li>' + data[0] + ' <button data-href="' + data[2] + '" data-item="' + data[1] + '" type="button" class="search-form__list_plus icon icon-arr_right"></button></li>';
                });

                html += '</ul></div>';

                $("#ajax_radar_search").append(html);

                $(".customscroll").mCustomScrollbar({
                    scrollbarPosition: 'outside'
                });
            }
        }).val("");

        $("#ajax_radar_search").on("click", "li", function (e) {
            event.stopPropagation();
            var type = $(".tab-pane.active").attr("id");

            $.post('/ajax/obligations/radar_add_item.php', {
                "secid": $(this).find("button").data("item"),
                "type": $(".tab-pane.active").attr("id")
            }, function (data) {
                if (type == "debstock_tab") {
                    $(".calculate_table.obligations tbody").prepend(data);
                } else if (type == "shares_tab"){
                    $(".calculate_table.actions tbody").prepend(data);
                } else if (type == "shares_usa_tab"){
                    $(".calculate_table.actions_usa tbody").prepend(data);
                } else if (type == "etf_tab"){
                    $(".calculate_table.etf tbody").prepend(data);
                }

                calcMain();
                checkRadio();
					 calculateTableActiveName();
                calculateTableCompanyName();
					 rebuildTooltips();
                overlayHide();
            });
        });
        $("#ajax_radar_search").on("click", "li button", function (event) {
            location.href = $(this).data("href");
            e.preventDefault();
        });
    }

});


