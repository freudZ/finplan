//Скрипт анализа портфеля

// скролл к якорю по клику пункта меню
// var analysisMenu = document.querySelectorAll('.portfolioAnalysis__menu button');
//
// if (analysisMenu.length > 0) {
// 	analysisMenu.forEach(function(link) {
// 		var block = document.querySelector(link.getAttribute('data-id'));
//
// 		link.addEventListener('mousedown', function() {
// 			console.log('aaa')
// 		})
//
// 		console.log('!!!', link, block)
//
// 		// link.addEventListener('click', function () {
// 		// 	console.log('aaa')
// 		// 	// block.scrollIntoView({
// 		// 	// 	behavior: "smooth",
// 		// 	// 	block:    "start"
// 		// 	// });
// 		// });
// 	});
// }
function goToBlock(id) {
	var block = document.querySelector(id);
	block.scrollIntoView({
		behavior: "smooth",
		block:    "start"
	});
}


//document.addEventListener('DOMContentLoaded', function(){
$(window).load(function() {
	analyseTab = document.querySelector('.portfolio_tabs .tab-pane.analyse_tab');
	if(!!analyseTab){
		activeAnalyseTab = analyseTab.classList.contains('active');
		if(activeAnalyseTab){
		  analysePortfolio(); //Запуск анализа портфеля
		}
	}
});

//Фиксированные цвета для кругового графика
var colors_analyse_arr = [];
colors_analyse_arr.push('#DDDDDD');//кеш
colors_analyse_arr.push('#339900');//облиги или облиги в рублях
colors_analyse_arr.push('#33CC33');//облиги в usd
colors_analyse_arr.push('#FFFF33');//акции или акции и етф в usd
colors_analyse_arr.push('#FF9900');//акции и етф в рублях


//Запуск анализа портфеля
function analysePortfolio(){

  //Проверки для блока 1
  checkBlock1('an_portfolio_common_squand');//Проверяем условиия для блока 1 (Риск)
  checkBlock1('an_portfolio_expected_return');//Проверяем условиия для блока 1 (Доходность)

  //График для блока 1
  var chartBlock1_arr = getChartBlock1Array();
  roundAnalyseChartUpdate(chartBlock1_arr, '.round_chart_outer_b1', 'an_b_1_1_chart', 'doughnut', true);

  //Проверки для блока 2
  checkBlock2('proportion_actives');//Проверяем условиия для блока 2 (Валютный баланс)

  //График для блока 2
  var chartBlock2_arr = getChartBlock2Array();
  roundAnalyseChartUpdate(chartBlock2_arr, '.round_chart_outer_b2', 'an_b_1_2_chart', 'doughnut', true);

  checkBlock1('an_shares_cnt', true);//Проверяем кол-во акций РФ
  checkBlock1('an_shares_usa_cnt', true);//Проверяем кол-во акций США
  checkBlock1('an_bonds_cnt', true);//Проверяем кол-во облиг РФ
  checkBlock1('an_bonds_euro_cnt', true);//Проверяем кол-во еврооблиг

	//График для блока 4
   var chartBlock4_arr = getChartBlock4Array();
	roundAnalyseChartUpdate(chartBlock4_arr, '.round_chart_outer_b4', 'an_b_1_4_chart', 'doughnut', false);

	//Проверка для блока 4 (Отраслевая диверсификация)
	checkBlock4();

}

//Общая функция проверки параметров для блока 1
function checkBlock1(paramName = '', hideWithNull=false){
  //console.log('arC paramName', paramName);
	var x = document.querySelector('.'+paramName).getAttribute('value');
	var x_isFinite = isFinite(x);
  //console.log('arC '+paramName+' x', x);
  //console.log('arC '+paramName+' isFinite', x_isFinite);
	if(x_isFinite){
	  var arConditions = getAnLibConditions(paramName);
	  //console.log('paramName= '+paramName+' arConditions:', arConditions);
	  if(arConditions.length ){
	  	for (var nodeCondition of arConditions) {
		  if(eval(nodeCondition.getAttribute('data-condition'))){
		  	var content = nodeCondition.innerHTML;
			content = content.replace('[x]', x);
			if('an_portfolio_common_squand'==paramName){
				var y = document.querySelector('.an_bonds_part').getAttribute('value');
				content = content.replace('[y]', y);
			}
			document.querySelector('.anLegend_'+paramName+' span').innerHTML = content;
			if(hideWithNull && x==0){
			 document.querySelector('.anLegend_'+paramName).classList.add('hidden');
			}
			break;
		  }
	  	}
	  }
	}
}


//Функция проверки параметров для блока 2
function checkBlock2(paramName = ''){
	var x = Number(document.querySelector('.an_valute_part').getAttribute('value')).toFixed(2);

	var x_isFinite = isFinite(x);
  //console.log('arC '+paramName+' x', x);
  //console.log('arC '+paramName+' isFinite', x_isFinite);
	if(x_isFinite){
	  var rub = 100-x;
	  var arConditions = getAnLibConditions(paramName);
	  if(arConditions.length){
	  	for (var nodeCondition of arConditions) {
		  if(eval(nodeCondition.getAttribute('data-condition'))){
		  	var content = nodeCondition.innerHTML;
			content = content.replace('[x]', x);
			content = content.replace('[rub]', Number(rub).toFixed(2));
			document.querySelector('.anLegend_'+paramName+' span').innerHTML = content;
			break;
		  }
	  	}
	  }
	}
}

//Проверка для блока 4 (Отраслевая диверсификация)
function checkBlock4(){
	var arSectors = document.querySelectorAll('.an_sectors_lib');
//	console.log('arSectors', arSectors);
	for(var i=3; i>0; i--){

		var arConditions = getAnLibConditions('an_sector_divers_'+i);
		var x = document.querySelectorAll('.an_sector_divers_'+i)[0].getAttribute('value');
		var eval_found = false;
		var key = 0;
		for (var nodeCondition of arConditions) {
			if(eval(nodeCondition.getAttribute('data-condition'))){
			  	var content = nodeCondition.innerHTML;
			   if(arSectors.length){
				  		var element = arSectors[key];
						if(i>1){
							let sect_x = arSectors[0].getAttribute('data-sector');
							content = content.replace('[sect_x]', sect_x);
						}
						if(i>2){
							if(!!arSectors[1]){
								let sect_y = arSectors[1].getAttribute('data-sector');
								content = content.replace('[sect_y]', sect_y);
								}
							if(!!arSectors[2]){
								let sect_z = arSectors[2].getAttribute('data-sector');
								content = content.replace('[sect_z]', sect_z);
								}


						}
						document.querySelector('.anLegend_sector_divers span').innerHTML = content;
				}
				eval_found = true;
				break;
			}
		}

		if(eval_found) break;
		key++;
	}

}

//Достаем условия и контент для проверяемого параметра
function getAnLibConditions(paramName=''){
	var arConditions = [];
		if(paramName.length)
		 arConditions = document.querySelectorAll('.anlib.param_'+paramName);
	return arConditions;
}

//Собираем массив для кругового графика блока 1
function getChartBlock1Array(){
  var chart_array = [];
  var free_money = 100;
  var portfolio_currencies_bonds = document.querySelector('.an_currency_part').getAttribute('value'); //Доля кеша
  var portfolio_deposits_bonds = document.querySelector('.an_bonds_part').getAttribute('value'); //Доля облиг
  var portfolio_deposits_euro_bonds = document.querySelector('.an_bonds_euro_part').getAttribute('value'); //Доля еврооблиг
  var portfolio_shares_bonds = document.querySelector('.an_shares_part').getAttribute('value'); //Доля акций РФ
  var portfolio_shares_usa = document.querySelector('.an_shares_usa_part').getAttribute('value'); //Доля акций США
  var portfolio_etf_shares_bonds = document.querySelector('.an_shares_etf_part').getAttribute('value'); //Доля ETF
  var common_actions = Number(portfolio_shares_bonds)+Number(portfolio_shares_usa)+Number(portfolio_etf_shares_bonds);
	 //кеш
    var chart_donut_arr_element = [];
    var chart_donut_arr_element_val = (Number(portfolio_currencies_bonds) * 100) / free_money;
    chart_donut_arr_element.push("Кеш", chart_donut_arr_element_val.toFixed(1), colors_analyse_arr[0]);
    if(chart_donut_arr_element_val>0) chart_array.push(chart_donut_arr_element);
	 //Облиги
    var chart_donut_arr_element = [];
    var chart_donut_arr_element_val = ((Number(portfolio_deposits_bonds)+Number(portfolio_deposits_euro_bonds)) * 100) / free_money;
    chart_donut_arr_element.push("Облигации", chart_donut_arr_element_val.toFixed(1), colors_analyse_arr[1]);
    if(chart_donut_arr_element_val>0) chart_array.push(chart_donut_arr_element);
	 //Акции
    var chart_donut_arr_element = [];
    var chart_donut_arr_element_val = (common_actions * 100) / free_money;
    chart_donut_arr_element.push("Акции (или ETF на акции)", chart_donut_arr_element_val.toFixed(1), colors_analyse_arr[3]);
    if(chart_donut_arr_element_val>0) chart_array.push(chart_donut_arr_element);
  return chart_array;
}

//Массив для графика валютного баланса
function getChartBlock2Array(){
  var chart_array = [];
  var free_money = 100;
  var valute_part = Number(document.querySelector('.an_valute_part').getAttribute('value'));

	 //Активы в руб
    var chart_donut_arr_element = [];
    var chart_donut_arr_element_val = 100-valute_part;
    chart_donut_arr_element.push("Активы в руб.", chart_donut_arr_element_val.toFixed(2), colors_analyse_arr[1]);
    if(chart_donut_arr_element_val>0) chart_array.push(chart_donut_arr_element);

	 //Активы в валюте
    var chart_donut_arr_element = [];
    var chart_donut_arr_element_val = valute_part;
    chart_donut_arr_element.push("Активы в валюте", chart_donut_arr_element_val.toFixed(2), colors_analyse_arr[3]);
    if(chart_donut_arr_element_val>0) chart_array.push(chart_donut_arr_element);
  return chart_array;
}

//Массив для графика диверсификации по секторам
function getChartBlock4Array(){
  var chart_array = [];
  var valute_part = Number(document.querySelector('.an_valute_part').getAttribute('value'));
  var arSectors = document.querySelectorAll('.an_sectors_lib');
  var arSectorSumm = Number(document.querySelectorAll('.an_arSectorsCSumm')[0].getAttribute('value'));
	  if(arSectors.length){
		//Случайные цвета для кругового графика
		var colors_analyse_big_arr = [];
			 color_array = document.querySelectorAll('.sectors_colors input');
			 for (var color_sect of color_array) {
				colors_analyse_big_arr.push(color_sect.getAttribute('value'));
			 }	  	
	  	//for (var key in arSectors) {
	  	for (var key = 0; key < arSectors.length; key++) {
	  		var element = arSectors[key];
		    var chart_donut_arr_element = [];
		    var chart_donut_arr_element_val = Number(element.getAttribute('data-prc'));
		    //var chart_donut_arr_element_val = (Number(element.getAttribute('value')) / arSectorSumm) * 100  ;
			// console.log('otr '+element.getAttribute('value')+' / '+arSectorSumm+' * 100 = '+chart_donut_arr_element_val);
			 var chart_donut_arr_name = element.getAttribute('data-sector');
		    chart_donut_arr_element.push(chart_donut_arr_name, chart_donut_arr_element_val.toFixed(2), colors_analyse_big_arr[key]);
		    if(chart_donut_arr_element_val>0) chart_array.push(chart_donut_arr_element);

	  	}
	  }

  return chart_array;
}


// вывод кругового графика
function roundAnalyseChartUpdate(chart_donut_arr, chartOuter = '.round_chart_outer', chartId = 'round_chart', type = 'doughnut', show_legend=false) {
	$('#'+chartId).remove();
	$(chartOuter).append('<canvas id="'+chartId+'"></canvas>');
	var round_chart = document.querySelector('#'+chartId);
	var round_chart_data = {
		type: type,
		data: {
			labels: [],
			datasets: []
		},
		options: {
			legend: {
				display: show_legend
			},
            aspectRatio: 1.2,
			rotation: 4.72,
			tooltips: {
				callbacks: {
				label: function(tooltipItem, data) {
					var allData = data.datasets[tooltipItem.datasetIndex].data;
					var tooltipLabel = data.labels[tooltipItem.index];
					var tooltipData = allData[tooltipItem.index];
					var total = 0;
					for (var i in allData) {
						total += allData[i];
					}
					var tooltipPercentage = Math.round((tooltipData / total) * 100);
					return tooltipLabel + ': ' + tooltipData + '%';
				}
				}
			},
		}
	}

	round_chart_data.data.labels = [];
	round_chart_data.data.datasets = [];

	var round_chart_values = [],
			round_chart_colors = [];
	for (var i = 0; i < chart_donut_arr.length; i++) {
		round_chart_data.data.labels.push(chart_donut_arr[i][0]);
		round_chart_values.push(chart_donut_arr[i][1]);
		round_chart_colors.push(chart_donut_arr[i][2]);
	}
	round_chart_data.data.datasets.push({
		data: round_chart_values,
		borderWidth: 1,
		backgroundColor: round_chart_colors
	});

	var roundChart = new Chart(round_chart, round_chart_data);
	//console.log('Маccив данных кругового графика: ', chart_donut_arr);
}
