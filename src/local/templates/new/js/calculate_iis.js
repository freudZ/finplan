if ($('#detailInput').length !== 0) {
    $('#detailInput').change(function (e) {
        $('#input_iis').prop('disabled', $(this).prop('checked'));
    });
}
if ($('#input_iis').length !== 0) {
    var input_iis = parseInt($('#input_iis').val());
    var input_period = parseInt($('#input_period').val());
    var text = '';

    for (var i = 0; i <= input_period - 1; i++) {

        text = "Год " + Number(i+1) + ":";

        if(i === 0) {
            text = "Первоначальный капитал";
        }

        if(i === 1) {
            text += "<br> (на начало года)";
        }

        $('#iis_detail_modal_list').append(
            '<div class="flex_row row">' +
            '<div class="col col_left col-xs-5">' +
            '<p class="t16 white m0">' + text + '</p>' +
            '</div>' +
            '<div class="col col_left col-xs-7">' +
            '<div class="form_element">' +
            '<input type="text" class="iis_detail_modal_price" value="' + input_iis + '" />' +
            '</div>' +
            '</div>' +
            '</div>'
        )
    }
    recountIIS();
}

$('#input_iis, #input_salary, #input_invest_rate, #input_period, #input_taxes, .iis_detail_modal_price, #detailInput').bind('change', function () {
    if (this.value.match(/[^0-9]/g)) {
        this.value = this.value.replace(/[^0-9]/g, '');
    }

    if ($(this).attr('id') == 'input_iis') {
        var price = parseInt($('#input_iis').val());

        $('#iis_detail_modal_list .iis_detail_modal_price').val(price);
    }

    if ($(this).attr('id') == 'input_period') {
        var price = parseInt($('#input_iis').val());
        var val = parseInt($(this).val()) - 1;
        var len = $('#iis_detail_modal_list .flex_row').length - 1;
        var text = '';

        if (val > len) {
            for (len; len < val; len++) {

                text = "Год " + Number(len+2) + ":";

                if(len === -1) {
                    text = "Первоначальный капитал";
                }

                if(len === 0) {
                    text += "<br> (на начало года)";
                }

                $('#iis_detail_modal_list').append(
                    '<div class="flex_row row">' +
                    '<div class="col col_left col-xs-5">' +
                    '<p class="t16 white m0">' + text + '</p>' +
                    '</div>' +
                    '<div class="col col_left col-xs-7">' +
                    '<div class="form_element">' +
                    '<input type="text" class="iis_detail_modal_price" value="' + price + '" />' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                )
            }
        } else if (val < len) {
            for (len; len > val; len--) {
                $('#iis_detail_modal_list .flex_row').eq(len).remove();
            }
        }
    }

    recountIIS();
});

function recountIIS() {
    var deposits = []; // значения детализированных полей "Введите размер пополнения ИИС в год"

    if ($('#detailInput').prop('checked') === false) {
        deposits = new Array(+$('#input_period').val() + 1);
        deposits.fill(parseInt($('#input_iis').val()));
    }

    if ($('#detailInput').prop('checked') === true) {
        for (var i = 0; i < $('body').find('.iis_detail_modal_price').length + 1; i++) {
            deposits.push(parseInt($('body').find('.iis_detail_modal_price').eq(i).val()) || 0);
        }
    }

    var salary = parseInt($('#input_salary').val()); // значение поля "Официальная ежемесячная заработная плата, с которой платится НДФЛ"
    var investRate = parseFloat($('#input_invest_rate').val()) / 100; // значение поля "Введите ставку доходности инвестиций"

    var period = parseInt($('#input_period').val()); // значение поля "Период на графике"
    var taxes = parseFloat($('#input_taxes').val()) / 100; // значение поля "Учитывать налоги на прирост капитала в расчетах по ставке (%)"

    var maxIisReturn = 52000; // максимально возможный возврат по ИИС
    var currentIisReturn = Math.min(maxIisReturn, salary * 12 * 0.13); // текущий возврат по ИИС

    // создание массива для ежемесячного расчета доходности
    var data = [];

    // создание массива для графика
    var chart_arr = [];

    var issSum = 0, yieldSum = 0;

    for (i = 0; i <= period; i++) {

        data[i] = {
            savingAmount: 0,
            deposit: 0,
            replFromIis: 0,
            yearYield: 0,
            initialCapital: 0,
            iisCapital: 0,
        };

        if(i === 0) {
            data[i].savingAmount = deposits[i];
            data[i].deposit = deposits[i];
            data[i].initialCapital = deposits[i];
            data[i].iisCapital = deposits[i];

            chart_arr.push([
                i,
                data[i].savingAmount,
                0,
                0,
                data[i].initialCapital,
                data[i].iisCapital,
            ]);
            continue;
        }

        var currentYear = data[i];
        var previousYear = data[i-1];

        //сумма ежегодного пополнения
        currentYear.deposit = deposits[i-1];
        var nextYearDeposit = i >= period ? 0 : deposits[i];

        //сумма первоначального капитала
        currentYear.initialCapital = nextYearDeposit + previousYear.initialCapital;

        var iisReturn = previousYear.deposit * 0.13;

        if(i === 2) {
            iisReturn = previousYear.deposit ? previousYear.deposit * 0.13 : previousYear.savingAmount * 0.13;
        }

        // пополнение от ИИС
        currentYear.replFromIis = (iisReturn > currentIisReturn) ? currentIisReturn : iisReturn;

        if(i < 2) {
            currentYear.replFromIis = 0;
        }

        if ((i > 2) && previousYear.deposit === 0) {
            currentYear.replFromIis = 0;
        }

        issSum += currentYear.replFromIis;

        //капитал с ИИС
        currentYear.iisCapital = currentYear.initialCapital + issSum;

        // считаем доходность на текущий год
        currentYear.yearYield = (previousYear.savingAmount + currentYear.replFromIis) * investRate * (1 - taxes);

        yieldSum += currentYear.yearYield;

        currentYear.savingAmount = currentYear.iisCapital + yieldSum;

        chart_arr.push([
            i,
            currentYear.savingAmount,
            previousYear.replFromIis,
            currentYear.deposit,
            currentYear.initialCapital,
            currentYear.iisCapital,
        ]);
    }
    iis1ChartRebuild(chart_arr, period);
    crateDataTable(data);
}

//рисуем таблицу
function crateDataTable(data) {
    var table = $('#iisDataTable tbody');
    var tableData = {
        breakpoints: {
            "xxs": 320,
            "xs": 480,
            "sm": 768,
            "md": 992,
            "lg": 1200,
            "xl": 1400
        },
        columns: [
            {
                name: 'period',
                breakpoints: 'xs',
                title: 'Период',
            },
            {
                name: 'capital',
                breakpoints: 'xs',
                title: 'Первоначальный капитал',
            },
            {
                name: 'iis',
                breakpoints: 'xs',
                title: 'С ИИС',
            },
            {
                name: 'perc',
                breakpoints: 'xs',
                title: 'С %',
            },
        ],
        rows: [],
    };

    for (var i = 0; i < data.length; i++) {
        tableData.rows[i] = {
            period: i,
            capital: parseInt(data[i].initialCapital).toLocaleString(),
            iis: parseInt(data[i].iisCapital).toLocaleString(),
            perc: parseInt(data[i].savingAmount).toLocaleString(),
        };
    }
    $('#iisDataTable').footable(tableData);
}

// график для ИИС
function iis1ChartRebuild(arr, years) {
    $('#iis1_chart').remove();
    $('.chart__outer').html('<canvas id="iis1_chart" class="iis_chart"></canvas>')

    var labels_arr = [], initial_arr = [], iss_arr = [], values_arr = [];

    for (var i = 0; i < arr.length; i++) {
        // создаем массивы для меток и для значений
        labels_arr.push('Год ' + arr[i][0]);
        values_arr.push(arr[i][1].toFixed(2));
        initial_arr.push(arr[i][4].toFixed(2));
        iss_arr.push(arr[i][5].toFixed(2));
    }

    // выводим весь прирост
    var val_max_output = (arr[arr.length-1][1] / 1000).toFixed(0);
    val_max_output = parseInt(val_max_output).toLocaleString();
    $('#years_txt').text(years);
    $('#val_max_output').text(val_max_output);

    // выводим прирост за счет ИИС
    var iis_output = ((arr[arr.length-1][5]) - (arr[arr.length-1][4])) / 1000;
    iis_output = parseInt(iis_output.toFixed(0)).toLocaleString();
    $('#iis_output').text(iis_output);

    var passive_output = ((arr[arr.length-1][1]) - (arr[arr.length-1][5])) / 1000;
    passive_output = parseInt(passive_output.toFixed(0)).toLocaleString();
    $('#passive_output').text(passive_output);

    // строим график
    var ctx = document.getElementById('iis1_chart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels_arr,
            datasets: [{
                label: 'Первоначальный капитал',
                data: initial_arr,
                backgroundColor: 'rgba(238,114,128,0.8)',
                borderColor: 'rgb(50,65,149)',
                borderWidth: 2
            },{
                label: 'С ИИС',
                data: iss_arr,
                backgroundColor: 'rgba(68,128,238,0.8)',
                borderColor: 'rgb(75,92,149)',
                borderWidth: 2
            },{
                label: 'с %',
                data: values_arr,
                backgroundColor: 'rgba(238, 211, 36, 0.8)',
                borderColor: 'rgb(5, 149, 72)',
                borderWidth: 2
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItems, data) {
                        return tooltipItems.yLabel + ' руб.';
                    }
                }
            },
            hover: {
                mode: 'index',
                intersect: false,
            }
        }
    });

    window.onload = function () {
        var ctx = document.getElementById('iis1_chart').getContext('2d');
        window.myLine = new Chart(ctx);
    }
}

// --------------------------
// --------------------------
// --------------------------

if ($('#input_yield_start').length != 0) {
  calcYieldRecount();
}

$('#input_yield_start, #input_refill, #input_yield_rate, #input_yield_period, #input_yield_taxes').bind('change', function() {
  if (this.value.match(/[^0-9]/g)) {
    this.value = this.value.replace(/[^0-9]/g, '');
  }

  calcYieldRecount();
});

$('#iis_type_a').on('change', function() {
  calcYieldRecount();
});

// основная функция подсчета доходности
function calcYieldRecount() {
	 var input_yield_taxes = $('#input_yield_taxes').val().length>0?$('#input_yield_taxes').val():0;
    var startDeposit = parseInt($('#input_yield_start').val()); // значение поля "Введите размер стартового капитала"
    var deposit = parseInt($('#input_refill').val()); // значение поля "Ежемесячное пополнение счета"
    var investRate = parseFloat($('#input_yield_rate').val()) / 100; // значение поля "Введите ставку доходности"
    var period =  parseInt($('#input_yield_period').val()); // значение поля "Период на графике"
    var yieldTaxes =  parseFloat(input_yield_taxes) / 100; // значение поля "Учитывать налоги на прирост капитала в расчетах по ставке (%)"
    var maxIisReturn = 52000; // максимально возможный возврат по ИИС
    var issOn = $('#iis_type_a').prop('checked');

    deposit *= 12;
    var deposits = new Array(period);
    deposits.fill(deposit);
    deposits[0] = startDeposit;


    // создание массива для ежемесячного расчета доходности
    var data = [];

    // создание массива для графика
    var chart_arr = [];

    var issSum = 0, yieldSum = 0;

    for (var i = 0; i <= period; i++) {

        data[i] = {};

        if (i === 0) {
            data[i] = {
                savingAmount: startDeposit,
                initialCapital: startDeposit,
                iisCapital: startDeposit,
                replFromIis: 0,
                yearYield: 0,
            };

            chart_arr.push([
                i,
                data[i].savingAmount,
                data[i].initialCapital,
                data[i].iisCapital,
            ]);

            continue;
        }

        var currentYear = data[i];
        var previousYear = data[i-1];
        var nextYearDeposit = (i >= period) ? 0 : deposits[i];

        currentYear.deposit = deposits[i-1];
        currentYear.initialCapital = previousYear.initialCapital + nextYearDeposit;

        currentYear.replFromIis = 0;

        if(issOn) {
            var iisReturn = previousYear.deposit * 0.13;

            if(i === 2) {
                iisReturn = previousYear.deposit ? previousYear.deposit * 0.13 : previousYear.savingAmount * 0.13;
            }

            // пополнение от ИИС
            currentYear.replFromIis = (iisReturn > maxIisReturn) ? maxIisReturn : iisReturn;

            if(i < 2) {
                currentYear.replFromIis = 0;
            }

            if ((i > 2) && previousYear.deposit === 0) {
                currentYear.replFromIis = 0;
            }
        }

        issSum += currentYear.replFromIis;

        //капитал с ИИС
        currentYear.iisCapital = currentYear.initialCapital + issSum;

        // считаем доходность на текущий год
        currentYear.yearYield = (previousYear.savingAmount + currentYear.replFromIis) * investRate * (1 - yieldTaxes);

        yieldSum += currentYear.yearYield;

        currentYear.savingAmount = currentYear.iisCapital + yieldSum;

        chart_arr.push([
            i,
            data[i].savingAmount,
            data[i].initialCapital,
            data[i].iisCapital,
        ]);
    }

    yieldChartRebuild(chart_arr, i);
    crateDataTable(data);
}

function yieldChartRebuild(arr, years) {
  $('#iis2_chart').remove();
  $('.chart__outer').append('<canvas id="iis2_chart" class="iis_chart"></canvas>')

  var labels_arr = [], values_arr = [], initial_arr = [], iss_arr = [];
  var val_max = 0;

  for (var i = 0; i < arr.length; i++) {
    // создаем массивы для меток и для значений
    labels_arr.push('Год ' + arr[i][0]);
    values_arr.push(arr[i][1].toFixed(2));
    initial_arr.push(arr[i][2].toFixed(2));
    iss_arr.push(arr[i][3].toFixed(2));


    // определяем максимальное значение
    if (arr[i][1] > val_max) {
      val_max = arr[i][1];
    }
  }

  // выводим весь прирост
  var val_max_output = (val_max / 1000).toFixed(0);
  val_max_output = parseInt(val_max_output).toLocaleString();
  $('#years_txt').text(years - 1);
  $('#val_max_output').text(val_max_output);

  // строим график
  var ctx = document.getElementById('iis2_chart').getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: labels_arr,
        datasets: [{
            label: 'Первоначальный капитал',
            data: initial_arr,
            backgroundColor: 'rgba(238,114,128,0.8)',
            borderColor: 'rgb(50,65,149)',
            borderWidth: 2
        },{
            label: 'С ИИС',
            data: iss_arr,
            backgroundColor: 'rgba(68,128,238,0.8)',
            borderColor: 'rgb(75,92,149)',
            borderWidth: 2
        },{
            label: '',
            data: values_arr,
            backgroundColor: 'rgb(238, 211, 36, 0.8)',
  					borderColor: 'rgb(5, 149, 72)',
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
				tooltips: {
					mode: 'index',
					intersect: false,
					callbacks: {
						label: function(tooltipItems, data) {
							return tooltipItems.yLabel + ' руб.';
						}
					}
				},
				hover: {
					mode: 'index',
					intersect: false,
				}
    }
  });
}
