$.validator.addMethod("phone", function(value, element) {
    console.log(/^[0-9+_\s-]+$/i.test(value), (value.match(/_/g) || []).length, (value.match(/_/g) || []).length <= 2)
    return this.optional(element) || (/^[0-9+_\s-]+$/i.test(value) && (value.match(/_/g) || []).length <= 2);
}, "Неверный формат данных");
