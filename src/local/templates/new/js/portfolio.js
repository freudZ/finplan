//Colors list generator
var colors_big_arr =[];
if ($.cookie('colors_cookie')) {
  colors_big_arr = JSON.parse($.cookie('colors_cookie'));
}
else {
  for (var i = 0; i < 200; i++) {
    var curr_donut_color_letter = '0123456789ABCDEF'.split('');
    var curr_donut_color = '#';
    for (var j = 0; j < 6; j++) {
      curr_donut_color += curr_donut_color_letter[Math.floor(Math.random() * 16)];
    }
    colors_big_arr.push(curr_donut_color);
  }
  $.cookie('colors_cookie', JSON.stringify(colors_big_arr), {expires: 1, path: '/'}
  );
}

function overlayShow() {
	if ($('.load_overlay')) {
		$('.load_overlay').css('display', 'block');
		setTimeout(function() {
			$('.load_overlay').css('opacity', 1);
		}, 10);
	}
}
function overlayHide() {
	if ($('.load_overlay')) {
		$('.load_overlay').css('opacity', 0);
		setTimeout(function() {
			$('.load_overlay').css('display', 'none');
		}, 300);
	}
}

function checkSubzeroCache(){
			 var pid = $('#portfolios_select').val().replace("portfolio_", "");
          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/portfolio.php",
            dataType: "json",
            data: {ajax: "y", action: 'checkSubZeroCache', 'pid':pid},
            cashe: false,
            async: false,
            success: function(data) {
					if(data == true){
					 $('.hasSubZeroCacheDescription').removeClass('hidden');
					}  else {
					 $('.hasSubZeroCacheDescription').addClass('hidden');
					}
              }
            }
          );
}

//Контроль доступности кнопки покупки активов по наличию кеша
function checkSummOnAddNewActive() {
  var lotsCount = parseInt($('body').find('#popup_add_row_number').val());
  var type = $('#popup_add_row_type').val();
  var currency = $('body').find('#popup_add_row_active_currency_id').val().toUpperCase();
  if (type === 'currencies') {
    currency = 'RUB';
  }
  var lotElementId = currency === 'RUB' ? '#popup_add_row_active_lastprice' : '#popup_add_row_active_lastprice_valute';
  var lotPrice = parseFloat($('body').find(lotElementId).val());
  var autoCheckboxChecked = $('body').find('#popup_add_subzero_cache_on').is(':checked');
  var errorText = $('body').find('.popup_add_row__summ_error');
  var saveButton = $('body').find('#popup_add_row .popup_change_row_save');
  var cacheElement = $('body').find('#cache-currency-' + currency);


  if (currency.length == 0 || $('#popup_add_row_free_cache li').length == 0 || cacheElement.length == 0) {
	 var cashLimit = 0;
  } else {
	 var cashLimit = parseFloat($('body').find('#cache-currency-' + currency).attr('data-cache'));
  }

  console.log('@@@@@@@', currency, cacheElement.length, cashLimit)

  var summ = lotPrice * lotsCount;
  console.log('>>>>>>>', summ, cashLimit, !autoCheckboxChecked)
  if (summ > cashLimit && !autoCheckboxChecked) {
    errorText.css('display', 'block');
    saveButton.css('display', 'none');
  } else {
    errorText.css('display', 'none');
    saveButton.css('display', 'inline-block');
  }
}


$(document).ready(function()
  {
	 //Кнопка показа логов бектеста для админа
	 if($('.backtest_errors_btton').length){
	 	$('.backtest_errors_btton').on('click', function(){
	 	  $('.backtest_errors_list').toggle();
	 	});
	 }


    $('.popup_portfolio_action_selector').on('change', function()
      {
        var selected_action = $(this).val(),
        current_popup = $(this).attr('data-current-popup');

        $('#'+current_popup).modal('hide');
        $('.modal-backdrop').remove();
        $('body, html').removeAttr('style');
        $('body').removeClass('modal-open');
        $('#'+selected_action).modal('show');
      }
    );

    //автокомплит облигации и акции
    if($("#autocomplete_obl_act_all").length) {
      var url = '/ajax/portfolio/actions.php';
      var ac = $('#autocomplete_obl_act_all').autocomplete(
        {
		  params: {
		  		'date': ($('#popup_add_row_date').length>0?$('#popup_add_row_date').val():''),
				'pid': ($('#portfolios_select').length>0?$('#portfolios_select').val().replace("portfolio_", ""):'')
		  },
        serviceUrl: function() {
            if($('#popup_add_row_type').val()=='bonds') {
              url = '/ajax/portfolio/obligations.php';
            }
            else if($('#popup_add_row_type').val()=='shares') {
              url = '/ajax/portfolio/actions.php';
            }
            else if($('#popup_add_row_type').val()=='shares_usa') {
              url = '/ajax/portfolio/actions_usa.php';
            }
            else if($('#popup_add_row_type').val()=='shares_etf') {
              url = '/ajax/portfolio/etf.php';
            }
            else if($('#popup_add_row_type').val()=='currencies') {
              url = '/ajax/portfolio/currencies.php';
            }
            return url;
          },
        onSearchStart: function () {
            $('#popup_add_row_active_id').val('');
            $('#popup_add_row_active_code').val('');
            $('#popup_add_row_active_url').val('');
            $('#popup_add_row_active_emitent_currency').val('');
            $('#popup_add_row_active_emitent_name').val('');
            $('#popup_add_row_active_emitent_url').val('');
            $('#popup_add_row_active_inlot_cnt').val('');
            $('#popup_add_row_active_price_one, #popup_add_row_price_one').val('');
            $('#popup_add_row_active_lastprice, #popup_add_row_price, #popup_add_row_active_price_orig').val('');
            $('#popup_add_row_active_secid').val('');
				$('#popup_add_row_free_cache li').remove();
          }
          ,
        onSelect: function (suggestion) {
            var lotCnt = 1;
            if(suggestion['date_now']) {
              $('#popup_add_row_date').val(suggestion['date_now']);
            }
            if(suggestion['price_valute']) {
              $('#popup_add_row_active_lastprice_valute').val(suggestion['price_valute']);
            }
            $('#popup_add_row_active_id').val(suggestion['id']);
            $('#popup_add_row_active_code').val(suggestion['code']);
            $('#popup_add_row_active_url').val(suggestion['url']);
            $('#popup_add_row_active_currency_id').val(suggestion['currencyId']);
            $('#popup_add_row_active_emitent_currency').val(suggestion['emitent_currency']);
            $('#popup_add_row_active_emitent_name').val(suggestion['emitent_name']);
            $('#popup_add_row_active_emitent_url').val(suggestion['emitent_url']);
            $('#popup_add_row_active_inlot_cnt').val(suggestion['inlot_cnt']);
            $('#popup_add_row_active_price_one, #popup_add_row_price_one, #popup_add_row_active_price_orig').val(suggestion['price']);
            if(suggestion["price_valute"]) {
              $('#popup_add_row_price_one').val(suggestion['price']);
              $('#popup_add_row_currency_rate').val(suggestion["rate"]);
              $('#popup_add_row_price_one_usd').val(suggestion["price_valute"]);
              $('.portfolio_add_price_usd_label span').text(' в '+suggestion['currencyId']);
            }
            $('#popup_add_row_currency_rate').val(suggestion['rate']);

				/*+ Ищем и обрабатываем свободный кеш */
				$('#popup_add_row_free_cache li').remove();
				if($(suggestion["cache"]).length>0){
					for (var key in suggestion["cache"]) {
					  $('#popup_add_row_free_cache').append('<li id="cache-currency-'+key+'" data-cache="'+suggestion["cache"][key].SUMM+'">'+key+': '+suggestion["cache"][key].SUMM+'</li>');
					};
				}
				/*- Ищем и обрабатываем свободный кеш */

            //удалить
            $('#popup_add_row_active_lastprice, #popup_add_row_price').val(suggestion['lastprice']);

            $('.popup_add_row_price_lot').text(suggestion['lastprice'] + ' ' + suggestion['currencyId'].toUpperCase());
            if(suggestion["price_valute"]) {
              $('.popup_add_row_price_lot').text(suggestion['lastprice'] +' ('+suggestion["price_valute"]+' '+suggestion['currencyId']+')');
            }

            //Кол-во лотов
            if($('#popup_add_row_number').val()) {
              lotCnt = Number($('#popup_add_row_number').val());
            }

            $('.popup_add_row_summ').text(lotCnt*suggestion['lastprice'] + ' ' + suggestion['currencyId'].toUpperCase());

            if(suggestion["price_valute"]) {
              var valute_sum_invest = lotCnt*suggestion['price_valute'];

              $('.popup_add_row_summ').text($('.popup_add_row_summ').text().replace('USD', 'RUB'));
              $('.popup_add_row_summ').text($('.popup_add_row_summ').text()+' ('+valute_sum_invest+' '+suggestion['currencyId']+')');
              //$('.obl_hidden').show();
				  $('.portfolio_add_price_usd_label').html('Цена <span>в '+suggestion['currencyId']+'</span>');
              $('.act_usd_price').show();
            } else {
            	$('.portfolio_add_price_usd_label').html('Цена за акцию <span></span>');
            	$('.act_usd_price').hide();
            }


            $('#popup_add_row_active_secid').val(suggestion['secid']);
				checkSummOnAddNewActive();
          },
        beforeRender: function (container) {
            $(".customscroll").mCustomScrollbar(
              {
              scrollbarPosition: 'outside'
              }
            );
          }
        }
      );

      //$('body .portfolioHistoryPagenav a.histPagenav').off('click');
      $('body').on('click', '.portfolioHistoryPagenav a.histPagenav', function()
        {
          var showPage = $(this).attr('data-page');
          $('.portfolioHistoryPagenav li').removeClass('active');
          $('.portfolioHistoryPagenav li .histPagenav.page_'+showPage).parents('li').addClass('active');
          $('.portfolioHistoryTablePages').addClass('hidden');
          $('.portfolioHistoryTablePages.phPage_'+showPage).removeClass('hidden');
        }
      );

      $('body .portfolioEventsPagenav a.eventPagenav').off('click');
      $('body').on('click', ' .portfolioEventsPagenav a.eventPagenav', function()
        {
          var showPage = $(this).attr('data-page');
          $('.portfolioEventsTable .portfolioEventsPagenav li').removeClass('active');
          $('.portfolioEventsTable .portfolioEventsPagenav li .eventPagenav.page_'+showPage).parents('li').addClass('active');
          $('.portfolioEventsTable .portfolioEventsTablePages').addClass('hidden');
          $('.portfolioEventsTable .portfolioEventsTablePages.evPage_'+showPage).removeClass('hidden');
        }
      );


      //Получение цены покупки по фифо при продаже
      $('#popup_sale_row_date').off('change input keyup');
      $('#popup_sale_row_date').on('change input keyup', function()
        {
          //Прописать парамтеры актива
          var activeType = $('#popup_remove_row_type').val();
          var activeCode = $('#popup_remove_row_active_code').val();
          var activeSecid = $('#popup_remove_row_active_secid').val();
          var activeCurrency = $('#popup_remove_row_currency_code').val();
          var pid = $('#portfolios_select').val().replace("portfolio_", "");
          var addDate = $('#popup_sale_row_date').val();
          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/getPriceOnDate.php", //getFifoBuyPrice.php
            dataType: "json",
            data: {ajax: "y", 'activeType': activeType, 'activeCode': activeCode, 'currency':activeCurrency, 'activeSecid': activeSecid, 'date': addDate, 'pid': pid}
              ,
            cashe: false,
            async: false,
            success: function(data) {
                getResponse(data);
              }
            }
          );

          function getResponse(data) {
            var lotprice = 0;
            var inlot_cnt = $('#popup_remove_row_active_inlot_cnt').val();
            var fifo_lotprice = 0;
            var lotprice_valute = 0;
            var lotnumber = $('#popup_change_row_number').val();
            var price = Number(data["price"]).toFixed(4);
            //var fifo_price = Number(data["fifo"]["price"]);
            if(data!=null) {


              //$('#popup_remove_row_active_inlot_cnt').val(Number(data["fifo"]["inlot_cnt"]));
              lotprice = ($('#popup_remove_row_active_inlot_cnt').val()*price).toFixed(4);
              //fifo_lotprice = ($('#popup_remove_row_active_inlot_cnt').val()*fifo_price).toFixed(2);
            }

            $('#popup_change_row_price').val(price);
            $('#popup_change_row_price, #popup_add_row_price_one').val(price);
            //$('#popup_change_row_fifo_price').val(fifo_price);
            if(Number(data["price_valute"])>0) {
              $('#popup_change_row_price').val(price);
              $('#popup_remove_row_currency_rate').val(data["rate"]);
				  console.log('rate on change calendar date', data["rate"]);
              $('#popup_add_row_price_one_usd').val(data["price_valute"]);
              $('.portfolio_add_price_usd_label span').text(' в '+data['currencyId']);
            }
            else {
              //$('#popup_remove_row_currency_rate').val(1);
            }
            // $('#popup_add_row_active_lastprice_valute').val(Number(data["price_valute"]));
            calcLotPriceDelActivePopup();
          }


        }
      );


      //Получение цены покупки по фифо при изъятии
      $('#popup_exclude_row_date').off('change input keyup');
      $('#popup_exclude_row_date').on('change input keyup', function()
        {
          //Прописать парамтеры актива
          var activeType = $('#popup_exclude_row_type').val();
          var activeCode = $('#popup_exclude_row_active_code').val();
          var activeSecid = $('#popup_exclude_row_active_secid').val();
          var pid = $('#portfolios_select').val().replace("portfolio_", "");
          var addDate = $('#popup_exclude_row_date').val();
          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/getFifoBuyPrice.php",
            dataType: "json",
            data: {ajax: "y", 'activeType': activeType, 'activeCode': activeCode, 'activeSecid': activeSecid, 'date': addDate, 'pid': pid}
              ,
            cashe: false,
            async: false,
            success: function(data) {
                getResponse(data);
              }
            }
          );

          function getResponse(data) {
            var lotprice = 0;
            var fifo_lotprice = 0;
            var lotprice_valute = 0;
            var lotnumber = $('#popup_exclude_row_number').val();
            var price = Number(data["hist"]["price"]).toFixed(2);
            var fifo_price = Number(data["fifo"]["price"]);
            if(data!=null) {
              lotprice = Number(price).toFixed(2);
              fifo_lotprice = ($('#popup_exclude_row_active_inlot_cnt').val()*fifo_price).toFixed(2);
            }

            $('#popup_exclude_row_price').val(price);
            $('#popup_exclude_row_price').val(price);
            $('#popup_exclude_row_fifo_price').val(fifo_price);
            if(Number(data["hist"]["price_valute"])>0) {
              $('#popup_exclude_row_price').val(price);
              $('#popup_exclude_row_currency_rate').val(data["hist"]["rate"]);
              $('#popup_exclude_row_price_one_usd').val(data["hist"]["price_valute"]);
              $('.portfolio_exclude_price_usd_label span').text(' в '+data["hist"]['currencyId']);
            }
            $('#popup_exclude_row_active_lastprice_valute').val(Number(data["hist"]["price_valute"]));
            $('.popup_exclude_row_price_lot').text(lotprice).attr('data-price_lot', lotprice);

            $('.popup_exclude_row_fifo_price_lot').text(fifo_lotprice);
            if(Number(data["hist"]["rate"])>1) {
              lotprice_valute = (lotprice/Number($('#popup_exclude_row_currency_rate').val()).toFixed(2));
              $('.popup_exclude_row_price_lot').text(lotprice + ' ('+ lotprice_valute.toFixed(2) +' '+data["hist"]['currencyId']+')').attr('data-price_lot', lotprice).attr('data-price_lot_valute', lotprice_valute);
            }
            $('.popup_exclude_row_summ').text(lotprice*Number(lotnumber));

            if(data["hist"]["rate"]>1) {
              $('.popup_exclude_row_summ').text($('.popup_exclude_row_summ').text() + ' ('+(lotprice_valute*Number(lotnumber)).toFixed(2)+' '+data["hist"]['currencyId']+')');
            }
          }


        }
      );


      //Обновляем цену и курс при перевыборе валюты для внесения
      $('#popup_add_cache_selector').on('change', function()
        {
          $('#popup_insert_row_date').change();
        }
      );

      //Пересчет при выборе даты в попапе внесения валюты в портфель
      $('#popup_insert_row_date').off('change input keyup');
      $('#popup_insert_row_date').on('change input keyup', function()
        {
          var activeType = $('#popup_insert_row_type').val();
          var activeCode = $('#popup_add_cache_selector').val();
          var activeSecid = $('#popup_add_cache_selector').val();
          var addDate = $('#popup_insert_row_date').val();

          if($(this).hasClass('popup_add_cache_selector')) {
            $(this).val($('#popup_add_cache_selector').val()).trigger('change.select2');
          }

          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/getPriceOnDate.php",
            dataType: "json",
            data: {ajax: "y", 'activeType': activeType, 'activeCode': activeCode, 'activeSecid': activeSecid, date: addDate}
              ,
            cashe: false,
            async: false,
            success: function(data) {
                getResponse(data);
              }
            }
          );

          function getResponse(data) {
            var lotprice = 0;
            var lotprice_valute = 0;
            var lotnumber = $('#popup_insert_row_number').val();
            price = Number(data["price"]);

            $('#popup_insert_row_active_id').val($('#popup_add_cache_selector option:selected').attr('data-active-id'));
            $('#popup_insert_row_active_url').val($('#popup_add_cache_selector option:selected').attr('data-active-url'));
            $('#popup_insert_row_active_inlot_cnt').val($('#popup_add_cache_selector option:selected').attr('data-inlot-cnt'));
            $('#popup_insert_row_active_lastprice').val($('#popup_add_cache_selector option:selected').attr('data-lastprice'));
            $('#popup_insert_row_active_currency_id').val($('#popup_add_cache_selector').val());
            $('#popup_insert_row_active_secid').val($('#popup_add_cache_selector').val());
            if(data!=null) {
              lotprice = ($('#popup_insert_row_active_inlot_cnt').val()*price).toFixed(2);
            }
            $('#popup_insert_row_active_price_one').val(price);
            $('#popup_insert_row_price_one, #popup_insert_row_active_price_orig').val(price);

            $('#popup_insert_row_active_lastprice_valute').val($('#popup_add_cache_selector option:selected').attr('data-lastprice'));
            $('.popup_insert_row_price_lot').text(lotprice);

            $('#popup_insert_row_price').val(lotprice);
            $('.popup_insert_row_summ').text(lotprice*Number(lotnumber));
          }
        }
      );

      //Обновляем цену и курс при перевыборе валюты для внесения от амортизации
      $('#popup_cashflow_cache_selector').on('change', function()
        {
          $('#popup_cashflow_row_date').change();
        }
      );

      //Пересчет при выборе даты в попапе внесения  от амортизации валюты в портфель
      $('#popup_cashflow_row_date').off('change input keyup');
      $('#popup_cashflow_row_date').on('change input keyup', function()
        {
          var activeType = $('#popup_cashflow_row_type').val();
          var activeCode = $('#popup_cashflow_cache_selector').val();
          var activeSecid = $('#popup_cashflow_cache_selector').val();
          var addDate = $('#popup_cashflow_row_date').val();

          if($(this).hasClass('popup_cashflow_cache_selector')) {
            $(this).val($('#popup_cashflow_cache_selector').val()).trigger('change.select2');
          }

          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/getPriceOnDate.php",
            dataType: "json",
            data: {ajax: "y", 'activeType': activeType, 'activeCode': activeCode, 'activeSecid': activeSecid, date: addDate}
              ,
            cashe: false,
            async: false,
            success: function(data) {
                getResponse(data);
              }
            }
          );

          function getResponse(data) {
            var lotprice = 0;
            var lotprice_valute = 0;
            var lotnumber = $('#popup_cashflow_row_number').val();
            price = Number(data["price"]);

            $('#popup_cashflow_row_active_id').val($('#popup_cashflow_cache_selector option:selected').attr('data-active-id'));
            $('#popup_cashflow_row_active_url').val($('#popup_cashflow_cache_selector option:selected').attr('data-active-url'));
            $('#popup_cashflow_row_active_inlot_cnt').val($('#popup_cashflow_cache_selector option:selected').attr('data-inlot-cnt'));
            $('#popup_cashflow_row_active_lastprice').val($('#popup_cashflow_cache_selector option:selected').attr('data-lastprice'));
            $('#popup_cashflow_row_active_currency_id').val($('#popup_cashflow_cache_selector').val());
            $('#popup_cashflow_row_active_secid').val($('#popup_cashflow_cache_selector').val());
            if(data!=null) {
              lotprice = ($('#popup_cashflow_row_active_inlot_cnt').val()*price).toFixed(2);
            }
            $('#popup_cashflow_row_active_price_one').val(price);
            $('#popup_cashflow_row_price_one, #popup_cashflow_row_active_price_orig').val(price);

            $('#popup_cashflow_row_active_lastprice_valute').val($('#popup_cashflow_cache_selector option:selected').attr('data-lastprice'));
            $('.popup_cashflow_row_price_lot').text(lotprice);

            $('#popup_cashflow_row_price').val(lotprice);
            $('.popup_cashflow_row_summ').text(lotprice*Number(lotnumber));
          }
        }
      );

      //Пересчет при выборе даты в попапе редактирования истории
      $('#hist_edit_row_date').off('change input keyup');
      $('#hist_edit_row_date').on('change input keyup', function()
        {

          var activeType = $('#popup_hist_edit_active_type').val();
          var activeCode = $('#popup_hist_edit_active_code').val();
          var activeSecid = $('#popup_hist_edit_active_secid').val();
          var activeCurrency = $('#popup_hist_edit_currency_id').val();
          var addDate = $('#hist_edit_row_date').val();
          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/getPriceOnDate.php",
            dataType: "json",
            data: {ajax: "y", 'activeType': activeType, 'activeCode': activeCode, 'currency':activeCurrency, 'activeSecid': activeSecid, date: addDate}
              ,
            cashe: false,
            async: false,
            success: function(data) {
                getResponse(data);
              }
            }
          );

          function getResponse(data) {
            updatePricesOnHistoryEditPopup(data);
          }
        }
      );

      function updatePricesOnHistoryEditPopup(data) {
        var thisPopup = $('#popup_edit_row_history_portfolio');
        var lotprice = 0;
        var lotprice_valute = 0;
        var lotnumber = $('#hist_edit_cnt_row_number').val();
        price = Number(data["price"]).toFixed(4);
        if($('#hist_edit_row_date').attr('useExistPrice')=='Y') {
          price = thisPopup.find('#popup_hist_edit_active_lastprice').val();

        }
        if(data!=null) {
          lotprice = ($('#popup_hist_edit_active_inlot_cnt').val()*price).toFixed(4);
			 if(Number(data["rate"])>0){
          price_valute = (Number(price)/data["rate"]).toFixed(4);
          lotprice_valute = (Number(price)/data["rate"]*$('#popup_hist_edit_active_inlot_cnt').val()).toFixed(4);
			 } else {
			  price_valute = 0;
			 }
          data["price_valute"] = price_valute;
        }
        $('#popup_hist_edit_active_price_one').val(price);
        console.log(price)
        $('#popup_hist_edit_active_price_orig').val(price);
        // $('#hist_edit_row_price, #popup_hist_edit_active_price_orig').val(price);
        if(Number(data["price_valute"])>0) {
          $('#hist_edit_row_price').val(price);
          $('#popup_hist_edit_currency_rate').val(data["rate"]);
          $('#popup_hist_edit_row_price_one_usd').val(data["price_valute"]);
          thisPopup.find('.portfolio_hist_edit_price_usd_label span').text(' в '+$('#popup_hist_edit_currency_id').val());
        }
        $('#popup_hist_edit_active_lastprice').val(lotprice);
        $('#popup_hist_edit_active_lastprice_valute').val(lotprice_valute);
        $('#hist_edit_row_date').attr('useExistPrice', 'N');
        calcLotPriceEditPopup();
      }

      //Пересчет при выборе даты в попапе добавления актива в портфель
      $('#popup_add_row_date').off('change input keyup');
      $('#popup_add_row_date').on('change input keyup', function()
        {
          var activeType = $('#popup_add_row_type').val();
          var activeCode = $('#popup_add_row_active_code').val();
          var activeSecid = $('#popup_add_row_active_secid').val();
          var currency = $('#popup_add_row_active_currency_id').val();
          var addDate = $('#popup_add_row_date').val();
			 var pid = $('#portfolios_select').val().replace("portfolio_", "");
          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/getPriceOnDate.php",
            dataType: "json",
            data: {ajax: "y", 'activeType': activeType, 'pid':pid, 'activeCode': activeCode, 'currency':currency, 'activeSecid': activeSecid, 'date': addDate, 'getCache': 'Y'},
            cashe: false,
            async: false,
            success: function(data) {
                if (!data['currencyId']) {
                  data['currencyId'] = currency;
                }
                getResponse(data);
              }
            }
          );

          function getResponse(data) {
            var lotprice = 0;
            var lotprice_valute = 0;
            var lotnumber = $('#popup_add_row_number').val();
            var activeType = $('#popup_add_row_type').val();

				/*+ Ищем и обрабатываем свободный кеш */
				$('#popup_add_row_free_cache li').remove();
				if($(data["cache"]).length>0){
					for (var key in data["cache"]) {
					  $('#popup_add_row_free_cache').append('<li id="cache-currency-'+key+'" data-cache="'+data["cache"][key].SUMM+'">'+key+': '+data["cache"][key].SUMM+'</li>');
					};
				}
				/*- Ищем и обрабатываем свободный кеш */

            price = Number(data["price"]);
            if(data!=null) {
              lotprice = ($('#popup_add_row_active_inlot_cnt').val()*price).toFixed(4);
            }

            $('#popup_add_row_active_price_one').val(price);
            $('#popup_add_row_price_one, #popup_add_row_active_price_orig').val(price);
            if(Number(data["price_valute"])>0) {
              $('#popup_add_row_price_one').val(price);
              $('#popup_add_row_currency_rate').val(data["rate"]);
              $('#popup_add_row_price_one_usd').val(data["price_valute"]);
              $('.portfolio_add_price_usd_label span').text(' в '+data['currencyId']);
            }
            $('#popup_add_row_active_lastprice').val(lotprice);
            $('#popup_add_row_active_lastprice_valute').val(Number(data["price_valute"]));
            $('.popup_add_row_price_lot').text(lotprice);
            if(Number(data["price_valute"])>0) {
              lotprice_valute = ($('#popup_add_row_active_inlot_cnt').val()*Number($('#popup_add_row_price_one').val())).toFixed(4);
              $('.popup_add_row_price_lot').text(lotprice + ' ('+ lotprice_valute +' '+data['currencyId']+')');
            }
            $('#popup_add_row_price').val(lotprice);
            $('.popup_add_row_summ').text(lotprice*Number(lotnumber));
            if(Number(data["price_valute"])>0 && activeType!='currencies') {
              $('.popup_add_row_summ').text($('.popup_add_row_summ').text().replace('USD', 'RUB'));
              $('.popup_add_row_summ').text($('.popup_add_row_summ').text() + ' ('+(lotprice_valute*Number(lotnumber))+' '+data['currencyId']+')');
            }

            checkSummOnAddNewActive();
          }
        }
      );

      $('#popup_add_row_price_one').off('change input keyup');
      $('#popup_add_row_price_one').on('change input keyup', function()
        {
          if ($(this).val().match(/[^0-9\.]/g)) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
          }

          var rate = Number($('#popup_add_row_currency_rate').val());
          var lotprice_valute = 0;
          var invest_summ = '';
			 var activeType = $('#popup_add_row_type').val();
			 var currencyId = $('#popup_add_row_active_currency_id').val();
          if(!isNaN($(this).val())) {
            lotprice = Number($('#popup_add_row_active_inlot_cnt').val()*$(this).val()).toFixed(4);
          }

          $('#popup_add_row_active_price_one').val($(this).val($(this).val()));

          var showValute = Number($('#popup_add_row_active_lastprice_valute').val())>0 && rate>0;

          $('.popup_add_row_price_lot').text(lotprice);

          if(showValute) {
          	if(activeType=='currencies'){
				 var priceOneCurrency = $('#popup_add_row_price_one').val();
          	} else {
				 var priceOneCurrency = ($(this).val()/rate).toFixed(4);
          	}

            $('#popup_add_row_price_one_usd').val(priceOneCurrency);
            $('#popup_add_row_active_lastprice_valute').val(priceOneCurrency);
            lotprice_valute = ($('#popup_add_row_active_inlot_cnt').val()*Number(priceOneCurrency)).toFixed(4);
            $('.popup_add_row_price_lot').text(lotprice + ' ('+ lotprice_valute +' '+currencyId+')');
          }

          $('#popup_add_row_active_lastprice, #popup_add_row_price').val(lotprice);
          if(!isNaN($('#popup_add_row_number').val()) && Number($('#popup_add_row_number').val())>0) {
            $('#popup_add_row_number').change();
          }

          checkSummOnAddNewActive();
        }
      );

      //Инпут цены в валюте
      $('#popup_add_row_price_one_usd').off('change input keyup');
      $('#popup_add_row_price_one_usd').on('change input keyup', function()
        {
          if ($(this).val().match(/[^0-9\.]/g)) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
          }
          var rate = $('#popup_add_row_currency_rate').val();
          var invest_summ = '';
          var lotprice_valute = '';
          if(!isNaN($(this).val())) {
            $('#popup_add_row_price_one').val(($(this).val()*rate).toFixed(2));
          }
          $('#popup_add_row_active_lastprice_valute').val($(this).val());
          //$('#popup_add_row_active_price_one').val($(this).val(($(this).val()*rate).toFixed(2)));

          if(!isNaN($('#popup_add_row_price_one').val())) {
            lotprice = $('#popup_add_row_active_inlot_cnt').val()*$('#popup_add_row_price_one').val();
          }
          $('.popup_add_row_price_lot').text(lotprice);
          if($('#popup_add_row_price_one_usd').val()) {
            lotprice_valute = ($('#popup_add_row_active_inlot_cnt').val()*Number($('#popup_add_row_price_one_usd').val())).toFixed(2);
            $('.popup_add_row_price_lot').text(lotprice + ' ('+ lotprice_valute +' '+$('#popup_add_row_active_currency_id').val()+')');
          }
          $('#popup_add_row_active_lastprice, #popup_add_row_price').val(lotprice);

          if(!isNaN($('#popup_add_row_number').val()) && Number($('#popup_add_row_number').val())>0) {
            $('#popup_add_row_number').change();
          }

          checkSummOnAddNewActive();
        }
      );

      //Инпут кол-ва лотов
      $('#popup_add_row_number').off('change input keyup');
      $('#popup_add_row_number').on('change input keyup', function()
        {
          if ($(this).val().match(/[^0-9]/g)) {
            $(this).val($(this).val().replace(/[^0-9]/g, ''));
          }
			 var activeType = $('#popup_add_row_type').val();
          var invest_summ = '';
          var invest_summ_valute = '';
          if(!isNaN($(this).val())) {
            invest_summ = $('#popup_add_row_active_lastprice').val()*$(this).val();
            invest_summ_valute = $('#popup_add_row_price_one_usd').val()*$(this).val();
          }
          $('.popup_add_row_summ').text(invest_summ.toFixed(4));
          if(invest_summ_valute>0 && activeType!='currencies') {
            $('.popup_add_row_summ').text($('.popup_add_row_summ').text().replace('USD', 'RUB'));
            $('.popup_add_row_summ').text($('.popup_add_row_summ').text()+' ('+invest_summ_valute+' '+$('#popup_add_row_active_currency_id').val()+')');
          }

          checkSummOnAddNewActive();
        }
      );


      //Инпут кол-ва валюты для попапа редактирования истории
      $('#hist_edit_cnt_row_number').off('change input keyup');
      $('#hist_edit_cnt_row_number').on('change input keyup', function()
        {
          if ($(this).val().match(/[^0-9]/g)) {
            $(this).val($(this).val().replace(/[^0-9]/g, ''));
          }
          var summ = '';
          if(!isNaN($(this).val())) {
            calcLotPriceEditPopup();
          }
        }
      );

      //Инпут цены в валюте для попапа редактирования истории
      $('#popup_hist_edit_row_price_one_usd').off('change input keyup');
      $('#popup_hist_edit_row_price_one_usd').on('change input keyup', function()
        {
          var currencyRate = $('#popup_hist_edit_currency_rate').val();
          var priceLot = (currencyRate*$(this).val()).toFixed(4);
          $('#hist_edit_row_price').val(priceLot);
          $('#popup_hist_edit_active_lastprice').val(priceLot);
          calcLotPriceEditPopup();
        }
      );

      //Инпут цены для попапа редактирования истории
      $('#hist_edit_row_price').off('change input keyup');
      $('#hist_edit_row_price').on('change input keyup', function()
        {
          var currencyRate = $('#popup_hist_edit_currency_rate').val();
          var currencyId = $('#popup_hist_edit_currency_id').val();

          if(!isNaN(currencyRate) && currencyId!='rub') {
            var priceLotCurrency = ($(this).val()/currencyRate).toFixed(4);
            $('#popup_hist_edit_row_price_one_usd').val(priceLotCurrency);
            $('#popup_hist_edit_active_price_one').val(priceLotCurrency);

          }
          calcLotPriceEditPopup();


          //  calcLotPriceEditPopup();
          /*          var summ = '';
               if(!isNaN($(this).val())) {
                 summ = $('#hist_edit_cnt_row_number').val()*$(this).val();
               }
               $('.hist_edit_row_summ').text(summ.toFixed(2));*/

        }
      );


      //Инпут кол-ва валюты для попапа внесения
      $('#popup_insert_row_number').off('change input keyup');
      $('#popup_insert_row_number').on('change input keyup', function()
        {
          if ($(this).val().match(/[^0-9\.]/g)) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
          }
          var invest_summ = '';
          var invest_summ_valute = '';
          if(!isNaN($(this).val())) {
            invest_summ = $('#popup_insert_row_price_one').val()*$(this).val();
          }
          $('.popup_insert_row_summ').text(invest_summ.toFixed(2));

        }
      );

      //Инпут курса валюты для попапа внесения
      $('#popup_insert_row_price_one').off('change input keyup');
      $('#popup_insert_row_price_one').on('change input keyup', function()
        {
          if ($(this).val().match(/[^0-9\.]/g)) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
          }
          var invest_summ = '';
          var invest_summ_valute = '';
          if(!isNaN($(this).val())) {
            invest_summ = $('#popup_insert_row_number').val()*$(this).val();
          }
          $('.popup_insert_row_summ').text(invest_summ.toFixed(2));

        }
      );



      //Инпут кол-ва валюты для попапа внесения амортизации
      $('#popup_cashflow_row_number').off('change input keyup');
      $('#popup_cashflow_row_number').on('change input keyup', function()
        {
          // if ($(this).val().match(/[^0-9]/g)) {
          //   $(this).val($(this).val().replace(/[^0-9]/g, ''));
          // }
          var invest_summ = '';
          var invest_summ_valute = '';
          if(!isNaN($(this).val())) {
            invest_summ = $('#popup_cashflow_row_price_one').val()*$(this).val();
          }
          $('.popup_cashflow_row_summ').text(invest_summ.toFixed(2));

        }
      );


      //$('#popup_add_row_type').off('change');
      $('#popup_add_row_type').on('change', function()
        {
          $('.portfolio_add_lotcnt_label').text('Количество лотов');
          $('.portfolio_add_lotcnt_input').attr('Укажите количество лотов');
          $('#autocomplete_obl_act_all').val('');
			 $('#popup_add_row_free_cache li').remove();//Очищаем данные найденного кеша от прошлых выборок
          if($('#popup_add_row_type').val()=='bonds') {
            $('.portfolio_add_price_label').text('Цена покупки с НКД, в руб');
            $('.obl_hidden').hide();
            $('.act_usd_price').hide();
          }
          else if($('#popup_add_row_type').val()=='shares') {
            $('.portfolio_add_price_label').text('Цена за акцию');
            $('.obl_hidden').show();
            $('.act_usd_price').hide();
          }
          else if($('#popup_add_row_type').val()=='currencies') {
            $('.portfolio_add_lotcnt_input').attr('Укажите количество валюты');
            $('.portfolio_add_lotcnt_label').text('Количество');
            $('.portfolio_add_price_label').text('Цена');
            $('.obl_hidden').hide();
            $('.act_usd_price').hide();
          }
          else if($('#popup_add_row_type').val()=='shares_usa') {
            $('.portfolio_add_price_label').text('Цена за акцию США');
            $('.obl_hidden').show();
            $('.act_usd_price').show();
          }

          else if($('#popup_add_row_type').val()=='shares_etf') {
            $('.portfolio_add_price_label').text('Цена за etf');
            $('.obl_hidden').show();
            $('.act_usd_price').hide();
          }
          clearAddActiveFormFields();
			 checkSummOnAddNewActive();
        }
      );
    }
    //Автокомплит

    $('#popup_change_row_price').off('change input keyup');
    $('#popup_change_row_price').on('change input keyup', function()
      {
        //запрет на ввод всего кроме цифр
        if ($(this).val().match(/[^0-9\.]/g)) {
          $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        }
		  if($('#popup_remove_row_currency_code').val()=='rub')
		   $('#popup_remove_row_currency_rate').val($(this).val());
        var invest_summ = '';
        var lotprice = 0;
        if(!isNaN($(this).val())) {
          lotprice = $(this).val()*$('#popup_remove_row_active_inlot_cnt').val();
          lotprice_valute = (lotprice/Number($('#popup_remove_row_currency_rate').val()).toFixed(2));
        }
        $('.popup_remove_row_price_lot').text(lotprice.toFixed(2)).attr('data-price_lot', lotprice.toFixed(2)).attr('data-price_lot_valute', lotprice_valute);


        if(!isNaN($('#popup_change_row_number').val()) && Number($('#popup_change_row_number').val())>0) {
          $('#popup_change_row_number').change();
        }
      }
    );

    $('#popup_change_row_number').off('change input keyup');
    $('#popup_change_row_number').on('change input keyup', function()
      {
        if ($(this).val().match(/[^0-9\.]/g)) {
          $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        }
       // calcLotPriceDelActivePopup();
                var invest_summ = '';
        var lotprice_valute = 0;
        var lotnumber = $('#popup_change_row_number').val();
        var currency_code = $('#popup_remove_row_currency_code').val();
            if(!isNaN($(this).val())) {
            	invest_summ = Number($('.popup_remove_row_price_lot').attr('data-price_lot'))*$(this).val();
		         if(currency_code!=='rub' && Number($('#popup_remove_row_currency_rate').val())>0){
		           var lotprice = ($('#popup_remove_row_active_inlot_cnt').val()*Number($('#popup_change_row_price').val()).toFixed(2));
					  if($('#popup_remove_row_type').val()=='currency'){
						lotprice_valute = 1;
					  } else {
						lotprice_valute = (lotprice/Number($('#popup_remove_row_currency_rate').val()).toFixed(2));
					  }

		          $('.popup_remove_row_price_lot').text(lotprice + ' ('+ lotprice_valute.toFixed(2) +' '+currency_code+')' ).attr('data-price_lot', lotprice).attr('data-price_lot_valute', lotprice_valute);
		          }
            }
            $('.popup_remove_row_summ').text(invest_summ.toFixed(2));
         if(currency_code!=='rub'){
           $('.popup_remove_row_summ').text( $('.popup_remove_row_summ').text() + ' ('+(lotprice_valute*Number(lotnumber)).toFixed(2)+' '+currency_code+')');
         }
      }
    );

    //Поле кол-ва в попапе изъятия
    $('#popup_exclude_row_number').off('change input keyup');
    $('#popup_exclude_row_number').on('change input keyup', function()
      {
        if ($(this).val().match(/[^0-9\.]/g)) {
          $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        }

        var invest_summ = '';
        var lotprice_valute = 0;
        var lotnumber = $('#popup_exclude_row_number').val();
        var currency_code = $('#popup_exclude_row_currency_code').val();
        if(!isNaN($(this).val())) {

          invest_summ = Number($('#popup_exclude_row_price').val())*$(this).val();
          /*      if(Number($('#popup_exclude_row_currency_rate').val())>1){
            var lotprice = ($('#popup_exclude_row_active_inlot_cnt').val()*Number($('#popup_exclude_row_price').val()).toFixed(2));
            lotprice_valute = lotnumber;
           $('.popup_remove_row_price_lot').text(lotprice + ' ('+ lotprice_valute.toFixed(2) +' '+currency_code+')' ).attr('data-price_lot', lotprice).attr('data-price_lot_valute', lotprice_valute);
           }*/
        }
        $('.popup_exclude_row_summ').text(invest_summ.toFixed(2));
        if(Number($('#popup_exclude_row_currency_rate').val())>1) {
          $('.popup_exclude_row_summ').text($('.popup_exclude_row_summ').text() + ' ('+Number(lotnumber).toFixed(2)+' '+currency_code+')');
        }
      }
    );

    //Поле цена в попапе изъятия
    $('#popup_exclude_row_price').off('change input keyup');
    $('#popup_exclude_row_price').on('change input keyup', function()
      {
        //запрет на ввод всего кроме цифр
        if ($(this).val().match(/[^0-9\.]/g)) {
          $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        }

        var invest_summ = '';
        var lotprice = 0;
        var lotnumber = $('#popup_exclude_row_number').val();
        if(!isNaN($(this).val())) {
          lotprice = $(this).val()*$('#popup_exclude_row_active_inlot_cnt').val();
          lotprice_valute = (Number(lotnumber).toFixed(2));
        }

        if(!isNaN($('#popup_exclude_row_number').val()) && Number($('#popup_exclude_row_number').val())>0) {
          $('#popup_exclude_row_number').change();
        }
      }
    );


    $('.portfolio_tables_selector li:not(.popup_add_btn)').on('click', function()
      {
        var elementId = $('#portfolios_select').val().replace("portfolio_", "");

        $('.portfolio_tables_selector li').removeClass('active');
        $(this).addClass('active');
        setPortfolioStatesToSession(elementId, 'portfolio_tables_selector', $(this).data('value')); //Сохраняем в сессию через ajax
        // $('.portfolio_tabs_table').hide();
        // $('.'+$(this).data('value')).show();
        $('.portfolio_valuation_table .portfolio_tabs_table__outer').css('display', 'none');
        $('.'+$(this).data('value')).closest('.portfolio_tabs_table__outer').css('display', 'block');

      }
    );

    //Кнопка удаления записи истории, а вместе с ней сопутствующих записей  в hl28
    $('body').on('click', '.hist-edit-delete-button', function()
      {
        var elementId = $('#portfolios_select').val().replace("portfolio_", "");
        var popup = $(this).parents('#popup_edit_row_history_portfolio');
        var histRowId = popup.find('#hist_edit_row_id').val();
        var histRowAction = popup.find('#hist_edit_row_action').val();
        //  console.log(histRowId);
        var arr = {'histRowId': histRowId, 'histRowAction': histRowAction};


        //console.log(arr);
        showPopupProgress('popup_edit_row_history_portfolio');
        setTimeout(function() {
          $.ajax(
              {
                type: "POST",
                url: "/ajax/portfolio/portfolio.php",
                dataType: "json",
                data: {ajax: "y", action: 'deleteHistoryDeal', portfolioId: elementId, data: arr, owner: 'popup'}
                ,
                cashe: false,
                async: false,
                success: function(data) {

                  setPortfolioStatesToSession(elementId, 'actives_filter', '');
                  portfolioChange(true);
                  $('#popup_edit_row_history_portfolio').modal('hide');

                  clearAddActiveFormFields();
                  recountTable();//Пересчитываем строки в таблице оценки портфеля
						//Обновляем кол-ва активов в фильтре по типам активов
						updateActiveTypeFilterCnt();

                  hidePopupProgress();

                  setVisibleAutobalanceButton($('#portfolios_select').val().replace("portfolio_", ""));
						updateActiveTabOnActiveChange();
                }
              }
          );
        }, 100);
      }
    );


    //Кнопка редактирования записи истории в hl28
    $('body').on('click', '.hist-edit-apply-button', function()
      {
        var elementId = $('#portfolios_select').val().replace("portfolio_", "");
        var popup = $(this).parents('#popup_edit_row_history_portfolio');
        var dealId = popup.find('#hist_edit_row_id').val();
        var dealCnt = popup.find('#hist_edit_cnt_row_number').val();
        var inlotCnt = popup.find('#popup_hist_edit_active_inlot_cnt').val();
        var dealPrice = popup.find('#hist_edit_row_price').val()*inlotCnt;
		  var price_one_currency = $('#popup_hist_edit_active_price_one').val();
        var initDate = popup.find('#popup_hist_edit_init_date').val();
        var dealDate = popup.find('#hist_edit_row_date').val();
        var dealDirection = popup.find('#hist_edit_row_action').val();
        var lastpriceActiveValute = popup.find('#popup_hist_edit_active_lastprice_valute').val();
        //  console.log(histRowId);

			var pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
			var dt1 = new Date(initDate.replace(pattern,'$3-$2-$1'));
			var oDateOne = new Date(dt1);
			var dt2 = new Date(dealDate.replace(pattern,'$3-$2-$1'));
			var oDateTwo = new Date(dt2);
			var recountDate = dealDate;
			  if(oDateOne - oDateTwo <= 0){
			    recountDate = initDate;
			  }
        var arr = {
        		'dealId': dealId,
				'price_one_currency':price_one_currency,
				'dealPrice': dealPrice,
				'dealCnt': dealCnt,
				'recountDate':recountDate,
				'dealDate': dealDate,
				'lastpriceActiveValute': lastpriceActiveValute,
        		'dealDirection': dealDirection,
        };

        //console.log(arr);
        showPopupProgress('popup_edit_row_history_portfolio');
        $.ajax(
          {
          type: "POST",
          url: "/ajax/portfolio/portfolio.php",
          dataType: "json",
          data: {ajax: "y", action: 'editHistoryDeal', portfolioId: elementId, data: arr, owner: 'popup'},
          cashe: false,
          async: false,
          success: function(data) {

                  if(!data.hist_start_date.length || data.hist_start_date.length==0){
                    updateHistoryStartDate();
                  } else {
                    updateHistoryStartDate(data.hist_start_date);
                  }

              setPortfolioStatesToSession(elementId, 'actives_filter', '');
              portfolioChange(true);
              $('#popup_edit_row_history_portfolio').modal('hide');

              clearAddActiveFormFields();
				  recountTable(); //Пересчитываем строки в таблице оценки портфеля
              hidePopupProgress();

              setVisibleAutobalanceButton($('#portfolios_select').val().replace("portfolio_", ""));
				  checkSubzeroCache();
				  updateActiveTabOnActiveChange();
            }
          }
        );
      }
    );

    //Кнопка добавления актива из попапа добавления
    $('.add-active-button').on('click', function()
      {
        var popup = $('#popup_add_row');
        popup.find('.popup_progress').addClass('active');
        var ths = $(this);

        setTimeout(function() {
          if ($('#popup_add_row_active_id').val()=='' || $('#popup_add_row .search-form__input').val() == '' || $('#popup_add_row_price').val() == '' || $('#popup_add_row_number').val() == '' || $('#popup_add_row_date').val() == '') {
            if ($('#popup_add_row .error_mess_empty').length == 0) {
              ths.before('<p class="error_mess_empty red">Одно или несколько полей не заполнено</span>');
            }
            return false;
          }
          var elementId = $('#portfolios_select').val().replace("portfolio_", "");
          if(ths.parents('.modal-body').find('#popup_add_row_type').val()=='bonds') {
            var typeActive = 'bond';
          }
          else if(ths.parents('.modal-body').find('#popup_add_row_type').val()=='shares') {
            var typeActive = 'share';
          }
          else if(ths.parents('.modal-body').find('#popup_add_row_type').val()=='shares_usa') {
            var typeActive = 'share_usa';
          }
          else if(ths.parents('.modal-body').find('#popup_add_row_type').val()=='shares_etf') {
            var typeActive = 'share_etf';
          }
          else if(ths.parents('.modal-body').find('#popup_add_row_type').val()=='currencies') {
            var typeActive = 'currency';
          }

          var idActive = ths.parents('.modal-body').find('#popup_add_row_active_id').val();
          var nameActive = ths.parents('.modal-body').find('#autocomplete_obl_act_all').val();
          var priceActive = ths.parents('.modal-body').find('#popup_add_row_price').val();
          var price_one = ths.parents('.modal-body').find('#popup_add_row_price_one').val();
          var lotsize = ths.parents('.modal-body').find('#popup_add_row_active_inlot_cnt').val();
          var cntActive = ths.parents('.modal-body').find('#popup_add_row_number').val();
          var dateActive = ths.parents('.modal-body').find('#popup_add_row_date').val();
          var codeActive = ths.parents('.modal-body').find('#popup_add_row_active_code').val();
          var urlActive = ths.parents('.modal-body').find('#popup_add_row_active_url').val();
          var currencyEmitent = ths.parents('.modal-body').find('#popup_add_row_active_emitent_currency').val();
          var currencyActive = ths.parents('.modal-body').find('#popup_add_row_active_currency_id').val();
          var nameActiveEmitent = ths.parents('.modal-body').find('#popup_add_row_active_emitent_name').val();
          var urlActiveEmitent = ths.parents('.modal-body').find('#popup_add_row_active_emitent_url').val();
          var lastpriceActive = ths.parents('.modal-body').find('#popup_add_row_active_lastprice').val();
          var lastpriceActiveValute = ths.parents('.modal-body').find('#popup_add_row_active_lastprice_valute').val();
          var secid = ths.parents('.modal-body').find('#popup_add_row_active_secid').val();
          var date_price_one = ths.parents('.modal-body').find('#popup_add_row_active_price_orig').val();
          var subzeroCacheOn = $('#popup_add_subzero_cache_on').prop('checked')?'Y':'N';

          var arr = {
          		'typeActive': typeActive,
					'idActive': idActive,
					'nameActive': nameActive,
					'priceActive': priceActive,
            	'cntActive': cntActive,
					'dateActive': dateActive,
					'codeActive': codeActive,
					'urlActive': urlActive,
            	'currencyEmitent': currencyEmitent,
            	'currencyActiveReal': currencyActive,
					'nameActiveEmitent': nameActiveEmitent,
					'urlActiveEmitent': urlActiveEmitent,
            	'lastpriceActive': lastpriceActive,
					'lastpriceActiveValute': lastpriceActiveValute,
					'secid': secid,
					'price_one': price_one,
					'lotsize': lotsize,
					'date_price_one': date_price_one,
            	'subzeroCacheOn': subzeroCacheOn,
          };


          //console.log(arr);
          showPopupProgress('popup_add_row');
          $.ajax(
              {
                type: "POST",
                url: "/ajax/portfolio/portfolio.php",
                dataType: "json",
                data: {ajax: "y", action: 'addPortfolioActive', portfolioId: elementId, data: arr, owner: 'popup'},
                cashe: false,
                async: false,
                success: function(data) {
                  let pid = $('#portfolios_select').val().replace("portfolio_", "");
                  //getResponse(data);
                  /*      if(data=='ok'){       */
                  //$("#actives_filter").val('');
                  if(!data.hist_start_date.length || data.hist_start_date.length==0){
                    updateHistoryStartDate();
                  } else {
                    updateHistoryStartDate(data.hist_start_date);
                  }
                  //console.log('hist_start_date', data.hist_start_date);
                  //$("#select2-actives_filter-container").text('Все').attr('title', 'Все');
                  //setPortfolioStatesToSession(elementId, 'actives_filter', '');
                  portfolioChange(true);
                  $('#popup_add_row').modal('hide');

                  clearAddActiveFormFields();
                  recountTable(); //Пересчитываем строки в таблице оценки портфеля
                  //Обновляем кол-ва активов в фильтре по типам активов
                  //updateActiveTypeFilterCnt();
						//updateTabChartsAndTable(true)
                  historyFilterUpdate(pid, [], true);
                  hidePopupProgress();

                  setVisibleAutobalanceButton(pid);

                  //Добавляем новый актив в список фильтра активов. После при перезагрузке страницы список будет сформирован на сервере уже по существующим активам
                  //AddActiveToHistoryTableFilter(arr);

                  updateActiveTabOnActiveChange();
                  /*      }else{
           console.log(data);
           }*/
                }
              }
          );
        }, 200);
      }
    );

	 function updateHistoryStartDate(date=''){
	 	var pid = $('#portfolios_select').val().replace("portfolio_", "");
		if(!date.length || date.length==0){
	 	$.ajax(
          {
          type: "POST",
          url: "/ajax/portfolio/portfolio.php",
          dataType: "json",
          data: {ajax: "y", action: 'getHistoryStartDate', portfolioId: pid},
          cashe: false,
          async: false,
          success: function(data) {
					//$('#portfolio_start_date').val(data);
					document.getElementById('portfolio_start_date').setAttribute('value',data);
					$('#phtf_date_from').attr('data-dt-start', data).attr('data-dt-start', data);
            }
          }
        );
		  } else {
		  	console.log('portfolio_start_date', date);
			document.getElementById('portfolio_start_date').setAttribute('value',date);
			//document.getElementById('phtf_date_from').setAttribute('value',date).setAttribute('data-dt-start',date);
			$('#phtf_date_from').attr('data-dt-start', date).attr('data-dt-start', date);
			  //$('#portfolio_start_date').val(date);
		  }
	 }

    //Кнопка внесения валюты из попапа внесения
    $('.insert-currency-button').on('click', function()
      {
        var popup = $('#popup_add_cache_row');
        popup.find('.popup_progress').addClass('active');
        var ths = $(this);

        setTimeout(function() {
          if ($('#popup_insert_row_active_id').val()=='' || $('#popup_insert_row_price_one').val() == '' ||
              $('#popup_insert_row_number').val() == '' || $('#popup_insert_row_date').val() == '') {
            if ($('#popup_insert_row .error_mess_empty').length == 0) {
              ths.before('<p class="error_mess_empty red">Одно или несколько полей не заполнено</span>');
            }
            return false;
          }
          var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
          var typeActive = 'currency';


          var idActive = ths.parents('.modal-body').find('#popup_insert_row_active_id').val();
          var nameActive = ths.parents('.modal-body').find('#popup_add_cache_selector option:selected').text();
          var priceActive = ths.parents('.modal-body').find('#popup_insert_row_price_one').val();
          var price_one = ths.parents('.modal-body').find('#popup_insert_row_price_one').val();
          var lotsize = ths.parents('.modal-body').find('#popup_insert_row_active_inlot_cnt').val();
          var cntActive = ths.parents('.modal-body').find('#popup_insert_row_number').val();
          var dateActive = ths.parents('.modal-body').find('#popup_insert_row_date').val();
          var codeActive = ths.parents('.modal-body').find('#popup_insert_row_active_secid').val();
          var urlActive = ths.parents('.modal-body').find('#popup_insert_row_active_url').val();
          //var currencyEmitent = $(this).parents('.modal-body').find('#popup_insert_row_active_emitent_currency').val();
          var currencyActive = ths.parents('.modal-body').find('#popup_insert_row_active_currency_id').val();
          var nameActiveEmitent = nameActive;
          var urlActiveEmitent = urlActive;
          var lastpriceActive = ths.parents('.modal-body').find('#popup_insert_row_active_lastprice').val();
          var lastpriceActiveValute = ths.parents('.modal-body').find('#popup_insert_row_active_lastprice_valute').val();
          var secid = ths.parents('.modal-body').find('#popup_insert_row_active_secid').val();
          var date_price_one = ths.parents('.modal-body').find('#popup_insert_row_active_price_orig').val();

          var arr = {
          		'typeActive': typeActive,
					'idActive': idActive,
					'nameActive': nameActive,
					'priceActive': priceActive,
            	'cntActive': cntActive,
					'dateActive': dateActive,
					'codeActive': codeActive,
					'urlActive': urlActive,
            	'currencyEmitent': currencyActive.toLowerCase(),
					'nameActiveEmitent': nameActiveEmitent,
					'urlActiveEmitent': urlActiveEmitent,
            	'lastpriceActive': lastpriceActive,
					'lastpriceActiveValute': lastpriceActiveValute,
					'secid': secid,
					'price_one': price_one,
					'lotsize': lotsize,
            	'date_price_one': date_price_one,
					'insert': 'Y'
          };


          //console.log(arr);
          showPopupProgress('popup_add_cache_row');
          $.ajax(
              {
                type: "POST",
                url: "/ajax/portfolio/portfolio.php",
                dataType: "text",
                data: {'ajax': "y", 'action': 'addPortfolioActive', 'portfolioId': portfolioId, 'data': arr, 'owner': 'popup'}
                ,
                cashe: false,
                async: false,
                success: function(data) {
                  //getResponse(data);
                  /*      if(data=='ok'){       */
                  $("#actives_filter").val('');
                  $("#select2-actives_filter-container").text('Все').attr('title', 'Все');
                  setPortfolioStatesToSession(portfolioId, 'actives_filter', '');
                  portfolioChange(true);
                  $('#popup_add_cache_row').modal('hide');

                  clearAddActiveFormFields();
                  hidePopupProgress();

                  setVisibleAutobalanceButton($('#portfolios_select').val().replace("portfolio_", ""));
                  checkSubzeroCache();
                  updateActiveTabOnActiveChange();
                  /*      }else{
           console.log(data);
           }*/
                }
              }
          );
        }, 200);

      }
    );

    //Кнопка внесения валюты из попапа внесения от амортизации
    $('.cashflow-currency-button').on('click', function()
      {
        var popup = $('#popup_add_cashflow_to_portfolio');
        popup.find('.popup_progress').addClass('active');
        var ths = $(this);

        setTimeout(function() {
          if ($('#popup_cashflow_row_active_id').val()=='' || $('#popup_cashflow_row_price_one').val() == '' ||
              $('#popup_cashflow_row_number').val() == '' || $('#popup_cashflow_row_date').val() == '') {
            if ($('#popup_cashflow_row .error_mess_empty').length == 0) {
              ths.before('<p class="error_mess_empty red">Одно или несколько полей не заполнено</span>');
            }
            return false;
          }
          var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
          var typeActive = 'currency';
          var actionType = $('#popup_cashflow_description option:selected').attr('data-type'); //Определение типа вносимых данных (див, купон, аморт, налог, комиссия)

          var idActive = ths.parents('.modal-body').find('#popup_cashflow_row_active_id').val();
          var nameActive = ths.parents('.modal-body').find('#popup_cashflow_cache_selector option:selected').text();
          var priceActive = ths.parents('.modal-body').find('#popup_cashflow_row_price_one').val();
          var price_one = ths.parents('.modal-body').find('#popup_cashflow_row_price_one').val();
          var lotsize = ths.parents('.modal-body').find('#popup_cashflow_row_active_inlot_cnt').val();
          var cntActive = ths.parents('.modal-body').find('#popup_cashflow_row_number').val();
          var dateActive = ths.parents('.modal-body').find('#popup_cashflow_row_date').val();
          var codeActive = ths.parents('.modal-body').find('#popup_cashflow_row_active_secid').val();
          var urlActive = ths.parents('.modal-body').find('#popup_cashflow_row_active_url').val();
          //var currencyEmitent = $(this).parents('.modal-body').find('#popup_cashflow_row_active_emitent_currency').val();
          var currencyActive = ths.parents('.modal-body').find('#popup_cashflow_row_active_currency_id').val();
          var nameActiveEmitent = nameActive;
          var urlActiveEmitent = urlActive;
          var lastpriceActive = ths.parents('.modal-body').find('#popup_cashflow_row_active_lastprice').val();
          var lastpriceActiveValute = ths.parents('.modal-body').find('#popup_cashflow_row_active_lastprice_valute').val();
          var secid = ths.parents('.modal-body').find('#popup_cashflow_row_active_secid').val();
          var date_price_one = ths.parents('.modal-body').find('#popup_cashflow_row_active_price_orig').val();
          var description = ths.parents('.modal-body').find('#popup_cashflow_description option:selected').val()+' '+ths.parents('.modal-body').find('#popup_cashflow_debstock_selector').val();
          var arr = {
          				'typeActive': typeActive,
							'actionType': actionType,
							'idActive': idActive,
							'nameActive': nameActive,
							'priceActive': priceActive,
            			'cntActive': cntActive,
							'dateActive': dateActive,
							'codeActive': codeActive,
							'urlActive': urlActive,
            			'currencyEmitent': currencyActive.toLowerCase(),
							'nameActiveEmitent': nameActiveEmitent,
							'urlActiveEmitent': urlActiveEmitent,
            			'lastpriceActive': lastpriceActive,
							'lastpriceActiveValute': lastpriceActiveValute,
							'secid': secid,
							'price_one': price_one,
							'lotsize': lotsize,
            			'date_price_one': date_price_one,
							'insert': 'Y',
							'description': description
          			  };

          var action = 'addPortfolioActive';
          if(actionType=='tax' || actionType=='comission'){
            action = 'insertTaxComission';
            var arr = {
            			  'deal_id': '',
							  'actionType': actionType,
							  'portfolioId': portfolioId,
							  'report_version': '',
							  'finished': '',
							  'typeActive':
							  'currency',
							  'idActive': idActive,
              			  'nameActive': nameActive,
							  'priceActive': priceActive,
							  'cnt': cntActive,
							  'dateActive': dateActive,
							  'codeActive': codeActive,
              			  'urlActive': urlActive,
							  'currencyEmitent': currencyActive.toLowerCase(),
							  'nameActiveEmitent': nameActiveEmitent,
              			  'urlActiveEmitent': urlActiveEmitent,
							  'lastpriceActive': lastpriceActive,
							  'secid': secid,
							  'price_one': price_one,
              			  'lotsize': lotsize,
							  'date_price_one': date_price_one,
							  'description': description,
							  'exclude': 'N'
							 };
          }
          //console.log(arr);
          showPopupProgress('popup_add_cashflow_to_portfolio');
          $.ajax(
              {
                type: "POST",
                url: "/ajax/portfolio/portfolio.php",
                dataType: "text",
                data: {'ajax': "y", 'action': action, 'portfolioId': portfolioId, 'data': arr, 'owner': 'popup'}
                ,
                cashe: false,
                async: false,
                success: function(data) {
                  //getResponse(data);
                  /*      if(data=='ok'){       */
                  $("#actives_filter").val('');
                  $("#select2-actives_filter-container").text('Все').attr('title', 'Все');
                  setPortfolioStatesToSession(portfolioId, 'actives_filter', '');
                  portfolioChange(true);
                  $('#popup_add_cashflow_to_portfolio').modal('hide');

                  clearAddActiveFormFields();
                  hidePopupProgress();

                  setVisibleAutobalanceButton($('#portfolios_select').val().replace("portfolio_", ""));
                  checkSubzeroCache();
                  updateActiveTabOnActiveChange();
                  /*      }else{
           console.log(data);
           }*/
                },
                error: function(xhr, status, error){
                  console.log('ошибка добавление актива: ', xhr.responseText);
                }

              }
          );
        }, 200);
      }
    );

    //Навигация по истории переключения страниц портфелей
    window.addEventListener('popstate', function(event)
      {
        // The popstate event is fired each time when the current history entry changes.
        // Call Back button programmatically as per user confirmation.
        history.back();
        // Uncomment below line to redirect to the previous page instead.
        window.location = document.referrer // Note: IE11 is not supporting this.
        history.pushState(null, null, window.location.pathname);
      }
      , false);

    //Событие показа модального окна внесения валюты в портфель
    $('#popup_add_cache_row').off('show.bs.modal');
    $('#popup_add_cache_row').on('show.bs.modal', function(e)
      {
        $('#popup_insert_row_date').change();
      }
    );

    //Событие показа модального окна изъятия валюты из портфеля
    $('#popup_exclude_cache_row').off('shown.bs.modal');
    $('#popup_exclude_cache_row').on('shown.bs.modal', function(e)
      {
        var firstActiveId = $('#filled_from_active').val();
        var activeSelector = $(this).find('select.popup_exclude_cache_selector');
        activeSelector.find('option').remove();
        if(!$('.portfolio_tabs_table.pt_active tbody tr[data-elem=currency]').length) {
          activeSelector.append('<option value="">Валюты не найдены</option>');
          $(this).find('.exclude-active-button').addClass('hidden');
        }
        else {
          $(this).find('.exclude-active-button').removeClass('hidden');
        }
       if($('#popup_exclude_cache_selector').val()==null){
        $('.portfolio_tabs_table.pt_active tbody tr[data-elem=currency]').each(function(indx, element)
          {
            let activeId = $(element).attr('data-iblock_elem_id');
            let activeName = $(element).find('.elem_name a').attr('data-original-title');
            activeSelector.append('<option value="'+activeId+'" '+((firstActiveId>0 && firstActiveId==activeId)? 'selected': '')+'>'+activeName+'</option>');

            if(firstActiveId==0) {
              firstActiveId = activeId;
            }

            if(firstActiveId>0) {
              fillActiveExcludePopup(firstActiveId);
            }
          }
        );
			}
        $('#popup_exclude_row_date').change();
      }
    );


    /*+ события инпутов фильтра таблицы истории */
    function AddActiveToHistoryTableFilter(arActive) {
      //   console.log(arActive);
      var add = true;
      $("#phtf_active option").each(function(indx, element)
        {
          if($(element).val()==arActive.idActive) {
            add = false;
            return false;
          }
        }
      );

      if(add) {
        $("#phtf_active").append('<option value="'+arActive.idActive+'" data-isin-code="'+arActive.codeActive+'">'+arActive.nameActive+'</option>');
      }
    }

	 //Кнопка показа автоопераций в фильре истории
	 $('body').on('change', '#phtf_Table_showAutoOperation', function()
      {
        //$('#phtf_Table_showAutoOperation').prop('checked')?'Y':'N';
        var ths = $(this);
        var arFilter =[];
        var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
        //if(ths.data('en')=='N') return false;
        if (ths.prop('checked') == true) {
          //  $('#portfolio_date, .p_table_input').removeAttr('disabled');
          $(this).next('label').text('Показывать связанные');
          //arFilter.push({'showChildDeals': true});
        }
        else {
          //  $('#portfolio_date, .p_table_input').attr('disabled', true);
          $(this).next('label').text('Скрывать связанные');
          //arFilter.push({'showChildDeals': false});
        }
        arFilter = getHistoryTableFilterFromHTML();
        historyTableUpdate(portfolioId, arFilter);
      }
    );

	 //Выбор селекта актива в фильтре таблицы истории
    $('body').on('change', '#phtf_active', function()
      {
        overlayShow();

        setTimeout(function() {
          var arFilter = new Map();
          var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");

          historyGraphicRebuild();
          console.log('historyGraphicRebuild -> change #phtf_active');
          arFilter = getHistoryTableFilterFromHTML();
          historyTableUpdate(portfolioId, arFilter);
        }, 10);
      }
    );

	 //Выбор селекта типа движения в фильтре таблицы истории
    $('body').on('change', '#phtf_historyActions', function()
      {
        overlayShow();

        setTimeout(function() {
          var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
          var arFilter = new Map();
          arFilter = getHistoryTableFilterFromHTML();
          historyTableUpdate(portfolioId, arFilter);
        });
      }
    );
    /*- события инпутов фильтра таблицы истории */

    $('select#popup_exclude_cache_selector').on('change', function()
      {
        fillActiveExcludePopup($('select#popup_exclude_cache_selector').val());
      }
    );

	 //Сохранение флага разрешения автокомпенсации отриц. кеша в портфеле
    $('body').on('change', '#popup_add_subzero_cache_on', function()
      {
        checkSummOnAddNewActive();
        var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
		  var checked = '';
		  if($(this).prop('checked')){
			checked='Y';
		  }
          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/portfolio.php",
            dataType: "json",
            data: {ajax: "y", action: 'setSubzeroCacheOn', 'portfolioId':portfolioId, 'checked':checked },
            cashe: false,
            async: false,
            success: function(data) {
					  if(data["value"].length>0){
						$('#portfolio_subzero_cache_on').val('Y');
					  } else {
						$('#portfolio_subzero_cache_on').val('');
					  }
              }
            }
          );


      }
    );

    //Событие показа модального окна добавления актива
    $('#popup_add_row').off('show.bs.modal');
    $('#popup_add_row').on('show.bs.modal', function(e)
      {
        //Выставляем селектор действий согласно открытой форме
        var current_popup = $(this).find('.popup_portfolio_action_selector').attr('data-current-popup');
        $(this).find('.popup_portfolio_action_selector').val(current_popup).trigger('change.select2');

		  if($('#portfolio_subzero_cache_on').val()=='Y'){
			$('#popup_add_subzero_cache_on').prop('checked', true);
		  } else {
			$('#popup_add_subzero_cache_on').prop('checked', false);
		  }

          /* отключение поля "курс", если выбран рубль */
        onShowCashflowModal();
      }
    );

    //Событие показа модального окна загрузки брокерского отчета
    $('#popup_load_from_broker2').off('shown.bs.modal');
    $('#popup_load_from_broker2').on('shown.bs.modal', function(e) {
        //Выставляем селектор действий согласно открытой форме
        var current_popup = $(this);
		  var portfolio_broker = $('#portfolio_broker').val();
		  var pid = $('#portfolios_select').val().replace("portfolio_", "");
		  //Если в портфель уже загружались отчеты - выставляем при показе окна загрузки привязанного брокера
		  if(portfolio_broker.length>0)
         $(this).find('#popup_load_broker').val(portfolio_broker).change().trigger('change.select2');

      //Выставляем портфель для загрузки текущим
      //setTimeout(function() {
        $(this).find('#popup_load_broker_portfolio').val(pid).change().trigger('change.select2');
      //}, 100);
    });


    //Событие показа модального окна добавления амортизации
    $('#popup_add_cashflow_to_portfolio').off('shown.bs.modal');
    $('#popup_add_cashflow_to_portfolio').on('shown.bs.modal', function(e)
      {
        //Выставляем селектор действий согласно открытой форме
        var current_popup = $(this).find('.popup_portfolio_action_selector').attr('data-current-popup');
        $(this).find('.popup_portfolio_action_selector').val(current_popup).trigger('change.select2');
		  $(this).find('#popup_cashflow_row_date').change();
        //Заполняем селектор активами
        var activeSelector = $(this).find('select.popup_cashflow_debstock_selector');
        var firstActiveId = $('#popup_cashflow_filled_from_active').val();
        //activeSelector.find('option').remove();
        $('.portfolio_tabs_table.pt_active tbody tr:not([data-elem=currency])').each(function(indx, element)
          {
            let activeId = $(element).attr('data-iblock_elem_id');
            let activeName = $(element).find('.elem_name a').attr('data-original-title');
            let activeType = $(element).attr('data-elem');
            let activeSecid = $(element).attr('data-secid');
            //activeSelector.append('<option value="'+activeId+'" '+((firstActiveId>0 && firstActiveId==activeId)? 'selected': '')+'>'+activeName+'</option>');
            if(firstActiveId==0) {
              firstActiveId = activeId;
            }


          }
        );


      }
    );

    //Событие показа модального окна удаления актива
    $('#popup_change_row').off('shown.bs.modal');
    $('#popup_change_row').on('shown.bs.modal', function(e)
      {
        //Выставляем селектор действий согласно открытой форме
        var current_popup = $(this).find('.popup_portfolio_action_selector').attr('data-current-popup');
        var this_popup = $(this);
        $(this).find('.popup_portfolio_action_selector').val(current_popup).trigger('change.select2');

        //Заполняем селектор активами
        var activeSelector = $(this).find('select.popup_del_active_selector');
        var firstActiveId = $('#filled_from_active').val();
        activeSelector.find('option').remove();
        $('.portfolio_tabs_table.pt_active tbody tr').each(function(indx, element)
          {
            let activeId = $(element).attr('data-iblock_elem_id');
            let activeName = $(element).find('.elem_name a').attr('data-original-title');
            let activeType = $(element).attr('data-elem');
            let activeSecid = $(element).attr('data-secid');
            if(activeType=='currency' && activeSecid=='RUB') {

            }
            else {
              activeSelector.append('<option value="'+activeId+'" '+((firstActiveId>0 && firstActiveId==activeId)? 'selected': '')+'>'+activeName+'</option>');

              if(firstActiveId==0) {
                firstActiveId = activeId;
              }
            }

          }
        );

        if(firstActiveId>0) {
          fillActiveDeletePopup(firstActiveId);
        }
        var activeType = this_popup.find('#popup_remove_row_type').val();
        if(activeType=='bond') {
          $('.portfolio_hist_edit_price_label').text('Цена с НКД, в руб');
          this_popup.find('.obl_hidden').hide();
			 if($('#popup_remove_row_currency_code').val()=='rub')
          	this_popup.find('.act_usd_price').hide();
        }
        else if(activeType=='share') {
          $('.portfolio_hist_edit_price_label').text('Цена за акцию');
          this_popup.find('.obl_hidden').show();
          this_popup.find('.act_usd_price').hide();
        }
        else if(activeType=='currency') {
          $('.portfolio_add_lotcnt_input').attr('Укажите количество валюты');
          $('.portfolio_add_lotcnt_label').text('Количество');
          $('.portfolio_hist_edit_price_label').text('Цена');
          this_popup.find('.obl_hidden').hide();
          this_popup.find('.act_usd_price').hide();
        }
        else if(activeType=='share_usa') {
          $('.portfolio_hist_edit_price_label').text('Цена за акцию США');
          this_popup.find('.obl_hidden').show();
          this_popup.find('.act_usd_price').show();
        }

        else if(activeType=='share_etf') {
          $('.portfolio_hist_edit_price_label').text('Цена за etf');
          this_popup.find('.obl_hidden').show();
          this_popup.find('.act_usd_price').hide();
        }

		  if($('#popup_remove_row_currency_code').val()=='rub'){  //для рублевых активов заменяет текст в лейблах на значения без валютных сумм в скобках
			 $('.popup_remove_row_price_lot').text($('#popup_remove_row_currency_rate').val());
			 $('.popup_remove_row_summ').text(Number($('#popup_remove_row_currency_rate').val())*$('#popup_change_row_number').val());
		  }

      }
    );

    $('select#popup_del_active_selector').on('change', function()
      {
        fillActiveDeletePopup($('select#popup_del_active_selector').val());
      }
    );


    //Событие показа модального окно удаления портфеля
    $('#popup_portfolio_remove_row').on('show.bs.modal', function(e)
      {
        var button = $(e.relatedTarget);
        var elementName = '';
        var elementId = '';
        if($('#portfolios_select').length>0) {
          elementId = $('#portfolios_select').val().replace("portfolio_", "");
          elementName = $('#portfolios_select option:selected').text();
        }
        else {
          elementName = button.data('portfolio-name');
          elementId = button.data('portfolio-id');
          $(e.currentTarget).find('.remove-portfolio-button').attr('data-portfolio-id', elementId);

        }

        var portolio_name = $(e.currentTarget).find('#del_portfolio_name');
        portolio_name.text(elementName);
        $(portolio_name).attr('data-portfolio-id', elementId);


      }
    );

    //Событие показа модального окно очистки портфеля
    $('#popup_portfolio_clear_row').on('show.bs.modal', function(e)
      {
        var button = $(e.relatedTarget);
        var elementName = '';
        var elementId = '';
        if($('#portfolios_select').length>0) {
          elementId = $('#portfolios_select').val().replace("portfolio_", "");
          elementName = $('#portfolios_select option:selected').text();
        }
        else {
          elementName = button.data('portfolio-name');
          elementId = button.data('portfolio-id');
          $(e.currentTarget).find('.clear-portfolio-button').attr('data-portfolio-id', elementId);

        }

        var portolio_name = $(e.currentTarget).find('#clear_portfolio_name');
        portolio_name.text(elementName);
        $(portolio_name).attr('data-portfolio-id', elementId);


      }
    );

    //Событие показа модального окна редактирования истории
    $('#popup_edit_row_history_portfolio').off('shown.bs.modal');
    $('#popup_edit_row_history_portfolio').on('shown.bs.modal', function(e)
      {
        var this_popup = $('#popup_edit_row_history_portfolio');
        var buttonData = $('.history_table').find('tr.row-id-'+$('#hist_edit_row_id').val()+' .edit_history_btn');
        var operation_date = buttonData.attr('data-operation-date');
        //console.log(operation_date);
        var price = buttonData.attr('data-price');
        var price_one = buttonData.attr('data-active-price-one');
        var cnt = buttonData.attr('data-cnt');
        var summ = Number(price)*Number(cnt);
        var row = buttonData.closest('tr');
        var activeType = row.attr('data-type-active');
        this_popup.find('#hist_edit_row_action').val(row.attr('data-row-direction'));
        this_popup.find('#popup_hist_edit_active_name').text(row.find('.elem_name a').attr('data-original-title'));
        this_popup.find('#popup_hist_edit_operation_name').text(row.find('.history_operation_name').text());

        this_popup.find('#hist_edit_row_price').val(price_one);
        this_popup.find('#popup_hist_edit_active_price_one').val(price_one);

        this_popup.find('#popup_hist_edit_active_lastprice').val(price);
        this_popup.find('.popup_hist_edit_row_price_lot').text(price);
        this_popup.find('#hist_edit_cnt_row_number').val(cnt);
        this_popup.find('#hist_edit_row_summ').text(summ);
        this_popup.find('#popup_hist_edit_active_type').val(activeType);
        this_popup.find('#popup_hist_edit_active_code').val(row.attr('data-active-code'));
        this_popup.find('#popup_hist_edit_active_secid').val(row.attr('data-active-secid'));
        this_popup.find('#popup_hist_edit_active_inlot_cnt').val(row.attr('data-active-inlot-cnt'));
        this_popup.find('#popup_hist_edit_currency_id').val(row.attr('data-active-currency'));
        this_popup.find('#popup_hist_edit_init_date').val(operation_date);
        this_popup.find('#hist_edit_row_date').val(operation_date).attr('useExistPrice', 'Y').datepicker('update', operation_date);
        //Видимость полей а зависимости от актива и замена текстовых названий
        $('.portfolio_hist_edit_lotcnt_label').text('Количество лотов');
        $('#hist_edit_cnt_row_number').attr('placeholder', 'Укажите количество лотов');

        if(activeType=='bond' && row.attr('data-active-currency')=='rub') {
          $('.portfolio_hist_edit_price_label').text('Цена с НКД, в руб');
          this_popup.find('.obl_hidden').hide();
          this_popup.find('.act_usd_price').hide();
        } else if(activeType=='bond' && row.attr('data-active-currency')!='rub' ) {
          $('.portfolio_hist_edit_price_label').text('Цена с НКД, в руб');
          this_popup.find('.obl_hidden').hide();
          this_popup.find('.act_usd_price').show();
        }
        else if(activeType=='share') {
          $('.portfolio_hist_edit_price_label').text('Цена за акцию');
          this_popup.find('.obl_hidden').show();
          this_popup.find('.act_usd_price').hide();
        }
        else if(activeType=='currency') {
          $('.portfolio_add_lotcnt_input').attr('Укажите количество валюты');
          $('.portfolio_add_lotcnt_label').text('Количество');
          $('.portfolio_hist_edit_price_label').text('Цена');
          this_popup.find('.obl_hidden').hide();
          this_popup.find('.act_usd_price').hide();
        }
        else if(activeType=='share_usa') {
          $('.portfolio_hist_edit_price_label').text('Цена за акцию США');
          this_popup.find('.obl_hidden').show();
          this_popup.find('.act_usd_price').show();
        }

        else if(activeType=='share_etf' && row.attr('data-active-currency')=='rub') {
          $('.portfolio_hist_edit_price_label').text('Цена за etf');
          this_popup.find('.obl_hidden').show();
          this_popup.find('.act_usd_price').hide();
        } else if(activeType=='share_etf' && row.attr('data-active-currency')!='rub') {
          $('.portfolio_hist_edit_price_label').text('Цена за etf');
          this_popup.find('.obl_hidden').show();
          this_popup.find('.act_usd_price').show();
        }

      }
    );

    //Событие показа модального окна копирования портфеля
    $('#popup_copy_portfolio').off('show.bs.modal');
    $('#popup_copy_portfolio').on('show.bs.modal', function(e)
      {
        var elementId = $('#portfolios_select').val().replace("portfolio_", "");
        var portolio_name = $(e.currentTarget).find('#copied_portfolio_name');
        portolio_name.text($('#portfolios_select option:selected').text());
        $(portolio_name).attr('data-portfolio-id', elementId);
      }
    );


    //Событие показа модального окна переименования портфеля
    $('#popup_portfolio_rename').off('show.bs.modal');
    $('#popup_portfolio_rename').on('show.bs.modal', function(e)
      {
        var button = $(e.relatedTarget);
        var elementName = '';
        var elementId = '';
        if($('#portfolios_select').length>0) {
          elementId = $('#portfolios_select').val().replace("portfolio_", "");
          elementName = $('#portfolios_select option:selected').text();
        }
        else {
          elementName = button.data('portfolio-name');
          elementId = button.data('portfolio-id');
          $(e.currentTarget).find('.remove-portfolio-button').attr('data-portfolio-id', elementId);

        }

        var portolio_name = $(e.currentTarget).find('#popup_rename_portfolio_name');
        portolio_name.val(elementName);
        $(portolio_name).attr('data-portfolio-id', elementId);
      }
    );

    //Событие показа модального окна редактирования портфеля
    $('#popup_portfolio_editdescr').off('show.bs.modal').on('show.bs.modal', function(e)
      {
        $('#popup_portfolio_descr').text($('.view_portf_descr .descr_text').text());
      }
    );


    //Событие показа модального окно создания портфеля
    $('#popup_add_portfolio').off('show.bs.modal');
    $('#popup_add_portfolio').on('show.bs.modal', function(e)
      {

        //Если находимся в калькуляторе - собираем отмеченные активы
        var selected_actives = $('.smart_table tr.checked');
        var container = $('.modal_actives_block .modal_actives_rows_container');
        $('#popup_add_portfolio_name').val('');
        container.html('');
        if(selected_actives.length>0) {//Если есть выбранные активы в таблице - показываем в окне блок добавляемых активов

          var cnt = 0;
          var allSumm = 0;
          selected_actives.each(function(indx, element)
            {
              var tr = $(element);

              //Обходим строку в скрытой таблице модального окна, в котором всплавает детальная страница актива, что бы не получить фантомный актив в портфель
              if (tr.closest('.modal-body_inner').length > 0) {
                return;
              }

              var name_block = tr.find('.elem_name a');
              var currencyId = tr.attr('data-currency');
              // var currencyRate = parseFloat(getCurrencyRate(currencyId));

              var active_name = name_block.attr('data-original-title')!== undefined? name_block.attr('data-original-title'): name_block.attr('title');
				  var active_name_portfolio = name_block.attr('portfolio-title')!== undefined? name_block.attr('portfolio-title') : name_block.attr('data-original-title');4

              var emitent_name = tr.find('.company_name a').data('original-title');
              var active_emitent_name = emitent_name!== undefined? emitent_name: tr.find('.company_name a').attr('title');
              var count_actives = tr.find('.numberof').val();
              var active_code = name_block.attr('href');
              var active_type = '';
				  var active_lotsize = tr.attr('data-lotsize')!== undefined ? tr.attr('data-lotsize') : 1;


              if(active_code.indexOf('actions/')>=0) {
                active_type = 'share';
              }
              else if(active_code.indexOf('actions_usa')>=0) {
                active_type = 'share_usa';
              }
              else if(active_code.indexOf('etf')>=0) {
                active_type = 'share_etf';
              }
              else if(active_code.indexOf('obligations')>=0) {
                active_type = 'bond';
              }

              if (tr.attr('data-currency') == 'rub') {
                currency_value = 1;
              }
              else if (tr.attr('data-currency') == 'usd') {
                currency_value = parseFloat($('#usd_currency').val());
              }
              else if (tr.attr('data-currency') == 'eur') {
                currency_value = parseFloat($('#eur_currency').val());
              }

              //console.log('active_type='+active_type+'currency='+currencyId+' currRate='+currency_value );
              //var active_type = (active_code.indexOf('actions')>=0 || active_code.indexOf('etf')>=0)?'share':'bond';
				  var active_buy_price = parseFloat(tr.find('.elem_buy_price').attr('data-price'));
				  var active_one_price = parseFloat(tr.find('.elem_cshare_price').attr('data-price'));

              //Для ETF Берем цену как цену одной акции, все равно в лоте 1 штука
              if(isNaN(active_buy_price)) {
                active_buy_price = parseFloat(tr.find('.elem_cshare_price').attr('data-price'));
              }
				  if(isNaN(active_one_price)) {
					 active_one_price = parseFloat(tr.find('.elem_buy_price').attr('data-price'));
				  }

              //Пересчитаем валютную цену в рубли
              if((currencyId!='rub' && currencyId!='sur') && (active_type=='bond' || active_type == 'share_etf' || active_type == 'share_usa')) {
                active_buy_price = active_buy_price*currency_value;
                active_one_price = active_one_price*currency_value;
              }


              active_code = active_code.replace('/lk/actions/', '');
              active_code = active_code.replace('/lk/actions_usa/', '');
              active_code = active_code.replace('/lk/obligations/', '');
              active_code = active_code.replace('/lk/etf/', '');
              active_code = active_code.replace('/lk/currency/', '');
              active_code = active_code.replace('/', '');
              $(container).append('<div id="act'+cnt+'" class="modal_active_row" data-active_url="'+name_block.attr('href')+'"'+
                'data-active_code="'+active_code+'"'+
                'data-active_lastprice="'+active_buy_price+'"'+
                'data-active_lotsize="'+active_lotsize+'"'+
                'data-active_price_one="'+(parseFloat(active_one_price))+'"'+
                'data-number="'+count_actives+'" '+
                'data-active_emitent_name="'+active_emitent_name+'" '+
                'data-active_emitent_url="'+tr.find('.company_name a').attr('href')+'" '+
                'data-active_secid="'+tr.attr('data-id')+'" '+
                'data-active_type="'+active_type+'" '+
                'data-active_emitent_currency="'+tr.attr('data-currency')+'" '+
                '>'+
                '<div class="modal_active_name">'+active_name_portfolio+'</div>'+
                '<div class="modal_active_count">'+count_actives+'</div>'+
                '<div class="modal_active_summ">'+(count_actives*active_buy_price).toLocaleString()+'</div>'+
                '</div>'
              );

              allSumm = allSumm+(count_actives*active_buy_price);
              cnt++;
            }
          );

          //Обрабатываем нераспределенную сумму
/*          var money_main = parseFloat($('#money_main').val());
          var money_in_work = parseFloat($('#money_in_work').val());
          var radar_free_money = money_main - money_in_work;

          if(selected_actives.length>0 && radar_free_money>0) {
            cnt++;
            $(container).append('<div id="act'+cnt+'" class="modal_active_row" data-active_url="/lk/currency/RUB/"'+
              'data-active_code="RUB"'+
              'data-active_lastprice="1"'+
              'data-active_lotsize="1"'+
              'data-active_price_one="1"'+
              'data-number="'+money_main+'" '+
              'data-active_emitent_name="Рубль" '+
              'data-active_emitent_url="/lk/currency/RUB/" '+
              'data-active_secid="RUB" '+
              'data-active_type="currency" '+
              'data-active_emitent_currency="rub" '+
              '>'+
              '<div class="modal_active_name">Рубль</div>'+
              '<div class="modal_active_count">'+radar_free_money+'</div>'+
              '<div class="modal_active_summ">'+radar_free_money.toLocaleString()+'</div>'+
              '</div>'
            );
          }*/

          if(cnt>0) {
            $('.modal_actives_block').show();
            $('.add_portfolio_from_radar').show();
          }

          $(container).append('<div class="modal_active_row active_row_footer">'+
            '<div class="modal_active_name"></div>'+
            '<div class="modal_active_count top-border">Итого: </div>'+
            '<div class="modal_active_summ top-border">'+(allSumm).toLocaleString()+'</div>'+
            '</div>'
          );

          //Показываем Выпадающий список с портфелями + новый (для добавления в портфель)
          //Получим список портфелей пользователя для показа в выпадающем списке.

          $.ajax(
            {
            type: "POST",
            url: "/ajax/portfolio/portfolio.php",
            dataType: "json",
            data: {ajax: "y", action: 'getPortfolioListForUser'}
              ,
            cashe: false,
            async: false,
            success: function(data) {
                var Pname;
                for (key in data) {
                  Pname = data[key].name;
						if(data[key].ver.length==0) continue;
                  $('#popup_add_to_portfolio_radar_list').append('<option value="'+key+'">'+Pname+'</option>');
                }

              }
            }
          );


        }


      }
    );


    //Событие показа модального окна автобаллансировки портфеля
    $('#popup_autoballance').on('show.bs.modal', function(e)
      {
        var button = $(e.relatedTarget);
        //$("#abl_invest_value").attr("min",$('.portfolio_elem_buy_price_final_summ').attr('data-value'))
        autobalance(true); //Заполняем из таблицы активов портфеля
      }
    );

    $('.abl_recount_button').on('click', function()
      {
        autobalance(false); //Не перезаполняем активами портфеля
      }
    );
    $('.abl_list_block input.abl_active_use').on('click', function()
      {
        autobalance(false);
      }
    );

    $('.setAutobalance').on('click', function()
      {
        setAutobalance();
      }
    );

    $('#popup_add_to_portfolio_radar_list').on('change', function()
      {
        if($('#popup_add_to_portfolio_radar_list').val()=='') {
          $('#popup_add_portfolio_name').removeAttr('disabled');
          $('#popup_add_portfolio .add-portfolio-button').text('Создать');
        }
        else {
          $('#popup_add_portfolio_name').attr('disabled', 'disabled');
          $('#popup_add_portfolio .add-portfolio-button').text('Добавить');
        }
        $('#popup_add_portfolio_name').val('');
      }
    );

    //Кнопка удаления портфеля во всплывалке
    $('.remove-portfolio-button').on('click', function()
      {
        var delete_from_lk = $(this).attr('data-portfolio-id');
        if(isNaN(delete_from_lk) || delete_from_lk=='undefined' || delete_from_lk=='') {
          delete_from_lk = false;
        }
        deletePortfolio(delete_from_lk);
      }
    );

    //Кнопка очистки портфеля во всплывалке
    $('.clear-portfolio-button').on('click', function()
      {
        var clear_from_lk = $(this).attr('data-portfolio-id');
        if(isNaN(clear_from_lk) || clear_from_lk=='undefined' || clear_from_lk=='') {
          clear_from_lk = false;
        }
        clearPortfolio(clear_from_lk);
      }
    );

    //Кнопка пересчета истории портфеля
    $('#recalculate_history').on('click', function()
      {
        recalculatePortfolio();
      }
    );
    //Кнопка сброса кеша портфеля
    $('#clearcache_portfolio').on('click', function()
      {
        clearcachePortfolio();
      }
    );

    //Кнопка копирования портфеля во всплывалке
    $('.modal-copy-portfolio-button').on('click', function()
      {
        copyPortfolio();
      }
    );

    //Кнопка переименования портфеля во всплывалке
    $('.modal-rename-portfolio-button').on('click', function()
      {

        renamePortfolio($(this).parents('.modal-body').find('#popup_rename_portfolio_name').attr('data-portfolio-id'));
      }
    );

    //Кнопка сохранения описания портфеля во всплывалке
    $('.modal-savedescr-portfolio-button').on('click', function()
      {
        editDescrPortfolio();
      }
    );


    //Кнопка добавления портфеля во всплывалке
    $('.add-portfolio-button').on('click', function()
      {
        var add_to_portfolio;

        if($('#popup_add_to_portfolio_radar_list').css('display')=='block') {
          add_to_portfolio = $('#popup_add_to_portfolio_radar_list').val();
          add_to_portfolio = add_to_portfolio.replace("portfolio_", "");
        }
        else {
          add_to_portfolio = '';
        }

        if ($('#popup_add_portfolio #popup_add_portfolio_name').val() == '' && add_to_portfolio == '') {
          if ($('#popup_add_portfolio .error_mess_empty').length == 0) {
            $(this).before('<p class="error_mess_empty red">Укажите название портфеля</span>');
          }
        }
        else {
          var actives_list =[];
          var container = $(this).parents('.modal-body').find('.modal_actives_rows_container>.modal_active_row:not(.active_row_footer)');

          $(container).each(function(indx, element)
            {
              if($(element).data('active_type')!='currency')
              actives_list.push(
                {'active_code': $(element).data('active_code'),
                'active_url': $(element).data('active_url'),
                'active_lastprice': $(element).data('active_lastprice'),
                'active_price_one': $(element).data('active_price_one'),
                'active_lotsize': $(element).data('active_lotsize'),
                'active_name': $(element).find('.modal_active_name').text(),
                'active_emitent_name': $(element).data('active_emitent_name'),
                'active_emitent_url': $(element).data('active_emitent_url'),
                'active_emitent_currency': $(element).data('active_emitent_currency'),
                'active_secid': $(element).data('active_secid'),
                'active_type': $(element).data('active_type'),
                'active_cnt': $(element).data('number'),
                }
              );
            }
          );
          $('.error_mess_empty').remove();
          addPortfolio(actives_list, add_to_portfolio);
        }
      }
    );


    initActivities();


    if($('.oblig_off').length>0) {
      $('.oblig_off .target_profit').text('-');
    }

    //Кнопка обновления коэффициентов портфеля
    $('.portfolio_recalculate_coeff').on('click', function()
      {
        var portfolioId = $(this).attr('data-portfolioId');
        updatePortfolioCoeff(portfolioId, true);
      }
    );


  }
); //$(document).ready

//Ребалансировка портфеля в popup'e
function autobalance(fillFromPortfolio = true) {
  var chart_array =[];
  var container_actives = $('.abl_list_block');
  var actives_count = $('.radar_table.pt_active tbody tr:not(.oblig_off, [data-elem=currency])').length;
  var popup_actives_count = $('.abl_list_block .abl_active input:checked').length;
  var abl_invest_summ;
  var reballancedSumm = 0;
  var maxActiveSumm = 0;
  var newInvestSumm = 0;
  var activesTable =[];
  //Если перезаполняем таблицу активов из открытого портфеля
  if(fillFromPortfolio) {
    container_actives.html('');
    abl_invest_summ = $('.portfolio_elem_buy_price_final_summ').attr('data-value');
    var limit_summ_per_active = (abl_invest_summ/actives_count).toFixed(1);
    for (var i = 0; i < actives_count; i++) {
      var tr = $('.radar_table.pt_active tbody tr:not(.oblig_off, [data-elem=currency])').eq(i);
      var countActives = (limit_summ_per_active/tr.data('price_buy')).toFixed(0); //Нестрогая балансировка (округляет кол-во до ближайшего целого в +)
      //var countActives = Math.trunc(limit_summ_per_active/tr.data('price_buy')); //Строгая балансировка (отбрасывает дробную часть кол-ва без округления)
      activesTable.push(
        {"ROW_ID": tr.data('id'),
        "NAME": tr.find('p.elem_name a').data('original-title'),
        "CHECKED": true,
        "COUNT_ACTIVES": countActives,
        "PRICE": tr.data('price_buy'),
        "LOTSIZE": tr.attr('data-lotsize'),
        "TYPE": tr.attr('data-elem'),
        "IBLOCK_ELEMENT_ID": tr.attr('data-iblock_elem_id'),
        "CURRENCY": tr.attr('data-currency'),
        "CURRENCY_RATE": tr.data('currency_rate')}
      );
      if(tr.data('price_buy')>maxActiveSumm)
        maxActiveSumm = tr.data('price_buy');
    }
    $(container_actives).attr('data-summ', abl_invest_summ);
    $('#abl_invest_value').val(abl_invest_summ);
  }
  else {//Если пересчитываем в попапе то таблицу активов из открытого портфеля больше не берем, используем ту, что существует в попапе
    abl_invest_summ = Number($(container_actives).attr('data-summ'));
    new_invest_summ = Number($('#abl_invest_value').val());
    //if(new_invest_summ > abl_invest_summ){
    abl_invest_summ = new_invest_summ;
    //}
    var limit_summ_per_active = (abl_invest_summ/popup_actives_count).toFixed(1);

    $('.abl_list_block .abl_active').each(function(indx, element)
      {
        var tr = $(element);

        var notUse = !tr.find('input:checked').length? true: false;
        if(!notUse) {
          var countActives = (limit_summ_per_active/tr.attr('data-PRICE')).toFixed(0); //Нестрогая балансировка (округляет кол-во до ближайшего целого в +)
          //var countActives = Math.trunc(limit_summ_per_active/tr.attr('data-PRICE')); //Строгая балансировка (отбрасывает дробную часть кол-ва без округления)
        }
        else {
          var countActives = 0;
        }
        var curActivePrice = tr.attr('data-PRICE');
        activesTable.push(
          {"ROW_ID": tr.attr('id'),
          "NAME": tr.attr('data-NAME'),
          "CHECKED": (tr.find('input:checked').length? true: false),
          "COUNT_ACTIVES": countActives,
          "PRICE": curActivePrice,
          "LOTSIZE": tr.attr('data-LOTSIZE'),
          "TYPE": tr.attr('data-TYPE'),
          "IBLOCK_ELEMENT_ID": tr.attr('data-IBLOCK_ELEMENT_ID'),
          "CURRENCY": tr.attr('data-CURRENCY'),
          "CURRENCY_RATE": tr.attr('data-CURRENCY_RATE')}
        );
        if(curActivePrice>maxActiveSumm && !notUse)
          maxActiveSumm = curActivePrice;

      }
    );

    $(container_actives).attr('data-summ', abl_invest_summ);
    $('#abl_invest_value').val(abl_invest_summ);
  }

  //Сначала мы куда то вводим 500, затем программа делит 500 на кол-во, например на 10, остаётся по 50 на актив. И дальше берётся столько лотов сколько будет до 50.
  var AllCountZero = true;
  var AllUnchecked = true;
  for (var i = 0; i < activesTable.length; i++) {
    if(activesTable[i]["COUNT_ACTIVES"]>0) {
      AllCountZero = false;
    }
    if(activesTable[i]["CHECKED"]) {
      AllUnchecked = false;
    }
    if(fillFromPortfolio) {
      if(i==0) {
        $(container_actives).append('<div class="abl_table_group_action"><div class="abl_tga_name">Выбрать/убрать все активы</div><div class="abl_tga_check"><input class="abl_active_use_all" checked="" type="checkbox"/></div></div>');
      }
      $(container_actives).append('<div id="'+activesTable[i]['ROW_ID']+'" class="abl_active modal_active_row " title="Цена: '+ activesTable[i]['PRICE'] +'"' +
        //'data-active_code="'+active_code+'"'+
        'data-PRICE="'+activesTable[i]['PRICE']+'"'+
        'data-LOTSIZE="'+activesTable[i]['PRICE']+'"'+
        'data-COUNT_ACTIVES="'+activesTable[i]['COUNT_ACTIVES']+'" '+
        'data-TYPE="'+activesTable[i]['TYPE']+'" '+
        'data-CURRENCY="'+activesTable[i]['CURRENCY']+'" '+
        'data-CURRENCY_RATE="'+activesTable[i]['CURRENCY_RATE']+'" '+
        'data-IBLOCK_ELEMENT_ID="'+activesTable[i]['IBLOCK_ELEMENT_ID']+'" '+
        'data-NAME="'+activesTable[i]['NAME']+'">'+
        '<div class="modal_active_name">'+activesTable[i]['NAME']+'</div>'+
        '<div class="modal_active_count">'+activesTable[i]['COUNT_ACTIVES']+'</div>'+
        '<div class="modal_active_summ">'+(activesTable[i]['COUNT_ACTIVES']*activesTable[i]['PRICE']).toLocaleString()+'</div>'+
        '<div class="modal_active_checkbox"><input class="abl_active_use" type="checkbox" '+(activesTable[i]['CHECKED']? 'checked=""': '')+'/></div>'+
        '</div>'
      );
    }
    else {
      var tr = $('.abl_list_block .abl_active#'+activesTable[i]['ROW_ID']);
      if(activesTable[i]['CHECKED']) {
        tr.removeClass('red');
      }
      else {
        tr.addClass('red');
      }

      if(activesTable[i]['CHECKED'] && activesTable[i]['COUNT_ACTIVES']==0) {
        tr.addClass('yellow');
      }
      else {
        tr.removeClass('yellow');
      }

      tr.attr('data-COUNT_ACTIVES', activesTable[i]['COUNT_ACTIVES']);
      tr.find('.modal_active_count').text(activesTable[i]['COUNT_ACTIVES']);
      tr.find('.modal_active_summ').text((activesTable[i]['COUNT_ACTIVES']*activesTable[i]['PRICE']).toLocaleString());
      if(activesTable[i]['CHECKED']) {
        tr.find('.modal_active_checkbox input').attr('checked', true);
      }
      else {
        tr.find('.modal_active_checkbox input').removeAttr('checked');
      }

    }
    //Сформируем массив кругового графика
    var chart_donut_arr_element =[];
    var curr_active_summ = activesTable[i]['COUNT_ACTIVES']*activesTable[i]['PRICE'];
    var chart_donut_arr_element_val = (curr_active_summ * 100) / abl_invest_summ;
    reballancedSumm += curr_active_summ;
    chart_donut_arr_element.push(activesTable[i]['NAME'], chart_donut_arr_element_val.toFixed(1), colors_big_arr[i]);
    chart_array.push(chart_donut_arr_element);
    $('.new_invest_summ span').text(reballancedSumm.toFixed(2));
  }

  if(abl_invest_summ==0 || AllCountZero || AllUnchecked) {
    var chartMessage = 'Недостаточно суммы для балансировки';
    if(AllUnchecked) {
      chartMessage = 'Не выбраны активы для балансировки';
    }
    chart_array =[];
    var chart_donut_arr_element =[];
    chart_donut_arr_element.push(chartMessage, 100, "#FFFF33");
    chart_array.push(chart_donut_arr_element);
    $('.setAutobalance').addClass('hidden');
  }
  else {
    $('.setAutobalance').removeClass('hidden');
  }


  roundChartUpdate(chart_array, '.abl_round_chart_outer', 'abl_round_chart');
  //$('.maxPart span').text(limit_summ_per_active);
  if($('.abl_list_block .abl_active.yellow').length) {
    $('.abl_info_list_block .yellow').removeClass('hidden');
  }
  else {
    $('.abl_info_list_block .yellow').addClass('hidden');
  }
  if($('.abl_list_block .abl_active.red').length) {
    $('.abl_info_list_block .red').removeClass('hidden');
  }
  else {
    $('.abl_info_list_block .red').addClass('hidden');
  }

  $('.abl_list_block .abl_active_use_all').off('change').on('change', function()
    {
      if($(this).prop("checked")) {
        $('.abl_active_use').prop('checked', true);
        autobalance(false);
      }
      else {
        $('.abl_active_use').prop('checked', false);
        autobalance(false);
      }

    }
  );

  $('.abl_list_block input.abl_active_use').off('click');
  $('.abl_list_block input.abl_active_use').on('click', function()
    {
      autobalance(false);
    }
  );
  /*    if(abl_invest_summ_in == 0){
       abl_invest_summ = $('.portfolio_elem_buy_price_final_summ').attr('data-value');
     } else {
      abl_invest_summ = abl_invest_summ_in;
      if(abl_invest_summ < $('.portfolio_elem_buy_price_final_summ').attr('data-value')) {
      //Если передана новая сумма через параметр и она меньше суммы портфеля получим самую большую цену и умножим на кол-во активов - это и будет новая минимальная сумма
       var min_price = 0;
         for (var i = 0; i < popup_actives_count; i++) {
       if(i==0 || min_price>$('.abl_list_block .abl_active').eq(i).attr('data-active_lastprice')){
       min_price = $('.abl_list_block .abl_active').eq(i).attr('data-active_lastprice');
       }
       }

       abl_invest_summ_tmp = min_price * popup_actives_count;
       if(abl_invest_summ_in<abl_invest_summ_tmp){
        abl_invest_summ = abl_invest_summ_tmp;
       }
       actives_count = popup_actives_count;
      }
     }*/
}

function setAutobalance() {
  showPopupProgress('popup_autoballance');
  var arActivesRows =[];
  $('.radar_table.pt_active tr[data\-elem=currency]').remove();//Удаляем строки с валютами
  $('.abl_list_block .abl_active').each(function(indx, element)
    {
      var tr = $(element);

      var notUse = (!tr.find('input:checked').length || Number(tr.find('.modal_active_count').text())==0)? true: false;
      if(!notUse) {

        var countActive = Number(tr.find('.modal_active_count').text());
        var summActive = tr.find('.modal_active_summ').text();
		  var summActiveRAW = summActive;
            summActive = summActive.replace(/\s+/ig, '');
            summActive = summActive.replace(/\,/ig, '.');
            summActive = Number(summActive);
        var deal_row = $('.radar_table.pt_active tr#'+tr.attr('id'));

        if(deal_row.length) {
          var cnt_input = deal_row.find('input.portfolio_elem_number').val(countActive);
          var income_summ = deal_row.attr('data-clearincome',summActive).find('p.portfolio_elem_buy_price_final').val(summActive);
          arActivesRows.push(deal_row.find('input.portfolio_elem_number').closest('tr'));
          rowUpdate($('.radar_table.pt_active tr#'+tr.attr('id')));
        }
      }
    }
  );
  //console.log('arActivesRows', arActivesRows);
  updatePortfolioActivesAutobalance(arActivesRows);
 // recountTable();
 // radarRecount();
  //tableFootUpdate();
  //Записываем суммы портфеля в инфоблок
  updateStatSumm($('.portfolio_elem_buy_price_curr_summ').attr('data-current_summ'), $('.portfolio_elem_buy_price_final_summ').attr('data-value'));
  location.reload();//Перезагружаем, т.к. вложенный капитал считается отдельно от таблицы по аналогии с жетым графиком

  hidePopupProgress();
  $('#popup_autoballance').modal('hide');

}


function getCurrencyRate(currencyId) {
  var currencyRate = 1;

  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'getCurrencyRate', data: {'currencyId': currencyId, 'date': ''}
      }
      ,
    cashe: false,
    async: false,
    success: function(data) {
        getResponse(data);
      }
    }
  );

  function getResponse(data) {
    currencyRate = parseFloat(data);
  }
  return currencyRate;
}

//Очищает поля формы добавления актива в портфель и скрытые поля этой формы
function clearAddActiveFormFields() {
  $('#popup_add_row_active_id').val('');
  $('#popup_add_row_active_price_one').val('');
  $('#popup_add_row_price_one_usd').val('');
  $('#popup_add_row_active_inlot_cnt').val('');
  $('#popup_add_row_price_one').val('');
  $('#popup_add_row .popup_add_row_price_lot').text('');
  $('#popup_add_row .popup_add_row_summ').text('');
  $('#popup_add_row_active_code').val('');
  $('#popup_add_row_active_url').val('');
  $('#popup_add_row_active_emitent_currency').val('');
  $('#popup_add_row_active_emitent_name').val('');
  $('#popup_add_row_active_emitent_url').val('');
  $('#popup_add_row_active_lastprice, #popup_add_row_price').val('');
  $('#popup_add_row_active_secid').val('');
  $('#popup_add_row #autocomplete_obl_act_all').val('');
  $('#popup_add_row #popup_add_row_number').val(1);
  $('#popup_add_row #popup_add_row_date').val('');
  $('#popup_add_row_active_lastprice_valute').val('');
  $('#popup_add_row_currency_rate').val('');
  $('#popup_add_row_active_price_orig').val('');
  $('#popup_add_row_active_currency_id').val('');
  $('#popup_add_row_price_one_usd').val('');
}


function setPortfolioStatesToSession(portfolioId, session_param, session_value) {
  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "html",
    data: {ajax: "y", action: 'setPortfolioStatesToSession', 'portfolioId': portfolioId, 'session_param': session_param, 'session_value': session_value}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        //getResponse(data);
        if(data=='ok') {
          // console.log(data);
        }
      }
    }
  );
}



//Заполняет данные попапа удаления актива по переданному id актива
function fillActiveDeletePopup(activeId) {
  var row = $('.portfolio_tabs_table.pt_active').find('tr[data-iblock_elem_id='+activeId+']');
  //var row = $(this).closest('tr');
  var id = activeId;
  var lotsize = 1;
  var price_buy = row.attr('data-price_buy');
  var price_one_buy = Number(row.find('.share_price_one').text());
  var name_block = row.find('.elem_name a');
  var emitent_name_block = row.find('.company_name a');
  var active_code = name_block.attr('href');

  active_code = active_code.replace('/lk/actions/', '');
  active_code = active_code.replace('/lk/actions_usa/', '');
  active_code = active_code.replace('/lk/obligations/', '');
  active_code = active_code.replace('/lk/etf/', '');
  active_code = active_code.replace('/lk/currency/', '');
  active_code = active_code.replace('/', '');


  if(row.attr('data-elem')=='share_etf' || row.attr('data-elem')=='share_usa') {
    var price_one_buy = price_buy;
  }
  if(row.attr('data-elem')=='share_usa' || row.attr('data-currency')!='rub' ||  row.attr('data-base-currency')!='rub') {
    $('#popup_change_row').find('.act_usd_price').show();
  } else {
    $('#popup_change_row').find('.act_usd_price').hide();
  }
  var number = parseFloat(row.find('.portfolio_elem_number').val());
  var name = row.find('.elem_name').text();
  var priceName = 'Цена выбытия за 1 акцию';
  $('#popup_change_row').find('.obl_hidden').show();
  if(row.attr('data-elem')=='bond') {
  	 var real_date_sell = row.attr('data-popup_remove_date_sell');
	 // $('#popup_change_row').find('#popup_sale_row_date').val(real_date_sell);
	 $('#popup_change_row').find('#popup_sale_row_date').datepicker('update', real_date_sell);
    priceName = 'Цена продажи с НКД, в руб';
    $('#popup_change_row').find('.obl_hidden').hide();
    price_one_buy = price_buy;
  }
  else {
    lotsize = row.attr('data-lotsize');
  }

  $('#popup_change_row').find('#popup_change_row_no_cache_operation').prop('checked', false);
  $('#popup_change_row').find('#activePriceTypeName').text(priceName);
  $('#popup_change_row').find('#popup_remove_row_active_inlot_cnt').val(lotsize);
  $('#popup_change_row').find('#popup_change_row_number').val(number);
  $('#popup_change_row').find('#popup_change_row_number').attr('max', number);
  $('#popup_change_row').find('.popup_change_row_number_current').text(number);
  //$('#popup_change_row').find('#popup_change_row_name').text(name);
  $('#popup_change_row').find('#idActive').val(id);
  $('#popup_change_row').find('#popup_remove_row_type').val(row.attr('data-elem'));
  $('#popup_change_row').find('#popup_remove_row_active_code').val(active_code);

  $('#popup_change_row').find('#popup_remove_row_name').val(name_block.attr('data-original-title'));
  $('#popup_change_row').find('#popup_remove_row_url').val(name_block.attr('href'));

  $('#popup_change_row').find('#popup_remove_row_emitent_name').val(emitent_name_block.attr('data-original-title'));
  $('#popup_change_row').find('#popup_remove_row_emitent_url').val(emitent_name_block.attr('href'));


  $('#popup_change_row').find('#popup_remove_row_active_secid').val(row.attr('data-secid'));
  $('#popup_change_row').find('#popup_remove_row_currency_code').val(!!row.attr('data-base-currency')?row.attr('data-base-currency'):row.attr('data-currency'));
  $('#popup_change_row').find('#popup_remove_row_date').val();
  $('#popup_change_row').find('#popup_change_row_price').val(price_one_buy);
  $('#popup_change_row').find('#portfolioNumber').val($('#portfolios_select').val().replace("portfolio_", ""));
  $('#popup_sale_row_date').change(); //update sell fifo price
  $('#popup_change_row_price').change();
  $('#popup_change_row_number').change();

  $('.popup_del_active').off('click');
  $('.popup_del_active').click(function()
    {
      var popup = $('#popup_change_row');

      var ths = $(this);

		setTimeout(function() {
        var new_number = number - parseFloat($('#popup_change_row_number').val());
        if (parseFloat($('#popup_change_row_number').val()) > number) {
          if (ths.siblings('.error_mess').length == 0) {
            ths.before('<p class="error_mess red">Вы указали слишком большое количество лотов</span>');
          }
          $('#popup_change_row_number').css('border-color', 'red');
        }
        else {
          if ($('#popup_change_row_number').val() == '' || $('#popup_change_row_price').val() == '') {
            if (ths.siblings('.error_mess_empty').length == 0) {
              ths.before('<p class="error_mess_empty red">Одно или несколько полей не заполнено</span>');
            }
          }
          else {
            $('.error_mess').remove();
            $('.error_mess_empty').remove();
				popup.find('.popup_progress').addClass('active');
            deletePortfolioActive($('#portfolioNumber').val(), id, $('#popup_change_row_number').val(), $('#popup_sale_row_date').val(), 'popup');

            portfolioChange();
            $('#popup_change_row_number').removeAttr('style');

            $('#popup_change_row').modal('hide');
            rowUpdate(row);
            tableFootUpdate();
            checkSubzeroCache();
            //radarRecount();
            updateActiveTabOnActiveChange();
          }
        }
		}, 200);
    }
  );

  return row; //Возвращаем строку таблицы активов для последующего ее обновления
}


//Заполняет данные попапа изъятия валюты по переданному id валюты
function fillActiveExcludePopup(activeId) {
  var row = $('.portfolio_tabs_table.pt_active').find('tr[data-iblock_elem_id='+activeId+']');
  //var row = $(this).closest('tr');
  var id = activeId;
  var lotsize = 1;
  var price_buy = row.attr('data-price_buy');
  var price_one_buy = Number(row.find('.share_price_one').text());
  var emitent_name_block = row.find('.company_name a');
  var name_block = row.find('.elem_name a');
  var active_code = name_block.attr('href');

  active_code = active_code.replace('/lk/currency/', '');
  active_code = active_code.replace('/', '');

  var price_one_buy = price_buy;

  var number = parseFloat(row.find('.portfolio_elem_number').val());
  var name = row.find('.elem_name').text();

  lotsize = row.attr('data-lotsize');
  $('#filled_from_active').val(activeId);
  $('#popup_exclude_cache_row').find('.error_mess').remove();
  $('#popup_exclude_cache_row').find('#popup_exclude_row_active_inlot_cnt').val(lotsize);
  $('#popup_exclude_cache_row').find('#popup_exclude_row_number').val(number);
  $('#popup_exclude_cache_row').find('#popup_exclude_row_number').attr('max', number);
  $('#popup_exclude_cache_row').find('.popup_exclude_row_number_current').text(number);
  //$('#popup_exclude_cache_row').find('#popup_change_row_name').text(name);
  $('#popup_exclude_cache_row').find('#popup_exclude_idActive').val(id).trigger('change.select2');
  $('#popup_exclude_cache_row').find('#popup_exclude_row_type').val(row.attr('data-elem'));
  $('#popup_exclude_cache_row').find('#popup_exclude_row_active_code').val(active_code);

  $('#popup_exclude_cache_row').find('#popup_exclude_row_name').val(name_block.attr('data-original-title'));
  $('#popup_exclude_cache_row').find('#popup_exclude_row_url').val(name_block.attr('href'));

  $('#popup_exclude_cache_row').find('#popup_exclude_row_emitent_name').val(emitent_name_block.attr('data-original-title'));
  $('#popup_exclude_cache_row').find('#popup_exclude_row_emitent_url').val(emitent_name_block.attr('href'));

  $('#popup_exclude_cache_row').find('#popup_exclude_row_active_secid').val(row.attr('data-secid'));
  $('#popup_exclude_cache_row').find('#popup_exclude_row_currency_code').val(row.attr('data-currency'));
  $('#popup_exclude_cache_row').find('#popup_exclude_row_currency_rate').val(price_one_buy);
  $('#popup_exclude_cache_row').find('#popup_exclude_row_active_inlot_cnt').val(row.attr('data-lotsize'));
  $('#popup_exclude_cache_row').find('#popup_exclude_row_date').val();
  $('#popup_exclude_cache_row').find('#popup_exclude_row_price').val(price_one_buy);
  $('#popup_exclude_cache_row').find('#excludePortfolioNumber').val($('#portfolios_select').val().replace("portfolio_", ""));
  $('#popup_exclude_row_date').change(); //update sell fifo price
  $('#popup_exclude_row_price').change();
  $('#popup_exclude_row_number').change();

  $('.popup_exclude_active').off('click');
  $('.popup_exclude_active').click(function()
    {
       var ths = $(this);

      setTimeout(function() {
        var new_number = number - parseFloat($('#popup_exclude_row_number').val());
        if (parseFloat($('#popup_exclude_row_number').val()) > number) {
          if (ths.siblings('.error_mess').length == 0) {
            ths.before('<p class="error_mess red">Вы указали слишком большое количество валюты</span>');
          }
          $('#popup_exclude_row_number').css('border-color', 'red');
        }
        else {
          if ($('#popup_exclude_row_number').val() == '' || $('#popup_exclude_row_price').val() == '') {
            if (ths.siblings('.error_mess_empty').length == 0) {
              ths.before('<p class="error_mess_empty red">Одно или несколько полей не заполнено</span>');
            }
          }
          else {
     var popup = $('#popup_exclude_cache_row');
      popup.find('.popup_progress').addClass('active');

            $('.error_mess').remove();
            $('.error_mess_empty').remove();

            excludePortfolioActiveFifo($('#excludePortfolioNumber').val(), id, $('#popup_exclude_row_number').val(), $('#popup_exclude_row_date').val(), 'popup', true);

            portfolioChange();
            $('#popup_exclude_row_number').removeAttr('style');

            $('#popup_exclude_cache_row').modal('hide');
            rowUpdate(row);
            tableFootUpdate();
            checkSubzeroCache();
            updateActiveTabOnActiveChange();
            //radarRecount();
          }
        }
      }, 200);
    }
  );

  return row; //Возвращаем строку таблицы активов для последующего ее обновления
}

function initActivities() {
  $('.portfolio-extlist-button').off('click');
  $('.portfolio-extlist-button').on('click', function(e)
    {
      e.preventDefault;
      var targetRow = '';
      if($(this).hasClass('active')) {//сворачиваем
        targetRow = $(this).data('target');

        var tmpName = $(this).text();
        $(this).text($(this).data('text'));
        $(this).data('text', tmpName);

        $('tr.'+targetRow).hide();
        $(this).removeClass('active');

      }
      else {//разворачиваем
        targetRow = $(this).data('target');

        var tmpName = $(this).text();
        $(this).text($(this).data('text'));
        $(this).data('text', tmpName);

        $('tr.'+targetRow).show();
        $(this).addClass('active');
      }
      return false;
    }
  );

  //Кнопка редактирования в строке таблицы истории, открывающая попап редактирования истории сделки
  $('body').off('click', '.history_table .edit_history_btn');
  $('body').on('click', '.history_table .edit_history_btn', function()
    {
      var row_id = $(this).attr('data-hist-id');
      $('#popup_edit_row_history_portfolio').find('#hist_edit_row_id').val(row_id);
      $('#popup_edit_row_history_portfolio').modal('show');
    }
  );

  //Кнопка удаления актива по фифо через попап продажи
  $('.popup_change_btn.remove_fifo_active').off('click');
  $('.popup_change_btn.remove_fifo_active').on('click', function()
    {
		var id = $(this).attr('data-active-id');
      var tr = $(this).parents('tr');
      //if(tr.attr('data-currency_id')=='rub' && tr.attr('data-elem')=='currency') {
      if(tr.attr('data-elem')=='currency') {
        var row = fillActiveExcludePopup(id); //Заполняем данные попапа для изъятия рубля
        var number = parseFloat(row.find('.portfolio_elem_number').val());
        $('#popup_exclude_cache_selector').val(id).trigger('change.select2'); //Отмечаем актив по которому уже заполнена форма
        $('#popup_exclude_cache_row').modal('show');
      }
      else {
        var row = fillActiveDeletePopup(id); //Заполняем данные попапа для удаления
        var number = parseFloat(row.find('.portfolio_elem_number').val());
        $('#popup_change_row').find('#filled_from_active').val(id); //Отмечаем актив по которому уже заполнена форма
        $('#popup_change_row').modal('show');
      }
    }
  );

  return false;
}

function initEditorActivities() {
  $('.portfolio_element_data').datepicker(
    {
    language: 'ru'
    }
  );

  $('.p_table_input').off('change keyup input ');
  $('.p_table_input').on('change keyup input ', function(eventObject)
    {

      var linked_rowId = $(this).parents('tr').data('linked_row');
      var linked_tr = $('.portfolio_valuation_table .pt_active tbody tr#'+linked_rowId);
      var cur_tr = $(this).parents('tr');
      var loaded_cnt = cur_tr.find('.portfolio_elem_number').data('loaded_cnt');
      var loaded_price = cur_tr.find('.portfolio_elem_buy_price').data('loaded_price');

      if(this.value == '' || this.value <= 0 || this.value == undefined || Number.isNaN(this.value)) {
        if($(this).hasClass('portfolio_elem_number')) {//Обновляем количество
          linked_tr.find('.portfolio_elem_number').val(loaded_cnt);
          cur_tr.find('.portfolio_elem_number').val(loaded_cnt);
        }
        if($(this).hasClass('portfolio_elem_buy_price') && $(this).hasClass('elem_buy_price_one')==false) {//Если обновляем цену лота
          linked_tr.find('.portfolio_elem_buy_price').val(loaded_price);
          cur_tr.find('.portfolio_elem_buy_price').val(loaded_price);
        }
      }

      if ($(this).hasClass('portfolio_elem_number')) {//Обновляем количество
        this.value = this.value.replace(/[^0-9]/g, '');
        linked_tr.find('.portfolio_elem_number').val(this.value);
        cur_tr.find('.portfolio_elem_buy_price_final').text((this.value*cur_tr.find('.portfolio_elem_buy_price').val()).toFixed(1));
        calcMidLotPrice(cur_tr, cur_tr.attr('data-secid'), linked_tr); //Пересчитываем средневзвешенные цены для главной строки и общее количество
        cur_tr.attr('data-now-summ', linked_tr.attr('data-current_exchange_price')*this.value);
        cur_tr.attr('data-ammort-summ', cur_tr.attr('data-ammort-one')*this.value);
      }
      else
        if($(this).hasClass('portfolio_elem_buy_price') && $(this).hasClass('elem_buy_price_one')==false) {//Обновляем цену лота
        this.value = this.value.replace(/[^0-9\-\.]/g, '');
        linked_tr.find('.portfolio_elem_buy_price').val(this.value);
        cur_tr.find('.portfolio_elem_buy_price_final').text((this.value*cur_tr.find('.portfolio_elem_number').val()).toFixed(1));
        if(cur_tr.attr('data-elem')=='share') {
          var price_one = rounLong(this.value/cur_tr.attr('data-inlot_cnt'), 4, 5);
          cur_tr.find('.elem_buy_price_one').text(price_one);
          cur_tr.find('.elem_buy_price_one').val(price_one);
          cur_tr.find('.elem_buy_price_one').attr('data-price_one', price_one);
          linked_tr.find('.share_price_one').text(price_one);
          calcMidLotPrice(cur_tr, cur_tr.attr('data-secid'), linked_tr); //Пересчитываем средневзвешенные цены для главной строки
        }

      }
      else if($(this).hasClass('elem_buy_price_one')) {//Если обновляем цену одной акции

        if ($(this).val().match(/[^0-9\.]/g)) {
          $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        }
        if($(this).val()=='' || isNaN($(this).val())) {
          $(this).val(0);
        }
        else {
          var price = (this.value*cur_tr.attr('data-inlot_cnt'));
          var price_one = $(this).val();
          $(this).attr('data-price_one', $(this).val());
          linked_tr.find('.portfolio_elem_buy_price').val(price);
          cur_tr.find('.elem_buy_price').val(price);
          cur_tr.find('.portfolio_elem_buy_price_final').text((price*cur_tr.find('.portfolio_elem_number').val()).toFixed(1));
          cur_tr.find('.elem_buy_price_one').text(price_one);
          cur_tr.find('.elem_buy_price_one').val(price_one);
          cur_tr.find('.elem_buy_price_one').attr('data-price_one', price_one);
          linked_tr.find('.share_price_one').text(price_one);
          calcMidLotPrice(cur_tr, cur_tr.attr('data-secid'), linked_tr); //Пересчитываем средневзвешенные цены для главной строки
        }


      }
      else if($(this).hasClass('portfolio_element_data')) {//Обновляем дату
        var sel_date = this.value;
        if(sel_date.length==10) {//Пока не ввели полную дату - не обновляем даты в связанной строке
          sel_date = sel_date.match(/^(\d{2})\.(\d{2})\.(\d{4})$/);
          sel_date = sel_date[3]+'-'+(sel_date[2])+'-'+sel_date[1];
          linked_tr.attr('data-real_date_buy', sel_date);
          linked_tr.attr('data-date_buy', sel_date);
          $('.portfolio_element_data').datepicker('update');
          updateAmortizationDealByDate(cur_tr.attr('data-secid'), sel_date, cur_tr.attr('data-deal_id'));
        }

      }
      //Пересчитываем сумму инвестиций по сделкам для главной строки
      var dealsumm = 0;
      $("tr[data-linked_row="+linked_rowId+"]").each(function(indx, element)
        {
          dealsumm += parseFloat($(element).find('td .portfolio_elem_buy_price_final').text());
        }
      );
      if(dealsumm>0) {
        linked_tr.attr('data-clearincome', dealsumm);
        linked_tr.find('td .portfolio_elem_buy_price_final').text(dealsumm).attr('data-value', dealsumm);
      }

      //console.log(linked_tr);
      var linked_tr = $('.portfolio_valuation_table .pt_active tbody tr#'+linked_rowId);
      linked_tr = $('.portfolio_valuation_table .pt_active tbody').find('tr#'+linked_rowId);
      rowUpdate(linked_tr);
      tableFootUpdate();


    }
  );


  //Выход из инпута с ценой лота
  $('.portfolio_elem_buy_price').off('blur');
  $('.portfolio_elem_buy_price').on('blur', function()
    {
      if(checkNoErrorsActiveRow($(this).closest('tr'))) {
        var linked_tr = $('.portfolio_valuation_table .pt_active tbody tr#'+$(this).closest('tr').data('linked_row'));
        linked_tr.attr('data-price_buy', parseFloat($(this).val()));
        updatePortfolioActive($(this).closest('tr'));
        radarRecount();
        tableFootUpdate();
      }
    }
  );

  //Выход из инпута с ценой за акцию
  $('.portfolio_elem_buy_price_one').off('blur');
  $('.portfolio_elem_buy_price_one').on('blur', function()
    {
      if(checkNoErrorsActiveRow($(this).closest('tr'))) {
        var linked_tr = $('.portfolio_valuation_table .pt_active tbody tr#'+$(this).closest('tr').data('linked_row'));
        linked_tr.attr('data-price_buy', parseFloat($(this).closest('tr').find('.portfolio_elem_buy_price').val()));
        updatePortfolioActive($(this).closest('tr'));
        radarRecount();
        tableFootUpdate();
      }
    }
  );

  //Выход из инпута с количеством лотов
  $('.portfolio_elem_number').off('blur');
  $('.portfolio_elem_number').on('blur', function()
    {
      if(checkNoErrorsActiveRow($(this).closest('tr'))) {
        updatePortfolioActive($(this).closest('tr'));
        radarRecount();
        tableFootUpdate();
      }
    }
  );

  //Изменение даты
  $('.portfolio_element_data').off('changeDate');
  $('.portfolio_element_data').on('changeDate', function()
    {
      var linked_rowId = $(this).closest('tr').attr('linked_row');
      var current_row = $(this).closest('tr');
      var linked_tr = $('.portfolio_valuation_table .pt_active tbody tr#'+$(this).closest('tr').data('linked_row'));
      linked_tr = $('.portfolio_valuation_table .pt_active tbody').find('tr#'+linked_rowId);
      var sel_date = $(this).val();
      sel_date = sel_date.match(/^(\d{2})\.(\d{2})\.(\d{4})$/);
      sel_date = sel_date[3]+'-'+(sel_date[2])+'-'+sel_date[1];
      if(current_row.attr('data-elem')=='bond' && !isNaN(current_row.attr('data-add-date-amort-summ'))) {
        updateAmortizationDealByDate(current_row.attr('data-secid'), sel_date, current_row.attr('data-deal_id'));
        // console.log(sel_date);
      }

      linked_tr.attr('data-date_buy', sel_date);
      linked_tr.attr('data-real_date_buy', sel_date);
      if(checkNoErrorsActiveRow($(this).closest('tr'))) {
        updatePortfolioActive($(this).closest('tr'));
        rowUpdate(linked_tr, linked_rowId);
        radarRecount();
        tableFootUpdate();
      }
    }
  );
}

//Расчет средневзвешенной цены лота при наличии нескольких сделок
function calcMidLotPrice(curr_tr, secid, linked_tr) {
  //Экранируем @ в тикере буржуйских акций
  secid = secid.replace("@", "\\@");

  //Если есть более одной сделки - пересчитываем средневзвешенную цену покупки лота и актива
  if($('.portfolio_tabs_table.pt_deals [data-secid="'+secid+'"]').length>1) {
    var midLotPrice = 0;
    var LotCntAll = 0;
    var LotCnt = 0;
    var InLotCnt = Number(linked_tr.attr('data-lotsize'));
    var midActivePrice = 0;

    $('.portfolio_tabs_table.pt_deals [data-secid="'+secid+'"]').each(function(indx, element)
      {
        LotCnt = parseFloat($(element).find('.portfolio_elem_number').val());
        midLotPrice += parseFloat($(element).find('.portfolio_elem_buy_price').val())*LotCnt;
        LotCntAll += LotCnt;
      }
    );
    var RoundCnt = 0;

    midLotPrice = midLotPrice/LotCntAll;
    midActivePrice = midLotPrice/InLotCnt;
    // console.log("midLotPrice="+midLotPrice+" midActivePrice="+midActivePrice+" InLotCnt="+InLotCnt);

    //if(Math.trunc(midLotPrice)>0){RoundCnt=2;} else {RoundCnt=5;}
    //midLotPrice = midLotPrice.toFixed(RoundCnt);
    midLotPrice = rounLong(midLotPrice, 2, 5);


    //if(Math.trunc(midActivePrice)>0){RoundCnt=2;} else {RoundCnt=5;}
    //midActivePrice = midActivePrice.toFixed(RoundCnt);
    midActivePrice = rounLong(midActivePrice, 2, 5);

    linked_tr.find('.portfolio_elem_buy_price').val(midLotPrice);
    linked_tr.find('.portfolio_elem_number').val(LotCntAll);
    linked_tr.find('.share_price_one').attr('data-price-one', midActivePrice);
    linked_tr.find('.share_price_one').text(midActivePrice);

  }

}

//Пересчет цен и сумм для окна редактирования сделки при изменении ценовых и количественных данных в инпутах попапа редактирования

function calcLotPriceEditPopup() {
  var popup = $('#popup_edit_row_history_portfolio');
  var cnt = $('#hist_edit_cnt_row_number').val();
  var activeType = $('#popup_hist_edit_active_type').val();
  var inLotCnt = $('#popup_hist_edit_active_inlot_cnt').val();
  var currencyId = $('#popup_hist_edit_currency_id').val();
  var cntCurrency = 0;
  var priceOne = $('#hist_edit_row_price').val();
  if(activeType=='currency' && currencyId!='rub' ){
		var priceCurrency = 1;
  } else {
  		var priceCurrency = $('#popup_hist_edit_row_price_one_usd').val();
  }

  //var currencyRate = $('#popup_hist_edit_currency_rate').val();
  var currencyRate = $('#popup_hist_edit_active_price_orig').val();
  var currencyLotprice = 0;
  var lotprice = 0;
  //Пересчет цены за 1 лот
  if(!isNaN(cnt)) {
    var thisPopup = $('#popup_edit_row_history_portfolio');
    var lotnumber = $('#hist_edit_cnt_row_number').val();
    popup.find('.popup_hist_edit_row_price_lot').text(priceOne*inLotCnt);
    if(Number(currencyRate)>0 && currencyId!='rub') {
      currencyLotprice = priceCurrency*inLotCnt;
      lotprice = currencyLotprice*currencyRate;
			console.log('currencyLotprice: priceCurrency='+priceCurrency+' * inLotCnt='+inLotCnt+' = '+currencyLotprice);
      currencyLotprice = currencyLotprice.toFixed(4);

      $('#popup_hist_edit_active_price_one').val(currencyLotprice);
      //$('#popup_hist_edit_active_lastprice').val(currencyLotprice);
      popup.find('.popup_hist_edit_row_price_lot').text(popup.find('.popup_hist_edit_row_price_lot').text() + ' ('+(currencyLotprice)+' '+currencyId+')');
		var hist_edit_row_summ = Number(priceOne*inLotCnt*cnt).toFixed(4);
      $('#hist_edit_row_summ').text(hist_edit_row_summ);
      $('#hist_edit_row_summ').text($('#hist_edit_row_summ').text() + ' ('+(currencyLotprice*cnt)+' '+currencyId+')');
		$('#popup_hist_edit_active_lastprice_valute').val(currencyLotprice);
    }
    else {
    	var hist_edit_row_summ = Number(priceOne*inLotCnt*cnt).toFixed(4);
      $('#hist_edit_row_summ').text(hist_edit_row_summ);

    }

    //Пересчет итого
    var thisPopup = $('#popup_edit_row_history_portfolio');
    var lotprice = $('#popup_hist_edit_active_lastprice').val();
    var lotprice_valute = $('#popup_hist_edit_active_lastprice_valute').val();
    var lotnumber = $('#hist_edit_cnt_row_number').val();
    var currencyId = $('#popup_hist_edit_currency_id').val();
	 var hist_edit_row_summ = Number(priceOne*inLotCnt*cnt).toFixed(4);
    $('#hist_edit_row_summ').text(hist_edit_row_summ);
    if(Number(currencyRate)>0 && currencyId!='rub') {
      $('#hist_edit_row_summ').text($('#hist_edit_row_summ').text() + ' ('+(Number(currencyLotprice*cnt).toFixed(4))+' '+currencyId+')');
    }

  }

}

$('#popup_change_row_price_currency').on('change input click', function()
  {
    var inLotCnt = $('#popup_remove_row_active_inlot_cnt').val();
    var currencyId = $('#popup_remove_row_currency_code').val();
    var cntCurrency = 0;
    var priceOneCurrency = $(this).val();
    var currencyRate = $('#popup_remove_row_currency_rate').val();
    var price = (priceOneCurrency*currencyRate).toFixed(2);
    $('#popup_change_row_price').val(price);
    calcLotPriceDelActivePopup(true);
  }
);

/**
 * Пересчет цен и сумм для окна редактирования сделки при изменении ценовых и количественных данных в инпутах попапа редактирования
 *
 * @param  bool   calcCurrencyOnly=false true - при вызове из событий инпута цены в валюте, что бы не перезаписывало введенное с клавиатуры значение
 *
 */
function calcLotPriceDelActivePopup(calcCurrencyOnly=false) {
  var popup = $('#popup_change_row');
  var cnt = $('#popup_change_row_number').val();
  var inLotCnt = $('#popup_remove_row_active_inlot_cnt').val();
  var currencyId = $('#popup_remove_row_currency_code').val();
  var cntCurrency = 0;
  var priceOne = $('#popup_change_row_price').val();
  var currencyRate = $('#popup_remove_row_currency_rate').val();
  var priceCurrency = (priceOne/currencyRate).toFixed(4);
  var currencyLotprice = 0;
  var lotprice = 0;
  //Пересчет цены за 1 лот
  if($('#popup_change_row_price_currency').val()==0 && calcCurrencyOnly==false) {
    $('#popup_change_row_price_currency').val(priceCurrency);
  }
  $('#popup_change_row_price').val()
  if(!isNaN(cnt)) {
    popup.find('.popup_remove_row_price_lot').text(priceOne*inLotCnt);
    if(Number(currencyRate)>0 && currencyId!='rub') {
      currencyLotprice = priceCurrency*inLotCnt;
      lotprice = currencyLotprice*currencyRate;
      currencyLotprice = currencyLotprice.toFixed(4);
      $('.popup_change_row_price').val(currencyLotprice);
      //$('#popup_hist_edit_active_lastprice').val(currencyLotprice);
      popup.find('.popup_remove_row_price_lot').text(popup.find('.popup_remove_row_price_lot').text() + ' ('+(currencyLotprice)+' '+currencyId+')');
      $('.popup_remove_row_summ').text(priceOne*inLotCnt*cnt);
      $('.popup_remove_row_summ').text($('.popup_remove_row_summ').text() + ' ('+(currencyLotprice*cnt)+' '+currencyId+')');
		if(calcCurrencyOnly==false){
      	$('#popup_change_row_price_currency').val(priceCurrency);
		}
    }
    else {
      $('.popup_remove_row_summ').text(priceOne*inLotCnt*cnt);

    }

    //Пересчет итого
    $('.popup_remove_row_summ').text(priceOne*inLotCnt*cnt);
    if(Number(currencyLotprice)>0) {
      $('.popup_remove_row_summ').text($('.popup_remove_row_summ').text() + ' ('+(currencyLotprice*cnt)+' '+currencyId+')');
    }

  }

}

//Получение значения амортизации на дату добавления облигации
function getAmortizationOnDate(secid, date) {
  var queryResult;
  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "text",
    data: {ajax: "y", action: 'getBondAmortValueByDate', secid: secid, date: date}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        getResponse(data);
      }
    }
  );
  function getResponse(value) {
    queryResult = parseFloat(value);
  }
  return queryResult
}

//Обновление амортизации облигаций при изменении даты добавления (даты покупки)
function updateAmortizationDealByDate(secid, date, deal_id) {
  var current_row = $('.portfolio_tabs_table.pt_deals .deal_row_'+deal_id);
  var linked_rowId = current_row.data('linked_row');
  var linked_tr = $('.portfolio_valuation_table .pt_active tbody').find('tr#'+linked_rowId);
  var allCnt = linked_tr.find('.portfolio_elem_number').val();
  var deal_Cnt = current_row.find('.portfolio_elem_number').val();

  var amort_summ = 0;
  amort_summ = parseFloat(getAmortizationOnDate(secid, date));
  var curr_summ = 0;
  curr_summ = parseFloat(current_row.attr('data-ammort-summ'));
  var facevalue_on_add_date = 0;
  facevalue_on_add_date = parseFloat(current_row.attr('data-initfacevalue'))-amort_summ;
  current_row.attr('data-add-date-facevalue', facevalue_on_add_date);
  current_row.attr('data-ammort-summ', amort_summ*deal_Cnt);
  current_row.attr('data-ammort-one', amort_summ);
  if(amort_summ>0 && amort_summ!=curr_summ) {
    var amort_mid = 0;
    $(".portfolio_tabs_table.pt_deals tr[data-linked_row="+linked_rowId+"]").each(function(indx, element)
      {
        var cnt = $(element).find('.portfolio_elem_number').val();
        var curr_amort_summ = parseFloat($(element).attr('data-ammort-summ'));
        amort_mid += (curr_amort_summ*Number(cnt)/Number(allCnt));
      }
    );
    var initfacevalue_orig = parseFloat(linked_tr.attr('data-initfacevalue_orig'));
    linked_tr.attr('data-initfacevalue', initfacevalue_orig-amort_mid);
  }
}

function checkNoErrorsActiveRow(row) {
  var res = true;
  var date = $(row).find('.portfolio_element_data').val();
  var price = $(row).find('.portfolio_elem_buy_price').val();
  var price_one = parseFloat($(row).find('.portfolio_elem_buy_price_one').attr('data-price_one'));
  var cnt = $(row).find('.portfolio_elem_number').val();

  /*  if(date==''){
    res = false;
  }*/
  if(parseFloat(price)<=0) {
    res = false;
  }
  if(parseFloat(price_one)<=0) {
    res = false;
  }
  if(Number(cnt)<=0) {
    res = false;
  }
  // console.log('res='+res+' date='+date+' price='+price+' price_one='+price_one+' cnt='+cnt);
  return res;
}

function setLocation(curLoc) {
  //location.href = curLoc;
  // console.log('server', HTTPS+'://'+SITE_SERVER_NAME+'/portfolio/'+curLoc+'/');
  history.pushState(null, null, HTTPS+'://'+SITE_SERVER_NAME+'/portfolio/'+curLoc+'/');
  //history.pushState(null, null, 'http://www.dev.fin-plan.org/portfolio/'+curLoc+'/');
}

function getPortfolioActivesList() {
  var arr = {};


  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'getPortfolioActivesList', portfolioId: $('#portfolios_select').val()}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        getResponse(data);
      }
    }
  );

  function getResponse(data) {
    arr = data;
  }

  //console.log(arr);
}

$('#portfolio_date').datepicker(
  {
  language: 'ru'
  }
);
// $('#portfolio_date').on('blur', function() {
//   $('#portfolio_date').datepicker('setDate');
// });

$('.portfolio_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e)
  {
    var elementId = $('#portfolios_select').val().replace("portfolio_", "");
    $('.portfolio_tabs .tab-pane').removeClass('active');
    $('.portfolio_tabs .tab-pane').removeClass('in');
    setPortfolioStatesToSession(elementId, 'portfolio_maintabs_selector', $(this).attr('data-targ')); //Сохраняем в сессию через ajax
    var target = $(this).attr('data-targ');
 	 if(target=='history_tab' || target=='mycache_tab' || target=='analyse_tab'){
 	 	 //Обновляем графики и таблицы если находимся в конкретной вкладке
  	    updateTabChartsAndTable(true);

		 $('.portfolio_actives_block').addClass('hidden');
	 } else {
		 if(target=='backtest_tab' || target=='valuation_tab'){
	 	 	 //Обновляем графики и таблицы если находимся в конкретной вкладке
	  	    updateTabChartsAndTable();
		 }
		 $('.portfolio_actives_block').removeClass('hidden');
	 }

	 if(target!='valuation_tab'){
		 $('.actives_type_filter_block').addClass('hidden');
	 } else {
	     //Вызаваем пересчет для расчета данных графика, что бы потом из этого расчета вызвалось обновление подвала таблицы с посчитанными параметрами по целевому просаду
		  //Тем самыв устраняем баг когда в просаде NAN при переходе на оценку портфеля из любой другой загруженной изначально вкладки
 		  radarRecount();
		 $('.actives_type_filter_block').removeClass('hidden');
	 }

	 var target = '.' +target;
    $('.portfolio_tabs').find(target).addClass('in');
    $('.portfolio_tabs').find(target).addClass('active');


  }
);

//Обновляет активную вкладку путем выставления признака data-need-reload = Y и триггера show
function updateActiveTabOnActiveChange(){
	$('.portfolio_tabs li').find('a[data-toggle="tab"]').attr('data-need-reload', 'Y');
	$('.portfolio_tabs li.active').removeClass('active').find('a[data-toggle="tab"]').attr('data-need-reload', 'Y').tab('show');
}

/* Перезагрузка для отдельных вкладок */
function reloadHistoryTab(clearFilter) { //История
	if($('#portfolios_select').length==0) return;
   historyGraphicRebuild(clearFilter);
	console.log('historyGraphicRebuild -> reloadHistoryTab');
}
function reloadMyCacheTab() { //Мои доходы
  if($('#portfolios_select').length==0) return;
  var val = $('#portfolios_select').val();
  var elementId = val.replace("portfolio_", "");
  updateMyCacheTab(elementId);
}
function reloadBacktestTab() { //Бектест
  backtestGraphicRebuild();
}
/* Перезагрузка для отдельных вкладок */


//Записываем суммы портфеля в инфоблок
function updateStatSumm(price_curr_txt, price_final_txt) {
  if(!$('#portfolios_select').length || $('#popup_autoballance.fade.in').length>0) {
    return false;
  }
  //Не обновляем значения сумм инвестиций и текущей для портфеля если в аргументах undefined
  if(price_curr_txt=='undefined' || price_final_txt=='undefined' ) return false;

  //Не обновляем значения сумм инвестиций и текущей для портфеля если находимся в перечисленных табах
  var activeTab = $('ul.portfolio_tabs li.active a').attr('data-targ');
  if(activeTab=='analyse_tab' || activeTab=='mycache_tab' || activeTab=='backtest_tab' ) return false;

  var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
  var arr = {'portfolioId': portfolioId, 'invest_summ': price_final_txt, 'current_summ': price_curr_txt};

  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {'ajax': "y", 'action': 'updatePortfolioStatSumm', 'data': arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {
      }
    }
  );
}

//Читаем суммы портфеля из инфоблок
function getStatSumm() {
  if(!$('#portfolios_select').length || $('#popup_autoballance.fade.in').length>0) {
    return false;
  }
  var pid = $('#portfolios_select').val().replace("portfolio_", "");
  var arr = [];
  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {'ajax': "y", 'action': 'getPortfolioStatSumm', 'pid': pid}
      ,
    cashe: false,
    async: false,
    success: function(data) {
    	getResponse(data);
      }
    }
  );

  function getResponse(data) {
    arr = data;
  }
  return arr;
}


//Обновляет текущий портфель согласно параметрам выбранным на странице
function updatePortfolio() {

  var elementId = $('#portfolios_select').val().replace("portfolio_", "");
  var date = $('#portfolio_date').val();
  var arr = {id: elementId, date: date};


  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "html",
    data: {ajax: "y", action: 'updatePortfolio', data: arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        getResponse(data);
      }
    }
  );

  function getResponse(data) {
    //console.log(data)
  }
  initEditorActivities();
  //updateTabChartsAndTable();
  overlayHide();
}

//Добавляет новый портфель
function addPortfolio(actives_list, add_to_portfolio) {
  var add_actives_list =[];
  var owner = '';
  if(actives_list.length>0) {
    add_actives_list = actives_list;
	 owner = 'popup';
  }
  var date = $('#popup_add_portfolio_date').val();
  var portfolio_name = $('#popup_add_portfolio_name').val();
  var arr = {'name': portfolio_name, 'date': date, 'actives_list': add_actives_list, 'add_to_exist_portfolio': add_to_portfolio};


  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'addPortfolio', data: arr, 'owner':owner}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        if(data["result"]=='ok') {
          //console.log(data);
          // window.top.location.href='/portfolio/portfolio_'+data["id"];
			 $('.calculate_table_clear_btn').trigger('click');//Очистка галок в радаре после создания портфеля
			 addNewPortfolioToPopupList(data["id"]);
          window.open('/portfolio/portfolio_'+data["id"], "_blank");
          $('#popup_add_portfolio').modal('hide');
        }
        else {
          alert('Ошибка создания портфеля: '+data["error_text"]);
        }
      }
    }
  );

  function addNewPortfolioToPopupList(newPid){
	 if(owner=='popup'){
	 	var popup_portfolio_list_select = $('#popup_add_portfolio #popup_add_to_portfolio_radar_list');
		popup_portfolio_list_select.append('<option value="portfolio_'+newPid+'">'+portfolio_name+'</option>').trigger('change.select2');
	 }
  }

}

function clearcachePortfolio(){
  overlayShow();

  var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
  $.ajax({
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'clearcachePortfolio', 'portfolioId': portfolioId},
    cashe: false,
    async: false,
    success: function (data) {
      if (data['result'] == 'success') {
        location.reload();
      } else if (data['result'] == 'error') {
        alert('Ошибка обновления портфеля: ' + data['message']);
      }

      overlayHide();
    },
    error: function () {
      overlayHide();
    }
  });

  return false;


}

//Пересчет истории портфеля
function recalculatePortfolio() {
  overlayShow();

  var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");

  $.ajax({
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'recalculatePortfolio', 'portfolioId': portfolioId},
    cashe: false,
    async: false,
    success: function (data) {
      if (data['result'] == 'ok') {
        location.reload();
      } else if (data['result'] == 'error') {
        alert('Ошибка обновления портфеля: ' + data['message']);
      }

      overlayHide();
    },
    error: function () {
      overlayHide();
    }
  });

  return false;
}

//Обновляет данные по всем активам при ребалансировке
function updatePortfolioActivesAutobalance(arActivesRows) {
  if(arActivesRows.length==0) return false;
  var arData =[];
  var hasChanges = false; //Наличие изменений в балансе
  var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
  for (i = 0; i<arActivesRows.length; i++) {
    let curr_tr = arActivesRows[i];
    var portfolioEditMode = $('#checkbox_watch').prop('checked')? 'Y': 'N';
    var activeId = curr_tr.closest('tr').attr('data-deal-id'); //ID сделки
    var activeType = curr_tr.closest('tr').attr('data-elem');
    var inlotCnt = curr_tr.closest('tr').attr('data-inlot_cnt');
    var iblockActiveId = curr_tr.closest('tr').attr('data-iblock_elem_id');
    var secid = curr_tr.closest('tr').data('secid');
    var cnt = Number(curr_tr.find('.portfolio_elem_number').val());
    var price = Number(curr_tr.find('.portfolio_elem_buy_price').val());
    var date = curr_tr.closest('tr').attr('data-real_date_buy');
    var loaded_cnt = Number(curr_tr.closest('tr').attr('data-loaded-cnt'));
    var price_one = curr_tr.find('.elem_buy_price_one').attr('data-price_one');
    var currency = curr_tr.closest('tr').attr('data-currency');
    var arr = {
    	'portfolioEditMode': portfolioEditMode,
		'inlotCnt': inlotCnt,
		'activeId': activeId,
		'activeType': activeType,
    	'iblockActiveId': iblockActiveId,
		'price_one': price_one,
		'date': date,
    	'cnt': cnt,
		'loaded_cnt': loaded_cnt,
		'price': price,
		'secid': secid,
		'currency': currency
    };

    if((cnt!=loaded_cnt) && hasChanges == false) {
      hasChanges = true;
    }
    arData.push(arr);
  }
  if(hasChanges)
    $.ajax(
      {
      type: "POST",
      url: "/ajax/portfolio/portfolio.php",
      dataType: "json",
      data: {ajax: "y", action: 'setAutobalance', 'portfolioId': portfolioId, 'data': arData},
      cashe: false,
      async: false,
      success: function(data) {
          console.log('autobal_res', data);
          if(data["result"]=='ok') {
          	//Выставляем флаг необходимости обновления содержимого всех вкладок при их открытии
				$('.portfolio_tabs li').find('a[data-toggle="tab"]').attr('data-need-reload', 'Y');

            updateActiveRowAfterSaveDBAutobalanceLink();
/*            if($('#popup_autoballance.fade.in').length==0)
              backtestGraphicRebuild();*/

				checkSubzeroCache();
          }
          else {
            alert('Ошибка обновления сделки портфеля: '+data["message"]);
          }
        }
      }
    );
  function updateActiveRowAfterSaveDBAutobalanceLink() {
    updateActiveRowsAfterSaveDBAutobalance(arActivesRows, arData);
  }
  console.log('arData', arData);

}


//Обновляет актив в портфолио при изменении в таблице по сделкам в режиме редактирования и добавляет движения в историю
function updatePortfolioActive(curr_tr) {
  var portfolioEditMode = $('#checkbox_watch').prop('checked')? 'Y': 'N';
  var activeId = curr_tr.closest('tr').attr('data-deal_id'); //ID сделки
  var activeType = curr_tr.closest('tr').attr('data-elem');
  var inlotCnt = curr_tr.closest('tr').attr('data-inlot_cnt');
  var iblockActiveId = curr_tr.closest('tr').attr('data-iblock_elem_id');
  var secid = curr_tr.closest('tr').data('secid');
  var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
  var cnt = Number(curr_tr.find('.portfolio_elem_number').val());
  var price = Number(curr_tr.find('.portfolio_elem_buy_price').val());
  var date = curr_tr.find('.portfolio_element_data').val();
  var loaded_cnt = Number(curr_tr.find('.portfolio_elem_number').attr('data-loaded_cnt'));
  var loaded_price = Number(curr_tr.find('.portfolio_elem_buy_price').attr('data-loaded_price'));
  var loaded_date = curr_tr.find('.portfolio_element_data').attr('data-loaded_date');
  var loaded_price_one = curr_tr.find('.elem_buy_price_one').attr('data-loaded_price_one');
  var price_one = curr_tr.find('.elem_buy_price_one').attr('data-price_one');
  var currency = curr_tr.closest('tr').attr('data-currency');
  var arr = {
  		'portfolioEditMode': portfolioEditMode,
		'inlotCnt': inlotCnt,
		'activeId': activeId,
		'activeType': activeType,
  		'iblockActiveId': iblockActiveId,
		'price_one': price_one,
		'loaded_price_one': loaded_price_one,
		'date': date,
  		'cnt': cnt,
		'loaded_cnt': loaded_cnt,
		'loaded_price': loaded_price,
		'price': price,
		'loaded_date': loaded_date,
  		'secid': secid,
		'currency': currency
  };


  //console.log('arrUpdate', arr);

  hasChanges = false;
  if((cnt!=loaded_cnt) || (price!=loaded_price) || (date!=loaded_date)) {
    hasChanges = true;
  }

  if(hasChanges)
    $.ajax(
      {
      type: "POST",
      url: "/ajax/portfolio/portfolio.php",
      dataType: "json",
      data: {ajax: "y", action: 'updatePortfolioActiveByDeal', 'portfolioId': portfolioId, 'data': arr}
        ,
      cashe: false,
      async: false,
      success: function(data) {

          if(data["result"]=='ok') {
            // console.log(data["message"]);
            updateActiveRowAfterSaveDBLink();
            if($('#popup_autoballance.fade.in').length==0)
              backtestGraphicRebuild();

				checkSubzeroCache();
          }
          else {
            alert('Ошибка обновления сделки портфеля: '+data["message"]);
          }
        }
      }
    );

  function updateActiveRowAfterSaveDBLink() {
    updateActiveRowAfterSaveDB(curr_tr, arr);

    /*   console.log('upd>curr_tr ',curr_tr);
   console.log('upd>arr ',arr);*/
  }

  //console.log('portfolioId:'+portfolioId+' activeId:'+activeId+' cnt:'+cnt+' price:'+price+' date:'+date);
}

function setVisibleAutobalanceButton(portfolioId) {
  var arr = {'portfolioId': portfolioId};

  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'isSimpleDeals', data: arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        setVisAutobalanceBtn(data);


      }
    }
  );

  function setVisAutobalanceBtn (data) {

    if(data=='Y') {
      $('.popup_portfolio_rebalance_btn').removeClass('hidden');
    }
    else {
      $('.popup_portfolio_rebalance_btn').addClass('hidden');
    }
  }
}

//Обновляет служебные данные в строке актива по сделке после успешного обновления в БД
function updateActiveRowAfterSaveDB(curr_tr, arr) {
	//TODO Удалить
/*  curr_tr.find('.portfolio_elem_number').attr('data-loaded_cnt', arr['cnt']);
  curr_tr.find('.portfolio_elem_buy_price').attr('data-loaded_price', arr['price']);
  curr_tr.find('.portfolio_element_data').attr('data-loaded_date', arr['date']);*/
}

//Обновляет служебные данные в строке актива по сделке после успешного обновления в БД в автобалансировке
function updateActiveRowsAfterSaveDBAutobalance(arCurr_tr, arr) {
	//TODO Удалить
/*  for (i = 0; i < arCurr_tr.length; i++) {
    let curr_tr = arCurr_tr[i];
    curr_tr.find('.portfolio_elem_number').attr('data-loaded_cnt', arr[i]['cnt']);
    curr_tr.find('.portfolio_elem_buy_price').attr('data-loaded_price', arr[i]['price']);
    curr_tr.find('.portfolio_element_data').attr('data-loaded_date', arr[i]['date']);
  }*/
}


function deletePortfolioDealWithHistory(deal_id) {
  var arr = {'portfolioId': $('#portfolios_select').val().replace("portfolio_", ""), 'dealId': deal_id};

  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "html",
    data: {ajax: "y", action: 'deleteDealWithHistory', data: arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {

        backtestGraphicRebuild();
        setVisibleAutobalanceButton($('#portfolios_select').val().replace("portfolio_", ""));

      }
    }
  );
}

//Удаляет (списывает) актив по количеству, по методу FIFO
function deletePortfolioActive(portfolioId, activeId, cnt, saleDate, owner = '', exclude = false) {
  var elementId = $('#portfolios_select').data("portfolio_", "");
  var date = $('#portfolio_date').val();
  var price = Number($('.popup_remove_row_price_lot').attr('data-price_lot'));
  var fifo_price = Number($('.popup_remove_row_fifo_price_lot').text()); //стоимость лота по цене фифо
  var price_one = $('#popup_change_row_price').val();
  var price_one_currency = $('#popup_change_row_price_currency').val();
  var fifo_price_one = $('#popup_change_row_fifo_price').val(); //Цена одного актива по цене фифо
  var currency = $('#popup_remove_row_currency_code').val();
  var currency_rate = $('#popup_remove_row_currency_rate').val();
  var activeType = $('#popup_remove_row_type').val();
  var codeActive = $('#popup_remove_row_active_code').val();
  var nameActive = $('#popup_remove_row_name').val();
  var urlActive = $('#popup_remove_row_url').val();
  var nameActiveEmitent = $('#popup_remove_row_emitent_name').val();
  var urlActiveEmitent = $('#popup_remove_row_emitent_url').val();
  var secid = $('#popup_remove_row_active_secid').val();
  var lotsize = $('#popup_remove_row_active_inlot_cnt').val();
  var noCacheOperation = $('#popup_change_row_no_cache_operation').prop('checked')?'Y':'N';
  var arr = {
  		'portfolioId': portfolioId,
		'idActive': activeId,
		'typeActive': activeType,
		'codeActive': codeActive,
		'nameActive': nameActive,
  		'urlActive': urlActive,
		'cnt': cnt,
		'secid': secid,
		'nameActiveEmitent': nameActiveEmitent,
		'urlActiveEmitent': urlActiveEmitent,
  		'price': price,
		'price_one_currency': price_one_currency,
		'fifo_price': fifo_price,
		'price_one': price_one,
		'fifo_price_one': fifo_price_one,
		'lotsize': lotsize,
		'saleDate': saleDate,
  		'currencyActive': currency,
		'currency_rate': currency_rate,
		'noCacheOperation': noCacheOperation,
  		'exclude': exclude ? 'Y' : ''
  };

  showPopupProgress('popup_change_row');
  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "html",
    data: {ajax: "y", action: 'deletePortfolioActive', data: arr, owner},
    cashe: false,
    async: false,
    success: function(data) {

        backtestGraphicRebuild();
        setVisibleAutobalanceButton($('#portfolios_select').val().replace("portfolio_", ""));
		  checkSubzeroCache();
  		  //Обновляем кол-ва активов в фильтре по типам активов
  		  updateActiveTypeFilterCnt();
      }
    }
  );


  hidePopupProgress();
}

//Изымает актив по количеству, по методу FIFO
function excludePortfolioActiveFifo(portfolioId, activeId, cnt, saleDate, owner = '', exclude = false) {
  var elementId = $('#portfolios_select').data("portfolio_", "");

  var date = $('#portfolio_date').val();
  var price = $('#popup_exclude_row_price').val();
  var fifo_price = $('#popup_exclude_row_fifo_price').val(); //стоимость лота по цене фифо
  var price_one = $('#popup_exclude_row_price').val();
  var fifo_price_one = $('#popup_exclude_row_fifo_price').val(); //Цена одного актива по цене фифо
  var currency = $('#popup_exclude_row_currency_code').val();
  var currency_rate = $('#popup_exclude_row_currency_rate').val();
  var activeType = $('#popup_exclude_row_type').val();
  var codeActive = $('#popup_exclude_row_active_code').val();
  var nameActive = $('#popup_exclude_row_name').val();
  var urlActive = $('#popup_exclude_row_url').val();
  var secid = $('#popup_exclude_row_active_secid').val();
  var lotsize = $('#popup_exclude_row_active_inlot_cnt').val();
  var arr = {
  		'portfolioId': portfolioId,
		'idActive': activeId,
		'activeType': activeType,
		'codeActive': codeActive,
		'nameActive': nameActive,
  		'urlActive': urlActive,
		'cnt': cnt,
		'secid': secid,
  		'price': price,
		'fifo_price': fifo_price,
		'price_one': price_one,
		'fifo_price_one': fifo_price_one,
		'lotsize': lotsize,
		'saleDate': saleDate,
  		'currencyActive': currency,
		'currency_rate': currency_rate,
  		'exclude': exclude ? 'Y' : ''
  };

  showPopupProgress('popup_exclude_cache_row');
  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "html",
    data: {ajax: "y", action: 'deletePortfolioActive', data: arr, owner}
      ,
    cashe: false,
    async: false,
    success: function(data) {

        backtestGraphicRebuild();
        setVisibleAutobalanceButton($('#portfolios_select').val().replace("portfolio_", ""));

      }
    }
  );


  hidePopupProgress();
}


//Копирует текущий портфель в новый с новым именем и копированием активов и истории из текущего 1 в 1
function copyPortfolio() {
  var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
  var newNamePortfolio = $('#popup_new_copy_portfolio_name').val();
  var portfolioDescr = $('.portfolio_description span.descr_text').text();
  var arr = {'portfolioId': portfolioId, 'name': newNamePortfolio, 'descr': portfolioDescr};


  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'copyPortfolio', data: arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        if(data['result']=='ok') {
          window.top.location.href = '/portfolio/portfolio_'+data['id']+'/';
        }
        else if(data['result']=='error') {
          alert('Не удалось создать копию выбранного портфеля.'+data['message']);
        }
      }
    }
  );

}

//Сохраняет описание портфеля
function editDescrPortfolio() {
  var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
  var newDescrPortfolio = $('#popup_portfolio_descr').val();
  var arr = {'portfolioId': portfolioId, 'descr': newDescrPortfolio};


  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'editDescrPortfolio', data: arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        if(data['result']=='ok') {
          $('.view_portf_descr .descr_text').text(newDescrPortfolio);
          $('#popup_portfolio_editdescr').modal('hide');
        }
        else if(data['result']=='error') {
          alert('Не удалось изменить описание портфеля. '+data['message']);
        }
      }
    }
  );
}

//Переименовывает текущий портфель
function renamePortfolio(portfolioId) {
  var redirect_success = '';
  if($('#portfolios_select').length>0) {
    var elementId = $('#portfolios_select').val().replace("portfolio_", "");
    redirect_success = '/portfolio/portfolio_'+elementId+'/';
  }
  else {
    var elementId = portfolioId;
  }
  var newNamePortfolio = $('#popup_rename_portfolio_name').val();
  var arr = {'portfolioId': elementId, 'newNamePortfolio': newNamePortfolio};


  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'renamePortfolio', data: arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        getResponse(data);
      }
    }
  );

  function getResponse(data) {
    if(data['result']=='ok') {
      if(redirect_success!='') {
        window.top.location.href = redirect_success;
      }
      else {
        // console.log(newNamePortfolio);
        $("#portfolio_lk_portfolio_"+elementId).text(newNamePortfolio);
      }

      $('#popup_portfolio_rename').modal('hide');
    }
    else if(data['result']=='error') {
      alert('Не удалось переименовать портфель. '+data['message']);
    }
  }

}


//Очищает текущий портфель согласно параметрам выбранным на странице
function clearPortfolio(delete_from_lk) {
  if($('#portfolios_select').length>0) {
    var elementId = $('#portfolios_select').val().replace("portfolio_", "");
  }
  else {
    var elementId = delete_from_lk;
  }
  var date = $('#portfolio_date').val();
  var arr = {id: elementId};


  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'clearPortfolio', data: arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        if(data['result']=='ok') {
          location.reload();
        }
        else if(data['result']=='error') {
          alert('Невозможно очистить выбранный портфель.'+data['message']);
        }
      }
    }
  );

}

//Удаляет текущий портфель согласно параметрам выбранным на странице
function deletePortfolio(delete_from_lk) {
  if($('#portfolios_select').length>0) {
    var elementId = $('#portfolios_select').val().replace("portfolio_", "");
  }
  else {
    var elementId = delete_from_lk;
  }
  var date = $('#portfolio_date').val();
  var arr = {id: elementId};
  var portfolio_list;
  var portfolio_next_link = '';
  var redirect_success = '';
  showPopupProgress('popup_portfolio_remove_row');
  if(delete_from_lk=='undefined' || delete_from_lk==false) {
    //Получим список портфелей пользователя для перенаправления на оставшийся портфель после удаления текущего либо на общую страницу портфелей, если нет больше ни одного портфеля.
    $.ajax(
      {
      type: "POST",
      url: "/ajax/portfolio/portfolio.php",
      dataType: "json",
      data: {ajax: "y", action: 'getPortfolioListForUser'}
        ,
      cashe: false,
      async: false,
      success: function(data) {
          getResponse(data);
        }
      }
    );

    function getResponse(data) {
      portfolio_list = data;
		portfolio_next_link = '';
      for (key in portfolio_list) {
       if(portfolio_list[key].ver.length>0)
        if(key!='portfolio_'+elementId && portfolio_next_link.length<=0) {
          portfolio_next_link = key+'/';
        }
      };


    }

    redirect_success = '/portfolio/'+portfolio_next_link;
  }
  else {
    arr = {id: delete_from_lk};


    redirect_success = '/personal/';
  }

  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'deletePortfolio', data: arr}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        if(data['result']=='ok') {
          window.top.location.href = redirect_success;
        }
        else if(data['result']=='error') {
          alert('Невозможно удалить выбранный портфель.'+data['message']);
			 hidePopupProgress();
        }
      }
    }
  );
 // hidePopupProgress();
}


//portfolioChange();
//$('#portfolios_select').change(portfolioChange); /* Смена портфеля без перезагрузки страницы */
$('#portfolios_select').change(portfolioChangeReload); /* Смена портфеля перезагрузкой страницы */
function portfolioChangeReload(){
	  /* Смена портфеля перезагрузкой */
  var val = $('#portfolios_select').val();
  if(val=='/portfolio/') {
    window.top.location.href = val;
  }
  else {
    window.top.location.href = '/portfolio/'+val;
  }
  /* Смена портфеля перезагрузкой */
}
$('#actives_type_filter li').each(function() {
  var ths = $(this);

  if (ths.hasClass('active') && ths.attr('data-value') !== 'clear_filter') {
    if (ths.find('.icon').length == 0) {
      ths.append('<span class="close">&times;</span>');
    }
  }
});
$('#actives_type_filter li').click(function() {
  var ths = $(this);

  if (!ths.hasClass('disabled')) {
    overlayShow();

    setTimeout(function() {

      var pid = $('#portfolios_select').val().replace("portfolio_", "");
      var act_type_filter = [];
	if (ths.attr('data-value') !== 'clear_filter') {
      if (ths.hasClass('active')) {
        ths.removeClass('active');
        ths.find('.close').remove();
      } else {
        if (ths.attr('data-value') !== 'clear_filter') {
          ths.addClass('active');
          if (ths.find('.icon').length == 0) {
            ths.append('<span class="close">&times;</span>');
          }
        }
      }

      $('#actives_type_filter li.active').each(function(indx, element){
        act_type_filter.push($(element).attr('data-value'));
      });
	 } else {
		$('#actives_type_filter li.active').removeClass('active').find('.close').remove();
	 }
      //   console.log('act_type_filter1',act_type_filter.toString());
      setPortfolioStatesToSession(pid, 'actives_type_filter', act_type_filter.toString()); //Сохраняем в сессию через ajax
      portfolioChange();
    }, 10);
  }
});

$('#portfolios_benchmark').change(function()
  {
    var pid = $('#portfolios_select').val().replace("portfolio_", "");
    setPortfolioStatesToSession(pid, 'selected_benchmark', $('#portfolios_benchmark').val()); //Сохраняем в сессию через ajax
	 $('ul.portfolio_tabs li.active a').attr('data-need-reload', 'Y');
    portfolioChange();
  }

);

$('#portfolios_benchmark_selfie').change(function()
  {
    var pid = $('#portfolios_select').val().replace("portfolio_", "");
    setPortfolioStatesToSession(pid, 'selected_porfolio_benchmark', $('#portfolios_benchmark_selfie').val()); //Сохраняем в сессию через ajax
	 $('ul.portfolio_tabs li.active a').attr('data-need-reload', 'Y');
    portfolioChange();
  }

);

//Обновление коэффициентов портфеля
function updatePortfolioCoeff(portfolioId, recalculate) {

  if(recalculate != true) {//Если требуется пересчет - отдаем в ajax параметр пересчета в инфоблок для конкретного портфеля и получаем уже сохраненные пересчитанные значения
    recalculate = false;
  }

  if(recalculate == true) {
    overlayShow();
  }

  //Получаем описание портфеля
  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "text",
    data: {ajax: "y", action: 'getIblockPortfolioCoeff', portfolioId: portfolioId, recalculate: recalculate}
      ,
    cashe: false,
    async: false,
    beforeSend: function(recalculate) {
        if(recalculate)
          overlayShow();
      }
      ,
    error: function(errData) {
        overlayHide();
        //console.log('updatePortfolioCoeff err',errData);
      }
      ,
    success: function(data) {
        //console.log(data);
        var arCoeff = JSON.parse(data);
        var sharp_rus = '';
        var sharp_usa = '';
        var treynor_rus = '';
        var treynor_usa = '';
        var sortino_rus = '';
        var sortino_usa = '';

        if(arCoeff["CF_SHARP_RUS_P"].length>0 && arCoeff["CF_SHARP_RUS_I"].length>0) {//Показываем Шарпа по РФ
          sharp_rus = '<p class=""><span class="">Шарп (РФ): </span><span class="valuation_tab_val">';
          sharp_rus += '<span class="'+(parseFloat(arCoeff["CF_SHARP_RUS_P"])>parseFloat(arCoeff["CF_SHARP_RUS_I"])? 'green': 'red')+'">'+arCoeff["CF_SHARP_RUS_P"]+'</span>';
          sharp_rus += ' > <span class="'+(parseFloat(arCoeff["CF_SHARP_RUS_I"])>0? 'green': 'red')+'">'+arCoeff["CF_SHARP_RUS_I"]+'</span> > 0</span></p>';
        }
        if(arCoeff["CF_SHARP_USA_P"].length>0 && arCoeff["CF_SHARP_USA_I"].length>0) {//Показываем Шарпа по РФ
          sharp_usa = '<p class=""><span class="">Шарп (США): </span><span class="valuation_tab_val">';
          sharp_usa += '<span class="'+(parseFloat(arCoeff["CF_SHARP_USA_P"])>parseFloat(arCoeff["CF_SHARP_USA_I"])? 'green': 'red')+'">'+arCoeff["CF_SHARP_USA_P"]+'</span>';
          sharp_usa += ' > <span class="'+(parseFloat(arCoeff["CF_SHARP_USA_I"])>0? 'green': 'red')+'">'+arCoeff["CF_SHARP_USA_I"]+'</span> > 0</span></p>';
        }

        if(arCoeff["CF_TREYNOR_RUS_P"].length>0 && arCoeff["CF_TREYNOR_RUS_I"].length>0) {//Показываем Шарпа по РФ
          treynor_rus = '<p class=""><span class="">Трейнор (РФ): </span><span class="valuation_tab_val">';
          treynor_rus += '<span class="'+(parseFloat(arCoeff["CF_TREYNOR_RUS_P"])>parseFloat(arCoeff["CF_TREYNOR_RUS_I"])? 'green': 'red')+'">'+arCoeff["CF_TREYNOR_RUS_P"]+'</span>';
          treynor_rus += ' > <span class="'+(parseFloat(arCoeff["CF_TREYNOR_RUS_I"])>0? 'green': 'red')+'">'+arCoeff["CF_TREYNOR_RUS_I"]+'</span> > 0</span></p>';
        }
        if(arCoeff["CF_TREYNOR_USA_P"].length>0 && arCoeff["CF_TREYNOR_USA_I"].length>0) {//Показываем Шарпа по РФ
          treynor_usa = '<p class=""><span class="">Трейнор (США): </span><span class="valuation_tab_val">';
          treynor_usa += '<span class="'+(parseFloat(arCoeff["CF_TREYNOR_USA_P"])>parseFloat(arCoeff["CF_TREYNOR_USA_I"])? 'green': 'red')+'">'+arCoeff["CF_TREYNOR_USA_P"]+'</span>';
          treynor_usa += ' > <span class="'+(parseFloat(arCoeff["CF_TREYNOR_USA_I"])>0? 'green': 'red')+'">'+arCoeff["CF_TREYNOR_USA_I"]+'</span> > 0</span></p>';
        }

        if(arCoeff["CF_SORTINO_RUS_P"].length>0 && arCoeff["CF_SORTINO_RUS_I"].length>0) {//Показываем Шарпа по РФ
          sortino_rus = '<p class=""><span class="">Сортино (РФ): </span><span class="valuation_tab_val">';
          sortino_rus += '<span class="'+(parseFloat(arCoeff["CF_SORTINO_RUS_P"])>parseFloat(arCoeff["CF_SORTINO_RUS_I"])? 'green': 'red')+'">'+arCoeff["CF_SORTINO_RUS_P"]+'</span>';
          sortino_rus += ' > <span class="'+(parseFloat(arCoeff["CF_SORTINO_RUS_I"])>0? 'green': 'red')+'">'+arCoeff["CF_SORTINO_RUS_I"]+'</span> > 0</span></p>';
        }
        if(arCoeff["CF_SORTINO_USA_P"].length>0 && arCoeff["CF_SORTINO_USA_I"].length>0) {//Показываем Шарпа по РФ
          sortino_usa = '<p class=""><span class="">Сортино (США): </span><span class="valuation_tab_val">';
          sortino_usa += '<span class="'+(parseFloat(arCoeff["CF_SORTINO_USA_P"])>parseFloat(arCoeff["CF_SORTINO_USA_I"])? 'green': 'red')+'">'+arCoeff["CF_SORTINO_USA_P"]+'</span>';
          sortino_usa += ' > <span class="'+(parseFloat(arCoeff["CF_SORTINO_USA_I"])>0? 'green': 'red')+'">'+arCoeff["CF_SORTINO_USA_I"]+'</span> > 0</span></p>';
        }

        //Вывод в DOM
        //if(IS_ADMIN=="Y"){
        $('.portfolio_coeff_cont').html(''); //Очищаем контейнер

        if(sharp_rus.length>0) {
          $('.portfolio_coeff_cont').append(sharp_rus);
        }
        if(sharp_usa.length>0) {
          $('.portfolio_coeff_cont').append(sharp_usa);
        }

        if(treynor_rus.length>0) {
          $('.portfolio_coeff_cont').append(treynor_rus);
        }
        if(treynor_usa.length>0) {
          $('.portfolio_coeff_cont').append(treynor_usa);
        }

        if(sortino_rus.length>0) {
          $('.portfolio_coeff_cont').append(sortino_rus);
        }
        if(sortino_usa.length>0) {
          $('.portfolio_coeff_cont').append(sortino_usa);
        }

        // } //IS_ADMIN

        overlayHide();
      }
    }
  );


}

function updateMyCacheTab(elementId) {
  //Обновляем id портфеля в атрибутах календарей на вкладке "Мои доходы"
  if($('.mycache_tab').length) {
    $('.events_calendar_dates_from.portfolio').attr('data-pid', elementId);
    $('.events_calendar_dates_to.portfolio').attr('data-pid', elementId).trigger('changeDate');
  }

}


function portfolioChange(hide_loader) {
  if(hide_loader!=true) {
    overlayShow();
  }



  var arr = {};


  /*    "portfolio_1": {date_curr: "2018-9-1", watch: true},
    "portfolio_2": {date_curr: "", watch: false}*/

  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {ajax: "y", action: 'getPortfolioListForUser'}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        getResponse(data);
      }
    }
  );

  function getResponse(data) {
    arr = data;
  }

  var val = $('#portfolios_select').val();

  if(val=='/portfolio/') {
    window.top.location.href = val;
  }
  else {
    $('.start_page_hidden').removeClass('hidden_port_startpage');
  }


  var elementId = val.replace("portfolio_", "");
  var date, status;
  var disableEdit = true;
  for (key in arr) {
    if (key == val) {
      date = String(arr[key].date_curr);
      status = arr[key].watch;
      disableEdit = false; //Если портфель пользовательский - разрешаем редактирвоание
    }
  }

  //обновляем id портфеля в атрибуте кнопки "пересчитать коэффициенты"
  $('.portfolio_recalculate_coeff').attr('data-portfolioId', elementId);

  setLocation(val); //Изменяем URL после выбора другого портфеля из списка

  var activeTab = $('ul.portfolio_tabs li.active a').attr('data-targ');
  //в этом месте подгружать новую таблицу
  //var tablecnt = 0;
  // console.log("pathval: "+val);
  //if(tablecnt==0){
	 var act_type_filter = [];
	 $('#actives_type_filter li.active').each(function(indx, element){
      act_type_filter.push($(element).attr('data-value'));
    });
	  console.log('act_type_filter2',act_type_filter.toString());
  $.ajax(
    {
    type: "POST",
    url: "/portfolio/index.php",
    dataType: "html",
    data: {ajax: "Y", 'ELEMENT_ID': val.replace("portfolio_", ""), 'activeTab': activeTab, 'disableEdit': disableEdit, 'actives_type_filter': act_type_filter.toString()}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        getResponseTable(data);
        updateTablesScrollbars();

      }
    }
  );
  //}
  function getResponseTable(data) {
    //tablecnt = 1;
    $('.portfolio_valuation_table').html(data);
    //Управляем видимостью блоков для редактирования
    if(disableEdit==true) {
      $('.edit_switch_block, .pf_descr_edit_btn, .portfolio_tabs_table.pt_active .icon-close, .popup_portfolio_remove_btn, .popup_add_btn').addClass('hidden');
    }
    else {
      $('.edit_switch_block, .pf_descr_edit_btn, .portfolio_tabs_table.pt_active .icon-close, .popup_portfolio_remove_btn, .popup_add_btn').removeClass('hidden');
    }
    recountTable();
    tableFootUpdate();
    radarRecount();
    //updateTabChartsAndTable();
    if ($('.tooltip_btn').length != 0) {
      $('.tooltip_btn').tooltip(
        {
        trigger: 'hover',
        placement: 'top'
        }
      );
    }
  }

  //Получаем описание портфеля
  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "text",
    data: {ajax: "y", action: 'getPortfolioDescription', portfolioId: elementId}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        //Перекидываем описание портфеля в верхнее не изменяемое поле
        if(data.length==1) {data = 'Без описания';}
        //console.log('descr',data.length);
        $('.view_portf_descr .descr_text').text(data);
        $('.table_portf_descr').text(data);
      }
    }
  );

  //updatePortfolioCoeff(elementId, false);

  initActivities();
  initEditorActivities();
  calculateTableCompanyName();
  calculateTableActiveName();
  if (date != '' && date != undefined && !isNaN(date)) {
    date_final = new Date(date);
    $('#portfolio_date').datepicker('update', date_final);
  }
  else {
    $('#portfolio_date').datepicker('update', '');
  }

  //Переключаем режим редактирования при перевыборе портфеля
  $('#checkbox_watch').prop('checked', status);

  $('ul.portfolio_tables_selector li.active').click();
	if($('ul.portfolio_tabs li.active a').attr('data-targ')=='history_tab')
  		historyFilterUpdate(elementId);
//  console.log('historyFilterUpdate -> portfolioChange');
  if(hide_loader!=true) {
    overlayHide();
  }
  setVisibleAutobalanceButton(elementId);

  overlayShow();
  updateTabChartsAndTable(true); //Очищаем фильтр для таблицы истории передавая true
  recountAmortization();
 // eventsCacheTableUpdate(elementId); //Подгружаем таблицу мои доходы
  overlayHide();


}

function recountAmortization(){
	$('.portfolio_valuation_table .pt_active tbody tr:not(.hidden-row)').each(function(indx, element)
	    {
	      var tr = $(element);
	   	if(tr.attr('data-elem')=='bond'){
					updateAmortizationDataInActive(tr.attr('data-id'));
				}
		});

	 recountTable();
    tableFootUpdate();
    radarRecount();
}

/*function updateTabChartsAndTable(clearFilter = false) {
	if($('#portfolios_select').length==0) return;
  historyGraphicRebuild(clearFilter);
  backtestGraphicRebuild();
  var val = $('#portfolios_select').val();
  var elementId = val.replace("portfolio_", "");
  updateMyCacheTab(elementId);
}*/

function updateTabChartsAndTable(clearFilter){
		var activeTab = $('ul.portfolio_tabs li.active a').attr('data-targ');
		var needReloadTab = $('ul.portfolio_tabs li.active a').attr('data-need-reload');
		console.log('needReloadTab',needReloadTab);
	   if(activeTab=='history_tab' && needReloadTab=='Y'){
  			if ($('#portfolioHistoryChart').length != 0){
  				//overlayShow();
    			historyGraphicRebuild();
			   arFilter = getHistoryTableFilterFromHTML(clearFilter);
				var val = $('#portfolios_select').val();
				var portfolioId =  val.replace("portfolio_", "");
      		historyTableUpdate(portfolioId, arFilter);
				//console.log('historyGraphicRebuild -> updateTabChartsAndTable clFilter flag=', clearFilter);
				//overlayHide();

/*				var val = $('#portfolios_select').val();
				var portfolioId =  val.replace("portfolio_", "");
				var filter = getHistoryTableFilterFromHTML();
			 	historyFilterUpdate(portfolioId, filter);*/
			  //	console.log('historyFilterUpdate -> updateTabChartsAndTable');
				$('ul.portfolio_tabs li.active a').attr('data-need-reload', 'N');
				}
		}
		if(activeTab=='analyse_tab' && needReloadTab=='Y'){
			var val = $('#portfolios_select').val();
			var portfolioId =  val.replace("portfolio_", "");
     		analyseTabUpdate(portfolioId);
	  		$('ul.portfolio_tabs li.active a').attr('data-need-reload', 'N');
		}
		if(activeTab=='mycache_tab' && needReloadTab=='Y'){
			var val = $('#portfolios_select').val();
			var portfolioId =  val.replace("portfolio_", "");
			//Обновляем данные на вкладке "Мои доходы"
			updateMyCacheTab(portfolioId);
			$('ul.portfolio_tabs li.active a').attr('data-need-reload', 'N');
		}
		if(activeTab=='backtest_tab' && needReloadTab=='Y'){
		   backtestGraphicRebuild();
		   $('ul.portfolio_tabs li.active a').attr('data-need-reload', 'N');
		}
		if(activeTab=='valuation_tab' && needReloadTab=='Y'){
		   recountTable();
			tableFootUpdate();
		   $('ul.portfolio_tabs li.active a').attr('data-need-reload', 'N');
		}
}


recountTable();
tableFootUpdate();
updateTabChartsAndTable();

function showPopupProgress(modalId) {
  $('body #'+modalId+' .popup_progress').addClass('active');
}

function hidePopupProgress() {
  $('.popup_progress').removeClass('active');
}

function recountTable() {


  //$('.portfolio_valuation_table .pt_active tbody tr:not(.oblig_off)').each(function(indx, element){
  $('.portfolio_valuation_table .pt_active tbody tr:not(.hidden-row)').each(function(indx, element)
    {
      var tr = $(element);
      //var target_profit = parseFloat(tr.attr('data-target_profit')!=''?tr.attr('data-target_profit'):0);
      var target_profit = parseFloat(tr.attr('data-target_profit'));
      var ShowValues = tr.find('.target_profit').attr('data-usa-access');
/*		if(tr.attr('data-elem')=='bond'){
		target_profit = updateAmortizationDataInActive(tr.attr('id'), true);
		}*/


      if(isNaN(target_profit)) {
        target_profit = 0;
      }

      if (target_profit < 0) {
        tr.find('.target_profit').addClass('line_red');
        if(ShowValues=='N') {
          target_profit_display = '-';
        }
        else {
          target_profit_display = target_profit + '%';
        }
        tr.find('.target_profit').text(target_profit_display);
      }
      else {
        if(ShowValues=='N') {
          target_profit_display = '-';
        }
        else {
          target_profit_display = target_profit + '%';
        }
        tr.find('.target_profit').addClass('line_green');
        tr.find('.target_profit').text(target_profit_display);
      }
      rowUpdate(tr);
      initActivities();
      initEditorActivities();
    }
  );



  //Записываем суммы портфеля в инфоблок
  updateStatSumm($('.portfolio_elem_buy_price_curr_summ').attr('data-current_summ'), $('.portfolio_elem_buy_price_final_summ').attr('data-value'));

}

function rowUpdate(tr, linked_rowId) {
  // console.log('tr id=',tr.attr('id'));
  // tr = $(tr);
  tr = $('.portfolio_valuation_table .pt_active tbody').find('tr#'+tr.attr('id'));

  var price = tr.find('.portfolio_elem_buy_price').val();
  var number = tr.find('.portfolio_elem_number').val();
  var curr_exchange_price = parseFloat(tr.attr('data-current_exchange_price'));
  var prognose_income = 0;
  var price_final, curr_price_final;

  if (price != '' && price != ' ' && price != '-' && price != 'NaN' && price != undefined && number != '' && number != ' ' && number != '-' && number != 'NaN' && number != undefined) {
    price = price.toString().replace(' ', '');
    price = parseFloat(price);

    number = number.toString().replace(' ', '');
    number = parseFloat(number);

    /*    price_final = price * number;
      price_final = price_final;*/
    price_final = parseFloat(tr.attr('data-clearincome'));
  }
  else {
    price_final = '-';
  }

  tr.find('.portfolio_elem_buy_price_final').text(price_final.toLocaleString());
  tr.find('.portfolio_elem_buy_price_final').attr('data-value', price_final);

  if (tr.attr('data-currency') == 'rub') {
    currency_value = 1;
  } else {
	 currency_value =  parseFloat(tr.attr('data-currency_rate'));
  }
/*  else if (tr.attr('data-currency') == 'usd') {
    currency_value = parseFloat($('#usd_currency').val());
  }
  else if (tr.attr('data-currency') == 'eur') {
    currency_value = parseFloat($('#eur_currency').val());
  }*/

  if (curr_exchange_price != '' && curr_exchange_price != ' ' && curr_exchange_price != '-' && curr_exchange_price != 'NaN' && curr_exchange_price != undefined && number != '' && number != ' ' && number != '-' && number != 'NaN' && number != undefined) {
    if(tr.attr('data-elem') != 'bond' && tr.attr('data-elem') != 'currency') {
      curr_price_final = curr_exchange_price * currency_value * number;
    }
    else if(tr.attr('data-elem') == 'currency') {
      curr_price_final = currency_value * number;
    } else {
		curr_price_final = curr_exchange_price * number;
    }
  }
  else {
    curr_price_final = '-';
  }

  tr.find('.portfolio_elem_buy_price_current').text(curr_price_final.toLocaleString());
  //console.log(curr_price_final);
  //tr.find('.portfolio_elem_buy_price_current').attr('data-value', curr_price_final);
  tr.find('.portfolio_elem_buy_price_current').attr('data-value', (curr_price_final=='-'? 0: curr_price_final));

  tr.find('.profit_main').removeClass('line_green');
  tr.find('.profit_main').removeClass('line_red');

  //console.log('code= '+(tr.find('.elem_name a').text())+' curr_price_final= '+curr_price_final+' price_final='+price_final);

  var curr_profit = (curr_price_final / price_final-1) * 100;

  //Проверим наличие амортизации и посчитаем ее
  if(tr.attr('data-elem')=='bond' && tr.attr('data-oblig-off')!='Y') {

    //Обновляем данные по амортизации для строки актива и получаем процент прироста curr_profit_A
    var curr_profit_A = updateAmortizationDataInActive(tr.attr('data-id'), true);
    var initfacevalue = parseFloat(tr.attr('data-initfacevalue'));
    var facevalue = parseFloat(tr.attr('data-facevalue'));
	 console.log('curr_profit='+curr_profit+' curr_profit_A='+curr_profit_A);
    if(tr.attr('data-elem')=='bond' && parseFloat(tr.attr('data-ammort_summ'))>0) {//Если амортизация облигации больше нуля - заменяем расчет доходности расчетом с учетом амортизации
      curr_profit = curr_profit_A;
    }
  }

  var profit_prefix = '+';
  var profit_cls = 'line_green';
  if (curr_profit < 0) {
    profit_prefix = '';
    profit_cls = 'line_red';
  }
  else if (curr_profit.toFixed(1) == -0.0) {
    profit_prefix = '';
    profit_cls = '';
  }
  else if (!isFinite(curr_profit)) {
    curr_profit = 0;
  }


  tr.find('.profit_main').attr('data-profit', curr_profit.toFixed(1)).text(profit_prefix + curr_profit.toFixed(1) + '%');

  var dataProfitSumm = curr_price_final - price_final;
  tr.find('.profit_main').attr('data-profit_summ', (!isFinite(dataProfitSumm)? 0: dataProfitSumm));
  //tr.find('.profit_main').attr('data-profit_summ', (curr_price_final - price_final));

  tr.find('.profit_main').removeClass('line_green').removeClass('line_red').addClass(profit_cls);

  tr.find('.profit_year').removeClass('line_green').removeClass('line_red');

  var dayDiffCount = dateDaysDiffCount(tr.attr('data-real_date_buy'), new Date());
  var year_profit = (curr_profit / dayDiffCount) * 365;
 // console.log('year_profit='+year_profit+' >> curr_prfit: '+curr_profit+' real_date_buy: '+tr.data('real_date_buy')+' day_diff: '+dayDiffCount);
  if (year_profit < 0) {
    profit_prefix = '';
    profit_cls = 'line_red';
  }
  else {
    profit_prefix = '+';
    profit_cls = 'line_green';
  }

  if(!isFinite(year_profit)) {
    year_profit = 0;
  }
  tr.find('.profit_year').text('(' + profit_prefix + year_profit.toFixed(1) + '%)');
  tr.find('.profit_year').addClass(profit_cls);

  tr.find('.target_price').removeClass('line_red');
  tr.find('.target_price').removeClass('line_green');
  var target_profit = parseFloat(tr.attr('data-target_profit'));
  /* var target_price = (curr_price_final * target_profit) + curr_price_final;
 target_price = target_price*1;
 var target_price_txt = target_price.toFixed(2);
 target_price_txt = parseFloat(target_price_txt);
 tr.find('.target_price')'(' + target_price_txt.toLocaleString() + ')');
 if (target_price < 0) {
  tr.find('.target_price').addClass('line_red');
 } else {
  tr.find('.target_price').addClass('line_green');
 }*/

  //Ожидаемый доход
  prognose_income = (curr_price_final/100)*target_profit;
  if(tr.data('elem')=='bond') {//Домножаем на 2 для облигаций
    //prognose_income = prognose_income*2;
  }

  if(isNaN(prognose_income) || prognose_income == '' || prognose_income == undefined) {
    prognose_income = 0;
  }

  tr.attr('data-prognose_income', prognose_income.toFixed(4));

  if(tr.data('elem')=='share' && curr_price_final>0) {
    tr.find('.target_price').attr('data-value', tr.data('drawdown')).addClass('line_red').text((-1*tr.data('drawdown'))+'%');
    tr.attr('data-drawdown_summ', ((curr_price_final/100)*tr.data('drawdown').toFixed(4)));
    //    console.log(tr.data('drawdown'));
  }
  if(tr.data('elem')=='share_etf' && curr_price_final>0) {
    tr.find('.target_price').attr('data-value', tr.data('drawdown')).addClass('line_red').text((-1*tr.data('drawdown'))+'%');
    tr.attr('data-drawdown_summ', ((curr_price_final/100)*tr.data('drawdown').toFixed(4)));
  }
  if(tr.data('elem')=='share_usa' && curr_price_final>0) {
    var ShowValue = tr.find('.target_price').attr('data-usa-access');
    var displayTargetValue = (-1*tr.data('drawdown'))+'%';
    if(ShowValue == 'N') {
      displayTargetValue = '-';
    }
    //tr.find('.target_price').attr('data-value', tr.data('drawdown')).addClass('line_red').text(displayTargetValue);
    tr.find('.target_price').attr('data-value', displayTargetValue).addClass('line_red').text(displayTargetValue);
    tr.attr('data-drawdown_summ', ((curr_price_final/100)*tr.data('drawdown').toFixed(4)));
  }
}

$('#portfolio_date').on('changeDate', function()
  {
    for (var i = 0; i < $('.portfolio_valuation_table .pt_active tbody tr').length; i++) {
      var tr = $('.portfolio_valuation_table .pt_active tbody tr').eq(i);
      rowUpdate(tr);
    }
    //Обнвовляем данные портфеля
    updatePortfolio();
    tableFootUpdate();
    radarRecount();
  }
);


function updateAmortizationDataInActive(bondTrId, returnIncrease) {
  /*  if(tr.attr('data-elem')=='bond' && tr.attr('data-oblig-off')!='Y'){ //amortization count for active bonds
  var amortStr = tr.find('p.amort_bond_sum');
  if(amortStr.length){ //if has amortization, recount amort. summ
    var initfaceValue = parseFloat(tr.attr('data-initfacevalue'))*number;
  var amortSumm;

  amortSumm = (parseFloat(tr.attr('data-initfacevalue_orig'))-parseFloat(tr.attr('data-facevalue')))*number;
  console.log('amortSumm', amortSumm);
  amortStr.find('span').text(amortSumm*-1).attr('data-original-title', 'По облигации погашено '+amortSumm+'\u00A0руб.');
  //Вызвать ф-цию для пересчета и обновления данных по амортизации в главной строке по активу
  }
  }
 */

 console.log('updateAmortizationDataInActive bondTrId',bondTrId);
 console.log('bondTrId',$('body #'+bondTrId));
  var activeStr = $('body #'+bondTrId);
  var activeId = activeStr.attr('data-iblock_elem_id');
  var val = $('#portfolios_select').val();
  var pid =  val.replace("portfolio_", "");
  var mid_buy_summ = 0; //Средн.взвеш. цена покупки
  var buy_summ = 0; //Сумма покупки по всем сделкам тек. облигации
  var cnt_summ = 0; //Суммарное кол-во по всем сделкам тек. облигации
  var now_summ = 0; //Сумма покупки на тек. момент по всем сделкам тек. облигации
  var mid_ammort = 0; //Средн.взвеш. гашение
  var summ_ammort = 0; //Сумма гашений
  var common_increase = 0; //Итоговый процентны прирост по усредненным показателям

 if($("body .smart_table.history_table").length>0){  //Если прогружена вкладка истории считаем амортизацию по ней
	  $("body .smart_table.history_table tr[data-active-id="+activeId+"]").each(function(indx, element)
	    {
	      console.log(activeId);
	      var hist_row = $(element);
	      var cnt = parseFloat(hist_row.attr('data-cnt'));
	      mid_buy_summ += parseFloat(hist_row.attr('data-price'))*cnt;
	      buy_summ += parseFloat(hist_row.attr('data-price-summ'));
	      cnt_summ += cnt;
	      now_summ += parseFloat(hist_row.attr('data-now-summ'));
	      mid_ammort += parseFloat(hist_row.attr('data-ammort_summ'))*cnt;
	    }
	  );

  console.log('now_summ='+now_summ+' amort '+activeId+': mid_ammort='+mid_ammort+' cnt='+cnt_summ);
  //Вычисление средневзвешенных значений
  mid_buy_summ = mid_buy_summ/cnt_summ;
  summ_ammort = mid_ammort;
  mid_ammort = mid_ammort/cnt_summ;
   console.log('calculateAmortizationIncrease', 'bondTrId='+bondTrId+' '+now_summ+'+'+summ_ammort+'/'+buy_summ);
   //common_increase = (((now_summ+summ_ammort)/buy_summ-1)*100);
  } else {  //Иначе берем готовые показатели из строки актива
	summ_ammort = parseFloat(activeStr.attr('data-ammort_summ'));
	now_summ = parseFloat(activeStr.find('.portfolio_elem_buy_price_current').attr('data-value'));
	buy_summ = parseFloat(activeStr.find('.portfolio_elem_buy_price_final').attr('data-value'));
  }
   common_increase = (((now_summ+summ_ammort)/buy_summ-1)*100);
  //Устанавливаем значения в активе
  var amortStr = activeStr.find('p.amort_bond_sum');
  if(amortStr.length) {
    amortStr.find('span').text(summ_ammort*-1).attr('data-original-title', 'По облигации погашено '+summ_ammort+'\u00A0руб.');
    //Вызвать ф-цию для пересчета и обновления данных по амортизации в главной строке по активу
  }

  //activeStr.find('.profit_main').text(common_increase+'%');

  //Возвращаем процент прироста если требуется
  if(returnIncrease!=undefined && returnIncrease==true) {
    // console.log('func_common_increase', common_increase);
    return common_increase;
  }
}


//Пересчитывает прирост для облигаий с амортизацией
function calculateAmortizationIncrease(bondTrId) {
  console.log('updateAmortizationDataInActive', bondTrId);
  common_increase = parseFloat(updateAmortizationDataInActive(bondTrId, true));
  return common_increase;
}

//Расчитывает для активов долю от общего портфеля и долю внутри типа активов и добавляет data-sharein-all и data-sharein-type
//Так же считает среднюю бету в разрезе типов активов
//вызов в обновлении подвала tableFootUpdate
function calculateSharesInPortfolio() {
  var allSumm = parseFloat($('.portfolio_elem_buy_price_curr_summ').attr('data-current_summ'));
  var allSharesSumm = 0;
  var midSharesBeta = 0;
  var allSharesUsaSumm = 0;
  var midSharesUsaBeta = 0;
  var allETFSumm = 0;
  var midETFBeta = 0;
  var allBondSumm = 0;
  var divider = 0;
  var currentShareOf = 0;
  $('.portfolio_valuation_table .pt_active tbody tr:not(.oblig_off)').each(function()
    {
      //sharein-all
      $(this).attr('data-sharein-all', ($(this).find('.portfolio_elem_buy_price_current').attr('data-value') * 100) / allSumm);

      //по акциям
      if($(this).attr('data-elem')=='share') {
        allSharesSumm += parseFloat($(this).find('.portfolio_elem_buy_price_current').attr('data-value'));
      }
      //по акциям США
      if($(this).attr('data-elem')=='share_usa') {
        allSharesUsaSumm += parseFloat($(this).find('.portfolio_elem_buy_price_current').attr('data-value'));
      }
      //по акциям США
      if($(this).attr('data-elem')=='share_etf') {
        allETFSumm += parseFloat($(this).find('.portfolio_elem_buy_price_current').attr('data-value'));
      }
      //по облигациям
      if($(this).attr('data-elem')=='bond') {
        allBondSumm += parseFloat($(this).find('.portfolio_elem_buy_price_current').attr('data-value'));
      }
    }
  );

  //Расставляем видовые доли для активов
  $('.portfolio_valuation_table .pt_active tbody tr:not(.oblig_off)').each(function()
    {
      //по акциям
      if($(this).attr('data-elem')=='share') {divider = allSharesSumm;}

      //по акциям США
      if($(this).attr('data-elem')=='share_usa') {divider = allSharesUsaSumm;}

      //по etf
      if($(this).attr('data-elem')=='share_etf') {divider = allETFSumm;}

      //по etf
      if($(this).attr('data-elem')=='bond') {divider = allBondSumm;}

      if(divider>0) {
        currentShareOf = parseFloat(($(this).find('.portfolio_elem_buy_price_current').attr('data-value') * 100) / divider);
        $(this).attr('data-sharein-type', currentShareOf);

        //средняя бета по акциям
        if($(this).attr('data-elem')=='share') {
          if($(this).attr('data-betta').length) //Не учитываем пустую бету
            midSharesBeta += parseFloat($(this).attr('data-betta'))*currentShareOf;
        }

        //средняя бета по акциям США
        if($(this).attr('data-elem')=='share_usa') {
          if($(this).attr('data-betta').length) //Не учитываем пустую бету
            midSharesUsaBeta += parseFloat($(this).attr('data-betta'))*currentShareOf;
        }

        //средняя бета по etf
        if($(this).attr('data-elem')=='share_etf') {
          if($(this).attr('data-betta').length) //Не учитываем пустую бету
            midETFBeta += parseFloat($(this).attr('data-betta'))*currentShareOf;
        }
      }
    }
  );

  midSharesBeta = midSharesBeta/100;
  midSharesUsaBeta = midSharesUsaBeta/100;
  midETFBeta = midETFBeta/100;
  //Показываем беты или скрываем если нулевые

  //Средняя бета по акциям РФ
  if($('.portfolio_valuation_table .pt_active tbody tr[data\\-elem=share]').length>0 && $('.mid_betta_p.shares').length>0) {
    var classSign = '';
    if(midSharesBeta<0) {
      classSign = 'line_red';
    }
    else {
      classSign = 'line_green';
    }
    $('.mid_betta_p.shares').removeClass('hidden').find('span.portfolio_betta').removeClass('line_red').removeClass('line_green').addClass(classSign).text(midSharesBeta.toFixed(2));
  }
  else {
    $('.mid_betta_p.shares').addClass('hidden').find('span.portfolio_betta').text('-');
  }

  //Средняя бета по акциям США
  if($('.portfolio_valuation_table .pt_active tbody tr[data\\-elem=share_usa]').length>0 && $('.mid_betta_p.shares_usa').length>0) {
    var classSign = '';
    if(midSharesUsaBeta<0) {
      classSign = 'line_red';
    }
    else {
      classSign = 'line_green';
    }
    $('.mid_betta_p.shares_usa').removeClass('hidden').find('span.portfolio_betta').removeClass('line_red').removeClass('line_green').addClass(classSign).text(midSharesUsaBeta.toFixed(2));
  }
  else {
    $('.mid_betta_p.shares_usa').addClass('hidden').find('span.portfolio_betta').text('-');
  }

  //Средняя бета по etf
  if($('.portfolio_valuation_table .pt_active tbody tr[data\\-elem=share_etf]').length>0 && $('.mid_betta_p.shares_etf').length>0) {
    var classSign = '';
    if(midETFBeta<0) {
      classSign = 'line_red';
    }
    else {
      classSign = 'line_green';
    }
    $('.mid_betta_p.shares_etf').removeClass('hidden').find('span.portfolio_betta').removeClass('line_red').removeClass('line_green').addClass(classSign).text(midETFBeta.toFixed(2));
  }
  else {
    $('.mid_betta_p.shares_etf').addClass('hidden').find('span.portfolio_betta').text('-');
  }

}


function tableFootUpdate(prognose_income_bond_chart) {
  var usa_actives_cnt = 0,
  all_actives_cnt = 0,
  not_money_actives_count = 0,
  price_final = 0,
  amort_summ = 0,
  price_curr = 0,
  price_filter_curr = 0,
  price_target = 0,
  actions_summ_curr = 0,
  actions_usa_summ_curr = 0,
  actions_etf_summ_curr = 0,
  actions_etf_valute_summ_curr = 0,
  oblig_summ_curr = 0,
  oblig_valute_summ_curr = 0,
  currency_summ = 0,
  valute_part_summ = 0,
  drawdown_summ = 0,
  summ_mult_on_days = 0,
  final_profit_summ = 0,
  prognose_income_bond = 0,
  prognose_income = 0,
  rolling_profit = 0,
  issuecapitalization = 0,
  activeFilterIsOn = false,
  p_e_portfolio = 0;

    if(!arguments.callee.caller) {
        callerName = "global";
        console.log('tableFootUpdate was called from global scope. prognose_income_bond_chart= ', prognose_income_bond_chart);
    } else {
        callerName = arguments.callee.caller.name;
        console.log('tableFootUpdate caller: ' + callerName + ' function. prognose_income_bond_chart= ', prognose_income_bond_chart);
    }

  //Вычисляем доли общие и доли видовые для активов
  calculateSharesInPortfolio();

  var footer_summ_row = $('.pt_active tfoot tr.footer_summ_data_row');

  all_actives_cnt = $('.portfolio_valuation_table .pt_active tbody tr').length;
  usa_actives_cnt = $('.portfolio_valuation_table .pt_active tbody tr.no_usa').length;

  if($('#actives_type_filter li.active').length>0){
	 activeFilterIsOn = true;
  }


  $('.portfolio_valuation_table .pt_active tbody tr:not(.oblig_off)').each(function()
    {
      //$('.portfolio_valuation_table .pt_active tbody tr').each(function() {
      var tr = $(this);
      final_profit_summ += parseFloat(tr.find('.profit_main').attr('data-profit_summ'));

		if(activeFilterIsOn){ //Если фильтр по активам включен - то не учитываем в текущей итоговой сумме кеш
			if(tr.attr('data-elem')!='currency'){
			  price_filter_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
			}
		} else {
			 price_filter_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
		}
		price_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
		prognose_income += parseFloat(tr.attr('data-prognose_income'));

      //price_final += parseFloat(tr.find('.portfolio_elem_buy_price_final').attr('data-value'));
      price_final = parseFloat(tr.attr('data-footer-income-summ'));
      if(tr.find('.portfolio_elem_buy_price_final').next('.amort_bond_sum').length>0) {
        amort_summ += parseFloat(tr.find('.portfolio_elem_buy_price_final').next('.amort_bond_sum').attr('data-delta'));
      }

      if(tr.attr('data-elem')=='share') {
        not_money_actives_count ++;
        actions_summ_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
        drawdown_summ += parseFloat(tr.attr('data-drawdown_summ')); //Просад
        issuecapitalization += parseFloat(tr.attr('data-issuecapitalization')); //Итоговая капа
        rolling_profit += parseFloat(tr.attr('data-rolling_profit')); //Итоговая прибыль
	//	console.log('rolling_profit '+tr.attr('data-id'), rolling_profit);
      }
      else if(tr.attr('data-elem')=='share_usa') {
        not_money_actives_count ++;
        actions_usa_summ_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
        valute_part_summ += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
        drawdown_summ += parseFloat(tr.attr('data-drawdown_summ')); //Просад
        issuecapitalization += parseFloat(tr.attr('data-issuecapitalization')); //Итоговая капа
        rolling_profit += parseFloat(tr.attr('data-rolling_profit')); //Итоговая прибыль
  //		console.log('rolling_profit '+tr.attr('data-id'), rolling_profit);
      }
      else if(tr.attr('data-elem')=='currency') {
        currency_summ += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
		  if(tr.attr('data-currency')!='rub'){
			  valute_part_summ += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
		  }
        //drawdown_summ += parseFloat(tr.attr('data-drawdown_summ')); //Просад
        //issuecapitalization += parseFloat(tr.attr('data-issuecapitalization')); //Итоговая капа
        //rolling_profit += parseFloat(tr.attr('data-rolling_profit')); //Итоговая прибыль
      }
      else if(tr.attr('data-elem')=='share_etf') {
        not_money_actives_count ++;
        actions_etf_summ_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
		  if(tr.attr('data-currency')!='rub'){
			  valute_part_summ += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
			  actions_etf_valute_summ_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
		  }
        drawdown_summ += parseFloat(tr.attr('data-drawdown_summ')); //Просад
        issuecapitalization += parseFloat(tr.attr('data-issuecapitalization')); //Итоговая капа

      }
      else if(tr.attr('data-elem')=='bond') {
        not_money_actives_count ++;
        oblig_summ_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
		  if(tr.attr('data-currency')!='rub'){
			  valute_part_summ += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
			  oblig_valute_summ_curr += parseFloat(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
		  }
        prognose_income_bond += parseFloat(tr.attr('data-prognose_income'));
	//	console.log('rolling_profit '+tr.attr('data-id'), rolling_profit);
      }
      price_target += parseFloat(tr.find('.target_price').attr('data-value'));

    }
  );

  if(prognose_income_bond_chart) {
    prognose_income_bond = prognose_income_bond_chart;
  }

  var price_final_txt = price_final.toFixed(2);
  price_final_txt = parseFloat(price_final_txt);
  $('.portfolio_elem_buy_price_final_summ').text(price_final_txt.toLocaleString()).attr('data-value', price_final_txt);
  footer_summ_row.attr('data-profit_summ', final_profit_summ.toFixed(1));
  var price_curr_txt = price_curr.toFixed(2);
  price_curr_txt = parseFloat(price_curr_txt);
  $('.portfolio_elem_buy_price_curr_summ').text(price_curr_txt.toLocaleString()).attr('data-current_summ', price_curr_txt);

  //Прирост за весь период c учетом амортизации (в подвале таблицы)

  var curr_profit = (((price_curr+amort_summ) / price_final - 1) * 100).toFixed(1);
  // console.log('curr_profit= ( ('+price_curr+'+'+amort_summ+')) / '+price_final+'-1)*100 = '+curr_profit);
  if(isNaN(curr_profit)) {
    curr_profit = '-';
    profit_cls = '';
  }
  else {
    var profit_prefix = '+';
    var profit_cls = 'line_green';
    if (curr_profit < 0) {
      profit_prefix = '';
      profit_cls = 'line_red';
    }
    else if (curr_profit == 0) {
      profit_prefix = '';
      profit_cls = '';
    }
    $('.profit_main_summ').attr('curr_profit', curr_profit);

/*	 console.log('infin', curr_profit);
	 console.log('infin isFinite', isFinite(curr_profit));*/
	 //isFinite - определяет, является ли аргумент конечным числом. Если аргумент является NaN, положительной или отрицательной бесконечностью, метод вернёт false; иначе возвращается true.
	 if(isFinite(curr_profit)==false){
	 	curr_profit = '-';
	 } else {
	      curr_profit = profit_prefix + curr_profit + '%';
	 }
  }

  $('.profit_main_summ').text(curr_profit);
  $('.profit_main_summ').removeClass('line_green').removeClass('line_red').addClass(profit_cls);


  $('.target_price_summ').removeClass('line_green');
  $('.target_price_summ').removeClass('line_red');
  var target_price_summ = ((price_target - price_final) / price_final) * 100;
  var target_price_summ_txt = target_price_summ.toFixed(2);
  target_price_summ_txt = parseFloat(target_price_summ_txt);
  if (target_price_summ < 0) {
    $('.target_price_summ').addClass('line_red');
    $('.target_price_summ').text(target_price_summ_txt.toLocaleString() + '%');
  }
  else {
    $('.target_price_summ').addClass('line_green');
    $('.target_price_summ').text('+' + target_price_summ_txt.toLocaleString() + '%');
  }

  //Дополнительные данные для расчетов показателей над таблицей активов:
  var summ_multi_on_days = 0;
  var final_summ_multi_on_days = 0;
  var final_profit = 0;
  var final_amort_summ = 0;
  $('.pt_active tbody tr').each(function()
    {
      var tr = $(this);
      summ_multi_on_days = tr.find('.portfolio_elem_buy_price_final').data('value')*tr.data('days_in_prtf')/$('.portfolio_elem_buy_price_final_summ').data('value');
      final_summ_multi_on_days += summ_multi_on_days;
      tr.attr('data-summ_multi_on_days', summ_multi_on_days);
      final_amort_summ += parseFloat(tr.find('.amort_bond_sum').attr('data-delta'));
    }
  );

  final_profit = parseFloat($('.portfolio_elem_buy_price_final_summ').data('value'))/footer_summ_row.data('profit_summ');

  footer_summ_row.attr('data-summ_multi_on_days', final_summ_multi_on_days.toFixed(0));
  footer_summ_row.attr('data-profit', final_profit);
  footer_summ_row.attr('data-prognose_income', prognose_income);
  if(prognose_income_bond_chart) {
    footer_summ_row.attr('data-prognose_income_bond', prognose_income_bond);
  }

  footer_summ_row.attr('data-drawdown_summ', drawdown_summ);


  // var all_profit_percent =(footer_summ_row.attr('data-profit_summ')/price_final_txt)*100;
  var all_profit_percent = ((footer_summ_row.find('.portfolio_elem_buy_price_curr_summ').attr('data-current_summ')+final_amort_summ)/price_final_txt - 1)*100;
  // console.log('all_profit_percent = (('+footer_summ_row.find('.portfolio_elem_buy_price_curr_summ').attr('data-current_summ')+'+'+final_amort_summ+')/'+price_final_txt+')*100 = '+all_profit_percent);
  var curr_profit = 0;
  curr_profit = parseFloat($('.profit_main_summ').attr('curr_profit'));
  footer_summ_row.attr('data-all_profit_percent', curr_profit.toFixed(2)+'%');

  //Прирост за весь период, в % годовых (в подвале таблицы)
  var profit_year_summ = ((curr_profit/footer_summ_row.attr('data-summ_multi_on_days'))*365).toFixed(2);

  if(isNaN(profit_year_summ)) {
    footer_summ_row.find('.profit_year_summ').removeClass('line_green').removeClass('line_red').text('-');
  }
  else {
    footer_summ_row.find('.profit_year_summ').removeClass('line_green').removeClass('line_red').addClass((profit_year_summ>0? 'line_green': 'line_red')).text('('+(profit_year_summ>0? '+': '')+profit_year_summ+'%)');
  }


  footer_summ_row.find('.portfolio_elem_active_counts_final_summ').text(not_money_actives_count);

  //Скрываем показатели в подвале, если в портфеле один актив США
  var hideTargetDrawdown = false;
  if(all_actives_cnt==usa_actives_cnt && usa_actives_cnt==1) {
    hideTargetDrawdown = true;
  }
  //Ожидаемая доходность портфеля
  if(activeFilterIsOn){
  	var portfolio_expected_return = ((footer_summ_row.data('prognose_income')/price_filter_curr)*100).toFixed(2);
  } else {
	 var portfolio_expected_return = ((footer_summ_row.data('prognose_income')/price_curr_txt)*100).toFixed(2);
  }

  if(isNaN(portfolio_expected_return) || hideTargetDrawdown) {
    portfolio_expected_return = '-';
  }
  else {
    portfolio_expected_return = portfolio_expected_return +'%';
    footer_summ_row.find('.prognose_income').addClass('line_green');
  }
  $('.portfolio_expected_return').text(portfolio_expected_return);
  footer_summ_row.find('.prognose_income').text(portfolio_expected_return);

   console.log('доходность портфеля формула: ('+footer_summ_row.data('prognose_income')+' / '+price_curr_txt+') * 100 =: '+portfolio_expected_return );
   console.log('доходность портфеля: prognose_income_bond: '+footer_summ_row.attr('data-prognose_income_bond')+' prognose_income: '+footer_summ_row.data('prognose_income')+' price_curr_txt: '+price_curr_txt );


  //Доля акций в портфеле
  var shares_part = ((actions_summ_curr/price_curr_txt)*100).toFixed(1);
  if(isNaN(shares_part)) {
    shares_part = '-';
  }
  else {
    shares_part = shares_part +'%';
  }
  $('.portfolio_shares_bonds').text(shares_part);

  //Доля акций США в портфеле
  if($('.portfolio_shares_usa').length) {
    var shares_usa_part = ((actions_usa_summ_curr/price_curr_txt)*100).toFixed(1);
    if(isNaN(shares_usa_part)) {
      shares_usa_part = '-';
    }
    else {
      shares_usa_part = shares_usa_part +'%';
    }
    $('.portfolio_shares_usa').text(shares_usa_part);
  }

  //Доля etf в портфеле
  if($('.portfolio_etf_shares_bonds').length) {
    var shares_etf_part = ((actions_etf_summ_curr/price_curr_txt)*100).toFixed(1);
    if(isNaN(shares_etf_part)) {
      shares_etf_part = '-';
    }
    else {
      shares_etf_part = shares_etf_part +'%';
    }
    $('.portfolio_etf_shares_bonds').text(shares_etf_part);
  }


  //Доля облигаций в портфеле
  var bonds_part = ((oblig_summ_curr/price_curr_txt)*100).toFixed(1);
  if(isNaN(bonds_part)) {
    bonds_part = '-';
  }
  else {
    bonds_part = bonds_part +'%';
  }
  $('.portfolio_deposits_bonds').text(bonds_part);

  //Доля кеша в портфеле
  var currency_part = ((currency_summ/price_curr_txt)*100).toFixed(1);
  if(isNaN(currency_part)) {
    currency_part = '-';
  }
  else {
    currency_part = currency_part +'%';
  }
  $('.portfolio_currencies_bonds').text(currency_part);

  //Общий риск портфеля
  //Потенциальный риск по акциям
  if(drawdown_summ>0) {
    actions_summ_curr = actions_summ_curr+actions_etf_summ_curr+actions_usa_summ_curr;
    var potential_drawdown = (drawdown_summ/actions_summ_curr)*-100;


    if(prognose_income_bond_chart) {
      var portfolio_common_squand = (((prognose_income_bond - drawdown_summ)/ price_curr_txt)*100).toFixed(1);
    }
    else {
      portfolio_common_squand = '';
    }
	 console.log('prognose_income_bond_chart ', prognose_income_bond_chart);
	 console.log('portfolio_common_squand calc', prognose_income_bond+' - '+drawdown_summ+' / '+price_curr_txt);
	 console.log('portfolio_common_squand', portfolio_common_squand);
	 console.log('oblig_summ_curr', oblig_summ_curr);
    if(oblig_summ_curr>0) {//Если в портфеле есть облигации и в фильтре выбрано "все" то показываем риск общий по портфелю
      potential_drawdown_footer = parseFloat(portfolio_common_squand);
    }
    else {
      potential_drawdown_footer = potential_drawdown;
    }

    $('.portfolio_max_squand').text(potential_drawdown.toFixed(2)+'%').addClass('line_red');
    footer_summ_row.find('.drawdown_income').text('('+(potential_drawdown_footer.toFixed(2))+'%)').addClass('line_red');

    if(hideTargetDrawdown) {
      footer_summ_row.find('.drawdown_income').text('-');
    }

    //console.log('Общий риск:', 'prognose_income_bond: '+prognose_income_bond+ ' drawdown_summ:'+drawdown_summ+' price_curr_txt: '+price_curr_txt);
    if(portfolio_common_squand == '') {
      $('.portfolio_common_squand').text('-').removeClass('line_green').removeClass('line_red');
    }
    else if(portfolio_common_squand>0) {
      $('.portfolio_common_squand').addClass('line_green').text(portfolio_common_squand+'%');
    }
    else {
      $('.portfolio_common_squand').addClass('line_red').text(portfolio_common_squand+'%');
    }
  }
  else {
    $('.portfolio_common_squand').text('-').removeClass('line_green').removeClass('line_red');
    $('.portfolio_max_squand').text('-').removeClass('line_green').removeClass('line_red');
    footer_summ_row.find('.drawdown_income').text('-').removeClass('line_green').removeClass('line_red');
  }


  //P/E для портфеляб если присутствуют акции
  if(actions_summ_curr>0 || actions_usa_summ_curr>0) {
     console.log('pe', 'inc='+issuecapitalization+' roll='+rolling_profit);
    p_e_portfolio = issuecapitalization/rolling_profit;

    if(!isNaN(p_e_portfolio)) {
      $('.portfolio_pe').text(p_e_portfolio.toFixed(1));
		if(p_e_portfolio<0){
			$('.portfolio_pe').removeClass('line_green').addClass('line_red');
		} else if(p_e_portfolio>0) {
      $('.portfolio_pe').removeClass('line_red').addClass('line_green');
	    }
	    else {
	      $('.portfolio_pe').removeClass('line_red').removeClass('line_green');
	    }
    }
    else {
      $('.portfolio_pe').text('-');
    }
  }
  else {
    $('.portfolio_pe').text('-');
  }

  //Записываем суммы портфеля в инфоблок
 // updateStatSumm(price_curr_txt, price_final_txt);

  /*   if ($('#portfolioHistoryChart').length != 0)
      historyGraphicRebuild();*/

}


/**
 * Обновляем кол-ва активов в фильтре по типам активов
 *
 */
function updateActiveTypeFilterCnt(){
	  var pid = $('#portfolios_select').val().replace("portfolio_", "");


		$.ajax({
		type: "POST",
		url: "/ajax/portfolio/portfolio.php",
		dataType: "json",
		data:{ajax: "y", action: 'getPortfolioActivesCnt', 'pid': pid},
		cashe: false,
		async: true,
		success: function(data){
			getResponse(data);
		}
		});

		 function getResponse(data){
	  		var actives_type_filter = $('.actives_type_filter');
	  		var share_cnt, share_etf_cnt, share_usa_cnt, bond_cnt;
	  		var share_obj, share_etf_obj, share_usa_obj, bond_obj;
		 	console.log('counts', data);
			share_cnt = Number(data.share);
			share_etf_cnt = Number(data.share_etf);
			share_usa_cnt = Number(data.share_usa);
			bond_cnt = Number(data.bond);

			if(isNaN(share_cnt)) share_cnt = 0;
			if(isNaN(share_etf_cnt)) share_etf_cnt = 0;
			if(isNaN(share_usa_cnt)) share_usa_cnt = 0;
			if(isNaN(bond_cnt)) bond_cnt = 0;


			share_obj = actives_type_filter.find('.share>span.cnt');
	  		share_obj.text(share_cnt);
			if(share_cnt===0){
				share_obj.parents('li').addClass('disabled');
			} else {
				share_obj.parents('li').removeClass('disabled');
			}

			share_etf_obj = actives_type_filter.find('.share_etf>span.cnt');
	  		share_etf_obj.text(share_etf_cnt);
			if(share_etf_cnt===0){
				share_etf_obj.parents('li').addClass('disabled');
			} else {
				share_etf_obj.parents('li').removeClass('disabled');
			}

			share_usa_obj = actives_type_filter.find('.share_usa>span.cnt');
	  		share_usa_obj.text(share_usa_cnt);
			if(share_usa_cnt===0){
				share_usa_obj.parents('li').addClass('disabled');
			} else {
				share_usa_obj.parents('li').removeClass('disabled');
			}

			bond_obj = actives_type_filter.find('.bond>span.cnt');
	  		bond_obj.text(bond_cnt);
			if(bond_cnt===0){
				bond_obj.parents('li').addClass('disabled');
			} else {
				bond_obj.parents('li').removeClass('disabled');
			}

		 }



}

/*var val_portfolio_select = $('#portfolios_select').val();
if($('#linear_chart_second').length>0 && val_portfolio_select!='/portfolio/') {
  backtestGraphicRebuild();
  if ($('#portfolioHistoryChart').length != 0)
    historyGraphicRebuild();
}*/

function backtestGraphicRebuild() {
  if($('.hidden_port_startpage').length>0) return false;
  var MONTHS =['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  var selectedBenchmark = $('#portfolios_benchmark').val();
  var selectedBenchmarkName = $('#portfolios_benchmark :selected').text();
  var selectedBenchmarkType = $('#portfolios_benchmark :selected').attr('data-type');
  var selectedBenchmarkCurrency = $('#portfolios_benchmark :selected').attr('data-currency');
  var selectedPortfolioBenchmark = $('#portfolios_benchmark_selfie').val();
  var selectedPortfolioBenchmarkName = 'Портфель: '+$('#portfolios_benchmark_selfie :selected').text();

  var arr = {};


  $.ajax(
    {
    type: "POST",
    url: "/ajax/portfolio/portfolio.php",
    dataType: "json",
    data: {
    		  'ajax': "y",
	 		  'action': 'getBackTestData',
			  'portfolioId': $('#portfolios_select').val().replace("portfolio_", ""),
			  'benchmark': selectedBenchmark,
			  'benchmarkType': selectedBenchmarkType,
			  'benchmarkCurrency': selectedBenchmarkCurrency,
			  'protfolio_benchmark': selectedPortfolioBenchmark
			  },
    cashe: false,
    async: false,
    success: function(data) {
        getResponse(data);
      }
    }
  );

  function getResponse(data) {
    arr = (data);
  }

  //  console.log('backtest:', arr);

  var chartDates =[];
  var chartBlueSum =[];
  var chartRedSum =[];
  var chartGreenSum =[];

 if(!!arr.ERRORS){
	Object.keys(arr.ERRORS).forEach(function(key)
	    {

	     $('.backtest_errors_list').append(key+' '+this[key]+'<br>');

	    }
	    , arr.ERRORS);
 }

 if(!!arr.DATA)
  Object.keys(arr.DATA).forEach(function(key)
    {
      chartDates.push(key);
      chartBlueSum.push(this[key]['SUM']);
      if(selectedBenchmark.length) {
        if(selectedBenchmarkType=='index') {
          chartRedSum.push(this[key]['INDEXES'][selectedBenchmark]["SUM"]);
        }
        else if(selectedBenchmarkType=='etf')
        {
          chartRedSum.push(this[key]['ETF'][selectedBenchmark]["SUM"]);
        }
        else if(selectedBenchmarkType=='currency')
        {
          chartRedSum.push(this[key]['CURRENCY'][selectedBenchmark]["SUM"]);
        }
      }
      if(selectedPortfolioBenchmark.length) {
        chartGreenSum.push(this[key]['BNCH_SUM']);
      }

    }
    , arr.DATA);


  var config = {
  type: 'line',
  data: {
    labels: chartDates,
    datasets:[
        {
        label: 'Стоимость портфеля акций',
        backgroundColor: '#0064ff',
        borderColor: '#0064ff',
        data: chartBlueSum,
        fill: false,
        }
        ,
        //Массив по индексу добавляется ниже
      ]
    }
    ,
  options: {
    responsive: true,
    tooltips: {
      mode: 'index',
      intersect: false,
      }
      ,
    hover: {
      mode: 'nearest',
      intersect: true
      }
      ,
    scales: {
      xAxes:[
          {
          display: true
          }
        ],
      yAxes:[
          {
          display: true
          }
        ]
      }
      ,
    plugins: {
        // Change options for ALL labels of THIS CHART
      datalabels: false
      }
    }
  }

  //Добавляем наборы данных если выбран бенчмарк по индексу
  if(selectedBenchmark.length) {
    var SecondDataset = {label: selectedBenchmarkName,
    fill: false,
    backgroundColor: '#ea0000',
    borderColor: '#ea0000',
    data: chartRedSum,}
    config['data']['datasets'].push(SecondDataset);
  }

  //Добавляем наборы данных если выбран бенчмарк по портфелю
  if(selectedPortfolioBenchmark.length) {
    var ThirdDataset = {label: selectedPortfolioBenchmarkName,
    fill: false,
    backgroundColor: '#009900',
    borderColor: '#009900',
    data: chartGreenSum,}
    config['data']['datasets'].push(ThirdDataset);
  }

  var ctx = document.getElementById('linear_chart_second').getContext('2d');
  if(window.myLine) {//Если обновляем при перевыборе потрфеля, то убиваем существующий объект, иначе при наведении мышкой по hover всплывает старый график..
    window.myLine.destroy();
  }
  window.myLine = new Chart(ctx, config);


  /*  window.onload = function() {
   var ctx = document.getElementById('linear_chart_second').getContext('2d');
   window.myLine = new Chart(ctx, config);
  }*/
}


$('.popup_add_row_save').click(function()
  {
    // var number = $('#')
    if ($('#popup_add_row .search-form__input').val() == '' || $('#popup_add_row_price').val() == '' || $('#popup_add_row_number').val() == '' || $('#popup_add_row_date').val() == '') {
      if ($('#popup_add_row .error_mess_empty').length == 0) {
        $(this).before('<p class="error_mess_empty red">Одно или несколько полей не заполнено</span>');
      }
    }
    else {
      $('.error_mess_empty').remove();

      $('#popup_change_row').modal('hide');

      radarRecount();
    }
  }
);

function dateParseString(string, separator) {

  var arr = string.split(separator)
  return new Date(parseInt(arr[2]), parseInt(arr[1] - 1), parseInt(arr[0]));
}

function dateParseToString(date) {
  var day = date.getDay();
  var month = date.getMonth() + 1;
  var year = date.getFullYear();

  if (day.length < 2) {
    day = '0' + day;
  }

  if (month.length < 2) {
    month = '0' + month;
  }

  return day + '.' + month + '.' + year;
}

var data1, data2, data3, data4;
var historyChart, currentLabels, datepickerFrom, datepickerTo;
var dateFromSavedVal, dateToSavedVal;

function getHistoryTableFilterFromHTML(clearFilter ) {
  var arFilter =[];
    if(!arguments.callee.caller) {
        callerName = "global";
        console.log('getHistoryTableFilterFromHTML was called from global scope. clearFilter= ', clearFilter);
    } else {
        callerName = arguments.callee.caller.name;
        console.log('getHistoryTableFilterFromHTML caller: ' + callerName + ' function. clearFilter= ', clearFilter);
    }
  if(clearFilter==true) return arFilter;

  //Дочерние сделки
  if($('#phtf_Table_showAutoOperation').prop('checked')) {
    arFilter.push({'showChildDeals': 'Y'}
    );
  }
  else {
    arFilter.push({'showChildDeals': 'N'}
    );
  }

  //Фильтр по активу
/*  if($('#phtf_active').length>0) {
    var active = '';
    var active = $('#phtf_active').val().length>0? $('#phtf_active').val(): '';
    var activeCode = $('#phtf_active').val().length>0? $('#phtf_active option:selected').attr('data-isin-code'): '';
    arFilter.push({'active': active},{'activeCode': activeCode});
  }*/

  //Движения
  if ($('#phtf_historyActions').length>0) {
    var histAction = $('#phtf_historyActions').val().length>0? $('#phtf_historyActions').val(): '';
    arFilter.push({'histAction': histAction});
  }

  //Диапазон дат
  if($('body').find('#phtf_date_from').length>0 && $('body').find('#phtf_date_to').length>0) {
    // var phtf_date_from = $('body').find('#phtf_date_from').val().length>0? $('body').find('#phtf_date_from').val(): '';
    // var phtf_date_to = $('body').find('#phtf_date_to').val().length>0? $('body').find('#phtf_date_to').val(): '';
    var phtf_date_from, phtf_date_to;

    if (!!dateFromSavedVal) {
      phtf_date_from = dateFromSavedVal;
    } else {
      phtf_date_from = $('body').find('#phtf_date_from').val().length>0? $('body').find('#phtf_date_from').val(): '';
    }

    if (!!dateToSavedVal) {
      phtf_date_to = dateToSavedVal;
    } else {
      phtf_date_to = $('body').find('#phtf_date_to').val().length>0? $('body').find('#phtf_date_to').val(): '';
    }
    arFilter.push({'date_from': phtf_date_from});
    arFilter.push({'date_to': phtf_date_to});
  }
/*	else {
	 var nowDate = new Date();
	 phtf_date_to = ('0'+nowDate.getDate()).slice(-2)  + '.' + ('0'+(nowDate.getMonth() + 1)).slice(-2)  + '.' + nowDate.getFullYear();
	 arFilter.push({'date_from': $('#portfolio_start_date').val()});
	 arFilter.push({'date_to': phtf_date_to});
	}*/




  return arFilter;
}

function historyGraphicRebuild(clearFilter) {
  // console.log('!!!!REBUILD')
  overlayShow();


  setTimeout(function() {
    var historyCTX;
    var chartData;

    data1 =[];
    data2 =[];
    data3 =[];
    data4 =[];

    var currentLabels = [];

    $('#portfolioHistoryChartOuter').addClass('showBgImage');
    // if(window.historyChart) {//Если обновляем при перевыборе потрфеля, то убиваем существующий объект, иначе при наведении мышкой по hover всплывает старый график..
    //     window.historyChart.destroy();
    // }
    if($('body').find('#portfolioHistoryChart').hasClass('builded')) {//Если обновляем при перевыборе потрфеля, то убиваем существующий объект, иначе при наведении мышкой по hover всплывает старый график..
      $('body').find('#portfolioHistoryChart').remove();
      $('#portfolioHistoryChartOuter').html('<div class="historyChart-bg"><img src="/local/templates/new/img/null_graph.png"></div><canvas id="portfolioHistoryChart"></canvas>');

    }

    var arrHistoryData =[];
    var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");

    // var showChildDeals = $('#phtf_Table_showAutoOperation').prop('checked')=='Y'?'Y':'N';

    var filter = getHistoryTableFilterFromHTML(clearFilter);
	 console.log('chart update filter 1', filter);
		console.log('historyGraphicRebuild -> ajax getPortfolioHistory');
    $.ajax(
        {
          type: "POST",
          url: "/ajax/portfolio/portfolio.php",
          dataType: "json",
          data: {ajax: "y", 'clearCache':'Y', 'view':'forHistGraph', action: 'getPortfolioHistory', 'portfolioId': portfolioId, 'filter': filter}
          ,
          cashe: false,
          async: false,
          success: function(data) {
            getResponse(data, filter);
            overlayHide();
          },
          error: function() {
            overlayHide();
          }
        }
    );
  }, 10);

  function getResponse(data, filter) {

    var events = {};
    var showOrangeYellowGraphs = ($('#phtf_active').length && $('#phtf_active').val().length)? false: true; //Если выбран актив для фильтрации - не показываем желтый и оранжевый графики
    arrHistoryData = data;
    if (arrHistoryData["result"]=="success") {

		    events = arrHistoryData["data"]["currentLabels"];
			 currentLabels = arrHistoryData["data"]["currentLabels"];
			 events = arrHistoryData["data"]["EVENTS"];
			 data1 = arrHistoryData["data"]["ALL_DEALS_SUMM"];
			 data2 = arrHistoryData["data"]["ALL_CHISTORY_SUMM"];
			 if(showOrangeYellowGraphs) {
				 data3 = arrHistoryData["data"]["ALL_INCOMES_SUMM"];
				 data4 = arrHistoryData["data"]["ALL_CACHE_SUMM"];
			 }

      chartData = {
      labels: currentLabels,
      datasets:[
          {
          type: 'line',
          label: 'Текущий капитал',
          borderColor: '#008940',
          borderWidth: 2,
          fill: false,
          lineTension: 0,
          data: data1,
          datalabels: {display: false}
          }
          ,
/*          {
          type: 'line',
          label: 'Дивиденды и купоны',
          borderColor: '#0033CC',
          borderWidth: 2,
          fill: false,
          lineTension: 0,
          data: data2,
          datalabels: {
            display: false
            }
          }*/
        ]
      }

      if(showOrangeYellowGraphs) {
        chartData.datasets.push(
          {
          type: 'line',
          label: 'Вложенный капитал',
          borderColor: '#FF9900',
          backgroundColor: '#FF9900', //#ffba00
          borderWidth: 2,
          fill: true,
          lineTension: 0,
          data: data3,
          datalabels: {
            display: function(context) {
            	var eventKey = context.chart.boxes[2].ticks[context.dataIndex];
                return events[eventKey].length > 0;
              }
              ,
            borderRadius: 5,
            backgroundColor: '#018940',
            padding: {
              top: 2,
              bottom: 2
              }
              ,
            font: {
              weight: 'bold'
              }
              ,
            color: '#ffffff',
            formatter: function(value, context) {
						let eventKey = context.chart.boxes[2].ticks[context.dataIndex];
                  return `${events[eventKey].length}`;
              }
            }
          }
        );
        chartData.datasets.push(
          {
          type: 'line',
          label: 'Денежные средства',
          borderColor: '#ffba00',
          backgroundColor: '#ffba00', //#ffba00
          borderWidth: 2,
          fill: true,
          lineTension: 0,
          data: data4,
          datalabels: {
            display: false
            }
          }
        );
      }

		 $('#portfolioHistoryChartOuter').removeClass('showBgImage');
      historyCTX = document.querySelector('#portfolioHistoryChart');
      historyChart = new Chart(historyCTX,
        {
        type: 'line',
        data: chartData,
        options: {
          responsive: true,
          title: {
            display: false
            },
          tooltips: {
            enabled: false,
            mode: 'index',
            position: 'nearest',
            custom: function(tooltipModel) {
              // Tooltip Element
              var tooltipEl = document.querySelector('#chartjs-tooltip');

              // Create element on first render
              if (tooltipEl) {
                tooltipEl.remove();
              }

              tooltipEl = document.createElement('div');
              tooltipEl.id = 'chartjs-tooltip';
              tooltipEl.innerHTML = '<table></table>';
              document.body.appendChild(tooltipEl);


              // Hide if no tooltip
              if (tooltipModel.opacity === 0) {
                tooltipEl.style.opacity = 0;
                return;
              }

              // Set caret Position
              tooltipEl.classList.remove('above', 'below', 'no-transform');
              if (tooltipModel.yAlign) {
                tooltipEl.classList.add(tooltipModel.yAlign);
              } else {
                tooltipEl.classList.add('no-transform');
              }

              function getBody(bodyItem) {
                return bodyItem.lines;
              }

              // Set Text
              if (tooltipModel.body) {
                var titleLines = tooltipModel.title || [];
                var bodyLines = tooltipModel.body.map(getBody);

                var innerHtml = '<thead>';

                titleLines.forEach(function(title) {
                  innerHtml += '<tr><th>' + title + '</th></tr>';
                });
                innerHtml += '</thead><tbody>';

                bodyLines.forEach(function(body, i) {
                  var colors = tooltipModel.labelColors[i];
                  var style = 'background:' + colors.borderColor;
                  var span = '<span style="' + style + '"></span>';
                  innerHtml += '<tr><td><div class="elem">' + span + body + '</div></td></tr>';
                });
                innerHtml += '</tbody>';

                var tableRoot = tooltipEl.querySelector('table');
                tableRoot.innerHTML = innerHtml;

                if (tooltipModel.afterBody.length > 0) {
                  var tooltipEvents = document.createElement('div');
                  tooltipEvents.classList.add = 'tooltioEventsList';

                  var tooltipSubtitle = document.createElement('h2');
                  tooltipSubtitle.innerText = 'События за предыдущий месяц:';

                  var tooltipEventsUl = document.createElement('ul');
                  if (tooltipModel.afterBody.length >= 10) {
                    tooltipEventsUl.classList.add('twoCols');
                  }

                  var index = tooltipModel.title[0];
                  if (events[index].length > 0) {
                    for (var i = 0; i < tooltipModel.afterBody.length; i++) {
                      tooltipEvents.innerHTML += `<li>${tooltipModel.afterBody[i]}</li>`
                    }
                  }

                  var tooltipWrapper = document.createElement('div');
                  tooltipWrapper.classList.add('chartjs-tooltip__inner');

                  tooltipEventsUl.appendChild(tooltipEvents);

                  tooltipWrapper.appendChild(tooltipSubtitle);
                  tooltipWrapper.appendChild(tooltipEventsUl);

                  tooltipEl.appendChild(tooltipWrapper);
                }
              }

              // `this` will be the overall tooltip
              var position = this._chart.canvas.getBoundingClientRect();

              // Display, position, and set styles for font
              tooltipEl.style.opacity = 1;
              tooltipEl.style.position = 'absolute';
              tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
              tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
              tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
              tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
              tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
              tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
              tooltipEl.style.pointerEvents = 'none';
            },

            callbacks: {
              afterBody: function (t, d) {
                  // var eventsArr =[' ', 'События за предыдущий месяц:', ' '];
                  var eventsArr = [];
                  var index = t[0].label;
                  if (events[index].length > 0) {
                    for (var i = 0; i < events[index].length; i++) {
                      eventsArr.push(`${i+1}: ${events[index][i]}`)
                    }
                    return eventsArr;
                  }
                  else {
                    return '';
                  }
                }
              }
            }
          }
        }
      );

      if (currentLabels.length > 0) {
        initHistoryDatepickers();
      }

      $('#portfolioHistoryChart').addClass('builded');

      var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
      //var arFilter = new Map();
		//arFilter = getHistoryTableFilterFromHTML();
		console.log('chart update filter 2', filter);
		historyFilterUpdate(portfolioId, filter);
      //historyTableUpdate(portfolioId, filter);

      // overlayShow();

    }
  }


  //console.log(arrHistoryData["data"]);
}

function onDateChange() {
  overlayShow();

  setTimeout(function() {
    var dateFrom = datepickerFrom.datepicker('getDate');
    var dateTo = datepickerTo.datepicker('getDate');
    var newDates =[];
    var newEvents =[];
    var newData1 =[], newData2 =[], newData3 =[], newData4 =[];
    var arFilter = new Map();
    var portfolioId = $('#portfolios_select').val().replace("portfolio_", "");
    for (var i = 0; i < currentLabels.length; i++) {
      var date = dateParseString(currentLabels[i], '.');

      if (date >= dateFrom && date <= dateTo) {
        newDates.push(currentLabels[i]);
        newEvents.push(currentLabels[i]);

        newData1.push(data1[i]);
        newData2.push(data2[i]);
        newData3.push(data3[i]);
        newData4.push(data4[i]);
      }
    }

    historyChart.data.labels = newDates;

    for (var i = 0; i < historyChart.data.datasets.length; i++) {
      if (historyChart.data.datasets[i].label == 'Текущий капитал') {
        historyChart.data.datasets[i].data = newData1;
      }
      else if (historyChart.data.datasets[i].label == 'Дивиденды и купоны') {
        historyChart.data.datasets[i].data = newData2;
      }
      else if (historyChart.data.datasets[i].label == 'Вложенный капитал') {
        historyChart.data.datasets[i].data = newData3;
      }
      else if (historyChart.data.datasets[i].label == 'Денежные средства') {
        historyChart.data.datasets[i].data = newData4;
      }
    }

    historyChart.update();
    arFilter = getHistoryTableFilterFromHTML();
    historyTableUpdate(portfolioId, arFilter);

    overlayHide();
  });
}

function initHistoryDatepickers() {
  $('#phtf_date_from').datepicker('destroy');
  $('#phtf_date_to').datepicker('destroy');

  var dateFromInput = document.querySelector('#phtf_date_from');
  var dateToInput = document.querySelector('#phtf_date_to');

  if (!!dateFromSavedVal) {
    console.log(dateFromSavedVal);
    dateFromInput.value = dateFromSavedVal;
  }
  if (!!dateToSavedVal) {
    dateToInput.value = dateToSavedVal;
  }

 // console.log('currentLabels', currentLabels);
  var startDate = dateParseString(currentLabels[0], '.');
  var endDate = dateParseString(currentLabels[currentLabels.length - 1], '.');

  datepickerFrom = $('#phtf_date_from').datepicker(
      {
        autoclose: true,
        language: 'ru',
        startDate: startDate,
        endDate: endDate
      }
  );
  datepickerTo = $('#phtf_date_to').datepicker(
      {
        autoclose: true,
        language: 'ru',
        startDate: startDate,
        endDate: endDate
      }
  );

  datepickerFrom.datepicker('update', dateFromSavedVal ? dateParseString(dateFromSavedVal, '.') : startDate)
      .on('changeDate', function(e)
          {
            dateFromSavedVal = dateFromInput.value;
            onDateChange(historyChart);
          }
      );
  datepickerTo.datepicker('update', dateToSavedVal ? dateParseString(dateToSavedVal, '.') : endDate)
      .on('changeDate', function(e)
          {
            dateToSavedVal = dateToInput.value;
            onDateChange(historyChart);
          }
      );
}

function historyTableUpdate(portfolioId, filter =[]) {
    overlayShow();

    setTimeout(function () {
      $.ajax({
        type: "POST",
        url: "/portfolio/tables/table_history.php",
        dataType: "html",
        data: {ajax: "y", 'ELEMENT_ID': portfolioId, 'filter': filter},
        cashe: false,
        async: true,
        success: function (data) {
          if ($('.tab-pane.history_tab .portfolioHistoryTable').length > 0) {
            $('.tab-pane.history_tab .portfolioHistoryTable').detach();
          }
          $('.tab-pane.history_tab').append(data);
          overlayHide();
        },
        error: function() {
          overlayHide();
        }
      });
    }, 100);
}

function analyseTabUpdate(portfolioId){
    overlayShow();

    setTimeout(function () {
      $.ajax({
        type: "POST",
        url: "/portfolio/tables/table_analyse.php",
        dataType: "html",
        data: {ajax: "y", 'ELEMENT_ID': portfolioId},
        cashe: false,
        async: true,
        success: function (data) {
          if ($('.tab-pane.analyse_tab .portfolioAnalyseTable').length > 0) {
            $('.tab-pane.analyse_tab .portfolioAnalyseTable').detach();
          }
          $('.tab-pane.analyse_tab').append(data);
			 analysePortfolio();
          overlayHide();
        },
        error: function() {
          overlayHide();
        }
      });
    }, 100);
}

function eventsCacheTableUpdate(portfolioId, filter =[]) {
  //   overlayShow();

  $.ajax(
    {
    type: "POST",
    url: "/portfolio/tables/events_cache.php",
    dataType: "html",
    data: {ajax: "y", 'ELEMENT_ID': portfolioId, 'filter': filter}
      ,
    cashe: false,
    async: false,
    success: function(data) {
        if($('.tab-pane.mycache_tab .portfolioEventsTable').length>0)
          $('.tab-pane.mycache_tab .portfolioEventsTable').detach();
        $('.tab-pane.mycache_tab').append(data);
        overlayHide();
      },
      error: function() {
        overlayHide();
      }
    }
  );
  //  overlayHide();
}

/**
 * Обновляет html блок с фильтром над таблицей истории
 *
 * @param  [add type]   portfolioId - id портфеля
 * @param  [add type]   filter = пока не используется
 *
 */
function historyFilterUpdate(portfolioId, filter =[], initSelects=true) {
  //   overlayShow();

  $.ajax(
    {
    type: "POST",
    url: "/portfolio/filters/table_history_filter.php",
    dataType: "html",
    data: {ajax: "y", 'ELEMENT_ID': portfolioId, 'filter': filter},
    cashe: false,
    async: false,
    success: function(data) {
        if($('.tab-pane.history_tab .portfolioHistoryTableFilter').length>0)
          $('.tab-pane.history_tab .portfolioHistoryTableFilter').html(data);
			   initHistoryFilterSelects(initSelects);
            initHistoryDatepickers();
        overlayHide();
      },
      error: function () {
        overlayHide();
      }
    }
  );
  //  overlayHide();
}

$('.portfolio_tabs a').on('shown.bs.tab', function (e) {
  setFirstLoadFalse();
});

$('.portfolio_tabs a[data-targ="history_tab"]').on('shown.bs.tab', function (e) {
  initHistoryFilterSelects(true);
});

/**
 * Инициализирует select2 для select в фильтре таблицы истории (запускается при перезагрузке html фильтра, при перевыборе портфеля)
 *
 * @return [add type]  [add description]
 */
function initHistoryFilterSelects(initSelects) {
  var isFirsLoad = $('.portfolio_tabs a[data-targ="history_tab"]').attr('data-first-load');

  console.log('!!!!!', isFirsLoad)
  function innerSelectInit() {
    $('.portfolioHistoryTableFilter select.midval').each(function() {
      if ($(this).siblings('.select2').length === 0) {
        var ths = $(this);

        if ($(this).closest('.tma').length == 0) {
          var data_placeholder = $(this).data('placeholder');
          var cls = $(this).attr('class');

          if (!$(this).hasClass('midval')) {
            $(this).select2({
              minimumResultsForSearch: Infinity
            });
          } else {
            $(this).select2({
              maximumInputLength: 10
            });
          }
          // if ($(this).attr('name') == 'quality_bonds')

          $(this).siblings('.select2').addClass(cls);
          $('.portfolioHistoryTableFilter select.midval').off('select2:open');
          $('.portfolioHistoryTableFilter select.midval').on('select2:open', function (evt) {
            ths.siblings('.select2-container').addClass(cls + '_dropdown');
          });
        }
      }
    });
  }

  if ($('.portfolioHistoryTableFilter select.midval').length != 0 && isFirsLoad === 'false') {
    console.log('!!!!! 2', isFirsLoad)
    innerSelectInit();
  }

  setFirstLoadFalse();
  setTimeout(function() {
    if ($('.portfolioHistoryTableFilter').find('.select2').length === 0) {
      console.log('!!!!!!!!!! finish')
      innerSelectInit();
    }
  }, 3000);
}

$('body').on('click', '.flt_update', function() {
	var val = $('#portfolios_select').val();
	var portfolioId =  val.replace("portfolio_", "");
	var filter = getHistoryTableFilterFromHTML();
 		 historyFilterUpdate(portfolioId, filter);
		 console.log('historyFilterUpdate -> click .flt_update');
}
);

function setFirstLoadFalse() {
  $('.portfolio_tabs a[data-targ="history_tab"]').attr('data-first-load', false);
}
// График в истории портфеля
/*var val_portfolio_select = $('#portfolios_select').val();
if ($('#portfolioHistoryChart').length != 0  && val_portfolio_select!='/portfolio/') {
 historyGraphicRebuild();
}*/

/* включение/отключение поля ввода курса валюты для внесения средств */

$('#popup_add_cache_row').on('shown.bs.modal', function() {
  if ($('#popup_add_cache_selector').val() == 'RUB') {
    hideCashCurrencyField();
  } else {
    showCashCurrencyField();
  }
});

$('#popup_add_cache_selector').on('change', function(e) {
  if ($('#popup_add_cache_selector').val() == 'RUB') {
    hideCashCurrencyField();
  } else {
    showCashCurrencyField();
  }
});

function hideCashCurrencyField() {
  // $('#popup_insert_row_price_one_outer').style('display', 'none');
  $('#popup_insert_row_price_one').prop('disabled', true);
}

function showCashCurrencyField() {
  // $('#popup_insert_row_price_one_outer').style('display', null);
  $('#popup_insert_row_price_one').prop('disabled', false);
}

/* включение/отключение поля ввода курса валюты для дивидендов, купонов и т.д. */

$('#popup_cashflow_cache_selector').on('change', function(e) {
  console.log('123123', $('#popup_cashflow_cache_selector').val())
  if ($('#popup_cashflow_cache_selector').val() == 'RUB') {
    hideCashflowCurrencyField();
  } else {
    showCashflowCurrencyField();
  }
});

function onShowCashflowModal() {
  if ($('#popup_cashflow_cache_selector').val() == 'RUB') {
    hideCashflowCurrencyField();
  } else {
    showCashflowCurrencyField();
  }
}

function hideCashflowCurrencyField() {
  // $('#popup_cashflow_row_price_one_outer').style('display', 'none');
  $('#popup_cashflow_row_price_one').prop('disabled', true);
}

function showCashflowCurrencyField() {
  // $('#popup_cashflow_row_price_one_outer').style('display', null);
  $('#popup_cashflow_row_price_one').prop('disabled', false);
}
