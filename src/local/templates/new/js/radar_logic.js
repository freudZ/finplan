// -----------------------------
// --- КУКИ И ДОП. МЕТОДЫ ------
// -----------------------------

//цвета для кругового графика
var colors_big_arr = [];
if ($.cookie('colors_cookie')) {
  colors_big_arr = JSON.parse($.cookie('colors_cookie'));
} else {
  for (var i = 0; i < 200; i++) {
    var curr_donut_color_letter = '0123456789ABCDEF'.split('');
    var curr_donut_color = '#';
    for (var j = 0; j < 6; j++ ) {
      curr_donut_color += curr_donut_color_letter[Math.floor(Math.random() * 16)];
    }
    colors_big_arr.push(curr_donut_color);
  }
  $.cookie('colors_cookie', JSON.stringify(colors_big_arr), {expires: 1, path: '/'});
}
// console.log('Массив цветов: ', colors_big_arr);
//-------

// проверка наличия ИИС на странице и включена ли галка
if ($('#iis').length != 0 && $('#iis').prop('checked')) {
  $.cookie('iis_cookie', 1, {expires: 1, path: '/'});
} else {
  $.cookie('iis_cookie', 0, {expires: 1, path: '/'});
}
//-------

// -----------------------------
// --- ОСНОВНАЯ ФУНКЦИЯ --------
// -----------------------------

$(window).on('load', function() {
  radarRecount();
});

function radarRecount() {
  var dates_sell_arr = []; // массив дат гашения ценных бумаг
  var dates_buy_arr = []; // массив дат покупки ценных бумаг
  var max_date; // самая поздняя дата в массиве дат гашения бумаг
  var dates_arr = []; // массив дат периодом в месяц

  var user_money = 0; // средства пользователя
  var buy_price_full = 0; // сумма цен покупки всех ценных бумаг
  var cur_price_full = 0; // текущая сумма цен всех ценных бумаг
  var buy_shares_price_full = 0; // сумма цен покупки всех акций
  var buy_bonds_price_full = 0; // сумма цен покупки всех облигаций
  var chart_donut_arr = []; // массив данных для кругового графика

  var rows_obj = {} // объект из данных по каждой строке

  // перебираем все выбранные ценные бумаги в таблице
  var i = 0;
  $('.radar_table.pt_active tbody tr:not(.oblig_off)').each(function(indx, element){
  var tr = $(element);
  //for (var i = 0; i < $('.radar_table.pt_active tbody tr').length; i++) {
  //  var tr = $('.radar_table.pt_active tbody tr').eq(i);
	// if(tr.attr('data-oblig-off')=='Y'){ continue;}
	 //console.log('tr count: '+$('.radar_table.pt_active tbody tr').length);
    // запускаем сбор и подсчет инфы по каждой бумаге
    var tr_data = tableRecount(tr);
     //console.log('123', tr_data)

    // суммируем цены покупки всех ценных бумаг (при условии отсутствия данных по средствам пользователя, пример - страница портфеля)
    if (user_money == 0) {
      buy_price_full += tr_data.price_buy;
     // cur_price_full += tr.find('.portfolio_elem_buy_price_current').data('value');
      cur_price_full += Number(tr.find('.portfolio_elem_buy_price_current').attr('data-value'));
      //console.log(Number(tr.find('.portfolio_elem_buy_price_current').attr('data-value')));
		if(tr.attr('data-elem')=='share' || tr.attr('data-elem')=='share_etf' || tr.attr('data-elem')=='share_usa'){
		  buy_shares_price_full +=	tr.find('.portfolio_elem_buy_price_current').attr('data-value');
		} else {
		 buy_bonds_price_full  += tr.find('.portfolio_elem_buy_price_current').attr('data-value');
 		}
    }

    // добавляем в массив дат гашения дату текущей бумаги из цикла
    dates_sell_arr.push(tr_data.date_sell);

    // добавляем в массив дат гашения дату текущей бумаги из цикла
    dates_buy_arr.push(tr_data.date_buy);

    // формируем общий объект с данными по всем строкам
    rows_obj[i] = tr_data;
	 i++;
  });
  // console.log('Массив дат гашения: ', dates_sell_arr);

  max_date = dateMax(dates_sell_arr);
  // console.log('Максимальная дата в массиве дат гашения: ', max_date);

  min_date = dateMin(dates_buy_arr);
  // console.log('Минимальная дата в массиве дат гашения: ', min_date);

  // дата начала отслеживания (если не задана, то по-умолчанию используется сегодняшняя дата)
  var date_begin = new Date();
  if ($('#portfolio_date').length != 0) {
    date_begin = min_date;
  }

  //	console.log(date_begin);
  //	console.log(max_date);

  dates_arr = datesArrayCreate(date_begin, max_date);
  // console.log('Маccив дат периодом в месяц: ', dates_arr);

  chart_donut_arr = chartDonutArrMaking(user_money, cur_price_full, buy_shares_price_full, buy_bonds_price_full);
//console.log('Маccив данных кругового графика: ', chart_donut_arr);

  // console.log('Объект с данными по всем строкам таблицы: ', rows_obj);
  var chart_linear_arr = chartLinearArrMaking(dates_arr, date_begin, rows_obj);
  // console.log('!!!', chart_linear_arr);

  // вывод графиков
  roundChartUpdate(chart_donut_arr);
  if(chart_linear_arr.chart_date!='NaN-NaN-NaN'){
  linearChartUpdate(chart_linear_arr);
  }
}

// -----------------------------
// --- ВСПОМОГАТЕЛЬНЫЕ МЕТОДЫ --
// -----------------------------

// вывод кругового графика
function roundChartUpdate(chart_donut_arr, chartOuter = '.round_chart_outer', chartId = 'round_chart') {
	$('#'+chartId).remove();
	$(chartOuter).append('<canvas id="'+chartId+'"></canvas>');
	var round_chart = document.querySelector('#'+chartId);
	var round_chart_data = {
		type: 'doughnut',
		data: {
			labels: [],
			datasets: []
		},
		options: {
			legend: {
				display: false
			},
            aspectRatio: 1.2,
			rotation: 4.72,
			tooltips: {
				callbacks: {
				label: function(tooltipItem, data) {
					var allData = data.datasets[tooltipItem.datasetIndex].data;
					var tooltipLabel = data.labels[tooltipItem.index];
					var tooltipData = allData[tooltipItem.index];
					var total = 0;
					for (var i in allData) {
						total += allData[i];
					}
					var tooltipPercentage = Math.round((tooltipData / total) * 100);
					return tooltipLabel + ': ' + tooltipData + '%';
				}
				}
			},
		}
	}

	round_chart_data.data.labels = [];
	round_chart_data.data.datasets = [];

	var round_chart_values = [],
			round_chart_colors = [];
	for (var i = 0; i < chart_donut_arr.length; i++) {
		round_chart_data.data.labels.push(chart_donut_arr[i][0]);
		round_chart_values.push(chart_donut_arr[i][1]);
		round_chart_colors.push(chart_donut_arr[i][2]);
	}
	round_chart_data.data.datasets.push({
		data: round_chart_values,
		borderWidth: 1,
		backgroundColor: round_chart_colors
	});

	var roundChart = new Chart(round_chart, round_chart_data);
	//console.log('Маccив данных кругового графика: ', chart_donut_arr);
}

// вывод линейного графика
function linearChartUpdate(chart_linear_arr) {
  $('#linear_chart').remove();
  $('.linear_chart_outer').append('<canvas id="linear_chart"></canvas>');
  var linear_chart = document.querySelector('#linear_chart');

  var labels_arr = [];

  var chart_data = {
    type: 'line',
    data: {
      labels: labels_arr,
      datasets: []
    },
    options: {
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: false,
        callbacks: {
          label: function(tooltipItems, data) {
            return tooltipItems.yLabel + ' руб.';
          }
        }
      },
      hover: {
        mode: 'index',
        intersect: false,
      },
    }
  }

  chart_data.data.labels = [];
  var money_in_work_arr = [],
      bonds_up_arr = [],
      shares_up_arr = [],
      shares_down_arr = [];

  for (var line in chart_linear_arr) {
    money_in_work_arr.push(chart_linear_arr[line].chart_first_profitability.toFixed(2));
    bonds_up_arr.push(chart_linear_arr[line].chart_month_profitability_bond.toFixed(2));
    shares_up_arr.push(chart_linear_arr[line].chart_month_profitability_share.toFixed(2));
    shares_down_arr.push(chart_linear_arr[line].chart_month_drawdown_share.toFixed(2));

    var date = new Date(chart_linear_arr[line].chart_date);
    if (isNaN(date.getTime())) {
      date = new Date();
    }


    var day = date.getDate()
    if (day < 10) day = '0' + day;

    var month = date.getMonth() + 1;
    if (month < 10) month = '0' + month;

    var year = date.getFullYear();

    date_txt = day + '.' + month + '.' + year;

    chart_data.data.labels.push(date_txt);
  }

  if (money_in_work_arr.length == 0) {alert()
    money_in_work_arr.push(0);
  }
  if (bonds_up_arr.length == 0) {
    bonds_up_arr.push(0);
  }
  if (shares_up_arr.length == 0) {
    shares_up_arr.push(0);
  }
  if (shares_down_arr.length == 0) {
    shares_down_arr.push(0);
  }
  if (chart_data.data.labels.length == 'NaN.NaN.NaN') {
    chart_data.data.labels = ['-'];
  }

  // console.log(labels_arr)

  chart_data.data.datasets.push({
    label: "Доход от акций",
    backgroundColor: 'rgba(0, 176, 80, 0)',
    borderColor: 'rgb(96, 255, 0)',
    borderDash: [10,5],
    data: shares_up_arr,
    radius: 0
  }, {
    label: "Просадка акций",
    backgroundColor: 'rgba(255, 65, 65, 0)',
    borderColor: 'rgb(255, 65, 65)',
    borderDash: [10,5],
    data: shares_down_arr,
    radius: 0
  }, {
    label: "Активы",
    backgroundColor: 'rgb(166, 219, 22)',
    borderColor: 'rgb(166, 219, 22)',
    data: money_in_work_arr,
    radius: 0
  }, {
    label: "Доход от облигаций",
    backgroundColor: 'rgb(238, 211, 36)',
    borderColor: 'rgb(238, 211, 36)',
    data: bonds_up_arr,
    radius: 0
  });

  var linearChart = new Chart(linear_chart, chart_data);
}

// преобразование даты в текст из Date-формата
function dateToString(date) {
  var day = date.getDate()
  day = day.toString()
  if (day.length < 2) {
    day = '0' + day;
  }

  var month = date.getMonth() + 1;
  month = month.toString()
  if (month.length < 2) {
    month = '0' + month;
  }

  var year = date.getFullYear();

  return {
    date: year + '-' + month + '-' + day,
    day: day,
    month: month,
    year: year
  }
}

// преобразование текстовой даты в Date-формат
function dateToDate(date) {
  var date = new Date(date);
  date.setHours(0);
  var day = date.getDate()
  var month = date.getMonth() + 1;
  var year = date.getFullYear();

  return {
    date: date,
    day: day,
    month: month,
    year: year
  }
}

// максимальная дата в массиве дат
function dateMax(arr) {
  var dates_arr = [];
  var max_date;

  for (var i = 0; i < arr.length; i++) {
    dates_arr.push(new Date(arr[i]));
  }

  max_date = new Date(Math.max.apply(null, dates_arr));

  return max_date;
}

// минимальная дата в массиве дат
function dateMin(arr) {
  var dates_arr = [];
  var min_date;

  for (var i = 0; i < arr.length; i++) {
    dates_arr.push(new Date(arr[i]));
  }

  min_date = new Date(Math.min.apply(null, dates_arr));

  return min_date;
}

// формирование массива дат периодом в месяц между датой начала и датой конца
function datesArrayCreate(date_begin, date_end) {
  var months_count = dateDiffCount(date_begin, date_end);

  if (months_count < 24) {
    months_count = 24;
  }

  var dates_arr = [];

  // добавляем в массив дат начальную (сегодняшнюю) дату
  dates_arr.push(dateToString(date_begin).date);

  // добавляем в массив каждый месяц между датой начала и датой конца
  var new_date = new Date(date_begin);
  var j = 0;
  for (var i = 0; i < months_count; i++) {
    new_date.setMonth(new_date.getMonth() + 1); // увеличиваем месяц на единицу
    new_date.setDate(1); // выставляем первый день месяца

    dates_arr.push(dateToString(new_date).date); // добавляем получившуюся дату в массив
  }

  return dates_arr;
}

// подсчет количества месяцев в промежутке между датами
function dateDiffCount(date_begin, date_end) {
  var months = date_end.getMonth() - date_begin.getMonth();
  if ( months < 0 ) {
    months += 12;
    date_end.setFullYear( date_end.getFullYear() - 1 );
  }

  var years = date_end.getFullYear() - date_begin.getFullYear();

  if (years < 1) {
    months_final = months;
  } else {
    months_final = months + (years * 12);
  }

  return months_final;
}

// подсчет количества дней в промежутке между датами
function dateDaysDiffCount(date_begin, date_end) {
  //	console.log('dateDaysDiffCount>> date_begin:'+date_begin+' date_end:'+date_end);
  var date_end_date = dateToDate(date_end).date;
  var date_begin_date = dateToDate(date_begin).date;
  var month_timeDiff = Math.abs(date_end_date.getTime() - date_begin_date.getTime());
  var month_days_diff = Math.ceil((month_timeDiff) / (1000 * 3600 * 24));

  return month_days_diff;
}

// прибавить n лет года к дате
function datePlusNYears(date, n) {
  date.setFullYear(date.getFullYear() + n); //добавляем к 2 года
  date.setDate(1); //выставляем день равный первому дню месяца
  date_string = dateToString(date).date;

  date.day = '01';

  return {
    date: dateToString(date).date,
    day: dateToString(date).day,
    month: dateToString(date).month,
    year: dateToString(date).year
  }
}

// формирование массива для кругового графика
function chartDonutArrMaking(user_money, buy_price_full, buy_shares_price_full, buy_bonds_price_full) {
  var chart_array = [];
  var free_money = 0; // количество нераспределенных средств пользователя

  if (user_money == 0) { // если отсутсвуют данные по количеству денег пользователя (например, на стр. портфеля) то берется значение суммы цен покупки всех ценных бумаг, т.е. общая сумма потраченных денег
    user_money = buy_price_full;
	 if(buy_price_full==0){
	  user_money = 1;
	  free_money = 1;
	 }
  } else {
    free_money = user_money - buy_price_full;
  }

  for (var i = 0; i < $('.radar_table.pt_active tbody tr:not(.oblig_off)').length; i++) {
    var tr = $('.radar_table.pt_active tbody tr:not(.oblig_off)').eq(i);
    var tr_data = tableRecount(tr);

    var chart_donut_arr_element = [];
   // var chart_donut_arr_element_val = (tr_data.price_buy * 100) / user_money;
    var chart_donut_arr_element_val = (tr.find('.portfolio_elem_buy_price_current').attr('data-value') * 100) / user_money;
// console.log('расчет: ', 'стоимость='+tr.find('.portfolio_elem_buy_price_current').attr('data-value')+' user_money='+user_money+' итого='+chart_donut_arr_element_val);
    chart_donut_arr_element.push(tr_data.name, chart_donut_arr_element_val.toFixed(1), colors_big_arr[i]);
    chart_array.push(chart_donut_arr_element);
  }

  if (free_money != 0) { // если имеются нераспределенные средства (при наличии известного количества денег пользователя) считаем долю нераспределенных средств и добавляем к массиву
    var free_money_val = (free_money * 100) / user_money;
    var free_money_element = ['Не распределено', free_money_val.toFixed(1), '#a6db16'];
    chart_array.push(free_money_element);
  }
//  console.log('chartDonutArrMaking', chart_array);
  return chart_array;
}

// формирование массива для линейного графика
function chartLinearArrMaking(dates_arr, date_begin, rows_obj) {
  var chart_array = {}; // основной объект для линейного графика
  //	console.log('Массив дат:'+dates_arr);
  for (var i = 0; i < dates_arr.length; i++) {
    var chart_array_current = {}; // объект для текущей итерации цикла

    var curr_date_Date = dateToDate(dates_arr[i]).date; // текущая дата из массива дат

    var rows_obj_current = {}; // промежуточный объект для всех бумаг

    var first_profitability = 0; // общий первоначальный капитал. одинаков для всех месяцев
    var month_profitability_bond = 0; // общий капитал по облигациям
    var month_profitability_share = 0; // общий капитал по акциям
    var month_drawdown_share = 0; // общий просад по акциям

    var iis_before = 0;  // значение ИИС предыдущего месяца
    var refill_before = 0;  // значение пополнения счета предыдущего месяца
    var refill = 0;  // значение пополнения счета текущего месяца
    var refill_profitability = 0; // чистый доход от ежемесячного пополнения

    var month_profitability_clear = 0; // чистый доход за месяц
    var month_profitability_bond_clear = 0; // чистый доход по облигациям за месяц
    var month_profitability_share_clear = 0; // чистый доход по акциям за месяц
    var month_drawdown_share_clear = 0; // чистый просад по акциям за месяц

  	if (i == 0) {
      // перебираем все "участвующие" ценные бумаги
      var bonds = 0, shares = 0, shares_usa = 0, share_etf =0; // подсчитываем количество облигаций и акций в наборе
      for (var row in rows_obj) {
        //first_profitability += rows_obj[row].price_buy;
        //first_profitability += rows_obj[row].price_sell;
        first_profitability += rows_obj[row].current_exchange_price;
//		  console.log('row',rows_obj[row]);

		   var curr_shareSell_Date = dateToDate(rows_obj[row].date_sell).date; //дата продажи акции

/*        if (rows_obj[row].type == 'bond') {
          bonds++;
        } else if ((rows_obj[row].type == 'share' || rows_obj[row].type == 'share_etf' || rows_obj[row].type == 'share_usa') && (curr_date_Date<=curr_shareSell_Date)) {
          shares++;
        } else if (rows_obj[row].type == 'share_etf' && (curr_date_Date<=curr_shareSell_Date)) {
          share_etf++;
        } else if (rows_obj[row].type == 'share_usa' && (curr_date_Date<=curr_shareSell_Date)) {
          share_usa++;
        }*/

         if (rows_obj[row].type == 'bond') {
          bonds++;
        } else if ((rows_obj[row].type == 'share' || rows_obj[row].type == 'share_etf' || rows_obj[row].type == 'share_usa') ) {
          shares++;
        } else if (rows_obj[row].type == 'share_etf' ) {
          share_etf++;
        } else if (rows_obj[row].type == 'share_usa' ) {
          share_usa++;
        }


        //создаем независимую копию объекта
        rows_obj_current[row] = rows_obj[row];
      }

	 //  if (bonds != 0) {  //Если с условием то общий риск портфеля считается с ошибкой
        month_profitability_bond = first_profitability;
    //  }
      if (shares != 0) {
        month_profitability_share = first_profitability;
        month_drawdown_share = first_profitability;
      }

    } else {
      first_profitability = chart_array[i - 1].chart_first_profitability;

      var new_data_obj = currentRowCount(chart_array[i - 1].rows_obj, dates_arr[i - 1], curr_date_Date);

      rows_obj_current = new_data_obj.new_object; // пересчитываем данные в каждой бумаге для текущего временного промежутка

      // подсчет месячного дохода по облигациям
	  //	if (bonds != 0) { //Если с условием то общий риск портфеля считается с ошибкой
      month_profitability_bond = new_data_obj.month_profitability_bond + chart_array[i - 1].chart_first_profitability;
	  //	}
// console.log('month_profitability_bond: '+month_profitability_bond+' obj.month_profitability_bond: '+new_data_obj.month_profitability_bond+' chart_first_profitability: '+chart_array[i - 1].chart_first_profitability );
	  //	console.log('curr_date_Date',curr_date_Date);
//	console.log('new_data_obj',new_data_obj);
      if (new_data_obj.shares_length > 0) {
        //	console.log('chart_array',chart_array);
        // подсчет месячного дохода по акциям
        month_profitability_share = new_data_obj.month_profitability_share + chart_array[i - 1].chart_first_profitability;

        // подсчет месячного просада по акциям
        month_drawdown_share = new_data_obj.month_drawdown_share + chart_array[i - 1].chart_first_profitability;
      } else {
        // подсчет месячного дохода по акциям
        month_profitability_share = 0;

        // подсчет месячного просада по акциям
        month_drawdown_share = 0;
      }

      iis_before = chart_array[i - 1].iis;
      refill_before = chart_array[i - 1].refill_before;
    }

    refill = first_profitability;

    // ИИС
    var iis = iisCount(dates_arr[i], date_begin, iis_before, refill_before, refill);

    chart_array_current['chart_date'] = dates_arr[i]; // добавление даты
    chart_array_current['rows_obj'] = rows_obj_current; // добавление объекта с данными по каждой бумаге
    chart_array_current['chart_first_profitability'] = first_profitability; // добавление первоначального общего капитала
    chart_array_current['chart_month_profitability_bond'] = month_profitability_bond; // добавление месячного дохода по облигациям
    chart_array_current['chart_month_profitability_share'] = month_profitability_share; // добавление месячного дохода по акциям
    chart_array_current['chart_month_drawdown_share'] = month_drawdown_share; // добавление месячного просада по акциям
    chart_array_current['chart_refill'] = refill; // ежемесячное пополнение счёта в текущем месяце. В первом месяце по-умолчанию равно общему капиталу. При отсутвтвии ежемесячного пополнения так же равно первоначальному общему капиталу
    // -----
    chart_array_current['refill_before'] = refill_before; // ежемесячное пополнение счёта в предыдущем месяце
    chart_array_current['refill_profitability'] = refill_profitability; // чистый доход от ежемесячного пополнения
    chart_array_current['iis'] = iis; // размер выплаты по ИИС в текущем месяце
    chart_array_current['month_profitability_clear'] =  month_profitability_clear; // чистый доход за месяц
    chart_array_current['month_profitability_bond_clear'] = month_profitability_bond_clear; // чистый доход по облигациям за месяц
    chart_array_current['month_profitability_share_clear'] = month_profitability_share_clear; // чистый доход по акциям за месяц
    chart_array_current['month_drawdown_share_clear'] = month_drawdown_share_clear; // чистый просад по акциям за месяц

    chart_array[i] = chart_array_current;
  }


	var prognose_income_bond = (chart_array_current['chart_month_profitability_bond']-chart_array_current['chart_first_profitability']).toFixed(2);

  	$('.footer_summ_data_row').attr('data-prognose_income_bond', prognose_income_bond);
	tableFootUpdate(prognose_income_bond);

 console.log('linear chart func> ', chart_array);
 // console.log('linear chart func> ', chart_array);
  return chart_array;
}

// пересчет данных по бумагам из прошлого временного промежутка для текущего
function currentRowCount(rows_obj_before, date, curr_date_Date) {
  //создаем независимую копию объекта
  var rows_obj_curr = {};
  for (var key in rows_obj_before) {
    var obj_curr = {}
    for (var subkey in rows_obj_before[key]) {
      obj_curr[subkey] = rows_obj_before[key][subkey];
    }
    rows_obj_curr[key] = obj_curr;
  }
 //  console.log(',,,',rows_obj_before)

  // суммы доходностей и просада бумаг
  var bond_profit_summ = 0;
  var share_profit_summ = 0;
  var share_drawdown_summ = 0;

  var share_etf_profit_summ = 0;
  var share_etf_drawdown_summ = 0;

  var share_usa_profit_summ = 0;
  var share_usa_drawdown_summ = 0;

  var shares_length = 0;
  var shares_etf_length = 0;
  var shares_usa_length = 0;
  //перебираем этот объект
   for (var row in rows_obj_curr) {
    var curr_date_off = dateToDate(rows_obj_curr[row].date_sell).date; // получаем дату продажи текущей бумаги

/*	 if (rows_obj_curr[row].type == 'bond'){
    var curr_date_off = dateToDate(rows_obj_curr[row].real_date_sell).date; // получаем дату продажи текущей бумаги
	} else {
	 var curr_date_off = dateToDate(rows_obj_curr[row].date_sell).date; // получаем дату продажи текущей бумаги
	}*/
    var curr_date_Date_before = dateToDate(date).date; // получаем дату предыдущего промежутка

    // получаем доход и просад текущей бумаги за предыдущий месяц
    var before_profit_bond = rows_obj_before[row].final_profit_bond,
        before_profit_share = rows_obj_before[row].final_profit_share,
        before_drawdown_share = rows_obj_before[row].final_drawdown_share;
//console.log('date: '+curr_date_Date_before+' curr_date_Date:'+curr_date_Date);

    // считаем доходность каждой бумаги в текущем месяце
    var curr_profit_bond = 0;
    var curr_profit_share = 0;
    var curr_drawdown_share = 0;
    var curr_etf_profit_share = 0;
    var curr_etf_drawdown_share = 0;
    var curr_usa_profit_share = 0;
    var curr_usa_drawdown_share = 0;
    // если дата текущего промежутка меньше или равна дате гашения текущей бумаги:

    if (curr_date_Date <= curr_date_off) {
      var curr_days_diff = dateDaysDiffCount(curr_date_Date_before, curr_date_Date);
	   rows_obj_curr[row].common_days_diff = rows_obj_curr[row].days_diff;
      //чистый доход облигации
      var clear_profit_debstock = 0;
      if (rows_obj_curr[row].type == 'bond') {
        //clear_profit_debstock = rows_obj_curr[row].price_sell - rows_obj_curr[row].price_buy;
        clear_profit_debstock = rows_obj_curr[row].price_sell - rows_obj_curr[row].price_buy;

        curr_profit_bond = ((clear_profit_debstock / rows_obj_curr[row].common_days_diff) * curr_days_diff) + before_profit_bond;
// console.log('row', rows_obj_curr[row]);
//console.log('bond clear_profit_debstock: '+clear_profit_debstock+' common_days_diff: '+rows_obj_curr[row].common_days_diff+' curr_days_diff: '+curr_days_diff+' before_profit_bond: '+before_profit_bond);

        rows_obj_curr[row].final_profit_bond = curr_profit_bond;
		//  console.log(curr_profit_bond);
      }

      //чистый доход и просад акции
      var clear_profit_share = 0;
      var clear_drawdown_share = 0;
	  	if (rows_obj_curr[row].type == 'share' || rows_obj_curr[row].type == 'share_etf' || rows_obj_curr[row].type == 'share_usa') {
        //clear_profit_share = (rows_obj_curr[row].price_buy * rows_obj_curr[row].profitability_share) / 100;
        clear_profit_share = (rows_obj_curr[row].price_sell * rows_obj_curr[row].profitability_share) / 100;
							 //7162
        curr_profit_share = ((clear_profit_share / rows_obj_curr[row].common_days_diff) * curr_days_diff) + before_profit_share;

        rows_obj_curr[row].final_profit_share = curr_profit_share;
		  //console.log('curr_profit_share: '+curr_profit_share+' clear_profit_share: '+clear_profit_share+' rows_obj_curr[row].common_days_diff: '+rows_obj_curr[row].common_days_diff+' curr_days_diff: '+curr_days_diff+' before_profit_share:'+before_profit_share);

        //--------------------------------

        //clear_drawdown_share = (rows_obj_curr[row].price_buy * rows_obj_curr[row].drawdown_share) / 100;
        clear_drawdown_share = (rows_obj_curr[row].price_sell * rows_obj_curr[row].drawdown_share) / 100;


		 curr_drawdown_share = ((clear_drawdown_share / rows_obj_curr[row].common_days_diff) * curr_days_diff) + before_drawdown_share;

        rows_obj_curr[row].final_drawdown_share = curr_drawdown_share;

        shares_length++;
		  if(rows_obj_curr[row].type == 'share_etf'){
			shares_etf_length++;
		  }
		  if(rows_obj_curr[row].type == 'share_usa'){
			shares_usa_length++;
		  }
      }


    } else {
      //чистый доход облигации
      if (rows_obj_curr[row].type == 'bond') {
        curr_profit_bond = before_profit_bond;
      }

      //чистый доход и просад акции
      if (rows_obj_curr[row].type == 'share' || rows_obj_curr[row].type == 'share_etf' || rows_obj_curr[row].type == 'share_usa') {
        curr_profit_share = before_profit_share;
        //--------------------------------
        curr_drawdown_share = before_drawdown_share;
		  //console.log('curr_profit_share: '+curr_profit_share+' curr_drawdown_share:'+curr_drawdown_share);
		  shares_length++;
		  if(rows_obj_curr[row].type == 'share_etf'){
			 shares_etf_length++;
		  }
		  if(rows_obj_curr[row].type == 'share_usa'){
			 shares_usa_length++;
		  }
      }


    }

     //console.log('>>>>>>>>>>>>>', curr_profit_bond, '|', clear_profit_debstock, curr_date_off)

    bond_profit_summ += curr_profit_bond;
    share_profit_summ += curr_profit_share;
    share_drawdown_summ += curr_drawdown_share;

    share_etf_profit_summ += curr_etf_profit_share;
    share_etf_drawdown_summ += curr_etf_drawdown_share;

    share_usa_profit_summ += curr_usa_profit_share;
    share_usa_drawdown_summ += curr_usa_drawdown_share;
  }
  if (shares_length == 0) {
    share_profit_summ = 0;
    share_drawdown_summ = 0;
  } else {
    share_profit_summ += bond_profit_summ;
    share_drawdown_summ = -share_drawdown_summ + bond_profit_summ;
  }
   //console.log('<<<<<<<<<<<<<', rows_obj_curr)

  return {
    new_object: rows_obj_curr,
    month_profitability_bond: bond_profit_summ,
    month_profitability_share: share_profit_summ,
    month_drawdown_share: share_drawdown_summ,
    shares_length: shares_length,
    shares_etf_length: shares_etf_length,
    shares_usa_length: shares_usa_length
  }
}

// подсчет ИИС
function iisCount(date, date_begin, iis, iis_curr, refill) {
  // console.log('????????????', date, date_begin, iis, iis_curr, refill)
  var iis_curr_date = new Date(date);
  var iis_curr_date_month = iis_curr_date.getMonth() + 1;
  var iis_curr_date_year = iis_curr_date.getFullYear();

  date_begin = new Date(date_begin);
  var date_begin_year = date_begin.getFullYear();

  var new_iis = iis;

  if ($('#iis').length != 0 && $('#iis').prop('checked')) {
    if (iis_curr_date_month == 12 && iis_curr_date_year == date_begin_year) {
      iis_curr = refill;
    }
    if (iis_curr_date_month == 3 && iis_curr_date_year == date_begin_year + 1) {
      new_iis = iis_curr * 0.13;
      if (new_iis > 52000) {
        new_iis = 52000;
      }
    }
  }

  return new_iis;
}

// сбор и подсчет данных по одной бумаге
function tableRecount(tr) {
    var type = tr.attr('data-elem'); //bond or share
    var currency = tr.attr('data-currency');

    // id и имя бумаги
    var id = tr.attr('data-id');
    var name = tr.find('.elem_name a:first-child()').text();

    // цены покупки и гашения одной бумаги
    var price_buy_one = parseFloat(tr.attr('data-price_buy'));
    var price_sell_one = parseFloat(tr.attr('data-price_sell'));
    var current_exchange_price_one = parseFloat(tr.attr('data-current_exchange_price'));

	 if(type=='share_etf' || type=='share_usa'){  //Пересчитаем цены для графикка по etf и акциям США
	  if(currency!='rub' && currency!='sur' && currency.length){
		 price_sell_one = price_sell_one * $('#'+currency+'_currency').val();
		 current_exchange_price_one = current_exchange_price_one * $('#'+currency+'_currency').val();

	  }
	 }

    // цены покупки и гашения указанного количества бумаг
    var quantity = 0;
    if (tr.find('.elem_number').length != 0) {
      quantity = parseFloat(tr.find('.elem_number').val());
    }
    var price_buy = 0;
    if (price_buy_one && price_buy_one != '' && price_buy_one != Number.isNaN(NaN)) {
      price_buy = price_buy_one * quantity;
    }
    var price_sell = 0;
    if (price_sell_one && price_sell_one != '' && price_sell_one != Number.isNaN(NaN)) {
      price_sell = price_sell_one * quantity;
    }
	 var current_exchange_price = 0;
    if (current_exchange_price_one && current_exchange_price_one != '' && current_exchange_price_one != Number.isNaN(NaN)) {
      current_exchange_price = current_exchange_price_one * quantity;
    }


    // дата покупки
    var date_buy = tr.attr('data-date_buy');
    var real_date_buy = tr.attr('data-real_date_buy');
/*    if (date_buy == 'today' || (date_buy == '' || date_buy == undefined || date_buy == Number.isNaN(NaN))) {
      date_buy = dateToString(new Date).date;
    } else if (date_buy == 'portfolio_date') {
      if ($('#portfolio_date').length != 0) {
        date_buy = dateToString($('#portfolio_date').datepicker('getDate')).date;
      } else {
        date_buy = dateToString(new Date).date;
      }

    }*/

    // дата гашения
    var date_sell = tr.attr('data-date_sell');
    var real_date_sell = tr.attr('data-real_date_sell');
    if (date_sell == '' || date_sell == undefined || Number.isNaN(date_sell)) {
      date_sell = dateToDate(date_buy).date;
      date_sell = datePlusNYears(date_sell, 2).date;
      tr.attr('data-date_sell', date_sell);
    }

    // количество дней межлу датой покупки и гашения
    //var days_diff = dateDaysDiffCount(dateToDate(date_buy).date, dateToDate(date_sell).date);
	 //Берем кол-во дней между датой покупки и реальной датой гашения
    var days_diff = dateDaysDiffCount(dateToDate(real_date_buy).date, dateToDate(real_date_sell).date);

    // чистый доход облигации
    var clear_income = 0;
    if (type == 'bond') {
      clear_income = price_sell - price_buy;
    }

    // доходность облигации
    var profitability_bond = 0;
        // profitability_bond_year = 0;
    if (type == 'bond') {
      profitability_bond = parseFloat(tr.attr('data-profitability'));
      if (profitability_bond == '' || profitability_bond == undefined || Number.isNaN(profitability_bond)) {
        profitability_bond = 0;
      }
      // profitability_bond_year = parseFloat(tr.attr('data-profitability_year'));
    }

    // доходность и просад акции
    var profitability_share = 0,
        drawdown_share = 0;
    if (type == 'share' || type == 'share_etf' || type == 'share_usa') {
      profitability_share = parseFloat(tr.attr('data-profitability'));
      if (profitability_share == '' || profitability_share == undefined || Number.isNaN(profitability_share)) {
        profitability_share = 0;
      }
      drawdown_share = parseFloat(tr.attr('data-drawdown'));
      if (drawdown_share == '' || drawdown_share == undefined || Number.isNaN(drawdown_share)) {
        drawdown_share = 0;
      }
    }

    // чистый доход и просад акции
    var clear_drawdown = 0;
    if (type == 'share' || type == 'share_etf' || type == 'share_usa') {
      //clear_income = (price_buy * profitability_share) / 100;
      clear_income = (price_sell * profitability_share) / 100;
		clear_income = parseFloat(clear_income.toFixed(4));
      //clear_drawdown = ((price_buy * drawdown_share) * (-1) / 100);
      clear_drawdown = ((price_sell * drawdown_share) * (-1) / 100);
    }

    return {
      name: name,
      id: id,
      type: type,
      currency: currency,
      quantity: quantity,
      price_buy: price_buy,
      price_sell: price_sell,
      current_exchange_price: current_exchange_price,
      date_buy: date_buy,
      date_sell: date_sell,
      clear_income: clear_income,
      clear_drawdown: clear_drawdown,
      profitability_bond: profitability_bond,
      profitability_share: profitability_share,
      drawdown_share: drawdown_share,
      final_profit_bond: 0,
      final_profit_share: 0,
      final_drawdown_share: 0,
      days_diff: days_diff
    }
}
