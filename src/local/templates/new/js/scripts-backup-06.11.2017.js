//гамбургер
$(function() {
	$('.hamburger').click(function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$('.outer').removeClass('opened');
			$('.main_menu_container').removeClass('opened');
		} else {
			$(this).addClass('active');
			$('.outer').addClass('opened');
			$('.main_menu_container').addClass('opened');
		}
	});
	/* $('body').mouseup(function (e) {
		var block = $('.main_menu_container');
		
		if (e.target!=block[0]&&!block.has(e.target).length){
			$('.main_menu_container').removeClass('opened');
			$('.hamburger').removeClass('active');
			$('.outer').removeClass('opened');
		}
	}); */
});

//клонирование заголовка широкого блока услуг
$(function() {
	$('.services_gray_container .services_element_title').each(function() {
		if ($(this).closest('.services_element').find('.services_element_title').length == 1) {
			var title = $(this).clone();
			$(this).closest('.services_element').prepend(title);
		}
	});
});
	
//masked input
$('input[name="phone"], input[name="userphone"], input[name="tel"]').mask('+7 (999) 999-99-99',{autoclear: false});
	
//автодобавление span.placeholder
$(function() {
	$('.form_element').each(function() {
		if ($(this).find('.placeholder').length == 0) {
			var place;
			
			if ($(this).find('input').attr('type') != 'checkbox' && $(this).find('input').attr('type') != 'radio' && $(this).find('input').attr('type') != 'submit') {
				if ($(this).find('input').length != 0) 
					place = $(this).find('input').attr('placeholder');
				else if ($(this).find('textarea').length != 0) 
					place = $(this).find('textarea').attr('placeholder');
				
				$(this).find('input').after('<span class="placeholder">' + place + '</span>');
			}
		}
	});
	
	$('.form_element input').blur(function() {
		if ($(this).attr('type') != 'checkbox' && $(this).attr('type') != 'radio' && $(this).attr('type') != 'submit') {
			if ($(this).val() != '' && $(this).val() != undefined) {
				$(this).closest('.form_element').addClass('not_empty');
			} else {
				$(this).closest('.form_element').removeClass('not_empty');
			}
		}
	});
});

//показать/скрыть пароль
$(function() {
	$('.show_password').click(function() {
		var input = $(this).closest('.form_element').find('input');
		
		$(this).hasClass('active') == true ? $(this).removeClass('active') : $(this).addClass('active');
		
		var type = input.attr('type') == "text" ? "password" : 'text',
			c = $(this).text() == "Скрыть пароль" ? "Показать пароль" : "Скрыть пароль";
			
		$(this).text(c);
		input.prop('type', type);
	});
});

//автодобавление id и for для радиокнопок и чекбоксов
$(function() {
	for (var i = 0; i < $('.checkbox').length; i++) {
		if (!$('.checkbox').eq(i).find('input[type="checkbox"]').is('[id]')) {
			$('.checkbox').eq(i).find('input[type="checkbox"]').attr('id', 'checkbox_' + i);
			$('.checkbox').eq(i).find('label').attr('for', 'checkbox_' + i);
		}
	}
	for (var j = 0; j < $('.radio').length; j++) {
		if (!$('.radio').eq(j).find('input[type="radio"]').is('[id]')) {
			$('.radio').eq(j).find('input[type="radio"]').attr('id', 'radio_' + j);
			$('.radio').eq(j).find('label').attr('for', 'radio_' + j);
		}
	}
});
	
//полноэкранный слайдер на главной
$(function() {
    if ($('.mainpage_wrapper').length != 0) {
		$('.wrapper').css('padding', 0);
		$('.outer').addClass('mainpage_outer');
		
		$('.mainpage_wrapper').fullpage({
			navigation: true,
			scrollingSpeed: 1000,
			normalScrollElements: '.fs_elem_text_04, .modal',
			touchSensitivity: 20
		});
		
		$('.bottom_elements .go_down').click(function() {
			$.fn.fullpage.moveSectionDown();
		});
		
		function fixbootstrapScroll (value) {
			var isTouchDevice = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|Windows Phone)/);

			if (isTouchDevice) {
				if (value) {
					$.fn.fullpage.setAutoScrolling(false);
				} else {
					$.fn.fullpage.setAutoScrolling(true);
				}
			}
		}
		
		$(document)
		.on('show.bs.modal', '.modal', function () {
			$(document.body).addClass('modal-open');
			fixbootstrapScroll(true);
			/* $(document.body).css({
				'overflow': 'hidden',
			}); */
		})
		.on('hidden.bs.modal', '.modal', function () {
			$(window).scrollTop(0);
			/* $(document.body).css({
				'overflow': 'auto',
			}); */
			$(document.body).removeClass('modal-open'); 
			fixbootstrapScroll(false);
			setTimeout(function() {
			$.fn.fullpage.reBuild();
			}, 1000);
		})
	}
});

//логика работы главного меню и событие скролла
$(function() {
	var scrlTop = function() {
		if ($(window).outerWidth() > 767 && !$('html').hasClass('mobile') && !$('html').hasClass('tablet') && $('.mainpage_outer').length == 0) {
			if ($(window).scrollTop() <= 40)
				$('.header').addClass('top');
			else
				$('.header').removeClass('top');
		}
	}
	scrlTop();
	
	$('.main_menu li').each(function() {
		if ($(this).find('ul').length != 0) {
			$(this).addClass('main_menu_toggle_outer');
			$(this).children('a').addClass('main_menu_toggle_link');
			$(this).attr('data-click', 0);
		}
	});
	
	if ($(window).outerWidth() < 768 || $('html').hasClass('mobile') || $('html').hasClass('tablet')) {
		$('.main_menu_toggle_link').on('click', function(event) {
			event.preventDefault();
			var k = $(this).closest('.main_menu_toggle_outer').attr('data-click');
			k = parseInt(k);
			
			$('.main_menu_toggle_outer').removeClass('opened');
			$('.main_menu_toggle_outer').attr('data-click', 0);
			
			if (k == 0) {
				$(this).closest('.main_menu_toggle_outer').addClass('opened');
				k++;
				$(this).closest('.main_menu_toggle_outer').attr('data-click', k);
			} else {
				location.href = $(this).attr('href');
			}
		})
	}
	
	/* var visible = function() {
		$(this).addClass('opened');
		$(this).find('.main_menu_toggle').show(600);
	}
	if ($(window).outerWidth() > 767 && !$('html').hasClass('mobile') && !$('html').hasClass('tablet')) {
		$('.main_menu_toggle_outer').mouseenter(function() {
			visible();
		});
	} else {
		
	} */
});

//слайдер обучения на главной
if ($('.learning_slider').length != 0) {
	var len = $('.learning_slider .learning_slide').length;
	var len_final;
	
	if (len < 2) {
		len_final = 1;
	} else if (len == 2) {
		len_final = 2;
		$('.learning_slider').addClass('len2');
	} else if (len > 2) {
		len_final = 3;
		$('.learning_slider').addClass('len3');
	}
	
	$('.learning_slider').slick({
		infinite: false,
		speed: 600,
		slidesToShow: len_final,
		slidesToScroll: 1,
		swipeToSlide: true,
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				swipeToSlide: true,
			}
		}, {
			breakpoint: 569,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				swipeToSlide: true,
			}
		}]
	});
}

//скроллбар контента на 4 слайде на главной
$(function(){
	$(window).on("load",function(){
		if ($(".fs_elem_text_04_inner").length != 0) {
			$(".fs_elem_text_04_inner").mCustomScrollbar({
				scrollbarPosition: "outside"
			});
		}
	});
});

//логика 5 слайда на главной
$(function() {
	if ($('.fs_elem_05_nav').length != 0) {
		$('.fs_elem_05_nav').slick({
			slide: 'li',
			speed: 300,
			slidesToShow: 1,
			respondTo: 'min',
			focusOnSelect: true,
			swipeToSlide: true,
			centerMode: true,
			centerPadding: '0',
			infinite: false,
			variableWidth: true
		});
	}

	$(window).on("load",function(){
		if ($('.matherials_slider').length != 0) {
			$('.matherials_slider').mCustomScrollbar({
				scrollbarPosition: "outside"
			});
		}
	});

	$(function() {
		var type;
		var number = 5;
		$('.fs_elem_05_nav li').click(function() {
			type = $(this).attr('data-type');
			
			$('.matherials_slider').addClass('hdn')
			$('.fs_elem_05_nav li').removeClass('opened');
			$(this).addClass('opened');
			
			$('.matherials_slider').each(function() {
				if ($(this).attr('data-type') == type) {
					$(this).removeClass('hdn')
				}
			});
			/* 
			
			 */
		});
	});
});

//слайдер отзывов на главной
if ($('.reviews_slider').length != 0) {
	$('.reviews_slider').slick({
		infinite: false,
		speed: 600,
		slidesToShow: 4,
		slidesToScroll: 1,
		swipeToSlide: true,
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				swipeToSlide: true,
			}
		}, {
			breakpoint: 992,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				swipeToSlide: true,
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				swipeToSlide: true,
			}
		}]
	});
}

//fancybox
$(document).ready(function() {
	if ($('.fancybox').length != 0) {
		$('.fancybox').fancybox({
			padding: 0
		});
	}
});

$(function() {
	$('#popup_reset_password_toggle, #popup_signup_toggle').click(function() {
		$('#popup_login').modal('toggle')
	});
});

//таблицы
$(function() {
    if ($('.smart_table').length != 0) {
        $('.smart_table').each(function() {
            if ($(this).closest('.smart_table_outer').length == 0) {
                $(".smart_table").wrap('<div class="smart_table_outer"></div>');
            }
        });

        $(".smart_table_outer").mCustomScrollbar({
            scrollbarPosition: 'outside',
            axis: 'yx',
            advanced: { autoExpandHorizontalScroll: true },
        });
        $(window).on("load",function(){
            $(".smart_table_outer").mCustomScrollbar('update');
        });
    }
    if ($('.simple_table').length != 0) {
        $('.simple_table').each(function() {
            if ($(this).closest('.simple_table_outer').length == 0) {
                $(".simple_table").wrap('<div class="simple_table_outer"></div>');
            }
        });

		/* $(".simple_table_outer").mCustomScrollbar({
		 scrollbarPosition: 'outside',
		 axis: 'x',
		 advanced: { autoExpandHorizontalScroll: true },
		 });
		 $(window).on("load",function(){
		 $(".simple_table_outer").mCustomScrollbar('update');
		 }); */
    }
});

//скроллинг левого меню
$(function() {
    if ($('.main_sidemenu').length != 0) {
        var sidemenu = $('.main_sidemenu').clone();
        if ($('.header .main_sidemenu').length == 0) {
            $('.header .main_menu_container').append(sidemenu);
        }

        $('.main_sidemenu_inner').each(function() {
            $(this).mCustomScrollbar({
                scrollbarPosition: 'outside'
            });
        });
        $(window).on("load",function(){
            $(".main_sidemenu_inner").mCustomScrollbar('update');
        });

        $('.main_sidemenu_link').on('click mouseenter', function() {
            var ths = $(this);
            setTimeout(function() {
                ths.closest('.main_sidemenu_element').find('.main_sidemenu_inner').mCustomScrollbar('update');
            }, 310);
        });


        var scrl = function() {
            if ($(window).outerWidth() >= 751) {
                var scrl_top = $(window).scrollTop();
                var window_height = $(window).outerHeight();

                var sidebar_height = $('.main_sidemenu').outerHeight();
                var sidebar_offset = $('.main_sidemenu').offset().top;

                var header_height = $('.header').outerHeight();
                var footer_offset = $('.footer').offset().top;

                var offset_constant_1 = 36; //константа отступа сверху, отлична от нуля при ширине экрана больше 992px

                var bottom_position = footer_offset - window_height + header_height + sidebar_height - 8;

                if (scrl_top < offset_constant_1) {
                    $('.main_sidemenu').removeClass('fixed');
                    $('.main_sidemenu').addClass('top');
                    $('.main_sidemenu').removeClass('bottom');
                    $('.main_sidemenu').removeAttr('style');
                } else if ((scrl_top >= offset_constant_1) && (scrl_top < bottom_position)) {
                    $('.main_sidemenu').addClass('fixed');
                    $('.main_sidemenu').removeClass('top');
                    $('.main_sidemenu').removeClass('bottom');
                    $('.main_sidemenu').removeAttr('style');
                } else {
                    $('.main_sidemenu').removeClass('fixed');
                    $('.main_sidemenu').removeClass('top');

                    if (scrl_top >= bottom_position) {
                        $('.main_sidemenu').addClass('bottom');
                        $('.main_sidemenu').css('top', footer_offset - sidebar_height - header_height + 8);
                    } else {
                        $('.main_sidemenu').removeClass('bottom');
                        $('.main_sidemenu').removeAttr('style');
                    }
                }
            } else {
                $('.main_sidemenu').removeClass('fixed');
                $('.main_sidemenu').removeClass('top');
                $('.main_sidemenu').removeClass('bottom');
                $('.main_sidemenu').removeAttr('style');
            }
        }

        scrl();
        $(window).on('load resize scroll', scrl);
    }
});

//календарь
$(function() {
    if ($('.datepicker').length != 0) {
        $('.datepicker').each(function() {
            $(this).datepicker({
                language: 'ru',
                startDate: '10/22/1996',
                endDate: '0d',
                templates: {
                    leftArrow: '<span class="icon icon-arr_left"></span>',
                    rightArrow: '<span class="icon icon-arr_right"></span>'
                }
            });
        });
    }
});

//календарь
$(function() {
    if ($('#calendar').length != 0) {
        var monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
        var dayNames = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];
        var events = [
            { "date": "28/10/2017", "link": "http://fin-plan.org/blog/2017/9/13/", "linkTarget": "_blank"},
            { "date": "29/10/2017", "link": "http://fin-plan.org/blog/2017/8/1/", "linkTarget": "_blank"},
            { "date": "30/10/2017", "link": "http://fin-plan.org/blog/2017/8/15/", "linkTarget": "_blank"},
        ];

        $('#calendar').bic_calendar({
            events: events,
            dayNames: dayNames,
            monthNames: monthNames,
            enableSelect: false,
            multiSelect: false,
            showDays: true,
            displayMonthController: true,
            displayYearController: true,
        });
    }
});

//запрет на ввод пробелов
$('input[type="text"], input[type="password"], textarea').bind("change keyup input", function() {
    if (($(this).attr('name') == 'mail') || ($(this).attr('name') == 'email')) {
        if (this.value.match(/\s+/g)) {
            this.value = this.value.replace(/\s+/g, '');
        }
    }
});
//-------

//ui sliders
if ($('.ui_slider').length != 0) {
    var slider = $('.ui_slider').each(function() {
        var min_val, max_val, step, slider_input;
        if ($(this).hasClass('time_slider')) {
            min_val = 6;
            max_val = 37;
            slider_input = $('#time_slider_input');
        } else if ($(this).hasClass('rate_slider')) {
            min_val = 5;
            max_val = 26;
            slider_input = $('#rate_slider_input');
        }

        changeVal = function(event, ui) {
            slider_input.val($(this).slider('value'));

            var block = slider_input.siblings('.rubber_block');
            var block_outer = $(this).closest('.ui_slider_outer').find('.rubber_outer');

            block.text(slider_input.val());
            slider_input.width(block.width());

            //----------
            if (slider_input.closest('.time_slider_outer').length != 0) {
                var rubber_width = slider_input.closest('.rubber_outer').outerWidth();
                slider_input.closest('.time_slider_outer').css('padding-right', rubber_width + 15);
            } else
            if (slider_input.closest('.rate_slider_outer').length != 0) {
                var rubber_width = slider_input.closest('.rubber_outer').outerWidth();
                slider_input.closest('.rate_slider_outer').css('padding-right', rubber_width + 15);
            }

            if ($(this).slider('value') > max_val - 1) {
                block_outer.find('.prefix').text('>');
                slider_input.val(max_val - 1);
            } else {
                var prefix = block_outer.find('input.rubber').attr('data-prefix');
                block_outer.find('.prefix').text(prefix);
            }
            //----------

            if ($(this).closest('.ui_slider_outer').hasClass('time_slider')) {
                if ($(this).val() == 37) {

                }
            }
        }

        $(this).slider({
            min: min_val,
            max: max_val,
            range: "min",
            create: changeVal,
            change: changeVal,
            slide: changeVal
        });

        step = parseInt(max_val / 5);
        for (var i = 0; i < 5; i ++) {
            $(this).siblings('.ui_slider_scale').find('li').eq(i).text(min_val + (step * i));
        }

        slider_input.bind("change keyup input click", function() {
            //запрет на ввод всего кроме цифр
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }

            slider_input.closest('.col').find('.ui_slider').slider("value", slider_input.val());
        });
    });
}
//-------

//текстовые поля с процентным постфиксом
if ($('.percents_input').length != 0) {
    $('.percents_input').bind("change keyup input", function(eventObject) {
        //запрет на ввод всего кроме цифр
        if ((eventObject.which != 37) && (eventObject.which != 39)) {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }

            refill = $(this).val();
            $(this).val(refill + '%');
        }

        //постановка курсора перед постфиксом текстового поля
        function setSelectionRange(input, selectionStart, selectionEnd) {
            if (input.setSelectionRange) {
                input.setSelectionRange(selectionStart, selectionEnd);
            } else if (input.createTextRange) {
                var range = input.createTextRange();
                range.collapse(true);
                range.moveEnd('character', selectionEnd);
                range.moveStart('character', selectionStart);
                range.select();
            }
        }
        function setCaretToPos(input, pos) {
            setSelectionRange(input, pos, pos);
        }
        setCaretToPos($(this)[0], refill.length);
        //-------

        if (refill == '') {
            refill = 0;
        } else {
            refill = parseInt(refill);
        }
    });
}

if ($('.modal_black').length != 0) {
    $('.modal_black').on('show.bs.modal', function (e) {
        setTimeout(function() {
            $('.modal-backdrop').addClass('modal_black_backdrop');
        }, 100);
    })
}

//форма подсчета
$(function() {
    if ($('.calculate_outer').length != 0) {
        var money_main,		//Сумма инвестиций
            money_in_work,	//Распределено
            money_free,		//Не распределено
			/* balance = [],	 */ //прирост за месяц
            refill = 0,			//размер ежемесячного пополнения счёта
            main_arr = [],	//финальный массив для вывода на график
            main_arr_curr = [],	//элемент финального массива

            obligation_name,//имя текущей облигации
            obligation_elem = [],// массив из имени и общей цены выбранной облигации
            obligations_arr = [],// массив из выбранных облигаций
            month_summ,		// сумма цен покупки выбранных облигаций за текущий месяц

            profit_main,	//общая доходность
            profit_year,	//годовая доходность

            elem_buy_price_one,	//Цена покупки одной текущей облигации
            elem_buy_price,	//Цена покупки текущей облигации * количество лотов
            numberof,		//Кол-во лотов текущей облигации
            currency_value, //Курс текущей валюты
            currency_text;	//Текущая валюта, текст

        var date_today,		//сегодня
            date_today_day,
            date_today_month,
            date_today_year,
            date_off,		//дата гашения
            date_off_today,	//количество дней между датой гашения и сегодня
            dates_arr = [], //массив из сегодня и дат гашения облигаций
            date_max,		//максимальная дата гашения в массиве дат гашения
            dates_period_arr = [], //массив из первых дней каждого месяца между сегодня и датой гашения, так же включает в себя сегодня

            months_final;	//количество месяцев в промежутке между сегодня и максимальной датой

        var donut_arr = [];
        var donut_arr_text = [];
        var donut_arr_values = [];
        var curr_donut_arr = [];
        var curr_donut_val;

        var money_in_work_arr = [];
        var main_arr_values = [];
        var refill_arr_values = [];

        var pie, chart;

        //цвета для кругового гоафика
        var colors_big_arr = [];
        for (var i = 0; i < 100; i++) {
            var curr_donut_color_letter = '0123456789ABCDEF'.split('');
            var curr_donut_color = '#';
            for (var j = 0; j < 6; j++ ) {
                curr_donut_color += curr_donut_color_letter[Math.floor(Math.random() * 16)];
            }
            colors_big_arr.push(curr_donut_color);
        }
        console.log('Массив цветов: ', colors_big_arr);
        //-------

        //цены
        var priceTextChange = function() {
            money_main = parseFloat($('#money_main').val());

            money_in_work = parseFloat($('#money_in_work').val());
            money_free = money_main - money_in_work;

            $('#money_main_text').text(money_main.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
            $('#money_in_work_text').text(money_in_work.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
            $('#money_free_text').text(money_free.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
        }

        var all_income = 0;
        var allIncomeChange = function() {
            all_income = all_income.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
            $('#all_income_text').text(all_income);
        }
        allIncomeChange();
        //-------

        //попап указания суммы инвестиций

        $('#popup_investment_summ').bind("change keyup input", function(eventObject) {
            var txt;
            //запрет на ввод всего кроме цифр
            if ((eventObject.which != 37) && (eventObject.which != 39)) {
                if (this.value.match(/[^0-9]/g)) {
                    this.value = this.value.replace(/[^0-9]/g, '');
                }

                txt = $(this).val();
                txt = txt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				/* $('#money_in_work_text').text(money_in_work.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")); */
                $(this).val(txt + ' руб.');
            }

            //постановка курсора перед постфиксом текстового поля
            function setSelectionRange(input, selectionStart, selectionEnd) {
                if (input.setSelectionRange) {
                    input.setSelectionRange(selectionStart, selectionEnd);
                } else if (input.createTextRange) {
                    var range = input.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', selectionEnd);
                    range.moveStart('character', selectionStart);
                    range.select();
                }
            }
            function setCaretToPos(input, pos) {
                setSelectionRange(input, pos, pos);
            }
            setCaretToPos($('#popup_investment_summ')[0], txt.length);
        });

        $('.popup_refill_change_summ').click(function() {
            setTimeout(function() {
                $('#popup_investment_amount').modal('show');
                var val = $('#popup_investment_summ').val();
                $('#popup_investment_summ').attr('data-canonical', val);
            }, 600);
        });

        if ($.cookie('popup_cookie') != 1) {
            $.cookie('popup_cookie', 0, {expires: 1, path: '/'});
            $('#popup_investment_amount').modal('show');
        } else {
            $('#money_main').val($.cookie('summ_cookie'));
            priceTextChange();
        }
        console.log('cookie: ' + $.cookie('popup_cookie'));
        console.log('cookie: ' + $.cookie('summ_cookie'));

        $('.popup_refill_submit').click(function() {
            var new_val = $('#popup_investment_summ').val();
            new_val = new_val.replace(/[^0-9]/g, '');
            new_val = parseFloat(new_val);

            var canonical_val = $('#popup_investment_summ').attr('data-canonical');
            canonical_val = parseFloat(canonical_val);

            var in_work_val = $('#money_in_work').val();
            in_work_val = parseFloat(in_work_val);

            if (new_val < in_work_val) {
                $('#popup_investment_summ').tooltip({
                    html: true,
                    placement: 'top',
                    trigger: 'manual',
                    title: 'Значение суммы инвестиций должно быть больше текущей суммы распределённых средств! <br/></br><span class="strong">В данный момент распределено: ' + in_work_val + ' руб.</span>',
                });

                $('#popup_investment_summ').tooltip('show')
            } else {
                $('#popup_investment_summ').tooltip('hide')
                $('#popup_investment_amount').modal('hide');
            }
        })

        $('#popup_investment_amount').on('hide.bs.modal', function(e) {
            var val = $('#popup_investment_summ').val();
            val = val.replace(/[^0-9]/g, '');
            val = parseInt(val);
            $.cookie('summ_cookie', val, {expires: 1, path: '/'});
            $('#money_main').val(val);
            priceTextChange();
            $.cookie('popup_cookie', 1, {expires: 1, path: '/'});
            console.log('cookie: ' + $.cookie('popup_cookie'));
            console.log('cookie: ' + $.cookie('summ_cookie'));
        })
        //----------

        //доходность
        $('.calculate_table table tbody tr').each(function() {
            var tr = $(this);
            date_today = new Date();

            tr.find('.numberof').attr('data-working_value', tr.find('.numberof').val());

            if (tr.attr('data-currency') == 'rub') {
                currency_value = 1;
            } else if (tr.attr('data-currency') == 'usd') {
                currency_value = parseFloat($('#usd_currency').val());
            } else if (tr.attr('data-currency') == 'eur') {
                currency_value = parseFloat($('#eur_currency').val());
            }

            var profit_current = parseFloat(tr.find('.elem_sell_price').attr('data-price')) * currency_value;

            var elem_buy_price_one_current = parseFloat(tr.find('.elem_buy_price').attr('data-price')) * currency_value;

            var profit_main_current = ((profit_current - elem_buy_price_one_current) / elem_buy_price_one_current) * 100;

            var date1 = date_today;
            var date2 = new Date(tr.find('.date_off').attr('data-date'));
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            date_off_today = Math.ceil(timeDiff / (1000 * 3600 * 24));

            var profit_year_current = (profit_main_current / date_off_today) * 365;

            tr.find('.profit_year').text(parseFloat(profit_year_current.toFixed(1)) + '%');
            tr.find('.profit_year').attr('data-profit_year', profit_year_current);

            tr.find('.profit_main').text(parseFloat(profit_main_current.toFixed(1)) + '%');
            tr.find('.profit_main').attr('data-profit_main', profit_main_current);
        });
        //-------

        var dateTextWrite = function() {
            $('.calculate_table .date_off').each(function() {
                var curr_date = $(this).attr('data-date');
                var curr_date_arr = curr_date.split('-');

                if (curr_date_arr[1].length < 2)
                    curr_date_arr[1] = '0' + curr_date_arr[1];
                if (curr_date_arr[2].length < 2)
                    curr_date_arr[2] = '0' + curr_date_arr[2];

                $(this).text(curr_date_arr[2] + '.' + curr_date_arr[1] + '.' + curr_date_arr[0])
            });
        }
        dateTextWrite();
        //-------

        //линейный график - инициализация
        var linear_chart = document.getElementById("linear_chart");

        var labels_arr = ['Сегодня', '6 мес', '12 мес', '18 мес', '24 мес'];
        var chart_data = {
            type: 'line',
            data: {
                labels: labels_arr,
                datasets: []
            },
            options: {
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return tooltipItems.yLabel + ' руб.';
                        }
                    }
                },
                hover: {
                    mode: 'index',
                    intersect: false,
                },
            }
        }

        var linearChart = new Chart(linear_chart, chart_data);
        //-------

        //круговой график - инициализация
        var round_chart = document.getElementById("round_chart");

        var round_labels_arr = ['Не распределено'];
        //круговой график
        var donut_money_in_work = (money_in_work * 100) / money_main;
        donut_money_in_work = parseInt(donut_money_in_work);
        var donut_money_free = 100 - donut_money_in_work;
        donut_money_free = parseInt(donut_money_free);

        var round_chart_data = {
            type: 'doughnut',
            data: {
                labels: [round_labels_arr],
                datasets: [{
                    data: [100],
                    borderWidth: 1,
                    backgroundColor: '#a6db16',
                }]
            },
            options: {
                legend: {
                    display: false
                },
                rotation: 4.72,
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var allData = data.datasets[tooltipItem.datasetIndex].data;
                            var tooltipLabel = data.labels[tooltipItem.index];
                            var tooltipData = allData[tooltipItem.index];
                            var total = 0;
                            for (var i in allData) {
                                total += allData[i];
                            }
                            var tooltipPercentage = Math.round((tooltipData / total) * 100);
                            return tooltipLabel + ': ' + tooltipData + '%';
                        }
                    }
                },
            }
        }

        var roundChart = new Chart(round_chart, round_chart_data);
        //-------

        var ths;
        var tr;

        var asd = 0;
        var curr_element_tag;
        $('.calculate_table .checkbox input').change(function() {
            curr_ths = $(this);
            curr_tr = curr_ths.closest('tr');

            if (curr_ths.attr('id') == 'calculate_table_all') {
                curr_ths_ths = $(this);
                curr_element_tag = 'input_checkbox_all';
                $('.calculate_table tbody .checkbox input').each(function() {
                    curr_ths = $(this);
                    curr_tr = curr_ths.closest('tr');

                    if (curr_ths_ths.prop('checked')) {
                        curr_ths.prop('checked', true);

                        curr_tr.css('opacity', 0);
                        curr_tr.addClass('checked');

                        setTimeout(function() {
                            $('.calculate_table tbody').prepend(curr_tr);
                        }, 300);
                        setTimeout(function() {
                            curr_tr.removeAttr('style');
                        }, 310);
                    } else {
                        curr_ths.prop('checked', false);

                        curr_tr.removeClass('checked');
                        curr_tr.css('opacity', 0);

                        setTimeout(function() {
                            for (var i = 0; i < $('.calculate_table tr.checked').length; i ++) {
                                $('.calculate_table tr.checked').eq(i).after(curr_tr);
                            }
                        }, 300);
                        setTimeout(function() {
                            curr_tr.removeAttr('style');
                        }, 310);
                    }

                    summCheck();
                });
                $('.calculate_table tbody tr').removeAttr('style');
            } else {
                curr_element_tag = 'input_checkbox';
                summCheck();
                if (asd > 0) {
                    summCheck();
                }
            }
        });
        $('.numberof').bind("change", function(eventObject) {
            //запрет на ввод всего кроме цифр
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }

            curr_element_tag = 'input_text';

            summCheck();
            if (asd > 0) {
                summCheck();
            }
        });
        //-------

        //ежемесячное пополнение счета
        $('#refill_value').bind("change keyup input", function(eventObject) {
            //запрет на ввод всего кроме цифр
            if ((eventObject.which != 37) && (eventObject.which != 39)) {
                if (this.value.match(/[^0-9]/g)) {
                    this.value = this.value.replace(/[^0-9]/g, '');
                }

                refill = $(this).val();
                $(this).val(refill + ' руб.');
            }

            //постановка курсора перед постфиксом текстового поля
            function setSelectionRange(input, selectionStart, selectionEnd) {
                if (input.setSelectionRange) {
                    input.setSelectionRange(selectionStart, selectionEnd);
                } else if (input.createTextRange) {
                    var range = input.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', selectionEnd);
                    range.moveStart('character', selectionStart);
                    range.select();
                }
            }
            function setCaretToPos(input, pos) {
                setSelectionRange(input, pos, pos);
            }
            setCaretToPos($('#refill_value')[0], refill.length);
            //-------

            if (refill == '') {
                refill = 0;
            } else {
                refill = parseInt(refill);
            }
        });
        $('#replenishment').change(function() {
            if ($('#replenishment').prop('checked')) {
                $('#popup_refill').modal('show');
            } else {
                $('#refill_value').val('');
                refill = 0;

                curr_element_tag = 'input_repl';
				/* curr_ths = $(this).closest('tr').find('.checkbox input');
				 curr_tr = curr_ths.closest('tr'); */

                summCheck();
                if (asd > 0) {
                    summCheck();
                }
            }
        });
        $('#iis').change(function() {
            summCheck();
        });
        $('.popup_refill_submit').click(function() {
            curr_element_tag = 'input_repl';

            summCheck();
            if (asd > 0) {
                summCheck();
            }

            console.log('Размер ежемесячного пополнения счёта', refill);
        });
        $('#popup_refill .close').click(function() {
            $('#replenishment').prop('checked', false);
            $('#popup_refill').modal('hide');

            summCheck();
            if (asd > 0) {
                summCheck();
            }
        });
        //-------

        //проверка на превышение суммы инвестиций
        var summCheck = function() {
            //перемещение строк в таблице
            if (curr_element_tag == 'input_checkbox') {
                if (curr_ths.closest('tbody').length != 0) {
                    if (curr_ths.prop('checked')) {
                        curr_tr.css('opacity', 0);
                        curr_tr.addClass('checked');

                        setTimeout(function() {
                            $('.calculate_table tbody').prepend(curr_tr);
                        }, 300);
                        setTimeout(function() {
                            curr_tr.removeAttr('style');
                        }, 310);
                    } else {
                        curr_tr.removeClass('checked');
                        curr_tr.css('opacity', 0);

                        setTimeout(function() {
                            for (var i = 0; i < $('.calculate_table tr.checked').length; i ++) {
                                $('.calculate_table tr.checked').eq(i).after(curr_tr);
                            }
                        }, 300);
                        setTimeout(function() {
                            curr_tr.removeAttr('style');
                        }, 310);
                    }
                }
            }
            //-------

            //обнуление переменных и массивов
            money_in_work = 0;
            dates_arr = [];
            donut_arr = [];
            donut_arr_text = [];
            donut_arr_values = [];
            donut_arr_colors = [];
            curr_donut_arr = [];

            var obligation_elements = [];
            main_arr_curr = [];
            //-------

            //массив дат гашения облигаций - занесение сегодняшней даты
            date_today = new Date();
            date_today_day;
            date_today_month;
            date_today_year;

            if (dates_arr[0] != date_today) {
                date_today_day = date_today.getDate();
                date_today_day = date_today_day.toString();
                if (date_today_day.length < 2) {
                    date_today_day = '0' + date_today_day;
                }

                date_today_month = date_today.getMonth() + 1;
                date_today_month = date_today_month.toString();
                if (date_today_month.length < 2) {
                    date_today_month = '0' + date_today_month;
                }

                date_today_year = date_today.getFullYear();

                dates_arr.push(date_today_year + '-' + date_today_month + '-' + date_today_day);
            }
            console.log('Маccив дат гашения облигаций: ', dates_arr);
            //-------

            $('.calculate_table tbody .checkbox input').each(function(index) {
                //массив из данных для линейного графика
                money_in_work_arr = [];
                main_arr_values = [];
                refill_arr_values = [];
                curr_donut_arr = [];

                obligation_elem = [];
                obligations_arr = [];
                main_arr = [];

                ths = $(this);
                tr = ths.closest('tr');

                if (ths.prop('checked')) {
                    //подсчет распределенных и нераспределенных средств
                    if (ths.prop('checked')) {
                        if (ths.closest('tbody').length != 0) {
                            elem_buy_price_one = parseFloat(tr.find('.elem_buy_price').attr('data-price'));
                            if (tr.attr('data-currency') == 'rub') {
                                currency_value = 1;
                            } else if (tr.attr('data-currency') == 'usd') {
                                currency_value = parseFloat($('#usd_currency').val());
                            } else if (tr.attr('data-currency') == 'eur') {
                                currency_value = parseFloat($('#eur_currency').val());
                            }
                        }
                    }
                    //-------

                    numberof = parseInt(tr.find('.numberof').val());

                    elem_buy_price_one *= currency_value;
                    elem_buy_price = elem_buy_price_one * numberof;


                    //добавление текущей даты гашения в массив дат гашения
                    date_off = new Date(tr.find('.date_off').attr('data-date'));

                    var date_off_day = date_off.getDate();
                    date_off_day = date_off_day.toString();
                    if (date_off_day.length < 2) {
                        date_off_day = '0' + date_off_day;
                    }

                    var date_off_month = date_off.getMonth() + 1;
                    date_off_month = date_off_month.toString();
                    if (date_off_month.length < 2) {
                        date_off_month = '0' + date_off_month;
                    }

                    var date_off_year = date_off.getFullYear();

                    var date_off_text = date_off_year + '-' + date_off_month + '-' + date_off_day;

                    dates_arr.push(date_off_text);

                    ths.attr('data-canonical', elem_buy_price);

                    console.log('Маccив дат гашения облигаций: ', dates_arr);

                    //нахождение максимальной даты в массиве дат
                    var dates_native_arr = [];

                    for (var i = 0; i < dates_arr.length; i++) {
                        dates_native_arr.push(new Date(dates_arr[i]));
                    }

                    date_max = new Date(Math.max.apply(null, dates_native_arr));
                    dates_native_arr = [];
                    console.log('Максимальная дата: ', date_max);
                    //-------

                    //подсчет количества месяцев в промежутке между сегодня и максимальной датой, между сегодня и датой гашения
                    function dateDiff() {
                        date1 = date_today;
                        date2 = date_max;

                        var months = date2.getMonth() - date1.getMonth();
                        if ( months < 0 ) {
                            months += 12;
                            date2.setFullYear( date2.getFullYear() - 1 );
                        }

                        var years = date2.getFullYear() - date1.getFullYear();

                        if (years < 1) {
                            months_final = months;
                        } else {
                            months_final = months + (years * 12);
                        }

                        return months_final;
                    }
                    var dateDiff2 = function() {
                        var date1 = date_today;
                        var date2 = date_off;
                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        date_off_today = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    }
                    dateDiff2();

                    months_final = dateDiff()
                    console.log('Количество месяцев: ', months_final)
                    //-------
                    //формирование второго массива дат с периодом в 1 месяц
                    var curr_period_date = new Date();

                    var l = 0;
                    dates_period_arr = [];
                    if (dates_period_arr[0] != date_today) {
                        var date_today_day = date_today.getDate();
                        date_today_day = date_today_day.toString();
                        if (date_today_day.length < 2) {
                            date_today_day = '0' + date_today_day;
                        }

                        var date_today_month = date_today.getMonth() + 1;
                        date_today_month = date_today_month.toString();
                        if (date_today_month.length < 2) {
                            date_today_month = '0' + date_today_month;
                        }

                        var date_today_year = date_today.getFullYear();

                        dates_period_arr.push(date_today_year + '-' + date_today_month + '-' + date_today_day);
                    }

                    for (var k = 0; k < months_final; k++) {
                        curr_period_date.setMonth(curr_period_date.getMonth() + 1);

                        var date_curr_period_day = curr_period_date.getDate();
                        date_curr_period_day = date_curr_period_day.toString();
                        if (date_curr_period_day.length < 2) {
                            date_curr_period_day = '0' + date_curr_period_day;
                        }

                        var date_curr_period_month = curr_period_date.getMonth() + 1;
                        date_curr_period_month = date_curr_period_month.toString();
                        if (date_curr_period_month.length < 2) {
                            date_curr_period_month = '0' + date_curr_period_month;
                        }

                        var date_curr_period_year = curr_period_date.getFullYear();

                        var date_curr_period_final = date_curr_period_year + '-' + date_curr_period_month + '-' + '01';

                        if (date_curr_period_final != dates_period_arr[k]) {
                            l++;
                        }
                        if (l == dates_period_arr.length) {
                            dates_period_arr.push(date_curr_period_final);
                        }
                    }

                    console.log('Маccив дат периодом в месяц: ', dates_period_arr);
                    //-------

                    var money_in_work_old = money_in_work;
                    money_in_work += elem_buy_price;

                    if (money_main < money_in_work) {
						/* alert('Вы превысили значение суммы инвестиций!'); */
                        $('#popup_money_error').modal('show');
                        curr_ths.closest('table').addClass('table_error');

                        if (curr_element_tag == 'input_checkbox' || curr_element_tag == 'input_checkbox_all') {
                            money_in_work -= elem_buy_price;

                            curr_ths.closest('tr').addClass('error');
                            curr_ths.prop('checked', false);

                            curr_tr.removeClass('checked');
                            curr_tr.css('opacity', 0);

                            setTimeout(function() {
                                for (var i = 0; i < $('.calculate_table tr.checked').length; i ++) {
                                    $('.calculate_table tr.checked').eq(i).after(curr_tr);
                                }
                            }, 300);
                            setTimeout(function() {
                                curr_tr.removeAttr('style');
                            }, 310);
                        } else if (curr_element_tag == 'input_text') {
                            money_in_work = money_in_work_old;
                        }
                    } else {
                        var money_in_work_text = money_in_work.toFixed(2);

                        $('#money_in_work').val(money_in_work_text);
                        priceTextChange();
                        //-------

                        curr_ths.closest('tr').removeClass('error');
                    }

                    //----------------------------
                    //----------------------------
                    //----------------------------

                    if (!tr.hasClass('error')) {
                        obligation_name = tr.find('.obligation_name').text();

                        profit_main = parseFloat(tr.find('.profit_main').attr('data-profit_main'));

                        profit_year = parseFloat(tr.find('.profit_year').attr('data-profit_year'));

                        obligation_elem.push(obligation_name, elem_buy_price, profit_main, profit_year, date_off_today, date_off);

                        obligation_elements.push(obligation_elem);

                        date_off_text = date_today_year + '-' + date_today_month + '-' + date_today_day;
                    }


                    //массив значений для кругового графика
                    curr_donut_val = (elem_buy_price * 100) / money_main;
                    curr_donut_arr.push(obligation_name, curr_donut_val, colors_big_arr[index]);
                    donut_arr.push(curr_donut_arr);
                }
            });

            if ($('.calculate_table tbody .checkbox input').prop('checked').length == 0) {
                $('#money_in_work').val(0);
                priceTextChange();
            }

            var free_val_summ = 0;
            for (var k = 0; k < donut_arr.length; k++) {
                free_val_summ += donut_arr[k][1];
            }
            free_val_summ = 100 - free_val_summ;
            free_val_arr = ['Не распределено', free_val_summ, '#a6db16'];
            donut_arr.push(free_val_arr);

            for (var i = 0; i < donut_arr.length; i++) {
                donut_arr_text.push(donut_arr[i][0])
                donut_arr_values.push(donut_arr[i][1].toFixed(1))
                donut_arr_colors.push(donut_arr[i][2])
            }
            console.log('Массив данных для кругового графика: ', donut_arr);

            if (ths.closest('table').hasClass('table_error')) {
                $('.calculate_table .numberof').each(function() {
                    $(this).val(parseInt($(this).attr('data-working_value')));
                });

                ths.closest('table').removeClass('table_error');

                asd = 1;
            } else {
                $('.calculate_table .numberof').each(function() {
                    $(this).attr('data-working_value', $(this).val());
                });

                asd = 0;
            }

            //отрисовка выбранных облигаций
            for (var i = 0; i < dates_period_arr.length; i++) {
                var date_period_current = new Date(dates_period_arr[i]);
                var date_period_before = new Date(dates_period_arr[i - 1]);

                var dateDiff3 = function() {
                    var date1 = date_period_current;
                    var date2 = date_period_before;
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    date_off_today = Math.ceil(timeDiff / (1000 * 3600 * 24));

                    return date_off_today;
                }

                var date_today_before = dateDiff3();

                if (i == 0) {
                    main_arr_curr.push(dates_period_arr[i]);

                    main_arr_curr.push(obligation_elements);

                    month_summ = 0;
                    for (var j = 0; j < obligation_elements.length; j++) {
                        month_summ += obligation_elements[j][1];
                    }

                    var iis = 0;

                    main_arr_curr.push(month_summ);
                    main_arr_curr.push(money_in_work);
                    main_arr_curr.push(iis);
                    main_arr.push(main_arr_curr);
                } else {
                    if (obligation_elements.length > 0) {
                        var Arr1 = main_arr[i - 1][1];
                        var Arr2 = JSON.parse(JSON.stringify(Arr1));

                        var curr_refill = main_arr[i - 1][3];

                        var second_month_summ = 0;
                        for (var j = 0; j < Arr1.length; j++) {
                            var curr_obl_price = Arr1[j][1];
                            var curr_date_off_today = Arr1[j][4];
                            var curr_profit_main = Arr1[j][2];
                            var curr_date_off = new Date(Arr1[j][5]);

                            var main_formula;
                            if (i == 1) {
                                if (date_period_current <= date_off) {
                                    main_formula = ((((curr_obl_price * curr_profit_main) / 100) / (curr_date_off_today)) * date_today_before) + curr_obl_price + refill;
                                } else {
                                    main_formula = Arr1[j][1];
                                }
                            } else if (i > 1) {
                                if (date_period_current <= curr_date_off) {
                                    var first_obl_price = main_arr[0][1][j][1];
                                    main_formula = ((((first_obl_price * curr_profit_main) / 100) / (curr_date_off_today)) * date_today_before) + curr_obl_price + refill;
                                } else {
                                    main_formula = Arr1[j][1];
                                }
                            }

                            Arr2[j][1] = main_formula;

                            second_month_summ += Arr2[j][1];

                            var second_main_arr = [];
                            second_main_arr.push(dates_period_arr[i], Arr2, second_month_summ, curr_refill + refill);
                        }

                        var iis = main_arr[i - 1][4];

                        iis_curr_date = new Date(second_main_arr[0]);

                        var iis_curr_date_month = iis_curr_date.getMonth() + 1;

                        var iis_curr_date_year = iis_curr_date.getFullYear();

                        if ($('#iis').prop('checked')){
                            if (iis_curr_date_month == 3 && iis_curr_date_year == date_today_year + 1) {
                                iis = second_main_arr[3] * 0.13;
                                if (iis > 52000) {
                                    iis = 52000;
                                }
                            }
                        }

                        second_main_arr[2] += iis;
                        second_main_arr.push(iis);

                        main_arr.push(second_main_arr);
                    }
                }
            }
            console.log('Финальный массив данных: ', main_arr);

            linearChart.data.labels = [];
            for (var i = 0; i < main_arr.length; i++) {
                money_in_work_arr.push(money_in_work);
                main_arr_values.push(parseFloat(main_arr[i][2].toFixed(2)));
                refill_arr_values.push(main_arr[i][3]);
                linearChart.data.labels.push(main_arr[i][0]);
            }

            console.log('----------------');
            //-------

            //перерисовка графиков
            linearChart.data.datasets = [];
            if ($('#replenishment').prop('checked')) {
                linearChart.data.datasets.push({
                    label: "Тело долга",
                    backgroundColor: 'rgb(166, 219, 22)',
                    borderColor: 'rgb(166, 219, 22)',
                    data: money_in_work_arr,
                    radius: 0
                }, {
                    label: "Ежемесячное пополнение счета",
                    backgroundColor: 'rgb(5, 149, 72)',
                    borderColor: 'rgb(5, 149, 72)',
                    data: refill_arr_values,
                    radius: 0
                }, {
                    label: "Доход от облигаций",
                    backgroundColor: 'rgb(238, 211, 36)',
                    borderColor: 'rgb(238, 211, 36)',
                    data: main_arr_values,
                    radius: 0
                });
            } else {
                linearChart.data.datasets.push({
                    label: "Тело долга",
                    backgroundColor: 'rgb(166, 219, 22)',
                    borderColor: 'rgb(166, 219, 22)',
                    data: money_in_work_arr,
                    radius: 0
                }, {
                    label: "Доход от облигаций",
                    backgroundColor: 'rgb(238, 211, 36)',
                    borderColor: 'rgb(238, 211, 36)',
                    data: main_arr_values,
                    radius: 0
                });
            }
            linearChart.update();

            donut_money_in_work = (money_in_work * 100) / money_main;
            donut_money_in_work = parseInt(donut_money_in_work);
            donut_money_free = 100 - donut_money_in_work;
            donut_money_free = parseInt(donut_money_free);
            roundChart.data.labels = [];
            roundChart.data.datasets = [];

            for (var i = 0; i < donut_arr_text.length; i++) {
                roundChart.data.labels.push(donut_arr_text[i]);
            }
            roundChart.data.datasets.push({
                data: donut_arr_values,
                borderWidth: 1,
                backgroundColor: donut_arr_colors
            });
            roundChart.update();
            //-------

            //вывод дохода за весь период
            var main_arr_0, main_arr_1;
            if (main_arr.length != 0) {
                for (var i = 0; i < main_arr.length; i ++) {
                    if (i == main_arr.length - 1) {
                        main_arr_0 = main_arr[i][2];
                        main_arr_1 = main_arr[i][3];
                    }
                }
            } else {
                main_arr_0 = 0;
                main_arr_1 = 0;
            }
            all_income = main_arr_0 - main_arr_1;
            allIncomeChange();
        }
        //-------


        var inputChange;
        //резиновый input
        if ($('input[type="text"], input[type="password"]').hasClass('rubber')) {
            $('input.rubber').each(function() {
                if ($(this).parent('.rubber_outer').length == 0) {
                    $(this).wrap('<span class="rubber_outer"></span>');
                }
                if ($(this).siblings('.rubber_block').length == 0) {
                    $(this).closest('.rubber_outer').append('<span class="rubber_block"></span>')
                }
                if ($(this).siblings('.postfix').length == 0) {
                    $(this).closest('.rubber_outer').append('<span class="postfix">' + $(this).attr('data-postfix') + '</span>')
                }
                if ($(this).siblings('.prefix').length == 0) {
                    $(this).closest('.rubber_outer').prepend('<span class="prefix">' + $(this).attr('data-prefix') + '</span>')
                }

                var input = $(this),
                    block = $(this).siblings('.rubber_block');

                inputChange = function() {
                    block.text(input.val());
                    input.width(block.width());

                    //----------
                    if (input.closest('.time_slider_outer').length != 0) {
                        var rubber_width = input.closest('.rubber_outer').outerWidth();
                        input.closest('.time_slider_outer').css('padding-right', rubber_width + 15);
                    } else
                    if (input.closest('.rate_slider_outer').length != 0) {
                        var rubber_width = input.closest('.rubber_outer').outerWidth();
                        input.closest('.rate_slider_outer').css('padding-right', rubber_width + 15);
                    }
                    //----------
                }
                inputChange();
                input.on('change input', inputChange);
            });
        }

        //формирование блоков цены
        $('.elem_buy_price, .elem_sell_price').each(function() {
            elem_buy_price = $(this).attr('data-price').toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");

            if ($(this).closest('tr').attr('data-currency') == "rub") {
                currency_text = 'р.';
                $(this).text(elem_buy_price + ' ' + currency_text);
            } else if ($(this).closest('tr').attr('data-currency') == "usd") {
                currency_text = '$';
                $(this).text(currency_text + ' ' + elem_buy_price);
            } else if ($(this).closest('tr').attr('data-currency') == "eur") {
                currency_text = '€';
                $(this).text(currency_text + ' ' + elem_buy_price);
            }
        });
        //-------
    }
});

//вкладки фин анализа в фильтре на странице подсчета
$(function() {
    if ($('.calculate_outer').length != 0) {
        var ths;

        var tabsFunction = function() {
            var destination = '#' + ths.val();

            $('.tab_container').removeAttr('style');
            setTimeout(function() {
                $('.tab_container').removeClass('active');
                $('.tab_container' + destination).addClass('active');
            }, 300);
            setTimeout(function() {
                $('.tab_container' + destination).css('opacity', 1);
            }, 310);
        }

        ths = $('#financial_analysis_select')
        tabsFunction();

        $('#financial_analysis_select').change(function() {
            ths = $(this);
            tabsFunction();
        });
    }
});

//подгрузка контента в попап при клике на название облигации
$(function() {
    var barchartRefresh = function() {
        if ($('.article_bar_chart').length != 0) {
            ohlc = [
                ['06/15/2009 16:00:00', 136.01, 139.5, 134.53, 139.48],
                ['06/08/2009 16:00:00', 143.82, 144.56, 136.04, 136.97],
                ['06/01/2009 16:00:00', 136.47, 146.4, 136, 144.67],
                ['05/26/2009 16:00:00', 124.76, 135.9, 124.55, 135.81],
                ['05/18/2009 16:00:00', 123.73, 129.31, 121.57, 122.5],
                ['05/11/2009 16:00:00', 127.37, 130.96, 119.38, 122.42],
                ['05/04/2009 16:00:00', 128.24, 133.5, 126.26, 129.19],
                ['04/27/2009 16:00:00', 122.9, 127.95, 122.66, 127.24],
                ['04/20/2009 16:00:00', 121.73, 127.2, 118.6, 123.9],
                ['04/13/2009 16:00:00', 120.01, 124.25, 115.76, 123.42],
                ['04/06/2009 16:00:00', 114.94, 120, 113.28, 119.57],
                ['03/30/2009 16:00:00', 104.51, 116.13, 102.61, 115.99],
                ['03/23/2009 16:00:00', 102.71, 109.98, 101.75, 106.85],
                ['03/16/2009 16:00:00', 96.53, 103.48, 94.18, 101.59],
                ['03/09/2009 16:00:00', 84.18, 97.2, 82.57, 95.93],
                ['03/02/2009 16:00:00', 88.12, 92.77, 82.33, 85.3],
                ['02/23/2009 16:00:00', 91.65, 92.92, 86.51, 89.31],
                ['02/17/2009 16:00:00', 96.87, 97.04, 89, 91.2],
                ['02/09/2009 16:00:00', 100, 103, 95.77, 99.16],
                ['02/02/2009 16:00:00', 89.1, 100, 88.9, 99.72],
                ['01/26/2009 16:00:00', 88.86, 95, 88.3, 90.13],
                ['01/20/2009 16:00:00', 81.93, 90, 78.2, 88.36],
                ['01/12/2009 16:00:00', 90.46, 90.99, 80.05, 82.33],
                ['01/05/2009 16:00:00', 93.17, 97.17, 90.04, 90.58],
                ['12/29/2008 16:00:00', 86.52, 91.04, 84.72, 90.75],
                ['12/22/2008 16:00:00', 90.02, 90.03, 84.55, 85.81],
                ['12/15/2008 16:00:00', 95.99, 96.48, 88.02, 90],
                ['12/08/2008 16:00:00', 97.28, 103.6, 92.53, 98.27],
                ['12/01/2008 16:00:00', 91.3, 96.23, 86.5, 94],
                ['11/24/2008 16:00:00', 85.21, 95.25, 84.84, 92.67],
                ['11/17/2008 16:00:00', 88.48, 91.58, 79.14, 82.58],
                ['11/10/2008 16:00:00', 100.17, 100.4, 86.02, 90.24],
                ['11/03/2008 16:00:00', 105.93, 111.79, 95.72, 98.24],
                ['10/27/2008 16:00:00', 95.07, 112.19, 91.86, 107.59],
                ['10/20/2008 16:00:00', 99.78, 101.25, 90.11, 96.38],
                ['10/13/2008 16:00:00', 104.55, 116.4, 85.89, 97.4],
                ['10/06/2008 16:00:00', 91.96, 101.5, 85, 96.8],
                ['09/29/2008 16:00:00', 119.62, 119.68, 94.65, 97.07],
                ['09/22/2008 16:00:00', 139.94, 140.25, 123, 128.24],
                ['09/15/2008 16:00:00', 142.03, 147.69, 120.68, 140.91]
            ];
            var min = "09-01-2008";
            var max = "06-22-2009";
            var tickInterval = "6 weeks";

            var plot2 = $.jqplot('chart2',[ohlc],{
                seriesDefaults:{yaxis:'y2axis'},
                grid: {
                    backgroundColor: 'transparent',
                    drawBorder: false,
                    shadow: false,
                },
                axes: {
                    xaxis: {
                        renderer:$.jqplot.DateAxisRenderer,
                        tickOptions:{formatString:'%b %e'},
                        min: min,
                        max: max,
                        tickInterval: tickInterval
                    },
                    y2axis: {
                        tickOptions:{formatString:'$%d'}
                    }
                },
                series: [{
                    renderer: $.jqplot.OHLCRenderer,
                    rendererOptions: {
                        candleStick:true,
                        lineWidth: 1,
                        wickColor: "#00a44c",
                        fillUpBody: true,
                        upBodyColor: "#00a44c",
                        downBodyColor: "#e92100",
                    }
                }],
                highlighter: {
                    show: true,
                    showMarker:false,
                    tooltipAxes: 'xy',
                    yvalues: 4,
                    formatString:'<table class="jqplot-highlighter"> \
					<tr><td>date:</td><td>%s</td></tr> \
					<tr><td>open:</td><td>%s</td></tr> \
					<tr><td>hi:</td><td>%s</td></tr> \
					<tr><td>low:</td><td>%s</td></tr> \
					<tr><td>close:</td><td>%s</td></tr></table>'
                }
            });

            $(window).resize(function() {
                plot2.replot();
            });
        }
    }
    barchartRefresh();

    $('.obligation_name a, .company_name a').click(function(e) {
        e.preventDefault();

        var prefix;
        if ($(this).closest('p').hasClass('obligation_name')) {
            prefix = 'obligation'
        } else if ($(this).closest('p').hasClass('company_name')) {
            prefix = 'company'
        }

        var href = $(this).attr('href');
        alert($('#' + prefix + '_detail_popup').attr('id'))
        $('#' + prefix + '_detail_popup .modal-body_title').html('');
        $('#' + prefix + '_detail_popup .modal-body_inner').html('');

        if ($(this).closest('p').hasClass('obligation_name')) {
            $('.candlestick_scripts').html('');
        }

        $('#' + prefix + '_detail_popup .modal-body_title').load(href + ' h1');

        $('#' + prefix + '_detail_popup .modal-body_inner').load(href + ' .article_inner');

        $('#' + prefix + '_detail_popup').modal('show');

        setTimeout(function() {
            var title_text = $('#' + prefix + '_detail_popup .modal-body_title').find('h1').text();
            $('#' + prefix + '_detail_popup .modal-body_title').find('h1').remove();
            $('#' + prefix + '_detail_popup .modal-body_title').text(title_text);
        }, 200);

        // setTimeout(function() {
        // 	$('#' + prefix + '_detail_popup .simple_table_outer').mCustomScrollbar({
        // 		scrollbarPosition: 'outside',
        // 		axis: 'x',
        // 		advanced: { autoExpandHorizontalScroll: true },
        // 	});
        // }, 300);

        if ($(this).closest('p').hasClass('obligation_name')) {
            setTimeout(function() {
                barchartRefresh();
            }, 310);
        }
        if ($(this).closest('p').hasClass('company_name')) {
            $('.modal_pricing_outer').load(href + ' #buy_subscribe_popup');
        }
    });
});

//обрезка текста
$(function() {
    if ($('.calculate_table .company_name').length != 0) {
        $('.calculate_table .company_name a').each(function() {
            var size = 8,
                newsContent = $(this),
                newsText = newsContent.text();

            if (newsText.length > size) {
                newsContent.text(newsText.slice(0, size) + '...');
            }
        });
    }
});

//валидация
$(function() {
	if ($('.fs_subscribe_form').length != 0) {
		$('.fs_subscribe_form').validate({
			rules: {
				name: {required: true, lettersonly: true},
				email: {required: true, truemail: true},
				phone: {required: true}
			},
			messages: {
				name: {
					required: "Данное поле не заполнено",
					lettersonly: "Неверный формат данных"
				},
				email: {
					required: "Данное поле не заполнено",
					truemail: "Неверный формат данных"
				},
				phone: {
					required: "Данное поле не заполнено",
				}
			},
			
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.prependTo( element.parent("div") );
			},
			submitHandler: function(form){
				$.post("/ajax/new/slide7.php", $(form).serialize(), function(data){
					$(".fs_subscribe_form").html(data.text);				
				}, "json");			
				return false;
			}
		});
	}
	
	if ($('.webinar_reg_form').length != 0) {
		$('.webinar_reg_form').validate({
			rules: {
				email: {required: true, truemail: true},
				name: {required: true, lettersonly: true},
				phone: {required: true}
			},
			messages: {
				email: {
					required: "Данное поле не заполнено",
					truemail: "Неверный формат данных"
				},
				name: {
					required: "Данное поле не заполнено",
					lettersonly: "Неверный формат данных"
				},
				phone: {
					required: "Данное поле не заполнено",
				}
			},
			
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.prependTo( element.parent("div") );
			},
			submitHandler: function(form){
				$.post("/ajax/new/slide3.php", $(form).serialize(), function(data){
					$("#popup_download .modal-title").html(data.name);	
					$("#popup_download .modal-body").html(data.text);				
				}, "json");			
				return false;
			}
		});
	}
	
	if ($('.header_login').length != 0) {
		$('.header_login').validate({
			rules: {
				email: {required: true, truemail: true},
				pass: {required: true, minlength: 6}
			},
			messages: {
				email: {
					required: "Данное поле не заполнено",
					truemail: "Неверный формат данных"
				},
				pass: {
					required: "Данное поле не заполнено",
					minlength: "Введите не менее 6 символов"
				}
			},
			
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.prependTo( element.parent("div") );
			},
		});
	}
	
	if ($('.header_reset_password').length != 0) {
		$('.header_reset_password').validate({
			rules: {
				email: {required: true, truemail: true}
			},
			messages: {
				email: {
					required: "Данное поле не заполнено",
					truemail: "Неверный формат данных"
				}
			},
			
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.prependTo( element.parent("div") );
			},
		});
	}
	
	if ($('.header_reset_password').length != 0) {
		$('.header_reset_password').validate({
			rules: {
				email: {required: true, truemail: true}
			},
			messages: {
				email: {
					required: "Данное поле не заполнено",
					truemail: "Неверный формат данных"
				}
			},
			
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.prependTo( element.parent("div") );
			},
		});
	}
	
	if ($('.header_signup').length != 0) {
		$('.header_signup').validate({
			rules: {
				name: {required: true, lettersonly: true},
				email: {required: true, truemail: true},
				pass_1: {required: true, minlength: 6},
				pass_2: {required: true, minlength: 6}
			},
			messages: {
				name: {
					required: "Данное поле не заполнено",
					lettersonly: "Неверный формат данных"
				},
				email: {
					required: "Данное поле не заполнено",
					truemail: "Неверный формат данных"
				},
				pass_1: {
					required: "Данное поле не заполнено",
					minlength: "Введите не менее 6 символов"
				},
				pass_2: {
					required: "Данное поле не заполнено",
					minlength: "Введите не менее 6 символов"
				}
			},
			
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.prependTo( element.parent("div") );
			},
		});
	}
	
	if ($('.body_signup').length != 0) {
		$('.body_signup').validate({
			rules: {
				email: {required: true, truemail: true},
				name: {required: true},
				phone: {required: true}
			},
			messages: {
				email: {
					required: "Данное поле не заполнено",
					truemail: "Неверный формат данных"
				},
				name: {
					required: "Данное поле не заполнено",
				},
				phone: {
					required: "Данное поле не заполнено",
				}
			},
			
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.prependTo( element.parent("div") );
			},
			submitHandler: function(form){
				$.post("/ajax/new/slide2.php", $(form).serialize(), function(data){
					$("#popup_join .modal-title").html(data.name);	
					$("#popup_join .modal-body").html(data.text);				
				}, "json");			
				return false;
			}
		});
	}
    if ($('#add_review_form').length != 0) {

        var validator = $('#add_review_form').validate({
            rules: {
                name: {required: true, lettersonly: true},
                email: {required: true, truemail: true},
                review_text: {required: true},
                confidencial_add_review: {required: true}
            },
            messages: {
                name: {
                    required: "Данное поле не заполнено",
                    lettersonly: "Неверный формат данных"
                },
                email: {
                    required: "Данное поле не заполнено",
                    truemail: "Неверный формат данных"
                },
                review_text: {
                    required: "Данное поле не заполнено",
                },
                confidencial_add_review: {
                    required: "Необходимо поставить галочку",
                }
            },

            errorElement: "span",
            errorPlacement: function(error, element) {
                error.prependTo( element.parent("div") );
            },
        });

        $('#add_review_form .dashed_link').click(function() {
            var form_elem_first = $(this).closest('.add_review_form_social_list_outer').find('.form_element').eq(0).clone();

            $(this).closest('.add_review_form_social_list_outer').find('.add_review_form_social_list').append(form_elem_first);

            var form_elem = $(this).closest('.add_review_form_social_list_outer').find('.add_review_form_social_list').find('.form_element');

            for (var i = 0; i < form_elem.length; i++) {
                if (i == form_elem.length - 1) {
                    form_elem.eq(i).prepend('<span class="remove">&times;</span>');
                    form_elem.eq(i).find('input').val('');
                }
            }
        })
        $('.add_review_form_social_list').on('click', '.form_element .remove', function() {
            var ths = $(this).closest('.form_element');
            ths.css('opacity', 0);
            setTimeout(function() {
                ths.remove();
            }, 300);
        });
    }

    if ($('#master_subscribe_form').length != 0) {
        $('#master_subscribe_form').validate({
            rules: {
                name: {required: true, lettersonly: true},
                email: {required: true, truemail: true},
                phone: {required: true},
                confidencial_master_subscribe: {required: true}
            },
            messages: {
                name: {
                    required: "Данное поле не заполнено",
                    lettersonly: "Неверный формат данных"
                },
                email: {
                    required: "Данное поле не заполнено",
                    truemail: "Неверный формат данных"
                },
                phone: {
                    required: "Данное поле не заполнено",
                },
                confidencial_master_subscribe: {
                    required: "Необходимо поставить галочку",
                }
            },

            errorElement: "span",
            errorPlacement: function(error, element) {
                error.prependTo( element.parent("div") );
            },
        });
    }
});