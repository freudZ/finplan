companyCounting();
function companyCounting() {
  if ($('.company_tables_outer').length != 0) {

    //текстовые блоки внутри таблицы Анализ денежных потоков
    var tc_investments_before = parseFloat($('.tc_investments td').eq(1).attr('data-value'));
    tc_investments_before = tc_investments_before.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    setTimeout(function() {
      $('body').find('.tc_investments td:nth-child(2)').text(tc_investments_before);
      $('body').find('.tc_investments td:nth-child(2)').text(tc_investments_before);
      $('body').find('.tc_investments_input').each(function() {
        $(this).val(tc_investments_before);
      });
    }, 1);
    //console.log('!@#', tc_investments_before)

    //таблица Анализ денежных потоков
    var cashFlowAnalisysTableChange = function() {

      var firstArr = [], firstArrCurr = [];

      var tc_discount_rate = $('#tc_discount_rate_1').val();
      if (tc_discount_rate == '' || tc_discount_rate == '%') {
        tc_discount_rate = 0;
      }
      var tc_fixed_assets_rate = $('#tc_fixed_assets_rate_1').val();
      if (tc_fixed_assets_rate == '' || tc_fixed_assets_rate == '%') {
        tc_fixed_assets_rate = 0;
      }
      var tc_net_cash_rate = $('#tc_net_cash_rate_1').val();
      if (tc_net_cash_rate == '' || tc_net_cash_rate == '%') {
        tc_net_cash_rate = 0;
      }
      var tc_net_profit_rate = $('#tc_net_profit_rate_1').val();
      if (tc_net_profit_rate == '' || tc_net_profit_rate == '%') {
        tc_net_profit_rate = 0;
      }

      firstArrCurr.push(tc_discount_rate, tc_fixed_assets_rate, tc_net_cash_rate, tc_net_profit_rate);

      for (elem in firstArrCurr) {
        if (firstArrCurr[elem] == '' || firstArrCurr[elem] == '%') {
          firstArrCurr[elem] = '0';
        }
        firstArrCurr[elem] = firstArrCurr[elem].replace(",",".");
        firstArrCurr[elem] = firstArrCurr[elem].replace("%","");
        firstArrCurr[elem] = firstArrCurr[elem].replace(" ","");
        firstArrCurr[elem] = parseFloat(firstArrCurr[elem]);
        firstArr.push(firstArrCurr[elem] / 100);
      }

      //подсчет количества столбцов
      var col_len = $('.tc_cash_flow_analisys_table tbody tr:first-child').find('td').length - 2;
      var secondArr = [];

      var tc_clear_profit_arr = [],
          tc_depreciation_arr = [],
          tc_investments_arr = [],
          tc_net_cash_flow_arr = [],
          tc_discount_coefficient_arr = [],
          tc_discounted_cash_flow_arr = [];

      var tc_clear_profit_first = parseFloat($('.tc_clear_profit td:nth-child(2)').attr('data-value')),
          tc_depreciation_first = parseFloat($('.tc_depreciation td:nth-child(2)').attr('data-value')),
          tc_investments_first = parseFloat($('.tc_investments td:nth-child(2)').attr('data-value'));

      tc_clear_profit_arr.push(tc_clear_profit_first);
      tc_depreciation_arr.push(tc_depreciation_first);
      tc_investments_arr.push(tc_investments_first);
      tc_discount_coefficient_arr.push(0);
      tc_discounted_cash_flow_arr.push(0);

      for (var i = 0; i < col_len; i ++) {
        var tc_clear_profit_curr = (tc_clear_profit_arr[i] * firstArr[3]) + tc_clear_profit_arr[i];
        tc_clear_profit_arr.push(tc_clear_profit_curr);

        var tc_depreciation_curr = (tc_depreciation_arr[i] * firstArr[1]) + tc_depreciation_arr[i];
        tc_depreciation_arr.push(tc_depreciation_curr);

        var tc_investments_curr;
        tc_investments_curr = $('.tc_investments td').eq(i + 2).find('input').val();
        if (tc_investments_curr == '' || tc_investments_curr == '%') {
          tc_investments_curr = '0';
        }
        tc_investments_curr = tc_investments_curr.replace(",",".");
        tc_investments_curr = tc_investments_curr.replace("%","");
        tc_investments_curr = tc_investments_curr.replace(" ","");
        tc_investments_curr = parseFloat(tc_investments_curr);
        tc_investments_arr.push(tc_investments_curr);
      }

      for (var i = 0; i < col_len + 1; i ++) {
        var tc_net_cash_flow_curr = tc_clear_profit_arr[i] + tc_depreciation_arr[i] - tc_investments_arr[i];
        tc_net_cash_flow_arr.push(tc_net_cash_flow_curr);

        var tc_discount_coefficient_curr,
            tc_discounted_cash_flow_curr;
        if (i > 0) {
          tc_discount_coefficient_curr = Math.pow((1 * firstArr[0]) + 1, i);
          tc_discount_coefficient_arr.push(tc_discount_coefficient_curr);

          tc_discounted_cash_flow_curr = tc_net_cash_flow_curr / tc_discount_coefficient_curr;
          tc_discounted_cash_flow_arr.push(tc_discounted_cash_flow_curr);
        }
      }

      var thirdArr = [];
      var tc_terminal_value = 0,
          tc_fair_value = 0,
          tc_current_capitalization = parseFloat($('.tc_current_capitalization').attr('data-value')),
          tc_growth_potential = 0;

      tc_current_capitalization = (tc_current_capitalization / 1000000).toFixed(2);

      tc_terminal_value = (((tc_net_cash_flow_arr[tc_net_cash_flow_arr.length - 1] * (1 + firstArr[2])) / (firstArr[0] - firstArr[2])) / tc_discount_coefficient_arr[tc_discount_coefficient_arr.length - 1]);

//      console.log('!!!!', tc_net_cash_flow_arr[tc_net_cash_flow_arr.length - 1], firstArr[3], firstArr[0], firstArr[2], tc_discount_coefficient_arr[tc_discount_coefficient_arr.length - 1])

      for (i in tc_discounted_cash_flow_arr) {
        tc_fair_value += tc_discounted_cash_flow_arr[i];
      }
      tc_fair_value += tc_terminal_value;

      tc_growth_potential = (tc_fair_value / tc_current_capitalization) - 1;

      thirdArr.push(tc_terminal_value, tc_fair_value, tc_current_capitalization, tc_growth_potential);

      //вывод полученных данных в таблицу
      for (i in tc_clear_profit_arr) {
        var tc_clear_profit_curr = Math.round(tc_clear_profit_arr[i]);
          tc_clear_profit_curr = tc_clear_profit_curr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        $('.tc_clear_profit td').eq(i).next('td').text(tc_clear_profit_curr);
      }
      for (i in tc_depreciation_arr) {
        var tc_depreciation_curr = Math.round(tc_depreciation_arr[i]);
          tc_depreciation_curr = tc_depreciation_curr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        $('.tc_depreciation td').eq(i).next('td').text(tc_depreciation_curr);
      }
      for (i in tc_net_cash_flow_arr) {
        var tc_net_cash_flow_curr = Math.round(tc_net_cash_flow_arr[i]);
          tc_net_cash_flow_curr = tc_net_cash_flow_curr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        $('.tc_net_cash_flow td').eq(i).next('td').text(tc_net_cash_flow_curr);
      }
      for (i in tc_discount_coefficient_arr) {
        var tc_discount_coefficient_curr = tc_discount_coefficient_arr[i].toFixed(2);
          tc_discount_coefficient_curr = tc_discount_coefficient_curr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        if (tc_discount_coefficient_curr != 0) {
          $('.tc_discount_coefficient td').eq(i).next('td').text(tc_discount_coefficient_curr);
        } else {
          $('.tc_discount_coefficient td').eq(i).next('td').text(' ');
        }
      }
      for (i in tc_discounted_cash_flow_arr) {
        var tc_discounted_cash_flow_curr = Math.round(tc_discounted_cash_flow_arr[i]);
          tc_discounted_cash_flow_curr = tc_discounted_cash_flow_curr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        if (tc_discounted_cash_flow_curr != 0) {
          $('.tc_discounted_cash_flow td').eq(i).next('td').text(tc_discounted_cash_flow_curr);
        } else {
          $('.tc_discounted_cash_flow td').eq(i).next('td').text(' ');
        }
      }

      var tc_terminal_value = Math.round(thirdArr[0]);
          tc_terminal_value = tc_terminal_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "\xa0");
      $('.tc_terminal_value').text(tc_terminal_value);
      var tc_fair_value = Math.round(thirdArr[1]);
          tc_fair_value = tc_fair_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "\xa0");
      $('.tc_fair_value').text(tc_fair_value);
      var tc_current_capitalization = Math.round(thirdArr[2]);
      var tc_current_capitalization__final = tc_current_capitalization;
          tc_current_capitalization__final = tc_current_capitalization__final.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "\xa0");
      $('.tc_current_capitalization').text(tc_current_capitalization__final);
      var tc_growth_potential = thirdArr[3] * 100;
          tc_growth_potential = tc_growth_potential.toFixed(2);
          tc_growth_potential = tc_growth_potential.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "\xa0");
      $('.tc_growth_potential').text(tc_growth_potential + '%');
    }
    //задержка перед первым выполнением, чтобы текстовые поля в таблице успели перезаписаться
    setTimeout(function() {
      cashFlowAnalisysTableChange();
    }, 10);

    $('.company_tables_outer').on("change keyup input", 'input', function(eventObject) {
      if ($(this).attr('id') == 'tc_discount_rate_1' || $(this).attr('id') == 'tc_fixed_assets_rate_1' || $(this).attr('id') == 'tc_net_cash_rate_1' || $(this).attr('id') == 'tc_net_profit_rate_1' || $(this).attr('id') == 'ia_company_growth_rate' || $(this).attr('id') == 'ia_projected_dividends' || $(this).hasClass('tc_investments_input') || $(this).hasClass('ia_re')) {
        var txt;
    		//запрет на ввод всего кроме цифр
    		if ((eventObject.which != 37) && (eventObject.which != 39)) {
    			if (this.value.match(/[^0-9.,]/g)) {
    				this.value = this.value.replace(/[^0-9.,]/g, '');
    			}

    			txt = $(this).val();
    			txt = txt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
          if ($(this).hasClass('tc_investments_input') || $(this).hasClass('ia_re')) {
    			  $(this).val(txt);
          } else {
    			  $(this).val(txt + '%');
          }
    		}

    		//постановка курсора перед постфиксом текстового поля
    		function setSelectionRange(input, selectionStart, selectionEnd) {
    		  if (input.setSelectionRange) {
    			input.setSelectionRange(selectionStart, selectionEnd);
    		  } else if (input.createTextRange) {
    			var range = input.createTextRange();
    			range.collapse(true);
    			range.moveEnd('character', selectionEnd);
    			range.moveStart('character', selectionStart);
    			range.select();
    		  }
    		}
    		function setCaretToPos(input, pos) {
    		  setSelectionRange(input, pos, pos);
    		}
    		setCaretToPos($(this)[0], txt.length);

        if ($(this).val() == '' || $(this).val() == '%') {
          $(this).attr('placeholder', '0');
          $(this).val('');
        }
      }

      setTimeout(function() {
        cashFlowAnalisysTableChange();
        investmentAnalysisChange();
      }, 10);
    });

    //таблица Инвестиционный анализ
    var investmentAnalysisChange = function() {
      var first_arr = [], first_arr_curr = [];

      var ia_current_re = $('#ia_current_re').val();
      if (ia_current_re == '' || ia_current_re == '%') {
        ia_current_re = 0;
      }
      var ia_equitable_re = $('#ia_equitable_re').val();
      if (ia_equitable_re == '' || ia_equitable_re == '%') {
        ia_equitable_re = 0;
      }
      var ia_company_growth_rate = $('#ia_company_growth_rate').val();
      if (ia_company_growth_rate == '' || ia_company_growth_rate == '%') {
        ia_company_growth_rate = 0;
      }
      var ia_projected_dividends = $('#ia_projected_dividends').val();
      if (ia_projected_dividends == '' || ia_projected_dividends == '%') {
        ia_projected_dividends = 0;
      }

      first_arr_curr.push(ia_current_re, ia_equitable_re, ia_company_growth_rate, ia_projected_dividends);

      for (elem in first_arr_curr) {
        if (first_arr_curr[elem] == '' || first_arr_curr[elem] == '%') {
          first_arr_curr[elem] = '0';
        }
        first_arr_curr[elem] = first_arr_curr[elem].replace(",",".");
        first_arr_curr[elem] = first_arr_curr[elem].replace("%","");
        first_arr_curr[elem] = first_arr_curr[elem].replace(" ","");
        first_arr_curr[elem] = parseFloat(first_arr_curr[elem]);

        if (elem > 1) {
          first_arr.push(first_arr_curr[elem] / 100);
        } else {
          first_arr.push(first_arr_curr[elem]);
        }
      }

      var ia_underestimation_increase = (((first_arr[1] / first_arr[0]) - 1) * 100);

      var ia_consensus_forecast = ia_underestimation_increase + (first_arr[2] * 100) + (first_arr[3] * 100);

      ia_underestimation_increase = ia_underestimation_increase.toFixed(1);
      ia_underestimation_increase = ia_underestimation_increase.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      $('.ia_underestimation_increase').text(ia_underestimation_increase + '%');

      ia_consensus_forecast = ia_consensus_forecast.toFixed(1);
      ia_consensus_forecast = ia_consensus_forecast.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      $('.ia_consensus_forecast').text(ia_consensus_forecast + '%');
    }
    setTimeout(function() {
      investmentAnalysisChange();
    }, 10);
  }
}

$('body').on('click', '.company_chart_tab a[data-toggle="tab"]', function (e) {
  e.preventDefault();
  //console.log('!!!')
  $(this).tab('show');
})

$('body').on('shown.bs.tab', '.company_chart_tab a[data-toggle="tab"]', function (e) {
  var hash_new_arr = e.target.toString().split('#');
  var hash_new = '#' + hash_new_arr[1];

  var hash_old_arr = e.relatedTarget.toString().split('#');
  var hash_old = '#' + hash_old_arr[1];

  var old_line = $(hash_old).find('tbody').find('tr.active').attr('data-number');
  $(hash_new).find('tbody').find('tr[data-number="' + old_line + '"]').trigger('click');
})
