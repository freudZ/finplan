class CRadarPreset{
    constructor(context){
	 this.obj_select = $(context);
	 this.typeFilter = this.obj_select.attr('name');
	 this.selectedPreset = this.getSelPreset();
	 this.selectedParams = this.getSelParams();
  }

  getSelPreset () {
  	 let preset = $('select[name='+this.obj_select.attr('name')+'] option:selected').text();
	 return preset;
  }

  getSelParams () {
  	let params = $('select[name='+this.obj_select.attr('name')+']').val();
	 return params;
  }

  doPreset () { //Запуск обновление выбранного фильтра
	 if(this.typeFilter=='preset_actions'){
		this.updateActionsFilter();
	 } else if(this.typeFilter=='preset_actions_usa'){
		this.updateActionsUSAFilter();
	 } else if(this.typeFilter=='preset_etf'){
		this.updateETFFilter();
	 } else if(this.typeFilter=='preset_obligations'){
		this.updateObligationsFilter();
	 }
  }

  setExtFilterToExpand(tab){
  	$(tab+' .calculate_more').removeClass('collapsed');
	var target = $(tab+' .calculate_more').attr('data-target');
	$(target).addClass('in').removeAttr('style');
  }

  getParamsArrayFromString () {

		 var obj = {};

       // разделяем параметры
	    var arr = this.selectedParams.split('&');
	    for (var i=0; i<arr.length; i++) {
	      // разделяем параметр на ключ => значение
	      var a = arr[i].split('=');

      // обработка данных вида: list[]=thing1&list[]=thing2
      //var paramNum = undefined;
      var paramNum = undefined;
      var paramName = a[0].replace(/\[\*\]/, function(v) {
        paramNum = v.slice(1,-1);
        return '';
      });

      // передача значения параметра ('true' если значение не задано)
      var paramValue = typeof(a[1])==='undefined' ? true : a[1];

      // если ключ параметра уже задан
      if (obj[paramName]) {
        // преобразуем текущее значение в массив
        if (typeof obj[paramName] === 'string') {
          obj[paramName] = [obj[paramName]];
        }
        // если не задан индекс...
        if (typeof paramNum === 'undefined') {
          // помещаем значение в конец массива
          obj[paramName].push(paramValue);
        }
        // если индекс задан...
        else {
          // размещаем элемент по заданному индексу
          obj[paramName][paramNum] = paramValue;
        }
      }
      // если параметр не задан, делаем это вручную
      else {
        obj[paramName] = paramValue;
      }
  }

  	 return obj;
  }

  //Выставляет фильтр по облигациям
  updateObligationsFilter () {
  	 var Params = this.getParamsArrayFromString();
	 this.obj_select.attr('freezed','Y');
	 this.setExtFilterToExpand('#debstock_tab');//Разворачиваем большо фильтр
	 for (var key in Params) {
		 //console.log('key= '+key+' cancel='+flag_cancel_update);
		 if(key == 'duration'){ this.setSliderValue('#debstock_tab', '.time_slider', Params[key] );   } //Слайдер
		 if(key == 'rate'){ this.setSliderValue('#debstock_tab', '.rate_slider', Params[key]);   } //Слайдер
		 if(key == 'turnover_week'){ this.toggleOnOff('#debstock_tab', '#checkbox_0', Params[key]);  }  //Переключатель
		 if(key == 'coupons_more'){ this.setInputValue('#debstock_tab', 'coupons_more', Params[key]);  }  //Текстовое поле
		 if(key == 'valute'){ this.setSelectValue('#debstock_tab', 'valute', Params[key]);  } //Список выбора
		 if(key == 'quality_bonds'){ this.setSelectValue('#debstock_tab', 'quality_bonds', Params[key]);  } //Список выбора
		 if(key == 'price_bonds'){ this.setSelectValue('#debstock_tab', 'price_bonds', Params[key]);  } //Список выбора
		 if(key == 'offerdate'){ this.setSelectValue('#debstock_tab', 'offerdate', Params[key]);  }  //Список выбора
		 if(key == 'tax'){ this.setSelectValue('#debstock_tab', 'tax', Params[key]);  }  //Список выбора
		 if(key == 'listlevel'){ this.setSelectValue('#debstock_tab', 'listlevel', Params[key]);  }  //Список выбора
		 if(key == 'coupon_type'){ this.setSelectValue('#debstock_tab', 'coupon_type', Params[key]);  }  //Список выбора
		 if(key == 'deprecation'){ this.setSelectValue('#debstock_tab', 'deprecation', Params[key]);  }  //Список выбора
		 if(key == 'payment_order'){ this.setSelectValue('#debstock_tab', 'payment_order', Params[key]);  }  //Список выбора
		 if(key == 'structural'){ this.setSelectValue('#debstock_tab', 'structural', Params[key]);  }  //Список выбора
		 if(key == 'duration_period'){ this.setSelectValue('#debstock_tab', 'duration_period', Params[key]);  }  //Список выбора

		 if(key == 'date_start_first'){ this.setInputValue('#debstock_tab', 'date_start_first', Params[key]);  }  //Текстовое поле
		 if(key == 'date_start_last'){ this.setInputValue('#debstock_tab', 'date_start_last', Params[key]);  }  //Текстовое поле
		 if(key == 'date_cancel_first'){ this.setInputValue('#debstock_tab', 'date_cancel_first', Params[key]);  }  //Текстовое поле
		 if(key == 'date_cancel_last'){ this.setInputValue('#debstock_tab', 'date_cancel_last', Params[key]);  }  //Текстовое поле

		 if(key == 'type[]'){ this.setRussianIndustryCheckboxes('#debstock_tab', 'type', Params[key]);  } //Чекбоксы виды облигаций

		 if(key == 'fin_analysis'){ this.setSelectValue('#debstock_tab', 'fin_analysis', Params[key]);  }  //Список выбора

		 if(key == 'month_value'){ this.setSelectValue('#debstock_tab', 'month_value', Params[key]);  }  //Список выбора
		 if(key == 'month[Рейтинг по активам, номер][top]'){ this.setSelectValue('#debstock_tab', 'month\\[Рейтинг\\ по\\ активам\\,\\ номер\\]\\[top\\]', Params[key]);  }  //Список выбора
		 if(key == 'month[Рейтинг по активам, номер][use]'){ this.toggleOnOff('#debstock_tab', 'checkbox_1', Params[key]);  }  //Переключатель
		 if(key == 'month[Рентабельность собственного капитала, %][percent]'){ this.setInputValue('#debstock_tab', 'month\\[Рентабельность\\ собственного\\ капитала\\,\\ \\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Рентабельность собственного капитала, %][use]'){ this.toggleOnOff('#debstock_tab', '#checkbox_2', Params[key]);  }  //Переключатель
		 if(key == 'month[Доля просрочки, %][percent]'){ this.setInputValue('#debstock_tab', 'month\\[Доля\\ просрочки\\,\\ \\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Доля просрочки, %][use]'){ this.toggleOnOff('#debstock_tab', '#checkbox_3', Params[key]);  }  //Переключатель
		 if(key == 'month[Н1,%][percent]'){ this.setInputValue('#debstock_tab', 'month\\[Н1\\,\\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Н1,%][use]'){ this.toggleOnOff('#debstock_tab', '#checkbox_4', Params[key]);  }  //Переключатель

		 if(key == 'period_value'){ this.setSelectValue('#debstock_tab', 'period_value', Params[key]);  }  //Список выбора
		 if(key == 'period[Темп прироста выручки][percent]'){ this.setInputValue('#debstock_tab', 'period\\[Темп\\ прироста\\ выручки\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Темп прироста выручки][use]'){ this.toggleOnOff('#debstock_tab', '#checkbox_5', Params[key]);  }  //Переключатель
		 if(key == 'period[Рентабельность собственного капитала][percent]'){ this.setInputValue('#debstock_tab', 'period\\[Рентабельность\\ собственного\\ капитала\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Рентабельность собственного капитала][use]'){ this.toggleOnOff('#debstock_tab', '#checkbox_6', Params[key]);  }  //Переключатель
		 if(key == 'period[Доля собственного капитала в активах][percent]'){ this.setInputValue('#debstock_tab', 'period\\[Доля\\ собственного\\ капитала\\ в\\ активах\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Доля собственного капитала в активах][use]'){ this.toggleOnOff('#debstock_tab', '#checkbox_7', Params[key]);  }  //Переключатель
		 if(key == 'radar-rating'){ this.setSelectValue('#debstock_tab', 'radar-rating', Params[key]);  }  //Список выбора
		 }
		  $('#debstock_tab #apply_update_obligations').change(); //Обновляем таблицу
	  this.obj_select.attr('freezed','N');
  }

  //Выставляет фильтр по акциям РФ
  updateActionsFilter () {
  	 var Params = this.getParamsArrayFromString();
  //	 console.log('Params', Params);
	 this.obj_select.attr('freezed','Y');
 	 this.setExtFilterToExpand('#shares_tab');//Разворачиваем большо фильтр
	 for (var key in Params) {
		 //console.log('key= '+key+' value='+Params[key]);
		 if(key == 'pe'){ this.setSliderValue('#shares_tab', '.coefficient_slider', Params[key] );   } //Слайдер
		 if(key == 'profitability'){ this.setSliderValue('#shares_tab', '.profitability_slider', Params[key]);   } //Слайдер
		 if(key == 'turnover_week'){ this.toggleOnOff('#shares_tab', '#checkbox_8', Params[key]);  }  //Переключатель
		 if(key == 'analitics'){ this.setSelectValue('#shares_tab', 'analitics', Params[key]);  } //Список выбора
		 if(key == 'capitalization'){ this.setSelectValue('#shares_tab', 'capitalization', Params[key]);  } //Список выбора
		 if(key == 'dividends'){ this.setInputValue('#shares_tab', 'dividends', Params[key]);  }  //Текстовое поле
		 if(key == 'export_share'){ this.setSelectValue('#shares_tab', 'export_share', Params[key]);  }  //Список выбора
		 if(key == 'future_dividends'){ this.toggleOnOff('#shares_tab', '#checkbox_9', Params[key]);  }  //Переключатель
		 if(key == 'in_index'){ this.setSelectValue('#shares_tab', 'in_index', Params[key]);  }  //Список выбора
		 if(key == 'index_candidate'){ this.setSelectValue('#shares_tab', 'index_candidate', Params[key]);  }  //Список выбора
		 if(key == 'industry[]'){ this.setRussianIndustryCheckboxes('#shares_tab', 'industry', Params[key]);  } //Чекбоксы отраслей
		 if(key == 'lotprice'){ this.setSelectValue('#shares_tab', 'lotprice', Params[key]);  }  //Список выбора
		 if(key == 'margin_deals'){ this.setSelectValue('#shares_tab', 'margin_deals', Params[key]);  }  //Список выбора
		 if(key == 'month-increase'){ this.setSelectValue('#shares_tab', 'month-increase', Params[key]);  }  //Список выбора
		 if(key == 'month_value'){ this.setSelectValue('#shares_tab', 'month_value', Params[key]);  }  //Список выбора
		 if(key == 'month[Доля просрочки, %][percent]'){ this.setInputValue('#shares_tab', 'month\\[Доля\\ просрочки\\,\\ \\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Доля просрочки, %][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_18', Params[key]);  }  //Переключатель
		 if(key == 'month[Н1,%][percent]'){ this.setInputValue('#shares_tab', 'month\\[Н1\\,\\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Н1,%][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_19', Params[key]);  }  //Переключатель
		 if(key == 'month[Рейтинг по активам, номер][top]'){ this.setSelectValue('#shares_tab', 'month\\[Рейтинг\\ по\\ активам\\,\\ номер\\]\\[top\\]', Params[key]);  }  //Список выбора
		 if(key == 'month[Рейтинг по активам, номер][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_16', Params[key]);  }  //Переключатель
		 if(key == 'month[Рентабельность собственного капитала, %][percent]'){ this.setInputValue('#shares_tab', 'month\\[Рентабельность\\ собственного\\ капитала\\,\\ \\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Рентабельность собственного капитала, %][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_17', Params[key]);  }  //Переключатель
		 if(key == 'betta'){ this.setSelectValue('#shares_tab', 'betta', Params[key]);  } //Список выбора
		 if(key == 'p-e'){ this.setSelectValue('#shares_tab', 'p-e', Params[key]);  } //Список выбора
		 if(key == 'peg'){ this.setSelectValue('#shares_tab', 'peg', Params[key]);  } //Список выбора
		 if(key == 'period_value'){ this.setSelectValue('#shares_tab', 'period_value', Params[key]);  }  //Список выбора
		 if(key == 'period[Доля собственного капитала в активах][percent]'){ this.setInputValue('#shares_tab', 'period\\[Доля\\ собственного\\ капитала\\ в\\ активах\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
	 	 if(key == 'period[Доля собственного капитала в активах][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_14', Params[key]);  }  //Переключатель
		 if(key == 'period[Рентабельность собственного капитала][percent]'){ this.setInputValue('#shares_tab', 'period\\[Рентабельность\\ собственного\\ капитала\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Рентабельность собственного капитала][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_12', Params[key]);  }  //Переключатель
		 if(key == 'period[Темп прироста выручки][percent]'){ this.setInputValue('#shares_tab', "period\\[Темп\\ прироста\\ выручки\\]\\[percent\\]", Params[key]);  }  //Текстовое поле
		 if(key == 'period[Темп прироста выручки][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_11', Params[key]);  }  //Переключатель
		 if(key == 'period[Темп роста активов][percent]'){ this.setInputValue('#shares_tab', 'period\\[Темп\\ роста\\ активов\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Темп роста активов][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_13', Params[key]);  }  //Переключатель

		 if(key == 'period[Темп роста прибыли][percent]'){ this.setInputValue('#shares_tab', 'period\\[Темп\\ роста\\ прибыли\\]\\[percent\\]', Params[key]);  }  //Текстовое поле

		 if(key == 'period[Темп роста прибыли][use]'){ this.toggleOnOff('#shares_tab', '#checkbox_15', Params[key]);}  //Переключатель
		 if(key == 'shares_all_sectors'){ this.toggleOnOff('#shares_tab', '#shares_all_sectors', Params[key]);  }  //Переключатель
		 if(key == 'three-year-increase'){ this.setSelectValue('#shares_tab', 'three-year-increase', Params[key]);  }  //Список выбора
	    if(key == 'year-increase'){ this.setSelectValue('#shares_tab', 'year-increase', Params[key]);  }  //Список выбора
		 if(key == 'no_dividends'){ this.toggleOnOff('#shares_tab', '#checkbox_10', Params[key]);  }  //Переключатель
		 if(key == 'radar-rating'){ this.setSelectValue('#shares_tab', 'radar-rating', Params[key]);  }  //Список выбора
	 	 }
		  $('#shares_tab #apply_update_actions').change(); //Обновляем таблицу
	  this.obj_select.attr('freezed','N');
  }

  //Выставляет фильтр по акциям США
  updateActionsUSAFilter () {
  	 var Params = this.getParamsArrayFromString();
	 this.obj_select.attr('freezed','Y');
	 this.setExtFilterToExpand('#shares_usa_tab');//Разворачиваем большо фильтр
	 for (var key in Params) {
		 //console.log('key= '+key+' cancel='+flag_cancel_update);
		 if(key == 'pe'){ this.setSliderValue('#shares_usa_tab', '.coefficient_slider_usa', Params[key] );   } //Слайдер
		 if(key == 'profitability'){ this.setSliderValue('#shares_usa_tab', '.profitability_slider_usa', Params[key]);   } //Слайдер
		 if(key == 'turnover_week'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_22', Params[key]);  }  //Переключатель
		 if(key == 'analitics'){ this.setSelectValue('#shares_usa_tab', 'analitics', Params[key]);  } //Список выбора
		 if(key == 'capitalization'){ this.setSelectValue('#shares_usa_tab', 'capitalization', Params[key]);  } //Список выбора
		 if(key == 'dividends'){ this.setInputValue('#shares_usa_tab', 'dividends', Params[key]);  }  //Текстовое поле
		 if(key == 'future_dividends'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_23', Params[key]);  }  //Переключатель
		 if(key == 'no_dividends'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_24', Params[key]);  }  //Переключатель

		 if(key == 'sector[]'){ this.setUSASectorsCheckboxes('#shares_usa_tab', 'sector', Params[key]);  } //Чекбоксы отраслей
		 if(key == 'price'){ this.setSelectValue('#shares_usa_tab', 'price', Params[key]);  }  //Список выбора
/*
		 if(key == 'month[Доля просрочки, %][percent]'){ this.setInputValue('#shares_usa_tab', 'month\\[Доля\\ просрочки\\,\\ \\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Доля просрочки, %][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_39', Params[key]);  }  //Переключатель
		 if(key == 'month[Н1,%][percent]'){ this.setInputValue('#shares_usa_tab', 'month\\[Н1\\,\\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Н1,%][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_40', Params[key]);  }  //Переключатель
		 if(key == 'month[Рейтинг по активам, номер][top]'){ this.setSelectValue('#shares_usa_tab', 'month\\[Рейтинг\\ по\\ активам\\,\\ номер\\]\\[top\\]', Params[key]);  }  //Список выбора
		 if(key == 'month[Рейтинг по активам, номер][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_37', Params[key]);  }  //Переключатель
		 if(key == 'month[Рентабельность собственного капитала, %][percent]'){ this.setInputValue('#shares_usa_tab', 'month\\[Рентабельность\\ собственного\\ капитала\\,\\ \\%\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'month[Рентабельность собственного капитала, %][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_38', Params[key]);  }  //Переключатель*/
		 if(key == 'betta'){ this.setSelectValue('#shares_usa_tab', 'betta', Params[key]);  } //Список выбора
		 if(key == 'p-e'){ this.setSelectValue('#shares_usa_tab', 'p-e', Params[key]);  } //Список выбора
		 if(key == 'peg'){ this.setSelectValue('#shares_usa_tab', 'peg', Params[key]);  } //Список выбора
		 if(key == 'period_value'){ this.setSelectValue('#shares_usa_tab', 'period_value', Params[key]);  }  //Список выбора
		 if(key == 'period[Доля собственного капитала в активах][percent]'){ this.setInputValue('#shares_usa_tab', 'period\\[Доля\\ собственного\\ капитала\\ в\\ активах\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Доля собственного капитала в активах][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_28', Params[key]);  }  //Переключатель
		 if(key == 'period[Рентабельность собственного капитала][percent]'){ this.setInputValue('#shares_usa_tab', 'period\\[Рентабельность\\ собственного\\ капитала\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Рентабельность собственного капитала][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_26', Params[key]);  }  //Переключатель
		 if(key == 'period[Темп прироста выручки][percent]'){ this.setInputValue('#shares_usa_tab', "period\\[Темп\\ прироста\\ выручки\\]\\[percent\\]", Params[key]);  }  //Текстовое поле
		 if(key == 'period[Темп прироста выручки][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_25', Params[key]);  }  //Переключатель
		 if(key == 'period[Темп роста активов][percent]'){ this.setInputValue('#shares_usa_tab', 'period\\[Темп\\ роста\\ активов\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Темп роста активов][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_27', Params[key]);  }  //Переключатель
		 if(key == 'period[Темп роста прибыли][percent]'){ this.setInputValue('#shares_usa_tab', 'period\\[Темп\\ роста\\ прибыли\\]\\[percent\\]', Params[key]);  }  //Текстовое поле
		 if(key == 'period[Темп роста прибыли][use]'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_29', Params[key]);  }  //Переключатель
		 if(key == 'three-year-increase'){ this.setSelectValue('#shares_usa_tab', 'three-year-increase', Params[key]);  }  //Список выбора
       if(key == 'month-increase'){ this.setSelectValue('#shares_usa_tab', 'month-increase', Params[key]);  }  //Список выбора
	    if(key == 'year-increase'){ this.setSelectValue('#shares_usa_tab', 'year-increase', Params[key]);  }  //Список выбора
	    if(key == 'industry'){ this.setSelectValue('#shares_usa_tab', 'industry', Params[key]);  }  //Список выбора
		 if(key == 'country'){ this.setSelectValue('#shares_usa_tab', 'country', Params[key]);  }  //Список выбора
		 if(key == 'currency'){ this.setSelectValue('#shares_usa_tab', 'currency', Params[key]);  }  //Список выбора
		 if(key == 'in_index'){ this.setSelectValue('#shares_usa_tab', 'in_index', Params[key]);  }  //Список выбора
		 if(key == 'top'){ this.setSelectValue('#shares_usa_tab', 'top', Params[key]);  }  //Список выбора
		 if(key == 'use_esg'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_20', Params[key]);  }  //Переключатель
		 if(key == 'use_halal'){ this.toggleOnOff('#shares_usa_tab', '#checkbox_21', Params[key]);  }  //Переключатель
		 if(key == 'radar-rating'){ this.setSelectValue('#shares_usa_tab', 'radar-rating', Params[key]);  }  //Список выбора
	 	 }
		  $('#shares_usa_tab #apply_update_actions_usa').change(); //Обновляем таблицу
	  this.obj_select.attr('freezed','N');
  }

  //Выставляет фильтр по ETF
  updateETFFilter () {
  	 var Params = this.getParamsArrayFromString();
	 this.obj_select.attr('freezed','Y');
	 this.setExtFilterToExpand('#etf_tab');//Разворачиваем большо фильтр
	 for (var key in Params) {
		 //console.log('key= '+key+' cancel='+flag_cancel_update);
		 if(key == 'ETF_AVG_ICNREASE'){ this.setSliderValue('#etf_tab', '.ETF_AVG_ICNREASE_slider', Params[key] );   } //Слайдер
		 if(key == 'ETF_COMISSION'){ this.setSliderValue('#etf_tab', '.ETF_COMISSION_slider', Params[key]);   } //Слайдер

		 if(key == 'betta'){ this.setSelectValue('#etf_tab', 'betta', Params[key]);  } //Список выбора
		 if(key == 'ETF_EXCHANGE'){ this.setSelectValue('#etf_tab', 'ETF_EXCHANGE', Params[key]);  } //Список выбора
		 if(key == 'ETF_TYPE_OF_ACTIVES'){ this.setSelectValue('#etf_tab', 'ETF_TYPE_OF_ACTIVES', Params[key]);  } //Список выбора
		 if(key == 'ETF_DIVIDENDS'){ this.setSelectValue('#etf_tab', 'ETF_DIVIDENDS', Params[key]);  } //Список выбора
		 if(key == 'ETF_COUNTRY'){ this.setSelectValue('#etf_tab', 'ETF_COUNTRY', Params[key]);  } //Список выбора
		 if(key == 'ETF_INDUSTRY'){ this.setSelectValue('#etf_tab', 'ETF_INDUSTRY', Params[key]);  } //Список выбора
		 if(key == 'ETF_REPLICATION'){ this.setSelectValue('#etf_tab', 'ETF_REPLICATION', Params[key]);  } //Список выбора
		 if(key == 'ETF_BASE_CURRENCY'){ this.setSelectValue('#etf_tab', 'ETF_BASE_CURRENCY', Params[key]);  } //Список выбора
		 if(key == 'ETF_TYPE'){ this.setSelectValue('#etf_tab', 'ETF_TYPE', Params[key]);  } //Список выбора
		 if(key == 'ETF_CURRENCY'){ this.setSelectValue('#etf_tab', 'ETF_CURRENCY', Params[key]);  } //Список выбора
		 if(key == 'EMITENT_ID'){ this.setSelectValue('#etf_tab', 'EMITENT_ID', Params[key]);  } //Список выбора
		 if(key == 'three-year-increase'){ this.setSelectValue('#etf_tab', 'three-year-increase', Params[key]);  }  //Список выбора
       if(key == 'month-increase'){ this.setSelectValue('#etf_tab', 'month-increase', Params[key]);  }  //Список выбора
	    if(key == 'year-increase'){ this.setSelectValue('#etf_tab', 'year-increase', Params[key]);  }  //Список выбора
		 if(key == 'radar-rating'){ this.setSelectValue('#etf_tab', 'radar-rating', Params[key]);  }  //Список выбора
	 	 }
		  $('#etf_tab #apply_update_etf').change(); //Обновляем таблицу
	  this.obj_select.attr('freezed','N');
  }

  //Выставляет значение переключателя
  toggleOnOff(tab, element, value, cancel_update){
  	element = tab+' '+element;
	console.log('element= "'+element+'" value='+value);
  	if(cancel_update == undefined){
  	$(element).attr('cancel_update','Y');
	}
  	if(value!='Y'){
	  $(element).val('').removeAttr('checked');
  	} else {
	  $(element).val('Y').prop('checked', true);
  	}
	$(element).attr('cancel_update','N');
  }

  //Выставляет значение слайдера
  setSliderValue(tab, element, value, cancel_update){
  	element = tab+' '+element;
  	if(value=="all"){
  		value = $(element).slider( "option", "max" );
  	} else {
  		value = value.replace("%","");
  	}
	  //console.log(element,element+'='+value);
	  if(cancel_update == undefined){
	   $(element).attr('cancel_update','Y');
	  }
	  $(element).slider({ "value": value});
	  $(element).attr('cancel_update','N');
  }

  //Выставляет значение выпадающего списка
  setSelectValue(tab, name, value, cancel_update){
  	if(cancel_update == undefined){
	 $(tab+' select[name='+name+']').attr('cancel_update','Y');
	}
	 if(name=='analitics'){
	 	if(!value.length){
	 		value='shares_company_tab';
	 	}
	 	$(tab+' .tab_container').removeClass('active');
		$(tab+' .tab_container#' + value).addClass('active').css('opacity', 1);
	 }
	 $(tab+' select[name='+name+']').val(value).change();
	 $(tab+' select[name='+name+']').attr('cancel_update','N');
  }

  //Выставляет значение выпадающего списка
  setInputValue(tab, name, value, cancel_update){
  	if(cancel_update == undefined){
  	$(tab+' input[name='+name+']').attr('cancel_update','Y');
	 }
	 $(tab+' input[name='+name+']').val(value);
	$(tab+' input[name='+name+']').attr('cancel_update','N');
  }

  //Выставляет чекбоксы отраслей для акций РФ
  setRussianIndustryCheckboxes(tab, name, values, cancel_update){
	 //var values = values.split('&');
	 $(tab+" input[name="+name+"\\[\\]]").each(function(indx, element){
	 	if(cancel_update == undefined){
		$(this).attr('cancel_update','Y');
		}

		if(!values.includes($(this).val())){
		  $(this).removeAttr('checked');
	  	} else {
		  $(this).prop('checked', true);
	  	}
		$(this).attr('cancel_update','N');
    });

  }

  //Выставляет чекбоксы секторов для акций США
  setUSASectorsCheckboxes(tab, name, values, cancel_update){
	 //var values = values.split('&');
	 $(tab+" input[name="+name+"\\[\\]]").each(function(indx, element){
	 	if(cancel_update == undefined){
		$(this).attr('cancel_update','Y');
		}

		if(!values.includes($(this).val())){
		  $(this).removeAttr('checked');
	  	} else {
		  $(this).prop('checked', true);
	  	}
		$(this).attr('cancel_update','N');
    });

  }

}