var calcCode, //код облигации для калькулятора
 calcName, //название облигации для калькулятора
 calcPrice, //цена облигации для калькулятора
 calcProfit, //доходность облигации для калькулятора
 calcCouponPrice = ''; //цена купонов облигации для калькулятора
 var calcCouponPriceArray = {}; //массив с ценами купонов облигации для калькулятора

(function(){
  $('.calc-action').hide();
})();
  //автокомплит облигации в калькуляторе
  if($("#autocomplete_obl_calc").length) {
        $('#autocomplete_obl_calc').autocomplete({
            serviceUrl: '/ajax/obligations/obligations_calc.php',
            onSelect: function (suggestion) {
                $('.calc-action').show();
                calcCode = suggestion.data.CODE;
                calcName = suggestion.data.NAME;
                calcPrice = parseFloat(suggestion.data.OBLIGATION.PROPS.LASTPRICE.VALUE) || parseFloat(suggestion.data.OBLIGATION.PROPS.LEGALCLOSE.VALUE);
                calcCouponPrice = suggestion.data.COUPONS[0]['Купоны'];
                calcProfit = suggestion.data.PROFIT;
                $('#coupons_modal_calc_result').text(calcProfit);
                $('#coupons_modal_calc_price').val(calcPrice);

                if(suggestion.data.UNKNOWN === 0){
                  $('#coupons_modal_calc_coupon_price').closest('.row').hide();
                  $('.coupons_detail_modal_calc').closest('.col').hide();
                } else {
                  $('#coupons_modal_calc_coupon_price').closest('.row').show();
                  $('.coupons_detail_modal_calc').closest('.col').show();
                  $('#coupons_modal_calc_coupon_price').val(0);
                  $('#coupons_detail_modal_calc_result').text(calcProfit);
                  $('#coupons_detail_modal_calc_price').val(calcPrice);
                  $('#coupons_detail_modal_calc_name').text(calcName);
                  renderCouponInputs(suggestion.data.COUPONS);
                }
            }
        });
    }

    function renderCouponInputs(coupons) {
      calcCouponPriceArray = $.map( coupons, function(val, i){
        var retObj = {
          date: val['Дата выплаты'],
          price: 0,
        }
        if(!parseFloat(val['Купоны'])){
           return retObj;
        }
        retObj.price = parseFloat(val['Купоны']);
        return retObj;
      });

      $('#coupons_detail_modal_calc_list').html('');
      $.map(calcCouponPriceArray, function(val, i){
        $('#coupons_detail_modal_calc_list').append(function(){
          return $('<div class="flex_row row"><div class="col col_left col-xs-5"><p class="t16 white m0">' + val.date + '</p></div><div class="col col_left col-xs-7"><div class="form_element"><input type="text" class="coupons_detail_modal_calc_price" value="' + val.price + '" placeholder="Внесите купон, в&nbsp;руб."/></div></div></div>');
        });
      });
    }

    //калькулятор
    $('body').on('change keyup input','#coupons_modal_calc_price, #coupons_modal_calc_coupon_price', function() {
        calcPrice = parseFloat($('#coupons_modal_calc_price').val());
        calcCouponPrice = parseFloat($('#coupons_modal_calc_coupon_price').val());
        $.ajax({
            url: '/ajax/obligations/obligations_calc.php',
            data: {
                method: 'changePrice',
                code: calcCode,
                price: calcPrice,
                coupon_price: calcCouponPrice,
            },
            dataType: 'json',
            type: 'POST',
            success: function(data){
                try{
                    var calcResult = data.suggestions.data.PROFIT;
                    $('#coupons_modal_calc_result').text(calcResult);
                    $('#coupons_detail_modal_calc_result').text(calcResult);
                } catch(e) {
                    $('#coupons_modal_calc_result').text(0);
                    $('#coupons_detail_modal_calc_result').text(0);
                }
            }
        });
    });

    $('body').on('change keyup input', '.coupons_detail_modal_calc_price, #coupons_detail_modal_calc_price', function(e){
      var calcCouponPrice = $.map($('.coupons_detail_modal_calc_price'), function(val) {
        return parseFloat($(val).val()) || 0;
      });
      var detailedPrice = $('#coupons_detail_modal_calc_price').val();


      $.ajax({
            url: '/ajax/obligations/obligations_calc.php',
            data: {
                method: 'changeDetailedCouponPrice',
                price: detailedPrice,
                code: calcCode,
                coupon_price: calcCouponPrice,
            },
            dataType: 'json',
            type: 'POST',
            success: function(data){
                try{
                    var calcResult = data.suggestions.data.PROFIT;
                    $('#coupons_modal_calc_result').text(calcResult);
                    $('#coupons_detail_modal_calc_result').text(calcResult);
                } catch(e) {
                    $('#coupons_modal_calc_result').text(0);
                    $('#coupons_detail_modal_calc_result').text(calcResult);
                }
            }
        });

    });

$('body').find('.coupons_detail_modal_calc').click(function() {
  $('#coupons_modal_calc').modal('hide');
  setTimeout(function() {
    $('#coupons_detail_modal_calc').modal('show');
  }, 200);
});
$('body').find('#coupons_detail_modal_calc .back_btn').click(function() {
  $('#coupons_detail_modal_calc').modal('hide');
  setTimeout(function() {
    $('#coupons_modal_calc').modal('show');
  }, 200);
});
$('.main_menu_toggle a').click(function(e) {
  if ($(this).attr('href') == '#coupons_modal_toggle_btn') {
    e.preventDefault();
    couponsProfitRecount();
    $('.calc-action').hide();
    $('#autocomplete_obl_calc').val('');
    $('#coupons_modal_calc').modal('show');
  }
});
$('body').on('click', '.coupons_modal_toggle_btn', customCalcModalShow);
$('body').on('click', '.obl_detail_calc_incode_btn', customCalcModalShow);

function customCalcModalShow() {
  $('.modal').modal('hide');
  couponsProfitRecount();
  $('.calc-action').hide();
  $('#autocomplete_obl_calc').val('');

  var id = '';
  if ($('.obligation__name').length != 0) {
    id = $('.obligation__name').text();
    $('#autocomplete_obl_calc').val(id);
    setTimeout(function() {
      $('#autocomplete_obl_calc').trigger('keyup');
    }, 100);
    setTimeout(function() {
      $('#autocomplete_obl_calc').focus();
    }, 200);
  }

  $('#coupons_modal_calc').modal('show');
}
$('body').find('.coupons_detail_modal_tgl').click(function() {
  $('#coupons_modal').modal('hide');
  couponsDetailProfitRecount();
  setTimeout(function() {
    $('#coupons_detail_modal').modal('show');
  }, 200);
});

$('body').find('#coupons_modal_price').bind('change keyup input', function() {
  var ths = $(this);

  couponsValCheck(ths);
  $('body').find('#coupons_detail_modal_price').val(ths.val());
  couponsProfitRecount();
});
$('body').find('#coupons_modal_coupon_price').bind('change keyup input', function() {
  var ths = $(this);

  couponsValCheck(ths);
  couponsProfitRecount();
});
$('body').find('#coupons_detail_modal_price').bind('change keyup input', function() {
  var ths = $(this);

  couponsValCheck(ths);
  $('body').find('#coupons_modal_price').val(ths.val());
  couponsDetailProfitRecount();
});
$('body').find('.coupons_detail_modal_price').bind('change keyup input', function() {
  var ths = $(this);

  couponsValCheck(ths);
  couponsDetailProfitRecount();
});

function couponsValCheck(ths) {
  if (ths.val().match(/[^.0-9]/g)) {
    ths.val(ths.val().replace(/[^.0-9]/g, ''));
  }
}

function couponsProfitRecount() {
  var price = $('body').find('#coupons_modal_price').val();
  price = parseFloat(price);
  var coupon_price = parseFloat($('body').find('#coupons_modal_coupon_price').val());
  var nkd = parseFloat($('body').find('#nkd_value').text());

  var work_price = ((price * 1000) / 100) + nkd;
  if (isNaN(work_price)) {
    work_price = 0;
  }
  // ------

  var face_value = 1000;

  var sell_price = 0;
  var coupon_price_summ = 0;
  var not_defined_coupons_len = 0;
  var days_count = 0;
  var day_today = new Date();

  for (var i = 0; i < $('#coupons_table tbody tr').length; i++) {
    var tr = $('#coupons_table tbody tr').eq(i);
    var tr_before = $('#coupons_table tbody tr').eq(i - 1);

    var coupon_price_curr = tr.attr('data-coupon_price');
    if (coupon_price_curr == '') {
      coupon_price_curr = coupon_price;
    } else {
      coupon_price_curr = parseFloat(coupon_price_curr);
    }
    coupon_price_summ += coupon_price_curr;

    var coupon_date_curr = new Date(tr.attr('data-date'));
    var coupon_date_before;
    if (i == 0) {
      coupon_date_before = day_today;
    } else {
      coupon_date_before = new Date(tr_before.attr('data-date'));
    }

    var days_count_curr = Math.ceil(Math.abs(coupon_date_curr.getTime() - coupon_date_before.getTime()) / (1000 * 3600 * 24));
    days_count += days_count_curr;
  }

  sell_price = face_value + coupon_price_summ;

  var coupons_main_profit = (sell_price - work_price) / work_price;
  var coupons_year_profit = ((coupons_main_profit / days_count) * 365) * 100;

  coupons_year_profit = coupons_year_profit.toFixed(2)
  $('#coupons_modal_result').text(coupons_year_profit);
}

function couponsDetailProfitRecount() {
  var price = $('body').find('#coupons_detail_modal_price').val();
  price = parseFloat(price);
  // var coupon_price = parseFloat($('body').find('#coupons_modal_coupon_price').val());
  var nkd = parseFloat($('body').find('#nkd_value').text());

  var work_price = ((price * 1000) / 100) + nkd;
  if (isNaN(work_price)) {
    work_price = 0;
  }
  // ------

  var face_value = 1000;

  var sell_price = 0;
  var coupon_price_summ = 0;
  var not_defined_coupons_len = 0;
  var days_count = 0;
  var day_today = new Date();

  for (var i = 0; i < $('#coupons_detail_modal_list > .row').length; i++) {
    var tr = $('#coupons_detail_modal_list > .row').eq(i);
    var tr_before = $('#coupons_detail_modal_list > .row').eq(i - 1);

    var coupon_price_curr = parseFloat(tr.find('.coupons_detail_modal_price').val());
    coupon_price_curr = parseFloat(coupon_price_curr);
    coupon_price_summ += coupon_price_curr;

    var coupon_date_curr = new Date(tr.attr('data-date'));
    var coupon_date_before;
    if (i == 0) {
      coupon_date_before = day_today;
    } else {
      coupon_date_before = new Date(tr_before.attr('data-date'));
    }

    var days_count_curr = Math.ceil(Math.abs(coupon_date_curr.getTime() - coupon_date_before.getTime()) / (1000 * 3600 * 24));
    days_count += days_count_curr;
  }

  sell_price = face_value + coupon_price_summ;

  var coupons_main_profit = (sell_price - work_price) / work_price;
  var coupons_year_profit = ((coupons_main_profit / days_count) * 365) * 100;

  coupons_year_profit = coupons_year_profit.toFixed(2)
  $('#coupons_detail_modal_result').text(coupons_year_profit);

  console.log(coupons_main_profit, days_count, sell_price, work_price)
}
