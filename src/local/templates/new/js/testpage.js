if ($('.test_slider').length != 0) {
  var all_money = 400000;
  var all_money_final = 0;
  var all_money_tag = $('.q02-money span');

  all_money_tag.text(0);

  var val_1 = 0, val_2 = 0, val_3 = 0;


  $('.question_02 .test_slider').each(function() {
    var ths = $(this);

    ths.slider({
  		min: 0,
  		max: all_money,
  		value: 10,
      step: 1000,
      range: "min",
  		create: function(event, ui) {
        changeSlider(event, ui, ths);
      },
  		slide: function(event, ui) {
        changeSlider(event, ui, ths);
      },
  		change: function(event, ui) {
        changeSlider(event, ui, ths);
      }
    });
  });

  function changeSlider(event, ui, ths) {
    var ths_index = ths.attr('data-index');

    var siblingsSliders = ths.closest('.test_slider_outer').siblings('.test_slider_outer').find('.test_slider');

    var ths_val = ui.value;
    ths.siblings('.test_slider__before').find('span').text(ths_val);

    if (ths.attr('data-index') == '1') {
      val_1 = ths_val;
    } else if (ths.attr('data-index') == '2') {
      val_2 = ths_val;
    } else if (ths.attr('data-index') == '3') {
      val_3 = ths_val;
    }

    all_money_final = all_money - (val_1 + val_2 + val_3);

    // -----------------
    var values_arr = [];

    if (all_money_final < 0) {
      if (ths_index == '1') {
        curr_slider_1 = $('.test_slider[data-index="2"]');
        curr_slider_2 = $('.test_slider[data-index="3"]');
      } else if (ths_index == '2') {
        curr_slider_1 = $('.test_slider[data-index="1"]');
        curr_slider_2 = $('.test_slider[data-index="3"]');
      } else if (ths_index == '3') {
        curr_slider_1 = $('.test_slider[data-index="1"]');
        curr_slider_2 = $('.test_slider[data-index="2"]');
      }

      var curr_slider;

      curr_slider_1.slider('value', curr_slider_1.slider('value') - 1000);

      curr_slider_2.slider('value', curr_slider_2.slider('value') - 1000);
    }

    if (event.type != 'slidecreate') {
      all_money_tag.text(all_money_final.toLocaleString());
    } else {
      ths.slider('value', 120);
    }
  }
}

// if ($('.testpage__baffet_img').length != 0) {
//   $(window).on('load resize', testpageBaffetImgLogic);
// }

// function testpageBaffetImgLogic() {
//   var img = $('.testpage__baffet_img');
//   var img_width = $('.testpage__baffet').width();
//   img.css('width', img_width);
//
//   var img_height = img.outerHeight();
//   $('.testpage__baffet').css('padding-top', img_height + 20);
// }

// $('.testpage__title').click(toFinalScreen(0));

function toFinalScreen(result) {
  var msg = $('.testpage__baffet_txt_success');
  if (result < 50) {
    var msg = $('.testpage__baffet_txt_fail');
  }

  $('.testpage__outer').addClass('final');

  $('.testpage__outer .col-right').css('opacity', 0);

  setTimeout(function() {
    $('.testpage__outer .col-right').css('display', 'none');
  }, 300);
  setTimeout(function() {
    $('.testpage__baffet_txt').css('opacity', 0);
    $('.testpage__baffet_txt').css('display', 'none');
    msg.css('display', 'inline-block');
    $('.testpage__baffet_txt_bottom').css('display', 'inline-block');
  }, 600);
  setTimeout(function() {
    $('.testpage__baffet__result').css('opacity', 1);
  }, 900);
}

testpageSteps();
function testpageSteps() {
  var result = 0;

  // to second step
  $('.q01-iphone, .q01-shares').click(function() {
    $('.question_01').css('opacity', 0);
    setTimeout(function() {
      $('.question_01').css('display', 'none');
      $('.question_02').css('display', 'block');
  		$("body,html").animate({
  			scrollTop:0
  		}, 600);
    }, 300);
    setTimeout(function() {
      $('.question_02').css('opacity', 1);
    }, 400);

    if ($(this).hasClass('q01-iphone')) {
      result += 0;
    } else if ($(this).hasClass('q01-shares')) {
      result += 15;
    }

    setTimeout(function() {
      $('.testpage__baffet_lvl .line').css('max-height', result + '%');
      $('.testpage__baffet_percent span').text(result);
      $('.testpage__number_txt').text(2);
    }, 300);
  });

  // to third step
  $('.question_02 .btn-next').click(function() {
    $('.question_02').css('opacity', 0);
    setTimeout(function() {
      $('.question_02').css('display', 'none');
      $('.question_03').css('display', 'block');
  		$("body,html").animate({
  			scrollTop:0
  		}, 600);
    }, 300);
    setTimeout(function() {
      $('.question_03').css('opacity', 1);
    }, 400);

    var minimum = 80000;
    var slider1_val = parseInt($('.question_02 .test_slider[data-index="1"]').slider("option", "value"));
    var slider2_val = parseInt($('.question_02 .test_slider[data-index="2"]').slider("option", "value"));
    var slider3_val = parseInt($('.question_02 .test_slider[data-index="3"]').slider("option", "value"));
    console.log(slider1_val, slider2_val, slider3_val)

    if (slider1_val >= 80000 && slider2_val >= 80000) {
      result += 15;
    } else {
      result += 0;
    }

    setTimeout(function() {
      $('.testpage__baffet_lvl .line').css('max-height', result + '%');
      $('.testpage__baffet_percent span').text(result);
      $('.testpage__number_txt').text(3);
    }, 300);
  });

  // to fourth step
  $('.question_03 .btn-next').click(function() {
    $('.question_03').css('opacity', 0);
    setTimeout(function() {
      $('.question_03').css('display', 'none');
      $('.question_04').css('display', 'block');
  		$("body,html").animate({
  			scrollTop:0
  		}, 600);
    }, 300);
    setTimeout(function() {
      $('.question_04').css('opacity', 1);
    }, 400);

    if ($('#q3_3').prop('checked') == true) {
      result += 15;
    } else {
      result += 0;
    }

    setTimeout(function() {
      $('.testpage__baffet_lvl .line').css('max-height', result + '%');
      $('.testpage__baffet_percent span').text(result);
      $('.testpage__number_txt').text(4);
    }, 300);
  });

  // to fifth step
  $('.question_04 .test_slider').slider({
		min: 0,
		max: 100,
		value: 50,
    step: 1,
    range: "min"
  });
  $('.question_04 .btn-next').click(function() {
    $('.question_04').css('opacity', 0);
    setTimeout(function() {
      $('.question_04').css('display', 'none');
      $('.question_05').css('display', 'block');
  		$("body,html").animate({
  			scrollTop:0
  		}, 600);
    }, 300);
    setTimeout(function() {
      $('.question_05').css('opacity', 1);
    }, 400);

    var slider5_val = parseInt($('.question_04 .test_slider[data-index="5"]').slider("option", "value"));
    console.log(slider5_val);

    if (slider5_val >= 45 && slider5_val < 70) {
      result += 5;
    } else if (slider5_val >= 70) {
      result += 15;
    } else {
      result += 0;
    }

    setTimeout(function() {
      $('.testpage__baffet_lvl .line').css('max-height', result + '%');
      $('.testpage__baffet_percent span').text(result);
      $('.testpage__number_txt').text(5);
    }, 300);
  });

  // to final screen
  $('.question_05 .btn-next').click(function() {
    $("body,html").animate({
      scrollTop:0
    }, 600);
    if ($('#q5_2').prop('checked') == true) {
      result += 10;
    }
    if ($('#q5_3').prop('checked') == true) {
      result += 10;
    }

    toFinalScreen(result);

    setTimeout(function() {
      $('.testpage__baffet_lvl .line').css('max-height', result + '%');
      $('.testpage__baffet_percent span').text(result);
    }, 300);
  });
}
