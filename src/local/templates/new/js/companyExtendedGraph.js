function companyExtendedRefresh() {
    //графики
    var chart, counter = 0;
    var chartRedraw = function() {
        if (counter > 0) {
            if ($('body').find("#key_indicators_quarters_chart")) {
                key_indicators_quarters_table =  $('body').find("#key_indicators_quarters_chart").data("value");
                key_indicators_quarters_table = JSON.stringify(key_indicators_quarters_table);
                //console.log('asdasd', key_indicators_quarters_table)
                $('body').find("#key_indicators_quarters_chart").siblings('iframe').remove();
                var parent = $('body').find("#key_indicators_quarters_chart").closest('.company_chart_outer');
                $('body').find("#key_indicators_quarters_chart").remove();
                parent.append("<canvas id='key_indicators_quarters_chart' class='company_chart' data-value='"+ key_indicators_quarters_table +"'></canvas>");
            }
            if ($('body').find("#key_indicators_years_chart")) {
                key_indicators_years_table =  $('body').find("#key_indicators_years_chart").data("value");
                key_indicators_years_table = JSON.stringify(key_indicators_years_table);
                $('body').find("#key_indicators_years_chart").siblings('iframe').remove();
                var parent = $('body').find("#key_indicators_years_chart").closest('.company_chart_outer');
                $('body').find("#key_indicators_years_chart").remove();
                parent.append("<canvas id='key_indicators_years_chart' class='company_chart' data-value='"+ key_indicators_years_table +"'></canvas>");
            }
            if ($('body').find("#key_indicators_industries_quarters_chart")) {
                key_indicators_industries_quarters_table =  $('body').find("#key_indicators_industries_quarters_chart").data("value");
                key_indicators_industries_quarters_table = JSON.stringify(key_indicators_industries_quarters_table);
                //console.log('asdasd', key_indicators_quarters_table)
                $('body').find("#key_indicators_industries_quarters_chart").siblings('iframe').remove();
                var parent = $('body').find("#key_indicators_industries_quarters_chart").closest('.company_chart_outer');
                $('body').find("#key_indicators_industries_quarters_chart").remove();
                parent.append("<canvas id='key_indicators_industries_quarters_chart' class='company_chart' data-value='"+ key_indicators_industries_quarters_table +"'></canvas>");
            }
            if ($('body').find("#key_indicators_industries_years_chart")) {
                key_indicators_industries_years_table =  $('body').find("#key_indicators_industries_years_chart").data("value");
                key_indicators_industries_years_table = JSON.stringify(key_indicators_industries_years_table);
                //console.log('asdasd', key_indicators_quarters_table)
                $('body').find("#key_indicators_industries_years_chart").siblings('iframe').remove();
                var parent = $('body').find("#key_indicators_industries_years_chart").closest('.company_chart_outer');
                $('body').find("#key_indicators_industries_years_chart").remove();
                parent.append("<canvas id='key_indicators_industries_years_chart' class='company_chart' data-value='"+ key_indicators_industries_years_table +"'></canvas>");
            }
            if ($('body').find("#key_indicators_macro_month_chart")) {
                key_indicators_macro_month_table =  $('body').find("#key_indicators_macro_month_chart").data("value");
                key_indicators_macro_month_table = JSON.stringify(key_indicators_macro_month_table);
                //console.log('asdasd', key_indicators_quarters_table)
                $('body').find("#key_indicators_macro_month_chart").siblings('iframe').remove();
                var parent = $('body').find("#key_indicators_macro_month_chart").closest('.company_chart_outer');
                $('body').find("#key_indicators_macro_month_chart").remove();
                parent.append("<canvas id='key_indicators_macro_month_chart' class='company_chart' data-value='"+ key_indicators_macro_month_table +"'></canvas>");
            }

            if ($('body').find("#key_indicators_macro_years_chart")) {
                key_indicators_macro_years_table =  $('body').find("#key_indicators_macro_years_chart").data("value");
                key_indicators_macro_years_table = JSON.stringify(key_indicators_macro_years_table);
                $('body').find("#key_indicators_macro_years_chart").siblings('iframe').remove();
                var parent = $('body').find("#key_indicators_macro_years_chart").closest('.company_chart_outer');
                $('body').find("#key_indicators_macro_years_chart").remove();
                parent.append("<canvas id='key_indicators_macro_years_chart' class='company_chart' data-value='"+ key_indicators_macro_years_table +"'></canvas>");
            }
        }

        var key_indicators_quarters_table =  [];
        if($('body').find("#key_indicators_quarters_chart").data("value")){
            key_indicators_quarters_table = $('body').find("#key_indicators_quarters_chart").data("value");
        }
        var key_indicators_years_table =  [];
        if($('body').find("#key_indicators_years_chart").data("value")){
            key_indicators_years_table = $('body').find("#key_indicators_years_chart").data("value");
        }

        var key_indicators_macro_month_table =  [];
        if($('body').find("#key_indicators_macro_month_chart").data("value")){
            key_indicators_macro_month_table = $('body').find("#key_indicators_macro_month_chart").data("value");
        }
        var key_indicators_macro_years_table =  [];
        if($('body').find("#key_indicators_macro_years_chart").data("value")){
            key_indicators_macro_years_table = $('body').find("#key_indicators_macro_years_chart").data("value");
        }


        var key_indicators_industries_years_table =  [];
        if($('body').find("#key_indicators_industries_years_chart").data("value")){
            key_indicators_industries_years_table = $('body').find("#key_indicators_industries_years_chart").data("value");
        }
        var key_indicators_industries_quarters_table =  [];
        if($('body').find("#key_indicators_industries_quarters_chart").data("value")){
            key_indicators_industries_quarters_table = $('body').find("#key_indicators_industries_quarters_chart").data("value");
        }

        $('body').find('.company_chart').each(function() {
            var ctx = $(this);
            var currencySign = 'руб.';
            var blue_prefix = 'Котировки акций, ';
            var labels = [], values_line = [], values_bar = [];
            if(ctx.closest('.company_chart_outer').siblings('.transform_table').find('tr.active').find('td:first-child .name').length) {
                var active_prefix = ctx.closest('.company_chart_outer').siblings('.transform_table').find('tr.active').find('td:first-child .name').text();
            } else {
                var active_prefix = ctx.closest('.company_chart_outer').siblings('.transform_table').find('tr.active').find('td:first-child').text();
            }
            var active_postfix = ctx.closest('.company_chart_outer').siblings('.transform_table').find('tr.active').attr('data-postfix');
            if(ctx.parents('.tab-pane').attr('data-blue-name')!=undefined){
                blue_prefix = ctx.parents('.tab-pane').attr('data-blue-name')+', ';
            }
            var chart_arr = [];
            //console.log('ctx', ctx);
            if(ctx.parents('.tab-pane').length && ctx.parents('.tab-pane').attr('data-currency-sign').length>0){
                currencySign = ctx.parents('.tab-pane').attr('data-currency-sign');
            }
            if (ctx.attr('id') == 'key_indicators_quarters_chart') {
                chart_arr = key_indicators_quarters_table;
            } else if (ctx.attr('id') == 'key_indicators_years_chart') {
                chart_arr = key_indicators_years_table;
            } else if (ctx.attr('id') == 'key_indicators_macro_month_chart') {
                chart_arr = key_indicators_macro_month_table;
            } else if (ctx.attr('id') == 'key_indicators_macro_years_chart') {
                chart_arr = key_indicators_macro_years_table;
            } else if (ctx.attr('id') == 'key_indicators_industries_quarters_chart') {
                chart_arr = key_indicators_industries_quarters_table;
            } else if (ctx.attr('id') == 'key_indicators_industries_years_chart') {
                chart_arr = key_indicators_industries_years_table;
            }
            //console.log('chart_arr: ', chart_arr);

            for (elem in chart_arr) {
                labels.push(chart_arr[elem][0]);

                values_line.push(parseFloat(chart_arr[elem][1]));
                //console.log('chart_arr0', chart_arr[elem])
                var bar_val_curr = 0;
                //ctx.closest('.company_chart_outer').siblings('.transform_table').find('tr.active td:not(.tableCurrentPeriodCell.hidden)').each(function() {
                ctx.closest('.company_chart_outer').siblings('.transform_table').find('tr.active td:not(.tableCurrentPeriodCell)').each(function() {
                    if ($(this).attr('data-date') == chart_arr[elem][0]) {
                        bar_val_curr = parseFloat($(this).attr('data-val'));
                    }
                });
                values_bar.push(bar_val_curr);
            }

            //Перезаполняем массив для столбцов для макро-показателей
            if(ctx.parents('.tab-content').find('.macro_table').find('select').length){
                var active_macro = ctx.parents('.tab-content').find('.macro_table').find('select option:selected');
                console.log('id', ctx.attr('id'));
                var secondMacroArr = [];
                if(ctx.attr('id') == 'key_indicators_macro_month_chart'){
                    secondMacroArr = active_macro.data('chart-m');
                } else if(ctx.attr('id') == 'key_indicators_macro_years_chart'){
                    secondMacroArr = active_macro.data('chart-y');
                }
                values_bar = [];
                for (elem in secondMacroArr) {

                    values_bar.push(parseFloat(secondMacroArr[elem][1]));
                }
                active_prefix = active_macro.text();
                active_postfix = ' '+active_macro.attr('data-currency-sign');
            }

           // console.log(values_line);
          //  console.log('chart_arr',chart_arr);
            //console.log(values_bar);

            setTimeout(function() {
                var chart_datasets = [];
                var scales_yAxes = [];
                chart_datasets = [{
                    type: 'line',
                    label: blue_prefix+currencySign,
                    yAxisID: 'y-axis-1',
                    backgroundColor: 'rgba(134, 182, 224, 0)',
                    borderColor: 'rgb(134, 182, 224)',
                    data: values_line,
                }, ];

                scales_yAxes = [{
                    type: 'linear',
                    position: 'left',
                    id: 'y-axis-1',
                    ticks: {
                        beginAtZero: true
                    }
                }, ];

                //console.log('values_bar', values_bar);
                if(values_bar.length>0){//Если есть данные по второму графику
                    chart_datasets.push(
                        {
                            label: active_prefix,
                            yAxisID: 'y-axis-2',
                            backgroundColor: 'rgb(253, 198, 49)',
                            borderColor: 'rgb(253, 198, 49)',
                            data: values_bar,
                        }
                    );
                    scales_yAxes.push({
                        type: 'linear',
                        position: 'right',
                        reverse: true,
                        id: 'y-axis-2',
                        ticks: {
                            beginAtZero: true
                        }
                    });

                }




                chart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: labels,
                        datasets: chart_datasets
                    },
                    options: {
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: scales_yAxes,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            callbacks: {
                                label: function (t, d) {
                                    if (t.datasetIndex === 0) {
                                        return t.yLabel + ' ' + currencySign;
                                    } else if (t.datasetIndex === 1) {
                                        return t.yLabel + active_postfix;
                                    }
                                }
                            }
                        }
                    }
                });
            }, 10);
        });





    }
    chartRedraw();

    $('input[name=show_industry_data]').on('change', function(){
        //var checked = $(this).find('input').prop('checked');
        var checked = $(this).is(':checked')?true:false;
        console.log('checked', checked);
        if(checked==true){
            $('.mid_vals_table').removeClass('hidden');
        } else {
            $('.mid_vals_table').addClass('hidden');
        }
        // alert('checkbox = '+checked);
        $( ".checkbox.midval-show input").prop('checked', checked);
    });

    $('body').on('click', '.transform_table tr', function() {
        $(this).closest('tbody').find('tr').removeClass('active');
        $(this).addClass('active');
        counter ++;
        chartRedraw();
        counter = 0;
    });

    $('body').on('change', '.macro_table select', function() {
        //$(this).closest('.macro_table').find('a').removeClass('active');
        //$(this).addClass('active');
        counter ++;
        chartRedraw();
        counter = 0;
    });

}

function chartIndustriesCapaRedraw(){

            if ($('body').find("#key_capa_quarters_chart")) {
                key_capa_quarters_table =  $('body').find("#key_capa_quarters_chart").data("value");
                key_capa_quarters_table = JSON.stringify(key_capa_quarters_table);
                //console.log('asdasd', key_indicators_quarters_table)
                $('body').find("#key_capa_quarters_chart").siblings('iframe').remove();
                var parent = $('body').find("#key_capa_quarters_chart").closest('.company_chart_outer');
                $('body').find("#key_capa_quarters_chart").remove();
                parent.append("<canvas id='key_capa_quarters_chart' class='company_chart_capa' data-value='"+ key_capa_quarters_table +"'></canvas>");
            }
            if ($('body').find("#key_capa_years_chart")) {
                key_capa_years_table =  $('body').find("#key_capa_years_chart").data("value");
                key_capa_years_table = JSON.stringify(key_capa_years_table);
                //console.log('asdasd', key_indicators_quarters_table)
                $('body').find("#key_capa_years_chart").siblings('iframe').remove();
                var parent = $('body').find("#key_capa_years_chart").closest('.company_chart_outer');
                $('body').find("#key_capa_years_chart").remove();
                parent.append("<canvas id='key_capa_years_chart' class='company_chart_capa' data-value='"+ key_capa_years_table +"'></canvas>");
            }
 $('body').find('.company_chart_capa').each(function() {
            var ctx = $(this);
        var key_capa_table = ctx.data("value");

			   var chart_datasets = [];
            var currencySign = '';
            var legend_name = '';
				var active_prefix = '';
				var active_postfix = '';
            var labels = [], values_line = [], values_bar = [];
            var chart_arr = [];
     //   console.log('ctx', ctx);
            if(ctx.parents('.tab-pane').length && ctx.parents('.tab-pane').attr('data-currency-sign').length>0){
                currencySign = ctx.parents('.tab-pane').attr('data-currency-sign');
            }
                chart_arr = key_capa_table;

      //  console.log('chart_arr: ', chart_arr);

            for (elem in chart_arr) {
                labels.push(chart_arr[elem][0]);
                var bar_val_curr = 0;
                values_bar.push(parseFloat(chart_arr[elem][1]).toFixed(2));
            }
				if(ctx.parents('.tab-pane').attr('data-legend-name')!=undefined){
                legend_name = ctx.parents('.tab-pane').attr('data-legend-name');
            }
				if(currencySign.length>0){
					legend_name = legend_name+', '+currencySign;
				}
		 // console.log(labels);
				setTimeout(function() {
               var chart_datasets = [];
                var scales_yAxes = [];
                chart_datasets = [{
                    type: 'bar',
                            label: legend_name,
                            yAxisID: 'y-axis-2',
                            backgroundColor: 'rgb(253, 198, 49)',
                            borderColor: 'rgb(253, 198, 49)',
                            data: values_bar,
                }, ];

                scales_yAxes = [{
                        type: 'linear',
                        position: 'right',
                        reverse: true,
                        id: 'y-axis-2',
                        ticks: {
                            beginAtZero: true
                        }
                    }, ];

	                chart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: labels,
                        datasets: chart_datasets
                    },
                    options: {
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: scales_yAxes,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            callbacks: {
                                label: function (t, d) {
                                    if (t.datasetIndex === 0) {
                                        return t.yLabel + ' ' + currencySign;
                                    } else if (t.datasetIndex === 1) {
                                        return t.yLabel + active_postfix;
                                    }
                                }
                            }
                        }
                    }
                });
					 }, 10);

					 });
}