var market_selector = 'all';
var mmap_filters = [];
var curr_element_tag;
//marketMapInit(market_selector);
$(document).ready(function(){
	marketMapInit(market_selector);

});

var calcMain = function() {
	//форма подсчета

	let tableType
	let coockieName
	if($('.shares_table').length){
		tableType = 'shares_table';
		coockieName = 'id_arr_shares_cookie';
	}
	if($('.shares_usa_table').length){
		tableType = 'shares_usa_table';
		coockieName = 'id_arr_shares_usa_cookie';
	}
  //	console.log('coockieName: ' + coockieName + ', tableType: ' + tableType);
	if ($('.calculate_outer').length != 0) {
		//автоматический подсчет даты гашения для таблицы акций
		$('.' + tableType + ' .date_off').each(function() {
			var today = new Date();
			today.setFullYear(today.getFullYear() + 2);

			var today_day = '01';

			var today_month = today.getMonth() + 1;
			today_month = today_month.toString()
			if (today_month.length < 2) {
				today_month = '0' + today_month;
			}

			var today_year = today.getFullYear();

			$(this).attr('data-date', today_year + '-' + today_month + '-' + today_day);
		});
		//автоматический подсчет цены гашения для таблицы акций
		persentsRefresh();
		function persentsRefresh() {
			$('.' + tableType + ' .cshare_up_p, .' + tableType + ' .cshare_down_p').each(function() {
				var val_p = parseFloat($(this).attr('data-value'));
				if ($(this).attr('data-value') && (val_p != NaN && val_p != '' && val_p != 0 && val_p != '0')) {
					$(this).find('span').text(val_p);
				} else {
					val_p = '-';
					$(this).text(val_p);
				}

				var curr_price = parseFloat($(this).closest('tr').find('.elem_buy_price').attr('data-price'));

				var val = (curr_price * val_p) / 100;
				var final_val = 0;
				if ($(this).hasClass('cshare_up_p')) {
					final_val = curr_price + val;
					$(this).closest('tr').find('.sell_price_up').attr('data-price', final_val);
				} else {
					final_val = curr_price - val;
					$(this).closest('tr').find('.sell_price_down').attr('data-price', final_val);
				}
			});
		}
		//автоматический подсчет доходности и просадки для таблицы акций
		$('.' + tableType + ' tr').each(function() {
			var sell_pice_elem,
				buy_price_elem;

			if (sell_pice_elem != '-') {
				sell_pice_elem = parseFloat($(this).find('.sell_price_up').attr('data-price'));
			} else {
				sell_pice_elem = 0;
			}

			if (buy_price_elem != '-') {
				buy_price_elem = parseFloat($(this).find('.elem_buy_price').attr('data-price'));
			} else {
				buy_price_elem = 0;
			}

			//общая доходность
			var profit_main = ((sell_pice_elem - buy_price_elem) / buy_price_elem) * 100;
			$(this).find('.profit_main_up').attr('data-value', profit_main);

			//годовая доходность
			var date_today = new Date();
			var date_off = new Date($(this).find('.date_off').attr('data-date'));
			var timeDiff = Math.abs(date_off.getTime() - date_today.getTime());
			date_off_today = Math.ceil(timeDiff / (1000 * 3600 * 24));

			var profit_year = (profit_main / date_off_today) * 365;
			$(this).find('.profit_year_up').attr('data-value', profit_year);
		});

		var money_main,		//Сумма инвестиций
			money_in_work,	//Распределено
			money_free,		//Не распределено
			/* balance = [],	 */ //прирост за месяц
			refill = 0,			//размер ежемесячного пополнения счёта
			main_arr = [],	//финальный массив для вывода на график
			main_arr_curr = [],	//элемент финального массива
			main_arr_second = [],	//финальный массив акций для вывода на график
			main_arr_second_curr = [],	//элемент финального массива акций

			table_elem_name,//имя текущей облигации
			table_elem = [],// массив из имени и общей цены выбранной облигации
			obligations_arr = [],// массив из выбранных облигаций
			month_summ,		// сумма цен покупки выбранных облигаций за текущий месяц
			share_month_summ,		// сумма цен покупки выбранных акций за текущий месяц

			profit_main,	//общая доходность
			profit_year,	//годовая доходность

			elem_buy_price_one,	//Цена покупки одной текущей облигации
			elem_buy_price,	//Цена покупки текущей облигации * количество лотов
			elem_sell_price_one,	//Цена продажи одной текущей облигации
			elem_sell_price,	//Цена продажи текущей облигации * количество лотов
			numberof,		//Кол-во лотов текущей облигации
			currency_value, //Курс текущей валюты
			currency_text;	//Текущая валюта, текст

		var date_today,		//сегодня
			date_today_day,
			date_today_month,
			date_today_year,
			date_off,		//дата гашения
			date_off_today,	//количество дней между датой гашения и сегодня
			dates_arr = [], //массив из сегодня и дат гашения облигаций
			date_max,		//максимальная дата гашения в массиве дат гашения
			dates_period_arr = [], //массив из первых дней каждого месяца между сегодня и датой гашения, так же включает в себя сегодня

			months_final;	//количество месяцев в промежутке между сегодня и максимальной датой

		var donut_arr = [];
		var donut_arr_text = [];
		var donut_arr_values = [];
		var curr_donut_arr = [];
		var curr_donut_val;

		var money_in_work_arr = [];
		var main_arr_values = [];
		var main_arr_shares_up_values = [];
		var main_arr_shares_down_values = [];
		var refill_arr_values = [];
		var profit_year_arr = [];
		var profit_year_up_arr = [];
		var profit_year_down_arr = [];

		var pie, chart;

		//автоматическое выставление ежемесячного пополнения из куков
		if ($.cookie('refill_cookie') && $.cookie('refill_cookie') != 0 && $.cookie('refill_cookie') != '') {
			$('#replenishment').prop('checked', true);
			$('#refill_value').val($.cookie('refill_cookie'));
			refill = parseFloat($.cookie('refill_cookie'));
		} else {
			$('#replenishment').prop('checked', false);
			$('#refill_value').val(0);
			refill = 0;
		}

		//автоматическое выставление ИИС из куков
		if ($.cookie('iis_cookie') && $.cookie('iis_cookie') != 0 && $.cookie('iis_cookie') != '') {
			$('#iis').prop('checked', true);
		} else {
			$('#iis').prop('checked', false);
		}

		//цены
		var priceTextChange = function() {
			money_main = parseFloat($('#money_main').val());

			money_in_work = parseFloat($('#money_in_work').val());
			money_free = money_main - money_in_work;

			$('#money_main_text').text(money_main.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$('#money_in_work_text').text(money_in_work.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$('#money_free_text').text(money_free.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
		}

		var all_income = 0,
			all_income_max = 0,
			all_income_min = 0;
		var allIncomeChange = function() {
			if (all_income === 'NaN' || all_income === NaN || all_income == '') {
				all_income = 0;
			} else {
				all_income = all_income.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			}
			$('#all_income_text').text(all_income);

			all_income_max = all_income_max.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			$('#shares_profit_max_text').text(all_income_max);

			all_income_min = all_income_min.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			$('#shares_profit_min_text').text(all_income_min);
		}
		allIncomeChange();
		//-------

		//попап указания суммы инвестиций
		$('#popup_investment_summ').bind("change keyup input", function(eventObject) {
			// var pos = getCursorPosition( document.getElementById('popup_investment_summ') );
			var txt;
			//запрет на ввод всего кроме цифр
			if (this.value.match(/[^0-9]/g)) {
				this.value = this.value.replace(/[^0-9]/g, '');
			}

			txt = $(this).val();
			if (txt.length > 18) {
				txt = txt.slice(0, 18);
				$(this).val(txt);
			}
		});


		$('.popup_refill_change_summ').click(function() {
			setTimeout(function() {
				$('#popup_investment_amount').modal('show');
				var val = $('#popup_investment_summ').val();
				$('#popup_investment_summ').attr('data-canonical', val);
			}, 600);
		});

		if ($.cookie('popup_cookie') != 1) {
			$.cookie('popup_cookie', 0, {expires: 1, path: '/'});
			$('#popup_investment_amount').modal('show');
		} else {
			$('#money_main').val($.cookie('summ_cookie'));
			priceTextChange();
		}
		//console.log('cookie: ' + $.cookie('popup_cookie'));
		//console.log('cookie, размер инвестиций: ' + $.cookie('summ_cookie'));
		$('.popup_refill_submit').click(function() {
			$('#popup_investment_amount').removeClass('origin');
			var new_val = $('#popup_investment_summ').val();
			new_val = new_val.replace(/[^0-9]/g, '');
			new_val = parseFloat(new_val);

			var canonical_val = $('#popup_investment_summ').attr('data-canonical');
			canonical_val = parseFloat(canonical_val);

			var in_work_val = $('#money_in_work').val();
			in_work_val = parseFloat(in_work_val);

			var tooltip_txt = '';

			$('#popup_investment_summ').tooltip({
				html: true,
				placement: 'top',
				trigger: 'manual'
			});

			if (new_val < in_work_val || new_val < 10000) {
				if (new_val < in_work_val) {
					tooltip_txt = 'Значение суммы инвестиций должно быть больше текущей суммы распределённых средств! <br/></br><span class="strong">В данный момент распределено: ' + in_work_val + ' руб.</span>';
				} else if (new_val < 10000) {
					tooltip_txt = 'Значение суммы инвестиций должно быть больше <span class="strong">10&nbsp;000&nbsp;руб.</span>';
				} else {
					tooltip_txt = '';
				}

				$('#popup_investment_summ').attr('data-original-title', tooltip_txt);

				setTimeout(function() {
					$('#popup_investment_summ').tooltip('show');
				}, 300)
			} else {
				$('#popup_investment_summ').tooltip('hide');
				$('#popup_investment_amount').modal('hide');
			}


		})

		$('#popup_investment_amount').on('hide.bs.modal', function(e) {
			var val = $('#popup_investment_summ').val();
			val = val.replace(/[^0-9]/g, '');
			val = parseInt(val);
			$.cookie('summ_cookie', val, {expires: 1, path: '/'});
			$('#money_main').val(val);
			priceTextChange();
			$.cookie('popup_cookie', 1, {expires: 1, path: '/'});
			console.log('cookie: ' + $.cookie('popup_cookie'));
			console.log('cookie: ' + $.cookie('summ_cookie'));
		})
		//----------

		//доходность
		$('.calculate_table table tbody tr').each(function() {
			var tr = $(this);
			date_today = new Date();

			tr.find('.numberof').attr('data-working_value', tr.find('.numberof').val());

			if (tr.attr('data-currency') == 'rub') {
				currency_value = 1;
			} else if (tr.attr('data-currency') == 'usd') {
				currency_value = parseFloat($('#usd_currency').val());
			} else if (tr.attr('data-currency') == 'eur') {
				currency_value = parseFloat($('#eur_currency').val());
			}

			var profit_current = 0;

			if ($(this).closest('.calculate_table').hasClass('debstock_table')) {
				profit_current = parseFloat(tr.find('.elem_sell_price').attr('data-price')) * currency_value;
			}

			var elem_buy_price_one_current = parseFloat(tr.find('.elem_buy_price').attr('data-price')) * currency_value;

		});
		//-------

		var dateTextWrite = function() {
			$('.calculate_table').find('.date_off').each(function() {
				var curr_date = $(this).attr('data-date');
				var curr_date_arr = curr_date.split('-');

				if (curr_date_arr[1].length < 2)
					curr_date_arr[1] = '0' + curr_date_arr[1];
				if (curr_date_arr[2].length < 2)
					curr_date_arr[2] = '0' + curr_date_arr[2];

				$(this).text(curr_date_arr[2] + '.' + curr_date_arr[1] + '.' + curr_date_arr[0])
			});
		}
		dateTextWrite();
		//-------

		//-------
		summCheck();

		var ths;
		var tr;

		var asd = 0;
		var curr_element_tag;


		//проверка на наличии акций в куках
		if ( $.cookie(coockieName) ) {
			var id_arr_shares = [];
			if ($.cookie(coockieName) !== null && $.cookie(coockieName) !== 'null') {
				id_arr_shares = JSON.parse($.cookie(coockieName));
			}
			//	console.log('Полученный массив акций из cookie: ' + id_arr_shares);

			for (var i = 0; i < id_arr_shares.length; i++) {
				var elem_arr = id_arr_shares[i].split('...');
				$('.calculate_table.' + tableType).find('tr[data-id="' + elem_arr[0] + '"]').find('.numberof').val(elem_arr[1]);
				$('.calculate_table.' + tableType).find('tr[data-id="' + elem_arr[0] + '"]').find('.checkbox input').prop('checked', true);
				$('.calculate_table.' + tableType).find('tr[data-id="' + elem_arr[0] + '"]').addClass('checked');

				curr_ths = $('.calculate_table.' + tableType).find('tr[data-id="' + id_arr_shares[i] + '"]').find('.checkbox input');
				curr_tr = curr_ths.closest('tr');

				curr_element_tag == 'input_checkbox';
				summCheck();

				curr_tr.addClass('checked');
				curr_tr.closest('tbody').prepend(curr_tr);
			}
		}

		$('.calculate_table').on('change', '.checkbox input', function() {
			// console.log('check');
			if ($(this).closest('tr').attr('data-access') && ($(this).closest('tr').attr('data-access') == false || $(this).closest('tr').attr('data-access') == 'false')) {
				$('#buy_subscribe_popup').modal('show');
				$(this).prop('checked', false);
			} else {
				var summ_col = $('.calculate_summ .shares_profit_max, .calculate_summ .shares_profit_min').closest('.col');

				if ($('.' + tableType + ' tbody .checkbox input:checked').length != 0) {
					if ($('.calculate_summ.with_shares').length == 0) {
						$('.calculate_summ').addClass('with_shares');
						summ_col.css({
							'display': 'block',
							'visibility': 'visible'
						});
						summ_col.css('opacity', 1);
					}
				} else {
					summ_col.css('opacity', 0);
					setTimeout(function() {
						$('.calculate_summ').removeClass('with_shares');
						summ_col.css({
							'display': 'none',
							'visibility': 'hidden'
						});
					}, 300);
				}

				//обнуление куков если не выбрано ни одной бумаги else {

				if ($(this).closest('.calculate_table').hasClass(tableType) && $(this).closest('tbody .checkbox input:checked').length == 0) {
					$.cookie(coockieName, '', {expires: 1, path: '/'});
				}

				curr_ths = $(this);
				curr_tr = curr_ths.closest('tr');

				curr_element_tag = 'input_checkbox';

				summCheck();
			}
		});

		$(window).load(function() {
			var summ_col = $('.calculate_summ .shares_profit_max, .calculate_summ .shares_profit_min').closest('.col');

			if ($('.' + tableType + ' tbody .checkbox input:checked').length != 0) {
				if ($('.calculate_summ.with_shares').length == 0) {
					$('.calculate_summ').addClass('with_shares');
					summ_col.css({
						'display': 'block',
						'visibility': 'visible'
					});
					summ_col.css('opacity', 1);
				}
			} else {
				summ_col.css('opacity', 0);
				setTimeout(function() {
					$('.calculate_summ').removeClass('with_shares');
					summ_col.css({
						'display': 'none',
						'visibility': 'hidden'
					});
				}, 300);
			}
		});

		$('.calculate_table').on('change', '.checkbox input.calculate_table_all', function() {
			if (($(this).attr('id') == 'calculate_table_all_share') && ($('.wrapper').attr('data-have-access') == false || $('.wrapper').attr('data-have-access') == 'false')) {
				$('#buy_subscribe_popup').modal('show');
				$(this).prop('checked', false);
			} else {
				var curr_summ = 0;

				var investment_summ = $('#popup_investment_summ').val();
				investment_summ = investment_summ.replace(/[^0-9]/g, '');
				investment_summ = parseInt(investment_summ);

				curr_ths_ths = $(this);
				curr_element_tag = 'input_checkbox_all';

				curr_ths_ths.closest('table').find('tbody .checkbox input').each(function() {
					curr_ths = $(this);
					curr_tr = curr_ths.closest('tr');

					var ths_price = parseFloat(curr_tr.find('.elem_buy_price').attr('data-price'));
					var ths_number = parseFloat(curr_tr.find('.numberof').val());
					var ths_currency_txt = curr_tr.attr('data-currency');
					var ths_currency = 1;

					if (ths_currency_txt == 'usd')
						ths_currency = parseFloat($('#usd_currency').val());
					else if (ths_currency_txt == 'eur')
						ths_currency = parseFloat($('#eur_currency').val());

					var ths_summ = ths_price * ths_number * ths_currency;

					if (curr_ths_ths.prop('checked')) {
						curr_summ += ths_summ;

						if (curr_summ > investment_summ) {
							// curr_tr.removeClass('checked');
							curr_tr.addClass('error');
							$('#popup_money_error').modal('show');
							curr_summ -= ths_summ;

							// setTimeout(function() {
							// 	for (var i = 0; i < curr_tr.closest('tbody tr.checked').length ; i ++) {
							// 		curr_tr.closest('tbody').find('tr.checked').eq(i).after(curr_tr);
							// 	}
							// }, 300);
							// setTimeout(function() {
							// 	curr_tr.removeAttr('style');
							// 	curr_tr.addClass('asd');
							// }, 310);

							return false;
						} else {
							curr_ths.prop('checked', true);

							curr_tr.css('opacity', 0);
							curr_tr.addClass('checked');

							// setTimeout(function() {
							// 	curr_tr.closest('tbody').prepend(curr_tr);
							// }, 300);
							// setTimeout(function() {
							// 	curr_tr.removeAttr('style');
							// }, 310);
						}
					} else {
						curr_ths.prop('checked', false);

						curr_tr.removeClass('checked');
						// curr_tr.css('opacity', 0);

						// setTimeout(function() {
						// 	for (var i = 0; i < curr_tr.closest('tbody tr.checked').length ; i ++) {
						// 		curr_tr.closest('tbody').find('tr.checked').eq(i).after(curr_tr);
						// 	}
						// }, 300);
						// setTimeout(function() {
						// 	curr_tr.removeAttr('style');
						// }, 310);
					}

					curr_tr.removeAttr('style');
				});

				summCheck();
				curr_ths.closest('tbody tr').removeAttr('style');
			}

			if ($(this).prop('checked') == false) {
				if ($(this).closest('.calculate_table').hasClass('debstock_table')) {
					$.cookie('id_arr_debstock_cookie', '', {expires: 1, path: '/'});
				} else if ($(this).closest('.calculate_table').hasClass(tableType)) {
					$.cookie(coockieName, '', {expires: 1, path: '/'});
				}
			}
		});

		$('.numberof').bind("change", function(eventObject) {
			if ($(this).closest('tr').attr('data-access') && ($(this).closest('tr').attr('data-access') == false || $(this).closest('tr').attr('data-access') == 'false')) {
				$('#buy_subscribe_popup').modal('show');
				$(this).prop('checked', false);
			} else {
				curr_element_tag = 'input_text';

				if (this.value == '' || this.value == 0 || this.value == '0') {
					this.value = 1;
				}

				summCheck();
				//console.log('summCheck');
				if (asd > 0) {
					summCheck();
				}
			}
		});

		$('.numberof').bind("keyup", function(eventObject) {
			//запрет на ввод всего кроме цифр
			if (this.value.match(/[^0-9]/g)) {
				this.value = this.value.replace(/[^0-9]/g, '');
			}

			if ($(this).closest('tr').attr('data-access') && ($(this).closest('tr').attr('data-access') == false || $(this).closest('tr').attr('data-access') == 'false')) {
				$('#buy_subscribe_popup').modal('show');
				$(this).prop('checked', false);
			}
		});
		//-------

		//ежемесячное пополнение счета
		$('#refill_value').bind("change keyup input", function(eventObject) {
			//запрет на ввод всего кроме цифр
			if ((eventObject.which != 37) && (eventObject.which != 39)) {
				if (this.value.match(/[^0-9]/g)) {
					this.value = this.value.replace(/[^0-9]/g, '');
				}

				refill = $(this).val();
				$(this).val(refill + ' руб.');
			}

			//постановка курсора перед постфиксом текстового поля
			function setSelectionRange(input, selectionStart, selectionEnd) {
				if (input.setSelectionRange) {
					input.setSelectionRange(selectionStart, selectionEnd);
				} else if (input.createTextRange) {
					var range = input.createTextRange();
					range.collapse(true);
					range.moveEnd('character', selectionEnd);
					range.moveStart('character', selectionStart);
					range.select();
				}
			}
			function setCaretToPos(input, pos) {
				setSelectionRange(input, pos, pos);
			}
			setCaretToPos($('#refill_value')[0], refill.length);
			//-------

			if (refill == '') {
				refill = 0;
			} else {
				refill = parseInt(refill);
			}
			$.cookie('refill_cookie', refill, {expires: 1, path: '/'});
		});
		$('#replenishment').change(function() {
			if ($('#replenishment').prop('checked')) {
				$('#popup_refill').modal('show');
			} else {
				$('#refill_value').val('');
				refill = 0;

				curr_element_tag = 'input_repl';
				/* curr_ths = $(this).closest('tr').find('.checkbox input');
                curr_tr = curr_ths.closest('tr'); */
				$.cookie('refill_cookie', '', {expires: 1, path: '/'});

				summCheck();
				if (asd > 0) {
					summCheck();
				}
			}
		});
		$('#iis').change(function() {
			summCheck();
		});
		$('.popup_refill_submit').click(function() {
			curr_element_tag = 'input_repl';

			summCheck();
			if (asd > 0) {
				summCheck();
			}

			//console.log('Размер ежемесячного пополнения счёта', refill);
		});
		$('#popup_refill .close').click(function() {
			$('#replenishment').prop('checked', false);
			$('#popup_refill').modal('hide');

			summCheck();
			if (asd > 0) {
				summCheck();
			}
		});
		//-------

		//проверка на превышение суммы инвестиций
		function summCheck() {
			//перемещение строк в таблице
			if (curr_element_tag == 'input_checkbox') {
				if (curr_ths.closest('tbody').length != 0) {
					if (curr_ths.prop('checked')) {
						curr_tr.css('opacity', 0);
						curr_tr.addClass('checked');

						setTimeout(function() {
							curr_tr.closest('tbody ').prepend(curr_tr);
						}, 300);
						setTimeout(function() {
							curr_tr.removeAttr('style');
						}, 310);
					} else {
						curr_tr.removeClass('checked');
						curr_tr.css('opacity', 0);

						setTimeout(function() {
							for (var i = 0; i < curr_tr.closest('tbody tr.checked').length; i ++) {
								curr_tr.closest('tbody tr.checked').eq(i).after(curr_tr);
							}
						}, 300);
						setTimeout(function() {
							curr_tr.removeAttr('style');
						}, 310);
					}
				}
			}
			//-------

			//обнуление переменных и массивов
			money_in_work = 0;
			dates_arr = [];
			donut_arr = [];
			donut_arr_text = [];
			donut_arr_values = [];
			donut_arr_colors = [];
			curr_donut_arr = [];
			profit_year_arr = [];
			profit_year_up_arr = [];
			profit_year_down_arr = [];
			//массив из id строк, нужен для cookie
			var id_arr_debstok = [];
			var id_arr_shares = [];
			var tr_id, tr_number;

			var table_elements = [];
			main_arr_curr = [];
			//-------

			//массив дат гашения облигаций - занесение сегодняшней даты
			date_today = new Date();
			date_today_day;
			date_today_month;
			date_today_year;

			if (dates_arr[0] != date_today) {
				date_today_day = date_today.getDate();
				date_today_day = date_today_day.toString();
				if (date_today_day.length < 2) {
					date_today_day = '0' + date_today_day;
				}

				date_today_month = date_today.getMonth() + 1;
				date_today_month = date_today_month.toString();
				if (date_today_month.length < 2) {
					date_today_month = '0' + date_today_month;
				}

				date_today_year = date_today.getFullYear();

				dates_arr.push(date_today_year + '-' + date_today_month + '-' + date_today_day);
			}
			//console.log('Маccив дат гашения облигаций: ', dates_arr);
			//-------

			$('.calculate_table tbody .checkbox input').each(function(index) {
				//массив из данных для линейного графика
				money_in_work_arr = [];
				main_arr_values = [];
				main_arr_shares_up_values = [];
				main_arr_shares_down_values = [];
				refill_arr_values = [];
				curr_donut_arr = [];

				table_elem = [];
				obligations_arr = [];
				main_arr = [];

				ths = $(this);
				tr = ths.closest('tr');

				if (ths.prop('checked')) {
					tr_id = tr.attr('data-id');
					tr_number = parseInt(tr.find('.numberof').val());
					if (ths.closest('.calculate_table').hasClass(tableType)) {
						id_arr_shares.push(tr_id + '...' + tr_number);
						$.cookie(coockieName, JSON.stringify(id_arr_shares), {expires: 1, path: '/'});
					}

					//подсчет распределенных и нераспределенных средств
					if (ths.prop('checked')) {
						if (ths.closest('tbody').length != 0) {
							if (tr.attr('data-elem_buy_price') != '-') {
								elem_buy_price_one = parseFloat(tr.attr('data-elem_buy_price'));
							} else {
								elem_buy_price_one = 0;
							}
							elem_sell_price_one = parseFloat(tr.find('.elem_sell_price').attr('data-price'));
							if (tr.attr('data-currency') == 'rub') {
								currency_value = 1;
							} else if (tr.attr('data-currency') == 'usd') {
								currency_value = parseFloat($('#usd_currency').val());
							} else if (tr.attr('data-currency') == 'eur') {
								currency_value = parseFloat($('#eur_currency').val());
							}
						}
					}
					//-------

					numberof = parseInt(tr.find('.numberof').val());

					elem_buy_price_one *= currency_value;
					elem_buy_price = elem_buy_price_one * numberof;

					elem_sell_price_one *= currency_value;
					elem_sell_price = elem_sell_price_one * numberof;


					//добавление текущей даты гашения в массив дат гашения
					date_off = tr.find('.date_off').attr('data-date');
					date_off = date_off.substr(0, date_off.length-2)+'01';
					date_off = new Date(date_off);

					var date_off_day = date_off.getDate();
					date_off_day = date_off_day.toString();
					if (date_off_day.length < 2) {
						date_off_day = '0' + date_off_day;
					}
					if (date_off_day != '01') {
						date_off_day = '01';
					}

					var date_off_month = date_off.getMonth() + 1;
					date_off_month = date_off_month.toString();
					if (date_off_month.length < 2) {
						date_off_month = '0' + date_off_month;
					}

					var date_off_year = date_off.getFullYear();

					var date_off_text = date_off_year + '-' + date_off_month + '-' + date_off_day;

					dates_arr.push(date_off_text);

					ths.attr('data-canonical', elem_buy_price);

					//console.log('Маccив дат гашения облигаций: ', dates_arr);

					//нахождение максимальной даты в массиве дат
					var dates_native_arr = [];

					for (var i = 0; i < dates_arr.length; i++) {
						dates_native_arr.push(new Date(dates_arr[i]));
					}

					date_max = new Date(Math.max.apply(null, dates_native_arr));
					dates_native_arr = [];
					//console.log('Максимальная дата: ', date_max);
					//-------

					//подсчет количества месяцев в промежутке между сегодня и максимальной датой, между сегодня и датой гашения
					function dateDiff() {
						date1 = date_today;
						date2 = date_max;

						var months = date2.getMonth() - date1.getMonth();
						if ( months < 0 ) {
							months += 12;
							date2.setFullYear( date2.getFullYear() - 1 );
						}

						var years = date2.getFullYear() - date1.getFullYear();

						if (years < 1) {
							months_final = months;
						} else {
							months_final = months + (years * 12);
						}

						return months_final;
					}
					var dateDiff2 = function() {
						var date1 = date_today;
						var date2 = date_off;
						var timeDiff = Math.abs(date2.getTime() - date1.getTime());
						date_off_today = Math.ceil(timeDiff / (1000 * 3600 * 24));
					}
					dateDiff2();

					months_final = dateDiff();
					if (months_final < 24) {
						months_final = 24;
					}
					//console.log('Количество месяцев: ', months_final)
					///-------
					//формирование второго массива дат с периодом в 1 месяц
					var curr_period_date = new Date();

					var l = 0;
					dates_period_arr = [];
					if (dates_period_arr[0] != date_today) {
						var date_today_day = date_today.getDate();
						date_today_day = date_today_day.toString();
						if (date_today_day.length < 2) {
							date_today_day = '0' + date_today_day;
						}

						var date_today_month = date_today.getMonth() + 1;
						date_today_month = date_today_month.toString();
						if (date_today_month.length < 2) {
							date_today_month = '0' + date_today_month;
						}

						var date_today_year = date_today.getFullYear();

						dates_period_arr.push(date_today_year + '-' + date_today_month + '-' + date_today_day);
					}

					for (var k = 0; k < months_final; k++) {
						curr_period_date.setMonth(curr_period_date.getMonth() + 1);

						var date_curr_period_day = curr_period_date.getDate();
						date_curr_period_day = date_curr_period_day.toString();
						if (date_curr_period_day.length < 2) {
							date_curr_period_day = '0' + date_curr_period_day;
						}

						var date_curr_period_month = curr_period_date.getMonth() + 1;
						date_curr_period_month = date_curr_period_month.toString();
						if (date_curr_period_month.length < 2) {
							date_curr_period_month = '0' + date_curr_period_month;
						}

						var date_curr_period_year = curr_period_date.getFullYear();

						var date_curr_period_final = date_curr_period_year + '-' + date_curr_period_month + '-' + '01';

						if (date_curr_period_final != dates_period_arr[k]) {
							l++;
						}
						if (l == dates_period_arr.length) {
							dates_period_arr.push(date_curr_period_final);
						}
					}

					var dates_period_arr_len = dates_period_arr.length;
					if (dates_period_arr_len > 36) {
						for (var k = 0; k < dates_period_arr_len; k ++) {
							if (k > 36) {
								dates_period_arr.splice(k, dates_period_arr_len - 1);
							}
						}
					}

					//console.log('Маccив дат периодом в месяц: ', dates_period_arr);
					//-------

					var money_in_work_old = money_in_work;
					money_in_work += elem_buy_price;

					//console.log('!!!!!!!!!!!!!!!!!!!!', money_main);
					if (money_main < money_in_work) {
						$('#popup_investment_amount .popup_calculate_origin span').html('<span class="strong">' + money_in_work.toLocaleString() + '</span> руб.');

						if (money_main > 0) {
							$('#popup_investment_amount').removeClass('origin');
							$('#popup_money_error').modal('show');
						} else {
							$('#popup_investment_amount').addClass('origin');
							$('#popup_investment_amount').modal('show');
						}

						curr_ths.closest('table').addClass('table_error');

						if (curr_element_tag == 'input_checkbox' || curr_element_tag == 'input_checkbox_all') {
							money_in_work -= elem_buy_price;

							curr_ths.closest('tr').addClass('error');
							curr_ths.prop('checked', false);

							curr_tr.removeClass('checked');
							curr_tr.css('opacity', 0);

							setTimeout(function() {
								var count_checked = curr_tr.closest('tbody').find('tr.checked').length;
								for (var i = 0; i < count_checked; i ++) {
									curr_tr.closest('tbody').find('tr.checked').eq(i).after(curr_tr);
								}
							}, 300);
							setTimeout(function() {
								curr_tr.removeAttr('style');
							}, 310);

						} else if (curr_element_tag == 'input_text') {
							money_in_work = money_in_work_old;
						}
					} else {
						var money_in_work_text = money_in_work.toFixed(2);

						$('#money_in_work').val(money_in_work_text);
						priceTextChange();
						//-------

						curr_ths.closest('tr').removeClass('error');
					}

					//----------------------------
					//----------------------------
					//----------------------------

					if (!tr.hasClass('error')) {
						table_elem_name = tr.find('.elem_name').text();

						element_type = '';
						if ($(this).closest('.calculate_table').hasClass('debstock_table')) {
							element_type = 'debstock';
						} else if ($(this).closest('.calculate_table').hasClass(tableType)) {
							element_type = 'share';
						}

						profit_main = 0;
						profit_year = 0;

						if ($(this).closest('.calculate_table').hasClass('debstock_table')) {
							profit_main = parseFloat(tr.find('.profit_main').attr('data-profit_main'));
							profit_year = parseFloat(tr.find('.profit_year').attr('data-profit_year'));
						}

						var profit_main_up = 0,
							profit_main_down = 0,
							profit_year_up = 0,
							profit_year_down = 0;

						if ($(this).closest('.calculate_table').hasClass(tableType)) {
							profit_year_up = parseFloat(tr.find('.cshare_up_p').attr('data-value'));
							profit_year_down = parseFloat('-' + tr.find('.cshare_down_p').attr('data-value'));
							profit_main_up = profit_year_up;
							profit_main_down = profit_year_down;
						}

						table_elem.push(table_elem_name, element_type, elem_buy_price, elem_sell_price, '|', 0, profit_main, profit_year, '|', 0, 0, profit_main_up, profit_main_down, profit_year_up, profit_year_down, '|', date_off_today, date_off);

						table_elements.push(table_elem);
					}

				}
			});
			//console.log('Массив выбранных элементов:', table_elements)

			profit_year_average = 0;
			profit_year_up_average = 0;
			profit_year_down_average = 0;

			for (var i = 0; i < profit_year_arr.length; i++) {
				profit_year_average += profit_year_arr[i];
			}
			profit_year_average = profit_year_average/profit_year_arr.length;

			for (var i = 0; i < profit_year_up_arr.length; i++) {
				profit_year_up_average += profit_year_up_arr[i];
			}
			profit_year_up_average = profit_year_up_average/profit_year_up_arr.length;

			for (var i = 0; i < profit_year_down_arr.length; i++) {
				profit_year_down_average += profit_year_down_arr[i];
			}
			profit_year_down_average = profit_year_down_average/profit_year_down_arr.length;

			if ($('.calculate_table tbody .checkbox input:checked').length == 0) {
				$('#money_in_work').val(0);
				priceTextChange();
			}

			var free_val_summ = 0;
			for (var k = 0; k < donut_arr.length; k++) {
				free_val_summ += donut_arr[k][1];
			}
			free_val_summ = 100 - free_val_summ;
			free_val_arr = ['Не распределено', free_val_summ, '#a6db16'];
			donut_arr.push(free_val_arr);

			for (var i = 0; i < donut_arr.length; i++) {
				donut_arr_text.push(donut_arr[i][0])
				donut_arr_values.push(donut_arr[i][1].toFixed(1))
				donut_arr_colors.push(donut_arr[i][2])
			}
			//console.log('Массив данных для кругового графика: ', donut_arr);

			if (ths.closest('table').hasClass('table_error')) {
				ths.closest('table .numberof').each(function() {
					$(this).val(parseInt($(this).attr('data-working_value')));
				});

				ths.closest('table').removeClass('table_error');

				asd = 1;
			} else {
				$('.calculate_table .numberof').each(function() {
					$(this).attr('data-working_value', $(this).val());
				});

				asd = 0;
			}

			//отрисовка выбранных облигаций
			for (var i = 0; i < dates_period_arr.length; i++) {
				var date_period_current = new Date(dates_period_arr[i]);
				var date_period_before = new Date(dates_period_arr[i - 1]);

				var dateDiff3 = function() {
					var date1 = date_period_current;
					var date2 = date_period_before;
					var timeDiff = Math.abs(date2.getTime() - date1.getTime());
					date_off_today = Math.ceil(timeDiff / (1000 * 3600 * 24));

					return date_off_today;
				}

				var date_today_before = dateDiff3();

				if (i == 0) {
					main_arr_curr.push(dates_period_arr[i]);

					main_arr_curr.push(table_elements);

					var month_summ = 0;

					for (var j = 0; j < table_elements.length; j++) {
						month_summ += table_elements[j][2];
					}

					main_arr_curr.push('График, общий первоначальный капитал:');
					main_arr_curr.push(month_summ);

					main_arr_curr.push('График, финальный доход от облигаций:');
					if ($('.debstock_table tbody .checkbox input:checked').length != 0) {
						main_arr_curr.push(month_summ);
					} else {
						main_arr_curr.push(0);
					}

					main_arr_curr.push('График, финальный доход от акций:');
					if ($('.' + tableType + ' tbody .checkbox input:checked').length != 0) {
						main_arr_curr.push(month_summ);
					} else {
						main_arr_curr.push(0);
					}

					main_arr_curr.push('График, финальный просад акций:');
					if ($('.' + tableType + ' tbody .checkbox input:checked').length != 0) {
						main_arr_curr.push(month_summ);
					} else {
						main_arr_curr.push(0);
					}

					//подсчет пополнения для графика
					var curr_refill_final = 0 + month_summ;

					//ИИС
					var iis = 0;
					iis_curr_date = new Date(main_arr_curr[0]);
					var iis_curr_date_month = iis_curr_date.getMonth() + 1;
					var iis_curr_date_year = iis_curr_date.getFullYear();
					var iis_curr = 0;
					if ($('#iis').prop('checked')){
						if (iis_curr_date_month == 12 && iis_curr_date_year == date_today_year) {
							iis_curr = curr_refill_final;
						}
						if (iis_curr_date_month == 3 && iis_curr_date_year == date_today_year + 1) {
							iis = iis_curr * 0.13;
							if (iis > 52000) {
								iis = 52000;
							}
						}

						$.cookie('iis_cookie', 1, {expires: 1, path: '/'});
					} else {
						$.cookie('iis_cookie', 0, {expires: 1, path: '/'});
					}

					main_arr_curr.push('Доход от облигаций:');
					main_arr_curr.push(0);

					main_arr_curr.push('Доход от акций:');
					main_arr_curr.push(0);

					main_arr_curr.push('Просад акций:');
					main_arr_curr.push(0);

					main_arr_curr.push('Пополнение:');
					main_arr_curr.push(0);

					main_arr_curr.push('График, пополнение:');
					main_arr_curr.push(month_summ);

					main_arr_curr.push('Доход от пополнения:');
					main_arr_curr.push(0);

					main_arr_curr.push('ИИС:');
					main_arr_curr.push(iis);
					main_arr_curr.push(iis_curr);

					main_arr.push(main_arr_curr);
				} else {
					if (table_elements.length > 0) {
						var second_main_arr = [];

						second_main_arr.push(dates_period_arr[i]);

						//получаем массив данных за предыдущий месяц
						var Arr1 = main_arr[i - 1][1];
						var Arr2 = JSON.parse(JSON.stringify(Arr1));

						//перебираем выбранные сроки таблицы из предыдущего месяца
						for (var j = 0; j < Arr1.length; j++) {
							var curr_date_off = new Date(Arr1[j][17]);

							var before_profit_debstock = Arr1[j][5],
								before_profit_share_up = Arr1[j][9],
								before_profit_share_down = Arr1[j][10];

							//считаем доходность каждой облигации
							var curr_profit_debstock = 0;
							var curr_profit_share_up = 0;
							var curr_profit_share_down = 0;
							if (date_period_current <= curr_date_off) {
								//количество дней между текущим месяцем и предыдущим
								var second_date_curr = new Date(dates_period_arr[i]);
								var second_date_prev = new Date(dates_period_arr[i - 1]);
								var month_timeDiff = Math.abs(second_date_curr.getTime() - second_date_prev.getTime());
								var month_days_diff = Math.ceil((month_timeDiff) / (1000 * 3600 * 24));

								//чистый доход облигации
								var clear_profit_debstock = 0;
								if (Arr1[j][1] == 'debstock') {
									clear_profit_debstock = Arr1[j][3] - Arr1[j][2];

									curr_profit_debstock = ((clear_profit_debstock / Arr1[j][16]) * month_days_diff) + Arr1[j][5];

									Arr2[j][5] = curr_profit_debstock;
								}

								//чистый доход и просад акции
								var clear_profit_share_up = 0;
								var clear_profit_share_down = 0;
								if (Arr1[j][1] == 'share') {
									clear_profit_share_up = (Arr1[j][2] * Arr1[j][11]) / 100;

									curr_profit_share_up = ((clear_profit_share_up / Arr1[j][16]) * month_days_diff) + Arr1[j][9];

									Arr2[j][9] = curr_profit_share_up;

									//--------------------------------

									clear_profit_share_down = (Arr1[j][2] * Arr1[j][12]) / 100;

									curr_profit_share_down = ((clear_profit_share_down / Arr1[j][16]) * month_days_diff) + Arr1[j][10];

									Arr2[j][10] = curr_profit_share_down;
								}
							} else {
								Arr2[j][5] = before_profit_debstock;
								Arr2[j][9] = before_profit_share_up;
								Arr2[j][10] = before_profit_share_down;
							}
						}

						second_main_arr.push(Arr2);

						second_main_arr.push('График, общий первоначальный капитал:');
						second_main_arr.push(month_summ);

						//дней в текущем месяце
						var second_curr_date = new Date(dates_period_arr[i]);
						var second_prev_date = new Date(dates_period_arr[i - 1]);
						var second_month_diff = Math.abs(second_curr_date.getTime() - second_prev_date.getTime());
						var second_month = Math.ceil(second_month_diff / (1000 * 3600 * 24));

						//подсчет пополнения
						var curr_refill = refill + main_arr[i - 1][17];
						//подсчет пополнения для графика
						var curr_refill_final = curr_refill + month_summ;
						//доход пополнения
						var profit_average = 0;
						if ($('.debstock_table tbody .checkbox input:checked').length != 0) {
							var profit_average_curr = 0;
							var profit_average_summ = 0;
							for (var j = 0; j < Arr2.length; j ++) {
								if (Arr2[j][1] == 'debstock') {
									profit_average_curr += Arr2[j][2] * Arr2[j][7];
									profit_average_summ += Arr2[j][2];
								}
							}
							profit_average_curr /= profit_average_summ;
							profit_average = profit_average_curr;
						}
						var curr_refill_profit = ((((curr_refill * profit_average) / 365 ) * second_month) / 100) + main_arr[i - 1][21];

						//ИИС
						var iis = main_arr[i - 1][23];
						iis_curr_date = new Date(second_main_arr[0]);
						var iis_curr_date_month = iis_curr_date.getMonth() + 1;
						var iis_curr_date_year = iis_curr_date.getFullYear();
						var iis_curr = 0;
						if ($('#iis').prop('checked')){
							var iis_curr = main_arr[i - 1][24];
							if (iis_curr_date_month == 12 && iis_curr_date_year == date_today_year) {
								iis_curr = curr_refill_final;
							}
							if (iis_curr_date_month == 3 && iis_curr_date_year == date_today_year + 1) {
								iis = iis_curr * 0.13;
								if (iis > 52000) {
									iis = 52000;
								}
							}
						}

						//подсчет общей доходности облигаций и акций для графика
						var profit_summ_debstock = 0,
							profit_summ_share_up = 0,
							profit_summ_share_down = 0;

						for (var j = 0; j < Arr2.length; j ++) {
							profit_summ_debstock += Arr2[j][5];
							profit_summ_share_up += Arr2[j][9];
							profit_summ_share_down += Arr2[j][10];
						}

						second_main_arr.push('График, финальный доход от облигаций:');
						var second_month_summ_debstock = profit_summ_debstock + month_summ + curr_refill + curr_refill_profit + iis;
						if ($('.debstock_table tbody .checkbox input:checked').length != 0) {
							second_main_arr.push(second_month_summ_debstock);
						} else {
							second_main_arr.push(0);
						}

						second_main_arr.push('График, финальный доход от акций:');
						var second_month_summ_share_up = profit_summ_share_up + month_summ + curr_refill + curr_refill_profit + profit_summ_debstock + iis;
						if ($('.' + tableType+ ' tbody .checkbox input:checked').length != 0) {
							//console.log(second_month_summ_share_up)
							if (second_month_summ_share_up !== 'NaN' || second_month_summ_share_up !== NaN || second_month_summ_share_up != '' || isNaN(second_month_summ_share_up) == true) {
								second_main_arr.push(second_month_summ_share_up);
							} else {
								second_main_arr.push(0);
							}
						} else {
							second_main_arr.push(0);
						}

						second_main_arr.push('График, финальный просад акций:');
						var second_month_summ_share_down = profit_summ_share_down + month_summ + curr_refill + curr_refill_profit + profit_summ_debstock + iis;
						if ($('.' + tableType+ ' tbody .checkbox input:checked').length != 0) {
							second_main_arr.push(second_month_summ_share_down);
						} else {
							second_main_arr.push(0);
						}

						second_main_arr.push('Доход от облигаций:');
						second_main_arr.push(profit_summ_debstock);

						second_main_arr.push('Доход от акций:');
						second_main_arr.push(profit_summ_share_up);

						second_main_arr.push('Просад акций:');
						second_main_arr.push(profit_summ_share_down);

						second_main_arr.push('Пополнение:');
						second_main_arr.push(curr_refill);

						second_main_arr.push('График, пополнение:');
						second_main_arr.push(curr_refill_final);

						second_main_arr.push('Доход от пополнения:');
						second_main_arr.push(curr_refill_profit);

						second_main_arr.push('ИИС:');
						second_main_arr.push(iis);
						second_main_arr.push(iis_curr);

						main_arr.push(second_main_arr);
					}
				}
			}
			//console.log('Финальный массив данных: ', main_arr);
			//console.log('массив id акций, новый: ' + id_arr_shares);

			//console.log('----------------');
			//-------


			donut_money_in_work = (money_in_work * 100) / money_main;
			donut_money_in_work = parseInt(donut_money_in_work);
			donut_money_free = 100 - donut_money_in_work;
			donut_money_free = parseInt(donut_money_free);


			//вывод дохода облигаций
			var debstock_0 = 0,
				debstock_1 = 0,
				debstock_2 = 0,
				share_up = 0,
				share_down = 0;
			if (main_arr.length != 0) {
				var i = main_arr.length - 1;
				debstock_0 = main_arr[i][11];
				debstock_1 = main_arr[i][21];
				debstock_2 = main_arr[i][23];
				share_up = main_arr[i][13];
				share_down = main_arr[i][15];
			}
			all_income = debstock_0 + debstock_1 + debstock_2;
			all_income_max = all_income + share_up;
			all_income_min = all_income + share_down;
			allIncomeChange();
		}
		//-------
		//-------


		var inputChange;
		//резиновый input
		if ($('input[type="text"], input[type="password"]').hasClass('rubber')) {
			$('input.rubber').each(function() {
				if ($(this).parent('.rubber_outer').length == 0) {
					$(this).wrap('<span class="rubber_outer"></span>');
				}
				if ($(this).siblings('.rubber_block').length == 0) {
					$(this).closest('.rubber_outer').append('<span class="rubber_block"></span>')
				}
				if ($(this).siblings('.postfix').length == 0) {
					$(this).closest('.rubber_outer').append('<span class="postfix">' + $(this).attr('data-postfix') + '</span>')
				}
				if ($(this).siblings('.prefix').length == 0) {
					$(this).closest('.rubber_outer').prepend('<span class="prefix">' + $(this).attr('data-prefix') + '</span>')
				}

				var input = $(this),
					block = $(this).siblings('.rubber_block');

				inputChange = function() {
					block.text(input.val());
					input.width(block.width());

					//----------
					if (input.closest('.time_slider_outer').length != 0) {
						var rubber_width = input.closest('.rubber_outer').outerWidth();
						input.closest('.time_slider_outer').css('padding-right', rubber_width + 15);
					} else
					if (input.closest('.rate_slider_outer').length != 0) {
						var rubber_width = input.closest('.rubber_outer').outerWidth();
						input.closest('.rate_slider_outer').css('padding-right', rubber_width + 15);
					}
					if (input.closest('.coefficient_slider_outer').length != 0) {
						var rubber_width = input.closest('.rubber_outer').outerWidth();
						input.closest('.coefficient_slider_outer').css('padding-right', rubber_width + 15);
					}
					if (input.closest('.profitability_slider_outer').length != 0) {
						var rubber_width = input.closest('.rubber_outer').outerWidth();
						input.closest('.profitability_slider_outer').css('padding-right', rubber_width + 15);
					}
					//----------
				}
				inputChange();
				input.on('change input', inputChange);
			});
		}

		$('.calculate_tabs a').click(function() {
			caltulateTabs($(this));
		});

		$('.cshares_turnover').each(function() {
			var elem_turn = $(this).attr('data-turn').toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			$(this).text(elem_turn);
		});
		//-------
	}
}

//проверка на введенную сумму инвестирования в попапе
function investSummCheck(value) {
	var obl_number = 0;
	if (value <= 100000) {
		obl_number = 1;
	} else if (value > 100000 && value <= 200000) {
		obl_number = 2;
	} else if (value > 200000 && value <= 300000) {
		obl_number = 3;
	} else if (value > 300000 && value <= 400000) {
		obl_number = 4;
	} else if (value > 400000 && value <= 500000) {
		obl_number = 5;
	} else if (value > 500000 && value <= 600000) {
		obl_number = 6;
	} else if (value > 600000 && value <= 700000) {
		obl_number = 7;
	} else if (value > 700000 && value <= 800000) {
		obl_number = 8;
	} else if (value > 800000 && value <= 900000) {
		obl_number = 9;
	} else if (value > 900000 && value <= 1000000) {
		obl_number = 10;
	} else if (value > 1000000 && value <= 5000000) {
		obl_number = 50;
	} else {
		obl_number = 100;
	}

	$.cookie('obligations_number_cookie', obl_number, {expires: 1, path: '/'});

	return obl_number;
}

$('.market_map_table tbody .checkbox input').on('change', function() {
	var ths = $(this);
	var tr = ths.closest('tr');

	if (ths.prop('checked')) {
		tr.addClass('checked');
		rowToTop(tr);
	} else {
		tr.removeClass('checked');
		rowToDown(tr);
	}

	mm_main_arr = [];
	var id_arr_shares = [];
	var id_arr_shares_usa = [];
	// marketMapInit(mm_main_arr);

	$('.market_map_table tbody .checkbox input').each(function() {
		var ths = $(this);
		var tr = ths.closest('tr');
		var tr_id;
		tr_number = parseInt(tr.find('.numberof').val());


		if (ths.prop('checked')) {
			tr_id = tr.attr('data-id');
			/*			if (ths.closest('.market_map_table').hasClass('shares_table')) {
                            id_arr_shares.push(tr_id);
                            $.cookie('id_arr_shares_cookie', JSON.stringify(id_arr_shares), {expires: 1, path: '/'});
                        }*/

			if (ths.closest('.market_map_table').hasClass(tableType)) {
				if(tableType == 'shares_table') {
					id_arr_shares.push(tr_id + '...' + tr_number);
					$.cookie(coockieName, JSON.stringify(id_arr_shares), {expires: 1, path: '/'});
				} else {
					id_arr_shares_usa.push(tr_id + '...' + tr_number);
					$.cookie(coockieName, JSON.stringify(id_arr_shares_usa), {expires: 1, path: '/'});
				}
			}

		}
	});
});


function rowToTop(tr) {
	tr.css('opacity', 0);
	setTimeout(function() {
		tr.closest('tbody').prepend(tr);
	}, 300);
	setTimeout(function() {
		tr.css('opacity', 1);
	}, 310);
}
function rowToDown(tr) {
	var len = tr.siblings('tr.checked').length;

	tr.css('opacity', 0);
	setTimeout(function() {
		tr.siblings('tr.checked').eq(len - 1).after(tr);
	}, 300);
	setTimeout(function() {
		tr.css('opacity', 1);
	}, 310);
}


$(window).resize(function() {
	//marketMapInit(market_selector)
});





function marketMapInitFromAllFilters(){
	market_selector = $('#market_map_sector option:selected').data('code');
	//marketMapInit(market_selector);
}

function getDataMarketMaps() {
	var result = null;
	var data = {
		exchange: $('#exchange').val(), //Фильтр по отрасли
		f_industry: $('#market_map_sector').val(), //Фильтр по отрасли
		f_market: $('#f_market').val(), //Фильтр по рынку (переключает инфобок акций)
		f_period: $('#market_map_period').val(), //Фильтр по периоду
		f_period_current: $('#f_period_current').val(), //Фильтр по периоду
		f_preset: $('#market_map_preset').val(), //пресет
		s_field: $('#table_sort_field').val(), //поле сортировки
		s_order: $('#table_sort_order').val(), //направление сортировки
	}

	$.ajax({
		url:"/lk/market_map/getData.php",
		type:"POST",
		dataType:"json",
		data:data,
		async: false,
		success:function(data){
			result = data;
		}
	});
	// console.log(result);
	return result;
}

function getDataActionsTable() {
	var result = null;
	var data = {
		exchange: $('#exchange').val(), //Фильтр по отрасли
		f_industry: $('#market_map_sector').val(), //Фильтр по отрасли
		f_market: $('#f_market').val(), //Фильтр по рынку (переключает инфобок акций)
		f_period: $('#market_map_period').val(), //Фильтр по периоду
		f_period_current: $('#f_period_current').val(), //Фильтр по периоду
		f_preset: $('#market_map_preset').val(), //пресет
		s_field: $('#table_sort_field').val(), //поле сортировки
		s_order: $('#table_sort_order').val(), //направление сортировки
		page:1
	}
	let tableType
	if($('.shares_table').length){
		tableType = 'shares_table';
	}
	if($('.shares_usa_table').length){
		tableType = 'shares_usa_table';
	}
	$.ajax({
		url:"/lk/market_map/getActionsTable.php",
		type:"POST",
		data:data,
		dataType:"html",
		async: false,
		success:function(data){
			$('.calculate_table').html(data);
			$('.calculate_table.' + tableType).css('display','block');
			updateTablesScrollbars();
			calculateTableActiveName();
			calculateTableCompanyName();
		}
	});

	return result;
}

function updateMMFilter() {
	var result = null;
	const exchange = $('#exchange').val();
	var data = {
		exchange: $('#exchange').val(), //Фильтр по отрасли
		f_industry: $('#market_map_sector').val(), //Фильтр по отрасли
		f_market: $('#f_market').val(), //Фильтр по рынку (переключает инфобок акций)
		f_period: $('#market_map_period').val(), //Фильтр по периоду
		f_period_current: $('#f_period_current').val(), //Фильтр по периоду
		f_preset: $('#market_map_preset').val(), //пресет
		s_field: $('#table_sort_field').val(), //поле сортировки
		s_order: $('#table_sort_order').val(), //направление сортировки
		page:1,
		ajax:'y'
	}

	if(data.exchange=='spbx'){
		$('.actionsMM').removeClass('shares_table');
		$('.actionsMM').addClass('shares_usa_table');
	} else {
		$('.actionsMM').removeClass('shares_usa_table');
		$('.actionsMM').addClass('shares_table');
	}

	$.ajax({
		url:"/lk/market_map/filter.php",
		type:"POST",
		data:data,
		dataType:"html",
		async: false,
		success:function(data){
			if(data){
				$('.relative .calculate_form').detach();
				$('.relative').prepend(data);
				initSelects();
			}
		}
	});
	//overlayHide();
	return result;
}

function add_table_row_actions(arr_data, pagenum){
	var table = $('#market_map_table');
	var page_elements_count = 10;
	//for()


}

//акции
$(".calculate_table.actionsMM").on("click", ".more_results button", function(){
	//updateActionsMarketMapTable("page="+$(this).data("next"));
	updateActionsMarketMapTable($(this).data("next"));
});



//function updateActionsMarketMapTable(dopParams = false){
function updateActionsMarketMapTable(page){
	overlayShow();
	var table = $(".calculate_table.actionsMM table");
	var params = [];


	var data = {
		exchange: $('#exchange').val(), //Фильтр по отрасли
		f_industry: $('#market_map_sector').val(), //Фильтр по отрасли
		f_market: $('#f_market').val(), //Фильтр по рынку (переключает инфобок акций)
		f_period: $('#market_map_period').val(), //Фильтр по периоду
		f_period_current: $('#f_period_current').val(), //Фильтр по периоду
		f_preset: $('#market_map_preset').val(), //пресет
		s_field: $('#table_sort_field').val(), //поле сортировки
		s_order: $('#table_sort_order').val(), //направление сортировки
		page:page
	}

	$.ajax({
		url:"/lk/market_map/getActionsTable.php",
		type:"POST",
		data:data,
		dataType:"html",
		async: false,
		success:function(data){
			/*				  $('.calculate_table').html(data);
                              $('.calculate_table.shares_table').css('display','block');*/
			$('#post_table').html(data);
			$(".calculate_step_title").html($(data).find(".calculate_step_title").html());
			console.log($(data).find(".smart_table"));
			//console.log($(data).find(".calculate_step_title").html());
			$(".market_map_table tbody").append($('#post_table').find(".smart_table tbody").html());
			$(".more-results-wrap").html($('#post_table').find(".more-results-wrap").html());
			$('#post_table').html('');

			calcMain();
			checkRadio();
			//calculateTableCompanyName();

			updateTablesScrollbars();
			calculateTableActiveName();
			calculateTableCompanyName();
			rebuildTooltips();
			overlayHide();
		}
	});



}

function marketMapInit(val) {
	//console.log(val)
	overlayShow();
	$('#market_map_chart').remove();
	$('.market_map_chart_outer').append('<div id="market_map_chart"></div>');

	// var arr = $('.market_map_chart_outer').attr('data-array');
	var arr_data = getDataMarketMaps();

	var arr_begin = [];
	var arr_axis = [];
	var arr_industry_axis = [];
	var arr_bubbles_sizes = 0;
	if(!arr_data['arr_begin'].length){
		arr_begin = [];
		arr_axis = [];
		overlayHide();
	} else {
		arr_begin = arr_data['arr_begin'];
		arr_bubbles_sizes = arr_data['arr_bubbles_sizes'];
		arr_axis = arr_data['arr_axis'];
		arr_industry_axis = arr_data['arr_industry_axis'];
		$('#table_sort_field').val(arr_axis['SORT_FIELD']);
		$('#table_sort_order').val(arr_axis['SORT_ORDER']);
		//console.log(arr_begin);
		//add_table_row_actions(arr_data, 1);
	}

	 console.log('arr1_length',arr_industry_axis);
	var arr = [];
	var axis_x_name = 'Прогноз';
	var axis_y_name = 'p/e';
	var max_x = 0, max_y = 0;
	if(arr_data['arr_begin'].length){
		var min_x = arr_begin[0][0], min_y = arr_begin[0][1];
	} else {
		var min_x = 0, min_y = 0;
	}

	if (val == 'all') {
		for (var i = 0; i < arr_begin.length; i++) {
			var curr_arr = arr_begin[i];

			curr_arr[5] = curr_arr[2];

			if (curr_arr[2] < arr_bubbles_sizes) {
				curr_arr[2] = arr_bubbles_sizes;
			}

			if (curr_arr[0] > max_x) {
				max_x = curr_arr[0];
			}
			if (curr_arr[0] < min_x) {
				min_x = curr_arr[0];
			}

			if (curr_arr[1] > max_y) {
				max_y = curr_arr[1];
			}
			if (curr_arr[1] < min_y) {
				min_y = curr_arr[1];
			}

			arr.push(curr_arr);
		}
	} else {
		for (var i = 0; i < arr_begin.length; i++) {
			var curr_arr = arr_begin[i];
			var curr_val = curr_arr[4];

			if (curr_val == val) {
				//console.log(curr_val)

				curr_arr[5] = curr_arr[2];

				if (curr_arr[2] < arr_bubbles_sizes) {
					curr_arr[2] = arr_bubbles_sizes;
				}

				if (curr_arr[0] > max_x) {
					max_x = curr_arr[0];
				}
				if (curr_arr[0] < min_x) {
					min_x = curr_arr[0];
				}

				if (curr_arr[1] > max_y) {
					max_y = curr_arr[1];
				}
				if (curr_arr[1] < min_y) {
					min_y = curr_arr[1];
				}

				arr.push(curr_arr);
			}
		}
	}


	//console.log(arr);
	if(arr.length==0 || !arr){
		$('#popup_empty_actions').modal('show');
		var cls = "inline_select select2-container--below inline_select_dropdown select2-container--focus";
		$("#market_map_sector option:selected").attr('disabled',true).trigger('change');
		$("#market_map_sector").val('all');
		$("#market_map_sector").trigger("change");

		$("#popup_empty_actions").on("hidden.bs.modal", function () {
			marketMapInit('all');
		});

		return true;
	}

	min_x -= 5;
	max_x += 5;
	min_y -= 10;
	max_y += 10;
	var axis_x_format = '%d';
	var axis_y_format = '%d';
	var format_x = format_y = '';
	//console.log(arr_axis);
	//console.log(arr_axis['NAME'].length);
	//Ограничиваем длину осей для варианта all и если заданы настройки осей в пресете
	if (val == 'all' && arr_axis['NAME'].length>0) {
		if(arr_axis['ALL_MAX_X']!='')
			max_x = Number(arr_axis['ALL_MAX_X']);
		if(arr_axis['ALL_MIN_X']!='')
			min_x = Number(arr_axis['ALL_MIN_X']);
		if(arr_axis['ALL_MAX_Y']!='')
			max_y = Number(arr_axis['ALL_MAX_Y']);
		if(arr_axis['ALL_MIN_Y']!='')
			min_y = Number(arr_axis['ALL_MIN_Y']);
		if(arr_axis['AXIS_X_NAME']!='')
			axis_x_name = arr_axis['AXIS_X_NAME'];
		if(arr_axis['AXIS_Y_NAME']!='')
			axis_y_name = arr_axis['AXIS_Y_NAME'];
	} else if (val !== 'all' && arr_axis['NAME'].length>0){

		if(arr_axis['AXIS_X_NAME']!='')
			axis_x_name = arr_axis['AXIS_X_NAME'];
		if(arr_axis['AXIS_Y_NAME']!='')
			axis_y_name = arr_axis['AXIS_Y_NAME'];

	        if(Object.keys(arr_industry_axis).length>0){
				 min_x = parseFloat(arr_industry_axis['MIN_X']);
				 max_x = parseFloat(arr_industry_axis['MAX_X']);
				 min_y = parseFloat(arr_industry_axis['MIN_Y']);
				 max_y = parseFloat(arr_industry_axis['MAX_Y']);
				 format_x = arr_industry_axis['FORMAT_X'];
				 format_y = arr_industry_axis['FORMAT_Y'];
			  }
	}



	if(format_x.length>0){
	  axis_x_format = format_x;
	}
	if(format_y.length>0){
	  axis_y_format = format_y;
	}
	if(arr.length>0)
		var plot1 = $.jqplot('market_map_chart',[arr],{

			height: 500,
			legend: {show:false},
			seriesDefaults:{
				renderer: $.jqplot.BubbleRenderer,
				rendererOptions: {
					bubbleAlpha: 0.6,
					highlightAlpha: 0.8,
					autoscalePointsFactor: -0.10,
					autoscaleMultiplier: 0.85,
					showLabels: false,
					autoscaleBubbles: true
				},
				shadow: false,
			},
			grid: {
				backgroundColor: '#fff',
				drawBorder: false,
				shadow: false
			},
			axes:{
				xaxis:{
					label: axis_x_name,
					labelOptions:{
						fontSize: '18px',
						textColor: '#000'
					},
					rendererOptions: { forceTickAt0: true, forceTickAt100: true },
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					pad: 1,
					min: min_x,
					max: max_x,
					tickOptions: {
						formatString: axis_x_format
					}
				},
				yaxis:{
					rendererOptions: { forceTickAt0: true, forceTickAt100: true },
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					labelOptions:{
						fontSize: '18px',
						textColor: '#000'
					},
					label: axis_y_name,
					pad: 1,
					min: min_y,
					max: max_y,
					tickOptions: {
						formatString: axis_y_format
					}

				}
			}
		});



	$('#market_map_chart').bind('jqplotDataHighlight', function (ev, seriesIndex, pointIndex, data, radius) {
		var chart_left = $('#market_map_chart').offset().left,
			chart_top = $('#market_map_chart').offset().top,
			x = plot1.axes.xaxis.u2p(data[0]),  // convert x axis unita to pixels on grid
			y = plot1.axes.yaxis.u2p(data[1]);  // convert y axis units to pixels on grid

		$('#market_map_tooltip').css({
			left: x + radius + 15,
			top: y + 10
		});
		var x_postfix = y_postfix = ext_postfix ='';
		if(arr_axis['AXIS_X_POSTFIX']!='') x_postfix = arr_axis['AXIS_X_POSTFIX'];

		$('#market_map_tooltip').html('<span class="market_map_tooltip_title">' +
			data[3] + '</span><br />' + arr_axis['AXIS_X_NAME'] + ': '+ data[0] + x_postfix + '<br />' + arr_axis['AXIS_Y_NAME'] + ': ' +
			data[1] + (arr_axis['AXIS_Y_POSTFIX']!=''?arr_axis['AXIS_Y_POSTFIX']:'') + '<br />' +  arr_axis['EXT_PARAM_NAME'] + ': ' + data[5] + (arr_axis['EXT_PARAM_POSTFIX']!=''?arr_axis['EXT_PARAM_POSTFIX']:''));

		$('#market_map_tooltip').show();
	});

	// Bind a function to the unhighlight event to clean up after highlighting.
	$('#market_map_chart').bind('jqplotDataUnhighlight', function (ev, seriesIndex, pointIndex, data) {
		$('#market_map_tooltip').empty();

		$('#market_map_tooltip').hide();
	});

	// $(window).bind('resize', function(event, ui) {
	//   plot1.replot( { resetAxes: true } );
	// });
	getDataActionsTable();

	$('.market_map_table thead .checkbox input').on('change', function() {
		$('.market_map_table tbody .checkbox input').prop('checked', true);
		$('.market_map_table tbody tr').addClass('checked');
	});

	$('#market_map_sector').unbind('select2:select');
	$('#market_map_sector').on('select2:select', function (e) {
		overlayShow();
		setTimeout(function() {
			var data = e.params.data;
			market_selector = data['element']['dataset']['code'];
			marketMapInit(market_selector);

			calculateTableActiveName();
			calculateTableCompanyName();
			overlayHide();
		},10);
	});

	$('#market_map_period, #market_map_preset').unbind('select2:select');
	$('#market_map_period, #market_map_preset').on('select2:select', function (e) {
		overlayShow();
		setTimeout(function() {
			var data = e.params.data;
			market_selector = $('#market_map_sector option:selected').data('code');
			marketMapInit(market_selector);

			calculateTableActiveName();
			calculateTableCompanyName();
			overlayHide();
		},10);

		//getDataActionsTable();

	});

	$('#exchange').off('select2:select');
	$('#exchange').on('select2:select', function (e) {
		overlayShow();
		setTimeout(function() {
			updateMMFilter();
			marketMapInit('all');

			calculateTableActiveName();
			calculateTableCompanyName();
			overlayHide();
		},10);
	});

	calcMain();
	calculateTableActiveName();
	calculateTableCompanyName();
	rebuildTooltips();
	overlayHide();
}
