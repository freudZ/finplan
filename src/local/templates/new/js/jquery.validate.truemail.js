$.validator.addMethod("truemail", function(value, element) {
	return this.optional(element) || /^(([A-Za-z0-9_\.\-])+)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/i.test(value);
}, "Некорректный Email");