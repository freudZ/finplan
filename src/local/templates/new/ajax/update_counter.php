<?
if (!defined('PUBLIC_AJAX_MODE')) {
    define('PUBLIC_AJAX_MODE', true);
}
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION, $USER;

CModule::IncludeModule("iblock");
$application = Application::getInstance();
$context = $application->getContext();

/*if(!$_POST['counter'] || !$APPLICATION->get_cookie('WEB_REG_ID')) {
    return;
}*/
if(!$_POST['counter'] || !Application::getInstance()->getContext()->getRequest()->getCookie("WEB_REG_ID")) {
    return;
}

//$elementID = intval($APPLICATION->get_cookie('WEB_REG_ID'));
$elementID = intval(Application::getInstance()->getContext()->getRequest()->getCookie("WEB_REG_ID"));
$arFilter = Array("IBLOCK_ID"=>46, "ID"=>$elementID);
$res = CIBlockElement::GetList(Array(), $arFilter, false);

if($ob = $res->GetNextElement()) {
    CIBlockElement::SetPropertyValues($elementID, 46, $_POST['counter'], 'WEB_REG_STAYING');

    if($_POST['counter'] >= 5400){

        $webRegVals = [];
        $webRegResult = CIBlockElement::GetProperty(46, $elementID);

        while ($webRegObj = $webRegResult->GetNext()) {
            $webRegVals[$webRegObj['CODE']] = $webRegObj['VALUE'];
        }

        $login = $webRegVals['WEB_REG_EMAIL'];

        $webPageFilter = [
            "IBLOCK_ID"=>40,
            "CODE"=>$webRegVals['WEB_REG_NAME'],
        ];

        $webPageResult = CIBlockElement::GetList(Array(), $webPageFilter, false);

        if ($webPageObj = $webPageResult->fetch()) {
            $webPageVals = [];
            $webPagePropertyRes = CIBlockElement::GetProperty(40, $webPageObj['ID']);
            while ($webPagePropertiesObj = $webPagePropertyRes->GetNext()) {
                $webPageVals[$webPagePropertiesObj['CODE']] = $webPagePropertiesObj['VALUE'];
            }
        }

        $webStartDate = new DateTime($webPageVals['WEB_DATE_TIME']);
        $now = new DateTime('now');

        //если пользователь посмотрел полтора часа вебинар, в период от начала до 4х часов
        if($now->getTimestamp() >= ($webStartDate->getTimestamp()+60*60*4)){
            $adder = new CrmAdder();
            $adder->addWebinarDate($webPageObj["NAME"], $webPageVals["WEB_DATE_NAME"], $login, $webStartDate->format('d.m.Y'));
        }
    }
}