<?
//Содержимое файла /bitrix/templates/.default/ajax/auth.php
//Изменено повеоедине после регистрации. Переходит по переданому из popup окна пути.
if (!defined('PUBLIC_AJAX_MODE')) {
    define('PUBLIC_AJAX_MODE', true);
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION, $USER;

switch($_REQUEST['TYPE'])
{
	case "CHANGE_PWD":
    {
        //Компонент с шаблоном errors выводит только ошибки
        $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "errors",
            Array(
                "REGISTER_URL" => "",
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => "/personal/profile/",
                "SHOW_ERRORS" => "Y"
            )
        );
        $APPLICATION->IncludeComponent(
                "bitrix:system.auth.changepasswd",
                "popup",
                Array(
                   
                )
            );
    }
        break;
    case "SEND_PWD":
    {
		if($_REQUEST["USER_LOGIN"]){
			$arResult = $USER->SendPassword($_REQUEST["USER_LOGIN"], $_REQUEST["USER_LOGIN"]);
			if($arResult["TYPE"]=="OK"){
				$new_password = randString(6, "123456789");
				
				$cUser = new CUser; 
				$sort_by = "ID";
				$sort_ord = "ASC";
				$arFilter = array(
				   "LOGIN_EQUAL" => $_REQUEST["USER_LOGIN"]
				);
				$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
				if ($arUser = $dbUsers->Fetch()) 
				{
					$user = new CUser;
					$fields = Array( 
						"PASSWORD" =>$new_password,
						"CONFIRM_PASSWORD" =>$new_password
					); 
					$user->Update($arUser["ID"], $fields);
				}
				
				$arEventFields = array(
					"PAS" => $new_password,
					"EMAIL" => $_REQUEST["USER_LOGIN"]
				);
				CEvent::Send("SEND_NEW_PASS", SITE_ID, $arEventFields);
				?><p>Новый пароль отправлен вам на электронную почту.</p><?
				exit();
			}
		}
		 
        //Компонент с шаблоном errors выводит только ошибки
        $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "errors",
            Array(
                "REGISTER_URL" => "",
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => "/personal/profile/",
                "SHOW_ERRORS" => "Y"
            )
        );
        $APPLICATION->IncludeComponent(
                "bitrix:system.auth.forgotpasswd",
                "new_popup",
                Array(
                   
                )
            );
    }
        break;

    case "REGISTRATION":
    {
        //Компонент с шаблоном errors выводит только ошибки
        $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "errors",
            Array(
                "REGISTER_URL" => "",
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => "/personal/profile/",
                "SHOW_ERRORS" => "Y"
            )
        );

        //Это компонент настраиваемой регистрации, либо используйте его (рекомендуется),
        //либо компонент bitrix:system.auth.registration
        $APPLICATION->IncludeComponent(
            "bitrix:main.register",
            "reg_new_simple",
            Array(
                "SHOW_FIELDS" => array("NAME"),
                "REQUIRED_FIELDS" => array("NAME"),
                "AUTH" => "Y",
                "USE_BACKURL" => "Y",
                //"SUCCESS_PAGE" => "/lk/obligation/",
                "SET_TITLE" => "N",
                "USER_PROPERTY" => array(),
                "USER_PROPERTY_NAME" => "",
            )
        );

        //Это простая регистрация, если импользуете ее, то выше компонент bitrix:main.register закомментируйте!
        //$APPLICATION->IncludeComponent("bitrix:system.auth.registration","",Array());


        //Если в настройках главного модуля отключено "Запрашивать подтверждение регистрации по E-mail"
        //и в настройках включена автоматическая авторизация после регистрации "AUTH" => "Y",
        //то пользователю будет показано это сообщение и страница перезагрузится,
        if($USER->IsAuthorized())
        {
            $APPLICATION->RestartBuffer();
            $backurl = $_POST["backurl"] ? $_POST["backurl"] : '/';
				LocalRedirect($backurl);

        } else {

            ?>
            <?
        }
    }
        break;

    default:
    {
        //Вместо компонента system.auth.form можете использовать компонент system.auth.authorize,
        //но не забудьте поменять вызов компонента в HTML на аналогичный

        //$APPLICATION->IncludeComponent("bitrix:system.auth.authorize","",Array());
	   $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "auth",
            Array(
                "REGISTER_URL" => "",
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => "/personal/profile/",
                "SHOW_ERRORS" => "Y"
            )
        );


        //1. Если нужно показать какую-нибудь информацию об успешном входе на сайт и перезагрузить страницу
        /*if($USER->IsAuthorized())
        {
            //Если посетитель авторизовался/вошел на сайт под своим логином и паролем, необходимо сбросить буфер,
            //иначе у нас будет выводиться выше по коду HTML-код формы авторизованного посетителя
            $APPLICATION->RestartBuffer();
            $backurl = $_REQUEST["backurl"] ? $_REQUEST["backurl"] : '/';

            //тут выводим любую информацию посетителю
            ?>
            <p>Дорогой <b><?=$USER->GetFullName();?>!</b><br/>
                <span style="color: #008000;">Вы зарегистрированы и успешно вошли на сайт!</span>
            </p>
            <p>Сейчас страница автоматически перезагрузится и Вы сможете продолжить <br/>работу под своим именем.</p>
            <script>
                function TSRedirectUser(){
                    window.location.href = '<?=$backurl;?>';
                }

                //Через 2 секунды перезагружаем страницу, чтобы вся страница знала, что посетитель авторизовался.
                //1000 - это 1 секунда
                window.setTimeout('TSRedirectUser()',2000);
            </script>
        <?
        }*/

        //2. Если нужно показать форму авторизованного посетителя и никуда не перенаправлять,
        //то условие выше  if($USER->IsAuthorized()){...}  полностью закомментируйте

        //3. Если не нужно выводить никакую информацию после авторизации, а немедленно перезагрузить страницу,
        //тогда аналогичный код выше закомментируйте, а этот раскомментируйте.
        if($USER->IsAuthorized())
        {
            $APPLICATION->RestartBuffer();
            $backurl = $_REQUEST["backurl"] ? $_REQUEST["backurl"] : '/';

            ?>
            <script>
                window.location.href = '<?=$backurl;?>';
            </script>
        <?
        }
    }
}
