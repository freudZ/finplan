<?
if (!defined('PUBLIC_AJAX_MODE')) {
    define('PUBLIC_AJAX_MODE', true);
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION, $USER;
CModule::IncludeModule("iblock");

$name = $_POST['name'];

if(!$name || !$APPLICATION->get_cookie('WEB_REG_ID')){
    return;
}

$elementID = intval($APPLICATION->get_cookie('WEB_REG_ID'));
$arFilter = Array("IBLOCK_ID"=>46, "ID"=>$elementID);
$webRegResult = CIBlockElement::GetList(Array(), $arFilter, false);
$propertyCodes = [
    'like' => 'WEB_REG_LIKE_COUNT',
    'dislike' => 'WEB_REG_DIS_COUNT',
    'fire' => 'WEB_REG_WANTS_COUNT',
];

if($webRegObj = $webRegResult->GetNextElement()) {
    $webRegPropertiesVals = [];
    $webRegProperties = CIBlockElement::GetProperty(46, $elementID);

    while ($webRegPropertiesObj = $webRegProperties->GetNext()) {
        $webRegPropertiesVals[$webRegPropertiesObj['CODE']] = $webRegPropertiesObj['VALUE'];
    }

    $voteCount = intval($webRegPropertiesVals[$propertyCodes[$name]])+1;
    CIBlockElement::SetPropertyValues($elementID, 46, $voteCount, $propertyCodes[$name]);
}

if(!$voteCount){
    return;
}

$arFilter = [
    "IBLOCK_ID"=>40,
    "CODE"=>$webRegPropertiesVals["WEB_REG_NAME"]
];
$webPageResult = CIBlockElement::GetList(Array(), $arFilter, false);

if($webPageObj = $webPageResult->fetch()) {
    $webPagePropertiesVals = [];
    $webPageProperties = CIBlockElement::GetProperty(40, $webPageObj['ID']);

    while ($webPagePropertiesObj = $webPageProperties->GetNext()) {
        $webPagePropertiesVals[$webPagePropertiesObj['CODE']] = $webPagePropertiesObj['VALUE'];
    }
}

if(!$webPageObj['ID']){
    return;
}

$email = $webRegPropertiesVals['WEB_REG_EMAIL'];
$webStartDate = new DateTime($webPagePropertiesVals['WEB_DATE_TIME']);

switch ($propertyCodes[$name]) {
    case 'WEB_REG_WANTS_COUNT': {
        CIBlockElement::SetPropertyValues($webPageObj['ID'], 40, $webPagePropertiesVals['WEB_WANTS']+=1, 'WEB_WANTS');
        $adder = new CrmAdder();
        $adder->addWantsDate($webPageObj["NAME"], $webPagePropertiesVals["WEB_DATE_NAME"], $email, $webStartDate->format('d.m.Y'));
    }
    break;
    case 'WEB_REG_LIKE_COUNT': {
        CIBlockElement::SetPropertyValues($webPageObj['ID'], 40, $webPagePropertiesVals['WEB_LIKES']+=1, 'WEB_LIKES');
    }
    break;
    case 'WEB_REG_DIS_COUNT': {
        if(!intval($webPagePropertiesVals['WEB_DISLIKES'])){
            CIBlockElement::SetPropertyValues($webPageObj['ID'], 40, $webPagePropertiesVals['WEB_DISLIKES']+=1, 'WEB_DISLIKES');
        }
    }
    break;
}

print_r(json_encode($webPagePropertiesVals));
