<?

//Содержимое файла /bitrix/templates/.default/ajax/auth.php
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;

if (!defined('PUBLIC_AJAX_MODE')) {
    define('PUBLIC_AJAX_MODE', true);
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION, $USER;

$application = Application::getInstance();
$context = $application->getContext();

CModule::IncludeModule("iblock");

$UserLogin = $_POST['email'];
$_SESSION['email'] = $UserLogin;
$rsUser = CUser::GetByLogin($UserLogin);

$arUser = $rsUser->Fetch() ?: getRegItemByUserEmail($UserLogin);


if(is_array($arUser)) {

    $cookie = new Cookie("WEB_SIGN", 1, time()+3+60*60*6);
    $cookie->setDomain($context->getServer()->getHttpHost());
    $cookie->setHttpOnly(false);
    $context->getResponse()->addCookie($cookie);
    //$context->getResponse()->flush("");   //Не отрабатывает после обновления битрикс до последней версии


    $APPLICATION->set_cookie("WEB_SIGN", 1, time()+3+60*60*6);
    $webRegID = webinarRegistration($_POST['email'], $_POST['WEB_NAME'], $arUser["PERSONAL_PHONE"]?:$arUser["PHONE"]);
    $APPLICATION->set_cookie("WEB_REG_ID", $webRegID, time()+3+60*60*6);

/*    $cookie = new Cookie("WEB_REG_ID", $webRegID, time()+3+60*60*6);
    $cookie->setDomain($context->getServer()->getHttpHost());
    $cookie->setHttpOnly(false);
    $context->getResponse()->addCookie($cookie);*/
    //$context->getResponse()->flush("");

   print_r(json_encode([
        'name' => $arUser['NAME'],
        'text' => "Добро пожаловать на вебинар!",
    ]));
	 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
	 exit();


}elseif ($_POST['email'] && $_POST['name'] && $_POST['phone_final']){

    $pass = generate_password(6);

    //$arResult = $USER->Register(strip_tags($_POST["USER_LOGIN"]), strip_tags($_POST["USER_NAME"]), "", $pass, $pass, strip_tags($_POST["USER_LOGIN"]));

    $user = new CUser;

    $arFields = Array(
        "NAME"              => strip_tags($_POST["name"]),
        "EMAIL"             => strip_tags($_POST["email"]),
        "LOGIN"             => strip_tags($_POST["email"]),
        "PERSONAL_PHONE "   => (string)$_POST["phone_final"],
        "ACTIVE"            => "Y",
        "GROUP_ID"          => array(3,4,6),
        "PASSWORD"          => $pass,
        "CONFIRM_PASSWORD"  => $pass
    );

    $arResult = $user->Add($arFields);

    if($arResult["TYPE"]=="ERROR"){
        print_r(json_encode([
            'name' => $_POST["name"],
            'text' => "При регистрации возникла ошибка!",
        ]));
    } else {
        $arEventFields = array(
            "EMAIL" => strip_tags($_POST["email"]),
            "LOGIN" => strip_tags($_POST["email"]),
            "PAS" => $pass
        );

        CEvent::Send("NEW_SEM_REG", SITE_ID, $arEventFields);

        $APPLICATION->set_cookie("WEB_SIGN", 1, time()+3+60*60*6);
        $cookie = new Cookie("WEB_SIGN", 1, time()+3+60*60*6);
        $cookie->setDomain($context->getServer()->getHttpHost());
        $cookie->setHttpOnly(false);

        $context->getResponse()->addCookie($cookie);

        //$context->getResponse()->flush("");

        $webRegID = webinarRegistration($_POST['email'], $_POST['WEB_NAME'], $_POST["phone_final"]);
        $APPLICATION->set_cookie("WEB_REG_ID", $webRegID, time()+3+60*60*6);
        $cookie = new Cookie("WEB_REG_ID", $webRegID, time()+3+60*60*6);
        $cookie->setDomain($context->getServer()->getHttpHost());
        $cookie->setHttpOnly(false);
        $context->getResponse()->addCookie($cookie);
        //$context->getResponse()->flush("");

        print_r(json_encode([
            'name' => $_POST["name"],
            'phone' => $_POST["phone_final"],
            'text' => "Добро пожаловать на вебинар!!",
        ]));
		  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
    }
} else {
    print_r(json_encode(false));
}

function generate_password($number)
{
    $arr = array('1','2','3','4','5','6','7','8','9','0');
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
        // Вычисляем случайный индекс массива
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}

function webinarRegistration($email, $webName, $phone){
    $el = new CIBlockElement;

    $props = [
        'WEB_REG_EMAIL' => $email,
        'WEB_REG_DATE' => date("d.m.Y H:i:s"),
        'WEB_REG_NAME' => $webName,
        'WEB_REG_PHONE' => $phone,
    ];

    $data = Array(
        "IBLOCK_ID"      => 46,
        "PROPERTY_VALUES"=> $props,
        "NAME"           => date("d.m.Y H:i:s"),
        "ACTIVE"         => "N",
    );

    $adder = new CrmAdder();
    $adder->add($webName);

    return $el->Add($data);
}

//Есть ли пользователь
function getRegItemByUserEmail($email){
    if(!$email){
        return false;
    }

    $arFilter = Array("IBLOCK_ID"=>19, "PROPERTY_EMAIL" => $email);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false);
    if($ob = $res->GetNextElement())
    {
        $userProperties = $ob->GetProperties();
        $result = [];
        foreach ($userProperties as $k => $v) {
            $result[$k] = $v['VALUE'];
        }

        return $result;
    }
}
