<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)?>
    <?if(ERROR_404!="Y"):?>
		<div class="footer">
            <?if($APPLICATION->GetCurPage()=="/" || $APPLICATION->GetCurPage()=="/lk/radar_in/" || $APPLICATION->GetCurPage()=="/test/" ):?>
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-6 col-sm-3">
                            <p>©&nbsp;2016-<script type="text/javascript">var mdate = new Date(); document.write(mdate.getFullYear());</script> ИП&nbsp;Кошин&nbsp;В.В.<br/><span>ОГРН&nbsp;316583500057741&nbsp;<br/>ИНН&nbsp;583708408904</span><br><a href="https://drive.google.com/file/d/1mA7bpyLgIpQs3TejsVVhe1Ht7vP31qw5/view" target="_blank" rel="nofollow">Образовательная лицензия №12416</a></p>
                        </div>

                        <div class="col col-xs-6 col-sm-3">
                            <p>Служба&nbsp;инвест-заботы о&nbsp;клиентах:<br/><a href="mailto:koshin@fin-plan.org">koshin@fin-plan.org</a></p>
                        </div>

                        <div class="col col-xs-6 col-sm-4">
                            <p>Написать директору проекта:<br/><a class="" href="https://vk.com/vitalykoshin" rel="nofollow" target="_blank">&bull;&nbsp;Вконтакте</a><br/><a class="" href="https://www.instagram.com/invest_finplan/"  rel="nofollow" target="_blank">&bull;&nbsp;Инстаграм</a></p>
                        </div>

                        <div class="col col-xs-6 col-sm-2">
                            <span class="footer_black">
                                <?global $footerPopupTextFilter;
								$footerPopupTextFilter["PROPERTY_HIDE_FOOTER"] = false;
								?>
                                <?$APPLICATION->IncludeComponent("bitrix:news.list", "footer_popups_links", Array(
                                "IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
                                    "IBLOCK_ID" => "15",	// Код информационного блока
                                    "NEWS_COUNT" => "6",	// Количество новостей на странице
                                    "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                                    "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                                    "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                                    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                                    "FILTER_NAME" => "footerPopupTextFilter",	// Фильтр
                                    "FIELD_CODE" => array(	// Поля
                                        0 => "",
                                        1 => "",
                                    ),
                                    "PROPERTY_CODE" => array(	// Свойства
                                        0 => "TOP_NAME",
                                        1 => "",
                                    ),
                                    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                                    "AJAX_MODE" => "N",	// Включить режим AJAX
                                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                    "CACHE_TYPE" => "A",	// Тип кеширования
                                    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                                    "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                                    "ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
                                    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                                    "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                                    "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                                    "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                                    "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                                    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                                    "PARENT_SECTION" => "",	// ID раздела
                                    "PARENT_SECTION_CODE" => "",	// Код раздела
                                    "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                                    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                                    "PAGER_TITLE" => "Новости",	// Название категорий
                                    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                                    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                                    "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                                    "DISPLAY_NAME" => "Y",	// Выводить название элемента
                                    "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                                    "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                                    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                ),
                                false
                            );?>
                            </span>
                        </div>
                    </div>
                </div>
            <?else:?>
                <div class="footer_first">
                    <div class="container">
                        <div class="row">
                            <div class="logo_col col col-xs-6 col-sm-3">
                                <a class="logo" href="/"><span>Fin</span>-plan</a>
                            </div>

                            <div class="col col-xs-6 col-sm-3">
                                <p>Служба&nbsp;инвест-заботы о&nbsp;клиентах:<br/><a href="mailto:koshin@fin-plan.org">koshin@fin-plan.org</a><br>&nbsp;</p>
                            </div>

                            <div class="col col-xs-6 col-sm-3">
                                <p>Написать директору проекта:<br/><a class="" href="https://vk.com/vitalykoshin" rel="nofollow" target="_blank">&bull;&nbsp;Вконтакте</a><br/><a class="" href="https://www.instagram.com/invest_finplan/"  rel="nofollow" target="_blank">&bull;&nbsp;Инстаграм</a></p>
                            </div>

                            <div class="col col-xs-6 col-sm-3">
                                <p>Все права защищены © 2015-<script type="text/javascript">var mdate = new Date(); document.write(mdate.getFullYear());</script> ИП Кошин В.В <br/>ОГРН 316583500057741/ ИНН 583708408904<br><a href="https://drive.google.com/file/d/1mA7bpyLgIpQs3TejsVVhe1Ht7vP31qw5/view" target="_blank" rel="nofollow">Образовательная лицензия №12416</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer_second">
                    <div class="container">
                        <div class="row">
							<?global $footerPopupTextFilter;
							$footerPopupTextFilter["PROPERTY_HIDE_FOOTER"] = false;
							?>
							<?$APPLICATION->IncludeComponent("bitrix:news.list", "footer_popups_links_inner", Array(
								"IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
								"IBLOCK_ID" => "15",	// Код информационного блока
								"NEWS_COUNT" => "6",	// Количество новостей на странице
								"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
								"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
								"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
								"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
								"FILTER_NAME" => "footerPopupTextFilter",	// Фильтр
								"FIELD_CODE" => array(	// Поля
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(	// Свойства
									0 => "TOP_NAME",
									1 => "",
								),
								"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
								"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
								"AJAX_MODE" => "N",	// Включить режим AJAX
								"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
								"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
								"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
								"CACHE_TYPE" => "A",	// Тип кеширования
								"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
								"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
								"CACHE_GROUPS" => "Y",	// Учитывать права доступа
								"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
								"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
								"SET_TITLE" => "N",	// Устанавливать заголовок страницы
								"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
								"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
								"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
								"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
								"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
								"PARENT_SECTION" => "",	// ID раздела
								"PARENT_SECTION_CODE" => "",	// Код раздела
								"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
								"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
								"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
								"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
								"PAGER_TITLE" => "Новости",	// Название категорий
								"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
								"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
								"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
								"DISPLAY_DATE" => "Y",	// Выводить дату элемента
								"DISPLAY_NAME" => "Y",	// Выводить название элемента
								"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
								"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
								"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
							),
								false
							);?>

                            <div class="col col-xs-6 col-sm-3">
                                <p class="footer_phone">тел.: <a href="tel:84952408186" data-type="tel">8 (495) 240-81-86</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer_third">
                    <div class="container">
                        <div class="row">
                            <div class="col col-xs-6 col-sm-9 col-lg-8">
                                <p>Примеры достижений касаются личных результатов, являются последствием личных знаний, знаний и опыта. <br/>Мы не несем ответственность за результаты, полученные другими людьми, поскольку они могут отличаться в зависимости от различных обстоятельств.</p>
                            </div>
                            <div class="col col-xs-6 col-sm-3 col-lg-4">
                                <ul class="share_list">
                                    <li><a href="https://www.facebook.com/Fin-planorg-1588477478078872/" target="_blank" class="icon icon-fb"></a></li>
                                    <li><a href="https://vk.com/fin_plan_org" target="_blank" class="icon icon-vk"></a></li>
                                    <li><a href="https://www.instagram.com/invest_finplan/" target="_blank" class="icon icon-inst"></a></li>
                                    <li><a href="https://www.youtube.com/channel/UCG6oEO3F-7W-_yhnwkqbFrw" target="_blank" class="icon icon-yt"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?endif;?>
		</div>

        
            <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/corona_widget.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?>
            <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include/common_popup_hl.php"),Array(),Array("MODE"=>"html","NAME"=>"блок")) ?>
	</div>
    <?endif;?>

	<?$APPLICATION->IncludeComponent("bitrix:news.list", "footer_popups_texts", Array(
		"IBLOCK_TYPE" => "FINPLAN",	// Тип информационного блока (используется только для проверки)
			"IBLOCK_ID" => "15",	// Код информационного блока
			"NEWS_COUNT" => "6",	// Количество новостей на странице
			"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
			"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
			"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
			"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
			"FILTER_NAME" => "",	// Фильтр
			"FIELD_CODE" => array(	// Поля
				0 => "",
				1 => "",
			),
			"PROPERTY_CODE" => array(	// Свойства
				0 => "TOP_NAME",
				1 => "",
			),
			"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
			"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
			"AJAX_MODE" => "N",	// Включить режим AJAX
			"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			"CACHE_TYPE" => "A",	// Тип кеширования
			"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
			"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
			"CACHE_GROUPS" => "Y",	// Учитывать права доступа
			"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
			"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
			"SET_TITLE" => "N",	// Устанавливать заголовок страницы
			"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
			"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
			"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
			"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
			"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
			"PARENT_SECTION" => "",	// ID раздела
			"PARENT_SECTION_CODE" => "",	// Код раздела
			"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
			"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
			"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
			"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
			"PAGER_TITLE" => "Новости",	// Название категорий
			"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
			"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
			"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
			"DISPLAY_DATE" => "Y",	// Выводить дату элемента
			"DISPLAY_NAME" => "Y",	// Выводить название элемента
			"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
			"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
			"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		),
		false
	);?>

    <?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_download_book.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

    <?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_invite_webinar.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

    <?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_invite_webinar_custom.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include/popup_login.php"),
		Array(),
		Array("MODE"=>"php")
	);?>

	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include/popup_resetpw.php"),
		Array(),
		Array("MODE"=>"php")
	);?>

	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include/popup_signup.php"),
		Array(),
		Array("MODE"=>"php")
	);?>
	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include/popup_simple_auth.php"),
		Array(),
		Array("MODE"=>"php")
	);?>
	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include/popup_signup_simple.php"),
		Array(),
		Array("MODE"=>"php")
	);?>
	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include/popup_feed.php"),
		Array(),
		Array("MODE"=>"php")
	);?>

    <?  $showPopupClose = \Bitrix\Main\Config\Option::get("grain.customsettings","SHOW_POPUP_CLOSE");
	 if($showPopupClose=='Y'){
		 $APPLICATION->IncludeFile(
	        $APPLICATION->GetTemplatePath("include/popup_close.php"),
	        Array(),
	        Array("MODE"=>"php")
	    );
		}
	 ?>

    <?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_add_review.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_buy_radar.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_pay_sber.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_available_to_everyone.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_available_to_everyone_form.php"),
        Array(),
        Array("MODE"=>"php")
    );?>



	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_add_active_to_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?
	  $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_add_cache_to_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<?
	  $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_add_cashflow_to_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<?
	  $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_edit_row_history_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?
  	  $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_remove_cache_from_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<?
  	  $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_deal_remove.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_remove_active_from_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_rename_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_mass_portfolio_remove.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	 
	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_portfolio_remove.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_portfolio_clear.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_add_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_copy_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_description_portfolio.php"),
        Array(),
        Array("MODE"=>"php")
    );?>



	<?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include/popup_load_from_broker2.php"),
        Array(),
        Array("MODE"=>"php")
    );?>

	<div class="modal fade" id="popup_join" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<p class="modal-title uppercase">Присоединиться</p>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		  </div>
		  <div class="modal-body">
			<p class="font_12 light text-center">Присоединяйтесь к сообществу профессиональных инвесторов и начните получать по почте материалы для пошагового освоения инвестирования и свежие инвест идеи</p>
			<form class="body_signup" method="post">

				<div class="form_element">
					<input type="text" placeholder="Ваше имя" name="name"/>
				</div>
				<div class="form_element">
					<input type="text" name="email" placeholder="Ваша электронная почта"/>
				</div>
				<div class="form_element">
					<input type="text" placeholder="Ваш телефон" name="phone"/>
				</div>

        <div class="confidencial_element form_element">
          <div class="checkbox">
            <input type="checkbox" id="confidencial_join" name="confidencial_join" checked required />
            <label for="confidencial_join">Нажимая кнопку, я даю согласие <br />на обработку персональных <br />данных и соглашаюсь с <br /></label><span data-toggle="modal" data-target="#footer354">пользовательским соглашением</span><label for="confidencial_join"> <br />и </label><span data-toggle="modal" data-target="#footer354">политикой конфиденциальности</span>
          </div>
        </div>

				<div class="form_element text-center">
				  <input type="submit" class="button" value="Подписаться"/>
				</div>
			</form>
		  </div>
		</div>
	  </div>
	</div>

    <div id="cookie_msg">
      <div class="container">
        <p>Мы используем куки-файлы, чтобы улучшить ваше восприятие нашего сайта. Просматривая наш сайт, вы соглашаетесь с использованием нами куки-файлов.</p>
        <span class="cookie_msg__close button">Я понял</span>
      </div>
    </div>

  <? /* if($USER->IsAdmin()): */ ?>

  	<div class="modal modal_black fade" id="coupons_modal_calc" tabindex="-1">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <p class="modal-title uppercase">Калькулятор облигации <br /><span id="coupons_modal_calc_name"></span></p>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  </div>
                  <div class="modal-body">
                      <div class="modal-body_inner">
  											<form class="innerpage_search_form" method="GET">
  												<div class="form_element">
  													<input type="text" value="<?=$_REQUEST["q"]?>" placeholder="Введите название облигации" name="q" id="autocomplete_obl_calc" />
  												</div>
  											</form>
                        <div class="flex_row row calc-action">
                            <div class="col col_left col-xs-5">
                                <p class="t16 white m0">Цена облигации, в&nbsp;%</p>
                            </div>
                            <div class="col col_left col-xs-7">
                                <div class="form_element">
                                    <input type="text" id="coupons_modal_calc_price" placeholder="Укажите цену облигации" value="<?=$arResult["OBLIGATION"]["PROPS"]["LASTPRICE"]["VALUE"]?>" />
                                </div>
                            </div>
                        </div>

                        <div class="flex_row row calc-action">
                            <div class="col col_left col-xs-5">
                                <p class="t16 white m0">Размер неизвестных купонов, в&nbsp;руб.</p>
                            </div>
                            <div class="col col_left col-xs-7">
                                <div class="form_element text-center">
                                    <input type="text" id="coupons_modal_calc_coupon_price" placeholder="Укажите размер неизвестных купонов" value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="flex_row row calc-action">
                            <div class="col col_left col-xs-5">
                                <p class="t16 white m0">&nbsp;</p>
                            </div>
                            <div class="col col_left col-xs-7">
                                <div class="form_element text-center">
                                    <span class="coupons_detail_modal_calc">детализированно</span>
                                </div>
                            </div>
                        </div>

                        <p class="coupons_modal_result text-center calc-action">Доходность = <span id="coupons_modal_calc_result"></span>%</p>

                        <? /*<p class="text-center mt20"><a href="/lk/obligations/<?=$arResult["OBLIGATION"]["PROPS"]["SECID"]?>">Открыть страницу облигации</a></p> */ ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="modal modal_black fade" id="coupons_detail_modal_calc" tabindex="-1">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <span class="back_btn"><span class="icon icon-arr_left"></span><span class="text">Назад</span></span>
                      <p class="modal-title uppercase">Детализированные купоны <br /><span id="coupons_detail_modal_calc_name"></span></p>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  </div>
                  <div class="modal-body">
                      <div class="modal-body_inner">
  											<div class="flex_row row">
  								        <div class="col col_left col-xs-5">
  								          <p class="t16 white m0">Цена облигации, в&nbsp;%</p>
  								        </div>
  								        <div class="col col_left col-xs-7">
  								          <div class="form_element">
  								            <input type="text" id="coupons_detail_modal_calc_price" placeholder="Укажите цену облигации" value="" />
  								          </div>
  								        </div>
  								      </div>
                        <div id="coupons_detail_modal_calc_list">

                        </div>
                        <p class="coupons_modal_result text-center">Доходность = <span id="coupons_detail_modal_calc_result">?</span>%</p>
                    </div>
                  </div>
              </div>
          </div>
      </div>

  <? /* endif; */ ?>

	<!-- <div class="modal fade" id="popup_not_available" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		  </div>
		  <div class="modal-body">
			<p>Данный курс будет доступен после изучения предыдущих уроков и выполнения всех заданий</p>
		  </div>
		</div>
	  </div>
	</div> -->

	<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/device.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/intlTelInput.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/intlTelInput_utils.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedinput.min.js"></script>


	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.lettersonly.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.truemail.js"></script>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fullpage.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/slick.min.js"></script>

    <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap-datepicker.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap-datepicker.ru.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.pack.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/fancybox/helpers/jquery.fancybox-media.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.autocomplete.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.ui.touch-punch.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.cookie.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/clipboard.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/select2.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/footable.core.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/pdf.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/radar_presets.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/radar_presets.js') ?>"></script>


	<?if(PORTFOLIO_PAGE=="Y"):?>
	 <script src="<?=SITE_TEMPLATE_PATH?>/js/moment-with-locales.min.js"></script>
	 <script src="<?=SITE_TEMPLATE_PATH?>/js/chart.bundle.min.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/chart.bundle.min.js') ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
	 <script src="<?=SITE_TEMPLATE_PATH?>/js/company.js"></script>
	 <script src="<?=SITE_TEMPLATE_PATH?>/js/calculate.js"></script>
	 <script src="<?=SITE_TEMPLATE_PATH?>/js/radar_logic.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/radar_logic.js') ?>"></script>
	 <script src="<?=SITE_TEMPLATE_PATH?>/js/portfolio.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/portfolio.js') ?>"></script>
	 <script src="<?=SITE_TEMPLATE_PATH?>/js/portfolio_analyse.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/portfolio_analyse.js') ?>"></script>

	<?endif;?>

	<?if(OBLIGATION_PAGE=="Y"):?>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/chart.bundle.min.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/chart.bundle.min.js') ?>"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/calculate.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/company.js"></script>
<!--  		<script src="--><?//=SITE_TEMPLATE_PATH?><!--/js/jquery.jqplot.min.js"></script>-->
  		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisTickRenderer.js"></script>
  		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasTextRenderer.js"></script>
  		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.dateAxisRenderer.js"></script>
  		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.ohlcRenderer.js"></script>
  		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.highlighter.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/portfolio.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/portfolio.js') ?>"></script>
	<?endif?>
	<?if(MARKET_MAP_PAGE=="Y"):?>
	   <script src="<?=SITE_TEMPLATE_PATH?>/js/chart.bundle.min.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/chart.bundle.min.js') ?>"></script>
	   <script src="<?=SITE_TEMPLATE_PATH?>/js/moment-with-locales.min.js"></script>

		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jqplot.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.bubbleRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.dateAxisRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasTextRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisTickRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.ohlcRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.highlighter.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.logAxisRenderer.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisLabelRenderer.js"></script>
	 <!--	<script src="<?=SITE_TEMPLATE_PATH?>/js/calculate.js"></script>  -->
		<script src="<?=SITE_TEMPLATE_PATH?>/js/company.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/market_map.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/market_map.js') ?>"></script>
	<?endif;?>
	<?if(ETF_RADAR_PAGE=="Y"):?>
	   <script src="<?=SITE_TEMPLATE_PATH?>/js/chart.bundle.min.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/chart.bundle.min.js') ?>"></script>
	   <script src="<?=SITE_TEMPLATE_PATH?>/js/moment-with-locales.min.js"></script>

		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jqplot.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.bubbleRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.dateAxisRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasTextRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisTickRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.ohlcRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.highlighter.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.logAxisRenderer.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisLabelRenderer.js"></script>
	 <!--	<script src="<?=SITE_TEMPLATE_PATH?>/js/calculate.js"></script>  -->
		<script src="<?=SITE_TEMPLATE_PATH?>/js/company.js"></script>
		 <script src="<?=SITE_TEMPLATE_PATH?>/js/radar_logic.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/radar_logic.js') ?>"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/radar_etf.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/radar_etf.js') ?>"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/portfolio.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/portfolio.js') ?>"></script>
	<?endif;?>
    <?if(TEST_PAGE=="Y"):?>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/moment-with-locales.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/bic_calendar.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/testpage.js"></script>
    <?endif?>
   
	<?if(OBLIGATION_DETAIL_PAGE=="Y" ):?>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jqplot.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisTickRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasTextRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.dateAxisRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.ohlcRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.highlighter.js"></script>

		<script src="<?=SITE_TEMPLATE_PATH?>/js/portfolio.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/portfolio.js') ?>"></script>
	<?endif?>
	<?if(INDEXES_DETAIL_PAGE=="Y"):?>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/chart.bundle.min.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/chart.bundle.min.js') ?>"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jqplot.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisTickRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasTextRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.dateAxisRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.ohlcRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.highlighter.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.logAxisRenderer.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisLabelRenderer.js"></script>		
		<script src="<?=SITE_TEMPLATE_PATH?>/js/company.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/portfolio.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/portfolio.js') ?>"></script>
	<?endif?>
    <?if(CALCULATOR_PAGE=="Y"):?>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/chart.bundle.min.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/chart.bundle.min.js') ?>"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/calculate_iis.js"></script>
		  <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jqplot.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisTickRenderer.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasTextRenderer.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.dateAxisRenderer.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.ohlcRenderer.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.highlighter.js"></script>
    <?endif;?>
  <script src="<?=SITE_TEMPLATE_PATH?>/js/coupons.js"></script>



	<?if(COMPANY_DETAIL_PAGE=="Y" || OBLIGATION_PAGE=="Y" || PORTFOLIO_PAGE=="Y" || ETF_RADAR_PAGE=="Y" || MACRO_RADAR_PAGE=="Y" || SHOW_CHART_GRAPH=="Y"):?>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/chart.bundle.min.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/chart.bundle.min.js') ?>"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jqplot.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisTickRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasTextRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.dateAxisRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.ohlcRenderer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.highlighter.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/company.js"></script>
	<?endif;?>

	<?if(LK_PAGE=="Y"):?>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/portfolio.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/portfolio.js') ?>"></script>
	<?endif;?>
	
	<?if(COMPANY_DETAIL_PAGE=="Y" || OBLIGATION_PAGE=="Y" || MARKET_MAP_PAGE=="Y" || PORTFOLIO_PAGE=="Y" || ETF_RADAR_PAGE=="Y" || MACRO_RADAR_PAGE=="Y" || INDEXES_DETAIL_PAGE=="Y" || CSite::InDir('/tehanaliz/')):?>
        <?// Подключаем скрипт отрисовки и обновления столбчатых графиков на страницах компаний, макро, отраслей?>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/companyExtendedGraph.js"></script>
        <script>
            companyExtendedRefresh();
		</script>
	<?endif?>


    <?if(CSite::InDir('/tehanaliz/')):?>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/chart.bundle.min.js?hash=<?= md5_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/js/chart.bundle.min.js') ?>"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jqplot.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasAxisTickRenderer.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.canvasTextRenderer.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.dateAxisRenderer.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.ohlcRenderer.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jqplot.highlighter.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/footable.core.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/company.js"></script>
    <?endif;?>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.plugin.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.countdown.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.countdown-ru.js"></script>


	<?if(!CSite::InDir("/lk/") && !CSite::InDir("/portfolio/") ):?>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>
    <script type="text/javascript" async>
        VK.Widgets.Group("vk_groups", {mode: 6, width: "215", height: "350", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 81295080);
    </script>
	<?endif;?>


    <script src="<?=SITE_TEMPLATE_PATH?>/more.js?r=<?=mt_rand(1, 99999)?>"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/scripts.js?r=<?=mt_rand(1, 99999)?>"></script>

	 
    <script async>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>


	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-56537774-2', 'auto');
	  ga('send', 'pageview');

	</script>


<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=f3675f1d1d656e63767a3a2616a597f5" charset="UTF-8" async></script>
  </body>
</html>
