<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	 		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
	 		 <div class="bg-success center-block index_blocks">
<ul>
<?
foreach($arResult as $arItem):
?>
			  <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
<?endforeach?>
	     </ul>
		 </div>
 		</div>

<?endif?>