<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */
global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';


$strReturn .= '<ul class="breadcrumb">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$homeicon = ($index == 0? '<i class="zmdi zmdi-home"></i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '
		<li class="breadcrumb-item"><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$homeicon.' '.$title.'</a></li>';
	}
	else
	{
		$strReturn .= '
			<li class="breadcrumb-item active">'.$title.'</li>';
	}
}

$strReturn .= '</ul>';

return $strReturn;
