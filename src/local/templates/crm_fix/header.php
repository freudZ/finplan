<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true);
include($_SERVER["DOCUMENT_ROOT"]."/.access.php");
$_SESSION["haveAccessCrm"] = false;
foreach($PERM["/"] as $group=>$p){
	if($p!="R"){
		continue;
	}
	if(in_array($group, $USER->GetUserGroupArray())){
		$_SESSION["haveAccessCrm"] = true;
		break;
	}
}

$_SESSION["datatable_draw"] = 0;

$crmAccess = new CrmAccess();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.png">

    <title><?=$APPLICATION->ShowTitle()?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?=SITE_TEMPLATE_PATH?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!--dynamic table-->
    <link href="<?=SITE_TEMPLATE_PATH?>/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/data-tables/DT_bootstrap.css" />
    <!--right slidebar-->
    <link href="<?=SITE_TEMPLATE_PATH?>/css/slidebars.css" rel="stylesheet"> 
	
	<link href="<?=SITE_TEMPLATE_PATH?>/assets/datatable-filter/jquery.dataTables.yadcf.css" rel="stylesheet">
	<link href="<?=SITE_TEMPLATE_PATH?>/css/select2.min.css" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap-datepicker.min.css" rel="stylesheet">  

    <!-- Custom styles for this template -->
	<link href="<?=SITE_TEMPLATE_PATH?>/css/style.css" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/css/style-responsive.css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/custom.css" rel="stylesheet" />


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=SITE_TEMPLATE_PATH?>/js/html5shiv.js"></script>
      <script src="<?=SITE_TEMPLATE_PATH?>/js/respond.min.js"></script>
    <![endif]-->

	<?$APPLICATION->ShowHead()?>
</head>
	<?
		global $USER;
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            if($arUser["LOGIN"]=="freud"){?>
				<style type="text/css">
				#bx-panel{
					position: fixed !important;
					bottom: 0;
					right: 0;
					left: 0;
				}
				 body{
					 margin-bottom:100px;
				 }

				</style>
            <?}
	 ?>
<body<?if(!$_SESSION["haveAccessCrm"]):?> class="login-body"<?endif?>>
	<?$APPLICATION->ShowPanel()?>
	<?if($_SESSION["haveAccessCrm"]):?>
    <section id="container" class="">
        <!--header start-->
        <header class="header white-bg">
            <!--logo start-->
            <a href="/" class="logo">Fin<span>plan</span></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
					<?foreach($crmAccess->getTableViewTypes() as $code=>$item):?>
						<li<?if($crmAccess->getCurTableView()==$code):?> class="active"<?endif?>>
							<a href="<?=$APPLICATION->GetCurPageParam("change_table_view=".$code, array("change_table_view"));?>">
								<?=$item["name"]?>
							</a>
						</li> 
					<?endforeach?>
                </ul>
            </div>
            <div class="top-nav ">
                <ul class="nav pull-right top-menu">
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
							<?
							$name = $USER->GetFullName();
							if(!$name){
								$name = $USER->GetLogin();
							}
							?>
                            <span class="username"><?=$name?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li><a href="/?logout=yes"><i class="fa fa-key"></i> Выйти</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </header>
        <!--header end-->
	<?endif?>