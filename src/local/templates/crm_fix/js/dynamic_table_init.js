Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};


$(document).ready(function() {

	var Dtable = $('#dynamic-table').DataTable({
		"ajax": {
			"url": '/ajax/table_fix.php',
			"data": function ( d ) {
				return $.extend({}, d, {
					"type": $('.dates > .btn-default').data("val"),
					"date": $('.dateval-input').val(),
					"dopExclude": $("#dopExclude").find('select, textarea, input').serialize(),
				});
			}
		},
		dom: 'Bfrtip',
		"autoWidth": true,
		"stateSave": true,
		"processing": true,
      "serverSide": true,
		"language": {
			"url": "/local/templates/crm/js/dataTables.russian.lang",
			processing: '<i class="progress"></i><span class="sr-only">Loading...</span> '
		},
		"lengthMenu": [ [10, 50, 100, 500, 1000], [10, 50, 100, 500, 1000] ],
		//"searching": false
       // "aaSorting": [[ 4, "desc" ]]
	   
    });
	
	$(".dates > *").click(function(){
		$(".dates > *").removeClass("btn-default");
		$(this).addClass("btn-default");
		
		Dtable.ajax.reload();
	});
	$(".dateval-input").on("dp.change", function(){
		Dtable.ajax.reload();
	});

	$('#dynamic-table').on("click", "tbody tr", function(){
		$(".sb-toggle-right").trigger('click');
		$(".sb-slidebar #ajax-info").empty().addClass("loader");
		
		$.post("/ajax/card_fix.php", {id: $(this).find("td:first").text()}, function(data){
			$(".sb-slidebar #ajax-info").removeClass("loader").html(data);
		});
	});

	$('.sb-slidebar .close').click(function(){
		$(".sb-toggle-right").trigger('click');
	});	
	
	function initDatePicker(){
		$('.datepicker-init').datetimepicker({
			//format: "dd.mm.yyyy",
            locale: 'ru'
			//autoclose: true
		});
	}
	
	$('.dateonlypicker-init').datetimepicker({
			format: "DD.MM.YYYY",
            locale: 'ru',
			//autoclose: true
	});
	initDatePicker();
	
	
	var filterParams = [
		{column_number : 1, filter_type: "text", filter_default_label: "Введите", filter_delay: 500},
		{column_number : 2, filter_type: "text", filter_default_label: "Введите", filter_delay: 500},
		{column_number : 3, filter_type: "text", filter_default_label: "Введите", filter_delay: 500},
		{
			column_number : 4, 
			filter_type: "multi_select", 
			select_type: 'select2', 
			data: $("#dynamic-table").data("items"), 
			filter_default_label: "Введите"
		},
		/*{
            column_number : 5,
            filter_type: "multi_select",
            select_type: 'select2',
            data: $("#dynamic-table").data("webinars"),
            filter_default_label: "Введите"
		},*/

        {column_number : 5, filter_type: "text", filter_default_label: "Введите", filter_delay: 500},
		//продукты
		{
			column_number : 6, 
			filter_type: "multi_select",
			select_type: 'select2', 
			data: $("#dynamic-table").data("products"), 
			filter_default_label: "Введите"
        },
		/*{
			column_number : 8,
            filter_type: "multi_select",
            select_type: 'select2',
            data: $("#dynamic-table").data("timezone"),
            filter_default_label: "Введите"
		}*/
	];

    //дата первой покуки
    filterParams.push({
        column_number : 7,
        filter_default_label: ["с =>", "по <="],
        filter_type: "range_date",
        date_format: 'DD.MM.YYYY',
        datepicker_type: 'bootstrap-datetimepicker',
        filter_plugin_options: {
            locale: 'ru',
        }
    });

    //дата последней покуки
    filterParams.push({
        column_number : 8,
        filter_default_label: ["с =>", "по <="],
        filter_type: "range_date",
        date_format: 'DD.MM.YYYY',
        datepicker_type: 'bootstrap-datetimepicker',
        filter_plugin_options: {
            locale: 'ru',
        }
    });

    //продажи
    filterParams.push({
        column_number: 9,
		filter_type: "text",
		filter_default_label: "Введите",
		filter_delay: 500
    });

    //Менеджер
    filterParams.push({
        column_number: 10,
        filter_type: "multi_select",
        select_type: 'select2',
        data: $("#dynamic-table").data("managers"),
        filter_default_label: "Введите"
    });
	
	if($("#dynamic-table").data("view")=="events" || $("#dynamic-table").data("view")=="invite"){

		filterParams.remove(3);
        filterParams.remove(8);

        //часовой пояс
        filterParams[8] = {
            column_number : 9,
            filter_type: "text", filter_default_label: "Введите", filter_delay: 500
        };
	} else if($("#dynamic-table").data("view")=="admin"){

		//utm 1
		filterParams.push({column_number : 11, filter_type: "text", filter_default_label: "Введите", filter_delay: 500});
		filterParams.push({column_number : 12, filter_type: "text", filter_default_label: "Введите", filter_delay: 500});
		filterParams.push({column_number : 13, filter_type: "text", filter_default_label: "Введите", filter_delay: 500});

		//соц сети
        filterParams.push({column_number : 14, filter_type: "text", filter_default_label: "Введите", filter_delay: 500});

		//часовой пояс
        filterParams.push({
            column_number : 15,
            filter_type: "text", filter_default_label: "Введите", filter_delay: 500
        });

		//mailchimp
		filterParams.push({
            column_number : 16,
            filter_type: "multi_select",
            select_type: 'select2',
            data: $("#dynamic-table").data("mailchimp"),
            filter_default_label: "Введите"
		});

		//дата создания
		filterParams.push({
			column_number : 17,
            filter_default_label: ["с =>", "по <="],
            filter_type: "range_date",
            date_format: 'DD.MM.YYYY',
            datepicker_type: 'bootstrap-datetimepicker',
            filter_plugin_options: {
                locale: 'ru',
            }
		});

		//filterParams.push({column_number : 18, filter_type: "text", filter_default_label: "Введите", filter_delay: 500});

		filterParams.push({
			column_number : 18,
            filter_default_label: ["с =>", "по <="],
            filter_type: "range_date",
            date_format: 'DD.MM.YYYY',
            datepicker_type: 'bootstrap-datetimepicker',
            filter_plugin_options: {
                locale: 'ru',
            }
		});
		filterParams.push({
			column_number : 19,
			filter_default_label: ["с =>", "по <="],
			filter_type: "range_date",
			date_format: 'DD.MM.YYYY',
			datepicker_type: 'bootstrap-datetimepicker',
			filter_plugin_options: {
				locale: 'ru',
			}
		});

        //прозвоны
        filterParams.push({
            column_number: 20,
            filter_type: "text",
            filter_default_label: "Введите",
            filter_delay: 500
        });

        filterParams.push({
            column_number : 21,
            filter_default_label: ["с =>", "по <="],
            filter_type: "range_date",
            date_format: 'DD.MM.YYYY',
            datepicker_type: 'bootstrap-datetimepicker',
            filter_plugin_options: {
                locale: 'ru',
            }
        });



		filterParams.push({column_number : 22, filter_type: "text", filter_default_label: "Введите", filter_delay: 500});
				//referal, status, promocode
		filterParams.push({column_number : 23, filter_type: "text", filter_default_label: "Введите", filter_delay: 500});
/*		filterParams.push({
            column_number : 23,
            filter_type: "multi_select",
            select_type: 'select2',
            //data: $("<select><option value=\"\"></option><option value=\"87\">Не рабочий</option><option value=\"88\">Рабочий</option><option value=\"89\">Использован</option></select>"),
            data: $("#dynamic-table").data("referals"),
            filter_default_label: "Введите"
		});*/

		filterParams.push({
            column_number : 24,
            filter_type: "multi_select",
            select_type: 'select2',
            //data: $("<select><option value=\"\"></option><option value=\"87\">Не рабочий</option><option value=\"88\">Рабочий</option><option value=\"89\">Использован</option></select>"),
            data: [{"label":"Не рабочий","value":"Не рабочий"},{"label":"Рабочий","value":"Рабочий"},{"label":"Использован","value":"Использован"},{"label":"Все заполненные","value":"Все заполненные"},{"label":"Все пустые","value":"Все пустые"}],
            filter_default_label: "Введите"
		});
		filterParams.push({column_number : 25, filter_type: "text", filter_default_label: "Введите", filter_delay: 500});
	}

	yadcf.init(Dtable, filterParams);
	
	
	$('#ajax-info').on("click", ".editable .edit", function(){
		var parent = $(this).closest(".editable"),
			item = parent.find(".val");
			
		if(item.data("type")=="input"){
			var text = item.text(),
				dopHtml = "";
			if(text=="Не задано"){
				text = "";
			}
			if(parent.data("field")=="PROP_NEW_CALL" || parent.data("field")=="PROP_WEBINARS_COME"){
				dopHtml += ' class="datepicker-init"';
			}
			
			item.html("<input "+dopHtml+" type='text' value='"+text+"'>");
			
			if(parent.data("field")=="PROP_NEW_CALL" || parent.data("field")=="PROP_WEBINARS_COME"){
				initDatePicker();
			}
		} else if(item.data("type")=="textarea"){
			var text = item.text();
			if(parent.data("field")=="PROP_SALE" || parent.data("field")=="PROP_WEBINARS_COME"){
				var t = item.html().split("</span>");
				t[0] = (item.find(".date").text()).replace("(", "").replace(")", "");
				parent.data("date", t[0]);
				text = t[1];
			}
			item.html("<textarea>"+text+"</textarea>");
		} else if(item.data("type")=="select"){
			var html = "<select>",
				selected = "";
			$(parent.data("values")).each(function(){
				selected = "";
				if(this.TEXT==item.text()){
					selected = "selected";
				}
				html += '<option '+selected+' value="'+this.VALUE+'">'+this.TEXT+'</option>';
			});
			html += "</select>";
			
			item.html(html);
		}
		
		parent.find(".edit").removeClass("fa-pencil").addClass("fa-check").removeClass("edit").addClass("save");
		
		return false;
	});
	
	$('#ajax-info').on("click", ".editable .save", function(){
		var parent = $(this).closest(".editable"),
			item = parent.find(".val"),
			val = "";
			
		if(item.data("type")=="input"){
			val = item.find("input").val();
		} else if(item.data("type")=="textarea"){
			val = item.find("textarea").val();
		} else if(item.data("type")=="select"){
			val = item.find("select").val();
		}

		var setVal = val;
		if(parent.data("field")=="PROP_SALE" || parent.data("field")=="PROP_WEBINARS_COME"){
			setVal = parent.data("date")+"`"+val;
		}
		
		var params = {
			"id": parent.data("id"),
			"field": parent.data("field"),
			"val": setVal,
		};
		
		if(parent.data("enum_id")){
			params.enum_id = parent.data("enum_id");
		}
		
		$.post("/ajax/save_field.php", params);
		
		if(item.data("type")=="select"){
			val = item.find("select option[value="+val+"]").text();
		}
		
		if(params.field=="PROP_SOCIALS"){
			val = '<a href="'+val+'" class="link" target="_blank">'+val+'</a>';
		}
		
		if(parent.data("field")=="PROP_SALE" || parent.data("field")=="PROP_WEBINARS_COME"){
			val = '<span class="date">('+parent.data("date")+')</span>'+val;
		}
			
		item.html(val);
		
		parent.find(".save").removeClass("fa-check").addClass("fa-pencil").removeClass("save").addClass("edit");
		
		return false;
	});
	$('#ajax-info').on("click", ".editable .delete", function(){
		var parent = $(this).closest(".editable"),
			params = {
				"id": parent.data("id"),
				"field": parent.data("field"),
				"delete": true,
				"enum_id": parent.data("enum_id")
			};
			
		$.post("/ajax/save_field.php", params);

		parent.remove();
		return false;
	});
	
	$('#ajax-info').on("click", ".addable .add", function(){
		var copy = $(this).closest(".addable").clone(),
			parent = $(this).closest(".addable"),
			item = parent.find(".val"),
			wrapper = $(this).closest(".wraps");
		
		if(item.data("type")=="input"){
			item.html("<input type='text' value=''>");
		} else if(item.data("type")=="textarea"){
			item.html("<textarea class='form-control'></textarea>");
		}
		
		parent.find(".add").removeClass("fa-plus").addClass("fa-check").removeClass("add").addClass("save");
		if(wrapper.hasClass("reverse")){
			wrapper.prepend(copy);
		} else {
			wrapper.append(copy);
		}
		return false;
	});
	
	$('#ajax-info').on("click", ".addable .save", function(){
		var parent = $(this).closest(".addable"),
			item = parent.find(".val"),
			val = "";
			
		if(item.data("type")=="input"){
			val = item.find("input").val();
		} else if(item.data("type")=="textarea"){
			val = item.find("textarea").val();
		}
		
		if(parent.data("field")=="PROP_SALE" || parent.data("field")=="PROP_WEBINARS_COME"){
			val = moment().format('L')+"`"+val;
		}
		
		var params = {
			"id": parent.data("id"),
			"field": parent.data("field"),
			"val": val,
			"new_val": true
		};
		$.post("/ajax/save_field.php", params, function(ret){
			parent.data("enum_id", ret);
		});
		
		if(params.field=="PROP_SOCIALS"){
			val = '<a href="'+val+'" class="link" target="_blank">'+val+'</a>';
		} else if(parent.data("field")=="PROP_SALE" || parent.data("field")=="PROP_WEBINARS_COME"){
			val = val.split("`");
			val = '<span class="date">('+val[0]+')</span>'+val[1];
		}
		
		item.html(val);
		
		parent.find(".save").removeClass("fa-check").addClass("fa-pencil").removeClass("save").addClass("edit");
		parent.removeClass("addable").addClass("editable").find("a:last").append('<i class="delete fa fa-times"></i>');
		
		return false;
	});
	
	
	$("#complex_action").submit(function(){
		var val = $("#complex_action select").val(),
			text = $("#complex_action select option[value="+val+"]").text();
		
		if(!val){
			return false;
		}
		
		$("#complex_popup .modal-body").html($("#complex_action_html div[data-item="+val+"]").html());
		$("#complex_popup .modal-body").append("<input type='hidden' name='action' value='"+val+"'>");
		initDatePicker();

		$("#complex_popup .modal-title").text(text);
		$("#complex_popup").modal("show");

		return false;
	});
	
	$("#complex_popup form").submit(function(){
		var params = $(this).serialize();
		$("#complex_popup button[type=submit]").prop("disabled", true);
		$("#complex_popup .modal-body").html("Выполняется...");
	
		$.post("/ajax/complex.php", params, function(data){
			var mes = "<p style='color:green'>"+data.suc+"</p>";
			if(!data.suc){
				mes = "<p style='color:reg'>"+data.err+"</p>";
			}
			$("#complex_popup .modal-body").html(mes);
			$("#complex_popup button[type=submit]").prop("disabled", false);
		},"json");
		
		return false;
	});

    $('#ajax-info').on("click", ".fa-phone", function () {
    	$.post("/ajax/call.php", {"phone": $(this).closest(".val").text().trim()}, function (data) {
			if(data=="success"){
				alert("Ожидайте звонка");
			} else {
				alert("Произошла ошибка при звонке");
			}
        });
    });

    //селект2
	$(".select2_init").select2({
        width: "100%"
	});

    var timer_click;
	$("#dopExclude select").change(reloadTableForExclude);

    $('#dopExclude input').keypress(function(event){
		if(event.which == 8){
			event.preventDefault();
		} else {
			clearTimeout(timer_click);
			timer_click = setTimeout(reloadTableForExclude, 1000);
		}
    });

	function reloadTableForExclude(){
        Dtable.ajax.reload();
	}
});