<?php

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

define("CLOSE_PORTFOLIO", false); //Закрыть раздел портфелей

if ($_SERVER["SERVER_NAME"] == "fin-plan.org" || $_SERVER["SERVER_NAME"] == "crm.fin-plan.org" || $_SERVER["SERVER_NAME"] == "dev.fin-plan.org") {
	require $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
}


//Подключение логирования
require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/classes/common/CLogger.php";

//Подключение проверки дат на выходные дни по РФ и США
require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/weekend_days.php";

//Подключение класса со списком временных зон
require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/timeZone.php";

//Запись статистики авторизаций с сохранением IP адреса
require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/user_login_stat.php";

//Сброс управляемого кеша инфоблоков
require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/managed_cache_iblocks.php";

//Подключение кастомного свойства инфоблока "Привязка к элементу инфоблока с датой"
require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/classes/custom_properties/link_data_property.php";


AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('CCustomTypeElementDate', 'GetUserTypeDescription'));

//Преобразование первого символа строки в верхний регистр
function mb_ucfirst($text) {
    return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
}

//Возвращает протокол работы сервера
function getServerProtocol() {
	$protocol = strtolower(mb_substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? 'https' : 'http';
	if ($_SERVER["SERVER_PORT"] == 443) {
		$protocol = 'https';
	} elseif (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
		$protocol = 'https';
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
		$protocol = 'https';
	}

	return $protocol;
}

AddEventHandler('main', 'OnAdminIBlockElementEdit', function () {
	CModule::IncludeModule("eremin.finplantools");
	$tabset = new fptPortfolioTabset();
	return [
		'TABSET' => 'fpt_portfolio_actives', //'fpt_portfolio_actives_tab',
		'Check' => [$tabset, 'check'],
		'Action' => [$tabset, 'action'],
		'GetTabs' => [$tabset, 'getTabList'],
		'ShowTab' => [$tabset, 'showTabContent'],
	];
});

//ID модельных портфелей для контроля доступа без купленного радара, только по ГС
$APPLICATION->modelPortfolioRusId = array(491380, 513065);
$APPLICATION->modelPortfolioUsaId = array(505924);

//Определение базы котировок для США
$usaPricesHlId = \Bitrix\Main\Config\Option::get("grain.customsettings", "USA_PRICE_BASE");
$APPLICATION->usaPricesHlId = !empty($usaPricesHlId)?$usaPricesHlId:29;

//Номера шаблонов ТА для ссылок на ГС
$APPLICATION->TA_template_GS_RUS = 6;
$APPLICATION->TA_template_GS_for_action_pages = 4;
$APPLICATION->TA_template_GS_USA = 5;

//Тикеры акций для который при получении цен с СПБ нужно дублировать запись в таблицу полигона
$APPLICATION->crossTickers = array('TCS', 'BMW@DE','SPB@US','1COV@DE','ADS@DE','AFX@DE','ALV@DE','BAS@DE','BAYN@DE','BOSS@DE','BVB@DE','CON@DE','DB1@DE','DBK@DE','DHER@DE','DPW@DE','DTE@DE','DWNI@DE','EOAN@DE','EVK@DE','EVT@DE','FME@DE','FRE@DE','HEI@DE','HEN3@DE','HOT@DE','IFX@DE','LHA@DE','MRK@DE','MTX@DE','MUV2@DE','PUM@DE','RHM@DE','RWE@DE','SHL@DE','SIE@DE','VNA@DE','VOW3@DE','KAP@GS');

//Массив тикеров для динамического вывода через новое API над контентом страниц
$APPLICATION->headerTickers = array("SP500"=>"INDEX", "USD"=>"CURRENCY", "MSCI"=>"INDEX", "MOEX"=>"INDEX", "NASDAQ"=>"INDEX");

//Какие индексы вхождения показвать на страницах акций
$APPLICATION->showIndexForActions = array("SP500", "NASDAQ", "MOEX", "MSCI");

//Смещение дат для расчета дивидендов и купонов в портфеле
$APPLICATION->offsetTorgDates = array(
    'dividends' => 2,
    'coupons' => 1,
);

//Даты кварталов
$APPLICATION->quartDates = array(
    1 => "03-31",
    2 => "06-30",
    3 => "09-30",
    4 => "12-31",
);

//Показатели отраслей, которые требуют пересчета в процентном виде для показа у эмитентов в таблицах
$APPLICATION->sectorsPercentNames = array(
  "Выручка за год (скользящая)",
  "Прибыль за год (скользящая)",
  "Активы",
  "Собственный капитал",
  "Оборотные активы",
  "Прошлая капитализация",
);

//экслюзивные материалы в блоге справа
$APPLICATION->exclusiveMaterial = array(
	"vk" => "https://vk.com/fin_plan_org?w=app5748831_-81295080",
	"telegram" => "https://tlgg.ru/finplanorg",
);

//правый всплывающий баннер в статье, через сколько показывать (секунд), время жизни куки (дней)
$APPLICATION->flyRightBannerConf = array(
	"open" => 20,
	"life" => 7,
);

//Начальные значения для формы etf
$APPLICATION->etfDefaultForm = array(
	"ETF_AVG_ICNREASE" => 0, //Средний прирост
	"ETF_COMISSION" => 1.5, //Комиссия
);

//Начальные значения для формы облигаций
$APPLICATION->obligationDefaultForm = array(
	"time" => 24, //Выберите срок облигаций
	"rentab" => 10, //Выберите уровень доходности ценных бумаг

	"pe" => 10, //коэффициент р/е акции
	"profitability" => 20, //Доходность по консенсус-прогнозy
);

//Начальные значения для формы акций сша
$APPLICATION->actionUSADefaultForm = array(
	"pe" => 15, //коэффициент р/е акции
	"profitability" => 20, //Доходность по консенсус-прогнозy
);

//Страница калькулятора облигаций/акции
$APPLICATION->obligationMainShareData = array(
	"img" => "/local/templates/new/img/картинка.jpg", //Картинга для шаринга
	"title" => "Супер калькулятор", //Заголовок
	"url" => "/lk/obligations", //Ссылка
);

//Страница компании (акции/облигации)
$APPLICATION->companyPage = array(
	"Инвестиционный анализ" => array(
		"Справедливый Р/Е" => "6.6",
	),
	"Анализ денежных потоков" => array(
		"Ставка дисконтирования" => "10,0%",
		"Темп прироста чистого денежного потока через 5 лет" => "5,0%",
	),
);

//Личный кабинет ссылки Радара
$APPLICATION->lkRadar = array(
	"Продлить" => "https://fin-plan.org/buy_learning/informatsionno-analiticheskiy-servis-fin-plan-radar-usa-rf-dostup-na-12-mesyatsev-zhurnal/",
	"Купить" => "https://fin-plan.org/buy_learning/informatsionno-analiticheskiy-servis-fin-plan-radar-usa-rf-dostup-na-12-mesyatsev-zhurnal/",
);


//Промежуточные попапы на радаре
$APPLICATION->lkRadarPopups = array(
	"качество облигаций" => "<span class='icon icon-close'></span>Этот фильтр доступен только в платной подписке. Данный фильтр позволяет в один клик выбрать все хорошие облигации на рынке. Для создания этого фильтра эксперты компании фин-план ежедневно отбирают лучшие облигации вручную. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#buy_subscribe_popup'>Выбрать вариант подписки</a>",
	"финансовый анализ - акции" => "<span class='icon icon-close'></span>Выбирайте компании с <span class='strong'>наилучшими показателями деятельности</span>, чтобы быть уверенным в качестве своих  инвестиций. Эксперты Fin-plan регулярно отслеживают отчетность компаний, консолидируют ее и делают расчеты ключевых показателей. Этот фильтр доступен только в платной подписке. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#buy_subscribe_popup'>Выбрать вариант подписки</a>",
	"финансовый анализ - облигации" => "<span class='icon icon-close'></span>Выбирайте облигации с <span class='strong'>высокой доходностью</span> и одновременно устойчивым финансовым положением эмитента, отобранные экспертами компании фин-план, которые регулярно отслеживают отчетность компаний консолидируют ее и делают расчеты ключевых показателей. Этот фильтр доступен только в платной подписке. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#buy_subscribe_popup'>Выбрать вариант подписки</a>",
	"общая заглушка фильтра" => "<span class='icon icon-close'></span>Данный фильтр доступен только в платной подписке. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#buy_subscribe_popup'>Выбрать вариант подписки</a>",
	"заглушка портфеля" => "<span class='icon icon-close'></span>Управление портфелями доступно в платной подписке. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#buy_subscribe_popup'>Выбрать вариант подписки</a>",
	"аналитика - акции" => "<span class='icon icon-close'></span>Выбирайте облигации с <span class='strong'>высокой доходностью</span> и одновременно устойчивым финансовым положением эмитента, отобранные экспертами компании фин-план, которые регулярно отслеживают отчетность компаний консолидируют ее и делают расчеты ключевых показателей. Этот фильтр доступен только в платной подписке. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#buy_subscribe_popup'>Выбрать вариант подписки</a>",
	"поиск" => "<span class='icon icon-close'></span>Функционал поиска доступен только в платной версии. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#buy_subscribe_popup'>Выбрать вариант подписки</a>",
	"события фильтр портфелей" => "<span class='icon icon-close'></span>Функционал поиска по портфелям доступен только в платной версии. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#buy_subscribe_popup'>Выбрать вариант подписки</a>",
	"события фильтр избранное" => "<span class='icon icon-close'></span>Функционал поиска по избранным активам доступен только авторизованным пользователям. <br /><br /><a class='button' href='#' data-toggle='modal' data-target='#popup_login'>Войти или зарегистрироваться</a>",
);

//Убрать из радара или теханализа акции/облигации с режимом торгов
$APPLICATION->ExcludeFromRadar = array(
	"акции" => array(
		"SMAL",
		"TQIF",
		"TQTF",
		"TQQI",
		"TQTD",
		"TQTE",
	),
	"etf" => array(
		"SMAL",
		"TQIF",
		"TQBR",
		"TQQI",
		"TQDE",
	),
	"облигации" => array(

	),
	"теханализ" => array(
		"SMAL",
		"TQIF",
		"TQQI",
		"TQPI",
	)
);

//Аварийный список рабочих брокеров для переключения на стейдж сервер, остальные скрываются
$APPLICATION->brokersList = array('bcs', 'tinkoff', 'finam', 'vtb');

//минимальный день графика
$APPLICATION->minGraphDate = 1304208000; //(new DateTime("2011-05-01"))->getTimestamp();

function isMobile() {
    $useragent=$_SERVER['HTTP_USER_AGENT'];
    return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)
      || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
}

//Форматирование числа
function formatNumber($v, $dig=2) {
	//нужно для отображение прочерков при отрицательных значениях PEG на странице компаний
	if (strpos($v, '-') !== false && strlen($v) === 1) {
		return $v;
	}
	$format = number_format(floatval($v), $dig, '.', ' ');
	return str_replace('.00', '', $format);
}

$APPLICATION->currencies = array(
	   "" => array("sign"=>"", "name"=>""),
		"USD" => array("sign"=>"$", "name"=>"долл."),
		"EUR" => array("sign"=>"€", "name"=>"евро"),
		"SUR" => array("sign"=>"₽", "name"=>"руб."),
		"RUB" => array("sign"=>"₽", "name"=>"руб."),
		"RUR" => array("sign"=>"₽", "name"=>"руб."),
		"GBP" => array("sign"=>"£", "name"=>"фунт.ст."),
		"CHF" => array("sign"=>"₣", "name"=>"швейц.фр."),
		"CNY" => array("sign"=>"元", "name"=>"кит. юан."),
		"KRW" => array("sign"=>"₩", "name"=>"южнокор. вон"),
		"INR" => array("sign"=>"₹", "name"=>"инд. руп."),
		"JPY" => array("sign"=>"¥", "name"=>"яп. иен"),
		"KZT" => array("sign"=>"₸", "name"=>"тенге"),
		"BRL" => array("sign"=>"R$", "name"=>"браз. реал."),
		"TWD" => array("sign"=>"NT$", "name"=>"тайв. долл."),
		"ILS" => array("sign"=>"₪", "name"=>"изр. шейкель"),
);





//Изменение валюты
function convertOblCurrency($val) {
	$arData = array(
	   "" => "",
		"USD" => "$",
		"SUR" => " руб.",
		"RUB" => " руб.",
		"GBP" => "£",
		"CHF" => " швец. фр.",
		"CNY" => " кит. юан.",
		"KRW" => " южнокор. вон",
		"INR" => " инд. руп.",
		"JPY" => " япон. иен",
		"KZT" => " тенге",
		"BRL" => " браз. реал.",
		"TWD" => " тайв. долл.",

	);

	if ($arData[$val]) {
		$val = $arData[$val];
	}

	return $val;
}

//Возвращает знак валюты по коду
function getCurrencySign($curCode = '', $returnText = false) {
	global $APPLICATION;
	$arCurrency = $APPLICATION->currencies[$curCode];
	return $returnText ? $arCurrency["name"] : $arCurrency["sign"];
}

//Проверяет для страницы оплаты наличие опции "Упрощенная оплата" в инфоблоке 14 - "Скрытые семинары" для переклчения на упрощенный шаблон страницы
function checkSimplePay() {
	global $APPLICATION;
	$result = false;
	if (CSite::InDir("/buy_learning/")) {
		CModule::IncludeModule("iblock");
		$arFilter = Array("IBLOCK_ID" => 14, "CODE" => $_REQUEST["ELEMENT_CODE"], "PROPERTY_SIMPLE_PAY_VALUE" => "Y", "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, false);
		if ($ob = $res->GetNextElement()) {
			$result = true;
		}
		unset($arFilter, $res, $ob);
	}
	return $result;
}



AddEventHandler("main", "OnBeforeProlog", "Redirect404");
function Redirect404() {
	global $APPLICATION;
	if (!defined('ADMIN_SECTION') && defined('ERROR_404') && $APPLICATION->GetCurUri() != "/404.php") {
		$APPLICATION->RestartBuffer();
		CHTTP::SetStatus("404 Not Found");
		LocalRedirect("/404.php");
	}
}

//значение валюты с ЦБ,
//ф-ция обертка для обратной совместимости с классом валют CCurrency
function getCBPrice($code, $date = false) {

	//if ($code == "RUB") {
	if ($code == "RUB" || $code == "SUR" || empty($code)) {
		return 1;
	}

	$date2 = date("d/m/Y");
	if ($date!=false) {
		$date2 = $date;
	}

	$cache_time = 86400;
	$cache_id   = $code . "-" . $date2;
	$cache_path = '/valute';
	$obCache = new CPHPCache();
	if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) // Если кэш валиден
	{
		$vars         = $obCache->GetVars();
		$valute_price = $vars["valute_price"];
	} elseif ($obCache->StartDataCache()) // Если кэш невалиден
	{

//Оригинальное получение валюты, при некорректных значениях нового метода - раскомментировать
/*	   $data = simplexml_load_string(file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" . $date2));
		foreach ($data->Valute as $valute) {
			if ($valute->CharCode == $code) {
				$valute = (array) $valute;

				$valute_price = str_replace(",", ".", $valute["Value"]) / $valute["Nominal"];

				break;
			}
		}*/

		//В случае ошибки WSDL cbr.ru будет получен курс старым способом, при этом редкие валюты не могут быть получены.
     global $CACHE_MANAGER;
     $CACHE_MANAGER->StartTagCache($cache_path);  //Старт тегирования
		 $arCurrency = getCurrency_rate($code, $date2);

		if(array_key_exists("CURS", $arCurrency)){
			$valute_price = floatval($arCurrency["CURS"]);
		}
     $CACHE_MANAGER->RegisterTag("valute"); //Отметка тегом
     $CACHE_MANAGER->EndTagCache(); //Финализация тегирования

		$obCache->EndDataCache(array("valute_price" => $valute_price));
	}

	$valute_price = str_replace(",", ".", $valute_price);
	return $valute_price;
}

//Не используется, была пробная версия по кешированию
function getCBPrice_old($code, $date = false) {

	if ($code == "RUB") {
		return 1;
	}

	$date2 = date("d/m/Y");
	if ($date!=false) {
		$date2 = $date;
	}

	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cacheTtl = 86400;
	$cacheId   = $code . "-" . $date2;
	$cache_path = '/valute';
       //$cache->clean("valute");
	if ($cache->read($cacheTtl, $cacheId)) {
            $vars = $cache->get($cacheId);
				$valute_price = $vars["valute_price"];
        } else {
     global $CACHE_MANAGER;

		     $CACHE_MANAGER->StartTagCache($cache_path);  //Старт тегирования
				$arCurrency = getCurrency_rate($code, $date2);
				if(array_key_exists("CURS", $arCurrency)){
					$valute_price = floatval($arCurrency["CURS"]);
				}
		     $CACHE_MANAGER->RegisterTag("valute"); //Отметка тегом
		     $CACHE_MANAGER->EndTagCache(); //Финализация тегирования

             $cache->set($cacheId, array("valute_price" => $valute_price));
        }

	$valute_price = str_replace(",", ".", $valute_price);
	return $valute_price;

}


function getCurrency_rateSql($code, $date){
	$arResult = array("CURS"=>1);
	if(strpos($date,"/")!==false){
		$date = str_replace("/",".",$date);
	}
	 $date = (new DateTime($date))->format('Y-m-d');
	 Global $DB;
	 $sql = "SELECT * FROM `hl_currency_rates` WHERE `UF_CODE` = '$code' AND `UF_DATE`<='$date' ORDER BY `hl_currency_rates`.`UF_DATE` DESC LIMIT 1";
	 $res = $DB->Query($sql);
	 while($row = $res->fetch()){
		$arResult = array("NUM_CODE"=>$row["UF_CODE"], "NAME"=>$row["UF_NAME"], "NOM"=>$row["UF_NOMINAL"], "ORIG_CURS"=>$row["UF_RATE"], "CURS"=>$row["UF_RATE"], "DIR"=>"0", "CURRENCY"=>$row["UF_RATE_CURRENCY"]);
	 }
	unset($res);
   return $arResult;
}

function getCurrency_rate($code, $date){
        return getCurrency_rateSql($code, $date2);
       // return getCurrency_rate1($code, $date2);
}

function getCurrency_rate1($code, $date){
	$arResult = array();
	if(strpos($date,"/")!==false){$date = str_replace("/",".",$date);}
	 $date = (new DateTime($date))->format('d.m.Y');
	$cbr = new CCurrency($date);
	$arResult = $cbr->getRate($code);

	unset($cbr);
   return $arResult;
}


//Возвращает список открытых всем вебинаров
//Настроен сброс кеша при интерактивном изменении/создании элементов инфоблока
function getAllOpenedWebinars() {
	$arWebinars = array();
	$cache      = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cacheId    = "getAllOpenedWebinars";
	$cacheTtl   = 86400 * 365;
	if ($cache->read($cacheTtl, $cacheId)) {
		$arWebinars = $cache->get($cacheId);
	} else {

		CModule::IncludeModule("iblock");
		$arFilter = Array("IBLOCK_ID" => 9, "PROPERTY_OPEN_ALL_REGISTERED_VALUE" => "Y", "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array("PROPERTY_ITEM_END" => "desc"), $arFilter, false, false, array("ID", "IBLOCK_ID", "NAME"));
		while ($row = $res->Fetch()) {
			$arWebinars[$row["ID"]] = $row["NAME"];
		}
		$cache->set($cacheId, $arWebinars);
	}

	return $arWebinars;
}

//проверка купил ли юзер вебинар
function checkPaySeminar($seminar, $clearCache = false, $UserId = false) {
	CModule::IncludeModule("iblock");
	global $USER;
	if (!$USER->IsAuthorized()) {
		return false;
	} else {
				$start = microtime(true);
		//Получаем кешированный список открытых для всех вебинаров

		$arOpenAllWebinar = getAllOpenedWebinars();

		if (count($arOpenAllWebinar) > 0) {
			if (array_key_exists($seminar, $arOpenAllWebinar)) {
				return true;
			}
		}

		//Если разрешено кеширование проверки доступа к семинарам
		if (\Bitrix\Main\Config\Option::get("grain.customsettings", "CHECK_PAY_SEMINAR_CACHE_ON") == "Y") {
			  //return checkPaySeminar_cached($seminar, $clearCache = false, $UserId = false);
			  return checkPaySeminar_cached($seminar, $clearCache, $UserId);
		} else {  //Если не разрешено кеширование проверки доступа к семинарам используем старый способ проверки
	  CModule::IncludeModule("iblock");
		//со сроком действия
		$UserId  = $UserId != false ? intval($UserId) : $USER->GetID();
		$arFilter2 = Array("IBLOCK_ID" => 11, "PROPERTY_ITEM" => $seminar, "PROPERTY_USER" => $UserId, "!PROPERTY_PAYED" => false, "!PROPERTY_ITEM_END" => false, ">=PROPERTY_ITEM_END" => date("Y-m-d") . " 00:00:00", "ACTIVE" => "Y");
		$res2      = CIBlockElement::GetList(Array("PROPERTY_ITEM_END" => "desc"), $arFilter2, false, Array("nTopCount" => 1), array("PROPERTY_ITEM_END", "ID"));

		//пожизнено
		$arFilter = Array("IBLOCK_ID" => 11, "PROPERTY_ITEM" => $seminar, "PROPERTY_USER" => $UserId, "!PROPERTY_PAYED" => false, "PROPERTY_ITEM_END" => false, "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1));

		if ($res2->SelectedRowsCount()) {
			unset($res2);
			return true;
		} elseif ($res->SelectedRowsCount()) {
			unset($res);
			return true;
		} else {
			$UserId  = $UserId != false ? intval($UserId) : $USER->GetID();
			$arFilter = Array("IBLOCK_ID" => 11, "PROPERTY_DOP_ITEMS" => $seminar, "PROPERTY_USER" => $UserId, "!PROPERTY_PAYED" => false, "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1));
			if ($ob = $res->GetNextElement()) {
				unset($res);
				return true;
			} else {
				unset($res);
				return false;
			}
		}


}
	 				$end = microtime(true);
		$d = $end-$start;
		CLogger::timing($d);
}
}

//Удаление папки с кешем проверки доступа семинаров для конкретного пользователя.
//Если пользователь не передан - будет очищен весь кеш проверок доступа к семинарам для всех пользователей.
function clearCachePaySeminar($UserId = false, $dir=''){
	if(empty($dir))
	  $dir = $_SERVER["DOCUMENT_ROOT"].'/bitrix/cache/checkPaySeminar_cached/' . ($UserId!=false?$UserId:'');
	    $files = array_diff(scandir($dir), ['.','..']);
    foreach ($files as $file) {
        (is_dir($dir.'/'.$file)) ? clearCachePaySeminar($UserId, $dir.'/'.$file) : unlink($dir.'/'.$file);
    }
    return rmdir($dir);
}

//кешированная проверка купил ли юзер вебинар
function checkPaySeminar_cached($seminar, $clearCache = false, $UserId = false) {
	CModule::IncludeModule("iblock");
	global $USER;

	$UserId   = !$UserId == false ? intval($UserId) : $USER->GetID();
	$cacheId  = "checkPaySeminar_" . $seminar . '_' . $UserId;
	$cache_dir = '/checkPaySeminar_cached/' . $UserId;
 	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();


	if ($clearCache == true) { //Принудительно сбрасываем кеш при изменении инфоблоков в админке (через события) или при покупке радара.

		$cache->clean($cacheId);
	}

	$cacheTtl = 86400; //Половина суток

	$defaultCheckResult = false;

	$cache_id = md5(serialize($arParams));


	 $obCache = new CPHPCache;
	 if($obCache->InitCache($cacheTtl, $cacheId, $cache_dir))
	 {
	     $defaultCheckResult = $obCache->GetVars();
	 }
	 elseif(CModule::IncludeModule("iblock") && $obCache->StartDataCache())
	 {
		//проверяем и кешируем
		$arNewFilter = array("IBLOCK_ID" => 11, "PROPERTY_USER" => $UserId, "!PROPERTY_PAYED" => false, "ACTIVE" => "Y");

		$arNewFilter[] = array("LOGIC" => "OR",
			array("LOGIC" => "AND", array("PROPERTY_ITEM" => $seminar, "!PROPERTY_ITEM_END" => false, ">=PROPERTY_ITEM_END" => date("Y-m-d") . " 00:00:00")),
			array("LOGIC" => "AND", array("PROPERTY_ITEM" => $seminar, "PROPERTY_ITEM_END" => false)),
			array("LOGIC" => "AND", array("PROPERTY_DOP_ITEMS" => $seminar)),
		);
		$resX = CIBlockElement::GetList(Array(), $arNewFilter, false, false);
		if ($resX->SelectedRowsCount()) {
			$defaultCheckResult = true;
		} else {
			$defaultCheckResult = false;
		}

	     $obCache->EndDataCache($defaultCheckResult);
	 }
	 else
	 {
	     $arResult = array();
	 }


	return $defaultCheckResult;
}

//дата окончания оплаты семинара
function checkPayEndDateSeminar($seminar) {
	CModule::IncludeModule("iblock");
	global $USER;
	if (!$USER->IsAuthorized()) {
		return false;
	}

	//со сроком действия
	$arFilter2 = Array("IBLOCK_ID" => 11, "PROPERTY_ITEM" => $seminar, "PROPERTY_USER" => $USER->GetID(), "!PROPERTY_PAYED" => false, "!PROPERTY_ITEM_END" => false, ">=PROPERTY_ITEM_END" => date("Y-m-d") . " 00:00:00", "ACTIVE" => "Y");
	$res2      = CIBlockElement::GetList(Array("PROPERTY_ITEM_END" => "desc"), $arFilter2, false, Array("nPageSize" => 1), array("PROPERTY_ITEM_END", "ID"));

	if ($item = $res2->GetNext()) {
		return $item["PROPERTY_ITEM_END_VALUE"];
	}
}

//проверка есть ли доступ к купленному семинару
function checkAccessSeminar($seminar) {
	global $USER;
	CModule::IncludeModule('highloadblock');

	$hlbl              = 11;
	$hlblock           = HL\HighloadBlockTable::getById($hlbl)->fetch();
	$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $entity->getDataClass();

	$main_query = new Entity\Query($entity);
	$main_query->setOrder(array("ID" => "desc"));
	$main_query->setFilter(array("UF_USER" => $USER->GetID(), "UF_SEMINAR" => $seminar));
	$exec = $main_query->exec();
	$exec = new CDBResult($exec);
	if (!$item = $exec->Fetch()) {
		return false;
	} else {
		return true;
	}
}

//проверка есть ли доступ к радару
function checkPayRadar($clearCache = false, $UserId = false) {
	//CModule::IncludeModule("iblock");

	global $USER;
	if (!$USER->IsAuthorized()) {
		return false;
	} else {
		//CHECK_PAY_RADAR_CACHE_ON
		//CHECK_PAY_SEMINAR_CACHE_ON
		if (\Bitrix\Main\Config\Option::get("grain.customsettings", "CHECK_PAY_RADAR_CACHE_ON") == "Y") { //Если разрешено кеширование проверки доступа к радару
			return checkPayRadar_cached($clearCache, $UserId);
		} else {  //Если не разрешено кеширование проверки доступа к радару используем запрос по старой схеме
			CModule::IncludeModule('iblock');
			$UserId  = $UserId != false ? intval($UserId) : $USER->GetID();
			$arFilter = Array("IBLOCK_ID" => 11, "!PROPERTY_RADAR_END" => false, ">=PROPERTY_RADAR_END" => date("Y-m-d"), "PROPERTY_USER" => $UserId, "!PROPERTY_PAYED" => false, "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1));
			if ($res->SelectedRowsCount()) {
				return true;
			} else {
				return false;
			}
		}
	}
}

//проверка есть ли доступ к акциям США в радаре
function checkPayUSARadar($clearCache = false, $UserId = false) {
	//CModule::IncludeModule("iblock");

	global $USER;
	if (!$USER->IsAuthorized()) {
		return false;
	} else { //Открыть при запуске кеширования проверки доступа к радару
		/*       return checkPayRadar_cached($clearCache, $UserId);
		}*/
		//Если брасывается кеш проверки доступности радара - делаем сброс кеша проверки
		if($clearCache==true)
		  checkPayRadar_cached($clearCache, $UserId);

		CModule::IncludeModule('iblock');
		$UserId  = $UserId != false ? intval($UserId) : $USER->GetID();
		$arFilter = Array("IBLOCK_ID" => 11, "!PROPERTY_ACCESS_USA" => false, "=PROPERTY_ACCESS_USA_VALUE" => 'Y', "!PROPERTY_RADAR_END" => false, ">=PROPERTY_RADAR_END" => date("Y-m-d"), "PROPERTY_USER" => $UserId, "!PROPERTY_PAYED" => false, "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1));
		if ($res->SelectedRowsCount()) {
			return true;
		} else {
			return false;
		}
	}
}

//проверка есть ли доступ к ГС США
function checkPayGSUSA($seminar, $UserId = false) {
	//CModule::IncludeModule("iblock");

	global $USER;
	if (!$USER->IsAuthorized()) {
		return false;
	} else { //Открыть при запуске кеширования проверки доступа к радару
		/*       return checkPayRadar_cached($clearCache, $UserId);
		}*/
		CModule::IncludeModule('iblock');
		$UserId  = $UserId != false ? intval($UserId) : $USER->GetID();
		$arFilter = Array("IBLOCK_ID" => 11, "=PROPERTY_ITEM" => $seminar, "!PROPERTY_ACCESS_GS_USA" => false, "=PROPERTY_ACCESS_GS_USA_VALUE" => 'Y', "!PROPERTY_ITEM_END" => false, ">=PROPERTY_ITEM_END" => date("Y-m-d"), "PROPERTY_USER" => $UserId, "!PROPERTY_PAYED" => false, "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1));
		if ($res->SelectedRowsCount()) {
			return true;
		} else {
			return false;
		}
	}
}

/**
 * Очищает кеш проверки доступности радара рф и сша. Вызывается после оплаты при возвращении на сайт
 *
 * @param  [add type]   $userId [add description]
 *
 * @return [add type]  [add description]
 */
function cleanCheckRadarsCache($userId){
	$cacheId = "checkPayRadar_" . $userId;
	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cache->clean($cacheId);
	$result = checkPayRadar_cached(true, $userId);
	$result = checkPayRadar_cached(false, $userId);
	CLogger::cleanCheckRadarsCache("user=".$userId." check=".$result?"Y":"N");
}

/**
 * кешированная проверка есть ли доступ к радару
 * Кеширует проверку доступа к радару на полсуток. Должен быть очищен при успешной покупке/продлении радара
 *
 * @param  $clearCache   boolean принудительно очистить кеш и перекешировать проверку
 * @param  $UserId       mixed если передан id пользователя - то будет работа с кешем именно этого пользователя, если не передан параметр или false - ,будет работа с кешем текущего пользователя
 *
 * @return [add type]
 */
function checkPayRadar_cached($clearCache = false, $UserId = false) {
	CModule::IncludeModule("iblock"); //Убрать из всех функций, перенести в шапку файла как общее подключение модуля.
	global $USER;
	if (!$USER->IsAuthorized()) {
		return false;
	}

	$UserId  = $UserId != false ? intval($UserId) : $USER->GetID();
	$cacheId = "checkPayRadar_" . $UserId;

	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

	if ($clearCache == true) { //Принудительно сбрасываем кеш при изменении инфоблоков в админке (через события) или при покупке радара.
		$cache->clean($cacheId);
	}



	$cacheTtl = 43200; //Половина суток

	$defaultCheckResult = false;
	if ($cache->read($cacheTtl, $cacheId)) {
		$defaultCheckResult = $cache->get($cacheId);
	} else {
		$arFilter = Array("IBLOCK_ID" => 11, "!PROPERTY_RADAR_END" => false, ">=PROPERTY_RADAR_END" => date("Y-m-d"), "PROPERTY_USER" => $UserId, "!PROPERTY_PAYED" => false, "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1));
		if ($res->SelectedRowsCount()) {
			$defaultCheckResult = true;
		} else {
			$defaultCheckResult = false;
		}

		$cache->set($cacheId, $defaultCheckResult);
	}

	return $defaultCheckResult;
}

//дата окончания оплаты радара
function checkPayEndDateRadar() {
	CModule::IncludeModule("iblock");
	global $USER;
	if (!$USER->IsAuthorized()) {
		return false;
	}

	//со сроком действия
	$arFilter2 = Array("IBLOCK_ID" => 11, "PROPERTY_USER" => $USER->GetID(), "!PROPERTY_PAYED" => false, "!PROPERTY_RADAR_END" => false, ">=PROPERTY_RADAR_END" => date("Y-m-d") . " 00:00:00", "ACTIVE" => "Y");
	$res2      = CIBlockElement::GetList(Array("PROPERTY_RADAR_END" => "desc"), $arFilter2, false, Array("nPageSize" => 1), array("PROPERTY_RADAR_END", "ID"));

	if ($item = $res2->GetNext()) {
		return $item["PROPERTY_RADAR_END_VALUE"];
	}
}

function checkBonusSeminar($seminar) {
	CModule::IncludeModule("iblock");
	global $USER;
	if (!$USER->IsAuthorized()) {
		return false;
	} else {
		CModule::IncludeModule("iblock");
		$arFilter = Array("IBLOCK_ID" => 11, "PROPERTY_ITEM" => $seminar, "PROPERTY_USER" => $USER->GetID(), "!PROPERTY_PAYED" => false, "!PROPERTY_BONUS" => false, "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1));
		if ($ob = $res->GetNextElement()) {
			return true;
		} else {
			return false;
		}
	}
}

function generateWebSertificate($seminar, $file) {
	global $USER;

	\Bitrix\Main\Loader::includeModule("highloadblock");
	$hlbl              = 13;
	$hlblock           = HL\HighloadBlockTable::getById($hlbl)->fetch();
	$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $entity->getDataClass();

	$main_query = new Entity\Query($entity);
	$main_query->setOrder(array("ID" => "desc"));
	$main_query->setFilter(array("UF_USER" => $USER->GetID(), "UF_ITEM" => $seminar));
	$exec = $main_query->exec();
	$exec = new CDBResult($exec);
	if (!$exec->SelectedRowsCount()) {
		$img = $_SERVER["DOCUMENT_ROOT"] . $file;

		$newImage = imagecreatefromjpeg($img);

		$grey = imagecolorallocate($newImage, 255, 255, 255);
		$font = $_SERVER["DOCUMENT_ROOT"] . "/upload/websertificate/font.ttf";

		$user = CUser::GetByID($USER->GetID())->Fetch();
		$name = explode(" ", $user["NAME"]);

		if ($name[0]) {
			imagettftext($newImage, 30, 0, 183, 135, $grey, $font, $name[0]);
		}
		if ($name[1]) {
			imagettftext($newImage, 30, 0, 183, 172, $grey, $font, $name[1]);
		}

		$tmpfname = tempnam($_SERVER["DOCUMENT_ROOT"] . "/upload/tmp", "");

		imagejpeg($newImage, $tmpfname);

		$data         = CFile::MakeFileArray($tmpfname);
		$data["name"] = "sert.jpg";

		$entity_data_class::add(array(
			"UF_ITEM" => $seminar,
			"UF_USER" => $USER->GetID(),
			"UF_FILE" => $data,
		));
	}
}


//Добавление юзера из админки вручную
AddEventHandler("main", "OnAfterUserAdd", "OnAfterUserAddHandler");
function OnAfterUserAddHandler(&$arFields) {

	$rsUser = CUser::GetByID($arFields["ID"]);
	$arUser = $rsUser->Fetch();
    $siteUrl = str_replace("/study","",$_SERVER["DOCUMENT_ROOT"]);
    require_once $siteUrl . "/api/mailchimp/MailChimp.php";
	 //CLogger::Mailchimp_subscribe(print_r(array("_REQUEST onAfterUserAdd init "=>$_REQUEST, "request uri "=>$_SERVER["REQUEST_URI"]), true));
	if ($_SERVER["REQUEST_URI"] == "/learning/get_pay_form.php") {
		$phone = $_REQUEST["phone"];
		$name  = $_REQUEST["fio"];
		$inter = [
			"987abfa295" => true
		];
		$tmp = [
			'email_address' => $arFields["LOGIN"],
			'status' => 'subscribed',
			"merge_fields" => [
				"EMAIL" => $arUser["LOGIN"],
				"FNAME" => $name,
				"LNAME" => "",
				"PHONE" => $phone
			],
			"interests" => $inter
		];
		$list_id = "b6050240b6";
	} elseif ($_SERVER["REQUEST_URI"] == "/local/templates/new/ajax/check.php") {
		$phone = $_REQUEST["USER_PHONE"];
		$name  = $_REQUEST["USER_NAME"];
		$inter = [
			"117a25fb4b" => true
		];
		$tmp = [
			'email_address' => $arFields["LOGIN"],
			'status' => 'subscribed',
			"merge_fields" => [
				"EMAIL" => $arUser["LOGIN"],
				"FNAME" => $name,
				"LNAME" => "",
				"PHONE" => $phone
			],
			"interests" => $inter
		];
		$list_id = "b6050240b6";
	} else if($_REQUEST["tilda"]==true || isset($_REQUEST["mcinterest"]) || !empty($_REQUEST["source_page"])){ //Для добавленных с тильды не подписываем по умолчанию, подписка делается позднее в файле хука tilda.php
		$tmp = array();
	}else {
		$name  = $arUser["NAME"];
		$inter = [
			"c27112d78f" => true
		];

		$tmp = [
			'email_address' => $arFields["LOGIN"],
			'status' => 'subscribed',
			"merge_fields" => [
				"EMAIL" => $arUser["LOGIN"],
				"FNAME" => $name,
				"LNAME" => "",
			],
			"interests" => $inter
		];
		$list_id = 'b6050240b6';

		//Добавление пользователя в CRM вручную при добавлении в админке битрикс:
		//$CCrmAdder = new CrmAdder;
		//$CCrmAdder->addSimple(19, false, 'Вручную', $arUser["ID"]);
	}

			//Обработаем подарок при регистрации, если он включен (обработка внутри метода класса)
			PromoGenerator::addPersonal($arFields["ID"]);
			$CBonusGift = new CBonusGift();
			$CBonusGift->setGift($arFields["ID"], 'setGiftAfterUserAdd');

	if(count($tmp)>0){
		//Очищаем существующие группы для указанного адреса почты, в ответ получаем true|false для определения наличия данной почты в мч

		$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');
		$existEmail = $MailChimp->clearGroups($arUser["LOGIN"], $list_id, 10);

		if($existEmail){
			$subscriber_hash = MailChimp::subscriberHash($arUser["LOGIN"]);
			$result = $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
							'interests'    => $tmp['interests'],
						]);
		} else {
			$result    = $MailChimp->post("lists/$list_id/members", $tmp);
		}
	}
}

/**
 * Проверка элемента на избранное
 * @param $id
 * @param $type
 * @return bool
 */
function checkInFavorite($id, $type) {
	global $USER;
	CModule::IncludeModule("highloadblock");

	$hlblock = HL\HighloadBlockTable::getById(12)->fetch();
	$entity  = HL\HighloadBlockTable::compileEntity($hlblock);

	$main_query = new Entity\Query($entity);
	$main_query->setOrder(array("ID" => "desc"));
	$main_query->setFilter(array("UF_USER" => $USER->GetID(), "UF_ITEM" => $id, "UF_TYPE" => $type));
	$exec = $main_query->exec();
	$exec = new CDBResult($exec);
	if ($item = $exec->Fetch()) {
		return true;
	} else {
		return false;
	}
}

/**
 * Получение списка избранного
 * @return array
 */
function getFavoriteList($userId = '') {
	$types = array(
		"action" => 33,
		"etf" => 33,
		"action_usa" => 56,
		"obligation" => 30,
		"usa_company" => 44,
		"obligation_company" => 29,
		"blog" => 5,
	);

	global $USER;
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("highloadblock");

	$hlblock           = HL\HighloadBlockTable::getById(12)->fetch();
	$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $entity->getDataClass();

	$arReturn = array();

	$changeSelectProps = array("PROPERTY_LASTPRICE", "PROPERTY_LEGALCLOSE", "PROPERTY_LASTCHANGEPRCNT", "PROPERTY_CURRENCY", "PROPERTY_ETF_CURRENCY");
	 $uid = intval($userId)>0?intval($userId):$USER->GetID();
	foreach ($types as $type => $ibID) {
		$main_query = new Entity\Query($entity);
		$main_query->setOrder(array("ID" => "desc"));
		$main_query->setSelect(array("UF_ITEM", "ID"));
		$main_query->setFilter(array("UF_USER" => $uid, "UF_TYPE" => $type));
		$exec = $main_query->exec();
		$exec = new CDBResult($exec);
		while ($item = $exec->Fetch()) {
			$arFilter = Array("IBLOCK_ID" => $ibID, "ID" => $item["UF_ITEM"]);
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, array("CODE", "NAME", "DETAIL_PAGE_URL"));
			if ($item2 = $res->GetNext()) {
				if (in_array($type, array("action", "action_usa", "etf", "obligation"))) {
					if ($type == "action" || $type == "etf") {
						$ib = 32;
					} else if ($type == "action_usa") {
						$ib = 55;
					} else {
						$ib = 27;
					}

					$arFilter2      = Array("IBLOCK_ID" => $ib, "CODE" => $item2["CODE"]);
					$res2           = CIBlockElement::GetList(Array(), $arFilter2, false, false, $changeSelectProps);
					$item2["PROPS"] = $res2->GetNext();

					if (in_array($ib, array(32, 55))) { //Определяем валюту и курс
						$currencyRate = 1;
						if ($type == 'etf' && ($item2["PROPS"]["PROPERTY_ETF_CURRENCY_VALUE"] != 'RUB' || $item2["PROPS"]["PROPERTY_ETF_CURRENCY_VALUE"] != 'SUR')) {
							$currencyRate                                = getCBPrice($item2["PROPS"]["PROPERTY_ETF_CURRENCY_VALUE"]);
							$item2["CURRENCY_RATE"]                      = $currencyRate;
							$item2["PROPS"]["PROPERTY_LASTPRICE_VALUE"]  = round(floatVal($item2["PROPS"]["PROPERTY_LASTPRICE_VALUE"]) * $currencyRate, 2);
							$item2["PROPS"]["PROPERTY_LEGALCLOSE_VALUE"] = round(floatVal($item2["PROPS"]["PROPERTY_LEGALCLOSE_VALUE"]) * $currencyRate, 2);
						}
						if ($type == 'action_usa' && ($item2["PROPS"]["PROPERTY_ETF_CURRENCY_VALUE"] != 'RUB' || $item2["PROPS"]["PROPERTY_ETF_CURRENCY_VALUE"] != 'SUR')) {
							$currencyRate                               = getCBPrice($item2["PROPS"]["PROPERTY_CURRENCY_VALUE"]);
							$item2["CURRENCY_RATE"]                     = $currencyRate;
							$item2["PROPS"]["PROPERTY_LASTPRICE_VALUE"] = round(floatVal($item2["PROPS"]["PROPERTY_LASTPRICE_VALUE"]) * $currencyRate, 2);
							// $item2["PROPS"]["PROPERTY_LEGALCLOSE_VALUE"] = round(floatVal($item2["PROPS"]["PROPERTY_LEGALCLOSE_VALUE"])*$currencyRate,2);
						}
					}

					if (floatval($item2["PROPS"]["PROPERTY_LASTPRICE_VALUE"]) <= 0 && floatval($item2["PROPS"]["PROPERTY_LEGALCLOSE_VALUE"]) > 0) {
						$item2["PROPS"]["PROPERTY_LASTPRICE_VALUE"] = $item2["PROPS"]["PROPERTY_LEGALCLOSE_VALUE"];
					}
				}

				$arReturn[$type][] = $item2;
			} else {
				$entity_data_class::Delete($item["ID"]);
			}
		}
	}

	return $arReturn;
}

/**
 * Названия для списка избранного
 * @return array
 */
function getFavoriteNames() {
	return [
		"blog" => "Статьи",
		"obligation" => "Облигации",
		"obligation_company" => "Эмитенты",
		"usa_company" => "Эмитенты США",
		"etf" => "ETF",
		"action" => "Акции",
		"action_usa" => "Акции США",
	];
}

AddEventHandler('main', 'OnEpilog', 'onEpilog', 1);
function onEpilog() {
	global $APPLICATION;
	$arPageProp = $APPLICATION->GetPagePropertyList();
	if ($arPageProp["OG:IMAGE"]) {
		$arPageProp["OG:IMAGE"] = "https://" . $_SERVER["SERVER_NAME"] . $arPageProp["OG:IMAGE"];
	}
	if (!$arPageProp["OG:TITLE"] && $arPageProp["TITLE"]) {
		$arPageProp["OG:TITLE"] = $arPageProp["TITLE"];
	}

	if ($arPageProp["OG:TITLE"]) {
		$arPageProp["OG:TITLE"] = str_replace('"', "'", htmlspecialchars_decode($arPageProp["OG:TITLE"]));
	}
	if ($arPageProp["OG:DESCRIPTION"]) {
		$arPageProp["OG:DESCRIPTION"] = str_replace('"', "'", htmlspecialchars_decode($arPageProp["OG:DESCRIPTION"]));
	}

	$arPageProp["OG:TYPE"] = "website";
	$arPageProp["OG:URL"]  = "https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];

	$arMetaPropName = array('og:title', 'og:description', 'og:url', 'og:image', "og:type");
	foreach ($arMetaPropName as $name) {
		//$key=mb_strtoupper($name,'UTF-8');
		$key = mb_strtoupper($name);
		if (isset($arPageProp[$key])) {
			//$APPLICATION->AddHeadString('<meta property="'.$name.'" content="'.htmlspecialchars($arPageProp[$key]).'">',$bUnique=true);
			$APPLICATION->AddHeadString('<meta property="' . $name . '" content="' . $arPageProp[$key] . '">', $bUnique = true);
		}
	}
}

AddEventHandler("main", "OnAfterUserUpdate", "OnAfterUserUpdateHandler");
function OnAfterUserUpdateHandler(&$arFields) {
	//удалить все сертификаты
	\Bitrix\Main\Loader::includeModule("highloadblock");
	$hlbl              = 13;
	$hlblock           = HL\HighloadBlockTable::getById($hlbl)->fetch();
	$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $entity->getDataClass();

	$main_query = new Entity\Query($entity);
	$main_query->setOrder(array("ID" => "desc"));
	$main_query->setSelect(array("ID"));
	$main_query->setFilter(array("UF_USER" => $arFields["ID"]));
	$exec = $main_query->exec();
	$exec = new CDBResult($exec);
	if ($exec->SelectedRowsCount()) {
		while ($item = $exec->GetNext()) {
			$entity_data_class::delete($item["ID"]);
		}
	}
}

//Устанавливает имена из оплат
function setNamesFromPays() {
	CModule::IncludeModule("iblock");
	$arUsers = array();

	$arFilter = Array("IBLOCK_ID" => 11);
	$res      = CIBlockElement::GetList(Array("ID" => "desc"), $arFilter, false, false);
	while ($ob = $res->GetNextElement()) {
		$item          = $ob->GetFields();
		$item["PROPS"] = $ob->GetProperties();

		if ($arUsers[$item["PROPS"]["USER"]["VALUE"]]) {
			continue;
		}

		$arUsers[$item["PROPS"]["USER"]["VALUE"]] = $item["PROPS"]["FIO"]["VALUE"];
	}

	foreach ($arUsers as $id => $name) {
		$user = new CUser;
		$user->Update($id, array(
			"NAME" => $name
		));
	}
}

//подгрузка классов
CModule::AddAutoloadClasses(
	'',
	array(
		'BaseHelper' => '/local/php_interface/classes/BaseHelper.php',

		'BaseBonuses' => '/local/php_interface/classes/promocodes/BaseBonuses.php',
		'Bonuses' => '/local/php_interface/classes/promocodes/Bonuses.php',
		'CBonusGift' => '/local/php_interface/classes/promocodes/BonusGift.php',
        'CUsersStat' =>'/local/php_interface/classes/common/CUsersStat.php',
		
		'BasePromo' => '/local/php_interface/classes/promocodes/BasePromo.php',
		'PromoGenerator' => '/local/php_interface/classes/promocodes/PromoGenerator.php',
		'Promocodes' => '/local/php_interface/classes/promocodes/Promocodes.php',

		"CCurrency" => "/local/php_interface/classes/radar/currency.php", //Работа с валютами, курсами и т.д.
		"RadarBase" => "/local/php_interface/classes/radar/radar_base.php",
		"CFundsUsa" => "/local/php_interface/classes/radar/fundsUsa.php", //Работа с фондами США
		"RadarPresets" => "/local/php_interface/classes/radar/radar_filter_presets.php", //Работа с настройками шаблонов для фильтров радара
		"MoexApi" => "/local/php_interface/classes/radar/moex_api.php", //Класс с инструментами работы с биржей
		"Actions" => "/local/php_interface/classes/radar/actions.php",
		"Obligations" => "/local/php_interface/classes/radar/obligations.php",
		"CCurrencyRadar" => "/local/php_interface/classes/radar/currency_radar.php", //Класс с валютами как активами радара
		"MoexSync" => "/local/php_interface/classes/radar/moex_sync.php", //Синхрронизация акций и облигаций с московской биржей, обновление цен акций
		"MoexGraph" => "/local/php_interface/classes/radar/moex_graph.php",
		"SpbexGraph" => "/local/php_interface/classes/radar/spbex_graph.php",
		"ActionsUsa" => "/local/php_interface/classes/radar/actions_usa.php",
		"SectorsUsa" => "/local/php_interface/classes/radar/sectors_usa.php", //Секторы США
		"CIndustriesRus" => "/local/php_interface/classes/radar/industries_rus.php", //Отрасли РФ
		"ETF" => "/local/php_interface/classes/radar/etf.php",
		"CFutures" => "/local/php_interface/classes/radar/futures.php",  //Фьючерсы
		"CGs" => "/local/php_interface/classes/radar/gs.php",  //Класс для работы с аналитикой ГС

		//"MailChimp" => "/api/mailchimp/MailChimp.php",
		"DataTable" => "/local/php_interface/classes/crm/DataTable.php",
		"DataTableFix" => "/local/php_interface/classes/crm/DataTableFix.php",
		"Card" => "/local/php_interface/classes/crm/Card.php",
		"CardFix" => "/local/php_interface/classes/crm/CardFix.php",
		"CrmAdder" => "/local/php_interface/classes/crm/CrmAdder.php",
		"CrmDataBase" => "/local/php_interface/classes/crm/CrmDataBase.php",
		"CrmComplex" => "/local/php_interface/classes/crm/CrmComplex.php",
		"CrmAccess" => "/local/php_interface/classes/crm/CrmAccess.php",
		"CCrmLeeLoo" => "/local/php_interface/classes/crm/CCrmLeeLoo.php",
		"CSmartSender" => "/local/php_interface/classes/crm/CSmartSender.php",
		"CSaleBot" => "/local/php_interface/classes/crm/CSaleBot.php",

		"RadarPlusAccess" => "/local/php_interface/classes/radar_plus/RadarPlusAccess.php", //data
		"RadarPlusActions" => "/local/php_interface/classes/radar_plus/RadarPlusActions.php", //data
		"RadarPlusTools" => "/local/php_interface/classes/radar_plus/RadarPlusTools.php", //data

		"RadarBaseMarketMap" => "/local/php_interface/classes/radar_marketmap/radar_base_mm.php", //Наследованный класс RadarBase для карты рынка
		"ActionsMM" => "/local/php_interface/classes/radar_marketmap/actions_market_map.php", //карта рынкка
		"ActionsMMUSA" => "/local/php_interface/classes/radar_marketmap/actions_market_map_usa.php", //карта рынкка США
		"CPortfolio" => "/local/php_interface/classes/portfolio/portfolio.php", //портфели
		"CHistory" => "/local/php_interface/classes/portfolio/CHistory.php", //Загрузка истории портфелей
		"CBeta" => "/local/php_interface/classes/radar/beta.php", //расчеты беты
		"CIndexes" => "/local/php_interface/classes/radar/indexes.php", //Класс для работы с индексами
		"CRadarEvents" => "/local/php_interface/classes/radar/radar_events.php", //Класс для работы с событиями радара
		"CRadarRatings" => "/local/php_interface/classes/radar/radar_ratings.php", //Класс для работы с рейтингами радара

		"CCustomPopup" => "/local/php_interface/classes/common/customPopups.php", //кастомные попапы
		"CBanners" => "/local/php_interface/classes/common/banners.php", //Управление кешированием, таргетированием и ротацией баннеров
		"CEnumerators" => "/local/php_interface/classes/common/CEnumerators.php", //Работа с автонумерацией для различных объектов (ТА, в будущем портфели и т.п.)
		"CLearning" => "/local/php_interface/classes/learning/CLearning.php", //Для работы с базой обучения из админки tools

	)
);


updateBaseCurrenciesToHL();
updateBaseCurrenciesToHL(true);


/**
 * Обновляет котировки базовых и редких валют ЦБ в общей таблице с валютными контрактами
 * Проверяет и устанавливает даты актуальности получения котировок
 *
 * @param  bool   $rare (Optional) true - Получить котировки только для редких валют, false или ничего - Получить котировки базовых валют за исключением редких
 * @param  string   $specialDate (Optional) Для принудительного получения котировок без обновления дат актуальности нужно передать дату в виде строки формата "d.m.Y"
 *
 * @return null Возврат происходит если дата актуальности больше или равна текущей дате. Обновление котировок не происходит
 */
function updateBaseCurrenciesToHL($rare=false, $specialDate = ''){
	Global $APPLICATION;
	CModule::IncludeModule('highloadblock');
	$dateCode = "CURRENCY_UPDATE_DATE";
  if($rare==true){
	$dateCode = 'RARE_'.$dateCode;
	}
	$lastDate = \Bitrix\Main\Config\Option::get("grain.customsettings",$dateCode);



	if(!empty($specialDate)){ //Режим заполнения цен на определенную дату
		$now = new DateTime($specialDate);
		$last = (new DateTime($specialDate))->modify('-1 days');
	} else { //Режим обычного добавления на текущую дату
		$now = new DateTime('now');
		$last = new DateTime($lastDate);
	}

	if($last->format('d.m.Y')>=$now->format('d.m.Y')){
	  	return; //Если за текущую дату было обновление - выходим
		}

/*  if($rare==false){
  	$queryDate = clone $now;
	} else {
	$queryDate = clone $now;
	$queryDate->modify('-1 days');
	}*/
	$queryDate = clone $now;
	$cbr = new CCurrency($queryDate->format('d.m.Y'));

	$arCodes = array_keys($APPLICATION->currencies);
	$arTmpCodes = array();
	$arExcludeCodes = array("", "SUR", "RUB", "RUR");
	$arCodes = array_diff($arCodes, $arExcludeCodes);
	$arRareCodes = array_intersect($arCodes, $cbr->rareCurrencyCharCodes);
   if($rare==true){
   	$arCodes = $arRareCodes;
   } else {
		$arCodes = array_diff($arCodes, $arRareCodes);
   }

	$hlbl              = 31;
	$hlblock           = HL\HighloadBlockTable::getById($hlbl)->fetch();
	$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $entity->getDataClass();
		$resHL = $entity_data_class::getList(array(
			"select" => array(
				"ID",
				"UF_DATE",
				"UF_ITEM",
			),
			"filter" => array("=UF_ITEM"=>array_values($arCodes), "UF_DATE"=>$now->format('d.m.Y')),
		));
	if($ob = $resHL->fetch()){  //Если выборка от текущей даты пустая - обновим цены в таблице валют
	//TODO Здесь просто получить коды валют для которых на дату есть записи в таблице БД и далее убрав else
	//Пропускать или обновлять (быстрее пропускать) записи опираясь на массив полученных кодов

	}  else {
	  $usdRate = $cbr->getRate("USD")["CURS"];
	  $eurRate = $cbr->getRate("EUR")["CURS"];
	  $updateDates = false;

	  foreach($arCodes as $code){

	   $arCbrate = $cbr->getRate($code);
		if($arCbrate["CURRENCY"]=="USD"){  //Если котировка прямая и валюта котировки USD то пересчитаем сколько долларов и рублей будет за единицу валюты
		  if($arCbrate["DIR"]==0 ){
		  $USD_PRICE = 1/$arCbrate["CURS"];
		  } else {
		  $USD_PRICE = $arCbrate["CURS"];
		  }

		  $RUB_PRICE = $usdRate*$USD_PRICE;
		  $EUR_PRICE = $RUB_PRICE/$eurRate;

		  echo "<pre  style='color:black; font-size:11px;'>";
		     'usd price='.print_r($USD_PRICE);
		     'usd rate='.print_r($usdRate);
		     'rub price='.print_r($RUB_PRICE);


		     echo "</pre>";
		}
		else if($arCbrate["CURRENCY"]=="RUB"){
		  if($arCbrate["DIR"]==0 ){
		  $USD_PRICE = $arCbrate["CURS"]/$usdRate;
		  } else {
		  $USD_PRICE = $arCbrate["CURS"]*$usdRate;
		  }
		  $RUB_PRICE = $arCbrate["CURS"];
		  $EUR_PRICE = $RUB_PRICE/$eurRate;

		 }
		  $USD_PRICE = round($USD_PRICE, 3);
		  $RUB_PRICE = round($RUB_PRICE, 3);
		  $EUR_PRICE = round($EUR_PRICE, 3);

			$arFields = array(
			 "UF_ITEM"=>$code,
			 "UF_DATE"=>$now->format('d.m.Y'),
			 "UF_CLOSE"=>$RUB_PRICE,
			 "UF_OPEN"=>$RUB_PRICE,
			 "UF_HIGH"=>$RUB_PRICE,
			 "UF_LOW"=>$RUB_PRICE,
			 "UF_CLOSE_USD"=>$USD_PRICE,
			 "UF_CLOSE_EUR"=>$EUR_PRICE,
			);

		  if(floatval($RUB_PRICE)<=0) continue; // Если цена в рублях не получена пропускаем добавление
	 	  $otvet = $entity_data_class::add($arFields);
        if ($otvet->isSuccess()) {
			 $updateDates = true;
        } else {
        	CLogger::UpdaterBaseCurrencies_309(implode(', ', $otvet->getErrors()));
        }
	  }
	  if($updateDates==true && empty($specialDate)){
	  	\Bitrix\Main\Config\Option::set("grain.customsettings",$dateCode, $now->format('d.m.Y'));
	  }

	}

}


//Сортировка объектов/массивов находящихся в массиве, по указанным полям.
function sort_nested_arrays($array, $args = array('votes' => 'desc')) {
	usort($array, function ($a, $b) use ($args) {
		$res = 0;

		$a = (object) $a;
		$b = (object) $b;

		foreach ($args as $k => $v) {
			if ($a->$k == $b->$k) {
				continue;
			}

			$res = ($a->$k < $b->$k) ? -1 : 1;
			if ($v == 'desc') {
				$res = -$res;
			}

			break;
		}

		return $res;
	});

	return $array;
}

if (!function_exists('mb_str_replace')) {
	function mb_str_replace($search, $replace, $subject, &$count = 0) {
		if (!is_array($subject)) {
			// Normalize $search and $replace so they are both arrays of the same length
			$searches     = is_array($search) ? array_values($search) : array($search);
			$replacements = is_array($replace) ? array_values($replace) : array($replace);
			$replacements = array_pad($replacements, count($searches), '');
			foreach ($searches as $key => $search) {
				$parts = mb_split(preg_quote($search), $subject);
				$count += count($parts) - 1;
				$subject = implode($replacements[$key], $parts);
			}
		} else {
			// Call mb_str_replace for each subject in array, recursively
			foreach ($subject as $key => $value) {
				$subject[$key] = mb_str_replace($search, $replace, $value, $count);
			}
		}
		return $subject;
	}
}

function array_search_key($n, $array) {
	//$i++;
	foreach ($array as $k => $v) {
		if ($k == $n) {
			return $k;
		}
	}
}

//Подключение к бирже
function ConnectMoex($dataUrl) {
	if (!$dataUrl) {
		return false;
	}
	$updateCoockie = false;
	$username      = "finplanorg@yandex.ru";
	$password      = "moex1z";

	$url         = 'https://passport.moex.com/authenticate';
	$cookieName  = 'MicexPassportCert';
	$coockieFile = $_SERVER["DOCUMENT_ROOT"] . "/moex_cookie.txt";
	$syncLogFile = $_SERVER["DOCUMENT_ROOT"] . "/log/syncMoexDaily_log.txt";
	$client      = new \GuzzleHttp\Client();

	$needCookie = unserialize(file_get_contents($coockieFile));

	$dateExpire = date("d.m.Y", filectime($coockieFile));
	$dateExpire = date("d.m.Y", strtotime("$dateExpire +5 day"));

	if ($dateExpire < date("d.m.Y")) {
		$updateCoockie = true;
	} else if (!$needCookie) {
		$updateCoockie = true;
	}

	//Удалим автоматически старый файл с куками биржи, если он создан более 5 дней назад
	if ($updateCoockie == true) {
		unlink($coockieFile);
	}

	if ($updateCoockie == true) {
		$options = array(
			'auth' => array(
				$username,
				$password
			)
		);

		$response = $client->get($url, $options);

		$log = 'time: ' . date('Y-m-d H:i:s') . '; func: ConnectMoex, auth; url: ' . $url;
		file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);

		$cookies = $response->getHeader('Set-Cookie');

		foreach ($cookies as $cookie_str) {
			$cookie = \GuzzleHttp\Cookie\SetCookie::fromString($cookie_str);
			if ($cookie->getName() == $cookieName) {
				$needCookie = $cookie;
				break;
			}
		}
		file_put_contents($coockieFile, serialize($needCookie));
	}

	$jar = new \GuzzleHttp\Cookie\CookieJar;
	$jar->setCookie($needCookie);
	try {
		$res     = $client->request("GET", $dataUrl, ['cookies' => $jar, 'http_errors' => true]);
		$strJson = array();
		$strJson = json_decode($res->getBody()->getContents(), true);
	} catch (RequestException $e) {

		$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectMoex, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getRequest());
		file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);

		if ($e->hasResponse()) {
			$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectMoex, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getResponse());
			file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);
		}
	}

	$needCookieUpdate = "";

	if ($updateCoockie) {
		$cookies = $res->getHeader('Set-Cookie');
		foreach ($cookies as $cookie_str) {
			$cookie = \GuzzleHttp\Cookie\SetCookie::fromString($cookie_str);
			if ($cookie->getName() == $cookieName) {
				$needCookieUpdate = $cookie;
				break;
			}
		}
	}

	if ($needCookieUpdate) {
		file_put_contents($coockieFile, serialize($needCookieUpdate));
	}

	return $strJson;
}

//переворот текста
function switcher($text, $arrow = 0) {
	$str[0] = array('й' => 'q', 'ц' => 'w', 'у' => 'e', 'к' => 'r', 'е' => 't', 'н' => 'y', 'г' => 'u', 'ш' => 'i', 'щ' => 'o', 'з' => 'p', 'х' => '[', 'ъ' => ']', 'ф' => 'a', 'ы' => 's', 'в' => 'd', 'а' => 'f', 'п' => 'g', 'р' => 'h', 'о' => 'j', 'л' => 'k', 'д' => 'l', 'ж' => ';', 'э' => '\'', 'я' => 'z', 'ч' => 'x', 'с' => 'c', 'м' => 'v', 'и' => 'b', 'т' => 'n', 'ь' => 'm', 'б' => ',', 'ю' => '.', 'Й' => 'Q', 'Ц' => 'W', 'У' => 'E', 'К' => 'R', 'Е' => 'T', 'Н' => 'Y', 'Г' => 'U', 'Ш' => 'I', 'Щ' => 'O', 'З' => 'P', 'Х' => '[', 'Ъ' => ']', 'Ф' => 'A', 'Ы' => 'S', 'В' => 'D', 'А' => 'F', 'П' => 'G', 'Р' => 'H', 'О' => 'J', 'Л' => 'K', 'Д' => 'L', 'Ж' => ';', 'Э' => '\'', '?' => 'Z', 'ч' => 'X', 'С' => 'C', 'М' => 'V', 'И' => 'B', 'Т' => 'N', 'Ь' => 'M', 'Б' => ',', 'Ю' => '.', );
	$str[1] = array('q' => 'й', 'w' => 'ц', 'e' => 'у', 'r' => 'к', 't' => 'е', 'y' => 'н', 'u' => 'г', 'i' => 'ш', 'o' => 'щ', 'p' => 'з', '[' => 'х', ']' => 'ъ', 'a' => 'ф', 's' => 'ы', 'd' => 'в', 'f' => 'а', 'g' => 'п', 'h' => 'р', 'j' => 'о', 'k' => 'л', 'l' => 'д', ';' => 'ж', '\'' => 'э', 'z' => 'я', 'x' => 'ч', 'c' => 'с', 'v' => 'м', 'b' => 'и', 'n' => 'т', 'm' => 'ь', ',' => 'б', '.' => 'ю', 'Q' => 'Й', 'W' => 'Ц', 'E' => 'У', 'R' => 'К', 'T' => 'Е', 'Y' => 'Н', 'U' => 'Г', 'I' => 'Ш', 'O' => 'Щ', 'P' => 'З', '[' => 'Х', ']' => 'Ъ', 'A' => 'Ф', 'S' => 'Ы', 'D' => 'В', 'F' => 'А', 'G' => 'П', 'H' => 'Р', 'J' => 'О', 'K' => 'Л', 'L' => 'Д', ';' => 'Ж', '\'' => 'Э', 'Z' => '?', 'X' => 'ч', 'C' => 'С', 'V' => 'М', 'B' => 'И', 'N' => 'Т', 'M' => 'Ь', ',' => 'Б', '.' => 'Ю', );
	return strtr($text, isset($str[$arrow]) ? $str[$arrow] : array_merge($str[0], $str[1]));
}


if(!function_exists('pluralText'))
{

    /**
     * Возврат окончания слова при склонении
     *
     * Функция возвращает окончание слова, в зависимости от примененного к ней числа
     * Например: 5 товаров, 1 товар, 3 товара
     *
     * @param int $value - число, к которому необходимо применить склонение
     * @param array $status - массив возможных окончаний
     * @return mixed
     */
    function pluralText($value=1, $status= array('','а','ов'))
    {
     $array =array(2,0,1,1,1,2);
     return $status[($value%100>4 && $value%100<20)? 2 : $array[($value%10<5)?$value%10:5]];
    }
}

/*AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
function OnBeforeUserUpdateHandler(&$arFields)
{
        $arFields["EMAIL"] = $arFields["LOGIN"];
        return $arFields;
}*/

AddEventHandler("main", "OnAfterUserRegister", "OnAfterUserRegisterHandler");
function OnAfterUserRegisterHandler(&$arFields) {
	$elem = new CrmAdder();

	$data = array(
		"email" => $arFields["LOGIN"],
		"name" => $arFields["NAME"],
	);
	//CLogger::AfterUserRegister(implode("; ",$arFields));
    $arFields["EMAIL"] = $arFields["LOGIN"];
    
    //Заполняем штатное поле email
    if(isset($arFields["USER_ID"]) && !empty($arFields["USER_ID"])) {
        $user = new CUser;
        $fields = array(
          "EMAIL" => $arFields["LOGIN"],
        );
        $user->Update($arFields["USER_ID"], $fields);
    }
	unset($_REQUEST);
	foreach ($data as $field => $val) {
		$_REQUEST[$field] = $val;
	}

	$elem->addSimple(19, true, "Регистрация на сайте");


	if(isset($arFields["USER_ID"]) && !empty($arFields["USER_ID"]))
		{  //Создаем индивидуальный промокод
		 // 	PromoGenerator::addPersonal($arFields["USER_ID"]);

			//Обработаем подарок при регистрации, если он включен (обработка внутри метода класса)
			//Комментировано 29.10.2020, т.к. срабатывает второй раз после OnAfterUserEnd  задваивает продажи, если была регистрация с промокодом, дающим
			//радар на 2 недели.
			//$CBonusGift = new CBonusGift();
			//$recalcProducts = $CBonusGift->setGift($arFields["USER_ID"], 'setGiftAfterUserRegister');


  			  $cdb = new CrmDataBase(false);
			  $cdb->setUsersProducts(array("=PROPERTY_USER" => $arFields["USER_ID"]));
			  unset($cdb);

		}

  	 if($arFields["NAME"]=='alextestnew'){ //Если это условие убрать - то при регистрации из попапа где-то виснет и auth.php не догружается из-за чего браузер выкидывает предупреждение.
//    	CLogger::MainRegister(print_r($arFields,true));
//    	CLogger::MainRegister("_REQUEST: ".print_r($_REQUEST,true));
   //  	CLogger::MainRegister("error: ".$user->LAST_ERROR);
		}

}

//При интерактивном редактировании оплат из админки вешаем на изменение инфоблока 11 событие, перекеширующее проверку доступности радара для конкретного пользователя
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementUpdateHandler");
function OnAfterIBlockElementUpdateHandler(&$arFields) {
	if ($arFields["IBLOCK_ID"] == 52) { //Сбрасываем кеш списка портфелей для конкретного пользователя по тегу
		global $CACHE_MANAGER;
		$CACHE_MANAGER->ClearByTag('list_user_' . $GLOBALS["USER"]->GetID());
	}

	if ($arFields["IBLOCK_ID"] == 11) {
		$userId = 0;

		if (array_key_exists("PROPERTY_VALUES", $arFields)) { //Получаем id пользователя указанного в элементе инфоблока, для обработки его кеша с доступом к радару
			if (isset($arFields["PROPERTY_VALUES"][77]) && is_array($arFields["PROPERTY_VALUES"][77])) {
				$userId = reset($arFields["PROPERTY_VALUES"][77]);
				if (array_key_exists('VALUE', $userId)) {
					$userId              = intval($userId['VALUE']);
				}
			}
		} else { //при программном обновлении, когда массив данных урезан:
			//Получаем свойство с пользователем для только что измененного элемента
			$arSelect = array("ID", "NAME", "PROPERTY_USER");
			$res      = CIBlockElement::GetList(array(), array("ID" => $arFields["ID"]), false, array(), $arSelect);
			if ($row = $res->fetch()) {
				$userId = $row["PROPERTY_USER_VALUE"];
			}
		}

		if (intval($userId) > 0) { //Если пользователь определен - то пересчитываем ему

			//Пересчитываем состав продуктов и даты покупок для CRM
			$cdb = new CrmDataBase(false);
			$cdb->setUsersProducts(array("=PROPERTY_USER" => $userId));
			unset($cdb);

			//Перегенерация кеша проверки доступности радара для конкретного пользователя
			$checkCachedPayRadar = checkPayRadar(true, $userId);

		   //Очищаем кеш доступа к семинарам
		   $clear = clearCachePaySeminar($userId);

		}
	}

	//Сброс кеша общедоступных вебинаров при интерактивном изменении в инфоблоке 9
	if ($arFields["IBLOCK_ID"] == 9) {
		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	  	$cache->clean("getAllOpenedWebinars");
	}

	if ($arFields["IBLOCK_ID"] == 19) {
		$cnt = 0;
		foreach ($arFields["PROPERTY_VALUES"][344] as $k => $v) {
			if (!empty($v["VALUE"])) {
				$cnt++;
			}
		}
		if ($cnt > 0) {
			CIBlockElement::SetPropertyValues(intval($arFields["ID"]), $arFields["IBLOCK_ID"], intval($cnt), "WEBINARS_CNT");

		  //Перестраиваем список пользователей, посетивших вебинары
		  //Так же прописано в /classes/crm/CrmDataBase.php
		  $CBanners = new CBanners();
		  $CBanners->setArraysWebinarsUsers(true);
		  unset($CBanners);
		}
	}

	//Изменение имени эмитента на в инфоблоке "Страницы облигаций" при изменении эмитента самой облигации
	if ($arFields["IBLOCK_ID"] == 27) {
		 if(!empty($arFields["PROPERTY_VALUES"][188]["VALUE"])){
			 $arSelect = Array("ID", "NAME" );
          $arFilter = Array("IBLOCK_ID"=>IntVal(26), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
          $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
          if($ob = $res->fetch()){
				 CIBlockElement::SetPropertyValues($ob["ID"], 30, $ob["NAME"], "EMITENT_NAME");
          }
		 }

	}

}

AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");
function OnAfterIBlockElementAddHandler($arFields) {
	if ($arFields["IBLOCK_ID"] == 52) { //Сбрасываем кеш списка портфелей для конкретного пользователя по тегу
		global $CACHE_MANAGER;
		$CACHE_MANAGER->ClearByTag('list_user_' . $GLOBALS["USER"]->GetID());
	}

	if ($arFields["IBLOCK_ID"] == 11) {
		CModule::IncludeModule("iblock");
		$res  = CIBlockElement::GetProperty(11, $arFields['ID'], [], ["CODE" => "USER"]);
		$res2 = CIBlockElement::GetProperty(11, $arFields['ID'], [], ["CODE" => "PAYED"]);

		$result  = $res->Fetch();
		$isPayed = $res2->Fetch();

		if ($result && $result['VALUE'] && $isPayed['VALUE']) {
			$arUser = CUser::GetByID($result['VALUE'])->Fetch();
            
            require_once $_SERVER["DOCUMENT_ROOT"] . "/api/mailchimp/MailChimp.php";

			$MailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');
			$list_id   = "b6050240b6";
			$result    = $MailChimp->get("search-members", array(
				"query" => $arUser["LOGIN"],
				"list_id" => $list_id
			));

			if ($MailChimp->success()) {
				if ($result["exact_matches"]["members"]) {
					foreach ($result["exact_matches"]["members"] as $member) {
						$result[] = $MailChimp->delete("lists/" . $list_id . "/members/" . $member["id"]);
					}
				}
			}
			//добавляем
			$tmp = [
				'email_address' => $arUser["LOGIN"],
				'status' => 'subscribed',
				"merge_fields" => [
					"EMAIL" => $arUser["LOGIN"],
					"FNAME" => $arUser["NAME"],
					"LNAME" => "",
				],
			];
			$list_id = "8733ecb52b";
			$result  = $MailChimp->post("lists/$list_id/members", $tmp);

			//mail("callbackkiller.kss@gmail.com", "look at this", json_encode($result));
		}

		$checkCachedPayRadar = checkPayRadar(true, $arUser["ID"]); //Перегенерация кеша проверки доступности радара для конкретного пользователя

		//Очищаем кеш доступа к семинарам
		$clear = clearCachePaySeminar($arUser["ID"]);

	  //Если добавляем продажу вручную и заполнили поле для активации пересчета - то пересчитаем продукты и даты в CRM для этого пользователя
	  if(!empty($arFields["PROPERTY_VALUES"][718]["n0"]["VALUE"])){

		//Пересчитываем состав продуктов и даты покупок для CRM
		      $cdb = new CrmDataBase(false);
		  	  	$cdb->setUsersProducts(array("=PROPERTY_USER"=>$arFields["PROPERTY_VALUES"][77]["n0"]["VALUE"]));
			  	unset($cdb);
			  //	CIBlockElement::SetPropertyValues(intval($arFields["ID"]), $arFields["IBLOCK_ID"], "", "RECOUNT_PRODUCTS_CRM");//Очищаем признак необходимости пересчета
	  }

	  //Перестраиваем список клиентов для таргетирования баннеров, так же прописано в файле упрвления кешированием /local/php_interface/include/managed_cache_iblocks.php
	  $CBanners = new CBanners();
	  $CBanners->setArraysClientsUsers(true);
	  unset($CBanners);

	}

	//Сброс кеша общедоступных вебинаров при интерактивном добавлении в инфоблоке 9
	if ($arFields["IBLOCK_ID"] == 9) {
		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cache->clean("getAllOpenedWebinars");
	}
}


AddEventHandler("main", "OnBeforeUserDelete", "OnBeforeUserDeleteHandler");
    // создаем обработчик события "OnBeforeUserDelete"
    function OnBeforeUserDeleteHandler($user_id)
    {
    	CModule::IncludeModule('highloadblock');
        // проверим есть ли связанные с удаляемым пользователем записи по бонусам
		$hlblock   = HL\HighloadBlockTable::getById(18)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();
		$rsData = $entityClass::getList(array(
		   'filter' => array("UF_USER" => $user_id),
		   'select' => array('ID')
		));

		while($item = $rsData->Fetch()) {
			$entityClass::delete($item["ID"]);
		}

		//Удалим сгенерированный промокод для удаляемого пользователя
		CModule::IncludeModule("iblock");
		Global $DB;
		$arSelect = Array("ID", "NAME", "IBLOCK_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal(36), "PROPERTY_USER"=>$user_id);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->fetch()){
		      $DB->StartTransaction();
			    if(!CIBlockElement::Delete($ob["ID"]))
			    {
			        $DB->Rollback();
			    }
			    else
			        $DB->Commit();
		}

		//Удалим сгенерированне покупки семинаров для удаляемого пользователя
		$arSelect = Array("ID", "NAME", "IBLOCK_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal(11), "PROPERTY_USER"=>$user_id);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->fetch()){
		      $DB->StartTransaction();
			    if(!CIBlockElement::Delete($ob["ID"]))
			    {
			        $DB->Rollback();
			    }
			    else
			        $DB->Commit();
		}

		//Удалим запись из CRM для удаляемого пользователя
		$arSelect = Array("ID", "NAME", "IBLOCK_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal(19), "PROPERTY_USER"=>$user_id);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->fetch()){
		      $DB->StartTransaction();
			    if(!CIBlockElement::Delete($ob["ID"]))
			    {
			        $DB->Rollback();
			    }
			    else
			        $DB->Commit();
		}

		  return true;
    }



AddEventHandler("iblock", "OnBeforeIBlockElementDelete", "OnBeforeIBlockElementDeleteHandler");
function OnBeforeIBlockElementDeleteHandler($ID) {
	//Проверяем удаление дефолтных пресетов сброса, прописанных в классе RadarPresets как защищенные от удаления
	if (in_array($ID, RadarPresets::getRadarResetPresetsId()) || in_array($ID, CBanners::getDefendedId())) {
		global $APPLICATION;
		$APPLICATION->throwException("Элемент запрещено удалять, так, как он является элементом по умолчанию");
		return false;
	}
}
