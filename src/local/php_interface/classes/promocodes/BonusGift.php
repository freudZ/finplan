<?
/**
 * Класс для раздачи подарков при различных событиях
 *
 * Подарки определяются в инфоблоке "Скрытые семинары" и формируются при событиях, которые обрабатываются в даннном классе соответствующими методами
 *
 * @copyright 2020
 * @version   0.0.1
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     29 class CBonusGift
 *     44   function setGift($uid, $method)
 *     59   function isOwnerClient($userId)
 *     74   function isGiftAlready($userId)
 *    103   function setGiftAfterUserRegister($uid)
 *
 * TOTAL FUNCTIONS: 4
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */
CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class CBonusGift {
 private $saleIblockId = 11; //Продажи
 private $accessHlId = 11; //HL инфоблок доступа к семинарам
 private $HiddenSeminarIblockId = 14; //Скрытые семинары
 private $bonusesHlId = 18; //Начисления бонусов


/**
 * Роутинг методов из единой точки входа
 *
 * @param  int   $uid  - id пользователя
 * @param  string   $method  - название метода класса, который будет выполнен
 *
 * @access public
 */
public function setGift($uid, $method){
	$recalcProducts = false;
  switch ($method) {
      case 'setGiftAfterUserRegister':
          $recalcProducts = $this->setGiftAfterUserRegister($uid, 'setGiftAfterUserRegister');
          break;
      case 'setGiftAfterUserAdd':
		  $recalcProducts = $this->setGiftAfterUserRegister($uid, 'setGiftAfterUserAdd');
          break;
 /*     case 2:
          echo "i равно 2";
          break;*/
  }
  return $recalcProducts;
}

//Определяет, является ли владелец промокода клиентом (делал ли покупки)
public function isOwnerClient($userId){
 	 $result = false;
	 $arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>IntVal($this->saleIblockId), "PROPERTY_USER"=>$userId, "PROPERTY_PAYED_VALUE"=>"Да");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->fetch()){
		if($result==false){
			$result = true;
			break;
		}
    }
	 return $result;
 }

//Определяет начислен ли уже подарочный бонус при регистрации
public function isGiftAlready($userId){
 	 $result = false;
	$hlblock   = HL\HighloadBlockTable::getById($this->bonusesHlId)->fetch();
	$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
	$entityClass = $entity->getDataClass();

	$resHL = $entityClass::getList(array(
		"select" => array(
			"*",
		),
		"filter" => array("UF_DESC"=>"Подарок при регистрации", "UF_USER"=>$userId),
	));

	if($row = $resHL->fetch()){
	  $result = true;
	}

	 return $result;

}


 /**
  * Добавляет подарок после регистрации пользователя
  *
  * @param  int   $uid - id пользователя
  *
  * @access private
  */
 private function setGiftAfterUserRegister($uid, $method=''){
 	 global $USER;
	 $ENABLE_BONUS_AFTER_REG = \Bitrix\Main\Config\Option::get("grain.customsettings","ENABLE_BONUS_AFTER_REG");
	 $ENABLE_BONUS_AFTER_REG_UTM = \Bitrix\Main\Config\Option::get("grain.customsettings","ENABLE_BONUS_AFTER_REG_WITH_UTM");
	 $BOUNUS_ELEMENT_ID = \Bitrix\Main\Config\Option::get("grain.customsettings","BOUNUS_ELEMENT_ID");
	 $BOUNUS_ELEMENT_CLIENT_ID = \Bitrix\Main\Config\Option::get("grain.customsettings","BOUNUS_ELEMENT_CLIENT_ID");
	 $BONUS_GIFT_BY_PROMOCODE = \Bitrix\Main\Config\Option::get("grain.customsettings","BONUS_GIFT_BY_PROMOCODE");

	 $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	 $cookieUtm = $request->getCookie("PERSONAL_PROMO");
	 if($ENABLE_BONUS_AFTER_REG=='Y' && $ENABLE_BONUS_AFTER_REG_UTM=='Y'){
	 	if(empty($cookieUtm))  return; //Если разрешено только при наличии utm, но в куки пусто - выходим

		$CPromocodes = new Promocodes();
		$ownerUserId = $CPromocodes->getReferalByPromocode($cookieUtm);
		if(intval($ownerUserId)<=0) return;  //Если не удалось найти владельца промокода - выходим

		$isOwnerClient = $this->isOwnerClient($ownerUserId);//Определение является ли владелец промо-кода клиентом (есть ли оплаченные покупки)

		if($isOwnerClient == true){
			$BOUNUS_ELEMENT_ID = $BOUNUS_ELEMENT_CLIENT_ID; //Если владелец промо-кода является клиентом - то переназначаем бонусный элемент
		}


	 }

	 if($ENABLE_BONUS_AFTER_REG!='Y') return; //Если не включено - выходим
	 if(intval($BOUNUS_ELEMENT_ID)<=0 && $BONUS_GIFT_BY_PROMOCODE!='Y') return; //Если не задан элемент скрытых семинаров не разрешены подарки по промокодам из utm - выходим
	 if(intval($uid)<=0) return; //Если не задан пользователь - выходим

	 $arHidSeminarElement = array();
	 //Получаем опорный элемент

			$arFilter = Array("IBLOCK_ID"=>$this->HiddenSeminarIblockId, "ACTIVE"=>"Y");
		     if($BONUS_GIFT_BY_PROMOCODE=='Y' && !empty($cookieUtm)){
		     	$userPromocode = '%'.$cookieUtm.'%';
				 $arFilter[] = array("LOGIC"=>"OR", array("ID"=>$BOUNUS_ELEMENT_ID), array("NAME"=>$userPromocode));
			 } else {
		     	$arFilter["ID"] = $BOUNUS_ELEMENT_ID;
			 }

			$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
		    $arFoundHidSeminars = array();
		     while($ob = $res->GetNextElement())
			{
				$arHidSeminarElement = $ob->GetFields();
				$arHidSeminarElement["PROPERTIES"] = $ob->GetProperties();
				$arFoundHidSeminars[] = $arHidSeminarElement;
			}

	 define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log/arFoundHidSeminars.txt");
	 AddMessage2Log('$BONUS_GIFT_BY_PROMOCODE = '.print_r($BONUS_GIFT_BY_PROMOCODE, true),'');
	 AddMessage2Log('$arFilter = '.print_r($arFilter, true),'');
	 AddMessage2Log('$arFoundHidSeminars = '.print_r($arFoundHidSeminars, true),'');

	 $globalBuy = false;
	 if(count($arFoundHidSeminars)) {
	 	foreach($arFoundHidSeminars as $arHidSeminarElement) {

	 	 $props = $arHidSeminarElement["PROPERTIES"];
		 $dop_items = $props["DOP_ITEMS"]["VALUE"];
		 $close_items = $props["CLOSE_ACCESS"]["VALUE"];
		 $radarDays = $props["RADAR_DAYS"]["VALUE"];
		 $itemDays = $props["ITEM_DAYS"]["VALUE"];
		 $ACCESS_USA = $props["ACCESS_USA"]["VALUE"];
		 $AddBonus = intval($props["ADD_BONUSES"]["VALUE"]);

		 $el = new CIBlockElement;


		 $rsUser = CUser::GetByID($uid);
		 $arUser = $rsUser->Fetch();

		 $PROP = array();
		 $PROP["FIO"] = $arUser["NAME"] . " " . $arUser["LAST_NAME"];
		 $PROP["PHONE"] = $arUser["PERSONAL_PHONE"];
		 $PROP["USER"] = $uid;
		 $PROP["ITEM"] = intval($props["ITEM"]["VALUE"]);
		 //$PROP["BUY_TYPE"] = $buyID; //способ оплаты
		 $PROP["DOP_ITEMS"] = $dop_items;
		 $PROP["CLOSE_ACCESS"] = $close_items;
		 if($ACCESS_USA == 'Y') {
			 $PROP["ACCESS_USA"] = array("VALUE" => 111);
		 }
		 $PROP["PAY_NAME"] = $arHidSeminarElement["NAME"];
		 if(!$PROP["ITEM"]) {
			 unset($PROP["ITEM"]);
		 }

		 if($radarDays) {
			 $end = checkPayEndDateRadar();
			 if($end) {
				 $end = new DateTime($end);
				 $end->modify("+" . $radarDays . "days");

				 $PROP["RADAR_END"] = $end->format("d.m.Y");
			 } else {
				 $dt = (new DateTime())->modify("+" . $radarDays . "days");
				 $PROP["RADAR_END"] = $dt->format("d.m.Y");
			 }
		 }
		 if($itemDays) {
			 $end = checkPayEndDateSeminar(intval($_POST["item"]));
			 if($end) {
				 $end = new DateTime($end);
				 $end->modify("+" . $itemDays . "days");
				 $PROP["ITEM_END"] = $end->format("d.m.Y");
			 } else {
				 $dt = (new DateTime())->modify("+" . $itemDays . "days");
				 $PROP["ITEM_END"] = $dt->format("d.m.Y");
			 }
		 }

		 $arLoadProductArray = array("IBLOCK_SECTION_ID" => false,
									 "IBLOCK_ID"         => $this->saleIblockId,
									 "PROPERTY_VALUES"   => $PROP,
									 "NAME"              => date("d.m.Y H:i:s"),
									 "ACTIVE"            => "Y",
									 "PREVIEW_TEXT"      => $arHidSeminarElement["NAME"]
		 );

		 //Определяем есть ли в опорном элементе позиции кроме начисления бонусов, создавать ли элемент в покупках
		 $addElementBuy = false;
		 if(intval($props["ITEM"]["VALUE"]) > 0) {
			 $addElementBuy = true;
		 }
		 if(!empty($dop_items)) {
			 $addElementBuy = true;
		 }
		 if(!empty($close_items)) {
			 $addElementBuy = true;
		 }
		 if(intval($radarDays) > 0 || intval($itemDays) > 0) {
			 $addElementBuy = true;
		 }
		 if($ACCESS_USA == 'Y') {
			 $addElementBuy = true;
		 }

		 //Создаем в покупках элемент на основе опорного
		 if($addElementBuy == true) {
		 	$globalBuy = true;
			 if($order_id = $el->Add($arLoadProductArray)) {
				 //Если указано - закрываем доступ к материалам
				 if($PROP["CLOSE_ACCESS"]) {
					 $hlblock = HL\HighloadBlockTable::getById($this->accessHlId)->fetch();
					 $entity = HL\HighloadBlockTable::compileEntity($hlblock);
					 $entity_data_class = $entity->getDataClass();

					 foreach($PROP["CLOSE_ACCESS"] as $val) {

						 $main_query = new Entity\Query($entity);
						 $main_query->setOrder(array("ID" => "desc"));
						 $main_query->setFilter(array("UF_USER" => $uid, "UF_SEMINAR" => $val));
						 $exec = $main_query->exec();
						 $exec = new CDBResult($exec);
						 if(!$exec->Fetch()) {
							 $entity_data_class::add(array("UF_USER"    => $uid,
														   "UF_SEMINAR" => $val
							 ));
						 }
					 }
				 }


				 //если куплен радар

				 if($PROP["RADAR_END"]) {
					 CIBlockElement::SetPropertyValues($order_id, $this->saleIblockId, 10, "PAYED");
					 $user = new CUser;
					 $fields = array("UF_RADAR_ACCESS" => true,
					 );
					 $user->Update($uid, $fields);
					 $checkCachedPayRadar = checkPayRadar(true, $uid); //Перегенерация кеша проверки доступности радара для конкретного пользователя
				 }

			 }

		 }//Если создаем покупку

		 //Начисление бонусов если они заданы
		 if($AddBonus > 0) {
			 Bonuses::addSimpleBonus($uid, $AddBonus, 'Подарок при регистрации');
		 }

	 }//foreach found hid seminars

		if($globalBuy){
/*						 $cdb = new CrmDataBase(false);
						 $cdb->setUsersProducts(array("=PROPERTY_USER" => $uid));
						 unset($cdb);*/
		}

	 }

    return $globalBuy;
 }


}

 ?>