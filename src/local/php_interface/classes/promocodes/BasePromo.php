<?
class BasePromo
{
	/**
	 * @var int ИД генератора
	 */
	public static $ibIdGenerator = 35;
	public static $ibIdPromo = 36;

	/**
	 * Загрузчик
	 */
	public function loader()
	{
		\Bitrix\Main\Loader::includeModule("iblock");
	}
	
	/**
	 * Загрузчик персанальной таблицы
	 */
	public function personalPromoLoader()
	{
		\Bitrix\Main\Loader::includeModule("highloadblock");
		
		$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(19)->fetch();
		$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();
		
		return $entityClass;
	}
	
	/**
	 * Элемент получение
	*/
	public static function getIbItem($filter, $select)
	{
		self::loader();

		$res = CIBlockElement::GetList(Array(), $filter, false, false, $select);
		if ($ob = $res->GetNext()) {
			return $ob;
		}
	}
	
	/**
	 * Элементы получение
	*/
	public static function getIbItems($filter, $select)
	{
		self::loader();

		$res = CIBlockElement::GetList(Array(), $filter, false, false, $select);
		while ($ob = $res->GetNext()) {
			$arReturn[] = $ob;
		}
		
		return $arReturn;
	}
	
	/**
	 * Добавление элемента
	 */
	public static function addIbItem($fields){
		self::loader();
		
		$el = new CIBlockElement;
		return $el->Add($fields);
	}
	
		
	/**
	 * Получение дат
	 */
	public static function getDates($length){
		$dt = new DateTime();
		$return = [
			"start" => $dt->format("d.m.Y"),
		];
		$dt->modify("+".$length."days");
		$return["end"] = $dt->format("d.m.Y");
		
		return $return;
	}
	
	/**
	 * Получение элемента списка
	 */
	public static function getSelectType($val, $ib, $code){
		$property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$ib, "CODE"=>$code, "VALUE"=>$val));
		if($enum_fields = $property_enums->GetNext())
		{
			return $enum_fields;
		}
	}
	
	/**
	 * Деактивация элемента
	 */
	public static function deactivateItem($id){
		self::loader();
		$el = new CIBlockElement;
		$el->Update($id, array(
			"ACTIVE" => "N"
		));
	}
}