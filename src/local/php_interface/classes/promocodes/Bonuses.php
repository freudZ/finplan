<?
class Bonuses extends BaseBonuses
{
	/**
	 * добавление  бонуса после покупки
	 */
	public static function addBonus($promoItem, $summ, $userID){
		if(!$promoItem || !$summ){
			return false;
		}

		$item = Promocodes::formOrderPromoItem($promoItem, $summ);
		
		//Сделаем начисление
		parent::addItem(array(
			"UF_TYPE" => 9,
			"UF_VAL" => $item["summ"],
			"UF_USER" => $promoItem["PROPERTY_USER_VALUE"],
			"UF_FROM_USER" => $userID,
			"UF_DESC" => "привлечение друга",
			"UF_DATE" => date("d.m.Y H:i:s"),
		));
	}

	/**
	 * добавление подарочных бонусов без проверок, от пользователя admin c id=1
	 */
	public static function addSimpleBonus($userID, $summ, $description){
		if(!$userID || !$summ){
			return false;
		}
		$bonusGift = new CBonusGift();
		$haveGift = $bonusGift->isGiftAlready($userID); //Проверим наличие подарка

		//Сделаем начисление если подарок еще не давался
		if($haveGift==false){
		parent::addItem(array(
			"UF_TYPE" => 9,
			"UF_VAL" => $summ,
			"UF_USER" => $userID,
			"UF_FROM_USER" => 1,
			"UF_DESC" => $description,
			"UF_DATE" => date("d.m.Y H:i:s"),
		));
		}
	}

	/**
	 * списание бонусов
	 */
	public static function removeBonus($userID, $summ, $payName) {
		parent::addItem(array(
			"UF_TYPE" => 10,
			"UF_VAL" => $summ,
			"UF_USER" => $userID,
			"UF_DESC" => $payName,
			"UF_DATE" => date("d.m.Y H:i:s"),
		));
	}
	
	/**
	 * получение доступной суммы бонусов
	 */
	public static function getTotalBonuses($userID){
		if(!$userID){
			return false;
		}
		
		$data = parent::getItems(array(
			"UF_USER" => $userID,
		), array(
			"UF_VAL",
			"UF_TYPE"
		));
		
		$total = 0;
		foreach($data as $item){
			if($item["UF_TYPE"]==9){
				$total += $item["UF_VAL"];
			} else {
				$total -= $item["UF_VAL"];
			}
		}
		
		return $total;
	}
	
	/**
	 * получение всех операций пользователя
	 */
	public static function getOperations($userID){
		if(!$userID){
			return false;
		}
		
		return parent::getItems(array(
			"UF_USER" => $userID,
		), array(
			"*"
		), array(
			"UF_DATE" => "desc"
		));
	}
	
	/**
	 * тип операции
	 */
	public static function getOperationType($id){
		if(!$id){
			return false;
		}
		
		if($id==9){
			return "plus";
		} else {
			return "minus";
		}
	}
}