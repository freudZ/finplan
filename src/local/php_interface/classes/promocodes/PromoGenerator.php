<?
class PromoGenerator extends BasePromo
{
	/**
	 * Действие после покупки
	 */
	public static function afterBuyAction($userId, $promocode = false, $summ = false, $bonuses=false, $payName=false)
	{
		if(!$userId){
			return false;
		}

		  $cookie_promocode = '';
		if(empty($promocode)){ //Если не передан промокод введенный вручную ищем его в куках для нового пользователя и в CRM со статусом "Рабочий" для начисления бонусов партнеру-владельцу такого промокода
		  $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		  $promocode = $request->getCookie("PERSONAL_PROMO");
		  $cookie_promocode = 'Y';

		$pc = new Promocodes(); //Ищем в CRM
		$promocode = $pc->getRefPersonalPromoCRM($userId);
		unset($pc);
		}



		define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log/PromoGenerator_afterBuyAction_log.txt");
		AddMessage2Log('$arFields = '.print_r(array("userId"=>$userId, "promocode"=>$promocode, "cookie_promocode"=>$cookie_promocode, "summ"=>$summ, "bonuses"=>$bonuses, "payName"=>$payName), true),'');

		if($promocode){
			Promocodes::setPromocodeAsUsed($promocode);
			
			//Если был введен персональный промокод - добавим бонусов
			if($summ){
				if($promoItem = Promocodes::isPersonalPromo($promocode)){
					Bonuses::addBonus($promoItem, $summ, $userId);

					//И запишем в таблицу использования
					Promocodes::addPersonalPromoUses($userId, $promoItem["PROPERTY_USER_VALUE"]);
					Promocodes::setRefPersonalPromoUsed($userId); //Отмечаем, что промокод использован в свойства пользователя
					Promocodes::updateUserReferalStatusCRM('', 89, $userId); //Отмечаем, что промокод использован в свойства эл-та 19 инфоблока (CRM)
				}
			}
		}
		
		//Если оплачен с бонусами
		if($bonuses){
			Bonuses::removeBonus($userId, $bonuses, $payName);
		}
		
		self::addAfterBuy($userId);
		//self::addPersonal($userId);
	}

	/**
	 * Добавляет промокод после покупки
	 */
	public static function addAfterBuy($userId)
	{
		//Удаляем текущие промокоды для следующей покупки
		self::deleteNextBuyPromo($userId);
		
		$generatorItem = parent::getIbItem(
			Array("IBLOCK_ID" => parent::$ibIdGenerator, "PROPERTY_TYPE" => 49, "ACTIVE" => "Y"),
			array("PROPERTY_DURATION", "PROPERTY_SET", "PROPERTY_SET_TYPE", "PROPERTY_LENGTH")
		);
		if($generatorItem) {			
			$dates = parent::getDates($generatorItem["PROPERTY_DURATION_VALUE"]);
			$type = parent::getSelectType($generatorItem["PROPERTY_SET_TYPE_VALUE"], parent::$ibIdPromo, "SET_TYPE");
			
			//генерим на след. покупку
			parent::addIbItem(array(
				"IBLOCK_ID" => parent::$ibIdPromo,
				"NAME" => self::generateCode($generatorItem["PROPERTY_LENGTH_VALUE"]),
				"PROPERTY_VALUES" => array(
					"TYPE" => 54,
					"USER" => $userId,
					"DATE_START" => $dates["start"],
					"DATE_END" => $dates["end"],
					"SET" => $generatorItem["PROPERTY_SET_VALUE"],
					"SET_TYPE" => $type["ID"],
				),
			));
		}
	}

	/**
	 * Удаляет действующие промокоды на след. покупки
	 */
	public static function deleteNextBuyPromo($userId){
		self::loader();

		$filter = array(
			"IBLOCK_ID" => parent::$ibIdPromo,
			"PROPERTY_USER" => $userId,
			"ACTIVE" => "Y",
			"PROPERTY_TYPE" => 54
		);
		$res = CIBlockElement::GetList(Array(), $filter, false, false, array("ID"));
		while ($ob = $res->GetNext()) {
			parent::deactivateItem($ob["ID"]);
		}
	}
	
	/**
	 * Добавляет персональный промокод
	 */
	public static function addPersonal($userId)
	{
		$generatorItem = parent::getIbItem(
			Array("IBLOCK_ID" => parent::$ibIdGenerator, "PROPERTY_TYPE" => 51, "ACTIVE" => "Y"),
			array("PROPERTY_DURATION", "PROPERTY_SET", "PROPERTY_SET_TYPE", "PROPERTY_LENGTH")
		);
		CLogger::PromoGenerator_addPersonal('generatorItem '.print_r($generatorItem, true));
		if($generatorItem) {

			//Проверяем, есть ли существующий промокод
			$generatedItem = parent::getIbItem(
				Array("IBLOCK_ID" => parent::$ibIdPromo, "PROPERTY_TYPE" => 56, "PROPERTY_USER"=>$userId),
				array("ID")
			);
			CLogger::PromoGenerator_addPersonal('generatedItem '.print_r($generatedItem, true));
			if(!$generatedItem){
				$dates = parent::getDates($generatorItem["PROPERTY_DURATION_VALUE"]);
				$type = parent::getSelectType($generatorItem["PROPERTY_SET_TYPE_VALUE"], parent::$ibIdPromo, "SET_TYPE");
				CLogger::PromoGenerator_addPersonal('dates and type '.print_r(array('dates'=>$dates, 'type'=>$type), true));
				//генерим персональный
				parent::addIbItem(array(
					"IBLOCK_ID" => parent::$ibIdPromo,
					"NAME" => self::generateCode($generatorItem["PROPERTY_LENGTH_VALUE"]),
					"PROPERTY_VALUES" => array(
						"TYPE" => 56,
						"USER" => $userId,
						"DATE_START" => $dates["start"],
						"DATE_END" => $dates["end"],
						"SET" => $generatorItem["PROPERTY_SET_VALUE"],
						"SET_TYPE" => $type["ID"],
					),
				));
			}
		}
		unset($generatorItem, $generatedItem);
	}
	
	/**
	 * Генератор кода
	 */
	public static function generateCode($length){
		$unique = false;
		while(!$unique){
			$code = randString($length, array(
				"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
				"0123456789",
			));
			
			$generatedItem = parent::getIbItem(
				Array("IBLOCK_ID" => parent::$ibIdPromo, "NAME" => $code),
				array("ID")
			);
			if(!$generatedItem){
				$unique = true;
			}
		}
		
		return $code;
	}
}