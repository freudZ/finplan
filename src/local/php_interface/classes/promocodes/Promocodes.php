<? CModule::IncludeModule("iblock");
class Promocodes extends BasePromo
{

	/**
	 * Проверяет, изменяет данные по персональному промокоду для пользователя перешедшего по реферальной ссылке и регистрирующегося или авторизующегося
	 * Значения для UF_PARTNER_STATUS: 11 - disabled (Не рабочий), 12 - enabled (Рабочий), 13 - used (Использован)
	 * Вызывается сразу после регистрации нового пользователя с параметром $statusId = 12
	 *
	 */
	public static function updateUserReferalStatus($UserEmail, $statusId, $userID=false){
	  global $APPLICATION, $USER;
	  if(intval($statusId)==0){
	  	$statusId = 11;
	  }

	  if($userID!=false){
	  	global $USER;
	  	  $rsUser = CUser::GetByID($userID);
	  	  $arUser = $rsUser->Fetch();
		  $UserEmail = $arUser["EMAIL"];
	  }

	  $USER_ID = intval($UserEmail);
	  if($USER_ID>0){
		 $db_res = CUser::GetList($o, $b, array("ID" => $USER_ID), array("SELECT"=>array("UF_PERSONAL_PROMO", "UF_PARTNER_STATUS", "UF_REFERAL_USER")));
		 $arUserPromo = array();
		 if($raw = $db_res->Fetch()){
			 $arUserPromo = array(
			  "UF_PERSONAL_PROMO"=>$raw["UF_PERSONAL_PROMO"],
			  "UF_PARTNER_STATUS"=>$raw["UF_PARTNER_STATUS"],
			  "UF_REFERAL_USER"=>$raw["UF_REFERAL_USER"],
			 );
			 $arLog["arUserPromo1"] = $arUserPromo;
			 if(empty($arUserPromo["UF_PERSONAL_PROMO"])){ //Если не заполнен промокод - проверим куки и заполним, если в куках он есть
			 	$promoVal = $APPLICATION->get_cookie("PERSONAL_PROMO");
				if(!empty($promoVal)){
				  $arUserPromo["UF_PERSONAL_PROMO"] = $promoVal;
				  $arUserPromo["UF_PARTNER_STATUS"] = $statusId; //Если передан статус 11 - значит пользователь авторизовался, а не зарегился, выключаем его промокод
				  $arUserPromo["UF_REFERAL_USER"] = self::getReferalByPromocode($promoVal);
				  $user = new CUser;
				  $user->Update($USER_ID, $arUserPromo);
				  $APPLICATION->set_cookie("PERSONAL_PROMO", ""); //очищаем куки с кодом
				}
			 } else if(!empty($arUserPromo["UF_PERSONAL_PROMO"]) && $statusId==13){  //Если подтверждаем, что использовали промокод
				  $arUserPromo["UF_PARTNER_STATUS"] = $statusId;
				  $user = new CUser;
				  $user->Update($USER_ID, $arUserPromo);
				  $APPLICATION->set_cookie("PERSONAL_PROMO", ""); //очищаем куки с кодом
			 }
		  }
	  }

	  define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/updateUserReferalStatus_log.txt");
     $arLog["statusId"] = $statusId;
     $arLog["userID"] = $userID;
     $arLog["UserEmail"] = $UserEmail;
     $arLog["arUser"] = $arUser;
     $arLog["USER_ID"] = $USER_ID;
     $arLog["arUserPromo2"] = $arUserPromo;
     $arLog["promoVal"] = $promoVal;

     AddMessage2Log('$arLog = '.print_r($arLog, true),'');


	}


	/**
	 * Возвращает массив с данными по всем рефералам данного пользователя
	 *
	 * @param  int   $userId
	 *
	 * @return array
	 *
	 * @access public
	 * @static
	 */
	public static function getUserReferalsToLK($userId, $full=false){
		  $arReferalsStat = array();
		  $userId = intval($userId);
	   if($userId>0){

		 global $USER;
         $rsUser = CUser::GetByID($userId);
         $arUser = $rsUser->Fetch();
		  $userEmail = "";
		 if(strpos($arUser["LOGIN"],"@")!==false){//Если логин в виде почты - используем
		  $userEmail = $arUser["LOGIN"];
		 } elseif(!empty($arUser["EMAIL"])){//Если логин не в виде почты, ищем в свойстве EMAIL - используем
		  $userEmail = $arUser["EMAIL"];
		 } else {//Если и EMAIL не указан - используем все же текстовый логин
		 	$userEmail = $arUser["LOGIN"];
		 }

		 if(!empty($userEmail)){
		 $arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_PROMOCODE", "PROPERTY_PROMOCODE_STATUS", "PROPERTY_EMAIL", "PROPERTY_USER", "PROPERTY_DATE_CREATE");
		 $arFilter = Array("IBLOCK_ID"=>IntVal(19), "PROPERTY_REFERAL"=>$userEmail,  );
		 	if($full==true){
		   $arGroupBy = false;
		 } else {
			$arGroupBy = array('PROPERTY_PROMOCODE_STATUS');
		 }
		 $res = CIBlockElement::GetList(Array(), $arFilter, $arGroupBy, false, $arSelect);
		 while($ob = $res->Fetch()){
		 	if($full==true){
			  $arReferalsStat[$ob["PROPERTY_PROMOCODE_STATUS_ENUM_ID"]][] = array("PROMOCODE"=>$ob["PROPERTY_PROMOCODE_VALUE"], "PROMOCODE_STATUS" => $ob["PROPERTY_PROMOCODE_STATUS_VALUE"],
			  																							 "USER_EMAIL"=>$ob["PROPERTY_EMAIL_VALUE"], "USER_ID"=>$ob["PROPERTY_USER_VALUE"], "DATE_CREATE"=>$ob["PROPERTY_DATE_CREATE_VALUE"]);
		 	} else {
			  $arReferalsStat[$ob["PROPERTY_PROMOCODE_STATUS_ENUM_ID"]] = $ob["CNT"];
		 	}

		 }
			}


		 }
		 return $arReferalsStat;
	}


	/**
	 * Проверяет, изменяет данные по персональному промокоду для пользователя перешедшего по реферальной ссылке и регистрирующегося или авторизующегося
	 * Функция-роутер для обратной совместимости
	 */
	public static function updateUserReferalStatusCRM($UserEmail, $statusId, $userID=false){
	  //$this->updateUserReferalStatusCRM_v1($UserEmail, $statusId, $userID); //Первоначальный вариант с тремя статусами
	  Promocodes::updateUserReferalStatusCRM_v2($UserEmail, $statusId, $userID); //Вариант со статусами рабочий/пустой
	}


	/**
	 * V 1 - Проверяет, изменяет данные по персональному промокоду для пользователя перешедшего по реферальной ссылке и регистрирующегося или авторизующегося [ для CRM (19 инфоблок) ]
	 * Значения для PROMOCODE_STATUS: 87 - disabled (Не рабочий), 88 - enabled (Рабочий), 89 - used (Использован)
	 * Вызывается сразу после регистрации нового пользователя с параметром $statusId = 88
	 *
	 */
	public static function updateUserReferalStatusCRM_v1($UserEmail, $statusId, $userID=false){
	  global $APPLICATION, $USER;
	  $crmIblockId = 19;
	  if(intval($statusId)==0){
	  	$statusId = 87;
	  }
	  CModule::IncludeModule("iblock");

	  if($userID!=false){
	  	global $USER;
	  	  $rsUser = CUser::GetByID($userID);
	  	  $arUser = $rsUser->Fetch();
		  $UserEmail = !empty($arUser["EMAIL"])?$arUser["EMAIL"]:$arUser["LOGIN"];
	  }
	  $USER_EMAIL = $UserEmail;
	  if(!empty($USER_EMAIL)){
		 $arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_PROMOCODE","PROPERTY_PROMOCODE_STATUS","PROPERTY_REFERAL");
       $arFilter = Array("IBLOCK_ID"=>IntVal($crmIblockId), "=PROPERTY_EMAIL"=>$USER_EMAIL);
       $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCnt"=>1), $arSelect);
       if($raw = $res->Fetch()){
			 $arUserPromo = array(
			  "PROMOCODE"=>$raw["PROPERTY_PROMOCODE_VALUE"],
			  "PROMOCODE_STATUS"=>$raw["PROPERTY_PROMOCODE_STATUS_ENUM_ID"],
			  "REFERAL"=>$raw["PROPERTY_REFERAL_VALUE"],
			 );
			 $arLog["arUserPromo1"] = $arUserPromo;

			 if(empty($arUserPromo["PROMOCODE"]) ){ //Если не заполнен промокод - проверим куки и заполним, если в куках он есть
			 	$promoVal = $APPLICATION->get_cookie("PERSONAL_PROMO");
				if(isset($_REQUEST["utm_personal"]) && !empty($_REQUEST["utm_personal"]) && empty($promoVal)){
				 $promoVal = trim($_REQUEST["utm_personal"]);
				}
				if(!empty($promoVal)){
				  $arUserPromo["PROMOCODE"] = $promoVal;
				  $arUserPromo["PROMOCODE_STATUS"] = $statusId; //Если передан статус 87 - значит пользователь авторизовался, а не зарегился, выключаем его промокод
				  $arUserPromo["REFERAL"] = self::getReferalByPromocodeCRM($promoVal);

				  foreach($arUserPromo as $PROPERTY_CODE=>$PROPERTY_VALUE){
				  CIBlockElement::SetPropertyValuesEx($raw["ID"], $crmIblockId, array($PROPERTY_CODE => $PROPERTY_VALUE));
					}
				  CIBlock::clearIblockTagCache($crmIblockId);
				  $APPLICATION->set_cookie("PERSONAL_PROMO", ""); //очищаем куки с кодом
/*		        $application = \Bitrix\Main\Application::getInstance();
		        $context = $application->getContext();
		        $context->getResponse()->flush('');*/

				}
			 } else if(!empty($arUserPromo["PROMOCODE"]) && $arUserPromo["PROMOCODE_STATUS"]==88 && $statusId==89){  //Если подтверждаем, что использовали промокод
				  CIBlockElement::SetPropertyValuesEx($raw["ID"], $crmIblockId, array("PROMOCODE_STATUS" => $statusId));
				   CIBlock::clearIblockTagCache($crmIblockId);
				  $APPLICATION->set_cookie("PERSONAL_PROMO", ""); //очищаем куки с кодом
			 }
		  }
	  }

	  define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/updateUserReferalStatusCRM_log.txt");
     $arLog["statusId"] = $statusId;
     $arLog["userID"] = $userID;
     $arLog["USER_EMAIL"] = $USER_EMAIL;
     $arLog["arUser"] = $arUser;
     $arLog["USER_ID"] = $USER_ID;
     $arLog["arUserPromo2"] = $arUserPromo;
     $arLog["promoVal"] = $promoVal;

     AddMessage2Log('$arLog = '.print_r($arLog, true),'');

	}

	/**
	 * V 2 - Проверяет, изменяет данные по персональному промокоду для пользователя перешедшего по реферальной ссылке и регистрирующегося или авторизующегося
	 * Значения для PROMOCODE_STATUS: 87 - disabled (Не рабочий), 88 - enabled (Рабочий)
	 * Вызывается сразу после регистрации нового пользователя с параметром $statusId = 88 или после авторизации для проверки статуса промокода или его затирания в пустое
	 *
	 */
	public static function updateUserReferalStatusCRM_v2($UserEmail, $statusId, $userID=false){
	  global $APPLICATION, $USER;
	  $crmIblockId = 19;
	  if(intval($statusId)==0 || intval($statusId)==87){
	  	$statusId = false;
	  }

	  CModule::IncludeModule("iblock");

	  if($userID!=false){  //Получаем почту пользователя если его ID передан. Из логина или св-ва EMAIL
	  	global $USER;
	  	  $rsUser = CUser::GetByID($userID);
	  	  $arUser = $rsUser->Fetch();
		  $UserEmail = !empty($arUser["EMAIL"])?$arUser["EMAIL"]:$arUser["LOGIN"];
	  }
	  $USER_EMAIL = $UserEmail;

	  if(!empty($USER_EMAIL)){ //Если есть почта - можно искать в ИБ CRM 19
		 $arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_PROMOCODE","PROPERTY_PROMOCODE_STATUS","PROPERTY_REFERAL");
       $arFilter = Array("IBLOCK_ID"=>IntVal($crmIblockId), "=PROPERTY_EMAIL"=>$USER_EMAIL);
       $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCnt"=>1), $arSelect);
       if($raw = $res->Fetch()){
			 $arUserPromo = array(
			  "PROMOCODE"=>$raw["PROPERTY_PROMOCODE_VALUE"],
			  "PROMOCODE_STATUS"=>$raw["PROPERTY_PROMOCODE_STATUS_ENUM_ID"],
			  "REFERAL"=>$raw["PROPERTY_REFERAL_VALUE"],
			 );
			 $arLog["arUserPromo1"] = $arUserPromo;

			 if(empty($arUserPromo["PROMOCODE"]) ){ //Если не заполнен промокод - проверим куки и заполним, если в куках он есть
			 	$promoVal = $APPLICATION->get_cookie("PERSONAL_PROMO");
				if(isset($_REQUEST["utm_personal"]) && !empty($_REQUEST["utm_personal"]) && empty($promoVal)){
				 $promoVal = trim($_REQUEST["utm_personal"]);
				}
				if(!empty($promoVal)){
				  $arUserPromo["PROMOCODE"] = $promoVal;
				  $arUserPromo["PROMOCODE_STATUS"] = $statusId; //Если передан статус 87 - значит пользователь авторизовался, а не зарегился, выключаем его промокод
				  $arUserPromo["REFERAL"] = self::getReferalByPromocodeCRM($promoVal);

				  foreach($arUserPromo as $PROPERTY_CODE=>$PROPERTY_VALUE){
				  CIBlockElement::SetPropertyValuesEx($raw["ID"], $crmIblockId, array($PROPERTY_CODE => $PROPERTY_VALUE));
					}
				  CIBlock::clearIblockTagCache($crmIblockId);
				  $APPLICATION->set_cookie("PERSONAL_PROMO", ""); //очищаем куки с кодом
/*		        $application = \Bitrix\Main\Application::getInstance();
		        $context = $application->getContext();
		        $context->getResponse()->flush('');*/

				}
			 } else if(!empty($arUserPromo["PROMOCODE"]) && $arUserPromo["PROMOCODE_STATUS"]==88 && $statusId==89){  //Если подтверждаем, что использовали промокод
				  CIBlockElement::SetPropertyValuesEx($raw["ID"], $crmIblockId, array("PROMOCODE_STATUS" => false)); //Затираем статус промокода
				   CIBlock::clearIblockTagCache($crmIblockId);
				  $APPLICATION->set_cookie("PERSONAL_PROMO", ""); //очищаем куки с кодом
			 }
		  }
	  }


	}

	/**
	 * Получает пользователя-реферала по промокоду
	 */
	public static function getReferalByPromocode($pCode){
	 global $APPLICATION, $USER;
	 CModule::IncludeModule("iblock");
		$result = 0;
		$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_USER");
		$arFilter = Array("IBLOCK_ID"=>IntVal(36), "NAME"=>$pCode);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCnt"=>1), $arSelect);
		while($arFields = $res->Fetch()){
		  $result = $arFields["PROPERTY_USER_VALUE"];
		}
		unset($arFields,$res,$arSelect,$arFilter);
		return $result;
	}

	/**
	 * Получает email пользователя-реферала по промокоду для CRM (19 инфоблок)
	 */
	public static function getReferalByPromocodeCRM($pCode){
	 global $APPLICATION, $USER;
	 CModule::IncludeModule("iblock");
		$result = '';
		$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_USER");
		$arFilter = Array("IBLOCK_ID"=>IntVal(36), "NAME"=>$pCode);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCnt"=>1), $arSelect);
		if($arFields = $res->Fetch()){
		 $db_res = CUser::GetList($o, $b, array("ID"=>$arFields["PROPERTY_USER_VALUE"]), array("SELECT"=>array("ID", "LOGIN", "EMAIL", "UF_REFERAL_USER", "UF_PARTNER_STATUS", "UF_PERSONAL_PROMO")));
		 if($raw = $db_res->Fetch()){
		 	if(empty($raw['EMAIL'])){
		 		$result = $raw['LOGIN'];
		 	} else {
				$result = $raw['EMAIL'];
		 	}

		}
		}
		unset($arFields,$res,$arSelect,$arFilter, $raw);
		return $result;
	}

	/**
	 * Возвращает персональный промокод, если он существует и в статусе "рабочий"
	 */
	public static function getRefPersonalPromo($UserId){
	  global $USER;
	  $return = '';
	  $USER_ID = intval($UserId);
	  $db_res = CUser::GetList($o, $b, array("ID_EQUAL_EXACT" => $USER_ID), array("SELECT"=>array("UF_PERSONAL_PROMO", "UF_PARTNER_STATUS", "UF_REFERAL_USER")));
	   if($raw = $db_res->Fetch()){

		  if(!empty($raw["UF_PERSONAL_PROMO"]) && intval($raw["UF_PARTNER_STATUS"])==12){ //Если есть промокод и он "рабочий"
			 $return = $raw["UF_PERSONAL_PROMO"];
		  }


		 }
		 return $return;
	}

	/**
	 * Возвращает персональный промокод, если он существует и в статусе "рабочий" [для CRM (19 инфоблок)]
	 */
	public static function getRefPersonalPromoCRM($UserId){
	  global $USER;
	  $return = '';
	  $crmIblockId = 19;
	  CModule::IncludeModule("iblock");

       $rsUser = CUser::GetByID($UserId);
       $arUser = $rsUser->Fetch();

	  $USER_EMAIL = $arUser["EMAIL"];

	  if(!empty($USER_EMAIL)){
		 $arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_PROMOCODE","PROPERTY_PROMOCODE_STATUS","PROPERTY_REFERAL");
       $arFilter = Array("IBLOCK_ID"=>IntVal($crmIblockId), "=PROPERTY_EMAIL"=>$USER_EMAIL);
       $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCnt"=>1), $arSelect);
       if($raw = $res->Fetch()){

		  if(!empty($raw["PROPERTY_PROMOCODE_VALUE"]) && intval($raw["PROPERTY_PROMOCODE_STATUS_ENUM_ID"])==88){ //Если есть промокод и он "рабочий"
			 $return = $raw["PROPERTY_PROMOCODE_VALUE"];
		  }
		 }
		 }
		 return $return;
	}

	/**
	 * Отмечаем, что промокод использован
	 */
	public static function setRefPersonalPromoUsed($UserId){
		 global $USER;
		 $USER_ID = intval($UserId);
		 $arUserPromo["UF_PARTNER_STATUS"] = 13; //использован
	    $user = new CUser;
	    $user->Update($USER_ID, $arUserPromo);
	}



	/**
	 * Проверка персонального промокода на двойное использование
	 */
	public static function checkPersonalPromoUses($code, $userId){
		$promoItem = self::isPersonalPromo($code);
		if($promoItem){
			$hl = self::personalPromoLoader();

			$res = $hl::getList(array(
				"filter" => array(
					"UF_USER" => $userId,
					"UF_OWNER" => $promoItem["PROPERTY_USER_VALUE"],
				),
			));
			if($res->getSelectedRowsCount()){
				return true;
			}
		}
	}

	/**
	 * Таблица проверки на двойное использование персонального промокода
	 */	
	public static function addPersonalPromoUses($user, $owner){
		$hl = self::personalPromoLoader();


		$hl::add(array(
			"UF_USER" => $user,
			"UF_OWNER" => $owner
		));
	}

	/**
	 * Проверка промокода - персональный
	 */
	public static function isPersonalPromo($code){
		$promoItem = parent::getIbItem(
			Array("IBLOCK_ID" => parent::$ibIdPromo, "NAME"=>$code, "ACTIVE" => "Y"),
			array("ID", "PROPERTY_TYPE", "PROPERTY_USER", "PROPERTY_DATE_END", "PROPERTY_SET", "PROPERTY_SET_TYPE", "PROPERTY_USED")
		);
		if($promoItem){
			return $promoItem;
		}
	}

	/**
	 * Установка промокода куку
	 */
	public static function setCookiePromo($code){
		if(!$code){
			return false;
		}

		if($item = self::isPersonalPromo($code)){
			global $USER;
			if($USER->IsAuthorized()){
				if($USER->GetID()==$item["PROPERTY_USER_VALUE"]){
					return false;
				}
			}

			//Считаем переходы по utm метке если куки с таким кодом еще нет у клиента
			$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
			$cookieValue = $request->getCookie("PERSONAL_PROMO");
			if($cookieValue!=$code){
			self::addUtmCountPromo($code);
			}

			global $APPLICATION;
			//$APPLICATION->set_cookie("PERSONAL_PROMO", $code, time()+60*60*24*30*12*2);
			$APPLICATION->set_cookie("PERSONAL_PROMO", $code, time()+60*60*24*7);



		}
	}

	public static function addUtmCountPromo($code){
		 $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_UTM_STAT_CLICK");
       $arFilter = Array("IBLOCK_ID"=>IntVal(36), "NAME"=>trim(htmlspecialchars($code)));
       $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCnt"=>1), $arSelect);
       if($ob = $res->fetch()){
		  $curCnt = intval($ob["PROPERTY_UTM_STAT_CLICK_VALUE"]);
		  $curCnt++;
		  CIBlockElement::SetPropertyValuesEx($ob["ID"], 36, array("UTM_STAT_CLICK" => $curCnt));
       }
	}


	/**
	 * Получение списка для лк
	 */
	public static function getUserPromocodes($userId){
		if(!$userId){
			return false;
		}

		$promoItems = BasePromo::getIbItems(
			Array("IBLOCK_ID" => BasePromo::$ibIdPromo, "PROPERTY_USER"=>$userId, "PROPERTY_USED"=>false, "ACTIVE" => "Y", ">=PROPERTY_DATE_END" => date("Y-m-d")),
			array("ID", "NAME", "PROPERTY_TYPE", "PROPERTY_USER", "PROPERTY_DATE_END", "PROPERTY_SET", "PROPERTY_SET_TYPE", "PROPERTY_USED", "PROPERTY_UTM_STAT_CLICK")
		);

		$arReturn = array();
		foreach($promoItems as $item){
			$type = parent::getSelectType($item["PROPERTY_TYPE_VALUE"], parent::$ibIdPromo, "TYPE");

			$arReturn[$type["XML_ID"]][] = $item;
		}

		return $arReturn;
	}

	/**
	 * Получение промокода
	 */
	public static function getPromocode($code, $userId=false, $summ){
		//Если без параметров
		if(!$code && !$summ){
			return false;
		}

		$promoItem = parent::getIbItem(
			Array("IBLOCK_ID" => parent::$ibIdPromo, "NAME"=>$code, "ACTIVE" => "Y"),
			array("ID", "PROPERTY_TYPE", "PROPERTY_USER", "PROPERTY_DATE_END", "PROPERTY_SET", "PROPERTY_SET_TYPE", "PROPERTY_USED")
		);


		//Можно ли использовать промокод
		if(is_array($promoItem)){
			if(self::canUseCode($promoItem, $userId)){
				return self::formOrderPromoItem($promoItem, $summ);
			}
		}
	}

	/**
	 * Установить промокод на след. покупку как использованный
	 */
	public static function setPromocodeAsUsed($code){
		self::loader();

		$promoItem = parent::getIbItem(
			Array("IBLOCK_ID" => parent::$ibIdPromo, "PROPERTY_TYPE"=>54, "NAME"=>$code, "ACTIVE" => "Y"),
			array("ID")
		);
		if($promoItem){
			CIBlockElement::SetPropertyValues($promoItem["ID"], parent::$ibIdPromo, 59, "USED");
			parent::deactivateItem($promoItem["ID"]);
		}
	}


	/**
	 * Деактивация промокодов по крону
	 */
	public static function cronDeactivateCodes(){
		self::loader();

		$filter = array(
			"IBLOCK_ID" => parent::$ibIdPromo,
			"<PROPERTY_DATE_END" => date("Y-m-d")
		);
		$res = CIBlockElement::GetList(Array(), $filter, false, false, array("ID"));
		while ($ob = $res->GetNext()) {
			parent::deactivateItem($ob["ID"]);
		}
	}

	/**
	 * Элемент для возврата на форму
	 */
	public static function formOrderPromoItem($promoItem, $summ){
		$return = array(
			"have" => true
		);

		$setType = parent::getSelectType($promoItem["PROPERTY_SET_TYPE_VALUE"], parent::$ibIdPromo, "SET_TYPE");
		$type = parent::getSelectType($promoItem["PROPERTY_TYPE_VALUE"], parent::$ibIdPromo, "TYPE");

		$return["summ"] = $promoItem["PROPERTY_SET_VALUE"];
		if($setType["EXTERNAL_ID"]=="PERCENT"){
			$return["summ"] = round($summ/100*$promoItem["PROPERTY_SET_VALUE"]);
		}
		$return["type"] = $type["EXTERNAL_ID"];

		return $return;
	}

	/**
	 * Можно ли использовать промокод
	 */
	public static function canUseCode($promoItem, $userId=false){
		$avail = true;

		//проверим, не истек ли
		$dt = new DateTime($promoItem["PROPERTY_DATE_END_VALUE"]);
		if($dt->getTimestamp()<time()){
			$avail = false;

			parent::deactivateItem($promoItem["ID"]);
		}

		//проверим, не использован ли
		if($promoItem["PROPERTY_USED_VALUE"]){
			$avail = false;

			parent::deactivateItem($promoItem["ID"]);
		}

		//проверим по типу
		if($avail){
			if(!self::checkPromoByType($promoItem, $userId)){
				$avail = false;
			}
		}



		return $avail;
	}

	/**
	 * Проверка промокода по типу
	 */
	public static function checkPromoByType($promoItem, $userId=false){
		$avail = true;

		//Если персональный и текущий пользователь = владелец
		if($userId){
			//if($promoItem["PROPERTY_TYPE_ENUM_ID"]==56 && $userId==$promoItem["PROPERTY_USER_VALUE"]){
			if($userId==$promoItem["PROPERTY_USER_VALUE"]){
			if($promoItem["PROPERTY_TYPE_ENUM_ID"]==56)
				$avail = false;
			}
		}



		return $avail;
	}
}