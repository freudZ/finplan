<?
class BaseBonuses
{
	/**
	 * Загрузчик
	 */
	public function loader($withHighload = false)
	{
		\Bitrix\Main\Loader::includeModule("iblock");
		
		if($withHighload){
			\Bitrix\Main\Loader::includeModule("highloadblock");
			
			$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(18)->fetch();
			$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();
			
			return $entityClass;
		}
	}
	
	/**
	 * Добавление элемента
	 */
	public static function addItem($fields){
		$hl = self::loader(true);
		
		$hl::add($fields);
	}
	
	/**
	 * Элементы получение
	*/
	public static function getItems($filter, $select, $sort = array("ID" => "desc"))
	{
		$hl = self::loader(true);
		
		$return = [];
		$res = $hl::getList(array(
			"filter" => $filter,
			"select" => $select,
			"order" => $sort,
		));
		while($item = $res->fetch()){
			$return[] = $item;
		}
		
		return $return;
	}
}