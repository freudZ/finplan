<?

use Bitrix\Highloadblock as HL;

//Графики с биржы
class MoexGraph
{
    private $hlId;
    private $filter;
    private $dataClasses = [];
    private $hlTable;
    private $from;
    private $interval;
    private $period;


    public function __construct()
    {
        \Bitrix\Main\Loader::includeModule("highloadblock");
    }


    public function getForAction($code)
    {
        $this->hlId = 24;
        $this->hlTable = "actions";
        $this->filter = [
            "UF_ITEM" => $code
        ];

        return $this->getForItem();
    }

    public function getForObligation($code)
    {
        $this->hlId = 25;
        $this->hlTable = "obligations";
        $this->filter = [
            "UF_ITEM" => $code
        ];

        return $this->getForItem();
    }

    public function getForActionByMonthWithFrom($code, $date)
    {
        $this->hlId = 24;
        $this->hlTable = "actions";
        $this->from = $date;
        $this->interval = 31;
        $this->filter = [
            "UF_ITEM" => $code
        ];

        $res = $this->getForItem();
        $items = json_decode($res["items"], true);
        foreach ($items as $n => $item) {
            $dt = new DateTime($item[0]);
            $items[$n] = [
                FormatDate("M. Y", $dt->getTimestamp()),
                $item[4],
            ];
        }

        return $items;
    }


    private function getForItem()
    {
        $arChartData = [];

		  global $USER;
		        $rsUser = CUser::GetByID($USER->GetID());
		        $arUser = $rsUser->Fetch();
		        if($arUser["LOGIN"]!="freud"){
               return $arChartData;
		        } else {
					$arChartData = $this->getForItem2($cacheLifetime);
					return $arChartData;
		        }

        $obCache = new CPHPCache();
        //$cacheLifetime = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")) - strtotime("now");
        $cacheLifetime = 3600;
        $cacheLifetime = 0;
        $cacheID = 'item' . $this->filter["UF_ITEM"]."-".$this->from."-".$this->interval;
        $cachePath = '/graph_new_hiload_id_' . $this->hlId . '/';

        if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
            $arChartData = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            if($this->from && $this->interval) {
                $interval = $this->interval;
                $from = new DateTime($this->from);
                $to = new DateTime($this->getToDate());
            } else {
                $from = new DateTime($this->getFromDate());
                $arChartData["real_from"] = $from;

                $to = new DateTime($this->getToDate());

                $diff = $from->diff($to);

                //тики через сколько дней
                $interval = 31;
                if ($diff->y < 1) {
                    $interval = 7;
                }
            }

            $arChartData["tick"] = intval($diff->days / 6 / 7) . " weeks";

            //получим период
            $period = new DatePeriod(
                $from,
                new DateInterval('P' . $interval . 'D'),
                $to
            );

            //сформируем даты по периоду
            $getDates = [];
            $prevValue = $from;
            foreach ($period as $key => $value) {
                if ($interval == 31) {
                    $getDates[] = [
                        "from" => date("Y-m-01", $value->getTimestamp()),
                        "to" => date("Y-m-t", $value->getTimestamp()),
                    ];
                } elseif ($interval == 7) {
                    if (!$key) {
                        continue;
                    }
                    $to = clone $value;
                    $to->modify("-1day");

                    $getDates[] = [
                        "from" => $prevValue->format("Y-m-d"),
                        "to" => $to->format("Y-m-d"),
                    ];
                    $prevValue = $value;
                }
            }

            //последний не полный интервал
            $dt = new DateTime($getDates[count($getDates) - 1]["to"]);
            $dt->modify("+1day");

            $getDates[] = [
                "from" => $dt->format("Y-m-d"),
                "to" => date("Y-m-d"),
            ];

            $connection = \Bitrix\Main\Application::getConnection();

            $queries = [];

            foreach ($getDates as $range) {
                 $query = "select MAX(UF_DATE) as UF_DATE, 
                        (select UF_OPEN from `hl_moex_" . $this->hlTable . "_data` WHERE UF_DATE >= '" . $range["from"] . "' and UF_ITEM = '" . $this->filter["UF_ITEM"] . "' order by UF_DATE asc limit 1) as UF_OPEN,
                        (select UF_CLOSE from `hl_moex_" . $this->hlTable . "_data` WHERE UF_DATE <= '" . $range["to"] . "' and UF_ITEM = '" . $this->filter["UF_ITEM"] . "' order by UF_DATE desc limit 1) as UF_CLOSE,
                        MAX(UF_HIGH) as UF_HIGH,
                        MIN(UF_LOW) as UF_LOW  from `hl_moex_" . $this->hlTable . "_data` WHERE UF_DATE >= '" . $range["from"] . "' and UF_DATE <= '" . $range["to"] . "' and UF_ITEM = '" . $this->filter["UF_ITEM"] . "'";
                 $queries[] = $query;
            }

            if ($queries) {
                $res = $connection->query(implode(" UNION ", $queries));

                while ($item = $res->fetch()) {
                    if (!$item["UF_DATE"]) {
                        continue;
                    }

                    $arChartData["items"][] = [
                        $item["UF_DATE"]->format("m/d/Y H:i:s"),
                        (float)$item["UF_OPEN"],
                        (float)$item["UF_HIGH"],
                        (float)$item["UF_LOW"],
                        (float)$item["UF_CLOSE"],
                    ];
                }
            }

            if (!$arChartData["items"]) {
                return [];
            }

            //min и max
            $min = new DateTime($arChartData["items"][0][0]);
            $arChartData["min"] = $min->format("m-d-Y");

            $max = new DateTime($arChartData["items"][count($arChartData["items"]) - 1][0]);
            $arChartData["max"] = $max->format("m-d-Y");


            $arChartData["items"] = json_encode($arChartData["items"]);
            $obCache->EndDataCache($arChartData);
        }

        return $arChartData;
    }

	 private function getForItem2($cacheTime){
	 	$arChartData = array();
		$arDatesAndTicks = $this->getDatesAndTicks();

                $from = $arDatesAndTicks["real_from"];
                $to = new DateTime($arDatesAndTicks["max"]);
                $diff = $from->diff($to);

					 $daysDiff = intval($diff->days);


                //тики через сколько дней
                $interval = 31;

					 $this->period = "M";

					 if($daysDiff<=31){
                	  $interval = 1;
						  $this->period = "D";
						  $arDatesAndTicks["tick"] = 1 . ' days';
					 } else if($daysDiff>31 && $daysDiff<=120){
                    $interval = 7;
						  $this->period = "W";
						  $arDatesAndTicks["tick"] = 7 . ' days';
					 } else {
						  $arDatesAndTicks["tick"] = floor($daysDiff/7/12) . ' weeks';
					 }


        if (array_key_exists("UF_BASE_TICKER", $this->filter)) { //Если получаем данные для графика фьючерсного контракта со склейкой по UF_BASE_TICKER
            $cacheId = 'item' . $this->filter["UF_BASE_TICKER"] . "-" . $this->from . "-" . $arDatesAndTicks["interval"];
        } else { //Для всех остальных графиков
            $cacheId = 'item' . $this->filter["UF_ITEM"] . "-" . $this->from . "-" . $arDatesAndTicks["interval"];
        }
/*			echo "<pre  style='color:black; font-size:11px;'>";
            print_r($cacheId);
            echo "</pre>";*/
	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cacheTtl = 86400 * 7;
       //$cache->clean("usa_actions_data");
	if ($cache->read($cacheTtl, $cacheId)) {
            $arChartData = $cache->get($cacheId);
        } else {
	      $this->calculateCandleHL();

			$arChartData["tick"] = $arDatesAndTicks["tick"];
			$arChartData["min"] = (new DateTime($arDatesAndTicks["min"]))->format('m-d-Y');
			$arChartData["max"] = (new DateTime($arDatesAndTicks["max"]))->format('m-d-Y');
			$arChartData["real_from"] = $arDatesAndTicks["real_from"];
	      $arChartData["items"] = json_encode($this->getCandleData());
			$cache->set($cacheId, $arChartData);
        }


		 return $arChartData;
	 }


	 /**
	  * Получает рассчитанные данные для свечей  в зависимости от периода. Если период день - то данные берет из таблицы котировок
	  *
	  * @return array массив с данными свечей
	  *
	  * @access private
	  */
	 private function getCandleData(){
		Global $DB;
		$arResult = array();
		$code = $this->filter["UF_ITEM"];
		if($this->period!='D'){
		  $hlCandleTableName = "hl_candle_graph_data";
		  $hlTickerField = "UF_TICKER";
		  $hlOrderField = "UF_DATE_TO";
		  $queryPeriod = " AND `UF_PERIOD`='$this->period'";
		} else {
		  $hlCandleTableName = "hl_moex_" . $this->hlTable . "_data";
		  $hlTickerField = "UF_ITEM";
		  $hlOrderField = "UF_DATE";
		  $queryPeriod = "";
		}

		$query = "SELECT * FROM `$hlCandleTableName` WHERE `$hlTickerField`='$code' $queryPeriod ORDER BY `$hlOrderField` ASC";
		$res = $DB->Query($query);
		while($row = $res->fetch()){
		  $arResult[] = [(new DateTime($row[$hlOrderField]))->format("m/d/Y H:i:s"), $row["UF_OPEN"], $row["UF_HIGH"], $row["UF_LOW"], $row["UF_CLOSE"],];
		}
		 return $arResult;
	 }


	 /**
	  * Расчитывает и записывает в БД данные для свечей по активам РФ
	  *
	  * @access private
	  */
	 private function calculateCandleHL(){
	 	Global $DB;
		$code = $this->filter["UF_ITEM"];
		$hlCandleTableName = "hl_candle_graph_data";
		$hlTableName = "hl_moex_" . $this->hlTable . "_data";
		//$hlTableName = "hl_spb_actions_data";
		  //Получим последние даты расчитанных свечей (по последним записям месячного диапазона) для всех акций
		  $querySelectAllCandles = "SELECT `ID`, `UF_TICKER`, `UF_PERIOD`, `UF_DATE_FROM`, `UF_DATE_TO` FROM `$hlCandleTableName` WHERE `UF_CURRENT_PERIOD`='Y' ORDER BY 'UF_DATE_TO' DESC";
		  $arAllCandles = array();
		  $res = $DB->Query($querySelectAllCandles);
		  while($row = $res->fetch()){
			 $arAllCandles[$row["UF_TICKER"]][$row["UF_PERIOD"]] = $row;
		  }

		  //Если в выборке свечей нет записи с текущим периодом - то на всякий случай затираем данные по текущему активу полностью и перезаписываем всю историю вместе с текущим периодом
		  //Эта ситуация так же возникает, если была удалена запись для текущего месяца.
			if(!array_key_exists('M', $arAllCandles[$code]) || !array_key_exists('W', $arAllCandles[$code])){
			  $queryClear = "DELETE FROM `$hlCandleTableName` WHERE `UF_TICKER` = '$code'";
			  $DB->StartTransaction();
			  $DB->Query($queryClear);
			  $DB->Commit();
			  $queryClear = '';
			  $arAllCandles = array();
			}

		  $query = "SELECT * FROM `$hlTableName` WHERE `UF_ITEM` = '$code'";
		  $hasDateFilter = false;
		  if(array_key_exists($code, $arAllCandles)){
			 if(!empty($arAllCandles[$code]['M']['UF_DATE_FROM']) && !empty($arAllCandles[$code]['W']['UF_DATE_FROM'])){
				$query .= " AND `UF_DATE`>='".(new DateTime($arAllCandles[$code]['M']['UF_DATE_FROM']))->format('Y-m-d')."'";
				$hasDateFilter = true;
			 }
		  }
		  $query .= " ORDER BY `UF_DATE` ASC";
		  $res = $DB->Query($query);

		  $arMonths = array();
		  $arWeeks = array();

		  while($row = $res->fetch()){
		  	$UF_CURRENT_PERIOD_MONTH = (new DateTime($row["UF_DATE"]))->getTimestamp()>=(new DateTime())->modify('first day of this month')->getTimestamp()?'Y':'';
		  	$UF_CURRENT_PERIOD_WEEK = (new DateTime($row["UF_DATE"]))->getTimestamp()>=(new DateTime())->modify('this week monday')->getTimestamp()?'Y':'';

			//дата для месяцев
			$dateMonth = (new DateTime($row["UF_DATE"]))->format('m.Y');
			//дата для недель
			$dateWeek = (new DateTime($row["UF_DATE"]))->format('W.m.Y'); //Номер недели, месяц, год


			//Расчет свечей для диапазона "месяц"
			//Если начинаем новый месяц
			if(!array_key_exists($dateMonth, $arMonths)){
			 $arMonths[$dateMonth]['UF_TICKER'] = $code;
			 $arMonths[$dateMonth]['UF_DATE_FROM'] = $row["UF_DATE"];
			 $arMonths[$dateMonth]['UF_DATE_TO'] = $row["UF_DATE"];
			 $arMonths[$dateMonth]['UF_PERIOD'] = "M"; //Диапазон - месяц
			 $arMonths[$dateMonth]['UF_CURRENT_PERIOD'] = $UF_CURRENT_PERIOD_MONTH;
			 $arMonths[$dateMonth]['UF_OPEN'] = $row["UF_OPEN"];
			 $arMonths[$dateMonth]['UF_HIGH'] = $row["UF_HIGH"];
			 $arMonths[$dateMonth]['UF_LOW'] = $row["UF_LOW"];
			 $arMonths[$dateMonth]['UF_CLOSE'] = $row["UF_CLOSE"];
			} else { //Если считаем для существующего месяца
			 $arMonths[$dateMonth]['UF_DATE_TO'] = $row["UF_DATE"];
			 if($arMonths[$dateMonth]['UF_HIGH']<$row["UF_HIGH"]){
			   $arMonths[$dateMonth]['UF_HIGH'] = $row["UF_HIGH"];
				}
			 if($arMonths[$dateMonth]['UF_LOW']>$row["UF_LOW"]){
			  	$arMonths[$dateMonth]['UF_LOW'] = $row["UF_LOW"];
			  }
			 $arMonths[$dateMonth]['UF_CLOSE'] = $row["UF_CLOSE"];
			}

			//Расчет свечей для диапазона "неделя"
			//Если начинаем новый месяц
				$add = false;
		     if($hasDateFilter && $UF_CURRENT_PERIOD_WEEK=='Y'){
		     	$add = true;
		     }else if($hasDateFilter && $UF_CURRENT_PERIOD_WEEK!='Y'){
		     	$add = false;
		     }else if(!$hasDateFilter){
				$add = true;
		     }

			 if($add){
			if(!array_key_exists($dateWeek, $arWeeks)){
			 $arWeeks[$dateWeek]['UF_TICKER'] = $code;
			 $arWeeks[$dateWeek]['UF_DATE_FROM'] = $row["UF_DATE"];
			 $arWeeks[$dateWeek]['UF_DATE_TO'] = $row["UF_DATE"];
			 $arWeeks[$dateWeek]['UF_PERIOD'] = "W"; //Диапазон - неделя
			 $arWeeks[$dateWeek]['UF_CURRENT_PERIOD'] = $UF_CURRENT_PERIOD_WEEK;
			 $arWeeks[$dateWeek]['UF_OPEN'] = $row["UF_OPEN"];
			 $arWeeks[$dateWeek]['UF_HIGH'] = $row["UF_HIGH"];
			 $arWeeks[$dateWeek]['UF_LOW'] = $row["UF_LOW"];
			 $arWeeks[$dateWeek]['UF_CLOSE'] = $row["UF_CLOSE"];
			} else { //Если считаем для существующей недели
			 $arWeeks[$dateWeek]['UF_DATE_TO'] = $row["UF_DATE"];
			 if($arWeeks[$dateWeek]['UF_HIGH']<$row["UF_HIGH"]){
			   $arWeeks[$dateWeek]['UF_HIGH'] = $row["UF_HIGH"];
				}
			 if($arWeeks[$dateWeek]['UF_LOW']>$row["UF_LOW"]){
			  	$arWeeks[$dateWeek]['UF_LOW'] = $row["UF_LOW"];
			  }
			 $arWeeks[$dateWeek]['UF_CLOSE'] = $row["UF_CLOSE"];
			}
			} //if add == true for week

		  } //while расчета свечей

		  $this->savePeriodsToDB($hlCandleTableName, $arAllCandles, $code, $arMonths, "M");
		  $this->savePeriodsToDB($hlCandleTableName, $arAllCandles, $code, $arWeeks, "W");

	 }


	/**
	 * Сохраняет расчет свечей в БД (добавляет или обновляет текущие периоды)
	 *
	 * @param  string   $hlCandleTableName название таблицы данных для свечей
	 * @param  link to array   $arAllCandles ссылка на массив с выбранными текущими периодами
	 * @param  string   $ticker тикер (isin) бумаги для которой строим график
	 * @param  array   $arData рассчитанные данные для какого то из периодов для записи в БД
	 * @param  string   $period период для записи в БД "M", "W"
	 *
	 */
	function savePeriodsToDB($hlCandleTableName, &$arAllCandles, $ticker=false, $arData=array(), $period=false){
	  Global $DB;
	  if(!$ticker || !$period || count($arData)<=0 || empty($hlCandleTableName)) return false; //Если переданы не все данные - выходим
	  //Запись в БД рассчитанных данных для свечей месяцев

	  foreach($arData as $k=>$value){

	        $DB->PrepareFields($hlCandleTableName);


                    $value["UF_CLOSE"] = (float)$value["UF_CLOSE"] == 0 ? (float)$value["UF_OPEN"] : (float)$value["UF_CLOSE"];
                    $value["UF_OPEN"] = (float)$value["UF_OPEN"] == 0 ? (float)$value["UF_CLOSE"] : (float)$value["UF_OPEN"];
                    $value["UF_HIGH"] = (float)$value["UF_HIGH"] == 0 ? $value["UF_CLOSE"] : (float)$value["UF_HIGH"];
                    $value["UF_LOW"] = (float)$value["UF_LOW"] == 0 ? $value["UF_CLOSE"] : (float)$value["UF_LOW"];

                    if ($value["UF_CLOSE"] == 0 && $value["UF_OPEN"] == 0 && ($value["UF_HIGH"] == 0 || $value["UF_LOW"] == 0)) {
                        $valOpenClose = $value["UF_HIGH"] > 0 ? $value["UF_HIGH"] : $value["UF_LOW"];
                        $value["UF_CLOSE"] = $valOpenClose;
                        $value["UF_OPEN"] = $valOpenClose;
                        $value["UF_HIGH"] = $value["UF_HIGH"] == 0 ? $value["UF_LOW"] : $value["UF_HIGH"];
                        $value["UF_LOW"] = $value["UF_LOW"] == 0 ? $value["UF_HIGH"] : $value["UF_LOW"];
                    }

	        $arFields = array(
	            "UF_TICKER"                    => "'".trim($value['UF_TICKER'])."'",
	            "UF_DATE_FROM"                 => "'".trim($value['UF_DATE_FROM'])."'",
	            "UF_DATE_TO"                   => "'".trim($value['UF_DATE_TO'])."'",
	            "UF_PERIOD"                    => "'".trim($value['UF_PERIOD'])."'",
	            "UF_CURRENT_PERIOD"            => "'".trim($value['UF_CURRENT_PERIOD'])."'",
	            "UF_OPEN"            			 => "'".floatval($value['UF_OPEN'])."'",
	            "UF_HIGH"            			 => "'".floatval($value['UF_HIGH'])."'",
	            "UF_LOW"            			    => "'".floatval($value['UF_LOW'])."'",
	            "UF_CLOSE"            			 => "'".floatval($value['UF_CLOSE'])."'",
	            );

	        $DB->StartTransaction();
			  //Если запись существует то обновляем ее по ID
	        if ($arAllCandles[$value['UF_TICKER']][$period]['UF_DATE_TO']==$value['UF_DATE_TO'])
	        {
				  if($value['UF_CURRENT_PERIOD']=='Y'){
				  	 $ID = $arAllCandles[$value['UF_TICKER']][$period]['ID'];
	             $DB->Update($hlCandleTableName, $arFields, "WHERE ID='".$ID."'", $err_mess.__LINE__);
				  }
	        }
	        else //Иначе добавляем новую запись
	        {
	           $ID = $DB->Insert($hlCandleTableName, $arFields, $err_mess.__LINE__);
	        }
	        if (strlen($strError)<=0)
	        {
	            $DB->Commit();
	        }
	        else {
	        	CLogger::MoexGraphCandleHLError($strError);
	        	$DB->Rollback();
				}
	  }
	}

	 /**
	  * Возвращает минимальную и максимальную даты для построения графика, так же тики в неделях
	  *
	  * @param  string   $ticker тикер или isin бумаге
	  *
	  * @return array  результат в массиве
	  *
	  * @access private
	  */
	 private function getDatesAndTicks(){
	 	Global $DB;
		$arReturn = array();
		if(empty($this->filter["UF_ITEM"])) return $arReturn;
		$tableName = 'hl_candle_graph_data';
		$hlTableName = "hl_moex_" . $this->hlTable . "_data";
		$period = 'W';
		$ticker = $this->filter["UF_ITEM"];
		$query = "SELECT MIN(`UF_DATE`) AS `DATE_MIN`, MIN(`UF_DATE`) AS `DATE_FROM`, MAX(`UF_DATE`) AS `DATE_MAX`, floor(DATEDIFF(MAX(`UF_DATE`), MIN(`UF_DATE`))) AS DAYS FROM `$hlTableName` WHERE `UF_ITEM` = '$ticker'";
		//$query = "SELECT MIN(`UF_DATE_TO`) AS `DATE_MIN`, MIN(`UF_DATE_FROM`) AS `DATE_FROM`, MAX(`UF_DATE_TO`) AS `DATE_MAX`, floor(DATEDIFF(MAX(`UF_DATE_TO`), MIN(`UF_DATE_FROM`))) AS DAYS FROM `$tableName` WHERE `UF_TICKER` = '$ticker' AND `UF_PERIOD` = '$period'";
		//$query = "SELECT MIN(`UF_DATE_TO`) AS `DATE_MIN`, MIN(`UF_DATE_FROM`) AS `DATE_FROM`, MAX(`UF_DATE_TO`) AS `DATE_MAX` FROM `$tableName` WHERE `UF_TICKER` = '$ticker' AND `UF_PERIOD` = '$period'";
		$res = $DB->Query($query);
		if($row = $res->fetch()){
		 $arReturn = array(
		 						 "real_from"=>new DateTime($row["DATE_FROM"]),
		 						 //"tick"=>$row["DAYS"],
		 						 "min"=>$row["DATE_MIN"],
		 						 "max"=>$row["DATE_MAX"],
								 );
		 $dayDiff = intval($row["DAYS"]);
 					 if($dayDiff<=31){
						  $this->period = "D";
						  $arReturn["tick"] = 1 . ' days';
						  $arReturn["interval"] = 1;
						  $arReturn["period"] = 'D';
					 } else if($dayDiff>31 && $dayDiff<=366){
						  $this->period = "W";
						  $arReturn["tick"] = 7 . ' days';
						  $arReturn["interval"] = 7;
						  $arReturn["period"] = 'W';
					 } else {
						  $arReturn["tick"] = floor($dayDiff/7/12) . ' weeks';
						  $arReturn["interval"] = 31;
						  $arReturn["period"] = 'M';

					 }

		}
		unset($query, $res, $row, $tableName, $period);
		return $arReturn;
	 }

    private function getFromDate()
    {
        $dataClass = $this->getHlDataClass();

        $res = $dataClass::getList([
            "filter" => $this->filter,
            "order" => [
                "UF_DATE" => "asc",
            ],
            "limit" => 1,
            "select" => [
                "UF_DATE",
            ],
        ]);

        if ($item = $res->fetch()) {
            return $item["UF_DATE"]->format("d.m.Y");
        }
    }

    private function getToDate()
    {
        $dataClass = $this->getHlDataClass();

        $res = $dataClass::getList([
            "filter" => $this->filter,
            "order" => [
                "UF_DATE" => "desc",
            ],
            "limit" => 1,
            "select" => [
                "UF_DATE",
            ],
        ]);

        if ($item = $res->fetch()) {
            return $item["UF_DATE"]->format("d.m.Y");
        }
    }

    private function getHlDataClass()
    {
        if (!$this->dataClasses[$this->hlId]) {
            $hlblock = HL\HighloadBlockTable::getById($this->hlId)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $this->dataClasses[$this->hlId] = $entity->getDataClass();
        }

        return $this->dataClasses[$this->hlId];
    }
}