<?use Bitrix\Highloadblock as HL;

	class RadarBaseMarketMap extends RadarBase {

		/**
		 * Формирует массив для нормализации длины шкал на карте рынка
		 *
		 * @return array()
		 *
		 * @access public
		 */
		public function getMarketMarNormalizeAxis(){
			 $iblockId = 61;
		   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
			$cacheId = "market_map_normalize_axis";
			$cacheTtl = 86400 * 365;
		       //$cache->clean("usa_actions_data");
			if ($cache->read($cacheTtl, $cacheId)) {
		            $arNormalizeParams = $cache->get($cacheId);
		        } else {

				  CModule::IncludeModule("iblock");
				    //Получаем разделы инфоблока
				    $arSections = array();
		          $arFilter = Array('IBLOCK_ID'=>IntVal($iblockId), 'GLOBAL_ACTIVE'=>'Y',);
		          $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, true);
		          while($ar_result = $db_list->GetNext())
		          {
		            $arSections[$ar_result['ID']] = $ar_result;
		          }

					 //Получим значения списочных свойств
					 $arListRes = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), array("IBLOCK_ID"=>$iblockId) );
					 $arListProps = array();
					 while($arListRow = $arListRes->GetNext()){
							 $arListProps[$arListRow["ID"]] = $arListRow["XML_ID"];
					 }

					 //Сформируем массив настроек нормализации
					 $arSelect = Array("ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PROPERTY_MIN_X", "PROPERTY_MAX_X", "PROPERTY_MIN_Y", "PROPERTY_MAX_Y", "PROPERTY_FORMAT_X", "PROPERTY_FORMAT_Y");
		          $arFilter = Array("IBLOCK_ID"=>IntVal($iblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		          $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
					 $arNormalizeParams = array();
		          while($ob = $res->GetNext()){
						$mainSection = 'default';
						$rootSection = 'default';
						if($ob["IBLOCK_SECTION_ID"]>0){
							$mainSection = $arSections[$ob["IBLOCK_SECTION_ID"]];
							if($mainSection["IBLOCK_SECTION_ID"]>0){
							 $rootSection = $arSections[$mainSection["IBLOCK_SECTION_ID"]];
							}
						  $arNormalizeParams[$rootSection["CODE"]][$mainSection["CODE"]][$ob["NAME"]] = array("FORMAT_X"=>$arListProps[$ob["PROPERTY_FORMAT_X_ENUM_ID"]], "FORMAT_Y"=>$arListProps[$ob["PROPERTY_FORMAT_Y_ENUM_ID"]], "MIN_X"=>$ob["PROPERTY_MIN_X_VALUE"], "MAX_X"=>$ob["PROPERTY_MAX_X_VALUE"], "MIN_Y"=>$ob["PROPERTY_MIN_Y_VALUE"], "MAX_Y"=>$ob["PROPERTY_MAX_Y_VALUE"]);
						}
		          }

			             $cache->set($cacheId, $arNormalizeParams);
		        }

		  return $arNormalizeParams;
		}


		//Получение списка периодов для карты рынка по условиям HL CompanyData
		public function getPeriodList($hlID = 14) {
			//В качестве подмены текущего квартала получаем жестко заданные в админке квартал и год
			$curActQuart = \Bitrix\Main\Config\Option::get("grain.customsettings", "ACTUAL_PERIOD_QUARTAL");
			$curActYear  = \Bitrix\Main\Config\Option::get("grain.customsettings", "ACTUAL_PERIOD_YEAR");

			$cache    = \Bitrix\Main\Application::getInstance()->getManagedCache();
			$cacheId  = "period_list_" . $curActQuart . $curActYear . (new DateTime())->format('d_m_Y') . $hlID;
			$cacheTtl = 86400;
			//$cache->clean("usa_actions_data");
			if ($cache->read($cacheTtl, $cacheId)) {
				$arKvartalPeriod = $cache->get($cacheId);
			} else {

				//текущий
				$kv            = @intval((date('n') - 1) / 4) + 1;
				$year          = date("Y");
				$show_kvartals = round(($year - 2015) * 4, 0) - 4;
				//$kv--;
				if ($kv == 0) {
					$kv = 4;
					$year--;
				}
				$currKv   = $kv;
				$currYear = $year;

				$hlblock     = HL\HighloadBlockTable::getById($hlID)->fetch();
				$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
				$entityClass = $entity->getDataClass();

				$res = $entityClass::getList(array(
					"filter" => array(
						"UF_KVARTAL" => $kv,
						"UF_YEAR" => $year
					),
					"select" => array(
						"*"
					),
					"limit" => 1,
				));

				if (!$res->getSelectedRowsCount()) {
					$kv--;
					if ($kv == 0) {
						$kv = 4;
						$year--;
					}
				}

				if ($res->getSelectedRowsCount()) {
					$kv   = $curActQuart;
					$year = $curActYear;
				}

				$arKvartalPeriod["LIST"] = array(
					$currKv . "-" . $currYear => "Текущий",
				);
				$arKvartalPeriod["LIST"][$curActQuart . "-" . $curActYear] = $curActQuart . " квартал " . $curActYear;

				while (count($arKvartalPeriod["LIST"]) < $show_kvartals) {
					$kv--;
					if ($kv == 0) {
						$kv = 4;
						$year--;
					}
					$arKvartalPeriod["LIST"][$kv . "-" . $year] = $kv . " квартал " . $year;
				}
				$arKvartalPeriod["CURR_KVARTAL"] = $currKv;
				$arKvartalPeriod["CURR_YEAR"]    = $currYear;
				$cache->set($cacheId, $arKvartalPeriod);
			}

			return array("LIST" => $arKvartalPeriod["LIST"], "CURR_KVARTAL" => $arKvartalPeriod["CURR_KVARTAL"], "CURR_YEAR" => $arKvartalPeriod["CURR_YEAR"]);
		}
	}
?>