<?
use Bitrix\Highloadblock as HL;

//Акции
class ActionsMM extends RadarBaseMarketMap {
	private $arItems = array();
	public $arOriginalsItems = array();
	public $arOriginalsItemsKeySECID = array();
	public $count;
	private $perPage = 5;
	public $havePayAccess = false;
	public $curPage = 1;
	public $used_current_period = false;
	public $f_period = '';
	public $arActionsPages = array();
	public $selectedItems = array();
	public $curActQuart = 0;
	public $curActYear = 0;


	function __construct(){
		$this->curPage = $_REQUEST["page"];
		$this->setPeriodForFilter();
		$this->setData();
		$this->havePayAccess = checkPayRadar();
	}

   //Определяет текущий период для фильтров и выставляет флаг об использовании текущего периода
	public function setPeriodForFilter(){
	//Фильтр по периоду
   $this->f_period = '';

  if(isset($_POST["f_period"]) && !empty(htmlspecialchars($_POST["f_period"]))){
	$this->f_period = $_POST["f_period"];

	 $this->used_current_period = false;

	if($this->f_period == $_POST["f_period_current"]){
	 $this->used_current_period = true;
	 //В качестве подмены текущего квартала получаем жестко заданные в админке квартал и год
	 $this->curActQuart = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_QUARTAL");
  	 $this->curActYear = \Bitrix\Main\Config\Option::get("grain.customsettings","ACTUAL_PERIOD_YEAR");

/*  	 $this->curActQuart = COption::GetOptionString("grain.customsettings", "ACTUAL_PERIOD_QUARTAL", "");
  	 $this->curActYear = COption::GetOptionString("grain.customsettings", "ACTUAL_PERIOD_YEAR", "");*/

/*		 $filter_period = explode('-',$this->f_period);
	 	 if($filter_period[0]==1){
	 	 	$filter_period[1] = $filter_period[1]-1;
			$filter_period[0] = 4;
	 	 } else {
			$filter_period[0] = $filter_period[0]-1;
	 	 }*/

	  	// $this->f_period = implode("-",$filter_period);
		 $this->f_period = $this->curActQuart."-".$this->curActYear;
	}

  }
	// echo "this->f_period".$this->f_period;

	}

	//отдельный элемент по коду
	public function getItem($code){
		foreach($this->arOriginalsItems as $item){
			if($item["SECID"]==$code){
				return $item;
			}
		}
		
		return false;
	}

	//отдельный элемент по коду
	public function getItemBySECID($SECID){
		if(array_key_exists($SECID, $this->arOriginalsItemsKeySECID)){
		  return $this->arOriginalsItemsKeySECID[$SECID];
		}
		return false;
	}

/**
* Compute the start and end date of some fixed o relative quarter in a specific year.
* @param mixed $quarter  Integer from 1 to 4 or relative string value:
*                        'this', 'current', 'previous', 'first' or 'last'.
*                        'this' is equivalent to 'current'. Any other value
*                        will be ignored and instead current quarter will be used.
*                        Default value 'current'. Particulary, 'previous' value
*                        only make sense with current year so if you use it with
*                        other year like: get_dates_of_quarter('previous', 1990)
*                        the year will be ignored and instead the current year
*                        will be used.
* @param int $year       Year of the quarter. Any wrong value will be ignored and
*                        instead the current year will be used.
*                        Default value null (current year).
* @param string $format  String to format returned dates
* @return array          Array with two elements (keys): start and end date.
*/
public static function get_dates_of_quarter($quarter = 'current', $year = null, $format = null)
{
    if ( !is_int($year) ) {
       $year = (new DateTime)->format('Y');
    }
    $current_quarter = ceil((new DateTime)->format('n') / 3);
    switch (  strtolower($quarter) ) {
    case 'this':
    case 'current':
       $quarter = ceil((new DateTime)->format('n') / 3);
       break;

    case 'previous':
       $year = (new DateTime)->format('Y');
       if ($current_quarter == 1) {
          $quarter = 4;
          $year--;
        } else {
          $quarter =  $current_quarter - 1;
        }
        break;

    case 'first':
        $quarter = 1;
        break;

    case 'last':
        $quarter = 4;
        break;

    default:
        $quarter = (!is_int($quarter) || $quarter < 1 || $quarter > 4) ? $current_quarter : $quarter;
        break;
    }
    if ( $quarter === 'this' ) {
        $quarter = ceil((new DateTime)->format('n') / 3);
    }
    $start = new DateTime($year.'-'.(3*$quarter-2).'-1 00:00:00');
    $end = new DateTime($year.'-'.(3*$quarter).'-'.($quarter == 1 || $quarter == 4 ? 31 : 30) .' 23:59:59');

    return array(
        'start' => $format ? $start->format($format) : $start,
        'end' => $format ? $end->format($format) : $end,
    );
}

	//Получает список цен акций по $SECID
	public function getActionPricesBySECID($SECID=''){
	 $arResult = array();
	 $ibID = 24; //MoexActionsData
			//TODO кешировать "cache"=>array("ttl"=>3600)  доступно с версии 16.5.9, сделать после обновления
			$hlblock   = HL\HighloadBlockTable::getById($ibID)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

			$arFilter = array("UF_ITEM"=>$SECID);

			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array(
					"UF_ITEM", "UF_DATE", "UF_CLOSE"
				),
				"order" => array(
					"ID" => "DESC"
				)
			));
			while($item = $res->fetch()){
			 $end = new DateTime($item["UF_DATE"]);
			 $arResult[$end->format('d.m.Y')] = $item["UF_CLOSE"];
			}

		  return $arResult;
	}

	//акции с просадом менее 10%
	public function getLowProsad(){
		global $APPLICATION;
		$arReturn = array();
		
		foreach($this->arOriginalsItems as $item){
			if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["акции"])){
				continue;
			}
			if($item["DYNAM"]["Просад"]<10){
				$arReturn[] = array(
					$item["SECID"],
					$item["NAME"],
					$item["DYNAM"]["Просад"]
				);
			}
		}
		
		return $arReturn;
	}

	//Возвращает массивы данных для построения графика пузырьков
	public function getBubbleGraphData($arData, $filter_period, $haveRadarAccess, $arCollationMirrorSector){
		$arr_begin = array();
		$max_bubble = 0;
        	foreach($arData["items"] as $item){

					 if(empty($item["COMPANY"]["NAME"])) continue;

					 if(!$haveRadarAccess){
						 $item["NAME"] = 'Название доступно в платной версии';
					 }
					  //Фильтруемый период
					  $arSelectedPeriod = array(); //Данные за выбранный период
					  $arPrevPeriod = array(); //Данные за этот же период год назад
							 if(array_key_exists($filter_period."-KVARTAL",$item["PERIODS"])){
							$arSelectedPeriod = $item["PERIODS"][$filter_period."-KVARTAL"];
						  }



					  $ar_filter_period = explode('-',$f_period);
					  //Получаем выбранный квартал за прошлый год
					  if(is_array($ar_filter_period)){
			 			$arYears = array(intval($ar_filter_period[1]),(intval($ar_filter_period[1])-1)); //TODO фильтруем по текущему выбранному году + год назад для расчетов итогов таблицы
						  if(array_key_exists($ar_filter_period[0]."-".end($arYears)."-KVARTAL",$item["PERIODS"])){
							$arPrevPeriod = $item["PERIODS"][$filter_period."-KVARTAL"];
						  }
			 			}



						if($_REQUEST["f_period"] == $_REQUEST["f_period_current"]){
							$use_current_period = true;
							$cap = round($item["PROPS"]["ISSUECAPITALIZATION"]/1000000,2);
							$curPE = round($item["DYNAM"]["PE"],2);
						} else {
							$use_current_period = false;
							$cap = round($arSelectedPeriod["Прошлая капитализация"],2);
							$curPE = round($arSelectedPeriod["P/E"],2);
						}

						//Назначим код сектора для работы с графиком
						$item["PROPS"]["PROP_SEKTOR_CODE"] = $arCollationMirrorSector[$item["PROPS"]["PROP_SEKTOR"]];

						if($_POST["f_preset"]==0){
						$curPE = ($use_current_period==true)?round($item["DYNAM"]["PE"],2):round($arSelectedPeriod["P/E"],2);
						$dynam_target = floatval($item["DYNAM"]["Таргет"])>0 ? $item["DYNAM"]["Таргет"] : 0;
/*						 define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/getData_log.txt");
                   AddMessage2Log('$item = '.print_r(array_merge($item,array("curPE"=>$curPE)), true),'');*/
						if($dynam_target>0 && $item["DYNAM"]["Дивиденды %"]>0 && $curPE>0 ) {
						 $add = true;
						 //Проверяем на ограничение диапазона, что бы не гонять лишние данные для всех отраслей
						 $gain = intval($arPresets[intval($_POST["f_preset"])]["ALL_MAX_X"]);
						 if($use_current_period && ($gain>0 && $curPE>$gain)){
						 $add = false;
						 }


						 if($add){


						$arr_begin[] = array(
													 $curPE,
													 round($dynam_target,2),
													 round($item["DYNAM"]["Дивиденды %"],2),
													 $item["NAME"],
													 $item["PROPS"]["PROP_SEKTOR_CODE"]
													 );
						  //Получение максимального значения для кругов
						  if($max_bubble<round($item["DYNAM"]["Дивиденды %"],2)) $max_bubble=round($item["DYNAM"]["Дивиденды %"],2);
							}//if add
						}
						}

						if($_POST["f_preset"]==1){
						$dynam_target = floatval($item["DYNAM"]["Таргет"])>0 ? $item["DYNAM"]["Таргет"] : 0;

						if($arSelectedPeriod["Темп роста прибыли"]>0 && $item["DYNAM"]["Дивиденды %"]>0 && 0>0) {
						$arr_begin[] = array(
													 0,//Недооценки пока не существует, сделаем позже
													 round($arSelectedPeriod["Темп роста прибыли"],2),
													 round($item["DYNAM"]["Дивиденды %"],2),
													 $item["NAME"],
													 $item["PROPS"]["PROP_SEKTOR_CODE"]
													 );
						}
						}

						if($_POST["f_preset"]==2){
						$dynam_target = floatval($item["DYNAM"]["Таргет"])>0 ? $item["DYNAM"]["Таргет"] : 0;

						if($dynam_target>0 && $item["DYNAM"]["Дивиденды %"]>0 ) {
						$arr_begin[] = array(
													 round($arSelectedPeriod["EV/Ebitda"],2),
													 round($dynam_target,2),//Прогноз
													 round($item["DYNAM"]["Дивиденды %"],2),
													 $item["NAME"],
													 $item["PROPS"]["PROP_SEKTOR_CODE"]
													 );
						}
						}
						if($_POST["f_preset"]==3){
						 $add = true;
						 //Проверяем на ограничение диапазона, что бы не гонять лишние данные для всех отраслей
						 $gain = intval($arPresets[intval($_POST["f_preset"])]["ALL_MAX_X"]);
						 //if($show_current_cap==true && ($gain>0 && $arSelectedPeriod["Темп роста прибыли"]>$gain)){
						 if( $gain>0 && $arSelectedPeriod["Темп роста прибыли"]>$gain){
						 $add = false;
						 }

						 if($add){



						if($cap && $arSelectedPeriod["Темп роста прибыли"]>0 && $arSelectedPeriod["Рентабельность собственного капитала"]>0) {
						$arr_begin[] = array(
													 round($arSelectedPeriod["Темп роста прибыли"],2),
													 round($arSelectedPeriod["Рентабельность собственного капитала"],2),//Рентабельность СК
													 $cap,
													 $item["NAME"],
													 $item["PROPS"]["PROP_SEKTOR_CODE"]
													 );
						  //Получение максимального значения для кругов
						  if($max_bubble<$cap) $max_bubble=$cap;
						  }//if add
						}
						}
		 }

		 return array("arr_begin"=>$arr_begin, "max_bubble"=>$max_bubble);
	}


	//акции с таргетом менее 10%
	public function getLowTarget(){
		global $APPLICATION;
		$arReturn = array();
		
		foreach($this->arOriginalsItems as $item){
			if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["акции"])){
				continue;
			}

			if($item["DYNAM"]["Таргет"]<10){
				$arReturn[] = array(
					$item["SECID"],
					$item["NAME"],
					$item["DYNAM"]["Таргет"]
				);
			}
		}

		return $arReturn;
	}

	private function setData(){
		global $APPLICATION;


		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "actions_mm_data".$_POST["f_industry"].$_POST["f_period"].$_POST["f_period_current"].$_POST["f_preset"];
		$cacheTtl = 86400*7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arItems = $cache->get($cacheId);
		} else {

			$arItems = array();
			$arItemsSECID = array();

			CModule::IncludeModule("iblock");
			CModule::IncludeModule("highloadblock");
			$ibID = 32;

			$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();



			$arPeriodsFilter = array(
					"!UF_YEAR" => false,

				);
			//Собираем данные по компаниям из 14 HL
			$res = $entityClass::getList(array(
				"filter" => $arPeriodsFilter,
				"select" => array(
					"*"
				),
				"order" => array(
					"ID" => "DESC"
				)
			));
			while($item = $res->fetch()){

				$item["UF_DATA"] = json_decode($item["UF_DATA"], true);
				
				if(isset($item["UF_KVARTAL"])){
					$type = "KVARTAL";
					$t = $item["UF_KVARTAL"];
				} else {
					$type = "MONTH";
					$t = $item["UF_MONTH"];
				}
				
				$item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
				$item["UF_DATA"]["PERIOD_VAL"] = $t;
				$item["UF_DATA"]["PERIOD_TYPE"] = $type;

				$arPeriods[$item["UF_COMPANY"]][$t."-".$item["UF_YEAR"]."-".$type] = $item["UF_DATA"];
			}

            //Подготовим страницы всех акций и закешируем
 				$arFilter = Array("IBLOCK_ID"=>33, "ACTIVE"=>"Y");
				$arActionsPages = array();
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL","CODE"));
				while($val = $res2->GetNext())
				{
					$arActionsPages[$val["CODE"]] = $val["DETAIL_PAGE_URL"];
				}

				//Подготовим выборку всех компаний
 				$arFilter = Array("IBLOCK_ID"=>26, "ACTIVE"=>"Y");
				$arCompanies = array();
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID","NAME"));
				while($val = $res2->Fetch())
				{
					$arCompanies[$val["ID"]] = array("NAME" => $val["NAME"]);
				}

				//Подготовим выборку по страницам компаний
				$arCompanyPages = array();
				$arFilter = Array("IBLOCK_ID"=>29, "ACTIVE"=>"Y");
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL", "NAME", "PROPERTY_VAL_ED_FIRST", "PROPERTY_CAPITAL_KOEF", "PROPERTY_CAPITAL_ACTION_CNT_VALUE", "PROPERTY_CAPITAL_FORMULA_DIVIDER_VALUE"));
				while($val = $res2->GetNext())
				{
					$arCompanyPages[$val["NAME"]]=$val;
				}



			//Предыдущий день
			$prevDay = new DateTime();
			$prevDay->modify("-1 day");

			//Если в куках есть выбранные акции - Получим их для проверки
			$selectedItems = $this->getSelectedItems();

			$arFilter = Array("IBLOCK_ID"=>$ibID, "PROPERTY_HIDEN"=>false);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
			while($ob = $res->GetNextElement())
			{
				$fields = $ob->GetFields();

				$item = array(
					"NAME" => $fields["NAME"],
					"SECID" => $fields["CODE"],
					//"URL" => $fields["DETAIL_PAGE_URL"],
				);

				if(array_key_exists($fields["CODE"],$arActionsPages)){
					$item["URL"] = $arActionsPages[$fields["CODE"]];
				}
				
				$props = $ob->GetProperties();

				foreach($props as $prop){
					if($prop["VALUE"]){
						if($prop["CODE"]=="PROP_TARGET"){
							$prop["VALUE"] = str_replace("%", "", $prop["VALUE"]);
						}
						$item["PROPS"][$prop["CODE"]] = $prop["VALUE"];
					}
				}
				$item["PROPS"]["PROP_PROSAD"] = str_replace(",", ".", $item["PROPS"]["PROP_PROSAD"]);
				$item["PROPS"]["PROP_TARGET"] = str_replace(",", ".", $item["PROPS"]["PROP_TARGET"]);
				
				//Убрать с режимами торгов
				if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["акции"]) || !$item["PROPS"]["BOARDID"]){
					continue;
				}
				
				//если нет LASTPRICE - берем LEGALCLOSE
				if($item["PROPS"]["LASTPRICE"]<=0 && $item["PROPS"]["LEGALCLOSE"]>0) {
					$item["PROPS"]["LASTPRICE"] = $item["PROPS"]["LEGALCLOSE"];
				}


				 //////////eemitents
				if($item["PROPS"]["EMITENT_ID"]){

					$item["COMPANY"] = array(
						"ID" => $item["PROPS"]["EMITENT_ID"],
						"NAME" => $arCompanies[$item["PROPS"]["EMITENT_ID"]]["NAME"],
						//"URL" => $comp["DETAIL_PAGE_URL"],
					);



					//страница компании
					if(array_key_exists($item["COMPANY"]["NAME"],$arCompanyPages))
					{

						$val = $arCompanyPages[$item["COMPANY"]["NAME"]];
						$item["COMPANY"]["URL"] = $val["DETAIL_PAGE_URL"];

						//если данные в долларах, перевести капитализацию
						if(strpos($val["PROPERTY_VAL_ED_FIRST_VALUE"], "долл")!==false && $item["PROPS"]["ISSUECAPITALIZATION"]){
							$item["PROPS"]["ISSUECAPITALIZATION_DOL"] = $item["PROPS"]["ISSUECAPITALIZATION"]/getCBPrice("USD", $prevDay->format("d/m/Y"));
						}

						if($val["PROPERTY_CAPITAL_KOEF_VALUE"]){
							$item["COMPANY"]["CAPITAL_KOEF"] = $val["PROPERTY_CAPITAL_KOEF_VALUE"];
						}

					}
				}
				if($item["PROPS"]["EMITENT_ID"] && $arPeriods[$item["PROPS"]["EMITENT_ID"]]){
					$item["PERIODS"] = $arPeriods[$item["PROPS"]["EMITENT_ID"]];
					$item["PERIODS"] = parent::calculatePeriodData($item);

				}
				
				$item["DYNAM"] = self::setDynamParams($item);





				if($_REQUEST["f_industry"]!=="all" && isset($_REQUEST["f_industry"])){//Если выбран конкретный сектор
					if($item["PROPS"]["PROP_SEKTOR"]==htmlspecialchars($_REQUEST["f_industry"]) && !array_key_exists($item["PROPS"]["SECID"],$selectedItems)){
					  if(self::accessInsertInResult($item))
					  		$arItems[] = $item;
					  		$arItemsSECID[$item["SECID"]] = $item;
					}
					//Для выбранного сектора проверим акции из кук и тоже их добавим, иначе при последующем выборе они пропадут из кук
					if(count($selectedItems)>0){
					  if(array_key_exists($item["PROPS"]["SECID"],$selectedItems)){
							  		$arItems[] = $item;
							  		$arItemsSECID[$item["SECID"]] = $item;
					  }
		 			}

				}else{
				  if(self::accessInsertInResult($item)){
						$arItems[] = $item;
						$arItemsSECID[$item["SECID"]] = $item;
						}
				}

			  //	$arItems[] = $item;
			}


			$cache->set($cacheId, $arItems);
		}




		$this->arOriginalsItems = $arItems;
		$this->arOriginalsItemsKeySECID = $arItemsSECID;
		$this->count = count($arItems);
	}

	//Возвращает массив акций отсортированный по показателю пресета в нужном порядке
	private function sortItemsByPreset(){
	global $APPLICATION;
		$arItems = $this->arItems;

/*	  		global $USER;
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            if($arUser["LOGIN"]=="freud"){
echo "<pre  style='color:black; font-size:11px;'>";
   print_r(count($arItems));
   echo "</pre>";
            }*/

	    $negArr = [];
	    $posArr = [];
	    $selArr = [];  //Для выбранных акций с плохими показателями
/*		echo "s_field=".$_POST["s_field"]."<br>";
		echo "f_period=".$this->f_period."<br>";
		echo "used_current_period=".$this->used_current_period==true?'Y':'N'."<br><br>";*/

		foreach($arItems as $item){  //Цикл для отбора
		if($_POST["s_field"]=="P/E"){//+отбор для сортировки по PE
		  {
		  	if($this->used_current_period){//Для текщего периода отбираем по динамике
	        if($item["DYNAM"]["PE"] <= 0 || !$item["DYNAM"]["PE"]) {
                $negArr[] = $item;
            } else {
	            $posArr[] = $item;
            }
				} else { //Для выбранного не текущего периода отбираем по данным периода
		        if($item["PERIODS"][$this->f_period."-KVARTAL"]["P/E"] <= 0 || !$item["PERIODS"][$this->f_period."-KVARTAL"]["P/E"]) {
	                $negArr[] = $item;
	            } else {
		            $posArr[] = $item;
	            }
				}
        } //-отбор для сортировки по PE
		}
		if($_POST["s_field"]=="Темп роста прибыли"){//+отбор для сортировки по Темпу роста прибыли
		  {
	        if($item["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"] <= 0 || !$item["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"]) {
                $negArr[] = $item;
            } else {
	            $posArr[] = $item;
            }
        } //-отбор для сортировки по Темпу роста прибыли
		}

		}//foreach


/*	  		global $USER;
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            if($arUser["LOGIN"]=="freud"){
echo "<pre  style='color:black; font-size:11px;'>";
   print_r(count($posArr));
   echo "</pre>";
            }*/


	  //Сортировка отрицательного и положительного массивов с отобранными по показателям данными

	  if($_POST["s_field"]=="P/E"){//+сортировка по PE
		if($this->used_current_period){//Для текщего периода сортируем по динамике
        usort($posArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) { return 0; }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? 1 : -1;
        });

        usort($negArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) { return 0;}
            if(!$a["DYNAM"]["PE"]) {return -1;}
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? -1 : 1;
        });
		 } else { //Для выбранного не текущего периода сортируем по данным периода
        usort($posArr, function($a,$b){
            if ($a["PERIODS"][$this->f_period."-KVARTAL"]["P/E"] == $b["PERIODS"][$this->f_period."-KVARTAL"]["P/E"]) { return 0; }
            return ($a["PERIODS"][$this->f_period."-KVARTAL"]["P/E"] > $b["PERIODS"][$this->f_period."-KVARTAL"]["P/E"]) ? 1 : -1;
        });

        usort($negArr, function($a,$b){
            if ($a["PERIODS"][$this->f_period."-KVARTAL"]["P/E"] == $b["PERIODS"][$this->f_period."-KVARTAL"]["P/E"]) { return 0;}
            if(!$a["PERIODS"][$this->f_period."-KVARTAL"]["P/E"]) {return -1;}
            return ($a["PERIODS"][$this->f_period."-KVARTAL"]["P/E"] > $b["PERIODS"][$this->f_period."-KVARTAL"]["P/E"]) ? -1 : 1;
        });
		 }
		 }//-сортировка по PE

	  if($_POST["s_field"]=="Темп роста прибыли"){//+сортировка по Темпу роста прибыли
        usort($posArr, function($a,$b){
            if ($a["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"] == $b["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"]) { return 0; }
            return ($a["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"] < $b["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"]) ? 1 : -1;
        });

        usort($negArr, function($a,$b){
            if ($a["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"] == $b["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"]) { return 0;}
            if(!$a["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"]) {return -1;}
            return ($a["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"] < $b["PERIODS"][$this->f_period."-KVARTAL"]["Темп роста прибыли"]) ? -1 : 1;
        });

		 }//-сортировка по Темпу роста прибыли

		 //Пробежимся по плохим отрицательным акциям и проверим их в списке выбранных
		 $selectedItems = $this->getSelectedItems();
		 if(count($selectedItems)>0)
		 foreach($negArr as $negItem){
			if(!$selectedItems[$negItem["PROPS"]["SECID"]]){
			  $selArr[] = $negItem;
			}
		 }


/*	  		global $USER;
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            if($arUser["LOGIN"]=="freud"){
echo "<pre  style='color:black; font-size:11px;'>";
   print_r($negArr);
   echo "</pre>";
            }*/

        $this->arItems = array_merge($selArr, $posArr);
        //$this->arItems = array_merge($posArr, $negArr);


	}


	//Проверяем заполненность всех параметров акции по пресету, если чего-то не хватает - не добавляем в таблицу результата
	public function accessInsertInResult($item){
		$local_res = true;


    	if(is_array($item["PERIODS"]) && array_key_exists($this->f_period."-KVARTAL",$item["PERIODS"])){
							$arSelectedPeriod = $item["PERIODS"][$this->f_period."-KVARTAL"];
		}

		if(array_key_exists($item["PROPS"]["SECID"],$this->selectedItems)) return true;

		if($_REQUEST["f_preset"]==0){
		 if(!isset($item["DYNAM"]["Таргет"]) || empty($item["DYNAM"]["Таргет"]) || $item["DYNAM"]["Таргет"]<=0 ) $local_res = false;
		 if($this->used_current_period==true){
			if($local_res==true && (!isset($item["DYNAM"]["PE"]) || empty($item["DYNAM"]["PE"]) || $item["DYNAM"]["PE"]<=0 )) $local_res = false;
		 }else{
			if($local_res==true && (!isset($arSelectedPeriod["P/E"]) || empty($arSelectedPeriod["P/E"]) || $arSelectedPeriod["P/E"]<=0 )) $local_res = false;
		 }

		 if($local_res==true && (!isset($item["DYNAM"]["Дивиденды %"]) || empty($item["DYNAM"]["Дивиденды %"]) || $item["DYNAM"]["Дивиденды %"]<=0 )) $local_res = false;

		}


		if($_REQUEST["f_preset"]==3){
		 if(!isset($arSelectedPeriod["Темп роста прибыли"]) || empty($arSelectedPeriod["Темп роста прибыли"]) || $arSelectedPeriod["Темп роста прибыли"]<=0) $local_res = false;
		 if($local_res==true && (!isset($arSelectedPeriod["Рентабельность собственного капитала"]) || empty($arSelectedPeriod["Рентабельность собственного капитала"]) ||$arSelectedPeriod["Рентабельность собственного капитала"]<=0 )) $local_res = false;
		 if($this->used_current_period==true){
		 	if($local_res==true && (!isset($item["PROPS"]["ISSUECAPITALIZATION"]) || empty($item["PROPS"]["ISSUECAPITALIZATION"]) || $item["PROPS"]["ISSUECAPITALIZATION"]<=0 )) $local_res = false;
		 } else {
         if($local_res==true && (!isset($arSelectedPeriod["Прошлая капитализация"]) || empty($arSelectedPeriod["Прошлая капитализация"]) || $arSelectedPeriod["Прошлая капитализация"]<=0 )) $local_res = false;
		 }

		}
	  return $local_res;
	}

	//таблица вывода
	public function getTable($page=1){

		$this->setFilter();
		//$this->sortItems();
		$this->sortItemsByPreset();
		$end = $page*$this->perPage;
		$start = $end-$this->perPage;

		$arItems = array();

		if($page==1){
			$arItems = $this->arItemsSelected;
		}

		for($i=$start;$i<$end;$i++){
			if($this->arItems[$i]){
				$arItems[] = $this->arItems[$i];
			}
		}


		$shows = $this->perPage*$page+count($this->arItemsSelected);
		if($shows>$this->count+count($this->arItemsSelected)){
			$shows = $this->count;
		}

		return array(
			"total_items" => $this->count+count($this->arItemsSelected),
			"cur_page" => $page,
			"shows" => $shows,
			"count_page" => ceil($this->count/$this->perPage),
			"items" => $arItems,
			"access" => $this->havePayAccess,
			"selected" => $this->getSelectedItems()
		);
	}

	//таблица вывода
	public function getTableGraph(){

		$this->setFilter();
		//$this->sortItems();
		//$this->sortItemsByPreset();

		$arItems = array();


		$arItems = count($this->arOriginalsItems)?$this->arOriginalsItems:array();



		$shows = $this->perPage*$page+count($this->arItemsSelected);
		if($shows>$this->count+count($this->arItemsSelected)){
			$shows = $this->count;
		}

		return array(
			"total_items" => $this->count+count($this->arItemsSelected),
			"cur_page" => $page,
			"shows" => $shows,
			"count_page" => ceil($this->count/$this->perPage),
			"items" => $arItems,
			"access" => $this->havePayAccess,
			"selected" => $this->getSelectedItems()
		);
	}

	//сортировка элементов
	private function sortItems(){
	    $negArr = [];
	    $posArr = [];
	    foreach ($this->arItems as $item) {
	        if($item["DYNAM"]["PE"] <= 0 || !$item["DYNAM"]["PE"]) {
                $negArr[] = $item;
            } else {
	            $posArr[] = $item;
            }

        }
        usort($posArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
                return 0;
            }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? 1 : -1;
        });

        usort($negArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
                return 0;
            }
            if(!$a["DYNAM"]["PE"]) {
                return -1;
            }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? -1 : 1;
        });

        $this->arItems = array_merge($posArr, $negArr);
	}

	//избранные элементы
	public function getSelectedItems(){
		$arData = array();
		if($_COOKIE["id_arr_shares_cookie"]){
			$tmp = json_decode($_COOKIE["id_arr_shares_cookie"]);
			foreach($tmp as $item){
				$t = explode("...", $item);
				$arData[$t[0]] = $t[1];
			}
		}
		$this->selectedItems = $arData;
		return $arData;
	}

	//фильтр для элементов
	private function setFilter(){
		//$r = $this->setDefaultFilter($_REQUEST);

		$selectedItems = $this->getSelectedItems();

		foreach($this->arOriginalsItems as $item){
			if(!$selectedItems[$item["PROPS"]["SECID"]]){
/*				//Отрасль
				if($_POST["f_industry"]){
					$items = array();
					foreach($_POST["f_industry"] as $v){
						if(!$v){
							continue;
						}
						$items[] = $v;
					}

					if($items){
						if(!in_array($item["PROPS"]["PROP_SEKTOR"], $items) || !$item["PROPS"]["PROP_SEKTOR"]){
							continue;
						}
					}
				}*/
				$this->arItems[] = $item;
			} else {
				$this->arItemsSelected[] = $item;
			}
		}

		$this->count = count($this->arItems);
	}

	//Фильтр по умолчанию
	private function setDefaultFilter($r){
		global $APPLICATION;
		if(!isset($r["pe"])){
		  	$r["pe"] = $APPLICATION->obligationDefaultForm["pe"];
		}	
		if(!isset($r["profitability"])){
			$r["profitability"] = $APPLICATION->obligationDefaultForm["profitability"];
		}
		if(!isset($r["turnover_week"])){
			$r["turnover_week"] = "y";
		}

		return $r;
	}
	
	private function setDynamParams($item){
		$return = array(); 
		
		//new Просад = ( 1 - old Просад / LASTPRICE ( LEGALCLOSE) ) х 100
		if($item["PROPS"]["PROP_PROSAD"] && $item["PROPS"]["LASTPRICE"]){
			$return["Просад"] =  round((1 - $item["PROPS"]["PROP_PROSAD"]/$item["PROPS"]["LASTPRICE"])*100, 2);
		}

		if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_MONTH"]) {
            $return["MONTH_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_MONTH"]) - 1) * 100), 2);
        }
        if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_ONE_YEAR"]) {
            $return["YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_ONE_YEAR"]) - 1) * 100), 2);
        }
        if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_THREE_YEAR"]) {
            $return["THREE_YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_THREE_YEAR"]) - 1) * 100), 2);
        }
		
		//new Target = ( old Target / LASTPRICE ( LEGALCLOSE) - 1 ) x 100
		if($item["PROPS"]["PROP_TARGET"] && $item["PROPS"]["LASTPRICE"]){
			$return["Таргет"] =  round(($item["PROPS"]["PROP_TARGET"]/$item["PROPS"]["LASTPRICE"]-1)*100, 2);
		}
		
		//Цена лота
		if($item["PROPS"]["LOTSIZE"] && $item["PROPS"]["LASTPRICE"]){
			$return["Цена лота"] = $item["PROPS"]["LOTSIZE"]*$item["PROPS"]["LASTPRICE"];
		}
		
		/*
		1) если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
		2) если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
		3) если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
		4) если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
		*/
		//Прибыль
		if(count($item["PERIODS"])){
			$tmp = sort_nested_arrays($item["PERIODS"], array('PERIOD_YEAR' => 'desc', 'PERIOD_VAL' => 'desc'));

            foreach($tmp as $periodTmp){
                if($periodTmp["CURRENT_PERIOD"]==false){
                    $lastItem = $periodTmp;
                    break;
                }
            }


			$curKvartal = intval((date('n')-1)/4)+1;
			$availYear = date("Y")-1;

			try {
			$lastDate = DateTime::createFromFormat('m.Y', $lastItem["PERIOD_VAL"].".".$lastItem["PERIOD_YEAR"]);
			$lastAvail = DateTime::createFromFormat('m.Y', $curKvartal.".".$availYear);

			if($lastDate && $lastAvail){
			if($lastDate->format("U") >= $lastAvail->format("U")){ 
				
				$year = DateTime::createFromFormat('Y', $lastItem["PERIOD_YEAR"]);
				$prevYear = clone $year;
				$prevYear->modify("-1year");
				
				if ($lastItem["PERIOD_VAL"]==1){
					//если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
					if(
						isset($item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["1-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] = 
							$item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["1-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==2){
					//если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
					if(
						isset($item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["2-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] = 
							$item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["2-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==3){
					//если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
					if(
						isset($item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["3-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] = 
							$item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["3-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==4){
					//если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
					if(
						isset($item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] = 
							$item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				}
			}
			}

			} catch (Exception $e) {
				?><pre><?print_r($lastItem)?></pre><?
				exit();
			} 
		}
		
		//РЕ
		if($return["Прибыль"] && ($item["PROPS"]["ISSUECAPITALIZATION"] || $item["PROPS"]["ISSUECAPITALIZATION_DOL"])){
			$val = $item["PROPS"]["ISSUECAPITALIZATION"];
			if($item["PROPS"]["ISSUECAPITALIZATION_DOL"]){
				$val = $item["PROPS"]["ISSUECAPITALIZATION_DOL"];
			}
			$return["PE"] = round($val/$return["Прибыль"]/1000000, 1);
            $return["P/Equity"] = round($val/$return["Собственный капитал"]/1000000, 1);
            $return["PEG"] = round($return["PE"]/$return["AVEREGE_PROFIT"], 1);
		}
		//Дивиденды %
/*		if($item["PROPS"]["PROP_DIVIDENDY_2018"] && $item["PROPS"]["LASTPRICE"]){
			$return["Дивиденды %"] = round($item["PROPS"]["PROP_DIVIDENDY_2018"]/$item["PROPS"]["LASTPRICE"]*100, 2);
		}*/

		//Дивиденды %
		if(!empty($item["PROPS"]["PROP_DIVIDENDY_2018"]) && !empty($item["PROPS"]["LASTPRICE"])){
			$return["Дивиденды %"] = round($item["PROPS"]["PROP_DIVIDENDY_2018"]/$item["PROPS"]["LASTPRICE"]*100, 2);
			if(strpos($item["PROPS"]["PROP_ISTOCHNIK"],"$")!==false){//Если расчет в валюте
			$cbr = new CCurrency((new DateTime())->format("d.m.Y"));
			$currencyRateUSD = $cbr->getRate("USD", false);
			//$currencyRateUSD = getCBPrice("USD", (new DateTime())->format("d/m/Y"));
			$return["Дивиденды %"] = round($item["PROPS"]["PROP_DIVIDENDY_2018"]*$currencyRateUSD/$item["PROPS"]["LASTPRICE"]*100, 2);
			unset($cbr);
			}
		} else if(floatval($item["PROPS"]["PROP_DIVIDENDY_2018"])>0 && empty($item["PROPS"]["LASTPRICE"]) ){
			$return["Дивиденды %"] = 0.1;
		}

		return $return;
	}

	//Возвращает уникальные значения для фильтра по акциям США
	public function getRadarFiltersValues(){
		global $APPLICATION;
	   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "actions_filters_data";
		$cacheTtl = 86400*7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arFilterEnums = $cache->get($cacheId);
		} else {
				CModule::IncludeModule("iblock");
			   $arFilterEnums = array("PROP_SEKTOR"=>array());

			  foreach($arFilterEnums as $k=>$v){
				 $rsResult = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 32, 'PROPERTY_HIDEN'=>false, '!PROPERTY_'.$k.'_VALUE' => false,'!NAME'=>false ),array('PROPERTY_'.$k));
				 while($arResult=$rsResult->Fetch()){
				   if(!empty($arResult['PROPERTY_'.$k.'_VALUE'])){
				    if(strpos($arResult['PROPERTY_'.$k.'_VALUE'],"/")!==false){
						$val = explode("/",$arResult['PROPERTY_'.$k.'_VALUE']);
						foreach($val as $param_val){
						  $arFilterEnums[$k][$param_val]=$param_val;
						}
				    } else {
				    	$val = $arResult['PROPERTY_'.$k.'_VALUE'];
				      $arFilterEnums[$k][$val]=$val;
					 }

					}
			    }
			  }

		   $cache->set($cacheId, $arFilterEnums);
		}

		return $arFilterEnums;
	}

	  //Возвращает сектора для фильтра и построения графика
	  public function getSectorArrays(){
	    $arFilterValues = $this->getRadarFiltersValues();
		 $arCollationsSector = array();
		 $arCollationMirrorSector = array();
		 foreach($arFilterValues["PROP_SEKTOR"] as $sector){
		 	$arParams = array("replace_space"=>"-","replace_other"=>"-");
		 	$trans = Cutil::translit($sector,"ru",$arParams);
			$arCollationsSector[] = array("name"=>$sector, "code"=>$trans);
			$arCollationMirrorSector[$sector] = $trans;
		 }

		 return array("SECTORS"=>$arCollationsSector, "SECTORS_MIRROR"=>$arCollationMirrorSector);
	  }

	//список отраслей  deprecated
	static function getIndustryList(){
		$return = array();
		return $return;
	}

}