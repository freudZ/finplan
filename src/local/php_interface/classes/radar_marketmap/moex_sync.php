<?

use Bitrix\Highloadblock as HL;

//Синхронизатор с биржей
class MoexSync
{
    private $hasFullData;
    private $code;
    private $hlId;
    private $tableName;

    public function __construct()
    {
        \Bitrix\Main\Loader::includeModule("highloadblock");
    }

    public function processAction($item, $code, $hasFullData = false)
    {
        $this->hasFullData = $hasFullData;
        $this->code = $code;
        $this->hlId = 24;
        $this->tableName = "hl_moex_actions_data";

        //Полностью прогруженно
        if ($this->processItem() && !$hasFullData) {
            CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 73, "MOEX_LOADED");
        }
    }

    public function processObligation($item, $code, $hasFullData = false)
    {
        $this->hasFullData = $hasFullData;
        $this->code = $code;
        $this->hlId = 25;
        $this->tableName = "hl_moex_obligations_data";

        //Полностью прогруженно
        if ($this->processItem() && !$hasFullData) {
            CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 74, "MOEX_LOADED");
        }
    }

    private function processItem()
    {
        global $DB;

        $interval = 24;

        if ($this->hasFullData) {
            $from = new DateTime("-2days");
        } else {
            $from = $this->getFromDateByCode($this->code);
        }

        $dataClass = $this->getHlDataClass();

        if ($this->hlId == 24) {
            $type = "shares";
        } else {
            $type = "bonds";
        }

        $baseUrl = $url = "http://iss.moex.com/iss/engines/stock/markets/" . $type . "/securities/" . $this->code . "/candles.json?from=" . $from->format("Y-m-d") . "&till=" . date("Y-m-d") . "&interval=" . $interval;
        $offset = 0;

        $connection = \Bitrix\Main\Application::getConnection();
        while (true) {
            $chartData = ConnectMoex($baseUrl . "&start=" . $offset);

            //Если нет данных
            if (!$chartData["candles"]["data"]) {
                break;
            }


            $DB->StartTransaction();
            foreach ($chartData["candles"]["data"] as $elem) {
                $date = new DateTime($elem[7]);

                $item = [
                    "UF_ITEM" => "'".$this->code."'",
                    "UF_DATE" => "'".$date->format("Y-m-d")."'",
                    "UF_OPEN" => "'".$elem[0]."'", //OPEN
                    "UF_CLOSE" => "'".$elem[1]."'", //CLOSE
                    "UF_HIGH" => "'".$elem[2]."'", //HIGH
                    "UF_LOW" => "'".$elem[3]."'", //LOW
                ];

                $res = $dataClass::getList([
                    "filter" => [
                        "UF_ITEM" => $this->code,
                        "UF_DATE" => $date->format("d.m.Y"),
                    ],
                    "select" => [
                        "ID",
                    ],
                ]);

                if ($hlItem = $res->fetch()) {
                    $params = [];
                    foreach ($item as $k => $v) {
                        $params[] = $k."=".$v;
                    }
                    $sql =  "UPDATE ".$this->tableName." SET ".implode(", ", $params)." WHERE id = ".$hlItem["ID"];
                    $connection->query($sql);
                    //$dataClass::update($hlItem["ID"], $item);
                } else {

                    $sql = "INSERT INTO ".$this->tableName." (".implode(", ", array_keys($item)).") VALUES (".implode(", ", array_values($item)).");";
                    $connection->query($sql);
                    //$dataClass::add($item);
                }
            }
            $DB->Commit();

            $offset += 500;
        }

        return true;
    }

    private function getFromDateByCode($code)
    {
        global $APPLICATION;
        if ($this->hlId == 24) {
            $type = "shares";
        } else {
            $type = "bonds";
        }

        $chartDataTime = ConnectMoex("http://iss.moex.com/iss/history/engines/stock/markets/" . $type . "/securities/" . $code . "/dates.json");

        $from = new DateTime($chartDataTime["dates"]["data"][0][0]);
        $arChartData["real_from"] = $from;
        if ($from->getTimestamp() < $APPLICATION->minGraphDate) {
            $from = new DateTime("@" . $APPLICATION->minGraphDate);
        }

        return $from;
    }

    private function getHlDataClass()
    {
        $hlblock = HL\HighloadBlockTable::getById($this->hlId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
}
