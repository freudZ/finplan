<?php
/*
* Пояснения:
* (*)  - Мы принимаем массив array('VALUE' => , 'DESCRIPTION' => ) и должны его же вернуть. Если поле с описанием - оно будет содержаться в соответствующем ключе.
*/

class listElementWithDescription
{

// инициализация пользовательского свойства для инфоблока
    function GetIBlockPropertyDescription()
    {
        return array(
          "PROPERTY_TYPE" => "E", // основываемся на привязке к элементам
          "USER_TYPE" => "listElementWithDescription",
          "DESCRIPTION" => "Привязка к элементам с доп.описанием",
          'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
          "ConvertToDB" => array(__CLASS__,"ConvertToDB"),
          "ConvertFromDB" => array(__CLASS__,"ConvertFromDB"),
        );
    }
    
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $value["DESCRIPTION"] = unserialize($value["DESCRIPTION"]);

// значения по умолчанию
        $arItem = Array(
          "ID" => 0,
          "IBLOCK_ID" => 0,
          "NAME" => ""
        );

// получение информации по выбранному элементу
        if(intval($value["VALUE"]) > 0)
        {
            $arFilter = Array(
              "ID" => intval($value["VALUE"]),
              "IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"],
            );
            
            $arItem = \CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID", "IBLOCK_ID", "NAME"))->Fetch();
        }

// сама строка с товаром и доп.значениями
        $html =
          'Товар: <input name="'.$strHTMLControlName[" value"].'"="" id="'.$strHTMLControlName[" value="'.htmlspecialcharsex($value[" value"]).'"="" size="5" type="text">'.
' <span id="sp_'.md5($strHTMLControlName[" value"]).'"="">'.$arItem["NAME"].'</span>   '.
'<input type="button" value="Выбрать" onclick="jsUtils.OpenWindow(\'/bitrix/admin/iblock_element_search.php?lang='.LANG.'&IBLOCK_ID='.$arProperty[" link_iblock_id"].'&n=".$strHTMLControlName["VALUE"]." \',="" 600,="" 500);"="">   '.
    ' Количество:<input type="text" id="quan" name="'.$strHTMLControlName[" description"].'[1]"="" value="'.htmlspecialcharsex($value[" description"][1]).'"="">'
    ;

return  $html;
}
    
    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        return;
    }
    
    function ConvertToDB($arProperty, $value) // сохранение в базу данных
    {
        $return = false;
        
        if( is_array($value) && array_key_exists("VALUE", $value) )
        {
            $return = array(
              "VALUE" => serialize($value["VALUE"])
            );
        }

// сериализацию убирать не стал, если понадобится сохранять несколько значений
        if( is_array($value) && array_key_exists("DESCRIPTION", $value) )
            $return["DESCRIPTION"] = serialize($value["DESCRIPTION"]);
        
        return $return;
    }
    
    function ConvertFromDB($arProperty, $value) // извлечение значений из Базы Данных
    {
        $return = false;
        
        if(!is_array($value["VALUE"]))
        {
            $return = array(
              "VALUE" => unserialize($value["VALUE"])
            );
        }
        
        return $return;
    }
}