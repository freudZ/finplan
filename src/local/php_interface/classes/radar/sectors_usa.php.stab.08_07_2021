<?php

use Bitrix\Highloadblock as HL;

/**
 * Class SectorsUsa
 */
class SectorsUsa
{
    public $arOriginalsItems = array();
    public $arEmitentsBySector = array(); //Список эмитентов и их принадлежности к секторам
    public $actionsIblockId = 55; //Id инфоблока акций США
    public $fixHLIblockId = 43; //Id HL инфоблока с ручными правками
    private $actionsArrForPvot = array();
    public $arFixParams = array(); //Данные ручных правок
    public $arParamsList = array(); //Список параметров для которых считать средние показатели
    public $industriesUsaIblockId = 58; //Id инфоблока с отраслями США
    public $arKvartalToMonth = array();
    public $arAllIndustriesSumm = array();//Суммарные показатели всех отраслей
    public $arSectorsCurrentPeriods = array();//Только текущие периоды по секторам
    public $arMonthToKvartal = array(1  => 1,
                                     2  => 1,
                                     3  => 1,
                                     4  => 2,
                                     5  => 2,
                                     6  => 2,
                                     7  => 3,
                                     8  => 3,
                                     9  => 3,
                                     10 => 4,
                                     11 => 4,
                                     12 => 4
    );
    public $currentPeriodMonth = 1; //Цифра текущего месяца для добавления текущего периода
    //
    public function __construct($deleteTmpFixesForRealValues = false)
    {
        global $APPLICATION;
        CModule::IncludeModule("highloadblock");
        CModule::IncludeModule("iblock");
        $this->arParamsList = array(
          'Активы',
          'Оборотные активы',
          'Собственный капитал',
          'Прошлая капитализация',
          'Прибыль за год (скользящая)',
          'Выручка за год (скользящая)'
        );
        $this->arKvartalToMonth = $APPLICATION->quartDates;
        $this->currentPeriodMonth = intval(date('m'));
        $this->setData($deleteTmpFixesForRealValues);
    }
    
    private function getHLFixData($forPeriods = false)
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->fixHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        $arFilter = array("UF_COUNTRY" => "USA");
        $resHL = $entityClass::getList(array(
          "select" => array("*",),
          "filter" => $arFilter,
          "order"  => array("UF_YEAR" => "ASC", "UF_KVARTAL" => "ASC"),
        ));
        $arHLFixData = $resHL->fetchAll();
        
        foreach ($arHLFixData as $k => $v) {
            if ($forPeriods == false) {
                $arResult[$v["UF_SECTOR_NAME"]][$v["UF_PARAM_NAME"]][$v["UF_ACTIVE_CODE"]][$v["UF_YEAR"]][$v["UF_KVARTAL"]] = array(
                  "UF_TYPE"  => $v["UF_TYPE"],
                  "UF_VALUE" => $v["UF_VALUE"]
                );
            } else {
                $arResult[$v["UF_SECTOR_NAME"]][$v["UF_ACTIVE_CODE"]][$v["UF_YEAR"]][$v["UF_KVARTAL"]][$v["UF_PARAM_NAME"]] = array(
                  "UF_TYPE"  => $v["UF_TYPE"],
                  "UF_VALUE" => $v["UF_VALUE"]
                );
            }
            
        }
        return $arResult;
    }
    
    /**
     * Расчет сводных итогов по стране из всех отраслей
     * @param $arItems
     * @return array
     */
    public function getCalculatedAllIndustries($arItems)
    {
        $arReturn = array();
        
        foreach ($arItems as $name => $val) {//Отрасль
            foreach ($val["PERIODS"] as $period => $arData) {//Периоды
                $arPeriod = explode("-", $period);
                foreach ($arData as $paramName => $paramValue) {//Параметры периодов
                    if (in_array($paramName, array(
                      "P/E",
                      "P/B",
                      "P/Equity",
                      "P/Sale",
                      "PERIOD_VAL",
                      "PERIOD_YEAR",
                      "PERIOD_TYPE",
                      "Рентабельность собственного капитала",
                      "Доля собственного капитала в активах",
                      "Темп роста прибыли",
                      "Темп роста активов",
                      "Темп прироста выручки"
                    ))) {
                        continue;
                    }
                    $arReturn[$period][$paramName] += $paramValue;

					     if($paramName == 'CURRENT_PERIOD'){
							 $arReturn[$period]['CURRENT_PERIOD'] = $paramValue;
						  }
                    if (!isset($arReturn[$period]["PERIOD_VAL"])) {
                        $arReturn[$period]["PERIOD_VAL"] = $arPeriod[0];
                    }
                    if (!isset($arReturn[$period]["PERIOD_YEAR"])) {
                        $arReturn[$period]["PERIOD_YEAR"] = $arPeriod[1];
                    }
                    if (!isset($arReturn[$period]["PERIOD_TYPE"])) {
                        $arReturn[$period]["PERIOD_TYPE"] = $arPeriod[2];
                    }
                    //Считаем мульипликаторы при наличии показател прошлой капитализации
                    if (isset($arReturn[$period]["Прошлая капитализация"])) {
                        if (isset($arReturn[$period]["Прибыль за год (скользящая)"]) && floatval($arReturn[$period]["Прибыль за год (скользящая)"]) > 0) {
                            $arReturn[$period]["P/E"] = $arReturn[$period]["Прошлая капитализация"] / $arReturn[$period]["Прибыль за год (скользящая)"];
                        }
                        
                        if (isset($arReturn[$period]["Активы"]) && floatval($arReturn[$period]["Активы"]) > 0) {
                            $arReturn[$period]['P/B'] = $arReturn[$period]['Прошлая капитализация'] / $arReturn[$period]['Активы'];
                        }
                        
                        if (isset($arReturn[$period]["Собственный капитал"]) && floatval($arReturn[$period]["Собственный капитал"]) > 0) {
                            $arReturn[$period]['P/Equity'] = $arReturn[$period]['Прошлая капитализация'] / $arReturn[$period]['Собственный капитал'];
                        }
                        
                        if (isset($arReturn[$period]["Выручка за год (скользящая)"]) && floatval($arReturn[$period]["Выручка за год (скользящая)"]) > 0) {
                            $arReturn[$period]['P/Sale'] = $arReturn[$period]['Прошлая капитализация'] / $arReturn[$period]['Выручка за год (скользящая)'];
                        }
                    }
                    if (floatval($arReturn[$period]["Прибыль за год (скользящая)"]) > 0 && floatval($arReturn[$period]["Собственный капитал"]) > 0) {
                        $arReturn[$period]['Рентабельность собственного капитала'] = ($arReturn[$period]['Прибыль за год (скользящая)'] / $arReturn[$period]['Собственный капитал']) * 100;
                    }
                    
                    if (floatval($arReturn[$period]["Собственный капитал"]) > 0 && floatval($arReturn[$period]["Активы"]) > 0) {
                        $arReturn[$period]['Доля собственного капитала в активах'] = ($arReturn[$period]['Собственный капитал'] / $arReturn[$period]['Активы']) * 100;
                    }
                    
                    //Период "год назад"
                    if (isset($arReturn[$period]["PERIOD_VAL"]) && isset($arReturn[$period]["PERIOD_YEAR"])) {
                        $lastYearPeriod = $arReturn[$period]["PERIOD_VAL"] . "-" . ($arReturn[$period]["PERIOD_YEAR"] - 1) . "-KVARTAL";
                    } else {
                        $lastYearPeriod = false;
                    }
                    
                    
                    //Темп прироста активов
                    if (isset($arReturn[$period]["Активы"]) && $lastYearPeriod != false && isset($arReturn[$lastYearPeriod]["Активы"])) {
                        $arReturn[$period]["Темп роста активов"] = round(($arReturn[$period]["Активы"] / $arReturn[$lastYearPeriod]["Активы"] - 1) * 100,
                          2);
                    }
                    //Темп роста прибыли
                    if (isset($arReturn[$period]["Прибыль за год (скользящая)"]) && $lastYearPeriod != false && isset($arReturn[$lastYearPeriod]["Прибыль за год (скользящая)"])) {
                        if ($arReturn[$period]["Прибыль за год (скользящая)"] < 0 && $arReturn[$lastYearPeriod]["Прибыль за год (скользящая)"] < 0) {
                            $arReturn[$period]["Темп роста прибыли"] = "";
                        } else {
                            $arReturn[$period]["Темп роста прибыли"] = (($arReturn[$period]["Прибыль за год (скользящая)"] / $arReturn[$lastYearPeriod]["Прибыль за год (скользящая)"]) - 1) * 100;
                            
                            if ($arReturn[$lastYearPeriod]["Прибыль за год (скользящая)"] < 0) {
                                $arReturn[$period]["Темп роста прибыли"] = abs($arReturn[$period]["Темп роста прибыли"]);
                            }
                        }
                    }
                    
                    //Темп прироста выручки
                    if (isset($arReturn[$period]["Выручка за год (скользящая)"]) && isset($arReturn[$lastYearPeriod]["Выручка за год (скользящая)"])) {
                        $arReturn[$period]["Темп прироста выручки"] = round(($arReturn[$period]["Выручка за год (скользящая)"] / $arReturn[$lastYearPeriod]["Выручка за год (скользящая)"] - 1) * 100,
                          2);
                    }
                    
                    
                }//Параметры периодов
                
                
            }
            
        }
        
        /*      global $USER;
               $rsUser = CUser::GetByID($USER->GetID());
               $arUser = $rsUser->Fetch();
                if($arUser["LOGIN"]=="freud"){
                  echo "<pre  style='color:black; font-size:11px;'>";
                    print_r($arReturn);
                  echo "</pre>";
                }*/
        
        
        return $arReturn;
    }
    
    //Возвращает данные по сектору
    public function getItem($code, $needExpandedPeriods = false, $year = false)
    {
        foreach ($this->arOriginalsItems as $item) {
            if ($item["CODE"] === $code) {
                
                $item = $needExpandedPeriods ? $this->getExpandedPeriods($item, $year) : $item;
                
                return $item;
            }
        }
        return false;
    }
    
    public function getSectorActionsData($sectorName)
    {
        foreach ($this->actionsArrForPvot as $k => $item) {
            
            if ($k === $sectorName) {
                return $item;
            }
        }
        return false;
    }
    
    public function GetSectorByEmitentId($emitentId = 0)
    {
        $arReturn = array();
        $sectorName = $this->arEmitentsBySector[$emitentId];
        foreach ($this->arOriginalsItems as $item) {
            if ($item["NAME"] === $sectorName) {
                unset($item["PERIODS"]);
                $arReturn = $item;
            }
        }
        
        return $arReturn;
    }
    
    public function GetSectorByName($sectorName = '')
    {
        $arReturn = array();
        foreach ($this->arOriginalsItems as $item) {
            if ($item["NAME"] === $sectorName) {
                unset($item["PERIODS"]);
                $arReturn = $item;
            }
        }
        
        return $arReturn;
    }
    
    private function setData($deleteTmpFixesForRealValues = false, $year = false)
    {
        global $APPLICATION;
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheId = "sectors_usa_data";
        if ($deleteTmpFixesForRealValues) {
            $cache->clean($cacheId);
        }
        $cacheTtl = 86400 * 7;

        if ($cache->read($cacheTtl, $cacheId)) {
            $arItemsCached = $cache->get($cacheId);
        } else {
            $arFixHandData = array();
            $arFixHandData = $this->getHLFixData(true);//Заполняем данные ручных правок
            $arRealFixData = array();//Данные о том, что для временного значения появилось реальное значение
            //
            $arItems = array();
            $arFilter = [
              "IBLOCK_ID"      => $this->actionsIblockId,
              "PROPERTY_SP500" => '%SP500%',
              "!PROPERTY_TYPE" => false,
              "=PROPERTY_COMMON_MITENT_ID"=>false,//Исключаем активы с общими эмитентами (типа ОЗОН)
              //'PROPERTY_SECID' => 'AMZN'
            ];
            $res = CIBlockElement::GetList([], $arFilter, false, false, [
              "ID",
              "IBLOCK_ID",
              "CODE",
              "NAME",
              "PROPERTY_ISSUECAPITALIZATION",
              "PROPERTY_SECID",
              "PROPERTY_SECTOR",
              "PROPERTY_EMITENT_ID",
              "PROPERTY_TYPE"
            ]);
            
            $arResult = array();
            $arActionsResult = array();
            $arEmitentsBySector = array();
            $resU = new ActionsUsa();
            $defaultArr = [
              'Прошлая капитализация'                => 0,
              'Прибыль за год (скользящая)'          => 0,
              'Выручка за год (скользящая)'          => 0,
              'P/E'                                  => 0,
              'Собственный капитал'                  => 0,
              'Активы'                               => 0,
              'Оборотные активы'                     => 0,
              'Рентабельность собственного капитала' => 0,
              'profit_temp'                          => 0,
              'proceeds_temp'                        => 0,
            ];
            
            
            //Cумма текущих капитализаций по секторам
            $arSectorsCapa = array();
            
            while ($item = $res->fetch()) {
                if ($item['PROPERTY_SECTOR_VALUE']) {
                    //if ($item['PROPERTY_SECTOR_VALUE'] && $item['PROPERTY_ISSUECAPITALIZATION_VALUE']) {
                    $actionItem = $resU->getItem($item["CODE"]);
                    
                    
                    if ($actionItem["PERIODS"]) {
                        
                        $arActionsResult[$item['PROPERTY_SECTOR_VALUE']][$actionItem['NAME']] = array(
                          "ACTION_CODE" => $item["CODE"],
                          "SECID"       => $actionItem["PROPS"]["SECID"],
                          "PERIODS"     => $actionItem['PERIODS']
                        );
                        //проверим присутствие в классе акций периодов из правочных данных и дополним необходимые периоды пустыми ключами, что бы
                        //было куда правочные данные показывать
                        foreach ($arFixHandData[$item['PROPERTY_SECTOR_VALUE']][$actionItem["PROPS"]["SECID"]] as $fYear => $fKvartals) {
                            foreach ($fKvartals as $fKvartal => $data) {
                                if (!array_key_exists($fKvartal . "-" . $fYear . "-KVARTAL", $actionItem["PERIODS"])) {
                                    $actionItem["PERIODS"][$fKvartal . "-" . $fYear . "-KVARTAL"] = array();
                                }
                            }
                            
                        }
                        
                        
                        foreach ($actionItem["PERIODS"] as $period => $periodArr) {
//тут в периодах нет 4 кв. 2020, по этому не выбираются фиксы в таблицу
                            //Проверим есть ли фикс значения для данного периода
                            $arPeriod = explode("-", $period);
                            foreach ($this->arParamsList as $paramName) {
//echo $period."->".$periodArr[$paramName]."<br>";
                                
                                try {
                                    $arFixParamValues = $arFixHandData[$item['PROPERTY_SECTOR_VALUE']][$actionItem["PROPS"]["SECID"]][$arPeriod[1]][$arPeriod[0]][$paramName];
                                    
                                    if ($arFixParamValues["UF_TYPE"] == "TMP") {
                                        if (floatval($periodArr[$paramName]) == 0) {
                                            $periodArr[$paramName] = $arFixParamValues["UF_VALUE"];
                                            $arActionsResult[$item['PROPERTY_SECTOR_VALUE']][$actionItem['NAME']]["PERIODS"][$period][$paramName] = $arFixParamValues["UF_VALUE"];
                                        }
                                        
                                        
                                    } elseif ($arFixParamValues["UF_TYPE"] == "ALWAYS") {
                                        $periodArr[$paramName] = $arFixParamValues["UF_VALUE"];
                                        $arActionsResult[$item['PROPERTY_SECTOR_VALUE']][$actionItem['NAME']]["PERIODS"][$period][$paramName] = $arFixParamValues["UF_VALUE"];
                                    }
                                } catch (Exception $e) {
                                
                                }
                                
                            }
                            
                            if (!isset($arResult[$item['PROPERTY_SECTOR_VALUE']][$period])) {
                                $arResult[$item['PROPERTY_SECTOR_VALUE']][$period] = $defaultArr;
                            }
                            
                            if (!$arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['PERIOD_VAL']) {
                                $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['PERIOD_VAL'] = $periodArr['PERIOD_VAL'];
                            }
                            
                            if (!$arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['PERIOD_YEAR']) {
                                $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['PERIOD_YEAR'] = $periodArr['PERIOD_YEAR'];
                            }
                            
                            if (!$arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['PERIOD_TYPE']) {
                                $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['PERIOD_TYPE'] = $periodArr['PERIOD_TYPE'];
                            }
                            
                            $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['Прошлая капитализация'] += $periodArr['Прошлая капитализация'];
                            $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['Прибыль за год (скользящая)'] += $periodArr['Прибыль за год (скользящая)'];
                            $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['Выручка за год (скользящая)'] += $periodArr['Выручка за год (скользящая)'];
                            $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['Собственный капитал'] += $periodArr['Собственный капитал'];
                            $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['Активы'] += $periodArr['Активы'];
                            $arResult[$item['PROPERTY_SECTOR_VALUE']][$period]['Оборотные активы'] += $periodArr['Оборотные активы'];
                        }
                        
                    }
                }
                
                //Суммируем только текущую капу действующих акций, и только уже отобранных для случая, когда у эмитента несколько видов акций
                if (!empty(trim($item['PROPERTY_SECTOR_VALUE']))) {
                    $arSectorsCapa[$item['PROPERTY_SECTOR_VALUE']] = $arSectorsCapa[$item['PROPERTY_SECTOR_VALUE']] + floatval($item['PROPERTY_ISSUECAPITALIZATION_VALUE']);
                    //Соберем id эмитентов по секторам
                    $arEmitentsBySector[$item['PROPERTY_EMITENT_ID_VALUE']] = $item['PROPERTY_SECTOR_VALUE'];
                }
                
            }
            
            foreach ($arResult as $sector => $periods) {
                foreach ($periods as $period => $periodArr) {
                    $arResult[$sector][$period]['P/E'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Прибыль за год (скользящая)'];
                    $arResult[$sector][$period]['Рентабельность собственного капитала'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Прибыль за год (скользящая)'];
                    $arResult[$sector][$period]['Доля собственного капитала в активах'] = ($arResult[$sector][$period]['Собственный капитал'] / $arResult[$sector][$period]['Активы']) * 100;
                    $arResult[$sector][$period]['P/B'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Активы'];
                    $arResult[$sector][$period]['P/Equity'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Собственный капитал'];
                    $arResult[$sector][$period]['P/Sale'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Выручка за год (скользящая)'];
                    
                }
            }

            $arSelect = array(
              "ID",
              "IBLOCK_ID",
              "CODE",
              "NAME",
              "PROPERTY_ISSUECAPITALIZATION",
              "PROPERTY_SECID",
            );
            $res = CIBlockElement::GetList([], ["IBLOCK_ID" => $this->industriesUsaIblockId], false, false, $arSelect);
            
            while ($sectorItem = $res->getNext()) {
                $sectorItem['PERIODS'] = $arResult[$sectorItem['NAME']];
                
                //Сортируем периоды
/*                uasort($sectorItem["PERIODS"], function ($item1, $item2) {
                    $result = false;
						  try {
                    if ($item1["PERIOD_TYPE"] == "KVARTAL" && $item1["PERIOD_YEAR"] > 2000 && $item2["PERIOD_YEAR"] > 2000) {
                        $dstr1 = $item1["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$item1["PERIOD_VAL"]];
                        $dt1 = new DateTime($dstr1);
                        $dstr2 = $item2["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$item2["PERIOD_VAL"]];
                        $dt2 = new DateTime($dstr2);
                        $result = $dt1->getTimestamp() < $dt2->getTimestamp();
                    }
						  } catch (Exception $e) {
						  	CLogger::SectorsUsaClassStr439($e->getMessage().print_r($item1, true));
							   // echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
							}
                    return $result;
                });*/

			//Первая сортировка для получения последнего периода в качестве опорного для добавления текущего
			uasort($sectorItem["PERIODS"], function ($a, $b) {
				$res = 0;

				// сравниваем кварталы в виде дат приведенных к 1 числу первого месяца каждого квартала
				if($a["PERIOD_TYPE"] == "KVARTAL"){
				$addDateA = new DateTime($a["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$a["PERIOD_VAL"]] . '-01'); $addDateB = new DateTime($b["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$b["PERIOD_VAL"]] . '-01');
				if ($addDateA != $addDateB) {
						return ($addDateA < $addDateB) ? 1 : -1; //desc
				}
				}

				return $res;
			});

                $sectorItem = $this->calculatePeriodData($sectorItem);
                CIBlockElement::SetPropertyValuesEx($sectorItem["ID"], $this->industriesUsaIblockId,
                  array("ISSUECAPITALIZATION" => round($arSectorsCapa[$sectorItem['NAME']] / 1000000, 2)));

			//Вторая сортировка после добавления текущего периода. Без нее он не оказывается первым в массиве
			uasort($sectorItem["PERIODS"], function ($a, $b) {
				$res = 0;

				// сравниваем кварталы в виде дат приведенных к 1 числу первого месяца каждого квартала
				if($a["PERIOD_TYPE"] == "KVARTAL"){
				$addDateA = new DateTime($a["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$a["PERIOD_VAL"]] . '-01'); $addDateB = new DateTime($b["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$b["PERIOD_VAL"]] . '-01');
				if ($addDateA != $addDateB) {
						return ($addDateA < $addDateB) ? 1 : -1; //desc
				}
				}

				return $res;
			});

                $arItems[] = $sectorItem;
            }
            
            $arFixHandData = $this->getHLFixData();//Заполняем данные ручных правок
            $arSectorsCurrentPeriods = array();
            foreach($arItems as $sectorItem){
                foreach($sectorItem["PERIODS"] as $arPeriod){
                    if($arPeriod["CURRENT_PERIOD"]==true){
                        $arSectorsCurrentPeriods[$sectorItem["NAME"]] = $arPeriod;
                        break;
                    }
                }
            }
            
            $arAllIndustriesSumm = array();
            $arAllIndustriesSumm = $this->getCalculatedAllIndustries($arItems);//Расчет сводных итогов по стране из всех отраслей
            //
            $arItemsCached = array(
              "arActionsResult"    => $arActionsResult,
              "arEmitentsBySector" => $arEmitentsBySector,
              "arItems"            => $arItems,
              "arFixHandData"      => $arFixHandData,
              "arAllIndustriesSumm" => $arAllIndustriesSumm,
              "arSectorsCurrentPeriods" => $arSectorsCurrentPeriods,
            );
            
            
            $cache->set($cacheId, $arItemsCached);
            
            
        }
        
        
        $this->arOriginalsItems = $arItemsCached["arItems"];
        $this->actionsArrForPvot = $arItemsCached["arActionsResult"];
        $this->arFixParams = $arItemsCached["arFixHandData"];
        $this->arEmitentsBySector = $arItemsCached["arEmitentsBySector"];
        $this->arAllIndustriesSumm = $arItemsCached["arAllIndustriesSumm"];
        $this->arSectorsCurrentPeriods = $arItemsCached["arSectorsCurrentPeriods"];
        
        unset($resU);
    }
    
    function getExpandedPeriods($arResult, $year = false)
    {
        $limit = 5;
        if (intval($year)) {
            $limit = 4;
        }
        if (is_string($year) && $year == 'all') {
            $limit = 999;
        }
        if ($arResult['PERIODS']) {
            foreach ($arResult['PERIODS'] as $period => $vals) {
                if (strpos($period, "MONTH") !== false) {
                    continue;
                }
                if ($year != false && intval($year) > 0) {
                    if ($vals["PERIOD_YEAR"] != $year) {
                        continue;
                    }
                }
                $arResult["EXPANDED_PERIODS"]["PERIODS"][] = $period;
                if (count($arResult["EXPANDED_PERIODS"]["PERIODS"]) >= $limit) {
                    break;
                }
            }
            foreach ($arResult["EXPANDED_PERIODS"]["PERIODS"] as $period) {
                foreach ($arResult['PERIODS'][$period] as $f => $v) {
                    if (in_array($f, ['profit_temp', 'proceeds_temp', 'PERIOD_VAL', 'PERIOD_YEAR', 'PERIOD_TYPE'])) {
                        continue;
                    }
                    $arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"][$f] = true;
                }
            }
            
            $arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"]);
            
            $needPeriod = 4;
            
            foreach ($arResult['PERIODS'] as $period => $vals) {
                $x[] = $vals;
                if ($vals["PERIOD_VAL"] == $needPeriod || !$vals["PERIOD_VAL"]) {
                    
                    $arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"][] = $period;
                    
                    if (count($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) >= $limit) {
                        break;
                    }
                }
            }
            
            foreach ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $period) {
                foreach ($arResult['PERIODS'][$period] as $f => $v) {
                    if (in_array($f, ['profit_temp', 'proceeds_temp', 'PERIOD_VAL', 'PERIOD_YEAR', 'PERIOD_TYPE'])) {
                        continue;
                    }
                    $arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"][$f] = true;
                }
            }
            $arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"]);
            
            //для графика квартала, берем последний - 1 квартал
            if ($arResult["EXPANDED_PERIODS"]["PERIODS"]) {
                $arKvartalToMonth = array(
                  1 => "03-31",
                  2 => "06-30",
                  3 => "09-30",
                  4 => "12-31",
                );
                
                $t = array_reverse($arResult["EXPANDED_PERIODS"]["PERIODS"]);
                $last = explode("-", $t[0]);
                
                if ($last[0] == 1) {
                    $last[0] = 4;
                    $last[1] -= 1;
                } else {
                    $last[0] -= 1;
                }
                $from = $last[1] . "-" . $arKvartalToMonth[$last[0]];
                
                /*
                1 квартал с 1 января по 31 марта
                2 квартал с 1 апреля по 30 июня
                3 квартал с 1 июля по 30 сентября
                4 квартал с 1 октбяря по 31 декабря
                */
                
                $arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"] = $this->getDatesArray($from);
                
            }
            
            //для года берем начала последнего года
            if ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) {
                $t = array_reverse($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]);
                $last = explode("-", $t[0]);
                $from = $last[1] . "-01-01";
                //график
                $arResult["EXPANDED_PERIODS"]["YEAR_CHART"] = $this->getDatesArray($from);
            }
        }
        
        return $arResult;
    }
    
    private function getDatesArray($startDate)
    {    //TODO написать защиту для корректной загрузке и здесь обход кривых значений
         $startDate = str_replace("Отрасль OLD", "2011",$startDate);
         $startDate = str_replace("Отрасль NEW", "2011",$startDate);
$startDate = str_replace("Сектор NEW-", "2011-01-01",$startDate);
$startDate = str_replace("Сверка с QUIK 22/07/2020-01-01", "2011-01-01",$startDate);
$startDate = str_replace("Ссылка на страницу компании в Радаре-", "2011",$startDate);

        try {
            $startDate = new DateTime($startDate);
        } finally {
            $startDate = new DateTime(date());
        }
        //$startDate = new DateTime($startDate);
        $to = (new DateTime(date('d.m.Y')))->add(new DateInterval('P2M'));
          
          $interval_val = 'P1M';
          $startDate = $startDate->modify('first day of this month');
          $period = new DatePeriod($startDate, //new DateInterval('P' . $interval . 'D'),
            new DateInterval($interval_val), $to);
          $getDates = [];
          foreach ($period as $key => $value) {
              $getDates[] = [
                FormatDate("M. Y", $value->getTimestamp()),
              ];
          }
 
        
        return $getDates;
    }
    
    function calculatePeriodData($item)
    {
        foreach ($item["PERIODS"] as $n => $val) {
            $lastYearPeriod = '';
            $lastYearPeriod = $val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL";
            //Темп прироста активов
            if ($val["Активы"] && $item["PERIODS"][$lastYearPeriod]["Активы"]) {
                $item["PERIODS"][$n]["Темп роста активов"] = round(($val["Активы"] / $item["PERIODS"][$lastYearPeriod]["Активы"] - 1) * 100,
                  2);
            }
            //Темп роста прибыли
            if ($val["Прибыль за год (скользящая)"] && $item["PERIODS"][$lastYearPeriod]["Прибыль за год (скользящая)"]) {
                if ($val["Прибыль за год (скользящая)"] < 0 && $item["PERIODS"][$lastYearPeriod]["Прибыль за год (скользящая)"] < 0) {
                    $item["PERIODS"][$n]["Темп роста прибыли"] = "";
                } else {
                    $item["PERIODS"][$n]["Темп роста прибыли"] = (($val["Прибыль за год (скользящая)"] / $item["PERIODS"][$lastYearPeriod]["Прибыль за год (скользящая)"]) - 1) * 100;
                    
                    if ($item["PERIODS"][$lastYearPeriod]["Прибыль за год (скользящая)"] < 0) {
                        $item["PERIODS"][$n]["Темп роста прибыли"] = abs($item["PERIODS"][$n]["Темп роста прибыли"]);
                    }
                }
            }
            
            //Темп прироста выручки
            if ($val["Выручка за год (скользящая)"] && $item["PERIODS"][$lastYearPeriod]["Выручка за год (скользящая)"]) {
                $item["PERIODS"][$n]["Темп прироста выручки"] = round(($val["Выручка за год (скользящая)"] / $item["PERIODS"][$lastYearPeriod]["Выручка за год (скользящая)"] - 1) * 100,
                  2);
            }
        }
    
        if(\Bitrix\Main\Config\Option::get("grain.customsettings","ADD_CURRENT_PERIOD")=="Y" ) {
            $this->addCurrentPeriod($item['PERIODS'],$item);
        }
        
        return $item;
    }
    /**
     * Добавляет текущий период (все данные из последнего существующего периода, а капа из акции текущая + мультики считаются)
     * @param $arPeriods
     * @param $item
     */
    public function addCurrentPeriod(&$arPeriods, $item)
    {
        //  $arPeriods = array_reverse($arPeriods);
        $arLastPeriod = reset($arPeriods);
        $arrPeriodName = array_keys($arPeriods);
        $arrPeriodName = reset($arrPeriodName);
        $arPeriodName = explode("-", $arrPeriodName);
        unset($arrPeriodName);
        
        $arPeriods = array_reverse($arPeriods);
        
        /*        global $USER;
                $rsUser = CUser::GetByID($USER->GetID());
                $arUser = $rsUser->Fetch();
                if($arUser["LOGIN"]=="freud"){
                    echo "<pre  style='color:black; font-size:11px;'>";
                    print_r($item);
                    echo "</pre>";
                }*/
        
        $arLastPeriod["CURRENT_PERIOD"] = true;
        //Капитализация из акции текущая
        $arLastPeriod["Прошлая капитализация"] = $item["PROPERTY_ISSUECAPITALIZATION_VALUE"] ;
        //$currPeriodName = $this->arMonthToKvartal[$this->currentPeriodMonth] . "-" . $arPeriodName[1] . "-" . $arPeriodName[2];
        $currPeriodName = $this->arMonthToKvartal[$this->currentPeriodMonth] . "-" . date('Y') . "-" . $arPeriodName[2];

        //$arLastPeriod["PERIOD_YEAR"] = $arPeriodName[1];
        $arLastPeriod["PERIOD_YEAR"] = date('Y');
        $arLastPeriod["PERIOD_VAL"] = $this->arMonthToKvartal[$this->currentPeriodMonth];
        
        $arPeriods[$currPeriodName] =  $arLastPeriod;
        unset($arLastPeriod);
        
        /*        global $USER;
                $rsUser = CUser::GetByID($USER->GetID());
                $arUser = $rsUser->Fetch();
                if($arUser["LOGIN"]=="freud"){
                    echo $item["NAME"]." ".$currPeriodName."<pre  style='color:black; font-size:11px;'>";
                    print_r($arPeriods[$currPeriodName]);
                    echo "</pre>";
                }*/
        
        //Расчет мультипликаторов
        //P/E
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Прибыль за год (скользящая)"]) {
            $arPeriods[$currPeriodName]["P/E"] = round($arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Прибыль за год (скользящая)"],
              2);
        }
        
        //P/B
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Активы"]) {
            $arPeriods[$currPeriodName]["P/B"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Активы"];
        }
        
        //P/Equity
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Собственный капитал"]) {
            $arPeriods[$currPeriodName]["P/Equity"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Собственный капитал"];
        }
        
        //P/Sale
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Выручка за год (скользящая)"]) {
            $arPeriods[$currPeriodName]["P/Sale"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Выручка за год (скользящая)"];
        }
        
        if ($arPeriods[$currPeriodName]["P/E"] > 0 && $arPeriods[$currPeriodName]["AVEREGE_PROFIT"] > 0) {
            //$arPeriods[$currPeriodName]["AVEREGE_PROFIT"] = floatval($averageProfitSum / 5);
            $arPeriods[$currPeriodName]["PEG"] = floatval($arPeriods[$currPeriodName]["P/E"]) / floatval($arPeriods[$currPeriodName]["AVEREGE_PROFIT"]);
        }
        
        //EV/EBITDA
        if ($arPeriods[$currPeriodName]["EBITDA"]) {
            $arPeriods[$currPeriodName]["EV/EBITDA"] = floatval($arPeriods[$currPeriodName]["Прошлая капитализация"] + $arPeriods[$currPeriodName]["Активы"] - $arPeriods[$currPeriodName]["Собственный капитал"]) / floatval($arPeriods[$currPeriodName]["EBITDA"]);
            if ($arPeriods[$currPeriodName]["EV/EBITDA"] == INF) {
                $arPeriods[$currPeriodName]["EV/EBITDA"] = 0;
            }
        }
        //DEBT/EBITDA
        if ($arPeriods[$currPeriodName]["EBITDA"]) {
            $arPeriods[$currPeriodName]["DEBT/EBITDA"] = floatval($arPeriods[$currPeriodName]["Активы"] - $arPeriods[$currPeriodName]["Собственный капитал"]) / floatval($arPeriods[$currPeriodName]["EBITDA"]);
            if ($arPeriods[$currPeriodName]["DEBT/EBITDA"] == INF) {
                $arPeriods[$currPeriodName]["DEBT/EBITDA"] = 0;
            }
        }
        //$arPeriods = $this->calculateCapa($arPeriods, $item, true,$currPeriodName);
        $arPeriods = array_reverse($arPeriods);

        unset($arPeriodName, $currPeriodName);
        //return $arPeriods;
    }
}