<?
//����� ��� ������ � ������� ���
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");

class CFundsUsa {
  	public $arFundsData = array();
  	public $arFundsDataByTickers = array();
  	public $arFundsNames = array();
	public $fundsTableName = 'hl_usa_funds_data';

	function __construct() {
	 	$this->setData();
	}

	public function getFundsNames(){
		Global $DB;
		//�������� ���������� �������� ������
		$arFunds = array();
		$query = "SELECT DISTINCT `UF_FUND` FROM `$this->fundsTableName` ORDER BY `UF_FUND`";
		$res = $DB->Query($query);
		$arFunds = array();
		while($row = $res->fetch()){
		  $arFunds[] = $row["UF_FUND"];
		}
		$this->arFundsNames = $arFunds;
		unset($arFunds);
		//return $this->arFundsNames;
	}

	/**
	 * ��������� ������ �� ������ ������ ��� ���������� � �������� ��� ����������
	 *
	 * @return [add type]  [add description]
	 *
	 * @access public
	 */
	public function setData(){
	  $arResult = array();
	  Global $DB;


	  $this->getFundsNames();
	  foreach($this->arFundsNames as $fundName){
	      $inTickers = array();
	      $outTickers = array();
			$query = "SELECT DISTINCT `UF_DATE` FROM `$this->fundsTableName` WHERE `UF_FUND` = '$fundName' ORDER BY `UF_DATE` DESC LIMIT 2";
			$res = $DB->Query($query);
			$arLastDatesFundsReports = array();
			$arLastDatesFunds = array();
			while($row = $res->fetch()){
			  $arLastDatesFundsReports[] = "'".$row["UF_DATE"]."'";
			  $arLastDatesFunds[] = $row["UF_DATE"];
			}

			$query = "SELECT `UF_DATE`, `UF_TICKER`, `UF_VALUE` FROM `$this->fundsTableName` WHERE `UF_FUND` = '$fundName' AND `UF_TICKER` != '' AND `UF_DATE` IN (".implode(", ", $arLastDatesFundsReports).") GROUP BY `UF_DATE`, `UF_TICKER` ORDER BY `UF_DATE` DESC, `UF_VALUE` DESC";
			$res = $DB->Query($query);
			$allTickersInReport = array();
			$arFundsReportTickers = array();
			while($row = $res->fetch()){
			  $arFundsReportTickers[$row["UF_DATE"]][$row["UF_TICKER"]] = $row["UF_VALUE"];
			  $allTickersInReport[$row["UF_TICKER"]] = '';
			}
			 //�������� ������
			 $inTickers = array_diff_key($arFundsReportTickers[$arLastDatesFunds[0]], $arFundsReportTickers[$arLastDatesFunds[1]]);
			 //�������� ������
			 $outTickers = array_diff_key($arFundsReportTickers[$arLastDatesFunds[1]], $arFundsReportTickers[$arLastDatesFunds[0]]);


		$this->arFundsData[$fundName] = array("BALANCE"=>$arFundsReportTickers[$arLastDatesFunds[0]], "IN"=>$inTickers, "OUT"=>$outTickers);
	  }

	  //�������������� ������ �������� ������ (�����)
	  foreach($this->arFundsData as $fund=>$data){
		  foreach($data["BALANCE"] as $ticker=>$value){
			 $this->arFundsDataByTickers[$ticker][$fund]["BALANCE"] = true;
			 $this->arFundsDataByTickers[$ticker][$fund]["IN"] = false;
			 $this->arFundsDataByTickers[$ticker][$fund]["OUT"] = false;
		  }
		  foreach($data["IN"] as $ticker=>$value){
			 $this->arFundsDataByTickers[$ticker][$fund]["IN"] = true;
			 $this->arFundsDataByTickers[$ticker][$fund]["OUT"] = false;
		  }
		  foreach($data["OUT"] as $ticker=>$value){
		  	$this->arFundsDataByTickers[$ticker][$fund]["BALANCE"] = false;
		  	$this->arFundsDataByTickers[$ticker][$fund]["IN"] = false;
			 $this->arFundsDataByTickers[$ticker][$fund]["OUT"] = true;

		  }
	  }


	}

}