<?
use Bitrix\Highloadblock as HL;

//Акции
class ETF extends RadarBase {
	private $arItems = array();
	public $arOriginalsItems = array();
	public $count;
	private $perPage = 5;
	public $havePayAccess = false;
   public $classType = self::TYPE_ACTION;
	public $currencyRateUSD = 1;

	function __construct(){
		$this->setData();
		$this->havePayAccess = checkPayRadar();
	}

	//отдельный элемент по коду
	public function getItem($code){

		foreach($this->arOriginalsItems as $item){
			if($item["SECID"]==$code){
				return $item;
			}
		}
		
		return false;
	}

	//отдельный элемент по коду
	public function getItemByCode($code) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["PROPS"]["ISIN"] == $code) {
				return $item;
			}
		}
		return false;
	}

	//поиск по названию
	public function searchByName($q, $limit = 10, $inview = array()){
		if(!$q){
			return false;
		}
		$arItems = array();

		//параметр для поиска в зависимости от типа
        $key = 'SECID';
        $boardIDs = [
			'TQTF',
			'TQTD',
			'TQTE',
        ];

		foreach($this->arOriginalsItems as $item){
			if(stripos($item["NAME"], $q) !== false || ($key && stripos($item['PROPS'][$key], $q) !== false)){
				if(in_array($item["PROPS"]["SECID"], $inview) || ($this->classType === self::TYPE_ACTION && !in_array($item["PROPS"]["BOARDID"], $boardIDs))){
					continue;
				}
				$arItems[] = $item;

				if(count($arItems)>=$limit){
					break;
				}
			}
		}

		return $arItems;
	}

	//акции с просадом менее 10%
	public function getLowProsad(){
		global $APPLICATION;
		$arReturn = array();
		
		foreach($this->arOriginalsItems as $item){
			if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["etf"])){ //Исключаем не etf активы
				continue;
			}
			if($item["DYNAM"]["Просад"]<10){
				$arReturn[] = array(
					$item["SECID"],
					$item["NAME"],
					$item["DYNAM"]["Просад"]
				);
			}
		}
		
		return $arReturn;
	}
	
	//etf с таргетом менее 10%
	public function getLowTarget(){ //Пока отключено
		global $APPLICATION;
		$arReturn = array();
		
/*		foreach($this->arOriginalsItems as $item){
			if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["etf"])){  //Исключаем не etf активы
				continue;
			}

			if($item["DYNAM"]["Таргет"]<10){
				$arReturn[] = array(
					$item["SECID"],
					$item["NAME"],
					$item["DYNAM"]["Таргет"]
				);
			}
		}*/

		return $arReturn;
	}

	//Возвращает уникальные значения для фильтров etf
	public function getRadarFiltersValues(){
		global $APPLICATION;
	   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "etf_filters_data";
		$cacheTtl = 86400*7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arFilterEnums = $cache->get($cacheId);
		} else {
				CModule::IncludeModule("iblock");
			   $arFilterEnums = array("ETF_TYPE_OF_ACTIVES"=>array(), "ETF_DIVIDENDS"=>array(),
											  "ETF_EXCHANGE"=>array(), "ETF_REPLICATION"=>array(),
											  "ETF_TYPE"=>array(),"EMITENT_ID"=>array(),
											  "ETF_CURRENCY"=>array(), "ETF_COUNTRY"=>array(),
											  "ETF_BASE_CURRENCY"=>array(), "ETF_INDUSTRY"=>array()
											  );

			  foreach($arFilterEnums as $k=>$v){
				 $rsResult = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 32,'PROPERTY_IS_ETF_ACTIVE_VALUE'=>'Y', 'PROPERTY_HIDEN'=>false, '!PROPERTY_'.$k.'_VALUE' => false,'!NAME'=>false ),array('PROPERTY_'.$k));

				 while($arResult=$rsResult->Fetch()){
				   if(!empty($arResult['PROPERTY_'.$k.'_VALUE']))
				   $arFilterEnums[$k][]=$arResult['PROPERTY_'.$k.'_VALUE'];
			    }
			  }
				if(count($arFilterEnums["EMITENT_ID"])>0){
					$arSelect = Array("ID", "NAME", "IBLOCK_ID");
					$arFilter = Array("IBLOCK_ID"=>IntVal(26), "ID"=>$arFilterEnums["EMITENT_ID"]);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
					while($arFields = $res->Fetch()){

					 $arFilterEnums["EMITENT_NAME"][$arFields["ID"]] = $arFields["NAME"];
					}
				}


		   $cache->set($cacheId, $arFilterEnums);
		}
		return $arFilterEnums;
	}


	private function setData(){
		global $APPLICATION;

		$this->currencyRateUSD = getCBPrice("USD", (new DateTime())->format("d/m/Y"));

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "etf_data";
		$cacheTtl = 86400*7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arItems = $cache->get($cacheId);
		} else {
			$arItems = array();

			CModule::IncludeModule("iblock");
			CModule::IncludeModule("highloadblock");
			$ibID = 32;

			$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

			$res = $entityClass::getList(array(
				"filter" => array(
					"!UF_YEAR" => false,
				),
				"select" => array(
					"*"
				),
				"order" => array(
					"ID" => "DESC"
				)
			));
			while($item = $res->fetch()){
				$item["UF_DATA"] = json_decode($item["UF_DATA"], true);
				
				if(isset($item["UF_KVARTAL"])){
					$type = "KVARTAL";
					$t = $item["UF_KVARTAL"];
				} else {
					$type = "MONTH";
					$t = $item["UF_MONTH"];
				}
				
				$item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
				$item["UF_DATA"]["PERIOD_VAL"] = $t;
				$item["UF_DATA"]["PERIOD_TYPE"] = $type;
				
				$arPeriods[$item["UF_COMPANY"]][$t."-".$item["UF_YEAR"]."-".$type] = $item["UF_DATA"];
			}

			//Предыдущий день
			$prevDay = new DateTime();
			$prevDay->modify("-1 day");

			$arFilter = Array("IBLOCK_ID"=>$ibID, "PROPERTY_IS_ETF_ACTIVE_VALUE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
			while($ob = $res->GetNextElement())
			{
				$fields = $ob->GetFields();

				$item = array(
					"ID" => $fields["ID"],
					"NAME" => $fields["NAME"],
					"SECID" => $fields["CODE"],
					//"URL" => $fields["DETAIL_PAGE_URL"],
				);
				
				//страница акции
				$arFilter = Array("IBLOCK_ID"=>33, "CODE"=>$fields["CODE"]);
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL"));
				if($val = $res2->GetNext())
				{
					$item["URL"] = str_replace("/actions/","/etf/",$val["DETAIL_PAGE_URL"]);
				}
				
				
				$props = $ob->GetProperties();


				foreach($props as $prop){
					if($prop["VALUE"] || $prop["CODE"]=="HIDEN"){
						if($prop["CODE"]=="PROP_TARGET"){
							$prop["VALUE"] = str_replace("%", "", $prop["VALUE"]);
						}
						$item["PROPS"][$prop["CODE"]] = $prop["VALUE"];
					}

				}



				$item["PROPS"]["PROP_PROSAD"] = str_replace(",", ".", $item["PROPS"]["PROP_PROSAD"]);
				$item["PROPS"]["PROP_TARGET"] = str_replace(",", ".", $item["PROPS"]["PROP_TARGET"]);
				
				//Убрать с режимами торгов
				if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["etf"]) || !$item["PROPS"]["BOARDID"]){ //Исключаем не etf активы
					continue;
				}

				//Убрать с выведенные с биржи
/*				if(!empty($item["PROPS"]["HIDEN"])){
					continue;
				}*/
				//если нет LASTPRICE - берем LEGALCLOSE
				if($item["PROPS"]["LASTPRICE"]<=0 && $item["PROPS"]["LEGALCLOSE"]>0) {
					$item["PROPS"]["LASTPRICE"] = $item["PROPS"]["LEGALCLOSE"];
				}
				
				if($item["PROPS"]["EMITENT_ID"]){
					$comp = CIBlockElement::GetByID($item["PROPS"]["EMITENT_ID"])->GetNext();
					$item["COMPANY"] = array(
						"ID" => $comp["ID"],
						"NAME" => $comp["NAME"],
						//"URL" => $comp["DETAIL_PAGE_URL"],
					);
					
					//страница компании
					$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$comp["NAME"]);
					$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL", "PROPERTY_VAL_ED_FIRST", "PROPERTY_CAPITAL_KOEF"));
					if($val = $res2->GetNext())
					{
						$item["COMPANY"]["URL"] = $val["DETAIL_PAGE_URL"];

						//если данные в долларах, перевести капитализацию
						if(strpos($val["PROPERTY_VAL_ED_FIRST_VALUE"], "долл")!==false && $item["PROPS"]["ISSUECAPITALIZATION"]){
							$item["PROPS"]["ISSUECAPITALIZATION_DOL"] = $item["PROPS"]["ISSUECAPITALIZATION"]/getCBPrice("USD", $prevDay->format("d/m/Y"));
						}
						
						if($val["PROPERTY_CAPITAL_KOEF_VALUE"]){
							$item["COMPANY"]["CAPITAL_KOEF"] = $val["PROPERTY_CAPITAL_KOEF_VALUE"];
						}
					}
				}
				if($item["PROPS"]["EMITENT_ID"] && $arPeriods[$item["PROPS"]["EMITENT_ID"]]){
					$item["PERIODS"] = $arPeriods[$item["PROPS"]["EMITENT_ID"]];
					$item["PERIODS"] = parent::calculatePeriodData($item);
				}

				$item["DYNAM"] = self::setDynamParams($item);

				$arItems[] = $item;
			}

			$cache->set($cacheId, $arItems);
		}
		
		$this->arOriginalsItems = $arItems;
		$this->count = count($arItems);
	}
	
	//таблица вывода
	public function getTable($page=1){
		$this->setFilter();
		$this->sortItems();
		
		$end = $page*$this->perPage;
		$start = $end-$this->perPage;
		
		$arItems = array();
		
		if($page==1){
			$arItems = $this->arItemsSelected;
		}
		
		for($i=$start;$i<$end;$i++){
			if($this->arItems[$i]){
				$arItems[] = $this->arItems[$i];
			}
		}
		
		$shows = $this->perPage*$page+count($this->arItemsSelected);
		if($shows>$this->count+count($this->arItemsSelected)){
			$shows = $this->count;
		}
		
		return array(
			"total_items" => $this->count+count($this->arItemsSelected),
			"cur_page" => $page,
			"shows" => $shows,
			"count_page" => ceil($this->count/$this->perPage),
			"items" => $arItems,
			"access" => $this->havePayAccess,
			//"selected" => $this->getSelectedItems()
		);
	}
	
	//сортировка элементов
	private function sortItems(){
	    $negArr = [];
	    $posArr = [];
	    foreach ($this->arItems as $item) {
	        if($item["DYNAM"]["PE"] <= 0 || !$item["DYNAM"]["PE"]) {
                $negArr[] = $item;
            } else {
	            $posArr[] = $item;
            }

        }
        usort($posArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
                return 0;
            }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? 1 : -1;
        });

        usort($negArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
                return 0;
            }
            if(!$a["DYNAM"]["PE"]) {
                return -1;
            }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? -1 : 1;
        });

        $this->arItems = array_merge($posArr, $negArr);
	}
	
	//избранные элементы
	public function getSelectedItems(){
		$arData = array();
		if($_COOKIE["id_arr_etf_cookie"]){
			$tmp = json_decode($_COOKIE["id_arr_etf_cookie"]);
			foreach($tmp as $item){
				$t = explode("...", $item);
				$arData[$t[0]] = $t[1];
			}
		}
		
		return $arData;
	}

	//фильтр для элементов
	private function setFilter(){
		$r = $this->setDefaultFilter($_REQUEST);
		$selectedItems = $this->getSelectedItems();
		
		foreach($this->arOriginalsItems as $item){
			if(!$selectedItems[$item["PROPS"]["SECID"]]){

				//Вышедшие с обращения убираем
				 if(!empty($item["PROPS"]["HIDEN"])) continue;

				//Страна
				if(array_key_exists('ETF_COUNTRY',$r) && !empty($r["ETF_COUNTRY"])){
					if($item["PROPS"]["ETF_COUNTRY"]!=$r["ETF_COUNTRY"] && "all"!=$r["ETF_COUNTRY"]){
						continue;
					}
				}

				//Отрасль
				if(array_key_exists('ETF_INDUSTRY',$r) && !empty($r["ETF_INDUSTRY"])){
					if($item["PROPS"]["ETF_INDUSTRY"]!=$r["ETF_INDUSTRY"] && "all"!=$r["ETF_INDUSTRY"]){
						continue;
					}
				}

				//Средний прирост
				if(array_key_exists('ETF_AVG_ICNREASE',$r) && intval($r["ETF_AVG_ICNREASE"])<=30){
					if(floatval($item["PROPS"]["ETF_AVG_ICNREASE"])<intval($r["ETF_AVG_ICNREASE"])){
						continue;
					}
				}

				//Биржа
				if(array_key_exists('ETF_EXCHANGE',$r) && !empty($r["ETF_EXCHANGE"])){
					if($item["PROPS"]["ETF_EXCHANGE"]!=$r["ETF_EXCHANGE"] && "all"!=$r["ETF_EXCHANGE"]){
						continue;
					}
				}

				//Тип активов
				if(array_key_exists('ETF_TYPE_OF_ACTIVES',$r) && !empty($r["ETF_TYPE_OF_ACTIVES"])){
					if($item["PROPS"]["ETF_TYPE_OF_ACTIVES"]!=$r["ETF_TYPE_OF_ACTIVES"] && "all"!=$r["ETF_TYPE_OF_ACTIVES"]){
						continue;
					}
				}

				//Дивиденды
				if(array_key_exists('ETF_DIVIDENDS',$r) && !empty($r["ETF_DIVIDENDS"])){
					if($item["PROPS"]["ETF_DIVIDENDS"]!=$r["ETF_DIVIDENDS"] && "all"!=$r["ETF_DIVIDENDS"]){
						continue;
					}
				}

				//Репликация
				if(array_key_exists('ETF_REPLICATION',$r) && !empty($r["ETF_REPLICATION"])){
					if($item["PROPS"]["ETF_REPLICATION"]!=$r["ETF_REPLICATION"] && "all"!=$r["ETF_REPLICATION"]){
						continue;
					}
				}

				//Betta filter
	            //  if($this->havePayAccess){
	                  if($r["betta"] && $r["betta"] !== 'all'){
	                      if(!$item["PROPS"]["BETTA"]){
	                          continue;
	                      }
	                      if($r["betta"] === '-999>-1' && $item["PROPS"]["BETTA"] > -1){
	                          continue;
	                      }
	                      if($r["betta"] === '-1>0' && ($item["PROPS"]["BETTA"] <= -1 || $item["PROPS"]["BETTA"]>0)){
	                          continue;
	                      }
	                      if($r["betta"] === '0>0.5' && ($item["PROPS"]["BETTA"] <= 0 || $item["PROPS"]["BETTA"]>0.5)){
	                          continue;
	                      }
	                      if($r["betta"] === '0.5>1' && ($item["PROPS"]["BETTA"] <= 0.5 || $item["PROPS"]["BETTA"]>1)){
	                          continue;
	                      }
	                      if($r["betta"] === '1>999' && $item["PROPS"]["BETTA"] <= 1){
	                          continue;
	                      }
	                      if($r["betta"] === '-9991>1' && $item["PROPS"]["BETTA"]>=1){
	                          continue;
	                      }
	                  }
	            // }
					 //Рейтинг радар filter
                if($this->havePayAccess){
                    if($r["radar-rating"] && $r["radar-rating"] !== 'all'){
                        if(!$item["PROPS"]["PRC_OF_USERS"]){
                            continue;
                        }

								if($r["radar-rating"] === '0-1' && $item["PROPS"]["PRC_OF_USERS"] > 1){
                            continue;
                        }
								if($r["radar-rating"] === '0-5' && $item["PROPS"]["PRC_OF_USERS"] > 5){
                            continue;
                        }
								if($r["radar-rating"] === '0-10' && $item["PROPS"]["PRC_OF_USERS"] > 10){
                            continue;
                        }
								if($r["radar-rating"] === '0-20' && $item["PROPS"]["PRC_OF_USERS"] > 20){
                            continue;
                        }
								if($r["radar-rating"] === '0-30' && $item["PROPS"]["PRC_OF_USERS"] > 30){
                            continue;
                        }
								if($r["radar-rating"] === '1-100' && $item["PROPS"]["PRC_OF_USERS"] < 1){
                            continue;
                        }
								if($r["radar-rating"] === '5-100' && $item["PROPS"]["PRC_OF_USERS"] < 5){
                            continue;
                        }
								if($r["radar-rating"] === '10-100' && $item["PROPS"]["PRC_OF_USERS"] < 10){
                            continue;
                        }
								if($r["radar-rating"] === '20-100' && $item["PROPS"]["PRC_OF_USERS"] < 20){
                            continue;
                        }
								if($r["radar-rating"] === '30-100' && $item["PROPS"]["PRC_OF_USERS"] < 30){
                            continue;
                        }
                    }
                }
				//Комиссия
				if(array_key_exists('ETF_COMISSION',$r) && !empty($r["ETF_COMISSION"])){
					if("all"!=$r["ETF_COMISSION"]){
						if("0.5"==$r["ETF_COMISSION"] && $item["PROPS"]["ETF_COMISSION"]>0.5){ continue; }//до 0,5 включительно
						if("1"==$r["ETF_COMISSION"] && $item["PROPS"]["ETF_COMISSION"]>1){ continue; }//до 1 включительно
						if("1.5"==$r["ETF_COMISSION"] && $item["PROPS"]["ETF_COMISSION"]>1.5){ continue; }//до 1,5 включительно
						if("2"==$r["ETF_COMISSION"] && $item["PROPS"]["ETF_COMISSION"]>2){ continue; }//до 2 включительно
					}
				}

				//Тип фонда
				if(array_key_exists('ETF_TYPE',$r) && !empty($r["ETF_TYPE"])){
					if($item["PROPS"]["ETF_TYPE"]!=$r["ETF_TYPE"] && "all"!=$r["ETF_TYPE"]){
						continue;
					}
				}

				//Оператор
				if(array_key_exists('EMITENT_ID',$r) && intval($r["EMITENT_ID"])>0){
					if($item["PROPS"]["EMITENT_ID"]!=intval($r["EMITENT_ID"])){
						continue;
					}
				}

				//Валюта
				if(array_key_exists('ETF_CURRENCY',$r) && !empty($r["ETF_CURRENCY"])){
					if($item["PROPS"]["ETF_CURRENCY"]!=$r["ETF_CURRENCY"] && "all"!=$r["ETF_CURRENCY"]){
						continue;
					}
				}

				//Базовая валюта
				if(array_key_exists('ETF_BASE_CURRENCY',$r) && !empty($r["ETF_BASE_CURRENCY"])){
					if(($item["PROPS"]["ETF_BASE_CURRENCY"]!=$r["ETF_BASE_CURRENCY"] && "all"!=$r["ETF_BASE_CURRENCY"] && "all_not_rub"!=$r["ETF_BASE_CURRENCY"]) || //фильтр не "все"
					  ("all_not_rub"==$r["ETF_BASE_CURRENCY"] && strpos($item["PROPS"]["ETF_BASE_CURRENCY"],"RUB")!==false)  //фильтр "не рубль"
					){
						continue;
					}
				}

				//Убирать из выдачи акции, по которым нет оборотов за последнюю неделю
				if($r["turnover_week"]){
					if($item["PROPS"]["VALTODAY"]<=0){
						continue;
					}
				}
				
                //рост за месяц
                if($this->havePayAccess){
                    if($r["month-increase"] && $r["month-increase"] !== 'all'){
                        if(!$item["DYNAM"]["MONTH_INCREASE"]){
                            continue;
                        }
                        if($r["month-increase"] === 'down50' && $item["DYNAM"]["MONTH_INCREASE"] > -50){
                            continue;
                        }
                        if($r["month-increase"] === 'down25' && $item["DYNAM"]["MONTH_INCREASE"] > -25){
                            continue;
                        }
                        if($r["month-increase"] === 'down10' && $item["DYNAM"]["MONTH_INCREASE"] > -10){
                            continue;
                        }
                        if($r["month-increase"] === 'down' && $item["DYNAM"]["MONTH_INCREASE"] > 0){
                            continue;
                        }
                        if($r["month-increase"] === 'up' && $item["DYNAM"]["MONTH_INCREASE"] < 0){
                            continue;
                        }
                        if($r["month-increase"] === 'up10' && $item["DYNAM"]["MONTH_INCREASE"] < 10){
                            continue;
                        }
                        if($r["month-increase"] === 'up25' && $item["DYNAM"]["MONTH_INCREASE"] < 25){
                            continue;
                        }
                        if($r["month-increase"] === 'up50' && $item["DYNAM"]["MONTH_INCREASE"] < 50){
                            continue;
                        }
                    }
                    if($r["year-increase"] && $r["year-increase"] !== 'all'){
                        if(!$item["DYNAM"]["YEAR_INCREASE"]){
                            continue;
                        }
                        if($r["year-increase"] === 'down50' && $item["DYNAM"]["YEAR_INCREASE"] > -50){
                            continue;
                        }
                        if($r["year-increase"] === 'down25' && $item["DYNAM"]["YEAR_INCREASE"] > -25){
                            continue;
                        }
                        if($r["year-increase"] === 'down10' && $item["DYNAM"]["YEAR_INCREASE"] > -10){
                            continue;
                        }
                        if($r["year-increase"] === 'down' && $item["DYNAM"]["YEAR_INCREASE"] > 0){
                            continue;
                        }
                        if($r["year-increase"] === 'up' && $item["DYNAM"]["YEAR_INCREASE"] < 0){
                            continue;
                        }
                        if($r["year-increase"] === 'up10' && $item["DYNAM"]["YEAR_INCREASE"] < 10){
                            continue;
                        }
                        if($r["year-increase"] === 'up25' && $item["DYNAM"]["YEAR_INCREASE"] < 25){
                            continue;
                        }
                        if($r["year-increase"] === 'up50' && $item["DYNAM"]["YEAR_INCREASE"] < 50){
                            continue;
                        }
                    }
                    if($r["three-year-increase"] && $r["three-year-increase"] !== 'all'){
                        if(!$item["DYNAM"]["THREE_YEAR_INCREASE"]){
                            continue;
                        }
                        if($r["three-year-increase"] === 'down50' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -50){
                            continue;
                        }
                        if($r["three-year-increase"] === 'down25' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -25){
                            continue;
                        }
                        if($r["three-year-increase"] === 'down10' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -10){
                            continue;
                        }
                        if($r["three-year-increase"] === 'down' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > 0){
                            continue;
                        }
                        if($r["three-year-increase"] === 'up' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 0){
                            continue;
                        }
                        if($r["three-year-increase"] === 'up10' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 10){
                            continue;
                        }
                        if($r["three-year-increase"] === 'up25' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 25){
                            continue;
                        }
                        if($r["three-year-increase"] === 'up50' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 50){
                            continue;
                        }
                    }
                }

				$this->arItems[] = $item;
			} else {
				$this->arItemsSelected[] = $item;
			}
		}
		$this->count = count($this->arItems);
	}

	    /**
     * Возвращает список ISIN кодов для фильтра списка ETF
     * @param $arFilterData
     * @return array
     */
	public function setFilterForList($arFilterData){
	    $listSelectedItems = array();

        foreach($this->arOriginalsItems as $item){
            unset($item["PERIODS"]);
           switch ($arFilterData[0]){
               case "all":
					    if(empty($item["PROPS"]["ETF_IS_ETF_ACTIVE"])) continue;
                   $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;
               case "ETF_COUNTRY":
						 if ($item["PROPS"]["ETF_COUNTRY"] != $arFilterData[1]) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;
               case "ETF_COUNTRY ETF_TYPE_OF_ACTIVES":
						 if ($item["PROPS"]["ETF_COUNTRY"] != $arFilterData[1]) continue;
						 if ($item["PROPS"]["ETF_TYPE_OF_ACTIVES"] != $arFilterData[2]) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;

                case "ETF_CURRENCY":
                   if($item["PROPS"]["ETF_CURRENCY"] != $arFilterData[1] || !$item["PROPS"]["ETF_CURRENCY"])	continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;
               case "ETF_CALC_BASE":
						 if ($item["PROPS"]["ETF_CALC_BASE"] != $arFilterData[1] || !$item["PROPS"]["ETF_CALC_BASE"]) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;

               case "ETF_TYPE_OF_ACTIVES":
						 if (strpos($item["PROPS"]["ETF_TYPE_OF_ACTIVES"], $arFilterData[1]) === false) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;

               case "EMITENT_ID":
                   if(!in_array($item["PROPS"]["EMITENT_ID"], $arFilterData[1]) || !$item["PROPS"]["EMITENT_ID"])	continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;

           }
        }
        return $listSelectedItems;
    }
	
	//Фильтр по умолчанию
	private function setDefaultFilter($r){
		global $APPLICATION;

				//Средний прирост
				if(!array_key_exists('ETF_AVG_ICNREASE',$r)){
					$r["ETF_AVG_ICNREASE"]= $APPLICATION->etfDefaultForm["ETF_AVG_ICNREASE"];
				}

				//Комиссия
				if(!array_key_exists('ETF_COMISSION',$r)){
				  $r["ETF_COMISSION"] = $APPLICATION->etfDefaultForm["ETF_COMISSION"];
				}


		return $r;
	}
	
	private function setDynamParams($item){
		$return = array();



		//new Просад = ( 1 - old Просад / LASTPRICE ( LEGALCLOSE) ) х 100
		if($item["PROPS"]["PROP_PROSAD"] && $item["PROPS"]["LASTPRICE"]){
			$return["Просад"] =  round((1 - $item["PROPS"]["PROP_PROSAD"]/$item["PROPS"]["LASTPRICE"])*100, 2);
		}

		  if($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_DAY"]) {
            $return["DAY_INCREASE"] = round($item["PROPS"]["ONE_DAY_DROP_INCREASE"], 2);
        }

		if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_MONTH"]) {
            $return["MONTH_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_MONTH"]) - 1) * 100), 2);
        }
        if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_ONE_YEAR"]) {
            $return["YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_ONE_YEAR"]) - 1) * 100), 2);
        }
        if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_THREE_YEAR"]) {
            $return["THREE_YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_THREE_YEAR"]) - 1) * 100), 2);
        }
		
		//new Target = ( old Target / LASTPRICE ( LEGALCLOSE) - 1 ) x 100
		if($item["PROPS"]["PROP_TARGET"] && $item["PROPS"]["LASTPRICE"]){
			$return["Таргет"] =  round(($item["PROPS"]["PROP_TARGET"]/$item["PROPS"]["LASTPRICE"]-1)*100, 2);
		}
		
		//Цена лота
		if($item["PROPS"]["LOTSIZE"] && $item["PROPS"]["LASTPRICE"]){
			$return["Цена лота"] = $item["PROPS"]["LOTSIZE"]*$item["PROPS"]["LASTPRICE"];
		}
		
		/*
		1) если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
		2) если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
		3) если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
		4) если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
		*/
		//Прибыль
		if($item["PERIODS"]){
			$tmp = sort_nested_arrays($item["PERIODS"], array('PERIOD_YEAR' => 'desc', 'PERIOD_VAL' => 'desc'));
			$lastItem = $tmp[0];
			
			$curKvartal = intval((date('n')-1)/4)+1;
			$availYear = date("Y")-1;

			try {
			$lastDate = DateTime::createFromFormat('m.Y', $lastItem["PERIOD_VAL"].".".$lastItem["PERIOD_YEAR"]);
			$lastAvail = DateTime::createFromFormat('m.Y', $curKvartal.".".$availYear);

			if($lastDate && $lastAvail){
			if($lastDate->format("U") >= $lastAvail->format("U")){ 
				
				$year = DateTime::createFromFormat('Y', $lastItem["PERIOD_YEAR"]);
				$prevYear = clone $year;
				$prevYear->modify("-1year");
				
				if ($lastItem["PERIOD_VAL"]==1){
					//если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
					if(
						isset($item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["1-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] = 
							$item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["1-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==2){
					//если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
					if(
						isset($item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["2-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] = 
							$item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["2-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==3){
					//если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
					if(
						isset($item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) && 
						isset($item["PERIODS"]["3-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] = 
							$item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["3-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==4){
					//если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
					if(
						isset($item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] = 
							$item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				}
			}
			}

			} catch (Exception $e) {
				?><pre><?print_r($lastItem)?></pre><?
				exit();
			} 
		}
		
		//РЕ
		if($return["Прибыль"] && ($item["PROPS"]["ISSUECAPITALIZATION"] || $item["PROPS"]["ISSUECAPITALIZATION_DOL"])){
			$val = $item["PROPS"]["ISSUECAPITALIZATION"];
			if(!empty($item["PROPS"]["ISSUECAPITALIZATION_DOL"])){
				$val = $item["PROPS"]["ISSUECAPITALIZATION_DOL"];
			}
			$return["PE"] = round($val/$return["Прибыль"]/1000000, 1);
            $return["P/Equity"] = round($val/$return["Собственный капитал"]/1000000, 1);
            $return["PEG"] = round($return["PE"]/$return["AVEREGE_PROFIT"], 1);
		}
		//Дивиденды %
		if(!empty($item["PROPS"]["PROP_DIVIDENDY_2018"]) && !empty($item["PROPS"]["LASTPRICE"])){
			$return["Дивиденды %"] = round($item["PROPS"]["PROP_DIVIDENDY_2018"]/$item["PROPS"]["LASTPRICE"]*100, 2);
			if(strpos($item["PROPS"]["PROP_ISTOCHNIK"],"$")!==false){//Если расчет в валюте
			$currencyRateUSD = getCBPrice("USD", (new DateTime())->format("d/m/Y"));
		  	//mail('alphaprogrammer@gmail.com','usd='.$currencyRateUSD.' PD2018='.$item["PROPS"]["PROP_DIVIDENDY_2018"].' lp='.$item["PROPS"]["LASTPRICE"]);
			$return["Дивиденды %"] = round($item["PROPS"]["PROP_DIVIDENDY_2018"]*$currencyRateUSD/$item["PROPS"]["LASTPRICE"]*100, 2);
		   //mail('alphaprogrammer@gmail.com','div proc ',$return["Дивиденды %"]);
			}
		}

		return $return;
	}
	
	//список отраслей
	static function getIndustryList(){
		$return = array();
		
		$cacheLifetime = 86400; 
		$cacheID = 'ActionsIndystryList'; 
		$cachePath = '/'.$cacheID;
		
		$obCache = new CPHPCache();
		if( $obCache->InitCache($cacheLifetime, $cacheID, $cachePath) )// Если кэш валиден
		{
			$return = $obCache->GetVars();// Извлечение переменных из кэша
		}
		elseif( $obCache->StartDataCache()  )// Если кэш невалиден
		{
			$ibID = 32;
			$arFilter = Array("IBLOCK_ID"=>$ibID, "!PROPERTY_PROP_SEKTOR"=>false);
			$res = CIBlockElement::GetList(Array(), $arFilter, array("PROPERTY_PROP_SEKTOR"), false);
			while($val = $res->GetNext())
			{
				$return[] = $val["PROPERTY_PROP_SEKTOR_VALUE"];
			}
			
		   $obCache->EndDataCache($return);// Сохраняем переменные в кэш.
		}
	
		return $return;
	}
}