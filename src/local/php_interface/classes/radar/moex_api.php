<?/**
 * Класс для получения данных с Московской биржи
 *
 * [add longDescription]
 *
 * @author    Александр Еремин <alphaprogrammer@gmail.com>
 * @copyright 2020
 * @license   GNU
 * @version   1.0
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     24 class MoexApi
 *     37   function getInstrument($SECID, $type=)
 *     55   function getInstrumentParam($SECID, $arParams = array())
 *
 * TOTAL FUNCTIONS: 2
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */

class MoexApi
{

  	 /**
  	  * Возвращает описание биржевого инструмента
  	  *
  	  * @param  string   $SECID - код бумаги
  	  * @param  string   $type (Optional) - тип получаемых данных
  	  *
  	  * @return bool|array
  	  *
  	  * @access public
  	  */
  	 public function getInstrument($SECID, $type="json"){
	 	  if(empty($SECID)) return false;
        $url = 'https://iss.moex.com/iss/securities/'.$SECID.'.'.$type;
        $data = ConnectMoex($url);
		  return $data;
	 }


  	 /**
  	  * Возвращает параметр или массив параметров инструмента
  	  *
  	  * @param  string   $SECID - код бумаги
  	  * @param  array   $arParams - коды параметров инструмента. Если входящий массив пустой - возвращает все парамтеры инструмента в результирующем массиве
  	  *
  	  * @return bool|array
  	  *
  	  * @access public
  	  */
  	 public function getInstrumentParam($SECID, $arParams = array()){
	 	  if(empty($SECID)) return false;
		  $data = $this->getInstrument($SECID);
		  $arResult = array();
		  $nameCol = -1;
		  $valueCol = -1;
		  if(isset($data['description']['data']) && count($data['description']['data'])>0){
			 for($i=0; $i<count($data['description']['columns']); $i++){
			 	if($data['description']['columns'][$i]=='name'){$nameCol = $i;}
			 	if($data['description']['columns'][$i]=='value'){$valueCol = $i;}
			 }

			 if($nameCol>=0 && $valueCol>=0){
			 	for($i=0; $i<count($data['description']['data']); $i++){
			 	  if(count($arParams)<=0 || in_array($data['description']['data'][$i][$nameCol],$arParams)){
					 $arResult[$data['description']['data'][$i][$nameCol]] = $data['description']['data'][$i][$valueCol];
			 	  }
			 	}
			 }
		  }
		  return $arResult;
	 }

}