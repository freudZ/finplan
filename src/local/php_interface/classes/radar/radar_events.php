<?
use Bitrix\Highloadblock as HL;

//Класс для работы с событиями радара

class CRadarEvents {
	private $hl_divid_rus       = 21;
	private $hl_divid_usa       = 32;
	private $hl_coupons         = 15;
	private $hl_radarEvents     = 36;
	private $acionsIblock       = 32;
	private $acionsUsaIblock    = 55;
	private $bondsIblock        = 27;
	public $arDateRange         = array("START" => "", "END" => ""); //Диапазон дат
	public $arFilteredDateRange = array("START" => "", "END" => ""); //Диапазон дат фильтрации
	public $arEvents            = array(); //Результирующий массив за весь диапазон дат с ключами по датам
	public $arEventsFiltered    = array(); //Отфильтрованый Результирующий массив
	public $arEventsTypes       = array("DIVIDENDS" => "Дивиденды по акциям РФ", "DIVIDENDS_USA" => "Дивиденды по акциям США",
		"OFERTA" => "Оферта по облигациям", "CANCELLATION" => "Гашения по облигациям", "COUPONS" => "Купоны по облигациям",
		"REPORT_RUS" => "Отчетность (компании РФ)", "REPORT_USA" => "Отчетность (компании США)",
		"IMPORTANT" => "Важные события", "SOVIET" => "Совет директоров",
		"GOSA" => "ГОСА/ВОСА", "WEBINAR" => "Вебинары"); //Массив типов событий
	public $arPortfolioEventKeys = array("DIVIDENDS", "DIVIDENDS_USA", "OFERTA", "CANCELLATION", "COUPONS", "REPORT_RUS", "REPORT_USA", "SOVIET", "GOSA"); //Ключи для которых фильтруется портфель

	public function __construct() {

		$this->setDateRange();
		//$this->setEventsDatelist();
		$this->setMainArray();
		$this->setFilter();
	}

	//Устанавливает диапазон расчетов значений
	private function setDateRange() {
		$now                        = new DateTime();
		$this->arDateRange["START"] = (clone $now)->modify("-14 days");
		$this->arDateRange["END"]   = (clone $now)->modify("+14 days");
		unset($now);
	}

	//Формирует основу результирующего массива с ключами-датами и значениями - типами событий
	private function setEventsDatelist(&$arResult) {
		$interval = new DateInterval('P1D');
		$period   = new DatePeriod($this->arDateRange["START"], $interval, $this->arDateRange["END"]);
		//$arKeys = array_flip($this->arEventsTypes);
		$arKeys = $this->arEventsTypes;

		foreach ($arKeys as $key => $element) {
			$arKeys[$key] = array();
		}

		foreach ($period as $date) {
			$arResult[($date)->format('d.m.Y')] = $arKeys;
		}
		unset($arKeys, $interval, $period);
	}

	//Заполняет результирующий массив / или достает из кеша
	private function setMainArray() {
		$cache    = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId  = "radar_events";
		$cacheTtl = 86400 * 7;
		//$cache->clean("radar_events");
		$arResult = array();
		if ($cache->read($cacheTtl, $cacheId)) {
			$arResult = $cache->get($cacheId);
		} else {
			CModule::IncludeModule("highloadblock");
			CModule::IncludeModule("iblock");
			$this->setEventsDatelist($arResult);
			$this->setDividendsData($arResult);
			$this->setDividendsDataUsa($arResult);
			$this->setOferta($arResult);
			$this->setCoupons($arResult);
			$this->setLoadedEvents($arResult);

			$cache->set($cacheId, $arResult);
		}
		$this->arEvents = $arResult;
	}

	public function setFilter($userId='') {
		Global $USER;
		$uid = intval($userId)>0?intval($userId):$USER->GetID();
		$this->arEventsFiltered = array();
		if ($_REQUEST["date"] && !empty($_REQUEST["date"])) {
			$filterDateFrom = new DateTime($_REQUEST["date"]);
		} else {
			$filterDateFrom = new DateTime();
		}
		if ($_REQUEST["dateto"] && !empty($_REQUEST["dateto"])) {
			$filterDateTo = new DateTime($_REQUEST["dateto"]);
		} else {
			$filterDateTo = (new DateTime())->modify('+7 days');
		}

		$this->arFilteredDateRange["START"] = $filterDateFrom->format('d.m.Y');
		$this->arFilteredDateRange["END"]   = $filterDateTo->format('d.m.Y');

		while ($filterDateFrom <= $filterDateTo) {
			$date = $filterDateFrom->format('d.m.Y');
			if (array_key_exists($date, $this->arEvents)) {
				$this->arEventsFiltered[$date] = $this->arEvents[$date];
			}

			$filterDateFrom->modify('+1 days');
		}

		//Фильтр по типам событий, если они выбраны
		$deleteEventKeys = array();
		if (isset($_REQUEST["eventTypes"])) {
			$selectedEventTypes = explode(",", $_REQUEST["eventTypes"]);
			$deleteEventKeys    = array_diff_key($this->arEventsTypes, array_flip($selectedEventTypes));
		}
		if (count($deleteEventKeys)) {
			foreach ($this->arEventsFiltered as $date => $events) {
				$this->arEventsFiltered[$date] = array_diff_key($events, $deleteEventKeys);
			}
		}

		$arFilteredTmp = array(); //Складываем отфильтрованные по активам портфеля и избранного

		//+Фильтр по избранному
		$arFavCodes = array();
		if (isset($_REQUEST["eventFavorites"]) && !empty($_REQUEST["eventFavorites"])) {
			$data = getFavoriteList(intval($userId)>0?intval($userId):'');
			if (count($data)>0) {
				$arNames = getFavoriteNames();
				foreach ($data as $keyDataType => $dataType) {
					switch ($keyDataType) {
						case "action":
						case "etf":
						case "action_usa":
						case "obligation":
							foreach ($dataType as $arFavActive) {
								if (!in_array($arFavActive["CODE"], $arFavCodes)) {
									$arFavCodes[] = $arFavActive["CODE"];
								}
							}

							break;
						case "usa_company":
						case "obligation_company":
							$arEmitentNames = array();
							foreach ($dataType as $arFavEmitent) {
								if (!in_array($arFavEmitent["NAME"], $arEmitentNames)) {
									$arEmitentNames[] = $arFavEmitent["NAME"];
								}
							}

							$arEmitentCodes = $this->getEmitentsActiveCodes($arEmitentNames);



							if (count($arEmitentCodes) > 0) {
								$arFavCodes = array_merge($arFavCodes, array_values($arEmitentCodes));
							}


						break;
					}
				}
			}
		} //-Фильтр по избранному



		//+Фильтр по активам выбранного портфеля
		$arPortfolioActiveCodes = array();
		if (isset($_REQUEST["eventPortfolio"]) && !empty($_REQUEST["eventPortfolio"])) {
			$portfolio = new CPortfolio();

			if(!is_array($_REQUEST["eventPortfolio"])){
			  $_REQUEST["eventPortfolio"] = explode(",", $_REQUEST["eventPortfolio"]);
			}


			if ($_REQUEST["eventPortfolio"][0] != 'all') { //Получаем список активов для одного портфеля
				$arPortfolioActiveCodes = array();
			   foreach($_REQUEST["eventPortfolio"] as $pid){
				 $arPortfolioActiveCodes = array_merge($arPortfolioActiveCodes, $portfolio->getPortfolioActiveCodeList($pid));
				}
			} else {
				$userPortfolioList      = $portfolio->getPortfolioListForUser($uid, false);
				$userPortfolioList      = array_keys($userPortfolioList);
				$userPortfolioList      = str_replace("portfolio_", "", $userPortfolioList);
				$arPortfolioActiveCodes = $portfolio->getPortfolioActiveCodeList($userPortfolioList);

				unset($userPortfolioList);
			}
			unset($portfolio);

		} //-Фильтр по активам выбранного портфеля

		if (count($arPortfolioActiveCodes) > 0 || count($arFavCodes) > 0) {
			$arFilterCodeList = array_merge($arPortfolioActiveCodes, $arFavCodes);


			//"DIVIDENDS", "DIVIDENDS_USA", "OFERTA", "CANCELLATION", "COUPONS", "REPORT_RUS", "REPORT_USA", "SOVIET", "GOSA"
			foreach ($this->arEventsFiltered as $date => $eventKeys) {
				foreach ($eventKeys as $keyEvent => $eventList) {
					foreach ($eventList as $key => $eventDetail) {
						switch ($keyEvent) {
							case "DIVIDENDS":
							case "DIVIDENDS_USA":
							case "OFERTA":
							case "CANCELLATION":
							case "COUPONS":
								if (in_array($eventDetail["ITEM"], $arFilterCodeList)) {
									$arFilteredTmp[$date][$keyEvent][] = $eventDetail;
								}
								break;
							case "REPORT_RUS":
							case "REPORT_USA":
							case "SOVIET":
							case "GOSA":
								if (strpos($eventDetail["UF_CODE"], ",") !== false) {
									$codes = explode(",", $eventDetail["UF_CODE"]);
								} else {
									$codes = array($eventDetail["UF_CODE"]);
								}

								foreach($codes as $oneCode) {
									if (in_array($oneCode, $arFilterCodeList)) {
								 		if(!array_key_exists($eventDetail["ID"], $arFilteredTmp[$date][$keyEvent]))
										   $arFilteredTmp[$date][$keyEvent][$eventDetail["ID"]] = $eventDetail;
								 	}
							  	}
								break;
							default:
								$arFilteredTmp[$date][$keyEvent][] = $eventDetail;
								break;
						}
					}
				}
			}




		} else if(count($arPortfolioActiveCodes) == 0 || count($arFavCodes) == 0){
		  $arFilteredTmp = $this->arEventsFiltered;
		}

		if (count($arFilteredTmp) > 0) {
			$this->arEventsFiltered = $arFilteredTmp;
			//$this->arEventsFiltered = array_filter($arFilteredTmp);
			unset($arFilteredTmp);
		} else if((isset($_REQUEST["eventFavorites"]) || isset($_REQUEST["eventPortfolio"])) && count($arFilteredTmp) <= 0){
			  $this->arEventsFiltered = array(); //Если выбраны фильтры но ничего не отфильтровано - очистим список ранее отфильтрованных событий
		}
	}

	//Возвращает из HL событий радара коды активов по названию эмитента
	public function getEmitentsActiveCodes($emitentName) {
		$arReturn = array();
		//При пустых входящих параметрах возвращаем пустой ответ не делая запроса
		if (empty($emitentName) || (is_array($emitentName) && count($emitentName) == 0)) {
			return $arReturn;
		}




		$hlblock     = HL\HighloadBlockTable::getById($this->hl_radarEvents)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				"UF_EMITENT" => array_values($emitentName),
				"!UF_CODE" => false,
			),
			"select" => array(
				"UF_CODE"
			),
		));

		while ($elem = $res->fetch()) {

			if (!empty($elem["UF_CODE"])) {
				if (strpos($elem["UF_CODE"], ",") !== false) {
					$arUfCodes = explode(",", $elem["UF_CODE"]);
					foreach($arUfCodes as $code){
					  if(!in_array($code ,$arReturn))
					 $arReturn[] = $code;
					}
				} else {
					if(!in_array($elem["UF_CODE"] ,$arReturn))
					 $arReturn[] = $elem["UF_CODE"];
				}
			}
		}
		return $arReturn;
	}

	private function setLoadedEvents(&$arResult) {
		$hlblock     = HL\HighloadBlockTable::getById($this->hl_radarEvents)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				//"!UF_DATA" => false,
				">=UF_DATE" => $this->arDateRange["START"]->format('d.m.Y'),
				"<=UF_DATE" => $this->arDateRange["END"]->format('d.m.Y'),
			),
			"select" => array(
				"*"
			),
		));

		while ($elem = $res->fetch()) {
			$arResult[$elem["UF_DATE"]->format('d.m.Y')][$elem["UF_TYPE"]][] = $elem;
		}
	}

	/**
	 * заполняет данные по дивидендам акций РФ
	 *
	 * @param  array   $arResult (Passed by reference) Массив результата
	 *
	 * @return array
	 *
	 * @access private
	 */
	private function setDividendsData(&$arResult) {
		$resA = new Actions;

		$hlblock     = HL\HighloadBlockTable::getById($this->hl_divid_rus)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				//"!UF_DATA" => false,
				"!=UF_DATA" => "[]",
			),
			"select" => array(
				"*"
			),
		));

		$arDatesInResult = array_keys($arResult);

		while ($elem = $res->fetch()) {
			$arDividends = json_decode($elem["UF_DATA"], true);

			foreach ($arDividends as $key => $val) {
				$curr_time = strval($val["Дата закрытия реестра"]);
				//$arResult[$curr_time]["DIVIDENDS"][] = $val;
				if (array_key_exists($curr_time, $arResult)) {
					$val["ITEM"] = $elem["UF_ITEM"];
					$radarItem   = $resA->getItem($elem["UF_ITEM"]);
					if (!empty($radarItem["PROPS"]["HIDEN"])) {
						continue;
					}
					//Исключаем бумаги вышедшие из обращения
					$val["NAME"] = $radarItem["NAME"];
					$val["URL"]  = $radarItem["URL"];
					if (!is_array($arResult[$curr_time]["DIVIDENDS"])) {$arResult[$curr_time]["DIVIDENDS"] = array();}
					$arResult[$curr_time]["DIVIDENDS"][] = $val;
				}
			}
		}
		unset($resA);
	}

	/**
	 * заполняет данные по дивидендам акций Usa
	 *
	 * @param  array   $arResult (Passed by reference) Массив результата
	 *
	 * @return array
	 *
	 * @access private
	 */
	private function setDividendsDataUsa(&$arResult) {
		$resAU = new ActionsUsa;

		$hlblock     = HL\HighloadBlockTable::getById($this->hl_divid_usa)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				//"!UF_DATA" => false,
				"!=UF_DATA" => "[]",
			),
			"select" => array(
				"*"
			),
		));

		$arDatesInResult = array_keys($arResult);

		while ($elem = $res->fetch()) {
			$arDividends = json_decode($elem["UF_DATA"], true);

			foreach ($arDividends as $key => $val) {
				$curr_time = strval($val["Дата закрытия реестра"]);
				//$arResult[$curr_time]["DIVIDENDS"][] = $val;
				if (array_key_exists($curr_time, $arResult)) {
					$val["ITEM"]                             = $elem["UF_ITEM"];
					$radarItem                               = $resAU->getItem($elem["UF_ITEM"]);
					$val["NAME"]                             = $radarItem["NAME"];
					$val["URL"]                              = $radarItem["URL"];
					$arResult[$curr_time]["DIVIDENDS_USA"][] = $val;
				}
			}
		}
		unset($resAU);
	}


	/**
	 * Возвращает дату следующей отчетности компании, если она есть БД и еще дата <= текущей дате
	 *
	 * @param  string   $companyName Название компании
	 * @param  string   $type Тип события календаря REPORT_RUS или REPORT_USA
	 *
	 * @return string  отформатированная дата d.m.Y или пустая строка
	 *
	 * @access public
	 */
	public function getNextReportDate($companyName, $type='REPORT_RUS'){
		Global $DB;
		$result = '';
		$query = "SELECT `UF_DATE` FROM `radar_events` WHERE `UF_EMITENT` = '".$DB->forSQL($companyName)."' AND `UF_TYPE` = '$type' ORDER BY `UF_DATE` DESC LIMIT 1";
		try {
				$res = $DB->Query($query);
				if($row = $res->fetch()){
				  $dt = new DateTime($row["UF_DATE"]);
				  $now = new DateTime();
				  if($now->getTimestamp()<=$dt->getTimestamp()){
				  	$result = $dt->format('d.m.Y');
				  }
				}} catch (Exception $e) {
		    		$result = 'Ошибка: '.  $e->getMessage();
		} finally {

		}
		return $result;
	}

	/**
	 * Заполняет данные по оферте и погашению облигаций
	 *
	 * @param  array   $arResult (Passed by reference) Массив результата
	 *
	 * @return array
	 *
	 * @access private
	 */
	private function setOferta(&$arResult) {
		$resO     = new Obligations;
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_ISIN", "PROPERTY_OFFERDATE", "PROPERTY_MATDATE");

		$arDateFilter = array();
		foreach (array_keys($arResult) as $dateItem) {
			$arDateFilter[] = (new DateTime($dateItem))->format('Y-m-d');
		}

	   // $arFilter = Array("IBLOCK_ID" => IntVal($this->bondsIblock),  "PROPERTY_OFFERDATE" => $arDateFilter, "PROPERTY_MATDATE" => $arDateFilter, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
		$arFilter = Array("IBLOCK_ID" => IntVal($this->bondsIblock),
                          array(
                            "LOGIC" => "OR",
                            array("PROPERTY_OFFERDATE" => $arDateFilter ),
                            array("PROPERTY_MATDATE" => $arDateFilter),
                          ),
                          "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->Fetch()) {
			$val["ITEM"] = $ob["PROPERTY_ISIN_VALUE"];
			$radarItem   = $resO->getItem($ob["PROPERTY_ISIN_VALUE"]);

			if (!empty($radarItem["PROPS"]["HIDEN"]) && !in_array("дефолтные", $radarItem["PROPS"]["CSV_VID_OLB"])) {
				//continue;
			}
			//Исключаем бумаги вышедшие из обращения
			$val["URL"]  = $radarItem["URL"];
			$val["NAME"] = $radarItem["NAME"];
			if (in_array("дефолтные", $radarItem["PROPS"]["CSV_VID_OLB"])) {
				$val["DEFAULT"]     = true;
				$val["DEFAULT_VID"] = $radarItem["PROPS"]["CSV_VID_DEFOLT"];
			} else {
				$val["DEFAULT"] = false;
			}
			if (!empty($radarItem["PROPS"]["ADDITIONAL_INCOME"])) {
				$val["STRUCTURED"] = true;
			} else {
				$val["STRUCTURED"] = false;
			}

			$itemOfertaDate       = (new DateTime($ob["PROPERTY_OFFERDATE_VALUE"]))->format('d.m.Y');
			$itemCancellationDate = (new DateTime($ob["PROPERTY_MATDATE_VALUE"]))->format('d.m.Y');
			if (!empty($ob["PROPERTY_OFFERDATE_VALUE"]) && array_key_exists($itemOfertaDate, $arResult)) {
				$arResult[$itemOfertaDate]["OFERTA"][] = $val;
			}
			if (!empty($ob["PROPERTY_MATDATE_VALUE"]) && array_key_exists($itemCancellationDate, $arResult)) {
				$arResult[$itemCancellationDate]["CANCELLATION"][] = $val;
			}

			/*   $arProps = $ob->GetProperties();
		print_r($arProps);*/
		}
		unset($resO);
	}

	/**
	 * заполняет данные по купонам облигаций
	 *
	 * @param  array   $arResult (Passed by reference) Массив результата
	 *
	 * @return array
	 *
	 * @access private
	 */
	private function setCoupons(&$arResult) {
		$resO = new Obligations;

		$hlblock     = HL\HighloadBlockTable::getById($this->hl_coupons)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				//"!UF_DATA" => false,
				"!=UF_DATA" => "[[]]",
			),
			"select" => array(
				"*"
			),
		));

		$arDatesInResult = array_keys($arResult);

		while ($elem = $res->fetch()) {
			$arCouponsData = json_decode($elem["UF_DATA"], true);

			foreach ($arCouponsData as $key => $val) {
				$curr_time = strval($val["Дата купона"]);
				if (array_key_exists($curr_time, $arResult)) {
					$val["ITEM"] = $elem["UF_ITEM"];
					$radarItem   = $resO->getItem($elem["UF_ITEM"]);

				   if(empty($radarItem["URL"])){ //Не добавляем купоны, для которых нет облиг в базе
							continue;
					}
					if (!empty($radarItem["PROPS"]["HIDEN"]) && !in_array("дефолтные", $radarItem["PROPS"]["CSV_VID_OLB"])) {
						continue;
					}
					//Исключаем бумаги вышедшие из обращения
					$val["URL"]  = $radarItem["URL"];
					$val["NAME"] = $radarItem["NAME"];
					if (in_array("дефолтные", $radarItem["PROPS"]["CSV_VID_OLB"])) {
						$val["DEFAULT"]     = true;
						$val["DEFAULT_VID"] = $radarItem["PROPS"]["CSV_VID_DEFOLT"];
					} else {
						$val["DEFAULT"] = false;
					}
					if (!empty($radarItem["PROPS"]["ADDITIONAL_INCOME"])) {
						$val["STRUCTURED"] = true;
					} else {
						$val["STRUCTURED"] = false;
					}
					$val["CURRENCY"] = $radarItem["PROPS"]["CURRENCYID"];
					$arResult[$curr_time]["COUPONS"][] = $val;
				}
			}
		}
		unset($resO);
	}

	public function getCacheSumForPortfolio($portfolioId=0, $uid=0, $dateFrom='', $dateTo=''){
	  $_REQUEST["eventPortfolio"] = $portfolioId;
	  $_REQUEST["eventTypes"] = "DIVIDENDS,DIVIDENDS_USA,COUPONS";

	}
} //end of class
