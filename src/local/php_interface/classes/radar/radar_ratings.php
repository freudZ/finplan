<?
use Bitrix\Highloadblock as HL;

 //Класс для работы с рейтингами радара

 class CRadarRatings {
	private $hl_prices_rus = 24;
	private $hl_prices_usa = 29;
	private $bondsIblock = 27;
	public $arPagerCount = array(20,50,100,200, 500, 1000, 2000);
	public $arRatingsType = array("INCREASE"=>"Лидеры роста РФ", "INCREASE_ETF"=>"Лидеры роста ETF", "INCREASE_USA"=>"Лидеры роста США", "DROP"=>"Лидеры падения РФ", "DROP_ETF"=>"Лидеры падения ETF", "DROP_USA"=>"Лидеры падения США",
											"CAP_RUS"=>"Капитализация РФ", "CAP_USA"=>"Капитализация США", "DIV_RUS"=>"Рейтинг дивидендов РФ", "DIV_USA"=>"Рейтинг дивидендов США",
											"PE_RUS"=>"Текущий P/E РФ", "PE_USA"=>"Текущий P/E США",
											"RSK_RUS"=>"Рентабельность СК РФ", "RSK_USA"=>"Рентабельность СК США",
											"SLIDE_PROCEEDS_RUS"=>"Скользящая выручка РФ", "SLIDE_PROCEEDS_USA"=>"Скользящая выручка США",
											"SLIDE_PROFIT_RUS"=>"Скользящая прибыль РФ", "SLIDE_PROFIT_USA"=>"Скользящая прибыль США",
											"TEMP_SLIDE_PROCEEDS_RUS"=>"Темп роста скользящей выручки РФ", "TEMP_SLIDE_PROCEEDS_USA"=>"Темп роста скользящей выручки США",
											"TEMP_SLIDE_PROFIT_RUS"=>"Темп роста скользящей прибыли РФ", "TEMP_SLIDE_PROFIT_USA"=>"Темп роста скользящей прибыли США",
											);//Типы рейтингов
	public $arNeedAuthRatings = array("PE_RUS", "PE_USA", "RSK_RUS", "RSK_USA", "SLIDE_PROCEEDS_RUS", "SLIDE_PROCEEDS_USA", "SLIDE_PROFIT_RUS", "SLIDE_PROFIT_USA",
												 "TEMP_SLIDE_PROCEEDS_RUS", "TEMP_SLIDE_PROCEEDS_USA", "TEMP_SLIDE_PROFIT_RUS", "TEMP_SLIDE_PROFIT_USA", "DIV_RUS", "DIV_USA");
	public $defaultRatingType = "INCREASE"; //Тип рейтинга по умолчанию
	public $defaultRatingPeriod = "DAY"; //Период по умолчанию
	public $arTypesForPeriod = array("INCREASE", "INCREASE_ETF", "INCREASE_USA", "DROP", "DROP_ETF", "DROP_USA"); //Типы рейтингов, для которых показывается выбор периода
	public $arRatingsList = array();
	public $arRatingsPeriod = array("DAY"=>"за день", "MONTH"=>"за месяц", "YEAR"=>"за год", "THREE_YEAR"=>"за три года"); //Список периодов с описаниями
	public $topDayCount = 20; //Количество актиов в топах для вывода
	public $arFilteredData = array();//Отфильтрованныйй набор данных
    public $arIndustryRus = array();
	public function __construct() { 
	  Global $APPLICATION;
	  $this->hl_prices_usa = $APPLICATION->usaPricesHlId;

	  if(isset($_REQUEST['topDayCount']) && intval($_REQUEST['topDayCount'])>0){
	      $this->topDayCount = intval($_REQUEST['topDayCount']);
      }
	  
      $this->fillIndustriesRusList();
	  //$this->setDatesRus();
	  $this->setMainArray();
	  $this->setFilter();
	}

	private function fillIndustriesRusList(){
        $this->arIndustryRus = array();
        $CIndustriesRus = new CIndustriesRus();
        $this->arIndustryRus = $CIndustriesRus->arOriginalsItems;
        unset($CIndustriesRus);
    }
	
	private function setDatesRus(){
	 $now = new DateTime();
	 $this->arDates = array("BEFORE"=>$now, "NOW"=>$now);


	}

	public function setMainArray(){
		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
      	$cacheId = "radar_ratings";
      	$cacheTtl = 0;//86400 * 2;
             //$cache->clean("radar_ratings");
			 $arResult = array();
      	if ($cache->read($cacheTtl, $cacheId)) {
                  $arResult = $cache->get($cacheId);
              } else {
              	CModule::IncludeModule("highloadblock");
					CModule::IncludeModule("iblock");

					$arTmpFormatArray = array("DAY"=>array(), "MONTH"=>array(), "YEAR"=>array(), "THREE_YEAR"=>array());

					$arResult = array("INCREASE"=>$arTmpFormatArray, "DROP"=>$arTmpFormatArray, "INCREASE_ETF"=>$arTmpFormatArray, "DROP_ETF"=>$arTmpFormatArray, "INCREASE_USA"=>$arTmpFormatArray, "DROP_USA"=>$arTmpFormatArray);

					$this->setIncreaseDropRus($arResult);
					$this->setIncreaseDropETF($arResult);
					$this->setIncreaseDropUsa($arResult);

					$cache->set($cacheId, $arResult);
              }

        
        $this->arRatingsList = $arResult;
	}

	//Формирует лидеров роста и падения по США
	private function setIncreaseDropUsa(&$arResult){
		$resAU = new ActionsUsa;

		$arAddPropsToResult = array("ISSUECAPITALIZATION");
		$arAddPeriodsToResult = array("Рентабельность собственного капитала", "Темп прироста выручки",
												"Темп роста прибыли", "Выручка за год (скользящая)", "Прибыль за год (скользящая)"
												);
		$arActionsIncrease = array();
		//Собираем все акции в более легкий массив
		foreach($resAU->arOriginalsItems as $arItem){
		   if(!empty($arItem["PROPS"]["EXCLUDE_FROM_RATING"])) continue; //Исключаем отмеченные акции из рейтинга
		   if(!empty($arItem["PROPS"]["HIDEN"])) continue; //Исключаем вышедшие из обращения
			if($arItem["COMPANY"]["CURRENCY"]!="млн. долл. США") continue;
			$lastPeriod = reset($arItem["PERIODS"]);
			unset($arItem["PERIODS"]);
		 $arActionsIncrease[] = array(
		 							"NAME"=>$arItem["NAME"], "CODE"=>$arItem["CODE"], "URL"=>$arItem["URL"],
									"PROPS"=>array_intersect_key($arItem["PROPS"], array_flip($arAddPropsToResult)), "DYNAM"=>$arItem["DYNAM"],
									"PERIODS"=>array_intersect_key($lastPeriod, array_flip($arAddPeriodsToResult)),
		 					 );
		}

		
		unset($resAU);

		//Дневной рост
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["DAY_INCREASE"] > $item1["DYNAM"]["DAY_INCREASE"];
		});
		$this->setIncreaseDropFromArray("DAY", "INCREASE_USA", $arResult, $arActionsIncrease);
		//Рост за месяц
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["MONTH_INCREASE"] <=> $item1["DYNAM"]["MONTH_INCREASE"];
		});
		$this->setIncreaseDropFromArray("MONTH", "INCREASE_USA", $arResult, $arActionsIncrease);
		//Рост за год
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["YEAR_INCREASE"] <=> $item1["DYNAM"]["YEAR_INCREASE"];
		});
		$this->setIncreaseDropFromArray("YEAR", "INCREASE_USA", $arResult, $arActionsIncrease);
		//Рост за три года
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["THREE_YEAR_INCREASE"] <=> $item1["DYNAM"]["THREE_YEAR_INCREASE"];
		});
		$this->setIncreaseDropFromArray("THREE_YEAR", "INCREASE_USA", $arResult, $arActionsIncrease);

		//Дневное падение
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["DAY_INCREASE"] > $item2["DYNAM"]["DAY_INCREASE"];
		});
		$this->setIncreaseDropFromArray("DAY", "DROP_USA", $arResult, $arActionsIncrease);

		//Падение за месяц
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["MONTH_INCREASE"] <=> $item2["DYNAM"]["MONTH_INCREASE"];
		});
		$this->setIncreaseDropFromArray("MONTH", "DROP_USA", $arResult, $arActionsIncrease);
		//Падение за год
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["YEAR_INCREASE"] <=> $item2["DYNAM"]["YEAR_INCREASE"];
		});
		$this->setIncreaseDropFromArray("YEAR", "DROP_USA", $arResult, $arActionsIncrease);
		//Падение за три года
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["THREE_YEAR_INCREASE"] <=> $item2["DYNAM"]["THREE_YEAR_INCREASE"];
		});
		$this->setIncreaseDropFromArray("THREE_YEAR", "DROP_USA", $arResult, $arActionsIncrease);

		//капа
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PROPS"]["ISSUECAPITALIZATION"] <=> $item1["PROPS"]["ISSUECAPITALIZATION"];
		});
		$this->setSimpleRatingsFromArray("CAP_USA", "PROPS", "ISSUECAPITALIZATION", $arResult, $arActionsIncrease);

		//Дивиденды
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["Дивиденды %"] <=> $item1["DYNAM"]["Дивиденды %"];
		});
		$this->setSimpleRatingsFromArray("DIV_USA", "DYNAM", "Дивиденды %", $arResult, $arActionsIncrease);

		//PE
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["PE"] <=> $item2["DYNAM"]["PE"];
		});
		$arActionsIncreasePE = $this->unsetArrayMinusValues($arActionsIncrease, true, "DYNAM", "PE");
		$this->setSimpleRatingsFromArray("PE_USA", "DYNAM", "PE", $arResult, $arActionsIncreasePE);

		//Рентабельность собственного капитала
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Рентабельность собственного капитала"] <=> $item1["PERIODS"]["Рентабельность собственного капитала"];
		});
		$this->setSimpleRatingsFromArray("RSK_USA", "PERIODS", "Рентабельность собственного капитала", $arResult, $arActionsIncrease);

		//Скользящая выручка
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Выручка за год (скользящая)"] <=> $item1["PERIODS"]["Выручка за год (скользящая)"];
		});
		$this->setSimpleRatingsFromArray("SLIDE_PROCEEDS_USA", "PERIODS", "Выручка за год (скользящая)", $arResult, $arActionsIncrease);

		//Скользящая прибыль
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Прибыль за год (скользящая)"] <=> $item1["PERIODS"]["Прибыль за год (скользящая)"];
		});
		$this->setSimpleRatingsFromArray("SLIDE_PROFIT_USA", "PERIODS", "Прибыль за год (скользящая)", $arResult, $arActionsIncrease);

		//Темп роста скользящей выручки
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Темп прироста выручки"] <=> $item1["PERIODS"]["Темп прироста выручки"];
		});
		$this->setSimpleRatingsFromArray("TEMP_SLIDE_PROCEEDS_USA", "PERIODS", "Темп прироста выручки", $arResult, $arActionsIncrease);

		//Темп роста скользящей прибыли
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Темп роста прибыли"] <=> $item1["PERIODS"]["Темп роста прибыли"];
		});
        

	 
		$this->setSimpleRatingsFromArray("TEMP_SLIDE_PROFIT_USA", "PERIODS", "Темп роста прибыли", $arResult, $arActionsIncrease);
	}

	//Формирует лидеров роста и падения по РФ
	private function setIncreaseDropRus(&$arResult){
		$resA = new Actions;

		$arAddPropsToResult = array("ISSUECAPITALIZATION");
		$arAddPeriodsToResult = array("Рентабельность собственного капитала", "Темп прироста выручки",
												"Темп роста прибыли", "Выручка за год (скользящая)", "Прибыль за год (скользящая)"
												);
		$arActionsIncrease = array();
		//Собираем все акции в более легкий массив
		foreach($resA->arOriginalsItems as $arItem){
			if(!empty($arItem["PROPS"]["EXCLUDE_FROM_RATING"])) continue; //Исключаем отмеченные акции из рейтинга
			if(!empty($arItem["PROPS"]["HIDEN"])) continue; //Исключаем вышедшие из обращения
			$lastPeriod = reset($arItem["PERIODS"]);
			unset($arItem["PERIODS"]);
			unset($arItem["DYNAM"]["Доля в индексах"]);
		 $arActionsIncrease[] = array(
		 							"NAME"=>$arItem["NAME"], "CODE"=>$arItem["CODE"], "URL"=>$arItem["URL"],
									"PROPS"=>array_intersect_key($arItem["PROPS"], array_flip($arAddPropsToResult)), "DYNAM"=>$arItem["DYNAM"],
									"PERIODS"=>array_intersect_key($lastPeriod, array_flip($arAddPeriodsToResult)),
		 					 );
		}

		unset($resA);



		//Дневной рост
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["DAY_INCREASE"] > $item1["DYNAM"]["DAY_INCREASE"];
		});
		$this->setIncreaseDropFromArray("DAY", "INCREASE", $arResult, $arActionsIncrease);
		//Рост за месяц
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["MONTH_INCREASE"] <=> $item1["DYNAM"]["MONTH_INCREASE"];
		});
		$this->setIncreaseDropFromArray("MONTH", "INCREASE", $arResult, $arActionsIncrease);
		//Рост за год
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["YEAR_INCREASE"] <=> $item1["DYNAM"]["YEAR_INCREASE"];
		});
		$this->setIncreaseDropFromArray("YEAR", "INCREASE", $arResult, $arActionsIncrease);
		//Рост за три года
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["THREE_YEAR_INCREASE"] <=> $item1["DYNAM"]["THREE_YEAR_INCREASE"];
		});
		$this->setIncreaseDropFromArray("THREE_YEAR", "INCREASE", $arResult, $arActionsIncrease);


		//Дневное падение
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["DAY_INCREASE"] > $item2["DYNAM"]["DAY_INCREASE"];
		});
		$this->setIncreaseDropFromArray("DAY", "DROP", $arResult, $arActionsIncrease);
		//Падение за месяц
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["MONTH_INCREASE"] <=> $item2["DYNAM"]["MONTH_INCREASE"];
		});
		$this->setIncreaseDropFromArray("MONTH", "DROP", $arResult, $arActionsIncrease);
		//Падение за год
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["YEAR_INCREASE"] <=> $item2["DYNAM"]["YEAR_INCREASE"];
		});
		$this->setIncreaseDropFromArray("YEAR", "DROP", $arResult, $arActionsIncrease);
		//Падение за три года
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["THREE_YEAR_INCREASE"] <=> $item2["DYNAM"]["THREE_YEAR_INCREASE"];
		});
		$this->setIncreaseDropFromArray("THREE_YEAR", "DROP", $arResult, $arActionsIncrease);

		//капа
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PROPS"]["ISSUECAPITALIZATION"] <=> $item1["PROPS"]["ISSUECAPITALIZATION"];
		});
		$this->setSimpleRatingsFromArray("CAP_RUS", "PROPS", "ISSUECAPITALIZATION", $arResult, $arActionsIncrease);

		//Дивиденды
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["Дивиденды %"] <=> $item1["DYNAM"]["Дивиденды %"];
		});
		$this->setSimpleRatingsFromArray("DIV_RUS", "DYNAM", "Дивиденды %", $arResult, $arActionsIncrease);

		//PE
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["PE"] <=> $item2["DYNAM"]["PE"];
		});
		$arActionsIncrease = $this->unsetArrayMinusValues($arActionsIncrease, true, "DYNAM", "PE");
		$this->setSimpleRatingsFromArray("PE_RUS", "DYNAM", "PE", $arResult, $arActionsIncrease);

		//Рентабельность собственного капитала
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Рентабельность собственного капитала"] <=> $item1["PERIODS"]["Рентабельность собственного капитала"];
		});
		$this->setSimpleRatingsFromArray("RSK_RUS", "PERIODS", "Рентабельность собственного капитала", $arResult, $arActionsIncrease);

		//Скользящая выручка
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Выручка за год (скользящая)"] <=> $item1["PERIODS"]["Выручка за год (скользящая)"];
		});
		$this->setSimpleRatingsFromArray("SLIDE_PROCEEDS_RUS", "PERIODS", "Выручка за год (скользящая)", $arResult, $arActionsIncrease);

		//Скользящая прибыль
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Прибыль за год (скользящая)"] <=> $item1["PERIODS"]["Прибыль за год (скользящая)"];
		});
		$this->setSimpleRatingsFromArray("SLIDE_PROFIT_RUS", "PERIODS", "Прибыль за год (скользящая)", $arResult, $arActionsIncrease);

		//Темп роста скользящей выручки
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Темп прироста выручки"] <=> $item1["PERIODS"]["Темп прироста выручки"];
		});
		$this->setSimpleRatingsFromArray("TEMP_SLIDE_PROCEEDS_RUS", "PERIODS", "Темп прироста выручки", $arResult, $arActionsIncrease);

		//Темп роста скользящей прибыли
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["PERIODS"]["Темп роста прибыли"] <=> $item1["PERIODS"]["Темп роста прибыли"];
		});
		$this->setSimpleRatingsFromArray("TEMP_SLIDE_PROFIT_RUS", "PERIODS", "Темп роста прибыли", $arResult, $arActionsIncrease);
	}

	//Формирует лидеров роста и падения ETF
	private function setIncreaseDropETF(&$arResult){
		$resETF = new ETF;

	    $arAddPropsToResult = array("ETF_CALC_BASE");
	  //	$arAddPeriodsToResult = array("Рентабельность собственного капитала", "Темп прироста выручки", "Темп роста прибыли");
		$arActionsIncrease = array();
		$arActionsDrop = array();
		//Собираем все акции в более легкий массив
		foreach($resETF->arOriginalsItems as $arItem){
			//$lastPeriod = reset($arItem["PERIODS"]);
			unset($arItem["PERIODS"]);
			unset($arItem["DYNAM"]["Доля в индексах"]);
		 $arActionsIncrease[] = array(
		 							"NAME"=>$arItem["NAME"], "CODE"=>$arItem["CODE"], "URL"=>$arItem["URL"],
									"PROPS"=>array_intersect_key($arItem["PROPS"], array_flip($arAddPropsToResult)),
									"DYNAM"=>$arItem["DYNAM"],
									//"PERIODS"=>array_intersect_key($lastPeriod, array_flip($arAddPeriodsToResult)),
		 					 );
		}
        $arActionsDrop = $arActionsIncrease;
		unset($resETF);



		//Дневной рост
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["DAY_INCREASE"] > $item1["DYNAM"]["DAY_INCREASE"];
		});
        $arActionsIncrease = $this->unsetArrayMinusValues($arActionsIncrease, true, "DYNAM", "DAY_INCREASE");
        $this->setIncreaseDropFromArray("DAY", "INCREASE_ETF", $arResult, $arActionsIncrease);
		//Рост за месяц
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["MONTH_INCREASE"] <=> $item1["DYNAM"]["MONTH_INCREASE"];
		});
        $arActionsIncrease = $this->unsetArrayMinusValues($arActionsIncrease, true, "DYNAM", "MONTH_INCREASE");
        $this->setIncreaseDropFromArray("MONTH", "INCREASE_ETF", $arResult, $arActionsIncrease);
		//Рост за год
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["YEAR_INCREASE"] <=> $item1["DYNAM"]["YEAR_INCREASE"];
		});
        $arActionsIncrease = $this->unsetArrayMinusValues($arActionsIncrease, true, "DYNAM", "YEAR_INCREASE");
        $this->setIncreaseDropFromArray("YEAR", "INCREASE_ETF", $arResult, $arActionsIncrease);
		//Рост за три года
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item2["DYNAM"]["THREE_YEAR_INCREASE"] <=> $item1["DYNAM"]["THREE_YEAR_INCREASE"];
		});
        $arActionsIncrease = $this->unsetArrayMinusValues($arActionsIncrease, true, "DYNAM", "THREE_YEAR_INCREASE");
        $this->setIncreaseDropFromArray("THREE_YEAR", "INCREASE_ETF", $arResult, $arActionsIncrease);
        
        
        $arActionsIncrease = $arActionsDrop;
        
		//Дневное падение
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["DAY_INCREASE"] > $item2["DYNAM"]["DAY_INCREASE"];
		});
		$arActionsIncrease = $this->unsetArrayPlusValues($arActionsIncrease, "DYNAM", "DAY_INCREASE");
		$this->setIncreaseDropFromArray("DAY", "DROP_ETF", $arResult, $arActionsIncrease);

		//Падение за месяц
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["MONTH_INCREASE"] <=> $item2["DYNAM"]["MONTH_INCREASE"];
		});
		$arActionsIncrease = $this->unsetArrayPlusValues($arActionsIncrease, "DYNAM", "MONTH_INCREASE");
		$this->setIncreaseDropFromArray("MONTH", "DROP_ETF", $arResult, $arActionsIncrease);
		//Падение за год
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["YEAR_INCREASE"] <=> $item2["DYNAM"]["YEAR_INCREASE"];
		});
		$arActionsIncrease = $this->unsetArrayPlusValues($arActionsIncrease, "DYNAM", "YEAR_INCREASE");
		$this->setIncreaseDropFromArray("YEAR", "DROP_ETF", $arResult, $arActionsIncrease);
		//Падение за три года
	 	usort($arActionsIncrease, function ($item1, $item2) {
	    return $item1["DYNAM"]["THREE_YEAR_INCREASE"] <=> $item2["DYNAM"]["THREE_YEAR_INCREASE"];
		});
		$arActionsIncrease = $this->unsetArrayPlusValues($arActionsIncrease, "DYNAM", "THREE_YEAR_INCREASE");
		$this->setIncreaseDropFromArray("THREE_YEAR", "DROP_ETF", $arResult, $arActionsIncrease);
	}

 //Удаляет из массива значения больше нуля, по полю $field
 private function unsetArrayPlusValues($arActionsIncrease, $key, $field){
	  $arTmp = array();
	  for($i=0; $i<count($arActionsIncrease); $i++){

	  	if($arActionsIncrease[$i][$key][$field]>=0) continue;

		  $arTmp[] = $arActionsIncrease[$i];

	  }
		$arActionsIncrease = $arTmp;
	 return $arActionsIncrease;
 }

 //Удаляет из массива значения меньше нуля, по полю $field
 private function unsetArrayMinusValues($arActionsIncrease, $incl_zero=true, $key, $field){
	  $arTmp = array();
	  for($i=0; $i<count($arActionsIncrease); $i++){
	  	if($incl_zero){
	  	if($arActionsIncrease[$i][$key][$field]>=0 && strval($arActionsIncrease[$i][$key][$field])!="-0"){
		  $arTmp[] = $arActionsIncrease[$i];
	  	}
		} else {
	  	if($arActionsIncrease[$i][$key][$field]>0){
		  $arTmp[] = $arActionsIncrease[$i];
	  	}
		}
	  }

    if(count($arTmp)>0){
		$arActionsIncrease = $arTmp;
    }
	 return $arActionsIncrease;
 }

 //Добавляет топ активов без периодов в результат
 private function setSimpleRatingsFromArray($type, $key, $propCode, &$arResult, &$arActionsIncrease){
		$cnt = 0;
 		foreach($arActionsIncrease as $k=>$arAction){
		 if(isset($arAction[$key][$propCode]) && !is_infinite($arAction[$key][$propCode]) && !is_nan($arAction[$key][$propCode]) && floatval($arAction[$key][$propCode])!==0){
		   $arResult[$type][] = $arAction;
			$cnt++;
		 if($cnt==$this->topDayCount-1) break; //Останавливаем заполнение при достижении указанного лимита для топа бумаг
		 } else {
		 	continue; //Пропускаем бумаги с незаполненными данными
		 }
		}
 }

 //Добавляет топ активов в результат
 private function setIncreaseDropFromArray($period="DAY", $type, &$arResult, &$arActionsIncrease){
		$cnt = 0;
 		foreach($arActionsIncrease as $k=>$arAction){
		 if(isset($arAction["DYNAM"][$period."_INCREASE"]) && !is_infinite($arAction["DYNAM"][$period."_INCREASE"]) && !is_nan($arAction["DYNAM"][$period."_INCREASE"]) && floatval($arAction["DYNAM"][$period."_INCREASE"])!==0){
		   $arResult[$type][$period][] = $arAction;
			$cnt++;
		 if($cnt==$this->topDayCount-1) break; //Останавливаем заполнение при достижении указанного лимита для топа бумаг
		 } else {
		 	continue; //Пропускаем бумаги с незаполненными данными
		 }
		}
 }

 private function setFilter(){
	  $this->arFilteredData = array();

		if(!isset($_REQUEST["ratingType"])){
			$ratingType = $this->defaultRatingType;
		} else {
			$ratingType = $_REQUEST["ratingType"];
		}
		if(!isset($_REQUEST["ratingPeriod"])){
			$ratingPeriod = $this->defaultRatingPeriod;
		} else {
			$ratingPeriod = $_REQUEST["ratingPeriod"];
		}

	  //Если рейтинг с периодами
      if(in_array($ratingType, $this->arTypesForPeriod)){
		 $this->arFilteredData = $this->arRatingsList[$ratingType][$ratingPeriod];
	  } else {
		 $this->arFilteredData = $this->arRatingsList[$ratingType];
	  }

 }

 }