<?
use Bitrix\Highloadblock as HL;

//облигации
class Obligations extends RadarBase {
	private $arItems = array();
	public $arOriginalsItems = array();
	public $count;
	private $perPage = 5;
	public $havePayAccess = false;
    public $classType = self::TYPE_OBLIGATION;
	
	function __construct(){
		$this->setData();
		$this->havePayAccess = checkPayRadar();
	}

	//отдельный элемент по коду
	public function getItem($code){
		foreach($this->arOriginalsItems as $item){
			if($item["SECID"]==$code){
				return $item;
			}
		}

		return false;
	}

	public function getItemBySecid($code){
		foreach($this->arOriginalsItems as $item){
			if($item["PROPS"]["SECID"]==$code){
				return $item;
			}
		}

		return false;
	}

	public function getItemByIsin($code){
		foreach($this->arOriginalsItems as $item){
			if($item["PROPS"]["ISIN"]==$code){
				return $item;
			}
		}

		return false;
	}

	private function setData(){
		global $APPLICATION;
		/*$obCache = \Bitrix\Main\Data\Cache::createInstance();
		$cache_time = 0;
		$cache_id = "data3";

		if( $obCache->initCache($cache_time,$cache_id, "/obligations/") )
		{
			$arItems = $obCache->GetVars();
		}elseif( $obCache->startDataCache()){
			*/

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "obligations_data";
		$cacheTtl = 86400*7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arItems = $cache->get($cacheId);
		} else {
			$arItems = array();

			CModule::IncludeModule("iblock");
			CModule::IncludeModule("highloadblock");
			$ibID = 27;

			$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

			$res = $entityClass::getList(array(
				"filter" => array(
					"!UF_YEAR" => false,
				),
				"select" => array(
					"*"
				),
				"order" => array(
					"ID" => "DESC"
				)
			));
			while($item = $res->fetch()){
				$item["UF_DATA"] = json_decode($item["UF_DATA"], true);
				
				$t = $item["UF_MONTH"];
				$type = "MONTH";
				
				if(!$t){
					$t = $item["UF_KVARTAL"];
					$type = "KVARTAL";
				}
				if($t && $item["UF_YEAR"]){
					$item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
					$item["UF_DATA"]["PERIOD_VAL"] = $t;
					$item["UF_DATA"]["PERIOD_TYPE"] = $type;
					
					$arPeriods[$item["UF_COMPANY"]][$t."-".$item["UF_YEAR"]."-".$type] = $item["UF_DATA"];
				}
                if(!$t && $item["UF_YEAR"]){
                    $item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
                    $item["UF_DATA"]["PERIOD_TYPE"] = "YEAR";
                    $arPeriods[$item["UF_COMPANY"]][$item["UF_YEAR"]."-".$type] = $item["UF_DATA"];
                }
			}

			$arFilter = Array("IBLOCK_ID"=>$ibID);
			/*global $USER;
			if($USER->IsAdmin()){
				$arFilter["NAME"] = "Акрон БО-3";
			}*/
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
			while($ob = $res->GetNextElement())
			{
				$fields = $ob->GetFields();
				
				$item = array(
					"ID" => $fields["ID"],
					"NAME" => $fields["NAME"],
					"SECID" => $fields["CODE"],
					//"URL" => $fields["DETAIL_PAGE_URL"],
				);
				
				//страница облигации
				$arFilter = Array("IBLOCK_ID"=>30, "CODE"=>$fields["CODE"]);
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL"));
				if($val = $res2->GetNext())
				{
					$item["URL"] = $val["DETAIL_PAGE_URL"];
				}
				
				$props = $ob->GetProperties();
				foreach($props as $prop){
					if($prop["VALUE"]){
						$item["PROPS"][$prop["CODE"]] = $prop["VALUE"];
					}
				}
				
				//Убрать с режимами торгов
				if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["облигации"])){
					continue;
				}

				//если нет LASTPRICE - берем LEGALCLOSE
				if($item["PROPS"]["LASTPRICE"]<=0 && $item["PROPS"]["LEGALCLOSE"]>0) { 
					$item["PROPS"]["LASTPRICE"] = $item["PROPS"]["LEGALCLOSE"];
				}
				
				if($item["PROPS"]["EMITENT_ID"]){
					$comp = CIBlockElement::GetByID($item["PROPS"]["EMITENT_ID"])->GetNext();
					$item["COMPANY"] = array(
						"ID" => $comp["ID"],
						"NAME" => $comp["NAME"],
						//"URL" => $comp["DETAIL_PAGE_URL"],
					);
					
					//страница компании
					$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$comp["NAME"]);
					$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL", "PROPERTY_CAPITAL_KOEF", "PROPERTY_IS_REGION"));
					if($val = $res2->GetNext())
					{
						$item["COMPANY"]["URL"] = $val["DETAIL_PAGE_URL"];
						
						if($val["PROPERTY_CAPITAL_KOEF_VALUE"]){
							$item["COMPANY"]["CAPITAL_KOEF"] = $val["PROPERTY_CAPITAL_KOEF_VALUE"];
						}

                        if($val["PROPERTY_IS_REGION_VALUE"]){
                            $item["COMPANY"]["IS_REGION"] = $val["PROPERTY_IS_REGION_VALUE"];
                        }
					}
				}
				
				if($item["PROPS"]["EMITENT_ID"] && $arPeriods[$item["PROPS"]["EMITENT_ID"]]){
					$item["PERIODS"] = $arPeriods[$item["PROPS"]["EMITENT_ID"]];
					$item["PERIODS"] = parent::calculatePeriodData($item);

					//Определим последние периоды для квартальной отчетности и по месяцам
					$item["LAST_PERIOD_KVARTAL"] = '';
					$item["LAST_PERIOD_MONTH"] = '';
					reset($item["PERIODS"]);
					foreach($item["PERIODS"] as $period=>$value){
						if(empty($item["LAST_PERIOD_KVARTAL"]) && strpos($period,"KVARTAL")!==false){
							$item["LAST_PERIOD_KVARTAL"] = $period;
						}
						if(empty($item["LAST_PERIOD_MONTH"]) && strpos($period,"MONTH")!==false){
							$item["LAST_PERIOD_MONTH"] = $period;
						}
						if(!empty($item["LAST_PERIOD_KVARTAL"]) && !empty($item["LAST_PERIOD_MONTH"])){
							break;
						}
					}

				}
				
				$item["DYNAM"] = self::setDynamParams($item);
				
				$arItems[] = $item;
			}

			 $cache->set($cacheId, $arItems);
		}
		
		$this->arOriginalsItems = $arItems;
		$this->count = count($arItems);
	}
	
	//таблица вывода
	public function getTable($page=1){
		$this->setFilter();
		$this->sortItems();
		
		$end = $page*$this->perPage;
		$start = $end-$this->perPage;
		
		$arItems = array();
		
		if($page==1){
			$arItems = $this->arItemsSelected;
		}
		
		for($i=$start;$i<$end;$i++){
			if($this->arItems[$i]){
				$arItems[] = $this->arItems[$i];
			}
		}
		
		$shows = $this->perPage*$page+count($this->arItemsSelected);
		if($shows>$this->count+count($this->arItemsSelected)){
			$shows = $this->count;
		}
		
		return array(
			"total_items" => $this->count+count($this->arItemsSelected),
			"cur_page" => $page,
			"shows" => $shows,
			"count_page" => ceil($this->count/$this->perPage),
			"items" => $arItems,
			"access" => $this->havePayAccess,
			//"selected" => $this->getSelectedItems()
		);
	}
	
	//сортировка элементов
	private function sortItems(){
		usort($this->arItems, function($a,$b){
			if ($a["DYNAM"]["Доходность годовая"] == $b["DYNAM"]["Доходность годовая"]) {
				return 0;
			}
			return ($a["DYNAM"]["Доходность годовая"] < $b["DYNAM"]["Доходность годовая"]) ? 1 : -1;
		});
	}
	
	//избранные элементы
	public function getSelectedItems(){
		$arData = array();
		if($_COOKIE["id_arr_debstock_cookie"]){
			$tmp = json_decode($_COOKIE["id_arr_debstock_cookie"]);
			foreach($tmp as $item){
				$t = explode("...", $item);
				$arData[$t[0]] = $t[1];
			}
		}
		
		return $arData;
	}
    
    /**
     * Возвращает список ISIN кодов для фильтра списка облигаций
     * @param $arFilterData
     * @return array
     */
	public function setFilterForList($arFilterData){
	    $listSelectedItems = array();

        foreach($this->arOriginalsItems as $item){
            unset($item["PERIODS"]);
           switch ($arFilterData[0]){
               case "all":
                   $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Погашение"=>$item["PROPS"]["MATDATE"]);
               break;
               case "type":
                   if(!in_array($arFilterData[1], $item["PROPS"]["CSV_VID_OLB"])) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Погашение"=>$item["PROPS"]["MATDATE"]);
               break;
               case "valute":
                   if($item["PROPS"]["CURRENCYID"]=="SUR" || $item["PROPS"]["CURRENCYID"]=="RUB") continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Погашение"=>$item["PROPS"]["MATDATE"]);
                   break;
               case "structural":
                   if(!$item["PROPS"]["ADDITIONAL_INCOME"]) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Погашение"=>$item["PROPS"]["MATDATE"]);
                   break;
               case "payment_order":
                   if (!$item["PROPS"]["CSV_PAYMENT_ORDER"] || $item["PROPS"]["CSV_PAYMENT_ORDER"][0] !== "субординированная") {
                       continue;
                   }
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Погашение"=>$item["PROPS"]["MATDATE"]);
               break;
           }
           
        }
        return $listSelectedItems;
    }

	//фильтр для элементов
	private function setFilter(){
		global $USER;
		$r = $this->setDefaultFilter($_REQUEST);
		$selectedItems = $this->getSelectedItems();
		  $db=true;
		foreach($this->arOriginalsItems as $item){
			//Вышедшие с обращения убираем
			if(!empty($item["PROPS"]["HIDEN"])) continue;

			if(!$selectedItems[$item["PROPS"]["SECID"]]){
				//если нет цены покупки
				/*
				if(!$item["DYNAM"]["Кол-во дней до погашения"] || !$item["DYNAM"]["Доходность годовая"]){
					continue;
				}
				*/

				//вышла с обращения
				if($item["PROPS"]["HIDEN"]){
					continue;
				}

				//Срок погашения, если истек - убираем
				if($item["PROPS"]["MATDATE"]){
					$dt = new DateTime($item["PROPS"]["MATDATE"]);
					if($dt->getTimestamp()<time()){
						continue;
					}
				}
				
				//Выберите срок облигаций  - Кол-во дней до погашения
				if($r["duration"]){
					if($r["duration"]==36){
						$r["duration"] = PHP_INT_MAX;
					}

					$m = $item["DYNAM"]["Кол-во дней до погашения"]/30;
					$m2 = $item["DYNAM"]["Кол-во дней до оферты"]/30;
					if(($m<=0 || $m>$r["duration"]) && ($m2<=0 || $m2>$r["duration"])){
						continue;
					}
				}

				//Выберите уровень доходности ценных бумаг - Доходность годовая
				if($r["rate"] && $r["rate"]<=25){

					$show = false;
					//$item["DYNAM"]["Доходность к офферте"]
					if(($item["DYNAM"]["Доходность годовая"] && $item["DYNAM"]["Доходность годовая"]<=$r["rate"]) || ($item["DYNAM"]["Доходность к офферте"] && $item["DYNAM"]["Доходность к офферте"]<=$r["rate"])){
						$show = true;
					}

					if(!$show){
						continue;
					}
				}

				//Кол-во купонных выплат в год
				if($r["coupons_in_year"] && $r["coupons_in_year"] != "all"){
                        if($r["coupons_in_year"] === '1' && $item["PROPS"]["COUPON_IN_YEARS"] != 1){
                            continue;
                        }
                        if($r["coupons_in_year"] === '2' && $item["PROPS"]["COUPON_IN_YEARS"] != 2){
                            continue;
                        }
                        if($r["coupons_in_year"] === '3' && $item["PROPS"]["COUPON_IN_YEARS"] != 3){
                            continue;
                        }
                        if($r["coupons_in_year"] === '4' && $item["PROPS"]["COUPON_IN_YEARS"] != 4){
                            continue;
                        }
                        if($r["coupons_in_year"] === '5-10'){
                        	if($item["PROPS"]["COUPON_IN_YEARS"] < 5 || $item["PROPS"]["COUPON_IN_YEARS"] > 10){
                            	continue;
									 }
                        }
                        if($r["coupons_in_year"] === '10-999'){
                        	if($item["PROPS"]["COUPON_IN_YEARS"] < 11){
                            	continue;
									 }
                        }
     			}

				//Убирать из выдачи облигации, по которым нет оборотов за последнюю неделю  - Объем торгов
				if($r["turnover_week"]){
					if($item["PROPS"]["VALUE"]<=0){
						continue;
					}
				}
				
				//купоны более % - Купоны в %
				if($r["coupons_more"]){
					$r["coupons_more"] = str_replace("%", "", $r["coupons_more"]);
				
					if($item["DYNAM"]["Купоны в %"]<=$r["coupons_more"]){
						continue;
					}
				}
				
				//Валюта облигации
				if($r["valute"] && $r["valute"]!="all"){
					if($item["PROPS"]["CURRENCYID"]!=$r["valute"]){
						continue;
					}
				}
				

				if($this->havePayAccess){
					//Качество облигаций
					if($r["quality_bonds"]=="without_default"){
						if(in_array("дефолтные", $item["PROPS"]["CSV_VID_OLB"])){
							continue;
						}
					}elseif($r["quality_bonds"]=="only_blue_chips"){
						if(!in_array("голубые фишки", $item["PROPS"]["CSV_VID_OLB"])){
							continue;
						}
					}elseif($r["quality_bonds"]=="only_default"){
						if(!in_array("дефолтные", $item["PROPS"]["CSV_VID_OLB"])){
							continue;
						}
					}elseif($r["quality_bonds"]=="no_risk"){
						if(in_array("дефолтные", $item["PROPS"]["CSV_VID_OLB"]) || $item["DYNAM"]["Доходность годовая"]>15){
							continue;
						}
					}

					//оферта облигаций
                    if($r["price_bonds"] && $r["price_bonds"] != "all"){
						  	if($r["price_bonds"]=="higher"){
								if(floatval($item["PROPS"]["LASTPRICE"])<100){
									continue;
								}
							}elseif($r["price_bonds"]=="higher110"){
								if(floatval($item["PROPS"]["LASTPRICE"])<110){ continue; }
							}elseif($r["price_bonds"]=="higher120"){
								if(floatval($item["PROPS"]["LASTPRICE"])<120){ continue; }
							}elseif($r["price_bonds"]=="higher130"){
								if(floatval($item["PROPS"]["LASTPRICE"])<130){ continue; }
							}elseif($r["price_bonds"]=="lower90"){
								if(floatval($item["PROPS"]["LASTPRICE"])>90){ continue; }
							}elseif($r["price_bonds"]=="lower80"){
								if(floatval($item["PROPS"]["LASTPRICE"])>80){ continue; }
							}elseif($r["price_bonds"]=="lower50"){
								if(floatval($item["PROPS"]["LASTPRICE"])>50){ continue; }
							}elseif($r["price_bonds"]=="equal"){
								if(floatval($item["PROPS"]["LASTPRICE"])!=100){ continue; }
							}
							elseif($r["price_bonds"]=="lower"){
								if(floatval($item["PROPS"]["LASTPRICE"])>=100){
									continue;
								}
                      }
						  }
                    //оферта облигаций
                    if($r["offerdate"] && $r["offerdate"] != "all"){
                        if(!$item["PROPS"]["OFFERDATE"] && $r["offerdate"] === 'yes'){
                            continue;
                        } elseif ($item["PROPS"]["OFFERDATE"] && $r["offerdate"] === 'no') {
                            continue;
                        }
                    }

                    //очередность выплат(субординация)
                    if($r["payment_order"] && $r["payment_order"] !== "all") {
                        if ($r["payment_order"] === 'yes') {
                            if (!$item["PROPS"]["CSV_PAYMENT_ORDER"] || $item["PROPS"]["CSV_PAYMENT_ORDER"][0] !== "субординированная") {
                                continue;
                            }
                        } elseif ($r["payment_order"] === 'no') {
                            if ($item["PROPS"]["CSV_PAYMENT_ORDER"][0] === "субординированная") {
                                continue;
                            }
                        }
                    }

                    //структурность
                    if($r["structural"] && $r["structural"] !== "all") {
                        if ($r["structural"] === 'yes') {
                            if (!$item["PROPS"]["ADDITIONAL_INCOME"]) {
                                continue;
                            }
                        } elseif ($r["structural"] === 'no') {
                            if ($item["PROPS"]["ADDITIONAL_INCOME"][0]) {
                                continue;
                            }
                        }
                    }

                    //дюрация
                    if($r["duration_period"] && $r["duration_period"] !== "all") {

                        $durationPeriods = [
                            "6m",
                            "12m",
                            "24m",
                            "36m",
                            "60m",
                            "84m",
                            "120m",
                        ];

                        if ($r["duration_period"] !== 'more' && in_array($r["duration_period"], $durationPeriods)) {

                            $index = array_search($r["duration_period"], $durationPeriods) - 1;
                            $start = $index >= 0 ? mb_substr($durationPeriods[$index], 0, -1) : 0;
                            $end = mb_substr($r["duration_period"], 0, -1);

                            if ($item["PROPS"]["DURATION"] < $start*30 || $item["PROPS"]["DURATION"] > $end*30) {
                                continue;
                            }
                        } elseif ($r["duration_period"] === 'more') {
                            if ($item["PROPS"]["DURATION"] < 3650) {
                                continue;
                            }
                        }
                    }

                    //листинг
                    if($r["listlevel"] && $r["listlevel"] !== "all"){
                        switch (true){
                            case intval($r["listlevel"]) ===  1 : {
                                if (intval($item["PROPS"]["LISTLEVEL"]) !== 1) {
                                    continue 2;
                                }
                            };break;
                            case intval($r["listlevel"]) ===  2 : {
                                if (intval($item["PROPS"]["LISTLEVEL"]) !== 2) {
                                    continue 2;
                                }
                            };break;
                            case intval($r["listlevel"]) ===  3 : {
                                if (intval($item["PROPS"]["LISTLEVEL"]) !== 3) {
                                    continue 2;
                                }
                            }
                        }
                    }

                    //тип купона
                    if ($r["coupon_type"] && $r["coupon_type"] !== 'all') {
                        if($r["coupon_type"] === 'float') {
                            if($item["DYNAM"]['COUPON_TYPE']!=='float') {
                                continue;
                            }
                        } elseif ($r["coupon_type"] === 'fix') {
                            if ($item["DYNAM"]['COUPON_TYPE']!=='fix') {
                                continue;
                            }
                        }
								if ($r["coupon_type"] === 'variable') {
                            if ($item["DYNAM"]['COUPON_TYPE']!=='variable') {
                                continue;
                            }
                        }
                    }
/*                    if ($r["coupon_type"] && $r["coupon_type"] !== 'all') {
                        if($r["coupon_type"] === 'float') {
                            if (!$item["DYNAM"]['COUPON_TYPE']) {
                                continue;
                            }
                        } elseif ($r["coupon_type"] === 'fix') {
                            if ($item["DYNAM"]['COUPON_TYPE']) {
                                continue;
                            }
                        }
								if ($r["coupon_type"] === 'variable') {
                            if (!$item["DYNAM"]['COUPON_VARIABLE']) {
                                continue;
                            }
                        }
                    }*/

					 //Рейтинг радар filter
                if($this->havePayAccess){
                    if($r["radar-rating"] && $r["radar-rating"] !== 'all'){
                        if(!$item["PROPS"]["PRC_OF_USERS"]){
                            continue;
                        }

								if($r["radar-rating"] === '0-1' && $item["PROPS"]["PRC_OF_USERS"] > 1){
                            continue;
                        }
								if($r["radar-rating"] === '0-5' && $item["PROPS"]["PRC_OF_USERS"] > 5){
                            continue;
                        }
								if($r["radar-rating"] === '0-10' && $item["PROPS"]["PRC_OF_USERS"] > 10){
                            continue;
                        }
								if($r["radar-rating"] === '0-20' && $item["PROPS"]["PRC_OF_USERS"] > 20){
                            continue;
                        }
								if($r["radar-rating"] === '0-30' && $item["PROPS"]["PRC_OF_USERS"] > 30){
                            continue;
                        }
								if($r["radar-rating"] === '1-100' && $item["PROPS"]["PRC_OF_USERS"] < 1){
                            continue;
                        }
								if($r["radar-rating"] === '5-100' && $item["PROPS"]["PRC_OF_USERS"] < 5){
                            continue;
                        }
								if($r["radar-rating"] === '10-100' && $item["PROPS"]["PRC_OF_USERS"] < 10){
                            continue;
                        }
								if($r["radar-rating"] === '20-100' && $item["PROPS"]["PRC_OF_USERS"] < 20){
                            continue;
                        }
								if($r["radar-rating"] === '30-100' && $item["PROPS"]["PRC_OF_USERS"] < 30){
                            continue;
                        }
                    }
                }
                    //аммортизация
                    if ($r["deprecation"] && $r["deprecation"] !== 'all') {

                        if($r["deprecation"] === 'yes') {
                            if (!$item["DYNAM"]['DEPRECATION']) {
                                continue;
                            }
                        } elseif ($r["deprecation"] === 'no') {
                            if ($item["DYNAM"]['DEPRECATION']) {
                                continue;
                            }
                        }
                    }

				}

                if ($r["tax"] && $r["tax"] !== "all") {
                    $from = new DateTime($item["PROPS"]["STARTDATE"]);
                    $dateCondition = $from->getTimestamp() > 1483228800 && $from->getTimestamp() < 1893369600;
                    $freeObl = false;

                    $obligationTypes = [
                        "федеральные",
                        "муниципальные",
                        "банковские",
                        "корпоративные",
                    ];

                    //если тип федеральные или муниципальные, то облигация освобождена от налогов
                    foreach ($obligationTypes as $k => $obligationType) {
                        if (($k === 0 || $k === 1) && in_array($obligationType, $item["PROPS"]["CSV_VID_OLB"])) {
                            $freeObl = true;
                        }
                    }

                    if($dateCondition) {
                        $freeObl = true;
                    }

                    if ($freeObl && $r["tax"] === 'yes') {
                        continue;
                    }

                    if (!$freeObl && $r["tax"] === 'no') {
                        continue;
                    }
                }

                //дата старта "от"
                if ($r["date_start_first"] && strlen($r["date_start_first"])>=9) {
                    $startDate = new DateTime($item["PROPS"]["STARTDATE"]);
                    $usersDate = new DateTime($r["date_start_first"]);
                    if ($usersDate > $startDate) {
                        continue;
                    }
                }

                //дата старта "до"
                if ($r["date_start_last"] && strlen($r["date_start_last"])>=9) {
                    $startDate = new DateTime($item["PROPS"]["STARTDATE"]);
                    $usersDate = new DateTime($r["date_start_last"]);
                    if ($usersDate < $startDate) {
                        continue;
                    }
                }

                //дата гашения "от"
                if ($r["date_cancel_first"] && strlen($r["date_cancel_first"])>=9) {
                    $cancelDate = new DateTime($item["PROPS"]["MATDATE"]);
                    $usersDate = new DateTime($r["date_cancel_first"]);
                    if ($usersDate > $cancelDate) {
                        continue;
                    }
                }

                //дата гашения "до"
                if ($r["date_cancel_last"] && strlen($r["date_cancel_last"])>=9) {
                    $cancelDate = new DateTime($item["PROPS"]["MATDATE"]);
                    $usersDate = new DateTime($r["date_cancel_last"]);
                    if ($usersDate < $cancelDate) {
                        continue;
                    }
                }

				
				//Вид облигаций
				if($r["type"]){
					foreach($r["type"] as $n=>$type){
						if(!$type){
							unset($r["type"][$n]);
						}
					}
					if($r["type"]){
						$show = false;
						foreach($r["type"] as $type){
							if(in_array($type, $item["PROPS"]["CSV_VID_OLB"])){
								$show = true;
							}
						}
						
						if(!$show){
							continue;
						}
					}
				}		

				//ОТЧЕТНОСТЬ период
				if($this->havePayAccess){
					if($r["period"]){
						if(in_array("корпоративные", $item["PROPS"]["CSV_VID_OLB"])){
							$show = true;
							
							foreach($r["period"] as $name=>$vals){
								if(!$vals["use"]){
									continue;
								}
								$vals["percent"] = trim(str_replace("%", "", $vals["percent"]));

								//Если фильтрация по последнему существующему у актива периоду то заменяем значение last на обозначенный период из кеша данных по активу $item["LAST_PERIOD_KVARTAL"]
								if($r["period_value"]=="last"){ //Сравниваем период для каждого актива по его последнему периоду
								  $item_period = str_replace("-KVARTAL", "", $item["LAST_PERIOD_KVARTAL"]);
									if($item["PERIODS"][$item_period."-KVARTAL"][$name] <= $vals["percent"] || !$item["PERIODS"][$item_period."-KVARTAL"][$name]){
										$show = false;
										break;
									}
								} else
								if($item["PERIODS"][$r["period_value"]."-KVARTAL"][$name] <= $vals["percent"] || !$item["PERIODS"][$r["period_value"]."-KVARTAL"][$name]){
									$show = false;
								}
								
								//if(!in_array("корпоративные", $item["PROPS"]["CSV_VID_OLB"])){
								//	$show = false;
								//}
							}
														

							if(!$show){
								continue;
							}
						}
					}
				}

				
				//ОТЧЕТНОСТЬ месяц
				if($this->havePayAccess){
					if($r["month"]){
						if(in_array("банковские", $item["PROPS"]["CSV_VID_OLB"])){
							$show = true;

							//Если фильтрация по последнему существующему у актива периоду то заменяем значение last на обозначенный период из кеша данных по активу $item["LAST_PERIOD_MONTH"]
							if($r["month_value"]=="last"){
							  $r["month_value"] = str_replace("-MONTH", "", $item["LAST_PERIOD_MONTH"]);
							}

							$r["month_value"] = explode("-", $r["month_value"]);
							$r["month_value"][1] = mb_substr($r["month_value"][1], -2);
							$r["month_value"] = implode("-", $r["month_value"]);

							foreach($r["month"] as $name=>$vals){
								if(!$vals["use"]){
									continue;
								}
								
								if($vals["top"]){
									if($item["PERIODS"][$r["month_value"]."-MONTH"][$name] > $vals["top"] || !$item["PERIODS"][$r["month_value"]."-MONTH"][$name]){
										$show = false;
									}
								} else {
									$vals["percent"] = trim(str_replace("%", "", $vals["percent"]));
						
									if($name=="Доля просрочки, %"){
										if($item["PERIODS"][$r["month_value"]."-MONTH"][$name] >= $vals["percent"] || !$item["PERIODS"][$r["month_value"]."-MONTH"][$name]){
											$show = false;
										}
									} else {
										if($item["PERIODS"][$r["month_value"]."-MONTH"][$name] <= $vals["percent"] || !$item["PERIODS"][$r["month_value"]."-MONTH"][$name]){
											$show = false;
										}
									}
								}
								//if(!in_array("банковские", $item["PROPS"]["CSV_VID_OLB"])){
								//	$show = false;
								//}
							}
							
							if(!$show){
								continue;
							}
						}
					}
				}
				
				$this->arItems[] = $item;
			} else {
				$this->arItemsSelected[] = $item;
			}
		}
		$this->count = count($this->arItems);
	}
	
	//Фильтр по умолчанию
	private function setDefaultFilter($r){
		global $APPLICATION;
		if(!isset($r["duration"]) ){
			$r["duration"] = $APPLICATION->obligationDefaultForm["time"];
		}	
		if(!isset($r["rate"])){
			$r["rate"] = $APPLICATION->obligationDefaultForm["rentab"];
		}
		if(!isset($r["turnover_week"])){
			$r["turnover_week"] = "y";
		}
		
		return $r;
	}
	
	private function setDynamParams($item){
		$return = array();
		
		//AA
		if($item["PROPS"]["OFFERDATE"]){
			//=N8-СЕГОДНЯ()
			$diff = date_diff(new DateTime($item["PROPS"]["OFFERDATE"]), new DateTime());
			$return["Кол-во дней до оферты"] = intval($diff->days)+1;
		}
		
		//AB
		if($item["PROPS"]["MATDATE"]){
			//=C8-СЕГОДНЯ()
			$diff = date_diff(new DateTime($item["PROPS"]["MATDATE"]), new DateTime());
			$return["Кол-во дней до погашения"] = intval($diff->days)+1;
		}

		//AC
		if($item["PROPS"]["COUPONVALUE"] && $return["Кол-во дней до оферты"] && $item["PROPS"]["COUPONPERIOD"] && $item["PROPS"]["FACEVALUE"]){
			//=J8*(ЦЕЛОЕ(AA8/K8)+1)+D8
			$return["Цена погашения с учетом купонов в дату оферты"] = $item["PROPS"]["COUPONVALUE"]*(intval($return["Кол-во дней до оферты"]/$item["PROPS"]["COUPONPERIOD"])+1)+$item["PROPS"]["FACEVALUE"];
		}

		//AD
		if($item["PROPS"]["COUPONVALUE"] && $return["Кол-во дней до погашения"] && $item["PROPS"]["COUPONPERIOD"] && $item["PROPS"]["FACEVALUE"]){
			//=J8*(ЦЕЛОЕ(AB8/K8)+1)+D8                   Размер купона * ((Кол-во дней до погашения/Длит. купона)+1)+Номинал
			$return["Цена погашения с учетом купонов"] = $item["PROPS"]["COUPONVALUE"]*(intval($return["Кол-во дней до погашения"]/$item["PROPS"]["COUPONPERIOD"])+1)+$item["PROPS"]["FACEVALUE"];
		}
		
		//Z
		if($item["PROPS"]["FACEVALUE"] && ($item["PROPS"]["LASTPRICE"] || $item["PROPS"]["LEGALCLOSE"]) && $item["PROPS"]["ACCRUEDINT"]){
			//=D8*G8/100+I8
			$return["Цена покупки"] = ($item["PROPS"]["FACEVALUE"] * ($item["PROPS"]["LASTPRICE"] ?: $item["PROPS"]["LEGALCLOSE"])) / 100 + $item["PROPS"]["ACCRUEDINT"];
		} else if($item["PROPS"]["FACEVALUE"] && ($item["PROPS"]["LASTPRICE"] || $item["PROPS"]["LEGALCLOSE"]) && empty($item["PROPS"]["ACCRUEDINT"])){
			$return["Цена покупки"] = ($item["PROPS"]["FACEVALUE"] * ($item["PROPS"]["LASTPRICE"] ?: $item["PROPS"]["LEGALCLOSE"])) / 100;
		}

		//W
		if($return["Цена погашения с учетом купонов"] && $return["Цена покупки"] && $return["Кол-во дней до погашения"]){
			//=((AD8/Z8-1)/AB8*365)*100
			$return["Доходность годовая"] = (($return["Цена погашения с учетом купонов"]/$return["Цена покупки"]-1)/$return["Кол-во дней до погашения"]*365)*100;

			$return["Доходность годовая"] = round($return["Доходность годовая"], 1);
		}

		//X
		if($return["Цена погашения с учетом купонов"] && $return["Цена покупки"]){
			//=(AD8/Z8-1)*100
			$return["Доходность общая"] = ($return["Цена погашения с учетом купонов"]/$return["Цена покупки"]-1)*100;

			$return["Доходность общая"] = round($return["Доходность общая"], 1);
		}
		
		///Y
		if($item["PROPS"]["COUPONPERIOD"] && $item["PROPS"]["COUPONVALUE"] && $return["Цена покупки"]){
			//=(ЦЕЛОЕ(365/K8)*J8/Z8)*100
			$return["Купоны в %"] = (intval(365/$item["PROPS"]["COUPONPERIOD"])*$item["PROPS"]["COUPONVALUE"]/$return["Цена покупки"])*100;
		}

		//V
		if($return["Цена погашения с учетом купонов в дату оферты"] && $return["Цена покупки"] && $return["Кол-во дней до оферты"]){
			//=((AC8/Z8-1)/AA8*365)*100
			$return["Доходность к офферте"] = (($return["Цена погашения с учетом купонов в дату оферты"]/$return["Цена покупки"]-1)/$return["Кол-во дней до оферты"]*365)*100;
		
			$return["Доходность к офферте"] = round($return["Доходность к офферте"], 1);
		}

		//паерерасчет параметров от купонов
		$res = $this->getCouponForItem($item, true);
		/*global $USER;
		if($USER->IsAdmin() && $item['SECID'] == "RU000A0JXTH2"){
			echo "<pre>";
			print_r($res);
			echo "</pre>";
		}*/
		if($res['deprecation']) {
            $return["DEPRECATION"] = true;
        }
		if($res['coupon_type']) {
            //$return["COUPON_TYPE"] = true;
            $return["COUPON_TYPE"] = $res['coupon_type'];
        }
/*		if($res['coupon_variable']) {
            $return["COUPON_VARIABLE"] = true;
        } else {
				$return["COUPON_VARIABLE"] = false;
        }*/

		if($res["COUPONS_TOTAL"]["Доходность годовая"]){
			$return["Доходность годовая"] = $res["COUPONS_TOTAL"]["Доходность годовая"];
		}
		if($res["COUPONS_TOTAL"]["Доходность общая"]){
			$return["Доходность общая"] = $res["COUPONS_TOTAL"]["Доходность общая"];
		}
		if($res["COUPONS_TOTAL"]["Цена гашения"]){
			$return["Цена погашения с учетом купонов"] = $res["COUPONS_TOTAL"]["Цена гашения"];
		}
		if($res["COUPONS_TOTAL"]["KUPONSI"]){
			$return["KUPONSI"] = $res["COUPONS_TOTAL"]["KUPONSI"];
		}

		return $return;
	}


	/**
	 * Возвращает список купонов с посчитанным номиналом
	 *
	 * @param  string   $code код бумаге
	 *
	 *	@param bool $facevalue_only  Возвращать только номинал с ключами по датам купонов
	 *
	 * @return array  массив купонов или массив номиналов к ключами по датам купонов
	 *
	 * @access public
	 */
	public function getCouponsByCode($code, $facevalue_only=false){
		$arResult = array();
		$arItem = $this->getItem($code);
		$hlblock   = HL\HighloadBlockTable::getById(15)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				"=UF_ITEM" => $code,
				"!=UF_DATA" => "[[]]",
				//"!UF_DATA" => false,
			),
			"select" => array(
				"UF_DATA"
			),
			//'cache' => array("ttl" => 3600)
		));
while($item = $res->fetch()){
	if(empty($item["UF_DATA"])) continue;
	$arCoupons = json_decode($item["UF_DATA"], true);



	foreach($arCoupons as $item){
		if($item["Дата"] && $item["Сумма гашения"]){
			$arCouponsDates[$item["Дата"]] = $item["Сумма гашения"];
		}
		if(trim($item["Примечание"])){
			$arCoupons["LIST_VALUES"][] = array(
				"NAME" => "Формула расчета купона",
				"VALUE" => $item["Примечание"],
			);
		}
	}

	$cup_cnt = 0;
	$cup_page_cnt = 0;

	foreach($arCoupons as $item){

		if(!$item["Дата купона"]){
			continue;
		}
		$dt = DateTime::createFromFormat('d.m.Y', trim($item["Дата купона"]));

			if($arCoupons["COUPONS"]){
				$t = $arCoupons["COUPONS"][count($arCoupons["COUPONS"])-1];
				$nominal = $t["Номинал"]-$t["Гашение"];
			} else {
				$nominal = $arItem["PROPS"]["INITIALFACEVALUE"];
			}

			$tmp = array(
				"Дата выплаты" => $dt->format("d.m.Y"),
				"Номинал" => $nominal,
				"Купоны" => $item["Ставка купона"]/100*$nominal*$item["Длительность купона"]/365,
				"Гашение" => $arCouponsDates[$dt->format("d.m.Y")]?$arCouponsDates[$dt->format("d.m.Y")]:0,
				"page" => $cup_page_cnt,
				"Неопределен" => isset($item["Ставка купона"]) && $item["Ставка купона"]=='-'?'Y':'N'
			);

			$tmp["Денежный поток"] = $tmp["Купоны"]+$tmp["Гашение"];

			$arCoupons["COUPONS"][] = $tmp;

		  $cup_cnt++;
		  if($cup_cnt==20){
		  	$cup_cnt = 0;
			$cup_page_cnt++;
		  }

	}
		  //Если нужно вернуть только номиналы по датам
		  if($facevalue_only){
		  	$arTmp = array();
			foreach($arCoupons["COUPONS"] as $arCoupon){
			  $arTmp[$arCoupon["Дата выплаты"]] = $arCoupon["Номинал"];
			}
			 $arCoupons["COUPONS"] = $arTmp;
			 unset($arTmp);
		  }
}
unset($res);

		return $arCoupons["COUPONS"];
	}

	public function getFacevalueOnDate($code, $date){
		if(!is_array($code)){
			$code = array($code);
		}
				     foreach($code as $codeval){
				      $oblig = $this->getItemBySecid($codeval);
						  if(count($oblig)>0) break;
						}
						$arCoupons = $this->getCouponsByCode($oblig["PROPS"]["SECID"], true);
						$queryDate = (new DateTime($date));


						$dtFacevalue = $oblig["PROPS"]["FACEVALUE"];
						foreach($arCoupons as $dt=>$facevalue){
						  $dtKey = new DateTime($dt);
							if($queryDate>=$dtKey){
						      $dtFacevalue = $facevalue;
							} else {
							break;
							}
						}
		return $dtFacevalue;
	}


	private function getCouponForItem($element, $getNote = false){
		CModule::IncludeModule("highloadblock");
		$arResult = [];

		$hlblock   = HL\HighloadBlockTable::getById(15)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				"=UF_ITEM" => $element["SECID"],
				"!=UF_DATA" => "[[]]",
				//"!UF_DATA" => false,
			),
			"select" => array(
				"UF_DATA"
			),
		));

		$showTotal = true;

		while($item = $res->fetch()){
			if(empty($item["UF_DATA"])) continue;
			$arCoupons = json_decode($item["UF_DATA"], true);

			$arTypeDetect = array("STAVKA"=>0, "COMMENT"=>0);

			foreach($arCoupons as $item){
				if($item["Дата"] && $item["Сумма гашения"]){
					$arCouponsDates[$item["Дата"]] = $item["Сумма гашения"];
				}
                //if(trim($item["Примечание"])){
/*                if(trim($item["Примечание"]) || (isset($item["Ставка купона"]) && $item["Ставка купона"]=='-')){
                    $arResult["coupon_type"][] = 1;
                }*/

					 //Определение переменного купона
					 if(isset($item["Ставка купона"]) && $item["Ставка купона"]=='-'){
						$arTypeDetect["STAVKA"] += 1;
					 }
					 if(!empty(trim($item["Примечание"]))){
						$arTypeDetect["COMMENT"] += 1;
					 }
			}

			//Если нет прочерков - тогда тип купона фиксированный
			if($arTypeDetect["STAVKA"]==0){
			  //$arResult["coupon_variable"] = true;
			  $arResult["coupon_type"] = 'fix';
			}

			//Если хотя бы один прочерк из всех купонов и ни одной формулы "примечание" - тогда ртип купона переменный
			if($arTypeDetect["STAVKA"]>0 && $arTypeDetect["COMMENT"]==0){
			  //$arResult["coupon_variable"] = true;
			  $arResult["coupon_type"] = 'variable';
			}

			//Если хотя бы один прочерк из всех купонов и есть хоть одна формула "примечание" - тогда ртип купона плавающий
			if($arTypeDetect["STAVKA"]>0 && $arTypeDetect["COMMENT"]>0){
			  //$arResult["coupon_type"][] = 1;
			  $arResult["coupon_type"] = 'float';
			}

			foreach($arCoupons as $item){

				if(!$item["Дата купона"]){
					continue;
				}

				$dt = DateTime::createFromFormat('d.m.Y', $item["Дата купона"]);
				//$dt = new DateTime($item["Дата купона"]);
				if($dt->getTimeStamp()>time()){

					if($arResult["COUPONS"]){
						$t = $arResult["COUPONS"][count($arResult["COUPONS"])-1];
						$nominal = $t["Номинал"]-$t["Гашение"];
					} else {
						$nominal = $element["PROPS"]["FACEVALUE"];
					}

/*					$tmp = array(
						"Дата выплаты" => $dt->format("d.m.Y"),
						"Номинал" => $nominal,
						"Купоны" => round($item["Ставка купона"]/100*$nominal*$item["Длительность купона"]/365, 2),
						"Гашение" => round($arCouponsDates[$dt->format("d.m.Y")]?$arCouponsDates[$dt->format("d.m.Y")]:0, 2),
					);*/

					$tmp = array(
						"Дата выплаты" => $dt->format("d.m.Y"),
						"Номинал" => $nominal,
						"Купоны" => $item["Ставка купона"]/100*$nominal*$item["Длительность купона"]/365,
						"Гашение" => $arCouponsDates[$dt->format("d.m.Y")]?$arCouponsDates[$dt->format("d.m.Y")]:0,
						"Неопределен" => isset($item["Ставка купона"]) && $item["Ставка купона"]=='-'?'Y':'N'
					);

					$tmp["Денежный поток"] = $tmp["Купоны"]+$tmp["Гашение"];


					$arResult["COUPONS"][] = $tmp;
				}
			}

			if(count($arResult["COUPONS"])>0){
				$firstNominal = "";
                $lastCoupon = 0;
				$lastDt = "";
				$lastDate = "";

				$dt = new DateTime(date('d.m.Y'));
                $beforeCancelationDays = 0;

				foreach($arResult["COUPONS"] as $n=>$item){
					if($n === 0){
						$firstNominal = $item["Номинал"];
					}
                    $lastCoupon = $item["Купоны"] ?: $lastCoupon;

					if($item["Гашение"]) {
                        $arResult["deprecation"][] = 1;
                    }

					//подсчет дней для корректного расчета годовой доходности
                    if ($n === 0) {
                        $previousDate = $dt;
                    } else {
                        $previousDate = $currentDate;
                    }
                    $currentDate = new DateTime($item["Дата выплаты"]);

                    $difference = $currentDate->diff($previousDate)->days;

                    $arResult["COUPONS"][$n]["Период, дней"] = $difference;

                    $intervalToNow = $dt->diff($currentDate)->days;

                    if ($intervalToNow < $difference) {
                        $beforeCancelationDays += $intervalToNow;
                        $arResult["COUPONS"][$n]["вспомогательный"] = $intervalToNow;
                    } else {
                        $arResult["COUPONS"][$n]["вспомогательный"] = $difference * $item["Номинал"] / $firstNominal;
                        $beforeCancelationDays += $difference * $item["Номинал"] / $firstNominal;
                    }

                    $arResult["COUPONS_TOTAL"]["Купоны"] += $item["Купоны"] ?: $lastCoupon;
                    $arResult["COUPONS_TOTAL"]["Дней до погашения приведенное"] += $arResult["COUPONS"][$n]["вспомогательный"];

					//if(!$item["Купоны"]){
					if($item["Неопределен"]=='Y'){
						$arResult["COUPONS"][$n]["Купоны"] = "Купон пока не определен";
						$showTotal = false;
						continue;
					}
					if(!$item["Денежный поток"]){
						$arResult["COUPONS"][$n]["Денежный поток"] = "Зависит от купона";
						$showTotal = false;
                        continue;
					}
				}

				$arResult["COUPONS_TOTAL"]["Цена гашения"] = $arResult["COUPONS_TOTAL"]["Купоны"]+$firstNominal;

                if($element["PROPS"]["FACEVALUE"] && $element["PROPS"]["LASTPRICE"]) {
                    $lastPrice = $element["PROPS"]["FACEVALUE"] * ($element["PROPS"]["LASTPRICE"] ?: $element["PROPS"]["LEGALCLOSE"]) / 100 + floatval($element["PROPS"]["ACCRUEDINT"]);
                }

				if(isset($lastPrice) && $lastPrice !== 0) {
					$arResult["COUPONS_TOTAL"]["Доходность общая"] = ($arResult["COUPONS_TOTAL"]["Цена гашения"] - $lastPrice) * 100 / $lastPrice;
					$arResult["COUPONS_TOTAL"]["Доходность годовая"] = round(($arResult["COUPONS_TOTAL"]["Доходность общая"] / $beforeCancelationDays) * 365, 2);
					$arResult["COUPONS_TOTAL"]["Доходность общая"] = round($arResult["COUPONS_TOTAL"]["Доходность общая"], 2);
				}

			}
		}

		if(!$showTotal && !$getNote){
			return false;
		}

		return $arResult;
	}
}
?>