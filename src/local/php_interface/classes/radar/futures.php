<?
/**
 * Фьючерсы
 *
 * [add longDescription]
 *
 * @category  [add CategoryName]
 * @package   [add PackageName]
 * @author    [add AuthorName] <[add AuthorEmail]>
 * @copyright 2020
 * @license   [add LicenseUrl]
 * @version   [add VersionNumber]
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     37 class CFutures extends RadarBase
 *     48   function __construct()
 *     54   function getItem($code)
 *     65   function getItemById($id)
 *     76   function getTable($page = 1)
 *    111   function setData()
 *    208   function setDynamParams($item)
 *    220   function setDefaultFilter($r)
 *    236   function setFilter()
 *    251   function getSelectedItems()
 *
 * TOTAL FUNCTIONS: 9
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
//Акции
class CFutures extends RadarBase {
	private $arItems         = array();
	public $arOriginalsItems = array();
	public $count;
	private $perPage        = 5;
	public $havePayAccess   = false;
	public $classType       = self::TYPE_FUTURES;
	public $currencyRateUSD = 1;
	public $IblockId        = 70;
	public $HLIblockId      = 38; //HL с ценами фьючерсов

	function __construct() {
		$this->setData();
		$this->havePayAccess = checkPayRadar();
	}

	//отдельный элемент по коду
	public function getItem($code) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["SECID"] == $code) {
				return $item;
			}
		}

		return false;
	}

	//отдельный элемент по id
	public function getItemById($id) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["ID"] == $id) {
				return $item;
			}
		}

		return false;
	}

	//таблица вывода
	public function getTable($page = 1) {
		$this->setFilter();
		$this->sortItems();

		$end   = $page * $this->perPage;
		$start = $end - $this->perPage;

		$arItems = array();

		if ($page == 1) {
			$arItems = $this->arItemsSelected;
		}

		for ($i = $start; $i < $end; $i++) {
			if ($this->arItems[$i]) {
				$arItems[] = $this->arItems[$i];
			}
		}

		$shows = $this->perPage * $page + count($this->arItemsSelected);
		if ($shows > $this->count + count($this->arItemsSelected)) {
			$shows = $this->count;
		}

		return array(
			"total_items" => $this->count + count($this->arItemsSelected),
			"cur_page" => $page,
			"shows" => $shows,
			"count_page" => ceil($this->count / $this->perPage),
			"items" => $arItems,
			"access" => $this->havePayAccess,
			//"selected" => $this->getSelectedItems()
		);
	}

	private function setData() {
		global $APPLICATION;

		$this->currencyRateUSD = getCBPrice("USD", (new DateTime())->format("d/m/Y"));

		$cache    = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId  = "futures_data";
		$cacheTtl = 86400 * 7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arItems = $cache->get($cacheId);
		} else {
			$arItems = array();

			CModule::IncludeModule("iblock");
			CModule::IncludeModule("highloadblock");

			//Предыдущий день
			$prevDay = new DateTime();
			$prevDay->modify("-1 day");

			$arFilter = Array("IBLOCK_ID" => $this->IblockId);
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false);
			while ($ob = $res->GetNextElement()) {
				$fields = $ob->GetFields();

				$item = array(
					"ID" => $fields["ID"],
					"NAME" => $fields["NAME"],
					"SECID" => $fields["CODE"],
					"URL" => $fields["DETAIL_PAGE_URL"],
				);

				$props = $ob->GetProperties();

				foreach ($props as $prop) {
					if ($prop["VALUE"] || $prop["VALUE"] == 0 || $prop["CODE"] == "HIDEN") {
						$item["PROPS"][$prop["CODE"]] = $prop["VALUE"];
					}
				}

				//Убрать с режимами торгов
				/*          if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["акции"]) || !$item["PROPS"]["BOARDID"]){
				continue;
				}*/

				//Убрать выведенные с биржи
				if (new DateTime($props["LASTTRADEDATE"]["VALUE"]) < new DateTime()) {
					continue;
				}

				if ($item["PROPS"]["EMITENT_ID"]) {
					$comp            = CIBlockElement::GetByID($item["PROPS"]["EMITENT_ID"])->GetNext();
					$item["COMPANY"] = array(
						"ID" => $comp["ID"],
						"NAME" => $comp["NAME"],
						//"URL" => $comp["DETAIL_PAGE_URL"],
					);

					//страница компании
					$arFilter = Array("IBLOCK_ID" => 29, "NAME" => $comp["NAME"]);
					$res2     = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL"));
					if ($val = $res2->GetNext()) {
						$item["COMPANY"]["URL"] = $val["DETAIL_PAGE_URL"];
					}
				}

				//Получим базовый актив
				if ($item["PROPS"]["ASSET_ID"]) {
					$comp          = CIBlockElement::GetByID($item["PROPS"]["ASSET_ID"])->GetNext();
					$item["ASSET"] = array(
						"ID" => $comp["ID"],
						"NAME" => $comp["NAME"],
						//"URL" => $comp["DETAIL_PAGE_URL"],
					);

					//страница акции
					$arFilter = Array("IBLOCK_ID" => 33, "NAME" => $comp["NAME"]);
					$res2     = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL"));
					if ($val = $res2->GetNext()) {
						$item["ASSET"]["URL"] = $val["DETAIL_PAGE_URL"];
					}
				}

				$item["DYNAM"] = self::setDynamParams($item);

				$arItems[] = $item;
			}

			$cache->set($cacheId, $arItems);
		}

		$this->arOriginalsItems = $arItems;
		$this->count            = count($arItems);
	}

	private function setDynamParams($item) {
		$return = array();

		//Цена лота
		if ($item["PROPS"]["LOTSIZE"] && $item["PROPS"]["LASTSETTLEPRICE"]) {
			$return["Цена лота"] = $item["PROPS"]["LOTSIZE"] * $item["PROPS"]["LASTSETTLEPRICE"];
		}

		return $return;
	}

	//Фильтр по умолчанию
	private function setDefaultFilter($r) {
		global $APPLICATION;
		if (!isset($r["pe"])) {
			//   $r["pe"] = $APPLICATION->obligationDefaultForm["pe"];
		}
		if (!isset($r["profitability"])) {
			//   $r["profitability"] = $APPLICATION->obligationDefaultForm["profitability"];
		}
		if (!isset($r["turnover_week"])) {
			//   $r["turnover_week"] = "y";
		}

		return $r;
	}

	//фильтр для элементов
	private function setFilter() {
		$r             = $this->setDefaultFilter($_REQUEST);
		$selectedItems = $this->getSelectedItems();

		foreach ($this->arOriginalsItems as $item) {
			if (!$selectedItems[$item["PROPS"]["SECID"]]) {
				$this->arItems[] = $item;
			} else {
				$this->arItemsSelected[] = $item;
			}
		}
		$this->count = count($this->arItems);
	}

	//избранные элементы
	public function getSelectedItems() {
		$arData = array();
		if ($_COOKIE["id_arr_futures_cookie"]) {
			$tmp = json_decode($_COOKIE["id_arr_futures_cookie"]);
			foreach ($tmp as $item) {
				$t             = explode("...", $item);
				$arData[$t[0]] = $t[1];
			}
		}

		return $arData;
	}
}
?>
