<?

use Bitrix\Highloadblock as HL;

//Синхронизатор с биржей
class MoexSync
{
    private $hasFullData;
    private $code;
	 private $element_id;
	 private $elementBoardId;
	 private $elementBaseTicker;
    private $hlId;
    private $tableName;
	 private $element_iblock_id;
	 private $ActionsHLdataClass;
	 private $ObligationHLdataClass;

    public function __construct()
    {
        \Bitrix\Main\Loader::includeModule("highloadblock");
        \Bitrix\Main\Loader::includeModule("iblock");
		  $this->ActionsHLdataClass = $this->getHlDataClass(24);
		  $this->ObligationHLdataClass = $this->getHlDataClass(25);
		  $this->FuturesHLdataClass = $this->getHlDataClass(38);
		  //CModule::IncludeModule("iblock");
    }

    public function processAction($item, $code, $hasFullData = false)
    {
        $this->hasFullData = $hasFullData;
        $this->code = $code;
        $this->hlId = 24;
        $this->tableName = "hl_moex_actions_data";
		  $this->element_id = $item["ID"];
		  $this->element_iblock_id = $item["IBLOCK_ID"];
        //Полностью прогруженно
        if ($this->processItem() && !$hasFullData) {
            CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 73, "MOEX_LOADED");
        }
    }

    public function processObligation($item, $code, $hasFullData = false)
    {
        $this->hasFullData = $hasFullData;
        $this->code = $code;
        $this->hlId = 25;
        $this->tableName = "hl_moex_obligations_data";

        //Полностью прогруженно
        if ($this->processItem() && !$hasFullData) {
            CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 74, "MOEX_LOADED");
        }
    }

    public function processFutures($item, $code, $hasFullData = false)
    {
        $this->hasFullData = $hasFullData;
        $this->code = $code;
        $this->hlId = 38;
        $this->tableName = "hl_moex_futures_data";
		  $this->element_id = $item["ID"];
		  $this->elementBaseTicker = $item["PROPERTY_ASSETCODE_VALUE"];
		  $this->elementBoardId = $item["PROPERTY_BOARDID_VALUE"];
		  $this->element_iblock_id = $item["IBLOCK_ID"];
        //Полностью прогруженно
        if ($this->processFuturesItem() && !$hasFullData) {
            CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], 70, "MOEX_LOADED");
        }
    }

	 private function processFuturesItem(){
        global $DB;

        $interval = 24;

        if ($this->hasFullData) {
            $from = new DateTime("-2days");
        } else {
            $from = $this->getFromDateByCode($this->code);
        }

		  $dataClass = $this->FuturesHLdataClass;

			//Список режимов торгов со ссылками для запросов
/*			$arBoardsId = array(
			 "RFUD"=>"https://iss.moex.com/iss/engines/futures/markets/forts/boards/RFUD/securities.json?iss.only=securities", //ФОРТС
			 "FIQS"=>"https://iss.moex.com/iss/engines/futures/markets/fortsiqs/boards/FIQS/securities.json?iss.only=securities", //Фьючерсы IQS
			 );*/

        if ($this->elementBoardId == "RFUD") {
            $type = "forts";
        } elseif ($this->elementBoardId == "FIQS") {
            $type = "fortsiqs";
        }

        $baseUrl = $url = "https://iss.moex.com/iss/engines/futures/markets/" . $type . "/boards/" . $this->elementBoardId . "/securities/" . $this->code . "/candles.json?from=" . $from->format("Y-m-d") . "&till=" . date("Y-m-d") . "&interval=" . $interval;
        $offset = 0;

        $connection = \Bitrix\Main\Application::getConnection();
        while (true) {


            $chartData = ConnectMoex($baseUrl . "&start=" . $offset);


            //Если нет данных
            if (!$chartData["candles"]["data"]) {
                break;
            }


            $DB->StartTransaction();
				 foreach ($chartData["candles"]["data"] as $elem) {
                $date = new DateTime($elem[7]);
                $item = [
                    "UF_ITEM" => "'".$this->code."'",
                    "UF_BASE_TICKER" => "'".$this->elementBaseTicker."'",
                    "UF_DATE" => "'".$date->format("Y-m-d")."'",
                    "UF_OPEN" => "'".$elem[0]."'", //OPEN
                    "UF_CLOSE" => "'".$elem[1]."'", //CLOSE
                    "UF_HIGH" => "'".$elem[2]."'", //HIGH
                    "UF_LOW" => "'".$elem[3]."'", //LOW
                ];


                $res = $dataClass::getList([
                    "filter" => [
                        "UF_ITEM" => $this->code,
                        "UF_DATE" => $date->format("d.m.Y"),
                    ],
                    "select" => [
                        "ID",
                    ],
                ]);

                if ($hlItem = $res->fetch()) {
                    $params = [];
                    foreach ($item as $k => $v) {
                        $params[] = $k."=".$v;
                    }
                    $sql =  "UPDATE ".$this->tableName." SET ".implode(", ", $params)." WHERE id = ".$hlItem["ID"];
                    $connection->query($sql);
                    //$dataClass::update($hlItem["ID"], $item);
                } else {

                    $sql = "INSERT INTO ".$this->tableName." (".implode(", ", array_keys($item)).") VALUES (".implode(", ", array_values($item)).");";
                    $connection->query($sql);
                    //$dataClass::add($item);
                }
            }
            $DB->Commit();
            $offset += 500;

        }

/*        if ($this->hlId == 24) {
            $this->updateActionPrice($this->element_id, $this->element_iblock_id, $this->code); //Обновим обе цены из HL инфоблока
        } else {

        }*/


        return true;
	 }

    private function processItem()
    {
        global $DB;


        $interval = 24;

        if ($this->hasFullData) {
            $from = new DateTime("-2days");
        } else {
            $from = $this->getFromDateByCode($this->code);
        }
		  //	  $from = new DateTime('11.06.2021');
        //$dataClass = $this->getHlDataClass();

        if ($this->hlId == 24) {
            $type = "shares";
				$dataClass = $this->ActionsHLdataClass;
        } else {
            $type = "bonds";
				$dataClass = $this->ObligationHLdataClass;
        }

        $baseUrl = $url = "http://iss.moex.com/iss/engines/stock/markets/" . $type . "/securities/" . $this->code . "/candles.json?from=" . $from->format("Y-m-d") . "&till=" . date("Y-m-d") . "&interval=" . $interval;
        $offset = 0;

        $connection = \Bitrix\Main\Application::getConnection();
        while (true) {


            $chartData = ConnectMoex($baseUrl . "&start=" . $offset);


            //Если нет данных
            if (!$chartData["candles"]["data"]) {
                break;
            }


            $DB->StartTransaction();
				 foreach ($chartData["candles"]["data"] as $elem) {
                $date = new DateTime($elem[7]);
                $item = [
                    "UF_ITEM" => "'".$this->code."'",
                    "UF_DATE" => "'".$date->format("Y-m-d")."'",
                    "UF_OPEN" => "'".$elem[0]."'", //OPEN
                    "UF_CLOSE" => "'".$elem[1]."'", //CLOSE
                    "UF_HIGH" => "'".$elem[2]."'", //HIGH
                    "UF_LOW" => "'".$elem[3]."'", //LOW
                ];

					 //Получение Номинала и НКД для облигации на текущую дату итерации
					 if($type=='bonds'){
					 	//http://iss.moex.com/iss/engines/stock/markets/bonds/securities/RU000A0JWSQ7/securities.json?from=2011-01-01&till=2020-09-06&interval=24
					 	$url_bond = "http://iss.moex.com/iss/engines/stock/markets/bonds/securities/".$this->code."/securities.json?from=".$date->format("Y-m-d")."&till=".$date->format("Y-m-d")."&interval=24";
						$data_bond = ConnectMoex($url_bond);
						CLogger::bondsFacevalue_log(print_r($data_bond["securities"]["data"][0], true), '/var/www/bitrix/data/www/fin-plan.org/log/');
						//ACCRUEDINT - 7
						//FACEVALUE - 10
						 if(count($data_bond["securities"]["data"])>0){
						   $item["UF_ACCRUEDINT"] = $data_bond["securities"]["data"][0][7];
						   $item["UF_FACEVALUE"] = $data_bond["securities"]["data"][0][10];
						}
						unset($data_bond);
					 }


                $res = $dataClass::getList([
                    "filter" => [
                        "=UF_ITEM" => $this->code,
                        "=UF_DATE" => $date->format("d.m.Y"),
                    ],
                    "select" => [
                        "ID",
                    ],
                ]);

                if ($hlItem = $res->fetch()) {
                    $params = [];
                    foreach ($item as $k => $v) {
                        $params[] = $k."=".$v;
                    }
                    $sql =  "UPDATE ".$this->tableName." SET ".implode(", ", $params)." WHERE id = ".$hlItem["ID"];
                    $connection->query($sql);
                    //$dataClass::update($hlItem["ID"], $item);
                } else {

                    $sql = "INSERT INTO ".$this->tableName." (".implode(", ", array_keys($item)).") VALUES (".implode(", ", array_values($item)).");";
                    $connection->query($sql);
                    //$dataClass::add($item);
                }
            }
            $DB->Commit();
            $offset += 500;

        }

/*        if ($this->hlId == 24) {
            $this->updateActionPrice($this->element_id, $this->element_iblock_id, $this->code); //Обновим обе цены из HL инфоблока
        } else {

        }*/


        return true;
    }

	 //Обновляет цену закрытия для акции
	 public function updateActionPrice($ElementId, $IblockId, $code){
		//$dataClass = $this->getHlDataClass();
		$dataClass = $this->ActionsHLdataClass;
      $res = $dataClass::getList(array(
          "filter" => array(
              "=UF_ITEM" => $code,
          ),
			 "order" => array("UF_DATE"=>"DESC"),
			 "limit" => 1,
          "select" => array("ID", "UF_CLOSE", "UF_DATE"),
      )
		);

	 if ($hlItem = $res->fetch()) {

/*		define("LOG_FILENAME", "/var/www/bitrix/data/www/fin-plan.org/updateActionPrice_log.txt");
      //AddMessage2Log($el->LAST_ERROR);return true;
      AddMessage2Log("code=".$code." elId=".$ElementId." IblId=".$IblockId.print_r($hlItem,true),'');*/
		if(floatval($hlItem['UF_CLOSE'])>0){
		$hlItem['UF_CLOSE'] = floatval($hlItem['UF_CLOSE']);
		CIBlockElement::SetPropertyValues($ElementId, $IblockId, $hlItem['UF_CLOSE'], "LASTPRICE");
		CIBlockElement::SetPropertyValues($ElementId, $IblockId, $hlItem['UF_CLOSE'], "LEGALCLOSE");
		//CLogger::UpdateActionsPrice('ok ElementId='.$ElementId.' uf_close'.$hlItem['UF_CLOSE']);
		}
		} else {
			//CLogger::UpdateActionsPrice('err ElementId='.$ElementId.' uf_close'.$hlItem['UF_CLOSE']);
		}
	 }

	 //Обновляет цену закрытия для облигации, т.к. биржа поменяла ключи полей с ценами в запросе
	 //https://iss.moex.com/iss/engines/stock/markets/bonds/securities.json на PREVLEGALCLOSEPRICE и PREVPRICE
	 public function updateObligationPrice($ElementId, $IblockId, $code){
		//$dataClass = $this->getHlDataClass();
		$dataClass = $this->ObligationHLdataClass;
      $res = $dataClass::getList(array(
          "filter" => array(
              "=UF_ITEM" => $code,
          ),
			 "order" => array("UF_DATE"=>"DESC"),
			 "limit" => 1,
          "select" => array("ID", "UF_CLOSE", "UF_DATE"),
      )
		);

	 if ($hlItem = $res->fetch()) {
		if(floatval($hlItem['UF_CLOSE'])>0){
		$hlItem['UF_CLOSE'] = floatval($hlItem['UF_CLOSE']);
		CIBlockElement::SetPropertyValues($ElementId, $IblockId, $hlItem['UF_CLOSE'], "LASTPRICE");
		CIBlockElement::SetPropertyValues($ElementId, $IblockId, $hlItem['UF_CLOSE'], "LEGALCLOSE");
		}
		}
	 }

	 //Обновляет цены закрытия и открытия для фьючерса
	 public function updateFuturesPrice($ElementId, $IblockId, $code){
	 	//Т.к. для элемента фьючерса кроме текущей цены открытия/закрытия нужна еще и цена закрытия предыдущей сессии - то выбираем две строки с сортировкой по дате по убыванию
		//$dataClass = $this->getHlDataClass();
		$dataClass = $this->ActionsHLdataClass;
      $res = $dataClass::getList(array(
          "filter" => array(
              "=UF_ITEM" => $code,
          ),
			 "order" => array("UF_DATE"=>"DESC"),
			 "limit" => 2,
          "select" => array("ID", "UF_OPEN", "UF_CLOSE", "UF_DATE"),
      )
		);

	 $cnt = 0;
	 if ($hlItem = $res->fetch()) {

/*		define("LOG_FILENAME", "/var/www/bitrix/data/www/fin-plan.org/updateActionPrice_log.txt");
      //AddMessage2Log($el->LAST_ERROR);return true;
      AddMessage2Log("code=".$code." elId=".$ElementId." IblId=".$IblockId.print_r($hlItem,true),'');*/
		if($cnt==0){  //Обновляем цены открытия/закрытия текущей сессии
			$hlItem['UF_OPEN'] = floatval($hlItem['UF_OPEN']);
			$hlItem['UF_CLOSE'] = floatval($hlItem['UF_CLOSE']);
			CIBlockElement::SetPropertyValues($ElementId, $IblockId, $hlItem['UF_OPEN'], "PREVPRICE");
			CIBlockElement::SetPropertyValues($ElementId, $IblockId, $hlItem['UF_CLOSE'], "LASTSETTLEPRICE");
		} elseif($cnt==1) { //Обновляем цену закрытия предыдущей сессии
			$hlItem['UF_CLOSE'] = floatval($hlItem['UF_CLOSE']);
			CIBlockElement::SetPropertyValues($ElementId, $IblockId, $hlItem['UF_OPEN'], "PREVSETTLEPRICE");
		}

		 $cnt++;
		}

	 }

    private function getFromDateByCode($code)
    {
        global $APPLICATION;
        if ($this->hlId == 24) {
            $type = "shares";
        } elseif ($this->hlId == 25) {
            $type = "bonds";
        } elseif ($this->hlId == 38) {
            $type = "futures";
		        if ($this->elementBoardId == "RFUD") {
		            $type = "forts";
		        } elseif ($this->elementBoardId == "FIQS") {
		            $type = "fortsiqs";
		        }
        }

		  if($this->hlId == 38 ){ //Для определения начальной даты для запроса цен по фьючерсам отдельный запрос
			 $chartDataTime = ConnectMoex("http://iss.moex.com/iss/history/engines/futures/markets/". $type . "/boards/" . $this->elementBoardId ."/securities/" . $code . "/dates.json");
		  } else {
			 $chartDataTime = ConnectMoex("http://iss.moex.com/iss/history/engines/stock/markets/" . $type . "/securities/" . $code . "/dates.json");
		  }


        $from = new DateTime($chartDataTime["dates"]["data"][0][0]);
        $arChartData["real_from"] = $from;
        if ($from->getTimestamp() < $APPLICATION->minGraphDate) {
            $from = new DateTime("@" . $APPLICATION->minGraphDate);
        }

        return $from;
    }

    private function getHlDataClass($HlId = false)
    {
    	 if(intval($HlId)>0){
		  $id = $HlId;
    	 } else {
		  $id = $this->hlId;
    	 }
        $hlblock = HL\HighloadBlockTable::getById($id)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
}
