<?
use Bitrix\Highloadblock as HL;

//Акции
class Actions extends RadarBase {
	private $arItems = array();
	public $arOriginalsItems = array();
	public $count;
	private $perPage = 5;
	public $havePayAccess = false;
    public $classType = self::TYPE_ACTION;
	public $currencyRateUSD = 1;

	function __construct(){
	  	$this->setData();
		$this->havePayAccess = checkPayRadar();
	}

	//отдельный элемент по коду
	public function getItem($code){
		foreach($this->arOriginalsItems as $item){
			if($item["SECID"]==$code){
				return $item;
			}
		}

		return false;
	}
		//отдельный элемент по коду
	public function getItemByCode($code) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["PROPS"]["ISIN"] == $code) {
				return $item;
			}
		}
		return false;
	}
	//отдельный элемент по id
	public function getItemById($id){
		foreach($this->arOriginalsItems as $item){
			if($item["ID"]==$id){
				return $item;
			}
		}
		return false;
	}
    
    //отдельный элемент по id
    public function getItemBySecid($secid){
        foreach($this->arOriginalsItems as $item){
            if($item["PROPS"]["SECID"]==$secid){
                return $item;
            }
        }
        return false;
    }
	
	//акции с просадом менее 10%
	public function getLowProsad(){
		global $APPLICATION;
		$arReturn = array();
		
		foreach($this->arOriginalsItems as $item){
			if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["акции"])){
				continue;
			}
			if($item["DYNAM"]["Просад"]<10){
				$arReturn[] = array(
					$item["SECID"],
					$item["NAME"],
					$item["DYNAM"]["Просад"]
				);
			}
		}
		
		return $arReturn;
	}
	
	//акции с таргетом менее 10%
	public function getLowTarget(){
		global $APPLICATION;
		$arReturn = array();
		
		foreach($this->arOriginalsItems as $item){
			if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["акции"])){
				continue;
			}

			if($item["DYNAM"]["Таргет"]<10){
				$arReturn[] = array(
					$item["SECID"],
					$item["NAME"],
					$item["DYNAM"]["Таргет"]
				);
			}
		}

		return $arReturn;
	}

	private function setData(){
global $APPLICATION;
  $cbr = new CCurrency((new DateTime())->format("d.m.Y"));
  $this->currencyRateUSD = $cbr->getRate("USD", false);
  unset($cbr);

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "actions_data";
		$cacheTtl = 86400*7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arItems = $cache->get($cacheId);
		} else {
			$arItems = array();

			CModule::IncludeModule("iblock");
			CModule::IncludeModule("highloadblock");
			$ibID = 32;

			$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

			$res = $entityClass::getList(array(
				"filter" => array(
					"!UF_YEAR" => false,
				),
				"select" => array(
					"*"
				),
				"order" => array(
					"ID" => "DESC"
				)
			));
			while($item = $res->fetch()){
				$item["UF_DATA"] = json_decode($item["UF_DATA"], true);

				if(isset($item["UF_KVARTAL"])){
					$type = "KVARTAL";
					$t = $item["UF_KVARTAL"];
				} else {
					$type = "MONTH";
					$t = $item["UF_MONTH"];
				}

				$item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
				$item["UF_DATA"]["PERIOD_VAL"] = $t;
				$item["UF_DATA"]["PERIOD_TYPE"] = $type;

				$arPeriods[$item["UF_COMPANY"]][$t."-".$item["UF_YEAR"]."-".$type] = $item["UF_DATA"];
			}

			//Предыдущий день
			$prevDay = new DateTime();
			$prevDay->modify("-1 day");
			$arFilter = Array("IBLOCK_ID"=>$ibID);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
			while($ob = $res->GetNextElement())
			{
				$fields = $ob->GetFields();

				$item = array(
					"ID" => $fields["ID"],
					"NAME" => $fields["NAME"],
					"SECID" => $fields["CODE"],
					"CODE" => $fields["CODE"],
					//"URL" => $fields["DETAIL_PAGE_URL"],
				);

				//страница акции
				$arFilter = Array("IBLOCK_ID"=>33, "CODE"=>$fields["CODE"]);
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL"));
				if($val = $res2->GetNext())
				{
					$item["URL"] = $val["DETAIL_PAGE_URL"];
				}


				$props = $ob->GetProperties();

				foreach($props as $prop){
					if($prop["VALUE"] || $prop["VALUE"]==0 || $prop["CODE"]=="HIDEN"){
						if($prop["CODE"]=="PROP_TARGET"){
							$prop["VALUE"] = str_replace("%", "", $prop["VALUE"]);
						}
						$item["PROPS"][$prop["CODE"]] = $prop["VALUE"];
					}

				}

				$item["PROPS"]["PROP_PROSAD"] = str_replace(",", ".", $item["PROPS"]["PROP_PROSAD"]);
				$item["PROPS"]["PROP_TARGET"] = str_replace(",", ".", $item["PROPS"]["PROP_TARGET"]);

				//Убрать с режимами торгов
				if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["акции"]) || !$item["PROPS"]["BOARDID"]){
					continue;
				}

				//Убрать пифы
				if($item["PROPS"]["IS_PIF_ACTIVE"]=="Y"){
					continue;
				}

				//Убрать выведенные с биржи
/*				if(!empty($item["PROPS"]["HIDEN"])){
					continue;
				}*/
				//если нет LASTPRICE - берем LEGALCLOSE
				if($item["PROPS"]["LASTPRICE"]<=0 && $item["PROPS"]["LEGALCLOSE"]>0) {
					$item["PROPS"]["LASTPRICE"] = $item["PROPS"]["LEGALCLOSE"];
				}

				if($item["PROPS"]["EMITENT_ID"]){
					$comp = CIBlockElement::GetByID($item["PROPS"]["EMITENT_ID"])->GetNext();
					$item["COMPANY"] = array(
						"ID" => $comp["ID"],
						"NAME" => $comp["NAME"],
						"DATE_CREATE"=>$comp["DATE_CREATE"]
						//"URL" => $comp["DETAIL_PAGE_URL"],
					);
				  	$item["COMPANY"]["NEXT_REPORT_DATE"] = parent::getNextReportDate($comp["NAME"], 'REPORT_RUS');

					//страница компании
					$arFilter = Array("IBLOCK_ID"=>29, "NAME"=>$comp["NAME"]);

					$arCompanyPageSelect = array("DETAIL_PAGE_URL", "PROPERTY_VAL_ED_FIRST", "PROPERTY_CAPITAL_KOEF", "PROPERTY_CHILD_COMPANY_RUS", "PROPERTY_CHILD_COMPANY_USA");
					$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, $arCompanyPageSelect);
					if($val = $res2->GetNext())
					{
						$item["COMPANY"]["URL"] = $val["DETAIL_PAGE_URL"];
                        $item["COMPANY"]["LINKED_COMPANY_RUS"] = $val["PROPERTY_CHILD_COMPANY_RUS_VALUE"];
                        $item["COMPANY"]["LINKED_COMPANY_USA"] = $val["PROPERTY_CHILD_COMPANY_USA_VALUE"];
                        $item["COMPANY"]["VAL_ED_FIRST"] = $val["PROPERTY_VAL_ED_FIRST_VALUE"];
						//если данные в долларах, перевести капитализацию
						if(strpos($val["PROPERTY_VAL_ED_FIRST_VALUE"], "долл")!==false && $item["PROPS"]["ISSUECAPITALIZATION"]){
							$cbr = new CCurrency($prevDay->format("d.m.Y"));
							//$item["PROPS"]["ISSUECAPITALIZATION_DOL"] = $item["PROPS"]["ISSUECAPITALIZATION"]/getCBPrice("USD", $prevDay->format("d/m/Y"));
							$item["PROPS"]["ISSUECAPITALIZATION_DOL"] = $item["PROPS"]["ISSUECAPITALIZATION"]/$cbr->getRate("USD",false);
							unset($cbr);
						}

						if($val["PROPERTY_CAPITAL_KOEF_VALUE"]){
							$item["COMPANY"]["CAPITAL_KOEF"] = $val["PROPERTY_CAPITAL_KOEF_VALUE"];
						}
					}


				}
				if($item["PROPS"]["EMITENT_ID"] && $arPeriods[$item["PROPS"]["EMITENT_ID"]]){
					$item["PERIODS"] = $arPeriods[$item["PROPS"]["EMITENT_ID"]];
					$item["PERIODS"] = parent::calculatePeriodData($item);

					//Определим последние периоды для квартальной отчетности и по месяцам
					$item["LAST_PERIOD_KVARTAL"] = '';
					$item["LAST_PERIOD_MONTH"] = '';
					reset($item["PERIODS"]);
					foreach($item["PERIODS"] as $period=>$value){
                        if($value["CURRENT_PERIOD"]==true) continue; //Исключаем текущий период
						if(empty($item["LAST_PERIOD_KVARTAL"]) && strpos($period,"KVARTAL")!==false){
							$item["LAST_PERIOD_KVARTAL"] = $period;
						}
						if(empty($item["LAST_PERIOD_MONTH"]) && strpos($period,"MONTH")!==false){
							$item["LAST_PERIOD_MONTH"] = $period;
						}
						if(!empty($item["LAST_PERIOD_KVARTAL"]) && !empty($item["LAST_PERIOD_MONTH"])){
							break;
						}
					}


				}

			  	$item["DYNAM"] = self::setDynamParams($item);

				$arItems[] = $item;
			}

			$cache->set($cacheId, $arItems);
		}
		self::setHaveGS($arItems);
	 	$this->arOriginalsItems = $arItems;
		$this->count = count($arItems);
	}

	//таблица вывода
	public function getTable($page=1){
		$this->setFilter();
		$this->sortItems();

		$end = $page*$this->perPage;
		$start = $end-$this->perPage;

		$arItems = array();

		if($page==1){
			$arItems = $this->arItemsSelected;
		}

		for($i=$start;$i<$end;$i++){
			if($this->arItems[$i]){
				$arItems[] = $this->arItems[$i];
			}
		}
		
		$shows = $this->perPage*$page+count($this->arItemsSelected);
		if($shows>$this->count+count($this->arItemsSelected)){
			$shows = $this->count;
		}

		return array(
			"total_items" => $this->count+count($this->arItemsSelected),
			"cur_page" => $page,
			"shows" => $shows,
			"count_page" => ceil($this->count/$this->perPage),
			"items" => $arItems,
			"access" => $this->havePayAccess,
			//"selected" => $this->getSelectedItems()
		);
	}
	
	//сортировка элементов
	private function sortItems(){
	    $negArr = [];
	    $posArr = [];
	    foreach ($this->arItems as $item) {
	        if($item["DYNAM"]["PE"] <= 0 || !$item["DYNAM"]["PE"]) {
                $negArr[] = $item;
            } else {
	            $posArr[] = $item;
            }

        }
        usort($posArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
                return 0;
            }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? 1 : -1;
        });

        usort($negArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
                return 0;
            }
            if(!$a["DYNAM"]["PE"]) {
                return -1;
            }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? -1 : 1;
        });

        $this->arItems = array_merge($posArr, $negArr);
	}

	//избранные элементы
	public function getSelectedItems(){
		$arData = array();
		if($_COOKIE["id_arr_shares_cookie"]){
			$tmp = json_decode($_COOKIE["id_arr_shares_cookie"]);
			foreach($tmp as $item){
				$t = explode("...", $item);
				$arData[$t[0]] = $t[1];
			}
		}

		return $arData;
	}

	//фильтр для элементов
	private function setFilter(){
		$r = $this->setDefaultFilter($_REQUEST);
		$selectedItems = $this->getSelectedItems();

/*if($r["no_dividends"]!="y") {
$filename = $_SERVER["DOCUMENT_ROOT"].'/yesdivs.csv';
$data = 'SECID;DIV2018;DYNAM_NODIVS;DIV_PRC'. PHP_EOL;
file_put_contents($filename, $data);
}*/

	  if($r["index_candidate"] && $r["index_candidate"]!='default'){
	  	$CIndexes = new CIndexes;
		$CandidateIndex = $CIndexes->getIndexCandidates($r["index_candidate"]);
	  }

		foreach($this->arOriginalsItems as $item){
			if(!$selectedItems[$item["PROPS"]["SECID"]]){
				if(!empty($item["PROPS"]["HIDEN"])) continue;
				//Отрасль
				if($r["industry"]){
					$items = array();
					foreach($r["industry"] as $v){
						if(!$v){
							continue;
						}
						$items[] = $v;
					}

					if($items){
						if(!in_array($item["PROPS"]["PROP_SEKTOR"], $items) || !$item["PROPS"]["PROP_SEKTOR"]){
							continue;
						}
					}
				}

				//Входят в индексы
				  if($r["index_candidate"] && $r["index_candidate"]!='default'){
					if(!in_array($item["ID"],$CandidateIndex)){
						continue;
					}
				  }

				if($r["in_index"] && $r["in_index"]!="default"){
					if(strpos($item["PROPS"]["IN_INDEXES"],$r["in_index"])===false){
						continue;
					}
				}


				//Возможность маржинальных сделок
				if($r["margin_deals"]=="short"){
					if(!$item["PROPS"]["PROP_SHORT"]){
						continue;
					}
				}
				if($r["margin_deals"]=="kredit"){
					if(!$item["PROPS"]["PROP_KREDIT"]){
						continue;
					}
				}

				//Выберите коэффициент р/е акции
				if ($r["pe"] && $r["pe"] !== 'all') {
					if($r["pe"] && $r["pe"]<=25){
						if(!$item["DYNAM"]["PE"] || $item["DYNAM"]["PE"]>$r["pe"]){
							continue;
						}
					if ($item["DYNAM"]["PE"] <0) {
						continue;
					}
					}
				}

				//Доходность по консенсус-прогнозу
				if($r["profitability"] && $r["profitability"]<=50){
					if(!$item["DYNAM"]["Таргет"] || $item["DYNAM"]["Таргет"]<$r["profitability"]){
						continue;
					}
				}

				//Убирать из выдачи акции, по которым нет оборотов за последнюю неделю
				if($r["turnover_week"]){
					if($item["PROPS"]["VALTODAY"]<=0){
						continue;
					}
				}

				//Нет дивидендов
 				if($r["no_dividends"]=='y'){
				  if($r["no_dividends"]=="y" && floatval($item["DYNAM"]["Дивиденды %"])>0){
				  		continue;
				  }
				}
				 else {

				//Дивиденды более
				if($r["dividends"]){
					$r["dividends"] = str_replace("%", "", $r["dividends"]);
					$r["dividends"] = str_replace(",", ".", $r["dividends"]);
					if(floatval($r["dividends"])){
						if(isset($r["future_dividends"]) && $r["future_dividends"]=="y"){//Проверяем галку учета будущих дивидендов
						 //Дивиденды более (только будущие)
								if(!$item["DYNAM"]["Будущие дивиденды %"] || $item["DYNAM"]["Будущие дивиденды %"]<$r["dividends"]){
									continue;
								}
						}else{  //Дивиденды более (Без учета будущих)
							if(floatval($item["DYNAM"]["Дивиденды %"])==0 || floatval($item["DYNAM"]["Дивиденды %"])<floatval($r["dividends"])){
								continue;
							}
						}
					}
				}

				}

				//Капитализация более
				if($r["capitalization"] && $r["capitalization"]!="all"){
					if($item["PROPS"]["ISSUECAPITALIZATION"]<$r["capitalization"]){
						continue;
					}
				}

				//Цена менее
				if($r["lotprice"] && $r["lotprice"]!="all"){
					$arLotPrice = explode("-",$r["lotprice"]);
					$curlotPrice = floatval($item["PROPS"]["LASTPRICE"])*intval($item["PROPS"]["LOTSIZE"]);

					if($curlotPrice<=floatval($arLotPrice[1])){
					} else {
							continue;
					}
				}
                
                // учитывать вхождение в портфель ГС США
                if ($r["into_gs_portfolio"]) {
                    if ($item["DYNAM"]["HAVE_GS"] !== "Y" ) {
                        continue;
                    }
                }
				//P/Equity filter
                if($this->havePayAccess){
                    if($r["p-e"] && $r["p-e"] !== 'all'){
                        if(!$item["DYNAM"]["P/Equity"]){
                            continue;
                        }
                        if($r["p-e"] === '1-2' && $item["DYNAM"]["P/Equity"] > 0.5){
                            continue;
                        }
                        if($r["p-e"] === '1' && $item["DYNAM"]["P/Equity"] > 1){
                            continue;
                        }
                        if($r["p-e"] === '3-2' && $item["DYNAM"]["P/Equity"] > 1.5){
                            continue;
                        }
                        if($r["p-e"] === '2' && $item["DYNAM"]["P/Equity"] > 2){
                            continue;
                        }
                    }
                }

					 //Betta filter
                if($this->havePayAccess){
                    if($r["betta"] && $r["betta"] !== 'all'){
                        if(!$item["PROPS"]["BETTA"]){
                            continue;
                        }
                        if($r["betta"] === '-999>-1' && $item["PROPS"]["BETTA"] > -1){
                            continue;
                        }
                        if($r["betta"] === '-1>0' && ($item["PROPS"]["BETTA"] <= -1 || $item["PROPS"]["BETTA"]>0)){
                            continue;
                        }
                        if($r["betta"] === '0>0.5' && ($item["PROPS"]["BETTA"] <= 0 || $item["PROPS"]["BETTA"]>0.5)){
                            continue;
                        }
                        if($r["betta"] === '0.5>1' && ($item["PROPS"]["BETTA"] <= 0.5 || $item["PROPS"]["BETTA"]>1)){
                            continue;
                        }
                        if($r["betta"] === '1>999' && $item["PROPS"]["BETTA"] <= 1){
                            continue;
                        }
                        if($r["betta"] === '-9991>1' && $item["PROPS"]["BETTA"]>=1){
                            continue;
                        }
                    }
                }
					 //Рейтинг радар filter
                if($this->havePayAccess){
                    if($r["radar-rating"] && $r["radar-rating"] !== 'all'){
                        if(!$item["PROPS"]["PRC_OF_USERS"]){
                            continue;
                        }

								if($r["radar-rating"] === '0-1' && $item["PROPS"]["PRC_OF_USERS"] > 1){
                            continue;
                        }
								if($r["radar-rating"] === '0-5' && $item["PROPS"]["PRC_OF_USERS"] > 5){
                            continue;
                        }
								if($r["radar-rating"] === '0-10' && $item["PROPS"]["PRC_OF_USERS"] > 10){
                            continue;
                        }
								if($r["radar-rating"] === '0-20' && $item["PROPS"]["PRC_OF_USERS"] > 20){
                            continue;
                        }
								if($r["radar-rating"] === '0-30' && $item["PROPS"]["PRC_OF_USERS"] > 30){
                            continue;
                        }
								if($r["radar-rating"] === '1-100' && $item["PROPS"]["PRC_OF_USERS"] < 1){
                            continue;
                        }
								if($r["radar-rating"] === '5-100' && $item["PROPS"]["PRC_OF_USERS"] < 5){
                            continue;
                        }
								if($r["radar-rating"] === '10-100' && $item["PROPS"]["PRC_OF_USERS"] < 10){
                            continue;
                        }
								if($r["radar-rating"] === '20-100' && $item["PROPS"]["PRC_OF_USERS"] < 20){
                            continue;
                        }
								if($r["radar-rating"] === '30-100' && $item["PROPS"]["PRC_OF_USERS"] < 30){
                            continue;
                        }
                    }
                }

				//Доля экспорта
				if($this->havePayAccess){
					if($r["export_share"]){
					  if(intval($r["export_share"])>0){
					  	if(floatval($item["PROPS"]["PROP_EXPORT_SHARE"])<intval($r["export_share"])){
					  		continue;
					  	}
					  } else if(intval($r["export_share"])<0){
					  	if(floatval($item["PROPS"]["PROP_EXPORT_SHARE"])>0){
					  		continue;
					  	}
					  }
					}
				}


				//PEG filter
                if($this->havePayAccess){
                    if($r["peg"] && $r["peg"] !== 'all'){
                        if(!$item["DYNAM"]["PEG"]){
                            continue;
                        }
                        if($r["peg"] === '1-2' && $item["DYNAM"]["PEG"] > 0.5){
                            continue;
                        }
                        if($r["peg"] === '1' && $item["DYNAM"]["PEG"] > 1){
                            continue;
                        }
                        if($r["peg"] === '3-2' && $item["DYNAM"]["PEG"] > 1.5){
                            continue;
                        }
                        if($r["peg"] === '2' && $item["DYNAM"]["PEG"] > 2){
                            continue;
                        }
                    }
                }

                //рост за месяц
                if($this->havePayAccess){
                    if($r["month-increase"] && $r["month-increase"] !== 'all'){
                        if(!$item["DYNAM"]["MONTH_INCREASE"]){
                            continue;
                        }
                        if($r["month-increase"] === 'down50' && $item["DYNAM"]["MONTH_INCREASE"] > -50){
                            continue;
                        }
                        if($r["month-increase"] === 'down25' && $item["DYNAM"]["MONTH_INCREASE"] > -25){
                            continue;
                        }
                        if($r["month-increase"] === 'down10' && $item["DYNAM"]["MONTH_INCREASE"] > -10){
                            continue;
                        }
                        if($r["month-increase"] === 'down' && $item["DYNAM"]["MONTH_INCREASE"] > 0){
                            continue;
                        }
                        if($r["month-increase"] === 'up' && $item["DYNAM"]["MONTH_INCREASE"] < 0){
                            continue;
                        }
                        if($r["month-increase"] === 'up10' && $item["DYNAM"]["MONTH_INCREASE"] < 10){
                            continue;
                        }
                        if($r["month-increase"] === 'up25' && $item["DYNAM"]["MONTH_INCREASE"] < 25){
                            continue;
                        }
                        if($r["month-increase"] === 'up50' && $item["DYNAM"]["MONTH_INCREASE"] < 50){
                            continue;
                        }
                    }
                    if($r["year-increase"] && $r["year-increase"] !== 'all'){
                        if(!$item["DYNAM"]["YEAR_INCREASE"]){
                            continue;
                        }
                        if($r["year-increase"] === 'down50' && $item["DYNAM"]["YEAR_INCREASE"] > -50){
                            continue;
                        }
                        if($r["year-increase"] === 'down25' && $item["DYNAM"]["YEAR_INCREASE"] > -25){
                            continue;
                        }
                        if($r["year-increase"] === 'down10' && $item["DYNAM"]["YEAR_INCREASE"] > -10){
                            continue;
                        }
                        if($r["year-increase"] === 'down' && $item["DYNAM"]["YEAR_INCREASE"] > 0){
                            continue;
                        }
                        if($r["year-increase"] === 'up' && $item["DYNAM"]["YEAR_INCREASE"] < 0){
                            continue;
                        }
                        if($r["year-increase"] === 'up10' && $item["DYNAM"]["YEAR_INCREASE"] < 10){
                            continue;
                        }
                        if($r["year-increase"] === 'up25' && $item["DYNAM"]["YEAR_INCREASE"] < 25){
                            continue;
                        }
                        if($r["year-increase"] === 'up50' && $item["DYNAM"]["YEAR_INCREASE"] < 50){
                            continue;
                        }
                    }
                    if($r["three-year-increase"] && $r["three-year-increase"] !== 'all'){
                        if(!$item["DYNAM"]["THREE_YEAR_INCREASE"]){
                            continue;
                        }
                        if($r["three-year-increase"] === 'down50' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -50){
                            continue;
                        }
                        if($r["three-year-increase"] === 'down25' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -25){
                            continue;
                        }
                        if($r["three-year-increase"] === 'down10' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -10){
                            continue;
                        }
                        if($r["three-year-increase"] === 'down' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > 0){
                            continue;
                        }
                        if($r["three-year-increase"] === 'up' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 0){
                            continue;
                        }
                        if($r["three-year-increase"] === 'up10' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 10){
                            continue;
                        }
                        if($r["three-year-increase"] === 'up25' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 25){
                            continue;
                        }
                        if($r["three-year-increase"] === 'up50' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 50){
                            continue;
                        }
                    }
                }

				//ОТЧЕТНОСТЬ период
				if($this->havePayAccess){
					if($r["period"]){
						if($item["PROPS"]["PROP_VID_AKTSIY"]=="корпоративные"){
							$show = true;
							
							foreach($r["period"] as $name=>$vals){
								if(!$vals["use"]){
									continue;
								}
								
								$vals["percent"] = trim(str_replace("%", "", $vals["percent"]));

								//Если фильтрация по последнему существующему у актива периоду то заменяем значение last на обозначенный период из кеша данных по активу $item["LAST_PERIOD_KVARTAL"]
								if($r["period_value"]=="last"){ //Сравниваем период для каждого актива по его последнему периоду
								  $item_period = str_replace("-KVARTAL", "", $item["LAST_PERIOD_KVARTAL"]);
									if($item["PERIODS"][$item_period."-KVARTAL"][$name] <= $vals["percent"]){
										$show = false;
										break;
									}
								} else
									if($item["PERIODS"][$r["period_value"]."-KVARTAL"][$name] <= $vals["percent"]){
										$show = false;
										break;
									}
							}
							
							if(!$show){
								continue;
							}
						}
					}
				}
			
				//ОТЧЕТНОСТЬ месяц
				if($this->havePayAccess){
					if($r["month"]){
						if($item["PROPS"]["PROP_VID_AKTSIY"]=="банковские"){
							$show = true;

							//Если фильтрация по последнему существующему у актива периоду то заменяем значение last на обозначенный период из кеша данных по активу $item["LAST_PERIOD_MONTH"]
							if($r["month_value"]=="last"){
							  $r["month_value"] = str_replace("-MONTH", "", $item["LAST_PERIOD_MONTH"]);
							}

							$r["month_value"] = explode("-", $r["month_value"]);
							$r["month_value"][1] = mb_substr($r["month_value"][1], -2);
							$r["month_value"] = implode("-", $r["month_value"]);

							
							foreach($r["month"] as $name=>$vals){
								if(!$vals["use"]){
									continue;
								}

								if($vals["top"]){
									if($item["PERIODS"][$r["month_value"]."-MONTH"][$name] > $vals["top"] || !$item["PERIODS"][$r["month_value"]."-MONTH"][$name]){
										$show = false;
									}
								} else {
									$vals["percent"] = trim(str_replace("%", "", $vals["percent"]));
									if($name=="Доля просрочки, %"){
										if($item["PERIODS"][$r["month_value"]."-MONTH"][$name] > $vals["percent"] || !$item["PERIODS"][$r["month_value"]."-MONTH"][$name]){
											$show = false;
										}
									} else {
										if($item["PERIODS"][$r["month_value"]."-MONTH"][$name] <= $vals["percent"]){
											$show = false;
										}
									}
								}
							}
							
							if(!$show){
								continue;
							}
						}
					}
				}
				
				$this->arItems[] = $item;
			} else {
				$this->arItemsSelected[] = $item;
			}
		}
		$this->count = count($this->arItems);
	}

    /**
     * Возвращает список ISIN кодов для фильтра списка акций
     * @param $arFilterData
     * @return array
     */
	public function setFilterForList($arFilterData){
	    $listSelectedItems = array();

        foreach($this->arOriginalsItems as $item){
            unset($item["PERIODS"]);
           switch ($arFilterData[0]){
               case "all":
                   $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Погашение"=>$item["PROPS"]["SECID"]);
               break;
               case "dividends":
                   if(floatval($item["DYNAM"]["Дивиденды %"])==0) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Дивиденды"=>$item["DYNAM"]["Дивиденды %"]);
               break;
               case "export_share":
                   if(floatval($item["PROPS"]["PROP_EXPORT_SHARE"])<=0) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Погашение"=>$item["PROPS"]["PROP_EXPORT_SHARE"]);
                   break;
               case "industry":
                   if(!in_array($item["PROPS"]["PROP_SEKTOR"], $arFilterData[1]) || !$item["PROPS"]["PROP_SEKTOR"])	continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Погашение"=>$item["PROPS"]["MATDATE"]);
               break;
               case "margin_deals":
					   if($arFilterData[1]=="short"){
                   if(!$item["PROPS"]["PROP_SHORT"]) continue;
						 }
					   if($arFilterData[1]=="kredit"){
                   if(!$item["PROPS"]["PROP_KREDIT"]) continue;
						 }
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Дивиденды"=>$item["DYNAM"]["Дивиденды %"]);
               break;

					case "lotprice":
					//Цена менее
					if($arFilterData[1] && $arFilterData[1]!="all"){
						$arLotPrice = explode("-",$arFilterData[1]);
						$curlotPrice = floatval($item["PROPS"]["LASTPRICE"])*intval($item["PROPS"]["LOTSIZE"]);

						if($curlotPrice<=floatval($arLotPrice[1])){
						} else {
								continue;
						}
						$listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "Дивиденды"=>$item["DYNAM"]["Дивиденды %"]);
					}

					break;




           }

        }
        return $listSelectedItems;
    }

	
	//Фильтр по умолчанию
	private function setDefaultFilter($r){
		global $APPLICATION;
		if(!isset($r["pe"])){
			$r["pe"] = $APPLICATION->obligationDefaultForm["pe"];
		}
		if(!isset($r["profitability"])){
			$r["profitability"] = $APPLICATION->obligationDefaultForm["profitability"];
		}
		if(!isset($r["turnover_week"])){
			$r["turnover_week"] = "y";
		}

		return $r;
	}

	/**
	 * Дополняет массив акций по ссылке данными о наличии аналитики ГС для каждой бумаги
	 *
	 * @param  array   $arItems ссылка на массив акций после основных расчетов
	 *
	 * @access private
	 */
	private function setHaveGS(&$arItems) {
        Global $APPLICATION;
		if (class_exists('CGs')) {
		    $CGs = new CGs();
			 $arIsin = array();
			 foreach($arItems as $k=>$v){
				if(!in_array($v["CODE"], $arIsin)){
					$arIsin[] = $v["CODE"];
				}
			 }
			 $arHaveGs = $CGs->checkGsIsin($arIsin);
            $arActivesInPortfolio = $CGs->getActivesInPortfolio($APPLICATION->modelPortfolioRusId);
				foreach($arItems as $k=>$v){
					if(in_array($v["CODE"], $arHaveGs)){
						$arItems[$k]["DYNAM"]["HAVE_GS"] = 'Y';
					} else {
						$arItems[$k]["DYNAM"]["HAVE_GS"] = 'N';
					}
                    if(in_array($v["CODE"], $arActivesInPortfolio)){
                        $arItems[$k]["DYNAM"]["INTO_GS_PORTFOLIO"] = 'Y';
                    } else {
                        $arItems[$k]["DYNAM"]["INTO_GS_PORTFOLIO"] = 'N';
                    }
				 }
		}
	}

	private function setDynamParams($item){
		$return = array();



		//new Просад = ( 1 - old Просад / LASTPRICE ( LEGALCLOSE) ) х 100
		if($item["PROPS"]["PROP_PROSAD"] && $item["PROPS"]["LASTPRICE"]){
			$return["Просад"] =  round((1 - $item["PROPS"]["PROP_PROSAD"]/$item["PROPS"]["LASTPRICE"])*100, 2);
		}

		  if($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_DAY"]) {
            $return["DAY_INCREASE"] = round($item["PROPS"]["ONE_DAY_DROP_INCREASE"], 2);
        }
		  if($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_MONTH"]) {
            $return["MONTH_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_MONTH"]) - 1) * 100), 2);
        }
        if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_ONE_YEAR"]) {
            $return["YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_ONE_YEAR"]) - 1) * 100), 2);
        }
        if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_THREE_YEAR"]) {
            $return["THREE_YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_THREE_YEAR"]) - 1) * 100), 2);
        }

		//new Target = ( old Target / LASTPRICE ( LEGALCLOSE) - 1 ) x 100
		if($item["PROPS"]["PROP_TARGET"] && $item["PROPS"]["LASTPRICE"]){
			$return["Таргет"] =  round(($item["PROPS"]["PROP_TARGET"]/$item["PROPS"]["LASTPRICE"]-1)*100, 2);
		}
		
		//Цена лота
		if($item["PROPS"]["LOTSIZE"] && $item["PROPS"]["LASTPRICE"]){
			$return["Цена лота"] = $item["PROPS"]["LOTSIZE"]*$item["PROPS"]["LASTPRICE"];
		}
		
		/*
		1) если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
		2) если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
		3) если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
		4) если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
		*/
		//Прибыль
		if($item["PERIODS"]){
			$tmp = sort_nested_arrays($item["PERIODS"], array('PERIOD_YEAR' => 'desc', 'PERIOD_VAL' => 'desc'));
			//$lastItem = $tmp[0];
            //Получаем реальный текущий период без синтетического
            foreach($tmp as $periodTmp){
                if($periodTmp["CURRENT_PERIOD"]==false){
                    $lastItem = $periodTmp;
                    break;
                }
            }
            
			$curKvartal = intval((date('n')-1)/4)+1;
			$availYear = date("Y")-1;

			try {
			$lastDate = DateTime::createFromFormat('m.Y', $lastItem["PERIOD_VAL"].".".$lastItem["PERIOD_YEAR"]);
			$lastAvail = DateTime::createFromFormat('m.Y', $curKvartal.".".$availYear);

			if($lastDate && $lastAvail){
			if($lastDate->format("U") >= $lastAvail->format("U")){
				
				$year = DateTime::createFromFormat('Y', $lastItem["PERIOD_YEAR"]);
				$prevYear = clone $year;
				$prevYear->modify("-1year");
				
				if ($lastItem["PERIOD_VAL"]==1){
					//если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
					if(
						isset($item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["1-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] =
							$item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["1-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==2){
					//если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
					if(
						isset($item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["2-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] =
							$item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["2-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==3){
					//если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
					if(
						isset($item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["3-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] =
							$item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["3-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==4){
					//если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
					if(
						isset($item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] =
							$item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				}
			}
			}

			} catch (Exception $e) {
				?><pre><?print_r($lastItem)?></pre><?
				exit();
			}
		}
		
		//РЕ
		if($return["Прибыль"] && ($item["PROPS"]["ISSUECAPITALIZATION"] || $item["PROPS"]["ISSUECAPITALIZATION_DOL"])){
			$val = $item["PROPS"]["ISSUECAPITALIZATION"];
			if(!empty($item["PROPS"]["ISSUECAPITALIZATION_DOL"])){
				$val = $item["PROPS"]["ISSUECAPITALIZATION_DOL"];
			}

			$return["PE"] = round($val/$return["Прибыль"]/1000000, 1);
			if($lastItem["Активы"])
			  $return["PB"] = round($val/$lastItem["Активы"]/1000000, 2);
            if($lastItem["Выручка за год (скользящая)"])
                $return["P/Sale"] = round($val/$lastItem["Выручка за год (скользящая)"]/1000000, 2);
            $return["P/Equity"] = round($val/$return["Собственный капитал"]/1000000, 1);
            if($return["AVEREGE_PROFIT"]>0) { //Если Темп прироста прибыли больше нуля - считаем PEG иначе не считаем его т.к. деление на ноль будет
				$return["PEG"] = round($return["PE"] / $return["AVEREGE_PROFIT"], 1);
			}
		}
		//Дивиденды %
		if(!empty($item["PROPS"]["PROP_DIVIDENDY_2018"]) && !empty($item["PROPS"]["LASTPRICE"])){
			$return["Дивиденды %"] = round($item["PROPS"]["PROP_DIVIDENDY_2018"]/$item["PROPS"]["LASTPRICE"]*100, 2);
			if(strpos($item["PROPS"]["PROP_ISTOCHNIK"],"$")!==false){//Если расчет в валюте
			$cbr = new CCurrency((new DateTime())->format("d.m.Y"));
			$currencyRateUSD = $cbr->getRate("USD", false);
			//$currencyRateUSD = getCBPrice("USD", (new DateTime())->format("d/m/Y"));
			$return["Дивиденды %"] = round($item["PROPS"]["PROP_DIVIDENDY_2018"]*$currencyRateUSD/$item["PROPS"]["LASTPRICE"]*100, 2);
			unset($cbr);
			}
		} else if(floatval($item["PROPS"]["PROP_DIVIDENDY_2018"])>0 && empty($item["PROPS"]["LASTPRICE"]) ){
			$return["Дивиденды %"] = 0.1;
		}

		//Отсутствие дивидендов
		  $return["Нет дивидендов"] = floatval($return["Дивиденды %"])>0?"N":"Y";
		  //TODO Считать как "нет дивидендов" если есть дивы за год но нет процента из-за пустой цены?
		  if(floatval($return["Дивиденды %"])==0){
			 $return["Нет дивидендов"] = "Y";
		  }
		  unset($res,$divItem);


		//Будущие дивиденды
		if(!empty($item["PROPS"]["FUTURE_DIVIDENDS"]) && !empty($item["PROPS"]["FUTURE_DIVIDENDS_PRC"])){
			$return["Будущие дивиденды %"] = round($item["PROPS"]["FUTURE_DIVIDENDS_PRC"], 2);
			$return["Будущие дивиденды"] = round($item["PROPS"]["FUTURE_DIVIDENDS"], 2);
		} else {
			$return["Будущие дивиденды %"] = 0;
			$return["Будущие дивиденды"] = 0;
		}

		//Доля в индексах
		 $return["Доля в индексах"] = self::setShareInTheIndex($item["PROPS"]["ISIN"]);

		//Кандидат в индексы



		return $return;
	}

	//Расчитывает доли в индексах, возвращает массив
	private function setShareInTheIndex($isinCode){
		if(empty($isinCode)) return array();//Если не передан код - возвращаем пустой массив
		$arReturn = array();
		$hlblock   = HL\HighloadBlockTable::getById(34)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();
		$resHL = $entityClass::getList(array(
				"filter" => array("UF_SHARE_CODE"=>$isinCode),
				"select" => array(
					"ID",
					"UF_INDEX",
					"UF_SHARE_CODE",
					"UF_SHARE_OF_INDEX",
					"UF_FREEFLOAT",
				),
			));
			while($item = $resHL->fetch()){
			 $arItemsInIndex[$item["UF_INDEX"]] = $item;
			}
	  //Получим данные по индексам если они присутствуют для акции и сформируем результирующий массив для вставки в динамическую секцию
	  if(count($arItemsInIndex)>0){
		 $arSelect = Array("ID","CODE", "NAME", "DETAIL_PAGE_URL", "IBLOCK_ID");
       $arFilter = Array("IBLOCK_ID"=>IntVal(53), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "CODE"=> array_keys($arItemsInIndex));
       $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelect);
       while($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
		  $arReturn[$arFields["CODE"]] = array("NAME"=>$arFields["NAME"], "DETAIL_PAGE_URL"=>$arFields["DETAIL_PAGE_URL"],
		  													"SHARE_OF_INDEX"=>$arItemsInIndex[$arFields["CODE"]]["UF_SHARE_OF_INDEX"],
		  													"FREEFLOAT"=>$arItemsInIndex[$arFields["CODE"]]["UF_FREEFLOAT"]
															);
       }
	  }

	  return $arReturn;
	}

	//список отраслей
	//deprecated
	static function getIndustryList(){
		$return = array();

/*		$cacheLifetime = 86400;
		$cacheID = 'ActionsIndystryList';
		$cachePath = '/'.$cacheID;

		$obCache = new CPHPCache();
		if( $obCache->InitCache($cacheLifetime, $cacheID, $cachePath) )// Если кэш валиден
		{
			$return = $obCache->GetVars();// Извлечение переменных из кэша
		}
		elseif( $obCache->StartDataCache()  )// Если кэш невалиден
		{
			$ibID = 32;
			$arFilter = Array("IBLOCK_ID"=>$ibID, "!PROPERTY_PROP_SEKTOR"=>false);
			$res = CIBlockElement::GetList(Array(), $arFilter, array("PROPERTY_PROP_SEKTOR"), false);
			while($val = $res->GetNext())
			{
				$return[] = $val["PROPERTY_PROP_SEKTOR_VALUE"];
			}

		   $obCache->EndDataCache($return);// Сохраняем переменные в кэш.
		}*/
		$return = self::getRadarFiltersValues()["PROP_SEKTOR"]; //Обратная совместимость
		return $return;
	}

	//Возвращает уникальные значения для фильтра по акциям США
	public function getRadarFiltersValues(){
		global $APPLICATION;
	   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "actions_rus_filters_data";
		$cacheTtl = 86400*7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arFilterEnums = $cache->get($cacheId);
		} else {
				CModule::IncludeModule("iblock");
			   $arFilterEnums = array("PROP_SEKTOR"=>array(), "ETF_INDUSTRY"=>array(), "ETF_EXCHANGE"=>array(), "ETF_COUNTRY"=>array(), "IN_INDEXES"=>array());

			  //Получим массив названий индексов с ключами по их коду
			  $arIndexNames = array();
			  $arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE");
			  $arFilter = Array("IBLOCK_ID"=>53, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			  $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			  while($ob = $res->fetch()){
				 $arIndexNames[$ob["CODE"]] = $ob["NAME"];
			  }


			  foreach($arFilterEnums as $k=>$v){
				 $rsResult = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 32, 'PROPERTY_HIDEN'=>false, '!PROPERTY_'.$k.'_VALUE' => false,'!NAME'=>false ),array('PROPERTY_'.$k));
				 while($arResult=$rsResult->Fetch()){
				   if(!empty($arResult['PROPERTY_'.$k.'_VALUE'])){
				    if(strpos($arResult['PROPERTY_'.$k.'_VALUE'],"/")!==false){
						$val = explode("/",$arResult['PROPERTY_'.$k.'_VALUE']);
						foreach($val as $param_val){
							$val = trim($param_val);

						  if($k == "IN_INDEXES"){
							$val = $arIndexNames[$param_val];
						  }
						  $arFilterEnums[$k][$param_val]=$val;
						}
				    } else {
				    	if($k == "IN_INDEXES"){
							$val = $arIndexNames[$param_val];
						  } else {
							$val = $arResult['PROPERTY_'.$k.'_VALUE'];
						  }

				      $arFilterEnums[$k][$val]=$val;
					 }

					}
			    }
			  }
			unset($arIndexNames, $rsResult);
		   $cache->set($cacheId, $arFilterEnums);
		}
		return $arFilterEnums;
	}




}