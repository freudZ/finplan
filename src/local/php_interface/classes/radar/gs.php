<?

/**
 * Класс для работы с аналитикой ГС
 *
 * @author    Александр Еремин <alphaprogrammer@gmail.com>
 */
class CGs
{
    private $hl_id = 50;
    private $db_table = 'hl_gs_analytics';
    private $date_format = 'd.m.Y';
    private $db;
    
    public function __construct()
    {
        global $DB;
        $this->db = $DB;
    }
    
    
    /**
     * Возвращает список событий с группировкой по isin из таблицы аналитики ГС
     *
     * @param string|array $isin     код isin или массив кодов isin
     * @param array        $arFields список полей таблицы для выборки. Если не указано будут выбраны все поля
     * @param integer      $limit    ограничение кол-ва выбранных записей (отличное от нуля только для детальных страниц с подгрузкой аякс)
     * @param integer      $offset   смещение выборки записей (отличное от нуля только для детальных страниц с подгрузкой аякс в сочетании с ненулевым $limit)
     *
     * @return array  массив событий для активов ГС с ключом по isin и событиями во вложенном массиве
     *
     * @access public
     */
    public function getList($isin = '', $arFields = array(), $limit = 0, $offset = 0)
    {
        if (empty($isin)) {
            return array();
        }
        return $this->getListCached($isin, $arFields, $limit, $offset);
    }
    
    public function getListCached($isin = '', $arFields = array(), $limit = 0, $offset = 0)
    {
        if (empty($isin)) {
            return array();
        }
        $cacheId = "gs_analitycs" . md5(serialize($isin) . serialize($arFields) . $limit . $offset);
        $cache_dir = "/tagged_gs_analitycs";
        $cacheTime = 0;//86400*7;
        $obCache = new CPHPCache;
        if ($obCache->InitCache($cacheTime, $cacheId, $cache_dir)) {
            $arResult = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);  //Старт тегирования
            $arResult = array();
            $arSelect = '*';
            if (count($arFields) > 0) {
                if (!in_array('UF_ISIN')) {
                    $arFields[] = 'UF_ISIN';
                }
                array_walk($arFields, function (&$x) {
                    $x = "`$x`";
                });
                $arSelect = implode(', ', $arFields);
            }
            
            if (!is_array($isin)) {
                $isin = array($isin);
            }
            array_walk($isin, function (&$x) {
                $x = "'$x'";
            });
            $isin = implode(', ', $isin);
            
            $query = "SELECT $arSelect FROM `$this->db_table` ";
            
            $query .= " WHERE `UF_ISIN` IN ( $isin )";
            $query .= " ORDER BY `UF_DATE` DESC, `UF_SORT` ASC";
            if ($limit > 0) {
                $query .= " LIMIT $limit";
            }
            if ($offset > 0) {
                $query .= " OFFSET $offset";
            }
            
            $res = $this->db->Query($query);
            
            while ($row = $res->fetch()) {
                
                
                if (!empty($row['UF_FILE'])) {
                    $row['UF_FILE'] = CFile::GetFileArray($row['UF_FILE']);
                    /*  ID - идентификатор файла.
                        TIMESTAMP_X - дата загрузки.
                        MODULE_ID - идентификатор модуля загрузившего файл.
                        HEIGHT - для картинок высота.
                        WIDTH - для картинок ширина.
                        FILE_SIZE - размер в байтах.
                        CONTENT_TYPE - тип содержимого, выдаётся mime-type.
                        SUBDIR - поддиректория внутри папки UPLOAD.
                        FILE_NAME - имя файла после преобразования и убирания некорректных символов. Если стоит опция в настройках главного модуля Сохранять исходные имена загружаемых файлов, то фактически не будет отличаться от ORIGINAL_NAME (Будет приведен в безопасный вид при включённой опции Автоматически заменять невалидные символы в именах загружаемых файлов).
                        ORIGINAL_NAME - оригинальное имя файла во время загрузки.
                        DESCRIPTION - описание.
                        SRC - относительный путь относительно DOCUMENT_ROOT.
                        EXTERNAL_ID - внешний идентификатор файла.*/
                    
                }
                if (!empty($row['UF_DATE'])) {
                    $row['UF_DATE'] = (new DateTime($row['UF_DATE']))->format($this->date_format);
                }
                $arResult[$row['UF_ISIN']][] = $row;
            }
            
            $CACHE_MANAGER->RegisterTag("analitycs_gs"); //Отметка тегом
            
            $CACHE_MANAGER->EndTagCache(); //Финализация тегирования
            
            $obCache->EndDataCache($arResult);
        }
        
        return $arResult;
    }
    
    public function getGsItemFromRadar($isin = '', $type = 'RUS')
    {
        $radarItem = array();
        $arResult = array();
        $arResult["ADDICTIONAL"] = array();
        
        if ($type == 'RUS') {
            $resA = new Actions();
            $radarItem = $resA->getItemByCode($isin);
        } else {
            if ($type == 'USA') {
                $resAU = new ActionsUsa();
                $radarItem = $resAU->getItemByCode($isin);
            }
        }
        
        $arResult["ADDICTIONAL"]["SECID"] = $radarItem['PROPS']['SECID'];
        $arResult["ADDICTIONAL"]["ISIN"] = $radarItem['PROPS']['ISIN'];
        $arResult["ADDICTIONAL"]["COUNTRY"] = $type;
        
        $arTableColumns = array(
          "NAME"                                 => "",
          "P/E"                                  => "",
          "P/Sale"                               => "",
          "Рентабельность собственного капитала" => "",
          "Доля собственного капитала в активах" => "",
          "Темп прироста выручки"                => "",
          "Темп роста прибыли"                   => "",
          "Таргет"                               => "",
          "Просад"                               => "",
          "GROW_HORIZON_PROGNOSE"                => ""
        );
        if (count($radarItem) > 0) {
            $arLastPeriod = reset($radarItem['PERIODS']);
            
            if (count($arLastPeriod) > 0) {
                foreach ($arTableColumns as $tk => $tv) {
                    $arResult["COMPANY"][$tk] = round($arLastPeriod[$tk], 3);
                }
                $arResult["COMPANY"]["GROW_HORIZON_PROGNOSE"] = $radarItem['PROPS']['GROW_HORIZON_PROGNOSE'];
                $arResult["COMPANY"]["Таргет"] = $radarItem['DYNAM']['Таргет'];
                $arResult["COMPANY"]["Просад"] = $radarItem['DYNAM']['Просад'];
                $arResult["COMPANY"]["NAME"] = $radarItem["NAME"];
            }
            
            if (isset($radarItem['PROPS']['EMITENT_ID']) && !empty($radarItem['PROPS']['EMITENT_ID'])) {
                if ($type == 'RUS') {
                    $resIndRus = new CIndustriesRus();
                    $arIndRus = $resIndRus->GetSectorByEmitentId($radarItem['PROPS']['EMITENT_ID']);
                    $arIndRus = $resIndRus->getItem($arIndRus['CODE'], true);
                    
                    $arLastPeriod = reset($arIndRus['PERIODS']);
                    $arResult["INDUSTRY"] = array();
                    if (count($arLastPeriod) > 0) {
                        foreach ($arTableColumns as $tk => $tv) {
                            if (array_key_exists($tk, $arLastPeriod)) {
                                $val = round($arLastPeriod[$tk], 3);
                            } else {
                                $val = '';
                            }
                            $arResult["INDUSTRY"][$tk] = $val;
                        }
                        $arResult["INDUSTRY"]["NAME"] = $arIndRus["NAME"];
                    }
                    
                    
                } else {
                    if (isset($radarItem['PROPS']['SECTOR']) && !empty($radarItem['PROPS']['SECTOR'])) {
                        $resSectUsa = new SectorsUsa();
                        
                        $arSectUsa = $resSectUsa->GetSectorByName($radarItem['PROPS']['SECTOR']);
                        $arSectUsa = $resSectUsa->getItem($arSectUsa['CODE']);
                        
                        $arLastPeriod = reset($arSectUsa['PERIODS']);
                        $arResult["INDUSTRY"] = array();
                        if (count($arLastPeriod) > 0) {
                            foreach ($arTableColumns as $tk => $tv) {
                                if (array_key_exists($tk, $arLastPeriod)) {
                                    $val = round($arLastPeriod[$tk], 3);
                                } else {
                                    $val = '';
                                }
                                $arResult["INDUSTRY"][$tk] = $val;
                            }
                            $arResult["INDUSTRY"]["NAME"] = $arSectUsa["NAME"];
                        }
                        
                    }
                }
            }
        }
        return $arResult;
    }
    
    
    /**
     * Возвращает список isin кодов активов, входящих в модельные портфели
     *
     * @param array $pid массив с id модельных портфелей, например отсюда: $APPLICATION->modelPortfolioRusId
     *
     * @return array   массив isin кодов активов, входящих в указанные в $pid портфели
     *
     * @access public
     */
    public function getActivesInPortfolio($pid)
    {
        $arResult = array();
        global $DB;
        $strPid = implode(",", $pid);
        if (empty($strPid)) {
            return $arResult();
        }
    
        $obCache = new CPHPCache;
        $life_time = 3600;
        $cache_id = "actions_with_analitycs_in_portfolio_" . $strPid;
        if ($obCache->StartDataCache($life_time, $cache_id, "/cache_dir")) {
        
            $sql = "SELECT DISTINCT `UF_ACTIVE_CODE`, `UF_HISTORY_DIRECTION`, `UF_HISTORY_CNT` FROM `portfolio_history` WHERE `UF_PORTFOLIO` in ($strPid) AND `UF_ACTIVE_TYPE` in ('share', 'share_usa')";
            $res = $DB->Query($sql);
            while ($row = $res->fetch()) {
                if ($row["UF_HISTORY_DIRECTION"] == 'PLUS') {
                    $arResult[$row["UF_ACTIVE_CODE"]] = $arResult[$row["UF_ACTIVE_CODE"]] + $row["UF_HISTORY_CNT"];
                } elseif ($row["UF_HISTORY_DIRECTION"] == 'MINUS') {
                    $arResult[$row["UF_ACTIVE_CODE"]] = $arResult[$row["UF_ACTIVE_CODE"]] - $row["UF_HISTORY_CNT"];
                }
            }
            $arTmp = array();
            foreach ($arResult as $k => $v) {
                if (intval($v) > 0) {
                    $arTmp[] = $k;
                }
            }
            $arResult = $arTmp;
            //Кешируем переменную (можно несколько)
            $obCache->EndDataCache(array(
              "arResult" => $arResult
            ));
        } else {
            // Если у нас был кеш до достаем закешированные переменные
            extract($obCache->GetVars());
        }
        return $arResult;
    }
    
    /**
     * Проверяет наличие isin в таблице аналитики ГС, вызывается из списка радара, портфелей или со страниц акций
     *
     * @param array $arIsin массив isin кодов для проверки
     *
     * @return array  Массив с выборкой уникальных найденных isin в таблице аналитики ГС
     *
     * @access public
     */
    public function checkGsIsin($arIsin)
    {
        if (empty($arIsin)) {
            return array();
        }
        $arResult = array();
        
        if (!is_array($arIsin)) {
            $arIsin = array($arIsin);
        }
        array_walk($arIsin, function (&$x) {
            $x = "'$x'";
        });
        $arIsin = implode(', ', $arIsin);
        
        $query = "SELECT DISTINCT `UF_ISIN` FROM `$this->db_table` ";
        $query .= " WHERE `UF_ISIN` IN ( $arIsin )";
        
        $res = $this->db->Query($query);
        
        while ($row = $res->fetch()) {
            $arResult[] = $row['UF_ISIN'];
        }
        return $arResult;
    }
    
    /**
     * Возвращает все уникальные isin коды для которых есть аналитика ГС
     *
     * @param array  $arIsin  массив isin кодов для проверки
     *
     * @param string $country код страны для которой надо получить список
     *
     * @return array  Массив с выборкой уникальных найденных isin в таблице аналитики ГС
     *
     * @access public
     */
    public function getAllGsIsins($arIsin = array(), $country = '')
    {
        
        $arResult = array();
        $isinWhere = '';
        $countryWhere = '';
        if (!is_array($arIsin)) {
            $arIsin = array($arIsin);
        }
        array_walk($arIsin, function (&$x) {
            $x = "'$x'";
        });
        $arIsin = implode(', ', $arIsin);
        
        $query = "SELECT DISTINCT `UF_ISIN` FROM `$this->db_table` ";
        if (!empty($arIsin)) {
            $isinWhere = " `UF_ISIN` IN ( $arIsin )";
        }
        if (!empty($country)) {
            $countryWhere = " `UF_COUNTRY` = '$country'";
        }
        
        if (!empty($isinWhere) || !empty($countryWhere)) {
            $query .= " WHERE ";
            if (!empty($isinWhere)) {
                $query .= $isinWhere;
            }
            if (!empty($countryWhere)) {
                $query .= (!empty($isinWhere) ? ' AND ' : '') . $countryWhere;
            }
        }
        
        $res = $this->db->Query($query);
        
        while ($row = $res->fetch()) {
            $arResult[] = $row['UF_ISIN'];
        }
        return $arResult;
    }
    
}

?>




