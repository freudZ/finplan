<?

use Bitrix\Highloadblock as HL;
use \Bitrix\Main\Application;

//Графики с биржи
class SpbexGraph
{
    private $hlId;
    private $filter;
    private $dataClasses = [];
    private $hlTable;
    private $from;
    private $interval;
    private $period;
    private $periodForFinPokGraph;   //Принудетильно выставляется для получения ежемесячных цен для графика финпоков
    private $cacheTag;
    
    
    public function __construct()
    {
        
        \Bitrix\Main\Loader::includeModule("highloadblock");
        global $APPLICATION;
        $this->hlId = $APPLICATION->usaPricesHlId;
        if ($this->hlId == 29) {
            $this->hlTable = "hl_spb_actions_data";
        } else {
            if ($this->hlId == 53) {
                $this->hlTable = "hl_polygon_actions_data";
            }
        }
        
    }
    
    public function getForAction($secID, $clearCache = false)
    {
        $this->filter = [
          "UF_ITEM" => $secID
        ];
        $this->cacheTag = "usa_actions_candle";
        return $this->getForItem();
    }
    
    //Возвращает вырезанный диапазон цен начиная с $date для графиков компаний
    public function getForActionByMonthWithFrom($code, $date, $clearCache = false, $forApi = false)
    {
        $this->from = $date;
        $this->interval = 31;
        $this->filter = ["UF_ITEM" => $code];
        $this->periodForFinPokGraph = 'M'; //Принудетильно выставляется период для получения ежемесячных цен для графика финпоков
        $res = $this->getForItem();
        $items = json_decode($res["items"], true);
        
        $arTmpItems = array();
        $arTranslateParams = array("replace_space" => "", "replace_other" => "");
        foreach ($items as $n => $item) {
            if (strpos($item[0], "-") !== false) {
                CLogger::ErrorUsaPrice(print_r(array("UF_ITEM" => $code, "item" => $item), true));
                continue;
            }
            $dt = DateTime::createFromFormat('m/d/Y 00:00:00', $item[0]);
            if ($dt->getTimestamp() < (new DateTime($this->from))->getTimestamp()) {
                continue;
            }
            //После обновления битрикс месяц стал по символу M выводиться с маленькой буквы, из-за чего не рисуются желтые столбики на графиках.
            //принудительно приводим первый символ строки в верхний регистр. mb_ucfirst лежит в init.php
            $date = FormatDate("M. Y", $dt->getTimestamp());
            $date = mb_ucfirst($date);
            if ($forApi) {
                $arTmpItems[$n] = array(
                  "key"          => Cutil::translit($date, "ru", $arTranslateParams),
                  "value"        => $item[4],
                  "formatedDate" => $dt->format('Y-m-d'),
                  "date"         => $dt->getTimestamp()
                );
            } else {
                $arTmpItems[$n] = [$date, $item[4],];
            }
        }
        $items = $arTmpItems;
        
        return $items;
    }
    
    private function getForItem($cacheTime = 0)
    {
        $arChartData = array();
        $arDatesAndTicks = $this->getDatesAndTicks();
        
        
        /*                $from = $arDatesAndTicks["real_from"];
                        $to = new DateTime($arDatesAndTicks["max"]);
                        $diff = $from->diff($to);
        
                             $daysDiff = intval($diff->days);*/
        
        
        //тики через сколько дней
        /*                $interval = 31;
        
                              if($dayDiff<=31){
                                  $this->period = "D";
                                  $arReturn["tick"] = 1 . ' days';
                                  $arReturn["interval"] = 1;
                                  $arReturn["period"] = 'D';
        
                             } else if($dayDiff>31 && $dayDiff<=366){
                                  $this->period = "W";
                                  $arReturn["tick"] = 14 . ' days';
                                  $arReturn["interval"] = 7;
                                  $arReturn["period"] = 'W';
                             } else {
                                 $this->period = "M";
                                  $arReturn["tick"] = floor($dayDiff/7/12) . ' weeks';
                                  $arReturn["interval"] = 31;
                                  $arReturn["period"] = 'M';
        
                             }*/
        
        
        if (array_key_exists("UF_BASE_TICKER",
          $this->filter)) { //Если получаем данные для графика фьючерсного контракта со склейкой по UF_BASE_TICKER
            $cacheId = 'item' . $this->filter["UF_BASE_TICKER"] . "-" . $this->from . "-" . $arDatesAndTicks["interval"];
        } else { //Для всех остальных графиков
            $cacheId = 'item' . $this->filter["UF_ITEM"] . "-" . $this->from . "-" . $arDatesAndTicks["interval"];
        }
        /*			echo "<pre  style='color:black; font-size:11px;'>";
                    print_r($cacheId);
                    echo "</pre>";*/
        $obCache = new CPHPCache();
        $cacheTtl = 86400;
        //$cache->clean("usa_actions_data");
        $cachePath = '/graph_new_hiload_id_' . $this->hlId . '/';
        $CacheId = 'usa_graphdata';
        if ($obCache->InitCache($cacheTime, $CacheId, $cachePath)) {
            $arChartData = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            global $CACHE_MANAGER;
            $cache_dir = "/usa_graphdata";
            $CACHE_MANAGER->StartTagCache($cache_dir);  //Старт тегирования
            $this->calculateCandleHL(); //Создание графика
            
            $arChartData["diff"] = $arDatesAndTicks["diff"];
            $arChartData["period"] = $arDatesAndTicks["period"];
            $arChartData["tick"] = $arDatesAndTicks["tick"];
            $arChartData["min"] = (new DateTime($arDatesAndTicks["min"]))->format('m-d-Y');
            $arChartData["max"] = (new DateTime($arDatesAndTicks["max"]))->format('m-d-Y');
            $arChartData["real_from"] = $arDatesAndTicks["real_from"];
            $arChartData["items"] = json_encode($this->getCandleData());
            
            
            $CACHE_MANAGER->RegisterTag("usa_graphdata"); //Отметка тегом
            $CACHE_MANAGER->EndTagCache(); //Финализация тегирования
            
            
            $obCache->EndDataCache($arChartData);
        }
        
        
        return $arChartData;
    }
    
    /**
     * Получает рассчитанные данные для свечей  в зависимости от периода. Если период день - то данные берет из таблицы котировок
     *
     * @return array массив с данными свечей
     *
     * @access private
     */
    private function getCandleData()
    {
        global $DB;
        $arResult = array();
        $code = $this->filter["UF_ITEM"];
        if ($this->period != 'D') {
            $hlCandleTableName = "hl_candle_graph_data_usa";
            $hlTickerField = "UF_TICKER";
            $hlOrderField = "UF_DATE_TO";
            $queryPeriod = " AND `UF_PERIOD`='$this->period'";
        } else {
            $hlCandleTableName = $this->hlTable;
            $hlTickerField = "UF_ITEM";
            $hlOrderField = "UF_DATE";
            $queryPeriod = "";
        }
        
        $query = "SELECT * FROM `$hlCandleTableName` WHERE `$hlTickerField`='$code' $queryPeriod ORDER BY `$hlOrderField` ASC";
        
        $res = $DB->Query($query);
        while ($row = $res->fetch()) {
            $arResult[] = [
              (new DateTime($row[$hlOrderField]))->format("m/d/Y H:i:s"),
              floatval($row["UF_OPEN"]),
              floatval($row["UF_HIGH"]),
              floatval($row["UF_LOW"]),
              floatval($row["UF_CLOSE"]),
            ];
        }
        return $arResult;
    }
    
    /**
     * Возвращает минимальную и максимальную даты для построения графика, так же тики в неделях
     *
     * @param string $ticker тикер или isin бумаге
     *
     * @return array  результат в массиве
     *
     * @access private
     */
    private function getDatesAndTicks()
    {
        global $DB;
        $arReturn = array();

//CLogger::getForItem(print_r(array("ITEM"=>$this->filter["UF_ITEM"]), true));
        if (empty($this->filter["UF_ITEM"])) {
            return $arReturn;
        }
        $tableName = 'hl_candle_graph_data_usa';
        $hlTableName = $this->hlTable;
        $period = 'W';
        $ticker = $this->filter["UF_ITEM"];
        //$query = "";
        $query = "SELECT MIN(`UF_DATE`) AS `DATE_MIN`, MIN(`UF_DATE`) AS `DATE_FROM`, MAX(`UF_DATE`) AS `DATE_MAX`, floor(DATEDIFF(MAX(`UF_DATE`), MIN(`UF_DATE`))) AS DAYS FROM `$hlTableName` WHERE `UF_ITEM` = '$ticker'";
        //CLogger::slowSql($query);
        //if($this->hlTable=="hl_polygon_actions_data") return $arReturn;
        //$query = "SELECT MIN(`UF_DATE_TO`) AS `DATE_MIN`, MIN(`UF_DATE_FROM`) AS `DATE_FROM`, MAX(`UF_DATE_TO`) AS `DATE_MAX`, floor(DATEDIFF(MAX(`UF_DATE_TO`), MIN(`UF_DATE_FROM`))) AS DAYS FROM `$tableName` WHERE `UF_TICKER` = '$ticker' AND `UF_PERIOD` = '$period'";
        //$query = "SELECT MIN(`UF_DATE_TO`) AS `DATE_MIN`, MIN(`UF_DATE_FROM`) AS `DATE_FROM`, MAX(`UF_DATE_TO`) AS `DATE_MAX` FROM `$tableName` WHERE `UF_TICKER` = '$ticker' AND `UF_PERIOD` = '$period'";
        $res = $DB->Query($query);
        if ($row = $res->fetch()) {
            $arReturn = array(
              "real_from" => new DateTime($row["DATE_FROM"]),
              //"tick"=>$row["DAYS"],
              "min"       => $row["DATE_MIN"],
              "max"       => $row["DATE_MAX"],
            );
            $dayDiff = intval($row["DAYS"]);
            if ($dayDiff <= 31) {
                $this->period = "D";
                $arReturn["tick"] = 1 . ' days';
                $arReturn["interval"] = 1;
                $arReturn["period"] = 'D';
                
            } else {
                if ($dayDiff > 31 && $dayDiff <= 366) {
                    $this->period = "W";
                    $arReturn["tick"] = 14 . ' days';
                    $arReturn["interval"] = 7;
                    $arReturn["period"] = 'W';
                } else {
                    $this->period = "M";
                    $arReturn["tick"] = floor($dayDiff / 7 / 12) . ' weeks';
                    $arReturn["interval"] = 31;
                    $arReturn["period"] = 'M';
                    
                }
            }
            
            if ($this->periodForFinPokGraph == 'M') { //Принудетильно выставляется период для получения ежемесячных цен для графика финпоков
                $this->period = "M";
                $arReturn["tick"] = floor($dayDiff / 7 / 12) . ' weeks';
                $arReturn["interval"] = 31;
                $arReturn["period"] = 'M';
            }
            
            $arReturn["diff"] = $dayDiff;
        }
        unset($query, $res, $row, $tableName, $period);
        return $arReturn;
    }
    
    /**
     * Расчитывает и записывает в БД данные для свечей по активам Сша
     *
     * @access private
     */
    private function calculateCandleHL()
    {
        global $DB;
        $code = $this->filter["UF_ITEM"];
        $hlCandleTableName = "hl_candle_graph_data_usa";
        $hlTableName = $this->hlTable;
        //$hlTableName = "hl_spb_actions_data";
        //Получим последние даты расчитанных свечей (по последним записям месячного диапазона) для всех акций
        $querySelectAllCandles = "SELECT `ID`, `UF_TICKER`, `UF_PERIOD`, `UF_DATE_FROM`, `UF_DATE_TO` FROM `$hlCandleTableName` WHERE `UF_CURRENT_PERIOD`='Y' ORDER BY 'UF_DATE_TO' DESC";
        $arAllCandles = array();
        $res = $DB->Query($querySelectAllCandles);
        
        //дата для месяцев
        $dateMonthNow = (new DateTime())->format('m.Y');
        //дата для недель
        $dateWeekNow = (new DateTime())->format('W.m.Y'); //Номер недели, месяц, год
        
        
        while ($row = $res->fetch()) {
            
            //Проверим переход на новые текущие периоды и в случае перехода не добавляем в выборку к активу устаревший текущий период
            //Далее если какого-то из периодов нет - то по активу будут стерты все свечи и посчитаны новые
            //дата для месяцев
            $dateMonth = (new DateTime($row["UF_DATE_TO"]))->format('m.Y');
            //дата для недель
            $dateWeek = (new DateTime($row["UF_DATE_TO"]))->format('W.m.Y'); //Номер недели, месяц, год
            if ($dateMonth != $dateMonthNow || $dateWeek != $dateWeekNow) {
            
            } else {
                $arAllCandles[$row["UF_TICKER"]][$row["UF_PERIOD"]] = $row;
            }
            
            
        }
        
        //Если в выборке свечей нет записи с текущим периодом - то на всякий случай затираем данные по текущему активу полностью и перезаписываем всю историю вместе с текущим периодом
        //Эта ситуация так же возникает, если была удалена запись для текущего месяца.
        if (!array_key_exists('M', $arAllCandles[$code]) || !array_key_exists('W', $arAllCandles[$code])) {
            $queryClear = "DELETE FROM `$hlCandleTableName` WHERE `UF_TICKER` = '$code'";
            $DB->StartTransaction();
            $DB->Query($queryClear);
            $DB->Commit();
            $queryClear = '';
            $arAllCandles = array();
        }
        
        $query = "SELECT * FROM `$hlTableName` WHERE `UF_ITEM` = '$code'";
        $hasDateFilter = false;
        if (array_key_exists($code, $arAllCandles)) {
            if (!empty($arAllCandles[$code]['M']['UF_DATE_FROM']) && !empty($arAllCandles[$code]['W']['UF_DATE_FROM'])) {
                $query .= " AND `UF_DATE`>='" . (new DateTime($arAllCandles[$code]['M']['UF_DATE_FROM']))->format('Y-m-d') . "'";
                $hasDateFilter = true;
            }
        }
        $query .= " ORDER BY `UF_DATE` ASC";
        $res = $DB->Query($query);
        
        $arMonths = array();
        $arWeeks = array();
        $arPrices = array();
        while ($row = $res->fetch()) {
            $UF_CURRENT_PERIOD_MONTH = (new DateTime($row["UF_DATE"]))->getTimestamp() >= (new DateTime())->modify('first day of this month')->getTimestamp() ? 'Y' : '';
            
            //дата для месяцев
            $dateMonth = (new DateTime($row["UF_DATE"]))->format('m.Y');
            
            //Расчет свечей для диапазона "месяц"
            //Если начинаем новый месяц
            if (!array_key_exists($dateMonth, $arMonths)) {
                $arMonths[$dateMonth]['UF_TICKER'] = $code;
                $arMonths[$dateMonth]['UF_DATE_FROM'] = $row["UF_DATE"];
                $arMonths[$dateMonth]['UF_DATE_TO'] = $row["UF_DATE"];
                $arMonths[$dateMonth]['UF_PERIOD'] = "M"; //Диапазон - месяц
                $arMonths[$dateMonth]['UF_CURRENT_PERIOD'] = $UF_CURRENT_PERIOD_MONTH;
                $arMonths[$dateMonth]['UF_OPEN'] = $row["UF_OPEN"];
                $arMonths[$dateMonth]['UF_HIGH'] = $row["UF_HIGH"];
                $arMonths[$dateMonth]['UF_LOW'] = $row["UF_LOW"];
                $arMonths[$dateMonth]['UF_CLOSE'] = $row["UF_CLOSE"];
            } else { //Если считаем для существующего месяца
                $arMonths[$dateMonth]['UF_DATE_TO'] = $row["UF_DATE"];
                if ($arMonths[$dateMonth]['UF_HIGH'] < $row["UF_HIGH"]) {
                    $arMonths[$dateMonth]['UF_HIGH'] = $row["UF_HIGH"];
                }
                if ($arMonths[$dateMonth]['UF_LOW'] > $row["UF_LOW"]) {
                    $arMonths[$dateMonth]['UF_LOW'] = $row["UF_LOW"];
                }
                $arMonths[$dateMonth]['UF_CLOSE'] = $row["UF_CLOSE"];
            }
            
            
        } //while расчета свечей
        $this->savePeriodsToDB($hlCandleTableName, $arAllCandles, $code, $arMonths, "M");
        
        
        //Считаем для недель
        $query = "SELECT * FROM `$hlTableName` WHERE `UF_ITEM` = '$code'";
        $hasDateFilter = false;
        if (array_key_exists($code, $arAllCandles)) {
            if (!empty($arAllCandles[$code]['W']['UF_DATE_FROM'])) {
                $query .= " AND `UF_DATE`>='" . (new DateTime($arAllCandles[$code]['W']['UF_DATE_FROM']))->format('Y-m-d') . "'";
                $hasDateFilter = true;
            }
        }
        $query .= " ORDER BY `UF_DATE` ASC";
        $res = $DB->Query($query);
        
        $arMonths = array();
        $arWeeks = array();
        $arPrices = array();
        while ($row = $res->fetch()) {
            $arPrices[] = $row;
        }
        foreach ($arPrices as $k => $row) {
            
            $UF_CURRENT_PERIOD_WEEK = (new DateTime($row["UF_DATE"]))->getTimestamp() >= (new DateTime())->modify('monday this week')->getTimestamp() ? 'Y' : '';
            //дата для недель
            $dateWeek = (new DateTime($row["UF_DATE"]))->format('W.m.Y'); //Номер недели, месяц, год
            
            //$UF_CURRENT_PERIOD_WEEK =  (new DateTime($row["UF_DATE"]))->format('W.m.Y') == (new DateTime())->format('W.m.Y');
            
            //Расчет свечей для диапазона "неделя"
            //Если начинаем новый месяц
            $add = false;
            if ($hasDateFilter && $UF_CURRENT_PERIOD_WEEK == 'Y' && !$uf_last_item_price) {
                $add = true;
            } else {
                if ($hasDateFilter && $UF_CURRENT_PERIOD_WEEK != 'Y') {
                    $add = false;
                } else {
                    if (!$hasDateFilter) {
                        $add = true;
                    }
                }
            }
            
            // if($add){
            if (!array_key_exists($dateWeek, $arWeeks)) {
                if ($k == count($arPrices) - 1) {
                    $UF_CURRENT_PERIOD_WEEK = 'Y';
                }
                $arWeeks[$dateWeek]['UF_TICKER'] = $code;
                $arWeeks[$dateWeek]['UF_DATE_FROM'] = $row["UF_DATE"];
                $arWeeks[$dateWeek]['UF_DATE_TO'] = $row["UF_DATE"];
                $arWeeks[$dateWeek]['UF_PERIOD'] = "W"; //Диапазон - неделя
                $arWeeks[$dateWeek]['UF_CURRENT_PERIOD'] = $UF_CURRENT_PERIOD_WEEK;
                $arWeeks[$dateWeek]['UF_OPEN'] = $row["UF_OPEN"];
                $arWeeks[$dateWeek]['UF_HIGH'] = $row["UF_HIGH"];
                $arWeeks[$dateWeek]['UF_LOW'] = $row["UF_LOW"];
                $arWeeks[$dateWeek]['UF_CLOSE'] = $row["UF_CLOSE"];
            } else { //Если считаем для существующей недели
                // $firstDayWeekFromDate = (new DateTime($row["UF_DATE"]))->modify('monday this week')->format('Y-m-d');
                if ($k == count($arPrices) - 1) {
                    $arWeeks[$dateWeek]['UF_CURRENT_PERIOD'] = $UF_CURRENT_PERIOD_WEEK;
                }
                //	 $arWeeks[$dateWeek]['UF_DATE_FROM'] = $firstDayWeekFromDate;
                $arWeeks[$dateWeek]['UF_DATE_TO'] = $row["UF_DATE"];
                if ($arWeeks[$dateWeek]['UF_HIGH'] < $row["UF_HIGH"]) {
                    $arWeeks[$dateWeek]['UF_HIGH'] = $row["UF_HIGH"];
                }
                if ($arWeeks[$dateWeek]['UF_LOW'] > $row["UF_LOW"]) {
                    $arWeeks[$dateWeek]['UF_LOW'] = $row["UF_LOW"];
                }
                $arWeeks[$dateWeek]['UF_CLOSE'] = $row["UF_CLOSE"];
            }
            // } //if add == true for week
        }
        $this->savePeriodsToDB($hlCandleTableName, $arAllCandles, $code, $arWeeks, "W");
        
    }
    
    
    /**
     * Сохраняет расчет свечей в БД (добавляет или обновляет текущие периоды)
     *
     * @param string $hlCandleTableName название таблицы данных для свечей
     * @param link to array   $arAllCandles ссылка на массив с выбранными текущими периодами
     * @param string $ticker            тикер (isin) бумаги для которой строим график
     * @param array  $arData            рассчитанные данные для какого то из периодов для записи в БД
     * @param string $period            период для записи в БД "M", "W"
     *
     */
    function savePeriodsToDB($hlCandleTableName, &$arAllCandles, $ticker = false, $arData = array(), $period = false)
    {
        global $DB;
        
        if (!$ticker || !$period || count($arData) <= 0 || empty($hlCandleTableName)) {
            return false;
        } //Если переданы не все данные - выходим
        //Запись в БД рассчитанных данных для свечей месяцев
        
        foreach ($arData as $k => $value) {
            
            
            $DB->PrepareFields($hlCandleTableName);
            
            
            $value["UF_CLOSE"] = (float)$value["UF_CLOSE"] == 0 ? (float)$value["UF_OPEN"] : (float)$value["UF_CLOSE"];
            $value["UF_OPEN"] = (float)$value["UF_OPEN"] == 0 ? (float)$value["UF_CLOSE"] : (float)$value["UF_OPEN"];
            $value["UF_HIGH"] = (float)$value["UF_HIGH"] == 0 ? $value["UF_CLOSE"] : (float)$value["UF_HIGH"];
            $value["UF_LOW"] = (float)$value["UF_LOW"] == 0 ? $value["UF_CLOSE"] : (float)$value["UF_LOW"];
            
            if ($value["UF_CLOSE"] == 0 && $value["UF_OPEN"] == 0 && ($value["UF_HIGH"] == 0 || $value["UF_LOW"] == 0)) {
                $valOpenClose = $value["UF_HIGH"] > 0 ? $value["UF_HIGH"] : $value["UF_LOW"];
                $value["UF_CLOSE"] = $valOpenClose;
                $value["UF_OPEN"] = $valOpenClose;
                $value["UF_HIGH"] = $value["UF_HIGH"] == 0 ? $value["UF_LOW"] : $value["UF_HIGH"];
                $value["UF_LOW"] = $value["UF_LOW"] == 0 ? $value["UF_HIGH"] : $value["UF_LOW"];
            }
            
            $arFields = array(
              "UF_TICKER"         => "'" . trim($value['UF_TICKER']) . "'",
              "UF_DATE_FROM"      => "'" . trim($value['UF_DATE_FROM']) . "'",
              "UF_DATE_TO"        => "'" . trim($value['UF_DATE_TO']) . "'",
              "UF_PERIOD"         => "'" . trim($value['UF_PERIOD']) . "'",
              "UF_CURRENT_PERIOD" => "'" . trim($value['UF_CURRENT_PERIOD']) . "'",
              "UF_OPEN"           => "'" . floatval($value['UF_OPEN']) . "'",
              "UF_HIGH"           => "'" . floatval($value['UF_HIGH']) . "'",
              "UF_LOW"            => "'" . floatval($value['UF_LOW']) . "'",
              "UF_CLOSE"          => "'" . floatval($value['UF_CLOSE']) . "'",
            );
            
            $DB->StartTransaction();
            //Если запись существует то обновляем ее по ID
            
            
            //if ($arAllCandles[$value['UF_TICKER']][$period]['UF_DATE_FROM']==$value['UF_DATE_FROM'] && $value['UF_CURRENT_PERIOD']=='Y')
            if ($arAllCandles[$value['UF_TICKER']][$period]) {
                //	  if($value['UF_CURRENT_PERIOD']=='Y'){
                $ID = $arAllCandles[$value['UF_TICKER']][$period]['ID'];
                $DB->Update($hlCandleTableName, $arFields, "WHERE ID='" . $ID . "'", $err_mess . __LINE__);
                //	  }
            } else //Иначе добавляем новую запись
            {
                $ID = $DB->Insert($hlCandleTableName, $arFields, $err_mess . __LINE__);
            }
            if (strlen($strError) <= 0) {
                $DB->Commit();
            } else {
                CLogger::SpbexGraphCandleHLError($strError);
                $DB->Rollback();
            }
        }
    }
    
    private function getFromDate()
    {
        $dataClass = $this->getHlDataClass();
        
        $res = $dataClass::getList([
          "filter" => $this->filter,
          "order"  => [
            "UF_DATE" => "asc",
          ],
          "limit"  => 1,
          "select" => [
            "UF_DATE",
          ],
        ]);
        
        if ($item = $res->fetch()) {
            return $item["UF_DATE"]->format("d.m.Y");
        }
    }
    
    private function getToDate()
    {
        $dataClass = $this->getHlDataClass();
        
        $res = $dataClass::getList([
          "filter" => $this->filter,
          "order"  => [
            "UF_DATE" => "desc",
          ],
          "limit"  => 1,
          "select" => [
            "UF_DATE",
          ],
        ]);
        
        if ($item = $res->fetch()) {
            return $item["UF_DATE"]->format("d.m.Y");
        }
    }
    
    private function getHlDataClass()
    {
        if (!$this->dataClasses[$this->hlId]) {
            $hlblock = HL\HighloadBlockTable::getById($this->hlId)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $this->dataClasses[$this->hlId] = $entity->getDataClass();
        }
        
        return $this->dataClasses[$this->hlId];
    }
}