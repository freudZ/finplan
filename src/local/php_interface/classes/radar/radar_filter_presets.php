<?
  //Управление пресетами (шаблонами) радара
  //Сброс управляемого кеша реализован в подключаемом в init.php файле /local/php_interface/include/managed_cache_iblocks.php


  class RadarPresets {

  private static $arAllResetElementsId = array(287810, 291605, 293042, 293038, 293279, 293270, 293324, 293323);
  public $presetIblockId = 59;
  public $resetName = "Сбросить все";
	function __construct(){

	}

	public function getPresetList(){
	 $arPresetList = array();
	   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "getPresetList";
		$cacheTtl = 86400 * 7;
	       //$cache->clean("getPresetList");
		if ($cache->read($cacheTtl, $cacheId)) {
	            $arPresetList = $cache->get($cacheId);
	        } else {

		CModule::IncludeModule("iblock");
		$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_*");
		$arFilter = Array("IBLOCK_ID"=>IntVal($this->presetIblockId), "ACTIVE"=>"Y");
		if(!$GLOBALS["USER"]->IsAdmin()){
		  $arFilter["PROPERTY_ADMIN_VLUE"]=false;
		}
		$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
		$arResetSharesParams = array();
		$arResetSharesUSAParams = array();
		$arResetETFParams = array();
		$arResetBondsParams = array();
		while($ob = $res->GetNextElement()){
		 $arFields = $ob->GetFields();
		 $arFields["PROPERTIES"] = $ob->GetProperties();


		 //Пресет сброса всегда должен быть с минимальной сортировкой, что бы можно было его сделать опорным для заполнения недостающих значений остальных пресетов типа
		 if($arFields["NAME"]!=$this->resetName ){
		 	if($arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="shares" && count($arResetSharesParams)<=0){
		 	  $arResetSharesParams = $arPresetList[$arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]][$this->resetName];
		 	}
		 	if($arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="shares_usa" && count($arResetSharesUSAParams)<=0){
		 	  $arResetSharesUSAParams = $arPresetList[$arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]][$this->resetName];
		 	}
 		 	if($arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="etf" && count($arResetETFParams)<=0){
		 	  $arResetETFParams = $arPresetList[$arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]][$this->resetName];
		 	}
		 	if($arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="bonds" && count($arResetBondsParams)<=0){
		 	  $arResetBondsParams = $arPresetList[$arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]][$this->resetName];

		 	}

		 }

		 $arPresetParams = array();
		 if($arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="shares"){ //Собираем пресеты для акций

			foreach($arFields["PROPERTIES"]["FILTER_SHARE"]["VALUE"] as $k=>$v){

			 if($arFields["NAME"]!=$this->resetName ){ //Если значение задано то берем его, иначе из массива параметров для сброса
			   $arPresetParams[$v["SUB_VALUES"]["PARAM_SHARE"]["VALUE_XML_ID"]] = !empty($v["SUB_VALUES"]["PRARM_SHARE_VALUES"]["VALUE"])?$v["SUB_VALUES"]["PRARM_SHARE_VALUES"]["VALUE"]:$arResetSharesParams[$v["SUB_VALUES"]["PARAM_SHARE"]["VALUE_XML_ID"]];
			 } else {
				$arPresetParams[$v["SUB_VALUES"]["PARAM_SHARE"]["VALUE_XML_ID"]] = $v["SUB_VALUES"]["PRARM_SHARE_VALUES"]["VALUE"];
			 }
			}
		 }
		 if($arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="shares_usa"){ //Собираем пресеты для акций США
			foreach($arFields["PROPERTIES"]["FILTER_SHARE_USA"]["VALUE"] as $k=>$v){

			 if($arFields["NAME"]!=$this->resetName ){ //Если значение задано то берем его, иначе из массива параметров для сброса
			   $arPresetParams[$v["SUB_VALUES"]["PARAM_SHARE_USA"]["VALUE_XML_ID"]] = !empty($v["SUB_VALUES"]["PRARM_SHARE_USA_VALUES"]["VALUE"])?$v["SUB_VALUES"]["PRARM_SHARE_USA_VALUES"]["VALUE"]:$arResetSharesUSAParams[$v["SUB_VALUES"]["PARAM_SHARE_USA"]["VALUE_XML_ID"]];
			 } else {
				$arPresetParams[$v["SUB_VALUES"]["PARAM_SHARE_USA"]["VALUE_XML_ID"]] = $v["SUB_VALUES"]["PRARM_SHARE_USA_VALUES"]["VALUE"];
			 }
			}
		 }
		 if($arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="etf"){ //Собираем пресеты для ETF
			foreach($arFields["PROPERTIES"]["FILTER_ETF"]["VALUE"] as $k=>$v){

			 if($arFields["NAME"]!=$this->resetName ){ //Если значение задано то берем его, иначе из массива параметров для сброса
			   $arPresetParams[$v["SUB_VALUES"]["PARAM_ETF"]["VALUE_XML_ID"]] = !empty($v["SUB_VALUES"]["PRARM_ETF_VALUES"]["VALUE"])?$v["SUB_VALUES"]["PRARM_ETF_VALUES"]["VALUE"]:$arResetETFParams[$v["SUB_VALUES"]["PARAM_ETF"]["VALUE_XML_ID"]];
			 } else {
				$arPresetParams[$v["SUB_VALUES"]["PARAM_ETF"]["VALUE_XML_ID"]] = $v["SUB_VALUES"]["PRARM_ETF_VALUES"]["VALUE"];
			 }
			}
		 }
		 if($arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="bonds"){ //Собираем пресеты для облигаций
			foreach($arFields["PROPERTIES"]["FILTER_BOND"]["VALUE"] as $k=>$v){

			 if($arFields["NAME"]!=$this->resetName ){
			 	   $value='';
				//Обработка диапазонов дат для филльтра (арифметика дат)
				if(in_array($v["SUB_VALUES"]["PARAM_BOND"]["VALUE_XML_ID"], array('date_start_first', 'date_start_last', 'date_cancel_first', 'date_cancel_last')) && !empty($v["SUB_VALUES"]["PRARM_BOND_VALUES"]["VALUE"])){
				   $value = (new DateTime())->modify($v["SUB_VALUES"]["PRARM_BOND_VALUES"]["VALUE"])->format('d.m.Y');
				} else {
					$value = $v["SUB_VALUES"]["PRARM_BOND_VALUES"]["VALUE"];
				}


				//Если значение задано то берем его, иначе из массива параметров для сброса
			   $arPresetParams[$v["SUB_VALUES"]["PARAM_BOND"]["VALUE_XML_ID"]] = !empty($value)?$value:$arResetBondsParams[$v["SUB_VALUES"]["PARAM_BOND"]["VALUE_XML_ID"]];
			 } else {
				$arPresetParams[$v["SUB_VALUES"]["PARAM_BOND"]["VALUE_XML_ID"]] = $v["SUB_VALUES"]["PRARM_BOND_VALUES"]["VALUE"];
			 }
			}
		 }
		 $arPresetList[$arFields["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]][$arFields["NAME"]] = $arPresetParams;
		}

		             $cache->set($cacheId, $arPresetList);
	        }
	$firephp = FirePHP::getInstance(true);
   $firephp->fb($arPresetList,FirePHP::LOG);

	return $arPresetList;
	}

	//Возвращает массив для построения списка select c выбором пресетов и параметрами запроса для value
	public function getPresetSelectArray(){
		$arAllPresets = $this->getPresetList();
		$arSelectValues = array();

		 if(count($arAllPresets))
		   foreach($arAllPresets as $type=>$arPresets){
		   	foreach($arPresets as $preset_name=>$arParams){
				  	   $arSelectValues[$type][$preset_name] = $this->mapped_implode("&",$arParams);//.'&ajax_actions=y';

		   	}

		   }


		return $arSelectValues;
	}

	private function mapped_implode($glue, $array, $symbol = '=') {
    return implode($glue, array_map(
            function($k, $v) use($symbol) {
            	 $arReturn = '';
            	if(strpos($v,'/')!==false){
						  $v = explode("/",$v);
						  foreach($v as $arV){
						    $arReturn .= (!empty($arReturn)?'&':'').$k."[]=".$arV;
						  }
					} else {
					 $arReturn = $k . $symbol . $v;
					}
					 return $arReturn;
            },
            array_keys($array),
            array_values($array)
            )
        );
  }

	//Возвращает ID пресетов, которые запрещено удалять
	static function getRadarResetPresetsId(){
	  return	self::$arAllResetElementsId;
	}



  }
