<?
use Bitrix\Highloadblock as HL;

//облигации
class RadarBase {

	const TYPE_OBLIGATION = 1;
	const TYPE_ACTION     = 2;
	const TYPE_ACTION_USA = 3;
	const TYPE_FUTURES    = 4;
	public $spb_actions_HL_Id       = 29; //id HL инфоблока с ценами акций
    public $arMonthToKvartal = array(1  => 1,
                                     2  => 1,
                                     3  => 1,
                                     4  => 2,
                                     5  => 2,
                                     6  => 2,
                                     7  => 3,
                                     8  => 3,
                                     9  => 3,
                                     10 => 4,
                                     11 => 4,
                                     12 => 4
    );
    public $currentPeriodMonth = 1; //Цифра текущего месяца для добавления текущего периода

	function __construct() {
		Global $APPLICATION;
		$this->spb_actions_HL_Id = $APPLICATION->usaPricesHlId;
	}

    //поиск по названию
	public function searchByName($q, $limit = 10, $inview = array()) {
		if (empty($q)) {
			return false;
		}
		$arItems = array();

		//параметр для поиска в зависимости от типа
		$key = '';
		switch ($this->classType) {
			case self::TYPE_OBLIGATION:
				$key = 'ISIN';
				break;
			case self::TYPE_ACTION:
				$key = 'SECID';
				break;
		}

		$boardIDs = [
			'TQBR',
			'TQDE',
			'TQDP',
			'TQPI',
		];

		foreach ($this->arOriginalsItems as $item) {
			if (mb_stripos($item["NAME"], $q) !== false || ($key && mb_stripos($item['PROPS'][$key], $q) !== false)) {
				if (in_array($item["PROPS"]["SECID"], $inview) || ($this->classType === self::TYPE_ACTION && !in_array($item["PROPS"]["BOARDID"], $boardIDs))) {
					continue;
				}
				if($this->classType === self::TYPE_ACTION && $item["PROPS"]["IS_PIF_ACTIVE"]=='Y'){
					continue;
				}
				if($this->classType === self::TYPE_ACTION && !empty($item["PROPS"]["HIDEN"])){
					continue;
				}
				$arItems[] = $item;

				if (count($arItems) >= $limit) {
					break;
				}
			}
		}

		return $arItems;
	}

	/**
	 * Возвращает дату следующей отчетности компании, если она есть БД и еще дата <= текущей дате
	 *
	 * @param  string   $companyName Название компании
	 * @param  string   $type Тип события календаря REPORT_RUS или REPORT_USA
	 *
	 * @return string  отформатированная дата d.m.Y или пустая строка
	 *
	 * @access public
	 */
	public function getNextReportDate($companyName, $type='REPORT_RUS'){
		Global $DB;
		$result = '';
		$query = "SELECT `UF_DATE` FROM `radar_events` WHERE `UF_EMITENT` = '".$DB->forSQL($companyName)."' AND `UF_TYPE` = '$type' ORDER BY `UF_DATE` DESC LIMIT 1";
		try {
				$res = $DB->Query($query);
				if($row = $res->fetch()){
				  $dt = new DateTime($row["UF_DATE"]);
				  $now = new DateTime();
				  if($now->getTimestamp()<=$dt->getTimestamp()){
				  	$result = $dt->format('d.m.Y');
				  }
				}} catch (Exception $e) {
		    		$result = 'Ошибка: '.  $e->getMessage();
		} finally {

		}
		return $result;
	}

	//рассчет данных по периодам
	public function calculatePeriodData($item) {

		if (!$item["PERIODS"]) {
			return false;
		}

        $this->currentPeriodMonth = intval((new Datetime())->format('m'));
		//Расчитаем смещение кварталов для компаний с нестандартным началом финансового года
		/*    if($item["COMPANY"]["ID"]==151848) {
		echo $item["COMPANY"]["NAME"]."<pre  style='color:black; font-size:11px;'>";
		print_r($item["COMPANY"]["PERIOD_OFFSET"]);
		echo "</pre>";
		}*/
		$periodOffset = 0;
		if (array_key_exists("PERIOD_OFFSET", $item["COMPANY"]) && !empty($item["COMPANY"]["PERIOD_OFFSET"])) {
			$periodOffset = intval($item["COMPANY"]["PERIOD_OFFSET"]);
		} else {
			$periodOffset = 0;
		}

		//сортировка
		$item["PERIODS"] = sort_nested_arrays($item["PERIODS"], array("PERIOD_YEAR" => "desc", "PERIOD_VAL" => "desc"));

		//ключи
		$tmp = array();
		foreach ($item["PERIODS"] as $val) {
			$tmp[$val["PERIOD_VAL"] . "-" . $val["PERIOD_YEAR"] . "-" . $val["PERIOD_TYPE"]] = $val;
		}
		$item["PERIODS"] = $tmp;

		$tmp2 = array();
		$l    = 0;


		foreach ($item["PERIODS"] as $n => $val) {
			$t                                  = explode('-', $n);
			$item["PERIODS"][$n]["PERIOD_YEAR"] = $t[1];
			$item["PERIODS"][$n]["PERIOD_VAL"]  = $t[0];

			if (mb_stripos($n, "-KVARTAL") !== false) {
				//умножение капитализации
				if ($item["COMPANY"]["CAPITAL_KOEF"] && $val["Прошлая капитализация"]) {
					$item["PERIODS"][$n]["Прошлая капитализация"] = $val["Прошлая капитализация"] / $item["COMPANY"]["CAPITAL_KOEF"];
				}

				//Доля собственного капитала в активах
				if ($val["Активы"] && $val["Собственный капитал"]) {
					$item["PERIODS"][$n]["Доля собственного капитала в активах"] = round(($val["Собственный капитал"] / $val["Активы"]) * 100, 2);
				}

				//Темп роста активов
				if ($val["Активы"] && $item["PERIODS"][$t[0] . "-" . ($t[1] - 1) . "-KVARTAL"]["Активы"]) {
					$item["PERIODS"][$n]["Темп роста активов"] = round(($val["Активы"] / $item["PERIODS"][$t[0] . "-" . ($t[1] - 1) . "-KVARTAL"]["Активы"] - 1) * 100, 2);
				}

				//Доля СК
				/*
				if($val["Активы"] && $val["Собственный капитал"]){
				$item["PERIODS"][$n]["Доля СК"] =  round(($val["Собственный капитал"]/$val["Активы"])*100, 2);
				}
				 */

				//выручка полугодовая
				if ($val["PERIOD_VAL"] == 4 && $val["Выручка  тек."] && $item["PERIODS"]["2-" . $t[1] . "-KVARTAL"]["Выручка  тек."]) {
					$item["PERIODS"][$n]["Выручка полугодовая"] = $val["Выручка  тек."] - $item["PERIODS"]["2-" . $t[1] . "-KVARTAL"]["Выручка  тек."];
				}
				if ($val["PERIOD_VAL"] == 2 && $val["Выручка  тек."]) {
					$item["PERIODS"][$n]["Выручка полугодовая"] = $val["Выручка  тек."];
				}

				//прибыль полугодовая
				if ($val["PERIOD_VAL"] == 4 && $val["Прибыль"] && $item["PERIODS"]["2-" . $t[1] . "-KVARTAL"]["Прибыль"]) {
					$item["PERIODS"][$n]["Прибыль полугодовая"] = $val["Прибыль"] - $item["PERIODS"]["2-" . $t[1] . "-KVARTAL"]["Прибыль"];
				}
				if ($val["PERIOD_VAL"] == 2 && $val["Прибыль"]) {
					$item["PERIODS"][$n]["Прибыль полугодовая"] = $val["Прибыль"];
				}
			}

			$tmp2[$l] = $n;
			$l++;
		}

		//перевернутый расчет
		$item["PERIODS"] = array_reverse($item["PERIODS"]);

		$lastReveneus = 0;
		$lastProfit   = 0;
		foreach ($item["PERIODS"] as $n => $val) {
			$t = explode('-', $n);

			if (mb_stripos($n, "-KVARTAL") !== false) {
				//Выручка квартальная
				if ($t[0] > 1 && $lastReveneus) {
					$prev = ($t[0] - 1) . "-" . $t[1] . "-KVARTAL";

					if ($lastPeriod == $prev) {
						$item["PERIODS"][$n]["Выручка квартальная"] = $val["Выручка  тек."] - $lastReveneus;
					}
				} else {
					$item["PERIODS"][$n]["Выручка квартальная"] = $val["Выручка  тек."];
				}
				$lastReveneus = $val["Выручка  тек."];

				//Прибыль квартальная
				if ($t[0] > 1 && $lastProfit) {
					$prev = ($t[0] - 1) . "-" . $t[1] . "-KVARTAL";

					if ($lastPeriod == $prev) {
						$item["PERIODS"][$n]["Прибыль квартальная"] = $val["Прибыль"] - $lastProfit;
					}
				} else {
					$item["PERIODS"][$n]["Прибыль квартальная"] = $val["Прибыль"];
				}
				$lastProfit = $val["Прибыль"];
				$lastPeriod = $n;
			}
		}
		$item["PERIODS"] = array_reverse($item["PERIODS"]);

		//Рентабельность собственного капитала за текущий и 3 месяца назад
		/*
		$tmp2 = array_reverse($tmp2);
		$arKeys = array_reverse(array_keys($item["PERIODS"]));
		foreach($item["PERIODS"] as $n=>$val){
		if($val["PERIOD_TYPE"]=="MONTH"){
		continue;
		}

		$key = array_search($n, $tmp2);
		if($key-3>=0){
		$pribil = 0;

		for($i=$key;$i>=$key-3;$i--){
		$pribil += $item["PERIODS"][$arKeys[$i]]["Прибыль квартальная"];
		}

		if($val["Прошлая капитализация"]){
		$item["PERIODS"][$n]["P/E"] = round($val["Прошлая капитализация"]/$pribil, 1);
		}
		if($val["Собственный капитал"]) {
		$item["PERIODS"][$n]["Рентабельность собственного капитала"] = round(($pribil / $val["Собственный капитал"]) * 100, 2);
		} else {
		$item["PERIODS"][$n]["Рентабельность собственного капитала"] = "";
		}
		} else {
		$item["PERIODS"][$n]["Рентабельность собственного капитала"] = "";
		}
		}*/

		$item["PERIODS"] = array_reverse($item["PERIODS"]);
		//выручка и прибыль скользящая
		$arFields = array(
			"Выручка за год (скользящая)" => array(
				"val" => "Выручка  тек.",
				"kvartal" => "Выручка квартальная",
				"half_year" => "Выручка полугодовая",
			),
			"Прибыль за год (скользящая)" => array(
				"val" => "Прибыль",
				"kvartal" => "Прибыль квартальная",
				"half_year" => "Прибыль полугодовая",
			)
		);

		//Двигаем капу
		if ($periodOffset != 0) {
			$item["PERIODS"] = $this->capaOffset($item["PERIODS"], $periodOffset);
			//$item["PERIODS"] = array_reverse($item["PERIODS"]);
		}

		foreach ($item["PERIODS"] as $n => $val) {
			foreach ($arFields as $name => $data) {
				if ($val["PERIOD_VAL"] == 4) {
					/*echo "<pre>";
					var_dump($val[$data["val"]]);
					var_dump($n);
					echo "</pre>";*/
					if ($val[$data["val"]]) {
						$item["PERIODS"][$n][$name] = $val[$data["val"]];
					}
				} elseif ($val["PERIOD_VAL"] == 3) {
					//прибыль
					if (
						$val[$data["kvartal"]] &&
						$item["PERIODS"]["2-" . $val["PERIOD_YEAR"] . "-KVARTAL"][$data["kvartal"]] &&
						$item["PERIODS"]["1-" . $val["PERIOD_YEAR"] . "-KVARTAL"][$data["kvartal"]] &&
						$item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["kvartal"]]
					) {
						$item["PERIODS"][$n][$name] = $val[$data["kvartal"]] +
							$item["PERIODS"]["2-" . $val["PERIOD_YEAR"] . "-KVARTAL"][$data["kvartal"]] +
							$item["PERIODS"]["1-" . $val["PERIOD_YEAR"] . "-KVARTAL"][$data["kvartal"]] +
							$item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["kvartal"]];
					} else {
						$item["PERIODS"][$n][$name] = $item["PERIODS"]["2-" . $val["PERIOD_YEAR"] . "-KVARTAL"][$name];
					}
				} elseif ($val["PERIOD_VAL"] == 2) {
					//прибыль
					if (
						$val[$data["half_year"]] &&
						$item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["half_year"]]
					) {
						$item["PERIODS"][$n][$name] = $val[$data["half_year"]] +
							$item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["half_year"]];
					} else {
						$item["PERIODS"][$n][$name] = $item["PERIODS"]["1-" . $val["PERIOD_YEAR"] . "-KVARTAL"][$name];
					}
				} elseif ($val["PERIOD_VAL"] == 1) {
					//прибыль
					if (
						$val[$data["kvartal"]] &&
						$item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["kvartal"]] &&
						$item["PERIODS"]["3-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["kvartal"]] &&
						$item["PERIODS"]["2-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["kvartal"]]
					) {
						$item["PERIODS"][$n][$name] = $val[$data["kvartal"]] +
							$item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["kvartal"]] +
							$item["PERIODS"]["3-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["kvartal"]] +
							$item["PERIODS"]["2-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$data["kvartal"]];
					} else {
						$item["PERIODS"][$n][$name] = $item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"][$name];
					}
				}
			}

			//Рентабельность СК
			if ($item["PERIODS"][$n]["Прибыль за год (скользящая)"] && $val["Собственный капитал"]) {
				if ($val["Собственный капитал"] < 0 && $item["PERIODS"][$n]["Прибыль за год (скользящая)"] < 0) {
					$item["PERIODS"][$n]["Рентабельность собственного капитала"] = "";
				} else {
					$item["PERIODS"][$n]["Рентабельность собственного капитала"] = $item["PERIODS"][$n]["Прибыль за год (скользящая)"] / $val["Собственный капитал"] * 100;
				}
			}

			//P/E
			if ($val["Прошлая капитализация"] && $item["PERIODS"][$n]["Прибыль за год (скользящая)"]) {
				$item["PERIODS"][$n]["P/E"] = round($val["Прошлая капитализация"] / $item["PERIODS"][$n]["Прибыль за год (скользящая)"], 2);
			}

			//P/B
			if ($item["PERIODS"][$n]["Прошлая капитализация"] && $item["PERIODS"][$n]["Активы"]) {
				$item["PERIODS"][$n]["P/B"] = $item["PERIODS"][$n]["Прошлая капитализация"] / $item["PERIODS"][$n]["Активы"];
			}

			//P/Equity
			if ($item["PERIODS"][$n]["Прошлая капитализация"] && $item["PERIODS"][$n]["Собственный капитал"]) {
				$item["PERIODS"][$n]["P/Equity"] = $item["PERIODS"][$n]["Прошлая капитализация"] / $item["PERIODS"][$n]["Собственный капитал"];
			}

			//P/Sale
			if ($item["PERIODS"][$n]["Прошлая капитализация"] && $item["PERIODS"][$n]["Выручка за год (скользящая)"]) {
				$item["PERIODS"][$n]["P/Sale"] = $item["PERIODS"][$n]["Прошлая капитализация"] / $item["PERIODS"][$n]["Выручка за год (скользящая)"];
			}
		}

		foreach ($item["PERIODS"] as $n => $val) {
			//Темп роста прибыли
			if ($val["Прибыль за год (скользящая)"] && $item["PERIODS"][$val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Прибыль за год (скользящая)"]) {
				if ($val["Прибыль за год (скользящая)"] < 0 && $item["PERIODS"][$val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Прибыль за год (скользящая)"] < 0) {
					$item["PERIODS"][$n]["Темп роста прибыли"] = "";
				} else {
					$item["PERIODS"][$n]["Темп роста прибыли"] = (($val["Прибыль за год (скользящая)"] / $item["PERIODS"][$val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Прибыль за год (скользящая)"]) - 1) * 100;

					if ($item["PERIODS"][$val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Прибыль за год (скользящая)"] < 0) {
						$item["PERIODS"][$n]["Темп роста прибыли"] = abs($item["PERIODS"][$n]["Темп роста прибыли"]);
					}

					//$item["PERIODS"][$n]["Темп роста прибыли"] = round($item["PERIODS"][$n]["Темп роста прибыли"], 2);
				}
			}

			//Темп прироста выручки
			if ($val["Выручка за год (скользящая)"] && $item["PERIODS"][$val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Выручка за год (скользящая)"]) {
				$item["PERIODS"][$n]["Темп прироста выручки"] = round(($val["Выручка за год (скользящая)"] / $item["PERIODS"][$val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Выручка за год (скользящая)"] - 1) * 100, 2);
			}
		}

		foreach ($item["PERIODS"] as $n => $val) {
			//средняя прибыль
			$averageProfitSum = 0;

			if (intval($val["PERIOD_VAL"]) === 4) {
				if ($val["Прибыль квартальная"]) {
					$averageProfitSum += floatval($item["PERIODS"]["4-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["3-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["2-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["1-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
				} elseif ($val["Прибыль полугодовая"]) {
					$averageProfitSum += floatval($item["PERIODS"]["4-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["2-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["2-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 2) . "-KVARTAL"]["Темп роста прибыли"]);
				}
			}

			if (intval($val["PERIOD_VAL"]) === 3) {
				if ($val["Прибыль квартальная"]) {
					$averageProfitSum += floatval($item["PERIODS"]["3-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["2-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["1-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["3-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
				}
			}

			if (intval($val["PERIOD_VAL"]) === 2) {
				if ($val["Прибыль квартальная"]) {
					$averageProfitSum += floatval($item["PERIODS"]["2-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["1-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["3-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["2-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
				} elseif ($val["Прибыль полугодовая"]) {
					$averageProfitSum += floatval($item["PERIODS"]["2-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["2-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 2) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["2-" . ($val["PERIOD_YEAR"] - 2) . "-KVARTAL"]["Темп роста прибыли"]);
				}
			}

			if (intval($val["PERIOD_VAL"]) === 1) {
				if ($val["Прибыль квартальная"]) {
					$averageProfitSum += floatval($item["PERIODS"]["1-" . $val["PERIOD_YEAR"] . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["3-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["2-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
					$averageProfitSum += floatval($item["PERIODS"]["1-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL"]["Темп роста прибыли"]);
				}
			}

			$item["PERIODS"][$n]["PEG"] = '-';

			$item["PERIODS"][$n]["AVEREGE_PROFIT"] = $averageProfitSum ? $averageProfitSum / 5 : 0;

			if ($item["PERIODS"][$n]["P/E"] > 0 && $item["PERIODS"][$n]["AVEREGE_PROFIT"] > 0) {
				$item["PERIODS"][$n]["AVEREGE_PROFIT"] = floatval($averageProfitSum / 5);
				$item["PERIODS"][$n]["PEG"]            = floatval($item["PERIODS"][$n]["P/E"]) / floatval($averageProfitSum / 5);
			}

			//EV/EBITDA
			if ($item["PERIODS"][$n]["EBITDA"]) {
				$item["PERIODS"][$n]["EV/EBITDA"] = floatval($item["PERIODS"][$n]["Прошлая капитализация"] + $item["PERIODS"][$n]["Активы"] - $item["PERIODS"][$n]["Собственный капитал"]) / floatval($item["PERIODS"][$n]["EBITDA"]);
				if ($item["PERIODS"][$n]["EV/EBITDA"] == INF) {
					$item["PERIODS"][$n]["EV/EBITDA"] = 0;
				}
			}
			//DEBT/EBITDA
			if ($item["PERIODS"][$n]["EBITDA"]) {
				$item["PERIODS"][$n]["DEBT/EBITDA"] = floatval($item["PERIODS"][$n]["Активы"] - $item["PERIODS"][$n]["Собственный капитал"]) / floatval($item["PERIODS"][$n]["EBITDA"]);
				if ($item["PERIODS"][$n]["DEBT/EBITDA"] == INF) {
					$item["PERIODS"][$n]["DEBT/EBITDA"] = 0;
				}
			}
		}

		$item["PERIODS"] = array_reverse($item["PERIODS"]);

		if ($item["COMPANY"]["IS_REGION"]) {
			foreach ($item["PERIODS"] as $n => $val) {
				$item["PERIODS"][$n]["Дефицит/профицит"]                            = floatval($item["PERIODS"][$n]["Доходы консолидированного бюджета"]) - floatval($item["PERIODS"][$n]["Расходы консолидированного бюджета"]);
				$item["PERIODS"][$n]["Дефицит / профицит в % от доходов"] = (floatval($item["PERIODS"][$n]["Дефицит/профицит"]) / floatval($item["PERIODS"][$n]["Доходы консолидированного бюджета"])) * 100;
				$item["PERIODS"][$n]["Доля долга в ВРП, в %"]                        = (floatval($item["PERIODS"][$n]["Государственный долг"]) / $item["PERIODS"][$n]["ВРП"]) * 100;
				unset($item["PERIODS"][$n]["PEG"]);
			}
		}

		//Смещаем периоды к с учетом финансового года компании. Данные в HL блоке остаются не смещенными, это следует учесть!!!
		if ($periodOffset != 0) {
			$arPeriodsTmp = $this->periodsOffset($item["PERIODS"], $periodOffset, $item["COMPANY"]);
			//Досчитываем капу и мультипликаторы при положительном сдвиге вправо (в будущее)
			if ($periodOffset > 0) {
				$arPeriodsTmp = $this->calculateCapa($arPeriodsTmp, $item);
			}

			$item["PERIODS"] = $arPeriodsTmp;
		}
		//Подключаем добавление текущего периода
        if(\Bitrix\Main\Config\Option::get("grain.customsettings","ADD_CURRENT_PERIOD")=="Y" ) {
                            $this->addCurrentPeriod($item["PERIODS"], $item);
                            //$item["PERIODS"] = $this->addCurrentPeriod($item["PERIODS"], $item);
        }
/*	  	if($item["PROPS"]["ISIN"]=="US91085A2033"){
	 	$firephp = FirePHP::getInstance(true);
      $firephp->fb($item["PERIODS"],FirePHP::LOG);
		}*/
		return $item["PERIODS"];
	}
    
    /**
     * Добавляет текущий период (все данные из последнего существующего периода, а капа из акции текущая + мультики считаются)
     * @param $arPeriods
     * @param $item
     */
    public function addCurrentPeriod(&$arPeriods, $item)
    {
        //  $arPeriods = array_reverse($arPeriods);
        $arLastPeriod = reset($arPeriods);
        $arrPeriodName = array_keys($arPeriods);
        $arrPeriodName = reset($arrPeriodName);
        $arPeriodName = explode("-", $arrPeriodName);
        unset($arrPeriodName);
        
        $arPeriods = array_reverse($arPeriods);
        
        $arLastPeriod["CURRENT_PERIOD"] = true;
        //Капитализация из акции текущая
		  //Если валюта отчетности отличается от доллара пересчитаем курс и прошлую капу

		  $currCapa = $item["PROPS"]["ISSUECAPITALIZATION"];

		  //Если для акций сша указана валюта отчетности отличная от доллара - получим курс и пересчитаем текущую капитализацию для текущего периода
		  if(array_key_exists("CURRENCY_REPORT",$item["COMPANY"])){
			// if(!empty($item["COMPANY"]["CURRENCY_REPORT"]) && $item["COMPANY"]["CURRENCY_REPORT"]!="USD"){
			 if(!empty($item["COMPANY"]["CURRENCY_REPORT"]) && $item["COMPANY"]["CURRENCY_REPORT"]!=$item["COMPANY"]["CURRENCY_CIRCULATION"]){
/*				 $cbr = new CCurrency((new DateTime())->format("d.m.Y"));
				 $rate = $cbr->getRate($item["COMPANY"]["CURRENCY_REPORT"],false);
				 if($rate<=0){$rate=1;}

				 $currCapa = $item["PROPS"]["ISSUECAPITALIZATION"]*$rate;  //Не учтено направление курса (прямой/обратный)*/
				 /*  */
				 			//Предыдущий день
/*							$prevDay = new DateTime();
							$prevDay->modify("-1 day");
							$cbr = new CCurrency($prevDay->format('d.m.Y'));
							$arReportRate = $cbr->getRate($item["COMPANY"]["CURRENCY_REPORT"], false);
							$arCirculationRate = $cbr->getRate($item["COMPANY"]["CURRENCY_CIRCULATION"], false);
 							if ($cbr->isRareCurrency($item["COMPANY"]["CURRENCY_REPORT"])) { //Если редкая валюта то считаем через доллары напрямую
								$currCapa = $item["PROPS"]["ISSUECAPITALIZATION"] * $arReportRate;
							} else { //Если не редкая валюта то считаем через рубли
								$currCapa = ($item["PROPS"]["ISSUECAPITALIZATION"] * $arCirculationRate) / $arReportRate;
							}*/



				 $currCapa = $item["PROPS"]["ISSUECAPITALIZATION_CURRENCY"];
			 }
		  }

						//если данные в долларах, перевести капитализацию
						if(mb_stripos($item["COMPANY"]["VAL_ED_FIRST"], "долл")!==false && $item["PROPS"]["ISSUECAPITALIZATION"]){
						  //Предыдущий день
						   $prevDay = new DateTime();
							$prevDay->modify("-1 day");
							$cbr = new CCurrency($prevDay->format("d.m.Y"));
							//$item["PROPS"]["ISSUECAPITALIZATION_DOL"] = $item["PROPS"]["ISSUECAPITALIZATION"]/getCBPrice("USD", $prevDay->format("d/m/Y"));
							$currCapa = $item["PROPS"]["ISSUECAPITALIZATION"]/$cbr->getRate("USD",false);
							unset($cbr);
						}


/*			   if($item["PROPS"]["SECID"]=="TSM"){
		  		$firephp = FirePHP::getInstance(true);
		  		$firephp->fb(array("capa"=>$currCapa, "rate"=>$rate, "CURR"=>$item["COMPANY"]["CURRENCY_REPORT"]),FirePHP::LOG);
		  	}*/

        $arLastPeriod["Прошлая капитализация"] = $currCapa / 1000000;
        if($arPeriodName[2]=="MONTH"){
            $currPeriodName = $this->currentPeriodMonth . "-" . date("y") . "-" . $arPeriodName[2];
            $arLastPeriod["PERIOD_VAL"] = $this->currentPeriodMonth;
        }else{
            $currPeriodName = $this->arMonthToKvartal[$this->currentPeriodMonth] . "-" . date("Y") . "-" . $arPeriodName[2];
				//CLogger::Curr_Period_Name($currPeriodName);
            $arLastPeriod["PERIOD_VAL"] = $this->arMonthToKvartal[$this->currentPeriodMonth];
        }

        $arLastPeriod["PERIOD_YEAR"] = $arPeriodName[1];
        
        
        $arPeriods[$currPeriodName] =  $arLastPeriod;
        unset($arLastPeriod);
        //Расчет мультипликаторов
        //P/E
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Прибыль за год (скользящая)"]) {
            $arPeriods[$currPeriodName]["P/E"] = round($arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Прибыль за год (скользящая)"],
              2);
        }
        
        //P/B
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Активы"]) {
            $arPeriods[$currPeriodName]["P/B"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Активы"];
        }
        
        //P/Equity
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Собственный капитал"]) {
            $arPeriods[$currPeriodName]["P/Equity"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Собственный капитал"];
        }
        
        //P/Sale
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Выручка за год (скользящая)"]) {
            $arPeriods[$currPeriodName]["P/Sale"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Выручка за год (скользящая)"];
        }
        
        if ($arPeriods[$currPeriodName]["P/E"] > 0 && $arPeriods[$currPeriodName]["AVEREGE_PROFIT"] > 0) {
            //$arPeriods[$currPeriodName]["AVEREGE_PROFIT"] = floatval($averageProfitSum / 5);
            $arPeriods[$currPeriodName]["PEG"] = floatval($arPeriods[$currPeriodName]["P/E"]) / floatval($arPeriods[$currPeriodName]["AVEREGE_PROFIT"]);
        }
        
        //EV/EBITDA
        if ($arPeriods[$currPeriodName]["EBITDA"]) {
            $arPeriods[$currPeriodName]["EV/EBITDA"] = floatval($arPeriods[$currPeriodName]["Прошлая капитализация"] + $arPeriods[$currPeriodName]["Активы"] - $arPeriods[$currPeriodName]["Собственный капитал"]) / floatval($arPeriods[$currPeriodName]["EBITDA"]);
            if ($arPeriods[$currPeriodName]["EV/EBITDA"] == INF) {
                $arPeriods[$currPeriodName]["EV/EBITDA"] = 0;
            }
        }
        //DEBT/EBITDA
        if ($arPeriods[$currPeriodName]["EBITDA"]) {
            $arPeriods[$currPeriodName]["DEBT/EBITDA"] = floatval($arPeriods[$currPeriodName]["Активы"] - $arPeriods[$currPeriodName]["Собственный капитал"]) / floatval($arPeriods[$currPeriodName]["EBITDA"]);
            if ($arPeriods[$currPeriodName]["DEBT/EBITDA"] == INF) {
                $arPeriods[$currPeriodName]["DEBT/EBITDA"] = 0;
            }
        }
        
        //$arPeriods = $this->calculateCapa($arPeriods, $item, true,$currPeriodName);
        $arPeriods = array_reverse($arPeriods);

        unset($arPeriodName, $currPeriodName);
       //return $arPeriods;
    }
	
	/**
	 * Досчитывает капу и мультипликаторы для периодов, образованных положительным сдвигом вправо (в будущее)
	 * @param $arPeriods
	 * @param $item
	 * @return mixed
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function calculateCapa($arPeriods, $item) {
		$arReturn = array();
		Global $DB;
		/*
		 * TODO: Можно перенести весь пересчет мультипликаторов для периодов в эту функцию и тогда убрать проверку заполненности капы,
		 * Новыя капа будет получаться только для пустого поля на курс последнего существующего дня квартала из итерации. Для расчета мультипликаторов
		 * будет использована или вновь полученная или уже загруженная и передвинутая с периодами капитализация
		 */

		$arCompany        = $item["COMPANY"];
		$quantity         = $item["PROPS"]["SECURITIES_QUANTITY"];
		$arKvartalToMonth = array(
			1 => "03-31",
			2 => "06-30",
			3 => "09-30",
			4 => "12-31",
		);

		//историческая капитализация для компаний США
/*		$hlblock = HL\HighloadBlockTable::getById($this->spb_actions_HL_Id)->fetch();
		$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();*/

		foreach ($arPeriods as $period => $val) {
			if (floatval($val["Прошлая капитализация"]) > 0) {
				continue;
			}

			$arPeriod = explode("-", $period);
			$quarter  = $arPeriod[0];
			$year     = $arPeriod[1];

			//Получим курс на конец текущего в итерации квартала
			$d = DateTime::createFromFormat('Y-m-d', $year . "-" . $arKvartalToMonth[$quarter]);
			if (isUsaWeekendDay($d->format("d.m.Y"))) {
				$finded = false;
				while (!$finded) {
					$d->modify("+1 days");
					if (!isUsaWeekendDay($d->format("d.m.Y"))) {
						$finded = true;
					}
				}
			}

			$table = 'hl_spb_actions_data';
			if($this->spb_actions_HL_Id==53){
			 $table = 'hl_polygon_actions_data';
			}
			 $uf_date = $d->format("Y-m-d");
			 $ticker = $item["PROPS"]["SECID"];
			$query = "SELECT `UF_CLOSE` FROM `$table` WHERE `UF_DATE` = '$uf_date' AND `UF_ITEM` = '$ticker'";

			if(!empty($ticker) && !empty($uf_date)){
				$res = $DB->Query($query);
			}
/*			$res = $entity_data_class::getList([
				"filter" => [
					"=UF_DATE" => $d->format("d.m.Y"),
					"=UF_ITEM" => $item["PROPS"]["SECID"],
				],
				"select" => [
					"UF_CLOSE",
				],
			]);*/
			$uf_close = 0;
			if ($itemPrice = $res->fetch()) {
				$uf_close = $itemPrice["UF_CLOSE"];
			}

			//Получим капитализацию
			$arPeriods[$period]["Прошлая капитализация"] = round(($uf_close * intval($quantity)) / 1000000, 2);

			//Конвертирование валюты капитализации если требуется
/*			if ($arCompany["CURRENCY_REPORT"] !== $arCompany["CURRENCY_CIRCULATION"] && !empty($arCompany["CURRENCY_REPORT"])) {
				$cbr               = new CCurrency((new DateTime($year . "-" . $arKvartalToMonth[$quarter]))->format('d.m.Y'));
				$arReportRate      = $cbr->getRate($arCompany["CURRENCY_REPORT"], false);
				$arCirculationRate = $cbr->getRate($arCompany["CURRENCY_CIRCULATION"], false);
				if ($cbr->isRareCurrency($arCompany["CURRENCY_REPORT"])) { //Если редкая валюта то считаем через доллары напрямую
					//Для редких валют у ЦБ есть курс не на все даты, ищем в прошлое до получения курса в случае отсутствия курса на конец квартала
					while (empty($arReportRate)) {
						$currClassDate = $cbr->getClassDate();
						$cbr->setClassDate((new DateTime($currClassDate))->modify('-1 days')->format('d.m.Y'));
						$arReportRate = $cbr->getRate($arCompany["CURRENCY_REPORT"], false);
					}

					$arPeriods[$period]["Прошлая капитализация"] = $arPeriods[$period]["Прошлая капитализация"] * $arReportRate;
				} else { //Если не редкая валюта то считаем через рубли
					$arPeriods[$period]["Прошлая капитализация"] = ($arPeriods[$period]["Прошлая капитализация"] * $arCirculationRate) / $arReportRate;
				}
			}*/

			$recalcCap = true; //включить блок пересчета капитализации
			//$recalcCap = false;
			if ($arCompany["CURRENCY_REPORT"] !== $arCompany["CURRENCY_CIRCULATION"] && !empty($arCompany["CURRENCY_REPORT"]) && $recalcCap) {
				//Достаем курс валюты отчетности
				$dateSql = (new DateTime($year . "-" . $arKvartalToMonth[$quarter]))->format('Y-m-d');
				$codeCurr = $arCompany["CURRENCY_REPORT"];
				$CURRENCY_REPORT = array("RATE"=>1);
				$sql = "SELECT * FROM `hl_currency_rates` WHERE `UF_DATE` >= '$dateSql' AND `UF_CODE` = '$codeCurr' LIMIT 1";
				$resCurr = $DB->Query($sql);
				if($rowCurr = $resCurr->fetch()){
				  $CURRENCY_REPORT = $rowCurr;
				}
				unset($rowCurr, $resCurr);

				//Достаем курс валюты обращения
				$CURRENCY_CIRCULATION = array("RATE"=>1);
				$codeCurr = $arCompany["CURRENCY_CIRCULATION"];
				$sql = "SELECT * FROM `hl_currency_rates` WHERE `UF_DATE` >= '$dateSql' AND `UF_CODE` = '$codeCurr' LIMIT 1";
				$resCurr = $DB->Query($sql);
				if($rowCurr = $resCurr->fetch()){
				  $CURRENCY_CIRCULATION = $rowCurr;
				}
				unset($rowCurr, $resCurr);
				//$cbr               = new CCurrency((new DateTime($year . "-" . $arKvartalToMonth[$quarter]))->format('d.m.Y'));
				$arReportRate      = $CURRENCY_REPORT["RATE"];
				$arCirculationRate = $CURRENCY_CIRCULATION["RATE"];
				if ($CURRENCY_REPORT["RARE"]==1) { //Если редкая валюта то считаем через доллары напрямую
					$arPeriods[$period]["Прошлая капитализация"] = $arPeriods[$period]["Прошлая капитализация"] * $arReportRate;
				} else { //Если не редкая валюта то считаем через рубли
					$arPeriods[$period]["Прошлая капитализация"] = ($arPeriods[$period]["Прошлая капитализация"] * $arCirculationRate) / $arReportRate;
				}
			}



			//Расчет мультипликаторов
			//P/E
			if ($arPeriods[$period]["Прошлая капитализация"] && $arPeriods[$period]["Прибыль за год (скользящая)"]) {
				$arPeriods[$period]["P/E"] = round($arPeriods[$period]["Прошлая капитализация"] / $arPeriods[$period]["Прибыль за год (скользящая)"], 2);
			}

			//P/B
			if ($arPeriods[$period]["Прошлая капитализация"] && $arPeriods[$period]["Активы"]) {
				$arPeriods[$period]["P/B"] = $arPeriods[$period]["Прошлая капитализация"] / $arPeriods[$period]["Активы"];
			}

			//P/Equity
			if ($arPeriods[$period]["Прошлая капитализация"] && $arPeriods[$period]["Собственный капитал"]) {
				$arPeriods[$period]["P/Equity"] = $arPeriods[$period]["Прошлая капитализация"] / $arPeriods[$period]["Собственный капитал"];
			}

			//P/Sale
			if ($arPeriods[$period]["Прошлая капитализация"] && $arPeriods[$period]["Выручка за год (скользящая)"]) {
				$arPeriods[$period]["P/Sale"] = $arPeriods[$period]["Прошлая капитализация"] / $arPeriods[$period]["Выручка за год (скользящая)"];
			}
		} //foreach;

/*    if($arCompany["ID"]==151848) {
echo $d->format("d.m.Y")."<pre  style='color:black; font-size:11px;'>";
print_r($arPeriods[$period]);
echo "</pre>";
}  */
		$arReturn = $arPeriods;

		return $arReturn;
	}

	//Смещает периоды на нужное кол-ва $offset
	public function periodsOffset($arPeriods, $offset, $arCompany) {
		$arReturn     = array();
		$arPeriods    = array_reverse($arPeriods);
		$periodValues = array_values($arPeriods);
		$offsetMod    = abs($offset);
		//Добавим пустые периоды для сдвига
		$keys = array_keys($arPeriods);

		$firstPeriod = $keys[0];
		$lastPeriod  = $keys[count($keys) - 1];

/*                if($arCompany["ID"]==151848) {
echo "<pre  style='color:black; font-size:11px;'>";
print_r(array("firstPeriod"=>$firstPeriod, "lastPeriod"=>$lastPeriod));
echo "</pre>";
}*/

		$arPeriod    = array();
		$cycleOffset = 1; //Приращение
		//Если отчетность полугодовая - приращение делаем равным 2 кварталам
		/*    if(intval($arCompany["HALF_YEAR"])>0){
		$cycleOffset = 2;
		}*/

		for ($j = 0; $j <= $offsetMod - 1; $j++) {
			if ($offset < 0) {
				if (count($arPeriod) == 0) {
					$arPeriod = explode("-", $firstPeriod);
				}

				//echo "first=".$firstPeriod."<br>";
				$arPeriod[0] -= $cycleOffset;
				if ($arPeriod[0] <= 0) { //Если вышли в предыдущий год
					if ($cycleOffset == 1) {
						$arPeriod[0] = 4;
						$arPeriod[1] -= 1;
					} else {
						/*             if($arPeriod[0]==-1){
					$arPeriod[0] = 3;
					$arPeriod[1] -= 1;
					}
					if($arPeriod[0]==0){
					$arPeriod[0] = 4;
					$arPeriod[1] -= 1;
					}*/
					}
				}
				$keys = array_merge(array(implode('-', $arPeriod)), $keys);
				unset($keys[count($keys) - 1]);
			} else if ($offset > 0) {
				if (count($arPeriod) == 0) {
					$arPeriod = explode("-", $lastPeriod);
				}

				//echo "lastPeriod=".$lastPeriod."<br>";
				$arPeriod[0] += $cycleOffset;
				if ($arPeriod[0] >= 5) {
					if ($cycleOffset == 1) {
						$arPeriod[0] = 1;
						$arPeriod[1] += 1;
					} else {
/*                if($arPeriod[0]==5){
$arPeriod[0] = 1;
$arPeriod[1] += 1;
}
if($arPeriod[0]==6){
$arPeriod[0] = 2;
$arPeriod[1] += 1;
}*/
					}
				}
				unset($keys[0]);
				$keys = array_merge($keys, array(implode('-', $arPeriod)));
			}
		}

		$arReturn = array_combine($keys, $periodValues);

		//Заменим внутренние значения квартала и года в периодах после сдвига
		foreach($arReturn as $period=>$val){
			$arPeriod = explode("-", $period);
			$arReturn[$period]["PERIOD_VAL"] = $arPeriod[0];
			$arReturn[$period]["PERIOD_YEAR"] = $arPeriod[1];
		}


		$arReturn = array_reverse($arReturn);
		return $arReturn;
	}

	/**
	 * Смещает капитализацию на инвертированное кол-во периодов.
	 * Запускается до смещения периодов для того, что бы сдвинув капу получить котировки "на после сдвиговые периоды"
	 * @param $arPeriods
	 * @param $offset
	 * @return mixed
	 */
	public function capaOffset($arPeriods, $offset) {
		$arResult = array();

/*    if($arCompany["ID"]==151983){
echo 'offset='.$cycleOffset;
echo "<pre  style='color:black; font-size:11px;'>";
print_r($keys);
echo "</pre>";
}     */

		$diffQuartalMod = $offset * -1; //Инвертируем сдвиг для капы

		$tmpCap = array();
		//Если сдвигаем назад, то капу нужно переносить вперед заранее и наоборот
		$cnt = 0;
		foreach ($arPeriods as $per => $val) {
			$tmpCap[$cnt] = $arPeriods[$per]["Прошлая капитализация"];
			$cnt++;
		}
		$tmpCap = $this->parallax($diffQuartalMod, $tmpCap); //Делаем сдвиг на инвертированное кол-во периодов

		$cnt = 0;
		foreach ($arPeriods as $per => $val) {
			$arPeriods[$per]["Прошлая капитализация"] = $tmpCap[$cnt];
			$cnt++;
		}

		$arResult = $arPeriods;
		return $arResult;
	}

	/**
	 * Сдвигает значения массива на $x позиций
	 * @param $x
	 * @param $array
	 * @return array
	 */
	public function parallax($x, $array) {
		$keys   = array_keys($array);
		$i      = 0;
		$result = array();
		reset($array);
		while ($i != count($array)) {
			$result[($keys[$i]) + $x] = $array[$keys[$i]];
			next($array);
			$i++;
		}
		return $result;
	}

	//сортировка полей периодов
	public static function sortPeriodFields($data) {
		if (!$data) {
			return false;
		}

		$fields = array(
			"Выручка за год (скользящая)" => array(
				"sort" => 10,
			),
			"Прибыль за год (скользящая)" => array(
				"sort" => 20,
			),
			"Активы" => array(
				"sort" => 30,
			),
			"Оборотные активы" => array(
				"sort" => 40,
			),
			"Собственный капитал" => array(
				"sort" => 50,
			),
			"Выручка  тек." => array(
				"sort" => 60,
				"name" => "Выручка (Отчет)",
			),
			"Выручка прошл. Год" => array(
				"sort" => 70,
				"name" => "Выручка (Прошлый год)",
			),
			"Прибыль" => array(
				"sort" => 80,
				"name" => "Прибыль (Отчет)",
			),
			"Выручка квартальная" => array(
				"sort" => 90,
				"name" => "Выручка (Квартальная)",
			),
			"Прибыль квартальная" => array(
				"sort" => 100,
				"name" => "Прибыль (Квартальная)",
			),
			"Выручка полугодовая" => array(
				"sort" => 110,
			),
			"Прибыль полугодовая" => array(
				"sort" => 120,
			),
			"EBITDA" => array(
				"sort" => 130,
			),
			"Темп роста прибыли" => array(
				"sort" => 160,
				"name" => "Темп прироста прибыли, %",
			),
			"Темп прироста выручки" => array(
				"sort" => 170,
				"name" => "Темп прироста выручки, %",
			),
			"Темп роста активов" => array(
				"sort" => 180,
				"name" => "Темп прироста активов, %",
			),
			"Рентабельность собственного капитала" => array(
				"sort" => 190,
				"name" => "Рентабельность собственного капитала, %",
			),
			"Доля собственного капитала в активах" => array(
				"sort" => 200,
				"name" => "Доля собственного капитала в активах, %",
			),
			"DEBT/EBITDA" => array(
				"sort" => 210,
			),
			"Доля СК" => array(
				"hide" => true,
			),
			"Прошлая капитализация" => array(
				"sort" => 220,
			),
			"P/E" => array(
				"sort" => 230,
			),
			"P/B" => array(
				"sort" => 240,
			),
			"P/Equity" => array(
				"sort" => 250,
			),
			"P/Sale" => array(
				"sort" => 260,
			),
			"ВРП" => array(
				"sort" => 270,
			),
			"Государственный долг" => array(
				"sort" => 280,
			),
			"Доля долга в ВРП, в %" => array(
				"sort" => 290,
			),
			"Доходы консолидированного бюджета" => array(
				"sort" => 300,
			),
			"Расходы консолидированного бюджета" => array(
				"sort" => 310,
			),
			"Дефицит/профицит" => array(
				"sort" => 320,
			),
			"Дефицит / профицит в % от доходов" => array(
				"sort" => 330,
			),
			"EV/EBITDA" => array(
				"sort" => 340,
			),
			"PEG" => array(
				"sort" => 350,
			),
			"Дивиденды об. (скользящий расчет), руб." => array(
				"sort" => 360,
			),
			"Дивиденды об. (скользящий расчет), %" => array(
				"sort" => 370,
			),
			"Дивиденды пр. (скользящий расчет), руб." => array(
				"sort" => 380,
			),
			"Дивиденды пр. (скользящий расчет), %" => array(
				"sort" => 390,
			),
		);

		$tmp = [];
		foreach ($data as $k => $v) {
			$tmp[] = $k;
		}

		usort($tmp, function ($a, $b) use ($fields) {
			if ($fields[$a]["sort"]) {
				$sortA = $fields[$a]["sort"];
			} else {
				$sortA = 100;
			}
			if ($fields[$b]["sort"]) {
				$sortB = $fields[$b]["sort"];
			} else {
				$sortB = 100;
			}

			if ($sortA == $sortB) {
				return 0;
			}
			return ($sortA < $sortB) ? -1 : 1;
		});

		$tmp2 = [];
		foreach ($tmp as $n => $item) {
			$tmp2[$item] = $item;
		}
		$tmp = $tmp2;

		foreach ($tmp as $n => $item) {
			if ($fields[$item]["name"]) {
				$tmp[$n] = $fields[$item]["name"];
			}
			if ($fields[$item]["hide"]) {
				unset($tmp[$n]);
			}
		}

		return $tmp;
	}
}
