<? /**
 * Работа с курсами валют
 *
 * Получает от ЦБ РФ курсы валют, а так же курсы редких валют к доллару США
 *
 * @copyright 2020
 * @version   0.1.1
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     35 class CCurrency
 *     62   function __construct($date=0)
 *     93   function setClassDate($date, $noRare=false)
 *     99   function getClassDate()
 *    110   function getSOAPDate($timeStamp, $withTime = false)
 *    119   function getXMLRareCurrencyCodes($date)
 *    139   function getXML($date)
 *    157   function getRareCurrencyCodes($date)
 *    174   function isRareCurrency($code)
 *    186   function getCurrencyCodes($date)
 *    205   function getRareXML($date)
 *    226   function getRate($currencyCode, $full=true)
 *    245   function getRateToParser()
 *    255   function getRateFromLocalDB($classDate, $code=)
 *    293   function getOldMethodCourse($currencyCode)
 *    310   function setRareCurrencyValues($date)
 *
 * TOTAL FUNCTIONS: 15
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */

class CCurrency {

 // WSDL службы Центробанка
 protected $WSDL = "http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL";
 // Экземпляр класса SoapClient
 protected $soap;
 //Дата на которую получаем курсы, задается в конструкторе
 public $classDate = "";
 // Дата запроса в формате SOAP
 protected $soapDatedate = "";
 // XML ответа Веб-службы
 protected $soapRareCodesResponse = "";
 protected $soapRareCurrencyResponse = "";
 // Ассоциативный массив с кодами валют
 public $currencyCodes = array();
 // Ассоциативный массив с кодами редких валют
 public $rareCurrencyCodes = array();
  // Массив с символьными кодами редких валют
 public $rareCurrencyCharCodes = array();
  // Ассоциативный массив с курсами редких
 public $rareCurrencyValues = array();
 public $arBaseHLCurrencies = array();
 public $hlCurrencyId = 31;
 public $error;


 // Первоначальная инициализация
 public function __construct($date=0)
 {
	 try {
		 $this->soap = new SoapClient($this->WSDL);
	 } catch(Exception $e){
		 CLogger::CCurrency_error_log(print_r($e->getMessage(), true));
		 $this->error = $e->getMessage();
	 } finally {

	 }

		 if ($date==0 ) {
			 $date = time();
		 } else {
			 $date = (new DateTime($date))->getTimestamp();
		 }
		 $this->setClassDate(date('d.m.Y', $date));




 }


 /**
  * Позволяет переназначить дату для получения курсов без пересоздания класса
  *
  * @param  DateTime   $date - объект DateTime c устанавливаемой датой
  *
  * @access public
  */
 public function setClassDate($date, $noRare=false){
	$this->classDate = (new DateTime($date))->getTimestamp();
	//if($noRare==false)   //TODO Починить редкие валюты
	  //$this->getRareCurrencyCodes($this->classDate);
 }

 public function getClassDate(){
	return (new DateTime(date("Y-m-d", $this->classDate)))->format('d.m.Y');
 }

 // Метод формирует строку с датой временем для SOAP вызода
 // http://www.w3.org/TR/xmlschema-2/#dateTime
 // Параметры:
 //   $timeStamp - дата/время в формате UNIX
 //   $withTime - необязательно если true,
 //                то преобразование вместе со временем суток,
 //                иначе только дата
 protected function getSOAPDate($timeStamp, $withTime = false)
 {
  $soapDate = date("Y-m-d", $timeStamp);
  return ($withTime) ?
   $soapDate .  "T" . date("H:i:s", $timeStamp) :
   $soapDate . "T00:00:00";
 }

 //Получает список кодов редких валют
 public function getXMLRareCurrencyCodes($date)
 {
  // Строка даты, на которую производится вызов
  $currentDate = $this->getSOAPDate($date);
  // Если предыдущий запрос службы был не на эту дату...
  if ($currentDate != $this->soapDateRareCurrencyCodes && empty($this->error))
  {
   // Вызов Веб-службы
   $this->soapDateRareCurrencyCodes = $currentDate;
   $params["On_date"] = $currentDate;
   //$response = $this->soap->GetReutersCursOnDate($params);
   $response = $this->soap->EnumReutersValutes($params);
   $this->soapRareCodesResponse = $response->EnumReutersValutesResult->any;
  }
  return  $this->soapRareCodesResponse;
 }

 // Метод возвращает XML строку с результатами вызова Веб-службы
 // Параметры:
 //   $date - дата, на которую производится запрос, 0 - сегодня
 public function getXML($date)
 {
  // Строка даты, на которую производится вызов
  $currentDate = $this->getSOAPDate($date);
  // Если предыдущий запрос службы был не на эту дату...
  if ($currentDate != $this->soapDate && empty($this->error))
  {
   // Вызов Веб-службы
   $this->soapDate = $currentDate;
   $params["On_date"] = $currentDate;
   $response = $this->soap->GetCursOnDateXML($params);
   $this->soapResponse = $response->GetCursOnDateXMLResult->any;
  }
  return  $this->soapResponse;
 }


 // Метод заполняет массив кодов валют
 protected function getRareCurrencyCodes($date)
 {
 	if(empty($this->error)) {
		$xml = simplexml_load_string($this->getXMLRareCurrencyCodes($date));
		$xPath = "//ReutersValutesList/EnumRValutes";
		$allCurrencies = $xml->xpath($xPath);
		foreach($allCurrencies as $currency) {
			$numCode = trim($currency->num_code);
			$charCode = trim($currency->char_code);
			$name = trim($currency->Title_ru);
			$this->rareCurrencyCodes[$numCode] = array("CHAR_CODE" => $charCode, "NAME" => $name);
			$this->rareCurrencyCharCodes[] = $charCode;
		}
	}//error empty
 }

 //Проверяет является ли валюта редкой или нет
 public function isRareCurrency($code){
 	$return = false;
 	if(in_array($code, $this->rareCurrencyCharCodes)){
 		$return = true;
 	}
	return $return;
 }

 // Метод заполняет массив кодов валют, по данным на текущий день
 //DIR - котировка
 //0 - Прямая – количество заданной валюты за один доллар США.
 //1 - Обратная - количество долларов США за единицу заданной валюты
 public function getCurrencyCodes($date)
 {

  $xml = simplexml_load_string($this->getXML($date));
  $xPath = "/ValuteData/ValuteCursOnDate";
  $allCurrencies = $xml->xpath($xPath);
  foreach ($allCurrencies as $currency)
  {
   $code = trim($currency->Vcode);
   $charCode = trim($currency->VchCode);
   $name = trim($currency->Vname);
	$nom = trim($currency->Vnom);
	$curs = trim($currency->Vcurs);
	$curs_nom = $curs/$nom;
   $this->currencyCodes[$charCode] = array("NUM_CODE"=>$code, "NAME"=>$name, "NOM"=>$nom, "ORIG_CURS"=>$curs, "CURS"=>$curs_nom, "DIR"=>"0", "CURRENCY"=>"RUB");
  }
  //CLogger::getCurrencyCodes(print_r($allCurrencies, true));
 }

 public function getRareXML($date)
 {
  // Строка даты, на которую производится вызов
  $currentDate = $this->getSOAPDate($date);
  // Если предыдущий запрос службы был не на эту дату...
  if ($currentDate != $this->soapDateRareCursOnDate)
  {
   // Вызов Веб-службы
   $this->soapDateRareCursOnDate = $currentDate;
   $params["On_date"] = $currentDate;
   $response = $this->soap->GetReutersCursOnDate($params);
   $this->soapRareCurrencyResponse = $response->GetReutersCursOnDateResult->any;
  }

  return  $this->soapRareCurrencyResponse;
 }

  // Метод возвращает курс указанной валюты
 // Параметры:
 //   $currencyCode - код валюты: USD, EUR и т.п.
 //   $full - true - вернуть полный масси, иначе только курс
 public function getRate($currencyCode, $full=true)
 {
  $return = '';
  if(empty($this->error)) {
	  $this->getRateFromLocalDB($this->classDate, $currencyCode);
	  //$this->getCurrencyCodes($this->classDate);
	  //$this->setRareCurrencyValues($this->classDate);
  } else {
	  $this->getOldMethodCourse($currencyCode);
  }
	 //$this->getOldMethodCourse($currencyCode);
  if($full){
	 $return = $this->currencyCodes[$currencyCode];
  } else {
	 $return = $this->currencyCodes[$currencyCode]["CURS"];
  }
  return $return;
 }

 public function getRateToParser(){
  if(empty($this->error)) {
	  //$this->getRateFromLocalDB($this->classDate);
	  $this->getCurrencyCodes($this->classDate);
	  $this->setRareCurrencyValues($this->classDate);
  } else {
	  $this->getOldMethodCourse($currencyCode);
  }
 }

 public function getRateFromLocalDB($classDate, $code=''){
 	 	   $classDate = date("d.m.Y", $classDate);
			$hlblock_id        = 44; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter = array('UF_DATE' => $classDate);
			if(!empty($code)){
				$arFilter["UF_CODE"] = $code;
			}
			$rsData = $entity_data_class::getList(array(
				'select' => array('*'),
				'filter' => $arFilter
			));
			$arReturn = array();
			while ($el = $rsData->fetch()) {
				$this->currencyCodes[$el["UF_CODE"]] = array(
					"NUM_CODE"=>"",
					"NAME"=>$el["UF_NAME"],
					"NOM"=>$el["UF_NOMINAL"],
					"ORIG_CURS"=>$el["UF_RATE"],
					"CURS"=>$el["UF_RATE"],
					"DIR"=>$el["UF_DIR"],
					"CURRENCY"=>$el["UF_RATE_CURRENCY"],
					"RARE"=>$el["UF_RARE"],
				);
			}

/*	  echo "<pre  style='color:black; font-size:11px;'>";
			   print_r($classDate);
			   print_r($this->currencyCodes);
			   echo "</pre>";*/
 }

	/**Получение курсов старым способом на случай отвала методов soap
	 * @param $currencyCode
	 */
 public function getOldMethodCourse($currencyCode){
 	  $cursDate = date('d.m.Y',$this->classDate);

	   $data = simplexml_load_string(file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" . $cursDate));
		foreach ($data->Valute as $valute) {
	 
			if ($valute->CharCode == $currencyCode) {
				$valute = (array) $valute;

				$valute_price = str_replace(",", ".", $valute["Value"]) / $valute["Nominal"];
				$this->currencyCodes[$currencyCode] = array("NUM_CODE"=>$currencyCode, "NAME"=>$currencyCode, "NOM"=>$valute["Nominal"], "ORIG_CURS"=>$valute["Value"], "CURS"=>$valute_price, "DIR"=>"0", "CURRENCY"=>"RUB");
				break;
			}
	}
 
}
 // Метод заполняет массив кодов валют, по данным на текущий день
 protected function setRareCurrencyValues($date)
 {
	//Если у ЦБ нет курсов редких валют на выходные то меняем дату пока не найдем курсы последние существующие
  	$find = false;
	$cntIterations = 0;
	 $dt_obj = new DateTime(date('d.m.Y', $date));

	 $finded = true;
    if (isWeekEndDay($dt_obj->format("d.m.Y"))) {
        $finded = false;

        while (!$finded) {
            $dt_obj->modify("-1 days");
            if (!isWeekEndDay($dt_obj->format("d.m.Y"))) {
                $finded = true;
            }
        }
    }
  if($finded){
  $xml = simplexml_load_string($this->getRareXML($dt_obj->getTimestamp()));
  $xPath = "//ReutersValutesData/Currency";
  $allCurrencies = $xml->xpath($xPath);
  }


  foreach ($allCurrencies as $currency)
  {
   $code = trim($currency->num_code);
   $curs = trim($currency->val);
   $dir = trim($currency->dir);
	$nom = 1;
   $arCharCode = $this->rareCurrencyCodes[$code];
	$arValue = array("NUM_CODE"=>$code, "NAME"=>$arCharCode["NAME"], "NOM"=>$nom, "CURS"=>$curs, "DIR"=>$dir, "CURRENCY"=>"USD");
	if($dir==1){ //Если курс обратный то пересчитываем в прямой
	 $arValue["ORIG_CURS"] = $arValue["CURS"];
	 //$arValue["DIR"] = 1/$arValue["CURS"];
	 $arValue["ORIG_DIR"] = $arValue["DIR"];
	 $arValue["DIR"] = 0;
	 //CLogger::RareCurrency(print_r($currency, true));
	}
	$arValue["CURS"] = $arValue["CURS"]/$arValue["NOM"];
	$this->currencyCodes[$arCharCode["CHAR_CODE"]] = $arValue;

  }
 }


}
