<?php

use Bitrix\Highloadblock as HL;

/**
 * Class CIndustriesRus
 */
class CIndustriesRus
{
    public $arOriginalsItems = array();
    public $arEmitentsBySector = array(); //Список эмитентов и их принадлежности к секторам
    public $actionsIblockId = 32; //Id инфоблока акций РФ
    public $fixHLIblockId = 43; //Id HL инфоблока с ручными правками
    public $industriesRusIblockId = 72; //Id инфоблока с отраслями РФ
    private $actionsArrForPvot = array();
    public $arFixParams = array(); //Данные ручных правок
    public $arParamsList = array(); //Список параметров для которых считать средние показатели
    public $arKvartalToMonth = array();
    public $arAllIndustriesSumm = array();//Суммарные показатели всех отраслей
    public $arIndustryCurrentPeriods = array();//Только текущие периоды по секторам
    public $arIndustryMinMax = array();//Min и Max данные посекторно для вывода ползунков на стр. акций
    public $arMonthToKvartal = array(1  => 1,
                                     2  => 1,
                                     3  => 1,
                                     4  => 2,
                                     5  => 2,
                                     6  => 2,
                                     7  => 3,
                                     8  => 3,
                                     9  => 3,
                                     10 => 4,
                                     11 => 4,
                                     12 => 4
    );
    public $arMonthRevers = array(
          "Янв." => "01",
          "Фев." => "02",
          "Мар." => "03",
          "Апр." => "04",
          "Май." => "05",
          "Июн." => "06",
          "Июл." => "07",
          "Авг." => "08",
          "Сен." => "09",
          "Окт." => "10",
          "Ноя." => "11",
          "Дек." => "12",
          "янв." => "01",
          "фев." => "02",
          "мар." => "03",
          "апр." => "04",
          "май." => "05",
          "июн." => "06",
          "июл." => "07",
          "авг." => "08",
          "сен." => "09",
          "окт." => "10",
          "ноя." => "11",
          "дек." => "12",
        );
    public $currentPeriodMonth = 1; //Цифра текущего месяца для добавления текущего периода
    
    function __construct($deleteTmpFixesForRealValues = false)
    {
        global $APPLICATION;
        CModule::IncludeModule("highloadblock");
        CModule::IncludeModule("iblock");
        $this->arParamsList = array(
          'Активы',
          'Оборотные активы',
          'Собственный капитал',
          'Прошлая капитализация',
          'Прибыль за год (скользящая)',
          'Выручка за год (скользящая)'
        );
        $this->arKvartalToMonth = $APPLICATION->quartDates;
        $this->currentPeriodMonth = intval(date('m'));
        $this->setData($deleteTmpFixesForRealValues);
        
    }
    
    
    
    /**
     * собирает данные
     * @param false $deleteTmpFixesForRealValues true приведет к сбросу кеша перед сборкой данных
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function setData($deleteTmpFixesForRealValues = false)
    {
        global $APPLICATION;
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheId = "industries_rus_data";
        if ($deleteTmpFixesForRealValues) {
            $cache->clean($cacheId);
        }
        
        $cacheTtl = 86400 * 7;
        
        if ($cache->read($cacheTtl, $cacheId)) {
            $arItemsCached = $cache->get($cacheId);
        } else {
            $arFixHandData = array();
            $arFixHandData = $this->getHLFixData(true);//Заполняем данные ручных правок
            
            $arRealFixData = array();//Данные о том, что для временного значения появилось реальное значение
            
            $arItems = array();
            $arFilter = array(
              "IBLOCK_ID"                     => $this->actionsIblockId,
              "!=PROPERTY_BOARDID"            => $APPLICATION->ExcludeFromRadar["акции"],
              "!PROPERTY_IS_ETF_ACTIVE_VALUE" => "Y",
              "!PROPERTY_IS_PIF_ACTIVE_VALUE" => "Y",
            );
            $arSelect = array(
              "ID",
              "IBLOCK_ID",
              "CODE",
              "NAME",
              "PROPERTY_ISSUECAPITALIZATION",
              "PROPERTY_PROP_DIVIDENDY_2018",
              "PROPERTY_LASTPRICE",
              "PROPERTY_HIDEN",
              "PROPERTY_SECID",
              "PROPERTY_PROP_SEKTOR",
              "PROPERTY_EMITENT_ID",
              "PROPERTY_PROP_TIP_AKTSII"
            );
            $res = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
            
            // Выберем обыкновенные акции (при наличии привилегированных
            $arActionCodes = array();
            $arAllActions = array();
            while ($item = $res->fetch()) {
            	if(!empty($item["PROPERTY_HIDEN_VALUE"])) continue;
                $arActionCodes[$item["PROPERTY_EMITENT_ID_VALUE"]][$item["PROPERTY_SECID_VALUE"]] = $item["PROPERTY_PROP_TIP_AKTSII_VALUE"];
                $arAllActions[$item["PROPERTY_SECID_VALUE"]] = $item;
            }
            
            
            foreach ($arActionCodes as $EmitentId => $arSecidType) {
                if (count($arSecidType) > 1) {
                    $arTmp = array();
                    foreach ($arSecidType as $iSecid => $iType) {//Перебираем акции в массиве ищем обыкновенные и добавляем во временный массив
                        if ($iType == 'Обыкновенная') {
                            $arTmp[$iSecid] = $iType;
                        }
                    }
                    if (count($arTmp) <= 0) { //Если не нашли обыкновенных - перебираем еще раз и добавляем привилегированные
                        foreach ($arSecidType as $iSecid => $iType) {
                            if ($iType == 'Привилегированная') {
                                $arTmp[$iSecid] = $iType;
                            }
                        }
                    }
                }
                if (count($arTmp) > 0) {
                    $arActionCodes[$EmitentId] = $arTmp;
                    unset($arTmp);
                }
            }
            
            $arResult = [];
				$arResultMinMax = array();
            $arActionsResult = array();
            $arEmitentsBySector = array();
            $resA = new Actions();
            $defaultArr = [
              'Прошлая капитализация'                => 0,
              'Прибыль за год (скользящая)'          => 0,
              'Выручка за год (скользящая)'          => 0,
              'P/E'                                  => 0,
              'Собственный капитал'                  => 0,
              'Активы'                               => 0,
              'Оборотные активы'                     => 0,
              'Рентабельность собственного капитала' => 0,
              'profit_temp'                          => 0,
              'proceeds_temp'                        => 0,
            ];
            
            
            
            //Cумма текущих капитализаций и дивидендов по секторам
            $arSectorsCapa = array();
            $arSectorsDivs = array();  //Сумма дивидендов по секторам
            $arSectorsPricesSumm = array(); //Сумма цен закрытия по секторам
            foreach ($arAllActions as $item) {
                //Если акция находится в отобранном по типу акций списке тикеров то обрабатываем ее и считаем
                if (array_key_exists($item['PROPERTY_SECID_VALUE'],
                  $arActionCodes[$item["PROPERTY_EMITENT_ID_VALUE"]])) {
                    if ($item['PROPERTY_PROP_SEKTOR_VALUE']) {
                        //if ($item['PROPERTY_PROP_SEKTOR_VALUE'] && $item['PROPERTY_ISSUECAPITALIZATION_VALUE']) {
                        $actionItem = $resA->getItem($item["CODE"]);
                        if ($actionItem["PERIODS"]) {
                            //Убираем текущий период у акций
                            $curPerName = '';
                            foreach($actionItem["PERIODS"] as $actPerName=>$actPerVal){
                                if($actPerVal["CURRENT_PERIOD"]==true){
                                  $curPerName = $actPerName;
                                  break;
                                }
                            }
                            if(!empty($curPerName))
                             unset($actionItem["PERIODS"][$curPerName]);
                            //----

/*        global $USER;
        $rsUser = CUser::GetByID($USER->GetID());
        $arUser = $rsUser->Fetch();
        if($arUser["LOGIN"]=="freud"){
$firephp = FirePHP::getInstance(true);
$it = $actionItem;
unset($it["PERIODS"]);
$firephp->fb($it,FirePHP::LOG);
        }*/

                            $arActionsResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$actionItem['NAME']] = array(
                              "ACTION_CODE" => $item["CODE"],
                              "SECID"       => $actionItem["PROPS"]["SECID"],
                              "PERIODS"     => $actionItem['PERIODS']
                            );
                            
                            if ($arFixHandData[$item['PROPERTY_PROP_SEKTOR_VALUE']][$actionItem["PROPS"]["SECID"]]) {
                                //Проверим есть значения для данного периода в базе
                                $arFixParamValues = $arFixHandData[$item['PROPERTY_PROP_SEKTOR_VALUE']][$actionItem["PROPS"]["SECID"]];
                                foreach ($arFixParamValues as $fPeriodYear => $fPeriodkvartals) {
                                    foreach ($fPeriodkvartals as $fPeriodKvartal => $fParams) {
                                        $checkPeriod = $fPeriodKvartal . '-' . $fPeriodYear . '-KVARTAL';
                                        if (!array_key_exists($checkPeriod, $actionItem["PERIODS"])) {
                                            $actionItem["PERIODS"][$checkPeriod] = $defaultArr;
                                        }
                                    }
                                }
                                
                            }
                            
                            foreach ($actionItem["PERIODS"] as $period => $periodArr) {

                                if (strpos($period, "MONTH") !== false) {
                                    continue;
                                }
                                //Проверим есть ли фикс значения для данного периода
                                $arPeriod = explode("-", $period);
                                foreach ($this->arParamsList as $paramName) {
                                    
                                    try {
                                        $arFixParamValues = $arFixHandData[$item['PROPERTY_PROP_SEKTOR_VALUE']][$actionItem["PROPS"]["SECID"]][$arPeriod[1]][$arPeriod[0]][$paramName];
                                        
                                        
                                        if ($arFixParamValues["UF_TYPE"] == "TMP") {
                                            if (floatval($periodArr[$paramName]) == 0) {
                                                $periodArr[$paramName] = $arFixParamValues["UF_VALUE"];
                                                $arActionsResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$actionItem['NAME']]["PERIODS"][$period][$paramName] = $arFixParamValues["UF_VALUE"];
                                            } else {
                                                $arRealFixData[] = $arFixParamValues["ID"];
                                            }
                                        } elseif ($arFixParamValues["UF_TYPE"] == "ALWAYS") {
                                            $periodArr[$paramName] = $arFixParamValues["UF_VALUE"];
                                            $arActionsResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$actionItem['NAME']]["PERIODS"][$period][$paramName] = $arFixParamValues["UF_VALUE"];
                                        }
                                    } catch (Exception $e) {
                                    
                                    }
                                    
                                }
                                
                                
                                /* echo $period."<pre  style='color:#000000; font-size:11px;'>";
                                   print_r($periodArr);
                                   echo "</pre>";*/
                                
                                if (!isset($arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period])) {
                                    $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period] = $defaultArr;
                                }
                                
                                if (!$arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['PERIOD_VAL']) {
                                    $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['PERIOD_VAL'] = $periodArr['PERIOD_VAL'];
                                }
                                
                                if (!$arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['PERIOD_YEAR']) {
                                    $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['PERIOD_YEAR'] = $periodArr['PERIOD_YEAR'];
                                }
                                
                                if (!$arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['PERIOD_TYPE']) {
                                    $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['PERIOD_TYPE'] = $periodArr['PERIOD_TYPE'];
                                }
                                
                                
                                $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['Прошлая капитализация'] += $periodArr['Прошлая капитализация'];
                                $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['Прибыль за год (скользящая)'] += $periodArr['Прибыль за год (скользящая)'];
                                $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['Выручка за год (скользящая)'] += $periodArr['Выручка за год (скользящая)'];
                                $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['Собственный капитал'] += $periodArr['Собственный капитал'];
                                $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['Активы'] += $periodArr['Активы'];
                                $arResult[$item['PROPERTY_PROP_SEKTOR_VALUE']][$period]['Оборотные активы'] += $periodArr['Оборотные активы'];
                            }

                             	  $curPeriod = reset($actionItem["PERIODS"]);

										  //Расчет минимумов и максимумов по основным показателям
										     $sector = $item['PROPERTY_PROP_SEKTOR_VALUE'];
											  $this->setMinMaxPeriodData($arResultMinMax, $curPeriod, $item, $sector, 'Выручка за год (скользящая)');
											  $this->setMinMaxPeriodData($arResultMinMax, $curPeriod, $item, $sector, 'Прибыль за год (скользящая)');
											  $this->setMinMaxPeriodData($arResultMinMax, $curPeriod, $item, $sector, 'Рентабельность собственного капитала');
											  $this->setMinMaxPeriodData($arResultMinMax, $curPeriod, $item, $sector, 'Доля собственного капитала в активах');
											  $this->setMinMaxPeriodData($arResultMinMax, $curPeriod, $item, $sector, 'P/E');
											  $this->setMinMaxPeriodData($arResultMinMax, $arResult, $actionItem, $sector, 'BETTA');
											  $this->setMinMaxPeriodData($arResultMinMax, $curPeriod, $item, $sector, 'Прошлая капитализация');
											  $this->setMinMaxPeriodData($arResultMinMax, $arResult, $actionItem, $sector, 'Дивиденды %');


                        }
                        
                    }
                }
                //Суммируем только текущую капу действующих акций, и только уже отобранных для случая, когда у эмитента несколько видов акций
                if (empty(trim($item['PROPERTY_HIDEN_VALUE'])) && array_key_exists($item['PROPERTY_SECID_VALUE'],
                    $arActionCodes[$item["PROPERTY_EMITENT_ID_VALUE"]])) {
                    $arSectorsCapa[$item['PROPERTY_PROP_SEKTOR_VALUE']] = $arSectorsCapa[$item['PROPERTY_PROP_SEKTOR_VALUE']] + floatval($item['PROPERTY_ISSUECAPITALIZATION_VALUE']);
                    $arSectorsDivs[$item['PROPERTY_PROP_SEKTOR_VALUE']] = $arSectorsDivs[$item['PROPERTY_PROP_SEKTOR_VALUE']] + floatval($item['PROPERTY_PROP_DIVIDENDY_2018_VALUE']);
						  if(floatval($item['PROPERTY_PROP_DIVIDENDY_2018_VALUE'])>0)
                    	$arSectorsPricesSumm[$item['PROPERTY_PROP_SEKTOR_VALUE']] = $arSectorsPricesSumm[$item['PROPERTY_PROP_SEKTOR_VALUE']] + floatval($item['PROPERTY_LASTPRICE_VALUE']);
                    //Соберем id эмитентов по секторам
                    $arEmitentsBySector[$item['PROPERTY_EMITENT_ID_VALUE']] = $item['PROPERTY_PROP_SEKTOR_VALUE'];
                }
                
            }
            
            
            foreach ($arResult as $sector => $periods) {
                
                foreach ($periods as $period => $periodArr) {
                    
                    $arResult[$sector][$period]['P/E'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Прибыль за год (скользящая)'];
                    $arResult[$sector][$period]['Рентабельность собственного капитала'] = ($arResult[$sector][$period]['Прибыль за год (скользящая)'] / $arResult[$sector][$period]['Собственный капитал']) * 100;
                    $arResult[$sector][$period]['Доля собственного капитала в активах'] = ($arResult[$sector][$period]['Собственный капитал'] / $arResult[$sector][$period]['Активы']) * 100;
                    $arResult[$sector][$period]['P/B'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Активы'];
                    $arResult[$sector][$period]['P/Equity'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Собственный капитал'];
                    $arResult[$sector][$period]['P/Sale'] = $arResult[$sector][$period]['Прошлая капитализация'] / $arResult[$sector][$period]['Выручка за год (скользящая)'];
                    
                }
                
                //Сортируем периоды
/*                uasort($arResult[$sector], function ($item1, $item2) {
                    $result = false;
                    if ($item1["PERIOD_TYPE"] == "KVARTAL" && $item1["PERIOD_YEAR"] > 2000 && $item2["PERIOD_YEAR"] > 2000) {
                        $dstr1 = $item1["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$item1["PERIOD_VAL"]];
                        $dt1 = new DateTime($dstr1);
                        $dstr2 = $item2["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$item2["PERIOD_VAL"]];
                        $dt2 = new DateTime($dstr2);
                        $result = $dt1 < $dt2;
                    }
                    return $result;
                });*/

					uasort($arResult[$sector], function ($a, $b) {
						$res = 0;

						// сравниваем кварталы в виде дат приведенных к 1 числу первого месяца каждого квартала
						if($a["PERIOD_TYPE"] == "KVARTAL" && $a["PERIOD_YEAR"] > 2000 && $b["PERIOD_YEAR"] > 2000){
						$addDateA = new DateTime($a["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$a["PERIOD_VAL"]] . '-01');
						$addDateB = new DateTime($b["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$b["PERIOD_VAL"]] . '-01');
						if ($addDateA != $addDateB) {
								return ($addDateA < $addDateB) ? 1 : -1; //desc
						}
						}

						return $res;
					});

                
            }
            
            $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $this->industriesRusIblockId), false, false,
              array(
                "ID",
                "IBLOCK_ID",
                "CODE",
                "NAME",
                "PROPERTY_HIDDEN",
                "PROPERTY_ISSUECAPITALIZATION"
              ));

            
            while ($sectorItem = $res->getNext()) {
                $sectorItem['PERIODS'] = $arResult[$sectorItem['NAME']];
					 //Считаем див. доходность по сектору
					 $div_yield = round(($arSectorsDivs[$sectorItem['NAME']]/$arSectorsPricesSumm[$sectorItem['NAME']])*100, 2);

					 $sectorItem["DIVS_YIELD"] = $div_yield;
                $sectorItem = $this->calculatePeriodData($sectorItem);

			//Вторая сортировка после добавления текущего периода. Без нее он не оказывается первым в массиве
					uasort($sectorItem['PERIODS'], function ($a, $b) {
						$res = 0;

						// сравниваем кварталы в виде дат приведенных к 1 числу первого месяца каждого квартала
						if($a["PERIOD_TYPE"] == "KVARTAL" && $a["PERIOD_YEAR"] > 2000 && $b["PERIOD_YEAR"] > 2000){
						$addDateA = new DateTime($a["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$a["PERIOD_VAL"]] . '-01');
						$addDateB = new DateTime($b["PERIOD_YEAR"] . "-" . $this->arKvartalToMonth[$b["PERIOD_VAL"]] . '-01');
						if ($addDateA != $addDateB) {
								return ($addDateA < $addDateB) ? 1 : -1; //desc
						}
						}

						return $res;
					});

                $arItems[] = $sectorItem;
                CIBlockElement::SetPropertyValuesEx($sectorItem["ID"], $this->industriesRusIblockId,
                  array("ISSUECAPITALIZATION" => round($arSectorsCapa[$sectorItem['NAME']] / 1000000, 2)));
                CIBlockElement::SetPropertyValuesEx($sectorItem["ID"], $this->industriesRusIblockId,
                  array("DIVIDENDS" => round($arSectorsDivs[$sectorItem['NAME']], 2)));
                CIBlockElement::SetPropertyValuesEx($sectorItem["ID"], $this->industriesRusIblockId,
                  array("LASTPRICE_SUMM_FOR_DIVS" => round($arSectorsPricesSumm[$sectorItem['NAME']], 2)));


					 CIBlockElement::SetPropertyValuesEx($sectorItem["ID"], $this->industriesRusIblockId,
                  array("DIVIDENDS_YIELD" => $div_yield));


                //$this->addCurrentPeriod($sectorItem['PERIODS'],$sectorItem);
    

                
            }



            $arFixHandData = $this->getHLFixData();//Заполняем данные ручных правок

            
            //Очистим временные правки, для которых появились реальные значения
            if (count($arFixHandData)) {
                $hlblock = HL\HighloadBlockTable::getById($this->fixHLIblockId)->fetch();
                $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                $entityClass = $entity->getDataClass();
                foreach ($arRealFixData as $fId) {
                    $entityClass::delete($fId);
                }
            }
            
            foreach($arItems as $sectorItem){
                foreach($sectorItem["PERIODS"] as $arPeriod){
                    if($arPeriod["CURRENT_PERIOD"]==true){
                        $arIndustryCurrentPeriods[$sectorItem["NAME"]] = $arPeriod;
                        break;
                    }
                }
            }
            
            $arAllIndustriesSumm = array();
            $arAllIndustriesSumm = $this->getCalculatedAllIndustries($arItems);//Расчет сводных итогов по стране из всех отраслей
            $arItemsCached = array(
              "arActionsResult"     => $arActionsResult,
              "arEmitentsBySector"  => $arEmitentsBySector,
              "arItems"             => $arItems,
              "arFixHandData"       => $arFixHandData,
              "arAllIndustriesSumm" => $arAllIndustriesSumm,
              "arIndustryCurrentPeriods" =>$arIndustryCurrentPeriods,
              "arIndustryMinMax" =>$arResultMinMax,
            );
            $cache->set($cacheId, $arItemsCached);
        }
        
        
        $this->arOriginalsItems = $arItemsCached["arItems"];
        $this->actionsArrForPvot = $arItemsCached["arActionsResult"];
        $this->arFixParams = $arItemsCached["arFixHandData"];
        $this->arEmitentsBySector = $arItemsCached["arEmitentsBySector"];
        $this->arAllIndustriesSumm = $arItemsCached["arAllIndustriesSumm"];
        $this->arIndustryCurrentPeriods = $arItemsCached["arIndustryCurrentPeriods"];
        $this->arIndustryMinMax = $arItemsCached["arIndustryMinMax"];

        
        unset($resA);
    }


	 /**
	  * Расчет min и max показателей по сектору для показа на страницах акций
	  *
	  * @param  array   $arResultMinMax (Passed by reference)
	  * @param  array   $curPeriod Массив с текущим периодом
	  * @param  array   $item Массив с данными акции
	  * @param  string   $sector Название сектора
	  * @param  string   $key Название параметра
	  *
	  * @access private
	  */
	 private function setMinMaxPeriodData(&$arResultMinMax, $curPeriod, $item, $sector, $key){
		  if(empty($arResultMinMax[$sector][$key]["MIN"])){
		  		$arResultMinMax[$sector][$key]["MIN"] = 0;
		  }
		  $val = 0;
		  if($key == "BETTA"){
			 $val = $item["PROPS"]["BETTA"];
		  } elseif ($key == "Дивиденды %"){
			 $val = $item["DYNAM"]["Дивиденды %"];
		  } else {
			 $val = $curPeriod[$key];
		  }

		  if($arResultMinMax[$sector][$key]["MAX"]<$val){
			 $aggregateName = "MAX";
		  } else {
			 $aggregateName = "MIN";
		  }
		  $arResultMinMax[$sector][$key][$aggregateName] = $val;
	 }

    /**
     * Расчет сводных итогов по стране из всех отраслей
     * @param $arItems
     * @return array
     */
    public function getCalculatedAllIndustries($arItems)
    {
        $arReturn = array();
        	CLogger::ArItems(print_r($arItems, true));
        foreach ($arItems as $name => $val) {//Отрасль
            foreach ($val["PERIODS"] as $period => $arData) {//Периоды
                $arPeriod = explode("-", $period);
                foreach ($arData as $paramName => $paramValue) {//Параметры периодов
                    if (in_array($paramName, array(
                      "P/E",
                      "P/B",
                      "P/Equity",
                      "P/Sale",
                      "PERIOD_VAL",
                      "PERIOD_YEAR",
                      "PERIOD_TYPE",
                      "Рентабельность собственного капитала",
                      "Доля собственного капитала в активах",
                      "Темп роста прибыли",
                      "Темп роста активов",
                      "Темп прироста выручки",
                    ))) {
                        continue;
                    }
                    //Период "4 кв. прошлого года"
                    if (isset($arReturn[$period]["PERIOD_VAL"]) && isset($arReturn[$period]["PERIOD_YEAR"])) {
                        $endlastYearPeriod = "4-" . ($arReturn[$period]["PERIOD_YEAR"] - 1) . "-KVARTAL";
                    } else {
                        $endlastYearPeriod = false;
                    }

                     $arReturn[$period][$paramName] += $paramValue;

					     if($paramName == 'CURRENT_PERIOD'){
							 $arReturn[$period]['CURRENT_PERIOD'] = $paramValue;
						  }

                    if (!isset($arReturn[$period]["PERIOD_VAL"])) {
                        $arReturn[$period]["PERIOD_VAL"] = $arPeriod[0];
                    }
                    if (!isset($arReturn[$period]["PERIOD_YEAR"])) {
                        $arReturn[$period]["PERIOD_YEAR"] = $arPeriod[1];
                    }
                    if (!isset($arReturn[$period]["PERIOD_TYPE"])) {
                        $arReturn[$period]["PERIOD_TYPE"] = $arPeriod[2];
                    }
                    //Считаем мульипликаторы при наличии показателя прошлой капитализации
                    if (isset($arReturn[$period]["Прошлая капитализация"])) {
                        if (isset($arReturn[$period]["Прибыль за год (скользящая)"]) && floatval($arReturn[$period]["Прибыль за год (скользящая)"]) > 0) {
                            $arReturn[$period]["P/E"] = $arReturn[$period]["Прошлая капитализация"] / $arReturn[$period]["Прибыль за год (скользящая)"];
                        }
                        
                        if (isset($arReturn[$period]["Активы"]) && floatval($arReturn[$period]["Активы"]) > 0) {
                            $arReturn[$period]['P/B'] = $arReturn[$period]['Прошлая капитализация'] / $arReturn[$period]['Активы'];
                        }
                        
                        if (isset($arReturn[$period]["Собственный капитал"]) && floatval($arReturn[$period]["Собственный капитал"]) > 0) {
                            $arReturn[$period]['P/Equity'] = $arReturn[$period]['Прошлая капитализация'] / $arReturn[$period]['Собственный капитал'];
                        }
                        
                        if (isset($arReturn[$period]["Выручка за год (скользящая)"]) && floatval($arReturn[$period]["Выручка за год (скользящая)"]) > 0) {
                            $arReturn[$period]['P/Sale'] = $arReturn[$period]['Прошлая капитализация'] / $arReturn[$period]['Выручка за год (скользящая)'];
                        }
                    }
                    if (floatval($arReturn[$period]["Прибыль за год (скользящая)"]) > 0 && floatval($arReturn[$period]["Собственный капитал"]) > 0) {
                        $arReturn[$period]['Рентабельность собственного капитала'] = ($arReturn[$period]['Прибыль за год (скользящая)'] / $arReturn[$period]['Собственный капитал']) * 100;
                    }
                    
                    if (floatval($arReturn[$period]["Собственный капитал"]) > 0 && floatval($arReturn[$period]["Активы"]) > 0) {
                        $arReturn[$period]['Доля собственного капитала в активах'] = ($arReturn[$period]['Собственный капитал'] / $arReturn[$period]['Активы']) * 100;
                    }
                    
                    //Период "год назад"
                    if (isset($arReturn[$period]["PERIOD_VAL"]) && isset($arReturn[$period]["PERIOD_YEAR"])) {
                        $lastYearPeriod = $arReturn[$period]["PERIOD_VAL"] . "-" . ($arReturn[$period]["PERIOD_YEAR"] - 1) . "-KVARTAL";
                    } else {
                        $lastYearPeriod = false;
                    }
                    
                    
                    //Темп прироста активов
                    if (isset($arReturn[$period]["Активы"]) && $lastYearPeriod != false && isset($arReturn[$lastYearPeriod]["Активы"])) {
                        $arReturn[$period]["Темп роста активов"] = round(($arReturn[$period]["Активы"] / $arReturn[$lastYearPeriod]["Активы"] - 1) * 100,
                          2);
                    }
                    //Темп роста прибыли
                    if (isset($arReturn[$period]["Прибыль за год (скользящая)"]) && $lastYearPeriod != false && isset($arReturn[$lastYearPeriod]["Прибыль за год (скользящая)"])) {
                        if ($arReturn[$period]["Прибыль за год (скользящая)"] < 0 && $arReturn[$lastYearPeriod]["Прибыль за год (скользящая)"] < 0) {
                            $arReturn[$period]["Темп роста прибыли"] = "";
                        } else {
                            $arReturn[$period]["Темп роста прибыли"] = (($arReturn[$period]["Прибыль за год (скользящая)"] / $arReturn[$lastYearPeriod]["Прибыль за год (скользящая)"]) - 1) * 100;
                            
                            if ($arReturn[$lastYearPeriod]["Прибыль за год (скользящая)"] < 0) {
                                $arReturn[$period]["Темп роста прибыли"] = abs($arReturn[$period]["Темп роста прибыли"]);
                            }
                        }
                    }


                    //Темп прироста выручки
                    if (isset($arReturn[$period]["Выручка за год (скользящая)"]) && isset($arReturn[$lastYearPeriod]["Выручка за год (скользящая)"])) {
                        $arReturn[$period]["Темп прироста выручки"] = round(($arReturn[$period]["Выручка за год (скользящая)"] / $arReturn[$lastYearPeriod]["Выручка за год (скользящая)"] - 1) * 100,
                          2);
                    }
                    
                    
                }//Параметры периодов

            }
            
        }
        
        /*      global $USER;
               $rsUser = CUser::GetByID($USER->GetID());
               $arUser = $rsUser->Fetch();
                if($arUser["LOGIN"]=="freud"){
                  echo "<pre  style='color:black; font-size:11px;'>";
                    print_r($arReturn);
                  echo "</pre>";
                }*/
        
        
        return $arReturn;
    }
    
    public function getItem($code, $needExpandedPeriods = false, $year = false)
    {
        if (empty($year)) {
            $year = false;
        }
        foreach ($this->arOriginalsItems as $item) {
            if ($item["CODE"] === $code) {
                $item = $needExpandedPeriods ? $this->getExpandedPeriods($item, $year) : $item;
                return $item;
            }
        }
        return false;
    }
    
    public function getSectorActionsData($sectorName)
    {
        foreach ($this->actionsArrForPvot as $k => $item) {
            
            if ($k === $sectorName) {
                return $item;
            }
        }
        return false;
    }
    
    public function GetSectorByEmitentId($emitentId = 0)
    {
        $arReturn = array();
        $sectorName = $this->arEmitentsBySector[$emitentId];
        foreach ($this->arOriginalsItems as $item) {
            if ($item["NAME"] === $sectorName) {
                unset($item["PERIODS"]);
                $arReturn = $item;
            }
        }
        
        return $arReturn;
    }
    
    /**
     * Возвращает из HL инфоблока список правок значений
     * @param false $forPeriods true вернет показатели в разрезе периодов (сектор-тикер-годы-кварталы-показатель),
     *                          false - периоды в разрезе показателей (сектор-тикер-показатель-годы-кварталы)
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function getHLFixData($forPeriods = false)
    {
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById($this->fixHLIblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();
        
        $arFilter = array("UF_COUNTRY" => "RUS");
        $resHL = $entityClass::getList(array(
          "select" => array("*",),
          "filter" => $arFilter,
          "order"  => array("UF_YEAR" => "ASC", "UF_KVARTAL" => "ASC"),
        ));
        $arHLFixData = $resHL->fetchAll();
        
        foreach ($arHLFixData as $k => $v) {
            if ($forPeriods == false) {
                $arResult[$v["UF_SECTOR_NAME"]][$v["UF_PARAM_NAME"]][$v["UF_ACTIVE_CODE"]][$v["UF_YEAR"]][$v["UF_KVARTAL"]] = array(
                  "ID"       => $v["ID"],
                  "UF_TYPE"  => $v["UF_TYPE"],
                  "UF_VALUE" => $v["UF_VALUE"]
                );
            } else {
                $arResult[$v["UF_SECTOR_NAME"]][$v["UF_ACTIVE_CODE"]][$v["UF_YEAR"]][$v["UF_KVARTAL"]][$v["UF_PARAM_NAME"]] = array(
                  "ID"       => $v["ID"],
                  "UF_TYPE"  => $v["UF_TYPE"],
                  "UF_VALUE" => $v["UF_VALUE"]
                );
            }
            
        }
        return $arResult;
    }
    
    public function calculatePeriodData($item)
    {

        $currentPeriodFlag = false;

		  reset($item["PERIODS"]);
        foreach ($item["PERIODS"] as $n => $val) {
            if($val["CURRENT_PERIOD"]==true) continue;
            if(array_key_exists("CURRENT_PERIOD", $val) && $val["CURRENT_PERIOD"]==true){
                $currentPeriodFlag = true;
            }
         
      
     
            $lastYearPeriod = '';
            $lastYearPeriod = $val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL";

            $lastKvartalPeriod = '';
            $lastKvartalPeriod = $val["PERIOD_VAL"] . "-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL";
            $endlastKvartalPeriod = "4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL";

				if($val["PERIOD_VAL"]==1){
				  $lastPeriodForGrowKvartal = "4-" . ($val["PERIOD_YEAR"] - 1) . "-KVARTAL";
				} else {
				  $lastPeriodForGrowKvartal = $val["PERIOD_VAL"]-1 . "-" . $val["PERIOD_YEAR"] . "-KVARTAL";
				}


            //Темп прироста активов
            if ($val["Активы"] && $item["PERIODS"][$lastYearPeriod]["Активы"]) {
                $item["PERIODS"][$n]["Темп роста активов"] = round(($val["Активы"] / $item["PERIODS"][$lastYearPeriod]["Активы"] - 1) * 100,
                  2);
            }
            //Темп роста прибыли
            if ($val["Прибыль за год (скользящая)"] && $item["PERIODS"][$lastYearPeriod]["Прибыль за год (скользящая)"]) {
                if ($val["Прибыль за год (скользящая)"] < 0 && $item["PERIODS"][$lastYearPeriod]["Прибыль за год (скользящая)"] < 0) {
                    $item["PERIODS"][$n]["Темп роста прибыли"] = "";
                } else {
                    $item["PERIODS"][$n]["Темп роста прибыли"] = (($val["Прибыль за год (скользящая)"] / $item["PERIODS"][$lastYearPeriod]["Прибыль за год (скользящая)"]) - 1) * 100;
                    
                    if ($item["PERIODS"][$lastYearPeriod]["Прибыль за год (скользящая)"] < 0) {
                        $item["PERIODS"][$n]["Темп роста прибыли"] = abs($item["PERIODS"][$n]["Темп роста прибыли"]);
                    }
                }
            }

            //Темп прироста капитализации к началу года
            if ($val["Прошлая капитализация"] && $item["PERIODS"][$endlastKvartalPeriod]["Прошлая капитализация"]) {
                $item["PERIODS"][$n]["Темп прироста капитализации с начала года"] = round(($val["Прошлая капитализация"] / $item["PERIODS"][$endlastKvartalPeriod]["Прошлая капитализация"] - 1) * 100, 2);
            }
		      //Темп прироста капитализации с прошлого квартала
		      if ($val["Прошлая капитализация"] && $item["PERIODS"][$lastPeriodForGrowKvartal]["Прошлая капитализация"]) {
		         $item["PERIODS"][$n]["Темп прироста капитализации с прошлого квартала"] = round(($val["Прошлая капитализация"] / $item["PERIODS"][$lastPeriodForGrowKvartal]["Прошлая капитализация"] - 1) * 100, 2);
		      }

            //Темп прироста выручки
            if ($val["Выручка за год (скользящая)"] && $item["PERIODS"][$lastYearPeriod]["Выручка за год (скользящая)"]) {
                $item["PERIODS"][$n]["Темп прироста выручки"] = round(($val["Выручка за год (скользящая)"] / $item["PERIODS"][$lastYearPeriod]["Выручка за год (скользящая)"] - 1) * 100,
                  2);
            }


        }
        if(\Bitrix\Main\Config\Option::get("grain.customsettings","ADD_CURRENT_PERIOD")=="Y" ) {
             $this->addCurrentPeriod($item['PERIODS'],$item);
        }
        
        return $item;
    }
    
    /**
     * Добавляет текущий период (все данные из последнего существующего периода, а капа из акции текущая + мультики считаются)
     * @param $arPeriods
     * @param $item
     */
    public function addCurrentPeriod(&$arPeriods, $item)
    {
        //  $arPeriods = array_reverse($arPeriods);
        $arLastPeriod = reset($arPeriods);

		  //	CLogger::calculatePeriodData(print_r($arPeriods, true));
        $arrPeriodName = array_keys($arPeriods);
        $arrPeriodName = reset($arrPeriodName);
        $arPeriodName = explode("-", $arrPeriodName);
        unset($arrPeriodName);
		  $lastPeriodForGrowKvartal = reset(array_keys($arPeriods)); //Предыдущий период (строка) для расчета приростов от прошлого квартала
		  $tmp = implode(", ",array_keys($arPeriods));
        $arPeriods = array_reverse($arPeriods);


        
        $arLastPeriod["CURRENT_PERIOD"] = true;
        //Капитализация из акции текущая
        $arLastPeriod["Прошлая капитализация"] = $item["PROPERTY_ISSUECAPITALIZATION_VALUE"] ;
        //$arLastPeriod["Дивидендная доходность"] = $item["DIVS_YIELD"] ;


        //$currPeriodName = $this->arMonthToKvartal[$this->currentPeriodMonth] . "-" . $arPeriodName[1] . "-" . $arPeriodName[2];
        $currPeriodName = $this->arMonthToKvartal[$this->currentPeriodMonth] . "-" . date('Y') . "-" . $arPeriodName[2];

        //$arLastPeriod["PERIOD_YEAR"] = $arPeriodName[1];
        $arLastPeriod["PERIOD_YEAR"] = date('Y');
        $arLastPeriod["PERIOD_VAL"] = $this->arMonthToKvartal[$this->currentPeriodMonth];
        $arPeriods[$currPeriodName] =  $arLastPeriod;
		  $endlastKvartalPeriod = "4-" . ($arLastPeriod["PERIOD_YEAR"] - 1) . "-KVARTAL";
        unset($arLastPeriod);
        
        //Расчет мультипликаторов
        //P/E
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Прибыль за год (скользящая)"]) {
            $arPeriods[$currPeriodName]["P/E"] = round($arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Прибыль за год (скользящая)"],
              2);
        }
        
        //P/B
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Активы"]) {
            $arPeriods[$currPeriodName]["P/B"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Активы"];
        }
        
        //P/Equity
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Собственный капитал"]) {
            $arPeriods[$currPeriodName]["P/Equity"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Собственный капитал"];
        }
        
        //P/Sale
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$currPeriodName]["Выручка за год (скользящая)"]) {
            $arPeriods[$currPeriodName]["P/Sale"] = $arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$currPeriodName]["Выручка за год (скользящая)"];
        }
        
        if ($arPeriods[$currPeriodName]["P/E"] > 0 && $arPeriods[$currPeriodName]["AVEREGE_PROFIT"] > 0) {
            //$arPeriods[$currPeriodName]["AVEREGE_PROFIT"] = floatval($averageProfitSum / 5);
            $arPeriods[$currPeriodName]["PEG"] = floatval($arPeriods[$currPeriodName]["P/E"]) / floatval($arPeriods[$currPeriodName]["AVEREGE_PROFIT"]);
        }
        
        //EV/EBITDA
        if ($arPeriods[$currPeriodName]["EBITDA"]) {
            $arPeriods[$currPeriodName]["EV/EBITDA"] = floatval($arPeriods[$currPeriodName]["Прошлая капитализация"] + $arPeriods[$currPeriodName]["Активы"] - $arPeriods[$currPeriodName]["Собственный капитал"]) / floatval($arPeriods[$currPeriodName]["EBITDA"]);
            if ($arPeriods[$currPeriodName]["EV/EBITDA"] == INF) {
                $arPeriods[$currPeriodName]["EV/EBITDA"] = 0;
            }
        }
        //DEBT/EBITDA
        if ($arPeriods[$currPeriodName]["EBITDA"]) {
            $arPeriods[$currPeriodName]["DEBT/EBITDA"] = floatval($arPeriods[$currPeriodName]["Активы"] - $arPeriods[$currPeriodName]["Собственный капитал"]) / floatval($arPeriods[$currPeriodName]["EBITDA"]);
            if ($arPeriods[$currPeriodName]["DEBT/EBITDA"] == INF) {
                $arPeriods[$currPeriodName]["DEBT/EBITDA"] = 0;
            }
        }

        //Темп прироста капитализации к началу года
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$endlastKvartalPeriod]["Прошлая капитализация"]) {
           $arPeriods[$currPeriodName]["Темп прироста капитализации с начала года"] = round(($arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$endlastKvartalPeriod]["Прошлая капитализация"] - 1) * 100, 2);
        }
        //Темп прироста капитализации с прошлого квартала
        if ($arPeriods[$currPeriodName]["Прошлая капитализация"] && $arPeriods[$lastPeriodForGrowKvartal]["Прошлая капитализация"]) {
          $arPeriods[$currPeriodName]["Темп прироста капитализации с прошлого квартала"] = round(($arPeriods[$currPeriodName]["Прошлая капитализация"] / $arPeriods[$lastPeriodForGrowKvartal]["Прошлая капитализация"] - 1) * 100, 2);
        }

		  if($item["DIVS_YIELD"]>0){
			 $arPeriods[$currPeriodName]["Дивидендная доходность"] = $item["DIVS_YIELD"];
		  }
        //$arPeriods = $this->calculateCapa($arPeriods, $item, true,$currPeriodName);
        $arPeriods = array_reverse($arPeriods);
        $this->arIndustryCurrentPeriods[$item["NAME"]] = $arPeriods[$currPeriodName];
        unset($arPeriodName, $currPeriodName);
        //return $arPeriods;
    }
    
    function getExpandedPeriods($arResult, $year = false)
    {
        $limit = 5;
        if (intval($year)) {
            $limit = 4;
        }
        if (is_string($year) && $year == 'all') {
            $limit = 999;
        }
        if ($arResult['PERIODS']) {
            foreach ($arResult['PERIODS'] as $period => $vals) {
                if (strpos($period, "MONTH") !== false) {
                    continue;
                }
                if ($year != false && intval($year) > 0) {
                    if ($vals["PERIOD_YEAR"] != $year) {
                        continue;
                    }
                }
                $arResult["EXPANDED_PERIODS"]["PERIODS"][] = $period;
                
                if (count($arResult["EXPANDED_PERIODS"]["PERIODS"]) >= $limit) {
                    break;
                }
            }
            foreach ($arResult["EXPANDED_PERIODS"]["PERIODS"] as $period) {
                foreach ($arResult['PERIODS'][$period] as $f => $v) {
                    if (in_array($f, ['profit_temp', 'proceeds_temp', 'PERIOD_VAL', 'PERIOD_YEAR', 'PERIOD_TYPE'])) {
                        continue;
                    }
                    $arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"][$f] = true;
                }
            }
            
            $arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["PERIOD_FIELDS"]);
            
            $needPeriod = 4;
            
            foreach ($arResult['PERIODS'] as $period => $vals) {
                $x[] = $vals;
                if ($vals["PERIOD_VAL"] == $needPeriod || !$vals["PERIOD_VAL"]) {
                    
                    $arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"][] = $period;
                    
                    if (count($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) >= $limit) {
                        break;
                    }
                }
            }
            
            foreach ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"] as $period) {
                foreach ($arResult['PERIODS'][$period] as $f => $v) {
                    if (in_array($f, ['profit_temp', 'proceeds_temp', 'PERIOD_VAL', 'PERIOD_YEAR', 'PERIOD_TYPE'])) {
                        continue;
                    }
                    $arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"][$f] = true;
                }
            }
            $arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"] = RadarBase::sortPeriodFields($arResult["EXPANDED_PERIODS"]["YEAR_PERIOD_FIELDS"]);
            
            //для графика квартала, берем последний - 1 квартал
            if ($arResult["EXPANDED_PERIODS"]["PERIODS"]) {
                $arKvartalToMonth = array(1 => "03-31", 2 => "06-30", 3 => "09-30", 4 => "12-31",);
                
                $t = array_reverse($arResult["EXPANDED_PERIODS"]["PERIODS"]);
                $last = explode("-", $t[0]);
                
                if ($last[0] == 1) {
                    $last[0] = 4;
                    $last[1] -= 1;
                } else {
                    $last[0] -= 1;
                }
                //$from = $last[1] . "-" . $this->arKvartalToMonth[$last[0]];
                $from = $last[1] . "-" . $arKvartalToMonth[$last[0]];
                
                /*
                1 квартал с 1 января по 31 марта
                2 квартал с 1 апреля по 30 июня
                3 квартал с 1 июля по 30 сентября
                4 квартал с 1 октбяря по 31 декабря
                */
                
                $arResult["EXPANDED_PERIODS"]["KVARTAL_CHART"] = $this->getDatesArray($from);
                
            }
            
            //для года берем начала последнего года
            if ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) {
                $t = array_reverse($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]);
                $last = explode("-", $t[0]);
                $from = $last[1] . "-01-01";
                //график
                $arResult["EXPANDED_PERIODS"]["YEAR_CHART"] = $this->getDatesArray($from);
            }
        }
        
        return $arResult;
    }

    /**
     * @param $startDate
     * @return array
     * @throws Exception
     */
    private function getDatesArray($startDate)
    {
        
        $startDate = new DateTime($startDate);
        $to = (new DateTime(date('d.m.Y')))->add(new DateInterval('P2M'));
        
        $interval_val = 'P1M';
        $startDate = $startDate->modify('first day of this month');
        $period = new DatePeriod($startDate, //new DateInterval('P' . $interval . 'D'),
          new DateInterval($interval_val), $to);
        $getDates = [];
        foreach ($period as $key => $value) {
        		//После обновления битрикс месяц стал по символу M выводиться с маленькой буквы, из-за чего не рисуются желтые столбики на графиках.
				//принудительно приводим первый символ строки в верхний регистр. mb_ucfirst лежит в init.php
				$date = FormatDate("M. Y", $value->getTimestamp());
				$date = mb_ucfirst($date);
            $getDates[] = [$date,];
        }

        return $getDates;
    }

	 /**
	  * Заполняет в массиве для графика текущей капой от сектора пустые значения кварталов от последнего доступного квартала назад
	  * Вызывается из result_modifier.php компонента news.detail шаблон industry_rus_detail и sector_usa_detail
	  *
	  * @return [add type]  [add description]
	  *
	  * @access private
	  */
	 public function fillEmptyPeriodsCapa($arChartData = array(), $arCurCapa = 0){
                //Заполним текущей капой от сектора пустые значения кварталов от последнего доступного квартала назад, для чего перевернем массив
                $curPeriod = (new DateTime('last day of this month'));
                $t = array_reverse($arChartData);
                for ($i = 0; $i < count($t); $i++) {
                    if (!empty($t[$i][1])) {
                        break;
                    }

                    $arPeriod = explode(" ", $t[$i][0]);
                    $dtPeriod = new DateTime('01.' . $this->arMonthRevers[mb_ucfirst($arPeriod[0])] . '.' . $arPeriod[1]);
						  $compare = $curPeriod->getTimeStamp() > $dtPeriod->getTimeStamp();
                     if ($compare) {
                     	$t[$i][1] = $arCurCapa;
							}
                }

                $arChartData = array_reverse($t);
                unset($t);
					 return $arChartData;
	 }
    
}