<?
//Класс для акций США с биржи СПБ
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
//Акции
class ActionsUsa extends RadarBase {
	private $arItems         = [];
	public $arOriginalsItems = [];
	public $count;
	private $perPage      = 5;
	public $havePayAccess = false;
	public $classType     = self::TYPE_ACTION;

	function __construct() {
		//exit();
	 	$this->setData();
		$this->havePayAccess = checkPayUSARadar();
	}

		//отдельный элемент по коду
	public function getItemByCode($code) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["CODE"] == $code) {
				return $item;
			}
		}
		return false;
	}
	//отдельный элемент по коду
	public function getItem($code) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["SECID"] == $code) {
				return $item;
			}
		}
		return false;
	}

	public function getItemByIsin($isin) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["PROPS"]["ISIN"] == $isin) {
				return $item;
			}
		}
		return false;
	}

	public function getItemBySecid($secid) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["PROPS"]["SECID"] == $secid) {
				return $item;
			}
		}
		return false;
	}

	//отдельный элемент по id эмитента
	public function getItemByEmitentId($emitentId) {
		foreach ($this->arOriginalsItems as $item) {
			if ($item["PROPS"]["EMITENT_ID"] == $emitentId) {
				return $item;
			}
		}
		return false;
	}

	//поиск по названию
	public function searchByName($q, $limit = 10, $inview = array()) {
		if (!$q) {
			return false;
		}
		$arItems = array();

		//параметр для поиска в зависимости от типа
		$key = '';
		switch ($this->classType) {
			case self::TYPE_ACTION:
				$key = 'SECID';
				break;
		}

		foreach ($this->arOriginalsItems as $item) {
			if (stripos($item["NAME"], $q) !== false || ($key && stripos($item['PROPS'][$key], $q) !== false)) {
				if (in_array($item["PROPS"]["SECID"], $inview)) {
					continue;
				}
				$arItems[] = $item;

				if (count($arItems) >= $limit) {
					break;
				}
			}
		}

		return $arItems;
	}

	//Возвращает уникальные значения для фильтра по акциям США
	public function getRadarFiltersValues() {
		global $APPLICATION;
		$cache    = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId  = "actions_usa_filters_data";
		$cacheTtl = 86400 * 7;
		//$cacheTtl = 0;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arFilterEnums = $cache->get($cacheId);
		} else {
			CModule::IncludeModule("iblock");
			$arFilterEnums = array("SECTOR" => array(), "INDUSTRY" => array(), "COUNTRY" => array(), "TOP" => array(), "SP500" => array(), "CURRENCY" => array());

			foreach ($arFilterEnums as $k => $v) {
				$rsResult = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 55, '=PROPERTY_HIDEN' => false, '!PROPERTY_' . $k . '_VALUE' => false, '!NAME' => false), array('PROPERTY_' . $k));
				while ($arResult = $rsResult->Fetch()) {
					if (!empty(trim($arResult['PROPERTY_' . $k . '_VALUE']))) {
						if (strpos($arResult['PROPERTY_' . $k . '_VALUE'], "/") !== false) {
							$val = explode("/", $arResult['PROPERTY_' . $k . '_VALUE']);
							foreach ($val as $param_val) {
								if(!empty($param_val)) {
								 if($k=='TOP' && in_array($param_val, array("ESG инвестирование","Халяльные инвестиции"))) continue;
								 $arFilterEnums[$k][$param_val] = $param_val;
								 }
							}
						} else {
							$val                     = $arResult['PROPERTY_' . $k . '_VALUE'];
							if(!empty($val)){
								if($k=='TOP' && in_array($val, array("ESG инвестирование","Халяльные инвестиции"))) continue;
							  $arFilterEnums[$k][$val] = $val;
							  }
						}
					}
				}
			}
			//Добавляем список инвест-фондов
			$CFunds = new CFundsUsa;
			$arFilterEnums["FUNDS"] = $CFunds->arFundsNames;
			unset($CFunds);
			asort($arFilterEnums["TOP"]);
			$cache->set($cacheId, $arFilterEnums);
		}

		return $arFilterEnums;
	}

	public function getCompanyAction($companyId) {
/*  $arFilter = Array("IBLOCK_ID" => 55, "PROPERTY_EMITENT_ID" => $companyId);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
while ($item = $res->GetNext()) {
$arResultCompany["COMPANY"]["ACTIONS"][] = $item;
}*/
		CModule::IncludeModule("iblock");
		//Выбираем обыкновенную акцию для компании
		$arFilter = Array("IBLOCK_ID" => 55, "PROPERTY_EMITENT_ID" => $companyId, "PROPERTY_SPECIES" => "Обыкновенная");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, array("CODE", "NAME", "DETAIL_PAGE_URL"));
		if ($ob = $res->GetNext()) {
			$item = $ob;
		} else {
			$arFilter = Array("IBLOCK_ID" => 55, "PROPERTY_EMITENT_ID" => $companyId, "!PROPERTY_PROP_TIP_AKTSII");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, array("CODE", "NAME", "DETAIL_PAGE_URL"));
			if ($ob = $res->GetNext()) {
				$item = $ob;
			}
		}

		if ($item) {
			return $item["CODE"];
		} else {
			return false;
		}
	}

	//таблица вывода
	public function getTable($page = 1) {
		$this->setFilter();
		$this->sortItems();

		$end   = $page * $this->perPage;
		$start = $end - $this->perPage;

		$arItems = array();

		if ($page == 1) {
			$arItems = $this->arItemsSelected;
		}

		for ($i = $start; $i < $end; $i++) {
			if ($this->arItems[$i]) {
				$arItems[] = $this->arItems[$i];
			}
		}

		$shows = $this->perPage * $page + count($this->arItemsSelected);
		if ($shows > $this->count + count($this->arItemsSelected)) {
			$shows = $this->count;
		}

		return array(
			"total_items" => $this->count + count($this->arItemsSelected),
			"cur_page" => $page,
			"shows" => $shows,
			"count_page" => ceil($this->count / $this->perPage),
			"items" => $arItems,
			"access" => $this->havePayAccess,
			//"selected" => $this->getSelectedItems()
		);
	}

	//фильтр для элементов
	private function setFilter() {
		$r = $this->setDefaultFilter($_REQUEST);

		$selectedItems = $this->getSelectedItems();

		foreach ($this->arOriginalsItems as $item) {
/*       if($item["PROPS"]["SECID"]=='AMZN'){
echo "<pre  style='color:black; font-size:11px;'>";
print_r($item);
echo "</pre>";
}*/      if(!empty($item["PROPS"]["HIDEN"])) continue;
			if (!$selectedItems[$item["PROPS"]["SECID"]]) {
				//Отрасль
				if ($r["sector"]) {
					$items = array();
					foreach ($r["sector"] as $v) {
						if (!$v) {
							continue;
						}
						$items[] = $v;
					}

					if ($items) {
						if (!in_array($item["PROPS"]["SECTOR"], $items) || !$item["PROPS"]["SECTOR"]) {
							continue;
						}
					}
				}

				//Входят в индексы
				/*          if($r["in_index"]=="SP500"){
				if(empty($item["PROPS"]["SP500"])){
				continue;
				}
				}*/
				if (!empty($r["in_index"]) && 'all' !== $r["in_index"]) {
					if (strpos($item["PROPS"]["SP500"], $r["in_index"]) === false) {
						continue;
					}
				}

				//Отрасль
				if (!empty($r["industry"]) && 'all' !== $r["industry"]) {
					if ($item["PROPS"]["INDUSTRY"] != $r["industry"]) {
						continue;
					}
				}

				//Страна
				if (!empty($r["country"]) && 'all' !== $r["country"]) {
					if ($item["PROPS"]["COUNTRY"] != $r["country"]) {
						continue;
					}
				}

				//Валюта
				if (!empty($r["currency"]) && 'all' !== $r["currency"]) {
					if ($item["PROPS"]["CURRENCY"] != $r["currency"]) {
						continue;
					}
				}

				//Подборка от fin-plan
				if (!empty($r["top"]) && 'all' !== $r["top"]) {
					if (strpos($item["PROPS"]["TOP"], $r["top"]) === false) {
						continue;
					}
				}

 				//учитывать esg инвестиции

				if ($r["use_esg"]) {
					if (strpos($item["PROPS"]["TOP"], "ESG инвестирование") === false ) {
						continue;
					}
				}
				
				// учитывать вхождение в портфель ГС США
                if ($r["into_gs_usa_portfolio"]) {
                    if ($item["DYNAM"]["HAVE_GS"] !== "Y" ) {
                        continue;
                    }
                }
                
				
				
				//учитывать Халяльные инвестиции
				if ($r["use_halal"]) {
					if (strpos($item["PROPS"]["TOP"], "Халяльные инвестиции") === false ) {
						continue;
					}
				}

				//Возможность маржинальных сделок
				/*          if($r["margin_deals"]=="short"){
				if(!$item["PROPS"]["SHORT"]){
				continue;
				}
				}*/
/*          if($r["margin_deals"]=="kredit"){
if(!$item["PROPS"]["CREDIT"]){
continue;
}
}*/
                //CAGR
                if ($r["cagr"] && $r["cagr"] !== 'all') {
                    if ($item["DYNAM"]["CAGR"] < intval($r["cagr"])) {
                            continue;
                    }
                }
                
				//Выберите коэффициент р/е акции
			  if ($r["pe"] && $r["pe"] !== 'all') {
				if ($r["pe"] && $r["pe"] <= 50) {
					if (!$item["DYNAM"]["PE"] || $item["DYNAM"]["PE"] > $r["pe"]) {
						continue;
					}
					if ($item["DYNAM"]["PE"] <0) {
						continue;
					}
				}
				}



				//Доходность по консенсус-прогнозу
				if ($r["profitability"] && $r["profitability"] < 50) {
					if (!$item["DYNAM"]["Таргет"] || $item["DYNAM"]["Таргет"] < $r["profitability"]) {
						continue;
					}
				}

				//Убирать из выдачи акции, по которым нет оборотов за последнюю неделю
				/*          if($r["turnover_week"]){
				if($item["PROPS"]["VALTODAY"]<=0){
				continue;
				}
				}*/

				//Нет дивидендов
				if ($r["no_dividends"]) {
					if ($r["no_dividends"] == "y" && $item["DYNAM"]["Нет дивидендов"] != 'Y') {
						continue;
					}
				} else {

					//Дивиденды более
					if ($r["dividends"]) {
						$r["dividends"] = str_replace("%", "", $r["dividends"]);
						$r["dividends"] = str_replace(",", ".", $r["dividends"]);
						if (floatval($r["dividends"])) {
							if (isset($r["future_dividends"]) && $r["future_dividends"] == "y") { //Проверяем галку учета будущих дивидендов
								//Дивиденды более (только будущие)
								if (!$item["DYNAM"]["Будущие дивиденды %"] || $item["DYNAM"]["Будущие дивиденды %"] < $r["dividends"]) {
									continue;
								}
							} else { //Дивиденды более (Без учета будущих)
								if (!$item["DYNAM"]["Дивиденды %"] || $item["DYNAM"]["Дивиденды %"] < $r["dividends"]) {
									continue;
								}
							}
						}
					}
				}

				//Цена менее
				if ($r["price"] && $r["price"] != "all") {
					$arPrice  = explode("-", $r["price"]);
					$curPrice = $item["PROPS"]["LASTPRICE"];

					if ($curPrice <= floatval($arPrice[1])) {
					} else {
						continue;
					}
				}

				//Инвест-фонды
				if ($r["fund"] && $r["fund"] != "all") {
				 $arFund  = explode("_", $r["fund"]);
				 //Исключаем активы не участвующие в выбранном фонде вовсе
				 if(count($item["FUNDS"])==0 || !array_key_exists($arFund[1], $item["FUNDS"])){
				 	continue;
				 }

				 if($item["FUNDS"][$arFund[1]][$arFund[0]] !== true){
				 	continue;
				 }

				}

				//Капитализация более
				if ($r["capitalization"] && $r["capitalization"] != "all") {
					$arCap  = explode("-", $r["capitalization"]);
					$curCap = $item["PROPS"]["ISSUECAPITALIZATION"];

					if ($curCap > floatval($arCap[0]) && $curCap < floatval($arCap[1])) {
					} else {
						continue;
					}
				} else if ($r["capitalization"] && $r["capitalization"] == "all") {
					$curCap = $item["PROPS"]["ISSUECAPITALIZATION"];
					if ($curCap <= 0) {
						continue;
					}
				}

				//P/Equity filter
				if ($this->havePayAccess) {
					if ($r["p-e"] && $r["p-e"] !== 'all') {
						if (!$item["DYNAM"]["P/Equity"]) {
							continue;
						}
						if ($r["p-e"] === '1-2' && $item["DYNAM"]["P/Equity"] > 0.5) {
							continue;
						}
						if ($r["p-e"] === '1' && $item["DYNAM"]["P/Equity"] > 1) {
							continue;
						}
						if ($r["p-e"] === '3-2' && $item["DYNAM"]["P/Equity"] > 1.5) {
							continue;
						}
						if ($r["p-e"] === '2' && $item["DYNAM"]["P/Equity"] > 2) {
							continue;
						}
					}
				}
				//Betta filter
				if ($this->havePayAccess) {
					if ($r["betta"] && $r["betta"] !== 'all') {
						if (!$item["PROPS"]["BETTA"]) {
							continue;
						}
						if ($r["betta"] === '-999>-1' && $item["PROPS"]["BETTA"] > -1) {
							continue;
						}
						if ($r["betta"] === '-1>0' && ($item["PROPS"]["BETTA"] <= -1 || $item["PROPS"]["BETTA"] > 0)) {
							continue;
						}
						if ($r["betta"] === '0>0.5' && ($item["PROPS"]["BETTA"] <= 0 || $item["PROPS"]["BETTA"] > 0.5)) {
							continue;
						}
						if ($r["betta"] === '0.5>1' && ($item["PROPS"]["BETTA"] <= 0.5 || $item["PROPS"]["BETTA"] > 1)) {
							continue;
						}
						if ($r["betta"] === '1>999' && $item["PROPS"]["BETTA"] <= 1) {
							continue;
						}
						if ($r["betta"] === '-9991>1' && $item["PROPS"]["BETTA"] >= 1) {
							continue;
						}
					}
				}
				//Доля экспорта
				/*          if($this->havePayAccess){
				if($r["export_share"]){
				if(intval($r["export_share"])>0){
				if(floatval($item["PROPS"]["PROP_EXPORT_SHARE"])<intval($r["export_share"])){
				continue;
				}
				} else if(intval($r["export_share"])<0){
				if(floatval($item["PROPS"]["PROP_EXPORT_SHARE"])>0){
				continue;
				}
				}
				}
				}*/

				//PEG filter
				if ($this->havePayAccess) {
					if ($r["peg"] && $r["peg"] !== 'all') {
						if (!$item["DYNAM"]["PEG"]) {
							continue;
						}
						if ($r["peg"] === '1-2' && $item["DYNAM"]["PEG"] > 0.5) {
							continue;
						}
						if ($r["peg"] === '1' && $item["DYNAM"]["PEG"] > 1) {
							continue;
						}
						if ($r["peg"] === '3-2' && $item["DYNAM"]["PEG"] > 1.5) {
							continue;
						}
						if ($r["peg"] === '2' && $item["DYNAM"]["PEG"] > 2) {
							continue;
						}
					}
				}

				//рост за месяц
				if ($this->havePayAccess) {
					if ($r["month-increase"] && $r["month-increase"] !== 'all') {
						if (!$item["DYNAM"]["MONTH_INCREASE"]) {
							continue;
						}
						if ($r["month-increase"] === 'down50' && $item["DYNAM"]["MONTH_INCREASE"] > -50) {
							continue;
						}
						if ($r["month-increase"] === 'down25' && $item["DYNAM"]["MONTH_INCREASE"] > -25) {
							continue;
						}
						if ($r["month-increase"] === 'down10' && $item["DYNAM"]["MONTH_INCREASE"] > -10) {
							continue;
						}
						if ($r["month-increase"] === 'down' && $item["DYNAM"]["MONTH_INCREASE"] > 0) {
							continue;
						}
						if ($r["month-increase"] === 'up' && $item["DYNAM"]["MONTH_INCREASE"] < 0) {
							continue;
						}
						if ($r["month-increase"] === 'up10' && $item["DYNAM"]["MONTH_INCREASE"] < 10) {
							continue;
						}
						if ($r["month-increase"] === 'up25' && $item["DYNAM"]["MONTH_INCREASE"] < 25) {
							continue;
						}
						if ($r["month-increase"] === 'up50' && $item["DYNAM"]["MONTH_INCREASE"] < 50) {
							continue;
						}
					}
					if ($r["year-increase"] && $r["year-increase"] !== 'all') {
						if (!$item["DYNAM"]["YEAR_INCREASE"]) {
							continue;
						}
						if ($r["year-increase"] === 'down50' && $item["DYNAM"]["YEAR_INCREASE"] > -50) {
							continue;
						}
						if ($r["year-increase"] === 'down25' && $item["DYNAM"]["YEAR_INCREASE"] > -25) {
							continue;
						}
						if ($r["year-increase"] === 'down10' && $item["DYNAM"]["YEAR_INCREASE"] > -10) {
							continue;
						}
						if ($r["year-increase"] === 'down' && $item["DYNAM"]["YEAR_INCREASE"] > 0) {
							continue;
						}
						if ($r["year-increase"] === 'up' && $item["DYNAM"]["YEAR_INCREASE"] < 0) {
							continue;
						}
						if ($r["year-increase"] === 'up10' && $item["DYNAM"]["YEAR_INCREASE"] < 10) {
							continue;
						}
						if ($r["year-increase"] === 'up25' && $item["DYNAM"]["YEAR_INCREASE"] < 25) {
							continue;
						}
						if ($r["year-increase"] === 'up50' && $item["DYNAM"]["YEAR_INCREASE"] < 50) {
							continue;
						}
					}
					if ($r["three-year-increase"] && $r["three-year-increase"] !== 'all') {
						if (!$item["DYNAM"]["THREE_YEAR_INCREASE"]) {
							continue;
						}
						if ($r["three-year-increase"] === 'down50' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -50) {
							continue;
						}
						if ($r["three-year-increase"] === 'down25' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -25) {
							continue;
						}
						if ($r["three-year-increase"] === 'down10' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > -10) {
							continue;
						}
						if ($r["three-year-increase"] === 'down' && $item["DYNAM"]["THREE_YEAR_INCREASE"] > 0) {
							continue;
						}
						if ($r["three-year-increase"] === 'up' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 0) {
							continue;
						}
						if ($r["three-year-increase"] === 'up10' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 10) {
							continue;
						}
						if ($r["three-year-increase"] === 'up25' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 25) {
							continue;
						}
						if ($r["three-year-increase"] === 'up50' && $item["DYNAM"]["THREE_YEAR_INCREASE"] < 50) {
							continue;
						}
					}
				}
 					 //Рейтинг радар filter
                if($this->havePayAccess){
                    if($r["radar-rating"] && $r["radar-rating"] !== 'all'){
                        if(!$item["PROPS"]["PRC_OF_USERS"]){
                            continue;
                        }

								if($r["radar-rating"] === '0-1' && $item["PROPS"]["PRC_OF_USERS"] > 1){
                            continue;
                        }
								if($r["radar-rating"] === '0-5' && $item["PROPS"]["PRC_OF_USERS"] > 5){
                            continue;
                        }
								if($r["radar-rating"] === '0-10' && $item["PROPS"]["PRC_OF_USERS"] > 10){
                            continue;
                        }
								if($r["radar-rating"] === '0-20' && $item["PROPS"]["PRC_OF_USERS"] > 20){
                            continue;
                        }
								if($r["radar-rating"] === '0-30' && $item["PROPS"]["PRC_OF_USERS"] > 30){
                            continue;
                        }
								if($r["radar-rating"] === '1-100' && $item["PROPS"]["PRC_OF_USERS"] < 1){
                            continue;
                        }
								if($r["radar-rating"] === '5-100' && $item["PROPS"]["PRC_OF_USERS"] < 5){
                            continue;
                        }
								if($r["radar-rating"] === '10-100' && $item["PROPS"]["PRC_OF_USERS"] < 10){
                            continue;
                        }
								if($r["radar-rating"] === '20-100' && $item["PROPS"]["PRC_OF_USERS"] < 20){
                            continue;
                        }
								if($r["radar-rating"] === '30-100' && $item["PROPS"]["PRC_OF_USERS"] < 30){
                            continue;
                        }
                    }
                }
				//ОТЧЕТНОСТЬ период
				if ($this->havePayAccess) {
					if ($r["period"]) {
						if ($item["PROPS"]["SPECIES"] == "корпоративные") {
							$show = true;
							foreach ($r["period"] as $name => $vals) {
								if (!$vals["use"]) {
									continue;
								}

								$vals["percent"] = trim(str_replace("%", "", $vals["percent"]));

								//Если фильтрация по последнему существующему у актива периоду то заменяем значение last на обозначенный период из кеша данных по активу $item["LAST_PERIOD_KVARTAL"]
								if ($r["period_value"] == "last") { //Сравниваем период для каждого актива по его последнему периоду
									$item_period = str_replace("-KVARTAL", "", $item["LAST_PERIOD_KVARTAL"]);
									if ($item["PERIODS"][$item_period . "-KVARTAL"][$name] <= $vals["percent"]) {
										$show = false;
										break;
									}
								} else
								if ($item["PERIODS"][$r["period_value"] . "-KVARTAL"][$name] <= $vals["percent"]) {
									$show = false;
									break;
								}
							}

							if (!$show) {
								continue;
							}
						}
					}
				}

				//ОТЧЕТНОСТЬ месяц
				if ($this->havePayAccess) {
					if ($r["month"]) {
						if ($item["PROPS"]["SPECIES"] == "банковские") {
							$show = true;

							$r["month_value"]    = explode("-", $r["month_value"]);
							$r["month_value"][1] = mb_substr($r["month_value"][1], -2);
							$r["month_value"]    = implode("-", $r["month_value"]);

							foreach ($r["month"] as $name => $vals) {
								if (!$vals["use"]) {
									continue;
								}

								if ($vals["top"]) {
									if ($item["PERIODS"][$r["month_value"] . "-MONTH"][$name] > $vals["top"] || !$item["PERIODS"][$r["month_value"] . "-MONTH"][$name]) {
										$show = false;
									}
								} else {
									$vals["percent"] = trim(str_replace("%", "", $vals["percent"]));
									if ($name == "Доля просрочки, %") {
										if ($item["PERIODS"][$r["month_value"] . "-MONTH"][$name] > $vals["percent"] || !$item["PERIODS"][$r["month_value"] . "-MONTH"][$name]) {
											$show = false;
										}
									} else {
										if ($item["PERIODS"][$r["month_value"] . "-MONTH"][$name] <= $vals["percent"]) {
											$show = false;
										}
									}
								}
							}

							if (!$show) {
								continue;
							}
						}
					}
				}

				$this->arItems[] = $item;
			} else {
				$this->arItemsSelected[] = $item;
			}
		}
		$this->count = count($this->arItems);
	}

    /**
     * Возвращает список ISIN кодов для фильтра списка акций США
     * @param $arFilterData
     * @return array
     */
	public function setFilterForList($arFilterData){
	    $listSelectedItems = array();

        foreach($this->arOriginalsItems as $item){
            unset($item["PERIODS"]);
           switch ($arFilterData[0]){
               case "all":
                   $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;
               case "country":
						 if ($item["PROPS"]["COUNTRY"] != $arFilterData[1]) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;
                case "currency":
                   if(!in_array($item["PROPS"]["CURRENCY"], $arFilterData[1]) || !$item["PROPS"]["CURRENCY"])	continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;
               case "top":
						 if (strpos($item["PROPS"]["TOP"], $arFilterData[1]) === false) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;
               case "dividends":
                   if(floatval($item["DYNAM"]["Дивиденды %"])==0) continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;

               case "sector":
                   if(!in_array($item["PROPS"]["SECTOR"], $arFilterData[1]) || !$item["PROPS"]["SECTOR"])	continue;
                   if(!in_array($item["PROPS"]["ISIN"], $listSelectedItems))
                       $listSelectedItems[$item["PROPS"]["ISIN"]] = array("DYNAM" => $item["DYNAM"], "QUOTATIONS_MONTH"=>$item["PROPS"]["QUOTATIONS_MONTH"]);
               break;



           }

        }
        return $listSelectedItems;
    }


	//Фильтр по умолчанию
	private function setDefaultFilter($r) {
		global $APPLICATION;
		if (!isset($r["pe"])) {
			$r["pe"] = $APPLICATION->actionUSADefaultForm["pe"];
		}
		if (!isset($r["profitability"])) {
			$r["profitability"] = $APPLICATION->actionUSADefaultForm["profitability"];
		}

		//Капитализация более
		if (!isset($r["capitalization"])) {
			$r["capitalization"] = 'all';
		}
/*    if(!isset($r["turnover_week"])){   //оборот
$r["turnover_week"] = "y";
}*/

		return $r;
	}

	//сортировка элементов
	private function sortItems($sortBy = 'ISSUECAPITALIZATION') {
		switch ($sortBy) {
			case 'ISSUECAPITALIZATION':
				$this->setSortByISSUECAPITALIZATION();
				break;
			case 'PE':
				$this->setSortByPE();
				break;
/*        case 2:
echo "i равно 2";
break;*/
			default:
				$this->setSortByPE();
		}

		//  $this->setSortByPE();
	}

	//Сортировка по Капе
	private function setSortByISSUECAPITALIZATION() {
		$negArr = [];
		$posArr = [];

		foreach ($this->arItems as $item) {
			if ($item["PROPS"]["ISSUECAPITALIZATION"] <= 0 || !$item["PROPS"]["ISSUECAPITALIZATION"]) {
				$negArr[] = $item;
			} else {
				$posArr[] = $item;
			}
		}
		usort($posArr, function ($a, $b) {
			if ($a["PROPS"]["ISSUECAPITALIZATION"] == $b["PROPS"]["ISSUECAPITALIZATION"]) {
				return 0;
			}
			return ($a["PROPS"]["ISSUECAPITALIZATION"] < $b["PROPS"]["ISSUECAPITALIZATION"]) ? 1 : -1;
		});

		usort($negArr, function ($a, $b) {
			if ($a["PROPS"]["ISSUECAPITALIZATION"] == $b["PROPS"]["ISSUECAPITALIZATION"]) {
				return 0;
			}
			if (!$a["PROPS"]["ISSUECAPITALIZATION"]) {
				return -1;
			}
			return ($a["PROPS"]["ISSUECAPITALIZATION"] < $b["PROPS"]["ISSUECAPITALIZATION"]) ? -1 : 1;
		});

		$this->arItems = array_merge($posArr, $negArr);
	}

	//Cортировка по PE
	private function setSortByPE() {
		$negArr = [];
		$posArr = [];

		foreach ($this->arItems as $item) {
			if ($item["DYNAM"]["PE"] <= 0 || !$item["DYNAM"]["PE"]) {
				$negArr[] = $item;
			} else {
				$posArr[] = $item;
			}
		}
		usort($posArr, function ($a, $b) {
			if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
				return 0;
			}
			return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? 1 : -1;
		});

		usort($negArr, function ($a, $b) {
			if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
				return 0;
			}
			if (!$a["DYNAM"]["PE"]) {
				return -1;
			}
			return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? -1 : 1;
		});

		$this->arItems = array_merge($posArr, $negArr);
	}

	//избранные элементы
	public function getSelectedItems() {
		$arData = array();
		if ($_COOKIE["id_arr_shares_usa_cookie"]) {
			$tmp = json_decode($_COOKIE["id_arr_shares_usa_cookie"]);
			foreach ($tmp as $item) {
				$t             = explode("...", $item);
				$arData[$t[0]] = $t[1];
			}
		}

		return $arData;
	}

	//Кеширует графики для всех компаний
	public function CacheCompanyGraphics() {
		CModule::IncludeModule("iblock");

		//Компании
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE");
		$arFilter = Array("IBLOCK_ID" => IntVal(43));
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, array("nTopCount" => 1000), $arSelect);
		$cnt      = 0;
		$graph    = new SpbexGraph();
		while ($arItem = $res->Fetch()) {
			$arResult   = $arItem;
			$actionCode = $this->getCompanyAction($arItem["ID"]);
			if ($actionCode != false) {
				$arResult['ACTION'] = $this->getItem($actionCode);
			}

			//Попутно перекешируем график акции
			$arResult_CHARTS_DATA = $graph->getForAction($arResult["ACTION"]["PROPS"]["SECID"], true);
			unset($arResult_CHARTS_DATA);

			//Ключеные показатели
			if ($arResult["ACTION"]["PERIODS"]) {
				foreach ($arResult["ACTION"]["PERIODS"] as $period => $vals) {
					if ($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
						if ($vals["PERIOD_TYPE"] == "KVARTAL") {continue;}
					}
					$arResult["EXPANDED_PERIODS"]["PERIODS"][] = $period;
					if (count($arResult["EXPANDED_PERIODS"]["PERIODS"]) >= 5) {break;}
				}

				$needPeriod = 4;
				if ($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {$needPeriod = 12;}

				foreach ($arResult["ACTION"]["PERIODS"] as $period => $vals) {
					$x[] = $vals;
					if ($vals["PERIOD_VAL"] == $needPeriod || !$vals["PERIOD_VAL"]) {
						$arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"][] = $period;

						if (count($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) >= 5) {break;}
					}
				}

				if ($arResult["ACTION"]["PROPS"]["SECID"]) {
					// $graph = new SpbexGraph();
					//для графика квартала, берем последний - 1 квартал
					if ($arResult["EXPANDED_PERIODS"]["PERIODS"]) {
						$arKvartalToMonth = array(
							1 => "03-31",
							2 => "06-30",
							3 => "09-30",
							4 => "12-31",
						);

						$t    = array_reverse($arResult["EXPANDED_PERIODS"]["PERIODS"]);
						$last = explode("-", $t[0]);

						if ($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
							$dt   = DateTime::createFromFormat("m-y", $last[0] . "-" . $last[1]);
							$from = FormatDate("Y-m", $dt->getTimestamp()) . "-01";
						} else {
							if ($last[0] == 1) {
								$last[0] = 4;
								$last[1] -= 1;
							} else {
								$last[0] -= 1;
							}
							$from = $last[1] . "-" . $arKvartalToMonth[$last[0]];
						}

						/*
						1 квартал с 1 января по 31 марта
						2 квартал с 1 апреля по 30 июня
						3 квартал с 1 июля по 30 сентября
						4 квартал с 1 октбяря по 31 декабря
						 */
						//график (для кеширования)
						$arResultTmp = $graph->getForActionByMonthWithFrom($arResult["ACTION"]["PROPS"]["SECID"], $from, true);
						unset($arResultTmp);
					}

					//для года берем начала последнего года
					if ($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]) {
						$t    = array_reverse($arResult["EXPANDED_PERIODS"]["YEAR_PERIODS"]);
						$last = explode("-", $t[0]);
						if ($arResult["ACTION"]["PROPS"]["PROP_VID_AKTSIY"] == "банковские") {
							$dt   = DateTime::createFromFormat("y", $last[1]);
							$from = FormatDate("Y", $dt->getTimestamp()) . "-01-01";
						} else {
							$from = $last[1] . "-01-01";
						}

						//график (для кеширования)
						$arResultTmp = $graph->getForActionByMonthWithFrom($arResult["ACTION"]["PROPS"]["SECID"], $from, true);
						unset($arResultTmp);
					}
					//unset($graph);
				}
			}
		} //while company
		unset($arResult);
	}
	//CacheCompanyGraphics

	private function setData() {
		global $APPLICATION, $DB;
		$cache    = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId  = "actions_usa_data";
		$cacheTtl = 86400 * 7;
		//$cacheTtl = 0;

		//$CRadarEvents = new CRadarEvents();
		//$this->currencyRateUSD = 0;
		$this->currencyRateUSD = getCBPrice("USD", (new DateTime())->format("d/m/Y"));

		if ($cache->read($cacheTtl, $cacheId)) {
			$arItems = $cache->get($cacheId);
		} else {
			$arItems = [];

			if (!class_exists("CCurrency")) {
				require $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/classes/radar/currency.php";
			}
		  //	$cbr = new CCurrency();
            //Данные по CAGR фильтру
            $arCagrData = array();
            $sql = "SELECT * FROM `hl_cagr_filter`";
            $resCagr = $DB->Query($sql);
            while($rowCagr = $resCagr->fetch()){
                $arCagrData[$rowCagr["UF_NAME"]] = $rowCagr["UF_VALUE"];
            }
			
			//данные по периодам
			CModule::IncludeModule("iblock");
			CModule::IncludeModule("highloadblock");
			$ibID = 55;

			//Разрешаем добавлять информацию по вхождению бумаги в фонды
			$CFunds = new CFundsUsa;
			$addFundsData = false;
			if(count($CFunds->arFundsDataByTickers)){
			 $addFundsData = true;
			}

			// OblCompanyDataUsa
			$hlblock     = HL\HighloadBlockTable::getById(22)->fetch();
			$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();

			$res = $entityClass::getList(array(
				"filter" => array(
					"!UF_YEAR" => false,
				),
				"order" => array(
					"ID" => "desc",
				),
				"select" => array(
					"*"
				),
			));

			while ($item = $res->fetch()) {
				$item["UF_DATA"] = json_decode($item["UF_DATA"], true);

				$t    = $item["UF_MONTH"];
				$type = "MONTH";

				if (!$t) {
					$t    = $item["UF_KVARTAL"];
					$type = "KVARTAL";
				}

				$item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
				$item["UF_DATA"]["PERIOD_VAL"]  = $t;
				$item["UF_DATA"]["PERIOD_TYPE"] = $type;

				$arPeriods[$item["UF_COMPANY"]][$t . "-" . $item["UF_YEAR"] . "-" . $type] = $item["UF_DATA"];
			}

			//Предыдущий день
			$prevDay = new DateTime();
			$prevDay->modify("-1 day");

			$arFilter = ["IBLOCK_ID" => $ibID];
			$res      = CIBlockElement::GetList([], $arFilter, false, false);
			$arRates = array();//Полученные курсы
			while ($ob = $res->GetNextElement()) {
				$fields = $ob->GetFields();

				$item = [
					"ID" => $fields["ID"],  
					"NAME" => $fields["NAME"],
					"SECID" => $fields["CODE"],
					"CODE" => $fields["CODE"],
				];



				//страница акции
				$arFilter = Array("IBLOCK_ID" => 56, "CODE" => $fields["CODE"]);
				$res2     = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL"));
				if ($val = $res2->GetNext()) {
					$item["URL"] = $val["DETAIL_PAGE_URL"];
				}

				$props = $ob->GetProperties();

				foreach ($props as $prop) {
					if ($prop["VALUE"] || $prop["CODE"] == "HIDEN") {
						$item["PROPS"][$prop["CODE"]] = $prop["VALUE"];
					}
				}

				$item["PROPS"]["DRAWDOWN"] = floatval(str_replace(",", ".", $item["PROPS"]["DRAWDOWN"]));
				$item["PROPS"]["TARGET"]   = floatVal(str_replace(",", ".", $item["PROPS"]["TARGET"]));

				//Убрать выведенные с биржи
			/*	if(!empty($item["PROPS"]["HIDEN"])){
					continue;
				}*/

				if ($item["PROPS"]["EMITENT_ID"] || $item["PROPS"]["COMMON_EMITENT_ID"]) {
				    //Если указан общий эмитент из РФ - делаем выбоорку из РФ
                    if(intval($item["PROPS"]["COMMON_EMITENT_ID"])>0){
                        $companyIblockId = 26;
                        $companyPagesIblockId = 29;
                        $emitentId = $item["PROPS"]["COMMON_EMITENT_ID"];
                    } else {
                        $companyIblockId = 43;
                        $companyPagesIblockId = 44;
                        $emitentId = $item["PROPS"]["EMITENT_ID"];
                    }

				//Если разрешено добавляем данные по фондам
				if($addFundsData && array_key_exists($item["PROPS"]["SECID"], $CFunds->arFundsDataByTickers)){
				  $item["FUNDS"] = $CFunds->arFundsDataByTickers[$item["PROPS"]["SECID"]];
				}

					$comp            = CIBlockElement::GetByID($emitentId)->Fetch();
					$item["COMPANY"] = [
						"ID" => $comp["ID"],
						"NAME" => $comp["NAME"],
					];

				 	$item["COMPANY"]["NEXT_REPORT_DATE"] = parent::getNextReportDate(trim($comp["NAME"]), 'REPORT_USA');

					//страница компании
					$arFilter = Array("IBLOCK_ID" => $companyPagesIblockId, "NAME" => $comp["NAME"]);
					$res2     = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL", "PROPERTY_CURRENCY", "PROPERTY_CURRENCY_REPORT", "PROPERTY_FIN_YEAR_START_DATE", "PROPERTY_HALF_YEAR", "PROPERTY_PERIOD_OFFSET", "PROPERTY_CURRENCY_CIRCULATION", "PROPERTY_CURRENCY_DIVIDENDS", "PROPERTY_CAPITAL_KOEF"));
					if ($val = $res2->GetNext()) {
						$item["COMPANY"]["URL"]                  = $val["DETAIL_PAGE_URL"];
						$item["COMPANY"]["CURRENCY"]             = $val["PROPERTY_CURRENCY_VALUE"];
						$item["COMPANY"]["CURRENCY_REPORT"]      = $val["PROPERTY_CURRENCY_REPORT_VALUE"];
						$item["COMPANY"]["FIN_YEAR_START_DATE"]  = $val["PROPERTY_FIN_YEAR_START_DATE_VALUE"];
						$item["COMPANY"]["HALF_YEAR"]            = $val["PROPERTY_HALF_YEAR_VALUE"];
						$item["COMPANY"]["PERIOD_OFFSET"]        = $val["PROPERTY_PERIOD_OFFSET_VALUE"];
						$item["COMPANY"]["CURRENCY_CIRCULATION"] = $val["PROPERTY_CURRENCY_CIRCULATION_VALUE"];
						$item["COMPANY"]["CURRENCY_DIVIDENDS"] = $val["PROPERTY_CURRENCY_DIVIDENDS_VALUE"];

						//если валюта отчетности заполнена, перевести капитализацию
						$recalcCap = true; //включить блок пересчета капитализации
						//$recalcCap = false;
						if (!empty($val["PROPERTY_CURRENCY_REPORT_VALUE"]) && !empty($item["PROPS"]["ISSUECAPITALIZATION"]) && $recalcCap) {
							$dateSql = $prevDay->format('Y-m-d');

						 if(!array_key_exists($val["PROPERTY_CURRENCY_REPORT_VALUE"], $arRates)){
						 		$codeCurr = $val["PROPERTY_CURRENCY_REPORT_VALUE"];
								$sql = "SELECT * FROM `hl_currency_rates` WHERE `UF_DATE` = '$dateSql' AND `UF_CODE` = '$codeCurr'";
								$resCurr = $DB->Query($sql);
								if($rowCurr = $resCurr->fetch()){
								  $arRates[$rowCurr["UF_CODE"]] = array('RARE'=>$rowCurr["UF_RARE"],"RATE"=>$rowCurr["UF_RATE"]);
								}
							}
						 if(!array_key_exists($val["PROPERTY_CURRENCY_CIRCULATION_VALUE"], $arRates) && !empty($val["PROPERTY_CURRENCY_CIRCULATION_VALUE"])){
						 	   $codeCurr = $val["PROPERTY_CURRENCY_CIRCULATION_VALUE"];
								$sql = "SELECT * FROM `hl_currency_rates` WHERE `UF_DATE` = '$dateSql' AND `UF_CODE` = '$codeCurr'";
								$resCurr = $DB->Query($sql);
								if($rowCurr = $resCurr->fetch()){
								  $arRates[$rowCurr["UF_CODE"]] = array('RARE'=>$rowCurr["UF_RARE"],"RATE"=>$rowCurr["UF_RATE"]);
								}
							}
							unset($resCurr);
						  //	$cbr->setClassDate($prevDay->format('d.m.Y'));
							$CURRENCY_REPORT_VALUE = array("RATE"=>1);
							if(array_key_exists($val["PROPERTY_CURRENCY_REPORT_VALUE"], $arRates)){
									$CURRENCY_REPORT_VALUE = $arRates[$val["PROPERTY_CURRENCY_REPORT_VALUE"]];
							}


							if ($CURRENCY_REPORT_VALUE["RARE"]==1) { //Если редкая валюта то считаем через доллары напрямую
								$arReportRate = $CURRENCY_REPORT_VALUE["RATE"];
					 			$item["PROPS"]["ISSUECAPITALIZATION_CURRENCY"] = $item["PROPS"]["ISSUECAPITALIZATION"] * $arReportRate;
							} else { //Если не редкая валюта то считаем через рубли
								$arReportRate = $CURRENCY_REPORT_VALUE["RATE"];
								$CURRENCY_CIRCULATION_VALUE = array("RATE"=>1);
								if(array_key_exists($val["PROPERTY_CURRENCY_CIRCULATION_VALUE"], $arRates)){
									$CURRENCY_CIRCULATION_VALUE = $arRates[$val["PROPERTY_CURRENCY_CIRCULATION_VALUE"]];
								}
								$arCirculationRate = $CURRENCY_CIRCULATION_VALUE["RATE"];
					 		   //$arCirculationRate = $cbr->getRate($val["PROPERTY_CURRENCY_CIRCULATION_VALUE"], false);
					 			$item["PROPS"]["ISSUECAPITALIZATION_CURRENCY"] = ($item["PROPS"]["ISSUECAPITALIZATION"] * $arCirculationRate) / $arReportRate;
							}
						}

						if ($val["PROPERTY_CAPITAL_KOEF_VALUE"]) {
							$item["COMPANY"]["CAPITAL_KOEF"] = $val["PROPERTY_CAPITAL_KOEF_VALUE"];
						}
					}
				}

				if ($item["PROPS"]["EMITENT_ID"] && $arPeriods[$item["PROPS"]["EMITENT_ID"]]) {
				//if ($a==$b) {
					$item["PERIODS"] = $arPeriods[$item["PROPS"]["EMITENT_ID"]];
					//Периоды могут быть смещены с учетом финансового года компании внутри calculatePeriodData.
					// Данные в HL блоке остаются не смещенными, это следует учесть!!!
		 		 	$item["PERIODS"] = $this->calculatePeriodData($item);
		 		 	//$item["PERIODS"] = (new RadarBase())->calculatePeriodData($item);

					//Определим последние периоды для квартальной отчетности и по месяцам
					$item["LAST_PERIOD_KVARTAL"] = '';
					$item["LAST_PERIOD_MONTH"]   = '';
					reset($item["PERIODS"]);
					foreach ($item["PERIODS"] as $period => $value) {
						if (empty($item["LAST_PERIOD_KVARTAL"]) && strpos($period, "KVARTAL") !== false) {
							$item["LAST_PERIOD_KVARTAL"] = $period;
						}
						//Пока периоды для банков отключены, т.к. пока не учитываем банки США
						/*                      if(empty($item["LAST_PERIOD_MONTH"]) && strpos($period,"MONTH")!==false){
						$item["LAST_PERIOD_MONTH"] = $period;
						}*/
						//if(!empty($item["LAST_PERIOD_KVARTAL"]) && !empty($item["LAST_PERIOD_MONTH"])){
						if (!empty($item["LAST_PERIOD_KVARTAL"])) {
							break;
						}
					}
				}

				$item["DYNAM"] = self::setDynamParams($item);
				
                //Добавляем значение CAGR
                $cagrValue = 0;
                if(!empty($item["PROPS"]["TOP"])) {
                    $arItemTop = explode("/", $item["PROPS"]["TOP"]);
                    if(!is_array($arItemTop)) {
                        $arItemTop = array($arItemTop);
                    }
                    $arItemTop = array_flip($arItemTop);
                    //Находим пересечение ключей массива CAGR и массива подборок актива
                    $arCagrIntersect = array_intersect_key($arCagrData, $arItemTop);
                    if (count($arCagrIntersect)) {
                        $cagrValue = max($arCagrIntersect);
                    }
                }
                $item["DYNAM"]["CAGR"] = $cagrValue;
                
				$arItems[] = $item;
			}
		  //	unset($cbr);
			$cache->set($cacheId, $arItems);
		}

		self::setHaveGS($arItems);
		$this->arOriginalsItems = $arItems;
		$this->count            = count($arItems);
	}


	/**
	 * Дополняет массив акций по ссылке данными о наличии аналитики ГС для каждой бумаги
     * так же проставляет признак вхождения в ГС-портфель
	 *
	 * @param  array   $arItems ссылка на массив акций после основных расчетов
	 *
	 * @access private
	 */
	private function setHaveGS(&$arItems) {
	    Global $APPLICATION;
		if (class_exists('CGs')) {
		    $CGs = new CGs();
			 $arIsin = array();
			 foreach($arItems as $k=>$v){
				if(!in_array($v["CODE"], $arIsin)){
					$arIsin[] = $v["CODE"];
				}
			 }
			 $arHaveGs = $CGs->checkGsIsin($arIsin);
			 $arActivesInPortfolio = $CGs->getActivesInPortfolio($APPLICATION->modelPortfolioUsaId);
				foreach($arItems as $k=>$v){
					if(in_array($v["CODE"], $arHaveGs)){
						$arItems[$k]["DYNAM"]["HAVE_GS"] = 'Y';
					} else {
						$arItems[$k]["DYNAM"]["HAVE_GS"] = 'N';
					}
                    if(in_array($v["CODE"], $arActivesInPortfolio)){
                        $arItems[$k]["DYNAM"]["INTO_GS_PORTFOLIO"] = 'Y';
                    } else {
                        $arItems[$k]["DYNAM"]["INTO_GS_PORTFOLIO"] = 'N';
                    }
				 }

		}
	}

	private function setDynamParams($item) {
		$result = [];
		if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_DAY"]) {
			$result["DAY_INCREASE"] = round($item["PROPS"]["ONE_DAY_DROP_INCREASE"], 2);
		}
		if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_MONTH"]) {
			$result["MONTH_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_MONTH"]) - 1) * 100), 2);
		}
		if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_ONE_YEAR"]) {
			$result["YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_ONE_YEAR"]) - 1) * 100), 2);
		}
		if ($item["PROPS"]["LASTPRICE"] && floatval($item["PROPS"]["QUOTATIONS_THREE_YEAR"]) > 0) {
			$result["THREE_YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_THREE_YEAR"]) - 1) * 100), 2);
		}

		if (floatval($item["PROPS"]["TARGET_CUSTOM"]) > 0) {
			$result["Таргет"] = $item["PROPS"]["TARGET_CUSTOM"];
		} else if ($item["PROPS"]["TARGET"]) {
			$result["Таргет"] = $item["PROPS"]["TARGET"];
		} else {
			$result["Таргет"] = 0;
		}

		//new Просад = ( 1 - old Просад / LASTPRICE ( LEGALCLOSE) ) х 100
		if (!empty($item["PROPS"]["DRAWDOWN"]) && floatval($item["PROPS"]["LASTPRICE"]) > 0) {
			if ($item["PROPS"]["DRAWDOWN"] < 25) {
				$result["Просад"] = 25;
			} else if ($item["PROPS"]["DRAWDOWN"] > 50) {
				$result["Просад"] = 50;
			} else {
				$result["Просад"] = $item["PROPS"]["DRAWDOWN"];
			}
		} else {
			$result["Просад"] = 0;
		}

		//Цена лота
		$result["Цена лота"] = $item["PROPS"]["LASTPRICE"];

		//Прибыль
		if ($item["PERIODS"]) {
			$tmp      = sort_nested_arrays($item["PERIODS"], array('PERIOD_YEAR' => 'desc', 'PERIOD_VAL' => 'desc'));
			foreach($tmp as $periodTmp){
                if($periodTmp["CURRENT_PERIOD"]==false){
                    $lastItem = $periodTmp;
                    break;
                }
            }

			$curKvartal = intval((date('n') - 1) / 4) + 1;
			$availYear  = date("Y") - 1;

			try {
				$lastDate  = DateTime::createFromFormat('m.Y', $lastItem["PERIOD_VAL"] . "." . $lastItem["PERIOD_YEAR"]);
				$lastAvail = DateTime::createFromFormat('m.Y', $curKvartal . "." . $availYear);

				if ($lastDate && $lastAvail) {
					if ($lastDate->format("U") >= $lastAvail->format("U")) {
						$year     = DateTime::createFromFormat('Y', $lastItem["PERIOD_YEAR"]);
						$prevYear = clone $year;
						$prevYear->modify("-1year");

						//deprecated Данные для расчета PE теперь берутся из последнего квартала
                      //  $result = $this->calcStandartPeriodDynam($result, $item);
						
					}
				}
			} catch (Exception $e) {
			}
            
        
            
            //Средний PE по отрасли
			$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_MID_PE");
			$arFilter = Array("IBLOCK_ID" => IntVal(58), "NAME" => $item["PROPS"]["SECTOR"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1), $arSelect);
			while ($ob = $res->Fetch()) {
				$result["Cреднеотраслевой PE"] = $ob["PROPERTY_MID_PE_VALUE"];
			}

			//РЕ
			if ($lastItem["Прибыль за год (скользящая)"] && $item["PROPS"]["ISSUECAPITALIZATION"]) {
				$val = $item["PROPS"]["ISSUECAPITALIZATION"];
				if (!empty($item["PROPS"]["ISSUECAPITALIZATION_CURRENCY"])) {
					$val = $item["PROPS"]["ISSUECAPITALIZATION_CURRENCY"];
				}
				$result["PE"]       = round($val / $lastItem["Прибыль за год (скользящая)"] / 1000000, 1);
                if($lastItem["Активы"])
                    $result["PB"] = round($val/$lastItem["Активы"]/1000000, 2);
                if($lastItem["Выручка за год (скользящая)"])
                    $result["P/Sale"] = round($val/$lastItem["Выручка за год (скользящая)"]/1000000, 2);
					 if($lastItem["Собственный капитал"])
					     $result["P/Equity"] = round($val / $lastItem["Собственный капитал"] / 1000000, 1);

				if ($lastItem["AVEREGE_PROFIT"]) {
					$result["PEG"] = round($result["PE"] / $lastItem["AVEREGE_PROFIT"], 1);
				}
			}
		}

		//Дивиденды %
		if (!empty($item["PROPS"]["DIVIDENDS"]) && !empty($item["PROPS"]["LASTPRICE"])) {

						  		//Пересчитаем эквивалент дивов за год в валюту обращения для корректного расчета процента годовых
								$arRussianCurrency = array("RUB", "RUR", "SUR");
								$divSumm = 0;
								if($item["COMPANY"]["CURRENCY_DIVIDENDS"]!=$item["COMPANY"]["CURRENCY_CIRCULATION"]){
									$divSumm = $item["PROPS"]["DIVIDENDS"];
									$showEqDivSumm = true;
								  if(!in_array($item["COMPANY"]["CURRENCY_CIRCULATION"], $arRussianCurrency)){
									  $divRubRate = getCBPrice($item["COMPANY"]["CURRENCY_DIVIDENDS"]);
									  $circRubRate = getCBPrice($item["COMPANY"]["CURRENCY_CIRCULATION"]);
									  $divSumm = $divSumm*$divRubRate;
									  $divSumm = $divSumm/$circRubRate;
								  } else {
									  $circRubRate = getCBPrice($item["COMPANY"]["CURRENCY_CIRCULATION"]);
									  $divSumm = $divSumm*$circRubRate;
								  }
									$divSumm = round($divSumm, 3);
									//$item["PROPS"]["DIVIDENDS"] = $divSumm;
								}

			$result["Дивиденды %"] = round(($divSumm>0?$divSumm:$item["PROPS"]["DIVIDENDS"]) / $item["PROPS"]["LASTPRICE"] * 100, 2);
		}

		//Отсутствие дивидендов
		//Делаем выборку и смотрим в json для выявления отсутствующих дивидендов
		$hlblock     = HL\HighloadBlockTable::getById(32)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
			"filter" => array(
				"UF_ITEM" => $item["SECID"], "!=UF_DATA" => "[]"
			),
			"select" => array(
				"UF_DATA"
			),
		));
		$noDivs = true;
		if ($divItem = $res->fetch()) {
			$arDivs = json_decode($divItem["UF_DATA"], true);
			if (count($arDivs) > 0) {
				$noDivs = false;
			}
		}
		if (floatval($item["PROPS"]["DIVIDENDS"]) > 0) { //Исключаем ситуацию при наличии дивов за год но отсутствии данных в хайлоаде с дивами
			$noDivs = false;
		}
		$result["Нет дивидендов"] = $noDivs == true ? "Y" : "N";
		unset($res, $divItem);

		//Будущие дивиденды
		if (floatval($item["PROPS"]["FUTURE_DIVIDENDS"]) > 0 && floatval($item["PROPS"]["FUTURE_DIVIDENDS_PRC"]) > 0) {
			$result["Будущие дивиденды %"] = round($item["PROPS"]["FUTURE_DIVIDENDS_PRC"], 2);
			$result["Будущие дивиденды"]   = round($item["PROPS"]["FUTURE_DIVIDENDS"], 2);
		}
		return $result;
	}
	
	public function calcStandartPeriodDynam($period, $item){
        if ($lastItem["PERIOD_VAL"] == 1) {
            //если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
            if (
              isset($item["PERIODS"]["1-" . $year->format("Y") . "-KVARTAL"]["Прибыль"]) &&
              isset($item["PERIODS"]["4-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"]) &&
              isset($item["PERIODS"]["1-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"])
            ) {
                $result["Прибыль"] =
                  $item["PERIODS"]["1-" . $year->format("Y") . "-KVARTAL"]["Прибыль"] +
                  $item["PERIODS"]["4-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"] -
                  $item["PERIODS"]["1-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"];
            }
            $result["Собственный капитал"] = $item["PERIODS"]["1-" . $year->format("Y") . "-KVARTAL"]["Собственный капитал"];
            $result["AVEREGE_PROFIT"]                        = $item["PERIODS"]["1-" . $year->format("Y") . "-KVARTAL"]["AVEREGE_PROFIT"];
        } elseif ($lastItem["PERIOD_VAL"] == 2) {
            //если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
            if (
              isset($item["PERIODS"]["2-" . $year->format("Y") . "-KVARTAL"]["Прибыль"]) &&
              isset($item["PERIODS"]["4-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"]) &&
              isset($item["PERIODS"]["2-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"])
            ) {
                $result["Прибыль"] =
                  $item["PERIODS"]["2-" . $year->format("Y") . "-KVARTAL"]["Прибыль"] +
                  $item["PERIODS"]["4-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"] -
                  $item["PERIODS"]["2-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"];
            }
            $result["Собственный капитал"] = $item["PERIODS"]["2-" . $year->format("Y") . "-KVARTAL"]["Собственный капитал"];
            $result["AVEREGE_PROFIT"]                        = $item["PERIODS"]["2-" . $year->format("Y") . "-KVARTAL"]["AVEREGE_PROFIT"];
        } elseif ($lastItem["PERIOD_VAL"] == 3) {
            //если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
            if (
              isset($item["PERIODS"]["3-" . $year->format("Y") . "-KVARTAL"]["Прибыль"]) &&
              isset($item["PERIODS"]["4-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"]) &&
              isset($item["PERIODS"]["3-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"])
            ) {
                $result["Прибыль"] =
                  $item["PERIODS"]["3-" . $year->format("Y") . "-KVARTAL"]["Прибыль"] +
                  $item["PERIODS"]["4-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"] -
                  $item["PERIODS"]["3-" . $prevYear->format("Y") . "-KVARTAL"]["Прибыль"];
            }
            $result["Собственный капитал"] = $item["PERIODS"]["3-" . $year->format("Y") . "-KVARTAL"]["Собственный капитал"];
            $result["AVEREGE_PROFIT"]                        = $item["PERIODS"]["3-" . $year->format("Y") . "-KVARTAL"]["AVEREGE_PROFIT"];
        } elseif ($lastItem["PERIOD_VAL"] == 4) {
            //если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
            if (
            isset($item["PERIODS"]["4-" . $year->format("Y") . "-KVARTAL"]["Прибыль"])
            ) {
                $result["Прибыль"] =
                  $item["PERIODS"]["4-" . $year->format("Y") . "-KVARTAL"]["Прибыль"];
            }
            $result["Собственный капитал"] = $item["PERIODS"]["4-" . $year->format("Y") . "-KVARTAL"]["Собственный капитал"];
            $result["AVEREGE_PROFIT"]                        = $item["PERIODS"]["4-" . $year->format("Y") . "-KVARTAL"]["AVEREGE_PROFIT"];
        }
        
        return $result;
    }
}