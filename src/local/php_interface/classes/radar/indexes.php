<?//Класс для работы с индексами
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

class CIndexes {
		private $IndexesIblockId = 53; //Инфоблок Индексы
		private $ActionsIblockId = 32; //Инфоблок Акции
		private $IndexesHLId = 34; //HL блок акции в индексах
		private $arMainHLIndexes = array(); //Массив индексов из HL c вложенными массивами входищими в индекс акциями
		private $arMainIndexes = array(); //Массив из тикеров getIndexesHL_tickers(), т.е. те, в составе которых указаны акции и они выведены отдельным верхним блоком в списке индексов
		private $arMainCandidates = array(); //Массив активов, с ключами по ID и значением в виде массива индексов в которые акция является кандидатом
	function __construct(){
		//$this->setData();
		//$this->havePayAccess = checkPayRadar();
	}


	/**
	 * Возвращает массив индексов
	 *
	 * @param  array   $arrFilter (Optional)  Можно передать массив кодов нужных индексов для выборки или любой другой фильтр
	 *
	 * @return array
	 *
	 * @access public
	 */
	public function getIndexesList($arrFilter = array()){
		$arResult = array();
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_EXCLUDE_FILTER");
      $arFilter = Array("IBLOCK_ID"=>IntVal($this->IndexesIblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		if(count($arrFilter)>0){
		 $arFilter = array_merge($arFilter, $arrFilter);
		}
      $res = CIBlockElement::GetList(Array("CODE"=>"ASC"), $arFilter, false, false, $arSelect);
      while($ob = $res->GetNextElement()){
       $arFields = $ob->GetFields();
       $arProps = $ob->GetProperties();
		 $arResult[$arFields["CODE"]] = array("ID"=>$arFields["ID"], "NAME"=>$arFields["NAME"], "EXCLUDE_FILTER"=>$arProps["EXCLUDE_FILTER"]["VALUE"]);

      }
		return $arResult;
	}

	//Возвращает список уникальных тикеров индексов из HL блока "Акции в индексах"
	public function getIndexesHL_tickers(){
		$arResult = array();
		$hlblock   = HL\HighloadBlockTable::getById($this->IndexesHLId)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();

		$result = $entityClass::getList([
			//'filter' => ['какой-нибудь фильтр'],
			//тут важно указать название столбца таблицы
			//(свойства HL блока, обычно начинающегося с UF)
			//по которому мы будем группировать отчет
			//второй элемент, это название для столбца,
			//в котором будут выведены итоговые суммы, в данном случае пусть это будет COUNT
			'select' => [
				'UF_INDEX',
				'COUNT'
			],

			//сортировку можно делать по столбцу с суммами
			'order' => [
				'COUNT' => 'DESC'
			],

			//тут мы должны указать как будет считаться столбец COUNT
			'runtime' => [
				//в качестве ключа используется поле, которое мы указали в select
				'COUNT' => [
					//тут указывается тип данных, для поля.
					//я встречал только примеры с integer и reference
					'data_type' => 'integer',

					//тут указывается функция, вроде count, max и т.п.
					//важно следить за кавычками и указывать правильные имена таблицы
					//(они обычно не совпадают с названиями HL блока)
					'expression' => ['count(`UF_INDEX`)']
				]
			]
		]);
		 while($row = $result->Fetch()){
			$arResult[$row["UF_INDEX"]] = $row["COUNT"];
		 }
	  return $arResult;
	}

	//Возвращает список уникальных тикеров индексов из HL блока "Акции в индексах" с добавлением остальных данных об индексе
	public function getIndexesHL_main(){
		$arResult = array();
		$hlblock   = HL\HighloadBlockTable::getById($this->IndexesHLId)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();

		$result = $entityClass::getList([
			'select' => [
				'*',
			],

			//сортировку можно делать по столбцу с суммами
			'order' => [
				'UF_INDEX' => 'ASC'
			],

		]);
		 while($row = $result->Fetch()){
			$arResult[$row["UF_INDEX"]][$row["UF_SHARE_CODE"]] = array("FREEFLOAT"=>$row["UF_FREEFLOAT"], "SHARE_OF_INDEX"=>$row["UF_SHARE_OF_INDEX"]);
		 }
	  return $arResult;
	}

	//Возвращает по коду индекса ID Акций-кандидатов
	public function getIndexCandidates($inndexCode, $codeMode=false){
		$arResult = array();
		$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_CANDIDATES");
      $arFilter = Array("IBLOCK_ID"=>IntVal($this->IndexesIblockId), "CODE"=>$inndexCode ,"ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
      $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($codeMode){
			$resA = new Actions();
		}
      while($ob = $res->fetch()){
      	if($codeMode){
			  $arItem = $resA->getItemById($ob["PROPERTY_CANDIDATES_VALUE"]);
			  if(array_key_exists("SECID",$arItem))
			   $arResult[] = $arItem["SECID"];
      	} else {
			  $arResult[] = $ob["PROPERTY_CANDIDATES_VALUE"];
      	}

      }
		unset($resA);
		return $arResult;
	}

	/**
	 * Пересчитывает для всех акций кандидатов на вхождение в индексы и сохраняем в инфоблоке индексы в св-во CANDIDATES и для каждой акции набор индексов в которые она является кандидатом
	 *
	 * @access public
	 */
	public function setIndexCandidate(){
	$resA = new Actions();

	//Получаем из HL основные заполненные индексы и их состав акций
	$arTickerListHL = $this->getIndexesHL_main();

	$arIndexMainFilter = array("CODE"=>array_keys($arTickerListHL));
	$this->arMainIndexes = $this->getIndexesList($arIndexMainFilter);

	 //Формируем фильтр исключения
	  $arExcludeActionsFilter = array();

	  foreach($this->arMainIndexes as $indCode=>$arData){
		if(count($arData["EXCLUDE_FILTER"])>0){
			foreach($arData["EXCLUDE_FILTER"] as $excl_id){
			  $arExcludeActionsFilter[] = $excl_id;
			}
		  //	$arExcludeActionsFilter = array_merge($arExcludeActionsFilter, $arData["EXCLUDE_FILTER"]);
		}

	  }


	  if(count($arExcludeActionsFilter)>0){
	  	$arSelect = Array("ID", "NAME", "IBLOCK_ID","CODE");
	  	$arFilter = Array("IBLOCK_ID"=>IntVal($this->ActionsIblockId), "ID"=>$arExcludeActionsFilter, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	  	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	  	while($ob = $res->fetch()){
		  $arExcludeActions[$ob["ID"]] = $ob["CODE"];
	  	}
	  }

	//Перебираем полученные индексы
	foreach($this->arMainIndexes as $code=>$arIndex){
	  $arIndexStaff = array("MAIN"=>array(), "IN"=>array() );

		//Заполняем массивы данными по акциям и значениями для фильтра
		foreach($arTickerListHL[$code] as $k=>$v){
		  $arIndexStaff["MAIN"][] = $resA->getItem($k);
		  $arIndexStaff["MAIN_FILTER"][] = $k;
		}

	 //Если задан фильтр исключения - добавим в общий фильтр его позиции
	 if(count($arIndex["EXCLUDE_FILTER"])>0){
	  foreach($arIndex["EXCLUDE_FILTER"] as $excludeId){
		$arIndexStaff["MAIN_FILTER"][] = $arExcludeActions[$excludeId];
	  }
	 }

		//Отсортируем массив входящих акций по свойству "Капитализация" по убыванию
	 	usort($arIndexStaff["MAIN"], function ($item1, $item2) {
	    return $item2["PROPS"]["CAP_X_FREEFLOAT"] <=> $item1["PROPS"]["CAP_X_FREEFLOAT"];
		});

	 //Получим кандидатов на добавление в индекс
	  $arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE", "PROPERTY_CAP_X_FREEFLOAT");
	  $arFilter = Array("IBLOCK_ID"=>IntVal($this->ActionsIblockId), "!CODE"=>$arIndexStaff["MAIN_FILTER"],  "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	  $res = CIBlockElement::GetList(Array("PROPERTY_CAP_X_FREEFLOAT"=>"DESC"), $arFilter, false, Array("nTopCount"=>5), $arSelect);
	  while($ob = $res->Fetch()){
	  	 $arCandidatIn = $resA->getItem($ob["CODE"]);
		 //Достаем фрифлоат отдельно, т.к. кандидат может быть не в текущем индексе.
		 $arCandidat = reset($arCandidatIn["DYNAM"]["Доля в индексах"]);
		 $arCandidatIn["DYNAM"]["FREEFLOAT"] = floatVal($arCandidat["FREEFLOAT"]);
		 //if($format=='min'){
		 //	$this->arMainIndexes[$code]["CANDIDATES"][] = array("SECID"=>$arCandidatIn["SECID"], "NAME"=>$arCandidatIn["NAME"]);
		 //} else {
		 	$this->arMainIndexes[$code]["CANDIDATES"][] = $arCandidatIn;
		 //}
		 $this->arMainIndexes[$code]["IN"][] = $arCandidatIn["ID"];
	  }

	  //Сохраняем список кандидатов в элемент индекса
		 $res = CIBlockElement::SetPropertyValuesEx($this->arMainIndexes[$code]["ID"],$this->IndexesIblockId, array("CANDIDATES" => $this->arMainIndexes[$code]["IN"]));

	}

	unset($resA, $arExcludeActionsFilter, $arExcludeActions, $res, $arSelect, $arFilter);

//	return $this->arMainIndexes;
	}

}