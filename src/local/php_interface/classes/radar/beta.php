<?php //Класс для расчета беты для американских и Российских акций
use Bitrix\Highloadblock as HL;
//use Bitrix\Main\Entity;
CModule::IncludeModule('highloadblock');
\Bitrix\Main\Loader::includeModule('iblock');


class CBeta extends CPortfolio {

	public $spb_actions_HL_Id       = 29; //id HL инфоблока с ценами акций
	public $spb_sp500_HL_Id         = 33; //id HL инфоблока с ценами индекса SP500
	public $spb_actions_iblockId    = 55; //id инфоблока с акциями США
	public $arDatesListUSA          = array(); //Колонка A - Список дат от сегодня и назад на -n лет
	public $yearsCalc               = 5; //Кол-во лет для диапазона дат в прошлое
	public $arSP500Prices           = array(); //Цены индекса SP500 для диапазона дат
	public $arSP500NowDate          = ''; //Последняя дата на которую есть цена индекса SP500, т.е. текущая последняя значащая дата
	public $arSP500PastDate         = ''; //Первая дата диапазона цен индекса. Расчитаывается как -n лет от $arSP500NowDate
	public $sheduleBetaUSAFlagName  = 'SHEDULE_RECALCULATE_BETA_USA'; //Свойство для планирования пересчета беты для акций США
	public $lastDateCalcBetaUSAName = 'LAST_RECALCULATE_BETA_USA'; //Свойство для фиксации последней даты пересчета беты для акций США
	public $sheduleBetaUSAFlagValue = 'N'; //Разрешение для пересчета беты для акций США
	public $BASIC_RISKFREE_RATE_USA = 0;
	public $actionUSAStartDates     = array(); //Стартовые даты цен на акции США в БД. Заполняется при получении цен на каждый актив

	function __construct() {
		Global $APPLICATION;
		$this->spb_actions_HL_Id = $APPLICATION->usaPricesHlId;
		$this->checkSheduleBetaUSACalc(); //Получаем флаг запланированного пересчета по США
	}

	//Подготавливает данные для пересчета беты для акций США
	public function initCalculateUsaData() {
		if ($this->sheduleBetaUSAFlagValue != 'Y') {
			return false;  //Если не запланирован пересчет по США - выходим
		}
		
	   //Общая инициализация и для пересчета и для вызова из Tools для проверки расчета
		$this->initCommonUsaData();

		return true;
	}

	//Общая инициализация и для пересчета и для вызова из Tools для проверки расчета
	public function initCommonUsaData(){
		$this->BASIC_RISKFREE_RATE_USA = floatval(COption::GetOptionString("grain.customsettings", "BASIC_RISKFREE_RATE_USA")) / 360;
		$this->arSP500NowDate  = $this->getSP500IndexLastUpdateDate(); //Последняя дата на которую есть цена индекса SP500, т.е. текущая последняя значащая дата
		$this->arSP500PastDate = (new DateTime($this->arSP500NowDate))->modify('-5 years')->format('d.m.Y'); //Получаем дату начала диапазона выборки цен индекса SP500
		$this->arSP500Prices   = $this->getSP500PricesForBeta();
	}

	public function getTickerUSAList(){
		$arReturn = array();
		$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_SECID");
      $arFilter = Array("IBLOCK_ID"=>IntVal($this->spb_actions_iblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
      $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
      while($ob = $res->fetch()){
		  $arReturn[$ob["PROPERTY_SECID_VALUE"]] = $ob["ID"];
      }
		return $arReturn;
	}

	//Получает дату начала обращения акции США по данным в HL блоке
	public function getActionDateStart($SECID = '') {
		$arActionsUSAdateStart = array();
		if (empty($SECID)) {
			return $arActionsUSAdateStart;
		}

		$hlblock     = HL\HighloadBlockTable::getById($this->spb_actions_HL_Id)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();
		$arFilter    = array('=UF_ITEM' => $SECID);
		$res         = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array(
				"UF_ITEM", "UF_DATE"
			),
			"limit" => 1,
			"order" => array(
				"UF_DATE" => "ASC"
			),
			//"group"=>array("UF_ITEM"),
			//"cache" => array("ttl" => 3600)
		));

		while ($item = $res->fetch()) {
			$arActionsUSAdateStart = (new DateTime($item["UF_DATE"]))->format('d.m.Y');
		}
		return $arActionsUSAdateStart;
	}

	public function calculateOneActionUSABeta($SECID, $return_debug = false) {
		$arReturn      = array();
		$arSharePrices = $this->getHLActivesUSAPricesForBeta($SECID); //Колонка C - Цены акции



		//Складываем полученные массивы в общий массив
		$cnt = 0;
		foreach ($arSharePrices as $date => $val) {
			$arSumm[$cnt] = array("DATE" => $date, "SUM_USA" => $val, "SP500" => $this->arSP500Prices[$date]);
			$cnt++;
		}

		//Расчет необходимых данных
		$arDebugSP500Prices = array();
		for ($i = 1; $i <= count($arSumm) - 1; $i++) {
			$arDebugSP500Prices[$arSumm[$i - 1]["DATE"]] = $this->arSP500Prices[$arSumm[$i - 1]["DATE"]]; //Собираем образанный слева массив цен индекса для вывода в отладку
			$curr_i                                      = $i - 1;
			$arIndexProfit[$curr_i]                      = log(floatval($arSumm[$i]["SP500"]) / floatval($arSumm[$i - 1]["SP500"])) * 100; //Столбец D - Доходность индекса
			$arShareProfit[$curr_i]                      = log(floatval($arSumm[$i]["SUM_USA"]) / floatval($arSumm[$i - 1]["SUM_USA"])) * 100; //Столбец E - Доходность акции
			$arDayliReturn[$curr_i]                      = round($this->BASIC_RISKFREE_RATE_USA, 4); //Столбец F - Допустимая дневная доходность
			$arIndexCovar[$curr_i]                       = ($arShareProfit[$curr_i] / 100 - $arDayliReturn[$curr_i] / 100) * ($arIndexProfit[$curr_i] / 100 - $arDayliReturn[$curr_i] / 100); //Столбец G - Ковариация доходности актива и индекса
			$arIndexDispersion[$curr_i]                  = pow($arIndexProfit[$curr_i] - $arDayliReturn[$curr_i], 2) / 10000; //Столбец H - Дисперсия доходности индекса
		}

		if ($return_debug == true) {
			$arReturn['DEBUG'] = array(
				"A" => array_keys($arSharePrices),
				"B" => array_values($arDebugSP500Prices),
				"C" => array_values($arSharePrices),
				"D" => $arIndexProfit,
				"E" => $arShareProfit,
				"F" => $arDayliReturn,
				"G" => $arIndexCovar,
				"H" => $arIndexDispersion,
			);

			$arReturn["DEBUG"]["LIST_OF_DATA"] = $arSumm;
		}

		unset($arDebugSP500Prices);
		//Расчет Беты
		$beta             = (array_sum($arIndexCovar) / count($arIndexCovar)) / (array_sum($arIndexDispersion) / count($arIndexDispersion));
		$arReturn["BETA"] = $beta;
		unset($arSharePrices, $arSumm, $arDebugSP500Prices, $arIndexProfit, $arShareProfit, $arDayliReturn, $arIndexCovar, $arIndexDispersion);
		return $arReturn;
	}

	//Получаем дату последнего обновления индекса SP500
	public function getSP500IndexLastUpdateDate() {
		$return = '';

		$hlblock     = HL\HighloadBlockTable::getById($this->spb_sp500_HL_Id)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();
		$arFilter    = array('!UF_CLOSE' => false);

		$res = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array(
				"UF_DATE"
			),
			"limit" => 1,
			"order" => array(
				"UF_DATE" => "DESC"
			),
			//"cache" => array("ttl" => 3600)
		));

		if ($item = $res->fetch()) {
			$return = $item["UF_DATE"];
		}
		return $return;
	}

	//Возвращает цены индекса SP500 по массиву дат, Формирует массив дат для дальнейших расчетов
	public function getSP500PricesForBeta() {
		$arPrices = array();
		//Получаем цены на индекс
		$hlblock     = HL\HighloadBlockTable::getById($this->spb_sp500_HL_Id)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();
		$arFilter    = array('<=UF_DATE' => $this->arSP500NowDate, '>=UF_DATE' => $this->arSP500PastDate);
		$res         = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array(
				"UF_DATE", "UF_CLOSE"
			),
			//"limit" => 20,
			"order" => array(
				"UF_DATE" => "ASC"
			),
			//"cache" => array("ttl" => 3600)
		));
		$this->arDatesListUSA = array();
		while ($item = $res->fetch()) {
			$date                        = (new DateTime($item["UF_DATE"]))->format('d.m.Y');
			$arPrices[$date]             = $item["UF_CLOSE"];
			$this->arDatesListUSA[$date] = 0;
		}
		return $arPrices;
	}

	//проверяет и возвращает сокращенный слева диапазон дат для расчетов
	public function getDatesListUSAForAction($SECID = '') {
		if (empty($SECID)) {
			return $this->arDatesListUSA;
		}

		$arReturn = array();
		foreach ($this->arDatesListUSA as $kd => $v) {
			if (strtotime($this->actionUSAStartDates[$SECID]) <= strtotime($kd)) {
				$arReturn[$kd] = 0;
			}
		}
		return $arReturn;
	}

	//Получает набор цен по активам США для расчета беты из поля UF_CLOSE_BETA (Загружаемые вручную, точные цены)
	public function getHLActivesUSAPricesForBeta($SECID) {
		$arPrices = array();
		$this->actionUSAStartDates = array();
		//Получаем стартовую дату для акции
		$this->actionUSAStartDates[$SECID] = $this->getActionDateStart($SECID);

		$arDatesListForAction = $this->getDatesListUSAForAction($SECID); //Корректируем слева массив дат для выборки
		if(!count($arDatesListForAction)){
			return $arPrices;
		}
		//Получаем цены на акцию
		$hlblock     = HL\HighloadBlockTable::getById($this->spb_actions_HL_Id)->fetch();
		$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();
		//$arFilter    = array('UF_DATE' => array_keys($this->arDatesListUSA), '=UF_ITEM' => $SECID);
		$arFilter = array('UF_DATE' => array_keys($arDatesListForAction), '=UF_ITEM' => $SECID);

		$res      = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array(
				"UF_ITEM", "UF_DATE", "UF_CLOSE_BETA"
			),
			//"limit" => 20,
			"order" => array(
				"UF_DATE" => "ASC"
			),
			//"cache" => array("ttl" => 3600)
		));

		while ($item = $res->fetch()) {
			$date            = (new DateTime($item["UF_DATE"]))->format('d.m.Y');
			$arPrices[$date] = $item["UF_CLOSE_BETA"];
		}

		//Преобразуем цены акции из формата портфеля в необходимый для расчетов дата=>цена
		$arTmp = array();
		foreach ($arDatesListForAction as $dt => $share) {
			$arTmp[$dt] = $arPrices[$dt];
		}
		if (count($arTmp) > 0) {
			$arPrices = $arTmp;
			unset($arTmp, $arDatesListForAction);
		}

		return $arPrices;
	}

	//Проверяет флаг запланированного пересчета беты для акций США, доп. настройки, вкладка коэффициенты и бета, SHEDULE_RECALCULATE_BETA_USA
	public function checkSheduleBetaUSACalc() {
		$this->sheduleBetaUSAFlagValue = COption::GetOptionString("grain.customsettings", $this->sheduleBetaUSAFlagName);
	}

	//Устанавливает флаг планирования пересчета беты США
	public function setSheduleBetaUSA($value) {
		COption::SetOptionString("grain.customsettings", $this->sheduleBetaUSAFlagName, $value ? 'Y' : 'N');
	}

	//Записывает в параметры время окончания пересчета беты США
	public function setSheduleBetaUSADate($date) {
		COption::SetOptionString("grain.customsettings", $this->lastDateCalcBetaUSAName, $date);
	}

}
