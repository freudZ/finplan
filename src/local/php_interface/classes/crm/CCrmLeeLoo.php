<?
//Класс для взаимодействия с системой LeeLoo через rest api
class CCrmLeeLoo {
  protected $token = "28vt2qbyjxegn0g8jx8v830g64z8n0i8gd1gu2ri1o7dwbtozd1ze1wjvu2tvtas6ggbtmrcl8534ic4s9751fpew2xmbr3yvsn2";
  protected $syncLogFile;

  public function __construct(){
  	$this->syncLogFile = $_SERVER["DOCUMENT_ROOT"] . "/log/LeeLoo_log.txt";
  }

  private function ConnectLeeLoo($dataUrl, $queryType = "GET", $arExtQuery = array()) {
		$arResult = array();
  		$client = new \GuzzleHttp\Client(['headers' => ['X-Leeloo-AuthToken' => $this->token]]);

		try {
		$arQuery = array('http_errors' => true, 'form_params' => $arExtQuery);
/*		if(count($arExtQuery)>0){
			$arQuery = array_merge($arQuery, array("body"=>$arExtQuery));
		}*/
		$res = $client->request($queryType, $dataUrl, $arQuery);
		$arResult = $res->getBody()->getContents();
		$arResult = json_decode($arResult, true);


		} catch (RequestException $e) {

			$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectLeeLoo, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getRequest());
			file_put_contents($this->syncLogFile, $log . PHP_EOL, FILE_APPEND);

			if ($e->hasResponse()) {
				$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectLeeLoo, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getResponse());
				file_put_contents($this->syncLogFile, $log . PHP_EOL, FILE_APPEND);

			}
		}
	 return $arResult;
  }

  //Получить список пользователей
  public function getUsersList(){

	 $result = $this->ConnectLeeLoo("https://api.leeloo.ai/api/v1/users?limit=3&offset=0");

  }

  //Получить список людей
  public function getPeopleList($limit = 20, $offset=0){
  	$arResult = array();
  	$arParams = array("limit"=>$limit, "offset"=>$offset );
	$arFilter = array("tags"=>array(), "last_message"=>array("from"=>"2011-01-01","to"=>(new DateTime())->format("Y-m-d")));

	$arQueryParams = "";

	foreach($arParams as $key=>$val){
	  $arQueryParams .= (empty($arQueryParams)?"?":"&").$key."=".$val;
	}

	foreach($arFilter as $key=>$val){
		if(count($val)>0 && $key=="tags"){
		  for($i=0; $i<count($val); $i++)
		   $arQueryParams .= "&filter[tags]=".$val[$i];
		}
		if($key=="last_message"){
			$arQueryParams .= "&filter[last_message][from]=".$val["from"];
			$arQueryParams .= "&filter[last_message][to]=".$val["to"];
		}
	}
	 $arResult = $this->ConnectLeeLoo("https://api.leeloo.ai/api/v2/people".$arQueryParams);




	 return $arResult;
  }

  //Получить информацию по одному человеку
  public function getPeople($id){
  	$arResult = array();
  	//https://api.leeloo.ai/api/v2/people/595f5d522a934035decc093d?include=contactedUsers,orders
	$arResult = $this->ConnectLeeLoo("https://api.leeloo.ai/api/v2/people/".$id);
	return $arResult;
  }

  public function getFullPeopleList(){
  	$arResult = array();
    try{
    $loop = true;
    $startPage = 0;
	 do{
	  $data = $this->getPeopleList(20, $startPage);

	  if(is_array($data["data"]) && count($data["data"])>0){
		foreach($data["data"] as $k=>$v){
		 $arResult[] = array("id"=>$v["id"], "name"=>$v["name"]);
		}

	  }

	  $startPage += 20;
	  $loop = (!$data["data"]) ? false : true;
	 }while($loop);


  } catch (Exception $e){
	mail("alphaprogrammer@gmail.com", "Error: CCrmLeeLoo getFullPeopleList", print_r($e->getMessage(), true));
    var_dump("getFullPeopleList: " . $e->getMessage());
		}

		return $arResult;
	 }

  public function getTags(){
  	$arResult = array();
  	//https://api.leeloo.ai/api/v2/people/595f5d522a934035decc093d?include=contactedUsers,orders

   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cacheId = "leeloo_tags";
	$cacheTtl = 86400 * 7;
        //$cache->clean("leeloo_tags");
	     if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else {
        	$arResult = $this->ConnectLeeLoo("https://api.leeloo.ai/api/v2/tags");
			$cache->set($cacheId, $arResult);
        }

	return $arResult;
  }

  /**
   * Отправить сообщение в чат пользователю
   *
   * @param  string   $channelId ID канала связи  - можно узнать в чатах.
   * @param  string   $message сообщение
   *
   * @return array  результат отправки сообщения
   *
   * @access public
   */
  public function sendMessage($channelId, $message){
 	  $arResult = $this->ConnectLeeLoo("https://api.leeloo.ai/api/v2/messages/send-message", "POST", array("account_id"=>$channelId, "text"=>$message));
	  return $arResult;
  }

  //Установить тег пользвателю
  public function setTags($uid, $tag){
	  $arResult = $this->ConnectLeeLoo("https://api.leeloo.ai/api/v2/people/".$uid."/add-tag", "PUT", array("tag_id"=>$tag));
	  return $arResult;
  }

  //Получить список тоннелей
  public function getTunnelsList(){
	$arResult = $this->ConnectLeeLoo("https://api.leeloo.ai/api/v2/tunnels?limit=20&offset=0");
	return $arResult;
  }

  //Получить информацию по тоннелю
  public function getTunnelInfo($tid){
  	$arResult = $this->ConnectLeeLoo("https://api.leeloo.ai/api/v2/tunnels/".$tid."?include=leadgentools");
	return $arResult;
  }

  public function getUserEventParams($uid=''){
  	global $USER;
	if(intval($uid)<=0){
		$uid = $USER->GetID();
	}
	$data = CUser::GetList(($by="ID"), ($order="ASC"),
	            array(
	                //'!UF_EVENTS_CHAT_ID' => false,
						 'ID' => intval($uid)
	            ),
				  array("SELECT"=>array("UF_*"))
	        );
	$arEventParams = array();
	while($arUser = $data->Fetch()) {
	  $arEventParams = array(
		"UF_EVENTS_ENABLE" => $arUser["UF_EVENTS_ENABLE"],
		"UF_EVENTS_CHAT_ID" => $arUser["UF_EVENTS_CHAT_ID"],
		"UF_EVENTS_PORTFOLIO" => explode(",",$arUser["UF_EVENTS_PORTFOLIO"]),
		"UF_EVENTS_TYPES" => explode(",",$arUser["UF_EVENTS_TYPES"]),
		"UF_EVENTS_USE_FAVORITES" => $arUser["UF_EVENTS_USE_FAVORITES"],
	  );
	}
/*	 if(!count($arEventParams)){
	  $arEventParams = array( "UF_EVENTS_ENABLE" => 0 );
	}*/
	return $arEventParams;
  }

  private function checkStrLenMessage($currentBlockCnt, $message='', $limit=1300){
	 if(strlen($message)>=$limit){
		$currentBlockCnt++;
	 }
	  return $currentBlockCnt;
  }

  //Рассылает сообщения согласно настройкам пользователей
  public function sendEventMessagesLeeLoo($userId=''){
  	global $USER;

	//Получить список пользователей со включенной рассылкой
	$arUsersFilter = array(
	                'UF_EVENTS_ENABLE' => 1,
	                '!UF_EVENTS_CHAT_ID' => false
	            );
	if(intval($userId)>0){
	  $arUsersFilter["ID"] = intval($userId);
	}
	$data = CUser::GetList(($by="ID"), ($order="ASC"),
	           $arUsersFilter,
				  array("SELECT"=>array("UF_*"))
	        );
	$arSendUsers = array();
	while($arUser = $data->Fetch()) {
	    $arSendUsers[$arUser['ID']] = array("NAME"=>$arUser['NAME'], "EMAIL"=>$arUser['EMAIL'], "UF_EVENTS_CHAT_ID"=>$arUser['UF_EVENTS_CHAT_ID']);
	}




	//Получаем события за день вперед

	$dt = new DateTime();
	$_REQUEST["date"] = (new DateTime())->modify('+1 days')->format('d.m.Y');

	if ($dt->format("N") == 5) {  //Если пятница - то получаем события за выходные и понедельник
		$_REQUEST["dateto"] = $dt->modify("+3 days")->format('d.m.Y');
	} else {
		$_REQUEST["dateto"] = $_REQUEST["date"];
	}


	$symbLimit = 1300;// Кол-во символов в одном сообщении

	//Отправка сообщений
	$sendedCnt = 0;
	$sendResult = array();
   foreach($arSendUsers as $uid => $arUser){
   	//Получаем события и портфели пользователя для фильтра
		$arUserEventParams = $this->getUserEventParams($uid);
		$_REQUEST["eventTypes"] = implode(",", $arUserEventParams["UF_EVENTS_TYPES"]);
		if(!empty($arUserEventParams["UF_EVENTS_PORTFOLIO"]))
		 $_REQUEST["eventPortfolio"] = $arUserEventParams["UF_EVENTS_PORTFOLIO"];
		if($arUserEventParams["UF_EVENTS_USE_FAVORITES"]==1){
			$_REQUEST["eventFavorites"] = 'Y';
			}


	$arEvents = array(); //События календаря
	$CREvents = new CRadarEvents();

		$CREvents->setFilter($uid);

		$arEvents = $CREvents->arEventsFiltered;

		$messageText = array();
		$messageBlockCnt = 0;
		if(count($arEvents)){
			$siteUrl = 'https://fin-plan.org';
			$cntEventsForUser = count($arEvents);
			foreach($arEvents as $date=>$arEventItems){
			 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
			 $messageText[$messageBlockCnt] .= 'События на '.$date.PHP_EOL.'-------'.PHP_EOL;
			 $messageText[$messageBlockCnt] .= 'Настроить отслеживание событий Вы можете в личном кабинете https://fin-plan.org/personal/#events_subscribe?uid='.$uid.PHP_EOL.PHP_EOL;
			 $eventTypeText = '';
			  foreach($arEventItems as $eventCode=>$eventList){
			  	if(count($eventList)==0) continue;
				 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
				 $messageText[$messageBlockCnt] .= $CREvents->arEventsTypes[$eventCode].':'.PHP_EOL;
				 $ecnt = 0;
					foreach($eventList as $detailEvent){
						if(count($eventList)==1){
						  $ecntTxt = '';
						} else {
						  $ecnt++;
						  $ecntTxt = $ecnt.') ';
						}

					  switch ($eventCode) {
						    case "DIVIDENDS":
						    case "DIVIDENDS_USA":
							    $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
						       $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' Тек.цена акции: '.$detailEvent["Цена на дату закрытия"].' Дивиденды: '.$detailEvent["Дивиденды"].PHP_EOL;
						       break;
							 case "COUPONS":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' '.($detailEvent["DEFAULT"]?'(дефолт)':'').($detailEvent["STRUCTURED"]?'(структурная)':'').' Купон: '.$detailEvent["-"].(is_numeric($detailEvent["-"])?' руб.':'').' Купон, в %: '.$detailEvent["Ставка купона"].(is_numeric($detailEvent["-"])?' %':'').PHP_EOL;
								 break;
							 case "CANCELLATION":
							 case "OFERTA":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' '.($detailEvent["DEFAULT"]?'(дефолт)':'').($detailEvent["STRUCTURED"]?'(структурная)':'').PHP_EOL;
							    break;
							 case "REPORT_USA":
							 case "REPORT_RUS":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
							    $arTitle = explode(" - ",$detailEvent["UF_TITLE"]);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["UF_EMITENT"].' - '.$detailEvent["UF_LINK"].' '.(count($arTitle)>1?$arTitle[1]:$detailEvent["UF_TITLE"]).PHP_EOL;
							    break;
							 case "GOSA":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .=  $ecntTxt.$detailEvent["UF_TITLE"].(!empty($detailEvent["UF_LINK"])?' '.$detailEvent["UF_LINK"]:'').PHP_EOL;
							 	 break;
							 default:
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["UF_TITLE"].( !empty($detailEvent["UF_DESCRIPTION"])?' '.$detailEvent["UF_DESCRIPTION"].' ':'').( !empty($detailEvent["UF_LINK"])?' '.$detailEvent["UF_LINK"]:'').PHP_EOL;
							 	 break;
						}

					}
					$messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
					$messageText[$messageBlockCnt] .= PHP_EOL;

			  }
			  //$messageText .= $eventTypeText;
			}

		}


   	foreach($arUser["UF_EVENTS_CHAT_ID"] as $chartId){
   		if($cntEventsForUser>0){
   		  for($m=0; $m<count($messageText); $m++){
		  	  $sendResult[] = $this->sendMessage($chartId, $messageText[$m]);
			  $sendedCnt++;
			  }
			  }
   	}

		unset($CREvents, $arEvents, $messageText);
   }



	 return json_encode(array("result"=>"success", "sendResult"=> explode(", ",$sendResult), "message"=>"Отправлено сообщений: ".$sendedCnt));
  }


}//class
