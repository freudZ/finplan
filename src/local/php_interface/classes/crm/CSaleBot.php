<?
//Класс для взаимодействия с системой SaleBot через rest api
//описание api https://docs.salebot.pro/api-v-konstruktore-salebot.pro/
class CSaleBot {
    public $bearer_token = "3456436uofgdkjherto98453kjert";
	protected $token = "389f65989931bdddb5d7b878f06e861a";
	protected $syncLogFile;
	protected $baseUrl;

	public function __construct(){
		$this->syncLogFile = $_SERVER["DOCUMENT_ROOT"]."/log/saleBotErrors.txt";
        $this->baseUrl = "https://chatter.salebot.pro/api/".$this->token."/";
	}

  private function ConnectSaleBot($dataUrl, $queryType = "GET", $arExtQuery = array()) {
		$arResult = array();
  		//$client = new \GuzzleHttp\Client(['headers' => ['Authorization' => "Bearer ".$this->token]]);
  		$client = new \GuzzleHttp\Client();

		try {
		$arQuery = array('http_errors' => true, 'form_params' => $arExtQuery);
/*		if(count($arExtQuery)>0){
			$arQuery = array_merge($arQuery, array("body"=>$arExtQuery));
		}*/
		$res = $client->request($queryType, $dataUrl, $arQuery);
		$arResult = $res->getBody()->getContents();
		$arResult = json_decode($arResult, true);


		} catch (RequestException $e) {

			$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectSaleBot, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getRequest());
			file_put_contents($this->syncLogFile, $log . PHP_EOL, FILE_APPEND);

			if ($e->hasResponse()) {
				$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectSaleBot, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getResponse());
				file_put_contents($this->syncLogFile, $log . PHP_EOL, FILE_APPEND);

			}
		}
	 return $arResult;
  }

  private function checkStrLenMessage($currentBlockCnt, $message='', $limit=1300){
	 if(strlen($message)>=$limit){
		$currentBlockCnt++;
	 }
	  return $currentBlockCnt;
  }

  public function checkSubscribe($uid=''){
	  $userParams = $this->getUserEventParams($uid);
	  $arResult = array("SUBSCRIBED"=>!empty($userParams["UF_SMART_SENDER_UID"]), "ENABLED"=>intval($userParams["UF_EVENTS_ENABLE"])>0);
	  return json_encode($arResult);
  }

  public function find_client_id_by_phone($phone){
      $res = array();
	  $url = $this->baseUrl."find_client_id_by_phone/find_client_id_by_phone";
      $arExtQuery = array("phone"=>$phone);
      $res = $this->ConnectSaleBot($url, "GET", $arExtQuery);
      return $res;
  }
  
    public function find_client_id_by_email($email){
        $res = array();
        $url = $this->baseUrl."find_client_id_by_phone/find_client_id_by_email";
        $arExtQuery = array("email"=>$email);
        $res = $this->ConnectSaleBot($url, "GET", $arExtQuery);
        return $res;
    }
    
    /**
     * Получение списка клиентов
     * @param int $offset Смещение относительно первого элемента
     * @param int $limit  Количество элементов в ответе/ По умолчанию 500, максимальное значение 500.
     * @return array
     */
    public function get_clients($offset = 0, $limit = 500){
        $res = array();
        $url = $this->baseUrl."get_clients/";
        $arExtQuery = array("offset"=>$offset,
                            limit=>$limit);
        $res = $this->ConnectSaleBot($url, "GET", $arExtQuery);
        return $res;
    }
    
  /**
   * Отправляет сообщение пользователю подписанному и имеющему id smart sender
   *
   * @param  string   $smUid id контакта в smart sender
   * @param  string   $message текст сообщения
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function sendMessage($smUid, $message=''){
     $url = "https://api.smartsender.com/v1/contacts/$smUid/send";
	  $arExtQuery = array("type"=>"text",
								 "content"=>$message,
								 "watermark"=>(new DateTime())->getTimestamp()
	  );
	  $res = $this->ConnectSaleBot($url, "POST", $arExtQuery);
	  return $res;
  }


  /**
   * Позволяет просмотреть подключенные каналы в проекте.
   *
   * @return array массив с ответом сервера
   *
   * @access public
   */
  public function getChannels(){
  	$res = array();
	$url = "https://api.smartsender.com/v1/channels?page=1&limitation=20";
	$res = $this->ConnectSaleBot($url);
	return $res;
  }


  /**
   * Позволяет просматривать существующие чаты.
   *
   * @return array массив с ответом сервера
   *
   * @access public
   */
  public function getChats(){
  	$res = array();
	$url = "https://api.smartsender.com/v1/chats?page=1&limitation=20";
	$res = $this->ConnectSaleBot($url);
	return $res;
  }

  /**
   * Позволяет получать список контактов в проекте.
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function getContacts(){
  	$res = array();
	$url = "https://api.smartsender.com/v1/contacts?page=1&limitation=20";
	$res = $this->ConnectSaleBot($url);
	return $res;
  }


  public function getContactInfo($uid){
  	$res = array();
	$url = "https://api.smartsender.com/v1/contacts/$uid/info";
	$res = $this->ConnectSaleBot($url);
	return $res;
  }


  /**
   * Позволяет просмотреть подключенный канал в проекте по идентификатору.
   *
   * @param  string   $cid id канала
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function getChannelById($cid){
  	$url = "https://api.smartsender.com/v1/channels/$cid";
	$res = $this->ConnectSaleBot($url);
	return $res;
  }





  /**
   * Получает свойства пользователя битрикс для рассылки и подписки
   *
   * @param  int/str   $uid id пользователя битрикс
   *
   * @return array  массив свойств пользователя
   *
   * @access public
   */
  public function getUserEventParams($uid=''){
  	global $USER;
	if(intval($uid)<=0){
		$uid = $USER->GetID();
	}
	$data = CUser::GetList(($by="ID"), ($order="ASC"),
	            array(
	                //'!UF_EVENTS_CHAT_ID' => false,
						 'ID' => intval($uid)
	            ),
				  array("SELECT"=>array("UF_*"))
	        );
	$arEventParams = array();
	while($arUser = $data->Fetch()) {
	  $arEventParams = array(
		"UF_EVENTS_ENABLE" => $arUser["UF_EVENTS_ENABLE"],
		"UF_SMART_SENDER_UID" => $arUser["UF_SMART_SENDER_UID"],
		"UF_EVENTS_PORTFOLIO" => explode(",",$arUser["UF_EVENTS_PORTFOLIO"]),
		"UF_EVENTS_TYPES" => explode(",",$arUser["UF_EVENTS_TYPES"]),
		"UF_EVENTS_USE_FAVORITES" => $arUser["UF_EVENTS_USE_FAVORITES"],
	  );
	}
/*	 if(!count($arEventParams)){
	  $arEventParams = array( "UF_EVENTS_ENABLE" => 0 );
	}*/
	return $arEventParams;
  }


  /**
   * Позволяет просмотреть подписки на воронки выбранного контакта.
   *
   * @param  string   $smUid id контакта в smart sender
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function getFunnels($smUid){
  	$res = array();
   $url = "https://api.smartsender.com/v1/contacts/$smUid/funnels?page=1&limitation=20";
	$res = $this->ConnectSaleBot($url);
	return $res;
  }


  //Рассылает сообщения согласно настройкам пользователей
  public function sendEventMessagesSmarSender($userId=''){
  	global $USER;

	//Получить список пользователей со включенной рассылкой
	$arUsersFilter = array(
	                'UF_EVENTS_ENABLE' => 1,
	                '!UF_SMART_SENDER_UID' => false
	            );
	if(intval($userId)>0){
	  $arUsersFilter["ID"] = intval($userId);
	}
	$data = CUser::GetList(($by="ID"), ($order="ASC"),
	           $arUsersFilter,
				  array("SELECT"=>array("UF_*"))
	        );
	$arSendUsers = array();
	while($arUser = $data->Fetch()) {
	    $arSendUsers[$arUser['ID']] = array("NAME"=>$arUser['NAME'], "EMAIL"=>$arUser['EMAIL'], "UF_SMART_SENDER_UID"=>$arUser['UF_SMART_SENDER_UID']);
	}

	//Получаем события за день вперед

	$dt = new DateTime();
	//$_REQUEST["date"] = (new DateTime())->modify('+1 days')->format('d.m.Y');
	$_REQUEST["date"] = (new DateTime())->format('d.m.Y');

	if ($dt->format("N") == 5) {  //Если пятница - то получаем события за выходные и понедельник
		//$_REQUEST["dateto"] = $dt->modify("+3 days")->format('d.m.Y');
		$_REQUEST["dateto"] = $dt->format('d.m.Y');
	} else {
		$_REQUEST["dateto"] = $_REQUEST["date"];
	}


	$symbLimit = 1300;// Кол-во символов в одном сообщении

	//Отправка сообщений
	$sendedCnt = 0;
	$sendResult = array();
   foreach($arSendUsers as $uid => $arUser){
   	//Получаем события и портфели пользователя для фильтра
		$arUserEventParams = $this->getUserEventParams($uid);
		$_REQUEST["eventTypes"] = implode(",", $arUserEventParams["UF_EVENTS_TYPES"]);
		if(!empty($arUserEventParams["UF_EVENTS_PORTFOLIO"]))
		 $_REQUEST["eventPortfolio"] = $arUserEventParams["UF_EVENTS_PORTFOLIO"];
		if($arUserEventParams["UF_EVENTS_USE_FAVORITES"]==1){
			$_REQUEST["eventFavorites"] = 'Y';
			}


	$arEvents = array(); //События календаря
	$CREvents = new CRadarEvents();

		$CREvents->setFilter($uid);

		$arEvents = $CREvents->arEventsFiltered;

		$messageText = array();
		$messageBlockCnt = 0;
		if(count($arEvents)){
			$siteUrl = 'https://fin-plan.org';
			$cntEventsForUser = count($arEvents);
			if(count($arEvents)==1 && count(reset($arEvents))==0){
			  $cntEventsForUser = 0;
			}
			foreach($arEvents as $date=>$arEventItems){
			 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
			 $messageText[$messageBlockCnt] .= 'События на '.$date.PHP_EOL.'-------'.PHP_EOL;
			 $messageText[$messageBlockCnt] .= 'Настроить отслеживание событий Вы можете в личном кабинете https://fin-plan.org/personal/#events_subscribe?uid='.$uid.PHP_EOL.PHP_EOL;
			 $eventTypeText = '';
			  foreach($arEventItems as $eventCode=>$eventList){
			  	if(count($eventList)==0) continue;
				 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
				 $messageText[$messageBlockCnt] .= $CREvents->arEventsTypes[$eventCode].':'.PHP_EOL;
				 $ecnt = 0;
					foreach($eventList as $detailEvent){
						if(count($eventList)==1){
						  $ecntTxt = '';
						} else {
						  $ecnt++;
						  $ecntTxt = $ecnt.') ';
						}

					  switch ($eventCode) {
						    case "DIVIDENDS":
						    case "DIVIDENDS_USA":
							    $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
						       $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' Тек.цена акции: '.$detailEvent["Цена на дату закрытия"].' Дивиденды: '.$detailEvent["Дивиденды"].PHP_EOL;
						       break;
							 case "COUPONS":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' '.($detailEvent["DEFAULT"]?'(дефолт)':'').($detailEvent["STRUCTURED"]?'(структурная)':'').' Купон: '.$detailEvent["-"].(is_numeric($detailEvent["-"])?' руб.':'').' Купон, в %: '.$detailEvent["Ставка купона"].(is_numeric($detailEvent["-"])?' %':'').PHP_EOL;
								 break;
							 case "CANCELLATION":
							 case "OFERTA":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' '.($detailEvent["DEFAULT"]?'(дефолт)':'').($detailEvent["STRUCTURED"]?'(структурная)':'').PHP_EOL;
							    break;
							 case "REPORT_USA":
							 case "REPORT_RUS":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
							    $arTitle = explode(" - ",$detailEvent["UF_TITLE"]);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["UF_EMITENT"].' - '.$detailEvent["UF_LINK"].' '.(count($arTitle)>1?$arTitle[1]:$detailEvent["UF_TITLE"]).PHP_EOL;
							    break;
							 case "GOSA":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .=  $ecntTxt.$detailEvent["UF_TITLE"].(!empty($detailEvent["UF_LINK"])?' '.$detailEvent["UF_LINK"]:'').PHP_EOL;
							 	 break;
							 default:
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["UF_TITLE"].( !empty($detailEvent["UF_DESCRIPTION"])?' '.$detailEvent["UF_DESCRIPTION"].' ':'').( !empty($detailEvent["UF_LINK"])?' '.$detailEvent["UF_LINK"]:'').PHP_EOL;
							 	 break;
						}

					}
					$messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
					$messageText[$messageBlockCnt] .= PHP_EOL;

			  }
			  //$messageText .= $eventTypeText;
			}

		}


   	if(!empty($arUserEventParams["UF_SMART_SENDER_UID"])){
   		if($cntEventsForUser>0){
   			CLogger::EventsSenderTest(print_r($arEvents, true));
   		  for($m=0; $m<count($messageText); $m++){
		  	  $sendResult[] = $this->sendMessage($arUserEventParams["UF_SMART_SENDER_UID"], $messageText[$m]);
			  $sendedCnt++;
			  }
			  }
   	}

		unset($CREvents, $arEvents, $messageText);
   }



	 return json_encode(array("result"=>"success", "sendResult"=> explode(", ",$sendResult), "message"=>"Отправлено сообщений: ".$sendedCnt));
  }

  }
	?>