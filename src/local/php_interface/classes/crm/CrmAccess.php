<?
class CrmAccess {
	private $type;

	public function __construct($checkAccess=true)
	{
		global $APPLICATION;
		global $USER;

		if(in_array(1, $USER->GetUserGroupArray()) || in_array(17, $USER->GetUserGroupArray())){
			$this->type = "admin";
		} elseif(in_array(7, $USER->GetUserGroupArray()) && !in_array(1, $USER->GetUserGroupArray())) {
			$this->type = "events";
		}
		if($this->type != "admin" && $this->type != "events"){
			$this->type = "manager";
		}



		//вид по умолчанию
		if(!$_SESSION["table_view"]){
			$_SESSION["table_view"] = $this->type;
		}
		
		if($_GET["change_table_view"]){
			if($this->type != "admin" && $_GET["change_table_view"]=="admin"){
				die("!!!");
			}
			
			$_SESSION["table_view"] = $_GET["change_table_view"];
			LocalRedirect($APPLICATION->GetCurPageParam("", array("change_table_view")));
		}
	}
	
	public function getCurTableView(){
		return $_SESSION["table_view"];
	}

	public function checkGlobalAccess(){
		global $APPLICATION;
		
		if(!$this->type){
			$APPLICATION->RestartBuffer();
			die("У вас нет доступа");
		}
	}

	public function getType(){
		return $this->type;
	}
	
	public function getTableViewTypes(){
		$tmp = [
			"admin" => [
				"name" => "Админ панель",
			],
			"events" => [
				"name" => "Заявки",
			],
			"invite" => [
				"name" => "Приглашения",
			],
		];
		
		if($this->type!="admin"){
			unset($tmp["admin"]);
		}
		
		return $tmp;
	}
}