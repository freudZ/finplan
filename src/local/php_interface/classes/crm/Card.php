<?

/*
 * Соц сети, множ строка - из файла
 * Манагер - доп поле, через црм
 * Вебинары - из файла, множ строка, даты через 15.04.2016//07.07.2018//
 * Продукты - добавляем поле сокращенное навание, код цвета
 * Продажи из файла - текстовое поле, множественное, более 250 символов
 * След звонок - из файла, просто строка, из файла
 * Приглашения на вебинар - textarea, из файла
 * Следующий звонок с приглашением:  --||-- выше
 *
 * Объединение емейлов, доп поле либо множественное,
 */
class Card extends CrmDataBase {
    private $item;
    private $id;
    private $colors;

    public function __construct($id)
	{
		if($id!="system"){
			\Bitrix\Main\Loader::includeModule("iblock");
			
			if(!$id){
				throw new Exception("Не передан ID");
			} else {
				$this->id = $id;
			}

			$this->initCard();
		}
	}

	private function initCard(){
        $el = CIBlockElement::GetByID($this->id)->GetNextElement();
        $this->item = $el->GetFields();
        $this->item["PROPS"] = $el->GetProperties();
		
		if($this->item["IBLOCK_ID"] != static::$ibID){
			throw new Exception("Не верный элемент");
		}
		
		if($this->item["PROPS"]["USER"]["VALUE"]){
		  //	$saleData = $this->getUsersProducts(array($this->item["PROPS"]["USER"]["VALUE"]));
			
		 //	$this->item["PRODUCTS"] = $saleData[$this->item["PROPS"]["USER"]["VALUE"]];
		}
    }

	public function showCard(){
	    $item = $this->item;
		?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3">
                    Пользователь сайта:
                    <?if($item["PROPS"]["USER"]["VALUE"]):?>
                    <?
                    global $USER;
                    $rsUser = CUser::GetByID($item["PROPS"]["USER"]["VALUE"]);
                    $arUser = $rsUser->Fetch();
                    echo '<br><a href="https://fin-plan.org/bitrix/admin/user_edit.php?lang=ru&ID='.$item["PROPS"]["USER"]["VALUE"].'" target="_blank">'.$arUser["NAME"].',<br>'.(!empty($arUser["EMAIL"])?'email: '.$arUser["EMAIL"]:'login: '.$arUser["LOGIN"]).'</a>';
                    ?>
                    <?else:?>
                        Не задан
                    <?endif;?>
				</div>
				<div class="col-sm-3">
				<?if($item["PROPS"]["USER"]["VALUE"]):?>
				 <a href="javascript:void(0);" class="crmUpdaterButton button " style=" background: rgba(51, 153, 0, 1);" data-user-id="<?= $item["PROPS"]["USER"]["VALUE"] ?>" ><i class="fa fa-update"></i> Обновить запись</a>
                    
     
				 <?endif;?>
				</div>
				<div class="col-sm-6">
					IP:
					<?if($item["PROPS"]["IP"]["VALUE"]):?>
					   <?=implode(", ", $item["PROPS"]["IP"]["VALUE"])?>
					<?else:?>
                        Не задан
					<?endif;?>
				</div>
			</div>
			<br>
			<div class="row user">
				<div class="col-sm-6">
					<div><span>Имя:</span></div>
					<?=implode(", ", $item["PROPS"]["NAME"]["VALUE"])?>
				</div>
                <div class="col-sm-6">
                    <div><span>Соц. сети:</span></div>
                    <?foreach($item["PROPS"]["SOCIALS"]["VALUE"] as $n=>$prop):?>
                        <?=$this->editableField($prop, "input", "PROP_SOCIALS", $item["PROPS"]["SOCIALS"]["PROPERTY_VALUE_ID"][$n])?>
                    <?endforeach?>
                    <?=$this->addableField("input", "PROP_SOCIALS")?><br>
                </div>
            </div>
            <div class="row user">
				<div class="col-sm-6">
					<div><span>E-mail:</span></div>
					<?=$this->editableField($item["PROPS"]["EMAIL"]["VALUE"], "input", "PROP_EMAIL")?>
				</div>
                <div class="col-sm-6">
                    <span>MailChimp:</span>
                    <?=implode(", ", $item["PROPS"]["MAILCHIMP"]["VALUE"])?>
                </div>
            </div>
            <div class="row user">
				<div class="col-sm-6">
					<div><span>Основной E-mail:</span></div>
					<?=$item["PROPS"]["USER_EMAIL"]["VALUE"]?>
				</div>
            <div class="col-sm-6">

            </div>
            </div>
            <div class="row user">
				<div class="col-sm-6">
					<div><span>Телефон:</span></div>
					<?foreach($item["PROPS"]["PHONE"]["VALUE"] as $n=>$prop):?>
						<?=$this->editableField($prop, "input", "PROP_PHONE", $item["PROPS"]["PHONE"]["PROPERTY_VALUE_ID"][$n])?>
					<?endforeach?>
					<?=$this->addableField("input", "PROP_PHONE")?><br>
				</div>
            <div class="col-sm-6">
                    <?$managers = $this->getManagers();
                    $curItem = "";
                    foreach ($managers as $val){
                        if($val["VALUE"]==$item["PROPS"]["MANAGER"]["VALUE"]){
                            $curItem = $val["TEXT"];
                        }
                    }
                    ?>
                    <span>Менеджер:</span> <?=$this->editableField($curItem, "select", "PROP_MANAGER", false, $managers)?>
            </div>
            </div>
            <div class="row user">
				<div class="col-sm-12">
					<span>Часовой пояс:</span>
					<?=implode(", ", $item["PROPS"]["HOURS"]["VALUE"])?>
				</div>
				<div class="col-sm-12">
					<span>Город:</span>
					<?=implode(", ", $item["PROPS"]["CITY"]["VALUE"])?>
				</div>
				<div class="col-sm-12">
					<span>Дата добавления (изменения):</span> <?=$item["PROPS"]["DATE_CREATE"]["VALUE"]?> (<?=$item["PROPS"]["DATE_UPDATE"]["VALUE"]?>)
				</div>
				<div class="col-sm-12">
					<span>Откуда:</span> <?=implode(", ", $item["PROPS"]["ITEM"]["VALUE"])?>
				</div>
				<div class="col-sm-12">
                    <?
                    $utm = [];
                    foreach (["utm_source", "utm_campaign", "utm_medium", "utm_term"] as $code){
                        if(!$item["PROPS"][$code]["VALUE"]){
                            continue;
                        }
                        //$utm[] = $item["PROPS"][$code]["VALUE"];
                        $utm[] = implode(", ", $item["PROPS"][$code]["VALUE"]);
                    }
                    ?>
					<span>UTM:</span>
                    <?if($utm):?>
                        <?=implode(", ", $utm)?>
                    <?else:?>
                        не заданы
                    <?endif;?>
				</div>
                <?
                if($item["PROPS"]["WEBINARS"]["VALUE"] || $item["PROPS"]["WANTS"]["VALUE"]){
                    $this->colors = $this->getAllWebinarColorDates();
                }
                ?>
				<div class="col-sm-12">
					<span>Вебинары:</span> 
					<?
                        if($item["PROPS"]["WEBINARS"]["VALUE"]){
                            $webinars = $this->convertWebinarDates($item["PROPS"]["WEBINARS"]["VALUE"], $this->colors);
                            echo implode(", ", $webinars);
                        }else{
                            echo 'Не заданы';
                        }
                    ?>
				</div>
                <div class="col-sm-12">
                    <span>Желаемые курсы:</span>
                    <?
                        if($item["PROPS"]["WANTS"]["VALUE"]){
                            $wants = $this->convertWebinarDates($item["PROPS"]["WANTS"]["VALUE"], $this->colors);
                            echo implode(", ", $wants);
                        } else {
                            echo 'Нет';
                        }
                    ?>
                </div>
				<div class="col-sm-12">
					<span>Продукты:</span> 
<!--					<?if($item["PRODUCTS"]):?>
						<?=implode(", ", $item["PRODUCTS"])?>
					<?else:?>
						Не заданы
					<?endif?>-->
					<?if($item["PROPS"]["PRODUCTS"]["VALUE"]):?>
						<?
											$arTmp = array();
					foreach($item["PROPS"]["PRODUCTS"]["VALUE"] as $k=>$valItem){
						$valItem = str_replace(" закончился", "", $valItem);
					  $arrValProps = json_decode(urldecode($item["PROPS"]["PRODUCTS_PROPS"]["VALUE"][$k]), true);
					  $date = ' ';
					  if(!empty($arrValProps["DATE_START"])){
						$date .= "(".$arrValProps["DATE_START"];
					  }
					  if(!empty($arrValProps["DATE_END"])){
						$date .= " - ".$arrValProps["DATE_END"].")";
					  } else if(!empty($arrValProps["DATE_START"])){
						$date .= ")";
					  }


					  //$arTmp[] = '<div class="product_item" style="'.implode(";", $arrVal["STYLE"]).'">'.$valItem.$date.'</div>';
					  $valItem = str_replace(" США", $arrValProps["HTML"],$valItem);
					  $arTmp[] = '<div class="product_item" style="'.implode(";", $arrValProps["STYLE"]).'">'.$valItem.$date.'</div>';
					}

				  	$val = implode(",",$arTmp);
					echo $val;
						?>
					<?else:?>
						Не заданы
					<?endif?>

				</div>
				<div class="col-sm-12 info" style="max-height: 250px;overflow-y: scroll;width: 98%">
					<span>Продажи:</span>
					<div>
						<?=$this->addableField("textarea", "PROP_SALE")?>
						<?
                        $item["PROPS"]["SALE"]["~VALUE"] = array_reverse($item["PROPS"]["SALE"]["~VALUE"]);
                        $item["PROPS"]["SALE"]["PROPERTY_VALUE_ID"] = array_reverse($item["PROPS"]["SALE"]["PROPERTY_VALUE_ID"]);
                        ?>
						<?foreach($item["PROPS"]["SALE"]["~VALUE"] as $n=>$prop):?>
						   <? if(strpos($prop["TEXT"], "`")===false) $prop["TEXT"] = (new DateTime())->format('d.m.Y')."`".$prop["TEXT"]; ?>
							<?=$this->editableField($prop["TEXT"], "textarea", "PROP_SALE", $item["PROPS"]["SALE"]["PROPERTY_VALUE_ID"][$n])?>
						<?endforeach?>
					</div>
				</div>
                <div class="col-sm-12 info">
				<div class="col-sm-6">
					<span>Следующий звонок:</span>
					<?=$this->editableField($item["PROPS"]["NEW_CALL"]["VALUE"], "input", "PROP_NEW_CALL")?>
				</div>
                <div class="col-sm-6">
                    <span>Лиды (через ;):</span>
                    <?=$this->editableField($item["PROPS"]["LEADS"]["VALUE"], "textarea", "PROP_LEADS")?>
                </div>
                </div>
				<div class="col-sm-12 info">
					<span>Приглашения на вебинар:</span>
					<div>
						<?=$this->addableField("textarea", "PROP_WEBINARS_COME")?>
						<?foreach($item["PROPS"]["WEBINARS_COME"]["~VALUE"] as $n=>$prop):?>
							<?=$this->editableField($prop["TEXT"], "textarea", "PROP_WEBINARS_COME", $item["PROPS"]["WEBINARS_COME"]["PROPERTY_VALUE_ID"][$n])?>
						<?endforeach?>
					</div>
				</div>
				
				<div class="col-sm-12 info">
					<span>Следующий звонок с приглашением:</span>
					<div>
						<?=$this->editableField($item["PROPS"]["NEXT_CALL_COME"]["VALUE"], "input", "PROP_NEXT_CALL_COME")?>
					</div>
				</div>
				
				<?if($item["PROPS"]["USER"]["VALUE"]):
					$totalBonuses = intval(Bonuses::getTotalBonuses($item["PROPS"]["USER"]["VALUE"]));
					$promoItems = Promocodes::getUserPromocodes($item["PROPS"]["USER"]["VALUE"]);?>
					<div class="col-sm-12">
						<span>Бонусы:</span> <?=$totalBonuses?>
					</div>
					
					<div class="col-sm-12">
						<span>Промокоды:</span> 
						
						<?if($promoItems):?>
							<ul>
								<li>ТЕСТ!!! 1HJ8789J (1 – до 17.06.18)</li>
								<li>676GHJJ (3 - бессрочно)</li>
							</ul>
						<?else:?>
							Нет промокодов
						<?endif?>
					</div>
				<?endif?>
			</div>
		</div>
		<?
	}

    public function getAllWebinarColorDates(){
        CModule::IncludeModule("iblock");
        $arReturn = array();

        $arFilter = Array("IBLOCK_ID"=>41, "!PROPERTY_HEX" => false);
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, ["NAME", "PROPERTY_HEX"]);
        while($item = $res->GetNext()){
            $arReturn[$item["NAME"]] = $item["PROPERTY_HEX_VALUE"];
        }


        return $arReturn;
    }

    public function convertWebinarDates($val, $colors){
        if(!$val) {
            return $val;
        }

        foreach ($val as $n=>$v){
            $baseColor = "";
            $textColor = "fff";
            foreach ($colors as $name=>$hex){
                if(strpos($v, $name)!==false){
                    $baseColor = $hex;
                }
            }

            if(!$baseColor) {
                $tmp = explode(" ", $v);
                $dt = new DateTime($tmp[0]);
                if($dt->getTimestamp()<time()) {
                    $baseColor = "999";
                }
            }
            if(!$baseColor) {
                //$baseColor = "fff";
                $baseColor = "999";
                $textColor = "000";
            }

            $val[$n] = '<div class="product_item" style="background: #'.$baseColor.';color:#'.$textColor.'">'.$v.'</div>';
        }

        return $val;
    }
	
	private function editableField($val, $type="input", $code, $enumId = false, $arSelectValues = []){
	    global $USER;
		$user = CUser::GetByID($USER->GetID())->Fetch();

		if(!$val){
			$val = "Не задано";
		} else {
			if($code=="PROP_PHONE"){
			    if($user["WORK_PHONE"]) {
					$val = "<i class='fa fa-phone'></i> " . $val;
				}
			}elseif($code=="PROP_SOCIALS"){
		        $val = '<a class="link" target="_blank" href="'.$val.'">'.$val.'</a>';
            }elseif($code=="PROP_SALE" || $code=="PROP_WEBINARS_COME"){
				$val = explode("`", $val);
				$itemDate = $val[0];
		        $val = '<span class="date">('.$val[0].')</span>'.$val[1];
            }
        }
		$dopStr = "";
		if($enumId){
			$dopStr .= ' data-enum_id="'.$enumId.'"';
		}
		if($arSelectValues){
			$dopStr .= ' data-values=\''.json_encode($arSelectValues).'\'';
		}
		$linkDopHtml = '<i class="edit fa fa-pencil"></i>';
		if($enumId){
			$linkDopHtml .= '<i class="delete fa fa-times"></i>';
		}
		if(($code=="PROP_SALE" || $code=="PROP_WEBINARS_COME") && $itemDate){
			$dt = new Datetime($itemDate);
			if($dt<(new DateTime(date("d.m.Y")))){
				$linkDopHtml = "";
			}
		}

		return trim('
			<span class="editable" data-field="'.$code.'" data-id="'.$this->item["ID"].'"'.$dopStr.'>
				<span class="val" data-type="'.$type.'">'.$val.'</span>
				<a href="#">
					'.$linkDopHtml.'
				</a>
			</span>
		');
	}
	
	private function addableField($type="input", $code){
		$class = "";
		if($code=="PROP_SALE" || $code=="PROP_WEBINARS_COME"){
			$class="reverse";
		}
		return trim('
			<div class="wraps '.$class.'">
				<span class="addable" data-field="'.$code.'" data-id="'.$this->item["ID"].'">
					<span class="val" data-type="'.$type.'"></span>
					<a href="#">
						<i class="add fa fa-plus"></i>
					</a>
				</span>
			</div>
		');
	}
	
	static function saveField($id, $field, $val, $enumID=false, $newVal = false) {
		/*if(!$val){
			return false;
		}*/
		\Bitrix\Main\Loader::includeModule("iblock");
		
		$el = CIBlockElement::GetByID($id)->GetNextElement();
        $item = $el->GetFields();
		$item["PROPS"] = $el->GetProperties();
		
		if($item["IBLOCK_ID"] != static::$ibID){
			throw new Exception("Не верный элемент");
		}
		
		if(strpos($field, "PROP_")!==false){
			//Если свойтсво
			$field = str_replace("PROP_", "", $field);
			
			$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("CODE"=>$field, "IBLOCK_ID"=>static::$ibID));
			if ($prop = $properties->GetNext())
			{
				$setValue = "";
				if($prop["PROPERTY_TYPE"]=="S" && !$prop["USER_TYPE"]){
					//Строка
					$setValue = array("VALUE" => $val);
					
					if($enumID || $newVal){
						$setValue = [];
						foreach($item["PROPS"][$prop["CODE"]]["PROPERTY_VALUE_ID"] as $n=>$propValId){
							$setValue[$propValId] = array("VALUE" => $item["PROPS"][$prop["CODE"]]["VALUE"][$n]);
						}
						
						if($enumID){
							$setValue[$enumID] = array("VALUE" => $val);
						} elseif($newVal){
							$setValue[0] = array("VALUE" => $val);
						}
					}
				} elseif($prop["PROPERTY_TYPE"]=="S" && $prop["USER_TYPE"]=="HTML"){
					//текст/хтмл
					$setValue = array("TEXT"=>$val, "TYPE"=>"text");
					
					if($enumID || $newVal){
						$setValue = [];
						foreach($item["PROPS"][$prop["CODE"]]["PROPERTY_VALUE_ID"] as $n=>$propValId){
							$setValue[$propValId] = array("VALUE" => $item["PROPS"][$prop["CODE"]]["VALUE"][$n]);
						}
						
						if($enumID){
							$setValue[$enumID] = array("VALUE" => array("TEXT"=>$val, "TYPE"=>"text"));
						} elseif($newVal){
							$setValue[0] = array("VALUE" => array("TEXT"=>$val, "TYPE"=>"text"));
						}
					}
				} elseif($prop["PROPERTY_TYPE"]=="S" && $prop["USER_TYPE"]=="UserID"){
					//пользователь
					$setValue = $val;
				} elseif($prop["PROPERTY_TYPE"]=="S" && $prop["USER_TYPE"]=="DateTime"){
				    //дата/время
					$setValue = $val;
				}

/*				echo "<pre>";
				print_r($prop);
				echo "</pre>";
echo "<pre>";
print_r($setValue);
echo "</pre>";*/
				CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $setValue, $prop["CODE"]);
				
				//Если новове значение, вернем id
				if($newVal){
					$lastPropID = 0;
					$db_props = CIBlockElement::GetProperty($item["IBLOCK_ID"], $item["ID"], array("sort"=>"desc"), Array("CODE"=>$prop["CODE"]));
					while($ar_props = $db_props->Fetch()){
						if($ar_props["PROPERTY_VALUE_ID"]>$lastPropID){
							$lastPropID = $ar_props["PROPERTY_VALUE_ID"];
						}
					}
					
					echo $lastPropID;
				}
			}
		} else {
			
		}
	}
	
	static function deleteField($id, $field, $enumID){
		$field = str_replace("PROP_", "", $field);
		
		\Bitrix\Main\Loader::includeModule("iblock");
		
		$el = CIBlockElement::GetByID($id)->GetNextElement();
        $item = $el->GetFields();
		$item["PROPS"] = $el->GetProperties();
		
		if($item["IBLOCK_ID"] != static::$ibID){
			throw new Exception("Не верный элемент");
		}
		
		$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("CODE"=>$field, "IBLOCK_ID"=>static::$ibID));
		if ($prop = $properties->GetNext())
		{
			$setValue = [];
			foreach($item["PROPS"][$prop["CODE"]]["PROPERTY_VALUE_ID"] as $n=>$propValId){
				if($propValId==$enumID){
					continue;
				}
				$setValue[$propValId] = array("VALUE" => $item["PROPS"][$prop["CODE"]]["VALUE"][$n]);
			}

			CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $setValue, $prop["CODE"]);
		}
	}
}