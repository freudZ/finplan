<?


class DataTable extends CrmDataBase {
	public $colums = array();
	public $pagen = array();
	public $select = array();
	public static $ibId = 19;
	private $access;
	public $firephp;
	public $BitrixUsers = array();
	private $arReplaceProductText = array();

	private $arReturn = array(
		"data" => array(),
		"draw" => 0,
		"recordsTotal" => 0,
		"recordsFiltered" => 0,
	);

	/**
	 * Очищает результат от мусора
	 *
	 * @param  array   $arFields
	 *
	 * @return array
	 *
	 * @access public
	 */
	public function clearResult($arFields) {
		foreach ($arFields as $key => $value) {
			if (0 === mb_strpos($key, '~')) {
				unset($arFields[$key]);
			} else {
				if (true == is_array($value)) {
					$arFields[$key] = $this->clearResult($arFields[$key]);
				}
			}
		}
		return $arFields;
	}

	//типы значений откуда для фильтра
	static function getItemTypes(){
		CModule::IncludeModule("iblock");
		$arReturn = array();

		$arFilter = Array("IBLOCK_ID"=>static::$ibId, "!PROPERTY_ITEM" => false);
		$res = CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_ITEM"), false, false);
		while($item = $res->GetNext()){
			$arReturn[] = mb_str_replace("'", '"', $item["PROPERTY_ITEM_VALUE"]);
		}

		return json_encode($arReturn, JSON_UNESCAPED_UNICODE);
	}

	public function __construct() {
		$_SESSION["datatable_draw"]++;
		$this->arReturn["draw"] = $_SESSION["datatable_draw"];
		$this->access = new CrmAccess();
		$this->arReplaceProductText = array(" закончится в тек. месяце", " закончится в след. месяце", " закончился");
	}

	//Вывод
	public function draw() {

		$this->setPagen();
		 $start = microtime(true);
		CModule::IncludeModule("iblock");

		$arFilter = Array("IBLOCK_ID"=>static::$ibId);
		$res = CIBlockElement::GetList($this->getSort(), $arFilter, false, $this->pagen);
		$rowsCnt = $res->SelectedRowsCount();

		//Всего
		$this->arReturn["recordsTotal"] = $rowsCnt;
		$this->arReturn["recordsFiltered"] = $rowsCnt;

	 	//Фильтрация
		if($data = $this->setFilter()){

		$arFilter = array_merge($arFilter, $data);




			$res = CIBlockElement::GetList($this->getSort(), $arFilter, false, $this->pagen);

			$this->arReturn["recordsFiltered"] = $res->SelectedRowsCount();
		}
		$this->setSessionItems($arFilter);

		while($ob = $res->GetNextElement())
		{
			$item = $ob->GetFields();
			$item["PROPS"] = $ob->GetProperties();
			$arItems[$item["ID"]] = $this->clearResult($item);

		}

		//добавление сторонних данных
		$arItems = $this->convertListItems($arItems);

		foreach($arItems as $item){
			$this->arReturn["data"][] = $this->getItemList($item);
		}

		$finish = microtime(true);
		$delta = $finish - $start;
		//вывод

		echo json_encode($this->arReturn);
	}

	//ид в сессию
	private function setSessionItems($filter){
		$_SESSION["datatable_ids"] = array();
		$res = CIBlockElement::GetList([], $filter, false, false, ["ID"]);
		while($item = $res->GetNext()){
			$_SESSION["datatable_ids"][] = $item["ID"];
		}
	}

	//сторонние данные
	private function convertListItems($arData){
		$arUsers = [];

		foreach($arData as $item){
			if($item["PROPS"]["USER"]["VALUE"]){
				$arUsers[] = $item["PROPS"]["USER"]["VALUE"];
			}
		}

		//Продукты
	 $saleData = $this->getUsersProducts($arUsers);

		foreach($arData as $n=>$item){
			if(!$item["PROPS"]["USER"]["VALUE"]){
				continue;
			}

		 $arData[$n]["PRODUCTS"] = $saleData[$item["PROPS"]["USER"]["VALUE"]];
		}




		return $arData;
	}




	//сортировка
	public function getSort(){
		$arReturn = array();

		//сортировка по типу
		$view = $this->access->getCurTableView();
		if(in_array($view, ["events", "invite"])){
			$prop = "PROPERTY_NEW_CALL";
			if($view=="invite"){
				$prop = "PROPERTY_NEXT_CALL_COME";
			}
			$arReturn[$prop] = "asc";
		}

		$column = $this->getColumn();
		foreach($_REQUEST["order"] as $n=>$item){
            $colName = $column[$item["column"]]["FIELD"];
            if(strpos($colName, "PROPS_")!==false){
                $colName = mb_str_replace("PROPS_", "PROPERTY_", $colName);
            }

            $arReturn[$colName] = $item["dir"];
		}
		return $arReturn;
	}

	//Заполнение элемента
	public function getItemList($item){

      global $USER;

		$managers = $this->getManagers();
		$curDt = new DateTime();
		$dates = $this->getAllWebinarColorDates();

		$tmp = array();
		foreach($this->getColumn() as $n=>$column){
			if(!$column["FIELD"]){
				$tmp[] = "";
				continue;
			}

			$val = $item[$column["FIELD"]];
			if(strpos($column["FIELD"], "PROPS_")!==false){
				$propName = mb_str_replace("PROPS_", "", $column["FIELD"]);
				$val = $item["PROPS"][$propName]["VALUE"];

				//Разбиваем на строки значения типа массивов для определенных полей
				if($propName=="IP"){
				if(is_array($item["PROPS"][$propName]["VALUE"])){
					$val = implode("<br>",$item["PROPS"][$propName]["VALUE"]);
				}
				}
				if($propName=="PRODUCTS"){
				if(is_array($item["PROPS"][$propName]["VALUE"])){
					$arTmp = array();
					foreach($item["PROPS"][$propName]["VALUE"] as $k=>$valItem){
					  $valItem = mb_str_replace($this->arReplaceProductText, "", $valItem);
					  $arrValProps = json_decode(urldecode($item["PROPS"][$propName."_PROPS"]["VALUE"][$k]), true);
					  $date = ' ';
					  if(!empty($arrValProps["DATE_START"])){
						$date .= "(".$arrValProps["DATE_START"];
					  }
					  if(!empty($arrValProps["DATE_END"])){
						$date .= " - ".$arrValProps["DATE_END"].")";
					  } else if(!empty($arrValProps["DATE_START"])){
						$date .= ")";
					  }


					  //$arTmp[] = '<div class="product_item" style="'.implode(";", $arrVal["STYLE"]).'">'.$valItem.$date.'</div>';
					  //$valItem = str_replace(" США", $arrValProps["HTML"],$valItem);
					  $valItem = mb_str_replace(" США", $arrValProps["HTML"],$valItem);
					  $arTmp[] = '<div class="product_item" style="'.implode(";", $arrValProps["STYLE"]).'">'.$valItem.$date.(!empty($arrValProps["PAY_DATA"])?' '.$arrValProps["PAY_DATA"]:'').'</div>';


					}

				  	$val = implode(",",$arTmp);



					//$val = "<pre>".print_r($item["PROPS"][$propName], true)."</pre>";
				}
				}

 				if($propName == "LEELOO_ID"){
 					array_unique($val);
					if(!empty($val) && is_array($val)){
						  $valTmp = '';
						foreach($val as $k=>$id){
						  $valTmp .= (!empty($valTmp)?'<br>':'').'<a href="https://app.leeloo.ai/crm/1?personCardId='.$id.'" target="_blank">'.($k+1).' Акк. LeeLoo</a>';
						}
						 $val = $valTmp;
					}
				}
 				if($propName == "SMART_SENDER_ID"){
					if(!empty($val)){
						  $valTmp = '';
						  $valTmp .= (!empty($valTmp)?'<br>':'').'<a href="https://console.smartsender.com/contacts/'.$val.'?project=fin-planorg-66030" target="_blank">'.$val.'</a>';
						 $val = $valTmp;
					}
				}
                if($propName == "SALEBOT_ID"){
                    if(!empty($val)){
                        $valTmp = '';
                        $valTmp .= (!empty($valTmp)?'<br>':'').'<a href="https://salebot.pro/projects/59670/clients/'.$val.'" target="_blank">'.$val.'</a>';
                        $val = $valTmp;
                    }
                }
			}



			if($column["FIELD"]=="PROPS_MANAGER" && $val){
				foreach ($managers as $val=>$item2){
					if($item2["VALUE"]==$val){
						$val = $item2["TEXT"];
						break;
					}
				}
			}


		if(($column["FIELD"]=="PROPS_PROMOCODE" || $column["FIELD"]=="PROPS_PROMOCODE_STATUS" || $column["FIELD"]=="PROPS_REFERAL") && $val){

		}


			if($column["FIELD"]=="PROPS_WEBINARS" || $column["FIELD"]=="PROPS_WANTS"){


				$val  = $this->convertWebinarDates($val, $dates, $item["ID"]);
			}

			if(in_array($column["FIELD"], ["PROPS_SALE", "PROPS_WEBINARS_COME"]) && $val){
				$val = mb_str_replace("`", " - ", $val[count($val)-1]["TEXT"]);
			}

			//красным даты просроченные
			if($column["FIELD"]=="PROPS_NEW_CALL"){
				$dt = new DateTime($val);
				if($dt<$curDt){
					$val = '<span style="color:red">'.$val."</spa>";
				}
			}

			if(!$val){
				$val = "";
			}

			$tmp[] = $val;
		}


		return $tmp;
	}

	public function convertWebinarDates($val, $colors, $rowId){
	    if(!$val) {
            return $val;
		}
 global $USER;
       $rsUser = CUser::GetByID($USER->GetID());
       $arUser = $rsUser->Fetch();


        foreach ($val as $n=>$v){
	        $baseColor = "";
			$textColor = "fff";
	        foreach ($colors as $name=>$hex){
	            if(mb_strpos($v, $name)!==false){
	                $baseColor = $hex;
	            }
	        }

			if(!$baseColor) {
	         $tmp = explode(" ", $v);
				if(count($tmp)>1) {
				$dt = new DateTime($tmp[0]);

				if($dt->getTimestamp()<time()) {
                    $baseColor = "999";
				}
				}
			}
			if(!$baseColor) {
				//$baseColor = "fff";
				$baseColor = "999";
				$textColor = "000";
			}

			$val[$n] = '<div class="product_item '.($n<3?'prod_always_vis':'').' product_item_'.$rowId.'" style="background: #'.$baseColor.';color:#'.$textColor.'; '.($n>2?' display:none;':'').'">'.$v.'</div>';
		 }
			if(count($val)>3){
			  $val[] = '<br><a href="javascript:void(0)" class="show_more_products" data-row-id="'.$rowId.'" data-alt-text="Свернуть">Показать все</a>';
			}
        return $val;
	}

	public function getAllWebinarColorDates(){
		CModule::IncludeModule("iblock");
		$arReturn = array();

		$arFilter = Array("IBLOCK_ID"=>41, "!PROPERTY_HEX" => false);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, ["NAME", "PROPERTY_HEX"]);
		while($item = $res->GetNext()){
			$arReturn[$item["NAME"]] = $item["PROPERTY_HEX_VALUE"];
		}


		return $arReturn;
    }

	//Фильтрация
	public function setFilter(){
		$arData = array();
        parse_str($_REQUEST["dopExclude"], $dopExclude);



		foreach($this->getColumn() as $n=>$column){
			if($v = $_REQUEST["columns"][$n]["search"]["value"]){
				$v = mb_str_replace(['\.', '\(', '\)', '\/',], [".", "(", ')',"/"], $v);
				//datatable калечит даты - добавляет / - режем

				if($column["SEARCH"]){
					if($column["SEARCH"]=="text"){
						if(mb_strpos($v, "Радар")!==false){
							$v = "%".$v;
						}  else {
						  $v = "%".$v."%";
						}
					}
				} else {
					$v = explode("|", $v);
				}

				 if($column["SEARCH"]=="int"){
						 //	$v = $v;
				   $arRuleSigns = array("<",">","<=",">=","!=","!");
					//для статуса промокода
					if($column["FIELD"]=="PROPS_PROMOCODE_STATUS"){
						if ($v == "Все пустые") {
							$arData["PROPERTY_PROMOCODE_STATUS"] = false;
						} else if( $v == "Все заполненные" ){
							$arData["!PROPERTY_PROMOCODE_STATUS"] = false;
						} else{
						  $arData["PROPERTY_PROMOCODE_STATUS_VALUE"] = $v;
						}
					} else {
						$rule = "";
						if(mb_strpos($v, $arRuleSigns)!==false){
						  if(mb_strpos($v, "<")!==false){ $rule = "<";}
						  if(mb_strpos($v, ">")!==false){ $rule = ">";}
						  if(mb_strpos($v, ">=")!==false){ $rule = ">=";}
						  if(mb_strpos($v, "<=")!==false){ $rule = "<=";}
						  if(mb_strpos($v, "!=")!==false){ $rule = "!=";}
						  if(mb_strpos($v, "!")!==false){ $rule = "!";}
						  $v = mb_str_replace($arRuleSigns, "", $v);
						}
 //		$firephp = FirePHP::getInstance(true);
 //     $firephp->fb(array("v"=>$v, "rule"=>$rule),FirePHP::LOG);

						  $arData[$rule.mb_str_replace("PROPS", "PROPERTY", $column["FIELD"])] = $v;
						  if(!in_array($rule, array(">",">=")))
						   $arData[">".mb_str_replace("PROPS", "PROPERTY", $column["FIELD"])] = 0;
					}

				} else if($column["SEARCH"]=="int_range"){
					 $arVal = explode("-yadcf_delim-",$v);
					 $arData[">=".mb_str_replace("PROPS", "PROPERTY", $column["FIELD"])] = $arVal[0];
					 if(count($arVal)>1 && !empty($arVal[1])){
					  $arData["<=".mb_str_replace("PROPS", "PROPERTY", $column["FIELD"])] = $arVal[1];
					 }

					}else if($column["SEARCH"]=="ibProducts"){
					   $column["FIELD"] = mb_str_replace("PROPS", "PROPERTY", $column["FIELD"]);
						if ($v == "Все пустые") {
							$arData[$column["FIELD"]] = false;
						} else if( $v == "Все заполненные" ){
							$arData["!".$column["FIELD"]] = false;
						} else if( $v == "Все продукты" ){
							$arData["=PROPERTY_HAVE_ALL_PRODUCTS"] = "Y";
						}else if($v == "ГС закончился") {
							global $DB;
							$arExclude = array('ГС закончится в тек. месяце', 'ГС закончится в след. месяце', 'ГС');
							$arGSOff = array();
							$res = $DB->Query("SELECT `IBLOCK_ELEMENT_ID`, `VALUE` FROM `b_iblock_element_prop_m19` WHERE `IBLOCK_PROPERTY_ID` = 707 AND VALUE LIKE 'ГС%' ORDER BY `IBLOCK_ELEMENT_ID`");
							$currId = 0;
							while($row = $res->fetch()){
							  $arGSOff[$row["IBLOCK_ELEMENT_ID"]][] = $row["VALUE"];
							  $currId = $row["IBLOCK_ELEMENT_ID"];
							}
							foreach($arGSOff as $rowId=>$arVals){
							  	if( in_array($arExclude[0], $arVals) || in_array($arExclude[1], $arVals) || in_array($arExclude[2], $arVals)){
							  		unset($arGSOff[$rowId]);   //Если в массиве есть исключаемые значения - удаляем такой id из массива
							  	}
							}
							if(count($arGSOff)>0){
							  $arData["=ID"] = array_keys($arGSOff);
							}
							unset($arGSOff);
						} else if($v == "ГС США закончился") {
							global $DB;
							$arExclude = array('ГС США закончится в тек. месяце', 'ГС США закончится в след. месяце', 'ГС США');
							$arGSOff = array();
							$res = $DB->Query("SELECT `IBLOCK_ELEMENT_ID`, `VALUE` FROM `b_iblock_element_prop_m19` WHERE `IBLOCK_PROPERTY_ID` = 707 AND VALUE LIKE 'ГС США%' ORDER BY `IBLOCK_ELEMENT_ID`");
							$currId = 0;
							while($row = $res->fetch()){
							  $arGSOff[$row["IBLOCK_ELEMENT_ID"]][] = $row["VALUE"];
							  $currId = $row["IBLOCK_ELEMENT_ID"];
							}
							foreach($arGSOff as $rowId=>$arVals){
							  	if( in_array($arExclude[0], $arVals) || in_array($arExclude[1], $arVals) || in_array($arExclude[2], $arVals)){
							  		unset($arGSOff[$rowId]);   //Если в массиве есть исключаемые значения - удаляем такой id из массива
							  	}
							}
							if(count($arGSOff)>0){
							  $arData["=ID"] = array_keys($arGSOff);
							}
							unset($arGSOff);
						} else{
						   $arData[$column["FIELD"]] = explode("|", $v);
						}



				} else {
					$column["FIELD"] = mb_str_replace("PROPS", "PROPERTY", $column["FIELD"]);

					if($v == "да" || $v[0] == "да" || $v == "%да%" || $v=="Все заполненные" || $v[0]=="Все заполненные") {
						$arData["!" . $column["FIELD"]] = false;
					} elseif($v == "нет" || $v[0] == "нет" || $v=="%нет%" || $v=="Все пустые" || $v[0]=="Все пустые"){

						$arData[$column["FIELD"]] = false;
					} elseif($column["SEARCH"]=="date"){
						$date = explode("-yadcf_delim-", $v);
						if($date[0]){
							$t = new DateTime($date[0]);
							$arData[">=".$column["FIELD"]] = $t->format("Y-m-d")." 00:00:00";
							$arData["!".$column["FIELD"]] = false;
						}
						if($date[1]){
							$t = new DateTime($date[1]);
							$arData["<=".$column["FIELD"]] = $t->format("Y-m-d")." 00:00:00";
							$arData["!".$column["FIELD"]] = false;
						}
						/*
						$t = new DateTime($v);
						$arData[">=".$column["FIELD"]] = $t->format("Y-m-d")." 00:00:00";
						$arData["<=".$column["FIELD"]] = $t->format("Y-m-d")." 23:59:59";
						*/
					} else {
						$arData[$column["FIELD"]] = $v;
					}

				}
			}
		}

		$resetManager = false;
		if($arData["PROPERTY_EMAIL"] && strlen($arData["PROPERTY_EMAIL"])>=8){
			$resetManager = true;
		}
		if($arData["PROPERTY_PHONE"] && strlen($arData["PROPERTY_PHONE"])>=8){
			$resetManager = true;
		}

		//фильтр по типу
		$view = $this->access->getCurTableView();
		if(in_array($view, ["events", "invite"]) && !$resetManager){
			$prop = "PROPERTY_NEW_CALL";
			if($view=="invite"){
				$prop = "PROPERTY_NEXT_CALL_COME";
			}
			if($_REQUEST["type"]=="today"){
				$arData[">=".$prop] = date("Y-m-d")." 00:00:00";
				$arData["<=".$prop] = date("Y-m-d")." 23:59:59";
			} elseif($_REQUEST["type"]=="week"){
				$t = new DateTime();
				$t->modify("+1week");
				//$arData[">=".$prop] = $t->format("Y-m-d")." 00:00:00";
				$arData["<=".$prop] = $t->format("Y-m-d")." 23:59:59";
			} elseif($_REQUEST["type"]=="date"){
				$t = new DateTime($_REQUEST["date"]);
				$arData[">=".$prop] = $t->format("Y-m-d")." 00:00:00";
				$arData["<=".$prop] = $t->format("Y-m-d")." 23:59:59";
			}
		}

		//исключения из фильтра
       /* if($dopExclude["PROPERTY_WEBINARS"]){
			if($arData["PROPERTY_WEBINARS"]){
				$arData[] = [
					"LOGIC" => "AND",
					array("PROPERTY_WEBINARS"=> $arData["PROPERTY_WEBINARS"]),
					array("!PROPERTY_WEBINARS" => "%" . $dopExclude["PROPERTY_WEBINARS"] . "%")
				];
				unset($arData["PROPERTY_WEBINARS"]);
			} else {
				$arData["!PROPERTY_WEBINARS"] = "%" . $dopExclude["PROPERTY_WEBINARS"] . "%";
			}
        }*/
       	if($dopExclude["PROPERTY_WEBINARS"]) {
		   $arFilter = Array("IBLOCK_ID" => static::$ibId, "PROPERTY_WEBINARS" => "%".$dopExclude["PROPERTY_WEBINARS"]."%");
		   $res = CIBlockElement::GetList(array(), $arFilter, false, false, ["ID"]);
		   while ($item = $res->GetNext()) {
			    $arData["!ID"][] = $item["ID"];
		    }
	   	}

        //исключение продукта из фильтра
       /* $haveProductFilter = false;
        foreach($this->getColumn() as $n=>$column) {
            if ($v = $_REQUEST["columns"][$n]["search"]["value"]) {
                if ($column["SEARCH"] == "products") {
                    $haveProductFilter = true;
                    break;
                }
            }
        }*/

        if(/*!$haveProductFilter && */$dopExclude["PRODUCTS"]){
			//$arData["PROPERTY_PRODUCTS"] =

		   $arFilter = Array("IBLOCK_ID" => static::$ibId, "PROPERTY_PRODUCTS" => $dopExclude["PRODUCTS"]);
		   $res = CIBlockElement::GetList(array(), $arFilter, false, false, ["ID"]);
		   while ($item = $res->GetNext()) {
			    $arData["!ID"][] = $item["ID"];
		    }

/*
				if(count($arData["PROPERTY_PRODUCTS"])>0 && in_array($dopExclude["PRODUCTS"], $arData["PROPERTY_PRODUCTS"])){
					$arData["PROPERTY_PRODUCTS"] = array_diff($arData["PROPERTY_PRODUCTS"], $dopExclude["PRODUCTS"]);
				}  else {
				  $arTmpData = $arData["PROPERTY_PRODUCTS"];
				  unset($arData["PROPERTY_PRODUCTS"]);
				  if(is_array($arTmpData)){
					 $arData[] = array("LOGIC"=>"AND", $arTmpData, array("!=PROPERTY_PRODUCTS"=>$dopExclude["PRODUCTS"]));
				  }  else {
					 $arData["!PROPERTY_PRODUCTS"] = $dopExclude["PRODUCTS"];
				  }

				}*/
        }

			if($dopExclude["BONUS_ONLY"]){
 					if($dopExclude["BONUS_ONLY"]!=='ALL'){
						  GLOBAL $DB;
						  $prefixSql = $dopExclude["BONUS_ONLY"]=='Y'?'':'NOT';
						  $query = "SELECT IBP.PROPERTY_77 FROM `b_iblock_element` IBL INNER JOIN b_iblock_element_prop_s11 IBP ON IBP.IBLOCK_ELEMENT_ID = IBL.ID WHERE IBL.IBLOCK_ID = 11 AND IBL.PREVIEW_TEXT $prefixSql LIKE '%Бонус %' GROUP BY IBP.PROPERTY_77";

						  $res = $DB->Query($query);
						  while($row = $res->fetch()){
						  $QueryResult[] = $row["PROPERTY_77"];
						  }
							$prefix = $dopExclude["BONUS_ONLY"]=='Y'?'=':'!';
                    $arData[$prefix."PROPERTY_USER"] = array_unique($QueryResult);

							}//if $dopExclude["BONUS_ONLY"]!=='ALL'
			}

		return $arData;
	}

	//Колонки
	public function setColumn() {


		$view = $this->access->getCurTableView();
		if($view=="events"){
		$tmp = array(
			array(
				"NAME" => "ID",
				"FIELD" => "ID",
			),
			array(
				"NAME" => "Почта",
				"FIELD" => "PROPS_EMAIL",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Имя",
				"FIELD" => "PROPS_NAME",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Телефон",
				"FIELD" => "PROPS_PHONE",
				"SEARCH" => "text",
			),
		   array(
			   "NAME" => "Дата звонка",
				"FIELD" => "PROPS_NEW_CALL",
				"SEARCH" => "date",
				),
			array(
				"NAME" => "Продукты",
				"FIELD" => "PROPS_PRODUCTS",
				"SEARCH" => "ibProducts",
			),
			array(
				"NAME" => "Откуда",
				"FIELD" => "PROPS_ITEM",
			),
			array(
				"NAME" => "Вебинары, даты",
				"FIELD" => "PROPS_WEBINARS",
                "SEARCH" => "text",
			),

            array(
                "NAME" => "Дата первой покупки",
                "FIELD" => "PROPS_DATE_FIRST_BUY",
                "SEARCH" => "date",
            ),
            array(
                "NAME" => "Дата последней покупки",
                "FIELD" => "PROPS_DATE_LAST_BUY",
                "SEARCH" => "date",
            ),
            array(
                "NAME" => "Покупка в этом месяце",
                "FIELD" => "PROPS_BUY_IN_MONTH",
                "SEARCH" => "text",
            ),
			array(
				"NAME" => "Продажи",
				"FIELD" => "PROPS_SALE",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Менеджер",
				"FIELD" => "PROPS_MANAGER",
                "SEARCH" => "text",
			),
		  array(
		      "NAME" => "Часовой пояс",
				"FIELD" => "PROPS_HOURS",
				"SEARCH" => "text",
				),
		  array(
		      "NAME" =>  "Часы, GRN",
				"FIELD" => "PROPS_HOURS_GRN",
				"SEARCH" => "int_range",
				)
		);
		  //	unset($tmp[7]);



         //   unset($tmp[10]);
		} elseif($view=="invite"){
		$tmp = array(
			array(
				"NAME" => "ID",
				"FIELD" => "ID",
			),
			array(
				"NAME" => "Почта",
				"FIELD" => "PROPS_EMAIL",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Имя",
				"FIELD" => "PROPS_NAME",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Телефон",
				"FIELD" => "PROPS_PHONE",
				"SEARCH" => "text",
			),
		   array(
				"NAME" => "Звонок с приглашением на вебинар",
				"FIELD" => "PROPS_NEXT_CALL_COME",
				"SEARCH" => "date",
				),
			array(
				"NAME" => "Продукты",
				"FIELD" => "PROPS_PRODUCTS",
				"SEARCH" => "ibProducts",
			),
			array(
				"NAME" => "Откуда",
				"FIELD" => "PROPS_ITEM",
			),
			array(
				"NAME" => "Вебинары, даты",
				"FIELD" => "PROPS_WEBINARS",
                "SEARCH" => "text",
			),

            array(
                "NAME" => "Дата первой покупки",
                "FIELD" => "PROPS_DATE_FIRST_BUY",
                "SEARCH" => "date",
            ),
            array(
                "NAME" => "Дата последней покупки",
                "FIELD" => "PROPS_DATE_LAST_BUY",
                "SEARCH" => "date",
            ),
            array(
                "NAME" => "Покупка в этом месяце",
                "FIELD" => "PROPS_BUY_IN_MONTH",
                "SEARCH" => "text",
            ),
			array(
				"NAME" => "Продажи",
				"FIELD" => "PROPS_SALE",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Менеджер",
				"FIELD" => "PROPS_MANAGER",
                "SEARCH" => "text",
			),
		  array(
		      "NAME" => "Часовой пояс",
				"FIELD" => "PROPS_HOURS",
				"SEARCH" => "text",
				),
		  array(
		      "NAME" =>  "Часы, GRN",
				"FIELD" => "PROPS_HOURS_GRN",
				"SEARCH" => "int_range",
				)
		);

		} elseif($view=="admin"){
		$tmp = array(
			array(
				"NAME" => "ID",
				"FIELD" => "ID",
			),
			array(
				"NAME" => "Почта",
				"FIELD" => "PROPS_EMAIL",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Почта основная",
				"FIELD" => "PROPS_USER_EMAIL",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Имя",
				"FIELD" => "PROPS_NAME",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Телефон",
				"FIELD" => "PROPS_PHONE",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Откуда",
				"FIELD" => "PROPS_ITEM",
			),
			array(
				"NAME" => "Вебинары, даты",
				"FIELD" => "PROPS_WEBINARS",
                "SEARCH" => "text",
			),
			array(
				"NAME" => "Вебинары, кол-во",
				"FIELD" => "PROPS_WEBINARS_CNT",
                "SEARCH" => "int",
			),
            array(
              "NAME" => "Лиды",
              "FIELD" => "PROPS_LEADS",
              "SEARCH" => "text",
            ),
			array(
				"NAME" => "Продукты",
				"FIELD" => "PROPS_PRODUCTS",
				"SEARCH" => "ibProducts",
			),
            array(
                "NAME" => "Дата первой покупки",
                "FIELD" => "PROPS_DATE_FIRST_BUY",
                "SEARCH" => "date",
            ),
            array(
                "NAME" => "Дата последней покупки",
                "FIELD" => "PROPS_DATE_LAST_BUY",
                "SEARCH" => "date",
            ),
            array(
                "NAME" => "Покупка в этом месяце",
                "FIELD" => "PROPS_BUY_IN_MONTH",
                "SEARCH" => "text",
            ),
			array(
				"NAME" => "Продажи",
				"FIELD" => "PROPS_SALE",
				"SEARCH" => "text",
			),
			array(
				"NAME" => "Менеджер",
				"FIELD" => "PROPS_MANAGER",
                "SEARCH" => "text",
			),

		);
			$tmp[] = array(
				"NAME" => "UTM: Источник",
				"FIELD" => "PROPS_utm_source",
				"SEARCH" => "text",
			);
			$tmp[] = array(
				"NAME" => "UTM: Кампания",
				"FIELD" => "PROPS_utm_campaign",
				"SEARCH" => "text",
			);
			$tmp[] = array(
				"NAME" => "UTM: Medium",
				"FIELD" => "PROPS_utm_medium",
				"SEARCH" => "text",
			);
			$tmp[] = array(
				"NAME" => "Соц сети",
				"FIELD" => "PROPS_SOCIALS",
				"SEARCH" => "text",
			);
			$tmp[] = array(
				"NAME" => "Часовой пояс",
				"FIELD" => "PROPS_HOURS",
				"SEARCH" => "text",
			);
			$tmp[] = array(
		      "NAME" => "Часы, GRN",
				"FIELD" => "PROPS_HOURS_GRN",
				"SEARCH" => "int_range",
				);
			$tmp[] = array(
				"NAME" => "Mailchimp листы",
				"FIELD" => "PROPS_MAILCHIMP",
			);
			$tmp[] = array(
				"NAME" => "Дата создания",
				"FIELD" => "PROPS_DATE_CREATE",
				"SEARCH" => "date",
			);
			$tmp[] = array(
				"NAME" => "Следующий звонок",
				"FIELD" => "PROPS_NEW_CALL",
				"SEARCH" => "date",
			);
			$tmp[] = array(
				"NAME" => "Следующий звонок с приглашением",
				"FIELD" => "PROPS_NEXT_CALL_COME",
				"SEARCH" => "date",
			);
			$tmp[] = array(
                "NAME" => "Прозвоны",
                "FIELD" => "PROPS_WEBINARS_COME",
                "SEARCH" => "text",
			);
			$tmp[] = array(
				"NAME" => "Дата изменения",
				"FIELD" => "PROPS_DATE_UPDATE",
				"SEARCH" => "date",
			);
			$tmp[] = array(
				"NAME" => "Желаемые курсы",
				"FIELD" => "PROPS_WANTS",
				"SEARCH" => "text",
			);
			$tmp[] = array(
				"NAME" => "Партнер",
				"FIELD" => "PROPS_REFERAL",
				"SEARCH" => "text",
			);
			$tmp[] = array(
				"NAME" => "Статус промокода",
				"FIELD" => "PROPS_PROMOCODE_STATUS",
				"SEARCH" => "int",
			);
			$tmp[] = array(
				"NAME" => "Промокод",
				"FIELD" => "PROPS_PROMOCODE",
				"SEARCH" => "text",
			);

		}
		//Для всех добавляем в конец таблицы
		   $tmp[] = array(
				"NAME" => "IP",
				"FIELD" => "PROPS_IP",
				"SEARCH" => "text",
			);
		   $tmp[] = array(
				"NAME" => "LeeLoo",
				"FIELD" => "PROPS_LEELOO_ID",
				"SEARCH" => "text",
			);
		   $tmp[] = array(
				"NAME" => "saleBot",
				"FIELD" => "PROPS_SALEBOT_ID",
				"SEARCH" => "text",
			);
		$this->column = $tmp;
	}

	//Колонки получение
	public function getColumn() {
		if(!$this->column){
			$this->setColumn();
		}
		return $this->column;
	}

	//пагинация
	public function setPagen() {
		$this->pagen = Array(
			"nPageSize"=>10,
			"iNumPage"=>1,
		);
		if($_REQUEST["length"]){
			$this->pagen["nPageSize"] = $_REQUEST["length"];
		}

		if($_REQUEST["start"]){
			$this->pagen["iNumPage"] = $_REQUEST["start"]/$this->pagen["nPageSize"]+1;

			if(!$this->pagen["iNumPage"]){
				$this->pagen["iNumPage"] = 1;
			}
		}
	}
}