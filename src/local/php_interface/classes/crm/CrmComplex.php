<?
class CrmComplex {
	private $ibID = 19;
	private $return = [
		"mes" => ""
	];
	private $r;
	private $mailChimp;
	
	public function __construct(){
		$this->r = \Bitrix\Main\Context::getCurrent()->getRequest();
		
		if(!$this->r->get("action")){
			$this->return["err"] = "Ошибка, нет действия или нет значения";
			echo json_encode($this->return);
			exit();
		}
		
	}
	
	public function doAction(){
		$actionName = "action_".$this->r->get("action");
		$this->$actionName();
		
		
		if(!$this->return["err"]){
			$this->return["suc"] = "Операция выполнена";
		}
		
		echo json_encode($this->return);
	}

	//Установка тега для выборки пользователей в лилу
	private function action_leeloo_setTags(){
		$r = $this->r;
		$tag = $r->get("leeloo_tag");
		if(!$tag){
			$this->return["err"] = "Не выбран тег ";
			return false;
		}

		\Bitrix\Main\Loader::includeModule("iblock");
		//действие
		$CLeeLoo = new CCrmLeeLoo;

		$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_LEELOO_ID");
		$arFilter = Array("IBLOCK_ID"=>$this->ibID, "ID"=>$_SESSION["datatable_ids"], "!PROPERTY_LEELOO_ID"=>false);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    	while($ob = $res->fetch()){
    		foreach($ob["PROPERTY_LEELOO_ID_VALUE"] as $lid){
    	    	$arResult = $CLeeLoo->setTags($lid, $tag);
			   usleep(300000);
			}

		}


	}
	
	private function action_manager(){
		$r = $this->r;

		if(!$r->get("manager")){
			$this->return["err"] = "Не выбран менеджер";
			return false;
		}

		\Bitrix\Main\Loader::includeModule("iblock");
		//действие
		foreach ($_SESSION["datatable_ids"] as $id) {
			CIBlockElement::SetPropertyValues($id, $this->ibID, $r->get("manager"), "MANAGER");
		}
	}
	
	private function action_next_call(){
		$r = $this->r;
		
		/*if(!$r->get("date")){
			$this->return["err"] = "Не выбрана дата";
			return false;
		}*/
		
		//действие
		\Bitrix\Main\Loader::includeModule("iblock");
		//действие
		foreach ($_SESSION["datatable_ids"] as $id) {
			CIBlockElement::SetPropertyValues($id, $this->ibID, $r->get("date"), "NEW_CALL");
		}
	}
	
	private function action_date_next_call(){
		$r = $this->r;
		
		/*if(!$r->get("date")){
			$this->return["err"] = "Не выбрана дата";
			return false;
		}*/
		
		//действие
		\Bitrix\Main\Loader::includeModule("iblock");
		//действие
		foreach ($_SESSION["datatable_ids"] as $id) {
			CIBlockElement::SetPropertyValues($id, $this->ibID, $r->get("date"), "NEXT_CALL_COME");
		}
	}
	
	private function connectMailchimp(){
		if(!$this->mailChimp){
            require_once("/var/www/bitrix/data/www/fin-plan.org/api/mailchimp/MailChimp.php");
			$this->mailChimp = new MailChimp('34faa0b56edca009ad85364d65533d09-us11');
			
		}
		return $this->mailChimp;
	}
	private function getMailChimpLists(){
		$MailChimp = $this->connectMailchimp();
		
		$result = $MailChimp->get("lists");
		
		$arLists = array();
		foreach($result["lists"] as $item){
			$arLists[$item["id"]] = array(
				"id" => $item["id"],
				"name" => str_replace('"', '', $item["name"]),
			);
		}
		
		return $arLists;
	}
	
	private function action_check_db_mailchimp(){
		$MailChimp = $this->connectMailchimp();
		$arLists = $this->getMailChimpLists();

        $params = array(
            "status" => "subscribed",
            "fields" => "members.email_address",
            "count" => 1000,
            "offset" => 0,
        );

        foreach($arLists as $item){
            for($params["offset"] = 0;$params["offset"]<15000;){
                $members = $MailChimp->get("lists/{$item["id"]}/members", $params);

                foreach ($members["members"] as $member) {
                    $arResult[$member["email_address"]][] = $item["name"];
                }

                $params["offset"] += 1000;

            };
        }
		
		if($arResult){
			\Bitrix\Main\Loader::includeModule("iblock");
			$keys = array_keys($arResult);
	
			$arFilter = Array("IBLOCK_ID"=>$this->ibID, "PROPERTY_EMAIL"=>$keys);
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("PROPERTY_EMAIL", "ID"));
			while($item = $res->GetNext()){
				if($val = $arResult[$item["PROPERTY_EMAIL_VALUE"]]){
                    $val = str_replace(['&amp;amp;quot;', '&amp;quot;', '&quot;', '&amp;amp;amp;quot;', '"'], '', $val);
                    CIBlockElement::SetPropertyValues($item["ID"], $this->ibID, $val, "MAILCHIMP");
				}
			}
			
			$arFilter = Array("IBLOCK_ID"=>$this->ibID, "!PROPERTY_EMAIL"=>$keys);
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("PROPERTY_EMAIL", "ID"));
			while($item = $res->GetNext()){
				CIBlockElement::SetPropertyValues($item["ID"], $this->ibID, "", "MAILCHIMP");
			}
		}

	}
	
	private function action_make_segment_mailchimp(){
		$r = $this->r;
		
		if(!$r->get("name")){
			$this->return["err"] = "Не указано имя";
			return false;
		}
		
		//действие
		$arListsItems = array();
		
		//Листы
		$arMailLists = $this->getMailChimpLists();

		\Bitrix\Main\Loader::includeModule("iblock");
/*		$arFilter = Array("IBLOCK_ID"=>$this->ibID, "!PROPERTY_MAILCHIMP"=>false, "ID"=>$_SESSION["datatable_ids"]);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("PROPERTY_EMAIL", "PROPERTY_MAILCHIMP"));
		while($item = $res->GetNext()){

		 $arListsItems[$item["PROPERTY_MAILCHIMP_VALUE"]]["ITEMS"][] = $item["PROPERTY_EMAIL_VALUE"];
		}*/

		$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_MAILCHIMP", "PROPERTY_EMAIL");
		$arFilter = Array("IBLOCK_ID"=>$this->ibID, "!PROPERTY_MAILCHIMP"=>false, "ID"=>$_SESSION["datatable_ids"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){
		 $item = $ob->GetFields();
		 foreach($item["PROPERTY_MAILCHIMP_VALUE"] as $mc_list){
			$arListsItems[$mc_list]["ITEMS"][] = $item["PROPERTY_EMAIL_VALUE"];

		 }
		}


		if(!$arListsItems){
			$this->return["err"] = "Нет данных для создания";
			return false;
		}
		
		foreach($arListsItems as $name=>$item){
			foreach($arMailLists as $id=>$data){
				if($data["name"]==$name){
					$arListsItems[$name]["ID"] = $id;
					break;
				}
			}
		}

		$MailChimp = $this->connectMailchimp();

		foreach($arListsItems as $name=>$data){
			if(!$data["ID"]){
				$this->return["err"] = "Не указан ID листа";
				return false;
			}
			
			$params = array(
				"name" => $r->get("name"),
				"static_segment" => $data["ITEMS"],
			);
			$res = $MailChimp->post("lists/{$data["ID"]}/segments", $params);
		}
	}
}