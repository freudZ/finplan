<?
CModule::IncludeModule("iblock");

class CrmAdder
{
    private $req = array(
      "name",
      "email",
      "phone_final"
    );
    private $landing = "";
    private $ibID;
    private $needAuthAfterRegister = false;
    private $NewUserId = false;
    
    public function checkRequired($fields = array())
    {
        if (!$fields) {
            $fields = $this->req;
        }
        
        foreach ($fields as $r) {
            if (!$_REQUEST[$r]) {
                exit();
            }
        }
        
        return $this;
    }
    
    public function getUserGeo()
    {
        $geo = json_decode(file_get_contents("http://ip-api.com/json/" . $_SERVER["REMOTE_ADDR"]));
        return $geo;
    }
    
    public function getNewUserId()
    {
        // $this->NewUserId = 1;
        return $this->NewUserId;
    }
    
    public function add($landing, $needAuthAfterRegister = false)
    {
        $this->landing = $landing;
        $this->needAuthAfterRegister = $needAuthAfterRegister;
        
        CModule::IncludeModule("iblock");
        
        //if($USER->IsAdmin()){
        $this->addExtended(19);
        //$this->registerUserToBitrix($_REQUEST);
        /*} else {
            $this->addSimple(19);
}*/
    
    }
    
    
    /**
     * Добавляет регистрирующегося на вебинар пользователя в битрикс, вызывается из header.php шаблона study_landing
     *
     * @param array $request
     *
     * @return int    id созданного пользователя
     *
     * @access public
     */
    public function registerUserToBitrix($request, $returnId = false)
    {
        global $USER, $APPLICATION;
        //mail('alphaprogrammer@gmail.com','reg user error1', ' request: '.print_r($request, true));
        if (isset($request["register"]) && htmlspecialchars($request["register"]) == "Y") {
//   $passs = randString(8, array("abcdefghijklnmopqrstuvwxyz","ABCDEFGHIJKLNMOPQRSTUVWX­YZ","0123456789","!@#\$%^&*()",));// Генерирует пароль из 8-ми символов
            $passs = randString(8, array(
              "abcdefghijknmpqrstuvwxyz",
              "ABCDEFGHJKLMNPQRSTUWXYZ",
              "0123456789"
            ));// Генерирует пароль из 8-ми символов
            
            $arFields = array(
              "EMAIL"            => $request["usermail"],
              // E-mail адрес, бывает важно чтобы он у каждого пользователя был уникален - проверьте свои настройки!
              "LOGIN"            => $request["usermail"],
              "NAME"             => $request["username"],
              "PERSONAL_PHONE"   => $request["phone_final"],
              "ACTIVE"           => "Y",
              //Делаем пользователя активным
              "GROUP_ID"         => array(3, 4, 6),
              //Id-групп пользователей сайта bitrix к которым принадлежит пользователь
              "PASSWORD"         => $passs,
              "CONFIRM_PASSWORD" => $passs,
            );
            //	mail('alphaprogrammer@gmail.com','reg user error0', ' request1: '.print_r($request, true));
            
            $user = new CUser;
            $id = $user->Add($arFields);
            if (intval($id) > 0) {
                
                $arEmailFields = array(
                  "EMAIL" => $request["usermail"],
                  "LOGIN" => $request["usermail"],
                  "PAS"   => $passs
                );
                
                CEvent::SendImmediate('NEW_SEM_REG', 's1', $arEmailFields, "Y", 9);
                // 	mail('alphaprogrammer@gmail.com','reg user ok', 'registered');
            } else {
                //   mail('alphaprogrammer@gmail.com','reg user error1', $user->LAST_ERROR.' request: '.print_r($request, true));
            }
            if ($returnId === true && intval($id) > 0) {
                return intval($id);
            }
        }
    }
    
    private function addExtended($ibID)
    {
        $this->ibID = $ibID;
        $mail = trim($_REQUEST["email"]);
        $item = $this->getItemByMail($mail);
        if ($item) {
            //	echo json_encode($this->getNewUserId());
            $this->updateItem($item);
        } else {
            if (mb_strpos($mail, "@") !== false) {
                $this->addSimple($ibID, true);
            }
        }
        
    }
    
    //обновление с учетом новых полей
    private function updateItem($item)
    {
        
        $PROPS = array();
        
        foreach ($item["PROPS"] as $prop) {
            if ($prop["CODE"] == "PROMOCODE_STATUS") {
                $PROPS[$prop["CODE"]] = $prop["VALUE_ENUM_ID"];
            } else {
                $PROPS[$prop["CODE"]] = $prop["VALUE"];
            }
            
        }
        
        //Добавляем поля
        if (!$_REQUEST["tilda"]) {
            $geo = $this->getUserGeo();
            $PROPS["IP"][] = $_SERVER["REMOTE_ADDR"];
            $PROPS["CITY"][] = $geo->city;
            $PROPS["HOURS"][] = $geo->timezone;
            $tList = new CTimeZoneList();
            $tZones = $tList->get_array();
            
            $PROPS["HOURS_GRN"][] = $tZones[$geo->timezone]["hours"];
            unset($tList, $tZones);
        }
        
        
        $PROPS["NAME"][] = $_REQUEST["name"];
        $PROPS["PHONE"][] = $_REQUEST["phone_final"];
        $PROPS["ITEM"][] = $this->landing;
        $PROPS["DATE_UPDATE"] = date("d.m.Y H:i:s");
        
        if (!$PROPS["USER"]) {
            if ($user = $this->findUserByMail($_REQUEST["email"])) {
                $PROPS["USER"] = $user["ID"];
                $PROPS["USER_EMAIL"] = $user["USER_EMAIL"];
                $PROPS["PHONE"][] = $user["PERSONAL_PHONE"];
                $PROPS["NAME"][] = $user["NAME"];
            }
        }
        
        //убираем дубли
        $tmp = array("CITY", "IP", "ITEM", "HOURS", "HOURS_GRN", "NAME", "PHONE");
        if ($_REQUEST["tilda"]) {
            $tmp = array("NAME", "PHONE");
        }
        foreach ($tmp as $code) {
            $PROPS[$code] = array_unique($PROPS[$code]);
        }
        
        CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $PROPS);
        
    }
    
    //Поиск по емейлу
    public function getItemByMail($mail)
    {
        if (!$mail || mb_strpos($mail, "@") === false || empty(trim($mail))) {
            return false;
        }
        
        //Попытка найти пользователя битиркс по email/логину и потом его передать по id в фильтр
        $findedUid = 0;
        $filter = array("LOGIN_EQUAL" => trim($mail));
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
        if ($arUser = $rsUsers->Fetch()) {
            $findedUid = $arUser["ID"];
        }
        
        $arFilter = array("IBLOCK_ID" => $this->ibID);
        if (intval($findedUid) > 0) { //Если пользователь найден/создан то добавляем сложный фильтр "ИЛИ"
            $arFilter["PROPERTY_USER"] = $findedUid;
            $arFilter[] = array(
              "LOGIC" => "OR",
              array("PROPERTY_USER" => $findedUid),
              array("PROPERTY_EMAIL" => $mail),
            );
        } else { //Иначе ищем только по свойству EMAIL
            $arFilter["PROPERTY_EMAIL"] = $mail;
        }
        $res = CIBlockElement::GetList(array(), $arFilter, false, false);
        if ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arFields["PROPS"] = $ob->GetProperties();
            $this->NewUserId = $arFields["PROPS"]["USER"]["VALUE"];
            if ($this->needAuthAfterRegister == true) {
                global $USER;
                $USER->Authorize($this->NewUserId);
                
            }
            return $arFields;
        }
    }
    
    //Поиск по ID юзера
    private function getItemByUserID($id)
    {
        if (!$id) {
            return false;
        }
        
        $arFilter = array("IBLOCK_ID" => 19, "PROPERTY_USER" => $id);
        $res = CIBlockElement::GetList(array(), $arFilter, false, false);
        if ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arFields["PROPS"] = $ob->GetProperties();
            
            return $arFields;
        }
    }
    
    //Есть ли пользователь (публичная обертка)
    public function findUserByEmail($mail)
    {
        return $this->findUserByMail($mail);
    }
    
    //Есть ли пользователь
    private function findUserByMail($mail)
    {
        if (mb_strpos($mail, "@") === false || empty(trim($mail))) {
            return false;
        }
        $filter = array("LOGIN_EQUAL" => trim($mail));
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
        if ($arUser = $rsUsers->Fetch()) {
            return $arUser;
        } else {
            $_REQUEST["register"] = "Y";
            $_REQUEST["usermail"] = $_REQUEST["email"];
            $_REQUEST["username"] = $_REQUEST["name"];
            $userId = $this->registerUserToBitrix($_REQUEST, true);
            $this->NewUserId = $userId;
            if ($this->needAuthAfterRegister == true) {
                global $USER;
                $USER->Authorize($userId);
                
            }
            $filter = array("ID" => $userId);
            $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
            if ($arUser = $rsUsers->Fetch()) {
                return $arUser;
            }
        }
    }
    
    
    /**
     * Добавляет запись в CRM
     *
     * @param int     $ibID    id инфоблока CRM
     * @param boolean $newForm (Optional)
     * @param string  $landing (Optional) Название лендинга с которого пришел пользователь
     * @param int     $userId  (Optional) Для добавления вручную пользователя из админки битрикс нужно передать его id из события onAfterUserAdd
     *
     * @access public
     */
    public function addSimple($ibID, $newForm = false, $landing = false, $userId = false)
    {
        $this->ibID = $ibID;
        if ($item = $this->getItemByMail(trim($_REQUEST["email"]))) {
            $this->updateItem($item);
        } else {
            \Bitrix\Main\Loader::includeModule("iblock");
            $el = new CIBlockElement;
            
            $PROP = array();
            
            if (!$_REQUEST["tilda"]) {
                $geo = $this->getUserGeo();
                
                $PROP["IP"] = $_SERVER["REMOTE_ADDR"];
                $PROP["CITY"] = $geo->city;
                $PROP["HOURS"] = $geo->timezone;
                $tList = new CTimeZoneList();
                $tZones = $tList->get_array();
                $PROP["HOURS_GRN"][] = $tZones[$PROP["HOURS"]]["hours"];
                unset($tList, $tZones);
            }
            
            $PROP["NAME"] = $_REQUEST["name"];
            $PROP["EMAIL"] = $_REQUEST["email"];
            $PROP["PHONE"] = $_REQUEST["phone_final"];
            $PROP["ITEM"] = $this->landing;
            
            if ($landing) {
                $PROP["ITEM"] = $landing;
            }
            
            //Для добавления вручную пользователя из админки битрикс нужно передать его id из события onAfterUserAdd
            if (intval($userId) > 0) {
                global $USER;
                $rsUser = CUser::GetByID($userId);
                $arUser = $rsUser->Fetch();
                $arUserName = array($arUser["NAME"]);
                if (!empty($arUser["LAST_NAME"])) {
                    $arUserName[] = $arUser["LAST_NAME"];
                }
                if (!empty($arUser["SECOND_NAME"])) {
                    $arUserName[] = $arUser["SECOND_NAME"];
                }
                
                $PROP["NAME"] = trim(implode(" ", $arUserName));
                $PROP["EMAIL"] = $arUser["LOGIN"];
                $PROP["USER_EMAIL"] = $arUser["LOGIN"];
                $PROP["PHONE"] = $arUser["PERSONAL_PHONE"] ?: $PROP["PHONE"];
                $PROP["DATE_CREATE"] = $arUser["DATE_REGISTER"];
                $PROP["DATE_UPDATE"] = date("d.m.Y H:i:s");
            }
            
            
            if ($newForm) {
                global $USER;
                if ($USER->IsAuthorized()) {
                    $PROP["USER"] = $USER->GetID();
                    if (!$PROP["NAME"]) {
                        $PROP["NAME"] = $USER->GetFullName();
                    }
                    if (!$PROP["EMAIL"]) {
                        $PROP["EMAIL"] = $USER->GetLogin();
                    }
                } elseif ($user = $this->findUserByMail($_REQUEST["email"])) {
                    $PROP["USER"] = $user["ID"];
                    if (!$PROP["NAME"]) {
                        $PROP["NAME"] = $user["NAME"];
                    }
                    if (!$PROP["EMAIL"]) {
                        $PROP["EMAIL"] = $user["LOGIN"];
                    }
                }
                
                $PROP["DATE_CREATE"] = date("d.m.Y H:i:s");
                if ($PROP["USER"]) {
                    $rsUser = CUser::GetByID($PROP["USER"]);
                    $arUser = $rsUser->Fetch();
                    $PROP["PHONE"] = $arUser["PERSONAL_PHONE"] ?: $PROP["PHONE"];
                    
                    $PROP["DATE_CREATE"] = $arUser["DATE_REGISTER"];
                }
                $PROP["DATE_UPDATE"] = date("d.m.Y H:i:s");
            }
            
            
            if ($_REQUEST["t_form"]) {
                $dt = new DateTime();
                $PROP["NEW_CALL"] = $dt->format("d.m.Y");
                $PROP["SALE"][] = array(
                  "TEXT" => $dt->format("d.m.Y") . '`Заявка с лендинга: "' . $_REQUEST["landing"] . '", форма: "[' . $_REQUEST["t_form"] . ']"',
                  "TYPE" => "text"
                );
            }
            
            $metkas = array(
              "utm_source",
              "utm_campaign",
              "utm_medium",
              "utm_term"
            );
            
            foreach ($metkas as $metka) {
                if ($_REQUEST[$metka]) {
                    $PROP[$metka] = $_REQUEST[$metka];
                }
            }
            
            $arLoadProductArray = array(
              "IBLOCK_ID"       => $ibID,
              "PROPERTY_VALUES" => $PROP,
              "NAME"            => date("d.m.Y H:i:s"),
              "ACTIVE"          => "N",
            );
            $new_elementCRM = $el->Add($arLoadProductArray);
            
            /*		define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/crm_adder_log.txt");
                  AddMessage2Log('$PROP = '.print_r($PROP, true),'');*/
            
            Promocodes::updateUserReferalStatusCRM('', 88,
              $PROP["USER"]); //Регистрируем пользователю промокод, если он переходил по ссылке с utm
            //Promocodes::updateUserReferalStatus('', 12, $PROP["USER"]); //Регистрируем пользователю промокод, если он переходил по ссылке с utm
            
            
        }  //add simple
        
        $this->NewUserId = $PROP["USER"];
    }
    
    //дата со страницы вебинара
    public function addWebinarDate($name, $color = false, $email = '', $webDate = '')
    {
        global $USER;
        
        $userID = $USER->GetID();
        
        if ($email) { //Ищем юзера по почте
            $userArr = $this->findUserByMail($email);
            $userID = $userArr['ID'];
        }
        
        $item = $this->getItemByUserID($userID); //Получаем запись CRM по привязке к пользователю
        if (!$item) { //Если не нашли запись - создаем ее
            $item = $this->addCrmItemFromWebinar($name, $email);
        }
        
        if (!is_array($item["PROPS"]["WEBINARS"]["VALUE"])) {
            $item["PROPS"]["WEBINARS"]["VALUE"] = array($item["PROPS"]["WEBINARS"]["VALUE"]);
        }
        
        $needItem = $webDate ?: date("d.m.Y");
        if ($color) {
            $needItem .= " " . $color;
        }
        
        foreach ($item["PROPS"]["WEBINARS"]["VALUE"] as $val) {
            if ($val == $needItem) {
                return;
            }
        }
        
        if ($item["PROPS"]["WEBINARS"]["VALUE"]) {
            array_unshift($item["PROPS"]["WEBINARS"]["VALUE"], $needItem);
        } else {
            $item["PROPS"]["WEBINARS"]["VALUE"][] = $needItem;
        }
        
        CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $item["PROPS"]["WEBINARS"]["VALUE"],
          "WEBINARS");
    }
    
    //дата со страницы вебинара
    public function addWantsDate($name, $color = false, $email = '', $webDate = '')
    {
        global $USER;
        
        $userID = $USER->GetID();
        
        if ($email) {
            $userArr = $this->findUserByMail($email);
            $userID = $userArr['ID'];
        }
        
        $item = $this->getItemByUserID($userID);
        if (!$item) {
            $item = $this->addCrmItemFromWebinar($name, $email);
        }
        
        if (!is_array($item["PROPS"]["WANTS"]["VALUE"])) {
            $item["PROPS"]["WANTS"]["VALUE"] = array($item["PROPS"]["WANTS"]["VALUE"]);
        }
        
        $needItem = $webDate ?: date("d.m.Y");
        if ($color) {
            $needItem .= " " . $color;
        }
        
        foreach ($item["PROPS"]["WANTS"]["VALUE"] as $val) {
            if ($val == $needItem) {
                return;
            }
        }
        
        if ($item["PROPS"]["WANTS"]["VALUE"]) {
            array_unshift($item["PROPS"]["WANTS"]["VALUE"], $needItem);
        } else {
            $item["PROPS"]["WANTS"]["VALUE"][] = $needItem;
        }
        
        
        CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $item["PROPS"]["WANTS"]["VALUE"], "WANTS");
    }
    
    public function addCrmItemFromWebinar($name, $email = '')
    {
        $this->addSimple(19, true, "Вебинар - " . $name . " - " . date("d.m.Y"));
        global $USER;
        $userID = $USER->GetID();
        
        if (!$userID && $email) {
            $userArr = $this->findUserByMail($email);
            $userID = $userArr['ID'];
        }
        return $this->getItemByUserID($userID);
    }
}