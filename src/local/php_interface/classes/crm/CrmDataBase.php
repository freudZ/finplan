<?
CModule::IncludeModule("iblock");

class CrmDataBase
{
    //иб заявок
    static $ibID = 19;
    protected $userType;
    protected $fixedProducts = array();
    private $arFilterAllProducts = array(); //Массив для сравнения и определения покупки всех продуктов
    
    public function __construct($checkAccess = true)
    {
        \Bitrix\Main\Loader::includeModule("iblock");
        $this->fixedProducts = array(
          [
            "label" => "ГС закончится в тек. месяце",
            "value" => "ГС закончится в тек. месяце",
          ],
          [
            "label" => "ГС закончится в след. месяце",
            "value" => "ГС закончится в след. месяце",
          ],
          [
            "label" => "ГС закончился",
            "value" => "ГС закончился",
          ],
          [
            "label" => "ГС США",
            "value" => "ГС США",
          ],
          [
            "label" => "ГС США закончится в тек. месяце",
            "value" => "ГС США закончится в тек. месяце",
          ],
          [
            "label" => "ГС США закончится в след. месяце",
            "value" => "ГС США закончится в след. месяце",
          ],
          [
            "label" => "ГС США закончился",
            "value" => "ГС США закончился",
          ],
          [
            "label" => "Радар",
            "value" => "Радар"
          ],
          [
            "label" => "Радар США",
            "value" => "Радар США"
          ],
          [
            "label" => "Радар США закончится в тек. месяце",
            "value" => "Радар закончится в тек. месяце США",
          ],
          [
            "label" => "Радар США закончится в след. месяце",
            "value" => "Радар закончится в след. месяце США",
          ],
          [
            "label" => "Радар США закончился",
            "value" => "Радар закончился США"
          ],
          [
            "label" => "Радар закончится в тек. месяце",
            "value" => "Радар закончится в тек. месяце",
          ],
          [
            "label" => "Радар закончится в след. месяце",
            "value" => "Радар закончится в след. месяце",
          ],
          [
            "label" => "Радар закончился",
            "value" => "Радар закончился"
          ],
          [
            "label" => "Все продукты",
            "value" => "Все продукты",
          ],
          [
            "label" => "Все пустые",
            "value" => "Все пустые",
          ],
          [
            "label" => "Все заполненные",
            "value" => "Все заполненные"
          ]
        );
        $this->arFilterAllProducts = array(
          "VIP",
          "ШРИ (арх)",
          "ШРИ (мст)",
          "ШРИ (мгн)",
          "Облиги",
          "Акции",
          "Портф",
          "Quick",
          "ТА1",
          "ТА2",
          "ФА1",
          "ФА2",
          "Радар",
          "Радар США",
          "ГС",
          "ГС США"
        );
        
        $access = new CrmAccess($checkAccess);
        $access->checkGlobalAccess();
    }
    
    //Проверяет пустое значение элемента массива, выхывается из array_filter: array_filter($arProps["WEBINARS"]["VALUE"], '$this->arr_empty')
    public function arr_empty($var)
    {
        return !empty($var);
    }
    
    //Пересчет кол-ва вебинаров для всего инфоблока (на cron) либо при переданном $rowId для конкретной записи
    public function setWebinarsCount($rowId = false)
    {
        $arSelect = array("ID", "NAME", "IBLOCK_ID");
        $arFilter = array("IBLOCK_ID" => static::$ibID, "!PROPERTY_WEBINARS" => false);
        $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);
        if ($rowId !== false) {
            $arFilter["ID"] = $rowId;
        }
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            //arr_empty declared in local/init.php
            $cnt = count(array_filter($arProps["WEBINARS"]["VALUE"], array($this, 'arr_empty')));
            CIBlockElement::SetPropertyValues(intval($arFields["ID"]), 19, intval($cnt), "WEBINARS_CNT");
        }
        
        //Перестраиваем список пользователей, посетивших вебинары, так же прописано в init.php
        $CBanners = new CBanners();
        $CBanners->setArraysWebinarsUsers(true);
        unset($CBanners);
        
    }
    
    //Пересчет покупок в текущем месяце
    public function setMonthBuying($rowId = false)
    {
        $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_DATE_LAST_BUY");
        $arFilter = array("IBLOCK_ID" => static::$ibID, "!DATE_LAST_BUY" => false);
        $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);
        if ($rowId !== false) {
            $arFilter["ID"] = $rowId;
        }
        while ($ob = $res->fetch()) {
            if (!empty($ob["PROPERTY_DATE_LAST_BUY_VALUE"])) {
                CIBlockElement::SetPropertyValues(intval($ob["ID"]), 19,
                  (new DateTime($ob["PROPERTY_DATE_LAST_BUY_VALUE"]))->format('m.Y'), "BUY_IN_MONTH");
            } else {
                CIBlockElement::SetPropertyValues(intval($ob["ID"]), 19, "", "BUY_IN_MONTH");
            }
            //$cnt = count(array_filter($arProps["WEBINARS"]["VALUE"], array($this, 'arr_empty')));
            
        }
    }
    
    
    //список продуктов пересчета покупок пользователя на крон
    public function getProductsForCronCalculate($ids, $json = false)
    {
        
        
        if (!$ids) {
            return false;
        }
        $arReturn = [];
        
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        
        $cacheId = "CRMgetProductsCron";
        //$cache->clean($cacheId);
        $cacheTtl = 0;
        if ($cache->read($cacheTtl, $cacheId)) {
            $arReturn = $cache->get($cacheId);
        } else {
            $arFilter = array("IBLOCK_ID" => 9, "ID" => $ids, "!NAME" => false);
            if ($ids == "all") {
                unset($arFilter["ID"]);
            }
            
            $res = CIBlockElement::GetList(array(), $arFilter, false, false,
              array("ID", "NAME", "PROPERTY_SHORT_NAME", "PROPERTY_COLOR_CODE"));
            while ($item = $res->GetNext()) {
                if ($item["PROPERTY_SHORT_NAME_VALUE"] == "IB") {
                    //CLogger::calcProducts(print_r($arReturn, true));
                }
                $name = $item["NAME"];
                if ($item["PROPERTY_SHORT_NAME_VALUE"]) {
                    $name = $item["PROPERTY_SHORT_NAME_VALUE"];
                }
                
                $bgColor = "";
                if ($item["PROPERTY_COLOR_CODE_VALUE"]) {
                    $bgColor = "background:" . $item["PROPERTY_COLOR_CODE_VALUE"];
                }
                
                if ($ids == "all") {
                    $arReturn[] = array(
                      "NAME"  => $name,
                      "VALUE" => $item["ID"],
                    );
                } else {
                    $arReturn[$item["ID"]] = array("NAME" => $name, "VALUE" => $name, "STYLE" => array(0 => $bgColor));
                }
            }
            
            if ($ids == "all") {
                //$arReturn = array_merge($arReturn, ["Радар", "Радар закончился"]);
                $arReturn = array_merge($arReturn, $this->fixedProducts);
                if ($json) {
                    $arReturn = json_encode($arReturn, JSON_UNESCAPED_UNICODE);
                }
            }
            $cache->set($cacheId, $arReturn);
        }
        
        return $arReturn;
    }
    
    //Пересчет списка покупок пользователя
    public function setUsersProducts($arrFilter = array(), $debug = false)
    {
        $arReturn = array();
        
        
        //$arReturn = $this->setUsersProducts_v1($arrFilter, $debug);//Единый диапазон радара (один общий блок)
        $arReturn = $this->setUsersProducts_v2($arrFilter, $debug);//Радар разделенный на отдельные блоки по покупкам
        
        return $arReturn;
    }
    
    //Перерсчет продуктов с Радаром разделенным на отдельные покупки
    private function setUsersProducts_v2($arrFilter = array(), $debug = false)
    {
        if ($debug) {
            //	CLogger::crm_setProducts_log('$arrFilter = '.print_r($arrFilter, true));
        }
        //TODO Сделать очистку продуктов при отсутствии записей о продажах (в случае когда несколько записей в crm и был единый пользователь проставлен, а потом его убрали в одной из записей и нужно очистить продукты)
        $arProductsID = array();
        $arRadar = array();
        $arRadarUSA = array();
        $arRadarStart = array();
        $arRadarEnd = array();
        $arSales = array();
        $arVip = array();
        $arReturn = array();
        $arUsers = array();
        $arUsersEmails = array();
        $arUsersBuyDates = array();//ID строк 19 инфоблока для которых заполнены даты первой и последней покупок, по ним определяем для какой строки записывать продукты, а для какой стирать.
        $arUserRealBuyes = array();//Даты покупок пользователей
        $greenBorder = "#14fd14";
        $orangeBorder = "#f97500";
        
        
        //Диапазоны для определения скорого окончания радара и ГС
        $currentMonthStart = (new DateTime())->modify('first day of this month')->format("d.m.Y");
        $currentMonthEnd = (new DateTime())->modify('last day of this month')->format("d.m.Y");
        $nextMonthStart = (new DateTime())->modify('first day of next month')->format("d.m.Y");
        $nextMonthEnd = (new DateTime())->modify('last day of next month')->format("d.m.Y");
        $currentDate = (new DateTime())->format("d.m.Y");
        
        
        //Получаем список пользователей по строкам из CRM
        $arSelect = array(
          "ID",
          "NAME",
          "IBLOCK_ID",
          "PROPERTY_USER",
          "PROPERTY_DATE_FIRST_BUY",
          "PROPERTY_DATE_LAST_BUY"
        );
        $arFilter = array("IBLOCK_ID" => 19);
        
        if (count($arrFilter) > 0 && !empty($arrFilter["=PROPERTY_USER"])) {
            $arFilter = array_merge($arFilter, $arrFilter);
        } else {
            if (count($arrFilter) > 0 && empty($arrFilter["=PROPERTY_USER"])) { //Если в массиве не передался пользователь - выходим
                return array();
            } else {
                $arFilter["!PROPERTY_USER"] = false;
            }
        }
        
        $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);
        
        while ($ob = $res->fetch()) {
            if (!in_array($ob["ID"], $arUsers[$ob["PROPERTY_USER_VALUE"]])) {
                $arUsers[$ob["PROPERTY_USER_VALUE"]][] = $ob["ID"];
            }
            //TODO убрать условие, т.к. теперь мы сами считаем и обновляем даты первой и последней покупки в этой функции, а не опираемся на заранее посчитанные
            if (!empty($ob["PROPERTY_DATE_FIRST_BUY_VALUE"]) || !empty($ob["PROPERTY_DATE_LAST_BUY_VALUE"])) {
                $arUsersBuyDates[$ob["PROPERTY_USER_VALUE"]][] = $ob["ID"];
            }
        }
        unset($res);
        
        //Получаем почту пользователей битрикс Для указания основного email в записи CRM
        $filter = array("ID" => implode(" | ", array_keys($arUsers)));
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
        while ($arUser = $rsUsers->Fetch()) {
            if ($debug) {
                //	CLogger::crm_setProducts_log('getlist users = '.print_r($arUser, true));
            }
            if (empty($arUser["EMAIL"])) {  //Если пустой емайл то смотрим в логин на предмет почтового адреса в виде логина
                if (strpos($arUser["LOGIN"], "@") !== false) {
                    $arUser["EMAIL"] = $arUser["LOGIN"];
                }
            }
            $arUsersEmails[$arUser["ID"]] = $arUser["EMAIL"];
        }
        unset($rsUsers);
        
        $arFilter = array("IBLOCK_ID" => 11, "!PROPERTY_PAYED" => false, "PROPERTY_USER" => array_keys($arUsers));
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, false);
        
        while ($ob = $res->GetNextElement()) {
            $item = $ob->GetFields();
            $item["PROPS"] = $ob->GetProperties();
            
            if (!in_array($item["PROPS"]["ITEM"]["VALUE"], $arProductsID) && !empty($item["PROPS"]["ITEM"]["VALUE"])) {
                $arProductsID[] = $item["PROPS"]["ITEM"]["VALUE"];
            }
            foreach ($item["PROPS"]["DOP_ITEMS"]["VALUE"] as $val) {
                if (!in_array($val, $arProductsID) && !empty($val)) {
                    $arProductsID[] = $val;
                }
            }
            $arUserRealBuyes[$item["PROPS"]["USER"]["VALUE"]][] = $item["DATE_CREATE"];//Фиксируем дату покупки
            
            if ($item["PROPS"]["VIP"]["VALUE"] == 'Y') {
                $arVip[$item["PROPS"]["USER"]["VALUE"]][] = $item["PROPS"]["VIP"]["VALUE"];
            }
            
            if ($item["PROPS"]["RADAR_END"]["VALUE"] && $item["PROPS"]["PAYED"]["VALUE"]) {
                $date = explode(" ", $item["NAME"]);
                try {
                    $date = (new DateTime($date[0]))->getTimestamp();
                } catch (Exception $e) {
                    unset($date);
                }
                
                $arUserRealBuyes[$item["PROPS"]["USER"]["VALUE"]][] = $item["DATE_CREATE"];//Фиксируем дату покупки
                $arRadarStart[$item["PROPS"]["USER"]["VALUE"]][] = $item["DATE_CREATE"];
                $arRadar[$item["PROPS"]["USER"]["VALUE"]][] = $item["PROPS"]["RADAR_END"]["VALUE"];
                $arRadarUSA[$item["PROPS"]["USER"]["VALUE"]][] = $item["PROPS"]["ACCESS_USA"]["VALUE"];
                $arDealName[$item["PROPS"]["USER"]["VALUE"]][] = $item["PROPS"]["PAY_NAME"]["VALUE"];
                $arGsUSA[$item["PROPS"]["USER"]["VALUE"]][] = $item["PROPS"]["ACCESS_GS_USA"]["VALUE"];
                //$arRadarUSA[$item["PROPS"]["USER"]["VALUE"]] = $item["PROPS"]["ACCESS_USA"]["VALUE"] == 'Y' && empty($arRadarUSA[$item["PROPS"]["USER"]["VALUE"]]) ? 'Y' : '';
                if ($date && $date > $arRadarEnd[$item["PROPS"]["USER"]["VALUE"]] || !$arRadarEnd[$item["PROPS"]["USER"]["VALUE"]]) {
                    $arRadarEnd[$item["PROPS"]["USER"]["VALUE"]] = $date;
                }
            }
            
            $arSales[] = $item;
        }
        
        //Определение VIP пользователя
        if (count($arVip)) {
            $arStylesVip = array("border: 1px solid #FFFF00; color: #FFFF00; background-color: #6600FF;");
            foreach ($arVip as $userId => $vip) {
                $arReturn[$userId][] = array("NAME" => "VIP", "STYLE" => $arStylesVip, "HTML" => "");
            }
            
            //
        }
        //Определение наличия радара США
        /*			if(in_array("Y",$arRadarUSA[$item["PROPS"]["USER"]["VALUE"]])){
                        $arRadarUSA[$item["PROPS"]["USER"]["VALUE"]] = "Y";
                    } else {
                        $arRadarUSA[$item["PROPS"]["USER"]["VALUE"]] = "";
                    }*/
        
        if ($debug) {
            //	 define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log/crm_setProducts_log.txt");
            //AddMessage2Log($el->LAST_ERROR);return true;
            //     AddMessage2Log('$arrFilter = '.print_r($arrFilter, true),'');
            //     AddMessage2Log('$arRadarUSA = '.print_r($arRadarUSA, true),'');
            //     AddMessage2Log('$arRadar = '.print_r($arRadar, true),'');
        }
        
        unset($res);
        
        if ($debug) {
            //	CLogger::crm_setProducts_log('$arSales = '.print_r($arSales, true));
        }
        
        
        
        if (count($arSales) == 0) {
            return array();
        }
        
        //$productsData = $this->getProductsForCronCalculate(array_unique($arProductsID));
        $productsData = $this->getProductsForCronCalculate($arProductsID);
        
        //собираем данные
        if (count($arSales) > 0) {
            foreach ($arSales as $item) {
                $ended = "";
                $endtext = "";
                $is_GS = strpos($productsData[$item["PROPS"]["ITEM"]["VALUE"]]["NAME"], "ГС") !== false ? true : false;
                $is_GS_USA = (strpos($productsData[$item["PROPS"]["ITEM"]["VALUE"]]["NAME"],
                    "ГС") !== false && $item["PROPS"]["ACCESS_GS_USA"]["VALUE"] == 'Y') ? true : false;
                
                $endStyle = "";
                if ($item["PROPS"]["ITEM_END"]["VALUE"] && $is_GS == true) {
                    $dt = new DateTime($item["PROPS"]["ITEM_END"]["VALUE"]);
                    $ended = $dt->format("d.m.Y");
                    
                    if (strtotime($ended) <= strtotime($currentDate)) {
                        $endStyle = "border: 2px dashed white";
                        $endtext = " закончился";
                    } else {
                        if (strtotime($ended) >= strtotime($currentMonthStart) && strtotime($ended) <= strtotime($currentMonthEnd)) {
                            $endStyle = "border: 2px dashed " . $orangeBorder;
                            $endtext = " закончится в тек. месяце";
                        } else {
                            if (strtotime($ended) >= strtotime($nextMonthStart) && strtotime($ended) <= strtotime($nextMonthEnd)) {
                                $endStyle = "border: 2px dashed " . $greenBorder;
                                $endtext = " закончится в след. месяце";
                            }
                        }
                    }
                    
                }
                
                $date = explode(" ", $item["NAME"]);
                $date = $date[0];
                $dopEnded = "(" . $date . ")";
                if (!empty($ended) && !$is_GS) {
                    $ended = '';
                }
                
                $arrStyle = array();
                if (!empty($endStyle)) {
                    $arrStyle[] = $endStyle;
                }
                
                $arStyles = array_merge($productsData[$item["PROPS"]["ITEM"]["VALUE"]]["STYLE"], $arrStyle);
                if (array_key_exists($item["PROPS"]["ITEM"]["VALUE"], $productsData)) {
                    $GsHtml = '';
                    if ($is_GS_USA) {
                        $GsHtml = " +<img src='/local/templates/crm/img/flags/us.png'/>";
                        $productsData[$item["PROPS"]["ITEM"]["VALUE"]]["NAME"] = $productsData[$item["PROPS"]["ITEM"]["VALUE"]]["NAME"] . ' США';
                    }
                    $arReturn[$item["PROPS"]["USER"]["VALUE"]][] = array(
                      "NAME"       => $productsData[$item["PROPS"]["ITEM"]["VALUE"]]["NAME"] . (!empty($ended) ? $endtext : ''),
                      "DATE_START" => $date,
                      "DATE_END"   => $ended,
                      "STYLE"      => $arStyles,
                      "HTML"       => $GsHtml
                    );
                }
                
                foreach ($item["PROPS"]["DOP_ITEMS"]["VALUE"] as $val) {
                    $arStylesDop = $productsData[$val]["STYLE"];
                    $arReturn[$item["PROPS"]["USER"]["VALUE"]][] = array(
                      "NAME"       => $productsData[$val]["NAME"],
                      "DATE_START" => $date,
                      "DATE_END"   => "",
                      "STYLE"      => $arStylesDop,
                      "HTML"       => ""
                    );//str_replace("</div>", $dopEnded . "</div>", $productsData[$val]);
                }
                
                //School
                $is_School = false;
                $schoolName = "ШРИ";
                $schoolTarif = "";
                $schoolColor = "";
                $saleName = $item["PROPS"]["PAY_NAME"]["VALUE"];
                $saleName = str_replace("&quot;", "", $saleName);
                $saleName = str_replace('"', "", $saleName);
                
                if (mb_strpos($saleName, "Школа разумного инвестирования") !== false) {
                    $is_School = true;
                    if (mb_strpos($saleName, "Архитектор") !== false) {
                        $schoolTarif = " (арх)";
                        $schoolColor = array(
                          "background: rgb(107,9,121)",
                          "background: linear-gradient(90deg, rgba(107,9,121,1) 11%, rgba(0,212,255,1) 99%)"
                        );
                    }
                    if (mb_strpos($saleName, "Мастер") !== false) {
                        $schoolTarif = " (мст)";
                        $schoolColor = array(
                          "background: rgb(107,9,121)",
                          "background: linear-gradient(90deg, rgba(107,9,121,1) 11%, rgba(0,255,128,1) 99%)"
                        );
                    }
                    if (mb_strpos($saleName, "Магнат") !== false) {
                        $schoolTarif = " (мгн)";
                        $schoolColor = array(
                          "background: rgb(107,9,121)",
                          "background: linear-gradient(90deg, rgba(107,9,121,1) 11%, rgba(255,0,0,1) 99%)"
                        );
                    }
                    
                    if (!empty($schoolTarif)) {
                        $schoolName .= $schoolTarif;
                        //$arReturn[$item["PROPS"]["USER"]["VALUE"]][] = "<div class='product_item' style='$schoolColor'>$schoolName</div>";
                        $arReturn[$item["PROPS"]["USER"]["VALUE"]][] = array(
                          "NAME"       => $schoolName,
                          "DATE_START" => "",
                          "DATE_END"   => "",
                          "STYLE"      => $schoolColor,
                          "HTML"       => ""
                        );
                    }
                }
                
            }
        }
        if ($debug && $arrFilter["=PROPERTY_USER"] == 7307) {
            //	AddMessage2Log('$arReturn = '.print_r($arReturn, true),'');
        }
        unset($arSales);
        
        //Радар
        if (count($arRadar) > 0) {
            foreach ($arRadar as $userId => $dates) {
                
                /*			$firephp = FirePHP::getInstance(true);
                         $firephp->fb($arRadarStart[$userId],FirePHP::LOG);
                         $firephp->fb($arRadar[$userId],FirePHP::LOG);
                         $firephp->fb($arRadarUSA[$userId],FirePHP::LOG);*/
                
                foreach ($arRadarStart[$userId] as $rk => $date) {
                    $arTmpRadar = array();
                    $maxItem = "";
                    $minItem = "";
                    $radarUsaHTML = "";
                    $radarBg = "blue";
                    $endtext = "";
                    $arStyles = array();
                    
                    //$minItem = new DateTime(reset($arRadarStart[$userId]));
                    $minItem = new DateTime($date);
                    
                    $maxItem = (new DateTime($arRadar[$userId][$rk]))->format("d.m.Y");
                    
                    $date = FormatDate("d.m.Y", $arRadar[$userId][$rk]);
                    if ($arRadarUSA[$userId][$rk] == "Y" && empty($radarUsaHTML)) {
                        $radarUsaHTML = " +<img src='/local/templates/crm/img/flags/us.png'/>";
                        $radarBg = '#3399FF';
                    }
                    
                    $payname = "<i>?</i><span>" . $arDealName[$userId][$rk] . "</span>";
                    
                    
                    //Добавляем маркеры окончания срока
                    if (strtotime($maxItem) <= strtotime($currentDate)) {
                        $arStyles = array_merge($arStyles, array("border: 2px dashed white"));
                        $endtext = " закончился";
                    } else {
                        if (strtotime($maxItem) >= strtotime($currentMonthStart) && strtotime($maxItem) <= strtotime($currentMonthEnd)) {
                            $arStyles = array_merge($arStyles, array("border: 2px dashed " . $orangeBorder));
                            $endtext = " закончится в тек. месяце";
                        } else {
                            if (strtotime($maxItem) >= strtotime($nextMonthStart) && strtotime($maxItem) <= strtotime($nextMonthEnd)) {
                                $arStyles = array_merge($arStyles, array("border: 2px dashed " . $greenBorder));
                                $endtext = " закончится в след. месяце";
                            }
                        }
                    }
                    
                    
                    $arStyles = array_merge(array("background:" . $radarBg), $arStyles);
                    
                    $arValueRadar = array(
                      "NAME"       => "Радар" . $endtext . (!empty($radarUsaHTML) ? " США" : ""),
                      "DATE_START" => $minItem->format("d.m.Y"),
                      "DATE_END"   => $maxItem,
                      "STYLE"      => $arStyles,
                      "HTML"       => $radarUsaHTML,
                      "PAY_DATA"   => $payname
                    );
                    $arReturn[$userId][] = $arValueRadar;
                }
                
            }
        }
        unset($arRadar);
        
        //Запись в инфоблок
        if (count($arReturn) > 0) {
            foreach ($arReturn as $uid => $data) {
                $arValues = array();
                $arValuesForAll = array(); //Массив для проверки на покупку всех продуктов
                $arValuesDescr = array();
                $value = '';
                $descr = '';
                if (is_array($data)) {
                    foreach ($data as $kd => $vd) {
                        if (empty($vd["NAME"])) {
                            continue;
                        }
                        $value = $vd["NAME"];
                        $descr = urlencode(json_encode($vd));
                        $arValues[] = array("VALUE" => $value, "DESCRIPTION" => "");
                        $arValuesForAll[] = $value;
                        $arValuesDescr[] = array("VALUE" => $descr, "DESCRIPTION" => $kd);
                        
                    }
                }
                
                $userMainEmail = strpos($arUsersEmails[$uid], "@") !== false ? strval($arUsersEmails[$uid]) : '';

                if (array_key_exists($uid, $arUsers) && count($arUsers[$uid]) > 0) {

                    foreach ($arUsers[$uid] as $rowId) {
                        
                        //TODO возможно убрать это условие, т.к. теперь мы сами считаем и обновляем даты первой и последней покупки в этой функции, а не опираемся на заранее посчитанные
                        //  if(in_array($rowId, $arUsersBuyDates[$uid])){
                        
                        $minBuyItem = "";
                        $maxBuyItem = "";
                        //Получаем даты первой и последней покупок пользователей
                        if (count($arUserRealBuyes[$uid])) {
                            usort($arUserRealBuyes[$uid], function ($item1, $item2) {
                                return new DateTime($item1) > new DateTime($item2);
                            });
                            $minBuyItem = new DateTime(reset($arUserRealBuyes[$uid]));
                            
                            usort($arUserRealBuyes[$uid], function ($item1, $item2) {
                                return new DateTime($item1) < new DateTime($item2);
                            });
                            $maxBuyItem = new DateTime(reset($arUserRealBuyes[$uid]));
                        }
                        if (count($arValues) == 0) {
                            $minBuyItem = '';
                            $maxBuyItem = '';
                        }
                        
                        //Определение покупки полного набора продуктов
                        $arIntersectProducts = array_intersect($arValuesForAll, $this->arFilterAllProducts);
                        $AllProductsBuyed = false;
                        if (count($arIntersectProducts) == count($this->arFilterAllProducts)) {
                            $AllProductsBuyed = true;
                        }
                        

                        
                        CIBlockElement::SetPropertyValues(intval($rowId), 19,
                          (empty($minBuyItem) ? "" : $minBuyItem->format("d.m.Y")), "DATE_FIRST_BUY");
                        CIBlockElement::SetPropertyValues(intval($rowId), 19,
                          (empty($maxBuyItem) ? "" : $maxBuyItem->format("d.m.Y")), "DATE_LAST_BUY");
                        CIBlockElement::SetPropertyValues(intval($rowId), 19,
                          (empty($maxBuyItem) ? "" : $maxBuyItem->format("m.Y")), "BUY_IN_MONTH");
                        
                        CIBlockElement::SetPropertyValues(intval($rowId), 19, $userMainEmail, "USER_EMAIL");
                        CIBlockElement::SetPropertyValuesEx(intval($rowId), 19,
                          array("PRODUCTS" => array("VALUE" => array("del" => "Y"))));
                        //CIBlockElement::SetPropertyValuesEx(intval($rowId), 19, array("PRODUCTS_PROPS" => Array ("VALUE" => array("del" => "Y"))));
                        CIBlockElement::SetPropertyValuesEx(intval($rowId), 19, array("PRODUCTS" => count($arValues)?$arValues:false));
                        CIBlockElement::SetPropertyValuesEx(intval($rowId), 19,
                          array("HAVE_ALL_PRODUCTS" => $AllProductsBuyed ? 'Y' : ''));
                        CIBlockElement::SetPropertyValuesEx(intval($rowId), 19,
                          array("PRODUCTS_PROPS" => $arValuesDescr));
                        
                        if ($debug) {
                            //	 CLogger::crm_setProducts_log('$productsArray = '.print_r(array("rowID"=>$rowId, "products"=>$arValues, "descr"=>$arValuesDescr), true));
                        }
                        
                        unset($arNewPropsValues, $arClearProps, $userMainEmail);
                        //	}
                    }
                }
            }
        } else {

        }
        
        //Обновляем дату актуальности
        //	  \Bitrix\Main\Config\Option::set("grain.customsettings","ACTUAL_DATE", (new DateTime(date()))->format("d.m.Y H:i:s") );
        
        return $arReturn;
    }
    
    
    //список покупок пользователя
    //deprecated
    protected function getUsersProducts($users = [], $arFilter = array())
    {
        CModule::IncludeModule("iblock");
        $arProductsID = [];
        $arRadar = [];
        $arRadarUSA = [];
        $arRadarEnd = [];
        $arRadarStart = [];
        $arReturn = [];
        
        if (!count($users)) {
            return $arReturn;
        }
        
        $arFilter = array("IBLOCK_ID" => 11, "!PROPERTY_PAYED" => false, "PROPERTY_USER" => $users);
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, false);
        
        while ($ob = $res->GetNextElement()) {
            $item = $ob->GetFields();
            $item["PROPS"] = $ob->GetProperties();
            
            $arProductsID[] = $item["PROPS"]["ITEM"]["VALUE"];
            foreach ($item["PROPS"]["DOP_ITEMS"]["VALUE"] as $val) {
                $arProductsID[] = $val;
            }
            if (!empty($item["PROPS"]["RADAR_END"]["VALUE"]) && $item["PROPS"]["PAYED"]["VALUE"]) {
                $date = explode(" ", $item["NAME"]);
                try {
                    $date = (new DateTime($date[0]))->getTimestamp();
                } catch (Exception $e) {
                    unset($date);
                }
                
                $arRadarStart[$item["PROPS"]["USER"]["VALUE"]][] = $item["DATE_CREATE"];
                $arRadar[$item["PROPS"]["USER"]["VALUE"]][] = $item["PROPS"]["RADAR_END"]["VALUE"];
                $arRadarUSA[$item["PROPS"]["USER"]["VALUE"]] = $item["PROPS"]["ACCESS_USA"]["VALUE"] == 'Y' && empty($arRadarUSA[$item["PROPS"]["USER"]["VALUE"]]) ? 'Y' : '';
                
                if ($date && ($date > $arRadarEnd[$item["PROPS"]["USER"]["VALUE"]] || !$arRadarEnd[$item["PROPS"]["USER"]["VALUE"]])) {
                    $arRadarEnd[$item["PROPS"]["USER"]["VALUE"]] = $date;
                }
            }
            
            $arSales[] = $item;
        }
        
        $productsData = $this->getProducts(array_unique($arProductsID));
        
        $curDt = new DateTime();
        
        //собираем данные
        foreach ($arSales as $item) {
            $ended = "";
            $is_GS = strpos($productsData[$item["PROPS"]["ITEM"]["VALUE"]], "ГС") !== false ? true : false;
            $is_School = false;
            $schoolName = "ШРИ";
            $schoolTarif = "";
            $schoolColor = "";
            $saleName = $item["PROPS"]["PAY_NAME"]["VALUE"];
            $saleName = str_replace("&quot;", "", $saleName);
            $saleName = str_replace('"', "", $saleName);
            
            if (strpos($saleName, "Школа разумного инвестирования") !== false) {
                
                $is_School = true;
                if (strpos($saleName, "Архитектор") !== false) {
                    $schoolTarif = " (арх)";
                    $schoolColor = "background: rgb(107,9,121); background: linear-gradient(90deg, rgba(107,9,121,1) 11%, rgba(0,212,255,1) 99%);";
                }
                if (strpos($saleName, "Мастер") !== false) {
                    $schoolTarif = " (мст)";
                    $schoolColor = "background: rgb(107,9,121); background: linear-gradient(90deg, rgba(107,9,121,1) 11%, rgba(0,255,128,1) 99%);";
                }
                if (strpos($saleName, "Магнат") !== false) {
                    $schoolTarif = " (мгн)";
                    $schoolColor = "background: rgb(107,9,121); background: linear-gradient(90deg, rgba(107,9,121,1) 11%, rgba(255,0,0,1) 99%);";
                }
                $schoolName .= $schoolTarif;
            }
            $endStyle = "";
            if ($item["PROPS"]["ITEM_END"]["VALUE"] && $is_GS == true) {
                $dt = new DateTime($item["PROPS"]["ITEM_END"]["VALUE"]);
                $ended = $dt->format("d.m.Y");
                
                if ($dt < $curDt) {
                    $endStyle .= "; border: 2px dashed white";
                }
            }
            
            $date = explode(" ", $item["NAME"]);
            $date = $date[0];
            
            $tmp = " (" . $date . (!empty($ended) && $is_GS ? ' - ' . $ended : '') . ")";
            $dopEnded = "(" . $date . ")";
            $addVal = str_replace("</div>", $tmp . "</div>", $productsData[$item["PROPS"]["ITEM"]["VALUE"]]);
            if (!empty($endStyle)) {
                $addVal = str_replace("background:blue", "background:blue$endStyle", $addVal);
            }
            
            $arReturn[$item["PROPS"]["USER"]["VALUE"]][] = $addVal;
            
            foreach ($item["PROPS"]["DOP_ITEMS"]["VALUE"] as $val) {
                $arReturn[$item["PROPS"]["USER"]["VALUE"]][] = str_replace("</div>", $dopEnded . "</div>",
                  $productsData[$val]);
            }
            
            
        }
        
        //Радар
        foreach ($arRadar as $userId => $dates) {
            $maxItem = "";
            $radarUsaHTML = "";
            $radarBg = "blue";
            
            usort($arRadarStart[$userId], function ($item1, $item2) {
                return new DateTime($item1) > new DateTime($item2);
            });
            $minItem = new DateTime(reset($arRadarStart[$userId]));
            
            usort($dates, function ($item1, $item2) {
                return new DateTime($item1) < new DateTime($item2);
            });
            $maxItem = new DateTime(reset($dates));
            
            $date = FormatDate("d.m.Y", $arRadarEnd[$userId]);
            if ($arRadarUSA[$userId] == 'Y' && empty($radarUsaHTML)) {
                $radarUsaHTML = " +<img src=\"" . SITE_TEMPLATE_PATH . "/img/flags/us.png\"/>";
                $radarBg = '#3399FF';
            }
            if ($maxItem < new DateTime()) {
                $arReturn[$userId][] = "<div class='product_item' style='background:$radarBg'>Радар" . $radarUsaHTML . " (" . $minItem->format("d.m.Y") . ") (Закочился - " . $maxItem->format("d.m.Y") . ")</div>";
            } else {
                $arReturn[$userId][] = "<div class='product_item' style='background:$radarBg'>Радар" . $radarUsaHTML . " (" . $minItem->format("d.m.Y") . " - " . $maxItem->format("d.m.Y") . ")</div>";
            }
        }
        
        return $arReturn;
    }
    
    //список продуктов
    public function getProducts($ids, $keys = false)
    {
        if (!$ids) {
            return false;
        }
        $arReturn = [];
        
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheId = "CRMgetProducts_" . $ids . $keys;
        $cache_dir = "/CRMgetProducts/";
        //$cache->clean($cacheId);
        $cacheTtl = 86400 * 7;
        if ($cache->read($cacheTtl, $cacheId)) {
            $arReturn = $cache->get($cacheId);
        } else {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);  //Старт тегирования
            
            if ($ids == "all") {
                $arReturn[] = array(
                  "label" => "VIP",
                  "value" => "VIP",
                );
            }
            
            $arReturn[] = array(
              "label" => "ШРИ (арх)",
              "value" => "ШРИ (арх)"
            );
            $arReturn[] = array(
              "label" => "ШРИ (мст)",
              "value" => "ШРИ (мст)"
            );
            $arReturn[] = array(
              "label" => "ШРИ (мгн)",
              "value" => "ШРИ (мгн)"
            );
            
            $arFilter = array("IBLOCK_ID" => 9, "ID" => $ids);
            if ($ids == "all") {
                unset($arFilter["ID"]);
            }
            
            $res = CIBlockElement::GetList(array(), $arFilter, false, false,
              array("ID", "NAME", "PROPERTY_SHORT_NAME", "PROPERTY_COLOR_CODE"));
            while ($item = $res->GetNext()) {
                $name = $item["NAME"];
                if ($item["PROPERTY_SHORT_NAME_VALUE"]) {
                    $name = $item["PROPERTY_SHORT_NAME_VALUE"];
                }
                
                $bgColor = "";
                if ($item["PROPERTY_COLOR_CODE_VALUE"]) {
                    $bgColor = "style='background:" . $item["PROPERTY_COLOR_CODE_VALUE"] . "'";
                }
                
                if ($ids == "all") {
                    $arReturn[] = array(
                      "label" => $name,
                      "value" => $keys ? $name : $item["ID"],
                    );
                } else {
                    $arReturn[$item["ID"]] = trim("
				<div class='product_item' " . $bgColor . ">
					" . $name . "
				</div>
				");
                }
            }
            
            if ($ids == "all") {
                //$arReturn = array_merge($arReturn, ["Радар", "Радар закончился"]);
                $arReturn = array_merge($arReturn, $this->fixedProducts);
                $arReturn = json_encode($arReturn, JSON_UNESCAPED_UNICODE);
            }
            $CACHE_MANAGER->RegisterTag("CRMgetProducts"); //Отметка тегом
            $CACHE_MANAGER->EndTagCache(); //Финализация тегирования
            
            
            $cache->set($cacheId, $arReturn);
            
        }
        
        return $arReturn;
    }
    
    public function getReferals($format = false)
    {
        
        $arReturn = array();
        $_db = CIBlockElement::GetList(array(), array(
            'IBLOCK_TYPE'             => 'FINPLAN', // тип инфоблока
            'IBLOCK_ID'               => static::$ibID, // ID инфоблока
            '!PROPERTY_REFERAL_VALUE' => false, // значение не должно быть пустым
          ), array('PROPERTY_REFERAL'));
        
        while ($arRow = $_db->Fetch()) {
            if (empty($arRow["PROPERTY_REFERAL_VALUE"])) {
                continue;
            }
            
            if ($format == "json") {
                $arReturn[] = [
                  "label" => $arRow["PROPERTY_REFERAL_VALUE"],
                  "value" => $arRow["PROPERTY_REFERAL_VALUE"],
                ];
            } else {
                $arReturn[] = array(
                  "VALUE" => $arRow["PROPERTY_REFERAL_VALUE"],
                  "TEXT"  => $arRow["PROPERTY_REFERAL_VALUE"],
                );
            }
        }
        
        if ($format == "json") {
            $arReturn[] = [
              "label" => "Все пустые",
              "value" => "Все пустые",
            ];
            $arReturn[] = [
              "label" => "Все заполненные",
              "value" => "Все заполненные",
            ];
            return json_encode($arReturn, JSON_UNESCAPED_UNICODE);
        }
        return $arReturn;
    }
    
    public function getManagers($format = false)
    {
        $arReturn = [];
        
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheId = "getManagers";
        $cacheTtl = 86400 * 7;
        //$cache->clean("getManagers");
        if ($cache->read($cacheTtl, $cacheId)) {
            $arReturn = $cache->get($cacheId);
        } else {
            $filter = array("GROUPS_ID" => array(7));
            $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
            while ($arUser = $rsUsers->Fetch()) {
                $fio = [];
                foreach (["LAST_NAME", "NAME", "SECOND_NAME"] as $field) {
                    if (!$arUser[$field]) {
                        continue;
                    }
                    
                    $fio[] = $arUser[$field];
                }
                
                if (!$fio) {
                    $fio[] = $arUser["LOGIN"];
                }
                
                $arReturn[] = array(
                  "VALUE" => $arUser["ID"],
                  "TEXT"  => implode(" ", $fio),
                );
            }
            $cache->set($cacheId, $arReturn);
        }
        
        if ($format == "json") {
            foreach ($arReturn as $n => $item) {
                $arReturn[$n] = [
                  "label" => $item["TEXT"],
                  "value" => $item["VALUE"],
                ];
            }
            $arReturn[] = [
              "label" => "Все пустые",
              "value" => "Все пустые",
            ];
            $arReturn[] = [
              "label" => "Все заполненные",
              "value" => "Все заполненные",
            ];
            
            return json_encode($arReturn, JSON_UNESCAPED_UNICODE);
        }
        
        return $arReturn;
    }
    
    public function getAllWebinars($format = false)
    {
        $arReturn = [];
        
        $arFilter = array("IBLOCK_ID" => static::$ibID, "!PROPERTY_WEBINARS" => false);
        $res = CIBlockElement::GetList(array(), $arFilter, ["PROPERTY_WEBINARS"], false, false);
        while ($ob = $res->GetNext()) {
            $arReturn[] = $ob["PROPERTY_WEBINARS_VALUE"];
        }
        
        if ($format) {
            $arReturn = array_merge($arReturn, ["Все пустые", "Все заполненные"]);
            return json_encode($arReturn, JSON_UNESCAPED_UNICODE);
        }
        
        return $arReturn;
    }
    
    public function getAllMainChimpLists($format = false)
    {
        $arReturn = [];
        
        $arFilter = array("IBLOCK_ID" => static::$ibID, "!PROPERTY_MAILCHIMP" => false);
        $res = CIBlockElement::GetList(array(), $arFilter, ["PROPERTY_MAILCHIMP"], false, false);
        while ($ob = $res->GetNext()) {
            $arReturn[] = str_replace(['&amp;amp;quot;', '&amp;quot;', '&quot;', '&amp;amp;amp;quot;', '"'], '',
              $ob["~PROPERTY_MAILCHIMP_VALUE"]);
        }
        
        if ($format) {
            $arReturn = array_unique($arReturn);
            $arReturn = array_merge($arReturn, ["Все пустые", "Все заполненные"]);
            
            foreach ($arReturn as $n => $v) {
                $arReturn[$n] = [
                  "label" => $v,
                  "value" => $v,
                ];
            }
            
            return json_encode($arReturn, JSON_UNESCAPED_UNICODE);
        }
        
        return $arReturn;
    }
    
    public function getAllTimezones($format = false)
    {
        $arReturn = [];
        
        $arFilter = array("IBLOCK_ID" => static::$ibID, "!PROPERTY_HOURS" => false);
        $res = CIBlockElement::GetList(array(), $arFilter, ["PROPERTY_HOURS"], false, false);
        while ($ob = $res->GetNext()) {
            $arReturn[] = $ob["PROPERTY_HOURS_VALUE"];
        }
        
        if ($format) {
            $arReturn = array_merge($arReturn, ["Все пустые", "Все заполненные"]);
            return json_encode($arReturn, JSON_UNESCAPED_UNICODE);
        }
        
        return $arReturn;
    }
}
