<?
//Класс для взаимодействия с системой CSmartSender через rest api
//описание api https://smartsendereu.atlassian.net/wiki/spaces/docsru/pages/97484808/
class CSmartSender {
	protected $token = "0eShcK83RDjqA57jmYL6cKqb6w2B3PabGU1k39qQ8ya4U03aLkYZ06T4eNPX";
	protected $syncLogFile;

	public function __construct(){
		//$this->syncLogFile = $_SERVER["DOCUMENT_ROOT"]."/log/smartSenderErrors.txt";
		$this->syncLogFile = "/var/www/bitrix/data/www/fin-plan.org/log/smartSenderErrors.txt";
	}

  private function ConnectSmartSender($dataUrl, $queryType = "GET", $arExtQuery = array()) {
		$arResult = array();
  		$client = new \GuzzleHttp\Client(['headers' => ['Authorization' => "Bearer ".$this->token]]);
     // CLogger::smSenderClient(print_r($client, true));
		try {
		$arQuery = array('http_errors' => true, 'form_params' => $arExtQuery);
/*		if(count($arExtQuery)>0){
			$arQuery = array_merge($arQuery, array("body"=>$arExtQuery));
		}*/
            
            CLogger::smSenderClient(print_r($arQuery, true));
		$res = $client->request($queryType, $dataUrl, $arQuery);
         //   CLogger::smSenderClient(print_r($client, true));
         
		$arResult = $res->getBody()->getContents();
		$arResult = json_decode($arResult, true);
  

		} catch (RequestException $e) {

			$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectSmartSender, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getRequest());
			file_put_contents($this->syncLogFile, $log . PHP_EOL, FILE_APPEND);
			CLogger::smartSenderErrors($log);

			if ($e->hasResponse()) {
				$log = 'err time: ' . date('Y-m-d H:i:s') . '; func: ConnectSmartSender, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getResponse());
				file_put_contents($this->syncLogFile, $log . PHP_EOL, FILE_APPEND);
                CLogger::smartSenderErrorsResponse($log);
			}
		}
	 return $arResult;
  }

  private function checkStrLenMessage($currentBlockCnt, $message='', $limit=1300){
	 if(strlen($message)>=$limit){
		$currentBlockCnt++;
	 }
	  return $currentBlockCnt;
  }

  public function checkSubscribe($uid=''){
	  $userParams = $this->getUserEventParams($uid);
	  $arResult = array("SUBSCRIBED"=>!empty($userParams["UF_SMART_SENDER_UID"]), "ENABLED"=>intval($userParams["UF_EVENTS_ENABLE"])>0);
	  return json_encode($arResult);
  }

  /**
   * Отправляет сообщение пользователю подписанному и имеющему id smart sender
   *
   * @param  string   $smUid id контакта в smart sender
   * @param  string   $message текст сообщения
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function sendMessage($smUid, $message=''){
      if(intval($smUid)<=0){
          $res = array("status"=>"error", "message"=>"empty smartsender id");
          return $res;
      }
     $url = "https://api.smartsender.com/v1/contacts/$smUid/send";
	  $arExtQuery = array("type"=>"text",
								 "content"=>$message,
								 "watermark"=>(new DateTime())->getTimestamp()
	  );
	  $res = $this->ConnectSmartSender($url, "POST", $arExtQuery);
	  return $res;
  }


  /**
   * Позволяет просмотреть подключенные каналы в проекте.
   *
   * @return array массив с ответом сервера
   *
   * @access public
   */
  public function getChannels(){
  	$res = array();
	$url = "https://api.smartsender.com/v1/channels?page=1&limitation=20";
	$res = $this->ConnectSmartSender($url);
	return $res;
  }


  /**
   * Позволяет просматривать существующие чаты.
   *
   * @return array массив с ответом сервера
   *
   * @access public
   */
  public function getChats(){
  	$res = array();
	$url = "https://api.smartsender.com/v1/chats?page=1&limitation=20";
	$res = $this->ConnectSmartSender($url);
	return $res;
  }

  /**
   * Позволяет получать список контактов в проекте.
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function getContacts(){
  	$res = array();
	$url = "https://api.smartsender.com/v1/contacts?page=1&limitation=20";
	$res = $this->ConnectSmartSender($url);
	return $res;
  }

  /**
   * Получение информации о контакте по идентификатору..
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function getContactInfo($uid){
  	$res = array();
	$url = "https://api.smartsender.com/v1/contacts/$uid/info";
	$res = $this->ConnectSmartSender($url);
	return $res;
  }


  /**
   * Позволяет просмотреть подключенный канал в проекте по идентификатору.
   *
   * @param  string   $cid id канала
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function getChannelById($cid){
  	$url = "https://api.smartsender.com/v1/channels/$cid";
	$res = $this->ConnectSmartSender($url);
	return $res;
  }


  /**
   * Позволяет изменить активность выбранного канала.
   *
   * @param  string   $cid id канала
   * @param  bool   $active активировать/деактивировать канал
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function updateChannelActive($cid, $active){
  	$url = "https://api.smartsender.com/v1/channels/$cid";
	$arExtQuery = array("state"=>$active);
	$res = $this->ConnectSmartSender($url, "PUT", $arExtQuery);
	return $res;
  }


  /**
   * Получает свойства пользователя битрикс для рассылки и подписки
   *
   * @param  int/str   $uid id пользователя битрикс
   *
   * @return array  массив свойств пользователя
   *
   * @access public
   */
  public function getUserEventParams($uid=''){
  	global $USER;
	if(intval($uid)<=0){
		$uid = $USER->GetID();
	}
	$data = CUser::GetList(($by="ID"), ($order="ASC"),
	            array(
	                //'!UF_EVENTS_CHAT_ID' => false,
						 'ID' => intval($uid)
	            ),
				  array("SELECT"=>array("UF_*"))
	        );
	$arEventParams = array();
	while($arUser = $data->Fetch()) {
	  $arEventParams = array(
		"UF_EVENTS_ENABLE" => $arUser["UF_EVENTS_ENABLE"],
		"UF_SMART_SENDER_UID" => $arUser["UF_SMART_SENDER_UID"],
		"UF_EVENTS_PORTFOLIO" => explode(",",$arUser["UF_EVENTS_PORTFOLIO"]),
		"UF_EVENTS_TYPES" => explode(",",$arUser["UF_EVENTS_TYPES"]),
		"UF_EVENTS_USE_FAVORITES" => $arUser["UF_EVENTS_USE_FAVORITES"],
	  );
	}
/*	 if(!count($arEventParams)){
	  $arEventParams = array( "UF_EVENTS_ENABLE" => 0 );
	}*/
	return $arEventParams;
  }


  /**
   * Позволяет просмотреть подписки на воронки выбранного контакта.
   *
   * @param  string   $smUid id контакта в smart sender
   *
   * @return array  массив с ответом сервера
   *
   * @access public
   */
  public function getFunnels($smUid){
  	$res = array();
   $url = "https://api.smartsender.com/v1/contacts/$smUid/funnels?page=1&limitation=20";
	$res = $this->ConnectSmartSender($url);
	return $res;
  }


  //Рассылает сообщения согласно настройкам пользователей
  public function sendEventMessagesSmarSender($userId=''){
  	global $USER;

	//Получить список пользователей со включенной рассылкой
	$arUsersFilter = array(
	                'UF_EVENTS_ENABLE' => 1,
	                '!UF_SMART_SENDER_UID' => false
	            );
	if(intval($userId)>0){
	  $arUsersFilter["ID"] = intval($userId);
	}
	$data = CUser::GetList(($by="ID"), ($order="ASC"),
	           $arUsersFilter,
				  array("SELECT"=>array("UF_*"))
	        );
	$arSendUsers = array();
	while($arUser = $data->Fetch()) {
	    $arSendUsers[$arUser['ID']] = array("NAME"=>$arUser['NAME'], "PHONE"=>$arUser['PERSONAL_PHONE'], "EMAIL"=>$arUser['EMAIL'], "UF_SMART_SENDER_UID"=>$arUser['UF_SMART_SENDER_UID']);
	}

	//Получаем события за день вперед

	$dt = new DateTime();
	//$_REQUEST["date"] = (new DateTime())->modify('+1 days')->format('d.m.Y');
	$_REQUEST["date"] = (new DateTime())->format('d.m.Y');

	if ($dt->format("N") == 5) {  //Если пятница - то получаем события за выходные и понедельник
		//$_REQUEST["dateto"] = $dt->modify("+3 days")->format('d.m.Y');
		$_REQUEST["dateto"] = $dt->format('d.m.Y');
	} else {
		$_REQUEST["dateto"] = $_REQUEST["date"];
	}


	$symbLimit = 1300;// Кол-во символов в одном сообщении

	//Отправка сообщений
	$sendedCnt = 0;
	$sendResult = array();
   foreach($arSendUsers as $uid => $arUser){
   	//Получаем события и портфели пользователя для фильтра
		$arUserEventParams = $this->getUserEventParams($uid);
		$_REQUEST["eventTypes"] = implode(",", $arUserEventParams["UF_EVENTS_TYPES"]);
		if(!empty($arUserEventParams["UF_EVENTS_PORTFOLIO"]))
		 $_REQUEST["eventPortfolio"] = $arUserEventParams["UF_EVENTS_PORTFOLIO"];
		if($arUserEventParams["UF_EVENTS_USE_FAVORITES"]==1){
			$_REQUEST["eventFavorites"] = 'Y';
			}


	$arEvents = array(); //События календаря
	$CREvents = new CRadarEvents();

		$CREvents->setFilter($uid);

		$arEvents = $CREvents->arEventsFiltered;

		$messageText = array();
		$messageBlockCnt = 0;
		if(count($arEvents)){
			$siteUrl = 'https://fin-plan.org';
			$cntEventsForUser = count($arEvents);
			if(count($arEvents)==1 && count(reset($arEvents))==0){
			  $cntEventsForUser = 0;
			}
			foreach($arEvents as $date=>$arEventItems){
			 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
			 $messageText[$messageBlockCnt] .= 'События на '.$date.PHP_EOL.'-------'.PHP_EOL;
			 $messageText[$messageBlockCnt] .= 'Настроить отслеживание событий Вы можете в личном кабинете https://fin-plan.org/personal/#events_subscribe?uid='.$uid.PHP_EOL.PHP_EOL;
			 $eventTypeText = '';
			  foreach($arEventItems as $eventCode=>$eventList){
			  	if(count($eventList)==0) continue;
				 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
				 $messageText[$messageBlockCnt] .= $CREvents->arEventsTypes[$eventCode].':'.PHP_EOL;
				 $ecnt = 0;
					foreach($eventList as $detailEvent){
						if(count($eventList)==1){
						  $ecntTxt = '';
						} else {
						  $ecnt++;
						  $ecntTxt = $ecnt.') ';
						}

					  switch ($eventCode) {
						    case "DIVIDENDS":
						    case "DIVIDENDS_USA":
							    $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
						       $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' Тек.цена акции: '.$detailEvent["Цена на дату закрытия"].' Дивиденды: '.$detailEvent["Дивиденды"].PHP_EOL;
						       break;
							 case "COUPONS":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' '.($detailEvent["DEFAULT"]?'(дефолт)':'').($detailEvent["STRUCTURED"]?'(структурная)':'').' Купон: '.$detailEvent["-"].(is_numeric($detailEvent["-"])?' руб.':'').' Купон, в %: '.$detailEvent["Ставка купона"].(is_numeric($detailEvent["-"])?' %':'').PHP_EOL;
								 break;
							 case "CANCELLATION":
							 case "OFERTA":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["NAME"].' - '.$siteUrl.$detailEvent["URL"].' '.($detailEvent["DEFAULT"]?'(дефолт)':'').($detailEvent["STRUCTURED"]?'(структурная)':'').PHP_EOL;
							    break;
							 case "REPORT_USA":
							 case "REPORT_RUS":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
							    $arTitle = explode(" - ",$detailEvent["UF_TITLE"]);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["UF_EMITENT"].' - '.$detailEvent["UF_LINK"].' '.(count($arTitle)>1?$arTitle[1]:$detailEvent["UF_TITLE"]).PHP_EOL;
							    break;
							 case "GOSA":
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .=  $ecntTxt.$detailEvent["UF_TITLE"].(!empty($detailEvent["UF_LINK"])?' '.$detailEvent["UF_LINK"]:'').PHP_EOL;
							 	 break;
							 default:
							 	 $messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
								 $messageText[$messageBlockCnt] .= $ecntTxt.$detailEvent["UF_TITLE"].( !empty($detailEvent["UF_DESCRIPTION"])?' '.$detailEvent["UF_DESCRIPTION"].' ':'').( !empty($detailEvent["UF_LINK"])?' '.$detailEvent["UF_LINK"]:'').PHP_EOL;
							 	 break;
						}

					}
					$messageBlockCnt	= $this->checkStrLenMessage($messageBlockCnt, $messageText[$messageBlockCnt], $symbLimit);
					$messageText[$messageBlockCnt] .= PHP_EOL;

			  }
			  //$messageText .= $eventTypeText;
			}

		}

   	if(!empty($arUserEventParams["UF_SMART_SENDER_UID"])){
   		if($cntEventsForUser>0){
   			//CLogger::EventsSenderTest(print_r($arEvents, true));
   		  for($m=0; $m<count($messageText); $m++){
   		      if(empty($messageText[$m])) continue;
		  	  $sendResult[] = $this->sendMessage($arUserEventParams["UF_SMART_SENDER_UID"], $messageText[$m]);
			  $sendedCnt++;
			  usleep(500);
			  }
			  }
   	}

		unset($CREvents, $arEvents, $messageText);
   }



	 return json_encode(array("result"=>"success", "sendResult"=> explode(", ",$sendResult), "message"=>"Отправлено сообщений: ".$sendedCnt));
  }

  }
	?>