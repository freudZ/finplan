<?php
class BaseHelper {
    static $ibID = 19;

	static function afterBuyAction($itemID){
		\Bitrix\Main\Loader::includeModule("iblock");

		$arFilter = Array("IBLOCK_ID"=>11, "ID" => $itemID);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("PROPERTY_USER", "PROPERTY_PAYED"));
		$arUserId = 0;
		if($item = $res->GetNext())
		{
		    if($item["PROPERTY_PAYED_VALUE"]) {

				$rsUser = CUser::GetByID($item["PROPERTY_USER_VALUE"]);
				$arUser = $rsUser->Fetch();
				$arUserId = $arUser["ID"];
				$arFilter = Array(
					"IBLOCK_ID" => self::$ibID,
					array(
						"LOGIC" => "OR",
						array("PROPERTY_USER" => $arUser["ID"]),
						array("PROPERTY_EMAIL" => $arUser["LOGIN"]),
					),
				);

				$res2 = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "PROPERTY_DATE_FIRST_BUY", "PROPERTY_USER"));
				if (!$res2->SelectedRowsCount()) {
					self::convertSaleItem($item["PROPERTY_USER_VALUE"], $arUser);

					$res2 = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "PROPERTY_DATE_FIRST_BUY", "PROPERTY_USER"));
					self::setDateBuyValues($res2->GetNext());
				} else {
					self::setDateBuyValues($res2->GetNext());
				}
			}
        }

		  //Пересчитываем продукты, даты
		  if($arUserId>0){
			$cdb = new CrmDataBase(false);
			$cdb->setUsersProducts(array("=PROPERTY_USER"=>$arUserId));
			unset($cdb);
			}


	}

	//конверт для юзера из продажи
	static function convertSaleItem($userID, $arUser){
		\Bitrix\Main\Loader::includeModule("iblock");
		//множ поля объединения
		$combFelds = array("NAME", "PHONE", "CITY", "HOURS");

		//социальные поля
		$socFields = array("utm_source", "utm_campaign", "utm_medium", "utm_term");

		//последние данные с полей
		$lastFields = array("ITEM", "IP");

		//сгруппированные данные
		$arUserData = array(
			"EMAIL" => trim($arUser["LOGIN"]),
			"USER" => $arUser["ID"],
		);

		foreach($combFelds as $item){
			$arUserData[$item] = array();
		}
		foreach($socFields as $item){
			$arUserData[$item] = "";
		}
		foreach($lastFields as $item){
			$arUserData[$item] = "";
		}
		$arUserData["DATE_CREATE"] = PHP_INT_MAX;
		$arUserData["DATE_UPDATE"] = false;


		$arFilter = Array("IBLOCK_ID"=>11, "PROPERTY_USER"=>$userID);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false);
		while($ob = $res->GetNextElement())
		{
			$fields = $ob->GetFields();
			$fields["PROPS"] = $ob->GetProperties();

			$arUserData["NAME"][] = $fields["PROPS"]["FIO"]["VALUE"];

			$val = static::convertPhone($fields["PROPS"]["PHONE"]["VALUE"]);

			if($val){
				$arUserData["PHONE"][] = $val;
			}

			if($fields["DATE_CREATE_UNIX"]<$arUserData["DATE_CREATE"]){
				$arUserData["DATE_CREATE"] = $fields["DATE_CREATE_UNIX"];
			}
			if($fields["TIMESTAMP_X_UNIX"]>$arUserData["DATE_UPDATE"]){
				$arUserData["DATE_UPDATE"] = $fields["TIMESTAMP_X_UNIX"];
			}
		}

		if($arUserData["DATE_CREATE"]){
			$arUserData["DATE_CREATE"] = date("d.m.Y H:i:s", $arUserData["DATE_CREATE"]);
		}
		if($arUserData["DATE_UPDATE"]){
			$arUserData["DATE_UPDATE"] = date("d.m.Y H:i:s", $arUserData["DATE_UPDATE"]);
		}

		//дата первой и последней покупки
        /*
        if($arUserData["DATE_CREATE"]) {
            $arUserData["DATE_FIRST_BUY"] = date("d.m.Y H:i:s", $arUserData["DATE_CREATE"]);
            $arUserData["DATE_LAST_BUY"] = date("d.m.Y H:i:s", $arUserData["DATE_CREATE"]);
        }
        */


        foreach($combFelds as $item){
			if($arUserData[$item]){
				$arUserData[$item] = array_unique($arUserData[$item]);
			}
		}

        file_put_contents( $_SERVER["DOCUMENT_ROOT"] . '/log/convertSaleItem_log.txt', json_encode($arUserData), FILE_APPEND);


        $el = new CIBlockElement;
		$arLoadProductArray = Array(
			"IBLOCK_ID" => self::$ibID,
			"NAME" => date("d.m.Y H:i:s"),
			"ACTIVE" => "N",
			"PROPERTY_VALUES" => $arUserData,
		);
		$el->Add($arLoadProductArray);

/*		if($el->LAST_ERROR){
			*/?><!--<pre><?/*print_r($el->LAST_ERROR)*/?></pre>--><?/*
			exit();
		}*/
	}

	static function setDateBuyValues($data){
        $arFilter = Array(
            "IBLOCK_ID" => 11,
            "PROPERTY_USER" => $data["PROPERTY_USER_VALUE"],
            "!PROPERTY_PAYED" => false
        );

        if(!$data["PROPERTY_DATE_FIRST_BUY_VALUE"]) {
            $res = CIBlockElement::GetList(array("DATE_CREATE" => "asc"), $arFilter, false, false, array("DATE_CREATE"));
            if ($item = $res->GetNext()) {
                CIBlockElement::SetPropertyValues($data["ID"], $data["IBLOCK_ID"], $item["DATE_CREATE"], "DATE_FIRST_BUY");
            }
        }

        $res = CIBlockElement::GetList(array("DATE_CREATE" => "desc"), $arFilter, false, false, array("DATE_CREATE"));
        if ($item = $res->GetNext()) {
            CIBlockElement::SetPropertyValues($data["ID"], $data["IBLOCK_ID"], $item["DATE_CREATE"], "DATE_LAST_BUY");
        }
    }

    /**
     * Установить всем элементам в ЦРМ даты покупок
     */
    static function setAllCrmItemsDatesBuy(){
        \Bitrix\Main\Loader::includeModule("iblock");

        $arFilter = Array(
            "IBLOCK_ID" => 19,
            "!PROPERTY_USER" => false,
            "PROPERTY_DATE_FIRST_BUY" => false,
            "PROPERTY_DATE_LAST_BUY" => false,
        );

        $res2 = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "PROPERTY_USER"));
        while ($item = $res2->GetNext()){

            $arFilter = Array(
                "IBLOCK_ID" => 11,
                "PROPERTY_USER" => $item["PROPERTY_USER_VALUE"],
                "!PROPERTY_PAYED" => false
            );

            $res = CIBlockElement::GetList(array("DATE_CREATE" => "asc"), $arFilter, false, false, array("DATE_CREATE"));
            if ($val = $res->GetNext()) {
                CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $val["DATE_CREATE"], "DATE_FIRST_BUY");
            }

            $res = CIBlockElement::GetList(array("DATE_CREATE" => "desc"), $arFilter, false, false, array("DATE_CREATE"));
            if ($val = $res->GetNext()) {
                CIBlockElement::SetPropertyValues($item["ID"], $item["IBLOCK_ID"], $val["DATE_CREATE"], "DATE_LAST_BUY");
            }
        }
    }

	static function convertPhone($val){
		$val = str_replace(array("(", ")", "-", " ", "_"), "", $val);
		if(strlen($val)!=12){
			return false;
		}
		return $val;
	}
}