<?

/**
 * Class CLearning Для работы с базой обучения из админки tools
 */
class CLearning
{
    
    public function getResponse($url, $json_decode=true)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_GET, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        
        if (!isset($response)) {
            return null;
        }
        if($json_decode){
            $response = json_decode($response, true);
        }
        return $response;
    }
    
    public function postResponse($url, $data, $json_decode=true)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        
        if (!isset($response)) {
            return null;
        }
        if($json_decode){
            $response = json_decode($response, true);
        }
        return $response;
    }
    
}

?>