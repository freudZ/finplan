<?php

/**
 * Class Loger - логирование данных
 *
 * для логирования данных вызвать метод класса Loger,
 * метод класса будет соответствовать имени файла,
 * 1й параметр метода - данные логирования, 2й параметр метода - абсолютный путь от корня сервера(не обязательный)
 *
 * пример вызова
 * СLogger::devel('HELLO WORLD');
 */

class CLogger
{
    
    public function __call($method, $params)
    {
        
        //if ($params[0]) {
            if (!$params[2]) { //Если не указан флаг отправки на почту то указываем false
                $params[2] = false;
            }
            if (!$params[3]) { //Если не указан флаг логирования времени и пути файла то указываем false
                $params[3] = false;
            } 
            self::writeLog($params[0], $method, $params[1], $params[2], $params[3]);
            
        //}
        
    }
    
    
    public static function __callStatic($method, $params)
    {
        
        //if ($params[0]) {

            self::writeLog($params[0], $method, $params[1]);
            
        //}
        
    }
    
    
    /**
     * @param        $content
     * @param string $filename
     * @param string $filePath
     * @param bool $sendEmail - отправлять еще и на почту
     */
    
    public static function writeLog($content, $filename = '', $filePath = '', $sendEmail = false, $logTimeAndPath = false)
    {
        
        if (!$filename) {
            $filename = 'devel';
        }
        
        
        if (!$filePath) {
            $filePath = $_SERVER["DOCUMENT_ROOT"] . "/log/";
        }

        $dateStamp = date('d.m.Y H:i:s ');
        if ($sendEmail) {
           mail("alphaprogrammer@gmail.com", "logger ".$dateStamp, $_SERVER['SCRIPT_FILENAME'] . "# $content\n");
        }
		  $timeAndPath = '';
		  if($logTimeAndPath){
			 $timeAndPath = $dateStamp . ' ' . $_SERVER['SCRIPT_FILENAME'] . "# ";
		  }
        $msg = $timeAndPath . "$content\n";

        $fullFileName = $filePath . '/' . $filename . '.log';

		  if(empty($content)){
		  	if(file_exists($fullFileName)!==false){
		  		unlink($fullFileName);
		  	}
		  }


        error_log($msg, 3, $fullFileName);
        
    }
    
}