<?/**
 * Класс для работы с таблицей хранящей автонумерацию для различных объектов БД
 *
 * @copyright 2021
 * @license   GNU
 * @version   1
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     24 class CEnumerators
 *     30   function __construct()
 *     45   function getNewEnum($type=)
 *     84   function getEnum($type=)
 *    117   function incrementNumber($enum, $typeEnumeration)
 *
 * TOTAL FUNCTIONS: 4
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */


class CEnumerators {
  private $hlIblockId = 54;
  private $hlTableName = 'hl_enumerators';
  private $enumTypes = array( "TA"=>"DIGIT",
                              "PORTFOLIO"=>"DIGIT",
  );
   function __construct(){

   }



/**
 * Делает приращение номера для $type, записывает в БД, кеширует и возвращает в результат
 *
 * @param  string   $type Тип объекта для которого запрошен новый номер
 *
 * @return string  Новый номер для объекта $type
 *
 * @access public
 */
public function getNewEnum($type=''){
      //Если передан незарегистрированный тип объекта - вернем ноль
      if(array_key_exists($type, $this->enumTypes)==false) return 0;
      $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
      $cacheId = "enumerators_".$type;
      $cacheTtl = 86400 * 7;
      $enum = 0;
      Global $DB;
          //$cache->clean("enumerators_".$type);
      if ($cache->read($cacheTtl, $cacheId)) {
               $enum = $cache->get($cacheId);
           } else {
               $query = "SELECT `UF_VALUE` FROM `$this->hlTableName` WHERE `UF_TYPE_ENUM` = '$type'";
               $res = $DB->Query($query);
               if($ob = $res->fetch()){
                $enum = $ob["UF_VALUE"];
               }
            $cache->set($cacheId, $enum);
           }

      //Получаем тип нумерации
      $typeEnumeration = $this->enumTypes[$type];
      $newNumber = $this->incrementNumber($enum, $typeEnumeration);

      $query = "UPDATE `$this->hlTableName` SET `UF_VALUE`='$newNumber' WHERE `UF_TYPE_ENUM`='$type'";
      $DB->Query($query);
      $cache->clean("enumerators_".$type);
      return $newNumber;
}


/** Возвращает текущее значение нумератора для переданного $type
 *
 * @param  string   $type Тип объекта для которого запрошен новый номер
 *
 * @return string  Текущий номер для объекта $type
 *
 * @access public
 */
public function getEnum($type=''){
      $result = 0;
      //Если передан незарегистрированный тип объекта - вернем ноль
      if(array_key_exists($type, $this->enumTypes)==false) return 0;
      $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
      $cacheId = "enumerators_".$type;
      $cacheTtl = 86400 * 7;

      Global $DB;
          //$cache->clean("enumerators_".$type);
      if ($cache->read($cacheTtl, $cacheId)) {
               $result = $cache->get($cacheId);
           } else {
               $query = "SELECT `UF_VALUE` FROM `$this->hlTableName` WHERE `UF_TYPE_ENUM` = '$type'";
               $res = $DB->Query($query);
               if($ob = $res->fetch()){
                $result = $ob["UF_VALUE"];
               }
            $cache->set($cacheId, $result);
           }
 return $result;
}

/**
 * Выполняет приращение согласно выбранному типу данных для нумерации объекта
 *
 * @param  string   $enum Текущее значение нумератора объекта из БД
 * @param  string   $typeEnumeration тип данных для нумерации
 *
 * @return string  Инкрементированный номер
 *
 * @access private
 */
private function incrementNumber($enum, $typeEnumeration){
   $result = false;
   if("DIGIT"==$typeEnumeration){
     $enum = intval($enum);
     $enum++;
     $result = $enum;
   }
   return $result;
}

}

