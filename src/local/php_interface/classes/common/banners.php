<?/**
 * Класс для работы с таргеритрованием и ротацией баннеров на сайте.
 *
 * Формирует фильтры для компонентов, показывающих баннеры; Пересчитывает и кеширует аудитории пользователей.
 *
 * @copyright 2020
 * @version   0.0.1
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     33 class CBanners
 *     50   function __construct($clearCache=false)
 *     60   function getBannerFilter()
 *    115   function getDefendedId()
 *    119   function clearCache($clearUserLists = true)
 *    154   function setArraysClientsUsers($clearCache=false)
 *    182   function setArraysWebinarsUsers($clearCache=false)
 *    218   function setArraysIndividualUsers($clearCache=false)
 *    247   function getBannersCookie($name=)
 *    255   function setBannersCookie($name=,$value=)
 *    274   function getBlogBannerId()
 *    320   function getRightBannerId($activeOnly=true)
 *    362   function getRightBanner($activeOnly=true)
 *    444   function getRightBannerHtml()
 *
 * TOTAL FUNCTIONS: 13
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */

class CBanners {
	public $arClientsUserList = array(); //Список id пользователей - клиентов (делавших покупку)
	public $arWebinarsUserList = array(); //Список id пользователей посетивших вебинары
	public $arIndividualUserList = array(); //Список id пользователей из настроек индивидуальных аудиторий
	public static $arDefendedId = array(371891, 371889, 371892, 371893); //Список защищенных от удаления id аудиторий показа
	public $arAuditoriesFilter = array(
	 "AUTH" => array("CLIENTS"=>371889, "NOCLIENT_NOWEB"=>371892, "NOCLIENT_WEB"=>371893),
	 "NOT_AUTH" => 371891,
	);

	private $crmIblockId = 19; //id инфоблока с CRM
	private $blogBannersIblockId = 45; //id инфоблока с банннерами для блога
	private $buyingOfSeminarsIblockId = 11; //id инфоблока с продажами
	private $rightSidebarBannersIblockId = 7; //id инфоблока с баннерами для правого сайдбара
	private $auditoriesIblockId = 71; //id инфоблока с настройками аудиторий "Виды целей таргетирования баннеров"
	private $noTarget = false; // не использовать таргетирование

	public function __construct($clearCache=false) {
	  if($clearCache==true){
	  	$this->clearCache();
	  } else {
	  $this->setArraysClientsUsers();
	  $this->setArraysWebinarsUsers();
	  $this->setArraysIndividualUsers();
	  }
	}

	public function getBannerFilter(){
		$arReturn = array("TYPE"=>"ALL", "FILTER" => array());
		Global $USER;
		$userType = 'ALL';
		if(!$USER->IsAuthorized()){ //Для неавторизованных пользователей
		    $userType = 'NOT_AUTH';
			 $arReturn = array("TYPE"=>$userType, "FILTER"=>array("LOGIC"=>"OR", array("PROPERTY_TARGETING" => $this->arAuditoriesFilter["NOT_AUTH"]), array("PROPERTY_TARGETING" => false)));
		} else {  //Для всех авторизованных
		  $UserId = $USER->GetID();
		  $arFiltersId = array();

		  //Ищем среди клиентов
		  if(in_array($UserId, $this->arClientsUserList)){
			 $arFiltersId = $this->arAuditoriesFilter["AUTH"]["CLIENTS"];
			 $userType = 'CLIENTS';
		  } else {
			 //если не клиент - определяем был ли он на вебинарах
			 if(in_array($UserId, $this->arWebinarsUserList)){
			   $arFiltersId = $this->arAuditoriesFilter["AUTH"]["NOCLIENT_WEB"];
				$userType = 'NOCLIENT_WEB';
		    } else {
				$arFiltersId = $this->arAuditoriesFilter["AUTH"]["NOCLIENT_NOWEB"];
				$userType = 'NOCLIENT_NOWEB';
		    }
		  }

		  if(array_key_exists($UserId, $this->arIndividualUserList) && count($this->arIndividualUserList[$UserId])>0){
		  	 if(!is_array($arFiltersId)){
		  	 	$arFiltersId = array($arFiltersId);
		  	 }
			 $arFiltersId = array_merge($arFiltersId, array_values($this->arIndividualUserList[$UserId]));
			 //$arFiltersId = implode("|", $arFiltersId);
			// $userType = 'INDIVIDUAL';
		  }


		  $arReturn = array("TYPE"=>$userType, "FILTER"=>array("LOGIC"=>"OR", array("PROPERTY_TARGETING" => $arFiltersId), array("PROPERTY_TARGETING" => false)));





		}

		return $arReturn;
	}


   /**
    * Возвращает список id запрещенные к удалению
    *
    * @return array
    *
    * @static
    */
   static function getDefendedId(){
   	return self::$arDefendedId;
   }

	public function clearCache($clearUserLists = true){
		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

		if($clearUserLists){
		//Очищаем кеш выборок пользователей
	  $this->setArraysClientsUsers(true);
	  $this->setArraysWebinarsUsers(true);
	  $this->setArraysIndividualUsers(true);
}

		//Удаляем кеш для фильтров верхних баннеров блога
		foreach($this->arAuditoriesFilter["AUTH"] as $k=>$v){
			$cache->clean('getBlogBannerId_'.$k);
			$cache->clean('getRightBanner_'.$k);
			$cache->clean('getRightBannerId_'.$k);
		}
			$cache->clean('getBlogBannerId_NOT_AUTH');
			$cache->clean('getRightBannerId_NOT_AUTH');
			$cache->clean('getRightBanner_NOT_AUTH');
			$cache->clean('getBlogBannerId_ALL');
			$cache->clean('getRightBannerId_ALL');
			$cache->clean('getRightBanner_ALL');

	}


  /**
   * Заполняет массивы клиентов или перестраивает
   *
   * @param  bool   $clearCache (Optional) принудительный сброс кеша перед получением/перестроением списка пользователей-клиентов
   *
   * @return set in class array
   *
   * @access public
   */
  public function setArraysClientsUsers($clearCache=false){
	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	 if($clearCache==true){
	  $cache->clean("setArraysClientsUsers");
	 }
	$cacheId = "setArraysClientsUsers";
	$cacheTtl = 86400 * 7;

	if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else {

		CModule::IncludeModule("iblock");
		$arResult = array();
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_PAYED", "PROPERTY_USER" );
		$arFilter = Array("IBLOCK_ID"=>IntVal($this->buyingOfSeminarsIblockId), "PROPERTY_PAYED"=>10);
		$res = CIBlockElement::GetList(Array("PROPERTY_USER"=>"ASC"), $arFilter, array("PROPERTY_USER"), false, $arSelect);
		//$ob = $res->fetch();
		while($ob = $res->fetch()){
			if(!in_array($ob["PROPERTY_USER_VALUE"],$arResult))
			$arResult[] = intval($ob["PROPERTY_USER_VALUE"]);
		}
		sort($arResult);
	             $cache->set($cacheId, $arResult);
        }
	 $this->arClientsUserList = $arResult;
  }

  public function setArraysWebinarsUsers($clearCache=false){
	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	 if($clearCache==true){
	  $cache->clean("setArraysWebinarsUsers");
	 }
	$cacheId = "setArraysWebinarsUsers";
	$cacheTtl = 86400 * 7;

	if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else {

		CModule::IncludeModule("iblock");
		$arResult = array();
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_WEBINARS_CNT", "PROPERTY_USER" );
		$arFilter = Array("IBLOCK_ID"=>IntVal($this->crmIblockId), "!PROPERTY_WEBINARS_CNT"=>false);
		$res = CIBlockElement::GetList(Array("PROPERTY_USER"=>"ASC"), $arFilter, array("PROPERTY_USER"), false, $arSelect);
		//$ob = $res->fetch();
		while($ob = $res->fetch()){
			if(!in_array($ob["PROPERTY_USER_VALUE"],$arResult))
			$arResult[] = $ob["PROPERTY_USER_VALUE"];
		}
	             $cache->set($cacheId, $arResult);
        }
	 $this->arWebinarsUserList = $arResult;
  }

  /**
   * Заполняет массивы клиентов индивидуальных аудиторий или перестраивает
   *
   * @param  bool   $clearCache (Optional) принудительный сброс кеша перед получением/перестроением списка пользователей индивидуальных аудиторий
   *
   * @return set in class array
   *
   * @access public
   */
  public function setArraysIndividualUsers($clearCache=false){
	$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	 if($clearCache==true){
	  $cache->clean("setArraysIndividualUsers");
	 }
	$cacheId = "setArraysIndividualUsers";
	$cacheTtl = 0;//86400 * 7;

	if ($cache->read($cacheTtl, $cacheId)) {
            $arResult = $cache->get($cacheId);
        } else {

		CModule::IncludeModule("iblock");
		$arResult = array();
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_USERS" );
		$arFilter = Array("IBLOCK_ID"=>IntVal($this->auditoriesIblockId), "SECTION_ID"=>94, "!PROPERTY_USERS"=>false);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		//$ob = $res->fetch();
		while($ob = $res->fetch()){

		  	if(!in_array($ob["ID"],$arResult[$ob["PROPERTY_USERS_VALUE"]]))
		     $arResult[strval($ob["PROPERTY_USERS_VALUE"])][] = intval($ob["ID"]);
		}
		ksort($arResult);
	             $cache->set($cacheId, $arResult);
        }
	 $this->arIndividualUserList = $arResult;
  }

  public function getBannersCookie($name=''){
  	if(empty($name)) return '';
  	$cookieValue = '';
  	$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	$cookieValue = $request->getCookie($name);
	return $cookieValue;
  }

  public function setBannersCookie($name='',$value=''){
  	if(empty($name)) return;
  	$cookie = new \Bitrix\Main\Web\Cookie($name, $value, time()+86400*30);
	$cookie->setSpread(\Bitrix\Main\Web\Cookie::SPREAD_DOMAIN); // распространять куки на все домены
	$cookie->setDomain("fin-plan.org"); // домен
	$cookie->setPath("/"); // путь
	$cookie->setSecure(false); // безопасное хранение cookie
	$cookie->setHttpOnly(false);
	\Bitrix\Main\Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
  }

  /**
   * Возвращает id баннера c приращением очереди +1 на каждом хите в рамках конкретной сессии для фильтра баннеров в блоге
   * Сброс управляемого кеша реализован в подключаемом в init.php файле /local/php_interface/include/managed_cache_iblocks.php
   *
   * @return array
   *
   * @access public
   */
  public	function getBlogBannerId(){
	$arBanners = array();
	$arFilterData = $this->getBannerFilter();

   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cacheId = "getBlogBannerId_".$arFilterData["TYPE"];
	$cacheTtl = 86400 * 365;
	if ($cache->read($cacheTtl, $cacheId)) {
            $arBanners = $cache->get($cacheId);
        } else {
			CModule::IncludeModule("iblock");
			$arFilter = Array("IBLOCK_ID" => $this->blogBannersIblockId, "ACTIVE"=>"Y");
			if(count($arFilterData["FILTER"])){
				$arFilter[] = $arFilterData["FILTER"];
			}
			$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, array("ID"));
			while($row=$res->Fetch()){
			  $arBanners[] = $row["ID"];
			}
			$cache->set($cacheId, $arBanners);
        }

	  //Управление счетчиком показов через куки
		 $blog_extended_banner = intval($this->getBannersCookie('blog_extended_banner'));
	  if($blog_extended_banner==count($arBanners) || $blog_extended_banner>count($arBanners)){ //Если счетчик максимальный или если изменился тип набора баннеров и поменялось их кол-во - то выставляем счетчик =1
	  	 $blog_extended_banner = 1;
	  } else if($blog_extended_banner<count($arBanners)){
	  	 $blog_extended_banner = $blog_extended_banner+1;
	  }
		$this->setBannersCookie('blog_extended_banner', $blog_extended_banner);

	  return $arBanners[$blog_extended_banner-1];

	}


  /**
   * Возвращает id баннера c приращением очереди +1 на каждом хите в рамках конкретной сессии для фильтра баннеров в блоге
   * Настроен сброс кеша при интерактивном изменении/создании элементов инфоблока
   *
   * @param  bool   $activeOnly (Optional) Выбирать только активные элементы инфоблока
   *
   * @return array
   *
   * @access public
   */
  public	function getRightBannerId($activeOnly=true){
	$arBanners = array();
	$arFilterData = $this->getBannerFilter();
   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	$cacheId = "getRightBannerId_".$arFilterData["TYPE"];
	$cacheTtl = 86400 * 365;
	if ($cache->read($cacheTtl, $cacheId)) {
            $arBanners = $cache->get($cacheId);
        } else {
			CModule::IncludeModule("iblock");
			$arFilter = Array("IBLOCK_ID" => $this->rightSidebarBannersIblockId);
			if($activeOnly==true){
			 $arFilter["ACTIVE"] = "Y";
			}
			if(count($arFilterData["FILTER"])){
				$arFilter[] = $arFilterData["FILTER"];
			}
			$arSelect = array("ID", "NAME", "PROPERTY_TITLE_TOP", "PROPERTY_ALT_TOP", "PROPERTY_LINK_TOP", "PROPERTY_TARGET_TOP", "PROPERTY_NOFOLLOW_TOP",
								   "PROPERTY_TITLE_BOTTOM", "PROPERTY_ALT_BOTTOM", "PROPERTY_LINK_BOTTOM", "PROPERTY_TARGET_BOTTOM", "PROPERTY_NOFOLLOW_BOTTOM",
									"PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_POPUP");
			$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
			while($row=$res->Fetch()){
			  $arBanners[] = $row["ID"];
			}
			$cache->set($cacheId, $arBanners);
        }

	  //Управление счетчиком показов через куки
		 $right_banner = intval($this->getBannersCookie('right_banner'));
	  if($right_banner==count($arBanners) || $right_banner>count($arBanners)){ //Если счетчик максимальный или если изменился тип набора баннеров и поменялось их кол-во - то выставляем счетчик =1
	  	 $right_banner = 1;
	  } else if($right_banner<count($arBanners)){
	  	 $right_banner = $right_banner+1;
	  }
		$this->setBannersCookie('right_banner', $right_banner);

	  return $arBanners[$right_banner-1];

	}

	//Возвращает данные баннера c приращением очереди +1 на каждом хите в рамках конкретной сессии для фильтра баннеров в блоге
	//Настроен сброс кеша при интерактивном изменении/создании элементов инфоблока
  public	function getRightBanner($activeOnly=true){
	$arBanners = array();

	$arFilterData = $this->getBannerFilter();

   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
	if($this->noTarget==true){
	$cacheId = "getRightBanner";
	}  else {
	$cacheId = "getRightBanner_".$arFilterData["TYPE"];
	}
	$cacheTtl = 86400 * 365;
	if ($cache->read($cacheTtl, $cacheId)) {
            $arBanners = $cache->get($cacheId);
        } else {
			CModule::IncludeModule("iblock");
			$arFilter = Array("IBLOCK_ID" => $this->rightSidebarBannersIblockId);
			if($activeOnly==true){
			 $arFilter["ACTIVE"] = "Y";
			  }
			if(count($arFilterData["FILTER"]) && $this->noTarget==false){
				$arFilter[] = $arFilterData["FILTER"];
			}
			$arSelect = array("ID", "NAME", "PROPERTY_TITLE_TOP", "PROPERTY_ALT_TOP", "PROPERTY_LINK_TOP", "PROPERTY_TARGET_TOP", "PROPERTY_NOFOLLOW_TOP",
								   "PROPERTY_TITLE_BOTTOM", "PROPERTY_ALT_BOTTOM", "PROPERTY_LINK_BOTTOM", "PROPERTY_TARGET_BOTTOM", "PROPERTY_NOFOLLOW_BOTTOM",
									"PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_POPUP_TOP", "PROPERTY_POPUP_BOTTOM");




			$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
			while($row=$res->Fetch()){
				$arTop = array();
				$arBottom = array();
			  foreach($row as $k=>$prop){
				 if(strpos($k, "TOP")!==false && strpos($k,"_ID")===false){
				 	$arTop[str_replace(array("PROPERTY_", "_TOP", "_VALUE"),"",$k)] = $prop;
				 }
				 if(strpos($k, "BOTTOM")!==false && strpos($k,"_ID")===false){
				 	$arBottom[str_replace(array("PROPERTY_", "_BOTTOM", "_VALUE"),"",$k)] = $prop;
				 }
				 if(strpos($k,"PREVIEW_PICTURE")!==false){
				 	$row["PREVIEW_PICTURE"] = CFile::GetPath($row["PREVIEW_PICTURE"]);
				 }
			  }

			  if(empty($arTop["ALT"])){
			  	$arTop["ALT"] = $arTop["TITLE"];
			  }
			  if(empty($arTop["TARGET"])){
			  	$arTop["TARGET"] = "_blank";
			  }
			  if(empty($row["DETAIL_PICTURE"])){
			  	$row["DETAIL_PICTURE"] = $row["PREVIEW_PICTURE"];
			  }

			  foreach($arBottom as $kb=>$propb){
			  	if(empty($propb)){
				  $arBottom[$kb] = $arTop[$kb];
			  	}
			  }
			  $arBanner["TOP"] = array_merge($arTop, array("PICTURE"=>$row["PREVIEW_PICTURE"]));;
			  $arBanner["BOTTOM"] = array_merge($arBottom, array("PICTURE"=>$row["DETAIL_PICTURE"]));
			  $arBanner["ID"] = $row["ID"];

			  $arBanners[] = $arBanner;
			}
			$cache->set($cacheId, $arBanners);
        }

	  //Управление счетчиком показов через куки
		 $right_banner = intval($this->getBannersCookie('right_banner'));
	  if($right_banner==count($arBanners) || $right_banner>count($arBanners)){ //Если счетчик максимальный или если изменился тип набора баннеров и поменялось их кол-во - то выставляем счетчик =1
	  	 $right_banner = 1;
	  } else if($right_banner<count($arBanners)){
	  	 $right_banner = $right_banner+1;
	  }
		$this->setBannersCookie('right_banner', $right_banner);

	  return $arBanners[$right_banner-1];
	}

  public function getRightBannerHtml(){
	  $arBanner = $this->getRightBanner();
	  $arHtmlReturn = array("TOP"=>"", "BOTTOM"=>"");
	  $modal_top_data = '';
	  $topLink = '#';
	  $bottomLink = '#';
	  $modal_bottom_data = '';
		 if(intval($arBanner["TOP"]["POPUP"])>0){
		 	$modal_top_data='data-content-id="'.$arBanner["TOP"]["POPUP"].'" data-toggle="modal" data-target="#popup_master_subscribe_custom"';
		 }
		 if(intval($arBanner["BOTTOM"]["POPUP"])>0){
		 	$modal_bottom_data='data-content-id="'.$arBanner["BOTTOM"]["POPUP"].'" data-toggle="modal" data-target="#popup_master_subscribe_custom"';
		 }
	  if(!empty($arBanner["TOP"]["LINK"]) && empty($arBanner["TOP"]["POPUP"])){
		 $topLink = $arBanner["TOP"]["LINK"];
		};
	  if(!empty($arBanner["BOTTOM"]["LINK"]) && empty($arBanner["BOTTOM"]["POPUP"])){
		 $bottomLink = $arBanner["BOTTOM"]["LINK"];
		};
	  $topStr = '<a data-popup="'.$arBanner["TOP"]["POPUP"].'" data-position="top" rel="'.($arBanner["TOP"]["NOFOLLOW"]=='Y'?'nofollow':'').'" href="'.$topLink.'" '.$modal_top_data.' target="'.$arBanner["TOP"]["TARGET"].'" title="'.$arBanner["TOP"]["TITLE"].'" alt="'.$arBanner["TOP"]["ALT"].'"><img src="'.$arBanner["TOP"]["PICTURE"].'" alt="'.$arBanner["TOP"]["ALT"].'"/></a>';
	  $bottomStr = '<a data-sessnum="'.$_SESSION["right_banner"].'" data-position="bottom" rel="'.($arBanner["TOP"]["NOFOLLOW"]=='Y'?'nofollow':'').'" href="'.$bottomLink.'" '.$modal_bottom_data.' target="'.$arBanner["TOP"]["TARGET"].'" title="'.$arBanner["TOP"]["TITLE"].'" alt="'.$arBanner["TOP"]["ALT"].'"><img src="'.$arBanner["TOP"]["PICTURE"].'" alt="'.$arBanner["TOP"]["ALT"].'"/></a>';

	  $arHtmlReturn["TOP"] = $topStr;
	  $arHtmlReturn["BOTTOM"] = $bottomStr;


	  return $arHtmlReturn;
   }

}


 ?>