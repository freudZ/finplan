<? /**
 * Class CUsersStat
 * Ведение статистики посещения страниц авторизованными пользователями
 * Определение новых пользователей радара для онбоардинга
 */
class CUsersStat
{
    private static $usersStatTableName = 'hl_users_page_stat';
    
    /**
     * Записывает в БД новое значение или увеличивает счетчик посещений и изменяет последнюю дату посещения для переданного ключа
     * @param        $uid
     * @param        $key
     * @param string $url
     * @return false
     */
    public static function setStat($uid, $key, $url = '')
    {
        //if($uid!=7307) return false;
        if (intval($uid) <= 0 || empty($key)) {
            return false;
        }
        global $DB;
        $datetime = (new DateTime())->format('Y-m-d H:i:s');
        $sql = "INSERT INTO `" . self::$usersStatTableName . "`  (`UF_USER_ID`, `UF_COUNTER`, `UF_KEY`,
        `UF_URL`, `UF_FIRST_DATETIME`, `UF_LAST_DATETIME`) VALUES ($uid, 1, '$key', '$url', '$datetime', '$datetime')
        ON DUPLICATE KEY UPDATE `UF_COUNTER` = `UF_COUNTER` + 1, `UF_LAST_DATETIME` = '$datetime'";
        $DB->Query($sql);
    }
    
    /**
     * Возвращает статистику по пользователю
     * Если передан ключ то возвращает простой массив для значения переданного ключа
     * Если ключи не переданы - возвращает вложенный массив с ключами по ключам записанной статистики
     * @param        $uid
     * @param string $key
     * @return array|false
     */
    public static function getStat($uid, $key = '')
    {
        if (intval($uid) <= 0) {
            return false;
        }
        global $DB;
        $arResult = array();
        $sql = "SELECT * FROM `" . self::$usersStatTableName . "` WHERE `UF_USER_ID` = $uid";
        if (!empty($key)) {
            $sql .= " AND `UF_KEY` = '$key'";
        }
        $res = $DB->Query($sql);
        while ($row = $res->fetch()) {
            if (!empty($key)) {
                $arResult = $row;
            } else {
                $arResult[$row["UF_KEY"]] = $row;
            }
        }
        return $arResult;
    }
    
    /** Проверяет первый ли раз после покупки пользователь зашел на страницу радара
     * Логика: Если у пользователя есть покупка после 01.10.2021 и кол-во заходов на страницу радара равно 1 - то он считается новым.
     * @param $uid
     * @return bool
     */
    public static function isNewRadarUser($uid)
    {
        if (CModule::IncludeModule("iblock")) {
            $arResult = false;
            $arSelect = array("ID", "NAME", "IBLOCK_ID");
            $arFilter = array("IBLOCK_ID"            => 11,
                              "ACTIVE"               => "Y",
                              "PROPERTY_USER"        => $uid,
                              ">=DATE_CREATE"        => '01-10-2021',
                              "PROPERTY_PAYED_VALUE" => 'Да'
            );
            $res = CIBlockElement::GetList(array("DATE_CREATE" => "DESC"), $arFilter, false, array("nTopCount" => 1),
              $arSelect);
            $payed = false;
            if ($ob = $res->fetch()) {
                $payed = true;
            }
        } else {
            $payed = false;
        }
        
        $arStat = self::getStat($uid, '/lk/obligations/');
        
        if ((count($arStat) && intval($arStat["UF_COUNTER"]) == 1) && $payed == true) {
            $arResult = true;
        } else {
            $arResult = false;
        }
        return $arResult;
    }
    
}

?>