<?/**
 * Класс для работы с кастомным содержимым для pop-up окон привязанных к баннерам
 *
 * @copyright 2020
 * @license   GNU
 * @version   1
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     23 class CCustomPopup
 *     27   function __construct()
 *     39   function getPopupsContentArray()
 *     69   function getContentById($elementId)
 *
 * TOTAL FUNCTIONS: 3
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */


class CCustomPopup {
  private $iblockId = 60;
  public $arContent = array();

	function __construct(){
		$this->getPopupsContentArray();
	}


/**
 * Формирует массив из активных элементов инфоблока, ключ по ID элемента
 *
 * @return array
 *
 * @access public
 */
public function getPopupsContentArray(){
	   $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
   	$cacheId = "custom_popup_iblock_60_data";
   	$cacheTtl = 86400 * 7;
          //$cache->clean("usa_actions_data");
   	if ($cache->read($cacheTtl, $cacheId)) {
               $arData = $cache->get($cacheId);
           } else {
           		CModule::IncludeModule("iblock");
					$arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_TEXT", "PREVIEW_TEXT", "PROPERTY_MAILCHIMP_LIST", "PROPERTY_MAILCHIMP_INTEREST");
					$arFilter = Array("IBLOCK_ID"=>IntVal($this->iblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
					while($ob = $res->Fetch()){
					 $arData[$ob["ID"]] = $ob;
					}
				$cache->set($cacheId, $arData);
           }
		$this->arContent = $arData;
}


/**
 * Возвращает массив с контентом отдельного элемента из общего массива по $elementId
 *
 * @param  int   $elementId Id элемента для которого нужно получить контент
 *
 * @return array
 *
 * @access public
 */
public function getContentById($elementId){
		$elementId = intval($elementId);
		if(count($this->arContent)>0 && array_key_exists($elementId,$this->arContent)){
		  $arResult = $this->arContent[$elementId];
		} else {
		  $arResult = array("error"=>"Содержимое не найдено");
		}
	 return $arResult;
}

}