<?php //Класс для работы сервиса "портфели"
//Версия с контроллером кеша с поддержкой итогов по дням.
//Так же расчет кеша для графика (желтый график) производится с учетом выборки итогов кеша по дням для указанного периода
	use Bitrix\Highloadblock as HL;
use Bitrix\Main\Application;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;

CModule::IncludeModule("highload");
	CModule::IncludeModule('highloadblock');
	//CModule::IncludeModule("iblock");
	Loader::includeModule('iblock');

	class CPortfolio {
		public $currentVersion              = '2.0';
		public $cacheOn			            = false;//Флаг разрешения использования кеширования. Устанавливается в дополнительных настройках "grain.customsettings", "PORTFOLIO_CACHE_ON"
		public $actions_iblockId            = 32; //id инфоблока с акциями
		public $portfolio_iblockId          = 52; //id инфоблока с портфелями
		public $Data_HL_Id                  = 27; //id HL инфоблока с данными портфелей
		public $hl_hist_Id                  = 28; //id HL инфоблока с историей портфелей
		public $hlHistName                  = 'portfolio_history'; //навание таблицы HL инфоблока с историей портфелей
		//public $spb_actions_HL_Id       		= 29; //id HL инфоблока с ценами акций США
		public $spb_actions_HL_Id       		= 53; //id HL инфоблока с ценами акций США
		//Имена таблиц MYSQL с ключами по id HL инфоблоков для прямых запросов за котировками
		public $hl_tables_prices				=array(24=>'hl_moex_actions_data',
															    25=>'hl_moex_obligations_data',
																 29=>'hl_spb_actions_data',
																 53=>'hl_polygon_actions_data',
																 31=>'hl_moex_currency_data');

		//public $listOfLogActions            = array("ADD", "UPDATE", "DELETE", "UNDO-EDIT", "UNDO-RESTORE");
		public $arHistoryActions            = array("ACT_PLUS" => "Покупка", "ACT_MINUS" => "Продажа", "CACHE_PLUS" => "Внесение", "CACHE_MINUS" => "Списание");
		public $arActiveTypes               = array("BOND", "SHARE");
		public $arRoubleCodes               = array("RUR", "RUB", "SUR", "rur", "rub", "sur"); //Массив кодов рублевой валюты
		public $arCurrencyCodesAdapter      = array("RUR"=>"RUB", "SUR"=>"RUB"); //Массив подмен кода валют (при загрузке из отчетов)
		public $portfolioLimit              = 40;
		public $excludePortfolioLimitsUsers = array();
		public $arKoeffDateList             = array(); //Список дат для подсчета коэффициентов портфелей
		public $resO, $resA, $resE, $resAU, $resCUR;
		public $analyseTableName			   = 'hl_portfolio_analyse_base';
		public $arSectorsColors 				= array(
															 "#339900",
															 "#FFCC00",
															 "#33CCCC",
															 "#FFCC99",
															 "#0000CC",
															 "#FF00FF",
															 "#6666CC",
															 "#009999",
															 "#66FFFF",
															 "#99FF66",
															 "#FFFF00",
															 "#990000",
															 "#006600",
															 "#CC9900",
															 "#FFCCFF",
															 "#CC6633",
															 "#FF0000",
															 "#FF6633",
															 "#CCFF66",
															 "#999966",
															 "#99CCCC",
															 "#CC99CC",
															);
		public $arMonthToKvartal = array( 1  => 1,
	                                     2  => 1,
	                                     3  => 1,
	                                     4  => 2,
	                                     5  => 2,
	                                     6  => 2,
	                                     7  => 3,
	                                     8  => 3,
	                                     9  => 3,
	                                     10 => 4,
	                                     11 => 4,
	                                     12 => 4
    	);
		public function __construct($loadClasses = false) {
			Global $APPLICATION;
			//Получаем id HL инфоблока с базой цен
			$this->spb_actions_HL_Id = $APPLICATION->usaPricesHlId;
			//Получаем список пользователей, для которых не нужно работать с кешем лимитов
			$arExludeLimitsUsers = \Bitrix\Main\Config\Option::get("grain.customsettings", "PORTFOLIO_NOCACHE_USER_LIMITS");
			$this->cacheOn = \Bitrix\Main\Config\Option::get("grain.customsettings", "PORTFOLIO_CACHE_ON")!='Y'?false:true;
			if (strpos($arExludeUsers, ",") !== false) {
				$this->excludePortfolioLimitsUsers = explode(",", $arExludeUsers);
			} else {
				$this->excludePortfolioLimitsUsers = array($arExludeLimitsUsers);
			}
			if ($loadClasses) {
				$this->init_radar_classes();
			}
		}

		public function init_radar_classes() {
			if (!$this->resO) {
				$this->resO   = new Obligations();
				$this->resA   = new Actions();
				$this->resE   = new ETF();
				$this->resAU  = new ActionsUsa();
				$this->resCUR = new CCurrencyRadar();
			}
		}

		/**
		 * Расчет текущей стоимости портфеля + расчет стартовой стоимости портфеля
		 *
		 * @param  [add type]   $pid (Optional) id портфеля
		 *
		 * @return array
		 *
		 * @access public
		 */
		public function calcPortfolio($pid, $cbr, $resA, $resO, $resE, $resAU) {
			//$portfolioActives = $this->getCalcPortfolioActivesList($pid, $resA, $resO, $resE, $resAU);
			$portfolioActives = $this->getPortfolioActivesList($pid, false, true, '', $resA, $resO, $resE, $resAU, true);

			$arPortfolio = array();

			$arResult              = array("START_SUMM" => 0, "CURR_SUMM" => 0);
			$currency_current_date = $this->getMonthEndWorkday(date('d.m.Y')); //Получаем ближайший рабочий день или текущий день, если он рабочий
			foreach ($portfolioActives as $aid => $aval) {
				$activeType = $aval['ROOT']['UF_ACTIVE_TYPE'];
				$radar_data = $aval['ROOT']['RADAR_DATA'];
				//$last_period = $radar_data["LAST_PERIOD"];
				$activeCurrencyId = strtoupper($aval['ROOT']['UF_CURRENCY']); //Валюта актива
				if ($activeType == 'bond' && !empty($aval['ROOT']['RADAR_DATA']['PROPS']['CURRENCYID'])) {
					$activeCurrencyId = $aval['ROOT']['RADAR_DATA']['PROPS']['CURRENCYID']; //Валюта актива
				}

				$cache    = Application::getInstance()->getManagedCache();
				$cacheId  = "calcPortfolioCurrencyRate" . md5($currency_current_date . $activeCurrencyId);
				$cacheTtl = 86400 * 365;
				//$cache->clean("usa_actions_data");
				if ($cache->read($cacheTtl, $cacheId)) {
					$currency_current_rate = $cache->get($cacheId);
				} else {
					$cbr->setClassDate($currency_current_date, true); //Не получаем коды редких валют
					if ($activeCurrencyId == 'SUR' || $activeCurrencyId == 'RUB') {
						$currency_current_rate = 1;
					} else {
						$currency_current_rate = $cbr->getRate(strtoupper($activeCurrencyId), false); //Получаем курс на ближайший рабочий или сегодняшний день
					}
					$cache->set($cacheId, $currency_current_rate);
				}

				$arPortfolio[$aid] = $aval['ROOT'];
				unset($arPortfolio[$aid]['ROOT']["RADAR_DATA"]);

				$arPortfolio[$aid]["currency_current_rate"] = $currency_current_rate;
				$price_real                                 = 0;
				if ($activeType == 'bond') { //Считаем текущую цену облигации принудительно
					$price_real = ($radar_data['PROPS']['FACEVALUE'] / 100) * (floatval($radar_data['PROPS']['LASTPRICE']) > 0 ? $radar_data['PROPS']['LASTPRICE'] : $radar_data['PROPS']['LEGALCLOSE']);

					if (floatval($radar_data['PROPS']['ACCRUEDINT']) > 0) { //НКД
						$price_real = $price_real + floatval($radar_data['PROPS']['ACCRUEDINT']);
					}
					$price_real = $price_real * $currency_current_rate;
				}

				$arPortfolio[$aid]["curr_exchange_price"] = ($activeType == 'bond' ? $price_real : (floatval($radar_data['DYNAM']['Цена лота']) > 0 ? floatval($radar_data['DYNAM']['Цена лота']) : 0));
				$currency                                 = strtoupper(trim($arPortfolio[$aid]["UF_CURRENCY"]));
				if ($currency == "RUB") {
					$arPortfolio[$aid]["currency_value"] = 1;
				} else {
					$arPortfolio[$aid]["currency_value"] = $currency_current_rate; //$cbr->getRate($currency, false);
				}

				$oblig_off = '';
				if ($activeType == 'bond') {
					$oblig_off = (new DateTime($radar_data['PROPS']['MATDATE'])) <= (new DateTime()) || $radar_data['PROPS']['HIDEN'] == 'Да' ? 'Y' : '';
				}

				if (empty($oblig_off)) {
					$arPortfolio[$aid] = $this->calcPortfolioRow($arPortfolio[$aid]);
					$arResult["START_SUMM"] += $arPortfolio[$aid]["portfolio_elem_buy_price_final"];
					$arResult["CURR_SUMM"] += $arPortfolio[$aid]["portfolio_elem_buy_price_current"];
				} else {
					unset($arPortfolio[$aid]);
				}
			}

			unset($arPortfolio, $radar_data);

			return $arResult;
		}

		//Расчет сумм по строке портфеля для статистики
		public function calcPortfolioRow($row) {
			$price               = $row["UF_LOTPRICE"];
			$number              = $row["UF_LOTCNT"];
			$curr_exchange_price = $row["curr_exchange_price"];
			$price               = $row["UF_LOTPRICE"];
			$prognose_income     = 0;
			$price_final         = 0;
			$curr_price_final    = 0;

			$price       = str_replace(' ', '', $price);
			$price       = floatval($price);
			$number      = str_replace(' ', '', $number);
			$number      = intval($number);
			$price_final = $price * $number;

			$row["portfolio_elem_buy_price_final"] = $price_final;
			$currency_value                        = $row["currency_value"];

			if ($row['UF_ACTIVE_TYPE'] != 'bond') {
				$curr_price_final = $curr_exchange_price * $currency_value * $number;
			} else {
				$curr_price_final = $curr_exchange_price * $number;
			}
			$row["portfolio_elem_buy_price_current"] = $curr_price_final;
			return $row;
		}

		//Пересчет сумм вложенных и текущих стоимостей портфеля
		public function calcStatSumm($pid){
		  Global $DB;
		  $arStatSumm = array();
		  $orangeIncomeSumm = 0;
		  $greenDealsSumm = 0;
		  //$query = "SELECT * FROM `portfolio_history` WHERE `UF_PORTFOLIO` = $pid  ORDER BY `UF_ADD_DATE` ASC";
		  //$resSql = $DB->Query($query);
		  $arDeals = array();
/*		  while($row = $resSql->fetch()){
			$arDeals[] = $row;
		  }*/

		  $arDeals = $this->getPortfolioActivesList($pid, false);

		  $kdate = (new DateTime())->format('d.m.Y');
		  foreach($arDeals as $arHistActive){
					/*+ Расчет ntreotq стоимости */
					if ($arHistActive["UF_ACTIVE_TYPE"] == 'share' || $arHistActive["UF_ACTIVE_TYPE"] == 'share_usa' || $arHistActive["UF_ACTIVE_TYPE"] == 'bond' || $arHistActive["UF_ACTIVE_TYPE"] == 'share_etf' || $arHistActive["UF_ACTIVE_TYPE"] == 'currency') {
						if (intval($arHistActive["UF_INLOT_CNT"]) == 0) {
							$resATmp                      = $arHistActive["UF_ACTIVE_TYPE"] != 'currency' ? $this->resA : $this->resCUR;
							$arActive                     = $resATmp->getItem($arElement["UF_ACTIVE_CODE"]);
							$arHistActive["UF_INLOT_CNT"] = $arActive["PROPS"]["LOTSIZE"];
							unset($arActive, $resATmp);
						}
						if (!array_key_exists($arHistActive["UF_SECID"], $arPrices)) {
							$price                               = $this->getHistActionsPrices($arHistActive["UF_ACTIVE_TYPE"], $kdate, $arHistActive["UF_SECID"], '',$arHistActive["UF_CURRENCY"]);
							$arPrices[$arHistActive["UF_SECID"]] = $price;
							$arResult[$kdate]["REAL_PRICES"][]   = array("UF_ACTIVE_TYPE" => $arHistActive["UF_ACTIVE_TYPE"], "UF_SECID" => $arHistActive["UF_SECID"], "PRICE" => $price);
						} else {
							$price = $arPrices[$arHistActive["UF_SECID"]];
						}
					/*- Расчет зеленого стоимости */
				$greenDealsSumm += ($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]) * floatval($price["price"]);
		  }
		  }

				 $arStatSumm["invest_summ"] = reset($arDeals)["FOOTER_INCOME_SUMM"];
				 $arStatSumm["current_summ"] = $greenDealsSumm;
				 $arStatSumm["portfolioId"] = $pid;
				 return $arStatSumm;
		}


		//Записывает суммы портфеля в инфоблок
		public function updatePortfolioStatSumm($arData) {
			$arData['invest_summ'] = floatval($arData['invest_summ']) > 0 ? floatval($arData['invest_summ']): 0;
			$arData['current_summ'] = floatval($arData['current_summ']) > 0 ? floatval($arData['current_summ']): 0;
			//if (floatval($arData['invest_summ']) > 0) {
				CIBlockElement::SetPropertyValuesEx($arData['portfolioId'], 52, array("INVEST_SUMM" => $arData['invest_summ']));
				CIBlockElement::SetPropertyValuesEx($arData['portfolioId'], 52, array("CURRENT_SUMM" => $arData['current_summ']));
			//}
		}

		//Читает суммы портфеля из инфоблока
		public function getPortfolioStatSumm($pid) {
			$pid = intval($pid);
			$arResult = array("INVEST_SUMM"=>0, "CURRENT_SUMM"=>0);
			if ($pid > 0) {
			   $arSelect = array("ID",  "PROPERTY_INVEST_SUMM", "PROPERTY_CURRENT_SUMM");
				$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$this->portfolio_iblockId, "ID"=>$pid), false, array(), $arSelect);

				if($row = $res->fetch()){
				 $arResult["INVEST_SUMM"] = floatval($row["PROPERTY_INVEST_SUMM_VALUE"]);
				 $arResult["CURRENT_SUMM"] = floatval($row["PROPERTY_CURRENT_SUMM_VALUE"]);
				}
			}
			return $arResult;
		}

		/**
		 * Возвращает массив с ключами из тикеров акций, etf и значнием из кол-ва вхождений данных бумаг в портфели пользователей
		 * Выполняется на кроне в вечерний пересчет акций c $setActionsCnt = true
		 *
		 * @param  boolean   $setActionsCnt (Optional) - Записывать активам кол-во вхождений в портфель
		 *
		 * @return array
		 *
		 * @access public
		 */
		public function getActionsCntInPortfolios($setActionsCnt = false) {
			//Todo Переделать на пересчет вхождений из HL 28
			//$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hl_deals_id)->fetch();
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hl_hist_Id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter = array('UF_ACTIVE_TYPE' => array('share', 'share_etf'));

			$arAllPortfolioList = $this->getPortfolioListAll();

			$rsData = $entity_data_class::getList(array(
				'order' => array('UF_ACTIVE_ID' => 'ASC'),
				'select' => array('ID', 'UF_ACTIVE_ID', 'UF_ACTIVE_NAME', 'UF_ACTIVE_CODE', 'UF_PORTFOLIO'),
				'filter' => $arFilter,
				'group' => array('UF_ACTIVE_ID')
			));
			$arResult = array();
			while ($el = $rsData->fetch()) {
				if(empty($el["UF_PORTFOLIO"])) continue;
				if (!array_key_exists($el["UF_PORTFOLIO"], $arResult[$el["UF_ACTIVE_ID"]]["PORTFOLIO"])) {
					$arResult[$el["UF_ACTIVE_ID"]]["CNT"] += 1;
					$arResult[$el["UF_ACTIVE_ID"]]["PORTFOLIO"][$el["UF_PORTFOLIO"]] = 1;
				}

				$portfolioOwner = $arAllPortfolioList[$el["UF_PORTFOLIO"]]["PROPERTIES"]["OWNER_USER"]["VALUE"];
				if (!array_key_exists($portfolioOwner, $arResult[$el["UF_ACTIVE_ID"]]["USERS"])) {
					$arResult[$el["UF_ACTIVE_ID"]]["USERS"][$portfolioOwner] = 1;
					$arResult[$el["UF_ACTIVE_ID"]]["CNT_USERS"] += 1;
				}
				if (!array_key_exists($el["UF_ACTIVE_ID"], $arResult)) {
					$arResult[$el["UF_ACTIVE_ID"]]["ITEM"] = $el;
				}
			}

			foreach ($arResult as $k => $v) {
				unset($arResult[$k]["PORTFOLIO"]);
				if ($setActionsCnt == true) {
					CIBlockElement::SetPropertyValuesEx($k, 32, array("CNT_IN_PORTFOLIO" => $v["CNT"]));
				}

				CIBlockElement::SetPropertyValuesEx($k, 32, array("CNT_IN_PORTFOLIO_USERS" => $v["CNT_USERS"]));
			}

			return $arResult;
		}

		/**
		 * Возвращает массив с ключами из тикеров акций США и значнием из кол-ва вхождений данной акции в портфели пользователей
		 * Выполняется на кроне в ночной пересчет акций c $setActionsCnt = true
		 *
		 * @param  boolean   $setActionsCnt (Optional) - Записывать активам кол-во вхождений в портфель
		 *
		 * @return array
		 *
		 * @access public
		 */
		public function getActionsUsaCntInPortfolios($setActionsCnt = false) {
			//Todo Переделать на пересчет вхождений из HL 28
			//$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hl_deals_id)->fetch();
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hl_hist_Id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter = array('UF_ACTIVE_TYPE' => array('share_usa'));

			$arAllPortfolioList = $this->getPortfolioListAll();

			$rsData = $entity_data_class::getList(array(
				'order' => array('UF_ACTIVE_ID' => 'ASC'),
				'select' => array('ID', 'UF_ACTIVE_ID', 'UF_ACTIVE_NAME', 'UF_ACTIVE_CODE', 'UF_PORTFOLIO'),
				'filter' => $arFilter,
				'group' => array('UF_ACTIVE_ID')
			));
			$arResult = array();
			while ($el = $rsData->fetch()) {
				if (!array_key_exists($el["UF_PORTFOLIO"], $arResult[$el["UF_ACTIVE_ID"]]["PORTFOLIO"])) {
					$arResult[$el["UF_ACTIVE_ID"]]["CNT"] += 1;
					$arResult[$el["UF_ACTIVE_ID"]]["PORTFOLIO"][$el["UF_PORTFOLIO"]] = 1;
				}

				$portfolioOwner = $arAllPortfolioList[$el["UF_PORTFOLIO"]]["PROPERTIES"]["OWNER_USER"]["VALUE"];
				if (!array_key_exists($portfolioOwner, $arResult[$el["UF_ACTIVE_ID"]]["USERS"])) {
					$arResult[$el["UF_ACTIVE_ID"]]["USERS"][$portfolioOwner] = 1;
					$arResult[$el["UF_ACTIVE_ID"]]["CNT_USERS"] += 1;
				}

				if (!array_key_exists($el["UF_ACTIVE_ID"], $arResult)) {
					$arResult[$el["UF_ACTIVE_ID"]]["ITEM"] = $el;
				}
			}

			foreach ($arResult as $k => $v) {
				unset($arResult[$k]["PORTFOLIO"]);
				if ($setActionsCnt == true) {
					CIBlockElement::SetPropertyValuesEx($k, 55, array("CNT_IN_PORTFOLIO" => $v["CNT"]));
				}

				CIBlockElement::SetPropertyValuesEx($k, 55, array("CNT_IN_PORTFOLIO_USERS" => $v["CNT_USERS"]));
			}

			return $arResult;
		}

		/**
		 * Возвращает массив с ключами из тикеров облигаций и значнием из кол-ва вхождений данной облигации в портфели пользователей
		 * Выполняется на кроне в ночной пересчет акций c $setBondCnt = true
		 *
		 * @param  boolean   $setActionsCnt (Optional) - Записывать активам кол-во вхождений в портфель
		 *
		 * @return array
		 *
		 * @access public
		 */
		public function getObligationsCntInPortfolios($setBondCnt = false) {
			//$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hl_hist_Id)->fetch();
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hl_hist_Id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter = array('UF_ACTIVE_TYPE' => array('bond'));

			$arAllPortfolioList = $this->getPortfolioListAll();

			$rsData = $entity_data_class::getList(array(
				'order' => array('UF_ACTIVE_ID' => 'ASC'),
				'select' => array('ID', 'UF_ACTIVE_ID', 'UF_ACTIVE_NAME', 'UF_ACTIVE_CODE', 'UF_PORTFOLIO'),
				'filter' => $arFilter,
				'group' => array('UF_ACTIVE_ID')
			));
			$arResult = array();
			while ($el = $rsData->fetch()) {
				if (!array_key_exists($el["UF_PORTFOLIO"], $arResult[$el["UF_ACTIVE_ID"]]["PORTFOLIO"])) {
					$arResult[$el["UF_ACTIVE_ID"]]["CNT"] += 1;
					$arResult[$el["UF_ACTIVE_ID"]]["PORTFOLIO"][$el["UF_PORTFOLIO"]] = 1;
				}

				$portfolioOwner = $arAllPortfolioList[$el["UF_PORTFOLIO"]]["PROPERTIES"]["OWNER_USER"]["VALUE"];
				if (!array_key_exists($portfolioOwner, $arResult[$el["UF_ACTIVE_ID"]]["USERS"])) {
					$arResult[$el["UF_ACTIVE_ID"]]["USERS"][$portfolioOwner] = 1;
					$arResult[$el["UF_ACTIVE_ID"]]["CNT_USERS"] += 1;
				}

				if (!array_key_exists($el["UF_ACTIVE_ID"], $arResult)) {
					$arResult[$el["UF_ACTIVE_ID"]]["ITEM"] = $el;
				}
			}

			foreach ($arResult as $k => $v) {
				unset($arResult[$k]["PORTFOLIO"]);
				if ($setBondCnt == true && $k > 0) {
					CIBlockElement::SetPropertyValuesEx($k, 27, array("CNT_IN_PORTFOLIO" => $v["CNT"]));
				}

				CIBlockElement::SetPropertyValuesEx($k, 27, array("CNT_IN_PORTFOLIO_USERS" => $v["CNT_USERS"]));
			}

			return $arResult;
		}

		/**
		 * Возвращает разрешение на кеширование лимитов портфелей для указанного пользователя
		 *
		 * @param  [add type]   $uid
		 *
		 * @return [add type]
		 *
		 * @access public
		 */
		public function checkUseCacheLimitsUser($uid) {
			$result = true;
			if (in_array($uid, $this->excludePortfolioLimitsUsers)) {
				$result = false;
			}
			return $result;
		}

		/**
		 * Очищает кеш проверки кол-ва портфелей для пользователя
		 *
		 * @param  int   $uid  id пользователя битрикс
		 *
		 * @access public
		 */
		public function clearCachePortfolioCntForUser($uid) {
			$this->clearPortfolioListForUserCache($uid); //Сбрасываем кеш списка портфелей для пользователя
			$this->clearPortfolioListAllCache($uid); //Сбрасываем кеш списка портфелей для пользователя
			//Если не используем кеширование лимитов портфелей для пользователя - выходим.
			//ТАких пользователей можно задать в настройках https://fin-plan.org/bitrix/admin/gcustomsettings.php?lang=ru&mess=ok&tabControl_active_tab=edit6

			if ($this->checkUseCacheLimitsUser($uid) == false) {return;}

			$cache = Application::getInstance()->getManagedCache();
			$cache->clean("checkPortfolio2.1CntForUser" . $uid);
		}

		/**
		 * Возвращает список валют доступных к внесению в портфель
		 *
		 * @return array  Массив валют из инфоблока
		 *
		 * @access public
		 */
		public function getCacheCurrencyList() {
			$cache    = Application::getInstance()->getManagedCache();
			$cacheId  = "base_currency_list" . (new DateTime())->format('d.m.Y');
			$cacheTtl = 86400 * 2;
			//$cache->clean("base_currency_list");
			if ($cache->read($cacheTtl, $cacheId)) {
				$arResult = $cache->get($cacheId);
			} else {
				$arSelect = Array("ID", "NAME", "CODE", "DETAIL_PAGE_URL", "PROPERTY_NAME", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_LOTSIZE", "PROPERTY_LASTPRICE");
				$arFilter = Array("IBLOCK_ID" => 54, "PROPERTY_BASE" => "Y", "ACTIVE" => "Y");
				$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				$arResult = array();
				while ($ob = $res->Fetch()) {
					$arResult[$ob["PROPERTY_SECID_VALUE"]] = array("NAME" => $ob["NAME"], "CODE" => $ob["CODE"], "FULLNAME" => $ob["PROPERTY_NAME_VALUE"],
						"ID" => $ob["ID"], "DETAIL_PAGE_URL" => "/lk/currency/" . $ob["CODE"] . "/",
						"LOTSIZE" => $ob["PROPERTY_LOTSIZE_VALUE"],
						"LASTPRICE" => $ob["PROPERTY_LASTPRICE_VALUE"],
					);
				}$cache->set($cacheId, $arResult);
			}

			return $arResult;
		}

		//Получает курс валюты на текущий или предыдущий рабочий день, если запрашиваемая дата приходится на выходной
		public function getCurrencyRate($currencyId = 'RUB', $date = '') {
			if (empty($date)) {
				$date = (new DateTime(date()))->format('d.m.Y');
			}
			$currencyId            = strtoupper($currencyId);

			$currency_current_date = $this->getMonthEndWorkday($date); //Получаем ближайший рабочий день или текущий день, если он рабочий
			$currency_current_rate = getCBPrice(($currencyId == 'SUR' ? 'RUB' : $currencyId), $currency_current_date); //Получаем курс на ближайший рабочий или сегодняшний день
			return floatVal($currency_current_rate);
		}

		//Получает список индексов для списка выбора бенчмарков в бек-тесте
		public function getBenchmarkIndexesList() {
			//TODO Кешировать со сбросом при изменении в инфоблоке
			$arSelect = Array("ID", "NAME", "PROPERTY_NAME", "IBLOCK_ID", "PROPERTY_SECID");
			$arFilter = Array("IBLOCK_ID" => IntVal(53), "PROPERTY_BACKTEST_USE_VALUE" => "Y", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$arResult = array();
			while ($ob = $res->Fetch()) {
				$arResult[$ob["PROPERTY_SECID_VALUE"]] = array("NAME" => $ob["NAME"], "FULLNAME" => $ob["PROPERTY_NAME_VALUE"], "ID" => $ob["ID"]);
			}

			return $arResult;
		}

		//Получает список ETF для списка выбора бенчмарков в бек-тесте
		public function getBenchmarkETFList() {
			//TODO Кешировать со сбросом при изменении в инфоблоке
			$arSelect = Array("ID", "NAME", "PROPERTY_NAME", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_ETF_CURRENCY", "PROPERTY_BACKTEST_NAME");
			$arFilter = Array("IBLOCK_ID" => IntVal(32), "PROPERTY_BACKTEST_USE_VALUE" => "Y", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$arResult = array();
			while ($ob = $res->Fetch()) {
				if (!empty($ob["PROPERTY_BACKTEST_NAME_VALUE"])) {
					$name = $ob["PROPERTY_BACKTEST_NAME_VALUE"];
				} else {
					$name = $ob["NAME"];
				}
				$arResult[$ob["PROPERTY_SECID_VALUE"]] = array("NAME" => $name, "FULLNAME" => $name, "ID" => $ob["ID"], "CURRENCY" => $ob["PROPERTY_ETF_CURRENCY_VALUE"]);
			}

			return $arResult;
		}

		//Получает список валют для списка выбора бенчмарков в бек-тесте
		public function getBenchmarkCurrencyList() {
			//TODO Кешировать со сбросом при изменении в инфоблоке
			$arSelect = Array("ID", "NAME", "PROPERTY_NAME", "IBLOCK_ID", "PROPERTY_SECID", "PROPERTY_SECNAME", "PROPERTY_BACKTEST_NAME");
			$arFilter = Array("IBLOCK_ID" => IntVal(54), "PROPERTY_BACKTEST_USE_VALUE" => "Y", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$arResult = array();
			while ($ob = $res->Fetch()) {
				if (!empty($ob["PROPERTY_BACKTEST_NAME_VALUE"])) {
					$name = $ob["PROPERTY_BACKTEST_NAME_VALUE"];
				} else {
					$name = $ob["PROPERTY_SECNAME_VALUE"];
				}
				$arResult[$ob["PROPERTY_SECID_VALUE"]] = array("NAME" => $name, "FULLNAME" => $name, "ID" => $ob["ID"]);
			}

			return $arResult;
		}

		//Получает ID элемента акции или облигации по массиву с кодами или кодами бумаг
		public function getActivesIdByCodeOrSECID($arCodes) {
			$arSelect = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_SECID");
			$arFilter = array("LOGIC" => "OR",
				array("PROPERTY_SECID" => array_keys($arCodes), "IBLOCK_ID" => 27), //Облигации
				array("CODE" => array_keys($arCodes), "IBLOCK_ID" => 32), //Акции
				array("CODE" => array_keys($arCodes), "IBLOCK_ID" => 55) //Акции США
			);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->Fetch()) {
				if (array_key_exists($ob["CODE"], $arCodes)) {
					$arCodes[$ob["CODE"]] = $ob["ID"];
				}
				if (array_key_exists($ob["PROPERTY_SECID_VALUE"], $arCodes)) {
					$arCodes[$ob["PROPERTY_SECID_VALUE"]] = $ob["ID"];
				}
			}
			return $arCodes;
		}

		public function getBacktestActiveMinDate($pid) {
		}

		/**
		 * Возвращает диапазон месяцев от заданной даты (текущая) до такой же даты n-лет назад, помесячно
		 *
		 * @param  int   $curYear  текущий год
		 * @param  int   $curMonth (Optional) текущий месяц
		 * @param  int   $backYears (Optional) количество лет для отсчета в обратную сторону
		 *
		 * @return array массив
		 *
		 * @access public
		 */
		public function getBacktestDateList($curYear, $curMonth = '01', $backYears = 2) {
			$arResult = array();
			$b_months = 0;
			$b_years  = $backYears;
			if (is_float($backYears)) {
				$arBackYears = explode('.', $backYears);
				$b_years     = intval($arBackYears[0]);
				$b_months    = intval($arBackYears[1]);
			}
			for ($cnt_m = $b_years * 12 + $b_months; $cnt_m >= 0; $cnt_m--) {
				$key                             = (new DateTime(($curYear) . '-' . $curMonth . '-01 -' . $cnt_m . 'month'));
				$key                             = (new DateTime($this->getMonthFirstWorkday($key->format('d.m.Y'))));
				$arResult[$key->format('d.m.Y')] = array();
			}

			$arResult[(new DateTime(date()))->format('d.m.Y')] = 0; //Добавим текущую дату

			return $arResult;
		}

		/**
		 * Возвращает диапазон дат от заданной даты (текущая) до такой же даты n-лет назад, по дням
		 *
		 * @param  int   $curYear  текущий год
		 * @param  int   $curMonth (Optional) текущий месяц
		 * @param  int   $backYears (Optional) количество лет для отсчета в обратную сторону
		 *
		 * @return array массив
		 *
		 * @access public
		 */
		public function getKoeffDateList($curDate, $backYears = 5) {
			$arResult = array();

			$cDate = new DateTime($curDate);
			$bDate = clone $cDate;
			$bDate->modify('-' . $backYears . ' years');

			while ($bDate <= $cDate) {
				$arResult[$bDate->format('d.m.Y')] = 0;
				$bDate->modify('+1 days');
			}

			return $arResult;
		}

		/**
		 * Возвращает кол-во актива на конкретную дату (по принципу количественного регистра накопления в 1С
		 *
		 * @param  int   $pid   ID портфеля
		 * @param  string   $date  строка с датой для получения среза количества, вида '01.12.2019'
		 * @param  int   $activeId (Optional)  ID Актива, если требуется фильтрация по конкретному активу
		 * @param  int   $activeCode (Optional)  ISIN код Актива, если требуется фильтрация по конкретному активу
		 * @param  bool   $forHistory (Optional)  Переключает выборку на поле с датой добавления для true и на дату движения если false (по умолчанию)
		 *
		 * @return array массив с ключами в виде ID активов и значениями в виде найденного количества на запрошенную дату
		 *
		 * @access public
		 */
		public function getActiveCountByDate($pid, $date, $activeId = false, $useSECID_KeysInResult = true, $shares_only = true, $activeCode = false, $forHistory = false) {
			$hlblock_id        = 28;
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$pid       = str_replace("portfolio_", "", $pid);
			$arFilter          = array('UF_PORTFOLIO' => $pid);
			if ($forHistory != false) {
				$arFilter["<=UF_ADD_DATE"] = (new DateTime($date))->format('d.m.Y 23:59:59');
			} else {
				$arFilter["<UF_HISTORY_DATA"] = (new DateTime($date))->format('d.m.Y 23:59:59');
			}

			if ($activeId != false) {
				$arFilter["UF_ACTIVE_ID"] = $activeId;
			}
			if ($activeCode != false) {
				$arFilter["UF_ACTIVE_CODE"] = $activeCode;
			}
			if ($shares_only == true) {
				$arFilter["UF_ACTIVE_TYPE"] = array("share", "share_etf", "share_usa");
			}

			$rsData = $entity_data_class::getList(array(
				'order' => array('UF_HISTORY_DATA' => 'ASC'),
				'select' => array('*'),
				'filter' => $arFilter,
				'group' => array('UF_HISTORY_DIRECTION')
			));

			$arMidPriceTmp        = array();
			$arActivesPricesTable = array();
			$arResult             = array();
			while ($el = $rsData->fetch()) {
				if ($useSECID_KeysInResult == true) {
					$elKey = $el["UF_SECID"];
					if ($forHistory) {
						$elKey = $el["UF_ACTIVE_CODE"];
					}
				} else {
					$elKey = $el["UF_ACTIVE_ID"];
				}

				if (empty($arResult["CURRENCY"][$elKey]) && !empty($el["UF_CURRENCY"])) {
					$arResult["CURRENCY"][$elKey] = $el["UF_CURRENCY"];
				}
				if (empty($arResult["TYPE"][$elKey]) && !empty($el["UF_ACTIVE_TYPE"])) {
					$arResult["TYPE"][$elKey] = $el["UF_ACTIVE_TYPE"];
				}
				if ($el["UF_HISTORY_DIRECTION"] == "MINUS") {
					$arResult["COUNT"][$elKey] = $arResult["COUNT"][$elKey] - $el["UF_HISTORY_CNT"];
				}
				if ($el["UF_HISTORY_DIRECTION"] == "PLUS") {
					$arResult["COUNT"][$elKey] = $arResult["COUNT"][$elKey] + $el["UF_HISTORY_CNT"];
				}
			}

			//Убираем активы с нулевм кол-вом, что бы не обрезался график
			$tmpCnt = array();
			foreach ($arResult["COUNT"] as $key => $val) {
				if (intval($val) > 0) {
					$tmpCnt[$key] = $val;
				}
			}
			if (count($tmpCnt) > 0) {
				$arResult["COUNT"] = $tmpCnt;
			}
			//    define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/backtest_etf_log.txt");
			//      AddMessage2Log('$arResult = '.print_r($arResult, true),'');
			// $firephp = FirePHP::getInstance(true);
			// $firephp->fb(array("filter"=>$arFilter, "el"=>$arResult),FirePHP::LOG);
			//           $firephp = FirePHP::getInstance(true);
			//           $firephp->fb(array("date"=>$date, "arResult"=>$arResult),FirePHP::LOG);

			return $arResult;
		}

		public function getActiveCountOnEventDate($pid, $activeISIN, $date) {
			$arResult                     = 0;
			$pid                          = str_replace("portfolio_", "", $pid);
			$hlblock_id                   = 28;
			$hlblock                      = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity                       = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class            = $entity->getDataClass();
			$arFilter                     = array('UF_PORTFOLIO' => $pid);
			$arFilter["<=UF_ADD_DATE"]     = (new DateTime($date))->format('d.m.Y 23:59:59');
			$arFilter["!=UF_ACTIVE_TYPE"] = "currency";
			$arFilter["=UF_ACTIVE_CODE"]  = $activeISIN;
			$rsData                       = $entity_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC'),
				'select' => array('*'),
				'filter' => $arFilter,
				'group' => array('UF_HISTORY_DIRECTION')
			));
			$arTmpResult = array();
			while ($el = $rsData->fetch()) {
				$elKey = $el["UF_ACTIVE_CODE"];

				if ($el["UF_HISTORY_DIRECTION"] == "MINUS") {
					$arResult -= $el["UF_HISTORY_CNT"];
				}
				if ($el["UF_HISTORY_DIRECTION"] == "PLUS") {
					$arResult += $el["UF_HISTORY_CNT"];
				}
			}

			return $arResult;
		}


		/**
		 * Возвращает количества активов в портфеле на даты дивидендов/купонов переданные в параметр $arDates
		 *
		 * @param  string   $pid id Портфеля
		 * @param  array   $arDates массив дат для группировки кол-ва актива arDates[isin] = array('dates');
		 *
		 * @return array  Массив количеств активов формата: arResult[activeISIN][date] = cnt
		 *
		 * @access public
		 */
		public function getActiveCountOnEventMultiDate($pid, $arDates) {
			$arResult                     = array();
			$pid                          = str_replace("portfolio_", "", $pid);
			$hlblock_id                   = 28;
			$hlblock                      = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity                       = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class            = $entity->getDataClass();
			$arFilter                     = array('UF_PORTFOLIO' => $pid);
			//$arFilter["<=UF_ADD_DATE"]     = (new DateTime(reset($arDates)))->format('d.m.Y 23:59:59');
			$arFilter["!=UF_ACTIVE_TYPE"] = "currency";
			$arFilter["=UF_ACTIVE_CODE"]  = array_keys($arDates);
			$rsData                       = $entity_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC', 'UF_ACTIVE_CODE'=>'ASC'),
				'select' => array("UF_ACTIVE_CODE", "UF_ADD_DATE", "UF_HISTORY_CNT", "UF_HISTORY_DIRECTION"),
				'filter' => $arFilter,
				//'group' => array('UF_HISTORY_DIRECTION')
			));
			$arTmpResult = array();
			while ($el = $rsData->fetch()) {
				$arTmpResult[$el["UF_ACTIVE_CODE"]][] = array("UF_ADD_DATE"=>$el["UF_ADD_DATE"], "UF_HISTORY_CNT"=>$el["UF_HISTORY_CNT"], "UF_HISTORY_DIRECTION"=>$el["UF_HISTORY_DIRECTION"]);
			}
  //$firephp = FirePHP::getInstance(true);


			foreach($arDates as $arDatesIsin => $arDates){//Обходим активы

			  foreach($arDates as $eventDate){ //Обходим даты событий для текущего актива
				foreach($arTmpResult[$arDatesIsin] as $k=>$el){//Считаем остаток на дату события
					if ($el["UF_HISTORY_DIRECTION"] == "MINUS") {
						if((new DateTime($el["UF_ADD_DATE"]))->getTimestamp()<=(new DateTime($eventDate))->getTimestamp())
						$arResult[$arDatesIsin][$eventDate] = $arResult[$arDatesIsin][$eventDate] - $el["UF_HISTORY_CNT"];
  //$firephp->fb(array("getActiveCountOnEventMultiDate"=>"", "minus"=>$el["UF_HISTORY_CNT"]),FirePHP::LOG);
					}
					if ($el["UF_HISTORY_DIRECTION"] == "PLUS") {
			  			if((new DateTime($el["UF_ADD_DATE"]))->getTimestamp()<=(new DateTime($eventDate))->getTimestamp())
							$arResult[$arDatesIsin][$eventDate] = $arResult[$arDatesIsin][$eventDate] + $el["UF_HISTORY_CNT"];
  //$firephp->fb(array("getActiveCountOnEventMultiDate"=>$arDatesIsin, "dt"=>$eventDate, "plus"=>$el["UF_HISTORY_CNT"]),FirePHP::LOG);
					}
				}

			  }

			}
  //$firephp->fb(array("getActiveCountOnEventMultiDate result"=>$arResult),FirePHP::LOG);
			return $arResult;
		}

		//Определяет первый рабочий день месяца по дате первого дня месяца
		public function getMonthFirstWorkday($date) {
			$dt = new DateTime($date);

			if ($dt->format("N") == 7) {
				$dt->modify("+1 days");
			} elseif ($dt->format("N") == 6) {
				$dt->modify("+2 days");
			}

			if (isWeekEndDay($dt->format("d.m.Y"))) {
				$finded = false;
				while (!$finded) {
					$dt->modify("+1 days");
					if (!isWeekEndDay($dt->format("d.m.Y"))) {
						$finded = true;
					}
				}
			}

			return $dt->format("d.m.Y");
		}

		//Определяет первый рабочий день месяца по дате первого дня месяца для США
		public function getMonthFirstWorkdayUsa($date) {
			$dt = new DateTime($date);

			if ($dt->format("N") == 7) {
				$dt->modify("+1 days");
			} elseif ($dt->format("N") == 6) {
				$dt->modify("+2 days");
			}

			if (isUsaWeekendDay($dt->format("d.m.Y"))) {
				$finded = false;
				while (!$finded) {
					$dt->modify("+1 days");
					if (!isUsaWeekendDay($dt->format("d.m.Y"))) {
						$finded = true;
					}
				}
			}

			return $dt->format("d.m.Y");
		}

		//Определяет последний рабочий день месяца по дате последнего дня месяца
		public function getMonthEndWorkday($date) {
			$dt = new DateTime($date);

			if ($dt->format("N") == 7) {
				$dt->modify("-2 days");
			} elseif ($dt->format("N") == 6) {
				$dt->modify("-1 days");
			}

			if (isWeekEndDay($dt->format("d.m.Y"))) {
				$finded = false;
				while (!$finded) {
					$dt->modify("-1 days");
					if (!isWeekEndDay($dt->format("d.m.Y"))) {
						$finded = true;
					}
				}
			}

			return $dt->format("d.m.Y");
		}

		/**
		 * Возвращает количество акций в лоте для массива акций по кодам бумаг - SECID
		 *
		 * @param  array   $arActionsSECID  массив коодов бумаг SECID
		 *
		 * @return array массив с ключами по кодам бумаг SECID и значениями - количествами акций в лоте
		 *
		 * @access public
		 */
		public function getActionsLotsizes($arActionsSECID) {
			$arResult = array();
			$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_LOTSIZE", "PROPERTY_SECID");
			$arFilter = Array("IBLOCK_ID" => 32, "PROPERTY_SECID" => $arActionsSECID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

			while ($ob = $res->Fetch()) {
				$arResult[$ob["PROPERTY_SECID_VALUE"]] = $ob["PROPERTY_LOTSIZE_VALUE"];
			}

			return $arResult;
		}

		/**
		 * Получает список цен всех акций по указанным датам на каждый день
		 *
		 * @param  array $queryDates Массив дат по каждому дню для запрашиваемого диапазона лет
		 * @param  array $SECID Массив кодов активов. В фильтр разных HL инфоблоков идет одинаковый массив, т.к., например в акциях не может оказаться код облигации и наоборот
		 *
		 * @return array  $arResult['01.10.2019'][Код актива] = цена на каждый рабочий день;
		 *
		 * @access public
		 */
		public function getHLActivesPricesEveryday($queryDates, $SECID = array(), $arActTypes, $arActCurrency) {
			Global $DB;
			$arResult = array();
			//Делаем массив первых рабочих дней месяцев для фильтра
			$arWorkDates = array();
			$arWorkDatesSql = array(); //Для запросов sql
			//$arWorkDates01 = array();
			$cur_time = intval(date('Hi'));
			$prev_day = (new DateTime(date()))->modify('-1 days');
			foreach ($queryDates as $qDate) {
				if ($qDate == (new DateTime(date()))->format('d.m.Y') && $cur_time < 1930) {
					$arWorkDates[] = $prev_day->format('d.m.Y');
					$arWorkDatesSql[] = "'".$prev_day->format('Y-m-d')."'";
					//$arWorkDates01[] = $prev_day->modify('+1 days')->format('d.m.Y');
				} else {
					$arWorkDates[] = $qDate;
					$arWorkDatesSql[] = "'".(new DateTime($qDate))->format('Y-m-d')."'";
					//$arWorkDates01[] = (new DateTime($qDate))->format('01.m.Y');
				}
			}

			$SECID_actions_etf = array();
			$SECID_usa         = array();
			$SECID_usaSql         = array();
			foreach ($SECID as $item_secid) {
				if ($arActTypes[$item_secid] == 'share_etf' || $arActTypes[$item_secid] == 'share') {
					$SECID_actions_etf[] = $item_secid;
				} else if ($arActTypes[$item_secid] == 'share_usa') {
					$SECID_usa[] = $item_secid;
					$SECID_usaSql[] = "'".$item_secid."'";
				}
			}

			//Получаем кол-ва в лоте для акций
			$arActions = $this->getActionsLotsizes($SECID_actions_etf);
			if (count($SECID_usa) > 0) {
				foreach ($SECID_usa as $item_secid_usa) {
					$arActions[$item_secid_usa] = 1; //Для акций США кол-во в лоте равно 1
				}
			}

			if (count($SECID_actions_etf)) {
				//Получаем цены на акции
				$hlblock     = HL\HighloadBlockTable::getById(24)->fetch();
				$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
				$entityClass = $entity->getDataClass();
				$arFilter    = array('UF_DATE' => $arWorkDates, 'UF_ITEM' => $SECID_actions_etf);

				$res = $entityClass::getList(array(
					"filter" => $arFilter,
					"select" => array(
						"UF_ITEM", "UF_DATE", "UF_CLOSE", "UF_ITEM"
					),
					//"limit" => 20,
					"order" => array(
						"UF_DATE" => "ASC"
					),
					//"cache" => array("ttl" => 3600)
				));

				while ($item = $res->fetch()) {
					$date = (new DateTime($item["UF_DATE"]))->format('d.m.Y');
					if ($cur_time < 1930 && $prev_day->format('d.m.Y') == (new DateTime($item["UF_DATE"]))->format('d.m.Y')) {
						$date = (new DateTime(date()))->format('d.m.Y');
					}
					$sumActEtf = $item["UF_CLOSE"] * $arActions[$item["UF_ITEM"]];
					if ($arActCurrency[$item["UF_ITEM"]] != 'rub' && $arActCurrency[$item["UF_ITEM"]] != 'sur') {
						$currencyRate = $this->getCurrencyRate(strtoupper($arActCurrency[$item["UF_ITEM"]]), $date);

						if ($currencyRate > 0) {
							$sumActEtf *= $currencyRate;
						}

						/*           echo $date." ".$item["UF_ITEM"]."<pre  style='color:black; font-size:11px;'>";
					print_r($arActCurrency[$item["UF_ITEM"]]);
					echo "</pre>";*/
					}

					$arResult[$date][$item["UF_ITEM"]] = round($sumActEtf, 2);
				}
			}

			if (count($SECID_usa) && count($arWorkDatesSql)) {
				//Получаем цены на акции США


/*				$hlblock     = HL\HighloadBlockTable::getById($this->spb_actions_HL_Id)->fetch();
				$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
				$entityClass = $entity->getDataClass();
				$arFilter    = array('UF_DATE' => $arWorkDates, 'UF_ITEM' => $SECID_usa);

				$res = $entityClass::getList(array(
					"filter" => $arFilter,
					"select" => array(
						"UF_ITEM", "UF_DATE", "UF_CLOSE", "UF_ITEM"
					),
					//"limit" => 20,
					"order" => array(
						"UF_DATE" => "ASC",
						"UF_ITEM" => "ASC"
					),
					//"cache" => array("ttl" => 3600)
				));*/
				$tableName = $this->hl_tables_prices[$this->spb_actions_HL_Id];
				$sql = "SELECT `UF_ITEM`, `UF_DATE`, `UF_CLOSE` FROM `$tableName` WHERE `UF_DATE` IN (".implode(',', $arWorkDatesSql).") AND `UF_ITEM` IN (".implode(',', $SECID_usaSql).") ORDER BY `UF_DATE` ASC, `UF_ITEM` ASC";
				$res = $DB->Query($sql);
				$lastItem  = '';
				$lastPrice = 0;
				$lastDate  = '';

				while ($item = $res->fetch()) {
					$date   = (new DateTime($item["UF_DATE"]))->format('d.m.Y');
					$sumUSA = floatVal($item["UF_CLOSE"] * $arActions[$item["UF_ITEM"]]);

					if ($arActCurrency[$item["UF_ITEM"]] != 'rub' && $arActCurrency[$item["UF_ITEM"]] != 'sur') {
						$currencyRate = $this->getCurrencyRate(strtoupper($arActCurrency[$item["UF_ITEM"]]), $date);
						if ($currencyRate > 0) {
							$sumUSA *= $currencyRate;
						}
					}
					if ($sumUSA > 0) {
						$lastPrice = $sumUSA;
						$lastItem  = $item["UF_ITEM"];
						$lastDate  = $date;
					} else if ($lastItem == $item["UF_ITEM"]) {
						if ($arResult[$lastDate][$item["UF_ITEM"]] > 0) {
							$sumUSA = $arResult[$lastDate][$item["UF_ITEM"]];
						}
						//  $sumUSA = $lastPrice;
					}

					$arResult[$date][$item["UF_ITEM"]] = round($sumUSA, 2);
				}
			}

			//Заполняем данные по недостающим датам данными из предыдущих дат
			$lastData = array();
			foreach ($arWorkDates01 as $date01) {
				if (array_key_exists($date01, $arResult)) {
					$lastData = $arResult[$date01];
				} elseif (!empty($lastData)) {
					$arResult[$date01] = $lastData;
				}
			}

			return $arResult;
		}

		/**
		 * Получает список цен всех акций и облигаций по указанным датам
		 *
		 * @param  array $queryDates Массив дат по первым числам месяцев для запрашиваемого диапазона лет
		 * @param  array $SECID Массив кодов активов. В фильтр разных HL инфоблоков идет одинаковый массив, т.к., например в акциях не может оказаться код облигации и наоборот
		 *
		 * @return array  $arResult['01.10.2019'][Код актива] = цена на первый рабочий день (может не совпадать с первым числом месяца);
		 *
		 * @access public
		 */
		public function getHLActivesPrices($queryDates, $SECID = array(), $arActTypes, $arActCurrency) {
			$arResult = array();
			//Делаем массив первых рабочих дней месяцев для фильтра
			$arWorkDates   = array();
			$arWorkDates01 = array();
			$cur_time      = intval(date('Hi'));
			$nowDate = new DateTime();
			$prev_day      = (new DateTime(date()))->modify('-1 days');
			foreach ($queryDates as $qDate) {
				if ($qDate == (new DateTime(date()))->format('d.m.Y') && $cur_time < 1930) {
					$arWorkDates[]   = $this->getMonthFirstWorkday($prev_day->format('d.m.Y'));
					$arWorkDates01[] = $prev_day->modify('+1 days')->format('d.m.Y');
				} else {
					$arWorkDates[]   = $this->getMonthFirstWorkday($qDate);
					$arWorkDates01[] = (new DateTime($qDate))->format('01.m.Y');
				}
			}

			$SECID_actions_etf = array();
			$SECID_usa         = array();
			foreach ($SECID as $item_secid) {
				if ($arActTypes[$item_secid] == 'share_etf' || $arActTypes[$item_secid] == 'share') {
					$SECID_actions_etf[] = $item_secid;
				} else if ($arActTypes[$item_secid] == 'share_usa') {
					$SECID_usa[] = $item_secid;
				}
			}

			//Получаем кол-ва в лоте для акций
			$arActions = $this->getActionsLotsizes($SECID_actions_etf);
			if (count($SECID_usa) > 0) {
				foreach ($SECID_usa as $item_secid_usa) {
					$arActions[$item_secid_usa] = 1; //Для акций США кол-во в лоте равно 1
				}
			}

			if (count($SECID_actions_etf)) {
				//Получаем цены на акции
				$hlblock     = HL\HighloadBlockTable::getById(24)->fetch();
				$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
				$entityClass = $entity->getDataClass();
				$arFilter    = array('UF_DATE' => $arWorkDates, 'UF_ITEM' => $SECID_actions_etf);

				$res = $entityClass::getList(array(
					"filter" => $arFilter,
					"select" => array(
						"UF_ITEM", "UF_DATE", "UF_CLOSE", "UF_ITEM"
					),
					//"limit" => 20,
					"order" => array(
						"UF_DATE" => "ASC"
					),
					//"cache" => array("ttl" => 3600)
				));

				while ($item = $res->fetch()) {
					if ((new DateTime($item["UF_DATE"]))->getTimestamp()==$prev_day->getTimestamp()){
							$date = (new DateTime($item["UF_DATE"]))->format('d.m.Y');
						} else {
							$date = (new DateTime($item["UF_DATE"]))->format('01.m.Y');
						}


					if ($cur_time < 1930 && $prev_day->format('d.m.Y') == (new DateTime($item["UF_DATE"]))->format('d.m.Y')) {
						$date = (new DateTime(date()))->format('d.m.Y');
					}

					$sumActEtf = $item["UF_CLOSE"] * $arActions[$item["UF_ITEM"]];
					if(!in_array(strtoupper($arActCurrency[$item["UF_ITEM"]]), $this->arRoubleCodes)){
						$currencyRate = $this->getCurrencyRate(strtoupper($arActCurrency[$item["UF_ITEM"]]), $date);
						if ($currencyRate > 0) {
						 	$sumActEtf *= $currencyRate;
						}

						/*           echo $date." ".$item["UF_ITEM"]."<pre  style='color:black; font-size:11px;'>";
					print_r($arActCurrency[$item["UF_ITEM"]]);
					echo "</pre>";*/
					}

					$arResult[$date][$item["UF_ITEM"]] = round($sumActEtf, 2);
				}
			}

			if (count($SECID_usa)) {
				//Проверяем список дат для америки на ее выходные и праздники, заменяем даты если они выпадают на нерабочие дни.
				//Для замены движемся в будущее +1 день пока он не праздник и не выходной
				$arWorkDaysUSA = array();
				foreach ($arWorkDates as $date) {
					$arWorkDaysUSA[] = $this->getMonthFirstWorkdayUsa((new DateTime($date))->format('d.m.Y'));
				}

				//Получаем цены на акции США
				$hlblock     = HL\HighloadBlockTable::getById($this->spb_actions_HL_Id)->fetch();
				$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
				$entityClass = $entity->getDataClass();
				//$arFilter    = array('UF_DATE' => $arWorkDates, 'UF_ITEM' => $SECID_usa);
				$arFilter = array('UF_DATE' => $arWorkDaysUSA, 'UF_ITEM' => $SECID_usa);

				$res = $entityClass::getList(array(
					"filter" => $arFilter,
					"select" => array(
						"UF_ITEM", "UF_DATE", "UF_CLOSE", "UF_ITEM"
					),
					//"limit" => 20,
					"order" => array(
						"UF_DATE" => "ASC",
						"UF_ITEM" => "ASC"
					),
					"cache" => array("ttl" => 3600)
				));
				$lastItem  = '';
				$lastPrice = 0;
				$lastDate  = '';
				while ($item = $res->fetch()) {
					$date   = (new DateTime($item["UF_DATE"]))->format('01.m.Y');
					$sumUSA = floatVal($item["UF_CLOSE"] * $arActions[$item["UF_ITEM"]]);

					if ($arActCurrency[$item["UF_ITEM"]] != 'rub' && $arActCurrency[$item["UF_ITEM"]] != 'sur') {
						$currencyRate = $this->getCurrencyRate(strtoupper($arActCurrency[$item["UF_ITEM"]]), $date);
						if ($currencyRate > 0) {
							$sumUSA *= $currencyRate;
						}
					}
					if ($sumUSA > 0) {
						$lastPrice = $sumUSA;
						$lastItem  = $item["UF_ITEM"];
						$lastDate  = $date;
					} else if ($lastItem == $item["UF_ITEM"]) {
						if ($arResult[$lastDate][$item["UF_ITEM"]] > 0) {
							$sumUSA = $arResult[$lastDate][$item["UF_ITEM"]];
						}
						//  $sumUSA = $lastPrice;
					}

					$arResult[$date][$item["UF_ITEM"]] = round($sumUSA, 2);
				}
			}

			uksort($arResult, function ($key1, $key2) {
				return (new DateTime($key1))->getTimestamp() <=> (new DateTime($key2))->getTimestamp();
			});

			//Заполняем данные по недостающим датам данными из предыдущих дат
			$lastData = array();
			foreach ($arWorkDates01 as $date01) {
				if (array_key_exists($date01, $arResult)) {
					$lastData = $arResult[$date01];
				} elseif (!empty($lastData)) {
					$arResult[$date01] = $lastData;
				}
			}

			//Получаем цены на облигации
			/*    $hlblock = HL\HighloadBlockTable::getById(25)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();
			$arFilter = array('UF_DATE' => $arWorkDates, 'UF_ITEM' => $SECID);

			$res = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array(
			"UF_ITEM", "UF_DATE", "UF_CLOSE", "UF_ITEM"
			),
			//"limit" => 20,
			"order" => array(
			"ID" => "DESC"
			),
			"cache" => array("ttl" => 3600)
			));

			while ($item = $res->fetch()) {
			$date = (new DateTime($item["UF_DATE"]))->format('01.m.Y');
			$arResult[$date][$item["UF_ITEM"]] = round($item["UF_CLOSE"], 2);
			}*/

			/*           echo "arWorkDates<pre  style='color:black; font-size:11px;'>";
			print_r($arResult);
			echo "</pre>";*/

			return $arResult;
		}

		/**
		 * Получает список цен индекса SP500 на каждый день указанного диапазона датам
		 *
		 * @param  array $queryDates Массив дат для запрашиваемого диапазона лет
		 *
		 * @return array  $arResult['01.10.2019'] = цена на первый рабочий день/на каждый день (может не совпадать с первым числом месяца);
		 *
		 * @access public
		 */
		public function getSP500IndexPrices($queryDates, $everyday = false) {
			$arResult = array();
			//Делаем массив первых рабочих дней месяцев для фильтра или массив всех дней диапазона
			$arWorkDates   = array();
			$arWorkDates01 = array();
			$cur_time      = intval(date('Hi'));
			$prev_day      = (new DateTime(date()))->modify('-1 days');
			foreach ($queryDates as $qDate) {
				if ($qDate == (new DateTime(date()))->format('d.m.Y') && $cur_time < 1930) {
					if ($everyday) {
						$arWorkDates[] = $prev_day->format('d.m.Y');
					} else {
						$arWorkDates[] = $this->getMonthFirstWorkday($prev_day->format('d.m.Y'));
					}
					$arWorkDates01[] = $prev_day->modify('+1 days')->format('d.m.Y');
				} else {
					if ($everyday) {
						$arWorkDates[] = $qDate;
					} else {
						$arWorkDates[] = $this->getMonthFirstWorkday($qDate);
					}

					$arWorkDates01[] = (new DateTime($qDate))->format(($everyday ? 'd' : '01') . '.m.Y');
				}
			}

			//Получаем цены на индекс
			$hlblock     = HL\HighloadBlockTable::getById(33)->fetch();
			$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();
			$arFilter    = array('UF_DATE' => $arWorkDates);

			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array(
					"UF_DATE", "UF_CLOSE"
				),
				//"limit" => 20,
				"order" => array(
					"UF_DATE" => "ASC"
				),
				"cache" => array("ttl" => 3600)
			));

			while ($item = $res->fetch()) {
				$date = (new DateTime($item["UF_DATE"]))->format(($everyday ? 'd' : '01') . '.m.Y');
				if ($cur_time < 1930 && $prev_day->format('d.m.Y') == (new DateTime($item["UF_DATE"]))->format('d.m.Y')) {
					$date            = (new DateTime(date()))->format('d.m.Y');
					$arResult[$date] = round($item["UF_CLOSE"], 2);
				} else {
					$arResult[$date] = round($item["UF_CLOSE"], 2);
				}
				$lasp_price = round($item["UF_CLOSE"], 2);
			}

			//Заполняем данные по недостающим датам данными из предыдущих дат
			$lastData = array();
			foreach ($arWorkDates01 as $date01) {
				if (array_key_exists($date01, $arResult)) {
					$lastData = $arResult[$date01];
				} elseif (!empty($lastData)) {
					$arResult[$date01] = $lastData;
				}
			}

			return $arResult;
		}

		/**
		 * Получает список цен указанного индекса по указанным датам
		 *
		 * @param  array $queryDates Массив дат для запрашиваемого диапазона лет
		 * @param  array $SECID Тикер индекса
		 *
		 * @return array  $arResult['01.10.2019'][Код Индекса] = цена на первый рабочий день/на каждый день (может не совпадать с первым числом месяца);
		 *
		 * @access public
		 */
		public function getMoexIndexesPrices($queryDates, $SECID, $everyday = false) {
			$arResult = array();
			//Делаем массив первых рабочих дней месяцев для фильтра  или массив всех дней диапазона
			$arWorkDates   = array();
			$arWorkDates01 = array();
			$cur_time      = intval(date('Hi'));
			$prev_day      = (new DateTime(date()))->modify('-1 days');
			foreach ($queryDates as $qDate) {
				if ($qDate == (new DateTime(date()))->format('d.m.Y') && $cur_time < 1930) {
					if ($everyday) {
						$arWorkDates[] = $prev_day->format('d.m.Y');
					} else {
						$arWorkDates[] = $this->getMonthFirstWorkday($prev_day->format('d.m.Y'));
					}
					$arWorkDates01[] = $prev_day->modify('+1 days')->format('d.m.Y');
				} else {
					if ($everyday) {
						$arWorkDates[] = $qDate;
					} else {
						$arWorkDates[] = $this->getMonthFirstWorkday($qDate);
					}

					$arWorkDates01[] = (new DateTime($qDate))->format(($everyday ? 'd' : '01') . '.m.Y');
				}
			}

			//Получаем цены на индекс
			$hlblock     = HL\HighloadBlockTable::getById(30)->fetch();
			$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();
			$arFilter    = array('UF_DATE' => $arWorkDates, 'UF_ITEM' => $SECID);

			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array(
					"UF_DATE", "UF_CLOSE", "UF_ITEM"
				),
				//"limit" => 20,
				"order" => array(
					"UF_DATE" => "ASC"
				),
				"cache" => array("ttl" => 3600)
			));

			while ($item = $res->fetch()) {
				$date = (new DateTime($item["UF_DATE"]))->format(($everyday ? 'd' : '01') . '.m.Y');
				if ($cur_time < 1930 && $prev_day->format('d.m.Y') == (new DateTime($item["UF_DATE"]))->format('d.m.Y')) {
					$date                              = (new DateTime(date()))->format('d.m.Y');
					$arResult[$date][$item["UF_ITEM"]] = round($item["UF_CLOSE"], 2);
				} else {
					$arResult[$date][$item["UF_ITEM"]] = round($item["UF_CLOSE"], 2);
				}
				$lasp_price = round($item["UF_CLOSE"], 2);
			}

			//Заполняем данные по недостающим датам данными из предыдущих дат
			$lastData = array();
			foreach ($arWorkDates01 as $date01) {
				if (array_key_exists($date01, $arResult)) {
					$lastData = $arResult[$date01];
				} elseif (!empty($lastData)) {
					$arResult[$date01] = $lastData;
				}
			}

			return $arResult;
		}

		/**
		 * Получает список цен указанного HL инфоблока по указанным датам
		 *
		 * @param  array $queryDates Массив дат для запрашиваемого диапазона лет
		 * @param  array $SECID Тикер ETF
		 *
		 * @return array  $arResult['01.10.2019'][Код Индекса] = цена на первый рабочий день/на каждый день (может не совпадать с первым числом месяца);
		 *
		 * @access public
		 */
		public function getMoexCommonPrices($queryDates, $SECID, $everyday = false, $hlId = 24) {
			$arResult = array();
			//Делаем массив первых рабочих дней месяцев для фильтра  или массив всех дней диапазона
			$arWorkDates   = array();
			$arWorkDates01 = array();
			$cur_time      = intval(date('Hi'));
			$prev_day      = (new DateTime(date()))->modify('-1 days');
			foreach ($queryDates as $qDate) {
				if ($qDate == (new DateTime(date()))->format('d.m.Y') && $cur_time < 1930) {
					if ($everyday) {
						$arWorkDates[] = $prev_day->format('d.m.Y');
					} else {
						$arWorkDates[] = $this->getMonthFirstWorkday($prev_day->format('d.m.Y'));
					}
					$arWorkDates01[] = $prev_day->modify('+1 days')->format('d.m.Y');
				} else {
					if ($everyday) {
						$arWorkDates[] = $qDate;
					} else {
						$arWorkDates[] = $this->getMonthFirstWorkday($qDate);
					}

					$arWorkDates01[] = (new DateTime($qDate))->format(($everyday ? 'd' : '01') . '.m.Y');
				}
			}

			//Получаем цены на индекс
			$hlblock     = HL\HighloadBlockTable::getById($hlId)->fetch();
			$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();
			$arFilter    = array('=UF_DATE' => $arWorkDates, '=UF_ITEM' => $SECID);

			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array(
					"UF_DATE", "UF_CLOSE", "UF_ITEM"
				),
				//"limit" => 20,
				"order" => array(
					"UF_DATE" => "ASC"
				),
				//"cache" => array("ttl" => 3600)
			));

			while ($item = $res->fetch()) {
				$date = (new DateTime($item["UF_DATE"]))->format(($everyday ? 'd' : '01') . '.m.Y');
				if ($cur_time < 1930 && $prev_day->format('d.m.Y') == (new DateTime($item["UF_DATE"]))->format('d.m.Y')) {
					$date                              = (new DateTime(date()))->format('d.m.Y');
					$arResult[$date][$item["UF_ITEM"]] = round($item["UF_CLOSE"], 2);
				} else {
					$arResult[$date][$item["UF_ITEM"]] = round($item["UF_CLOSE"], 2);
				}
				$lasp_price = round($item["UF_CLOSE"], 2);
			}

			//Заполняем данные по недостающим датам данными из предыдущих дат
			$lastData = array();
			foreach ($arWorkDates01 as $date01) {
				if (array_key_exists($date01, $arResult)) {
					$lastData = $arResult[$date01];
				} elseif (!empty($lastData)) {
					$arResult[$date01] = $lastData;
				}
			}

			return $arResult;
		}

		public function getEditModeForPortfolio($uid, $pid) {
			$res      = true;
			$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_USER", "PROPERTY_OWNER_USER", "PROPERTY_TRACKING_DATE");
			$arFilter = Array("IBLOCK_ID" => IntVal($this->portfolio_iblockId), "ID" => $pid, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if ($arItem = $res->fetch()) {
				if ($arItem["PROPERTY_OWNER_USER_VALUE"] != $GLOBALS["USER"]->GetID()) {
					$res = false;
				}
			}

			return $res;
		}

		/**
		 * Возвращает полный список портфелей из инфоблока с ключом по id портфеля
		 *
		 * @return array
		 *
		 * @access public
		 */
		public function getPortfolioListAll($uid = false) {
		  $cache_id = md5('getPortfolioListAll'.$uid);
         $cache_dir = "/portfolio/getPortfolioListAll";

         $obCache = new CPHPCache;
			$cacheTime = $this->cacheOn?86400*7:0;
         if($obCache->InitCache($cacheTime, $cache_id, $cache_dir))
         {
             $arResult = $obCache->GetVars();
         }
         elseif($obCache->StartDataCache())
         {
             global $CACHE_MANAGER;
             $CACHE_MANAGER->StartTagCache($cache_dir);  //Старт тегирования
        	    	$arResult = $this->getPortfolioListAllCached($uid);
             $CACHE_MANAGER->RegisterTag("getPortfolioListAll_".$uid); //Отметка тегом
             $CACHE_MANAGER->EndTagCache(); //Финализация тегирования
             $obCache->EndDataCache($arResult);
         }
         else
         {
             $arResult = array();
         }

			return $arResult;
		}
		public function clearPortfolioListAllCache($uid){
			global $CACHE_MANAGER;
			$CACHE_MANAGER->ClearByTag("getPortfolioListAll_".$uid);
		}
		public function getPortfolioListAllCached($uid = false) {
			$arReturn = array();
				$arSelect = Array("ID", "NAME", "IBLOCK_ID", "DATE_CREATE", "PREVIEW_TEXT", "PROPERTY_OWNER_USER", "PROPERTY_BROKER", "PROPERTY_TRACKING_DATE", "PROPERTY_INVEST_SUMM", "PROPERTY_CURRENT_SUMM", "PROPERTY_VERSION", "PROPERTY_LOADED_FILES");
				$arFilter = Array("IBLOCK_ID" => IntVal($this->portfolio_iblockId));
				if ($uid != false && intval($uid) > 0) {
					$arFilter["PROPERTY_USER"] = $uid;
				}
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->fetch()) {
/*					$arResult[$ob["ID"]] = array(
						"PROPERTIES" => array(
							"OWNER_USER" => array("VALUE" => $ob["PROPERTY_OWNER_USER_VALUE"]),
							"INVEST_SUMM" => array("VALUE" => $ob["PROPERTY_INVEST_SUMM_VALUE"]),
							"CURRENT_SUMM" => array("VALUE" => $ob["PROPERTY_CURRENT_SUMM_VALUE"]),
						),
					);*/
					$arResult[$ob["ID"]]["PROPERTIES"]["OWNER_USER"]["VALUE"] = $ob["PROPERTY_OWNER_USER_VALUE"];
					$arResult[$ob["ID"]]["PROPERTIES"]["INVEST_SUMM"]["VALUE"] = $ob["PROPERTY_INVEST_SUMM_VALUE"];
					$arResult[$ob["ID"]]["PROPERTIES"]["CURRENT_SUMM"]["VALUE"] = $ob["PROPERTY_CURRENT_SUMM_VALUE"];
					$arResult[$ob["ID"]]["PROPERTIES"]["VERSION"]["VALUE"] = $ob["PROPERTY_VERSION_VALUE"];
					$arResult[$ob["ID"]]["PROPERTIES"]["BROKER"]["VALUE"] = $ob["PROPERTY_BROKER_VALUE"];
					$arResult[$ob["ID"]]["PROPERTIES"]["TRACKING_DATE"]["VALUE"] = $ob["PROPERTY_TRACKING_DATE_VALUE"];
					$arResult[$ob["ID"]]["PROPERTIES"]["DATE_CREATE"]["VALUE"] = $ob["DATE_CREATE"];
					if(!empty($ob["PROPERTY_LOADED_FILES_VALUE"]))
				  		$arResult[$ob["ID"]]["PROPERTIES"]["LOADED_FILES"]["VALUE"][] = $ob["PROPERTY_LOADED_FILES_VALUE"];

				}


			return $arResult;
		}

		//Проверяет является ли текущий пользователь владельцем запрашиваемого портфеля
		public function isPortfolioOwner($pid, $json = false) {
				$arResult = array();

				$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PREVIEW_TEXT", "PROPERTY_OWNER_USER", );
				$arFilter = Array("IBLOCK_ID" => IntVal($this->portfolio_iblockId), "ID" => $pid, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
				$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

				$isOwner = false;
				if($ob = $res->fetch()) {


					if($GLOBALS["USER"]->GetID() == $ob["PROPERTY_OWNER_USER_VALUE"]){
						$isOwner = true;
					}
				}
				if($json){
					$isOwner = json_encode($isOwner);
				}
				return $isOwner;
		}

		/**
		 * Возвращает список портфелей для указанного пользователя
		 *
		 * @param  int   $uid - ID Пользователя системы, привязанного к портфелю
		 * @param  bool   $json (Optional) - Если true - то результат отдается в формате json, иначе в виде обычного массива
		 *
		 * @return array/json
		 *
		 * @access public
		 */
		public function getPortfolioListForUser($uid, $json = true) {
		 $cache_id = md5(serialize(array($uid, $json)));
        $cache_dir = "/portfolio/getPortfolioListForUser";

        $obCache = new CPHPCache;
        if($obCache->InitCache($this->cacheOn?86400*7:0, $cache_id, $cache_dir))
        {
            $arResult = $obCache->GetVars();
        }
        elseif($obCache->StartDataCache())
        {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);  //Старт тегирования

       	   $arResult = $this->getPortfolioListForUserCached($uid, $json);

            $CACHE_MANAGER->RegisterTag("PortfolioListForUser_".$uid); //Отметка тегом
            $CACHE_MANAGER->EndTagCache(); //Финализация тегирования

            $obCache->EndDataCache($arResult);
        }
        else
        {
            $arResult = array();
        }
		  	//$firephp = FirePHP::getInstance(true);
			//$firephp->fb(array("port_list"=>$arResult),FirePHP::LOG);

		  return $arResult;
		}

		/**
		 * Сбрасывает по тегу кеш списка портфелей для конкретного пользователя
		 *
		 * @param  [add type]   $uid [add description]
		 *
		 * @return [add type]  [add description]
		 *
		 * @access public
		 */
		public function clearPortfolioListForUserCache($uid){
			global $CACHE_MANAGER;
			$CACHE_MANAGER->ClearByTag("PortfolioListForUser_".$uid);
		}

		public function getPortfolioListForUserCached($uid, $json = true) {
				$arResult = array();

				$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PREVIEW_TEXT", "PROPERTY_*", );
				$arFilter = Array("IBLOCK_ID" => IntVal($this->portfolio_iblockId), "PROPERTY_USER" => $uid, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
				$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

				while ($ob = $res->GetNextElement()) {
					$arItem               = $ob->GetFields();
					$arItem["PROPERTIES"] = $ob->GetProperties();
					$namePostfix          = '';

					if ($GLOBALS["USER"]->IsAdmin()) {
						if ($arItem["PROPERTIES"]["OWNER_USER"]["VALUE"] != $GLOBALS["USER"]->GetID()) {
							$rsUser      = CUser::GetByID($arItem["PROPERTIES"]["OWNER_USER"]["VALUE"]);
							$arUser      = $rsUser->Fetch();
							$namePostfix = ' [' . $arUser["LOGIN"] . ']';
						}
					}

					$arCoefficients = array();
					foreach ($arItem["PROPERTIES"] as $propCode => $arProperty) {
						if (strpos($propCode, "CF_") !== false) {
							$arCoefficients[$propCode] = $arProperty["VALUE"];
						}
					}

					$arLoadedBrokerFiles = array();
					if(!empty($arItem["PROPERTIES"]["LOADED_FILES"]["VALUE"])){
					  foreach($arItem["PROPERTIES"]["LOADED_FILES"]["VALUE"] as $k=>$v){
						 $arLoadedBrokerFiles[] = $v . ' ' . $arItem["PROPERTIES"]["LOADED_FILES"]["DESCRIPTION"][$k];
					  }
					}

					$arResult["portfolio_" . $arItem["ID"]] = array(
						"id" => $arItem["ID"],
						"date_curr" => (new DateTime($arItem["PROPERTIES"]["TRACKING_DATE"]["VALUE"]))->format('Y-m-d'),
						"name" => $arItem["NAME"] . $namePostfix,
						"owner_id" => $arItem["PROPERTIES"]["OWNER_USER"]["VALUE"],
						"descr" => $arItem["PREVIEW_TEXT"],
						"ivest_summ" => $arItem["PROPERTIES"]["INVEST_SUMM"]["VALUE"],
/*						"current_summ" => $arItem["PROPERTIES"]["CURRENT_SUMM"]["VALUE"],*/
						"subzero_cache_on"=>$arItem["PROPERTIES"]["SUBZERO_CACHE_ON"]["VALUE"],
						"coefficients" => $arCoefficients,
						"ver" => $arItem["PROPERTIES"]["VERSION"]["VALUE"],
						"broker" => $arItem["PROPERTIES"]["BROKER"]["VALUE"],
						"broker_files" => $arLoadedBrokerFiles,
					);
				}

			if ($json) {
				$arResult = json_encode($arResult);
			}
			return $arResult;
		}

		//Получает из инфоблока портфеля его коэффициенты, если требуется то пересчитывает конкретный портфель
		public function getIblockPortfolioCoeff($pid, $recalculate = false) {
			$arResult = array();
			if ($recalculate == true) {
				$this->setIblockPortfolioCoeff($pid);
			}
			$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PREVIEW_TEXT", "PROPERTY_*", );
			$arFilter = Array("IBLOCK_ID" => IntVal($this->portfolio_iblockId), "ID" => $pid, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

			while ($ob = $res->GetNextElement()) {
				$arProps = $ob->GetProperties();
				foreach ($arProps as $propCode => $arProperty) {
					if (strpos($propCode, "CF_") !== false) {
						$arResult[$propCode] = $arProperty["VALUE"];
					}
				}
			}

			return $arResult;
		}


		/**
		 * Возвращает вид портфеля по типу сделок
		 *	Простой портфель это тот, где на каждый актив есть лишь одна запись без движений и может присутствовать кеш (в т.ч. с движениями)
		 *
		 * @param  [add type]   $pid id портфеля
		 *
		 * @return bool  true - простой |false - не простой
		 *
		 * @access public
		 */
		public function isSimpleDeals($pid) {
			Global $DB;
			$return = true;
			$pid = $this->clearPortfolioNumber($pid);
			if(intval($pid)<=0) return false;
			$query = "SELECT COUNT(ID) AS `ROWS`, `UF_ACTIVE_NAME` FROM `portfolio_history` WHERE `UF_PORTFOLIO` = $pid AND `UF_HISTORY_DIRECTION` != 'TOTAL' AND `UF_ACTIVE_TYPE` != 'currency' GROUP BY `UF_ACTIVE_ID` ORDER BY `ROWS` DESC";
			$res = $DB->Query($query);

			if($row = $res->fetch()){
				if(intval($row['ROWS'])>1){
					$return = false;
				}

			}
			unset($query, $res);
			return $return;
		}


		/**
		 * Возвращает очищенный от посторонних символов номер портфеля
		 *
		 * @param  str   $pid Строка с id портфеля, частью url портфеля и прочими символами
		 *
		 * @return str  очищенный номер портфеля
		 *
		 * @access public
		 */
		public function clearPortfolioNumber($pid){
			$strWithoutChars = preg_replace('/[^0-9]/', '', $pid);
			return $strWithoutChars;
		}

		//Пересчитывает и сохраняет в инфоблок коэффициенты портфеля. Вызывается из cron и при принудительном пересчете в getIblockPortfolioCoeff
		public function setIblockPortfolioCoeff($pid) {
			$portfolioKoeff = $this->getKoeffPortfolio(intval($pid));
			//записываем рассчитанные коэффициенты в инфоблок портфеля
			//Шарп
			if (!empty($portfolioKoeff["RUS"]["SHARP"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["RUS"]["SHARP"]["PORTFOLIO"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_SHARP_RUS_P" => floatval($portfolioKoeff["RUS"]["SHARP"]["PORTFOLIO"])));
			}
			if (!empty($portfolioKoeff["RUS"]["SHARP"]["INDEX"]) && !is_nan($portfolioKoeff["RUS"]["SHARP"]["INDEX"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_SHARP_RUS_I" => floatval($portfolioKoeff["RUS"]["SHARP"]["INDEX"])));
			}
			if (!empty($portfolioKoeff["USA"]["SHARP"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["USA"]["SHARP"]["PORTFOLIO"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_SHARP_USA_P" => floatval($portfolioKoeff["USA"]["SHARP"]["PORTFOLIO"])));
			}
			if (!empty($portfolioKoeff["USA"]["SHARP"]["INDEX"]) && !is_nan($portfolioKoeff["USA"]["SHARP"]["INDEX"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_SHARP_USA_I" => floatval($portfolioKoeff["USA"]["SHARP"]["INDEX"])));
			}

			//Трейнор
			if (!empty($portfolioKoeff["RUS"]["TREYNOR"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["RUS"]["TREYNOR"]["PORTFOLIO"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_TREYNOR_RUS_P" => floatval($portfolioKoeff["RUS"]["TREYNOR"]["PORTFOLIO"])));
			}
			if (!empty($portfolioKoeff["RUS"]["TREYNOR"]["INDEX"]) && !is_nan($portfolioKoeff["RUS"]["TREYNOR"]["INDEX"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_TREYNOR_RUS_I" => floatval($portfolioKoeff["RUS"]["TREYNOR"]["INDEX"])));
			}
			if (!empty($portfolioKoeff["USA"]["TREYNOR"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["USA"]["TREYNOR"]["PORTFOLIO"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_TREYNOR_USA_P" => floatval($portfolioKoeff["USA"]["TREYNOR"]["PORTFOLIO"])));
			}
			if (!empty($portfolioKoeff["USA"]["TREYNOR"]["INDEX"]) && !is_nan($portfolioKoeff["USA"]["TREYNOR"]["INDEX"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_TREYNOR_USA_I" => floatval($portfolioKoeff["USA"]["TREYNOR"]["INDEX"])));
			}

			//Сортино
			if (!empty($portfolioKoeff["RUS"]["SORTINO"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["RUS"]["SORTINO"]["PORTFOLIO"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_SORTINO_RUS_P" => floatval($portfolioKoeff["RUS"]["SORTINO"]["PORTFOLIO"])));
			}
			if (!empty($portfolioKoeff["RUS"]["SORTINO"]["INDEX"]) && !is_nan($portfolioKoeff["RUS"]["SORTINO"]["INDEX"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_SORTINO_RUS_I" => floatval($portfolioKoeff["RUS"]["SORTINO"]["INDEX"])));
			}
			if (!empty($portfolioKoeff["USA"]["SORTINO"]["PORTFOLIO"]) && !is_nan($portfolioKoeff["USA"]["SORTINO"]["PORTFOLIO"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_SORTINO_USA_P" => floatval($portfolioKoeff["USA"]["SORTINO"]["PORTFOLIO"])));
			}
			if (!empty($portfolioKoeff["USA"]["SORTINO"]["INDEX"]) && !is_nan($portfolioKoeff["USA"]["SORTINO"]["INDEX"])) {
				CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("CF_SORTINO_USA_I" => floatval($portfolioKoeff["USA"]["SORTINO"]["INDEX"])));
			}
			unset($portfolioKoeff);
		}


		/**
		 * Проверяет наличие отрицательного кеша на текущую дату в портфеле.
		 *
		 * @param  int, string   $pid id проверяемого портфеля
		 *
		 * @return bool  Если есть отрицательные итоги - вернет истину, в противно случае ложь
		 *
		 * @access public
		 */
		public function checkSubZeroCache($pid){
			Global $DB;
			if(empty($pid)) return false;
			$arResult = false;
			$arCache = $this->getCacheOnBeginDate($pid, date('d.m.Y'));
			if(count($arCache)>0)
				foreach($arCache as $currency=>$summ){
					if($summ['SUMM']<0){
					 $arResult = true;
					 break;
					}
				 }

			return $arResult;
		}


		/**
		 * Проверяет разрешенное кол-во добавленных портфелей и возвращает количество личных и сторонних привязанных портфелей, а так же общее количество.
		 *
		 * @param  int   $uid - ID Пользователя системы, привязанного к портфелю
		 * @param  $json         (Optional) - Если true - то результат отдается в формате json, иначе в виде обычного массива
		 *
		 * @return array/json
		 *
		 * @access public
		 */
		public function checkPortfolioCntForUser($uid, $json = true) {
			$cache   = Application::getInstance()->getManagedCache();
			$cacheId = "checkPortfolio2.1CntForUser" . $uid;

			$cacheTtl = $this->checkUseCacheLimitsUser($uid) == true ? 86400 : 0;
			//$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
			//$cache->clean("checkPortfolioCntForUser".$uid);
			if ($cache->read($cacheTtl, $cacheId)) {
				$arResult = $cache->get($cacheId);
			} else {
				/* code */$arResult["PORTFOLIO"] = array("OWNER" => 0, "LINKED" => 0, "ALL" => 0);
				$arResult["PORTFOLIO"]["LIMIT"]  = 0;

				//Получаем собственные портфели пользователя
				$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_USER", "PROPERTY_OWNER_USER", "PROPERTY_TRACKING_DATE");
				$arFilter = Array("IBLOCK_ID" => IntVal($this->portfolio_iblockId), "PROPERTY_OWNER_USER" => $uid, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
				$resOwner = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

				if ($resOwner) {
					$arResult["PORTFOLIO"]["OWNER"] = intval($resOwner->SelectedRowsCount());
				}
				unset($resOwner);

				//Получаем портфели где совладельцем указан текущий пользователь
				$arSelect  = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_USER", "PROPERTY_OWNER_USER", "PROPERTY_TRACKING_DATE");
				$arFilter  = Array("IBLOCK_ID" => IntVal($this->portfolio_iblockId), "PROPERTY_USER" => $uid, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
				$resLinked = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

				/*          if ($resLinked) {
				$arResult["PORTFOLIO"]["LINKED"] = intval($resLinked->SelectedRowsCount());
				}*/

				while ($rowLinked = $resLinked->fetch()) {
					if ($rowLinked["PROPERTY_OWNER_USER_VALUE"] != $rowLinked["PROPERTY_USER_VALUE"]) {
						$arResult["PORTFOLIO"]["LINKED"] += 1;
					}
				}

				unset($resLinked);

				$arResult["PORTFOLIO"]["ALL"] = $arResult["PORTFOLIO"]["OWNER"] + $arResult["PORTFOLIO"]["LINKED"];

				//получаем лимит по портфелям для пользователя
				$rsUser                         = CUser::GetByID($uid);
				$arUser                         = $rsUser->Fetch();
				$arResult["PORTFOLIO"]["LIMIT"] = $arUser['UF_PORTFOLIO_CNT'];

				if (intval($arResult["PORTFOLIO"]["LIMIT"]) <= 0) {
					$arResult["PORTFOLIO"]["LIMIT"] = $this->portfolioLimit;
				}
				$cache->set($cacheId, $arResult);
			}

			if ($GLOBALS["USER"]->IsAdmin() && $GLOBALS["USER"]->getId() == $uid) {
				$arResult["PORTFOLIO"]["LIMIT"] = 9999;
				$arResult["ADMIN"]              = "Y";
			} else {
				$arResult["ADMIN"] = "N";
			}

			if ($json) {
				$arResult = json_encode($arResult);
			}
			return $arResult;
		}

		/**
		 * Формирует массив с данными для построения графика бек-тест портфеля
		 *
		 * @param  int   $pid  - ID портфеля
		 * @param  int   $backYears (Optional) - Количество лет для обратного вычисления диапазона. По умолчанию 2 года
		 *
		 * @return array [01.12.2019] => Array
		(
		[CNT] => Array
		(
		[GAZP] => 90
		[RSTIP] => 10
		)

		[PRICE] => Array
		(
		[RSTIP] => 1.48
		[GAZP] => 254.05
		)
		[SUM] => 22879.3
		)
		 *
		 * @access public
		 */
		public function getBackTestData($pid, $backYears = 2, $benchmark_secid = '', $portfolio_benchmark_index = '', $benchmarkType = 'index', $benchmarkCurrency = 'rub') {
			$arErrors = array();
			$arDates = $this->getBacktestDateList(date('Y'), date('m'), $backYears);
			$benchmarkCurrency = strtoupper($benchmarkCurrency);
			if (count($arDates) > 0) {
				$arActivesCodeTmp     = array();
				$arDatesTmp           = array();
				$arBnchPortfolio      = array(); //Массив данных для сравнения портфеля
				$arBnchCountActives   = array();
				$arBnchActivesCodeTmp = array();
				$arBnchDatesTmp       = array();

				$arCountActives = $this->getActiveCountByDate($pid, (new DateTime())->format('d.m.Y'));
				$arActivesCurrency = $arCountActives["CURRENCY"];
				$arActivesTypes    = $arCountActives["TYPE"];
				$arCountActives    = $arCountActives["COUNT"];

				if (!empty($portfolio_benchmark_index)) { //Если передан id портфеля для сравнения
					$arBnchCountActives    = $this->getActiveCountByDate($portfolio_benchmark_index, (new DateTime())->format('d.m.Y'));
					$arBnchActivesCurrency = $arBnchCountActives["CURRENCY"];
					$arBnchTypes           = $arBnchCountActives["TYPE"];
					$arBnchCountActives    = $arBnchCountActives["COUNT"];
				}

				if (count($arCountActives) <= 0) {
					return array();
				}

				foreach ($arDates as $kd => $vd) {
					//$arCountActives = array();

					if (count($arCountActives) > 0) {
						foreach ($arCountActives as $kc => $vc) {
							$arActivesCodeTmp[$kc] = '';
						}
						$arDates[$kd] = array("CNT" => $arCountActives, "PRICE" => 0);
						$arDatesTmp[] = $kd; //Формируем список дат для получения цен только, если количества присутствуют на эту дату.
					}

					if (count($arBnchCountActives) > 0) {
						foreach ($arBnchCountActives as $kc => $vc) {
							$arBnchActivesCodeTmp[$kc] = '';
						}
						$arDates[$kd]     = array_merge($arDates[$kd], array("BNCH_CNT" => $arBnchCountActives, "BNCH_PRICE" => 0));
						$arBnchDatesTmp[] = $kd; //Формируем список дат для получения цен только, если количества присутствуют на эту дату.
					}
				}

				//Получаем цены MOEX по датам первых рабочих дней всех месяцев в диапазоне
				$arActivePrices = $this->getHLActivesPrices($arDatesTmp, array_keys($arActivesCodeTmp), $arActivesTypes, $arActivesCurrency);

				if (!empty($portfolio_benchmark_index)) {
					//Получаем цены MOEX по датам первых рабочих дней всех месяцев в диапазоне для сравнения портфеля
					$arBnchActivePrices = $this->getHLActivesPrices($arBnchDatesTmp, array_keys($arBnchActivesCodeTmp), $arBnchTypes, $arBnchActivesCurrency);
				}

				if (!empty($benchmark_secid) && $benchmarkType == 'index') { //Если передан индекс для бек-теста - получим массив цен для него и добавим в результирующий массив
					//Получаем цены по индексам MOEX по датам первых рабочих дней всех месяцев в диапазоне
					$arIndexPrices = $this->getMoexIndexesPrices($arDatesTmp, $benchmark_secid);
				}

				if (!empty($benchmark_secid) && $benchmarkType == 'etf') { //Если передан etf для бек-теста - получим массив цен для него и добавим в результирующий массив
					//Получаем цены по etf MOEX по датам первых рабочих дней всех месяцев в диапазоне
					$arETFPrices = $this->getMoexCommonPrices($arDatesTmp, $benchmark_secid, false, 24);

					if(!in_array($benchmarkCurrency, $this->arRoubleCodes)){
					 $arEtfTmp = array();
					 foreach($arETFPrices as $kd=>$val){
					  	//$tmpRate = $this->getCurrencyRate($benchmarkCurrency, $kd);
					  	$tmpRate = getCBPrice($benchmarkCurrency, $kd);
					  	$arEtfTmp[$kd][key($val)] = round(floatval(reset($val))*$tmpRate, 2);
					  }
					  $arETFPrices = $arEtfTmp;
					  //unset($arEtfTmp);
					}
 			}

				if (!empty($benchmark_secid) && $benchmarkType == 'currency') { //Если передана валюта для бек-теста - получим массив цен для нее и добавим в результирующий массив
					//Получаем цены по валютам MOEX по датам первых рабочих дней всех месяцев в диапазоне
					$arCurrencyPrices = $this->getMoexCommonPrices($arDatesTmp, $benchmark_secid, false, 31);
				}

				//Дополняем последнюю актуальную дату ценами активов с их страниц
				$today = (new DateTime(date()))->format('d.m.Y');


			  	$arActivePrices[$today] = $this->getActivesPrices(array(), array_keys($arActivesCodeTmp), false, true);

				if (!empty($portfolio_benchmark_index)) { //Если передан id портфеля для сравнения
					$arBnchActivePrices[$today] = $this->getActivesPrices(array(), array_keys($arBnchActivesCodeTmp), false, true);
				}

				$first  = true;
				$indCnt = 0;
				$etfCnt = 0;

				//Обрезаем график бек-теста слева
				//Если количество тикеров в массиве CNT не равно количеству тикеров в массиве $arActivePrices[$kd] то не добавляем такой период. Т.к. график получается недостоверный, когда цен на некоторые активы нет в истории..
				$arTmp = array();

				foreach ($arDates as $kd => $vd) {
					if (count($arActivePrices[$kd]) == count($arDates[$kd]["CNT"])) {
						$arTmp[$kd] = $vd;
					} else {
					 $diff = array_diff(array_keys($arDates[$kd]["CNT"]), array_keys($arActivePrices[$kd]));
					 if(is_array($diff) && count($diff)>0){
					  $arErrors[$kd] = implode(', ', $diff);
					  }
					}
				}
				if (count($arTmp) > 0) {
					$arDates = $arTmp;
				}

				//Обрезаем график по бенчмарку ETF слева, по наличию истории бенчмарка
				if (!empty($benchmark_secid) && $benchmarkType == 'etf') {
					$arTmp = array();
					foreach ($arDates as $kd => $vd) {
						if (array_key_exists($kd, $arETFPrices) == true) {
							$arTmp[$kd] = $vd;
						}
					}
					if (count($arTmp) > 0) {
						$arDates = $arTmp;
					}
				}

				foreach ($arDates as $kd => $vd) {
					if (count($arActivePrices[$kd]) >= 0) {
						$arDates[$kd]["PRICE"] = $arActivePrices[$kd];
						$arDates[$kd]["SUM"]   = 0;

						foreach ($vd["CNT"] as $kSECID => $vCnt) {
							$arDates[$kd]["SUM"] = round($arDates[$kd]["SUM"] + intval($vCnt) * floatval($arDates[$kd]["PRICE"][$kSECID]), 2);
						}

						if (!empty($benchmark_secid) && $benchmarkType == 'index') {
							if ($indCnt == 0 && floatval($arDates[$kd]["SUM"]) > 0) { //Получаем по первой не нулевой сумме кол-во для индекса и более не меняем его для расчета.
								$indCnt = round(floatval($arDates[$kd]["SUM"]) / floatval($arIndexPrices[$kd][$benchmark_secid]), 2);
							}

							if (isset($arIndexPrices[$kd][$benchmark_secid]) && floatval($arIndexPrices[$kd][$benchmark_secid]) > 0 && $arIndexPrices[$kd][$benchmark_secid] != NULL) {
								$indexSum = round($indCnt * floatval($arIndexPrices[$kd][$benchmark_secid]), 2);
							}

							$arDates[$kd]["INDEXES"][$benchmark_secid]["PRICE"] = $arIndexPrices[$kd][$benchmark_secid];
							$arDates[$kd]["INDEXES"][$benchmark_secid]["CNT"]   = $indCnt;
							$arDates[$kd]["INDEXES"][$benchmark_secid]["SUM"]   = $indexSum;
						}
						if (!empty($benchmark_secid) && $benchmarkType == 'etf') {
							if ($etfCnt == 0 && floatval($arDates[$kd]["SUM"]) > 0) { //Получаем по первой не нулевой сумме кол-во для ETF и более не меняем его для расчета.
								$etfCnt = round(floatval($arDates[$kd]["SUM"]) / floatval($arETFPrices[$kd][$benchmark_secid]), 2);
							}
							if (isset($arETFPrices[$kd][$benchmark_secid]) && floatval($arETFPrices[$kd][$benchmark_secid]) > 0) {
								$indexSum = round($etfCnt * floatval($arETFPrices[$kd][$benchmark_secid]), 2);
							}
							$arDates[$kd]["ETF"][$benchmark_secid]["PRICE"] = $arETFPrices[$kd][$benchmark_secid];
							$arDates[$kd]["ETF"][$benchmark_secid]["CNT"]   = $etfCnt;
							$arDates[$kd]["ETF"][$benchmark_secid]["SUM"]   = $indexSum;
						}

						if (!empty($benchmark_secid) && $benchmarkType == 'currency') {
							if ($indCnt == 0 && floatval($arDates[$kd]["SUM"]) > 0) { //Получаем по первой не нулевой сумме кол-во для ETF и более не меняем его для расчета.
								$indCnt = round(floatval($arDates[$kd]["SUM"]) / floatval($arCurrencyPrices[$kd][$benchmark_secid]), 2);
							}

							if (isset($arCurrencyPrices[$kd][$benchmark_secid]) && floatval($arCurrencyPrices[$kd][$benchmark_secid]) > 0) {
								$indexSum = round($indCnt * floatval($arCurrencyPrices[$kd][$benchmark_secid]), 2);
							}
							$arDates[$kd]["CURRENCY"][$benchmark_secid]["PRICE"] = $arCurrencyPrices[$kd][$benchmark_secid];
							$arDates[$kd]["CURRENCY"][$benchmark_secid]["CNT"]   = $indCnt;
							$arDates[$kd]["CURRENCY"][$benchmark_secid]["SUM"]   = $indexSum;
						}
					}

					if (!empty($portfolio_benchmark_index) && count($arBnchActivePrices[$kd]) > 0) {
						$arDates[$kd]["BNCH_PRICE"] = $arBnchActivePrices[$kd];
						$arDates[$kd]["BNCH_SUM"]   = 0;
						foreach ($vd["BNCH_CNT"] as $kSECID => $vCnt) {
							$arDates[$kd]["BNCH_SUM"] = $arDates[$kd]["BNCH_SUM"] + round(intval($vCnt) * floatval($arDates[$kd]["BNCH_PRICE"][$kSECID]), 2);
						}
					}

					$first = false;
				}

		//обрезалось тут

				unset($arActivesCodeTmp, $arDatesTmp);
			}

			return array("DATA"=>$arDates, "ERRORS"=>$arErrors);
		}

		//Формирует список дат от текущей на 5 лет назад
		public function setKoeffDatesList() {
			$this->arKoeffDateList = $this->getKoeffDateList(date('d.m.Y'), 5);
		}

		/**
		 * Формирует массив с данными для рассчета коэффициентов Шарпа, Сортино, Трейнора в портфеле
		 *
		 * @param  int   $pid  - ID портфеля
		 * @param  int   $backYears (Optional) - Количество лет для обратного вычисления диапазона. По умолчанию 2 года
		 *
		 * @return array [01.12.2019] => Array
		(
		[CNT] => Array
		(
		[GAZP] => 90
		[RSTIP] => 10
		)

		[PRICE] => Array
		(
		[RSTIP] => 1.48
		[GAZP] => 254.05
		)
		[SUM] => 22879.3
		)
		 *
		 * @access public
		 */
		public function getKoeffData($pid, $backYears = 5, $index = 'IMOEX', $indexSP500 = 'SP500') {
			$logname = 'sharp_'.$pid;

 $begin = microtime(true);
			if (count($this->arKoeffDateList) <= 0) {
				$this->arKoeffDateList = $this->getKoeffDateList(date('d.m.Y'), 5);
			}
 CLogger::{$logname}('1 arKoeffDateList time: '.(microtime(true) - $begin).PHP_EOL);

			$arDates = $this->arKoeffDateList;
			if (count($arDates) > 0) {
				$arActivesCodeTmp = array();
				$arDatesTmp       = array();
  $begin = microtime(true);
				$arCountActives   = $this->getActiveCountByDate($pid, (new DateTime(date('d.m.Y')))->format('d.m.Y'));
  CLogger::{$logname}('2 getActiveCountByDate time: '.(microtime(true) - $begin).PHP_EOL);
				$arActivesCurrency = $arCountActives["CURRENCY"];
				$arActivesTypes    = $arCountActives["TYPE"];
				$arCountActives    = $arCountActives["COUNT"];

				if (count($arCountActives) <= 0) {
					return array();
				}
  $begin = microtime(true);
				foreach ($arDates as $kd => $vd) {
					//$arCountActives = array();

					if (count($arCountActives) > 0) {
						foreach ($arCountActives as $kc => $vc) {
							$arActivesCodeTmp[$kc] = '';
						}
						$arDates[$kd] = array("CNT" => $arCountActives, "PRICE" => 0);
						$arDatesTmp[] = $kd; //Формируем список дат для получения цен только, если количества присутствуют на эту дату.
					}
				}
  CLogger::{$logname}('3 цикл $arDates time: '.(microtime(true) - $begin).PHP_EOL);
				//Получаем цены MOEX на каждую дату в диапазоне
  $begin = microtime(true);
				$arActivePrices = $this->getHLActivesPricesEveryday($arDatesTmp, array_keys($arActivesCodeTmp), $arActivesTypes, $arActivesCurrency);
  CLogger::{$logname}('4 getHLActivesPricesEveryday time: '.(microtime(true) - $begin).PHP_EOL);

				if (!empty($index)) { //Если передан индекс - получим массив цен для него и добавим в результирующий массив
					//Получаем цены по индексам MOEX по датам первых рабочих дней всех месяцев в диапазоне или по всем дням диапазона
  $begin = microtime(true);
					$arIndexPrices = $this->getMoexIndexesPrices($arDatesTmp, $index, true);
  CLogger::{$logname}('5 getMoexIndexesPrices time: '.(microtime(true) - $begin).PHP_EOL);
				}

				if (!empty($indexSP500)) { //Если передан индекс - получим массив цен для него и добавим в результирующий массив
					//Получаем цены по индексам SP500 по датам первых рабочих дней всех месяцев в диапазоне или по всем дням диапазона
  $begin = microtime(true);
					$arIndexSP500Prices = $this->getSP500IndexPrices($arDatesTmp, true);
  CLogger::{$logname}('6 getSP500IndexPrices time: '.(microtime(true) - $begin).PHP_EOL);
				}

				//Дополняем последнюю актуальную дату ценами активов с их страниц
				$today = (new DateTime(date()))->format('d.m.Y');

				$arActivePrices[$today] = $this->getActivesPrices(array(), array_keys($arActivesCodeTmp), false, true);

				$first  = true;
				$indCnt = 0;

				//Обрезаем график бек-теста слева
				//Если количество тикеров в массиве CNT не равно количеству тикеров в массиве $arActivePrices[$kd] то не добавляем такой период. Т.к. график получается недостоверный, когда цен на некоторые активы нет в истории..
  $begin = microtime(true);
  				$arTmp = array();
				foreach ($arDates as $kd => $vd) {
					if (count($arActivePrices[$kd]) == count($arDates[$kd]["CNT"])) {
						$arTmp[$kd] = $vd;
					}
				}

				if (count($arTmp) > 0) {
					$arDates = $arTmp;
				}

				foreach ($arDates as $kd => $vd) {
					if (count($arActivePrices[$kd]) >= 0) {
						$arDates[$kd]["PRICE"]   = $arActivePrices[$kd];
						$arDates[$kd]["SUM"]     = 0;
						$arDates[$kd]["SUM_USA"] = 0;
						$arDates[$kd]["SUM_ETF"] = 0;
						foreach ($vd["CNT"] as $kSECID => $vCnt) {
							if ($arActivesTypes[$kSECID] == 'share_usa') {
								$arDates[$kd]["SUM_USA"] = round($arDates[$kd]["SUM_USA"] + intval($vCnt) * floatval($arDates[$kd]["PRICE"][$kSECID]), 2);
							} else if ($arActivesTypes[$kSECID] == 'share') {
								$arDates[$kd]["SUM"] = round($arDates[$kd]["SUM"] + intval($vCnt) * floatval($arDates[$kd]["PRICE"][$kSECID]), 2);
							} else if ($arActivesTypes[$kSECID] == 'share_etf') {
								$arDates[$kd]["SUM_ETF"] = round($arDates[$kd]["SUM_ETF"] + intval($vCnt) * floatval($arDates[$kd]["PRICE"][$kSECID]), 2);
							}
						}

						if (!empty($index)) {
							if ($indCnt == 0 && floatval($arDates[$kd]["SUM"]) > 0) { //Получаем по первой не нулевой сумме кол-во для индекса и более не меняем его для расчета.
								$indCnt = round(floatval($arDates[$kd]["SUM"]) / floatval($arIndexPrices[$kd][$index]), 2);
							}

							if (isset($arIndexPrices[$kd][$index]) && floatval($arIndexPrices[$kd][$index]) > 0) {
								$indexSum = round($indCnt * floatval($arIndexPrices[$kd][$index]), 2);
							}
							$arDates[$kd]["INDEXES"][$index]["PRICE"] = $arIndexPrices[$kd][$index];
							$arDates[$kd]["INDEXES"][$index]["CNT"]   = $indCnt;
							$arDates[$kd]["INDEXES"][$index]["SUM"]   = $indexSum;
						}

						if (!empty($indexSP500)) {
							if ($indCntUSA == 0 && floatval($arDates[$kd]["SUM_USA"]) > 0) { //Получаем по первой не нулевой сумме кол-во для индекса и более не меняем его для расчета.
								$indCntUSA = round(floatval($arDates[$kd]["SUM_USA"]) / floatval($arIndexSP500Prices[$kd]), 2);
							}

							if (isset($arIndexSP500Prices[$kd]) && floatval($arIndexSP500Prices[$kd]) > 0) {
								$indexSum = round($indCntUSA * floatval($arIndexSP500Prices[$kd]), 2);
							}
							$arDates[$kd]["INDEXES"][$indexSP500]["PRICE"] = $arIndexSP500Prices[$kd];
							$arDates[$kd]["INDEXES"][$indexSP500]["CNT"]   = $indCntUSA;
							$arDates[$kd]["INDEXES"][$indexSP500]["SUM"]   = $indexSum;
						}
					}

					$first = false;
				}
  CLogger::{$logname}('7 Обрезка графика и формирование $arDates time: '.(microtime(true) - $begin).PHP_EOL);
				//обрезалось тут

				unset($arActivesCodeTmp, $arDatesTmp);
			} //if

			return $arDates;
		}

		public function finplan_standard_deviation(array $a, $sample = false) {
			$n = count($a);
			if ($n === 0) {
				trigger_error("The array has zero elements", E_USER_WARNING);
				return false;
			}
			if ($sample && $n === 1) {
				trigger_error("The array has only 1 element", E_USER_WARNING);
				return false;
			}
			$mean  = array_sum($a) / $n;
			$carry = 0.0;
			foreach ($a as $val) {
				$d = ((double) $val) - $mean;
				$carry += $d * $d;
			};
			if ($sample) {
				--$n;
			}
			return sqrt($carry / $n);
		}

		//Возвращает рассчитанные коэффициенты и отладочную информацию для сверки расчетов
		public function getKoeffPortfolio($pid, $returnDebugTable = false) {
			$arReturn = array("RUS" => array(), "USA" => array());
			$logname = 'sharp_'.$pid;
			CLogger::{$logname}();

	 $begin = microtime(true);
			//Получим данные
			$arData = $this->getKoeffData($pid);
	 CLogger::{$logname}('getKoeffData time: '.(microtime(true) - $begin).PHP_EOL);
	 //CLogger::{$logname}('getKoeffData data: '.print_r($arData, true).PHP_EOL);
	 CLogger::{$logname}('--------------------'.PHP_EOL);

	 $begin = microtime(true);
			$arSumm = array();
			$cnt    = 0;
			foreach ($arData as $date => $actives) {
				$arSumm[$cnt] = array("DATE" => $date, "SUM" => $actives["SUM"], "SUM_USA" => $actives["SUM_USA"], "IMOEX" => $actives["INDEXES"]["IMOEX"]["SUM"], "SP500" => $actives["INDEXES"]["SP500"]["SUM"]);
				$cnt++;
			}
	 CLogger::{$logname}('creating arSumm time: '.(microtime(true) - $begin).PHP_EOL);
	 CLogger::{$logname}('--------------------'.PHP_EOL);

			unset($arData);
	 $begin = microtime(true);
			$arIMOEX          = array();
			$arSP500          = array();
			$arProfit         = array();
			$arProfitUSA      = array();
			$arDayliReturn    = array();
			$arDayliReturnUSA = array();
			$volatDown        = array();
			$volatDownUSA     = array();
			$diffSquare       = array();
			$diffSquareUSA    = array();
			$volatDownIMOEX   = array();
			$volatDownSP500   = array();
			$diffSquareIMOEX  = array();
			$diffSquareSP500  = array();

			$BASIC_RISKFREE_RATE     = floatval(COption::GetOptionString("grain.customsettings", "BASIC_RISKFREE_RATE")) / 360;
			$BASIC_RISKFREE_RATE_USA = floatval(COption::GetOptionString("grain.customsettings", "BASIC_RISKFREE_RATE_USA")) / 360;

			for ($i = 1; $i < count($arSumm) - 1; $i++) {
				//Лист "Коэффициенты портфеля"
				$curr_i                    = $i - 1;
				$arIMOEX[$curr_i]          = log(floatval($arSumm[$i]["IMOEX"]) / floatval($arSumm[$i - 1]["IMOEX"])) * 100; //Столбец A по РФ - Доходность индекса
				$arSP500[$curr_i]          = log(floatval($arSumm[$i]["SP500"]) / floatval($arSumm[$i - 1]["SP500"])) * 100; //Столбец A по США - Доходность индекса
				$arProfit[$curr_i]         = log(floatval($arSumm[$i]["SUM"]) / floatval($arSumm[$i - 1]["SUM"])) * 100; //Столбец B по РФ - Доходность инвестиционного портфеля
				$arProfitUSA[$curr_i]      = log(floatval($arSumm[$i]["SUM_USA"]) / floatval($arSumm[$i - 1]["SUM_USA"])) * 100; //Столбец B по США - Доходность инвестиционного портфеля
				$arDayliReturn[$curr_i]    = round($BASIC_RISKFREE_RATE, 2); //Столбец C по РФ - Допустимая дневная доходность
				$arDayliReturnUSA[$curr_i] = round($BASIC_RISKFREE_RATE_USA, 4); //Столбец C по США - Допустимая дневная доходность

				//Расчеты по текущей строке
				$volatDown[$curr_i]    = ($arProfit[$curr_i] < $arDayliReturn[$curr_i] ? $arProfit[$curr_i] : 0); //Столбец D по РФ - Волатильность вниз
				$volatDownUSA[$curr_i] = ($arProfitUSA[$curr_i] < $arDayliReturnUSA[$curr_i] ? $arProfitUSA[$curr_i] : 0); //Столбец D по РФ - Волатильность вниз

				$diffSquare[$curr_i]    = ($volatDown[$curr_i] / 100 - $arDayliReturn[$curr_i] / 100) * ($volatDown[$curr_i] / 100 - $arDayliReturn[$curr_i] / 100); //Столбец D по РФ - Квадрат разницы
				$diffSquareUSA[$curr_i] = ($volatDownUSA[$curr_i] / 100 - $arDayliReturnUSA[$curr_i] / 100) * ($volatDownUSA[$curr_i] / 100 - $arDayliReturnUSA[$curr_i] / 100); //Столбец D по США - Квадрат разницы

				//Расчеты по текущей строке для индексов
				$volatDownIMOEX[$curr_i] = ($arIMOEX[$curr_i] < $arDayliReturn[$curr_i] ? $arIMOEX[$curr_i] : 0); //Столбец G по РФ - Волатильность "вниз" индекса
				$volatDownSP500[$curr_i] = ($arSP500[$curr_i] < $arDayliReturnUSA[$curr_i] ? $arSP500[$curr_i] : 0); //Столбец G по РФ - Волатильность "вниз" индекса

				$diffSquareIMOEX[$curr_i] = ($volatDownIMOEX[$curr_i] / 100 - $arDayliReturn[$curr_i] / 100) * ($volatDownIMOEX[$curr_i] / 100 - $arDayliReturn[$curr_i] / 100); //Столбец H по РФ - Квадрат разницы индекса
				$diffSquareSP500[$curr_i] = ($volatDownSP500[$curr_i] / 100 - $arDayliReturnUSA[$curr_i] / 100) * ($volatDownSP500[$curr_i] / 100 - $arDayliReturnUSA[$curr_i] / 100); //Столбец H по США - Квадрат разницы индекса
			}

			$IMOEX_profit = array_sum($arIMOEX) / count($arIMOEX); //Доходность индекса IMOEX
			$SP500_profit = array_sum($arSP500) / count($arSP500); //Доходность индекса SP500

			$portfolio_profit    = array_sum($arProfit) / count($arProfit); // Доходность портфеля РФ
			$portfolioUSA_profit = array_sum($arProfitUSA) / count($arProfitUSA); //Доходность портфеля США

			$portfolio_standardDeviation    = $this->finplan_standard_deviation($arProfit); //Стандартное отклонение портфеля РФ
			$portfolioUSA_standardDeviation = $this->finplan_standard_deviation($arProfitUSA); //Стандартное отклонение портфеля США

			$IMOEX_standardDeviation = $this->finplan_standard_deviation($arIMOEX); //Стандартное отклонение портфеля эталонного портфеля (индекса) РФ
			$SP500_standardDeviation = $this->finplan_standard_deviation($arSP500); //Стандартное отклонение портфеля эталонного портфеля (индекса) США

			//Трейнор
			$treynCovar     = stats_covariance($arIMOEX, $arProfit) / 10000; //Ковариация
			$treynVariance  = stats_variance($arIMOEX) / 10000; //Дисперсия индекса IMOEX
			$treynBeta      = $treynCovar / $treynVariance; //Бета портфеля РФ
			$treynBetaIMOEX = 1; //Бета индекса IMOEX

			$treynCovarUSA    = stats_covariance($arSP500, $arProfitUSA) / 10000; //Ковариация
			$treynVarianceUSA = stats_variance($arSP500) / 10000; //Дисперсия индекса IMOEX
			$treynBetaUSA     = $treynCovarUSA / $treynVarianceUSA;
			$treynBetaSP500   = 1; //Бета индекса SP500

			//Сортино
			$sortinoTmp      = sqrt(array_sum($diffSquare) / count($diffSquare));
			$sortinoIMOEXTmp = sqrt(array_sum($diffSquareIMOEX) / count($diffSquareIMOEX));
			$sortino         = (($portfolio_profit - $BASIC_RISKFREE_RATE) / $sortinoTmp) / 100;
			$sortinoIMOEX    = (($IMOEX_profit - $BASIC_RISKFREE_RATE) / $sortinoIMOEXTmp) / 100;

			$sortinoTmpUSA      = sqrt(array_sum($diffSquareUSA) / count($diffSquareUSA));
			$sortinoSP500TmpUSA = sqrt(array_sum($diffSquareSP500) / count($diffSquareSP500));
			$sortinoUSA         = (($portfolioUSA_profit - $BASIC_RISKFREE_RATE_USA) / $sortinoTmpUSA) / 100;
			$sortinoSP500USA    = (($SP500_profit - $BASIC_RISKFREE_RATE_USA) / $sortinoSP500TmpUSA) / 100;

			if ($returnDebugTable) {
				$arReturn["RUS"] = array(
					"A" => $arIMOEX,
					"B" => $arProfit,
					"C" => $arDayliReturn,
					"D" => $volatDown,
					"E" => $diffSquare,
					"G" => $volatDownIMOEX,
					"H" => $diffSquareIMOEX,
				);
				$arReturn["RUS"]["SHARP_CALCULATE"] = array(
					"Доходность индекса IMOEX" => $IMOEX_profit,
					"Доходность портфеля" => $portfolio_profit,
					"Дневная безрисковая доходность" => $BASIC_RISKFREE_RATE,
					"Стандартное отклонение портфеля" => $portfolio_standardDeviation,
					"Стандартное отклонение индекса" => $IMOEX_standardDeviation,
				);
				$arReturn["RUS"]["TREYNOR_CALCULATE"] = array(
					"Бета" => $treynBeta,
					"Бета индекса IMOEX" => $treynBetaIMOEX,
					"Ковариация" => $treynCovar,
					"Дисперсия индекса" => $treynVariance,
				);
				$arReturn["RUS"]["SORTINO_CALCULATE"] = array(
					"Корень" => $sortinoTmp,
				);

				$arReturn["USA"] = array(
					"A" => $arSP500,
					"B" => $arProfitUSA,
					"C" => $arDayliReturnUSA,
					"D" => $volatDownUSA,
					"E" => $diffSquareUSA,
					"G" => $volatDownSP500,
					"H" => $diffSquareSP500,
				);
				$arReturn["USA"]["SHARP_CALCULATE"] = array(
					"Доходность индекса SP500" => $SP500_profit,
					"Доходность портфеля" => $portfolioUSA_profit,
					"Дневная безрисковая доходность" => $BASIC_RISKFREE_RATE_USA,
					"Стандартное отклонение портфеля" => $portfolioUSA_standardDeviation,
					"Стандартное отклонение индекса" => $SP500_standardDeviation,
				);

				$arReturn["USA"]["TREYNOR_CALCULATE"] = array(
					"Бета" => $treynBetaUSA,
					"Бета индекса SP500" => $treynBetaSP500,
					"Ковариация" => $treynCovarUSA,
					"Дисперсия индекса" => $treynVarianceUSA,
				);

				$arReturn["USA"]["SORTINO_CALCULATE"] = array(
					"Корень" => $sortinoTmpUSA,
				);
				//Отдаем в отладку ряды
				$arReturn["LIST_OF_DATA"] = $arSumm;
			}
			unset($arSumm);

			//Считаем коэффициент шарпа
			$arReturn["RUS"]["SHARP"] = array(
				"PORTFOLIO" => round(($portfolio_profit - $BASIC_RISKFREE_RATE) / $portfolio_standardDeviation, 5),
				"INDEX" => round(($IMOEX_profit - $BASIC_RISKFREE_RATE) / $IMOEX_standardDeviation, 5),
			);
			$arReturn["USA"]["SHARP"] = array(
				"PORTFOLIO" => round(($portfolioUSA_profit - $BASIC_RISKFREE_RATE_USA) / $portfolioUSA_standardDeviation, 5),
				"INDEX" => round(($SP500_profit - $BASIC_RISKFREE_RATE_USA) / $SP500_standardDeviation, 5),
			);

			//Считаем коэффициент трейнора
			$arReturn["RUS"]["TREYNOR"] = array(
				"PORTFOLIO" => round(($portfolio_profit - $BASIC_RISKFREE_RATE) / $treynBeta, 5) / 100,
				"INDEX" => round(($IMOEX_profit - $BASIC_RISKFREE_RATE) / $treynBetaIMOEX, 5) / 100,
			);
			//Считаем коэффициент трейнора
			$arReturn["USA"]["TREYNOR"] = array(
				"PORTFOLIO" => round(($portfolioUSA_profit - $BASIC_RISKFREE_RATE_USA) / $treynBetaUSA, 5) / 100,
				"INDEX" => round(($SP500_profit - $BASIC_RISKFREE_RATE_USA) / $treynBetaSP500, 5) / 100,
			);

			//Считаем коэффициент сортино
			$arReturn["RUS"]["SORTINO"] = array(
				"PORTFOLIO" => round($sortino, 5),
				"INDEX" => round($sortinoIMOEX, 5),
			);
			$arReturn["USA"]["SORTINO"] = array(
				"PORTFOLIO" => round($sortinoUSA, 5),
				"INDEX" => round($sortinoSP500USA, 5),
			);

	 	 CLogger::{$logname}('Итоговый расчет time: '.(microtime(true) - $begin).PHP_EOL);
	    CLogger::{$logname}('--------------------'.PHP_EOL);

			return $arReturn;
		}


		/**
		 * Возвращает количества активов по типам для подписей фильтра по типам активов на вкладке оценки портфелей
		 *
		 * @param  int, string   $pid id портфеля
		 *
		 * @return array  массив с ключами по типам акции
		 *
		 * @access public
		 */
		public function getPortfolioActivesCnt($pid=0){
			Global $DB;
			$pid = intval($pid);
			if($pid==0) return array();
			$query = "select `UF_ACTIVE_TYPE`, `UF_ACTIVE_ID`, `UF_HISTORY_CNT`, `UF_HISTORY_DIRECTION` from `portfolio_history` WHERE `UF_PORTFOLIO` = $pid AND `UF_ACTIVE_TYPE`!='currency' ORDER BY `UF_ADD_DATE`";
			$arResult = array();

			$res = $DB->Query($query);

			while($row = $res->Fetch()){
				if($row['UF_HISTORY_DIRECTION']=='PLUS'){
					$arResult[$row['UF_ACTIVE_TYPE']]['ACTIVES'][$row['UF_ACTIVE_ID']] += $row['UF_HISTORY_CNT'];
				} else {
					$arResult[$row['UF_ACTIVE_TYPE']]['ACTIVES'][$row['UF_ACTIVE_ID']] -= $row['UF_HISTORY_CNT'];
				}
			}

			$arTmp = array();
			foreach($arResult as $type=>$actives){
				$arTmp[$type] = 0;
			  foreach($actives['ACTIVES'] as $active){
				 if($active>0){
					$arTmp[$type] ++;
				 }
			  }
			}
			$arResult = $arTmp;

			return $arResult;
		}

		//Возвращает список кодов активов, входящих в указаный id портфеля
		public function getPortfolioActiveCodeList($id) {
			$arReturn          = array();
			$hlblock_id        = 28;
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$arFilter          = array('UF_PORTFOLIO' => $id);
			$rsData            = $entity_data_class::getList(array(
				//'order' => array('UF_ADD_DATE' => 'DESC'),
				'select' => array('UF_ACTIVE_CODE', 'UF_HISTORY_CNT', 'UF_HISTORY_DIRECTION'),
				'filter' => $arFilter,
			));
			while ($row = $rsData->fetch()) {
				if($row['UF_HISTORY_DIRECTION']=='PLUS'){
					$arResult[$row['UF_ACTIVE_CODE']] += $row['UF_HISTORY_CNT'];
				} else {
					$arResult[$row['UF_ACTIVE_CODE']] -= $row['UF_HISTORY_CNT'];
				}
			}

			foreach($arResult as $code=>$cnt){
			  if($cnt>0){
				$arReturn[] = $code;
			  }
			}
			return $arReturn;
		}

		/**
		 * Возвращает список активов портфеля для построения таблицы активов, построения круговой диаграммы и линейного графика
		 *
		 * @param  int   $pid  - ID портфеля
		 * @param  bool   $json (Optional) Возвращает результат в формате json если true
		 * @param  bool   $group (Optional) Группирует активы если true или отдает каждый актив в отдельной строке без группировки
		 * @param  array   $actives_filter (Optional) Фильтрует активы  по типу если передан
		 *
		 * @return array, json
		 *
		 * @access public
		 */
		public function getPortfolioActivesList($pid, $json = true, $group = true, $actives_filter = array(), $resA = false, $resO = false, $resE = false, $resAU = false, $forCalc = false) {
			 $debug = false;
			 //$debug = true;
if($pid == 482296){
    $debug = true;
}
			if (intval($pid) <= 0) {
				return false;
			}
if($debug){
CLogger::{'getPHistory_'.$pid}('');
}
        if(count($actives_filter)>0 && !empty($actives_filter[0])){
			 //$actives_filter = explode(',', $actives_filter);
			 $actives_filter[] = 'currency';
			 $arrFilter["activeType"] = $actives_filter;
			 }
			$this->cleanPortfolioHistory($pid);

			$arTmpHistory            = $this->getPortfolioHistory($pid, $arrFilter);


if($debug){
CLogger::{'getPHistory_'.$pid}(print_r($arTmpHistory, true));
}
			$arTmpHistory            = $this->sortPortfolioHistory($arTmpHistory, 'asc');
			$now_Date                = strtotime(date('d.m.Y'));

			$arActivesCodeTmp     = array();
			//$arActivesBondCodes   = array(); //Фильтр кодов для получения списка купонов всех облиг портфеля
			$arMidPriceTmp        = array(); //Тут складываем даты, кол-ва и цены покупки для получения цен фифо для расчета средневзвешенной при продажах
			$arActivesPricesTable = array();
			$arFilterISIN = array();  //Фильтр по ISIN для расчета амортизации
			$arActivesCnt          = array(); //Кол-во активов и валют расчетное из PLUS минус MINUS
			$arActivesMidPriceData = array(); //Данные по сделкам активов для расчета средневзвешенной цены
			$fifoStartPrice        = 0;
			$fifoStartCnt          = 0;
			$fifoMinusCnt          = 0;
			$orangeIncomeSumm      = 0;
			$arBondsDateCounts = array();//Массив количеств остатков облиг на даты
if($debug){
CLogger::{'Ocenka_midprice_cnt_'.$pid}();
CLogger::{'Ocenka_cache_'.$pid}();
CLogger::{'history_'.$pid}();
CLogger::{'Ocenka_midprice_cnt_'.$pid}('место;Актив;Тикер;Операция;Цена покупки; Кол-во; Сумма; Тек. кол-во; Тек сумма');
CLogger::{'Ocenka_income_'.$pid}();
CLogger::{'Ocenka_count_'.$pid}();
CLogger::{'Ocenka_count_'.$pid}('Дата сделки; id актива; назв. актива; операция; кол-во для сложения');
CLogger::{'Ocenka_midpriceUSD_'.$pid}();
CLogger::{'Ocenka_midpriceUSD_'.$pid}('Дата сделки; id актива; назв. актива; операция; кол-во; Цена лота;');
CLogger::{'history_'.$pid}(print_r($arTmpHistory, true));
}
			foreach ($arTmpHistory as $k => $arHistActive) {
				$start = microtime(true);
				$actId = $arHistActive["UF_ACTIVE_ID"];
				if (!array_key_exists($actId, $arResult)) {
					$arResult[$actId] = $arHistActive;
				}

				$notRUB = false; //Флаг не рублей
				//Собираем фильтр isin для расчета амортизации
				if ($arHistActive["UF_ACTIVE_TYPE"] == 'bond') {
					$arFilterISIN[] = $arHistActive["UF_ACTIVE_CODE"];
				}

				$dealValuteRate = 1;
                $arHistActive["UF_ADD_DATE"] = new DateTime($arHistActive["UF_ADD_DATE"]);
				if ($arHistActive["UF_ACTIVE_TYPE"] == 'currency' && !in_array($arHistActive["UF_SECID"], $this->arRoubleCodes)) {
					if ($arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_PLUS"]) {
						$dt_ = $arHistActive["UF_ADD_DATE"]->format('d.m.Y');
					} else {
						$dt_ = $key;
					}




					$currencySecid = $arHistActive["UF_SECID"];
					if(!empty($arHistActive["UF_ACTIVE_CURRENCY"])){
					 // $currencySecid = $arHistActive["UF_ACTIVE_CURRENCY"];
					}

					$dt_                          = $arHistActive["UF_ADD_DATE"]->format('d.m.Y');
					$arCurrenyRate                = $this->getHistActionsPrices($arHistActive["UF_ACTIVE_TYPE"], $dt_, $currencySecid, '');
					$arHistActive["UF_CURR_RATE"] = $arCurrenyRate["rate"];
					$notRUB                       = true;

					if(!empty($arHistActive["UF_ACTIVE_CURRENCY"]) && !in_array($arHistActive["UF_ACTIVE_CURRENCY"], $this->arRoubleCodes)){
					  $priceCurrency = $this->getHistActionsPrices('currency', $arHistActive["UF_ADD_DATE"], strtoupper($arHistActive["UF_ACTIVE_CURRENCY"]), '');
					  $dealValuteRate = $priceCurrency["price"];
					}
//				$firephp = FirePHP::getInstance(true);
//            $firephp->fb(array("plus"=>$currencySecid,"val"=>$arHistActive["UF_ACTIVE_CURRENCY"], "lotprice"=>$arHistActive["UF_LOTPRICE"], "val_rate"=>$dealValuteRate),FirePHP::LOG);

				}

				  	$arHistActive["UF_LOTPRICE"] = $arHistActive["UF_LOTPRICE"]*$dealValuteRate;


				//$arHistActive["UF_RUB_SUMM_INCOMING"] = $arHistActive["UF_LOTCNT"] * ($notRUB ? $arHistActive["UF_CURR_RATE"] : $arHistActive["UF_LOTPRICE"]);
				$arHistActive["UF_RUB_SUMM_INCOMING"] = $arHistActive["UF_LOTCNT"] * $arHistActive["UF_LOTPRICE"]; //Для валют так же считаем по цене приобретения


				//определяем флаг операций из денежного потока
						$is_cashflow = false;
						if(strpos($arHistActive["UF_DESCRIPTION"], "Амортизация") !== false ||
						   strpos($arHistActive["UF_DESCRIPTION"], "Купон") !== false ||
							strpos($arHistActive["UF_DESCRIPTION"], "Дивиденды")!== false ||
							strpos($arHistActive["UF_DESCRIPTION"], "Налог") !== false
							){
							  $is_cashflow = true;
							}
			//Если есть облиги и соответственно фильтр по их isin кодам - получим доступные купоны
			$arCoupons = array();

			if (count($arFilterISIN) > 0) { //Если есть облигации в портфеле - проверим и рассчитаем их амортизацию
				//Получаем купоны из HL OblCouponsData
				$arCoupons = $this->getBondCouponsForPortfolio($arFilterISIN);
			}


				if ($arHistActive["UF_HISTORY_DIRECTION"] == "PLUS") {
				  //	if (empty($arHistActive["UF_AUTO"])) { //Не обсчитываем списание автоопераций
if($debug) CLogger::{'Ocenka_count_'.$pid}($arHistActive["UF_ADD_DATE"]->format('d.M.Y').';'.$actId.';'.$arHistActive["UF_ACTIVE_NAME"].'; Плюсуем к '.$arActivesCnt[$actId].' + '.$arHistActive["UF_LOTCNT"] .' = '.($arActivesCnt[$actId]+$arHistActive["UF_LOTCNT"]).';'.$arHistActive["UF_LOTCNT"]);

					   //$arActivesCnt[$actId] += round($arHistActive["UF_LOTCNT"], 2);
						$arActivesCnt[$actId] += $arHistActive["UF_LOTCNT"];

						$arActivesMidPriceData[$actId] += $arHistActive["UF_LOTCNT"] * $arHistActive["UF_LOTPRICE"]; //Сумма покупки актива
						$arMidPriceTmp[$actId][] = array("ID"=>$arHistActive["ID"], "UF_ACTIVE_ID"=>$actId, "UF_ADD_DATE"=>$arHistActive["UF_ADD_DATE"], "PRICE" => $arHistActive["UF_LOTPRICE"], "CNT" => $arHistActive["UF_LOTCNT"]);
				  //	}
				 if ($arHistActive["UF_ACTIVE_TYPE"] == 'currency'){
if($debug) CLogger::{'Ocenka_midpriceUSD_'.$pid}($arHistActive["UF_ADD_DATE"]->format('d.M.Y').';'.$actId.';'.$arHistActive["UF_ACTIVE_NAME"].';'.$arHistActive["UF_HISTORY_DIRECTION"].';'.str_replace(".",",",$arHistActive["UF_LOTCNT"]).';'.str_replace(".",",", $arHistActive["UF_LOTPRICE"]));

				  }
if($debug){CLogger::{'Ocenka_midprice_cnt_'.$pid}('+: '.$actId.';'.$arHistActive["UF_SECID"].'; Прибавляем кол-во '.$arHistActive["UF_LOTCNT"]. ' по цене '.str_replace(".",",", $arHistActive["UF_LOTPRICE"]).';'.str_replace(".",",", $arHistActive["UF_LOTPRICE"]).';'.$arHistActive["UF_LOTCNT"].';'.($arHistActive["UF_LOTPRICE"] * $arHistActive["UF_LOTCNT"]).';'.$arActivesCnt[$actId].';'.$arActivesMidPriceData[$actId]);}
				 
					//+Считаем итог суммы вложений (как на оранжевом графике)  //&& !empty($arHistActive["UF_CALCULATED"])
						if($is_cashflow==false && $arHistActive["UF_ACTIVE_TYPE"] == "currency" && $arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_PLUS"] &&
						!empty($arHistActive["UF_ADD_DATE"]) && empty($arHistActive["UF_AUTO"]) && $arHistActive["UF_CLEAR_CACHE"] == 'Y' ){
								$orangeIncomeSumm += $arHistActive["UF_RUB_SUMM_INCOMING"];
if($debug) CLogger::{'Ocenka_income_'.$pid}('+'.$arHistActive["UF_RUB_SUMM_INCOMING"].' = '.$orangeIncomeSumm);

							 }
					//-Считаем итог суммы вложений (как на оранжевом графике)
 				  if ($arHistActive["UF_ACTIVE_TYPE"] == 'bond') {

					 $arBondsDateCounts[$arHistActive['UF_ACTIVE_CODE']]["PLUS"][] = array("ALG"=>0,
					 "UF_ADD_DATE"=>$arHistActive["UF_ADD_DATE"]->format('d.m.Y'),
					 "CNT"=>$arHistActive["UF_LOTCNT"],
					 "AMMORT_SUMM_ON_ADD_DATE"=>$this->getBondAmortValueByDate($arHistActive["UF_ACTIVE_CODE"], $arHistActive["UF_ADD_DATE"])
					 );
				  }
				} else {
					//if (empty($arHistActive["UF_AUTO"])) { //Не обсчитываем списание автоопераций
if($debug) CLogger::{'Ocenka_count_'.$pid}($arHistActive["UF_ADD_DATE"]->format('d.M.Y').';'.$actId.';'.$arHistActive["UF_ACTIVE_NAME"].'; Минусуем из '.$arActivesCnt[$actId].' - '.$arHistActive["UF_LOTCNT"] .' = '.($arActivesCnt[$actId]-$arHistActive["UF_LOTCNT"]).';-'.$arHistActive["UF_LOTCNT"]);
						//$arActivesCnt[$actId] -= round($arHistActive["UF_LOTCNT"],2);
						$arActivesCnt[$actId] -= $arHistActive["UF_LOTCNT"];
					//}
					if ($arHistActive["UF_ACTIVE_TYPE"] == 'currency' && in_array($arHistActive["UF_SECID"], $this->arRoubleCodes) ) {
						$arActivesMidPriceData[$actId] -= $arHistActive["UF_LOTCNT"] * $arHistActive["UF_LOTPRICE"]; //Сумма продажи рублей
	if($debug) CLogger::{'Ocenka_midpriceUSD_'.$pid}($arHistActive["UF_ADD_DATE"]->format('d.M.Y').';'.$actId.';'.$arHistActive["UF_ACTIVE_NAME"].';'.$arHistActive["UF_HISTORY_DIRECTION"].';'.str_replace(".",",",$arHistActive["UF_LOTCNT"]).';'.str_replace(".",",", $arHistActive["UF_LOTPRICE"]));
					}
					if ($arHistActive["UF_ACTIVE_TYPE"] == 'currency' && !in_array($arHistActive["UF_SECID"], $this->arRoubleCodes) ) {
	if($debug) CLogger::{'Ocenka_midpriceUSD_'.$pid}($arHistActive["UF_ADD_DATE"]->format('d.M.Y').';'.$actId.';'.$arHistActive["UF_ACTIVE_NAME"].';'.$arHistActive["UF_HISTORY_DIRECTION"].';'.str_replace(".",",",$arHistActive["UF_LOTCNT"]).';'.str_replace(".",",", $arHistActive["UF_LOTPRICE"]));
					}

					//+Считаем итог суммы вложений (как на оранжевом графике)
					if ($arHistActive["UF_ACTIVE_TYPE"] == "currency" && !empty($arHistActive["UF_ADD_DATE"]) && $arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_MINUS"] &&
						empty($arHistActive["UF_AUTO"]) && $arHistActive["UF_CLEAR_CACHE"] == 'Y'
					) {
						//if($pid==406738){   CLogger::Orange_ocenka_406738('3: '.$arHistActive["UF_SECID"].' - '.$arHistActive["UF_ADD_DATE"].' orangeIncomeSumm='.$orangeIncomeSumm.' - '.($arHistActive["UF_LOTCNT"] * $arHistActive["UF_LOTPRICE"]).' = '.($orangeIncomeSumm-($arHistActive["UF_LOTCNT"] * $arHistActive["UF_LOTPRICE"])));   }
					  	$orangeIncomeSumm -= $arHistActive["UF_LOTCNT"] * $arHistActive["UF_LOTPRICE"]; //изъятие про цене покупки валюты
if($debug) CLogger::{'Ocenka_income_'.$pid}('-'.$arHistActive["UF_RUB_SUMM_INCOMING"].' = '.$orangeIncomeSumm);
					}
					//-Считаем итог суммы вложений (как на оранжевом графике)

 							if ($arHistActive["UF_ACTIVE_TYPE"] == 'bond') {
								$arBondsDateCounts[$arHistActive['UF_ACTIVE_CODE']]["MINUS"][] = array("ALG"=>1,
								"UF_ADD_DATE"=>$arHistActive["UF_ADD_DATE"]->format('d.m.Y'),
								"CNT"=>$arHistActive["UF_LOTCNT"],
								"AMMORT_SUMM_ON_ADD_DATE"=>$this->getBondAmortValueByDate($arHistActive["UF_ACTIVE_CODE"], $arHistActive["UF_ADD_DATE"])
								);
							}



				}

				//  $firephp = FirePHP::getInstance(true);
				// $firephp->fb(array("clearIncomeSumm ".$actId=>$arActivesMidPriceData[$actId]),FirePHP::LOG);

				$arActivesCodeTmp[$arHistActive['UF_ACTIVE_CODE']] = '';
				$arResult[$actId]["CLEAR_INCOME_SUMM"]             = $arActivesMidPriceData[$actId];
				$arResult[$actId]["UF_LOTCNT"]                     = $arActivesCnt[intval($actId)];

				$arActivesPricesTable[$arHistActive["UF_ACTIVE_TYPE"]][$actId] = 0;
			}




			//Обсчитываем минуса для расчета средневзвешенной цены и амортизации
			$mpCounter = array();
			foreach ($arTmpHistory as $k => $arHistActive) {

				$sum_ammort = 0;
				if (($arHistActive["UF_ACTIVE_TYPE"] == 'currency' && in_array($arHistActive["UF_SECID"], $this->arRoubleCodes))) {
					continue;
				}

				$actId = $arHistActive["UF_ACTIVE_ID"];

				if ($arHistActive["UF_HISTORY_DIRECTION"] == "MINUS") {
					if (!array_key_exists($actId, $mpCounter)) {
						$mpCounter[$actId] = 0;
					}
					$counter =  $mpCounter[$actId]; //Счетчик перемещения по сделкам покупок актива $actId
//if($debug){CLogger::{'Ocenka_midprice_cnt_'.$pid}($actId.'; Счетчик '.$counter);}

					$dealPlusCnt   = $arMidPriceTmp[$actId][$counter]["CNT"]; //Количество актива для сделки покупки с номером $counter
					$dealPlusPrice = $arMidPriceTmp[$actId][$counter]["PRICE"]; //Цена актива для сделки покупки с номером $counter

				  	$dealPlusPrice = $dealPlusPrice*$dealValuteRate;

					$minusCnt = $arHistActive["UF_LOTCNT"];
					if ($minusCnt <= $dealPlusCnt) {  //Если продажа умещается в кол-ве купленного уменьшаем в массиве хранящем кол-во покупок на величину продажи вычитаем сумму
						$dealPlusCnt                                      = $dealPlusCnt - $minusCnt;
						$arMidPriceTmp[$actId][$counter]["CNT"] = $dealPlusCnt;
						$arActivesMidPriceData[$actId] -= $dealPlusPrice * $minusCnt; //Расчет
if($debug){CLogger::{'Ocenka_midprice_cnt_'.$pid}('1: '.$actId.';'.$arHistActive["UF_SECID"].'; Вычитаем из пок. '.$counter.': '.$dealPlusCnt.' - '.$minusCnt. ' = '.$arMidPriceTmp[$actId][$counter]["CNT"].';'.$dealPlusPrice.';'.$dealPlusCnt.';'.($dealPlusPrice * $minusCnt).';'.$arMidPriceTmp[$actId][$counter]["CNT"].';'.$arActivesMidPriceData[$actId]);}

						if ($dealPlusCnt == 0) {
							$dealPlusCnt   = $arMidPriceTmp[$actId][$counter]["CNT"];
							$dealPlusPrice = $arMidPriceTmp[$actId][$counter]["PRICE"];
							$mpCounter[$actId]++;
						}
					}
					else if($minusCnt > $dealPlusCnt){   //Если продажа не умещается в кол-ве купленного уменьшаем в массиве,пошагово минусуем количества и считаем суммы

						$arActivesMidPriceData[$actId] -= $dealPlusPrice * $dealPlusCnt; //Расчет
						$tmpCnt                         = $minusCnt - $dealPlusCnt; //Получаем остаток для дальнейшего списания    15.0528-14 = 1.0528
						$arMidPriceTmp[$actId][$counter]["CNT"] = 0; //Из текущей покупки все списали, сохранили остаток ноль
if($debug){CLogger::{'Ocenka_midprice_cnt_'.$pid}('2: '.$actId.';'.$arHistActive["UF_SECID"].'; Вычитаем из пок. '.$counter.': есть '.$dealPlusCnt.' - '.$minusCnt.' = списать из след. покупки '.$tmpCnt.';'.$dealPlusPrice.';'.$dealPlusCnt.';'.($dealPlusPrice * $dealPlusCnt).';'.$arMidPriceTmp[$actId][$counter]["CNT"].';'.$arActivesMidPriceData[$actId] );}
						//Проходим по следующим покупкам, получая цену для расчета минусования суммы
						foreach($arMidPriceTmp[$actId] as $k=>$arPlusDeal){
						  if($k<=$counter) continue; //Игнорируем уже сминусованные покупки
						  if ($tmpCnt == 0) {
						  	break; //Выставляем счетчик на текущий номер. Если досписали в ноль - выходим из цикла
						  }
							$dealPlusCnt   = $arMidPriceTmp[$actId][$k]["CNT"]; //Количество актива для сделки покупки с номером $counter
							$dealPlusPrice = $arMidPriceTmp[$actId][$k]["PRICE"]; //Цена актива для сделки покупки с номером $counter

								if ($tmpCnt <= $dealPlusCnt) { //Если остаток для дальнейшего списания помещается в покупку из текущего шага то минусуем и вычитаем суммы
									$arActivesMidPriceData[$actId] -= $dealPlusPrice * $tmpCnt; //Расчет
									$arMidPriceTmp[$actId][$k]["CNT"] = $dealPlusCnt-$tmpCnt;
if($debug){CLogger::{'Ocenka_midprice_cnt_'.$pid}('3: '.$actId.';'.$arHistActive["UF_SECID"].'; Вычитаем из пок. '.$k.': есть '.$dealPlusCnt.' - '.$tmpCnt. ' = '.$arMidPriceTmp[$actId][$k]["CNT"].';'.$dealPlusPrice.';'.$tmpCnt.';'.($dealPlusPrice * $tmpCnt).';'.$arMidPriceTmp[$actId][$counter]["CNT"].';'.$arActivesMidPriceData[$actId] );}
									$tmpCnt = 0;
									if ($dealPlusCnt-$tmpCnt == 0) {
						  				$mpCounter[$actId]=$k+1; break; //Выставляем счетчик на текущий номер. Если досписали в ноль - выходим из цикла
						  			}
     							} else { //Если остаток для списания не поместился в покупку то вычитаем из тек.покупки ее кол-во по цене этой покупки и продолжаем цикл
									$arActivesMidPriceData[$actId] -= $dealPlusPrice * $dealPlusCnt; //Расчет
if($debug){CLogger::{'Ocenka_midprice_cnt_'.$pid}('4: '.$actId.';'.$arHistActive["UF_SECID"].'; Вычитаем из пок. '.$k.': есть '.$dealPlusCnt.' - '.$tmpCnt. ' = списать из след. покупки '.($tmpCnt-$dealPlusCnt).';'.$dealPlusPrice.';'.$dealPlusCnt.';'.($dealPlusPrice * $dealPlusCnt).';'.$arMidPriceTmp[$actId][$counter]["CNT"].';'.$arActivesMidPriceData[$actId] );}
									$tmpCnt                                           = $tmpCnt - $dealPlusCnt; //Получаем остаток для дальнейшего списания
									$arMidPriceTmp[$actId][$k]["CNT"] = 0; //Из текущей покупки все списали, сохранили остаток ноль
									$mpCounter[$actId] = $k+1;
									}
						}
					}
				}

			}

         //Перевернем массив остатков после вычисления средневзвешенной цены, делаем ключом ID записи покупки HL инфоблока
/* 		$arTmpFinalCounts = array();
			foreach($arMidPriceTmp as $aid=>$arItems){
			  foreach($arItems as $k=>$arItem){
					if($arItem["CNT"]>0){
				   	$arTmpFinalCounts[$aid][] = array("ACTIVE_ID"=>$aid, "UF_ADD_DATE"=>(new DateTime($arItem["UF_ADD_DATE"]))->format('d.m.Y'), "CNT"=>$arItem["CNT"]);
					}
				}
				usort($arTmpFinalCounts[$aid], function ($a, $b) {
				$res = 0;
            // сравниваем даты добавления
				$addDateA = new DateTime($a['UF_ADD_DATE']); $addDateB = new DateTime($b['UF_ADD_DATE']);
				if ($addDateA != $addDateB) {
						return ($addDateA->getTimestamp() < $addDateB->getTimestamp()) ? 1 : -1; //desc
				}
				return $res;
			});
			}*/
            
            if($debug) {
                CLogger::{'Ocenka_arActivesMidPriceData_' . $pid}(print_r($arActivesMidPriceData, true));
            }
			$arTmpHistActive = array();
			foreach ($arResult as $active_key => $arActive) {
				if ($arActive["UF_LOTCNT"] > 0) {
					if ($arActive["UF_ACTIVE_TYPE"] == 'bond') {
					$arActive["REDEMPTION"] = $arBondsDateCounts[$arActive["UF_ACTIVE_CODE"]];
					}

					//Переназначаем дату добавления актива по фактически оставшемуся активу.
					//Т.к. списание по фифо считается только для графика истории, а для оценки считается общий остаток (приход минус расход) то в массиве активов чаще всего лежит
					//самый первый купленный элемент как основа для данных. Соответственно ранее система для построения графика на оценке портфелей прибавляла к дате продажи 2 года,
					//получая крайнюю правую дату для графика и его рассчетов, что могло оказаться некорректным при ситуации если актив купили очень давно, затем еще покупали и продавали..
					//Теперь при итоговом формировании массива активов с общим остатком проверяется максимальная дата добавления для сделок покупки, по которым после расчета продаж по фифо
					//остались ненулевые количества. Именно эта дата и используется в качестве опорной для графика

					if($arActive["UF_ACTIVE_TYPE"] != 'bond' && $arActive["UF_ACTIVE_TYPE"] != 'currency'){
					  	$arActive["UF_GRAPH_DATE"] = (new DateTime())->format('d.m.Y');
					}
					$arActive["CLEAR_INCOME_SUMM"]  = $arActivesMidPriceData[$active_key];
					$arActive["FOOTER_INCOME_SUMM"] = $orangeIncomeSumm;
					$arTmpHistActive[$active_key]   = $arActive;
				}
			}
			$arResult = $arTmpHistActive;
			unset($arTmpHistActive);

   		//Перебираем массив активов и заполняем ранее полученными ценами
			foreach ($arResult as $active_key => $arActive) {
				//Для облигаций находим ближайшие купоны и добавляем их в результат ROOT для вывода на вкладке показателей в портфеле по облигациям
				if ($arActive["UF_ACTIVE_TYPE"] == 'bond') {
					if (array_key_exists($arActive["UF_ACTIVE_CODE"], $arCoupons)) {
						$futureCoupon = '';
						foreach ($arCoupons[$arActive["UF_ACTIVE_CODE"]] as $arCoupon) {
							if (strtotime($arCoupon["Дата купона"]) >= $now_Date) {
								$arResult[$active_key]["LAST_COUPON"] = $arCoupon;
								break;
							}
						}
					}
				} //bond

					//запишем цену для корневого элемнета
					$ActiveRootUF_ACTIVE_CODE = $arActive["UF_ACTIVE_CODE"];
					$oblig_off = 'N';



					if ($arResult[$active_key]["UF_ACTIVE_TYPE"] == 'bond') {
					  if(!isset($arResult[$active_key]["RADAR_DATA"])){
						$arResult[$active_key]["RADAR_DATA"] = $this->resO->getItem($ActiveRootUF_ACTIVE_CODE);
						$arResult[$active_key]["RADAR_DATA"]["LAST_PERIOD"] = reset($arResult[$active_key]["RADAR_DATA"]["PERIODS"]);
						}

						//Считаем данные для построения таблицы в html
						if(!empty($arResult[$active_key]["RADAR_DATA"]['PROPS']) && intval((new DateTime($arResult[$active_key]["RADAR_DATA"]['PROPS']['MATDATE']))->format('Y'))>0)
					  	$oblig_off=(new DateTime($arResult[$active_key]["RADAR_DATA"]['PROPS']['MATDATE']))<=(new DateTime()) || $arResult[$active_key]["RADAR_DATA"]['PROPS']['HIDEN']=='Да'?'Y':'N';
						$arResult[$active_key]["TABLE_DATA"]["oblig-off"] = $oblig_off;

						if (!array_key_exists("Цена покупки", $arResult[$active_key]["RADAR_DATA"])) { //Если в динамике нет ключа "Цена покупки" то посчитаем из свойств облигации
							$arResult[$active_key]["RADAR_DATA"]["DYNAM"]["Цена покупки"] = round(($arResult[$active_key]["RADAR_DATA"]["PROPS"]["FACEVALUE"] / 100) * ($arResult[$active_key]["RADAR_DATA"]["PROPS"]["LEGALCLOSE"] ?: $arResult[$active_key]["RADAR_DATA"]["PROPS"]["LASTPRICE"]), 2);
						}

						//Считаем начальный номинал на дату приобретения актива
						if (array_key_exists("ADD_DATE_AMORT_SUMM", $arResult[$active_key])) {
							$arResult[$active_key]["ADD_DATE_INITIALFACEVALUE"] = floatval($arResult[$active_key]["RADAR_DATA"]["PROPS"]["INITIALFACEVALUE"]) - floatval($arResult[$active_key]["ADD_DATE_AMORT_SUMM"]);
						}

						//Если есть данные по амортизации - считаем  амортизацию
						$ammortSumm = 0;

						if(count($arResult[$active_key]["REDEMPTION"])>0){
						  $activeRedemption = $arResult[$active_key]["REDEMPTION"];
						  if(array_key_exists("MINUS", $activeRedemption)){ //Если облига продавалась то считаем амортизацию по остаткам фифо

						  $arPlus = $this->sortArrayByDate($activeRedemption["PLUS"], 'UF_ADD_DATE', 'asc');
						  $arMinus = $this->sortArrayByDate($activeRedemption["MINUS"], 'UF_ADD_DATE', 'asc');

						  foreach($arMinus as $arMinusVal){
						  	$minusCnt = $arMinusVal["CNT"];
							 $arPlusTmp = array();
							 foreach($arPlus as $kPlus=>$vPlus){
								if($vPlus["CNT"]>$minusCnt){
								  $vPlus["CNT"] = $vPlus["CNT"]-$minusCnt;
								  $arPlusTmp[] = $vPlus;
								  $minusCnt = 0;
								} else if($vPlus["CNT"]==$minusCnt){
								  $minusCnt = 0;
								} else if($vPlus["CNT"]<$minusCnt){
								  $minusCnt = $minusCnt-$vPlus["CNT"];
								}

							 }
							 $arPlus = $arPlusTmp;
						  }
						  	$arResult[$active_key]["REDEMPTION"]["PLUS"] = $arPlus;

						  }
						  if(array_key_exists("PLUS", $arResult[$active_key]["REDEMPTION"])){//Если продаж облиги не было то считаем по данным покупок

						     foreach($arResult[$active_key]["REDEMPTION"]["PLUS"] as $arAmort){
								 if($arAmort["AMMORT_SUMM_ON_ADD_DATE"]>0){
								 	$ammortSumm += ($arAmort["AMMORT_SUMM_ON_ADD_DATE"]*$arAmort["CNT"]);
								 }
						     }

						  }

						}
						$arResult[$active_key]["REDEMPTION_SUMM"] = $ammortSumm;



					} else if ($arResult[$active_key]["UF_ACTIVE_TYPE"] == 'currency' && !isset($arResult[$active_key]["RADAR_DATA"])) {
						$arResult[$active_key]["RADAR_DATA"] = $this->resCUR->getItem($arResult[$active_key]["UF_ACTIVE_CODE"]);
					} else if ($arResult[$active_key]["UF_ACTIVE_TYPE"] == 'share' && !isset($arResult[$active_key]["RADAR_DATA"])) {
						$arResult[$active_key]["RADAR_DATA"]                = $this->resA->getItem($arResult[$active_key]["UF_ACTIVE_CODE"]);
						$arResult[$active_key]["RADAR_DATA"]["LAST_PERIOD"] = reset($arResult[$active_key]["RADAR_DATA"]["PERIODS"]);
					} else if ($arResult[$active_key]["UF_ACTIVE_TYPE"] == 'share_usa' && !isset($arResult[$active_key]["RADAR_DATA"])) {
						$arResult[$active_key]["RADAR_DATA"]                = $this->resAU->getItem($arResult[$active_key]["UF_ACTIVE_CODE"]);
						$arResult[$active_key]["RADAR_DATA"]["LAST_PERIOD"] = reset($arResult[$active_key]["RADAR_DATA"]["PERIODS"]);
					} else if ($arResult[$active_key]["UF_ACTIVE_TYPE"] == 'share_etf' && !isset($arResult[$active_key]["RADAR_DATA"])) {
						$arResult[$active_key]["RADAR_DATA"]                = $this->resE->getItem($arResult[$active_key]["UF_ACTIVE_CODE"]);
						$arResult[$active_key]["RADAR_DATA"]["LAST_PERIOD"] = reset($arResult[$active_key]["RADAR_DATA"]["PERIODS"]);
					}
					if (isset($arResult[$active_key]["RADAR_DATA"]["PERIODS"])) {
						unset($arResult[$active_key]["RADAR_DATA"]["PERIODS"]);
					}
					//Добавляем данные для построения таблицы в html
					$arResult[$active_key]["TABLE_DATA"]["oblig-off"] = $oblig_off;


			}



		  	usort($arResult, function ($a, $b) {
				$res = 0;
				$activesWeight = array("currency"=>0, "share"=>1, "share_etf"=>2, "share_usa"=>3, "bond"=>4);
				// сравниваем тип актива
				$inta = intval($activesWeight[$a["UF_ACTIVE_TYPE"]]); $intb = intval($activesWeight[$b["UF_ACTIVE_TYPE"]]);
				if ($inta != $intb) {
					return ($inta > $intb) ? 1 : -1; //asc
				}

				// сравниваем название по алфавиту
				$stra = strval($a["UF_ACTIVE_NAME"]); $strb = strval($b["UF_ACTIVE_NAME"]);
				if ($stra != $strb) {
					return ($stra > $strb) ? 1 : -1; //asc
				}
				return $res;
			});

			//Получим купоны для облигаций, если таковые есть в портфеле
			//$arPortfolioCoupons = $this->getBondCouponsForPortfolio(array_keys($arActivesBondCodes));
			//Дополняем массив данными по амортизации облигаций
			//$arResult = $this->getCalcAmortization($arResult);

			if ($json) {
				$arResult = json_encode($arResult);
			}
			return $arResult;
		}


		/**
		 * Вычисляет сумму прошедших до даты покупки гашений по сделке
		 *
		 * @var function (){ [add description]
		 *
		 * @access public
		 */
		private function calcCancellationAmount($ufAddDate,$isin,&$arCoupons){
		  $amort_summ_on_date_add = 0;
		  $amort_summ_on_date_add = $this->getCancellationAmount($ufAddDate->format('d.m.Y'), $isin, $arCoupons);
		  return $amort_summ_on_date_add;
		}

		/**
		 * getBondAmortValueByDate - Возвращает сумму амортизации для облигации на дату ее добавления в портфель (дату покупки)
		 * используется в режиме редактирования активов в портфеле при изменении даты добавления, вызывается из js асинхронно
		 *
		 * @param  string   $SECID  SECID-код облигации
		 * @param  string   $addDate  дата добавления в портфель в строковом представлении d.m.Y
		 *
		 * @return float  сумма амортизации
		 *
		 * @access public
		 */
		public function getBondAmortValueByDate($SECID, $addDate) {
                 return $this->getBondAmortValueByDateSql($SECID, $addDate);
                // return $this->getBondAmortValueByDate1($SECID, $addDate);
		}

		public function getBondAmortValueByDate1($SECID, $addDate) {
			$arResult = 0;
			//Получаем купоны из HL OblCouponsData
			$hlblock_id        = 15;
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$arFilter          = array('UF_ITEM' => $SECID, '!UF_DATA' => false, '!=UF_DATA' => '[[]]');
			$arCoupons         = array();
			$rsData            = $entity_data_class::getList(array(
				'order' => array('UF_ITEM' => 'DESC'),
				'select' => array('*'),
				'filter' => $arFilter,
			));
			while ($el = $rsData->fetch()) {
				$arCoupons = json_decode($el['UF_DATA'], true);
			}
			$amort_summ_on_date_add = 0; //Сумма гашений с даты покупки до сегодняшней даты включительно для расчета номинала на дату покупки

			for ($i = 0; $i < count($arCoupons); $i++) { //Пробегаем по купонам
				if (array_key_exists('Сумма гашения', $arCoupons[$i])) { //Если указана сумма гашения - значит погашен и обрабатываем
					//$date     = new DateTime($arCoupons[$i]['Дата']); //Дата гашения купона
					//$add_Date = new DateTime($addDate); //Дата покупки актива
					$date     = strtotime($arCoupons[$i]['Дата']); //Дата гашения купона
					$add_Date = strtotime($addDate); //Дата покупки актива
					$now_Date = strtotime(date('d.m.Y')); //Дата текущая
					if ($date >= $add_Date && $date <= $now_Date) {
						//Суммируем прошедшие до даты покупки гашения
						$amort_summ_on_date_add += floatval($arCoupons[$i]['Сумма гашения']);
					}
				}
			}

			$arResult = floatVal($amort_summ_on_date_add);
			return $arResult;
		}

		function getBondAmortValueByDateSql($SECID, $addDate) {
			if (empty($SECID)) {
				return 0;
			}

			Global $DB;
			$arResult = 0;
			$arCoupons         = array();
			$sql = "SELECT * FROM `hl_obl_coupons_data` WHERE `UF_DATA` != '' AND `UF_ITEM` = '$SECID' AND `UF_DATA` != '[[]]'";
			$rsData = $DB->Query($sql);
			//Получим список купонов по облигации
			$arReturn = array();
			//Получаем купоны из HL OblCouponsData
			while ($el = $rsData->fetch()) {
				$arCoupons = json_decode($el['UF_DATA'], true);
			}
 			$amort_summ_on_date_add = 0; //Сумма гашений с даты покупки до сегодняшней даты включительно для расчета номинала на дату покупки

			for ($i = 0; $i < count($arCoupons); $i++) { //Пробегаем по купонам
				if (array_key_exists('Сумма гашения', $arCoupons[$i])) { //Если указана сумма гашения - значит погашен и обрабатываем
					//$date     = new DateTime($arCoupons[$i]['Дата']); //Дата гашения купона
					//$add_Date = new DateTime($addDate); //Дата покупки актива
					$date     = strtotime($arCoupons[$i]['Дата']); //Дата гашения купона
					$add_Date = strtotime($addDate); //Дата покупки актива
					$now_Date = strtotime(date('d.m.Y')); //Дата текущая
					if ($date >= $add_Date && $date <= $now_Date) {
						//Суммируем прошедшие до даты покупки гашения
						$amort_summ_on_date_add += floatval($arCoupons[$i]['Сумма гашения']);
					}
				}
			}
			unset($rsData);
			$arResult = floatVal($amort_summ_on_date_add);
			return $arResult;
		}


		/**
		 * getCalcAmortization - Дополняет массив с активами портфеля суммой амортизации облиигаций и первоначальным номиналом расчитанным на дату добавления облигации
		 *
		 * @param  array   $arActives  массив активов для портфеля
		 *
		 * @return array дополненный массив активов для портфеля
		 *
		 * @access public
		 */
		public function getCalcAmortization($arActives) {
			$arFilterISIN = array();
			foreach ($arActives as $active_key => $arActive) {
				if ($arActives[$active_key]["UF_ACTIVE_TYPE"] == 'bond') {
					$arFilterISIN[] = $arActives[$active_key]["UF_ACTIVE_CODE"];
				}
			}
			if (count($arFilterISIN) > 0) { //Если есть облигации в портфеле - проверим и рассчитаем их амортизацию
				//Получаем купоны из HL OblCouponsData
				$arCoupons = $this->getBondCouponsForPortfolio($arFilterISIN);

				//Расчет амортизации облигаций с учетом даты покупки
				$nowDate = new DateTime();
				foreach ($arActives as $active_key => $arActive) {
					$sum_ammort = 0;
					if ($arActives[$active_key]["UF_ACTIVE_TYPE"] == 'bond' && array_key_exists($arActives[$active_key]["UF_ACTIVE_CODE"], $arCoupons)) {
						if (count($arActive['ITEMS']) > 0) { //Если несколько сделок
							$mid_buy_summ    = 0; //Средн.взвеш. цена покупки
							$buy_summ        = 0; //Сумма покупки по всем сделкам тек. облигации
							$cnt_summ        = 0; //Суммарное кол-во по всем сделкам тек. облигации
							$now_summ        = 0; //Сумма покупки на тек. момент по всем сделкам тек. облигации
							$mid_ammort      = 0; //Средн.взвеш. гашение
							$mid_summ_ammort = 0; //Сумма гашений
							$common_increase = 0; //Итоговый процентны прирост по усредненным показателям

							foreach ($arActive['ITEMS'] as $ki => $arItem) {
								$amort_summ_on_date_add = 0; //Сумма прошедших до даты покупки гашений для расчета номинала на дату покупки
								$radarData              = $arActives[$active_key]["RADAR_DATA"];

								//Вычисляем сумму прошедших до даты покупки гашений по сделке
								$amort_summ_on_date_add = $this->getCancellationAmount($arActives[$active_key]["ITEMS"][$ki]["UF_ADD_DATE"], $arActives[$active_key]["ITEMS"][$ki]["UF_ACTIVE_CODE"], $arCoupons);

								//Номинал облигации на момент покупки
								$arActives[$active_key]["ITEMS"][$ki]["ADD_DATE_INITIALFACEVALUE"] = floatval($radarData["PROPS"]["INITIALFACEVALUE"]) - $amort_summ_on_date_add;

								//Сумма покупки
								$arActives[$active_key]["ITEMS"][$ki]["BOND_BUY_SUMM"] = $arActives[$active_key]["ITEMS"][$ki]["UF_LOTPRICE"] * $arActives[$active_key]["ITEMS"][$ki]["UF_LOTCNT"];

								//Сумма текущая
								$arActives[$active_key]["ITEMS"][$ki]["BOND_FACEVALUE_SUMM"] = floatval($radarData["PROPS"]["FACEVALUE"]) * $arActives[$active_key]["ITEMS"][$ki]["UF_LOTCNT"];

								//Размер амортизации для одной облигации
								$arActives[$active_key]["ITEMS"][$ki]["BOND_AMORT_ONE"] = $arActives[$active_key]["ITEMS"][$ki]["ADD_DATE_INITIALFACEVALUE"] - $radarData["PROPS"]["FACEVALUE"];
								/*                      echo "<pre  style='color:black; font-size:11px;'>";
								print_r($arActives[$active_key]["ITEMS"][$ki]);
								echo "</pre>";*/
								//Сумма гашения
								$arActives[$active_key]["ITEMS"][$ki]["BOND_AMORT_SUMM"] = $arActives[$active_key]["ITEMS"][$ki]["BOND_AMORT_ONE"] * $arActives[$active_key]["ITEMS"][$ki]["UF_LOTCNT"];

								//Процентный прирост по кол-ву купленных облигаций
								$arActives[$active_key]["ITEMS"][$ki]["BOND_INCREASE"] = round((($arActives[$active_key]["ITEMS"][$ki]["BOND_FACEVALUE_SUMM"] + $arActives[$active_key]["ITEMS"][$ki]["BOND_AMORT_SUMM"]) / $arActives[$active_key]["ITEMS"][$ki]["BOND_BUY_SUMM"] - 1) * 100, 2);

								//Суммирование для расчета средневзвешенных показателей
								$mid_buy_summ += $arActives[$active_key]["ITEMS"][$ki]["BOND_BUY_SUMM"];
								$buy_summ += $arActives[$active_key]["ITEMS"][$ki]["BOND_BUY_SUMM"];
								$cnt_summ += $arActives[$active_key]["ITEMS"][$ki]["UF_LOTCNT"];
								$now_summ += $arActives[$active_key]["ITEMS"][$ki]["BOND_FACEVALUE_SUMM"];
								$sum_ammort += $arActives[$active_key]["ITEMS"][$ki]["BOND_AMORT_SUMM"];
								$mid_summ_ammort += $arActives[$active_key]["ITEMS"][$ki]["BOND_AMORT_SUMM"];
							}

							//$arActives[$active_key]["ITEMS"][$ki]["ADD_DATE_AMORT_SUMM"] = ($amort_summ-$amort_from_add_date);
						} else { //Если единственная сделка
							$mid_buy_summ    = 0; //цена покупки
							$buy_summ        = 0; //Сумма покупки тек. облигации
							$cnt_summ        = 0; //Кол-во тек. облигации
							$now_summ        = 0; //Сумма покупки на тек. момент по облигации
							$mid_ammort      = 0; //Гашение
							$mid_summ_ammort = 0; //Сумма гашений
							$common_increase = 0; //Итоговый процентн прироста

							//Вычисляем сумму гашения
							$amort_summ_on_date_add = $this->getCancellationAmount($arActives[$active_key]["UF_ADD_DATE"], $arActives[$active_key]["UF_ACTIVE_CODE"], $arCoupons);

							$radarData = $arActives[$active_key]["RADAR_DATA"];
							//Номинал облигации на момент покупки
							$arActives[$active_key]["ADD_DATE_INITIALFACEVALUE"] = floatval($radarData["PROPS"]["INITIALFACEVALUE"]) - $amort_summ_on_date_add;

							//Сумма покупки
							$arActives[$active_key]["BOND_BUY_SUMM"] = $arActives[$active_key]["UF_LOTPRICE"] * $arActives[$active_key]["UF_LOTCNT"];

							//Сумма текущая
							$arActives[$active_key]["BOND_FACEVALUE_SUMM"] = floatval($radarData["PROPS"]["FACEVALUE"]) * $arActives[$active_key]["UF_LOTCNT"];

							//Размер амортизации для одной облигации
							$arActives[$active_key]["BOND_AMORT_ONE"] = $arActives[$active_key]["ADD_DATE_INITIALFACEVALUE"] - $radarData["PROPS"]["FACEVALUE"];

							//Сумма гашения
							$arActives[$active_key]["BOND_AMORT_SUMM"] = $arActives[$active_key]["BOND_AMORT_ONE"] * $arActives[$active_key]["UF_LOTCNT"];

							//Процентный прирост по кол-ву купленных облигаций
							$arActives[$active_key]["BOND_INCREASE"] = round((($arActives[$active_key]["BOND_FACEVALUE_SUMM"] + $arActives[$active_key]["BOND_AMORT_SUMM"]) / $arActives[$active_key]["BOND_BUY_SUMM"] - 1) * 100, 2);

							//Суммирование для расчета средневзвешенных показателей
							$mid_buy_summ += $arActives[$active_key]["BOND_BUY_SUMM"];
							$buy_summ += $arActives[$active_key]["BOND_BUY_SUMM"];
							$cnt_summ += $arActives[$active_key]["UF_LOTCNT"];
							$now_summ += $arActives[$active_key]["BOND_FACEVALUE_SUMM"];
							$sum_ammort += $arActives[$active_key]["BOND_AMORT_SUMM"];
							$mid_summ_ammort += $arActives[$active_key]["BOND_AMORT_SUMM"];
						} //Если единственная сделка

						$mid_buy_summ                                  = $mid_buy_summ / $cnt_summ;
						$mid_summ_ammort                               = $mid_summ_ammort / $cnt_summ;
						$common_increase                               = (($now_summ + $sum_ammort) / $buy_summ - 1) * 100;
						$arActives[$active_key]["BOND_MID_BUY_PRICE"]  = $mid_buy_summ;
						$arActives[$active_key]["BOND_BUY_SUMM"]       = $buy_summ;
						$arActives[$active_key]["BOND_CURRENT_SUMM"]   = $now_summ;
						$arActives[$active_key]["BOND_SUM_AMORT"]      = $sum_ammort;
						$arActives[$active_key]["BOND_MID_AMORT_SUMM"] = $mid_summ_ammort;
						$arActives[$active_key]["BOND_AMORT_SUMM"]     = round($common_increase, 2);
					} //if bond and has coupons
				} //foreach actives
			} //if(count($arFilterISIN)>0)

			return $arActives;
		}

		/**
		 * Возвращает массив купонов из HL OblCouponsData для всего портфеля, сгруппированных по ISIN кодам бумаг
		 *
		 * @param  array   $arFilterISIN (Optional) массив ISIN кодов облигаций портфеля
		 *
		 * @return array
		 */
		function getBondCouponsForPortfolio($arFilterISIN = array()) {
			        return $this->getBondCouponsForPortfolioSql($arFilterISIN);
			      //  return $this->getBondCouponsForPortfolio1($arFilterISIN);
		}

		function getBondCouponsForPortfolio1($arFilterISIN = array()) {
			if (count($arFilterISIN) <= 0) {
				return array();
			}

			//Получим список купонов по облигации
			$arReturn = array();
			//Получаем купоны из HL OblCouponsData
			$hlblock_id        = 15;
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$arFilter          = array('UF_ITEM' => $arFilterISIN, '!UF_DATA' => false, '!=UF_DATA' => '[[]]');

			$rsData = $entity_data_class::getList(array(
				'order' => array('UF_ITEM' => 'DESC', "UF_DATA" => "ASC"),
				'select' => array('*'),
				'filter' => $arFilter,
			));
			while ($el = $rsData->fetch()) {
				$arReturn[$el['UF_ITEM']] = json_decode($el['UF_DATA'], true);
			}

			/*       if(in_array("RU000A0JV4L2", $arFilterISIN)){
			define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/coupons_log.txt");
			AddMessage2Log('$arReturn = '.print_r($arReturn, true),'');
			}*/

			return $arReturn;
		}

		function getBondCouponsForPortfolioSql($arFilterISIN = array()) {
			if (count($arFilterISIN) <= 0) {
				return array();
			}
			Global $DB;
			$isinSql = "'".implode("','", $arFilterISIN)."'";
			$sql = "SELECT * FROM `hl_obl_coupons_data` WHERE `UF_DATA` != '' AND `UF_ITEM` IN ($isinSql) AND `UF_DATA` != '[[]]'";
			$rsData = $DB->Query($sql);
			//Получим список купонов по облигации
			$arReturn = array();
			//Получаем купоны из HL OblCouponsData
			while ($el = $rsData->fetch()) {
				$arReturn[$el['UF_ITEM']] = json_decode($el['UF_DATA'], true);
			}
			unset($rsData);
			return $arReturn;
		}

		//Возвращает сумму гашения по облигации с учетом даты покупки
		function getCancellationAmount($addDate = '', $ISIN, $arCoupons) {
			$amort_summ_on_date_add = 0; //Сумма прошедших до даты покупки гашений для расчета номинала на дату покупки
			for ($i = 0; $i < count($arCoupons[$ISIN]); $i++) { //Пробегаем по купонам
				if (array_key_exists('Сумма гашения', $arCoupons[$ISIN][$i])) { //Если указана сумма гашения - значит погашен и обрабатываем
					$date     = new DateTime($arCoupons[$ISIN][$i]['Дата']); //Дата гашения купона
					$add_Date = new DateTime($addDate); //Дата покупки актива
					if ($date <= $add_Date) {
						//Суммируем прошедшие до даты покупки гашения
						$amort_summ_on_date_add += floatval($arCoupons[$ISIN][$i]['Сумма гашения']);
					}
				}
			}
			return $amort_summ_on_date_add;
		}

		/**
		 * Получает актуальную цену актива (акции, облигации) по ID или по Коду бумаги (SECID), если он передан. Формат выходного массива зависит от входящих параметров.
		 *
		 * @param  array        $arActivesPricesTable
		 * @param  array        $arSECID      (Optional) Если передан массив кодов ценных бумаг, то он передается в оба блока кода по акциям и облигациям и получение цены происходит по коду бумаг.
		 *
		 * @return array
		 *
		 * @access public
		 */
		public function getActivesPrices($arActivesPricesTable, $arSECID = array(), $add_bonds = false, $forBacktest = false) {
			$arActivesPricesSECIDTable = array();
			if ($add_bonds == true) {
				if (count($arActivesPricesTable['bond']) > 0 || count($arSECID) > 0) { //Получим цену облигаций на странице облигации - http://fin-plan.org/lk/obligations/RU000A0ZYBS1/ значение "Текущая цена с учетом НКД"
					$arBondFilter = array('IBLOCK_ID' => 27);
					if (count($arSECID) > 0) {
						$arBondFilter["PROPERTY_SECID"] = $arSECID;
					} else {
						$arBondFilter["ID"] = array_keys($arActivesPricesTable['bond']);
					}

					$arSelect = Array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_SECID');
					$dbItem   = CIBlockElement::GetList(Array(), $arBondFilter, false, false, $arSelect);

					while ($arItem = $dbItem->fetch()) {
						$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"), false);
						while ($arProperty = $dbProperty->GetNext()) {
							if (!empty($arProperty['VALUE'])) {
								$arItem['PROPERTIES'][$arProperty['CODE']] = $arProperty['VALUE'];
							}
						}

						$price                                                     = 0;
						$price                                                     = ($arItem['PROPERTIES']['FACEVALUE'] / 100) * (!empty($arItem['PROPERTIES']['LASTPRICE']) ? $arItem['PROPERTIES']['LASTPRICE'] : $arItem['PROPERTIES']['LEGALCLOSE']) + $arItem['PROPERTIES']['ACCRUEDINT'];
						$arActivesPricesTable['bond'][$arItem["ID"]]['PRICE']      = $price;
						$arActivesPricesTable['bond'][$arItem["ID"]]['PROPERTIES'] = $arItem['PROPERTIES'];
						if (count($arSECID) > 0) {
							$arActivesPricesSECIDTable[$arItem['PROPERTIES']["SECID"]]["ACTUAL_PRICE"] = $price;
							$arActivesPricesSECIDTable[$arItem['PROPERTIES']["SECID"]]["PROPERTIES"]   = $arItem['PROPERTIES'];
						}

						unset($price);
					}
				}
			}

			if ((count($arActivesPricesTable['share']) > 0 || count($arActivesPricesTable['share_etf'])>0) || count($arSECID) > 0) { //Получим цену акций на странице акции - http://fin-plan.org/lk/actions/RU0009028674/ значение "Цена акции"
				$arShareFilter = array('IBLOCK_ID' => 32);
				if (count($arSECID) > 0) {
					$arShareFilter["PROPERTY_SECID"] = $arSECID;
				} else {
					$arShareFilter["ID"] = array_keys($arActivesPricesTable['share']);
				}

				$arSelect = Array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_SECID');
				$dbItem   = CIBlockElement::GetList(Array(), $arShareFilter, false, false, $arSelect);

				while ($arItem = $dbItem->fetch()) {
					$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"), false);
					while ($arProperty = $dbProperty->GetNext()) {
						if ($arProperty['VALUE']) {
							$arItem['PROPERTIES'][$arProperty['CODE']] = $arProperty['VALUE'];
						}
					}

					$price = 0;
					$currencyRate = 1;
					if(array_key_exists('CURRENCY', $arItem['PROPERTIES']))
						if ($arItem['PROPERTIES']['CURRENCY'] != 'RUB' || $arItem['PROPERTIES']['CURRENCY'] != 'CUR') {
							$currencyRate = getCBPrice(strtoupper($arItem['PROPERTIES']['CURRENCY']), (new DateTime())->format('d.m.Y'));
						}

					if(!empty($arItem['PROPERTIES']['IS_ETF_ACTIVE']))
						if ($arItem['PROPERTIES']['ETF_CURRENCY'] != 'RUB' || $arItem['PROPERTIES']['ETF_CURRENCY'] != 'CUR') {
							$currencyRate = getCBPrice(strtoupper($arItem['PROPERTIES']['ETF_CURRENCY']), (new DateTime())->format('d.m.Y'));
						}
					$priceTmp = !empty($arItem['PROPERTIES']['LASTPRICE']) ? $arItem['PROPERTIES']['LASTPRICE'] : $arItem['PROPERTIES']['LEGALCLOSE'];

					$price = round(floatval($priceTmp) * $currencyRate, 5);
					$arActivesPricesTable['share'][$arItem["ID"]]["PRICE"]      = $price;
					$arActivesPricesTable['share'][$arItem["ID"]]["PROPERTIES"] = $arItem['PROPERTIES'];

					if (count($arSECID) > 0) {
						if ($forBacktest == true) { //Для бектеста отдаем цену в простом формате
							$arActivesPricesSECIDTable[$arItem['PROPERTIES']["SECID"]] = $price * $arItem['PROPERTIES']['LOTSIZE'];
						} else {
							$arActivesPricesSECIDTable[$arItem['PROPERTIES']["SECID"]]["ACTUAL_PRICE"] = $price;
							$arActivesPricesSECIDTable[$arItem['PROPERTIES']["SECID"]]["PROPERTIES"]   = $arItem['PROPERTIES'];
						}
					}
					unset($price);
				}
			}
			if (count($arActivesPricesTable['share_usa']) > 0 || count($arSECID) > 0) { //Получим цену акций США на странице акции - http://fin-plan.org/lk/actions_usa/RU0009028674/ значение "Цена акции"
				$arShareFilter = array('IBLOCK_ID' => 55);
				if (count($arSECID) > 0) {
					$arShareFilter["PROPERTY_SECID"] = $arSECID;
				} else {
					$arShareFilter["ID"] = array_keys($arActivesPricesTable['share_usa']);
				}

				$arSelect = Array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_SECID');
				$dbItem   = CIBlockElement::GetList(Array(), $arShareFilter, false, false, $arSelect);

				while ($arItem = $dbItem->fetch()) {
					$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"), false);
					while ($arProperty = $dbProperty->GetNext()) {
						if ($arProperty['VALUE']) {
							$arItem['PROPERTIES'][$arProperty['CODE']] = $arProperty['VALUE'];
						}
					}
					$price        = 0;
					$currencyRate = 1;
					if ($arItem['PROPERTIES']['CURRENCY'] != 'RUB' || $arItem['PROPERTIES']['CURRENCY'] != 'CUR') {
						$currencyRate = getCBPrice(strtoupper($arItem['PROPERTIES']['CURRENCY']), (new DateTime())->modify('-1 days')->format('d.m.Y'));
					}

					$price = round(floatval($arItem['PROPERTIES']['LASTPRICE']) * $currencyRate, 3);

					$arActivesPricesTable['share_usa'][$arItem["ID"]]["PRICE"]      = $price;
					$arActivesPricesTable['share_usa'][$arItem["ID"]]["PROPERTIES"] = $arItem['PROPERTIES'];

					if (count($arSECID) > 0) {
						if ($forBacktest == true) { //Для бектеста отдаем цену в простом формате
							$arActivesPricesSECIDTable[$arItem['PROPERTIES']["SECID"]] = $price;
						} else {
							$arActivesPricesSECIDTable[$arItem['PROPERTIES']["SECID"]]["ACTUAL_PRICE"] = $price;
							$arActivesPricesSECIDTable[$arItem['PROPERTIES']["SECID"]]["PROPERTIES"]   = $arItem['PROPERTIES'];
						}
					}
					unset($price);
				}
			}
			if (count($arSECID) > 0) {
				$retValue = $arActivesPricesSECIDTable;
			} else {
				$retValue = $arActivesPricesTable;
			}

			return $retValue;
		}

		//Получает портфель без наполнения по id
		public function getPortfolio($id = 0) {
			$arResult = array();
			$arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "IBLOCK_ID", "PROPERTY_*");
			$arFilter = Array("IBLOCK_ID" => IntVal($this->portfolio_iblockId), "ID" => $id, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1), $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields               = $ob->GetFields();
				$arFields["PROPERTIES"] = $ob->GetProperties();
				$arResult               = $arFields;
			}
			return $arResult;
		}

		//Очистка кеша графика и таблицы истории
		public function cleanGraphDataCache($pid) {
/*			$cache      = new CPHPCache();
			$cache_dir = "/portfolio/port_graph_data";
			$cache_id = 'graph_'.$pid;
			$cache->Clean($cache_id, $cache_dir);*/

		  global $CACHE_MANAGER;
		  $CACHE_MANAGER->ClearByTag("PortfolioHistoryGraph_".$pid);

		}



		/**
		 * Добавляет актив в HL инфоблок 28 с привязкой к портфелю
		 *
		 * @param  int, string  $pid  - ID Портфеля для привязки
		 * @param  array $data  - Массив полей из формы ввода
		 * @param  array $copy_data  - Массив полей копируемого актива
		 * @param  string $cacheActivity  - Признак пополнения или покупки при добавлении валют  (указывает. что актив добавлен (куплен) из окна интерактивно, а не программно)
		 *
		 * @return string    "ok" на латинице - в случае успешного добавления, иначе описание ошибки. Используется для закрытия модального окна добавления актива
		 *
		 * @access public
		 */
		public function addPortfolioActive($pid, $data = array(), $copy_data = array(), $cacheActivity = "", $auto = false, $no_recount_cache = false, $owner='user') {
			$debug = false;
			$debug = true;
			if (intval($pid) > 0 && (count($data) > 0 || count($copy_data) > 0)) {
				if (!isset($data['typeActive']) && isset($data['activeType'])) {
					$data['typeActive'] = $data['activeType'];
				}
				if (count($copy_data) > 0) { //Если добавление вызвано из ф-ции копирования - то в $copy_data лежит массив полей для HL инфоблока
					$arMass = $copy_data;
					//Сформируем значения массива для передачи в историю при добавлении
					$data["cntActive"]   = $copy_data["UF_LOTCNT"];
					$data["priceActive"] = $copy_data["UF_LOTPRICE"];
				} else { //Иначе, если добавляем из радара или интерактивно из всплывалки - то формируем массив полей для  HL инфоблока
				   //Основная валюта
					$activeCurrency = !empty($data['currencyActive']) ? $data['currencyActive'] : $data['currencyEmitent'];

					//Спаренная валюта (от валютных пар, если не равна рублям или если не равна основной валюте)
				   $activeSecondCurrency = strtolower(!empty($data['currencyActiveReal']) ? $data['currencyActiveReal'] : $data['currencyActive']);
					if($activeSecondCurrency==strtolower($activeCurrency)){
					  $activeSecondCurrency = '';
					}



					$arMass = Array(
						'UF_PORTFOLIO' => $pid,
						'UF_ACTIVE_ID' => $data['idActive'],
						'UF_ACTIVE_TYPE' => $data['typeActive'],
						'UF_ACTIVE_CODE' => $data['codeActive'],
						'UF_ACTIVE_NAME' => $data['nameActive'],
						'UF_ACTIVE_URL' => $data['urlActive'],
						'UF_EMITENT_NAME' => $data['nameActiveEmitent'],
						'UF_EMITENT_URL' => $data['urlActiveEmitent'],
						'UF_ADD_DATE' => $data['dateActive'],
						//'UF_PERIOD_INCREASE'   => $data['dateActive'],
						'UF_ACTIVE_LASTPRICE' => $data['lastpriceActive'],
						'UF_CURRENCY' => $activeCurrency,
						'UF_ACTIVE_CURRENCY' => $activeSecondCurrency,
						'UF_INVEST_SUM' => '',
						'UF_LOTCNT' => $data['cntActive'],
						'UF_PRICE_ONE' => $data['price_one'],
						'UF_INLOT_CNT' => $data['lotsize'],
						'UF_LOTPRICE' => $data['priceActive'],
						'UF_SECID' => $data['secid'],
					);
				}

				//  $firephp = FirePHP::getInstance(true);
				//  $firephp->fb(array("before add"=>"", "arMass"=>$arMass),FirePHP::LOG);

				if (isset($data["insert"]) && $data["insert"] == "Y") {
					$cacheActivity = $this->arHistoryActions["CACHE_PLUS"];
				}

				// if ($otvet->isSuccess()) {
				//$data["UF_DEAL_ID"]= $otvet->getId();
				$data["portfolioId"] = $pid;
				//if($data['typeActive']=='currency'){
				$data["UF_HISTORY_ACT"] = $cacheActivity; //Признак действия покупка/внесение. Из попапа всегда покупка.
				//}
				if ($auto && $data['typeActive'] == 'currency') {
					$data["UF_AUTO"] = "Y";
				}

				if (isset($data["insert"]) && $data["insert"] == "Y") {
					$data["UF_AUTO"]        = "";
					$data["UF_CLEAR_CACHE"] = "Y"; //Признак чистого внесения кеша
				}

				if (isset($data["description"]) && !empty($data["description"])) {
					$data["UF_DESCRIPTION"] = $data["description"]; //Добавляем коммент к операции (для внесения от амортизации содержит "Амортизация ОФЗ 29045")
				}

				if(isset($data["actionType"]) && !empty($data["actionType"])){
					$data["UF_CASHFLOW_TYPE"] = $data["actionType"];
				}

				if ($debug) {
					// CLogger::BrokerLoader('pid=' . $pid . ' Добавление в HL ' . print_r($data, true));
				}
				$otvetHistory = $this->addToHistory('PLUS', $data);
				//Добавляем родительский ID записи истории
				$data["UF_PARENT_ID"] = $otvetHistory > 0 ? $otvetHistory : '';

				//Управление кешем портфеля при добавлении актива, если добавляется не валюта или валюта покупается из попапа интерактивно

						if($no_recount_cache==false) {
							$this->cacheController2($pid, $data, 'PLUS', $owner, ($data['subzeroCacheOn']=='Y'?true:false));

						}
						if($no_recount_cache==false || $owner=='user') {
						  $this->clearCacheComplex($pid, 'addPortfolioActive');//Очистка всего кеша по портфелю
						}


				return array("result"=>"success", "message"=>"", "hist_start_date"=>$this->getHistoryStartDate($pid) );
				/*          } else {
			return 'Error: ' . implode(', ', $otvet->getErrors()) . "";
			}*/
			}
		}

		/**
		 * Возвращает историю портфеля
		 * @param $pid
		 * @return array
		 * @throws \Bitrix\Main\ArgumentException
		 * @throws \Bitrix\Main\ObjectPropertyException
		 * @throws \Bitrix\Main\SystemException
		 */
		public function getPortfolioHistory($pid, $arrFilter = ''){
			$cache_id = md5($pid.serialize($arrFilter));
		  	 $cache_dir = "/portfolio/getPortfolioHistory";
		  	 $arResult = array();
			 $obCache = new CPHPCache;
			 $cacheTime = $this->cacheOn?86400*31:0;
			 if($obCache->InitCache($cacheTime, $cache_id, $cache_dir))
			 {
			     $arResult = $obCache->GetVars();
			 }
			 elseif($obCache->StartDataCache())
			 {
			     global $CACHE_MANAGER;
			     $CACHE_MANAGER->StartTagCache($cache_dir);  //Старт тегирования
				  	$arResult = $this->getPortfolioHistoryCached($pid, $arrFilter);
			     $CACHE_MANAGER->RegisterTag("PortfolioHistory_".$pid); //Отметка тегом
			     $CACHE_MANAGER->EndTagCache(); //Финализация тегирования
			     $obCache->EndDataCache($arResult);
			 }

			 return $arResult;
		}

		public function cleanPortfolioHistory($pid){
		  global $CACHE_MANAGER;
		  $CACHE_MANAGER->ClearByTag("PortfolioHistory_".$pid);
		}


		/**
		 * Отдельно получаем список активов в портфеле (без отбора для списка в фильтре по активам) и сохраняем в сессию
		 *
		 * @param  [add type]   $pid [add description]
		 *
		 * @return [add type]  [add description]
		 *
		 * @access public
		 */
		public function getActivesForHistoryFilter($pid){
			Global $DB;
			$arReturn = array();
			$query = "select `UF_ACTIVE_ID`, `UF_ACTIVE_NAME`, `UF_ACTIVE_CODE` from `portfolio_history` WHERE `UF_PORTFOLIO`=".$pid." AND (`UF_HISTORY_DIRECTION`='PLUS' OR `UF_HISTORY_DIRECTION`='MINUS') GROUP BY `UF_ACTIVE_ID` ORDER BY `UF_ACTIVE_NAME` ASC";
			$res = $DB->Query($query);

			while($row = $res->Fetch()){
				$arResult[$row["UF_ACTIVE_ID"]] = array('NAME' => $row["UF_ACTIVE_NAME"], 'CODE' => $row["UF_ACTIVE_CODE"]);
			}
			return $arResult;
		}

		public function getPortfolioHistoryCached($pid, $arrFilter = '') {
			Global $DB;
			$arResult = array();

			if (intval($pid) > 0) {
				if (!$this->resO) {
				 	$this->init_radar_classes();
				}

				$hlblock_id        = 28;
				$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
				$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();
				$arFilter          = array('UF_PORTFOLIO' => $pid, array("LOGIC" => "OR",
					array('=UF_HISTORY_DIRECTION' => 'MINUS'),
					array('=UF_HISTORY_DIRECTION' => 'PLUS'),
				)
				);

				$arActiveTypes = array('share', 'share_usa', 'share_etf', 'bond', 'currency');
				if (!empty($arrFilter['activeType']) ) {
					$arFilter['=UF_ACTIVE_TYPE'] = $arrFilter['activeType'];
				}

				if (!empty($arrFilter['active'])) {
					$arFilter['=UF_ACTIVE_ID'] = $arrFilter['active'];
				}
				if (!empty($arrFilter['date_from'])) {
					//$arFilter['>=UF_ADD_DATE'] = \Bitrix\Main\Type\DateTime::createFromUserTime($arrFilter['date_from']);
					$arFilter['>=UF_ADD_DATE'] = \Bitrix\Main\Type\DateTime::createFromUserTime($arrFilter['date_from'].' 00:00:00');
					$arFilter['<=UF_ADD_DATE'] = \Bitrix\Main\Type\DateTime::createFromUserTime($arrFilter['date_to'].' 23:59:59');
				}

				$rsData = $entity_data_class::getList(array(
					'order' => array('UF_ADD_DATE' => 'ASC'),
					'select' => array('*'),
					'filter' => $arFilter,
					//'cache' => array("ttl" => 3600)
				));
				$arResult = $rsData->fetchAll();


				$currency_current_date = $this->getMonthEndWorkday(date('d.m.Y')); //Получаем ближайший рабочий день или текущий день, если он рабочий

				foreach ($arResult as $k => $v) {
					if ($v["UF_ACTIVE_TYPE"] == 'bond') {
						//Добавляем данные в историю для амортизации
						$arResult[$k]["BOND_SUM_AMORT"] = $this->getBondAmortValueByDate($v["UF_SECID"], $v["UF_ADD_DATE"]);
						} else {
						$arResult[$k]["BOND_SUM_AMORT"] = 0;
						}

						if ($arResult[$k]["UF_ACTIVE_TYPE"] == 'bond') {
							$arResult[$k]["RADAR_DATA"] = $this->resO->getItem($arResult[$k]["UF_ACTIVE_CODE"]);
							if (!array_key_exists("Цена покупки", $arResult[$k]["RADAR_DATA"])) { //Если в динамике нет ключа "Цена покупки" то посчитаем из свойств облигации
								$arResult[$k]["RADAR_DATA"]["DYNAM"]["Цена покупки"] = round(($arResult[$k]["RADAR_DATA"]["PROPS"]["FACEVALUE"] / 100) * ($arResult[$k]["RADAR_DATA"]["PROPS"]["LEGALCLOSE"] ?: $arResult[$k]["RADAR_DATA"]["PROPS"]["LASTPRICE"]), 2);
							}

							//Считаем начальный номинал на дату приобретения актива
							if (array_key_exists("ADD_DATE_AMORT_SUMM", $arResult[$k])) {
								$arResult[$k]["ADD_DATE_INITIALFACEVALUE"] = floatval($arResult[$k]["RADAR_DATA"]["PROPS"]["INITIALFACEVALUE"]) - floatval($arResult[$k]["ADD_DATE_AMORT_SUMM"]);
							}

							$activeCurrencyId = $arResult[$k]['UF_CURRENCY']; //Валюта актива
							if (!empty($arResult[$k]["RADAR_DATA"]['PROPS']['CURRENCYID'])) {
								$activeCurrencyId = $arResult[$k]["RADAR_DATA"]['PROPS']['CURRENCYID']; //Валюта актива
							}
							$currency_current_rate = getCBPrice(strtoupper($activeCurrencyId == 'SUR' ? 'RUB' : $activeCurrencyId), $currency_current_date); //Получаем курс на ближайший рабочий или сегодняшний день
							$price_real            = ($arResult[$k]["RADAR_DATA"]['PROPS']['FACEVALUE'] / 100) * (floatval($arResult[$k]["RADAR_DATA"]['PROPS']['LASTPRICE']) > 0 ? $arResult[$k]["RADAR_DATA"]['PROPS']['LASTPRICE'] : $arResult[$k]["RADAR_DATA"]['PROPS']['LEGALCLOSE']);
							if (floatval($arResult[$k]["RADAR_DATA"]['PROPS']['ACCRUEDINT']) > 0) { //НКД
								$price_real = $price_real + floatval($arResult[$k]["RADAR_DATA"]['PROPS']['ACCRUEDINT']);
							}
							$price_real                                                                                   = $price_real * $currency_current_rate;
							$arResult[$k]["RADAR_DATA"]["DYNAM"]["Цена покупки для амортизации"] = round($price_real * $arResult[$k]["UF_LOTCNT"], 2);
							$arResult[$k]["RADAR_DATA"]["LAST_PERIOD"] = reset($arResult[$k]["RADAR_DATA"]["PERIODS"]);
						} else if ($arResult[$k]["UF_ACTIVE_TYPE"] == 'currency') {
							if(!empty($arResult[$k]["UF_ACTIVE_CURRENCY"]) && !in_array($arResult[$k]["UF_ACTIVE_CURRENCY"], $this->arRoubleCodes)){
							  $currency_current_rate = getCBPrice(strtoupper($arResult[$k]["UF_ACTIVE_CURRENCY"]), $currency_current_date);
							  $arResult[$k]["UF_BUY_CURRENCY_RATE"] = $currency_current_rate;
							}

							$arResult[$k]["RADAR_DATA"] = $this->resCUR->getItem($arResult[$k]["UF_ACTIVE_CODE"]);
						} else if ($arResult[$k]["UF_ACTIVE_TYPE"] == 'share') {
							$arResult[$k]["RADAR_DATA"]                = $this->resA->getItem($arResult[$k]["UF_ACTIVE_CODE"]);
							$arResult[$k]["RADAR_DATA"]["LAST_PERIOD"] = reset($arResult[$k]["RADAR_DATA"]["PERIODS"]);
						} else if ($arResult[$k]["UF_ACTIVE_TYPE"] == 'share_usa') {
							$arResult[$k]["RADAR_DATA"]                = $this->resAU->getItem($arResult[$k]["UF_ACTIVE_CODE"]);
							$arResult[$k]["RADAR_DATA"]["LAST_PERIOD"] = reset($arResult[$k]["RADAR_DATA"]["PERIODS"]);
						} else if ($arResult[$k]["UF_ACTIVE_TYPE"] == 'share_etf') {
							$arResult[$k]["RADAR_DATA"]                = $this->resE->getItem($arResult[$k]["UF_ACTIVE_CODE"]);
							$arResult[$k]["RADAR_DATA"]["LAST_PERIOD"] = reset($arResult[$k]["RADAR_DATA"]["PERIODS"]);
						}
						unset($arResult[$k]["RADAR_DATA"]["PERIODS"]);

				}

			}
			//$APPLICATION->portfolioHistory[$pid] = $arResult;
			return $arResult;
		}


		/**
		 * Сортирует записи массива по значениям ключа $dateKey
		 *
		 * @param  array    $array Массив для сортировки
		 * @param  string   $dateKey Ключ по которому в переданном массиве лежит дата в формате "d.m.Y"
		 * @param  string   $dateSort (Optional) Направление сортировки ask/desc
		 *
		 * @return array  Отсортированный массив с данными
		 *
		 * @access public
		 */
		public function sortArrayByDate($array, $dateKey = 'UF_ADD_DATE', $dateSort = 'asc'){
			//Форматируем массив вкладывая сопутствующие операции как дочерние элементы к основным операциям для дальнейшей раздельной сортировки
			$arArrayWithChild = array();
			usort($array, function ($a, $b) use ($dateSort) {
				$res = 0;
            // сравниваем даты добавления
				$addDateA = new DateTime($a['UF_ADD_DATE']); $addDateB = new DateTime($b['UF_ADD_DATE']);
				if ($addDateA != $addDateB) {
					//return ($addDateA < $addDateB) ? 1 : -1; //desc
					if ($dateSort == 'desc') {
						return ($addDateA < $addDateB) ? 1 : -1; //desc
					} else {
						return ($addDateA > $addDateB) ? 1 : -1; //asc
					}
				}

				// сравниваем ID
				$inta = intval($a["ID"]); $intb = intval($b["ID"]);
				if ($inta != $intb) {
					return ($inta < $intb) ? 1 : -1; //desc
				}
				return $res;
			});

			return $array;
		}

		/*
		 * Сортировка массива по двум параметрам с помощью usort()
		 */
		public function sortPortfolioHistory($array, $dateSort = 'desc') {
			//Форматируем массив вкладывая сопутствующие операции как дочерние элементы к основным операциям для дальнейшей раздельной сортировки
			$arArrayWithChild = array();
/*			foreach ($array as $arItem) {
				if (empty($arItem["UF_PARENT_ID"])) {
					if (!array_key_exists("CHILD", $arArrayWithChild[$arItem["ID"]])) {
						$arItem["CHILD"] = array();
					}
					$arArrayWithChild[$arItem["ID"]] = $arItem;
				} else {
					$arArrayWithChild[$arItem["UF_PARENT_ID"]]["CHILD"][] = $arItem;
				}
			}*/

		  //	$array = $arArrayWithChild;

			usort($array, function ($a, $b) use ($dateSort) {
				$res = 0;

				// сравниваем даты добавления
				$addDateA = new DateTime($a['UF_ADD_DATE']); $addDateB = new DateTime($b['UF_ADD_DATE']);
				if ($addDateA != $addDateB) {
					//return ($addDateA < $addDateB) ? 1 : -1; //desc
					if ($dateSort == 'desc') {
						return ($addDateA->getTimestamp() < $addDateB->getTimestamp()) ? 1 : -1; //desc
					} else {
						return ($addDateA->getTimestamp() > $addDateB->getTimestamp()) ? 1 : -1; //asc
					}
				}

				// сравниваем ID
				$inta = intval($a["ID"]); $intb = intval($b["ID"]);
				if ($inta != $intb) {
					return ($inta < $intb) ? 1 : -1; //desc
				}



				//Сортируем сопутствующие сделки если они есть для элемента a
/*				if (count($a["CHILD"])) {
					usort($a["CHILD"], function ($ac, $bc) {
						$res = 0;
						// сравниваем ID
						$intac = intval($ac["ID"]); $intbc = intval($bc["ID"]);
						if ($intac != $intbc) {
							return ($intac > $intbc) ? 1 : -1; //asc
						}
						return $res;
					});
				}*/

				//Сортируем сопутствующие сделки если они есть для элемента b
/*				if (count($b["CHILD"])) {
					usort($b["CHILD"], function ($ac, $bc) {
						$res = 0;
						// сравниваем ID
						$intac = intval($ac["ID"]); $intbc = intval($bc["ID"]);
						if ($intac != $intbc) {
							return ($intac > $intbc) ? 1 : -1; //asc
						}
						return $res;
					});
				}*/

				// сравниваем буквы
				/*          $var1 = preg_replace('~[0-9]+~', '', $a);
				$var2 = preg_replace('~[0-9]+~', '', $b);
				$compare = strcmp( $var1, $var2 ); // А считается меньше Б
				if( $compare !== 0 ){
				return ( $compare > 0 ) ? 1 : -1;
				}*/

				return $res;
			});

/*			$arArrayLinear = array(); //Перекладываем вложенный массив с CHILD в линейный без вложенных CHILD
			foreach ($array as $arItem) {
				$arChildTmp = array();
				if (count($arItem["CHILD"]) > 0) {
					$arChildTmp = $arItem["CHILD"];
					unset($arItem["CHILD"]);
				}
				$arArrayLinear[] = $arItem;
				foreach ($arChildTmp as $arChildItem) {
					$arArrayLinear[] = $arChildItem;
				}
			}
			$array = $arArrayLinear;*/

			return $array;
		}

		/**
		 * Возвращает для портфеля id сделок из отчетов брокера, статус незавершенных, версию парсера и isin актива
		 * @param $pid
		 * @return array
		 * @throws \Bitrix\Main\ArgumentException
		 * @throws \Bitrix\Main\ObjectPropertyException
		 * @throws \Bitrix\Main\SystemException
		 */
		public function getPortfolioReportIdFinished($pid, $dealIdKeys = false) {
			Global $DB;
			$arResult = array();
			if (intval($pid) > 0) {
				$hlblock_id        = 28;
				$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
				$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();
				$arFilter          = array('UF_PORTFOLIO' => $pid, '!UF_REPORT_DEAL_ID' => false);
				$rsData            = $entity_data_class::getList(array(
					'order' => array('UF_ADD_DATE' => 'ASC'),
					'select' => array('ID', 'UF_REPORT_DEAL_ID', 'UF_PARSER_VER', 'UF_FINISHED', 'UF_ACTIVE_CODE', 'UF_ACTIVE_ID', 'UF_ACTIVE_TYPE'),
					'filter' => $arFilter,
					//'cache' => array("ttl" => 3600)
				));

				while ($item = $rsData->fetch()) {
					if ($dealIdKeys) {
						$arResult[$item["UF_REPORT_DEAL_ID"]] = $item;
					} else {
						$arResult[] = $item;
					}
				}
			}
			//CLogger::getPortfolioReportIdFinished(print_r($arResult, true));

			return $arResult;
		}

		/**
		 * Возвращает дату начала истории портфеля (дату первого добавления актива)
		 * @param $pid
		 * @return array
		 * @throws \Bitrix\Main\ArgumentException
		 * @throws \Bitrix\Main\ObjectPropertyException
		 * @throws \Bitrix\Main\SystemException
		 */
		public function getHistoryStartDate($pid) {
			Global $DB;
			$arResult = (new DateTime())->format('d.m.Y');
			if (intval($pid) > 0) {
				$hlblock_id        = 28;
				$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
				$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();
				$arFilter          = array('UF_PORTFOLIO' => $pid);
				$rsData            = $entity_data_class::getList(array(
					'order' => array('UF_ADD_DATE' => 'ASC'),
					'select' => array('ID', 'UF_ADD_DATE'),
					'filter' => $arFilter,
					'limit' => 1,
					'order' => array("UF_ADD_DATE" => "ASC")
					//'cache' => array("ttl" => 3600)
				));

				while ($item = $rsData->fetch()) {
					$arResult = (new DateTime($item['UF_ADD_DATE']))->format('d.m.Y');
				}

/*			$trace = debug_backtrace();
			$caller = $trace[1];

			$firephp = FirePHP::getInstance(true);
			$firephp->fb(array("getHistoryStartDate"=>$caller['function'], "pid"=>$pid, "date"=>$arResult),FirePHP::LOG);*/

			}

			return $arResult;
		}

		//Выбирает из переданного массива портфеля нужный актив по коду
		public function getPortfolioElemBySecid($portfolioActives, $code) {
			$arReturn = array();
			foreach ($portfolioActives as $aid => $aval) {
				if ($aval["UF_SECID"] == $code || $aval["UF_ACTIVE_CODE"] == $code) {
					$arReturn = $aval;
					break;
				}
			}
			return $arReturn;
		}

		/**
		 * Возвращает собранные данные для графика истории портфеля, включая дивиденды, купоны
		 *
		 * @param  [add type]   $pid [add description]
		 * @param  [add type]   $arFilter (Optional)
		 * @param  [add type]   $clearCache (Optional)
		 *
		 * @return [add type]  [add description]
		 *
		 * @access public
		 */
		public function getHistoryDataForGraph($pid, $arFilter = array(), $clearCache = false, $debug = false) {
		 // $arResult = $this->getHistoryDataForGraphCached($pid, $arFilter);
		 // return $arResult;
			//$debug = false;
			if(in_array($pid, array(510307))){
				$debug = true;
			}
			//$debug = true;

			$life_time = ($this->cacheOn && !$debug)?86400*31:0;
			$cache_dir = "/portfolio/port_graph_data";
			$cache_id = 'graph_'.$pid; //. md5(serialize($arFilter));
			$from = '';
if($debug){
CLogger::{'history_load_'.$pid}();
}
$begin = microtime(true);

			 $obCache = new CPHPCache;
			 if($obCache->InitCache($life_time, $cache_id, $cache_dir))
			 {
			     $arResult = $obCache->GetVars();
				  $from = 'cache';
			 }
			 elseif($obCache->StartDataCache())
			 {
			     global $CACHE_MANAGER;
			     $CACHE_MANAGER->StartTagCache($cache_dir);  //Старт тегирования
				  	$arResult = $this->getHistoryDataForGraphCached($pid, $arFilter, $debug);
			     $CACHE_MANAGER->RegisterTag("PortfolioHistoryGraph_".$pid); //Отметка тегом
			     $CACHE_MANAGER->EndTagCache(); //Финализация тегирования
			     $obCache->EndDataCache($arResult);
				  $from = 'real';
			 }
if($debug){
CLogger::{'history_load_'.$pid}('from: '.$from.' time: '.(microtime(true) - $begin));
}


			//$arResult = $this->getHistoryDataForGraphCached($pid, $arFilter);
			return $arResult;
		}

		public function getHistoryDataForGraphCached($pid, $arFilter = array(), $debug=false) {
			$arResult      = array();
			$arCacheEvents = array(); //Планируемый доход
			$arCoupons     = array();

		  if($debug){
			 CLogger::{'YellowSumm_'.$pid}();
			 CLogger::{'GreenSumm_'.$pid}();
			 CLogger::{'GreenSumm_'.$pid}('место;дата;валюта;тикер;актив;движение;количество;сумма;цена р.;Цена вал.;Курс;Дата курса;код валюты');
			 CLogger::{'OrangeSumm_'.$pid}();
			 CLogger::{'OrangeSumm_'.$pid}('Операция;Дата;сумма;');
			}

			if (count($_REQUEST['filter']) > 0) {
				foreach ($_REQUEST['filter'] as $k => $val) {
					$key = key($val);
					if ($key == 'active') {
						$arFilter[$key] = $val[$key];
					}
					if ($key == 'activeCode') {
						$arFilter[$key] = $val[$key];
					}
					if ($key == 'date_from' || $key == 'date_to') {
						$arFilter[$key] = $val[$key];
					}
				}
			} else {
				$arFilter = array();
			}

			if(!array_key_exists('date_from', $arFilter)){
				$arFilter['date_from'] = $this->getHistoryStartDate($pid);
			}
			if(!array_key_exists('date_from', $arFilter)){
				$arFilter['date_to'] = (new DateTime())->format('d.m.Y');
			}

//$firephp = FirePHP::getInstance(true);
//$firephp->fb(array("getHistoryDataForGraphCached"=>$arFilter),FirePHP::LOG);
			//Если фильтуем по конкретному активу - то для зеленого графика отсекаем все операции с кешем по флагу $is_filteredByActive
			$is_filteredByActive = (array_key_exists('active', $arFilter) && !empty($arFilter['active'])) ? true : false;

			$arTmpHistory = $this->getPortfolioHistory($pid, $arFilter); //0.09 - 1.3 s
		if($debug){
			CLogger::getHistoryFiltered(print_r($arTmpHistory, true));
		}
			$arResultHistory = array(); //Сюда складываем движения за каждый месяц

			if (count($arTmpHistory) == 0) {
				return $arResult;
			}

			$startdate = (new DateTime(reset($arTmpHistory)["UF_ADD_DATE"]->format('d.m.Y')))->modify('first day of this month');  //01.01.2021 Считает в общую шкалу

			$period = new DatePeriod(
				$startdate,
				new DateInterval('P1M'),
				new DateTime('first day of next month')
			);
			$nowDate = new DateTime();
			foreach ($period as $key => $value) {
/*			  $firephp = FirePHP::getInstance(true);
           $firephp->fb(array("label"=>$value->format('d.m.Y')),FirePHP::LOG);*/
				if ($value->getTimestamp() < $nowDate->getTimestamp()) {
					$arResult[$value->format('01.m.Y')] = array();
				}
			}
			$arResult[(new DateTime())->format('d.m.Y')] = array(); //Добавляем в конец графика текущее число

			//Собираем фильтры для получения цен на даты действий с активами портфелей
			$arActionPricesRus      = array();
			$arCurrenciesPricesRus  = array();
			$arObligationsPricesRus = array();
			$arActionPricesUsa      = array();

			$arActions    = array();
			$arActionsUsa = array();
			$arBonds      = array();

			//Получаем массив кодов активов для выборки по дивидендам и купонам
			$arCodes = array();

//$firephp = FirePHP::getInstance(true);
			foreach ($arTmpHistory as $k => $arElement) {
				$operateDate = new DateTime($arElement["UF_ADD_DATE"]->format('d.m.Y'));

				//Получаем массив кодов активов для выборки по дивидендам и купонам
				if ($arElement["UF_ACTIVE_TYPE"] == 'share' || $arElement["UF_ACTIVE_TYPE"] == 'share_etf') {
					$arActionPricesRus[$arElement["UF_SECID"]][$operateDate] = $arElement["UF_DATE_PRICE_ONE"];
					$arActions[$arElement["UF_ACTIVE_ID"]]               = $arElement["UF_ACTIVE_CODE"];
				} else if ($arElement["UF_ACTIVE_TYPE"] == 'bond') {
					$arObligationsPricesRus[$arElement["UF_SECID"]][$operateDate] = $arElement["UF_DATE_PRICE_ONE"];
					$arBonds[$arElement["UF_ACTIVE_ID"]]                      = $arElement["UF_ACTIVE_CODE"];
				} else if ($arElement["UF_ACTIVE_TYPE"] == 'share_usa') {
					$arActionPricesUsa[$arElement["UF_SECID"]][$operateDate] = $arElement["UF_DATE_PRICE_ONE"];
					$arActionsUsa[$arElement["UF_ACTIVE_ID"]]            = $arElement["UF_ACTIVE_CODE"];
				} else if ($arElement["UF_ACTIVE_TYPE"] == 'currency') {
					$arCurrenciesPricesRus[$arElement["UF_SECID"]][$operateDate] = $arElement["UF_DATE_PRICE_ONE"];
				}
				if (!array_key_exists($arElement["UF_SECID"], $portfolioActives)) {
					$portfolioActives[$arElement["UF_SECID"]] = array("UF_SECID" => $arElement["UF_SECID"], "UF_ACTIVE_CODE" => $arElement["UF_ACTIVE_CODE"], "UF_ACTIVE_URL" => $arElement["UF_ACTIVE_URL"], "UF_LOTCNT" => $arElement["UF_LOTCNT"], "UF_INLOT_CNT" => $arElement["UF_INLOT_CNT"], "UF_EMITENT_NAME" => $arElement["UF_EMITENT_NAME"], "UF_EMITENT_URL" => $arElement["UF_EMITENT_URL"]);
				}




				//$diff        = date_diff($operateDate, $nowDate)->format("%a");
  					$date1=strtotime($arElement["UF_ADD_DATE"]->format('d.m.Y'));
					$date2=strtotime($nowDate->format('d.m.Y'));
					$diff= $date2-$date1;
					$diff = floor($diff/(60*60*24));
				//if(($operateDate < $nowDate)==true){
				if (intval($diff) > 0 && ($operateDate->format('m.Y') != $nowDate->format('m.Y'))) {
					$dt  = clone $operateDate;
	  //			 	$firephp->fb(array("dt1"=>$dt->format('d.m.Y'), "date_diff"=>$diff),FirePHP::LOG);
					$key = $dt->modify('first day of this month')->modify('+1 month')->format('01.m.Y');
	  //			 	$firephp->fb(array("dt2"=>$dt->format('d.m.Y'), "date_diff"=>$diff, "key"=>$key),FirePHP::LOG);
					unset($dt);
				} else {
					$key = ((new DateTime())->format('d.')) . (new DateTime($arElement["UF_ADD_DATE"]->format('d.m.Y')))->format('m.Y');
				}

				   // $firephp->fb(array("op"=>$operateDate->format('d.m.Y'), "add"=>$arElement["UF_ADD_DATE"]->format('d.m.Y'), "date_diff"=>$diff, "cmp"=>($operateDate < $nowDate), "key"=>$key),FirePHP::LOG);
				//$is_amortization = strpos($arElement["UF_DESCRIPTION"], "Амортизация") !== false ? true : false;
				if ($arElement["UF_ACTIVE_TYPE"] != "currency") {
					//$prefix = ($arElement["UF_HISTORY_DIRECTION"]=='PLUS'?'Покупка':'Продажа');
					$eventText = $arElement["UF_HISTORY_ACT"] . ' ' . $arElement["UF_ACTIVE_NAME"];
				} else {
					$eventText = $arElement["UF_HISTORY_ACT"] . ' ' . $arElement["UF_ACTIVE_NAME"];
					if (!empty($arElement["UF_DESCRIPTION"]) && !empty($arElement["UF_CASHFLOW_TYPE"])) {
						$eventText = $arElement["UF_DESCRIPTION"];
					}
					//$prefix = ($arElement["UF_HISTORY_DIRECTION"]=='PLUS'?'Пополнение':'Расход');
				}

				$arResultHistory[$key][] = array("UF_ACTIVE_CODE" => $arElement["UF_ACTIVE_CODE"],
					"UF_ACTIVE_TYPE" => $arElement["UF_ACTIVE_TYPE"],
					"UF_SECID" => $arElement["UF_SECID"],
					"UF_ADD_DATE" => $arElement["UF_ADD_DATE"]->format('d.m.Y'),
					"UF_LOTCNT" => $arElement["UF_LOTCNT"],
					"UF_INLOT_CNT" => $arElement["UF_INLOT_CNT"],
					"UF_LOTPRICE" => $arElement["UF_LOTPRICE"],
					"UF_DATE_PRICE_ONE" => $arElement["UF_DATE_PRICE_ONE"],
					"UF_HISTORY_DIRECTION" => $arElement["UF_HISTORY_DIRECTION"],
					"UF_HISTORY_ACT" => $arElement["UF_HISTORY_ACT"],
					"UF_AUTO" => $arElement["UF_AUTO"],
					"UF_CLEAR_CACHE" => $arElement["UF_CLEAR_CACHE"]
				);

				if ($arElement["UF_ACTIVE_TYPE"] == 'currency' && empty($arElement["UF_AUTO"])) {
/*					if ($is_filteredByActive && $arElement["UF_ACTIVE_CODE"] == $arFilter['activeCode']) {
						if(!in_array($eventText, $arResult[$key]["EVENTS"][$arElement["UF_SECID"]]))
						  $arResult[$key]["EVENTS"][$arElement["UF_SECID"]][] = $eventText; //. ' ' . $arElement["UF_HISTORY_SUM"];
					} else if (!$is_filteredByActive) {*/
						if(!in_array($eventText, $arResult[$key]["EVENTS"][$arElement["UF_SECID"]]))
						  $arResult[$key]["EVENTS"][$arElement["UF_SECID"]][] = $eventText; //. ' ' . $arElement["UF_HISTORY_SUM"];
					//}
				}
				if ($arElement["UF_ACTIVE_TYPE"] != 'currency' && !empty($arElement["UF_ACTIVE_NAME"])) {
					if(!in_array($eventText, $arResult[$key]["EVENTS"][$arElement["UF_SECID"]]))
						$arResult[$key]["EVENTS"][$arElement["UF_SECID"]][] = $eventText;
				}

				$arResult[$key]["EVENTS_CHISTORY"] = array();

				$notRUB = false; //Флаг не рублей
				if ($arElement["UF_ACTIVE_TYPE"] == 'currency' && !in_array($arElement["UF_SECID"], $this->arRoubleCodes)) {
					if ($arElement["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_PLUS"]) {
						$dt_ = $arElement["UF_ADD_DATE"]->format('d.m.Y');
					} else {
						$dt_ = $key;
					}
					$dt_                       = $arElement["UF_ADD_DATE"]->format('d.m.Y');
					$arCurrenyRate             = $this->getHistActionsPrices($arElement["UF_ACTIVE_TYPE"], $dt_, $arElement["UF_SECID"], '');
					$arElement["UF_CURR_RATE"] = $arCurrenyRate["rate"];
					$notRUB                    = true;
				}

				$arAddElement = array("UF_ACTIVE_CODE" => $arElement["UF_ACTIVE_CODE"],
					"UF_ACTIVE_TYPE" => $arElement["UF_ACTIVE_TYPE"],
					"UF_ACTIVE_NAME" => $arElement["UF_ACTIVE_NAME"],
					"UF_ADD_DATE" => $arElement["UF_ADD_DATE"]->format('d.m.Y'),
					"UF_SECID" => $arElement["UF_SECID"],
					"UF_LOTCNT" => $arElement["UF_LOTCNT"],
					"UF_INLOT_CNT" => $arElement["UF_INLOT_CNT"],
					"UF_LOTPRICE" => $arElement["UF_LOTPRICE"],
					"UF_DATE_PRICE_ONE" => $arElement["UF_DATE_PRICE_ONE"],
					"UF_HISTORY_DIRECTION" => $arElement["UF_HISTORY_DIRECTION"],
					"UF_HISTORY_ACT" => $arElement["UF_HISTORY_ACT"],
					//"UF_RUB_SUMM_INCOMING" => $arElement["UF_LOTCNT"] * ($notRUB ? $arElement["UF_CURR_RATE"] : $arElement["UF_LOTPRICE"]),
					"UF_RUB_SUMM_INCOMING" => $arElement["UF_LOTCNT"] * $arElement["UF_LOTPRICE"],
					"UF_INCOMING_DATE" => $dt_,
					"UF_CURR_RATE" => $arElement["UF_CURR_RATE"],
					"UF_AUTO" => $arElement["UF_AUTO"],
					"UF_CALCULATED" => $arElement["UF_CALCULATED"],
					"UF_CLEAR_CACHE" => $arElement["UF_CLEAR_CACHE"],
					"UF_DESCRIPTION" => $arElement["UF_DESCRIPTION"],
					"UF_CURRENCY" => $arElement["UF_CURRENCY"],
					"UF_ACTIVE_CURRENCY" => $arElement["UF_ACTIVE_CURRENCY"],
					"UF_CASHFLOW_TYPE" => $arElement["UF_CASHFLOW_TYPE"]);
				 if($arElement["UF_ACTIVE_TYPE"]=='bond'){
				 	$arAddElement["MATDATE"] = $arElement["RADAR_DATA"]['PROPS']['MATDATE'];
	  			   $arAddElement["HIDEN"] = $arElement["RADAR_DATA"]['PROPS']['HIDEN'];
				 }
					$arResult[$key]["ACTIVES"][] = $arAddElement;
					unset($arAddElement);
			}

			$pid = str_replace("portfolio_", "", $pid);

			$st_Date = $startdate->format('d.m.Y');
//	  		$this->getDividendsData($arCacheEvents, array_values($arActions), $st_Date, $pid);
//	  	  	$this->getDividendsUsaData($arCacheEvents, array_values($arActionsUsa), $st_Date, $pid, true);
//	  		$this->getCoupons($arCacheEvents, array_values($arBonds), $st_Date, $pid);

			//Очищаем
			$endDate = (new DateTime($key));

			//Перебираем результирующие даты и дополняем их значениями для зеленого графика
			$lastActives      = array();
			$lastIncomeSumm   = 0;
			$lastBlueSumm     = 0;
			$greenDealsSumm   = 0;
			$blueCacheSumm    = 0;
			$orangeIncomeSumm = 0;
			$yellowIncomeCnt  = array();
			$yellowLastCnt    = array();
			$currPeriodIncome = array();

			/*       $firephp = FirePHP::getInstance(true);
			$firephp->fb($arResult,FirePHP::LOG);*/
			$orangeCumulative = array();

			foreach ($arResult as $kdate => $data) {
				$arResult[$kdate]["ALL_CACHE_SUMM"] = 0;
				if (count($data["ACTIVES"]) <= 0) {
					$arResult[$kdate]["ACTIVES"]          = $lastActives;
					$arResult[$kdate]["ALL_INCOMES_SUMM"] = $lastIncomeSumm;
				} else {
  //$firephp = FirePHP::getInstance(true);
  //$firephp->fb(array("real ".$kdate=>$arResult[$kdate]["ACTIVES"], "last ".$kdate=>$lastActives, "ALL_INCOMES_SUMM"=>$lastIncomeSumm),FirePHP::LOG);

					$arResult[$kdate]["ACTIVES"] = array_merge($arResult[$kdate]["ACTIVES"], $lastActives);
				}

				if (!array_key_exists("EVENTS", $arResult[$kdate])) {
					$arResult[$kdate]["EVENTS"] = array();
				}
				if (!array_key_exists("EVENTS_CHISTORY", $arResult[$kdate])) {
					$arResult[$kdate]["EVENTS_CHISTORY"]   = array();
					$arResult[$kdate]["ALL_CHISTORY_SUMM"] = $lastBlueSumm;
				}

				$greenDealsSumm   = 0;
				$blueCacheSumm    = 0;
				$yellowIncomeCnt  = array();
				$orangeIncomeSumm = 0;
				$arPrices         = array();

				foreach ($arResult[$kdate]["ACTIVES"] as $arHistActive) {
					$histAddDate = (array_key_exists("UF_ADD_DATE", $arHistActive) ? $arHistActive["UF_ADD_DATE"] : $kdate);

					if ((new DateTime($kdate))->format('m.Y') == (new DateTime())->format('m.Y')) {
						$histAddDate = $kdate;
					}

					/*+ Расчет оранжевого графика */
					//Если не рублевая валюта - получаем ее курс на дату и используем для формирования графика
					$notRUB = false; //Флаг не рублей
					if ($arHistActive["UF_ACTIVE_TYPE"] == 'currency' && !in_array($arHistActive["UF_SECID"], $this->arRoubleCodes)) {
						if ($arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_PLUS"]) {
							$dt = $arHistActive["UF_ADD_DATE"];
						} else {
							$dt = $kdate;
						}
						$arCurrenyRate                    = $this->getHistActionsPrices($arHistActive["UF_ACTIVE_TYPE"], $dt, $arHistActive["UF_SECID"], '');
						$arHistActive["UF_LOTPRICE_RATE"] = $arCurrenyRate["rate"];
						$notRUB                           = true;
					}

					if ($arHistActive["UF_HISTORY_DIRECTION"] == "PLUS") {
						//Считаем плюс кеша (желтый график)
						if ($arHistActive["UF_ACTIVE_TYPE"] == 'currency') {
							if ($arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["ACT_PLUS"] ||
								($arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_PLUS"] && empty($arHistActive["UF_AUTO"]))) {
	  //							$yellowIncomeCnt[$arHistActive["UF_SECID"]] += $arHistActive["UF_LOTCNT"];
 						}
							if ($arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_PLUS"]) {
								$arHistActive["UF_LOTPRICE_RATE"] = $arHistActive["UF_DATE_PRICE_ONE"];
							}
						}
						//определяем флаг операций из денежного потока
						$is_cashflow = false;
						if($arHistActive["UF_CASHFLOW_TYPE"] === "dividends" ||
						   $arHistActive["UF_CASHFLOW_TYPE"] === "coupon" ||
						   $arHistActive["UF_CASHFLOW_TYPE"] === "bond_redemption"
							){
							  $is_cashflow = true;
							}
 //$firephp = FirePHP::getInstance(true);
 //$firephp->fb(array("kdate"=>$kdate, "is_cashflow"=>$is_cashflow,"UF_CASHFLOW_TYPE"=>$arHistActive["UF_CASHFLOW_TYPE"]),FirePHP::LOG);

																					  //&& empty($arHistActive["UF_CALCULATED"])
						if($is_cashflow==false && $arHistActive["UF_ACTIVE_TYPE"] == "currency" && $arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_PLUS"] &&
							!empty($arHistActive["UF_ADD_DATE"]) && $arHistActive["UF_CLEAR_CACHE"] == "Y" && empty($arHistActive["UF_AUTO"])){
								$orangeIncomeSumm += $arHistActive["UF_RUB_SUMM_INCOMING"];
		if($debug)	CLogger::{'OrangeSumm_'.$pid}('+;'.$arHistActive["UF_ADD_DATE"].';'.$arHistActive["UF_RUB_SUMM_INCOMING"].';');
							 }
						//-
					} else if ($arHistActive["UF_HISTORY_DIRECTION"] == "MINUS") {
						//Считаем минус кеша (желтый график)
						if ($arHistActive["UF_ACTIVE_TYPE"] == 'currency') {
							if ($arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["ACT_MINUS"] ||
								($arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_MINUS"] && empty($arHistActive["UF_AUTO"]))
							) {
	 //							$yellowIncomeCnt[$arHistActive["UF_SECID"]] -= $arHistActive["UF_LOTCNT"];
 						}                                                                                                                                                                  //&& empty($arHistActive["UF_AUTO"])
							if (!empty($arHistActive["UF_ADD_DATE"]) && $arHistActive["UF_HISTORY_ACT"] == $this->arHistoryActions["CACHE_MINUS"]  && $arHistActive["UF_CLEAR_CACHE"] == "Y" && empty($arHistActive["UF_AUTO"])) {
							  	$orangeIncomeSumm -= $arHistActive["UF_LOTCNT"] * $arHistActive["UF_LOTPRICE"]; //изъятие по цене покупки валюты
		if($debug)	CLogger::{'OrangeSumm_'.$pid}('+;'.$arHistActive["UF_ADD_DATE"].';'.$arHistActive["UF_RUB_SUMM_INCOMING"].';');
							}
						}
					}
					/*- Расчет оранжевого графика */

					if ($arHistActive["UF_ACTIVE_TYPE"] == 'share' || $arHistActive["UF_ACTIVE_TYPE"] == 'share_etf' || $arHistActive["UF_ACTIVE_TYPE"] == 'currency') {
						if (intval($arHistActive["UF_INLOT_CNT"]) == 0) {
							$resATmp                      = $arHistActive["UF_ACTIVE_TYPE"] != 'currency' ? $this->resA : $this->resCUR;
							$arActive                     = $resATmp->getItem($arElement["UF_ACTIVE_CODE"]);
							$arHistActive["UF_INLOT_CNT"] = $arActive["PROPS"]["LOTSIZE"];
							unset($arActive, $resATmp);
						}
						if (!array_key_exists($arHistActive["UF_SECID"], $arPrices)) {
							$price                               = $this->getHistActionsPrices($arHistActive["UF_ACTIVE_TYPE"], $kdate, $arHistActive["UF_SECID"], '',$arHistActive["UF_CURRENCY"]);
							$arPrices[$arHistActive["UF_SECID"]] = $price;
							$arResult[$kdate]["REAL_PRICES"][]   = array("UF_ACTIVE_TYPE" => $arHistActive["UF_ACTIVE_TYPE"], "UF_SECID" => $arHistActive["UF_SECID"], "PRICE" => $price);
						} else {
							$price = $arPrices[$arHistActive["UF_SECID"]];
						}

/*						if ($is_filteredByActive == true) {
							if ($arHistActive["UF_HISTORY_DIRECTION"] == "PLUS") {
								if ($arHistActive["UF_ACTIVE_TYPE"] != 'currency') {
									$greenDealsSumm += ($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]) * floatval($price["price"]);
								}
							} else {
								if ($arHistActive["UF_ACTIVE_TYPE"] != 'currency') {
									$greenDealsSumm -= ($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]) * floatval($price["price"]);
								}
							}
						} else {*/
							if ($arHistActive["UF_HISTORY_DIRECTION"] == "PLUS") {
								$greenDealsSumm += ($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]) * floatval($price["price"]);
  if($debug) CLogger::{'GreenSumm_'.$pid}('1;'.$kdate.';'.$arHistActive["UF_CURRENCY"].';'.$arHistActive["UF_SECID"].';'.$arHistActive["UF_ACTIVE_NAME"].';PLUS;'.($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]).';'.($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]) * floatval($price["price"]).';'.$price["price"]);
							} else {
						    //	if($arHistActive["UF_ACTIVE_TYPE"] != 'currency'){
								$greenDealsSumm -= ($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]) * floatval($price["price"]);
						    //	}
  if($debug) CLogger::{'GreenSumm_'.$pid}('2;'.$kdate.';'.$arHistActive["UF_CURRENCY"].';'.$arHistActive["UF_SECID"].';'.$arHistActive["UF_ACTIVE_NAME"].';MINUS;'.($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]).';'.($arHistActive["UF_INLOT_CNT"] * $arHistActive["UF_LOTCNT"]) * floatval($price["price"]).';'.$price["price"]);

							}
						//}
					} else if ($arHistActive["UF_ACTIVE_TYPE"] == 'bond') {

						$oblig_off=(new DateTime($arHistActive['MATDATE']))<=(new DateTime()) || $arHistActive['HIDEN']=='Да'?true:false;
						$price = $this->getHistActionsPrices($arHistActive["UF_ACTIVE_TYPE"], $kdate, $arHistActive["UF_SECID"], $arHistActive["UF_ACTIVE_CODE"]);
						$tmpPrice = $price["price"];
						if (!empty(strtoupper($arHistActive["UF_CURRENCY"])) && !in_array(strtoupper($arHistActive["UF_CURRENCY"]), $this->arRoubleCodes)) {
							//$priceCurrency = $this->getHistActionsPrices('currency', $kdate, 'USD');
							$priceCurrency["rate"] = getCurrency_rate(strtoupper($arHistActive["UF_CURRENCY"]), $kdate)["CURS"];
							if ($priceCurrency["rate"]) {
								$tmpPrice = $price["price"] * $priceCurrency["rate"];
							}
						}

						if (!empty(strtoupper($arHistActive["UF_ACTIVE_CURRENCY"])) && !in_array(strtoupper($arHistActive["UF_ACTIVE_CURRENCY"]), $this->arRoubleCodes)) {
							//$priceCurrency = $this->getHistActionsPrices('currency', $kdate, 'USD');
							$priceCurrency["rate"] = getCurrency_rate(strtoupper($arHistActive["UF_ACTIVE_CURRENCY"]), $kdate)["CURS"];
							if ($priceCurrency["rate"]) {
								$tmpPrice = $price["price"] * $priceCurrency["rate"];
								//$firephp = FirePHP::getInstance(true);
								//$firephp->fb(array("bond"=>$arHistActive["UF_ACTIVE_NAME"],"UF_ACTIVE_CURRENCY"=>$arHistActive["UF_ACTIVE_CURRENCY"]),FirePHP::LOG);

							}
						}
						$price["price"] = $tmpPrice;
						unset($tmpPrice);


						$arResult[$kdate]["REAL_PRICES"][] = array("UF_ACTIVE_TYPE" => $arHistActive["UF_ACTIVE_TYPE"], "UF_SECID" => $arHistActive["UF_SECID"], "PRICE" => $price);
				  if($oblig_off==false)
						if ($arHistActive["UF_HISTORY_DIRECTION"] == "PLUS") {
							//   if($pid==406736 && $kdate==(new DateTime())->format('d.m.Y')){  CLogger::Green_graph_406736('3; '.$arHistActive["UF_SECID"].'; '.(new DateTime($arHistActive["UF_ADD_DATE"]))->format('d.m.Y').'; '.$price["price"].'; '.$greenDealsSumm.'; '.(($arHistActive["UF_LOTCNT"]) * floatval($price["price"])).'; '.($greenDealsSumm+(($arHistActive["UF_LOTCNT"]) * floatval($price["price"]))));   }
							$greenDealsSumm += ($arHistActive["UF_LOTCNT"]) * floatval($price["price"]);
  if($debug) CLogger::{'GreenSumm_'.$pid}('3;'.$kdate.';'.$arHistActive["UF_CURRENCY"].';'.$arHistActive["UF_SECID"].';'.$arHistActive["UF_ACTIVE_NAME"].';PLUS;'.$arHistActive["UF_LOTCNT"].';'.($arHistActive["UF_LOTCNT"]) * floatval($price["price"]).';'.$price["price"].';'.$price["price_valute"].';'.$price["rate"].';'.$price["rate_date"].';'.$price["currencyId"]);
						} else {
							//  if($pid==406736 && $kdate==(new DateTime())->format('d.m.Y')){  CLogger::Green_graph_406736('4; '.$arHistActive["UF_SECID"].'; '.(new DateTime($arHistActive["UF_ADD_DATE"]))->format('d.m.Y').'; '.$price["price"].'; '.$greenDealsSumm.'; -'.(($arHistActive["UF_LOTCNT"]) * floatval($price["price"])).'; '.($greenDealsSumm-(($arHistActive["UF_LOTCNT"]) * floatval($price["price"]))));   }
							$greenDealsSumm -= ($arHistActive["UF_LOTCNT"]) * floatval($price["price"]);
  if($debug) CLogger::{'GreenSumm_'.$pid}('4;'.$kdate.';'.$arHistActive["UF_CURRENCY"].';'.$arHistActive["UF_SECID"].';'.$arHistActive["UF_ACTIVE_NAME"].';MINUS;'.$arHistActive["UF_LOTCNT"].';'.($arHistActive["UF_LOTCNT"]) * floatval($price["price"]).';'.$price["price"].';'.$price["price_valute"].';'.$price["rate"].';'.$price["rate_date"].';'.$price["currencyId"]);
						}

						//      if($pid==403979)
					} else if ($arHistActive["UF_ACTIVE_TYPE"] == 'share_usa') {
						$price = $this->getHistActionsPrices($arHistActive["UF_ACTIVE_TYPE"], $kdate, $arHistActive["UF_SECID"], '',$arHistActive["UF_CURRENCY"]);
						//  $arActionPricesUsa[$arElement["UF_SECID"]][$addDate] = $price;
						$arResult[$kdate]["REAL_PRICES"][] = array("UF_ACTIVE_TYPE" => $arHistActive["UF_ACTIVE_TYPE"], "UF_SECID" => $arHistActive["UF_SECID"], "PRICE" => $price);
						if ($arHistActive["UF_HISTORY_DIRECTION"] == "PLUS") {
							//  if($pid==406736 && $kdate==(new DateTime())->format('d.m.Y')){  CLogger::Green_graph_406736('5; '.$arHistActive["UF_SECID"].';'.(new DateTime($arHistActive["UF_ADD_DATE"]))->format('d.m.Y').'; '.$price["price"].'; '.$greenDealsSumm.'; '.(($arHistActive["UF_LOTCNT"]) * floatval($price["price"])).'; '.($greenDealsSumm+(($arHistActive["UF_LOTCNT"]) * floatval($price["price"]))));   }
							$greenDealsSumm += ($arHistActive["UF_LOTCNT"]) * floatval($price["price"]);                                                                                                                                             //цена р.;Цена вал.;Курс;Дата курса;код валюты
  if($debug) CLogger::{'GreenSumm_'.$pid}('5;'.$kdate.';'.$arHistActive["UF_CURRENCY"].';'.$arHistActive["UF_SECID"].';'.$arHistActive["UF_ACTIVE_NAME"].';PLUS;'.$arHistActive["UF_LOTCNT"].';'.($arHistActive["UF_LOTCNT"]) * floatval($price["price"]).';'.$price["price"].';'.$price["price_valute"].';'.$price["rate"].';'.$price["rate_date"].';'.$price["currencyId"]);
						} else {
							//   if($pid==406736 && $kdate==(new DateTime())->format('d.m.Y')){  CLogger::Green_graph_406736('6; '.$arHistActive["UF_SECID"].';'.(new DateTime($arHistActive["UF_ADD_DATE"]))->format('d.m.Y').'; '.$price["price"].'; '.$greenDealsSumm.'; -'.(($arHistActive["UF_LOTCNT"]) * floatval($price["price"])).'; '.($greenDealsSumm-(($arHistActive["UF_LOTCNT"]) * floatval($price["price"]))));   }
							$greenDealsSumm -= ($arHistActive["UF_LOTCNT"]) * floatval($price["price"]);
  if($debug) CLogger::{'GreenSumm_'.$pid}('6;'.$kdate.';'.$arHistActive["UF_CURRENCY"].';'.$arHistActive["UF_SECID"].';'.$arHistActive["UF_ACTIVE_NAME"].';MINUS;'.$arHistActive["UF_LOTCNT"].';'.($arHistActive["UF_LOTCNT"]) * floatval($price["price"]).';'.$price["price"].';'.$price["price_valute"].';'.$price["rate"].';'.$price["rate_date"].';'.$price["currencyId"]);

						}

						// if($pid==403979)
					}
				}

				//События по денежным потокам в плюс.
				//Налоги и комиссии подтягиваются в событиях с активами ниже

				if (floatval($arResult[$kdate]["ALL_INCOMES_SUMM"]) == 0) {
					//$arResult[$kdate]["ALL_INCOMES_SUMM"] =  $lastIncomeSumm;
				}

				$lastActives = array();
				foreach ($arResult[$kdate]["ACTIVES"] as $lastAct) {
					unset($lastAct["UF_ADD_DATE"]);
					// if(!isset($lastAct["UF_ADD_DATE"]) && ($lastAct["UF_HISTORY_ACT"]==$this->arHistoryActions["CACHE_MINUS"] || $lastAct["UF_HISTORY_ACT"]==$this->arHistoryActions["CACHE_PLUS"])) continue;
					$lastActives[] = $lastAct;
				}

				//Оранжевый
				if ($lastIncomeSumm == 0) {
					$arResult[$kdate]["ALL_INCOMES_SUMM"]  = round($orangeIncomeSumm, 2);
					$arResult[$kdate]["LAST_INCOMES_SUMM"] = round($orangeIncomeSumm, 2);
				} else {
					$arResult[$kdate]["ALL_INCOMES_SUMM"]  = $lastIncomeSumm + round($orangeIncomeSumm, 2);
					$arResult[$kdate]["LAST_INCOMES_SUMM"] = $lastIncomeSumm;
				}
				$lastIncomeSumm = $arResult[$kdate]["ALL_INCOMES_SUMM"];

				//Желтый кеш

				 //Определяем режим получения итогов кеша на дату
				 //Если обсчитываемая дата равна текущей (в крайней правой точке графика) - то получаем итог по кешу включая текущую дату
				 //Если в любой более левой точке то получаем итог кеша без учета тика на графике
				 if((new DateTime($kdate))->getTimestamp()===(new DateTime(date("d.m.Y")))->getTimestamp()){
				 //if((new DateTime($kdate))===(new DateTime(date()))){
				 	$getExcludeCurrDate = false;
				 }else {
					$getExcludeCurrDate = true;
				 }

				$arYellowCache = $this->getCacheOnBeginDate($pid,$kdate, array(),$getExcludeCurrDate); //Получаем кеш исключая текущую дату
				//$arYellowCache = $this->getCacheOnBeginDate($pid,$kdate, array(),false); //Получаем кеш включая текущую дату
				//if((new DateTime($kdate))->format('d')=='01' && count($arYellowCache)>0){
				//Если внесение кеша попадает на 1 число или старт графика - то обнуляем массив кеша на эту дату, что бы он не рисовался желтым сразу слева на графике
				if((new DateTime($kdate))->getTimestamp() == ($startdate)->getTimestamp() && count($arYellowCache)>0){
				  	$arYellowCache = array();
				}
//$firephp = FirePHP::getInstance(true);
//$firephp->fb(array("dt"=>$kdate, "excl"=>$getExcludeCurrDate, "keyD"=>(new DateTime($kdate))->getTimestamp(), "currD"=>(new DateTime(date("d.m.Y")))->getTimestamp(), "Y"=>$arYellowCache),FirePHP::LOG);

				//$arResult[$el['UF_ACTIVE_CODE']] = array('ID'=>$el['ID'], 'SUMM'=>$el['UF_HISTORY_SUM'], 'RUB_SUMM'=>$rub_summ, 'RATE'=>$el['UF_PRICE_ONE']);
			  //	$arResult[$kdate]["ALL_CACHE_SUMM"] = 0;
	if($debug){
	 CLogger::{'YellowSumm_'.$pid}($kdate.' на начало = '.$arResult[$kdate]["ALL_CACHE_SUMM"]);
	 CLogger::{'YellowSumm_'.$pid}($kdate.' запрос arYellowCache '.print_r($arYellowCache, true));
	}
				foreach($arYellowCache as $currencyKey=>$arCache){
					if(!in_array($currencyKey, $this->arRoubleCodes)){
						//$arCurrenyRate             = $this->getHistActionsPrices('currency', $kdate, $currencyKey);
						//$arCurrenyRate             = getCBPrice($currencyKey,$kdate);
//CLogger::{'YellowSumm_'.$pid}($kdate.' запрос arCurrenyRate '.print_r($arCurrenyRate, true));
						//$arResult[$kdate]["ALL_CACHE_SUMM"] = $arResult[$kdate]["ALL_CACHE_SUMM"] + floatval($arCache['SUMM'])*$arCurrenyRate["rate"];
						$arResult[$kdate]["ALL_CACHE_SUMM"] = $arResult[$kdate]["ALL_CACHE_SUMM"] + floatval($arCache['RUB_SUMM']);
 if($debug) CLogger::{'YellowSumm_'.$pid}($kdate.' + '.$arCache['RUB_SUMM'].' = '.$arResult[$kdate]["ALL_CACHE_SUMM"]);
					} else {
						$arResult[$kdate]["ALL_CACHE_SUMM"] = $arResult[$kdate]["ALL_CACHE_SUMM"] + floatval($arCache['SUMM']);
 if($debug) CLogger::{'YellowSumm_'.$pid}($kdate.' + '.$arCache['SUMM'].' = '.$arResult[$kdate]["ALL_CACHE_SUMM"]);
					}

				}

				$arResult[$kdate]["ALL_CACHE_SUMM"] = round($arResult[$kdate]["ALL_CACHE_SUMM"], 4);

				$arResult[$kdate]["ALL_DEALS_SUMM"] = round($greenDealsSumm, 2);

/*				if ($blueCacheSumm > 0) { //Если есть купоны или дивы на конец месяца дополняем накопление
					$arResult[$kdate]["ALL_CHISTORY_SUMM"] = round($blueCacheSumm + $lastBlueSumm, 2);
					$lastBlueSumm                          = $arResult[$kdate]["ALL_CHISTORY_SUMM"];
				} else { //Иначе используем для графика прошлое значение
					$arResult[$kdate]["ALL_CHISTORY_SUMM"] = round($lastBlueSumm, 2);
				}*/
			}


			foreach ($arResult as $kdate => $val) {
				$arResult[$kdate]["ALL_CHISTORY_SUMM"] = round($arResult[$kdate]["ALL_CHISTORY_SUMM"] + $arResult[$kdate]["ALL_DEALS_SUMM"], 2);
			}

			$arTmp = array();
			$arFinalEventsTmp = array();

			uksort($arResult, function ($key1, $key2) {
				return new DateTime($key1) <=> new DateTime($key2);
			});

			foreach($arResult as $kdate => $val){
				$arALL_DEALS_SUMM[] = $arResult[$kdate]["ALL_DEALS_SUMM"];
				$arALL_CHISTORY_SUMM[] = $arResult[$kdate]["ALL_CHISTORY_SUMM"];
				$arALL_INCOMES_SUMM[] = $arResult[$kdate]["ALL_INCOMES_SUMM"];
				$arALL_CACHE_SUMM[] = $arResult[$kdate]["ALL_CACHE_SUMM"];
				//Собираем названия событий для всплывающей подсказки графика
				$arEventsTmp[$kdate] = array();
	 		 //	$firephp = FirePHP::getInstance(true);
	 		 //	$firephp->fb(array("kdate"=>$kdate, "EVENTS"=>$arResult[$kdate]["EVENTS"], "EVENTS_TMP"=>$arEventsTmp[$kdate] ),FirePHP::LOG);
				foreach($arResult[$kdate]["EVENTS"] as $ek=>$ev){
					if(count($ev)>1){
					  $arEventsTmp[$kdate] = array_merge($arEventsTmp[$kdate], array_values($ev));
					} else {
						foreach(array_values($ev) as $eventValue)
					  		$arEventsTmp[$kdate][] = $eventValue;
					}
				}
				$arFinalEventsTmp[$kdate] = array_merge($arEventsTmp[$kdate], $arResult[$kdate]["EVENTS_CHISTORY"]);
			}

			 $tmpCurrentLabels = array_keys($arResult);
			 $arResult = array();
			 $arResult["currentLabels"] = $tmpCurrentLabels;
			 $arResult["EVENTS"] = $arFinalEventsTmp;
			 $arResult["ALL_DEALS_SUMM"] = $arALL_DEALS_SUMM;
			 $arResult["ALL_CHISTORY_SUMM"] = $arALL_CHISTORY_SUMM;
			 $arResult["ALL_INCOMES_SUMM"] = $arALL_INCOMES_SUMM;
			 $arResult["ALL_CACHE_SUMM"] = $arALL_CACHE_SUMM;
			 unset($arTmp, $arEventsTmp, $arFinalEventsTmp,  $tmpCurrentLabels, $arALL_DEALS_SUMM, $arALL_CHISTORY_SUMM, $arALL_INCOMES_SUMM, $arALL_CACHE_SUMM);


			return $arResult;
		}

		/**
		 * заполняет данные по дивидендам акций РФ в переданный по ссылке массив
		 *
		 * @param  array   $arResult (Passed by reference) Массив результата
		 * @param array  $arCodes  массив кодов акций РФ
		 * @param strng  $startDate  дата первого движения в истории портфеля
		 *
		 * @return array
		 *
		 * @access private
		 */
		public function getDividendsData(&$arResult, $arCodes, $startDate, $pid) {
			//$resA = new Actions;
			global $APPLICATION;
			$hlblock     = HL\HighloadBlockTable::getById(21)->fetch();
			$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();

			$res = $entityClass::getList(array(
				"filter" => array(
					"=UF_ITEM" => $arCodes,
					"!=UF_DATA" => "[]",
				),
				"select" => array(
					"*"
				),
			));

			//Получим краткую инфу по акциям из инфоблока 32
			$arSelect  = Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_HIDEN");
			$arFilter  = Array("IBLOCK_ID" => IntVal(32), "CODE" => $arCodes);
			$res2      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$arActions = array();
			while ($ob = $res2->fetch()) {
				$arActions[$ob["CODE"]] = array("NAME" => $ob["NAME"], "HIDEN" => $ob["PROPERTY_HIDEN_VALUE"]);
			}

			//$arDatesInResult = array_keys($arResult);
			$startDate       = new DateTime($startDate);
			$arActivesDivs = array();
			$arDatesForCount = array();
			while ($elem = $res->fetch()) {
				$arDividends = json_decode($elem["UF_DATA"], true);
				$arActivesDivs[$elem["UF_ITEM"]]["DIVIDENDS"] = $arDividends;
				foreach ($arDividends as $key => $val) {
				  $curr_time       = strval($val["Дата закрытия реестра (T-2)"]);
				  $arDatesForCount[$elem["UF_ITEM"]][] = $curr_time;
				}
			}

			$arCountsOnDates = $this->getActiveCountOnEventMultiDate($pid, $arDatesForCount);

			//$firephp = FirePHP::getInstance(true);
			//	$firephp->fb(array("arCountsOnDates"=>$arCountsOnDates, "arDatesForCount"=>$arDatesForCount),FirePHP::LOG);
			foreach ($arActivesDivs as $activeIsin=>$el) {
				$arDividends = $el["DIVIDENDS"];
				foreach ($arDividends as $key => $val) {
					$curr_time       = strval($val["Дата закрытия реестра (T-2)"]);
					$curr_time_close = strval($val["Дата закрытия реестра"]);
					$curr_timeDT     = new DateTime($curr_time);
					$val["COUNT"] = 0;
     
					//if ($curr_timeDT->getTimestamp() >= $startDate->getTimestamp() && $countOnDate["COUNT"][$elem["UF_ITEM"]] > 0) {
					if ($curr_timeDT->getTimestamp() >= $startDate->getTimestamp() && $arCountsOnDates[$activeIsin][$curr_time] > 0) {
						$val["ITEM"]  = $activeIsin;
						$val["COUNT"] = $arCountsOnDates[$activeIsin][$curr_time];
						//$radarItem   = $resA->getItem($elem["UF_ITEM"]);
						//Исключаем бумаги вышедшие из обращения
						/*             if (!empty($radarItem["PROPS"]["HIDEN"])) {
						continue;
						}*/
						if (!empty($arActions[$activeIsin]["HIDEN"])) {
							continue;
						}

						$val["NAME"] = $arActions[$activeIsin]["NAME"];
      
						//Пересчитываем в рубли если это еврооблига


						$re = '/[\d++\.]+/m';
						preg_match_all($re, $val["Дивиденды"], $matches, PREG_SET_ORDER, 0);
						if(count($matches)>1){
							$rubDiv = floatval(reset($matches[1]));
							$val["Дивиденды"] = $rubDiv;
							$val["valute"] = 'Y';
						} else {
                            $val["valute"] = 'N';
                        }



						//$val["URL"]  = "/lk/actions/" . $elem["UF_ITEM"] . "/";
						if (!is_array($arResult[$curr_time_close]["DIVIDENDS"])) {$arResult[$curr_time_close]["DIVIDENDS"] = array();}
						$arResult[$curr_time_close]["DIVIDENDS"][] = $val;
					}
				}
			}
			 unset($arActions, $res2, $arCountsOnDates, $arDatesForCount);
			//   unset($resA);
		}

		/**
		 * заполняет данные по дивидендам акций США в переданный по ссылке массив
		 *
		 * @param  array   $arResult (Passed by reference) Массив результата
		 * @param array  $arCodes  массив кодов акций США
		 * @param strng  $startDate  дата первого движения в истории портфеля
		 * @param strng  $pid  ID портфеля
		 * @param bool  $convert Возвращать дивиденды в рублях
		 *
		 * @return array
		 *
		 * @access private
		 */
		public function getDividendsUsaData(&$arResult, $arCodes, $startDate, $pid, $convert = false) {
			$resA = new ActionsUsa();

			$hlblock     = HL\HighloadBlockTable::getById(32)->fetch();
			$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();

			$res = $entityClass::getList(array(
				"filter" => array(
					"=UF_ITEM" => $arCodes,
					"!=UF_DATA" => "[]",
				),
				"select" => array(
					"*"
				),
			));

			$arDatesInResult = array_keys($arResult);
			$startDate       = new DateTime($startDate);
			$arRates = array();

			$arActivesDivs = array();
			$arDatesForCount = array();
			while ($elem = $res->fetch()) {
				$arDividends = json_decode($elem["UF_DATA"], true);
				$arActivesDivs[$elem["UF_ITEM"]]["DIVIDENDS"] = $arDividends;
				foreach ($arDividends as $key => $val) {
				  $curr_time       = strval($val["Дата закрытия реестра (T-2)"]);
				  $arDatesForCount[$elem["UF_ITEM"]][] = $curr_time;
				}
			}


			$arCountsOnDates = $this->getActiveCountOnEventMultiDate($pid, $arDatesForCount);

			foreach ($arActivesDivs as $activeIsin=>$el) {
				$arDividends = $el["DIVIDENDS"];
				foreach ($arDividends as $key => $val) {
					$curr_time       = strval($val["Дата закрытия реестра (T-2)"]);
					$curr_time_close = strval($val["Дата закрытия реестра"]);
					$curr_timeDT     = new DateTime($curr_time);

					if ($convert) {
						$arDiv = explode("(", $val["Дивиденды"]);
						$currencyDiv = 0;
						if (count($arDiv)>0){
							$currencyDiv =floatval( preg_replace("/[^,.0-9]/", '', $arDiv[0]));
						}
						if (count($arDiv) > 1) {
							$val["Дивиденды руб"] = floatval(str_replace(" руб.)", "", $arDiv[1]));
						}
					}

					$val["COUNT"] = 0;
					//if ($curr_timeDT >= $startDate && $countOnDate["COUNT"][$elem["UF_ITEM"]] > 0) {
					if ($curr_timeDT >= $startDate && $arCountsOnDates[$activeIsin][$curr_time] > 0) {
						$val["ITEM"]  = $activeIsin;
						$val["COUNT"] = $arCountsOnDates[$activeIsin][$curr_time];
						$radarItem    = $resA->getItem($activeIsin);
						if (!empty($radarItem["PROPS"]["HIDEN"])) { //Исключаем бумаги вышедшие из обращения
							continue;
						}

						$val["NAME"] = $radarItem["NAME"];
						$val["URL"]  = $radarItem["URL"];
						$val["CURRENCY_DIV"] = $currencyDiv;
						$val["CURRENCY"]  = $radarItem["PROPS"]["CURRENCY"];
                        $val["valute"] = 'Y';
				  	if(floatval($val['Дивиденды руб'])==0){
                 if(!in_array($val["CURRENCY"], $arRates)){
						  $arRates[$val["CURRENCY"]] = getCBPrice($val["CURRENCY"]);
					  	}
                  $val['Дивиденды руб'] = round($arRates[$val["CURRENCY"]] * $val["CURRENCY_DIV"],3);

						$val["Дивиденды"] = $arDiv[0].'('.$val['Дивиденды руб'].' руб.)';
				 	}

						if (!is_array($arResult[$curr_time_close]["DIVIDENDS_USA"])) {$arResult[$curr_time_close]["DIVIDENDS_USA"] = array();}
						$arResult[$curr_time_close]["DIVIDENDS_USA"][] = $val;
					}
				}
			}

			unset($resA, $arDividends);
		}

		public function getMidRedemptionValues($pid,$activeId){
			Global $DB;
			$arResult = array();
			if(empty($pid) || empty($activeId)) return $arResult;


			$query = "SELECT * FROM `portfolio_history` WHERE `UF_PORTFOLIO`=$pid AND `UF_ACTIVE_ID`";

/*			  $("body .smart_table.history_table tr[data-active-id="+activeId+"]").each(function(indx, element)
    {
      console.log(activeId);
      var hist_row = $(element);
      var cnt = parseFloat(hist_row.attr('data-cnt'));
      mid_buy_summ += parseFloat(hist_row.attr('data-price'))*cnt;
      buy_summ += parseFloat(hist_row.attr('data-price-summ'));
      cnt_summ += cnt;
      now_summ += parseFloat(hist_row.attr('data-now-summ'));
      mid_ammort += parseFloat(hist_row.attr('data-ammort_summ'))*cnt;
    }
  );

  console.log('now_summ='+now_summ+' amort '+activeId+': mid_ammort='+mid_ammort+' cnt='+cnt_summ);
  //Вычисление средневзвешенных значений
  mid_buy_summ = mid_buy_summ/cnt_summ;
  summ_ammort = mid_ammort;
  mid_ammort = mid_ammort/cnt_summ;
   console.log('calculateAmortizationIncrease', 'bondTrId='+bondTrId+' '+now_summ+'+'+summ_ammort+'/'+buy_summ);
  common_increase = (((now_summ+summ_ammort)/buy_summ-1)*100);*/
		}

		/**
		 * заполняет данные по купонам облигаций в переданный по ссылке массив
		 *
		 * @param  array   $arResult (Passed by reference) Массив результата
		 * @param array  $arCodes  массив кодов облигаций
		 * @param strng  $startDate  дата первого движения в истории портфеля
		 * @return array
		 *
		 * @access private
		 */
		public function getCoupons(&$arResult, $arCodes, $startDate, $pid) {
			//$resO = new Obligations;
			if(!$this->resO){
				$this->init_radar_classes();
			}
			$resO = $this->resO;

			$hlblock     = HL\HighloadBlockTable::getById(15)->fetch();
			$entity      = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();

			$res = $entityClass::getList(array(
				"filter" => array(
					"=UF_ITEM" => $arCodes,
					"!=UF_DATA" => "[[]]",
				),
				"select" => array(
					"*"
				),
			));

			$startDate       = new DateTime($startDate);
			$arActivesCoupons = array();
			$arDatesForCount = array();
			while ($elem = $res->fetch()) {
				$arCoupons = json_decode($elem["UF_DATA"], true);
				$arActivesCoupons[$elem["UF_ITEM"]]["COUPONS"] = $arCoupons;
				foreach ($arCoupons as $key => $val) {
					$curr_time   = strval($val["Дата купона"]);
					$curr_timeDT = new DateTime($curr_time);

					$dt = clone $curr_timeDT;
					$dt->modify("-" . $APPLICATION->offsetTorgDates["coupons"] . " days");
					if ($dt->format("N") == 7) {
						$dt->modify("-2 days");
					} elseif ($dt->format("N") == 6) {
						$dt->modify("-1 days");
					}

					if (isWeekEndDay($dt->format("d.m.Y"))) {
						$finded = false;
						while (!$finded) {
							$dt->modify("-1 days");
							if (!isWeekEndDay($dt->format("d.m.Y"))) {
								$finded = true;
							}
						}
					}
				  $arDatesForCount[$elem["UF_ITEM"]][] = $dt->format('d.m.Y');
				}
			}

			$arCountsOnDates = $this->getActiveCountOnEventMultiDate($pid, $arDatesForCount);

			$now = new DateTime();
			foreach ($arActivesCoupons as $activeIsin=>$el) {
				$radarItem    = $resO->getItem($activeIsin);
				$arCouponsData = $el["COUPONS"];
				foreach ($arCouponsData as $key => $val) {
					$curr_time   = strval($val["Дата купона"]);
					$curr_timeDT = new DateTime($curr_time);

					$dt = clone $curr_timeDT;
					$dt->modify("-" . $APPLICATION->offsetTorgDates["coupons"] . " days");
					if ($dt->format("N") == 7) {
						$dt->modify("-2 days");
					} elseif ($dt->format("N") == 6) {
						$dt->modify("-1 days");
					}

					if (isWeekEndDay($dt->format("d.m.Y"))) {
						$finded = false;
						while (!$finded) {
							$dt->modify("-1 days");
							if (!isWeekEndDay($dt->format("d.m.Y"))) {
								$finded = true;
							}
						}
					}

					//$countOnDate = $this->getActiveCountByDate($pid, $dt->format('d.m.Y'), false, true, false, $elem["UF_ITEM"], true);
					//$countOnDate = $this->getActiveCountOnEventDate($pid, $elem["UF_ITEM"], $dt->format('d.m.Y'));
					$countOnDate = floatval($arCountsOnDates[$activeIsin][$dt->format('d.m.Y')]);
					$val["COUNT"] = 0;

					if ($curr_timeDT->getTimestamp() >= $startDate->getTimestamp() && $countOnDate > 0) {

						$val["ITEM"]  = $activeIsin;
						$val["COUNT"] = $countOnDate;

						if (empty($radarItem["URL"])) { //Не добавляем купоны, для которых нет облиг в базе
							continue;
						}
						if (!empty($radarItem["PROPS"]["HIDEN"]) && !in_array("дефолтные", $radarItem["PROPS"]["CSV_VID_OLB"])) {
							continue;
						}

						//Исключаем бумаги вышедшие из обращения
						$val["URL"]  = $radarItem["URL"];
						$val["NAME"] = $radarItem["NAME"];
						if (in_array("дефолтные", $radarItem["PROPS"]["CSV_VID_OLB"])) {
							$val["DEFAULT"]     = true;
							$val["DEFAULT_VID"] = $radarItem["PROPS"]["CSV_VID_DEFOLT"];
						} else {
							$val["DEFAULT"] = false;
						}
						if (!empty($radarItem["PROPS"]["ADDITIONAL_INCOME"])) {
							$val["STRUCTURED"] = true;
						} else {
							$val["STRUCTURED"] = false;
						}

						//Пересчитываем в рубли если это еврооблига
					$val["CURRENCY"] = $radarItem["PROPS"]["CURRENCYID"];
					if(!in_array($val["CURRENCY"], $this->arRoubleCodes)){
					if($dt->getTimestamp()<=$now->getTimestamp()){
						$rateDate = $dt->format('d.m.Y');
					} else {
						$rateDate = $now->format('d.m.Y');
					}
						$val["RATE"] = getCBPrice($radarItem["PROPS"]["CURRENCYID"], $rateDate);
						$val["-".$val["RATE"]] = $val["-"];
						$val["-"] = round(($val["-"]*$val["RATE"]), 3);
					}
						$arResult[$curr_time]["COUPONS"][] = $val;
					}
				}
			}
			unset($resO);
		}

		/**
		 * Получаем цену покупки актива по логике фифо из сделки HL28
		 * @param $arDates array массив дат
		 * @param $arSecid array массив кодов бумаг
		 * @return array
		 * @throws \Bitrix\Main\ArgumentException
		 * @throws \Bitrix\Main\ObjectPropertyException
		 * @throws \Bitrix\Main\SystemException
		 */
		public function getFifoBuyPrice($pid, $activeType, $activeCode = '') {
			Global $DB;
		    $arResult = array("price" => 0, "price_valute" => 0, "rate" => 0);
			$sql = "SELECT `ID`, `UF_PRICE_ONE`, `UF_INLOT_CNT`, `UF_ADD_DATE`  FROM `$this->hlHistName` WHERE `UF_PORTFOLIO` = '$pid' AND `UF_ACTIVE_CODE` = '$activeCode' AND `UF_HISTORY_DIRECTION` = 'PLUS' ORDER BY `UF_ADD_DATE` ASC LIMIT 1";
			$rsData = $DB->Query($sql);
//TODO Удалить если не возникает проблем 			
/*			$hlId     = 28;
			$hlblock           = HL\HighloadBlockTable::getById($hlId)->fetch();
			$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$arFilter          = array('=UF_PORTFOLIO' => $pid, '=UF_ACTIVE_CODE' => $activeCode, "=UF_HISTORY_DIRECTION" => "PLUS");
			$rsData            = $entity_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC'),
				'select' => array('ID', 'UF_PRICE_ONE', 'UF_INLOT_CNT', 'UF_ADD_DATE'),
				'filter' => $arFilter,
				'limit' => 1
			));*/

			if ($item = $rsData->fetch()) {
				$arResult["price"]      = floatval($item["UF_PRICE_ONE"]);
				$arResult["inlot_cnt"]  = floatval($item["UF_INLOT_CNT"]);
				//$arResult["price_date"] = $item["UF_ADD_DATE"]->format('d.m.Y');
				$arResult["price_date"] = (new DateTime($item["UF_ADD_DATE"]))->format('d.m.Y');
			}
			unset($rsData);
			return $arResult;
		}

		/**
		 * Получаем цены из moexActionData или spbActionsData на акции,облигации, etf
		 * @param $arDates array массив дат
		 * @param $arSecid array массив кодов бумаг
		 * @return array
		 * @throws \Bitrix\Main\ArgumentException
		 * @throws \Bitrix\Main\ObjectPropertyException
		 * @throws \Bitrix\Main\SystemException
		 */
		public function getHistActionsPrices($activeType, $date, $activeSecid, $activeCode = '', $currency='rub') {
			//Создаем объект кеша
			$obCache   = new CPHPCache;
			$life_time = $this->cacheOn?86400*7:0;

			$arResult = $this->getHistActionsPricesCached($activeType, $date, $activeSecid, $activeCode, $currency);
			return $arResult;

			$cache_id = md5(serialize(array($activeType, $date, $activeSecid, $activeCode)));
			if ($obCache->StartDataCache($life_time, $cache_id, "/portfolio/getHistActionsPrices")) {
				$arResult = $this->getHistActionsPricesCached($activeType, $date, $activeSecid, $activeCode, $currency);
				//Кешируем переменную (можно несколько)
				$obCache->EndDataCache(array(
					"arResult" => $arResult
				));
			} else {
				// Если у нас был кеш до достаем закешированные переменные
				extract($obCache->GetVars());
			}
			return $arResult;
		}

		public function getHistActionsPricesCached($activeType, $date, $activeSecid, $activeCode = '', $currency='rub') {
			Global $DB;
			$arResult        = array("price" => 0, "price_valute" => 0, "rate" => 0);
			$activeCodeSecid = '';
			$hlId            = 24;
			switch ($activeType) {
				case "shares":
				case "share":
				case "shares_etf":
				case "share_etf":
					$hlId = 24;
					break;
				case "shares_usa":
				case "share_usa":
					$hlId = $this->spb_actions_HL_Id;
					break;
				case "bonds":
				case "bond":
					if (!$this->resO) {
						$this->init_radar_classes();
					}
					$hlId            = 25;
					$ResObl          = $this->resO;
					$arObligItem     = $ResObl->getItemByIsin($activeCode);
					$activeCodeSecid = $arObligItem["PROPS"]["SECID"];
					break;
				case "currency":
				case "currencies":
					$hlId = 31;
					break;
			}
/*			$hlblock           = HL\HighloadBlockTable::getById($hlId)->fetch();
			$entity            = HL\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();*/
            if(is_object($date)){
                CLogger::errorDate5135portfolio(print_r($date, true));
                $startdate    = $date->modify('-15 days');
                $enddate      = $date->modify('+1 days');
            } else {
                $startdate    = (new DateTime($date))->modify('-15 days');
                $enddate      = (new DateTime($date))->modify('+1 days');
            }
			$arDatesRange = array();

			$period       = new DatePeriod(
				$startdate,
				new DateInterval('P1D'),
				$enddate
			);
			foreach ($period as $key => $value) {
				$arDatesRange[] = $value->format('Y-m-d');
			}

			//$arItem = array($activeSecid);
			if (!empty($activeSecid)) {
				$arItem[] = $activeSecid;
			}
			if (!empty($activeCode)) {
				$arItem[] = $activeCode;
			}
			if (!empty($activeCodeSecid)) {
				$arItem[] = $activeCodeSecid;
			}

			$tableName = $this->hl_tables_prices[$hlId];
			$query = "SELECT * FROM `$tableName` WHERE ";
			//Добавляем в запрос условие по тикеру/isin или их группе
			array_walk($arItem, function(&$x) {$x = "'$x'";});//Экранируем кавычками элементы массива (тикеры или isin)
			if(count($arItem)>1){
				$query .= " `UF_ITEM` IN (".implode(",", $arItem).") ";
			} else {
				$query .= " `UF_ITEM` = ".reset($arItem)." ";
			}
			//Добавляем массив дат в условие
			array_walk($arDatesRange, function(&$x) {$x = "'$x'";});//Экранируем кавычками элементы массива (тикеры или isin)
		 	//$query .= " AND ( `UF_DATE` BETWEEN ".reset($arDatesRange)." AND ".$arDatesRange[count($arDatesRange)-1]." ) ";
		 	//$query .= " AND  `UF_DATE` <= '".(new DateTime($date))->format('Y-m-d')."'  ";
		 	$query .= " AND  `UF_DATE` <= '".$enddate->format('Y-m-d')."'  ";
			$query .= " ORDER BY `UF_DATE` DESC ";
			if(count($arItem)==1){
			 	$query .= " LIMIT 1";
			}

			if(count($arItem)>0){
			 $rsData = $DB->Query($query);


/*			$arFilter = array('=UF_ITEM' => $arItem, '=UF_DATE' => $arDatesRange);
			$rsData   = $entity_data_class::getList(array(
				'order' => array('UF_DATE' => 'DESC'),
				'select' => array('ID', 'UF_*'),
				'filter' => $arFilter,
			));*/

			//if ($hlId == 29) {$resAU = $this->resAU;}

			if ($item = $rsData->fetch()) {
				$arResult["price"] = floatval($item["UF_CLOSE"]);
				if($item["UF_DATE"]){
					$item["UF_DATE"] = (new DateTime($item["UF_DATE"]));
				}
				if ($hlId == 25) {//Облиги
					if (intval($item["UF_FACEVALUE"]) <= 0) {
						$fv = floatval($ResObl->getFacevalueOnDate($arItem, $item["UF_DATE"]->format('d.m.Y')));
					} else {
						$fv = floatval($item["UF_FACEVALUE"]);
					}

					//Если запрос на текущую дату то берем цену и нкд из инфоблока, т.к. там может быть актуальная информация после запуска парсера облиг вручную
					if ((new DateTime())->format('d.m.Y') == (new DateTime($date))->format('d.m.Y')) {
						if (count($arObligItem)) {
							$arResult["price"]     = !empty($arObligItem["PROPS"]["LASTPRICE"]) ? $arObligItem["PROPS"]["LASTPRICE"] : $arObligItem["PROPS"]["LEGALCLOSE"];
							$item["UF_CLOSE"]      = $arObligItem["PROPS"]["LEGALCLOSE"];
							$item["UF_ACCRUEDINT"] = $arObligItem["PROPS"]["ACCRUEDINT"];
							$fv                    = $arObligItem["PROPS"]["FACEVALUE"];
							unset($arObligItem);
						}
					}

					$arResult["uf_close"]   = floatval($item["UF_CLOSE"]);
					$arResult["price"]      = ($arResult["price"] * $fv / 100) + floatval($item["UF_ACCRUEDINT"]);
					$arResult["facevalue"]  = $fv;
					$arResult["accint"]     = floatval($item["UF_ACCRUEDINT"]);
					$arResult["price_date"] = $item["UF_DATE"]->format('d.m.Y');

					//Для еврооблиг добавляем цену в валюте и курс с датой
					$currency_value = 1;
					if (strpos(strtolower($currency), 'rub') === false) {
						$currency_value = getCBPrice(strtoupper($currency), (new DateTime($date))->format('d.m.Y'));

					$arResult["price_valute"] = $arResult["price"];
					$arResult["price"]        = $arResult["price"] * $currency_value;
					$arResult["rate"]         = $currency_value;
					$arResult["rate_date"]    = $item["UF_DATE"]->format('d.m.Y');
					}

				}
				if ($hlId == $this->spb_actions_HL_Id) {  //Акции США
					//$itemAU         = $resAU->getItemBySecid($activeSecid);
					$currency_value = 1;
					if (strpos(strtolower($currency), 'rub') === false) {
						$currency_value = getCBPrice(strtoupper($currency), (new DateTime($date))->format('d.m.Y'));
						// $currency_value = getCBPrice(strtoupper($itemAU['PROPS']['CURRENCY']),$item["UF_DATE"]->format('d.m.Y'));
					}
					$arResult["price_valute"] = $arResult["price"];
					$arResult["rate"]         = $currency_value;
					$arResult["rate_date"]    = $item["UF_DATE"]->format('d.m.Y');
					$arResult["price"]        = $arResult["price"] * $currency_value;
					$arResult["currencyId"]   = strtolower($currency);
				}
				if ($activeType=='share_etf' || $activeType=='shares_etf') {
					//$itemE         = $this->resE->getItemBySecid($activeSecid);
					$currency_value = 1;
					if (strpos(strtolower($currency), 'rub') === false) {
						$currency_value = getCBPrice(strtoupper($currency), (new DateTime($date))->format('d.m.Y'));
					}

					$arResult["price_valute"] = $arResult["price"];
					$arResult["rate"]         = $currency_value;
					$arResult["rate_date"]    = $item["UF_DATE"]->format('d.m.Y');
					$arResult["price"]        = round($arResult["price"] * $currency_value, 4);
					$arResult["currencyId"]   = strtolower($currency);
				}

				if ($hlId == 31) {
					$currency_value           = 1;
					$arResult["price_valute"] = $arResult["price"];
					$arResult["rate"]         = $item["UF_CLOSE"];
					$arResult["rate_in_usd"]  = $item["UF_CLOSE_USD"];
					$arResult["rate_in_eur"]  = $item["UF_CLOSE_EUR"];
					$arResult["rate_date"]    = $item["UF_DATE"]->format('d.m.Y');
					$arResult["price"]        = $arResult["price"] * $currency_value;
					$arResult["price_in_usd"] = $arResult["price"] * floatval($item["UF_CLOSE_USD"]);
					$arResult["price_in_eur"] = $arResult["price"] * floatval($item["UF_CLOSE_EUR"]);
					$arResult["currencyId"]   = strtolower($item["UF_ITEM"]);
				}
			} else {
				//$firephp = FirePHP::getInstance(true);
				//$firephp->fb(array("label"=>$hlId,"filter"=>$arFilter, "val"=>$arResult),FirePHP::LOG);
				if ($activeSecid == 'RUB') {
					$arResult["price_valute"] = 1;
					$arResult["rate"]         = 1;
					$arResult["rate_date"]    = (new DateTime($date))->format('d.m.Y');
					$arResult["price"]        = 1;
					$arResult["currencyId"]   = 'rub';
				} else {
					$arResult["price_valute"] = $item["UF_CLOSE"];
					$arResult["rate"]         = $item["UF_CLOSE"];
					$arResult["rate_date"]    = (new DateTime($date))->format('d.m.Y');
					$arResult["price"]        = $arResult["price"] * $currency_value;
					$arResult["currencyId"]   = strtolower($item["UF_ITEM"]);
				}
			}
			} else {
					$arResult["price_valute"] = 1;
					$arResult["rate"]         = 1;
					$arResult["rate_date"]    = (new DateTime($date))->format('d.m.Y');
					$arResult["price"]        = 1;
					$arResult["currencyId"]   = 'rub';
			}

			if ($hlId == 25) { //unset($ResObl);
			}
			if ($hlId == $this->spb_actions_HL_Id) { //unset($resAU);
			}
			return $arResult;
		}

		/**
		 * Обновляет элемент инфоблока "Портфели" с id = $id
		 *
		 * @param  int,string   $id - ID элемента инфоблока "Портфели"
		 * @param  array   $arData  - Массив параметров -свойств инфоблока "Портфели"
		 *
		 *
		 * @access public
		 */
		public function updatePortfolio($id, $arData) {
			$ELEMENT_ID = intval($id);
			foreach ($arData as $kd => $val) {
				if ($kd == 'date') {
					CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array('TRACKING_DATE' => (new DateTime($val))->format('d.m.Y')));
				}
			}
		}

		/**
		 * Сохраняет описание портфеля
		 *
		 * @param  int,string   $id - ID элемента инфоблока "Портфели"
		 * @param  array   $arData  - Массив параметров - свойств инфоблока "Портфели"
		 *
		 * @return  json возвращает латинский текст "ok" или строку с описанием ошибок
		 *
		 * @access public
		 */
		public function editDescrPortfolio($id, $arData) {
			$el                 = new CIBlockElement;
			$arLoadProductArray = Array(
				"PREVIEW_TEXT" => htmlspecialchars($arData['descr']),
			);
			if ($el->Update(intval($id), $arLoadProductArray)) {
				$result = array('result' => 'ok', 'message' => 'Описание портфеля изменено');
			} else {
				$result = array('result' => 'error', 'message' => $el->LAST_ERROR);
			}
			unset($el);
			return $result;
		}

		public function getPortfolioDescription($id) {
			$rsElement = CIBlockElement::GetByID(intval($id));
			if ($rsElement && ($obElement = $rsElement->GetNextElement())) {
				$arElement = $obElement->GetFields();
				return htmlspecialchars_decode($arElement["PREVIEW_TEXT"]);
			} else {
				return '';
			}
		}

		/**
		 * Удаляет портфель вместе со всеми связанными данными, включая историю
		 *
		 * @param  int   $pid  ID удаляемого портфеля
		 * @param  bool   $deactivate (Optional) TODO: для двухшагового удаления портфеля через деактивацию и если не произойдет активации - то по агенту или крону такой портфель будет удален со всеми связями
		 *
		 * @return json возвращает латинский текст "ok" или строку с описанием ошибок
		 *
		 * @access public
		 */
		public function deletePortfolio($pid, $deactivate = false) {
			Global $DB;
			$result = '';
			$errors = '';

			$hlblock_id        = 28; // Удаляем HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$rsData            = $entity_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC'),
				'select' => array('ID'),
				'filter' => array('UF_PORTFOLIO' => $pid)
			));
			$result28 = true;
			while ($el = $rsData->fetch()) {
				$res = $entity_data_class::delete($el["ID"]);
				if (!$res->isSuccess() && $result28 == true) {
					$result28 = false;
					$errors .= implode(", ", $res->getErrorMessages());
					break;
				}
			}

			//Если удалились активы и история портфеля - удаляем сам портфель из инфоблока
			if ($result28 && empty($errors)) {
				//Сначала получим пользователя удаляемого портфеля для последующего сброса его кеша по кол-ву портфелей
				$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_OWNER_USER");
				$arFilter = Array("IBLOCK_ID" => IntVal(52), "ID" => $pid);
				$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCnt" => 1), $arSelect);
				if ($ob = $res->Fetch()) {
					$portfolioUser = intval($ob["PROPERTY_OWNER_USER_VALUE"]);
				} else {
					$portfolioUser = false;
				}

				$DB->StartTransaction();
				if (!CIBlockElement::Delete($pid)) {
					$result = 'Error';
					$errors .= (!empty($errors) ? ', ' : '') . 'Ошибка удаления портфеля';
					$result = $errors;
					$DB->Rollback();
				} else {
					$DB->Commit();
				}

				$result = array('result' => 'ok', 'message' => 'Портфель полностью удален');
			} else {
				$result = array('result' => 'error', 'message' => $errors);
			}

			if ($portfolioUser != false) {
				$this->clearCachePortfolioCntForUser($portfolioUser);
			}

			return json_encode($result);
		}

		/**
		 * Очищает портфель со всеми связанными данными, включая историю
		 *
		 * @param  int   $pid  ID удаляемого портфеля
		 * @param  bool   $deactivate (Optional) TODO: для двухшагового удаления портфеля через деактивацию и если не произойдет активации - то по агенту или крону такой портфель будет удален со всеми связями
		 *
		 * @return json возвращает латинский текст "ok" или строку с описанием ошибок
		 *
		 * @access public
		 */
		public function clearPortfolio($pid, $deactivate = false) {
			Global $DB, $USER;
			$result = '';
			$errors = '';

			$hlblock_id        = 28; // Удаляем HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$rsData            = $entity_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC'),
				'select' => array('ID'),
				'filter' => array('=UF_PORTFOLIO' => $pid)
			));
			$result28 = true;
			while ($el = $rsData->fetch()) {
				$res = $entity_data_class::delete($el["ID"]);
				if (!$res->isSuccess() && $result28 == true) {
					$result28 = false;
					$errors .= implode(", ", $res->getErrorMessages());
					break;
				}
			}

			CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("LOADED_FILES" => false));
			CIBlockElement::SetPropertyValuesEx($pid, $this->portfolio_iblockId, array("BROKER" => ''));

			if (empty($errors)) {
				$result = array('result' => 'ok', 'message' => 'Портфель очищен');
				$this->clearCacheComplex($pid, 'clearPortfolio');//Очистка всего кеша по портфелю
				$this->clearPortfolioListForUserCache($USER->GetID());
			} else {
				$result = array('result' => 'error', 'message' => $errors);
			}

			return json_encode($result);
		}

		/**
		 * Создает новый портфель, если передан список активов - то добавляет в созданный портфель переданные активы текущей датой и перенаправляет на страницу созданного портфеля
		 *
		 * @param  array   $data
		 *
		 * @return text
		 *
		 * @access public
		 */
		public function addPortfolio($data) {
			Global $USER;
			$el                      = new CIBlockElement;
			$PROP                    = array();
			$PROP['OWNER_USER']      = $USER->GetID(); //Владелец
			$PROP['USER']            = $PROP['OWNER_USER']; //пользователи
			$PROP['TRACKING_DATE']   = (new DateTime($data['date']))->format('d.m.Y'); //Дата отслеживания портфеля
			$PROP['VERSION']         = $this->currentVersion; //Версия портфеля
			if(array_key_exists('reportType', $data)){
			  $PROP['BROKER']        = $data['reportType']; //Версия брокера
			}
            if(array_key_exists('reportType', $data)){
                $PROP['BROKER']        = $data['reportType']; //Версия брокера
            }
			//$firephp = FirePHP::getInstance(true);
			//$firephp->fb(array("brokerReportFile"=>$data['brokerReportFile']),FirePHP::LOG);
			$PROP['LOADED_FILES']	 = array("n0" => array("VALUE"=>$data['brokerReportFile'], "DESCRIPTION"=>(new DateTime())->format('d.m.Y H:i')));

			$arLoadProductArray = Array(
				"MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
				"IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
				"IBLOCK_ID" => 52, //Портфели
				"PROPERTY_VALUES" => $PROP,
				"NAME" => $data['name'],
				"PREVIEW_TEXT" => $data['descr'],
				"ACTIVE" => "Y", // активен
			);

			if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
				$result = array("result" => "ok", "id" => $PRODUCT_ID);
			} else {
				$result = array("result" => "error", "error_text" => $el->LAST_ERROR);
			}

			$this->clearCachePortfolioCntForUser($PROP['USER']);

			return $result;
		}

		/**
		 * Копирует портфель со всей историей
		 *
		 * @param  int   $pid id копируемого портфеля
		 * @param  array   $arData Массив данных копируемого портфеля (id и название нового)
		 *
		 * @return array  массив с новым id или с описанием ошибок
		 *
		 * @access public
		 */
		public function copyPortfolio($pid, $arData) {
			Global $DB;
			$arErrors = array();

			$hlblockHist_id        = 28; // портфели история
			$hlblockHist           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblockHist_id)->fetch();
			$entityHist            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblockHist);
			$entityHist_data_class = $entityHist->getDataClass();
			//Копируем сам портфель
			$newPid = 0;
			if (count($arData['data']) > 0 && empty($arData['data']['add_to_exist_portfolio'])) {
				$arResult = $this->addPortfolio($arData['data']);
			} else if (count($arData['data']) > 0 && !empty($arData['data']['add_to_exist_portfolio'])) {
				$arResult = intval($arData['data']['add_to_exist_portfolio']);
			}
			if ($arResult["result"] == "ok") {
				$newPid = $arResult["id"];
                if(empty($arData['data']['add_to_exist_portfolio'])){
                    //Копируем установку галки автопополнения кеша если была копия в новый портфель
                    $sql = "SELECT `ID` FROM `b_iblock_property` WHERE `IBLOCK_ID` = '$this->portfolio_iblockId' && `CODE` = 'SUBZERO_CACHE_ON'";
                    $res = $DB->Query($sql);
                    if($row = $res->fetch()){
                        $prop_id = $row['ID'];
                    } else {
                        $prop_id = 825;
                    }
                    $sql = "SELECT `PROPERTY_$prop_id` FROM `b_iblock_element_prop_s".$this->portfolio_iblockId."` WHERE `IBLOCK_ELEMENT_ID` = '$pid'";
                    $res = $DB->Query($sql);
                    $subzero_cache_on_value = '';
                    if($row = $res->fetch()){
                        $subzero_cache_on_value = $row['PROPERTY_'.$prop_id];
                    }
                    if(!empty($subzero_cache_on_value)){
                        $sql = "UPDATE `b_iblock_element_prop_s".$this->portfolio_iblockId."` SET `PROPERTY_$prop_id` = $subzero_cache_on_value WHERE `IBLOCK_ELEMENT_ID` = '".$arResult["id"]."'";
                        $DB->Query($sql);
                    }
                }
                
                
			} else {
				$arErrors[] = $arResult["message"];
			}

			//Копируем портфель через временную таблицу
			$query1 = "CREATE TEMPORARY TABLE `temporary_portfolio_$pid` AS SELECT * FROM `portfolio_history` WHERE `UF_PORTFOLIO` = '$pid'";
			$query2 = "UPDATE `temporary_portfolio_$pid` SET `UF_PORTFOLIO` = '$newPid'";
			$query3 = "UPDATE `temporary_portfolio_$pid` SET `ID`=0";
			$query4 = "INSERT INTO `portfolio_history` SELECT * FROM `temporary_portfolio_$pid`";
			CLogger::QueryCopy();
			CLogger::QueryCopy($query);
			$DB->StartTransaction();

			$arResult["result"] = count($arErrors) > 0 ? 'error' : 'ok';
			if (count($arErrors)) {
				$arResult["errors"] = implode(", ", $arErrors);
			}

			if (strlen($strError)<=0 && count($arErrors)<=0){
				$DB->Query($query1);
				$DB->Query($query2);
				$DB->Query($query3);
				$DB->Query($query4);
			   $DB->Commit(); //сохранить последние изменения в базе и закрыть транзакцию
			}
			else{
			   $DB->Rollback(); //откатить изменения  и закрыть транзакцию
			}


			unset($entityHist_data_class, $hlblockHist, $entityHist, $rsDataCopied, $arDealsCopied, $dealNewCopy);
			return $arResult;
		}

		public function renamePortfolio($data) {

			if (!empty($data["newNamePortfolio"])) {
				$el                 = new CIBlockElement;
				$arLoadProductArray = Array(
					"NAME" => $data["newNamePortfolio"],
				);
				if ($el->Update(intval($data["portfolioId"]), $arLoadProductArray)) {
					$result = array("result" => "ok", "id" => "Портфель " . $data["portfolioId"] . " успешно переименован.");
				} else {
					$result = array("result" => "error", "message" => $el->LAST_ERROR);
				}
			} else {
				$result = array("result" => "error", "message" => "Новое название не может быть пустым!");
			}
			return $result;
		}

		//Изменяет сделки активов в портфеле при автобалансе
		public function setAutobalance($pid, $data) {
			Global $DB;
		 //	CLogger::autobalance(print_r($data, true));

			$arResult = array("ERROR" => array(), "SUCCESS" => array());
			if (intval($pid) > 0 && count($data) > 0) {
				$UF_HISTORY_DIRECTION = '';
				$text_comment         = '';

				//Удаляем все записи о валютах
				$query = "DELETE FROM `portfolio_history` WHERE `UF_PORTFOLIO`=$pid AND (`UF_ACTIVE_TYPE`='currency' OR `UF_HISTORY_DIRECTION`='TOTAL' )";
				$DB->Query($query);

				$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hl_hist_Id)->fetch();
				$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();
				$arDates = array();
				foreach ($data as $k => $arRow) {
					$arDates[] = new DateTime($arRow['date']);
					$arMass = Array(
						'UF_ADD_DATE' => $DB->ForSql((new DateTime($arRow['date']))->format('d.m.Y')),
						'UF_LOTCNT' => $arRow['cnt'],
						'UF_HISTORY_CNT' => $arRow['cnt'],
						'UF_LOTPRICE' => $arRow['price'],
						'UF_HISTORY_SUM' => $arRow['price']*$arRow['cnt'],
						'UF_SECID' => $DB->ForSql($arRow['secid']),
					);
					if (isset($arRow['price_one']) && floatval($arRow['price_one']) > 0) {
						$arMass['UF_PRICE_ONE'] = $arRow['price_one'];
					}
					$error_msg = '';
					//$firephp = FirePHP::getInstance(true);
					//$firephp->fb(array("arRow"=>$arRow),FirePHP::LOG);
				 //	$otvet     = $entity_data_class::update($arRow['activeId'], $arMass);

				   $otvet     = $entity_data_class::update($arRow['activeId'], $arMass);
					//$arResult["ERROR"][]=$otvet->getErrors();

					$arMass['UF_ACTIVE_ID']   = $arRow['iblockActiveId'];
					$arMass['UF_ACTIVE_TYPE'] = $arRow['activeType'];
					$arMass['UF_PORTFOLIO']   = $pid;
					$arRow["portfolioId"]     = $pid;
					if ($arRow["portfolioEditMode"] == "Y") { //В режиме правки только обновляем связанную запись в истории
						$otvetHistory = $this->updateHistory($arRow);
					}

					if ($otvet->isSuccess(true)) {
						$arResult["SUCCESS"][] = 'Информация по сделке ' . $arRow['activeId'] . ' успешно обновлена';
					} else {
						$error_msg = 'Error: ' . implode(', ', $otvet->getErrors()) . ";";
						$arResult["ERROR"][] = $error_msg;
					}
				} //foreach $data
			}
			$resStatus = 'error';
			if (count($arResult["ERROR"]) > 0) {
				$resText = implode('; ', $arResult["ERROR"]);
			} else {
			  //	$dateStart = $this->get
			  //Сортируем по возрастанию список дат добавления активов, что бы получить стартовую (самую левую) дату для пересчета итогов и кеша
				usort($arDates, function($addDateA, $addDateB){
					$res = 0;
					if ($addDateA != $addDateB) {
							return ($addDateA > $addDateB) ? 1 : -1; //asc
					}
				  return $res;
				});

				$dateStart = reset($arDates)->format('d.m.Y');
				//$firephp = FirePHP::getInstance(true);
				//$firephp->fb(array("startDate"=>$dateStart,"val"=>""),FirePHP::LOG);
				//Запускаем пересчет итогов и кеша
				$this->recountCacheOnPortfolio($pid,$dateStart,false, 'autobalance');
				$this->clearCacheComplex($pid, 'setAutobalance');
				$resStatus = 'ok';
				$resText   = implode('; ', $arResult["SUCCESS"]);
			}
			return array('result' => $resStatus, 'message' => $resText);
		}

		//Изменяет сделки активов в портфеле при автобалансе
		public function setAutobalance_old($pid, $data) {
			Global $DB;
			$arResult = array("ERROR" => array(), "SUCCESS" => array());
			if (intval($pid) > 0 && count($data) > 0) {
				$UF_HISTORY_DIRECTION = '';
				$text_comment         = '';

				$hlblock_id        = 27; // указываете ид вашего Highload-блока
				$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
				$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();

				foreach ($data as $k => $arRow) {
					$arMass = Array(
						'UF_ADD_DATE' => $DB->ForSql($arRow['date']),
						'UF_LOTCNT' => $arRow['cnt'],
						'UF_LOTPRICE' => $arRow['price'],
						'UF_SECID' => $DB->ForSql($arRow['secid']),
					);
					if (isset($arRow['price_one']) && floatval($arRow['price_one']) > 0) {
						$arMass['UF_PRICE_ONE'] = $arRow['price_one'];
					}
					$error_msg = '';
					$otvet     = $entity_data_class::update($arRow['activeId'], $arMass);

					//$arResult["ERROR"][]=$otvet->getErrors();

					$arMass['UF_ACTIVE_ID']   = $arRow['iblockActiveId'];
					$arMass['UF_ACTIVE_TYPE'] = $arRow['activeType'];
					$arMass['UF_PORTFOLIO']   = $pid;
					$arRow["portfolioId"]     = $pid;
					if ($arRow["portfolioEditMode"] == "Y") { //В режиме правки только обновляем связанную запись в истории
						$otvetHistory = $this->updateHistory($arRow);
					}

					if ($otvet->isSuccess(true) && $otvetHistory->isSuccess()) {
						$arResult["SUCCESS"][] = 'Информация по сделке ' . $arRow['activeId'] . ' успешно обновлена' . implode(', ', $otvetHistory->getErrors());
					} else {
						$error_msg = 'Error: ' . implode(', ', $otvet->getErrors()) . ";";
						if (!$otvetHistory->isSuccess()) {
							$error_msg .= ' Error History: ' . implode(', ', $otvetHistory->getErrors()) . "";
						}
						$arResult["ERROR"][] = $error_msg;
					}
				} //foreach $data
			}
			$resStatus = 'error';
			if (count($arResult["ERROR"]) > 0) {
				$resText = implode('; ', $arResult["ERROR"]);
			} else {
				$resStatus = 'ok';
				$resText   = implode('; ', $arResult["SUCCESS"]);
			}
			return array('result' => $resStatus, 'message' => $resText);
		}

		//Изменяет сделку актива в портфеле
		public function updatePortfolioActiveByDeal($pid, $data) {
			if (intval($pid) > 0 && count($data) > 0) {
				$UF_HISTORY_DIRECTION = '';
				$text_comment         = '';

				$hlblock_id        = 27; // указываете ид вашего Highload-блока
				$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
				$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();
				$arMass            = Array(
					'UF_ADD_DATE' => $data['date'],
					'UF_LOTCNT' => $data['cnt'],
					'UF_LOTPRICE' => $data['price'],
					'UF_SECID' => $data['secid'],
				);
				if (isset($data['price_one']) && floatval($data['price_one']) > 0) {
					$arMass['UF_PRICE_ONE'] = $data['price_one'];
				}
				$error_msg = '';
				$otvet     = $entity_data_class::update($data['activeId'], $arMass);

				$arMass['UF_ACTIVE_ID']   = $data['iblockActiveId'];
				$arMass['UF_ACTIVE_TYPE'] = $data['activeType'];
				$arMass['UF_PORTFOLIO']   = $pid;
				$data["portfolioId"]      = $pid;
				if ($data["portfolioEditMode"] == "Y") { //В режиме правки только обновляем связанную запись в истории
					$otvetHistory = $this->updateHistory($data);
				}
				unset($hlblock, $entity, $entity_data_class);

				if ($otvet->isSuccess() && $otvetHistory->isSuccess()) {
					echo json_encode(array('result' => 'ok', 'message' => 'Информация по сделке успешно обновлена' . implode(', ', $otvetHistory->getErrors())));
				} else {
					$error_msg = 'Error: ' . implode(', ', $otvet->getErrors()) . ";";
					if (!$otvetHistory->isSuccess()) {
						$error_msg .= ' Error History: ' . implode(', ', $otvetHistory->getErrors()) . "";
					}
					echo json_encode(array('result' => 'error', 'message' => $error_msg));
				}
			}
		}

		//Удаляет автодобавленную валюту при покупке актива (делает авто расход из cacheController)
		public function deletePortfolioAutoAddedCurrency($pid, $data, $summ) {
			$summ              = round($summ, 4);
			$hlblock_id        = 28; // HL Портфели
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$rsData            = $entity_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC'),
				'select' => array('*'),
				'filter' => array('UF_PORTFOLIO' => $pid, 'UF_ACTIVE_TYPE' => 'currency', 'UF_ACTIVE_ID' => $data['activeId'], "UF_PARENT_ID" => $data['UF_PARENT_ID'], "UF_LOTCNT" => $summ)
			));

			while ($el = $rsData->fetch()) {
				$arHistoryData = array();

				$arHistoryData["portfolioId"]       = $pid;
				$arHistoryData["cntActive"]         = $data['cnt'];
				$arHistoryData["idActive"]          = $el['UF_ACTIVE_ID'];
				$arHistoryData["codeActive"]        = $el['UF_ACTIVE_CODE'];
				$arHistoryData["typeActive"]        = $el['UF_ACTIVE_TYPE'];
				$arHistoryData["nameActive"]        = $el['UF_ACTIVE_NAME'];
				$arHistoryData["urlActive"]         = $el['UF_ACTIVE_URL'];
				$arHistoryData["currencyActive"]    = !empty($el['UF_CURRENCY']) ? $el['UF_CURRENCY'] : $data['currencyActive'];
				$arHistoryData["priceActive"]       = $data['UF_PRICE_ONE'];
				$arHistoryData["price_one"]         = $data['UF_PRICE_ONE'];
				$arHistoryData["UF_HISTORY_PRICE"]  = $el['UF_HISTORY_PRICE'];
				$arHistoryData["secid"]             = $el['UF_SECID'];
				$arHistoryData["nameActiveEmitent"] = $el['UF_EMITENT_NAME'];
				$arHistoryData["UF_AUTO"]           = "Y";
				$arHistoryData["urlActiveEmitent"]  = $el['UF_EMITENT_URL'];

				//  $arCacheData = $arHistoryData;
				//  $arCacheData["priceActive"]= $el['UF_PRICE_ONE']*$el["UF_LOTCNT"];
				$data = array_merge($data, $arHistoryData);

				$data["UF_HISTORY_ACT"] = $this->arHistoryActions["CACHE_MINUS"];
				//   $firephp                = FirePHP::getInstance(true);
				//   $firephp->fb(array("deleteAutoAdded" => $data), FirePHP::LOG);

				$otvetHistory = $this->addToHistory('MINUS', $data);
				//  $result = $entity_data_class::delete($el['ID']); //Удаляем актив
			}
		}

		//Удаление актива валюты (кеша)
		public function deletePortfolioCurrency($pid, $data, $clearCurrency = "", $historyAction = "", $no_recount_cache=false, $owner='user') {
			$CURRENCY_CODE = strtoupper(!empty($data["currencyActive"]) ? $data["currencyActive"] : $data["currencyEmitent"]);
			//Получим курс на дату сделки
			//  $dateprice = ($CURRENCY_CODE == "RUB" ? 1 : $this->getHistActionsPrices('currency', $data["dateActive"], $CURRENCY_CODE)["price"]);

			if ($CURRENCY_CODE == "RUB") {
				$data['fifo_price'] = 1;
			}
			$data['cnt']  = round($data['cnt'], 4);
			$historyPrice = $data['price'];
			if (isset($data["exclude"]) && $data["exclude"] == "Y") {
				$historyAction = $this->arHistoryActions["CACHE_MINUS"];
			}
			$data["UF_HISTORY_ACT"] = $historyAction;
			$arHistoryData          = array();
			$arCacheData            = array();
			if ($data['price']) {
				$data["priceActive"] = $data['price'];
			}
			$data["portfolioId"]    = $pid;
			$data['cntActive']      = $data['cnt'];
			$data["currencyActive"] = $data['currencyActive'];
			//$data["priceActive"]= $data['fifo_price'];
			$data["UF_HISTORY_PRICE"] = $historyPrice;
			$data["UF_HISTORY_SUM"]   = $data['cnt'] * $historyPrice;
			$data["UF_ADD_DATE"]      = $data["dateActive"]; //(new DateTime($data["dateActive"]))->format('d.m.Y');
			//$data["priceActive"]= $historyPriceFifo;
			if (isset($data["exclude"]) && $data["exclude"] == "Y") {
			  //	if($no_recount_cache!=true) {
					$data["UF_AUTO"] = "";
			 //	}
				$data["UF_CLEAR_CACHE"] = "Y"; //Признак чистого изъятия кеша
			}
			if (isset($data["description"]) && !empty($data["description"])) {
				$data["UF_DESCRIPTION"] = $data["description"]; //Добавляем коммент к операции (для налогов или комиссий содержит "Амортизация ОФЗ 29045")
			}
			if (!empty($data["actionType"])) {
				$data['UF_CASHFLOW_TYPE'] = $data['actionType'];
			}
			$otvetHistory = $this->addToHistory('MINUS', $data);

			if($no_recount_cache==false ) {
				if($data['actionType']!='tax' && $data['actionType']!='comission'){  //Если списываем не налог или комиссию и не из загрузчика отчетов - то запускаем кеш-контроллер
				  $this->cacheController2($pid, $data, 'MINUS', $owner);
				}

				if($data['actionType']=='tax' || $data['actionType']=='comission'){
					//Пересчитываем автооперации и итоги кеша по дням вправо для налогов и комиссий, т.к. для них не запускается
				  	//cacheController2 в котором делается пересчет
				  $this->recountCacheOnPortfolio($pid, $data["UF_ADD_DATE"]);
				}

				$this->clearCacheComplex($pid, 'deletePortfolioCurrency');//Очистка всего кеша по портфелю
			}
			return true;
		}

		//Удаляет актив при выбытии по методу фифо
		public function deletePortfolioActive($pid, $data, $clearCurrency = "", $historyAction = '', $no_recount_cache=false, $owner='user') {

			// CLogger::BrokerLoader('pid=' . $pid . ' deletePortfolioActive $historyAction='.$historyAction.'; data=' . print_r($data, true) . ' > ');

			if (isset($data["activeType"]) && !isset($data["typeActive"])) {
				$data['typeActive'] = $data["activeType"];
			}
			if ($data['typeActive'] == 'currency') { //Если удаляем валюту или кеш - то перенаправляем на отдельную функцию

				$ret = $this->deletePortfolioCurrency($pid, $data, $clearCurrency, $historyAction, false, $owner);
				return $ret;
			}

			if ($data['cnt']) {$data['cnt'] = round($data['cnt'], 2);
				$data["cntActive"]                      = $data['cnt'];
			}
			$data["UF_HISTORY_ACT"] = $historyAction;
			$data["portfolioId"]    = $pid;

			if ($data['price']) {
				//$data["priceActive"] = $data['price'];
				$data["priceActive"] = $data['price_one']*$data['lotsize'];
			}
			//Флаг игнорирования операций с кешем для данной записи.
			if(isset($data["noCacheOperation"])){
				$data["UF_NO_CACHE_OPERATION"] = $data["noCacheOperation"];
			}
			$data["UF_HISTORY_ACT"] = $this->arHistoryActions["ACT_MINUS"];
			//        $firephp                = FirePHP::getInstance(true);
			//        $firephp->fb(array("addHist" => "minus", "data" => $data), FirePHP::LOG);
			$otvetHistory = $this->addToHistory('MINUS', $data);

			//Добавляем родительский ID записи истории
			$data["UF_PARENT_ID"] = $otvetHistory > 0 ? $otvetHistory : '';

			//Управление кешем портфеля при удалении актива, если удаляется не валюта
			//if ($data['typeActive'] != 'currency') {
			if($no_recount_cache==false) {//Если удаляем интерактивно - то пересчитываем кеш вправо и сбрасываем закешированые данные
				$this->cacheController2($pid, $data, 'MINUS', $owner);

			}

			if($no_recount_cache==false || $owner=='user'){
			  $this->clearCacheComplex($pid, 'deletePortfolioActive');//Очистка всего кеша по портфелю
			}
			//}



			return true;
		}

		//Удаляет сделку и всю ее историю из портфеля
		public function deleteDealWithHistory($pid, $dealId) {
			//Удаление истории сделки
			$hlblock_id        = 28; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$rsData            = $entity_data_class::getList(array(
				'select' => array('ID'),
				'filter' => array('UF_PORTFOLIO' => $pid, 'UF_DEAL_ID' => $dealId)
			));

			while ($el = $rsData->fetch()) {
				$entity_data_class::delete($el["ID"]);
			}

			//удаление сделки
			$hlblock_id        = 27; // HL Портфели
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$entity_data_class::delete($dealId);

			return array("result" => "ok", "message" => "Сделка и ее история удалены из портфеля");
		}

		//Удаляет запись истории с сопутствующими записями по валютам, удаляет запись в HL28
		public function deleteHistoryDeal($pid, $dealId) {

			$hlblock_id        = 28; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter28 = array('UF_PORTFOLIO' => $pid, 'ID' => $dealId);
			$rsData = $entity_data_class::getList(array(
				'select' => array('ID', 'UF_ADD_DATE'),
				'filter' => $arFilter28
			));

			//Получаем записи истории по удаляемой сделке
			while ($el = $rsData->fetch()) {
				$entity_data_class::delete($el["ID"]);
				$this->recountCacheOnPortfolio($pid, $el["UF_ADD_DATE"]);
			}
			$this->clearCacheComplex($pid, 'deleteHistoryDeal');//Очистка всего кеша по портфелю
			return json_encode(array("result" => "success", "message" => ""));
		}

/*		public function deleteHistoryDeal($pid, $dealId) {
			Global $DB;
			$error = "";
			if(intval($pid)<=0 || intval($dealId)<=0) {
				$error = "Не передан id портфеля или сделки";
			}

			if(empty($error)){
				$query = "SELECT `ID`, `UF_ADD_DATE` FROM `portfolio_history` WHERE `UF_PORTFOLIO` = $pid AND `ID` = $dealId";
				$res = $DB->Query($query);

				if($row = $res->Fetch()){
					$DB->StartTransaction();
					try {
					    $delQuery = "DELETE FROM `portfolio_history` WHERE `UF_PORTFOLIO` = $pid AND `ID` = $dealId";
					  	 $DB->Query($delQuery);
						 $this->recountCacheOnPortfolio($pid, (new DateTime($row["UF_ADD_DATE"]))->format('d.m.Y'));

				       $DB->Commit(); //сохранить последние изменения в базе и закрыть транзакцию
					} catch (Exception $e) {
						 $DB->Rollback(); //откатить изменения  и закрыть транзакцию
					    $error = 'Поймано исключение: '.  $e->getMessage();
					} finally {

					}
				} else {
				  $error = "Сделки с id ".$dealId." не обнаружено!";
				}
			}
			$this->clearCacheComplex($pid, 'deleteHistoryDeal');//Очистка всего кеша по портфелю
			return json_encode(array("result" => empty($error)?"success":"error", "message" => empty($error)?"Удаление сделки из истории завершено":$error));
		}*/

		//Редактирование сделки в истории
		public function editHistoryDeal($pid, $data) {
			$dealId                = $data["dealId"];
			$dealPrice             = floatval($data["dealPrice"]);
			$dealCnt               = floatval($data["dealCnt"]);
			$lastpriceActiveValute = floatval($data["lastpriceActiveValute"]);
			$lastpriceActive       = floatval($data["dealPrice"]);
			$price_one_currency    = floatval($data["price_one_currency"]);
			$dealDate              = htmlspecialchars($data["dealDate"]);
			$recountDate           = htmlspecialchars($data["recountDate"]);

			$hlblock_id        = 28; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter28 = array('UF_PORTFOLIO' => $pid, 'ID' => $dealId	);
			$rsData = $entity_data_class::getList(array(
				'select' => array('*'),
				'filter' => $arFilter28
			));

			$deleteMainDeal = false; //Удалять ли сделку и ее дочерние сделки c созданием новой или делать update

			$arMainDeal   = array(); //Поля редактируемой сделки для пересохранения
			//Получаем записи истории по изменяемой сделке
			while ($el = $rsData->fetch()) {
				if ($el["ID"] == $dealId) {
					$arMainDeal        = $el;
					$dealHistDirection = $el["UF_HISTORY_DIRECTION"];
				}
			}

			//Проверяем изменения в основной сделке

			$arMainDeal["UF_ADD_DATE"] = $dealDate;

			$arMainDeal["UF_LOTCNT"]      = $dealCnt;
			$arMainDeal["UF_HISTORY_CNT"] = $dealCnt;
			$recountPrice                 = true;

			$arMainDeal["UF_LOTPRICE"]      = $dealPrice;
			$arMainDeal["UF_HISTORY_PRICE"] = $dealPrice;
			$arMainDeal['UF_PRICE_ONE']     = $dealPrice / floatval($arMainDeal['UF_INLOT_CNT']);
			$arMainDeal['UF_HISTORY_SUM']   = $dealPrice * floatval($arMainDeal['UF_LOTCNT']);

			//Действия с активами в истории

 			 //Просто апдейтим поля сделки
				$entity_data_class::update($arMainDeal["ID"], $arMainDeal);
				$this->recountCacheOnPortfolio($pid, $recountDate); //После апдейта пересчитываем автооперации и итоги кеша по дням вправо
				$this->clearCacheComplex($pid, 'editHistoryDeal');//Очистка всего кеша по портфелю
				$result = array("result"=>"success" , "hist_start_date"=>$this->getHistoryStartDate($pid));
			return $result;
		}

		/**
		 * Добавляет движения в историю
		 *
		 * @param  string   $type вид движения PLUS, MINUS
		 * @param  array    $data данные сделки
		 *
		 * @return [add type]  [add description]
		 *
		 * @access public
		 */
		public function addToHistory($type, $data) {
/*			$hlblock_id        = 28; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();*/

				   //Основная валюта
					$activeCurrency = !empty($data['currencyActive']) ? $data['currencyActive'] : $data['currencyEmitent'];

					//Спаренная валюта (от валютных пар, если не равна рублям или если не равна основной валюте)
				   $activeSecondCurrency = strtolower(!empty($data['currencyActiveReal']) ? $data['currencyActiveReal'] : $data['currencyActive']);
					if($activeSecondCurrency==strtolower($activeCurrency)){
					  $activeSecondCurrency = '';
					}
		//	$firephp = FirePHP::getInstance(true);
		//	$firephp->fb(array("AddToHistory"=>"","val"=>""),FirePHP::LOG);

			$arFields["UF_HISTORY_DIRECTION"] = $type;
			//$arFields["UF_DEAL_ID"]           = $data["UF_DEAL_ID"];
			$arFields["UF_HISTORY_ACT"]       = $data["UF_HISTORY_ACT"];
			$arFields["UF_PORTFOLIO"]         = $data["portfolioId"];
			$arFields["UF_HISTORY_CNT"]       = strval($data["cntActive"]);
			$arFields["UF_LOTCNT"]            = strval($data["cntActive"]);
			$arFields["UF_INLOT_CNT"]         = strval(!empty($data["lotsize"]) ? $data["lotsize"] : $data["UF_INLOT_CNT"]);
			$arFields["UF_ACTIVE_ID"]         = $data["idActive"];
			$arFields["UF_ACTIVE_CODE"]       = $data['codeActive'];
			$arFields["UF_ACTIVE_TYPE"]       = $data["typeActive"];
			$arFields["UF_ACTIVE_NAME"]       = str_replace("+", "", $data["nameActive"]);
			$arFields["UF_ACTIVE_URL"]        = $data["urlActive"];
			$arFields["UF_CURRENCY"]          = $activeCurrency;
			$arFields["UF_ACTIVE_CURRENCY"]   = $activeSecondCurrency;
			$arFields["UF_LOTPRICE"]          = strval($data["priceActive"]);
			$arFields["UF_HISTORY_PRICE"]     = strval($data["priceActive"]);
			if(array_key_exists('secid', $data))
			  $arFields["UF_SECID"]           = $data['secid'];
			if(array_key_exists('UF_CALCULATED', $data))
			  $arFields["UF_CALCULATED"]        = $data['UF_CALCULATED'];
			$arFields['UF_EMITENT_NAME']      = $data['nameActiveEmitent'];
			$arFields['UF_EMITENT_URL']       = $data['urlActiveEmitent'];
			$arFields["UF_HISTORY_SUM"]       = strval(round($data["cntActive"] > 0 ? ($data["priceActive"] * $data["cntActive"]) : $data["priceActive"], 2));
			if (array_key_exists('deal_id', $data)) {
				$arFields["UF_REPORT_DEAL_ID"] = $data['deal_id'];
			}
			if (array_key_exists('finished', $data)) {
				$arFields["UF_FINISHED"] = !$data['finished'];
			}
			if (array_key_exists('report_version', $data)) {
				$arFields["UF_PARSER_VER"] = $data['report_version'];
			}
			/*       $firephp = FirePHP::getInstance(true);
			$firephp->fb(array("UF_HISTORY_SUM" => $arFields["UF_HISTORY_SUM"], "formula"=>$data["priceActive"]." * ".$data["cntActive"]),FirePHP::LOG);
			 */
			if (!empty($data["UF_AUTO"])) {
				$arFields['UF_AUTO'] = $data['UF_AUTO'];
			}
			if (!empty($data["UF_CLEAR_CACHE"])) {
				$arFields['UF_CLEAR_CACHE'] = $data['UF_CLEAR_CACHE'];
			}
			if (!empty($data["UF_DESCRIPTION"])) {
				$arFields['UF_DESCRIPTION'] = $data['UF_DESCRIPTION'];
			}
			if (!empty($data["UF_CASHFLOW_TYPE"])) {
				$arFields['UF_CASHFLOW_TYPE'] = $data['UF_CASHFLOW_TYPE'];
			}
			if (array_key_exists('noCacheOperation', $data)) {
				$arFields["UF_NO_CACHE_OPERATION"] = $data['noCacheOperation'];
			}

			if ($type == "MINUS" && !empty($data["saleDate"])) { //Если передана дата продажи - заменяем dateActive на нее для совместимости и унификации записи в историю
				$data["dateActive"] = $data["saleDate"];
			}

			//Получаем цену на дату добавления/удаления актива
			if ($data['secid'] != "RUB") {
				$PriceForAddDate                 = $this->getHistActionsPrices($data["typeActive"], $data["dateActive"], $data["secid"], $data["codeActive"]);
				$arFields["UF_DATE_PRICE_ONE"]   = $PriceForAddDate["price"];
				$arFields["UF_ACTIVE_LASTPRICE"] = floatval($data["lotsize"]) > 0 ? (floatval($data["lotsize"]) * $PriceForAddDate["price"]) : $PriceForAddDate["price"];
				$arFields["UF_PRICE_ONE"]        = $data["price_one"];
				//$arFields["UF_LOTPRICE"] = $arFields["UF_ACTIVE_LASTPRICE"];
				//$arFields["UF_HISTORY_PRICE"] = $arFields["UF_ACTIVE_LASTPRICE"];
				if ($data["typeActive"] == 'currency' && !empty($data['UF_CLEAR_CACHE'])) {
					$arFields["UF_PRICE_ONE"] = $data["priceActive"];
					$arFields["UF_LOTPRICE"]  = $data["priceActive"] * $data["lotsize"];
				}
			} else {
				$arFields["UF_DATE_PRICE_ONE"]   = 1;
				$arFields["UF_ACTIVE_LASTPRICE"] = 1;
				$arFields["UF_PRICE_ONE"]        = 1;
			}

	  //		$arFields["UF_ADD_DATE"]     = (new DateTime($data["dateActive"]))->format('d.m.Y H:i:s');
	  //		$arFields["UF_HISTORY_DATA"] = (new DateTime(date()))->format('d.m.Y H:i:s');

			$arFields["UF_ADD_DATE"]     = (new DateTime($data["dateActive"]))->format('Y-m-d');
			$arFields["UF_HISTORY_DATA"] = (new DateTime(date()))->format('Y-m-d');

	 //		 $firephp = FirePHP::getInstance(true);
    //      $firephp->fb(array("bond sell portf"=>$data, "fields"=>$arFields),FirePHP::LOG);

			$this->addSQLHistory($arFields);

/*			$result  = $entity_data_class::add($arFields);
			$addedId = 0;
			if (!$result->isSuccess()) {
				//  CLogger::addHistoryErrors(print_r($result->getErrors(), true));
			} else {
				//  CLogger::okAddHistory(print_r($result->getErrors(),true)." ID=".$result->getId());
				$addedId = $result->getId();
			}*/
		  //	$this->clearCacheComplex($data["portfolioId"], 'addToHistory');//Очистка всего кеша по портфелю

			return $addedId;
		}

		/**
		 * Обновляет запись истории связанную со сделкой, вызывается только для изменений на вкладке "по сделкам" когда включен режим редактирования.
		 *
		 * @param  array   $data Массив данных о сделке
		 *
		 * @return object  объект с результатом обновления HL инфоблока
		 *
		 * @access public
		 */
		public function updateHistory($data) {
			$hlblock_id        = 28; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$rsData = $entity_data_class::getList(array(
				'select' => array('ID'),
				'filter' => array('UF_PORTFOLIO' => $data["portfolioId"], 'UF_DEAL_ID' => $data['activeId'])
			));
			$rowHistoryId = 0;
			if ($el = $rsData->fetch()) {
				$rowHistoryId = $el["ID"];
			}

			$arFields["UF_DEAL_ID"] = $data["activeId"];
			//$data["UF_HISTORY_ACT"]       = $act;   //TODO После запуска новой истории удалить колонку из инфоблока
			$arFields["UF_HISTORY_CNT"]   = $data["cnt"];
			$arFields["UF_LOTCNT"]        = $data["cnt"];
			$arFields["UF_LOTPRICE"]      = $data["price"];
			$arFields["UF_HISTORY_PRICE"] = $data["price"];
			$arFields["UF_HISTORY_SUM"]   = $data["cnt"] > 0 ? ($data["price"] * $data["cnt"]) : $data["price"];
			//Получаем цену на дату добавления актива
			$PriceForAddDate                 = $this->getHistActionsPrices($data["activeType"], $data["date"], $data["secid"], '');
			$arFields["UF_DATE_PRICE_ONE"]   = $PriceForAddDate["price"];
			$arFields["UF_ACTIVE_LASTPRICE"] = intval($data["inlotCnt"]) > 0 ? (intval($data["inlotCnt"]) * $PriceForAddDate["price"]) : $PriceForAddDate["price"];

			$arFields["UF_PRICE_ONE"]    = $data["price_one"];
			$arFields["UF_ADD_DATE"]     = (new DateTime($data["date"]))->format('d.m.Y H:i:s');
			$arFields["UF_HISTORY_DATA"] = (new DateTime(date()))->format('d.m.Y H:i:s');
			if (intval($rowHistoryId) > 0) {
				$otvet = $entity_data_class::update($rowHistoryId, $arFields);
			} else {
				CLogger::upd_portfolio_history_error(print_r($arFields, true));
			}
			return $otvet;
		}


		//Возвращает переменную по портфелю из сессии
		public function getPortfolioStatesFromSession($pid, $session_param) {

			return $_SESSION['PORTFOLIO'][$pid][$session_param];
		}

		//Сохраняет переменную по портфелю в сессию
		public function setPortfolioStatesToSession($pid, $session_param, $session_value) {			//$_SESSION[$pid][$session_param] = $session_value;
			if(is_array($session_value)){
				$session_value = serialize($session_value);
			}
			$_SESSION['PORTFOLIO'][$pid][$session_param] = $session_value;

			return 'ok';
		}

		/** Возвращает итоговые записи по кешу на дату или диапазон дат, по всем или заданной валюте
		 * Используется для расчета кеша при выводе на график
		 * @param       $pid id портфеля
		 * @param array $arDates массив дат в формате d.m.Y
		 * @param array $arCurrencies массив кодов валют
		 * @return array Массив из итоговых сумм кеша с ключами по датам
		 */
		public function getCacheTotalRows($pid, $arDates=array(), $arCurrencies=array()){
			$arResult = array();

			$hlblock_id        = $this->hl_hist_Id; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter = array('UF_PORTFOLIO' => $pid, 'UF_ACTIVE_TYPE'=>'cache');
			if(count($arDates)){
				$arFilter['UF_ADD_DATE'] = $arDates;
			}
			if(count($arCurrencies)){
				$arFilter['UF_ACTIVE_CODE'] = $arCurrencies;
			}
			//Todo Сократить кол-во полей в селекте после отладки и тестирования
			$rsData = $entity_data_class::getList(array(
			'select' => array('ID', 'UF_ADD_DATE', 'UF_ACTIVE_TYPE', '*'),
			'filter' => $arFilter,
			'order'	=> array('UF_ADD_DATE'=>'ASC'),
			));

			while ($el = $rsData->fetch()) {
				$rub_summ = $el['UF_HISTORY_SUM'];
				if(!in_array($el['UF_ACTIVE_CODE'], $this->arRoubleCodes)){
					$rub_summ = $el['UF_HISTORY_SUM']*$el['UF_PRICE_ONE'];
				}
				$arResult[(new DateTime($el["UF_ADD_DATE"]))->format('d.m.Y')]['ITEMS'][] = array('ID'=>$el['ID'], 'SUMM'=>$el['UF_HISTORY_SUM'], 'RUB_SUMM'=>$rub_summ, 'RATE'=>$el['UF_PRICE_ONE'], 'CURRENCY'=>$el['UF_ACTIVE_CODE']);
				$arResult[(new DateTime($el["UF_ADD_DATE"]))->format('d.m.Y')]['TOTAL'] += $rub_summ;
			}
			return $arResult;
		}

		/** Возвращает остаток кеша на начало дня указанной даты в указанной валюте
		 * @param $pid	id портфеля
		 * @param $date	Дата на начало которой требуется получить остаток кеша по портфелю
		 * @param $currency Код валюты для которой нужно получить остаток кеша
		 * @return array массив из записей по итогам кеша на ближайшую предыдущую существующую в БД запись
		 */
		public function getCacheOnBeginDate($pid, $date, $arCurrency=array(), $excludeCurrentDate = false){
			$arResult = array();
			$hlblock_id        = $this->hl_hist_Id; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$sign = '<=';
			if($excludeCurrentDate){ //Если получаем кеш до текущей даты не включая ее (для графика истории, желтого кеша обычно)
				$sign = '<';
			}
			$arFilter = array('UF_PORTFOLIO' => $pid, 'UF_ACTIVE_TYPE'=>'cache', $sign.'UF_ADD_DATE' => $date);
			if(count($arCurrency)>0){
				$arFilter['UF_ACTIVE_CODE']	= $arCurrency;
			}
			$rsData = $entity_data_class::getList(array(
			'select' => array('ID', 'UF_HISTORY_SUM', 'UF_ADD_DATE', 'UF_PRICE_ONE', 'UF_ACTIVE_CODE'),
			'filter' => $arFilter,
			'order'	=> array('UF_ADD_DATE'=>'ASC'),
			//'limit'	=> 10,
			));

		   $kDate = '';
			while ($el = $rsData->fetch()) {

				if(empty($kDate)){
					$kDate = $el['UF_ADD_DATE'];
				}
				$currDate = $el['UF_ADD_DATE'];
				//if($currDate!=$kDate) break;
				$rub_summ = $el['UF_HISTORY_SUM'];
				$rate = 1;
				if(!in_array($el['UF_ACTIVE_CODE'], $this->arRoubleCodes)){
					$rate = getCBPrice($el['UF_ACTIVE_CODE'],$date);
					$rub_summ = $el['UF_HISTORY_SUM']*$rate;
				}
				$arResult[$el['UF_ACTIVE_CODE']] = array('ID'=>$el['ID'], 'SUMM'=>floatval($el['UF_HISTORY_SUM']), 'RUB_SUMM'=>floatval($rub_summ), 'RATE'=>floatval($rate));
			}
			return $arResult;
		}

		/** Пересчитывает итоговые данные по кешу на дату сделки в соответствии с ее валютой, либо создает отсутствующий
		 * итог либо удаляет итоговую запись при удалении всех сделок в искомой валюте на текущую дату.
		 * @param $pid	id портфеля
		 * @param $arDeal массив сделки
		 * @param $direction направление сделки
		 * @param $owner владелец запуска операций user|report
		 */
		public function cacheController2($pid, $arDeal, $direction, $owner='user', $addPopupSubzeroOn=false){
			$debug = false;
			$arErrors = array();
			if (!empty($arDeal["editDate"])) {
				$arDeal["dateActive"] = $arDeal["editDate"];
				$arDeal["saleDate"]   = $arDeal["editDate"];
			}
			if (empty($arDeal["dateActive"]) && $direction=="MINUS") {
				$arDeal["dateActive"] = $arDeal["saleDate"];
			}

			//Пересчитываем кеш всего портфеля с текущей даты включительно
			//$this->recountCacheOnPortfolio($pid, $arDeal['dateActive'], false, $owner, $addPopupSubzeroOn);
			$this->recountCacheOnPortfolio($pid, $arDeal['dateActive'], false, $owner);
		}


		/**
		 * Устанавливает флаг разрешения автокомпенсации отрицательного кеша при покупке активов в ручном режиме на дату сделки
		 *
		 * @param  int, string   $pid id портфеля
		 * @param  string   $state Любое непустое значение включает флаг
		 *
		 * @return array  сообщение для ajax вызова
		 *
		 * @access public
		 */
		public function setSubzeroCacheOn($pid, $state=''){
			Global $USER;
			$ELEMENT_ID = intval($pid);
			CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array('SUBZERO_CACHE_ON' => !empty($state)?276:false));
			$this->clearPortfolioListForUserCache($USER->GetID());
			return array("result"=>"ok", "value"=>$state);
		}

		public function getSubzeroCacheOn($pid) {
 		   $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_SUBZERO_CACHE_ON");
			$res = CIBlockElement::GetList(array(), array("ID"=>$pid), false, array(), $arSelect);
			$rsElement = CIBlockElement::GetByID(intval($id));
			if ($row= $res->fetch()) {
			   return $row["PROPERTY_SUBZERO_CACHE_ON_VALUE"];
			} else {
				return '';
			}
		}


		/**
		 * Пересчитывает итоги кеша на каждый день вправо с момента переданной даты
		 *
		 * @param  int, string   $pid id портфеля
		 * @param  string   $dateStart дата в виде строки формата "d.m.Y"
		 * @param  bool   $disableCompensationCache (optional) true - Не делать компенсацию отриц. кеша при загрузке из отчета брокера
		 * @param  string  $owner (optional) user - перерасчет после ручных действий пользователя, report - после загрузки отчета брокера
		 *
		 * @return array  сообщение для ajax вызова
		 *
		 * @access public
		 */
		public function recountCacheOnPortfolio($pid, $dateStart, $disableCompensationCache=false, $owner='user'){
			$trace = debug_backtrace();
			$caller = $trace[1];
			$debug = false;
			//$debug = true;
		  //	$firephp = FirePHP::getInstance(true);
		  //	$firephp->fb(array("recountCacheOnPortfolio"=>$caller['function'], "dateStart"=>$dateStart, "disableCompensationCache"=>$disableCompensationCache, "owner"=>$owner),FirePHP::LOG);
		   $arResult = array();
			Global $DB;
			if(empty($dateStart)){
				$dateStart = $this->getHistoryStartDate($pid);
			}

			if($owner=='user'){  //Проверяем разрешено ли автокомпенсировать отриц. кеш в день сделки при ручном вводе покупки активов
				$subzeroCacheOn = !empty($this->getSubzeroCacheOn($pid))?true:false;
			} else if($owner=='autobalance'){
				$subzeroCacheOn = true;
			} else {
				$subzeroCacheOn = fasle;
			}

			$arErrors = array();
			$delQuery = 'DELETE FROM portfolio_history WHERE UF_CALCULATED = "Y"  AND UF_PORTFOLIO = "'.$pid.'" AND UF_ADD_DATE >="'.(new DateTime($dateStart))->format('Y-m-d').'"';
			$DB->Query($delQuery);//Удаляем все автопополнения/автосписания кеша в будущее с переданной даты включительно
if($debug){
	 CLogger::{'recountCache_'.$pid}();
	 CLogger::{'recountTaxComission_'.$pid}();
	 CLogger::{'recountCache_'.$pid}('Удалены старые итоги и автопополнения');
}
			$arCurrencyList = $this->getCacheCurrencyList();  //список валют доступных к внесению в портфель

			//делаем выборку активов с переданной даты включительно в будущее до текущего дня
			$hlblock_id        = $this->hl_hist_Id; // HL Портфели история
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$rsData = $entity_data_class::getList(array(
			'select' => array('*'),
			'filter' => array('UF_PORTFOLIO' => $pid, '>=UF_ADD_DATE'=>$dateStart),
			'order' => array('UF_ADD_DATE'=>'ASC'),
			));

			$arPortfolioDeals = array();  //Массив сделок по дням

			while ($deal = $rsData->fetch()) {//Обходим все сделки портфеля в будущее
			   $ufAddDate = (new DateTime($deal['UF_ADD_DATE']))->format('d.m.Y'); //Дата движения конкретной записи
			   $currency = strtoupper($deal["UF_CURRENCY"]); //Код валюты (для активов - в чем покупается/продается, для денег код самих денег
				//Если в сделке облига или ETF с валютой актива отличной от рубля - то это еврооблига, заменим код валюты для расчета на код валюты актива
				if(($deal["UF_ACTIVE_TYPE"]=="bond" || $deal["UF_ACTIVE_TYPE"]=="share_etf" || $deal["UF_ACTIVE_TYPE"]=="currency") && !in_array(strtoupper($deal["UF_ACTIVE_CURRENCY"]), $this->arRoubleCodes)){
					if(!empty($deal["UF_ACTIVE_CURRENCY"])){
					$currency = strtoupper($deal["UF_ACTIVE_CURRENCY"]);
					}
				}
			   $secid = strtoupper($deal["UF_SECID"]);
				$isRouble = in_array($currency, $this->arRoubleCodes); //Рублевая сделка или в валюте
				$isCurrency = $deal['UF_ACTIVE_TYPE']=='currency'; //Сделка с активом или с деньгами
			 //	$arPortfolioDeals[$ufAddDate]['DEALS'][] = $deal;  //Собираем сделки в массив по датам их движений
					if($isCurrency){
						$summ = $deal['UF_HISTORY_CNT']; //Сумму для операций с деньгами берем из их кол-ва
					} else {
						$summ = $deal['UF_HISTORY_SUM'];
					}
  	if($debug) CLogger::{'recountCache_'.$pid}($ufAddDate.' Код валюты за покупку '.$deal['UF_SECID'].' = '.$currency);

				   //формируем суммы по сделкам за операционный день для каждой валюты
					if($deal['UF_HISTORY_DIRECTION'] == 'PLUS'){
						if(!$isCurrency) { //Покупки активов
						  if(!$isRouble){  //Если покупается не рублевый актив - переведем сумму покупки в валюту по курсу на день сделки что бы вычесть из дневной суммы в этой валюте
								$rate = getCBPrice($currency,$ufAddDate);
								$summ = round($summ/$rate, 4);
								$arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency] = $arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency] - $summ;
						  } else {
						  	//Если покупается рублевый актив - используем $summ без пересчета по курсу
						  	   $arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency] = $arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency] - $summ;
						  }
  	if($debug) CLogger::{'recountCache_'.$pid}($ufAddDate.' CURRENCY_PLUS_FROM_SELL за покупку '.$deal['UF_SECID'].' в '.$currency.' = '.$arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency]);

						  $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$currency] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$currency] - $summ;
						}
						else if($isCurrency && empty($deal['UF_CLEAR_CACHE'])){
						$baseCurrency = 'RUB';//Если не указана другая валюта покупки текущей валюты (не пара типа EURUSD)

						if(!in_array(strtoupper($deal["UF_ACTIVE_CURRENCY"]), $this->arRoubleCodes)){
							if(!empty($deal["UF_ACTIVE_CURRENCY"])){
						  $baseCurrency = strtoupper($deal["UF_ACTIVE_CURRENCY"]);
						  }
						}
						//Купили валюту, прибавили к этой валюте и вычли из рублей в рублевом эквиваленте
						//Если покупаем валюту (не внесение)
	if($debug) CLogger::{'recountCache_'.$pid}(PHP_EOL.'secid='.$secid.' Дневные суммы сделок на '.$ufAddDate.' до покупки '.PHP_EOL.print_r($arPortfolioDeals[$ufAddDate]['DAY_SUMM'],true));
							$arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$baseCurrency] = $arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$baseCurrency] - $deal['UF_HISTORY_SUM'];
						   $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] + $summ; //Прибавляем к сумме за день в покупаемой валюте
							$arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] = $arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] + $summ; //Прибавляем к сумме денег за день отдельно от активов
						   $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$baseCurrency] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$baseCurrency] - $deal['UF_HISTORY_SUM']; //Вычитаем из суммы за день в рублях
	if($debug) CLogger::{'recountCache_'.$pid}(PHP_EOL.'покупка '.$secid.' '.$ufAddDate.' в сумме = '.$summ);

						}
						else if($isCurrency && $deal['UF_CLEAR_CACHE']=='Y'  ) { //Вненесение денег и амортизация
							   $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] + $summ;
								$arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] = $arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] + $summ; //Прибавляем к сумме денег за день отдельно от активов
							}

					}
					//Расход, при отсутствии запрета обработки кеша для текущей записи сделки
					if($deal['UF_HISTORY_DIRECTION'] == 'MINUS' && $deal['UF_NO_CACHE_OPERATION']!=='Y'){
						if(!$isCurrency) { //Продажи активов
					  		if(!$isRouble){ //Если продается не рублевый актив - переведем сумму продажи в валюту по курсу на день сделки
								$rate = getCBPrice($currency,$ufAddDate);
								$summ = round($summ/$rate, 4);
								$arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency] = $arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency] + $summ;
								$arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$currency] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$currency] + $summ;
							} else {
								$arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$currency] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$currency] + $summ;
								$arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency] = $arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$currency] + $summ;
							}
						}
						if($deal['UF_ACTIVE_TYPE']=='currency' && $deal['UF_CLEAR_CACHE']=='Y' && empty($deal['UF_CALCULATED'])) { //Изъятие валют
						   $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] - $summ;
							$arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] = $arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] - $summ;
						}

						//Продажа валюты. Не считаем налоги и комиссии. Рубли не продаются, только изымаются или списываются при покупке
						//игнорируем налоги и комиссии, что бы не вносилась сумма от продажи валюты, т.к. они работают на основе продажи валюты, только без внесения автокомпенсаций
					  	if($deal['UF_ACTIVE_TYPE']=='currency' && empty($deal['UF_CLEAR_CACHE']) &&
					  	($deal['UF_CASHFLOW_TYPE']!='tax' && $deal['UF_CASHFLOW_TYPE']!='comission') ) {
  						$baseCurrency = 'RUB';//Если не указана другая валюта покупки текущей валюты (не пара типа EURUSD)

						if(!in_array($deal["UF_ACTIVE_CURRENCY"], $this->arRoubleCodes)){
							if(!empty($deal["UF_ACTIVE_CURRENCY"])){
						  			$baseCurrency = strtoupper($deal["UF_ACTIVE_CURRENCY"]);
						  }
						}
	if($debug) CLogger::{'recountCache_'.$pid}(PHP_EOL.'продажа '.$secid.' '.$ufAddDate.' '.PHP_EOL.$summ);
						   $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] - $summ; //Вычитаем из суммы за день в продаваемой валюте
							$arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] = $arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] - $summ;
							$arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$baseCurrency] = $arPortfolioDeals[$ufAddDate]['CURRENCY_PLUS_FROM_SELL'][$baseCurrency] + $deal['UF_HISTORY_SUM'];
						   $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$baseCurrency] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$baseCurrency] + $deal['UF_HISTORY_SUM']; //Прибавляем к сумме за день в рублях
						}
						//Если это налог или комиссия - собираем дневной итог по ним для последующего вычитания из дневного кеша при расчете TOTAL за дату дня
						if($deal['UF_ACTIVE_TYPE']=='currency' && ($deal['UF_CASHFLOW_TYPE']=='tax' || $deal['UF_CASHFLOW_TYPE']=='comission')){
							$arPortfolioDeals[$ufAddDate]['DAY_TAX_COMISSION_SUMM'][$secid] = $arPortfolioDeals[$ufAddDate]['DAY_TAX_COMISSION_SUMM'][$secid] + $summ;
							$arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] = $arPortfolioDeals[$ufAddDate]['DAY_SUMM'][$secid] - $summ; //Вычитаем из суммы за день в валюте взимания налога или комиссии
							$arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] = $arPortfolioDeals[$ufAddDate]['CURRENCY'][$secid] - $summ;
						}
					}
			} //- Обходим все сделки портфеля в будущее

	if($debug) CLogger::{'recountCache_'.$pid}('Суммы по операционным дням '.PHP_EOL.print_r($arPortfolioDeals, true));

 			//Получаем остаток кеша на начало даты текущей операции

			$arCacheBegin = $this->getCacheOnBeginDate($pid, $dateStart);
			$arPortfolioDeals[$dateStart]['BEGIN_CACHE'] = $arCacheBegin; //Задаем найденный кеш на начало даты старта для дальнейшего расчета
			 foreach($arPortfolioDeals[$keyDate]['BEGIN_CACHE'] as $currencyKey=>$summ){
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' Кеш на начало стартовый '.$currencyKey.' = '.$summ["SUMM"]);
				}

	 	   //Обходим собринный массив дневных сумм и делаем итоговые автодобавления/автосписания
			foreach($arPortfolioDeals as $keyDate=>$arDealsData){

				if(!array_key_exists('BEGIN_CACHE',$arPortfolioDeals[$keyDate])){//Если на дату нет кеша на начало - формируем из ранее полученного кеша на стартовую дату запроса
					$arPortfolioDeals[$keyDate]['BEGIN_CACHE'] = $arCacheBegin;
				}


			 foreach($arPortfolioDeals[$keyDate]['BEGIN_CACHE'] as $currencyKey=>$summ){
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' Весь кеш на начало дн. '.$currencyKey.' = '.$summ["SUMM"]);
				}

				$arCacheBegin = array();

 				foreach($arDealsData['DAY_SUMM'] as $currencyKey => $daySumm){

/*  						 echo $keyDate." daySumm= ".$daySumm." <pre  style='color:black; font-size:11px;'>";
                      print_r($arPortfolioDeals[$keyDate]['BEGIN_CACHE']);
                      echo "</pre>";*/


					$rate = 1;
					if(!in_array($currencyKey, $this->arRoubleCodes)){ $rate = getCBPrice($currencyKey,$keyDate); }

					//Сумма на начало дня + сумма за день в валюте итерации
					$beginAndDaySumm = $arPortfolioDeals[$keyDate]['BEGIN_CACHE'][$currencyKey]['SUMM']+$daySumm;
if($debug){
    CLogger::{'recountCache_'.$pid}($keyDate.' сумма дня '.$currencyKey.' = '.$daySumm);
	 CLogger::{'recountCache_'.$pid}($keyDate.' кеш на нач. дня '.$currencyKey.' = '.$arPortfolioDeals[$keyDate]['BEGIN_CACHE'][$currencyKey]['SUMM']);
	 CLogger::{'recountCache_'.$pid}($keyDate.' CURRENCY '.$currencyKey.' = '.$arPortfolioDeals[$keyDate]['CURRENCY'][$currencyKey]);
	 CLogger::{'recountCache_'.$pid}($keyDate.' CURRENCY_PLUS_FROM_SELL '.$currencyKey.' = '.$arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]);
	 CLogger::{'recountCache_'.$pid}($keyDate.' начало + сумма дня '.$currencyKey.' = '.$beginAndDaySumm);
}
					//Если сумма на конец дня отличается от суммы денег за день то делаем внесения или компенсации отрицательных итогов дня
  //					if($arPortfolioDeals[$keyDate]['CURRENCY'][$currencyKey]!=$beginAndDaySumm) {
					//if($arPortfolioDeals[$keyDate]['BEGIN_CACHE'][$currencyKey]['SUMM']+$daySumm<0) {

					//Вносим деньги от продажи активов
						if($arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]>0){ //Используем заранее посчитанную сумму внесения денег
						   $addSumm = $arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey];
							$arFields = array('UF_PORTFOLIO'=>$pid,
											  'UF_LOTPRICE'=>$rate,
											  'UF_LOTCNT'=>abs($addSumm),
											  'UF_ACTIVE_ID'=>$arCurrencyList[$currencyKey]['ID'],
											  'UF_ACTIVE_TYPE'=>'currency',
											  'UF_ADD_DATE'=>(new DateTime($keyDate))->format('Y-m-d'),
											  'UF_ACTIVE_NAME'=>$arCurrencyList[$currencyKey]['NAME'],
											  'UF_ACTIVE_URL'=>$arCurrencyList[$currencyKey]['DETAIL_PAGE_URL'],
											  'UF_EMITENT_NAME'=>$arCurrencyList[$currencyKey]['NAME'],
											  'UF_EMITENT_URL'=>$arCurrencyList[$currencyKey]['DETAIL_PAGE_URL'],
											  'UF_ACTIVE_CODE'=>$currencyKey,
											  'UF_CURRENCY'=>strtolower($currencyKey),
											  'UF_ACTIVE_LASTPRICE'=>$arCurrencyList[$currencyKey]['LASTPRICE'],
											  'UF_HISTORY_CNT'=>abs($addSumm),
											  'UF_HISTORY_PRICE'=>$rate,
											  'UF_HISTORY_SUM'=>round(abs($addSumm)*$rate, 4),
											  'UF_HISTORY_DIRECTION'=>'PLUS',
											  'UF_HISTORY_ACT'=>'Внесение',
											  'UF_HISTORY_DATA'=>(new DateTime())->format('Y-m-d'),
											  'UF_SECID'=>$currencyKey,
											  'UF_PRICE_ONE'=>$rate,
											  'UF_INLOT_CNT'=>1,
											  'UF_DATE_PRICE_ONE'=>$rate,
											  'UF_AUTO'=>'Y',
											  //'UF_CLEAR_CACHE'=>'Y',
											  'UF_CALCULATED' => 'Y',
											  'UF_DESCRIPTION'=>'Внесение от продажи бумаг',
											  'UF_FINISHED'=>"0");
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' Внесение от продажи бумаг '.$currencyKey.' = '.abs($addSumm));
							$this->addSQLHistory($arFields);
						}

						//Компенсация отрицательного итога дня по валюте
						$sub_compensation = 0; //Сумма компенсации отриц. кеша

						/* Если сумма на нач.дня+сумма за день < 0 и не запрещена общая компенсация отриц.кеша
						и пользователем включен флаг автопополнения и дата сделки равана дате в момент обсчета с минусовой суммой - компенсируем отриц. кеш */

					 if ($beginAndDaySumm<0 && !$disableCompensationCache)
						if (($subzeroCacheOn==true && $keyDate==$dateStart) || ($subzeroCacheOn==true && (new DateTime($keyDate))->getTimestamp()>(new DateTime($dateStart))->getTimestamp()) || $owner=='autobalance'){
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' $beginAndDaySumm. (блок компенс.) = '.$beginAndDaySumm);
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' CURRENCY_PLUS_FROM_SELL (блок компенс.) = '.PHP_EOL.print_r($arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'], true));
						//  $addSumm = $arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey];
						//  $addSumm = strval($arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey])-$arPortfolioDeals[$keyDate]['BEGIN_CACHE'][$currencyKey]['SUMM'];
						//  $addSumm = $arPortfolioDeals[$keyDate]['BEGIN_CACHE'][$currencyKey]['SUMM']+strval($arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]);
							$addSumm = $beginAndDaySumm;
							$sub_compensation = $addSumm;
							$arFields = array('UF_PORTFOLIO'=>$pid,
											  'UF_LOTPRICE'=>$rate,
											  'UF_LOTCNT'=>$DB->ForSql(abs($addSumm)),
											  'UF_ACTIVE_ID'=>$arCurrencyList[$currencyKey]['ID'],
											  'UF_ACTIVE_TYPE'=>'currency',
											  'UF_ADD_DATE'=>(new DateTime($keyDate))->format('Y-m-d'),
											  'UF_ACTIVE_NAME'=>$arCurrencyList[$currencyKey]['NAME'],
											  'UF_ACTIVE_URL'=>$arCurrencyList[$currencyKey]['DETAIL_PAGE_URL'],
											  'UF_EMITENT_NAME'=>$arCurrencyList[$currencyKey]['NAME'],
											  'UF_EMITENT_URL'=>$arCurrencyList[$currencyKey]['DETAIL_PAGE_URL'],
											  'UF_ACTIVE_CODE'=>$currencyKey,
											  'UF_CURRENCY'=>strtolower($currencyKey),
											  'UF_ACTIVE_LASTPRICE'=>$arCurrencyList[$currencyKey]['LASTPRICE'],
											  'UF_HISTORY_CNT'=>$DB->ForSql(abs($addSumm)),
											  'UF_HISTORY_PRICE'=>$rate,
											  'UF_HISTORY_SUM'=>$DB->ForSql(round(abs($addSumm)*$rate, 4)),
											  'UF_HISTORY_DIRECTION'=>'PLUS',
											  'UF_HISTORY_ACT'=>'Внесение',
											  'UF_HISTORY_DATA'=>(new DateTime())->format('Y-m-d'),
											  'UF_SECID'=>$currencyKey,
											  'UF_PRICE_ONE'=>$rate,
											  'UF_INLOT_CNT'=>1,
											  'UF_DATE_PRICE_ONE'=>$rate,
											  //'UF_AUTO'=>'Y',
											  'UF_CLEAR_CACHE'=>'Y',
											  'UF_CALCULATED' => 'Y',
											  'UF_DESCRIPTION'=>'Компенсация отрицательного кеша',
											  'UF_FINISHED'=>'0');
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' Автопополнение '.$currencyKey.' '.abs($addSumm));
							$this->addSQLHistory($arFields);

						}
					//Массив полей для использования штатной функции удаления активов
					$arAutoinsertCurrencyFields = array(
					'typeActive'=>'currency',
					'idActive'=>$arCurrencyList[$currencyKey]['ID'],
					'nameActive'=>$arCurrencyList[$currencyKey]['NAME'],
					'priceActive'=>$rate,
					'cntActive'=>abs($daySumm),
					'dateActive'=>$keyDate,
					'codeActive'=>$currencyKey,
					'urlActive'=>$arCurrencyList[$currencyKey]['DETAIL_PAGE_URL'],
					'currencyEmitent'=>strtolower($currencyKey),
					'nameActiveEmitent'=>$arCurrencyList[$currencyKey]['NAME'],
					'urlActiveEmitent'=>$arCurrencyList[$currencyKey]['DETAIL_PAGE_URL'],
					'lastpriceActive'=>$rate,
					'lastpriceActiveValute'=>$rate,
					'secid'=>$currencyKey,
					'price_one'=>$rate,
					'lotsize'=>1,
					'date_price_one'=>$rate,
					'insert'=>'Y',
					);

					   //Если сумма денег за день меньше нуля - делаем изъятие (выше уже добавлена сумма для компенсации отриц. суммы за день)
						//if ( $daySumm < 0 ){                             //Возможно добавить в условие && $subzeroCacheOn==true && $keyDate==$dateStart
						$haveExclude = false;
						//if($beginAndDaySumm<0 && !$disableCompensationCache){
// $firephp = FirePHP::getInstance(true);
// $firephp->fb(array("keydate"=>$keyDate, "dayTax"=>$arPortfolioDeals[$keyDate]['DAY_TAX_COMISSION_SUMM'][$currencyKey],"daySumm"=>abs($beginAndDaySumm)),FirePHP::LOG);

						if($beginAndDaySumm<0 && (floatval($arPortfolioDeals[$keyDate]['DAY_TAX_COMISSION_SUMM'][$currencyKey])<abs($beginAndDaySumm)) && !$disableCompensationCache){
							$haveExclude = true;
							unset($arAutoinsertCurrencyFields['insert']);
							$arAutoinsertCurrencyFields['UF_AUTO'] = 'Y';
							//$arAutoinsertCurrencyFields['exclude'] = 'Y';
							$arAutoinsertCurrencyFields['actionType'] = '';
							$arAutoinsertCurrencyFields['cnt'] = $arAutoinsertCurrencyFields['cntActive'];
							$arAutoinsertCurrencyFields['UF_LOTCNT'] = abs($daySumm);
							$arAutoinsertCurrencyFields['UF_HISTORY_CNT'] = abs($daySumm);
							$arAutoinsertCurrencyFields['UF_HISTORY_SUM'] = abs($daySumm);
							$arAutoinsertCurrencyFields['UF_DESCRIPTION'] = 'Изъятие по отрицательному итогу за день';
							$arAutoinsertCurrencyFields['UF_CALCULATED'] = 'Y';
							$this->deletePortfolioCurrency($pid, $arAutoinsertCurrencyFields, '', $this->arHistoryActions["CACHE_MINUS"], true, true);
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' Изъятие по отрицательному итогу за день CNT='.$arAutoinsertCurrencyFields['cntActive'].' UF_HISTORY_SUM='.abs($daySumm));
						}

	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' '.$currencyKey.' CURRENCY_PLUS_FROM_SELL='.$arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey].' $daySumm='.abs($daySumm));

						//Делаем, только если не было изъятия по отр. итогу за день или если суммы вложенных/внесенных денег за день равна сумме покупок за бумаги
						if ($arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]<0 && $daySumm!=0 && $owner!='autobalance' && !$haveExclude){
							unset($arAutoinsertCurrencyFields['insert']);
							$Summ = $arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]-$sub_compensation; //Сумма затрат на покупку бумаг + сумма на нач.дня+пополнения за день.
							$arAutoinsertCurrencyFields['UF_AUTO'] = 'Y';
							//$arAutoinsertCurrencyFields['exclude'] = 'Y';
							$arAutoinsertCurrencyFields['cnt'] = abs($Summ);
							$arAutoinsertCurrencyFields['cntActive'] = abs($Summ);
							$arAutoinsertCurrencyFields['actionType'] = '';
							$arAutoinsertCurrencyFields['UF_LOTCNT'] = abs($Summ);
							$arAutoinsertCurrencyFields['UF_HISTORY_CNT'] = abs($Summ);
							$arAutoinsertCurrencyFields['UF_HISTORY_SUM'] = abs($Summ);
							$arAutoinsertCurrencyFields['UF_DESCRIPTION'] = 'Списание по покупке бумаг';
							$arAutoinsertCurrencyFields['UF_CALCULATED'] = 'Y';
							$this->deletePortfolioCurrency($pid, $arAutoinsertCurrencyFields, '', $this->arHistoryActions["CACHE_MINUS"], true, true);
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' Списание по покупке бумаг '.$currencyKey.' = '.$Summ);
						}
						  $tmpSumm = abs($arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]-$addSumm);
   if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' haveExclude='.$haveExclude.' Сумма покупки минус внесенная в текущую дату = '.$tmpSumm.' CURRENCY = '. abs($arPortfolioDeals[$keyDate]['CURRENCY'][$currencyKey]).' CURRENCY_PLUS_FROM_SELL=' . $arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]);

						  if ($arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]<0 &&
						  		(isset($arPortfolioDeals[$keyDate]['CURRENCY'][$currencyKey]) && $tmpSumm==abs($arPortfolioDeals[$keyDate]['CURRENCY'][$currencyKey])) &&
								$owner!='autobalance' ){
						   unset($arAutoinsertCurrencyFields['insert']);
							$Summ = $arPortfolioDeals[$keyDate]['CURRENCY_PLUS_FROM_SELL'][$currencyKey]-$sub_compensation; //Сумма затрат на покупку бумаг + сумма на нач.дня+пополнения за день.
							$arAutoinsertCurrencyFields['UF_AUTO'] = 'Y';
							//$arAutoinsertCurrencyFields['exclude'] = 'Y';
							$arAutoinsertCurrencyFields['cnt'] = abs($Summ);
							$arAutoinsertCurrencyFields['cntActive'] = abs($Summ);
							$arAutoinsertCurrencyFields['actionType'] = '';
							$arAutoinsertCurrencyFields['UF_LOTCNT'] = abs($Summ);
							$arAutoinsertCurrencyFields['UF_HISTORY_CNT'] = abs($Summ);
							$arAutoinsertCurrencyFields['UF_HISTORY_SUM'] = abs($Summ);
							$arAutoinsertCurrencyFields['UF_DESCRIPTION'] = 'Списание по покупке бумаг';
							$arAutoinsertCurrencyFields['UF_CALCULATED'] = 'Y';
							$this->deletePortfolioCurrency($pid, $arAutoinsertCurrencyFields, '', $this->arHistoryActions["CACHE_MINUS"], true, true);
	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' Списание по покупке бумаг '.$currencyKey.' = '.$Summ);
						}

					//Подводим итоги - дописываем записи остатков кеша на конец дня
					$TotalDayCalc = $arPortfolioDeals[$keyDate]['BEGIN_CACHE'][$currencyKey]['SUMM'] + $daySumm;

					if(floatval($arPortfolioDeals[$keyDate]['DAY_TAX_COMISSION_SUMM'][$currencyKey])>0){
				  //	  $TotalDayCalc = $TotalDayCalc - floatval($arPortfolioDeals[$keyDate]['DAY_TAX_COMISSION_SUMM'][$currencyKey]);
					}

					/* Если итог отрицательный то приводим его к нулю и не запрещена общая компенсация отриц.кеша
						и пользователем включен флаг автопополнения и дата сделки равана дате в момент обсчета с минусовой суммой - приводим итог к нулю */
					if($TotalDayCalc<0 && $disableCompensationCache==false){

					//if(($subzeroCacheOn==true && $keyDate>=$dateStart) || $owner=='autobalance'){
					//if(($subzeroCacheOn==true && $keyDate<=$dateStart) || $owner=='autobalance'){
					if(($subzeroCacheOn==true) || $owner=='autobalance'){
					  	$TotalDayCalc = 0;
  if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' >= '.$dateStart.' TotalDayCalc = '.$TotalDayCalc);
					 }
					 }
					$arPortfolioDeals[$keyDate]['END_CACHE'][$currencyKey]['SUMM'] = $TotalDayCalc;

					//$arCacheBegin[$currencyKey]['SUMM'] = $TotalDayCalc;
					$arFields = array('UF_HISTORY_SUM' => strval($TotalDayCalc),
										  'UF_ADD_DATE' => (new DateTime($keyDate))->format('Y-m-d'),
										  'UF_ACTIVE_TYPE' => 'cache',
										  'UF_HISTORY_DIRECTION' => 'TOTAL',
										  'UF_CALCULATED' => 'Y',
										  'UF_CURRENCY'=> strtolower($currencyKey),
										  'UF_PORTFOLIO'=> $pid,
										  'UF_SECID'=> $currencyKey,
										  'UF_ACTIVE_CODE'=> $currencyKey,
										  'UF_PRICE_ONE'=> $rate
					);
				  $this->addSQLHistory($arFields);

	if($debug) CLogger::{'recountCache_'.$pid}($keyDate.' Добавлен. итог '.$currencyKey.' = '.$TotalDayCalc);

					} //- foreach $arDealsData['DAY_SUMM'] as $currencyKey => $daySumm

			 $arCacheBegin = $this->getCacheOnBeginDate($pid, $keyDate, array(), false);
			} //- foreach $arPortfolioDeals as $keyDate=>$arDealsData


		  if(count($arErrors)>0){

			$arResult = array("result"=>"error", "message"=>implode('; ',$arErrors));
		  } else {
		  //Получаем суммы инвестиций и текущую стоимость портфеля и обновляем в инфоблоке текущему портвелю
		  //TODO Включить если не обновляются суммы в инфоблоке портфеля при прочих действиях
		  	$arStatSumm = $this->calcStatSumm($pid);
		  	$this->updatePortfolioStatSumm($arStatSumm);

		  	$arResult = array("result"=>"ok", "message"=>"История портфеля пересчитана");
		  }


		  return $arResult;
		}

		public function addSQLHistory($arFields=array()){
			global $DB;
			if(count($arFields)==0) return false;

			$arTmpFields = array();
			foreach($arFields as $k=>$v){
				if(empty($v)) continue;
				$arTmpFields[$k] = "'".$DB->ForSql($v)."'";
			}

			$ID = $DB->Insert("portfolio_history", $arTmpFields, $err_mess.__LINE__);

 	 //		$firephp = FirePHP::getInstance(true);
    //     $firephp->fb(array("addToHistory"=>$arTmpFields, "ID"=>$ID, "strError"=>$strError),FirePHP::LOG);

			return true;
			//$addQuery = 'INSERT INTO `finplan_dev`.`portfolio_history` (`ID`, `UF_PORTFOLIO`, `UF_LOTPRICE`, `UF_LOTCNT`, `UF_INVEST_SUM`, `UF_PERIOD_INCREASE`, `UF_ACTIVE_ID`, `UF_ACTIVE_TYPE`, `UF_ADD_DATE`, `UF_ACTIVE_NAME`, `UF_ACTIVE_URL`, `UF_EMITENT_NAME`, `UF_EMITENT_URL`, `UF_ACTIVE_CODE`, `UF_CURRENCY`, `UF_ACTIVE_LASTPRICE`, `UF_HISTORY_CNT`, `UF_HISTORY_PRICE`, `UF_HISTORY_SUM`, `UF_HISTORY_DIRECTION`, `UF_HISTORY_ACT`, `UF_HISTORY_DATA`, `UF_SECID`, `UF_PRICE_ONE`, `UF_INLOT_CNT`, `UF_DATE_PRICE_ONE`, `UF_DEAL_ID`, `UF_AUTO`, `UF_PARENT_ID`, `UF_CLEAR_CACHE`, `UF_DESCRIPTION`, `UF_REPORT_DEAL_ID`, `UF_PARSER_VER`, `UF_FINISHED`) VALUES (NULL, '407199', '1', '505.8', NULL, NULL, '403799', 'currency', '2020-09-14', 'Рубль', '/lk/currency/RUB/', 'Рубль', '/lk/currency/RUB/', 'RUB', 'rub', '1', '505.8', '1', '505.8', 'PLUS', 'Внесение', NULL, 'RUB', '1', '1', '1', NULL, 'Y', NULL, 'Y', 'Компенсация отрицательного кеша', NULL, NULL, '1');';

		}

		/**
		 * Возвращает массив со всеми существующими сделками по валютам
		 *
		 * @param  int   $pid id портфеля
		 *
		 * @return array  Массив найденных сделок по валютам с ключами по кодам валют
		 *
		 * @access public
		 */
		public function findCurrencyDeals($pid, $dealDate = '') {
			global $APPLICATION;
			$arCurrencyCodes = array_keys($APPLICATION->currencies);
			$arCurrDeals     = array();

			//Получаем ID элементов валют для дальнейшего удаления/добавления
			$arSelect     = Array("ID", "NAME", "IBLOCK_ID", "CODE");
			$arFilter     = Array("IBLOCK_ID" => 54, "ACTIVE" => "Y", "CODE" => $arCurrencyCodes);
			$res1         = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$arCurrencyId = array();
			while ($ob1 = $res1->fetch()) {
				$arCurrencyId[$ob1["CODE"]] = $ob1["ID"];
			}

			$hlblock_id        = 28; // HL Портфели сделки
			$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();
			$resHL             = $entity_data_class::getList(array(
				"select" => array(
					"*",
				),
				"filter" => array("UF_ACTIVE_TYPE" => "currency", "UF_SECID" => $arCurrencyCodes, "UF_PORTFOLIO" => $pid, "<=UF_ADD_DATE" => $dealDate),
				"order" => array("UF_SECID" => "ASC"),
			));

			while ($ob = $resHL->fetch()) {
				if (!in_array($ob["UF_SECID"], $this->arRoubleCodes)) {
					$price                          = $this->getHistActionsPrices($ob["UF_ACTIVE_TYPE"], $dealDate, $ob["UF_SECID"], '');
					$ob["UF_LOTPRICE"]              = $price["price"];
					$ob["UF_LOTPRICE_ON_DEAL_DATE"] = $price["price"];
				}
				$arCurrDeals[$ob["UF_SECID"]]["DEALS"][] = $ob;
				if ($ob["UF_HISTORY_DIRECTION"] == "PLUS") {
					$arCurrDeals[$ob["UF_SECID"]]["RUB"] += round(($ob["UF_LOTPRICE"] * $ob["UF_LOTCNT"]), 3);
					$arCurrDeals[$ob["UF_SECID"]]["CNT"] += round($ob["UF_LOTCNT"], 2);
				} else {
					$arCurrDeals[$ob["UF_SECID"]]["RUB"] -= round(($ob["UF_LOTPRICE"] * $ob["UF_LOTCNT"]), 3);
					$arCurrDeals[$ob["UF_SECID"]]["CNT"] -= round($ob["UF_LOTCNT"], 2);
				}
				if (!array_key_exists("ID", $arCurrDeals[$ob["UF_SECID"]])) {
					$arCurrDeals[$ob["UF_SECID"]]["ID"]   = $arCurrencyId[$ob["UF_SECID"]];
					$arCurrDeals[$ob["UF_SECID"]]["NAME"] = $ob["UF_ACTIVE_NAME"];
				}
			}

			$arTmp = array();
			foreach ($arCurrDeals as $k => $deal) {
				if ($deal["CNT"] == 0) {
					continue;
				}

				$arTmp[$k] = $deal;
			}
			$arCurrDeals = $arTmp;
			unset($arTmp);
			return $arCurrDeals;
		}


  /**
   * Возвращает список событий (купоны, дивиденды) для активов портфеля с учетом фильтра по датам
   *
   * @param  [add type]   $pid   id Портфеля
   * @param  [add type]   $request (Optional) фильтр из реквеста
   *
   * @return [add type]  массив событий для построения таблицы
   *
   * @access public
   */
  public function getPortfolioEventList($pid, $request=array()){
		 $cache_id = md5(serialize(array($uid, $request)));
        $cache_dir = "/portfolio/getPortfolioEventList";
        $obCache = new CPHPCache;
        //if($obCache->InitCache($this->cacheOn?86400*7:0, $cache_id, $cache_dir))
        if($obCache->InitCache(0, $cache_id, $cache_dir))
        {
            $arResult = $obCache->GetVars();
				$from = "cache";
        }
        elseif($obCache->StartDataCache())
        {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);  //Старт тегирования

       	   $arResult = $this->getPortfolioEventListCached($pid, $request);

            $CACHE_MANAGER->RegisterTag("PortfolioEventList_".$pid); //Отметка тегом
            $CACHE_MANAGER->EndTagCache(); //Финализация тегирования

            $obCache->EndDataCache($arResult);
				$from = "real";
        }
        else
        {
            $arResult = array();
				$from = "no";
        }

	//	  $firephp = FirePHP::getInstance(true);
   //     $firephp->fb(array("getPortfolioEventList"=>$from, "pid"=>$pid, "filter"=>$request),FirePHP::LOG);

		  return $arResult;
  }


  /**
   * Очистка всех видов кеша портфеля сразу
   *
   * @param  int, string   $pid [add description]
   *
   * @access public
   */
  public function clearCacheComplex($pid, $operation=''){

			$trace = debug_backtrace();
			$caller = $trace[1];

	//		$firephp = FirePHP::getInstance(true);
	//		$firephp->fb(array("clearCacheComplex"=>$caller['function'], "pid"=>$pid, "operation"=>$operation),FirePHP::LOG);


		$this->cleanGraphDataCache($pid);
		$this->cleanPortfolioEventListCache($pid);
		$this->cleanPortfolioHistory($pid);

		//$firephp = FirePHP::getInstance(true);
      //$firephp->fb(array("ClearCacheComplex"=>"pid ".$pid,"operation"=>$operation),FirePHP::LOG);
  }

  public function cleanPortfolioEventListCache($pid){
			global $CACHE_MANAGER;
			$CACHE_MANAGER->ClearByTag("PortfolioEventList_".$pid);
  }

  public function getPortfolioEventListCached($pid, $request=array()){
	 $arResult = array();
	 $portfolioActives = array();
			if (count($_REQUEST['filter']) > 0) {
				foreach ($_REQUEST['filter'] as $k => $val) {
					$key = key($val);
					if ($key == 'active') {
						$arFilter[$key] = $val[$key];
					}
					if ($key == 'activeCode') {
						$arFilter[$key] = $val[$key];
					}
				}
			} else {
				$arFilter = array();
			}
		//$portfolioActives = $this->getPortfolioActivesList($pid, false, false, array());
		$arTmpHistory = $this->getPortfolioHistory($pid, $arFilter); //0.09 - 1.3 s


		if(count($arTmpHistory)){
			$startDate = $this->getHistoryStartDate($pid);

			$arEvents = array();

			$arActions = array();
			$arActionsUsa = array();
			$arBonds = array();

			foreach ($arTmpHistory as $k => $arElement) {
				$addDate = $arElement["UF_ADD_DATE"]->format('d.m.Y');
				//Формируем массив кодов и акций РФ и eft для получения цен на даты их добавления.
				if ($arElement["UF_ACTIVE_TYPE"] == 'share' || $arElement["UF_ACTIVE_TYPE"] == 'share_etf') {
 					$arActions[$arElement["UF_ACTIVE_ID"]]               = $arElement["UF_ACTIVE_CODE"];
				} else if ($arElement["UF_ACTIVE_TYPE"] == 'bond') {
					$arBonds[$arElement["UF_ACTIVE_ID"]]                      = $arElement["UF_ACTIVE_CODE"];
				} else if ($arElement["UF_ACTIVE_TYPE"] == 'share_usa') {
					$arActionsUsa[$arElement["UF_ACTIVE_ID"]]            = $arElement["UF_ACTIVE_CODE"];
				}



				if (!array_key_exists($arElement["UF_ACTIVE_CODE"], $portfolioActives)) {
					$portfolioActives[$arElement["UF_ACTIVE_CODE"]] = array(
					"UF_SECID" => $arElement["UF_SECID"],
					"UF_ACTIVE_CODE" => $arElement["UF_ACTIVE_CODE"],
					"UF_LOTCNT" => $arElement["UF_LOTCNT"],
					"UF_INLOT_CNT" => $arElement["UF_INLOT_CNT"],
					"UF_ACTIVE_URL" => $arElement["UF_ACTIVE_URL"],
					"UF_EMITENT_NAME" => $arElement["UF_EMITENT_NAME"],
					"UF_EMITENT_URL" => $arElement["UF_EMITENT_URL"]
					);
				}
			}



		//if(count($arActions)>0)
    		$this->getDividendsData($arEvents, array_values($arActions), $startDate, $pid);
		//if(count($arActionsUsa)>0)
	  	  	$this->getDividendsUsaData($arEvents, array_values($arActionsUsa), $startDate, $pid, true);
		//if(count($arBonds)>0)
			$this->getCoupons($arEvents, array_values($arBonds), $startDate, $pid);

		 } //count($portfolioActives)

		 $endDate = (new DateTime($request["dateto"]));
		 $startDatePost = (new DateTime(!isset($request["date"])?$startDate:$request["date"]));
		 if(count($portfolioActives)>0 && count($arEvents)>0){

		  	 //$firephp = FirePHP::getInstance(true);
		    //$firephp->fb(array("arEvents"=>$arEvents),FirePHP::LOG);

			foreach($arEvents as $eventDate=>$eventType){
				if( (new DateTime($eventDate))->getTimestamp()<$startDatePost->getTimestamp() || (new DateTime($eventDate))->getTimestamp()>$endDate->getTimestamp() ) continue;
				foreach($eventType as $eventTypeKey=>$arEventsList){
					foreach($arEventsList as $k=>$arEventDetail){
						switch ($eventTypeKey) {
		                case "COUPONS":
								  if(floatval($arEventDetail['-'])==0) continue;
                            if($request['valuteOnly']=='y' && ($arEventDetail['valute']!='Y' || !isset($arEventDetail['valute']))){ continue; }
							  	  $arPortfElement = $this->getPortfolioElemBySecid($portfolioActives, $arEventDetail['ITEM']);

							     if(count($arPortfElement)){
							     //$curr_TimeReal = (new DateTime($eventDate))->modify('- '.$APPLICATION->offsetTorgDates['coupons'].' days')->format('d.m.Y');
								  //$countActiveOnDate = $portfolio->getActiveCountOnEventDate($portf_id, $arPortfElement['UF_ACTIVE_CODE'], $curr_TimeReal);

							     //$arEventDetail["COUNT"] = $countActiveOnDate;
								  $arEventDetail["SUMM"] = $arEventDetail["COUNT"]*$arEventDetail['-'];
								  $arEventDetail["SUM_ONE"] = $arEventDetail['-'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['ITEMS'] = $arEventDetail;
								  $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['COUNT'] = $arEventDetail["COUNT"];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_ACTIVE_URL'] = $arPortfElement["UF_ACTIVE_URL"];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_EMITENT_URL'] = $arPortfElement['UF_EMITENT_URL'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_EMITENT_NAME'] = $arPortfElement['UF_EMITENT_NAME'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_ACTIVE_NAME'] = $arEventDetail["NAME"];
								  $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']["SUMM"] += $arEventDetail["SUMM"];
								  }
								  $arPortfElement = array();
		                break;

							 case "DIVIDENDS_USA":
		 					 $arPortfElement = $this->getPortfolioElemBySecid($portfolioActives, $arEventDetail['ITEM']);
							     if(count($arPortfElement)){
							        if($request['valuteOnly']=='y' && $arEventDetail['valute']!='Y'){ continue; }
							     //$curr_TimeReal = (new DateTime($eventDate))->modify('- '.$APPLICATION->offsetTorgDates['dividends'].' days')->format('d.m.Y');
								  //$countActiveOnDate = $portfolio->getActiveCountOnEventDate($portf_id, $arPortfElement['UF_ACTIVE_CODE'], $curr_TimeReal);
							     //$arEventDetail["COUNT"] = $countActiveOnDate*$arPortfElement['UF_INLOT_CNT'];

							     $arEventDetail["COUNT"] = $arEventDetail["COUNT"]*$arPortfElement['UF_INLOT_CNT'];
								  $arEventDetail["SUMM"] = $arEventDetail["COUNT"]*$arEventDetail['Дивиденды руб'];
								  $arEventDetail["SUM_ONE"] = $arEventDetail['Дивиденды руб'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['ITEMS'] = $arEventDetail;
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['COUNT'] = $arEventDetail["COUNT"];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_ACTIVE_URL'] = $arPortfElement["UF_ACTIVE_URL"];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_ACTIVE_NAME'] = $arEventDetail["NAME"];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_EMITENT_URL'] = $arPortfElement['UF_EMITENT_URL'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_EMITENT_NAME'] = $arPortfElement['UF_EMITENT_NAME'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']["SUMM"] += $arEventDetail["SUMM"];
								  }
								  $arPortfElement = array();
							 break;
		                case "DIVIDENDS":
							 $arPortfElement = $this->getPortfolioElemBySecid($portfolioActives, $arEventDetail['ITEM']);

							     if(count($arPortfElement)){
                                     if($request['valuteOnly']=='y' && ($arEventDetail['valute']!='Y' || !isset($arEventDetail['valute']))){ continue; }
                                 
							     //$curr_TimeReal = (new DateTime($eventDate))->format('d.m.Y');
							     //$curr_TimeReal = (new DateTime($eventDate))->modify('- '.$APPLICATION->offsetTorgDates['dividends'].' days')->format('d.m.Y');
								  //$countActiveOnDate = $portfolio->getActiveCountOnEventDate($portf_id, $arPortfElement['UF_ACTIVE_CODE'], $curr_TimeReal);

							     $arEventDetail["COUNT"] = $arEventDetail["COUNT"]*$arPortfElement['UF_INLOT_CNT'];
								  $arEventDetail["SUMM"] = $arEventDetail["COUNT"]*$arEventDetail['Дивиденды'];
								  $arEventDetail["SUM_ONE"] = $arEventDetail['Дивиденды'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['ITEMS'] = $arEventDetail;
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['COUNT'] = $arEventDetail["COUNT"];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_ACTIVE_URL'] = $arPortfElement["UF_ACTIVE_URL"];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_ACTIVE_NAME'] = $arEventDetail["NAME"];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_EMITENT_URL'] = $arPortfElement['UF_EMITENT_URL'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']['UF_EMITENT_NAME'] = $arPortfElement['UF_EMITENT_NAME'];
		                    $arEventsTmp[$eventDate][$arEventDetail['ITEM']]['INFO']["SUMM"] += $arEventDetail["SUMM"];
								  }
								  $arPortfElement = array();
							 break;
		            }
					 }
				}

			}
			$arResult = $arEventsTmp;
		}
    
/*      global $USER;
      $rsUser = CUser::GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();
      if($arUser["LOGIN"]=="freud"){
          $firephp = FirePHP::getInstance(true);
          $firephp->fb($request,FirePHP::LOG);
          $firephp->fb($arResult,FirePHP::LOG);
      }*/


	 return $arResult;
  }


/**
 * Возвращает данные для блоков вкладки "Анализ"
 *
 * @param  int, string   $pid id портфеля
 *
 * @return array  Массив с результатами
 *
 * @access public
 */
public function getAnalysePortfolio($pid){
	$arResult = array('B1'=>array(), 'B2'=>array(), 'B3'=>array(), 'B4'=>array());

	$rsElement = CIBlockElement::GetByID(intval($pid));
   		if($rsElement && ($obElement = $rsElement->GetNextElement())){
   			$arElement = $obElement->GetFields();
   }
	$arResult["NAME"] = $arElement["NAME"];
	$portfolioActives = $this->getPortfolioActivesList(str_replace("portfolio_", "", $pid), false, true, array());
	$arStatSumm = $this->getPortfolioStatSumm($pid);
	$free_money = 100;
	$not_money_actives_count = 0; //Не денежные активы (т.е. бумаги, а не кеш)
	$actions_summ_curr = 0; //Сумма по акциям РФ
	$actions_usa_summ_curr = 0; //Сумма по акциям США
	$actions_etf_summ_curr = 0; //Сумма по етф
	$actions_etf_valute_summ_curr = 0; //Сумма по етф в валюте
	$oblig_summ_curr = 0; //Сумма по облигам
	$oblig_valute_summ_curr = 0; //Сумма по еврооблигам
	$currency_summ = 0; //Сумма кеша
	$valute_part_summ = 0; //Сумма валютных активов
	$drawdown_summ = 0;//Сумма просада
	$rolling_profit = 0;//Итоговая прибыль
	$bond_profit = 0;//Итоговая прибыль по облигам
	$prognose_income_bond = 0;
	$arSectors = array();//Секторы
	$arSectorsDiv3 = 0;//Сумма первых трех максимальных секторов
	$arSectorsDiv2 = 0;//Сумма первых двух максимальных секторов
	$arSectorsDiv1 = 0;//Значение первого максимального сектора
	$arSectorsCSumm = 0;//Сумма бумаг из всех секторов
	$arSectorsPriceSumm = 0;//Сумма стоимости бумаг из всех секторов

	$arCnt_actions = array(); //Кол-во акций РФ
	$arCnt_actions_usa = array(); //Кол-во акций США
	$arCnt_bonds = array(); //Кол-во облиг
	$arCnt_bond_euro = array(); //Кол-во еврооблиг
	$total_prognose_income = 0; //Потенциал портфеля
	$currency_current_date  = $this->getMonthEndWorkday(date('d.m.Y')); //Получаем ближайший рабочий день и
	$arSectorsColors = $this->arSectorsColors;
	$arBlock2ActivesAnalyse = array();
$arDebug = array();
	$activeType = '';

 	//$firephp = FirePHP::getInstance(true);
   //$firephp->fb(array("portfolioActives"=>$portfolioActives),FirePHP::LOG);

	foreach ($portfolioActives as $aid => $aval){ //Цикл по составу портфеля на текущую дату

	  $activeType = $aval['UF_ACTIVE_TYPE'];
	  $radar_data = $aval['RADAR_DATA'];
 /*	  echo "<pre  style='color:black; font-size:11px;'>";
	     print_r($radar_data);
	     echo "</pre>";*/
	  $activeCurrencyId = $aval['UF_CURRENCY'];

	  $oblig_off = false;
	  if(!empty($radar_data['PROPS']) && intval((new DateTime($radar_data['PROPS']['MATDATE']))->format('Y'))>0)
	  		$oblig_off=(new DateTime($radar_data['PROPS']['MATDATE']))<=(new DateTime()) || $radar_data['PROPS']['HIDEN']=='Да'?true:false;

	//Вычисляем даты продажи активов
	 if($activeType == 'bond'){
		$date_sell = new dateTime(!empty($radar_data['PROPS']['OFFERDATE'])?$radar_data['PROPS']['OFFERDATE']:$radar_data['PROPS']['MATDATE']);
	 } else {
 		$date_sell = (new DateTime($aval['UF_ADD_DATE']))->modify('+2 years')->modify('first day of this month');
	   if(!empty($aval['UF_GRAPH_DATE'])){
		    $date_sell = (new DateTime($aval['UF_GRAPH_DATE']))->modify('+2 years')->modify('first day of this month');
		}
	 }


	 //Таргет для текущего актива
	 $obl_profit = !empty($radar_data['PROPS']['OFFERDATE'])?$radar_data['DYNAM']['Доходность к офферте']:$radar_data['DYNAM']['Доходность годовая'];
	 $obl_profit = $obl_profit;//*$currency_current_rate;
	 $shares_target = floatval($radar_data['DYNAM']['Таргет'])>0?$radar_data['DYNAM']['Таргет']:0;
	 if ($activeType == 'share_etf'){
	 	$shares_target = floatval($radar_data['PROPS']['ETF_AVG_ICNREASE'])>0 ?$radar_data['PROPS']['ETF_AVG_ICNREASE']:$shares_target;
	 }
	  $target_profit = $activeType == 'bond' ? $obl_profit : $shares_target;


	  $notRouble = !in_array(strtoupper($activeCurrencyId), $this->arRoubleCodes);
	  //Формируем курс валюты актива
	  if (in_array(strtoupper($activeCurrencyId), $this->arRoubleCodes)) {
	    $currency_value = 1;
	  } else {
	 	$currency_current_rate = getCurrency_rate(strtoupper($activeCurrencyId=='SUR'?'RUB':$activeCurrencyId),$currency_current_date);//Получаем курс на ближайший рабочий или сегодняшний день
		if($currency_current_rate["CURS"]){
		  $currency_value = $currency_current_rate["CURS"];
		} else {
		  $currency_value = 1;
		}
	  }

 	  $price_real = 0;
		if($activeType == 'bond'){//Считаем текущую цену облигации принудительно
			$price_real = ($radar_data['PROPS']['FACEVALUE']/100)*(floatval($radar_data['PROPS']['LASTPRICE'])>0?$radar_data['PROPS']['LASTPRICE']:$radar_data['PROPS']['LEGALCLOSE']);
			if(floatval($radar_data['PROPS']['ACCRUEDINT'])>0) {  //НКД
				$price_real = $price_real+floatval($radar_data['PROPS']['ACCRUEDINT']);
			}
			$currency_value = 1;
            $currency_current_rate = array();//обнуляем переменную с курсом
			if((!empty($aval['UF_ACTIVE_CURRENCY']) && !in_array(strtoupper($aval['UF_ACTIVE_CURRENCY']), $this->arRoubleCodes)) || $notRouble){
			//if(!in_array(strtoupper($aval['UF_ACTIVE_CURRENCY']), $this->arRoubleCodes) || $notRouble){
				$currency_current_rate = getCurrency_rate(strtoupper($aval['UF_ACTIVE_CURRENCY']),$currency_current_date);//Получаем курс на ближайший рабочий или сегодняшний день
			}
	 		if($currency_current_rate["CURS"]){
			  $currency_value = $currency_current_rate["CURS"];
			} else {
			  $currency_value = 1;
			}
            $price_real_tmp = $price_real;
           
		 $price_real = $price_real * $currency_value;
/*            $firephp = FirePHP::getInstance(true);
            $firephp->fb(array("id"=>$aval['UF_ACTIVE_ID'],
                               "active_currency"=>strtoupper($aval['UF_ACTIVE_CURRENCY']),
                               "uf_currency"=>strtoupper($aval['UF_CURRENCY']),
                               "usl1"=>(!empty($aval['UF_ACTIVE_CURRENCY']) && !in_array(strtoupper($aval['UF_ACTIVE_CURRENCY']), $this->arRoubleCodes)) || $notRouble,
                               "not_rouble"=>$notRouble,
                               "cur_rate"=>$currency_current_rate,
                               "currency_value"=>$currency_value,
                               "price_real"=>$price_real,
                               "price_real_tmp"=>$price_real_tmp
        ),FirePHP::LOG);*/
		}

	  $current_price = $activeType == 'bond' ? $price_real : (floatval($radar_data['DYNAM']['Цена лота'])>0?floatval($radar_data['DYNAM']['Цена лота']):0);

	 //Формируем текущую цену актива
    if($activeType != 'bond' && $activeType != 'currency') {
      $curr_price_final = $current_price * $currency_value * $aval['UF_LOTCNT'];
    }
    else if($activeType == 'currency') {
      $curr_price_final = $currency_value * $aval['UF_LOTCNT'];
    } else {
		$curr_price_final = $current_price * $aval['UF_LOTCNT'];
    }

	  //Ожидаемый доход по активу


	  $prognose_income = round(floatval(($curr_price_final/100)*$target_profit), 4);
/*	    echo "<pre  style='color:black; font-size:11px;'>";
     print_r(array("cpf"=>$curr_price_final, "trg"=>$target_profit, "progn_income"=>$prognose_income));
     echo "</pre>";*/
	  if(($activeType=='bond' && !$oblig_off) || $activeType!='bond'){
	  $total_prognose_income  += $prognose_income;
	  }



      if($activeType=='share') {
        $not_money_actives_count ++;
        $actions_summ_curr += $curr_price_final;
		  $arCnt_actions[$aval["UF_ACTIVE_ID"]] = $aval["UF_ACTIVE_ID"];
        $drawdown_summ += (($curr_price_final/100)*floatval($radar_data['DYNAM']['Просад'])); //Просад
        //issuecapitalization += parseFloat(tr.attr('data-issuecapitalization')); //Итоговая капа
        //rolling_profit += parseFloat(tr.attr('data-rolling_profit')); //Итоговая прибыль
      } else if($activeType=='share_usa') {
        $not_money_actives_count ++;
        $actions_usa_summ_curr += $curr_price_final;
        $valute_part_summ += $curr_price_final;
		  $arCnt_actions_usa[$aval["UF_ACTIVE_ID"]] = $aval["UF_ACTIVE_ID"];
        $drawdown_summ += (($curr_price_final/100)*floatval($radar_data['DYNAM']['Просад'])); //Просад
        //issuecapitalization += parseFloat(tr.attr('data-issuecapitalization')); //Итоговая капа
        //rolling_profit += parseFloat(tr.attr('data-rolling_profit')); //Итоговая прибыль
      } else if($activeType=='currency') {
        $currency_summ += $curr_price_final;
		  if($notRouble){
			  $valute_part_summ += $curr_price_final;
		  }
        //drawdown_summ += parseFloat(tr.attr('data-drawdown_summ')); //Просад
        //issuecapitalization += parseFloat(tr.attr('data-issuecapitalization')); //Итоговая капа
        //rolling_profit += parseFloat(tr.attr('data-rolling_profit')); //Итоговая прибыль
      }
      else if($activeType=='share_etf') {
        $not_money_actives_count ++;
        $actions_etf_summ_curr += $curr_price_final;
		  if((!empty($aval['UF_ACTIVE_CURRENCY']) && !in_array(strtoupper($aval['UF_ACTIVE_CURRENCY']), $this->arRoubleCodes) ) || $notRouble){
			  $valute_part_summ += $curr_price_final;
			  $actions_etf_valute_summ_curr += $curr_price_final;
		  }
        $drawdown_summ += (($curr_price_final/100)*floatval($radar_data['DYNAM']['Просад'])); //Просад
        //issuecapitalization += parseFloat(tr.attr('data-issuecapitalization')); //Итоговая капа

      }
      else if($activeType=='bond' && !$oblig_off) {
        $not_money_actives_count ++;

		  if((!empty($aval['UF_ACTIVE_CURRENCY']) && !in_array(strtoupper($aval['UF_ACTIVE_CURRENCY']), $this->arRoubleCodes)) || $notRouble){
			  $valute_part_summ += $curr_price_final;
			  $oblig_valute_summ_curr += $curr_price_final;
			  $arCnt_bond_euro[$aval["UF_ACTIVE_ID"]] = $aval["UF_ACTIVE_ID"];
		  } else {
           $oblig_summ_curr += $curr_price_final;
              $arDebug[$aval["UF_ACTIVE_ID"]] = array("oblig_summ_curr"=>$oblig_summ_curr, "curr_price_final"=>$curr_price_final);
			  $arCnt_bonds[$aval["UF_ACTIVE_ID"]] = $aval["UF_ACTIVE_ID"];
		  }

		  $bond_profit += ($curr_price_final/100*$obl_profit);
        $prognose_income_bond += $prognose_income;
	//	console.log('rolling_profit '+tr.attr('data-id'), rolling_profit);
      }

		  //Блок 2 - оценка активов
		  if(!in_array($aval["UF_ACTIVE_ID"] ,$arBlock2ActivesAnalyse[$activeType]) && $activeType!='currency'){
			 // $arBlock2ActivesAnalyse[$activeType][$aval["UF_ACTIVE_ID"]]["ITEM"] = $aval;
			  if($activeType=='share_usa') {
				 $activeTypeBlock2 = 'share';
			  } else {
				 $activeTypeBlock2 = $activeType;
			  }
			  $arBlock2ActivesAnalyse[$activeTypeBlock2][$aval["UF_ACTIVE_ID"]] = $this->analyseActiveItem($aval);
		  }

	//Блок 4. диверсификация по отраслям
		 if($activeType!='currency')
		 if($activeType!='share_usa'){
		 	if(!empty($radar_data['PROPS']['PROP_SEKTOR'])){
		 		$arSectorsCSumm++;
		 		$arSectorsPriceSumm +=$curr_price_final;
		  		$arSectors[$radar_data['PROPS']['PROP_SEKTOR']." РФ"]["COUNT"] += 1;
		  		$arSectors[$radar_data['PROPS']['PROP_SEKTOR']." РФ"]["SUMM"] += $curr_price_final;
				}
		 } else {
		 	if(!empty($radar_data['PROPS']['SECTOR'])){

		 		$arSectorsCSumm++;
				$arSectorsPriceSumm +=$curr_price_final;
				$arSectors[$radar_data['PROPS']['SECTOR']." США"]["COUNT"] += 1;
				$arSectors[$radar_data['PROPS']['SECTOR']." США"]["SUMM"] += $curr_price_final;
				}
		 }





	} //Цикл по активам портфеля на текущую дату

		 foreach($arSectors as $k=>$v){
		  //$arSectors[$k]["PRC"] = ($v["COUNT"]/$arSectorsCSumm) * 100;
		  $arSectors[$k]["PRC"] = ($v["SUMM"]/$arSectorsPriceSumm) * 100;
		 }
		 arsort($arSectors);


  //Считаем доли в портфеле
  $shares_part = round(($actions_summ_curr/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $shares_usa_part = round(($actions_usa_summ_curr/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $shares_etf_part = round(($actions_etf_summ_curr/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $shares_etf_valute_part = round(($actions_etf_valute_summ_curr/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $bonds_part = round(($oblig_summ_curr/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $bonds_euro_part = round(($oblig_valute_summ_curr/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $bonds_all_part = round((($oblig_valute_summ_curr+$oblig_summ_curr)/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $currency_part = round(($currency_summ/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $valute_part = round(($valute_part_summ/$arStatSumm['CURRENT_SUMM'])*100, 2);

  //Потенциал портфеля
  $drawdown_summ *=-1;
  $portfolio_expected_return = round(($total_prognose_income/$arStatSumm['CURRENT_SUMM'])*100, 2);
  $portfolio_common_squand = round((($drawdown_summ+$bond_profit)/ $arStatSumm['CURRENT_SUMM'])*100, 2);

  //Диверсификация по отраслям/секторам
   $sectCnt = 1;
   foreach($arSectors as $value){
	  if($sectCnt<2){
		 $arSectorsDiv1 = $value["PRC"];
	  }else
	  if($sectCnt<3){
		 $arSectorsDiv2 += $value["PRC"];
	  }else
	  if($sectCnt<4){
		 $arSectorsDiv3 += $value["PRC"];
	  }else
	  if($sectCnt==4){
		 break;
	  }
	 $sectCnt++;
   }

/*  $firephp = FirePHP::getInstance(true);
  $firephp->fb($arDebug,FirePHP::LOG);*/
  
  
  //Формируем результат
  $arResult['B1']['invest_summ'] = $arStatSumm['INVEST_SUMM'];
  $arResult['B1']['current_summ'] = $arStatSumm['CURRENT_SUMM'];
  $arResult['B1']['shares_part'] = $shares_part;
  $arResult['B1']['shares_usa_part'] = $shares_usa_part;
  $arResult['B1']['shares_etf_part'] = $shares_etf_part;
  $arResult['B1']['shares_etf_valute_part'] = $shares_etf_valute_part;
  $arResult['B1']['bonds_part'] = $bonds_part;
  $arResult['B1']['bonds_euro_part'] = $bonds_euro_part;
  $arResult['B1']['bonds_all_part'] = $bonds_all_part;
  $arResult['B1']['currency_part'] = $currency_part;
  $arResult['B1']['valute_part'] = $valute_part;
  $arResult['B1']['shares_cnt'] = count($arCnt_actions);
  $arResult['B1']['shares_usa_cnt'] = count($arCnt_actions_usa);
  $arResult['B1']['bonds_cnt'] = count($arCnt_bonds);
  $arResult['B1']['bonds_euro_cnt'] = count($arCnt_bond_euro);
  $arResult['B1']['bond_profit'] = $bond_profit;
  $arResult['B1']['portfolio_expected_return'] = $portfolio_expected_return;
  $arResult['B1']['drawdown_summ'] = $drawdown_summ;
  $arResult['B1']['portfolio_shares_squand'] = round($drawdown_summ/($actions_summ_curr+$actions_usa_summ_curr+$actions_etf_summ_curr)*100, 2);
  $arResult['B1']['portfolio_common_squand'] = $portfolio_common_squand;
  $arResult['B1']['arSectors'] = $arSectors;
  $arResult['B1']['sector_divers_1'] = $arSectorsDiv1;
  $arResult['B1']['sector_divers_2'] = $arSectorsDiv2;
  $arResult['B1']['sector_divers_3'] = $arSectorsDiv3;
  $arResult['B1']['arSectorsCSumm'] = $arSectorsCSumm;
  $arResult['B1']['arSectorsPriceSumm'] = $arSectorsPriceSumm;
  $arResult['B2']['arActivesAnalyse'] = $arBlock2ActivesAnalyse;



  $arResult['sectors_colors'] = $arSectorsColors;
  $arResult['stat_summ'] = $arStatSumm;


	return $arResult;
}


/**
 * Анализирует актив портфеля и возвращает краткий массив с описанием проблем и классом цевета предупреждения
 *
 * @param  [add type]   $aval (Optional)
 *
 * @return [add type]  [add description]
 *
 * @access public
 */
public function analyseActiveItem($aval=array()){
	$message = '';
	$warrningClass = ''; //Класс цвета буллета возле актива в списке
	$arWarnings = array(0=>'warn_green', 1=>'warn_orange', 2=>'warn_red');
	$arErrors = array();
/*	echo "<pre  style='color:black; font-size:11px;'>";
      print_r($aval);
      echo "</pre>";*/
	$activeType = $aval['UF_ACTIVE_TYPE'];
	$radar_data = $aval['RADAR_DATA'];
	$activeCurrencyId = $aval['UF_CURRENCY'];

	$oblig_off = false;
	if(!empty($radar_data['PROPS']) && intval((new DateTime($radar_data['PROPS']['MATDATE']))->format('Y'))>0)
			$oblig_off=(new DateTime($radar_data['PROPS']['MATDATE']))<=(new DateTime()) || $radar_data['PROPS']['HIDEN']=='Да'?true:false;

	if($activeType=='share' || $activeType=='share_usa') {
	  //PEG
	  if(floatval($radar_data['DYNAM']['PEG'])>1.5){
		$arErrors[] = 'Акция переоценена: PEG = '.$radar_data['DYNAM']['PEG'].' (норма =1)';
		$warrningClass = $arWarnings[1];//orange
	  }
	  //BETA
	  if(floatval($radar_data['PROPS']['BETTA'])>1.1){
		$arErrors[] = 'Высоко-волатильная акция: бета = '.round($radar_data['PROPS']['BETTA'], 3);
	  }
	  //Таргет
	  if(!isset($radar_data['DYNAM']['Таргет']) || floatval($radar_data['DYNAM']['Таргет'])<0){
		$arErrors[] = 'Расчетный потенциал акции ниже 0';
		$warrningClass = $arWarnings[1];//orange
	  }

	  //Доля собственного капитала в активах
	  $isBankSector = $radar_data['PROPS']['PROP_SEKTOR']=="Банковский сектор"; //Определяем банковский сектор

	  if(isset($radar_data['LAST_PERIOD']['Доля собственного капитала в активах']) && !empty($radar_data['LAST_PERIOD']['Доля собственного капитала в активах'])){
		 $curr_value = floatval($radar_data['LAST_PERIOD']['Доля собственного капитала в активах']);
		 if($isBankSector){ //Для банков свои условия
			 if($curr_value<10 && $curr_value>=8){
			 	$arErrors[] = 'Доля СК немного не соответствует нормативу в 10%';
				//$warrningClass = $arWarnings[1];//orange
			 } elseif($curr_value<8){
		  		$arErrors[] = 'Низкая финансовая устойчивость бизнеса (доля СК = '.$curr_value.'%)';
				$warrningClass = $arWarnings[2];//red
			 }
		 } else {  //Для остальных секторов проверяем по всем градациям
			 if($activeType=='share'){
				 if($curr_value<50 && $curr_value>=40){
				 	$arErrors[] = 'Доля СК немного не соответствует нормативу в 50%';
					//$warrningClass = $arWarnings[1];//orange
				 } elseif($curr_value<40){
			  		$arErrors[] = 'Низкая финансовая устойчивость бизнеса (доля СК = '.$curr_value.'%)';
					$warrningClass = $arWarnings[2];//red
				 }
			 } elseif($activeType=='share_usa'){
				 if($curr_value<30 && $curr_value>=27){
				 	$arErrors[] = 'Доля СК немного не соответствует нормативу в 30%';
					//$warrningClass = $arWarnings[1];//orange
				 } elseif($curr_value<27){
			  		$arErrors[] = 'Низкая финансовая устойчивость бизнеса (доля СК = '.$curr_value.'%)';
					$warrningClass = $arWarnings[2];//red
				 }
			 }
		 }

	  }
	  //Скользящая прибыль последняя
	  if(isset($radar_data['LAST_PERIOD']['Прибыль за год (скользящая)']) && !empty($radar_data['LAST_PERIOD']['Прибыль за год (скользящая)'])){
		 $curr_value = floatval($radar_data['LAST_PERIOD']['Прибыль за год (скользящая)']);
		 if($curr_value<=0){
		 	  $kvartal = $radar_data['LAST_PERIOD']['PERIOD_VAL'];
		 	  $year = $radar_data['LAST_PERIOD']['PERIOD_YEAR'];
			if(intval($radar_data['LAST_PERIOD']['CURRENT_PERIOD'])==1){  //Для текущего периода квартал оказывается пустой по акциям США, по этому получаем текущий квартал принудительно
			  $now = new DateTime(date());
			  $kvartal = $this->arMonthToKvartal[intval($now->format('m'))];
			  $year = $now->format('Y');
			}

	  		$arErrors[] = 'Убыточный бизнес: скользящая прибыль '.$kvartal.'кв. '.$year.'г. = '.$curr_value;
			$warrningClass = $arWarnings[1];//orange
			}
	  }
	  //Темпы роста выручки
	  if(isset($radar_data['LAST_PERIOD']['Темп прироста выручки']) && !empty($radar_data['LAST_PERIOD']['Темп прироста выручки'])){
		 $curr_value = floatval($radar_data['LAST_PERIOD']['Темп прироста выручки']);
		 if($curr_value<=0){
	  		$arErrors[] = 'Выручка падает: отрицательные темпы роста выручки ('.$curr_value.'%)';
			$warrningClass = $arWarnings[1];//orange
			}
	  }
	  //Затяжной нисходящий тренд
	  if(floatval($radar_data["DYNAM"]["MONTH_INCREASE"])<0 && floatval($radar_data["DYNAM"]["YEAR_INCREASE"])<0 && floatval($radar_data["DYNAM"]["THREE_YEAR_INCREASE"])<0){
	  		$arErrors[] = '<span class="red">Затяжной нисходящий тренд по акции</span>';
			$warrningClass = $arWarnings[1];//orange
	  }

	} else if($activeType=='share_etf'){
	  //Комиссия
	  if(floatval($radar_data['PROPS']['ETF_COMISSION'])>1){
		$arErrors[] = 'Комиссия более 1%, скорее всего можно найти альтернативный фонд с более низкой комиссией, воспользуйтесь <a href="https://fin-plan.org/lk/obligations/#etf_tab_lnk" target="_blank">фильтром по ETF</a> в Radar.';
	  }
	  //Двойная репликация
	  if(strpos($radar_data['PROPS']['ETF_REPLICATION'], "Двойная")!==false){
		$arErrors[] = 'У данного фонда двойная репликация, что означает двойную комиссию';
		$warrningClass = $arWarnings[1];//orange
	  }
	  //Затяжной нисходящий тренд
	  if(floatval($radar_data["DYNAM"]["MONTH_INCREASE"])<0 && floatval($radar_data["DYNAM"]["YEAR_INCREASE"])<0 && floatval($radar_data["DYNAM"]["THREE_YEAR_INCREASE"])<0){
	  		$arErrors[] = '<span class="red">Затяжной нисходящий тренд по ETF</span>';
			$warrningClass = $arWarnings[1];//orange
	  }

	} else if($activeType=='bond' && !$oblig_off){
	  //Дефолтные
	  if(!empty($radar_data['PROPS']['CSV_VID_DEFOLT'])){
		$arErrors[] = '<span class="red">'.$radar_data['PROPS']['CSV_VID_DEFOLT'].'</span>';
		$warrningClass = $arWarnings[2];//red
	  }
	  //Структурные
	  if(!empty($radar_data['PROPS']['ADDITIONAL_INCOME'])){
		$arErrors[] = '<span>Облигация является структурной. Подробнее о таких облигациях можно узнать в статье<br>
		"<a target="_blank" href="https://fin-plan.org/blog/investitsii/strukturnye-obligatsii/">Структурные облигации</a>". Параметры структурной облигации:'.$radar_data['PROPS']['ADDITIONAL_INCOME'].'</span>';
		$warrningClass = $arWarnings[1];//orange
	  }
	  //Субординированные
	  if(!empty($radar_data['PROPS']['CSV_PAYMENT_ORDER'])){
		if($radar_data["PROPS"]['CSV_PAYMENT_ORDER'][0]=='Субординированная')
			$arErrors[] = $radar_data["PROPS"]['CSV_PAYMENT_ORDER'];
			$warrningClass = $arWarnings[2];//red
	  }
	  //ОФерта
	  if(!empty($radar_data['PROPS']['OFFERDATE'])){
		$arErrors[] = 'Дата оферты: '.$radar_data["PROPS"]['OFFERDATE'];
		$warrningClass = $arWarnings[1];//orange
	  }
	  //Доля собственного капитала в активах
	  if(isset($radar_data['LAST_PERIOD']['Доля собственного капитала в активах']) && !empty($radar_data['LAST_PERIOD']['Доля собственного капитала в активах'])){
		 $curr_value = floatval($radar_data['LAST_PERIOD']['Доля собственного капитала в активах']);
		 if($curr_value<50 && $curr_value>=40){
		 	$arErrors[] = 'Доля СК немного не соответствует нормативу в 50%';
			$warrningClass = $arWarnings[1];//orange
		 } elseif($curr_value<40){
	  		$arErrors[] = 'Низкая финансовая устойчивость бизнеса (доля СК = '.$curr_value.'%)';
			$warrningClass = $arWarnings[2];//red
		 }
	  }
	  //Скользящая прибыль последняя
	  if(isset($radar_data['LAST_PERIOD']['Прибыль за год (скользящая)']) && !empty($radar_data['LAST_PERIOD']['Прибыль за год (скользящая)'])){
		 $curr_value = floatval($radar_data['LAST_PERIOD']['Прибыль за год (скользящая)']);
		 if($curr_value<=0){
		 	  $kvartal = $radar_data['LAST_PERIOD']['PERIOD_VAL'];
		 	  $year = $radar_data['LAST_PERIOD']['PERIOD_YEAR'];
			if(intval($radar_data['LAST_PERIOD']['CURRENT_PERIOD'])==1){  //Для текущего периода квартал оказывается пустой по акциям США, по этому получаем текущий квартал принудительно
			  $now = new DateTime();
			  $kvartal = $this->arMonthToKvartal[intval($now->format('m'))];
			  $year = $now->format('Y');
			}

	  		$arErrors[] = 'Убыточный бизнес: скользящая прибыль '.$kvartal.'кв. '.$year.'г. = '.$curr_value;
			$warrningClass = $arWarnings[1];//orange
			}
	  }
	  //Темпы роста выручки
	  if(isset($radar_data['LAST_PERIOD']['Темп прироста выручки']) && !empty($radar_data['LAST_PERIOD']['Темп прироста выручки'])){
		 $curr_value = floatval($radar_data['LAST_PERIOD']['Темп прироста выручки']);
		 if($curr_value<=0){
	  		$arErrors[] = 'Выручка падает: отрицательные темпы роста выручки ('.$curr_value.'%)';
			$warrningClass = $arWarnings[1];//orange
			}

	  }


	}

	if(count($arErrors)<=0){
		$message = 'Избыточных рисков не выявлено.';
		$warrningClass = '';
	} else {
		$message = implode(". ", $arErrors);
	}

	$arReturn = array("ACTIVE_NAME"=>$aval["UF_ACTIVE_NAME"], "WARNING_CLASS"=>$warrningClass, "ACTIVE_URL"=>$aval["UF_ACTIVE_URL"], "ACTIVE_MESSAGE"=>$message);

	return $arReturn;
}

static function getAnalyseLibStatic(){
	return self::getAnalyseLib();
}

public function getAnalyseLib(){
	Global $DB;
	$arResult = array();
	$query = "SELECT * FROM `$this->analyseTableName` ORDER BY `UF_PARAM` ASC, `UF_SORT` ASC";
	$res = $DB->Query($query);
	while($row = $res->Fetch()){
	 $arResult[] = $row;
	}

  return $arResult;
}

}?>