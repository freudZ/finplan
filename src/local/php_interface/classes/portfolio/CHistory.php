<?
use GuzzleHttp\Psr7;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Bitrix\Main\Loader;
	 Loader::includeModule('iblock');

/**
 * Класс для работы с загрузкой портфелей из брокерских отчетов
 *
 * @author    [add AuthorName] <[add AuthorEmail]>
 * @package   [add PackageName]
 */
class CHistory{
 private $remoteService = '';
 public $ActionsIblockID = 32;
 public $ActionsUsaIblockID = 55;
 public $ObligationsIblockID = 27;
 public $CPortfolio;
 public $resA;
 public $resE;
 public $resAU;
 public $resO;
 public $resCUR;
 public $hl_broker_load_stat_Id = 48;
 function __construct($loadClasses = false){
	//$this->remoteService = 'http://149.154.65.243:8005/'; //stage
  	//$this->remoteService = 'http://185.154.53.106:8000/'; //prod
	$this->remoteService = \Bitrix\Main\Config\Option::get("grain.customsettings","BROKER_REPORT_SERVER");

	$this->CPortfolio = new CPortfolio($loadClasses);
	if($loadClasses){
	  	$this->resA = $this->CPortfolio->resA;
	   $this->resE = $this->CPortfolio->resE;
		$this->resAU = $this->CPortfolio->resAU;
		$this->resO = $this->CPortfolio->resO;
		$this->resCUR = $this->CPortfolio->resCUR;
	}
 }

 public function saveStat($brokerType='', $filename='',  $filesize = 0, $time = 0){
 	Global $USER;
		 CModule::IncludeModule("highload");
    CModule::IncludeModule('highloadblock');
 				$hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hl_broker_load_stat_Id)->fetch();
				$entity            = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();
				$arStat = array(
						"UF_DATE" => (new DateTime())->format('d.m.Y H:i:s'),
						"UF_FILENAME" => $filename,
						"UF_BROKER_TYPE" => $brokerType,
						"UF_USER_ID" => $USER->GetID(),
						"UF_SERVER" => $this->remoteService,
						"UF_FILESIZE" => round($filesize/1024, 2),
						"UF_LOAD_TIME" => $time,
				);
				$otvet  = $entity_data_class::add($arStat);
 }
 /** TODO Удалить после тестирования
  * Производит запрос к сервису парсера
  *
  * @param  string   $method (Optional)  ['POST', 'GET'] режим запроса
  * @param  string   $command команда, вид запроса
  * @param  array   $arParams (Optional) дополнительные параметры запроса в массиве
  *
  * @return array  масссив с результатами парсинга
  *
  * @access private
  */
 private function connectService_Guzzle($method="POST", $command, $arParams=array()){
  global $USER;
        $rsUser = CUser::GetByID($USER->GetID());
        $arUser = $rsUser->Fetch();
/*        if($arUser["LOGIN"]=="freud"){
        $result = $this->connectServiceCurl($method, $command, $arParams);
		  return $result;
        }*/

   $url         = $this->remoteService;
	$client      = new \GuzzleHttp\Client();
	$url .= $command;
	$syncLogFile = $_SERVER["DOCUMENT_ROOT"] . "/log/error_HistoryService_log.txt";

	$options = array();
	if(!empty($arParams) && $command=='parse_report'){ //Для отправки файла отчета на парсинг проверяем и заполняем необходимые параметры
	$options =  array(
				'multipart' => [
			  [
			    'name'     => 'broker',
			    'contents' => $arParams["reportType"]
			  ],
			  [
			    'name'     => 'file',
			    'contents' => fopen($arParams["filePath"], 'r')
			  ]
 		]);
 	}

	try {
		$options['http_errors'] = true;
		$res     = $client->request($method, $url, $options);
		$strJson = array();
		$strJson = json_decode($res->getBody()->getContents(), true);
	} catch (\GuzzleHttp\Exception\RequestException $e) {

	  //	$logRequest = 'err logRequest time: ' . date('Y-m-d H:i:s') . '; func: connectService, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getRequest());
	  //	CLogger::error_historyParse($logRequest);
		//file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);

		if ($e->hasResponse()) {
			$response = $e->getResponse();
			$strJson = array("result"=>"error", "status"=>$response->getStatusCode(), "message"=>$response->getReasonPhrase());
		   CLogger::error_connect_historyParse(print_r($strJson, true));

		  //	$logResponse = 'err logResponse time: ' . date('Y-m-d H:i:s') . '; func: connectService, request; url: ' . $dataUrl . ' exception: ' . Psr7\str($e->getResponse());
		  //	CLogger::error_historyParse($logResponse);
			//file_put_contents($syncLogFile, $log . PHP_EOL, FILE_APPEND);
		} else {
			$strJson = array("result"=>"error", "message"=>"Неизвестная ошибка сервера");
		}


	}
	return $strJson;
 }

 /**
  * Производит запрос к сервису парсера
  *
  * @param  string   $method (Optional)  ['POST', 'GET'] режим запроса
  * @param  string   $command команда, вид запроса
  * @param  array   $arParams (Optional) дополнительные параметры запроса в массиве
  *
  * @return array  масссив с результатами парсинга
  *
  * @access private
  */
  public function connectService($method="POST", $command, $arParams=array()){
  	$url  = $this->remoteService;
	$url .= $command;
	$curl = curl_init();

	$arCurlParams = array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => $method,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	);

	if($command=='parse_report'){
	  $arCurlParams[CURLOPT_POSTFIELDS] = array('file_name'=> new CURLFILE($arParams["filePath"]),'broker' => $arParams["reportType"]);
	}


	try {
		curl_setopt_array($curl, $arCurlParams);
		$strJson = json_decode(curl_exec($curl), true);
	}
	catch (Exception $e) {
		if ($err = curl_error($curl)) {
			$response = $e->getResponse();
			$strJson = array("result"=>"error", "status"=>"curl error", "message"=>$err);
		   CLogger::error_connect_historyParse(print_r($strJson, true));
		} else {
			$strJson = array("result"=>"error", "message"=>"Неизвестная ошибка сервера");
		}
	} finally {
	  curl_close($curl);
	}
	return $strJson;
  }

 /**
  * Возвращает список доступных для загрузки типов брокеров
  *
  * @return array  массив типов брокеров
  *
  * @access public
  */
 public function getBrokersTypes(){
  $arResult = array();
	$arResult = $this->connectService('GET', 'brokers');
  return $arResult["brokers"];
 }


 /**
  * Загружает файл отчета и возвращает распарсенные результаты в виде массива
  *
  * @param  [add type]   $reportType [add description]
  * @param  [add type]   $filePath [add description]
  * @param  [add type]   $fname [add description]
  *
  * @return [add type]  [add description]
  *
  * @access public
  */
 public function uploadAndParse($reportType, $filePath, $fname){
	$arResult = array();
	 $filesize = 0;
	if(file_exists($filePath)){
		$filesize = filesize($filePath);
		$filesize = $filesize/1024;
	}

	//CLogger::ReportFilesize(print_r(array("path"=>$filePath, "size kb"=>$filesize), true));

/*	if($filesize>1024){
	 $arReport['result']='error';
	 $arReport['message']='Размер файла превышает 750кб';
	 return $arResult;
	}*/

	$arParams = array("reportType"=>$reportType, "filePath"=>$filePath);
	$arResult = $this->connectService('POST', 'parse_report', $arParams);

	if(count($arResult["deals"])>0){

	 $this->sortDealsByOrderDate($arResult["deals"]);
	 $arTmpIsinTicker = array();//Временный массив для отслеживания дублей ненайденных элементов по склейке isin+ticker
	 foreach($arResult["deals"] as $k=>$arDeal){
	 	$finded = $this->findItem($arDeal);
		if($finded["item"]!=false){
		 $arResult["deals"][$k]["finded"] = $finded;
		} else {
		 $arResult["deals"][$k]["finded"] = array();
		 if(!in_array($arDeal['isin'].$arDeal['ticker'] ,$arTmpIsinTicker)){
		 	$arTmpIsinTicker[] = $arDeal['isin'].$arDeal['ticker'];
		 	$arResult["deals_nofound"][] = $arDeal;
		 }
		}
	 }

	}

  //	CLogger::UploadAndParseBRReport(print_r($arResult, true));

	return $arResult;
 }

 public function sortDealsByOrderDate(&$arDeals){
 			   $dateSort = 'asc';
 				usort($arDeals, function ($a, $b) use ($dateSort) {
				$res = 0;

				// сравниваем даты добавления
				$DateA = (new DateTime($a['order_datetime']))->getTimestamp(); $DateB = (new DateTime($b['order_datetime']))->getTimestamp();
				if ($DateA != $DateB) {
					//return ($addDateA < $addDateB) ? 1 : -1; //desc
					if ($dateSort == 'desc') {
						return ($DateA < $DateB) ? 1 : -1; //desc
					} else {
						return ($DateA > $DateB) ? 1 : -1; //asc
					}
				}

				// сравниваем ID
				$inta = intval($a["id"]); $intb = intval($b["id"]);
				if ($inta != $intb ) {
						return ($inta < $intb) ? 1 : -1; //desc
				}

				// сравниваем буквы
				/*          $var1 = preg_replace('~[0-9]+~', '', $a);
				$var2 = preg_replace('~[0-9]+~', '', $b);
				$compare = strcmp( $var1, $var2 ); // А считается меньше Б
				if( $compare !== 0 ){
				return ( $compare > 0 ) ? 1 : -1;
				}*/

				return $res;
			});
		  //	return $arDeals;
 }

 public function getHistActionsPrices($CPortfolio, $activeType, $date, $activeSecid, $activeCode){
	 return $this->CPortfolio->getHistActionsPrices($activeType, $date, $activeSecid, $activeCode);
 }

 /**
  * Создает сам портфель
  *
  * @param  string   $portfolioName Название портфеля из формы
  * @param  array   $arData Массив с распарсенными данными из брокерского отчета о датах портфеля
  *
  * @return [add type]  [add description]
  *
  * @access public
  */
 public function createPortfolio($portfolioName, $reportType, $arData, $fileData){
 	//$CPortfolio = new CPortfolio();
	Global $USER;
	$arDataPortfolio = array("date"=>$arData["start_date"], "name"=>$portfolioName, "reportType"=>$reportType);
	$newPortfolio = $this->CPortfolio->addPortfolio($arDataPortfolio);
	$this->CPortfolio->clearPortfolioListForUserCache($USER->GetID());
	$arReturn = array("result"=>"error", "message"=>"Ошибка создания портфеля");
	if($newPortfolio["result"]=="ok"){
		 $arReturn = array("result"=>"ok", "id"=>$newPortfolio["id"]);
	}  else {
		 $arReturn = array("result"=>"error", "message"=>$newPortfolio["error_text"]);
	}
	return $arReturn;
 }


  /**
   * Ищет активы в классах
   *
   * @param  array   $arDeal массив сделки
   *
   * @return array  массив с найденным элементом
   *
   * @access public
   */
  public function findItem($arDeal){
	  $item  = false;
	  $arFindedItem = array();
  if(isset($arDeal["isin"]) || isset($arDeal["ticker"]) || isset($arDeal["short_asset_name"])){

  			//Ищем облигу по isin коду
		  $itemTickerIsin = isset($arDeal["isin"])?$arDeal["isin"]:$arDeal["ticker"];
		  $item = $this->resO->getItemByIsin($itemTickerIsin);
		  if(is_array($item)){
		  	$classType = 'Obligations';
			$arFindedItem[$classType] = array("class"=>$classType, "item"=>$item);
		  } else if( strpos($itemTickerIsin, "SU")!==false ){  //если не нашли - проверяем наличие знаков SU в коде и если они там есть - то ищем еще раз но уже по SECID
			 $item = $this->resO->getItemBySecid($itemTickerIsin);
				if(is_array($item)){
		  			$classType = 'Obligations';
					$arFindedItem[$classType] = array("class"=>$classType, "item"=>$item);
		  		}
		  }

		//Если сразу не нашли в облигах - ищем в оставшихся инфоблоках
	 if(count($arFindedItem)<=0){
		 	$item = $this->resA->getItem($arDeal["isin"]);
			if(is_array($item)){
				$classType = 'Actions';
				$arFindedItem[$classType] = array("class"=>$classType, "item"=>$item);
			}

		 	$item = $this->resE->getItem($arDeal["isin"]);
			if(is_array($item)){
				$classType = 'ActionsEtf';
				$arFindedItem[$classType] = array("class"=>$classType, "item"=>$item);
		   }

		 	$item = $this->resAU->getItem($arDeal["isin"]);
			if(is_array($item)){
				$classType = 'ActionsUsa';
				$arFindedItem[$classType] = array("class"=>$classType, "item"=>$item);
		   }

		  if(count($arFindedItem)>1){ ///Если найдено больше одной акции то проверяем валюту сделки и если это не рубли то используем найденную акцию США
		  $is_currency = !in_array(strtoupper($arDeal["price_currency"]), $this->CPortfolio->arRoubleCodes)?true:false;
	     if($is_currency){
				  $item = $arFindedItem['ActionsUsa']["item"];
				  $classType = 'ActionsUsa';
			  }else{
 				  $item = $arFindedItem['Actions']["item"];
				  $classType = 'Actions';
			  }
		  } else if(count($arFindedItem)==1) { //Иначе использем единственную найденную бумагу
		  	     $arItem = reset($arFindedItem);
				  $item = $arItem["item"];
				  $classType = $arItem["class"];
		  }

		}

		 if(count($arFindedItem)<=0){

			$codeToFind = !empty($arDeal["ticker"])?$arDeal["ticker"]:$arDeal["isin"];
			if(array_key_exists('short_asset_name', $arDeal) && empty($codeToFind)){
			 $codeToFind = $arDeal["short_asset_name"];
			}
		 	$item = $this->resCUR->getItem($codeToFind);  //Ищем валюту по тикеру или по isin если тикера нет
			if(is_array($item)){
			 if(!empty($item["PROPS"]["LINKED_BASE_CURRENCY"])){
				//если есть привязка к основной валюте получаем конечный элемент валюты по id из привязки
				$itemCurrency = $this->resCUR->getItemById($item["PROPS"]["LINKED_BASE_CURRENCY"]);
			  	}
			 if($itemCurrency && !empty($item["PROPS"]["LINKED_BUY_CURRENCY"])){
				//если есть привязка к основной валюте получаем конечный элемент валюты по id из привязки
				$itemCurrency["BUY_CURRENCY"] = $this->resCUR->getItemById($item["PROPS"]["LINKED_BUY_CURRENCY"])["SECID"];
			  	}
			$item = $itemCurrency;
			$classType = 'Currencies';
		  	}
			 //unset($itemCurrency);
		 }


 		  $tmpItem = array(
			"ID"=>$item["ID"],
			"URL"=>$item["URL"],
			"ACTIVE_TYPE"=>$item["ACTIVE_TYPE"],
			"NAME"=>$item["NAME"],
			"COMPANY"=>array("NAME"=>$item["COMPANY"]["NAME"], "URL"=>$item["COMPANY"]["URL"]),
			"PRICE_ON_DATE"=>$item["PRICE_ON_DATE"],
			"PROPS"=>array(
			  "SECID"=>$item["PROPS"]["SECID"],
			  "IS_ETF_ACTIVE"=>$item["PROPS"]["IS_ETF_ACTIVE"],
			  "ETF_CURRENCY"=>($classType=='ActionsEtf'?$item["PROPS"]["ETF_CURRENCY"]:''),
			  "LOTSIZE"=>$item["PROPS"]["LOTSIZE"],
			  "ISIN"=>$item["PROPS"]["ISIN"],
			  //"SECID"=>$item["PROPS"]["SECID"],
			),
		  );

			if($classType=='Currencies'){
			 $tmpItem['PROPS']['BUY_CURRENCY'] = $item["BUY_CURRENCY"];
			}

			$item = $tmpItem;
	}
	if(intval($item["ID"])<=0){
				$item=false;
			}
	  	//	CLogger::findItem(print_r(array("classType"=>$classType, "arDeal"=>$arDeal, "item"=>$item), true));

		//  unset($item["PERIODS"]);
	 return array("item"=>$item, "classType"=>$classType);
  }

  /**
   * Заполняет активами портфель и историю 2 для мультишагового режима загрузки
   *
   * @param  [add type]   $pid [add description]
   * @param  [add type]   $arReportData [add description]
   *
   * @return [add type]  [add description]
   *
   * @access public
   */
  public function fillPortfolioFromReport2($pid, $arReportData, $arPortfolio, $reportType, $fileData){
	 $arResult = array();
	 $start = microtime(true);
	 $debug = false;
	// $debug = true;
	 //Добавим в список загруженных файлов загружаемый файл.
	   $arLoadedFiles = array();
	   $arSelect = array("ID", "NAME", "PROPERTY_LOADED_FILES");
      $res = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $this->CPortfolio->portfolio_iblockId, 'ID'=>$pid), false, array(), $arSelect);
		$k = 0;
		if($row = $res->fetch()){
		  foreach($row['PROPERTY_LOADED_FILES_VALUE'] as $k=>$v){
			$arLoadedFiles[$k] = Array("VALUE"=>$v, "DESCRIPTION"=>$row['PROPERTY_LOADED_FILES_DESCRIPTION'][$k]);
		  }
		  $k++;
		}


		$arLoadedFiles[$k] = Array("VALUE"=>$fileData, "DESCRIPTION"=>(new DateTime())->format('d.m.Y H:i'));
      CIBlockElement::SetPropertyValueCode($pid, "LOADED_FILES", $arLoadedFiles);
      CIBlockElement::SetPropertyValueCode($pid, "BROKER", $reportType);


    if(count($arReportData["cashflow"])>0 && intval($pid)>0){
     //Обрабатываем ключ cashflow в результатах парсера
         $this->fillFromCashflow($pid, $arReportData["cashflow"], $arReportData);
	 }


	 if(count($arReportData["deals"])>0 && intval($pid)>0){

	 	CModule::IncludeModule("iblock");

	  $arActivesFromIblock = array("share"=>array(), "share_etf"=>array(), "share_usa"=>array(), "bond"=>array(), "currency"=>array());
	  $arNofoundArray = array();
		if(count($arReportData["nofoundArray"])){
		  foreach($arReportData["nofoundArray"] as $arNofound){
		  	 $defindedIsin = explode(" - ", $arNofound["found"]);
			 $arNofoundArray[$arNofound["isin"]] = $this->findItem(array("isin"=>$defindedIsin[0]));
		  }
		}

		//Ищем номера сделок отчетов в указанном портфеле
		$arReportDealsId = $this->CPortfolio->getPortfolioReportIdFinished($pid, true);
		foreach($arReportData["deals"] as $arDeal){
		 if(array_key_exists("id", $arDeal)){
		 	if(count($arReportDealsId)>0 && array_key_exists($arDeal["id"], $arReportDealsId)) continue;
		 }

		 if(!array_key_exists("item", $arDeal["finded"])) continue;
		 if(count($arDeal["finded"])<=0) continue;

		 $findedItem = $arDeal["finded"];

		 $dealCurrency = $arDeal['price_currency'];
		 //Если валюту нужно адаптировать по коду к базе сайта то пользуемся адаптером в классе CPortfolio
		 if(array_key_exists(strtoupper($dealCurrency), $this->CPortfolio->arCurrencyCodesAdapter)){
		 	$dealCurrency = $this->CPortfolio->arCurrencyCodesAdapter[strtoupper($dealCurrency)];
		 }

		 if(is_array($findedItem) && is_array($findedItem["item"])){
		  $item = $findedItem["item"];
		  $classType = $arDeal["finded"]["classType"];
			try {
			  $tmpDate = isset($arDeal["order_date"])?$arDeal["order_date"]:$arDeal["payment_date"];
			  $dealDate = (new DateTime($tmpDate))->format('d.m.Y');
			 // $dealDate = (new DateTime($arDeal["payment_date"]))->format('d.m.Y');
			} catch (Exception $e) {
			    $arExceptions[] = "Дата сделки: ".$tmpDate." содержит ошибку: ".$e->getMessage();
				 continue;
			} finally {
			}
		  switch($classType){
			 case "Actions":
			 case "ActionsEtf":
			 if($item["PROPS"]["IS_ETF_ACTIVE"]=="Y"){
					$key = 'share_etf';
				} else {
					$key = 'share';
				}
			 $arActivesFromIblock[$key][$item["PROPS"]["SECID"]] = $item;
			 $item["ACTIVE_TYPE"] = $key;
			 $price = $this->CPortfolio->getHistActionsPrices('share', $dealDate, $item["PROPS"]["SECID"], '');
			 $item["PRICE_ON_DATE"] = $price["price"];
			 break;
			 case "ActionsUsa":
				$key = 'share_usa';
				//$item["PROPS"]["LOTSIZE"] = 1; //Для акций США всегда в лоте одна акция
				unset($item["PERIODS"], $item["DYNAM"]["Доля в индексах"]);
				$arActivesFromIblock[$key][$item["PROPS"]["SECID"]] = $item;
			 $item["ACTIVE_TYPE"] = $key;
			 $price = $this->CPortfolio->getHistActionsPrices('share_usa', $dealDate, $item["PROPS"]["SECID"], '');
			 $item["PRICE_ON_DATE"] = $price["price"];
			 break;
			 case "Obligations":
				$key = 'bond';
				unset($item["PERIODS"], $item["DYNAM"]["Доля в индексах"]);
				$arActivesFromIblock[$key][$item["SECID"]] = $item;
			 $item["ACTIVE_TYPE"] = $key;
			 $price = $this->CPortfolio->getHistActionsPrices('bond', $dealDate, $item["PROPS"]["SECID"], $item["PROPS"]["ISIN"], $dealCurrency);
			 $item["PRICE_ARRAY"] = $price;
			 $item["PRICE_ON_DATE"] = $price["price"];



			 break;
			 case "Currencies":
				$key = 'currency';
				$arActivesFromIblock[$key][$item["PROPS"]["SECID"]] = $item;
			 $item["ACTIVE_TYPE"] = $key;
			 $price = $this->CPortfolio->getHistActionsPrices('currency', $dealDate, $item["PROPS"]["SECID"], '');
			 $tmpPrice = $price["price"];

			 if(!in_array($item["PROPS"]["BUY_CURRENCY"], $this->CPortfolio->arRoubleCodes)){
			  $tmpPrice = $price["price_in_".strtolower($item["PROPS"]["BUY_CURRENCY"])];
			 }
			 $item["PRICE_ON_DATE"] = $price["price"];
			 break;
		  }
		   $arDeal["RADAR"] = $item;

        $idActive = $arDeal["RADAR"]["ID"];




        $arActiveArray = array(
		  'deal_id'=>$arDeal["id"],
		  'report_version'=> $arReportData["version"],
		  'finished'=>$arDeal["finished"],
		  'typeActive'=> $arDeal["RADAR"]["ACTIVE_TYPE"],
		  'idActive'=> $arDeal["RADAR"]["ID"],
		  'nameActive'=> $arDeal["RADAR"]["NAME"],
		  'priceActive'=> $arDeal["price"]*(!empty($arDeal["RADAR"]["PROPS"]["LOTSIZE"])?$arDeal["RADAR"]["PROPS"]["LOTSIZE"]:1),
        'cntActive'=> $arDeal["amount"]/(!empty($arDeal["RADAR"]["PROPS"]["LOTSIZE"])?$arDeal["RADAR"]["PROPS"]["LOTSIZE"]:1),
		  'dateActive'=> $dealDate,
		  'codeActive'=> $arDeal["RADAR"]["PROPS"]["ISIN"],
		  'urlActive'=> $arDeal["RADAR"]["URL"],
        'currencyEmitent'=> strtolower($dealCurrency),
		  'nameActiveEmitent'=> $arDeal["RADAR"]["COMPANY"]["NAME"],
		  'urlActiveEmitent'=> $arDeal["RADAR"]["COMPANY"]["URL"],
        'lastpriceActive'=> $arDeal["RADAR"]["PRICE_ON_DATE"],
		  'secid'=> $arDeal["RADAR"]["PROPS"]["SECID"],
		  'price_one'=> $arDeal["price"],  //0.1062
		  'lotsize'=> $arDeal["RADAR"]["PROPS"]["LOTSIZE"],
		  'date_price_one'=> $arDeal["RADAR"]["PRICE_ON_DATE"],
        );

		  if($key=='currency'){
			$arActiveArray['currencyEmitent']= strtolower($arDeal["RADAR"]["PROPS"]["SECID"]);
			$arActiveArray['secid']= $arDeal["RADAR"]["PROPS"]["SECID"];
			$arActiveArray['codeActive']= $arDeal["RADAR"]["PROPS"]["SECID"];

			if(!in_array($arDeal["RADAR"]["PROPS"]["BUY_CURRENCY"], $this->CPortfolio->arRoubleCodes)){
				$arActiveArray["currencyActiveReal"] = strtolower($arDeal["RADAR"]["PROPS"]["BUY_CURRENCY"]);
			}

		  }


		  if($key=='share_etf'){
			$priceCurrency = $this->CPortfolio->getHistActionsPrices('currency', $dealDate, strtoupper($dealCurrency), '');
			if($priceCurrency["price"]){
			  $priceCurrency = $priceCurrency["price"];
			} else {
			  $priceCurrency = 1;
			}

			$arActiveArray["currencyActiveReal"] = strtolower($dealCurrency);
		  	$arActiveArray["lotsize"] = 1;
		  	$arActiveArray["lastpriceActiveValute"] = $arDeal["price"]*(!empty($arDeal["RADAR"]["PROPS"]["LOTSIZE"])?$arDeal["RADAR"]["PROPS"]["LOTSIZE"]:1);
		  	$arActiveArray["lastpriceActive"] = $arDeal["RADAR"]["PRICE_ON_DATE"]*(!empty($arDeal["RADAR"]["PROPS"]["LOTSIZE"])?$arDeal["RADAR"]["PROPS"]["LOTSIZE"]:1)*$priceCurrency;
		  	$arActiveArray["priceActive"] = $arDeal["price"]*(!empty($arDeal["RADAR"]["PROPS"]["LOTSIZE"])?$arDeal["RADAR"]["PROPS"]["LOTSIZE"]:1)*$priceCurrency;
		  	$arActiveArray["price_one"] = $arDeal["price"]*$priceCurrency;
		  }

		  if($key=='bond'){ //Расчет цены облигации
		  	$arActiveArray["lotsize"] = 1;
			$arDeal["nkd"] = str_replace(",", ".", $arDeal["nkd"]);
			$arDeal["nkd"] = str_replace(" ", "", $arDeal["nkd"]);
			if($reportType=="tinkoff" || $reportType=="finam"){
			 $priceBond = floatval($arDeal["price"])+(floatval($arDeal["nkd"])/floatval($arDeal["amount"]));//Делим суммарный НКД на кол-во для добавления к цене за облигу;

			}
			if($reportType=="bcs"){//Для БКС цена облиг приходит в процентах, по этому делаем расчет цены по формуле «Сумма выручки» + «НКД» / кол-во
			 $priceBond = (floatval($arDeal["total"])+floatval($arDeal["nkd"]))/floatval($arDeal["amount"]);
			}
			if($reportType=="vtb"){//Для ВТБ цена облиг приходит в процентах, по этому делаем расчет цены по формуле «Сумма выручки» / кол-во
			 $priceBond = floatval($arDeal["total"])/floatval($arDeal["amount"]);
			}
			 $priceCurrency = 1;
			if(!in_array($dealCurrency, $this->CPortfolio->arRoubleCodes)){
				$priceCurrency = $this->CPortfolio->getHistActionsPrices('currency', $dealDate, strtoupper($dealCurrency), '');
				if($priceCurrency["price"]){
				  $priceCurrency = $priceCurrency["price"];
				} else {
				  $priceCurrency = 1;
				}
				$arActiveArray["currencyEmitent"] = "rub";
			}
			$arActiveArray["currencyActiveReal"] = strtolower($dealCurrency);
		  	$arActiveArray["price_one"] = $priceBond * $priceCurrency;
		  	$arActiveArray["priceActive"] = $priceBond * $priceCurrency;



		  }
		  if($key=='share_usa'){
			$priceCurrency = $this->CPortfolio->getHistActionsPrices('currency', $dealDate, strtoupper($dealCurrency), '');
			if($priceCurrency["price"]){
			  $priceCurrency = $priceCurrency["price"];
			} else {
			  $priceCurrency = 1;
			}
            
              global $USER;
              $rsUser = CUser::GetByID($USER->GetID());
              $arUser = $rsUser->Fetch();
              if($arUser["LOGIN"]=="freud"){
                  CLogger::AvonPrice("priceCurrency=".$priceCurrency.print_r($arDeal, true). print_r($arActiveArray, true));
              }
			
			
		  	$arActiveArray["lotsize"] = 1;
		  	$arActiveArray["lastpriceActiveValute"] = $arDeal["price"]*(!empty($arDeal["RADAR"]["PROPS"]["LOTSIZE"])?$arDeal["RADAR"]["PROPS"]["LOTSIZE"]:1);
		  	$arActiveArray["lastpriceActive"] = $arDeal["RADAR"]["PRICE_ON_DATE"]*(!empty($arDeal["RADAR"]["PROPS"]["LOTSIZE"])?$arDeal["RADAR"]["PROPS"]["LOTSIZE"]:1)*$priceCurrency;
		  	$arActiveArray["priceActive"] = $arDeal["price"]*(!empty($arDeal["RADAR"]["PROPS"]["LOTSIZE"])?$arDeal["RADAR"]["PROPS"]["LOTSIZE"]:1)*$priceCurrency;
		  	$arActiveArray["price_one"] = $arDeal["price"]*$priceCurrency;
		  }

		 if(isset($arDeal["finded"]["item"]) && !empty($arActiveArray["idActive"])){
			if($arDeal["action"]=="buy"){
			  if($debug){
			  	CLogger::BrokerLoader('pid='.$pid.' Покупка актива '.$arDeal["RADAR"]["NAME"].' > ');
			  }
				  if($key=='share_etf'){
					 //Для etf получаем код валюты из найденного соответствия в базе, а не из отчета
					 $dealCurrency = $arDeal["RADAR"]["PROPS"]["ETF_CURRENCY"];
					 //Если валюту нужно адаптировать по коду к базе сайта то пользуемся адаптером в классе CPortfolio
					 if(array_key_exists(strtoupper($dealCurrency), $this->CPortfolio->arCurrencyCodesAdapter)){
					 	$dealCurrency = $this->CPortfolio->arCurrencyCodesAdapter[strtoupper($dealCurrency)];

					 }
					 $arActiveArray['currencyEmitent'] = strtolower($dealCurrency);
				  }


		 	  $this->CPortfolio->addPortfolioActive($pid, $arActiveArray, array(), $this->CPortfolio->arHistoryActions["ACT_PLUS"], false, true);
			} else if($arDeal["action"]=="sell"){
				$arActiveArray["portfolioId"] = $pid;
				$arActiveArray['UF_HISTORY_ACT']= $this->CPortfolio->arHistoryActions["ACT_MINUS"];
				$arActiveArray['saleDate']= $dealDate;
				$arActiveArray['cnt']= $arActiveArray['cntActive'];

				  if($debug){
				  	CLogger::BrokerLoader('pid='.$pid.' Продажа актива '.$arDeal["RADAR"]["NAME"].' > ');
				  }

  //			 $firephp = FirePHP::getInstance(true);
  //        $firephp->fb(array("bond sell"=>$arActiveArray, "calc price"=>$priceBond, "deal"=>$arDeal),FirePHP::LOG);

			   //$this->CPortfolio->addToHistory("MINUS", $arActiveArray);
			   $this->CPortfolio->deletePortfolioActive($pid, $arActiveArray, '', $this->CPortfolio->arHistoryActions["ACT_MINUS"], true);

			}
		 }

		 }

		}

	 }
  
	 //Пересчитываем кеш портфеля с первой даты отчета
      Global $DB, $USER;
      $query = 'SELECT UF_ADD_DATE FROM `portfolio_history` WHERE `UF_PORTFOLIO` = '.$pid.' ORDER BY `UF_ADD_DATE` ASC LIMIT 1';
      $minDate = date('d.m.Y');
      $dbRes = $DB->Query($query);
      if($row = $dbRes->fetch()){
          $minDate = (new DateTime($row['UF_ADD_DATE']))->format('d.m.Y');
      }
      $this->CPortfolio->recountCacheOnPortfolio($pid, $minDate, true, 'report'); // третий парамтер true отключает добавление компенсаций отриц. кеша и обнуления отрицательных итогов за день при расчете после загрузки отчета
		$this->CPortfolio->clearCacheComplex($pid);
	 //	$this->CPortfolio->cleanPortfolioHistory($pid);
	 //	$this->CPortfolio->cleanGraphDataCache($pid);
		$this->CPortfolio->clearPortfolioListForUserCache($USER->GetID());

	  $finish = microtime(true);
		$delta = $finish - $start;
				  if($debug){
				  	CLogger::BrokerLoader('pid='.$pid.' Время выполнения '.round($delta,5).' | ');
				  }
		$arReturn = array("result"=>count($arExceptions)>0?"error":"success", "message"=>count($arExceptions)>0?implode("; ",$arExceptions):'Портфель успешно создан', "time"=>round($delta,5));
		return $arReturn;
		//  unset($CPortfolio);
  }


  /**
   * Дозаполняет портфель данными из ключа casheflow (внесение амортизации, валюты, налоги, погашения облиг, в общем описанного по мере развития сервиса)
   *
   * @return [add type]  [add description]
   *
   * @access public
   */
  public function fillFromCashflow($pid, $arCashflow=array(), $arReportData){
  	 $arResult = array();
	 $arErrors = array();
	 $arTmpActivesCodes = array();
	 $arTmpCurrenciesCodes = array();
	 foreach($arCashflow as $arCashflowDeal){
		 //Если валюту нужно адаптировать по коду к базе сайта то пользуемся адаптером в классе CPortfolio
		 if(array_key_exists(strtoupper($arCashflowDeal['currency']), $this->CPortfolio->arCurrencyCodesAdapter)){
		 	$arCashflowDeal['currency'] = $this->CPortfolio->arCurrencyCodesAdapter[strtoupper($arCashflowDeal['currency'])];
		 }
		 if($arCashflowDeal['type']=='bond_redemption' || $arCashflowDeal['type']=='tax' || $arCashflowDeal['type']=='comission' || $arCashflowDeal['type']=='dividends' || $arCashflowDeal['type']=='coupon'){
		 	//Налоги, дивы, купоны, амортизация
		 	 $ActName = explode('/',$arCashflowDeal['details']);
			 $ActName = $ActName[0];
			 if(!in_array($ActName,$arTmpActivesCodes)){
			 $arTmpActivesCodes[] = "%".$ActName."%";
			 }
			 if(!in_array($arCashflowDeal['currency'], $arTmpCurrenciesCodes)){
			 	$arTmpCurrenciesCodes[] = $arCashflowDeal['currency'];
			 }
		 }
		 else if($arCashflowDeal['type']=='cash_input' || $arCashflowDeal['type']=='cash_output'){
			 if(!in_array($arCashflowDeal['currency'], $arTmpCurrenciesCodes)){
			 	$arTmpCurrenciesCodes[] = $arCashflowDeal['currency'];
			 }
		 }
			else {continue;}
	 }
	// CLogger::BrokerLoader_redemption("arTmpActivesCodes=".print_r($arTmpActivesCodes, true));
	 //CLogger::BrokerLoader_redemption("arTmpCurrenciesCodes=".print_r($arTmpCurrenciesCodes, true));


					$arCurrenciesCodes = array();
					$arSelectCur     = Array("ID", "NAME", "CODE", "DETAIL_PAGE_URL");
					$arFilterCur     = Array("IBLOCK_ID" => 54, "ACTIVE" => "Y", "CODE" => $arTmpCurrenciesCodes);
					$resCur         = CIBlockElement::GetList(Array(), $arFilterCur, false, false, $arSelectCur);
					$resCur->SetUrlTemplates();
			  while($rowCur = $resCur->GetNextElement()){
			  	 $arFields = $rowCur->GetFields();
			  	$arCurrenciesCodes[$arFields["CODE"]] = $arFields;
			  }


	 // CLogger::BrokerLoader_redemption("arCurrenciesCodes=".print_r($arCurrenciesCodes, true));
   $arFilter = Array("IBLOCK_ID"=>27, "NAME" => $arTmpActivesCodes );

	$res = CIBlockElement::GetList(Array("sort"=>"asc", "NAME"=>"asc"), $arFilter, false, false, array("ID", "NAME", "IBLOCK_ID", "PROPERTY_ISIN"));
	$arActiveCodes = array();
	while($ob = $res->fetch()){
		$arActiveCodes[$ob["NAME"]] = $ob["PROPERTY_ISIN_VALUE"];
	}

	 //CLogger::BrokerLoader_redemption("arActiveCodes ".print_r($arActiveCodes, true));

    foreach($arCashflow as $k=>&$arCashflowDeal){
       $dealCurrency = $arCashflowDeal['currency'];
		 //Если валюту нужно адаптировать по коду к базе сайта то пользуемся адаптером в классе CPortfolio
		 if(array_key_exists(strtoupper($dealCurrency), $this->CPortfolio->arCurrencyCodesAdapter)){
		 	$dealCurrency = $this->CPortfolio->arCurrencyCodesAdapter[strtoupper($dealCurrency)];
		 }
	  if($arCashflowDeal['type']=='bond_redemption' || $arCashflowDeal['type']=='dividends' || $arCashflowDeal['type']=='coupon' ){
	  	//Фиксируем налоги, дивы, купоны, амортизация
	  $ActName = "";
		 if(!array_key_exists('active_code',$arCashflowDeal)){ //Если не привязан актив - ищем в сделках по названию
		 	 $ActName = explode('/',$arCashflowDeal['details']);
			 $ActName = $ActName[0];
			 if(array_key_exists($ActName, $arActiveCodes)){
				$arCashflow[$k]['active_code'] = $arActiveCodes[$ActName];
			 }
		 }

		  //Формируем название записи
		  if($arCashflowDeal['type']=='bond_redemption'){
		  	$description = (strpos($ActName,'Амортизация')===false?'Амортизация ':'').$ActName;
			$summ = $arCashflowDeal["input_value"];
		  } else if($arCashflowDeal['type']=='dividends'){
		  	$description = (strpos($ActName,'Дивиденды')===false?'Дивиденды ':'').$ActName;
			$summ = $arCashflowDeal["input_value"];
		  } else if($arCashflowDeal['type']=='coupon'){
		  	$description = (strpos($ActName,'Купон')===false?'Купон ':'').$ActName;
			$summ = $arCashflowDeal["input_value"];
		  }



		  $is_currency = !in_array(strtoupper($dealCurrency), $this->CPortfolio->arRoubleCodes)?true:false;
		  $lastpriceActive = 1;
		  if($is_currency){
		  	$priceCurrency = $this->getHistActionsPrices($pid,"currency", $arCashflowDeal['date'], strtoupper($dealCurrency), '')["price"];
		  	$lastpriceActive = $this->getHistActionsPrices($pid,"currency", date('d.m.Y'), strtoupper($dealCurrency), '')["price"];
		  }



        $arActiveArray = array(
		  'deal_id'=>'',
		  'report_version'=> $arReportData["version"],
		  'finished'=>'',
		  'actionType'=>$arCashflowDeal['type'],
		  'typeActive'=> 'currency',
		  'idActive'=> $arCurrenciesCodes[$dealCurrency]["ID"],
		  'nameActive'=> $arCurrenciesCodes[$dealCurrency]["NAME"],
		  'priceActive'=> ($is_currency?$priceCurrency:1),
        'cntActive'=>  $summ, //
		  'dateActive'=> $arCashflowDeal['date'],
		  'codeActive'=> strtoupper($dealCurrency),
		  'urlActive'=>  $arCurrenciesCodes[$dealCurrency]["DETAIL_PAGE_URL"],
        'currencyEmitent'=> strtolower($dealCurrency),
		  'nameActiveEmitent'=> '',//$arCurrenciesCodes[$arCashflowDeal['currency']]["NAME"],
		  'urlActiveEmitent'=> '',//$arCurrenciesCodes[$arCashflowDeal['currency']]["DETAIL_PAGE_URL"],
        'lastpriceActive'=> $lastpriceActive,
		  'secid'=> strtoupper($dealCurrency),
		  'price_one'=> ($is_currency?$priceCurrency:1),
		  'lotsize'=> 1,
		  'date_price_one'=> ($is_currency?$priceCurrency:1),
		  'description'=>$description,
		  'insert'=>'Y',
        );
		  //CLogger::BrokerLoader_redemption("arActiveArray ".print_r($arActiveArray, true));

			 $this->CPortfolio->addPortfolioActive($pid, $arActiveArray, array(), $this->CPortfolio->arHistoryActions["CACHE_PLUS"], true, true);
	  }
	  elseif($arCashflowDeal['type']=='cash_input'){ //Внесение кеша
		  $is_currency = !in_array(strtoupper($dealCurrency), $this->CPortfolio->arRoubleCodes)?true:false;
		  $lastpriceActive = 1;
		  if($is_currency){
	   	$priceCurrency = $this->getHistActionsPrices($pid,"currency", $arCashflowDeal['date'], strtoupper($dealCurrency), '')["price"];
		  	$lastpriceActive = $this->getHistActionsPrices($pid,"currency", date('d.m.Y'), strtoupper($dealCurrency), '')["price"];
		  }
        $arActiveArray = array(
		  'deal_id'=>'',
		  'report_version'=> $arReportData["version"],
		  'finished'=>'',
		  'typeActive'=> 'currency',
		  'idActive'=> $arCurrenciesCodes[$dealCurrency]["ID"],
		  'nameActive'=> $arCurrenciesCodes[$dealCurrency]["NAME"],
		  'priceActive'=> ($is_currency?$priceCurrency:1),
        'cntActive'=>  $arCashflowDeal["input_value"], //
		  'dateActive'=> $arCashflowDeal['date'],
		  'codeActive'=> strtoupper($dealCurrency),
		  'urlActive'=>  $arCurrenciesCodes[$dealCurrency]["DETAIL_PAGE_URL"],
        'currencyEmitent'=> strtolower($dealCurrency),
		  'nameActiveEmitent'=> '',
		  'urlActiveEmitent'=> '',
        'lastpriceActive'=> $lastpriceActive,
		  'secid'=> strtoupper($dealCurrency),
		  'price_one'=> ($is_currency?$priceCurrency:1),
		  'lotsize'=> 1,
		  'date_price_one'=> ($is_currency?$priceCurrency:1),
		  'description'=>'',
		  'insert'=>'Y',
        );
		  //CLogger::BrokerLoader_redemption("arActiveArray ".print_r($arActiveArray, true));

			 $this->CPortfolio->addPortfolioActive($pid, $arActiveArray, array(), $this->CPortfolio->arHistoryActions["CACHE_PLUS"], true, true);
	  }
	  elseif($arCashflowDeal['type']=='cash_output' || $arCashflowDeal['type']=='tax' || $arCashflowDeal['type']=='comission'){ //Изъятие кеша


		  $is_currency = !in_array(strtoupper($dealCurrency), $this->CPortfolio->arRoubleCodes)?true:false;
		  $lastpriceActive = 1;
		  $exclude = 'Y';
		  if($is_currency){
	   	$priceCurrency = $this->getHistActionsPrices($pid,"currency", $arCashflowDeal['date'], strtoupper($dealCurrency), '')["price"];
		  	$lastpriceActive = $this->getHistActionsPrices($pid,"currency", date('d.m.Y'), strtoupper($dealCurrency), '')["price"];
		  }

		  //Формируем название записи
		if($arCashflowDeal['type']=='tax' || $arCashflowDeal['type']=='comission'){
		  $ActName = "";
			 if(!array_key_exists('active_code',$arCashflowDeal)){ //Если не привязан актив - ищем в сделках по названию
			 	 $ActName = explode('/',$arCashflowDeal['details']);
				 $ActName = $ActName[0];
				 if(array_key_exists($ActName, $arActiveCodes)){
					$arCashflow[$k]['active_code'] = $arActiveCodes[$ActName];
				 }
			 }
		 }
		  if($arCashflowDeal['type']=='tax'){
		  	$description = (strpos($ActName,'Налог')===false?'Налог ':'').$ActName;
			$exclude = 'N';
		  } else if($arCashflowDeal['type']=='comission'){
		  	$description = (strpos($ActName,'Комиссия')===false?'Комиссия ':'').$ActName;
			$exclude = 'N';
		  } else {
		  	$description = '';
		  }


        $arActiveArray = array(
		  'deal_id'=>'',
		  'actionType'=>$arCashflowDeal['type'],
		  'report_version'=> $arReportData["version"],
		  'finished'=>'',
		  'typeActive'=> 'currency',
		  'idActive'=> $arCurrenciesCodes[$dealCurrency]["ID"],
		  'nameActive'=> $arCurrenciesCodes[$dealCurrency]["NAME"],
		  'priceActive'=> ($is_currency?$priceCurrency:1),
        'cnt'=>  $arCashflowDeal["output_value"], //
		  'dateActive'=> $arCashflowDeal['date'],
		  'codeActive'=> strtoupper($dealCurrency),
		  'urlActive'=>  $arCurrenciesCodes[$dealCurrency]["DETAIL_PAGE_URL"],
        'currencyEmitent'=> strtolower($dealCurrency),
		  'nameActiveEmitent'=> '',
		  'urlActiveEmitent'=> '',
        'lastpriceActive'=> $lastpriceActive,
		  'secid'=> strtoupper($dealCurrency),
		  'price_one'=> ($is_currency?$priceCurrency:1),
		  'lotsize'=> 1,
		  'date_price_one'=> ($is_currency?$priceCurrency:1),
		  'description'=>$description,
		  'exclude' => $exclude
        );
		  //CLogger::BrokerLoader_redemption("arActiveArray ".print_r($arActiveArray, true));

			 $this->CPortfolio->deletePortfolioCurrency($pid, $arActiveArray, array(), $this->CPortfolio->arHistoryActions["CACHE_MINUS"], true, 'report');
	  }else {
	  	continue;
	  }

	 }
	  return $arResult;
  }
  
}