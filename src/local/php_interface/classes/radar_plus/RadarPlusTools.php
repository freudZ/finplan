<?

Class RadarPlusTools {
  public $arMonthToKvartal = array(
				"01"=>1,
				"02"=>1,
				"03"=>1,
				"04"=>2,
				"05"=>2,
				"06"=>2,
				"07"=>3,
				"08"=>3,
				"09"=>3,
				"10"=>4,
				"11"=>4,
				"12"=>4,
			);
		/**
	 * Возвращает массив кварталов по годам для указанного стартового и конечного квартала
	 *
	 * @param  string   $pStart (Optional)  '4-2011'
	 *
	 * @param  string   $pEnd (Optional)  '2-2019'
	 *
	 * @return array  массив с входящими в диапазон кварталами (включительно с начальным и конечным периодами)
	 *
	 * @access public
	 */
	public function getPeriodRange($pStart='', $pEnd='', $addCurrentPeriod=false){
	    		global $APPLICATION;

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "periods_rplus_data".($addCurrentPeriod?'_current':'');
		$cacheTtl = 86400*7;

		if ($cache->read($cacheTtl, $cacheId)) {
			$arFilterPeriods = $cache->get($cacheId);
		} else {
	  $arFilterPeriods = array("DIPSLAY_RANGE"=>array(), "FILTER_ARRAY"=>array());



	  $arPerStart = explode('-',$pStart);
	  $arPerEnd = explode('-',$pEnd);
	  $deltaYear = $arPerEnd[1]-$arPerStart[1];

	  if(count($arPerStart)<=1 || count($arPerEnd)<=1 || $deltaYear<0 || ($deltaYear==0 && $arPerStart[0]>$arPerEnd[0])) return $arFilterPeriods;

	  $per_kv = $arPerStart[0];
	  for($per=$arPerStart[1]; $per<=$arPerEnd[1]; $per++){
	   if($per==$arPerEnd[1]){
			$endkv = $arPerEnd[0];
	   } else {
	   	$endkv=4;
	   }
	   for($pk=$per_kv; $pk<=$endkv;$pk++){
		$arFilterPeriods["DIPSLAY_RANGE"][] = $pk."-".$per;
		$arFilterPeriods["RANGE_VALUES"][$pk."-".$per] = 0;
		$arFilterPeriods["FILTER"][] = array(
    array(
       "LOGIC" => "AND",
       array("UF_KVARTAL" => $pk),
       array("UF_YEAR" => $per),
    )
);
		}
		$per_kv=1;

		if($per==$arPerEnd[1] && $per_kv==$arPerEnd[0]) break;

	  }

	  if($addCurrentPeriod){ //Если добавляем текущий период
	    $curKv = $this->arMonthToKvartal[date('n')];
		 $cutYear = date("Y");
		 $arFilterPeriods["DIPSLAY_RANGE"][] = $curKv."-".$cutYear;
		 $arFilterPeriods["RANGE_VALUES"][$curKv."-".$cutYear] = 0;
		$arFilterPeriods["FILTER"][] = array(
	    array(
	       "LOGIC" => "AND",
	       array("UF_KVARTAL" => $curKv),
	       array("UF_YEAR" => $cutYear),
	  	  )
			);
	  }
	  $arFilterPeriods["CURRENT_PERIOD"] = $curKv."-".$cutYear;

	  $cache->set($cacheId, $arFilterPeriods);
		}

	  return $arFilterPeriods;
	}


  /**
   * Сравнивает две записи квартал-год в логике: $qyOne [старше/младше/равно] $qyTwo
   *
   * @param  string   $qyOne "1-2019"
   * @param  string   $qyTwo "2-2019"
   *
   * @return int [1/-1/0] [старше/младше/равно]
   *
   * @access public
   */
  public function compareQuartalYear($qyOne, $qyTwo){
	$res = false;
	$arQYOne = explode("-",$qyOne);
	$arQYTwo = explode("-",$qyTwo);

	if($arQYOne[1]==$arQYTwo[1]){//если год одинаковый то сравниваем кварталы
	 if($arQYOne[0]==$arQYTwo[0]){$res=0;}
	 if($arQYOne[0]<$arQYTwo[0]){$res=1;}
	 if($arQYOne[0]>$arQYTwo[0]){$res=-1;}

	} else { //если годы разные - то сравниваем годы
	 if($arQYOne[1]<$arQYTwo[1]){$res=1;}
	 if($arQYOne[1]>$arQYTwo[1]){$res=-1;}
	}

	 return $res;
  }


  /**
   * Возвращает список компаний и их ID
   *
   * @param  bool   $USA (Optional) true если нужно вернуть копании США
   *
   * @return array
   *
   * @access public
   */
  public function getCompanyList($USA=false){
  		global $APPLICATION;

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "companies_rplus_data".($USA?'_usa':'');
		$cacheTtl = 86400*7;
		if ($cache->read($cacheTtl, $cacheId)) {
			$arCompanies = $cache->get($cacheId);
		} else {
			$arCompanies = array();
	  CModule::IncludeModule("iblock");
	   $iblockID = 26;
		$actionsIblockId = 32;
		if($USA==true){
			$iblockID = 43;
			$actionsIblockId = 0; //TODO прописать id нового инфоблока с акциями США
			}

       //Получаем информацию по акциям только с установленным эмитентом и находящиеся в обороте на бирже //"=PROPERTY_HIDEN"=>false
	    $arFilterActions = Array("IBLOCK_ID"=>$actionsIblockId, "ACTIVE"=>"Y", "!PROPERTY_EMITENT_ID"=>false);

		 $res = CIBlockElement::GetList(Array(), $arFilterActions, false, false, array("NAME", "IBLOCK_ID", "CODE", "PROPERTY_EMITENT_ID"));
		 $arCompanyFilter = array();
		 while($ob = $res->Fetch())
		{
		  $arCompanyFilter[$ob["PROPERTY_EMITENT_ID_VALUE"]]=$ob["PROPERTY_EMITENT_ID_VALUE"];
		}

		if(count($arCompanyFilter)){


		$arSelect = Array("ID", "NAME", "IBLOCK_ID");
		$arFilter = Array("IBLOCK_ID"=>IntVal($iblockID), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>array_keys($arCompanyFilter));
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->Fetch()){
		 $arCompanies[$ob["ID"]] = $ob["NAME"];
		}

		}
	 			$cache->set($cacheId, $arCompanies);
		}

  	 return $arCompanies;
  }

}
