<?
class RadarPlusAccess {
	private $type;

	public function __construct()
	{
		global $APPLICATION;
		global $USER;

		if($USER->IsAdmin()){
			$this->type = "admin";
		} elseif(in_array(8, $USER->GetUserGroupArray())) {
			$this->type = "manager";
		}

		//вид по умолчанию
		if(!$_SESSION["radar_plus_table_view"]){
			$_SESSION["radar_plus_table_view"] = "actives";
		}

		if($_GET["change_table_view"]){
	 /*		if($this->type != "actives" && $_GET["change_table_view"]=="actives"){
				die("!!!");
			}*/

			$_SESSION["radar_plus_table_view"] = $_GET["change_table_view"];
			LocalRedirect($APPLICATION->GetCurPageParam("", array("change_table_view")));
		}
	}

	public function getCurTableView(){
		return $_SESSION["radar_plus_table_view"];
	}

	public function checkGlobalAccess(){
		global $APPLICATION;

		if(!$this->type){
			$APPLICATION->RestartBuffer();
			die("У вас нет доступа");
		}
	}

	public function getType(){
		return $this->type;
	}

	public function getTableViewTypes(){
		$tmp = [
			"actives" => [
				"name" => "Представление по активам",
			],
			"periods" => [
				"name" => "Представление по периодам",
			],
		];

		if($this->type!="admin"){
			unset($tmp["admin"]);
		}

		return $tmp;
	}
}