<? use Bitrix\Highloadblock as HL;
	CModule::IncludeModule("highload");
//Акции расширенного радара
class RadarPlusActions extends RadarBase {
	private $arItems = array();
	public $arOriginalsItems = array();
	public $arOriginalsItemsKeySECID = array();
	public $arActionsPrices = array(); //Список цен акций для списка компаний с ключем по SECID
	public $MoexActionsDataFilter= array();//Фильтр для получения цен акций по последним рабочим дням
	public $MoexActionsDataHLIblockId = 24;
	public $oblActionDividDataHLIblockId = 21;//Данные по дивидендам
	public $arOblActionDividData = array();
	public $count;
	private $perPage = 5;
	public $havePayAccess = false;
   public $arMonthToKvartal = array(
				"01"=>1,
				"02"=>1,
				"03"=>1,
				"04"=>2,
				"05"=>2,
				"06"=>2,
				"07"=>3,
				"08"=>3,
				"09"=>3,
				"10"=>4,
				"11"=>4,
				"12"=>4,
			);

	public $arCouseKvartalDates = array(1=>"30.04",2=>"30.06",3=>"30.09", 4=>"31.12");

	function __construct(){
		$this->setDatesFilterForMoexActionsData();
		//$this->setDividendsOfQuart();
		$this->setData();
	}

	//отдельный элемент по коду
	public function getItem($code){
		foreach($this->arOriginalsItems as $item){
			if($item["SECID"]==$code){
				return $item;
			}
		}

		return false;
	}

	//Вычисляет и кеширует дивиденды по акциям и суммированные по кварталам
   public function setDividendsOfQuart(){
		global $APPLICATION;

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "actions_rplus_dividends_data";
		$cacheTtl = 86400*7;

		if ($cache->read($cacheTtl, $cacheId)) {
			$this->arOblActionDividData = $cache->get($cacheId);
		} else {
			CModule::IncludeModule("highloadblock");

			//TODO кешировать "cache"=>array("ttl"=>3600)  доступно с версии 16.5.9, сделать после обновления
			$hlblock   = HL\HighloadBlockTable::getById($this->oblActionDividDataHLIblockId)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

			$arFilter = array('!=UF_DATA'=>'[]');
			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array("UF_DATA", "UF_ITEM"),
				//"limit" => 20,
				"order" => array(
					"ID" => "DESC"
				)
			));
			while($item = $res->fetch()){
			 $arData = json_decode($item["UF_DATA"], true);

			 foreach($arData as $arPeriod){
			 $end = new DateTime($arPeriod["Дата закрытия реестра"]);
			 $year = $end->format('Y');
			 $month = $this->arMonthToKvartal[$end->format('m')];
			 $this->arOblActionDividData[$item["UF_ITEM"]][$year][$month] += floatval($arPeriod["Дивиденды"]);
				}
			}
			 $cache->set($cacheId, $this->arOblActionDividData);
		 }

   }

	//отдельный элемент по коду
	public function getItemBySECID($SECID){
		if(array_key_exists($SECID, $this->arOriginalsItemsKeySECID)){
		  return $this->arOriginalsItemsKeySECID[$SECID];
		}

		return false;
	}
/**
* Compute the start and end date of some fixed o relative quarter in a specific year.
* @param mixed $quarter  Integer from 1 to 4 or relative string value:
*                        'this', 'current', 'previous', 'first' or 'last'.
*                        'this' is equivalent to 'current'. Any other value
*                        will be ignored and instead current quarter will be used.
*                        Default value 'current'. Particulary, 'previous' value
*                        only make sense with current year so if you use it with
*                        other year like: get_dates_of_quarter('previous', 1990)
*                        the year will be ignored and instead the current year
*                        will be used.
* @param int $year       Year of the quarter. Any wrong value will be ignored and
*                        instead the current year will be used.
*                        Default value null (current year).
* @param string $format  String to format returned dates
* @return array          Array with two elements (keys): start and end date.
*/
public static function get_dates_of_quarter($quarter = 'current', $year = null, $format = null)
{
    if ( !is_int($year) ) {
       $year = (new DateTime)->format('Y');
    }
    $current_quarter = ceil((new DateTime)->format('n') / 3);
    switch (  strtolower($quarter) ) {
    case 'this':
    case 'current':
       $quarter = ceil((new DateTime)->format('n') / 3);
       break;

    case 'previous':
       $year = (new DateTime)->format('Y');
       if ($current_quarter == 1) {
          $quarter = 4;
          $year--;
        } else {
          $quarter =  $current_quarter - 1;
        }
        break;

    case 'first':
        $quarter = 1;
        break;

    case 'last':
        $quarter = 4;
        break;

    default:
        $quarter = (!is_int($quarter) || $quarter < 1 || $quarter > 4) ? $current_quarter : $quarter;
        break;
    }
    if ( $quarter === 'this' ) {
        $quarter = ceil((new DateTime)->format('n') / 3);
    }
    $start = new DateTime($year.'-'.(3*$quarter-2).'-1 00:00:00');
    $end = new DateTime($year.'-'.(3*$quarter).'-'.($quarter == 1 || $quarter == 4 ? 31 : 30) .' 23:59:59');

    return array(
        'start' => $format ? $start->format($format) : $start,
        'end' => $format ? $end->format($format) : $end,
    );
}


	//Получает список цен всех акций
	public function getActionPrices($SECID=''){
	 $arResult = array();
			//TODO кешировать "cache"=>array("ttl"=>3600)  доступно с версии 16.5.9, сделать после обновления
			$hlblock   = HL\HighloadBlockTable::getById($this->MoexActionsDataHLIblockId)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

		if(!empty($SECID)){
		  $arFilter[] = array('LOGIC' => 'AND', array('UF_ITEM'=>$SECID));
		  $this->MoexActionsDataFilter = $arFilter[]= $this->MoexActionsDataFilter;
		} else {
		  $this->MoexActionsDataFilter = $this->MoexActionsDataFilter;
		}

			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array(
					"UF_ITEM", "UF_DATE", "UF_CLOSE", "UF_ITEM"
				),
				//"limit" => 20,
				"order" => array(
					"ID" => "DESC"
				)
			));

			while($item = $res->fetch()){

			 $end = new DateTime($item["UF_DATE"]);
			 $year = $end->format('Y');
			 $month = $this->arMonthToKvartal[$end->format('m')];
			 $date = $end->format('d.m.Y');
			 $this->arActionsPrices[$item["UF_ITEM"]][$year][$month][$date]["PRICE"] = round($item["UF_CLOSE"],2);
			}

		  return $arResult;
	}


	//Получает список цен акций по $SECID
	public function getActionPricesBySECID($SECID=''){
	 $arResult = array();
			//TODO кешировать "cache"=>array("ttl"=>3600)  доступно с версии 16.5.9, сделать после обновления
			$hlblock   = HL\HighloadBlockTable::getById($this->MoexActionsDataHLIblockId)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

			$arFilter = array("UF_ITEM"=>$SECID);
	   $nowYear = intval(date('Y'));
		$firstYear = 2014;
		$arFilter[] = array('LOGIC' => 'OR');

		for($i=$nowYear;$i>=$firstYear;$i--){
			for($j=1;$j<=12;$j++){
			 $arFilter[] = array(
			'<=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime('31.'.$j.'.'.$i),
			'>=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime('26.'.$j.'.'.$i)
		    );
			}
		}

			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array(
					"UF_ITEM", "UF_DATE", "UF_CLOSE"
				),
				"order" => array(
					"ID" => "DESC"
				)
			));
			while($item = $res->fetch()){
			 $end = new DateTime($item["UF_DATE"]);
			 $arResult[$end->format('d.m.Y')] = $item["UF_CLOSE"];
			}

		  return $arResult;
	}

	//Получает цены акций по периодам и высчитывает капу по формуле, если заданы переменные для акции
	public function setFormulaCapForPeriods($arCompanyPeriods,$arItemProps){


// D7
/*$cache = Bitrix\Main\Data\Cache::createInstance();
if ($cache->initCache(86400, $arItemProps["SECID"], "/getActionPricesBySECID/"))
{
    $arActionPrices = $cache->getVars();
}
elseif ($cache->startDataCache())
{
    $arActionPrices = $this->arActionsPrices[$arItemProps["SECID"]];
    if ($isInvalid)
    {
        $cache->abortDataCache();
    }
    // ...
    $cache->endDataCache($arActionPrices);
}*/


	 $arActionPrices = $this->arActionsPrices[$arItemProps["SECID"]];

	  foreach($arCompanyPeriods as $kper=>$vper){
	      if($vper["PERIOD_TYPE"]=="KVARTAL"){
				 $end_data = self::get_dates_of_quarter(intval($vper["PERIOD_VAL"]), intval($vper["PERIOD_YEAR"]), 'd.m.Y');

				 if(!array_key_exists($end_data['end'],$arActionPrices[$vper["PERIOD_YEAR"]][$vper["PERIOD_VAL"]])){ //Если на текущую дату нет записей то ищем назад по датам до первой найденной
					for($i=1; $i<30; $i++ ){
					 $end = new DateTime($end_data['end']);
					 $end->modify("-".$i."day")->format( 'd.m.Y' );
					 if(array_key_exists($end->format( 'd.m.Y' ),$arActionPrices[$vper["PERIOD_YEAR"]][$vper["PERIOD_VAL"]])){
					  $end_data = $end->format( 'd.m.Y' );
					  break;
					 }
					}
				 }
			} else if($vper["PERIOD_TYPE"]=="MONTH"){
				 $d = new DateTime( '01.'.$vper["PERIOD_VAL"].'.20'.$vper["PERIOD_YEAR"] );
				 $end_data = $d->format( 't.m.Y' );
				 if(!array_key_exists($end_data,$arActionPrices[$vper["PERIOD_YEAR"]][$vper["PERIOD_VAL"]])){ //Если на текущую дату нет записей то ищем назад по датам до первой найденной
				 for($i=1; $i<30; $i++ ){
					 $end = new DateTime($end_data);
					 $end->modify("-1day");
					 if(array_key_exists($end,$arActionPrices[$vper["PERIOD_YEAR"]][$vper["PERIOD_VAL"]])){
					  $end_data = $end->format( 'd.m.Y' );
					  break;
					 }
					}
				 }

				 $arCompanyPeriods[$kper]["LEGALCLOSE"] = $arActionPrices[$vper["PERIOD_YEAR"]][$vper["PERIOD_VAL"]]["PRICE"];
				 $arCompanyPeriods[$kper]["LEGALCLOSE_DATE"] = $end_data['end']['end'];
				 $arCompanyPeriods[$kper]["LEGALCLOSE_PERIOD"] = $kper;


			}
/*			 echo $vper["PERIOD_TYPE"]."_".$vper["PERIOD_YEAR"]."_".$vper["PERIOD_VAL"]."_".$end_data['end']."_price = ".$arActionPrices[$vper["PERIOD_YEAR"]][$vper["PERIOD_VAL"]][$end_data['end']]["PRICE"]."<pre  style='color:black; font-size:11px;'>";
                print_r( $arActionPrices[$vper["PERIOD_YEAR"]][$vper["PERIOD_VAL"]][$end_data['end']]["PRICE"]);
                print_r( $arActionPrices);
                echo "</pre>";*/


/*						if(intval($arItemProps["CAPITAL_ACTION_CNT"])>0 && intval($arItemProps["CAPITAL_FORMULA_DIVIDER"])>0){
						  $cap = $arItemProps["CAPITAL_ACTION_CNT"]*$arCompanyPeriods[$kper]["LEGALCLOSE"]/$arItemProps["CAPITAL_FORMULA_DIVIDER"];
						  $arCompanyPeriods[$kper]["Прошлая капитализация"] = round($cap,2);

					}*/


	  }
	 return $arCompanyPeriods;
	}

	//Определяет $comparingQ на больше, меньше или равно по отношению к $baseQ - обычно стартовая дата обращения
	public function QuartalsCompare($baseQ = '1-2011', $comparingQ = '1-2011'){
		$arBase = explode("-",$baseQ);
		$arComp = explode("-",$comparingQ);
		$year = 0;
		$quart = 0;
		if($arComp[1]>$arBase[1]){
			$year = 1;
		}elseif($arComp[1]<$arBase[1]){
			$year = -1;
		}
		if($arComp[0]>$arBase[0]){
			$quart = 1;
		}elseif($arComp[0]<$arBase[0]){
			$quart = -1;
		}

		if($year>0){
			$result = 1;
		}elseif($year<0){
			$result = -1;
		}elseif($year==0){

			if($quart>0){
			  $result = 1;
			}elseif($quart<0){
			  $result = -1;
			}elseif($quart==0){
			  $result = 0;
			}

		}




		return $result;
	}

	private function setData(){
		global $APPLICATION;

		$cache = Bitrix\Main\Data\Cache::createInstance();
		//$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "actions_rplus_data";
		$cacheTtl = 86400*7;
		$cacheDir = '/'.$cacheId."/";
		//if ($cache->read($cacheTtl, $cacheId)) {
		if ($cache->initCache($cacheTtl, $cacheId, $cacheDir)) {
			//$arItems = $cache->get($cacheId);
			$arItems = $cache->getVars();

			//$arItemsSECID = $cache->get($cacheId.'_secid');
		} else {
			$cache->startDataCache();
			$arItems = array();
			//$arItemsSECID = array();
			CModule::IncludeModule("iblock");
			CModule::IncludeModule("highloadblock");

			include($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/classes/fpt_actions_dividents.php");
			$cDivs = new fptActionsDividends();
			$arActionsStartQuartals = $cDivs->arActionsStartQuartals;
			$arActionsStartQuartalsCompany = $cDivs->arActionsStartQuartalsCompany;
			$ibID = 32;

			$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

			if(is_string($f_period)){ //Если передана строка - то фильтр для представления по активам
			$f_period = explode('-',$f_period);
			if(is_array($f_period)){
			 $arYears = array(intval($f_period[1]),(intval($f_period[1])-1)); //фильтруем по текущему выбранному года + год назад для расчетов итогов таблицы
			// $arFilter = array("UF_KVARTAL"=>intval($f_period[0]),"UF_YEAR"=>$arYears, "!UF_YEAR" => false);
			}

			} else if(is_array($f_period)){ //Если передан массив с диапазоном - то фильтр для представления по диапазонам
		  //	 $arFilter = $f_period;

			}

			$arFilter["!UF_YEAR"] = false;
			$arFilter["UF_MONTH"] = false;
			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array(
					"*"
				),
				"order" => array(
					"ID" => "DESC"
				)
			));
			$arPeriods_Types=array();
			while($item = $res->fetch()){


				$item["UF_DATA"] = json_decode($item["UF_DATA"], true);

				if(isset($item["UF_KVARTAL"])){
					$type = "KVARTAL";
					$t = $item["UF_KVARTAL"];
				} else {
					$type = "MONTH";
					$t = $item["UF_MONTH"];
				}

				$item["UF_DATA"]["PERIOD_YEAR"] = $item["UF_YEAR"];
				$item["UF_DATA"]["PERIOD_VAL"] = $t;
				$item["UF_DATA"]["PERIOD_TYPE"] = $type;
				$arPeriods_Types[$item["UF_COMPANY"]]=$type;
				$curActionStartPeriod = $arActionsStartQuartalsCompany[$item["UF_COMPANY"]];
				if($this->QuartalsCompare($curActionStartPeriod,$t."-".$item["UF_YEAR"])>=0) //Если период больше или равен стартовому периоду - добавляем
				$arPeriods[$item["UF_COMPANY"]][$t."-".$item["UF_YEAR"]."-".$type] = $item["UF_DATA"];
			}




            //Подготовим страницы всех акций и закешируем
 				$arFilter = Array("IBLOCK_ID"=>33, "ACTIVE"=>"Y");
				$arActionsPages = array();
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL","CODE","PROPERTY_SLIDING_DIVS", "PROPERTY_DIVIDENDS"));

				while($val = $res2->GetNext())
				{
					$arDivs = unserialize($val["~PROPERTY_DIVIDENDS_VALUE"]["TEXT"]);
					$arSlideDivs = unserialize($val["~PROPERTY_SLIDING_DIVS_VALUE"]["TEXT"]);
					$arActionsPages[$val["CODE"]] = array("DETAIL_PAGE_URL"=>$val["DETAIL_PAGE_URL"], "DIVIDENDS"=>$arDivs, "SLIDING_DIVS"=>$arSlideDivs);
				}

				//Подготовим выборку всех компаний
 				$arFilter = Array("IBLOCK_ID"=>26, "ACTIVE"=>"Y");
				$arCompanies = array();
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID","NAME"));
				while($val = $res2->Fetch())
				{
					$arCompanies[$val["ID"]] = array("NAME" => $val["NAME"]);
				}

				//Подготовим выборку по страницам компаний
				$arCompanyPages = array();
				$arFilter = Array("IBLOCK_ID"=>29, "ACTIVE"=>"Y");
				$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, array("DETAIL_PAGE_URL", "NAME", "PROPERTY_VAL_ED_FIRST", "PROPERTY_CAPITAL_KOEF", "PROPERTY_CAPITAL_ACTION_CNT", "PROPERTY_CAPITAL_FORMULA_DIVIDER"));
				while($val = $res2->GetNext())
				{
					$arCompanyPages[$val["NAME"]]=$val;
				}

			//Получаем таблицу исторических курсов доллара для компний, отчитывающихся в долларах
			$arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_COURSE");
         $arFilter = Array("IBLOCK_ID"=>IntVal(50), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
         $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$arHistoryCourses = array();
			while($ob = $res->Fetch()){
			 $arHistoryCourses[$ob["NAME"]] = $ob["PROPERTY_COURSE_VALUE"];
         }


			//Предыдущий день
			$prevDay = new DateTime();
			$prevDay->modify("-1 day");
			$USD_CURRENT = getCBPrice("USD", $prevDay->format("d/m/Y"));

			$arFilter = Array("IBLOCK_ID"=>$ibID);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
			while($ob = $res->GetNextElement())
			{
				$fields = $ob->GetFields();

				$item = array(
					"NAME" => $fields["NAME"],
					"SECID" => $fields["CODE"],
					//"URL" => $fields["DETAIL_PAGE_URL"],
				);

				//страница акции
				if(array_key_exists($fields["CODE"],$arActionsPages)){
					$item["URL"] = $arActionsPages[$fields["CODE"]]["DETAIL_PAGE_URL"];
				}

				$props = $ob->GetProperties();

				foreach($props as $prop){
					if($prop["VALUE"]){
						if($prop["CODE"]=="PROP_TARGET"){
							$prop["VALUE"] = str_replace("%", "", $prop["VALUE"]);
						}
						$item["PROPS"][$prop["CODE"]] = $prop["VALUE"];
					}
				}
				$this->getActionPrices($item["PROPS"]["SECID"]);
				$item["PROPS"]["PROP_PROSAD"] = str_replace(",", ".", $item["PROPS"]["PROP_PROSAD"]);
				$item["PROPS"]["PROP_TARGET"] = str_replace(",", ".", $item["PROPS"]["PROP_TARGET"]);

				//Убрать с режимами торгов
				if(in_array($item["PROPS"]["BOARDID"], $APPLICATION->ExcludeFromRadar["акции"]) || !$item["PROPS"]["BOARDID"]){
					continue;
				}

				//если нет LASTPRICE - берем LEGALCLOSE
				if($item["PROPS"]["LASTPRICE"]<=0 && $item["PROPS"]["LEGALCLOSE"]>0) {
					$item["PROPS"]["LASTPRICE"] = $item["PROPS"]["LEGALCLOSE"];
				}

				if($item["PROPS"]["EMITENT_ID"]){
					$item["COMPANY"] = array(
						"ID" => $item["PROPS"]["EMITENT_ID"],
						"NAME" => $arCompanies[$item["PROPS"]["EMITENT_ID"]]["NAME"],
						//"URL" => $comp["DETAIL_PAGE_URL"],
					);

					//страница компании
					$USE_DOLLARS = false;
					if(array_key_exists($item["COMPANY"]["NAME"],$arCompanyPages))
					{
						$val = $arCompanyPages[$item["COMPANY"]["NAME"]];
						if(strpos($val["PROPERTY_VAL_ED_FIRST_VALUE"], "долл")!==false ) $USE_DOLLARS = true;
						$item["COMPANY"]["URL"] = $val["DETAIL_PAGE_URL"];
						$item["COMPANY"]["USE_DOLLARS"] = $USE_DOLLARS?"Y":"N";

						//если данные в долларах, перевести капитализацию
						if($USE_DOLLARS && $item["PROPS"]["ISSUECAPITALIZATION"]){
						 	$item["PROPS"]["ISSUECAPITALIZATION_DOL"] = $item["PROPS"]["ISSUECAPITALIZATION"]/$USD_CURRENT;
						}

						if($val["PROPERTY_CAPITAL_KOEF_VALUE"]){
							$item["COMPANY"]["CAPITAL_KOEF"] = $val["PROPERTY_CAPITAL_KOEF_VALUE"];
						}

						//Принудительно считаем капитализацию, если задано кол-ва акций и знаменатель для формулы (св-ва на странице компании)
/*						if(intval($val["PROPERTY_CAPITAL_ACTION_CNT_VALUE"])>0 && intval($val["PROPERTY_CAPITAL_FORMULA_DIVIDER_VALUE"])>0){
						  $cap = $val["PROPERTY_CAPITAL_ACTION_CNT_VALUE"]*$item["PROPS"]["LASTPRICE"]/$val["PROPERTY_CAPITAL_FORMULA_DIVIDER_VALUE"];
						  $item["PROPS"]["CAP_CALC_BY_FORMULA"] = round($cap,2);
						  $item["PROPS"]["CAPITAL_ACTION_CNT"] = $val["PROPERTY_CAPITAL_ACTION_CNT_VALUE"];
						  $item["PROPS"]["CAPITAL_FORMULA_DIVIDER"] = $val["PROPERTY_CAPITAL_FORMULA_DIVIDER_VALUE"];

						} else{
						  $item["PROPS"]["CAP_CALC_BY_FORMULA"] = 0;
						}*/

					}
				}
				if($item["PROPS"]["EMITENT_ID"] && $arPeriods[$item["PROPS"]["EMITENT_ID"]]){
					$item["PERIODS"] = $arPeriods[$item["PROPS"]["EMITENT_ID"]];
/*					if($item["PROPS"]["EMITENT_ID"]==62423){
						echo "<pre  style='color:black; font-size:11px;'>";
						   print_r($item["PERIODS"]);
						   echo "</pre>";
					}*/
					//Переведем показатели за периоды в рубли если учет показателей в $
					foreach($item["PERIODS"] as $kp=>$ar_period) {
					 $exp_period = explode("-",$kp);

/*


						if(strpos($val["PROPERTY_VAL_ED_FIRST_VALUE"], "долл")!==false ){
						  	$ar_period["Прошлая капитализация"] = $ar_period["Прошлая капитализация"]*$USD_CURRENT_PERIOD;
						}*/

					 $arPrice = $this->arActionsPrices[$item["PROPS"]["SECID"]][$exp_period[1]][$exp_period[0]];
					 $curPrice = reset($arPrice);
					 $item["PERIODS"][$kp]["LEGALCLOSE"] = $curPrice["PRICE"];
					 //$item["PERIODS"][$kp]["Дивиденды"] = $this->arOblActionDividData[$item["PROPS"]["ISIN"]][$exp_period[1]][$exp_period[0]]?:0;

					 $arDividends = array();
					 $arDividends = $arActionsPages[$fields["CODE"]]["DIVIDENDS"];

					 $arSlideDivs = array();
					 $arSlideDivs = $arActionsPages[$fields["CODE"]]["SLIDING_DIVS"];

					 $kp_divs = $exp_period[0].'-'.$exp_period[1];
					 if(array_key_exists($kp_divs,$arSlideDivs)){
					 	$item["PERIODS"][$kp]["Скользящие Дивиденды"] = $arSlideDivs[$kp_divs]["Скользящие дивиденды"];
					 	$item["PERIODS"][$kp]["Див. доходность"] = $arSlideDivs[$kp_divs]["Див. доходность %"];
					} else {
					 	$item["PERIODS"][$kp]["Скользящие Дивиденды"] = 0;
					 	$item["PERIODS"][$kp]["Див. доходность"] = 0;
					}

					}




					$item["PERIODS"] = parent::calculatePeriodData($item);
					$item["PERIODS"] = $this->setFormulaCapForPeriods($item["PERIODS"],$item["PROPS"]);

				}

				$item["DYNAM"] = self::setDynamParams($item);

				if($USE_DOLLARS){
				$item["DYNAM"]["Прибыль"] = $item["DYNAM"]["Прибыль"]*$USD_CURRENT;
				}

				foreach($item["PERIODS"] as $kper=>$vper){
					$ar_kper = explode("-",$kper);
				if($USE_DOLLARS){

						$CUR_COURSE = $arHistoryCourses[str_replace("-KVARTAL","",$kper)];

					 $end_data = self::get_dates_of_quarter(intval($ar_kper[0]), intval($ar_kper[1]), 'd.m.Y');
					 $prevDayPeriod = new DateTime($end_data["end"]);

					 $prevDayPeriod->modify("-1 day");
			       $USD_CURRENT_PERIOD = getCBPrice("USD", $prevDayPeriod->format("d/m/Y"));

						//echo "Имя=".$item["NAME"]." ПрСкольз=".$vper["Прибыль за год (скользящая)"]." курс=".$CUR_COURSE." Mult=".$vper["Прибыль за год (скользящая)"]*$CUR_COURSE."<br>";
/*					 if($item["PROPS"]["EMITENT_ID"]==62443){
					 	echo "per=".$kper." курс=".$CUR_COURSE." Выр.Скольз (до расчета)=".$item["PERIODS"][$kper]["Выручка за год (скользящая)"];
					 }*/

						$item["PERIODS"][$kper]["Прибыль за год (скользящая)"] = $item["PERIODS"][$kper]["Прибыль за год (скользящая)"]*$CUR_COURSE;
						$item["PERIODS"][$kper]["Выручка за год (скользящая)"] = $item["PERIODS"][$kper]["Выручка за год (скользящая)"]*$CUR_COURSE;

/*					 if($item["PROPS"]["EMITENT_ID"]==62443){
					 	echo " Выр.Скольз (после расчета)=".$item["PERIODS"][$kper]["Выручка за год (скользящая)"]."<br>";
					 }*/


						$course_date = new DateTime($arCouseKvartalDates[$ar_kper[0]].'.'.$ar_kper[1]);

						//$CUR_COURSE = getCBPrice("USD", $course_date->format("d/m/Y"));
						//echo "Имя=".$item["NAME"]." Активы=".$vper["Активы"]." курс=".$CUR_COURSE." Mult=".$vper["Активы"]*$CUR_COURSE."<br>";
/*						if($item["PROPS"]["EMITENT_ID"]==62423){
							echo "<pre  style='color:black; font-size:11px;'>";
							   echo "<pre  style='color:black; font-size:11px;'>";
							      print_r($arCompanyPages[$item["COMPANY"]["NAME"]]);
							      echo "</pre>";
							   echo "</pre>";
						}*/

						//Если тек.элемент - депозитарная расписка, да еще и в долларах - капу не перемножаем, т.к. она уже расчитана функцией на кроне
						if(intval($arCompanyPages[$item["COMPANY"]["NAME"]]["PROPERTY_CAPITAL_ACTION_CNT_VALUE"])<=0){
						$item["PERIODS"][$kper]["Прошлая капитализация"] = $vper["Прошлая капитализация"]*$USD_CURRENT_PERIOD;
						}
						$item["PERIODS"][$kper]["Оборотные активы"] = $vper["Оборотные активы"]*$USD_CURRENT_PERIOD;
						$item["PERIODS"][$kper]["Активы"] = $item["PERIODS"][$kper]["Активы"]*$USD_CURRENT_PERIOD;
						$item["PERIODS"][$kper]["Собственный капитал"] = $item["PERIODS"][$kper]["Собственный капитал"]*$USD_CURRENT_PERIOD;

/*	 						echo "<pre  style='color:black; font-size:11px;'>";
							   print_r($item["PERIODS"]);
							   echo "</pre>";*/
						  //	$item["PROPS"]["ISSUECAPITALIZATION_DOL"] = $item["PROPS"]["ISSUECAPITALIZATION"]/$USD_CURRENT;
				}


				}// foreach $item["PERIODS"]

				//+Заполним пустые периоды данными из каждого последнего заполненного
				$arDebugAdded = array();
				$existUf_Data = array();
				//Для выведенных из обращения уберем ошибку в капе
            if(strpos($item["PROPS"]["ISSUECAPITALIZATION"],"E")!==false && !empty($item["PROPS"]["HIDEN"])){
					$item["PROPS"]["ISSUECAPITALIZATION"]=0;
					}

						$curKv = $this->arMonthToKvartal[date('n')];
		 				$cutYear = date("Y");

						$current_preiod = $curKv."-".$cutYear."-KVARTAL";
						$item['CURRENT_PERIOD'] = $current_preiod;

						for($y=2011;$y<=date("Y");$y++){
						 for($kv = ($y==2011?4:1); $kv<=4; $kv++){
						 	$periodKey = $kv."-".$y."-KVARTAL";

						  if(array_key_exists($periodKey,$item["PERIODS"]) ){//Если период есть в массиве - запишем его в переменную для копировния на случай пустого следующего периода
							 $existUf_Data = $item["PERIODS"][$periodKey];

/*						 						 global $USER;
                      					      $rsUser = CUser::GetByID($USER->GetID());
                      					      $arUser = $rsUser->Fetch();
                      					      if($arUser["LOGIN"]=="freud"){
                      				          if($GLOBALS["USER"]->IsAdmin() && $item["PROPS"]["EMITENT_ID"]==62423){
                      				          	echo $periodKey."+<pre  style='color:black; font-size:11px;'>";
                      				          	   print_r($existUf_Data);
                      				          	   echo "</pre>";
                      				          }
                      				      }*/

						  } else { //Если периода нет в массиве - добавим его из переменной $existUf_Data по условию ниже
							// $quartCompare = $this->QuartalsCompare($current_preiod,$periodKey);
							// $quartCompareStart = $this->QuartalsCompare($arStartQuart,$periodKey);
						  	if(count($existUf_Data) && empty($item["PROPS"]["HIDEN"]) && $current_preiod!==$periodKey ){  //Если не текущий период и не делистинг то скопируем полностью из прошлого периода
						  	//if(count($existUf_Data) && empty($item["PROPS"]["HIDEN"]) && $quartCompare<0 ){  //Если не текущий период и не делистинг то скопируем полностью из прошлого периода
							  $item["PERIODS"][$periodKey] = $existUf_Data;
							  $item["PERIODS"][$periodKey]["FROM_LAST_PERIOD"] = "Y";
							  $item['ADDED_PERIODS'][] = $periodKey;
						  	} elseif(count($existUf_Data) && empty($item["PROPS"]["HIDEN"]) && $current_preiod==$periodKey  ) { //Если текущий период и не делистинг - возьмем капу текущую, а остальное из прошлого периода
						  	//} elseif(count($existUf_Data) && empty($item["PROPS"]["HIDEN"]) && $quartCompare==0  ) { //Если текущий период и не делистинг - возьмем капу текущую, а остальное из прошлого периода
							  $arCurPeriod = explode("-",$periodKey);
							  $arCurPeriod = $arCurPeriod[0]."-".$arCurPeriod[1];
							  $item["PERIODS"][$periodKey] = $existUf_Data;
							  $item["PERIODS"][$periodKey]["Прошлая капитализация"] = $item["PROPS"]["ISSUECAPITALIZATION"];
							  $item["PERIODS"][$periodKey]["PE"] = $item["DYNAM"]["PE"];
							  $item["PERIODS"][$periodKey]["P/Equity"] = $item["DYNAM"]["P/Equity"];
							  $item["PERIODS"][$periodKey]["P/S"] = round(($item["PROPS"]["ISSUECAPITALIZATION"]/1000000)/$existUf_Data["Выручка за год (скользящая)"],2);
							  $item["PERIODS"][$periodKey]["P/B"] = round(($item["PROPS"]["ISSUECAPITALIZATION"]/1000000)/$existUf_Data["Активы"],2);
					  		  $item["PERIODS"][$periodKey]["Скользящие Дивиденды"] = $arSlideDivs[$arCurPeriod]["Скользящие дивиденды"];  //Получаем из таблицы скользящих дивов
					 		  $item["PERIODS"][$periodKey]["Див. доходность"] = $arSlideDivs[$arCurPeriod]["Див. доходность %"];

							  $item["PERIODS"][$periodKey]["FROM_LAST_PERIOD"] = "Y";
							  $item['ADDED_PERIODS'][] = $periodKey;
						  	}
						  }
						 }
						}



/*					  						 global $USER;
					  					      $rsUser = CUser::GetByID($USER->GetID());
					  					      $arUser = $rsUser->Fetch();
					  					      if($arUser["LOGIN"]=="freud"){
					  				          if($GLOBALS["USER"]->IsAdmin() && $item["PROPS"]["EMITENT_ID"]==62423){  //Акрон
					  				          	echo "Под админом <pre  style='color:black; font-size:11px;'>";
					  				          	   print_r($item);
					  				          	   echo "</pre>";
					  				          }
					  				      }*/

				//-Заполним пустые периоды данными из каждого последнего заполненного

				$arItems[] = $item;
				//$arItemsSECID[$item["SECID"]] = $item;
			}

			//$cache->set($cacheId, $arItems);
			$cache->endDataCache($arItems);
			//$cache->set($cacheId.'_secid', $arItemsSECID);
		}
/*		echo "<pre  style='color:black; font-size:11px;'>";
           print_r($this->arActionsPrices);
      echo "</pre>";*/
		$this->arOriginalsItems = $arItems;
		$this->arOriginalsItemsKeySECID = $arItemsSECID;
		$this->count = count($arItems);
	}

//Определяет последний рабочий день месяца по дате последнего дня месяца
public function getMonthEndWorkday($date){
    $dt = new DateTime($date);

    if($dt->format("N")==7){
        $dt->modify("-2 days");
    } elseif($dt->format("N")==6){
        $dt->modify("-1 days");
    }

    if(isWeekEndDay($dt->format("d.m.Y"))){
        $finded = false;
        while(!$finded){
            $dt->modify("-1 days");
            if(!isWeekEndDay($dt->format("d.m.Y"))){
                $finded = true;
            }
        }
    }


    return $dt->format("d.m.Y");
}

  //Формирует фильтр дат по последним рабочим дням кварталов до 2014 года
   public function setDatesFilterForMoexActionsData(){
		 $this->MoexActionsDataFilter = array();
		 $nowYear = intval(date('Y'));
		 $firstYear = 2014;
			$arKvartalToMonth = array(
				1 => array("03","31"),
				2 => array("06","30"),
				3 => array("09","30"),
				4 => array("12","31"),
			);

		$this->MoexActionsDataFilter=array('LOGIC' => 'OR');
		for($i=$nowYear;$i>=$firstYear;$i--){
			for($j=1;$j<=count($arKvartalToMonth);$j++){
			 $findDate = $this->getMonthEndWorkday($arKvartalToMonth[$j][1].'.'.$arKvartalToMonth[$j][0].'.'.$i);
			 //Для случая с пустой ценой добавим еще выборку день назад
			 $dayMinus = new DateTime($findDate);
			 $dayMinus = $dayMinus->modify('-1 days')->format('d.m.Y');
			 $findDateMinusOneDay = $this->getMonthEndWorkday($dayMinus);

			 $this->MoexActionsDataFilter[] = array(
			'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($findDate)
		    );
			 $this->MoexActionsDataFilter[] = array(
			'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($findDateMinusOneDay)
		    );
			}
		}
		    //Добавим текущую (вчерашнюю) цену в фильтр
			 $now = $this->getMonthEndWorkday((new DateTime)->modify('-1 days')->format('d.m.Y'));
			 $this->MoexActionsDataFilter[] = array(
			'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($now)
		    );
	}

  	//таблица вывода
	public function getTable($page=1){
		$this->setFilter();
		$this->sortItems();

		$end = $page*$this->perPage;
		$start = $end-$this->perPage;

		$arItems = array();

		if($page==1){
			$arItems = $this->arItemsSelected;
		}

		for($i=$start;$i<$end;$i++){
			if($this->arItems[$i]){
				$arItems[] = $this->arItems[$i];
			}
		}

		$shows = $this->perPage*$page+count($this->arItemsSelected);
		if($shows>$this->count+count($this->arItemsSelected)){
			$shows = $this->count;
		}

		return array(
			"total_items" => $this->count+count($this->arItemsSelected),
			"cur_page" => $page,
			"shows" => $shows,
			"count_page" => ceil($this->count/$this->perPage),
			"items" => $arItems,
			"access" => $this->havePayAccess,
			//"selected" => $this->getSelectedItems()
		);
	}

		//сортировка элементов
	private function sortItems(){
	    $negArr = [];
	    $posArr = [];
		  if(!empty($_POST['s_field']) && $_POST['s_field']=='P/E'){
		  	die();
		  }
	    foreach ($this->arItems as $item) {
	        if($item["DYNAM"]["PE"] <= 0 || !$item["DYNAM"]["PE"]) {
                $negArr[] = $item;
            } else {
	            $posArr[] = $item;
            }

        }
        usort($posArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
                return 0;
            }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? 1 : -1;
        });

        usort($negArr, function($a,$b){
            if ($a["DYNAM"]["PE"] == $b["DYNAM"]["PE"]) {
                return 0;
            }
            if(!$a["DYNAM"]["PE"]) {
                return -1;
            }
            return ($a["DYNAM"]["PE"] > $b["DYNAM"]["PE"]) ? -1 : 1;
        });

        $this->arItems = array_merge($posArr, $negArr);
	}

		//избранные элементы
	public function getSelectedItems(){
		$arData = array();
		if($_COOKIE["id_arr_shares_cookie"]){
			$tmp = json_decode($_COOKIE["id_arr_shares_cookie"]);
			foreach($tmp as $item){
				$t = explode("...", $item);
				$arData[$t[0]] = $t[1];
			}
		}

		return $arData;
	}



	private function setDynamParams($item){
		$return = array();

		//new Просад = ( 1 - old Просад / LASTPRICE ( LEGALCLOSE) ) х 100
		if($item["PROPS"]["PROP_PROSAD"] && $item["PROPS"]["LASTPRICE"]){
			$return["Просад"] =  round((1 - $item["PROPS"]["PROP_PROSAD"]/$item["PROPS"]["LASTPRICE"])*100, 2);
		}

		if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_MONTH"]) {
            $return["MONTH_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_MONTH"]) - 1) * 100), 2);
        }
        if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_ONE_YEAR"]) {
            $return["YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_ONE_YEAR"]) - 1) * 100), 2);
        }
        if ($item["PROPS"]["LASTPRICE"] && $item["PROPS"]["QUOTATIONS_THREE_YEAR"]) {
            $return["THREE_YEAR_INCREASE"] = round(((floatval($item["PROPS"]["LASTPRICE"]) / floatval($item["PROPS"]["QUOTATIONS_THREE_YEAR"]) - 1) * 100), 2);
        }

		//new Target = ( old Target / LASTPRICE ( LEGALCLOSE) - 1 ) x 100
		if($item["PROPS"]["PROP_TARGET"] && $item["PROPS"]["LASTPRICE"]){
			$return["Таргет"] =  round(($item["PROPS"]["PROP_TARGET"]/$item["PROPS"]["LASTPRICE"]-1)*100, 2);
		}

		//Цена лота
		if($item["PROPS"]["LOTSIZE"] && $item["PROPS"]["LASTPRICE"]){
			$return["Цена лота"] = $item["PROPS"]["LOTSIZE"]*$item["PROPS"]["LASTPRICE"];
		}

		/*
		1) если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
		2) если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
		3) если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
		4) если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
		*/
		//Прибыль
		if($item["PERIODS"]){
			$tmp = sort_nested_arrays($item["PERIODS"], array('PERIOD_YEAR' => 'desc', 'PERIOD_VAL' => 'desc'));
			$lastItem = $tmp[0];

			$curKvartal = intval((date('n')-1)/4)+1;
			$availYear = date("Y")-1;

			try {
			$lastDate = DateTime::createFromFormat('m.Y', $lastItem["PERIOD_VAL"].".".$lastItem["PERIOD_YEAR"]);
			$lastAvail = DateTime::createFromFormat('m.Y', $curKvartal.".".$availYear);

			if($lastDate && $lastAvail){
			if($lastDate->format("U") >= $lastAvail->format("U")){

				$year = DateTime::createFromFormat('Y', $lastItem["PERIOD_YEAR"]);
				$prevYear = clone $year;
				$prevYear->modify("-1year");

				if ($lastItem["PERIOD_VAL"]==1){
					//если у нас последний имеющийся период это 1 квартал - то ПРИБЫЛЬ = прибыль за 1 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 1 квартал 2016 года
					if(
						isset($item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["1-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] =
							$item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["1-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["1-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==2){
					//если у нас последний имеющийся период это 2 квартал - то ПРИБЫЛЬ = прибыль за 2 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 2 квартал 2016 года
					if(
						isset($item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["2-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] =
							$item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["2-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["2-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==3){
					//если у нас последний имеющийся период это 3 квартал как сейчас - то ПРИБЫЛЬ = прибыль за 3 квартал 2017 года + Прибыль за 4 квартал 2016 года - Прибыль за 3 квартал 2016 года
					if(
						isset($item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]) &&
						isset($item["PERIODS"]["3-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] =
							$item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Прибыль"]+
							$item["PERIODS"]["4-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"]-
							$item["PERIODS"]["3-".$prevYear->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["3-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				} elseif ($lastItem["PERIOD_VAL"]==4){
					//если у нас последний имеющийся период это 4 квартал  - то ПРИБЫЛЬ = прибыль за 4 квартал
					if(
						isset($item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Прибыль"])
					){
						$return["Прибыль"] =
							$item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Прибыль"];
					}
                    $return["Собственный капитал"] = $item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["Собственный капитал"];
                    $return["AVEREGE_PROFIT"] = $item["PERIODS"]["4-".$year->format("Y")."-KVARTAL"]["AVEREGE_PROFIT"];
				}
			}
			}

			} catch (Exception $e) {
				?><pre><?print_r($lastItem)?></pre><?
				exit();
			}
		}

		//РЕ
		if($return["Прибыль"] && ($item["PROPS"]["ISSUECAPITALIZATION"] || $item["PROPS"]["ISSUECAPITALIZATION_DOL"])){
			$val = $item["PROPS"]["ISSUECAPITALIZATION"];
			if($item["PROPS"]["ISSUECAPITALIZATION_DOL"]){
				$val = $item["PROPS"]["ISSUECAPITALIZATION_DOL"];
			}
			$return["PE"] = round($val/$return["Прибыль"]/1000000, 1);
            $return["P/Equity"] = round($val/$return["Собственный капитал"]/1000000, 1);
            $return["PEG"] = round($return["PE"]/$return["AVEREGE_PROFIT"], 1);
		}
		//Дивиденды %
		if($item["PROPS"]["PROP_DIVIDENDY_2018"] && $item["PROPS"]["LASTPRICE"]){
			$return["Дивиденды %"] = round($item["PROPS"]["PROP_DIVIDENDY_2018"]/$item["PROPS"]["LASTPRICE"]*100, 2);
		}

		return $return;
	}


 }

