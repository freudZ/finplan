<? //Работа с датами для определения выходных дней

//выходные дни basicdata.ru - работает
function isWeekEndDay($date) {
	return isWeekEndDay_data_gov_ru($date); // - Сервис лежит с сентября 2020
	//return isWeekEndDay_basicdata($date);  - Данные по api до 2017 года
}


function isWeekEndDay_basicdata($date) {
	$date = new DateTime($date);

	$year = date($date->format("Y"));

	$arData = [];

	$obCache       = new CPHPCache();
	$cacheLifetime = 86400 * 30;
	//$cacheLifetime = 0;
	$cacheID   = 'items' . $year;
	$cachePath = '/weekend_days/';

	if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
		$arData = $obCache->GetVars();
	} elseif ($obCache->StartDataCache()) {
		$data = json_decode(file_get_contents("http://basicdata.ru/api/json/calend/"), true);

		foreach ($data as $item) {
			if ($item["data"] == $year) {
				for ($i = 1; $i <= 12; $i++) {
					$dt        = DateTime::createFromFormat("n.Y", $i . "." . $year);
					$monthDays = explode(",", $item[FormatDate("f", $dt->getTimestamp())]);

					foreach ($year[$i] as $day) {
						if ($day["isWorking"]==2) {
							$t = DateTime::createFromFormat("j.n.Y", $day . "." . $i . "." . $year);
							if ($t) {
								$arData[] = $t->format("d.m.Y");
							}
						} else {
							continue;
						}
					}
				}
				break;
			}
		}

		//Подключаем дополнительный календарь по РФ
		require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/holidays_calendars/rus_holiday.php";

		if (count($arHolidaysRus) > 0) {
			foreach ($arHolidaysRus as $dateH) {
				if (!in_array($dateH, $arData)) {
					$arData[] = $dateH;
				}
			}
		}

		$obCache->EndDataCache($arData);
	}

	if (in_array($date->format("d.m.Y"), $arData)) {
		return true;
	}
}


//выходные дни data.gov.ru - не работает
function isWeekEndDay_data_gov_ru($date) {
	$date = new DateTime($date);

	$year = date($date->format("Y"));

	$arData = [];

	$obCache       = new CPHPCache();
	$cacheLifetime = 86400 * 30;
	//$cacheLifetime = 0;
	$cacheID   = 'items' . $year;
	$cachePath = '/weekend_days/';

	if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
		$arData = $obCache->GetVars();
	} elseif ($obCache->StartDataCache()) {
		$data = json_decode(file_get_contents("https://data.gov.ru/api/json/dataset/7708660670-proizvcalendar/version/20151123T183036/content?access_token=2e079c90aad8bb1a5cd59ebea95f147b"), true);
		foreach ($data as $item) {
			if ($item["Год/Месяц"] == $year) {
				for ($i = 1; $i <= 12; $i++) {
					$dt        = DateTime::createFromFormat("n.Y", $i . "." . $year);
					$monthDays = explode(",", $item[FormatDate("f", $dt->getTimestamp())]);

					foreach ($monthDays as $day) {
						if (strpos($day, "*") !== false) { //Праздничные дни тоже считаем выходными, по этому прибиваем звездочку и добавляем в массив к выходным
							$day = str_replace("*", "", $day);
							//  continue;
						}
						if (strpos($day, "+") !== false) { //Перенесанные выходные дни тоже считаем выходными, по этому прибиваем плюс и добавляем в массив к выходным
							$day = str_replace("+", "", $day);
							//  continue;
						}
						$t = DateTime::createFromFormat("j.n.Y", $day . "." . $i . "." . $year);
						if ($t) {
							$arData[] = $t->format("d.m.Y");
						}
					}
				}
				break;
			}
		}

		//Подключаем дополнительный календарь по РФ
		require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/holidays_calendars/rus_holiday.php";

		if (count($arHolidaysRus) > 0) {
			foreach ($arHolidaysRus as $dateH) {
				if (!in_array($dateH, $arData)) {
					$arData[] = $dateH;
				}
			}
		}

		$obCache->EndDataCache($arData);
	}

	if (in_array($date->format("d.m.Y"), $arData)) {
		return true;
	}
}

//Выходные США
function isUsaWeekendDay($date) {

	$date          = new DateTime($date);
	$year          = date($date->format("Y"));
	$obCache       = new CPHPCache();
	$cacheLifetime = 86400 * 30;
	$cacheID       = 'items' . $year;
	$cachePath     = '/usa_weekend_days/';

	if (in_array($date->format("N"), [6, 7])) {
		return true;
	}

	$arData = [];
	if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
		$arData = $obCache->GetVars();
	} elseif ($obCache->StartDataCache()) {
		$data = json_decode(file_get_contents("https://www.federalpay.org/holidays/json/$year"), true);
		foreach ($data as $item) {
			$holidayDate = DateTime::createFromFormat('F j, Y', $item['date']);
			//$arData[] = (new DateTime($holidayDate))->format("d.m.Y");
			$arData[] = $holidayDate->format("d.m.Y");
		}

		//Подключаем дополнительный календарь
		include $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/holidays_calendars/usa_holiday.php";

		if (count($arHolidaysUsa) > 0) {
			foreach ($arHolidaysUsa as $dateH) {
				if (!in_array($dateH, $arData)) {
					$arData[] = $dateH;
				}
			}
		}

		$obCache->EndDataCache($arData);
	}

	return in_array($date->format("d.m.Y"), $arData) ? true : false;
}

 ?>