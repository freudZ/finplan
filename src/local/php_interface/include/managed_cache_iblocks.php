<? //Сброс кеша при интерактивных изменениях в инфоблоках
	AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "clearUserManagedIblockCache");
	AddEventHandler("iblock", "OnAfterIBlockElementAdd", "clearUserManagedIblockCache");
    function clearUserManagedIblockCache(&$arFields)
    {
		  $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

		  //Перестраиваем список клиентов для таргетирования баннеров, так же прописано в init.php
		  if($arFields["IBLOCK_ID"] == 11){
			  $CBanners = new CBanners();
			  $CBanners->setArraysClientsUsers(true);
			  unset($CBanners);
		  }
		//Сброс кеша отраслей РФ
		if($arFields["IBLOCK_ID"] == 72){
			$cache->clean("industries_rus_data");
		}

		  //Сброс кеша списка пресетов фильтров радара
		  if($arFields["IBLOCK_ID"] == 59){
					$cache->clean("getPresetList");
		  }

		  //Сброс кеша массива списочных свойств для фильтра акций и etf радара
		  if($arFields["IBLOCK_ID"] == 32 || $arFields["IBLOCK_ID"] == 53){
					$cache->clean("actions_rus_filters_data");
		  }

		  //Сброс кеша массива списка всех портфелей при изменении в инфоблоке 52
		  if($arFields["IBLOCK_ID"] == 52){
					$cache->clean("all_portfolio_list");
		  }
		  //Сброс кеша массива списка базовых валют для внесения в портфели в инфоблоке 54
		  if($arFields["IBLOCK_ID"] == 54){
					$cache->clean("base_currency_list");
		  }

		  //Сброс кеша массива списочных свойств для фильтра акций США
		  if($arFields["IBLOCK_ID"] == 55){
					$cache->clean("actions_usa_filters_data");
		  }

		  //Сброс кеша списка баннеров блога при интерактивном изменении в инфоблоке 45
		  if($arFields["IBLOCK_ID"] == 45){
					$CBanners = new CBanners();
					$CBanners->clearCache(false);  //без сброса списков пользователей клиентов и посмотревших вебинары
					unset($CBanners);
		  }
 		  //Сброс кеша списка правых баннеров при интерактивном изменении в инфоблоке 7
		  if($arFields["IBLOCK_ID"] == 7){
					$CBanners = new CBanners();
					$CBanners->clearCache(false);  //без сброса списков пользователей клиентов и посмотревших вебинары
					unset($CBanners);
		  }
		  //Сброс кеша таргетирования баннеров при интерактивном изменении в инфоблоке 71
		  if($arFields["IBLOCK_ID"] == 71){
					$CBanners = new CBanners();
					$CBanners->clearCache(true);  //co сбросом списка пользователей клиентов, посмотревших вебинары и индивидуалов
					unset($CBanners);
		  }

 		  //Сброс кеша количества тегов блога при интерактивном изменении в инфоблоке 5
		  if($arFields["IBLOCK_ID"] == 5){
					$cache->clean("blog_tags_count");
		  }
 		  //Сброс кеша списка контента для кастомных popup-ов при интерактивном изменении в инфоблоке 60
		  if($arFields["IBLOCK_ID"] == 60){
					$cache->clean("custom_popup_iblock_60_data");
		  }
 		  //Сброс кеша списка параметров нормализации шкал карты рынка при интерактивном изменении в инфоблоке 60
		  if($arFields["IBLOCK_ID"] == 61){ //classes/radar_base_mm.php getMarketMarNormalizeAxis()
					$cache->clean("market_map_normalize_axis");
		  }

 		  //Сброс кеша списка индикаторов для теханализа при изменении в инфоблоках индикаторов их шаблонов и их линий
		  if($arFields["IBLOCK_ID"] == 65 || $arFields["IBLOCK_ID"] == 66 || $arFields["IBLOCK_ID"] == 67){ //api/techanalysis/classes/CTechAnalysisApi.php getIndicatorsList()
					$cache->clean("TA_indicators_list_full");
					$cache->clean("TA_indicators_list_min");
					$cache->clean("TA_indicators_lines");
		  }
		 unset($cache);
    }

	AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", "clearUserManagedIblockCacheForSection");
	AddEventHandler("iblock", "OnAfterIBlockSectionAdd", "clearUserManagedIblockCacheForSection");
    function clearUserManagedIblockCacheForSection(&$arFields)
    {
		  $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

 		  //Сброс кеша списка параметров нормализации шкал карты рынка при интерактивном изменении в инфоблоке 60
		  if($arFields["IBLOCK_ID"] == 61){ //classes/radar_base_mm.php getMarketMarNormalizeAxis()
					$cache->clean("market_map_normalize_axis");
		  }
		 unset($cache);
    }

//Очищаем общий тегированный кеш выборки аналитики ГС при действиях с хайлоад блоком OnAfterAllChange
$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('', 'HLGSAnalyticsOnAfterAdd', 'OnAfterAllChange');
$eventManager->addEventHandler('', 'HLGSAnalyticsOnAfterUpdate', 'OnAfterAllChange');
$eventManager->addEventHandler('', 'HLGSAnalyticsOnAfterDelete', 'OnAfterAllChange');

function OnAfterAllChange(\Bitrix\Main\Entity\Event $event) {
  			global $CACHE_MANAGER;
			$CACHE_MANAGER->ClearByTag("analitycs_gs");
	$CacheFolder = $_SERVER["DOCUMENT_ROOT"]."/bitrix/cache/tagged_gs_analitycs/";
if (is_dir($CacheFolder)) {
    rmdir($CacheFolder);
}
		  //	$firephp = FirePHP::getInstance(true);
		  //	$firephp->fb(array("event"=>$event),FirePHP::LOG);
}

//Необходимые функции перестроения кеша
