<?  //Запись статистики авторизаций с сохранением IP адреса
use Bitrix\Highloadblock as HL;
CModule::IncludeModule('highloadblock');


  AddEventHandler("main", "OnAfterUserLogin", "OnAfterUserLoginHandler");

    function OnAfterUserLoginHandler(&$fields)
    { global $USER;
    	//if($GLOBALS["USER"]->IsAdmin()) return;

		$arGroups = $USER->GetUserGroupArray();
		if($USER->IsAdmin() || in_array(5, $arGroups)){
			return;
		}


		$hlblock   = HL\HighloadBlockTable::getById(35)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entity_data_class = $entity->getDataClass();

        // если логин не успешен то
        if($fields['USER_ID']<=0)
        {
            // счетчик неудавшихся попыток логина
            $_SESSION["AUTHORIZE_FAILURE_COUNTER"]++;

            // если количество неудачных попыток авторизации превышает 3, то
            if ($_SESSION["AUTHORIZE_FAILURE_COUNTER"]>3)
            {
                // ищем пользователя по логину
				    $data = array();
                $rsUser = CUser::GetByLogin($fields['LOGIN']);
                // и если нашли, то
                if ($arUser = $rsUser->Fetch())
                {
                	$data["UF_USER"] = $fields['USER_ID'];
						$data["UF_EMAIL"]       = $arUser["EMAIL"];
						$data["UF_LOGIN"]       = $fields['LOGIN'];
                }
				$date = new Bitrix\Main\Type\DateTime(date('Y-m-d H:i:s',time()),'Y-m-d H:i:s');
				$data["UF_LOGIN_DATE"]       = $date;
				$data["UF_IP"]       = $_SERVER["REMOTE_ADDR"];
				$data["UF_ERROR_AUTH_10"]       = "Y";
			  	$result = $entity_data_class::add($data);

            }
        } else { //Если успешная авторизация
		   	$data = array();
			  	$data["UF_USER"] = $fields['USER_ID'];
                $rsUser = CUser::GetByLogin($fields['LOGIN']);
                // и если нашли, то
                if ($arUser = $rsUser->Fetch())
                {
						$data["UF_EMAIL"]       = $arUser["EMAIL"];
						$data["UF_LOGIN"]       = $fields['LOGIN'];
                }
				$date = new Bitrix\Main\Type\DateTime(date('Y-m-d H:i:s',time()),'Y-m-d H:i:s');
				$data["UF_LOGIN_DATE"]       = $date;
				$data["UF_IP"]       = $_SERVER["REMOTE_ADDR"];
			  	$result = $entity_data_class::add($data);
        }
    }

 ?>