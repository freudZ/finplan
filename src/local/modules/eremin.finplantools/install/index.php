<?php

/* ������ ������������ ��� ������������� �������� �� ������� �����������������.
 */
IncludeModuleLangFile(__FILE__);

if (class_exists("eremin_finplantools")) {
	return;
}

Class eremin_finplantools extends CModule {
	var $MODULE_ID = "eremin.finplantools";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_GROUP_RIGHTS = "Y";


	function eremin_finplantools() {
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = mb_substr($path, 0, strlen($path) - strlen("/index.php"));
		include $path . "/version.php";

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = GetMessage("FPT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("FPT_MODULE_DESCRIPTION");

		$this->PARTNER_NAME = GetMessage("FPT_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("FPT_PARTNER_URL");
	}

	function DoInstall() {
		global $APPLICATION;

		if (!IsModuleInstalled("eremin.finplantools")) {
			$this->InstallFiles();
		}
		return true;
	}

	function InstallDB() {
		$eventManager = \Bitrix\Main\EventManager::getInstance();
	//$eventManager->registerEventHandler("main", "OnAdminIBlockElementEdit", "eremin.finplantools", "Eremin\\Finplantools\\fptPortfolioTabset", "getTabList");
	}

	function UnInstallDB() {
	}

	function DoUninstall() {
		$this->UnInstallFiles();
		return true;
	}

	function UnInstallEvents() {
		return true;
	}

	function InstallFiles() {
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true);
		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/eremin.finplantools/install/components/news.list/show_anketa", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/.default/components/bitrix/news.list/show_anketa", true);
		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/eremin.finplantools/install/components/news.list/show_anketa/ajax", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/.default/components/bitrix/news.list/show_anketa/ajax/", true);
		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/eremin.finplantools/install/components/news.list/show_anketa/css", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/.default/components/bitrix/news.list/show_anketa/css/", true);
	  //	CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/eremin.finplantools/install/components/news.list/show_anketa/lang/ru", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/.default/components/bitrix/news.list/show_anketa/lang/ru/", true);
		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/eremin.finplantools/install/components/news.list/show_anketa/lang/en", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/.default/components/bitrix/news.list/show_anketa/lang/en/", true);
		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/eremin.finplantools/install/public/anketa", $_SERVER["DOCUMENT_ROOT"] . "/anketa", true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/install/panel", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/panel/eremin.finplantools/", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/install/images", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/images", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/install/include", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/include", true, true);
		RegisterModule("eremin.finplantools");
		//CUrlRewriter::Add(array("CONDITION" => "#^/anketa/([a-zA-Z\\-]+)/(.*)#", "RULE" => "SECTION_CODE=\$1", "ID" => "eremin.finplantools", "PATH" => "/anketa/index.php"));

		return true;
	}

	function UnInstallFiles() {
		//DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/eremin.finplantools/install/components/bitrix/news.list/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/.default/components/bitrix/news.list/show_anketa");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/install/admin/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/eremin.finplantools/install/panel/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/panel/eremin.finplantools");
	  //	DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/news.list/show_anketa");
	  //	DeleteDirFilesEx("/anketa");
		DeleteDirFilesEx("/bitrix/include/eremin.finplantools");
		DeleteDirFilesEx("/bitrix/images/eremin.finplantools");
		UnRegisterModule("eremin.finplantools");
		//CUrlRewriter::Delete(array("PATH" => "/anketa/index.php"));
		return true;
	}

}

?>
