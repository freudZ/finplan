<?php
/**
 * Класс для работы с инфоблоком хранящим информацию о выполнении заданий на кроне
 *
 * [add longDescription]
 *
 * @category  [add CategoryName]
 * @package   [add PackageName]
 * @author    [add AuthorName] <[add AuthorEmail]>
 * @copyright 2019
 * @license   [add LicenseUrl]
 * @version   [add VersionNumber]
 */

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *     35 class fptСrontools
 *     41   function __construct($monitor_mode=false)
 *     48   function getCronList()
 *     60   function changeStartTime($taskId=0,$clear=false)
 *     72   function changeEndTime($taskId=0,$clear=false)
 *     83   function changeWorkTime($taskId=0,$microtime)
 *     88   function addErrorDescription($taskId=0, $error=)
 *
 * TOTAL FUNCTIONS: 6
 * (This index is automatically created/updated by the WeBuilder plugin "DocBlock Comments")
 *
 */

use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

class fptСrontools {

	public $CronIblockId = 51;
	public $arCronlist = array(); //Список зарегистрированных заданий крон на стороне хостинга. Заводятся вручную в инфоблоке


  function __construct($monitor_mode=false) {
  	if($monitor_mode){ //Для режима монитора инициализируем данные
	$this->getCronList();//Получаем список заданий из инфоблока
	}

  }

  public function getCronList(){
	 $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "IBLOCK_ID", "PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>IntVal($this->CronIblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array("SORT"=>"DESC"), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement()){
     $arFields = $ob->GetFields();
     $arFields["PROPERTIES"] = $ob->GetProperties();
	  $this->arCronlist[$arFields["ID"]] = $arFields;
    }
  }

  //Записывает время начала работы скрипта
  public function changeStartTime($taskId=0,$clear=false){
	  if($clear){
	  	$time = '';
	  } else{
		$time = ConvertTimeStamp(time(),"FULL");
	  }
  	  CIBlockElement::SetPropertyValuesEx($taskId, false, array("DATE_LAST_START" => $time));
	  $this->changeEndTime($taskId,true);
	  $this->changeWorkTime($taskId,0,true);
  }

  //Записывает время окончания работы скрипта
  public function changeEndTime($taskId=0,$clear=false){
	  if($clear){
	  	$time = '';
	  } else{
		$time = ConvertTimeStamp(time(),"FULL");
	  }
  	  CIBlockElement::SetPropertyValuesEx($taskId, false, array("DATE_LAST_END" => $time));

  }

  //Записывает время работы скрипта
  public function changeWorkTime($taskId=0,$microtime){
  	  CIBlockElement::SetPropertyValuesEx($taskId, false, array("WORK_TIME" => $microtime));
  }

  //Записываем ошибки работы скрипта
  public function addErrorDescription($taskId=0, $error=""){
	  CIBlockElement::SetPropertyValuesEx($taskId, false, array("WORK_ERRORS" => array("VALUE"=>$error, "DESCRIPTION"=>ConvertTimeStamp(time(),"FULL"))));
  }

}