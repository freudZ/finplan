<?
use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");
class fptTools {
  public $CompanyPagesIblockId = 29;
  public $CompanyIblockId = 26;
  public $ActionsIblockId = 32;
  public $MoexActionsDataHLIblockId = 24;
  public $EmitentId = '';
  public $arCompanyList = array(); //Список компаний с данными из 14 хайлоада
  public $arCompanyListFormula = array(); //Список компаний с данными формулы
  public $arActions = array(); //Список акций для списка компаний с ключем по emitent_id
  public $arActionsPrices = array(); //Список цен акций для списка компаний с ключем по SECID
  public $arActionsCurrentCapa = array(); //Список текущих капитализаций акций для списка компаний с ключем по SECID
  public $MoexActionsDataFilter= array();//Фильтр для получения цен акций по последним рабочим дням
  public $arCapa = array();//Массив для хранения рассчетной капы по компаниям и кварталам
  public $arMonthToKvartal = array(
				"01"=>1,
				"02"=>1,
				"03"=>1,
				"04"=>2,
				"05"=>2,
				"06"=>2,
				"07"=>3,
				"08"=>3,
				"09"=>3,
				"10"=>4,
				"11"=>4,
				"12"=>4,

			);
   function __construct(){
	  $this->setDatesFilterForMoexActionsData();

	}


//Определяет последний рабочий день месяца по дате последнего дня месяца
public function getMonthEndWorkday($date){
    $dt = new DateTime($date);

    if($dt->format("N")==7){
        $dt->modify("-2 days");
    } elseif($dt->format("N")==6){
        $dt->modify("-1 days");
    }

    if(isWeekEndDay($dt->format("d.m.Y"))){
        $finded = false;
        while(!$finded){
            $dt->modify("-1 days");
            if(!isWeekEndDay($dt->format("d.m.Y"))){
                $finded = true;
            }
        }
    }


    return $dt->format("d.m.Y");
}

/**
* Compute the start and end date of some fixed o relative quarter in a specific year.
* @param mixed $quarter  Integer from 1 to 4 or relative string value:
*                        'this', 'current', 'previous', 'first' or 'last'.
*                        'this' is equivalent to 'current'. Any other value
*                        will be ignored and instead current quarter will be used.
*                        Default value 'current'. Particulary, 'previous' value
*                        only make sense with current year so if you use it with
*                        other year like: get_dates_of_quarter('previous', 1990)
*                        the year will be ignored and instead the current year
*                        will be used.
* @param int $year       Year of the quarter. Any wrong value will be ignored and
*                        instead the current year will be used.
*                        Default value null (current year).
* @param string $format  String to format returned dates
* @return array          Array with two elements (keys): start and end date.
*/
public static function get_dates_of_quarter($quarter = 'current', $year = null, $format = null)
{
    if ( !is_int($year) ) {
       $year = (new DateTime)->format('Y');
    }
    $current_quarter = ceil((new DateTime)->format('n') / 3);
    switch (  strtolower($quarter) ) {
    case 'this':
    case 'current':
       $quarter = ceil((new DateTime)->format('n') / 3);
       break;

    case 'previous':
       $year = (new DateTime)->format('Y');
       if ($current_quarter == 1) {
          $quarter = 4;
          $year--;
        } else {
          $quarter =  $current_quarter - 1;
        }
        break;

    case 'first':
        $quarter = 1;
        break;

    case 'last':
        $quarter = 4;
        break;

    default:
        $quarter = (!is_int($quarter) || $quarter < 1 || $quarter > 4) ? $current_quarter : $quarter;
        break;
    }
    if ( $quarter === 'this' ) {
        $quarter = ceil((new DateTime)->format('n') / 3);
    }
    $start = new DateTime($year.'-'.(3*$quarter-2).'-1 00:00:00');
    $end = new DateTime($year.'-'.(3*$quarter).'-'.($quarter == 1 || $quarter == 4 ? 31 : 30) .' 23:59:59');

    return array(
        'start' => $format ? $start->format($format) : $start,
        'end' => $format ? $end->format($format) : $end,
    );
}

public function checkCurrentQuarter($year,$kv){

  if(intval($year)==intval(date('Y')) && intval($kv)==intval($this->arMonthToKvartal[date('m')]) ){
  	return true;
  } else {
  	return false;
  }
}

	//Записывает исторические данные по прошлой капитализации для указанной компании в OblCompanyData  HL 14
	public function saveOneCompanyHistory($company_id, $cname=''){
	   $arCompanies = $this->getCompanyList();
		$SECID = $this->arActions[$company_id];
		$arPrices = $this->arActionsPrices[$SECID];
		CModule::IncludeModule("highloadblock");
		$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();


		$arCompany = $arCompanies[$this->arCnames[$company_id]];
		ksort($arCompany["OblCompanyData"]);
		$arResult = '';
        Global $USER;
        if($USER->IsAdmin()){
            $firephp = FirePHP::getInstance(true);
            $firephp->fb($arPrices,FirePHP::LOG);
        }
		 foreach($arCompany["OblCompanyData"] as $year=>&$arKvartals){
				 for($i=1; $i<=4; $i++){
							 if(!array_key_exists($i,$arKvartals)){
							  $arKvartals[$i] = array();
							 }
				 }

			 ksort($arKvartals);

			 foreach($arKvartals as $kv=>$kvData){
			  //Теперь капа переписывается вне зависимости существует она или вновь добавляется
					//if(!$kvData["UF_DATA"]["Прошлая капитализация"] && count($kvData["UF_DATA"])>2){ //Если есть исторические данные
					if(count($kvData["UF_DATA"])>2){ //Если есть исторические данные
					//if(!$kvData["UF_DATA"]["Прошлая капитализация"]){ //Если есть исторические данные
						  		      $priceKvData = $arPrices[$year][$kv];
										$bd_elem =key($priceKvData);
										$lastCap = round(($arPrices[$year][$kv][$bd_elem]["CAPITALIZATION"]/1000000), 2);
                            	$kvData["UF_DATA"]["Прошлая капитализация"] = $lastCap;

					 if($lastCap>0){ //Если прошлая капа>0  - то сохраняем
					  $jsonData = json_encode($kvData["UF_DATA"]);
					  $act_type = '';
					  if(intval($kvData["ID"])>0){
				  				$entityClass::update($kvData["ID"], array(
                                "UF_DATA" => $jsonData
                            ));
								$act_type = 'Обновлено';
						} else {
							  $entityClass::add(array(
                                "UF_DATA" => $jsonData,
										  "UF_YEAR" => $year,
										  "UF_KVARTAL" => $kv,
										  "UF_COMPANY" => $company_id,
                            ));
							  $act_type = 'Добавлено';
						}

						$arResult .= ' '.$year.'-'.$kv.'-капа='.$lastCap.' '.$act_type.'; ';

					 }


					}

			 }//foreach $arKvartals



		 } // foreach $arCompany["OblCompanyData"]

		 if(empty($arResult)) $arResult = 'Обновление данных не требуется';
		return array('status'=>$arResult);
	}

	//Получает из $fptTools->arActionsPrices текущую капитализацию
	public function GetCurCap($SECID){
		return  $this->arActionsCurrentCapa[$SECID];
	}


	//Записывает текущую капитализацию
	public function saveCurrentCap($company_id, $curCapa = 0){
		$iblockId = 32;//Акции
		$arSelect = Array("ID", "NAME", "IBLOCK_ID");
      $arFilter = Array("IBLOCK_ID"=>IntVal($iblockId), "PROPERTY_EMITENT_ID"=>intval($company_id), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
      $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
      if($ob = $res->GetNextElement()){
       $arFields = $ob->GetFields();
       $arProps = $ob->GetProperties();
      }
		$error = '';
		if(is_array($arFields) && count($arFields)>0){
			 try{
			 	CIBlockElement::SetPropertyValues($arFields["ID"], $iblockId, $curCapa, "ISSUECAPITALIZATION");

			 }catch (Exception $e){

    			$error = var_dump("Capitalization: " . $e->getMessage());
			}
		}


		return array('result'=>(!empty($error)?$error:'Текущая капитализация записана'),'status'=>(!empty($error)?'error':'success'));

	}


  //Формирует фильтр дат по последним рабочим дням кварталов до 2014 года
   public function setDatesFilterForMoexActionsData(){
		 $this->MoexActionsDataFilter = array();
		 $nowYear = intval(date('Y'));
		 $firstYear = 2014;
			$arKvartalToMonth = array(
				1 => array("03","31"),
				2 => array("06","30"),
				3 => array("09","30"),
				4 => array("12","31"),
			);

		$this->MoexActionsDataFilter=array('LOGIC' => 'OR');
		for($i=$nowYear;$i>=$firstYear;$i--){
			for($j=1;$j<=count($arKvartalToMonth);$j++){
			 $findDate = $this->getMonthEndWorkday($arKvartalToMonth[$j][1].'.'.$arKvartalToMonth[$j][0].'.'.$i);
			 //Для случая с пустой ценой добавим еще выборку день назад
			 $dayMinus = new DateTime($findDate);
			 $dayMinus = $dayMinus->modify('-1 days')->format('d.m.Y');
			 $findDateMinusOneDay = $this->getMonthEndWorkday($dayMinus);

			 $this->MoexActionsDataFilter[] = array(
			'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($findDate)
		    );
			 $this->MoexActionsDataFilter[] = array(
			'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($findDateMinusOneDay)
		    );
			}
		}
		    //Добавим текущую (вчерашнюю) цену в фильтр
			 $now = $this->getMonthEndWorkday((new DateTime)->modify('-1 days')->format('d.m.Y'));
			 $this->MoexActionsDataFilter[] = array(
			'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($now)
		    );
	}

	//Возвращает список страниц компаний, у которых задан знаменатель для расчета капы по формуле
	public function getCompanyList(){
		$arCompanyList = array();
		$arCompanyId = array();
		//Получаем страницы компаний по фильтру с количеством акций
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_CAPITAL_FORMULA_DIVIDER", "PROPERTY_CAPITAL_ACTION_CNT");
		$arFilter = Array("IBLOCK_ID"=>IntVal($this->CompanyPagesIblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_CAPITAL_ACTION_CNT"=>false, "!NAME"=>false);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->Fetch()){
		  if(empty($ob["NAME"])) continue;
		 $arCompanyList[$ob["NAME"]] = array("COMPANY_PAGE_ID"=>$ob["ID"], "ACTIONS_CNT"=>$ob["PROPERTY_CAPITAL_ACTION_CNT_VALUE"], "CAPITAL_FORMULA_DIVIDER"=>$ob["PROPERTY_CAPITAL_FORMULA_DIVIDER_VALUE"]);
		 }

		//Получаем компании по фильтру из страниц компаний
		$arSelect = Array("ID", "NAME", "IBLOCK_ID");
      $arFilter = Array("IBLOCK_ID"=>IntVal($this->CompanyIblockId), "NAME"=>array_keys($arCompanyList), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
      $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
      while($ob = $res->Fetch()){
		 $arCompanyList[$ob["NAME"]]["COMPANY_ID"] = $ob["ID"];
		 $arCompanyId[$ob["NAME"]] = $ob["ID"];
		 $this->arCnames[$ob["ID"]] = $ob["NAME"];
		 $arCompanyListFormula[$ob["ID"]] = $arCompanyList[$ob["NAME"]];

       //$arProps = $ob->GetProperties();
      }

		//Полуучаем из HL данные по компаниям в json и сразу декодируем в массив
		$hlblock   = HL\HighloadBlockTable::getById(14)->fetch();
		$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
		$entityClass = $entity->getDataClass();

		$res = $entityClass::getList(array(
				"filter" => array("=UF_COMPANY">array_values($arCompanyId)),
				"select" => array(
					"*"
				),
				"order" => array(
					"ID" => "DESC"
				)
			));
		while($item = $res->fetch()){
		 $cname = array_search($item["UF_COMPANY"], $arCompanyId, true);
		// echo $cname;
		 if(empty($cname) ) continue;
		 $arCompanyList[$cname]["OblCompanyData"][$item["UF_YEAR"]][$item["UF_KVARTAL"]] = array("ID"=>$item["ID"],"UF_DATA"=>json_decode($item["UF_DATA"], true));
		}
		$this->arCompanyList = $arCompanyList;
		$this->arCompanyListFormula = $arCompanyListFormula;
		$this->arActions = $this->getActions(array_values($arCompanyId));

		$this->calcCapaToArray(array_values($this->arActions));

		return $arCompanyList;
	}

	//Возвращает список акций по id эмитента
   public function getActions($emitentId){
	 $arActions = array();
	 $arSelect = Array("ID", "NAME", "IBLOCK_ID","PROPERTY_SECID", "PROPERTY_EMITENT_ID");
    $arFilter = Array("IBLOCK_ID"=>IntVal($this->ActionsIblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_EMITENT_ID"=>$emitentId);
    $res = CIBlockElement::GetList(Array("PROPERTY_EMITENT_ID"=>"ASC"), $arFilter, false, false, $arSelect);
    while($ob = $res->Fetch()){
	  $arActions[$ob["PROPERTY_EMITENT_ID_VALUE"]] = $ob["PROPERTY_SECID_VALUE"];
	  $this->getActionPrices($ob["PROPERTY_SECID_VALUE"]);
    }
	 return $arActions;
   }

	//Получает список цен всех акций
	public function getActionPrices($SECID=''){
	 $arResult = array();
			//TODO кешировать "cache"=>array("ttl"=>3600)  доступно с версии 16.5.9, сделать после обновления
			$hlblock   = HL\HighloadBlockTable::getById($this->MoexActionsDataHLIblockId)->fetch();
			$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
			$entityClass = $entity->getDataClass();

		if(!empty($SECID)){
		  $arFilter[] = array('LOGIC' => 'AND', array('UF_ITEM'=>$SECID));
		  $this->MoexActionsDataFilter = $arFilter[]= $this->MoexActionsDataFilter;
		} else {
		  $this->MoexActionsDataFilter = $this->MoexActionsDataFilter;
		}



			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array(
					"UF_ITEM", "UF_DATE", "UF_CLOSE", "UF_ITEM"
				),
				//"limit" => 20,
				"order" => array(
					"ID" => "DESC"
				)
			));

			$arMonthToKvartal = array(
				"01"=>1,
				"02"=>1,
				"03"=>1,
				"04"=>2,
				"05"=>2,
				"06"=>2,
				"07"=>3,
				"08"=>3,
				"09"=>3,
				"10"=>4,
				"11"=>4,
				"12"=>4,

			);


			while($item = $res->fetch()){
			 $end = new DateTime($item["UF_DATE"]);
			 $year = $end->format('Y');
			 $month = $arMonthToKvartal[$end->format('m')];
			 $date = $end->format('d.m.Y');
			 $this->arActionsPrices[$item["UF_ITEM"]][$year][$month][$date]["PRICE"] = round($item["UF_CLOSE"],2);
			}

		  return $arResult;
	}

  //Рассчитывает капу в массив класса для последующей записи в БД
  public function calcCapaToArray($arSECID=array()){
	  $this->arCapa = array();

	  foreach($arSECID as $SECID){
	  	$arActionPrice = &$this->arActionsPrices[$SECID];

		$keyAction = array_search($SECID, $this->arActions, true);
		$arCompanyFormula = $this->arCompanyListFormula[$keyAction];
		//$keyCompany = array_search($keyAction, array_column($arr, 'COMPANY_ID'));
		if(is_array($arActionPrice)){
		  foreach($arActionPrice as $year=>&$arKvartals){
		  	foreach($arKvartals as $kvartal=>&$blockDate){
			 $bd_elem =key($blockDate);
			 $blockDate[$bd_elem]["COMPANY_ID"] = $keyAction;
			 $blockDate[$bd_elem]["ACTIONS_CNT"] = $arCompanyFormula["ACTIONS_CNT"];
			 $blockDate[$bd_elem]["CAPITAL_FORMULA_DIVIDER"] = $arCompanyFormula["CAPITAL_FORMULA_DIVIDER"];
			 //$blockDate[$bd_elem]["CAPITALIZATION"] = round($arCompanyFormula["ACTIONS_CNT"]*$blockDate[$bd_elem]["PRICE"]/$arCompanyFormula["CAPITAL_FORMULA_DIVIDER"],2);
			 $blockDate[$bd_elem]["CAPITALIZATION"] = round($arCompanyFormula["ACTIONS_CNT"]*$blockDate[$bd_elem]["PRICE"],2);
			  if($this->checkCurrentQuarter($year,$kvartal)==true){
			  	$this->arActionsCurrentCapa[$SECID] = $blockDate[$bd_elem]["CAPITALIZATION"];
			  }

		  	}
		  }
		}
	  }
  }


/**
 * Обновляет годовые дивиденды для акции США
 *
 * @param  int   $ElementId
 * @param  int   $IblockId
 * @param  string   $code
 * @param  float   $LastPrice
 *
 * @access public
 */
public function updateYearDividends($ElementId, $IblockId, $code, $LastPrice=0){
		  CModule::IncludeModule("iblock");
        $hlblock = HL\HighloadBlockTable::getById(32)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $dataClass= $entity->getDataClass();

      $res = $dataClass::getList(array(
          "filter" => array(
              "UF_ITEM" => $code,
          ),
			 "limit" => 1,
          "select" => array("ID", "UF_ITEM", "UF_DATA"),
			 //"cache" => array("ttl" => 3600)
      )
		);

	  if ($hlItem = $res->fetch()) {


	  	if(!empty($hlItem["UF_DATA"])){
		  $divData = json_decode($hlItem["UF_DATA"], true);
		  if(!count($divData)) return;
			$endDate = '';  // Дата окончания годового периода
			$startDate = '';  // Дата начала годового периода
			$now = new DateTime(date());

			//Поиск окончания диапазона
			foreach($divData as $coupon){
			  $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
			  if($now<$closeDate){
			  	$endDate = $closeDate;
			  } else if($now>=$closeDate && empty($endDate)){
				  $endDate = $closeDate;
				 break;
			  }
			}
          //Если найденная первая запись не попадает в текущий год то используем для старта текущую дату
            if(intval($endDate->format('Y'))<intval($now->format('Y'))){
                $endDate = $now;
            }
			
		//$startDate = (clone $endDate)->modify('-1 year');
			$startDate = (clone $endDate)->modify("-1 years")->modify("+1 month");

			$YearSum = 0;
			$YearPrc = 0;
			$FutureYearSum = 0;
			$FutureYearPrc = 0;
			$Description = '';
			foreach($divData as $coupon){
			  $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
			  if($closeDate<=$endDate && $closeDate>=$startDate){

				 $textDiv = explode(" (",$coupon["Дивиденды"]); //Выкидываем пересчитанную в рубли сумму в скобках
				 $YearSum += floatval($textDiv[0]);
			 //	 $Description .= (strlen($Description)>0?'; ':'').$coupon["Дивиденды"].' руб. ('.$closeDate->format('d.m.Y').' - '.($now<$closeDate?'рекомендовано':'выплачено').')';
				 $Description .= (strlen($Description)>0?'; ':'').$textDiv[0].' от '.$closeDate->format('d.m.Y').' - '.($now<$closeDate?'рекомендовано':'выплачено');
			  }

			}
			//Считаем будущие дивиденды
			foreach($divData as $coupon){
			  $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
			  if($closeDate>$now ){
				 $textDiv = explode(" (",$coupon["Дивиденды"]); //Выкидываем пересчитанную в рубли сумму в скобках
				 $FutureYearSum += floatval($textDiv[0]);
			  }

			}

			$Description = ""."(".$Description.")";

			if(floatval($LastPrice)>0){
			$YearPrc = round($YearSum/$LastPrice*100,2);
			$FutureYearPrc = round($FutureYearSum/$LastPrice*100,2);
			}
		 try {

		   CIBlockElement::SetPropertyValues($ElementId, $IblockId, $YearSum, "DIVIDENDS");
			CIBlockElement::SetPropertyValues($ElementId, $IblockId, $Description, "SOURCE");
			//Записываем будущие дивиденды
		   CIBlockElement::SetPropertyValues($ElementId, $IblockId, $FutureYearSum, "FUTURE_DIVIDENDS");
			CIBlockElement::SetPropertyValues($ElementId, $IblockId, $FutureYearPrc, "FUTURE_DIVIDENDS_PRC");

		  } catch (Exception $e) {
        $err = $e->getMessage();
		  echo "<pre  style='color:black; font-size:11px;'>";
		     print_r($err);
		     echo "</pre>";
    }
		}
	  }
	  unset($res, $dataClass, $entity, $hlblock, $closeDate);
	 }//updateYearDividends

/**
 * Обновляет годовые дивиденды для акции РФ
 *
 * @param  int   $ElementId
 * @param  int   $IblockId
 * @param  string   $code
 * @param  float   $LastPrice
 *
 * @access public
 */
public function updateYearDividendsRus($ElementId, $IblockId, $code, $LastPrice=0){
		  CModule::IncludeModule("iblock");
        $hlblock = HL\HighloadBlockTable::getById(21)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $dataClass= $entity->getDataClass();

      $res = $dataClass::getList(array(
          "filter" => array(
              "UF_ITEM" => $code,
          ),
			 "limit" => 1,
          "select" => array("ID", "UF_ITEM", "UF_DATA"),
			 //"cache" => array("ttl" => 3600)
      )
		);

	  if ($hlItem = $res->fetch()) {


	  	if(!empty($hlItem["UF_DATA"])){

		  $divData = json_decode($hlItem["UF_DATA"], true);
		  if(!count($divData)) return;
			$endDate = '';  // Дата окончания годового периода
			$startDate = '';  // Дата начала годового периода
			$now = new DateTime(date());

			//Поиск окончания диапазона
/*			foreach($divData as $coupon){
			  $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
			  if($now<$closeDate){
			  	$endDate = $closeDate;
			  } else if($now>=$closeDate && empty($endDate)){
				  $endDate = $closeDate;
				 break;
			  }
			}*/

			//$startDate = (clone $endDate)->modify('-1 year');

			//$YearSum = 0;
			//$YearPrc = 0;
			$FutureYearSum = 0;
			$FutureYearPrc = 0;
			//$Description = '';
/*			foreach($divData as $coupon){
			  $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
			  if($closeDate<=$endDate && $closeDate>=$startDate){

				 $textDiv = explode(" (",$coupon["Дивиденды"]); //Выкидываем пересчитанную в рубли сумму в скобках
				 $YearSum += floatval($textDiv[0]);
			 //	 $Description .= (strlen($Description)>0?'; ':'').$coupon["Дивиденды"].' руб. ('.$closeDate->format('d.m.Y').' - '.($now<$closeDate?'рекомендовано':'выплачено').')';
				 $Description .= (strlen($Description)>0?'; ':'').$textDiv[0].' от '.$closeDate->format('d.m.Y').' - '.($now<$closeDate?'рекомендовано':'выплачено');
			  }

			}*/
			//Считаем будущие дивиденды
			foreach($divData as $coupon){
			  $closeDate = new DateTime($coupon["Дата закрытия реестра"]);
			  if($closeDate>$now ){
				 $textDiv = explode(" (",$coupon["Дивиденды"]); //Выкидываем пересчитанную в рубли сумму в скобках
				 $FutureYearSum += floatval($textDiv[0]);
			  }

			}

			//$Description = ""."(".$Description.")";

			if(floatval($LastPrice)>0){
			//$YearPrc = round($YearSum/$LastPrice*100,2);
			$FutureYearPrc = round($FutureYearSum/$LastPrice*100,2);
			}
		 try {

		   //CIBlockElement::SetPropertyValues($ElementId, $IblockId, $YearSum, "DIVIDENDS");
			//CIBlockElement::SetPropertyValues($ElementId, $IblockId, $Description, "SOURCE");
			//Записываем будущие дивиденды
		   CIBlockElement::SetPropertyValues($ElementId, $IblockId, $FutureYearSum, "FUTURE_DIVIDENDS");
			CIBlockElement::SetPropertyValues($ElementId, $IblockId, $FutureYearPrc, "FUTURE_DIVIDENDS_PRC");

		  } catch (Exception $e) {
        $err = $e->getMessage();
		  echo "<pre  style='color:black; font-size:11px;'>";
		     print_r($err);
		     echo "</pre>";
    }
		}
	  }

	 }//updateYearDividends

}