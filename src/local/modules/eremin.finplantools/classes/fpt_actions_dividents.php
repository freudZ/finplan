<?php

use Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");
class fptActionsDividends {
	public $CompanyPagesIblockId = 29;
	public $CompanyIblockId = 26;
	public $ActionsIblockId = 32;
	public $MoexActionsDataHLIblockId = 24;
	public $oblActionDividDataHLIblockId = 21; //Данные по дивидендам
	public $EmitentId = '';
	public $arCompanyList = array(); //Список компаний с данными из 14 хайлоада
	public $arActions = array(); //Список акций для списка компаний с ключем по emitent_id
	public $arActionsPrices = array(); //Список цен акций для списка компаний с ключем по SECID
	public $arActionsStartQuartals = array(); //Список стартовых кварталов для списка компаний с ключем по SECID
	public $arActionsStartQuartalsCompany = array(); //Список ID компаний для списка компаний с ключем по SECID
	public $arActionsPricesRaw = array(); //Список цен акций для списка компаний с ключем по SECID и датами без указания периодов
	public $MoexActionsDataFilter = array(); //Фильтр для получения цен акций по последним рабочим дням
	public $arActionsDiv = array(); //Массив для хранения рассчитанных данных по ценам акций на начало и конец периодов и скользящих дивидендов
	public $arActionsSlideDiv = array(); //Массив для хранения скользящих дивидендов
	public $arIsinTicker = array(); // Соответствия ISIN кода и TICKER для выбора из таблицы цен акций
	public $arSECIDId = array(); // Соответствия кода ценной бумаги и TICKER для выбора из таблицы цен акций
	public $arMonthToKvartal = array(
		"01" => 1,
		"02" => 1,
		"03" => 1,
		"04" => 2,
		"05" => 2,
		"06" => 2,
		"07" => 3,
		"08" => 3,
		"09" => 3,
		"10" => 4,
		"11" => 4,
		"12" => 4,

	);
	public $arMonthToKvartalStart = array(
		"01" => 1,
		"04" => 2,
		"07" => 3,
		"10" => 4,
	);
	public $arQuartDateRanges = array(); //Диапазоны дат для кварталов
	public $arSlidingDivDateRanges = array(); //Диапазоны дат для суммирования скользящих дивидендов. Текущий квартал в итерации + 3 квартала назад.
	public $arDatesForDividQuartBack = array(); //Заполняется вычисленными датами начала и окончания (минус три квартала от текущего) с ключем по вычисляемому кварталу (текущему в итерации)

	function __construct() {
		$this->setDatesFilterForMoexActionsData();
		$this->setSlidingDivRanges();
		$this->setIsinTicker();
		$this->getActionDivs();
		$this->getActionPrices();
		$this->calculateDivs();
	}

	//Заполняет массив соответствий ISIN кода и TICKER для выбора из таблицы цен акций $arIsinTicker
	public function setIsinTicker() {
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_ISIN", "PROPERTY_SECID", "PROPERTY_EMITENT_ID");
		$arFilter = Array("IBLOCK_ID" => IntVal($this->ActionsIblockId), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->Fetch()) {
			$this->arIsinTicker[$ob["PROPERTY_ISIN_VALUE"]] = $ob["PROPERTY_SECID_VALUE"];
			$this->arSECIDId[$ob["PROPERTY_EMITENT_ID_VALUE"]] = $ob["PROPERTY_SECID_VALUE"];
		}

	 //Получим все компании являющиеся эмитентами акций в массиве arSECIDId
	  $arTmp = array();
	  $arSelect1 = Array("ID", "NAME", "IBLOCK_ID");
     $arFilter1 = Array("IBLOCK_ID"=>IntVal(26), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>array_keys($this->arSECIDId));
     $res1 = CIBlockElement::GetList(Array(), $arFilter1, false, false, $arSelect1);
     while($ob1 = $res1->Fetch()){
		 $arTmp[$this->arSECIDId[$ob1["ID"]]] = $ob1["ID"];
      }
	  $this->arSECIDId=$arTmp;
	  unset($arTmp);

	}

	//Рассчитывает скользящие дивиденды для акций
	public function calculateDivs() {
		$graph = new MoexGraph();
		foreach ($this->arActionDivs as $code => $dates) { //Цикл по акциям

			$nowYear = intval(date('Y'));
			$realFromDate = (new DateTime($graph->getForActionDateFrom($this->arIsinTicker[$code])));//->format('d.m.Y');
			$firstYear = 2011;
			$firstKv = 1;
/*			if(intval($realFromDate->format('Y'))>$firstYear){ //выключено иначе остаются только некоторые кварталы с явными выплатами без перекрытия соседними
				$firstYear = intval($realFromDate->format('Y'));
				$firstKv = $this->arMonthToKvartal[$realFromDate->format('m')];
			}*/
			$debug_cnt = 0;
			for ($i = $nowYear; $i >= $firstYear; $i--) {
				for ($j = $firstKv; $j<5; $j++) {

					if ($this->checkCurrentQuarter($i, $j)==false) {
						$dStart = new DateTime($this->arSlidingDivDateRanges[$j . '-' . $i]["start"]); //начало диапазона текущего в итерации квартала
						$dEnd = new DateTime($this->arSlidingDivDateRanges[$j . '-' . $i]["end"]); //конец диапазона текущего в итерации квартала
						foreach ($dates as $date => $div) { //Цикл по выплатам дивидендов
							$divDate = new DateTime($date);

							if ($divDate >= $dStart && $divDate <= $dEnd) {

/*          if($code=="US29355E2081"){
echo "calculateDivs<pre  style='color:black; font-size:11px;'>";
print_r("квартал: ".$j."-".$i." Диапазон: ".$this->arSlidingDivDateRanges[$j.'-'.$i]["start"]."-".$this->arSlidingDivDateRanges[$j.'-'.$i]["end"]." Сумма= [Накопленное в квартале ".$this->arActionsSlideDiv[$code][$j.'-'.$i]["Скользящие дивиденды"]."] + [Попадающая выплата ".$div."] = ".($this->arActionsSlideDiv[$code][$j.'-'.$i]["Скользящие дивиденды"]+floatval($div)));
echo "</pre>";
}*/
                   	$this->arActionsSlideDiv[$code][$j . '-' . $i]["Скользящие дивиденды"] = $this->arActionsSlideDiv[$code][$j . '-' . $i]["Скользящие дивиденды"] + floatval($div);

							}

						} //Цикл по выплатам дивидендов

/*                      if($code=="US29355E2081"){
$price = $this->arActionsPricesRaw[ $this->arIsinTicker[$code] ][$pDateStart];
echo "calculateDivs<pre  style='color:black; font-size:11px;'>";
echo "Дата начала: ".$dStart->format('d.m.Y')." Цена:".$price;
echo "</pre>";
}*/

						$pDateStart = $this->getMonthFirstWorkday($dStart->format('d.m.Y'));


/*         if($this->arIsinTicker[$code] = "ENPL"){
//echo $realFromDate.">".$pDateStart."=".($this->arActionsPricesRaw[ $this->arIsinTicker[$code]][$realFromDate])."<br>";
if($debug_cnt==0)
{  echo "<pre  style='color:black; font-size:11px;'>";
print_r($this->arActionsPricesRaw[ $this->arIsinTicker[$code]]);
echo "</pre>";}
$debug_cnt++;
}*/
					$pDateEnd = $this->getMonthEndWorkday($dEnd->format('d.m.Y'));
						if ($realFromDate >= (new DateTime($pDateStart)) && $realFromDate<=(new DateTime($pDateEnd))) {
							$pDateStart = $realFromDate->format('d.m.Y');
						}
						$price = $this->arActionsPricesRaw[$this->arIsinTicker[$code]][$pDateStart];
						$priceEnd = $this->arActionsPricesRaw[$this->arIsinTicker[$code]][$pDateEnd];
						$this->arActionsSlideDiv[$code][$j . '-' . $i]["TICKER"] = $this->arIsinTicker[$code];
						$this->arActionsSlideDiv[$code][$j . '-' . $i]["Цена акции на начало диапазона"] = $price;
						$this->arActionsSlideDiv[$code][$j . '-' . $i]["Цена акции на конец диапазона"] = $priceEnd;
						$this->arActionsSlideDiv[$code][$j . '-' . $i]["Дата начала диапазона"] = $pDateStart;
						$this->arActionsSlideDiv[$code][$j . '-' . $i]["Дата окончания диапазона"] = $pDateEnd;
						if (!array_key_exists("Скользящие дивиденды", $this->arActionsSlideDiv[$code][$j . '-' . $i])) {
							$this->arActionsSlideDiv[$code][$j . '-' . $i]["Скользящие дивиденды"] = 0;
						}
						if (floatval($price) > 0) {
							$this->arActionsSlideDiv[$code][$j . '-' . $i]["Див. доходность %"] = ($this->arActionsSlideDiv[$code][$j . '-' . $i]["Скользящие дивиденды"] / $price) * 100;
						} else {
							$this->arActionsSlideDiv[$code][$j . '-' . $i]["Див. доходность %"] = 0;
						}

					}

				} //кварталы
			} //Годы
		} //Цикл по акциям

	}

	//Проверяет является ли переданный квартал текущим
	public function checkCurrentQuarter($year, $kv) {

		if (intval($year) == intval(date('Y')) && intval($kv) == intval($this->arMonthToKvartal[date('m')])) {
			return true;
		} else {
			return false;
		}
	}

	//Расчитывает диапазоны дат для суммирования скользящих дивидендов. Текущий квартал в итерации + 3 квартала назад.
	public function setSlidingDivRanges() {
		$this->arSlidingDivDateRanges = array();
		$nowYear = intval(date('Y'));
		$firstYear = 2011;
		for ($i = $nowYear; $i >= $firstYear; $i--) {
			for ($j = 1; $j <= 4; $j++) {

				if ($j < 4) {
					$start = $this->arQuartDateRanges[$i - 1][$j + 1]["start"]; // Начальная дата диапазона
					$end = $this->arQuartDateRanges[$i][$j]["end"]; // Конечная дата диапазона
				} else {
					$start = $this->arQuartDateRanges[$i][$j - 3]["start"]; // Начальная дата диапазона
					$end = $this->arQuartDateRanges[$i][$j]["end"]; // Конечная дата диапазона
				}
				$this->arSlidingDivDateRanges[$j . '-' . $i] = array("start" => $start, "end" => $end);
			}
		}
	}

	//Получает и суммирует дивиденды по акциям
	public function getActionDivs() {
		global $APPLICATION;

		CModule::IncludeModule("highloadblock");

		//TODO кешировать "cache"=>array("ttl"=>3600)  доступно с версии 16.5.9, сделать после обновления
		$hlblock = HL\HighloadBlockTable::getById($this->oblActionDividDataHLIblockId)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
		$entityClass = $entity->getDataClass();

		$arFilter = array('!=UF_DATA' => '[]');
		$res = $entityClass::getList(array(
			"filter" => $arFilter,
			"select" => array("UF_DATA", "UF_ITEM"),
			//"limit" => 20,
			"order" => array(
				"ID" => "DESC"
			)
		));
		while ($item = $res->fetch()) {
			$arData = json_decode($item["UF_DATA"], true);

			foreach ($arData as $arPeriod) {
				$end = new DateTime($arPeriod["Дата закрытия реестра"]);
				$year = $end->format('Y');
				$month = $this->arMonthToKvartal[$end->format('m')];
				if (strpos($arPeriod["Дивиденды"], "(") !== false) {
					$arPeriod["Дивиденды"] = explode("(", $arPeriod["Дивиденды"]);
					if (count($arPeriod["Дивиденды"]) > 1) {
						$arPeriod["Дивиденды"] = str_replace(" руб.)", "", $arPeriod["Дивиденды"][1]);

					}
				}

/*        if($item["UF_ITEM"]=="US29355E2081"){
echo "getActionDivs<pre  style='color:black; font-size:11px;'>";
print_r($arPeriod["Дата закрытия реестра"]."=".$arPeriod["Дивиденды"]);
echo "</pre>";
}*/

				$this->arActionDivs[$item["UF_ITEM"]][$arPeriod["Дата закрытия реестра"]] = $arPeriod["Дивиденды"];
			}
		}

	}

	//Получает список цен всех акций
	public function getActionPrices($SECID = '') {
		$arResult = array();

		$cache = Bitrix\Main\Data\Cache::createInstance();
		if ($cache->initCache(86400, 'fpt_getActionPrices', '/fpt_getActionPrices/')) {
			$result = $cache->getVars();
		} elseif ($cache->startDataCache()) {
			$result = array();
			//TODO кешировать "cache"=>array("ttl"=>3600)  доступно с версии 16.5.9, сделать после обновления
			$hlblock = HL\HighloadBlockTable::getById($this->MoexActionsDataHLIblockId)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();

			if (!empty($SECID)) {
				$arFilter[] = array('LOGIC' => 'AND', array('UF_ITEM' => $SECID));
				$this->MoexActionsDataFilter = $arFilter[] = $this->MoexActionsDataFilter;
			} else {
				$this->MoexActionsDataFilter = $this->MoexActionsDataFilter;
			}

			$res = $entityClass::getList(array(
				"filter" => $this->MoexActionsDataFilter,
				"select" => array(
					"UF_ITEM", "UF_DATE", "UF_CLOSE"
				),
				"order" => array(
					"UF_DATE" => "ASC"
				)
			));

			while ($item = $res->fetch()) {
				$end = new DateTime($item["UF_DATE"]);
				$year = $end->format('Y');
				$month = $this->arMonthToKvartal[$end->format('m')];
				$date = $end->format('d.m.Y');
				$keyDate = $end->format('d') <= 15 ? 'start' : 'end';
				$result['arActionsPrices'][$item["UF_ITEM"]][$year][$month][$keyDate][$date] = round($item["UF_CLOSE"], 2);
				$result['arActionsPricesRaw'][$item["UF_ITEM"]][$date] = round($item["UF_CLOSE"], 2);
			}

			//Получим выборку цен и дат старта обращения для всех компаний и добавим
			Global $DB;
			$results = $DB->Query("SELECT MIN(`UF_DATE`) as `UF_DATE`,`UF_ITEM`,`UF_CLOSE` FROM `hl_moex_actions_data` WHERE `UF_CLOSE`>0 GROUP BY `UF_ITEM` ORDER BY `UF_DATE` ASC");
			$start_array = array();

			while ($row = $results->Fetch()) {
				$end = new datetime($row['UF_DATE']);
				$year = $end->format('Y');
				$month = $this->arMonthToKvartal[$end->format('m')];
				$date = $end->format('d.m.Y');
				$keyDate = $end->format('d') <= 15 ? 'start' : 'end';
				$result['arActionsPrices'][$item["UF_ITEM"]][$year][$month][$keyDate][$date] = round($row["UF_CLOSE"], 2);
				$result['arActionsPricesRaw'][$row["UF_ITEM"]][$date] = round($row["UF_CLOSE"], 2);
				$result['arActionsStartQuartals'][$row["UF_ITEM"]] = $month."-".$year;
				$result['arActionsStartQuartalsCompany'][$this->arSECIDId[$row["UF_ITEM"]]] = $month."-".$year;
			}

			if ($isInvalid) {
				$cache->abortDataCache();
			}
/*			$result['arActionsPrices'] = $this->arActionsPrices;
			$result['arActionsPricesRaw'] = $this->arActionsPricesRaw;
			$result['arActionsStartQuartals'] = $this->arActionsStartQuartals;*/

			$cache->endDataCache($result);
		}
		$this->arActionsPrices = $result['arActionsPrices'];
		$this->arActionsPricesRaw = $result['arActionsPricesRaw'];
		$this->arActionsStartQuartals = $result['arActionsStartQuartals'];
		$this->arActionsStartQuartalsCompany = $result['arActionsStartQuartalsCompany'];
		return $arResult;
	}

	//Вычисляет и кеширует дивиденды по акциям и суммированные по кварталам в обратном порядке (текущий квартал минус три вкартала назад)
	public function setDividendsOfQuartBack() {
		global $APPLICATION;

		$arDatesForDividQuartBack = array();

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$cacheId = "actions_rplus_dividendsBack_data";
		$cacheTtl = 86400 * 7;

		if ($cache->read($cacheTtl, $cacheId)) {
			$this->arOblActionDividData = $cache->get($cacheId);
		} else {
			CModule::IncludeModule("highloadblock");

			//TODO кешировать "cache"=>array("ttl"=>3600)  доступно с версии 16.5.9, сделать после обновления
			$hlblock = HL\HighloadBlockTable::getById($this->oblActionDividDataHLIblockId)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();

			$arFilter = array('!=UF_DATA' => '[]');
			$res = $entityClass::getList(array(
				"filter" => $arFilter,
				"select" => array("UF_DATA", "UF_ITEM"),
				//"limit" => 20,
				"order" => array(
					"ID" => "DESC"
				)
			));
			while ($item = $res->fetch()) {
				$arData = json_decode($item["UF_DATA"], true);

				foreach ($arData as $arPeriod) {
					$end = new DateTime($arPeriod["Дата закрытия реестра"]);
					$year = $end->format('Y');
					$month = $this->arMonthToKvartal[$end->format('m')];
					$this->arOblActionDividData[$item["UF_ITEM"]][$year][$month] += floatval($arPeriod["Дивиденды"]);
				}
			}
			$cache->set($cacheId, $this->arOblActionDividData);
		}

	}

/**
 * Compute the start and end date of some fixed o relative quarter in a specific year.
 * @param mixed $quarter  Integer from 1 to 4 or relative string value:
 *                        'this', 'current', 'previous', 'first' or 'last'.
 *                        'this' is equivalent to 'current'. Any other value
 *                        will be ignored and instead current quarter will be used.
 *                        Default value 'current'. Particulary, 'previous' value
 *                        only make sense with current year so if you use it with
 *                        other year like: get_dates_of_quarter('previous', 1990)
 *                        the year will be ignored and instead the current year
 *                        will be used.
 * @param int $year       Year of the quarter. Any wrong value will be ignored and
 *                        instead the current year will be used.
 *                        Default value null (current year).
 * @param string $format  String to format returned dates
 * @return array          Array with two elements (keys): start and end date.
 */
	public static function get_dates_of_quarter($quarter = 'current', $year = null, $format = null) {
		if (!is_int($year)) {
			$year = (new DateTime)->format('Y');
		}
		$current_quarter = ceil((new DateTime)->format('n') / 3);
		switch (strtolower($quarter)) {
			case 'this':
			case 'current':
				$quarter = ceil((new DateTime)->format('n') / 3);
				break;

			case 'previous':
				$year = (new DateTime)->format('Y');
				if ($current_quarter == 1) {
					$quarter = 4;
					$year--;
				} else {
					$quarter = $current_quarter - 1;
				}
				break;

			case 'first':
				$quarter = 1;
				break;

			case 'last':
				$quarter = 4;
				break;

			default:
				$quarter = (!is_int($quarter) || $quarter < 1 || $quarter > 4) ? $current_quarter : $quarter;
				break;
		}
		if ($quarter === 'this') {
			$quarter = ceil((new DateTime)->format('n') / 3);
		}
		$start = new DateTime($year . '-' . (3 * $quarter - 2) . '-1 00:00:00');
		$end = new DateTime($year . '-' . (3 * $quarter) . '-' . ($quarter == 1 || $quarter == 4 ? 31 : 30) . ' 23:59:59');

		return array(
			'start' => $format ? $start->format($format) : $start,
			'end' => $format ? $end->format($format) : $end,
		);
	}

//Определяет последний рабочий день месяца по дате последнего дня месяца
	public function getMonthEndWorkday($date) {
		$dt = new DateTime($date);

		if ($dt->format("N") == 7) {
			$dt->modify("-2 days");
		} elseif ($dt->format("N") == 6) {
			$dt->modify("-1 days");
		}

		if (isWeekEndDay($dt->format("d.m.Y"))) {
			$finded = false;
			while (!$finded) {
				$dt->modify("-1 days");
				if (!isWeekEndDay($dt->format("d.m.Y"))) {
					$finded = true;
				}
			}
		}

		return $dt->format("d.m.Y");
	}

//Определяет первый рабочий день месяца по дате начала месяца
	public function getMonthFirstWorkday($date) {
		$dt = new DateTime($date);

		if ($dt->format("N") == 7) {
			$dt->modify("+1 days");
		} elseif ($dt->format("N") == 6) {
			$dt->modify("+2 days");
		}

		if ($this->isFirstWorkDay($dt->format("d.m.Y"))) {
			$finded = false;
			while (!$finded) {
				$dt->modify("+1 days");
				if (!$this->isFirstWorkDay($dt->format("d.m.Y"))) {
					$finded = true;
				}
			}
		}

		return $dt->format("d.m.Y");
	}

//выходные дни
	function isFirstWorkDay($date) {
		$date = new DateTime($date);

		$year = date($date->format("Y"));

		$arData = [];

		$obCache = new CPHPCache();
		$cacheLifetime = 86400 * 30;
		//$cacheLifetime = 0;
		$cacheID = 'items' . $year;
		$cachePath = '/first_quart_work_days/';

		if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
			$arData = $obCache->GetVars();
		} elseif ($obCache->StartDataCache()) {
			$data = json_decode(file_get_contents("https://data.gov.ru/api/json/dataset/7708660670-proizvcalendar/version/20151123T183036/content?access_token=2e079c90aad8bb1a5cd59ebea95f147b"), true);
			foreach ($data as $item) {
				if ($item["Год/Месяц"] == $year) {
					for ($i = 1; $i <= 12; $i++) {
						$dt = DateTime::createFromFormat("n.Y", $i . "." . $year);
						$monthDays = explode(",", $item[FormatDate("f", $dt->getTimestamp())]);

						foreach ($monthDays as $day) {
							if (strpos($day, "*") !== false && strpos($day, "+") !== false) {
								continue;
							}
							$t = DateTime::createFromFormat("j.n.Y", $day . "." . $i . "." . $year);
							if ($t) {
								$arData[] = $t->format("d.m.Y");
							}
						}
					}
					break;
				}
			}
			$obCache->EndDataCache($arData);
		}

		if (in_array($date->format("d.m.Y"), $arData)) {
			return true;
		}
	}

	//Формирует фильтр дат по последним рабочим дням кварталов до 2014 года
	public function setDatesFilterForMoexActionsData() {
		$this->MoexActionsDataFilter = array();
		$nowYear = intval(date('Y'));
		$firstYear = 2011;
		$arKvartalToMonth = array(
			1 => array("03", "31"),
			2 => array("06", "30"),
			3 => array("09", "30"),
			4 => array("12", "31"),
		);
		$arKvartalToMonthFirst = array(
			1 => array("01", "01"),
			2 => array("04", "01"),
			3 => array("07", "01"),
			4 => array("10", "01"),
		);
		$this->MoexActionsDataFilter = array('LOGIC' => 'OR');
		for ($i = $nowYear; $i >= $firstYear; $i--) {
			for ($j = 1; $j <= count($arKvartalToMonth); $j++) {
				$findDate = $this->getMonthEndWorkday($arKvartalToMonth[$j][1] . '.' . $arKvartalToMonth[$j][0] . '.' . $i);
				$findfirstDate = $this->getMonthFirstWorkday($arKvartalToMonthFirst[$j][1] . '.' . $arKvartalToMonthFirst[$j][0] . '.' . $i);
				//Для случая с пустой ценой добавим еще выборку день назад
				$dayMinus = new DateTime($findDate);
				$dayMinus = $dayMinus->modify('-1 days')->format('d.m.Y');
				$findDateMinusOneDay = $this->getMonthEndWorkday($dayMinus);

				//Для случая с пустой ценой добавим еще выборку день вперед
				$dayPlus = new DateTime($findfirstDate);
				$dayPlus = $dayPlus->modify('+1 days')->format('d.m.Y');
				$findDatePlusOneDay = $this->getMonthFirstWorkday($dayPlus);

				$this->MoexActionsDataFilter[] = array(
					'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($findDate)
				);
				$this->MoexActionsDataFilter[] = array(
					'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($findDateMinusOneDay)
				);
				$this->MoexActionsDataFilter[] = array(
					'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($findfirstDate)
				);
				$this->MoexActionsDataFilter[] = array(
					'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($findDatePlusOneDay)
				);

				//Заполняем начальную и конечные даты поквартально
				$this->arQuartDateRanges[$i][$j] = $this->get_dates_of_quarter($j, $i, 'd.m.Y');

			}
		}
		//Добавим текущую (вчерашнюю) цену в фильтр
		$now = $this->getMonthEndWorkday((new DateTime)->modify('-1 days')->format('d.m.Y'));
		$this->MoexActionsDataFilter[] = array(
			'=UF_DATE' => \Bitrix\Main\Type\DateTime::createFromUserTime($now)
		);
	}

}
