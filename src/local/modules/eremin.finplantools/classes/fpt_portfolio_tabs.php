<?
 use Bitrix\Main\Context,
	  Bitrix\Highloadblock as HL;
 use \Bitrix\Iblock\PropertyEnumerationTable;
 use Bitrix\Main\Grid\Options as GridOptions;
 use Bitrix\Main\UI\PageNavigation;
 CModule::IncludeModule("iblock");
 CModule::IncludeModule("highloadblock");

class fptPortfolioTabset
{

	public function getTabList($elementInfo)
	{

		$request = Context::getCurrent()->getRequest();
		$addTabs = $elementInfo['ID'] > 0
			&& $elementInfo['IBLOCK']['ID'] == 52
			&& (!isset($request['action']) || $request['action'] != 'copy');

		return $addTabs ? [
			[
				"DIV"   => 'actives_table',
				"SORT"  => PHP_INT_MAX,
				"TAB"   => 'Активы',
				"TITLE" => 'Активы в портфеле',
			],
		] : null;
	}

	public function showTabContent($div, $elementInfo, $formData){
			Global $APPLICATION, $DB;
			require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
			$list_id = "tbl_fpt_actives_list"; // ID таблицы
			$grid_options = new GridOptions($list_id);
			/**
			 * Навигация
			 */
			$nav_params = $grid_options->GetNavParams();
			//$nav = new PageNavigation('tbl_fpt_actives_list');
			// создаем объект пагинации
			$nav = new \Bitrix\Main\UI\PageNavigation($list_id);

			echo "<pre  style='color:black; font-size:11px;'>";
            print_r($_POST);
            echo "</pre>";


			/**
			 * Параметры запроса
			 */
			 $qyery_order = array();
    		//$qyery_order = array("UF_ADD_DATE"=>"asc");
			//$grid_options->SetSorting("UF_ADD_DATE","asc");
			/**
			 * Сортировка
			 *
			 */
		 	$sort = $grid_options->GetSorting(['sort' => ['UF_ADD_DATE' => 'asc'], 'vars' => ['by' => 'by', 'order' => 'order']]);




			if (($_GET['grid_id'] ?? null) === $list_id) {
			    if (isset($_GET['grid_action']) and $_GET['grid_action'] === 'sort') {
					  //if(key($sort['sort'])==$_GET['by']){
						if($_GET['order']=='asc'){
							$_GET['order'] = 'desc';
						} else {
							$_GET['order'] = 'asc';
						}
					 // }

			        $qyery_order = array($DB->ForSql($_GET['by'])=>$DB->ForSql($_GET['order']));
					  //$grid_options->SetSorting($_GET['by'],$_GET['order']);

/*			$firephp = FirePHP::getInstance(true);
         $firephp->fb($qyery_order,FirePHP::LOG);*/
			    }
			} else if(count($sort["sort"])) {
			        $qyery_order = array(key($sort['sort'])=>current($sort['sort']));
					 // $grid_options->SetSorting(key($sort['sort']),current($sort['sort']));
			}


		/**
		 * Фильтрация
		 */
		$filterOption = new Bitrix\Main\UI\Filter\Options($list_id);
		$filterData = $filterOption->getFilter(array());
		$arrfilter = array();

		foreach ($filterData as $key => $value) {
			 if ($key === 'UF_ACTIVE_TYPE' && count($value) > 0) {
				 $arrfilter['UF_ACTIVE_TYPE'] = $value;
			 }
			 if ($key === 'UF_ACTIVE_NAME' && strlen($value) > 0) {
			  $arrfilter['UF_ACTIVE_NAME'] = "%".$value."%";
			 }
			 if ($key === 'UF_LOTCNT' && strlen($value) > 0) {
			  $arrfilter['UF_LOTCNT'] = $value;
			 }
			 if ($key === 'UF_LOTPRICE' && strlen($value) > 0) {
			  $arrfilter['UF_LOTPRICE'] = $value;
			 }
			 if ($key === 'UF_ACTIVE_LASTPRICE' && strlen($value) > 0) {
			  $arrfilter['UF_ACTIVE_LASTPRICE'] = $value;
			 }
			 if (stripos($key, 'UF_ADD_DATE') !== FALSE) {
				 if(empty($filterData['UF_ADD_DATE_to'])){
				 	$filterData['UF_ADD_DATE_to'] = (new DateTime())->format('d.m.Y 23:59:59');
				 }
		       $arrfilter['>=UF_ADD_DATE'] = $filterData['UF_ADD_DATE_from'];
		       $arrfilter['<=UF_ADD_DATE'] = $filterData['UF_ADD_DATE_to'];
			 }
			 if ($key === 'UF_CURRENCY' && strlen($value) > 0) {
			  $arrfilter['UF_CURRENCY'] = $value;
			 }
		 	 if ($key === 'UF_ACTIVE_CODE' && strlen($value) > 0) {
			  $arrfilter['UF_ACTIVE_CODE'] = "%".$value."%";
			 }
			 if ($key === 'UF_SECID' && strlen($value) > 0) {
			  $arrfilter['UF_SECID'] = "%".$value."%";
			 }
		}


			$hlblock_id = 27;
			$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter = array('UF_PORTFOLIO'=>$elementInfo["ID"]);

			$rsData = $entity_data_class::getList(
			array(
			 'filter' => $arFilter,
			 'select' => array('ID'),
			 'count_total' => true,
			)
			);
		   $nav->allowAllRecords(true)//Показать все
		        //->setRecordCount($rsData->getSelectedRowsCount())//Для работы кнопки "показать все"
		        ->setRecordCount($rsData->getCount())//Для работы кнопки "показать все"
		        ->setPageSize($nav_params['nPageSize'])//Параметр сколько отображать на странице
		        ->initFromUri();
			 unset($rsData);



			if(count($arrfilter)>0){
			  $arFilter = array_merge($arFilter,$arrfilter);
			}


			$rsData = $entity_data_class::getList(array(
				'order' => $qyery_order,
				'select' => array('*'),
				'filter' => $arFilter,
				'limit' => $nav->getLimit(),
				'offset' => $nav->getOffset(),
				'count_total' => true,
			));



/**
 * Список фильтров
 */
$filter_list = array(
	        array("id" => "UF_ACTIVE_TYPE",
	        'type' => 'list',
           'items' => array('share' => 'Акции', 'share_usa'=>'Акции США', 'share_etf'=>'ETF', 'bond'=>'Облигации'),
			  'params' => ['multiple' => 'Y'],
	        "name" => "Тип актива",
	        "default" => true
	        ),
	        array("id" => "UF_ACTIVE_NAME",
	        'type' => 'text',
	        "name" => "Название актива",
	        "default" => true
	    		),
				array("id" => "UF_SECID",
				    'type' => 'text',
				    "name" => "Код бумаги (тикер, SECID)",
				    "default" => true
				),
			  array(  "id"    =>"UF_LOTCNT",
			    "name"  =>"Количество лотов",
			    'type' => 'text',
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_LOTPRICE",
			    "name"  =>"Цена приобретения 1 лота",
			    'type' => 'text',
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_ACTIVE_LASTPRICE",
			    "name"  =>"Тек. стоимость актива",
			    'type' => 'text',
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_ADD_DATE",
			    "name"  => "Дата добавления в портфель",
			    'type' => 'date',
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_CURRENCY",
			    "name"  =>"Валюта",
			    'type' => 'text',
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_ACTIVE_CODE",
			    "name"  =>"Код актива",
			    'type' => 'text',
			    "default"  =>true,
			  )

/*    [
        "id" => "FIRST_LITER",
        'type' => 'list',
        'items' => ['Y' => 'Показать'],
        //'params' => ['multiple' => 'Y'],
        "name" => GetMessage("CODERUN_LK_LK_CLIENT_LIST_TABLE_FIRST_LITER"),
        "default" => true
    ],*/
);

	/**
	 * Колонки таблицы
	 */
	$arHeaders = array(
	    array(  "id"    =>"ID",
				    "name"  =>"ID",
				    "sort"    =>"ID",
				    "align"    =>"right",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_ACTIVE_TYPE",
				    "name"  =>"Тип",
				    "sort"    =>"UF_ACTIVE_TYPE",
				    "align"    =>"right",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_ACTIVE_NAME",
				    "name"  =>"Актив",
				    "sort"    =>"UF_ACTIVE_NAME",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_SECID",
				    "name"  =>"Тикер",
				    "sort"    =>"UF_SECID",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_LOTCNT",
				    "name"  =>"Кол-во лотов",
				    "sort"    =>"UF_LOTCNT",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_LOTPRICE",
				    "name"  =>"Цена приобр. 1 лота",
				    "sort"    =>"UF_LOTPRICE",
				    "align"    =>"center",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_ACTIVE_LASTPRICE",
				    "name"  =>"Тек. стоим. актива",
				    "sort"    =>"UF_ACTIVE_LASTPRICE",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_ADD_DATE",
				    "name"  => "Дата добавл.",
				    "sort"    =>"UF_ADD_DATE",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_CURRENCY",
				    "name"  =>"Валюта",
				    "sort"    =>"UF_CURRENCY",
				    "default"  =>true,
				  ),
				  array(  "id"    =>"UF_ACTIVE_CODE",
				    "name"  =>"Код актива",
				    "sort"    =>"UF_ACTIVE_CODE",
				    "default"  =>true,
				  )
	);

		 $snippet = new \Bitrix\Main\Grid\Panel\Snippet();

		 require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
		 ?>
		 <tr>
		 	<td>
		     <div class="adm-toolbar-panel-container">
			    <div class="adm-toolbar-panel-flexible-space">
			        <?php
			        $APPLICATION->IncludeComponent('bitrix:main.ui.filter', '', array(
			            'FILTER_ID' => $list_id,
			            'GRID_ID' => $list_id,
			            'FILTER' => $filter_list,
			            'ENABLE_LIVE_SEARCH' => true,
			            'ENABLE_LABEL' => true
			        )
					  );
			        ?>
			    </div>
			      <div class="adm-toolbar-panel-align-right">
			    </div>
			</div>

			 <? // выведем таблицу списка элементов

							/**
							 * Данные по каждому выбранному элементу таблицы
							 */
							$list = [];
							$arTypesList = array('share'=>'Акция', 'share_usa'=>'Акция США', 'share_etf'=>'ETF', 'bond'=>'Облиг.');
							while ($row = $rsData->fetch()) {

								$aCols = array(
							      "UF_ACTIVE_TYPE" => $arTypesList[$row['UF_ACTIVE_TYPE']],
							      "UF_ACTIVE_NAME" => '<a href="'.$row['UF_ACTIVE_URL'].'" target="_blank">'.$row['UF_ACTIVE_NAME'].'</a><br><a href="'.$row['UF_EMITENT_URL'].'" target="_blank">'.$row['UF_EMITENT_NAME'].'</a>',
							   );
							    $list[] = [
							        'data' => $row,
									  'columns' => $aCols,
							        'actions' => array(
							            array(
							                'text' => 'Редактировать',
							                'default' => true,
							                'onclick' => "editPortfolioElement(".$row["ID"].")",
							            ),
							//            [
							//                'text' => 'Удалить',
							//                'default' => true,
							//                'onclick' => 'if(confirm("Точно?")){document.location.href="?op=delete&id=' . $row['ID'] . '"}'
							//            ]
							        )
							    ];
							}

						 \Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/css/main/grid/webform-button.css');

							$APPLICATION->IncludeComponent('bitrix:main.ui.grid', 'portfolio_grid', array(
							    'GRID_ID' => $list_id,
							    'COLUMNS' => $arHeaders,
							    'ROWS' => $list,
							    'SHOW_ROW_CHECKBOXES' => true,
							    'NAV_OBJECT' => $nav,
							    'AJAX_MODE' => 'N',
							    'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
							    'PAGE_SIZES' => array(
							        array('NAME' => '20', 'VALUE' => '20'),
							        array('NAME' => '50', 'VALUE' => '50'),
							        array('NAME' => '100', 'VALUE' => '100')
							    ),
							    'AJAX_OPTION_JUMP' => 'N',
							    'SHOW_CHECK_ALL_CHECKBOXES' => true,
							    'SHOW_ROW_ACTIONS_MENU' => true,
							    'SHOW_GRID_SETTINGS_MENU' => true,
							    'SHOW_NAVIGATION_PANEL' => true,
							    'SHOW_PAGINATION' => true,
							    'SHOW_SELECTED_COUNTER' => true,
							    'SHOW_TOTAL_COUNTER' => true,
							    'SHOW_PAGESIZE' => true,
							    'SHOW_ACTION_PANEL' => true,
							    'ALLOW_COLUMNS_SORT' => true,
							    'ALLOW_COLUMNS_RESIZE' => true,
							    'ALLOW_HORIZONTAL_SCROLL' => true,
							    'ALLOW_SORT' => true,
							    'ALLOW_PIN_HEADER' => true,
							    'AJAX_OPTION_HISTORY' => 'N',
							    'TOTAL_ROWS_COUNT_HTML' => '<span class="main-grid-panel-content-title">Всего:</span> <span class="main-grid-panel-content-text">' . $nav->getRecordCount() . '</span>',
							    'ACTION_PANEL' => array(
							        'GROUPS' => array(
							            'TYPE' => array(
							                'ITEMS' => array(
												     //$snippet->getForAllCheckbox(),
							                    array(
							                        'ID' => 'set-type',
							                        'TYPE' => 'DROPDOWN',
							                        'ITEMS' => array(
							                            array('VALUE' => '', 'NAME' => '- Выбрать -'),
							                            array('VALUE' => 'setInlotCnt', 'NAME' => 'Заполнить количество в лотах'),
							                            array('VALUE' => 'setActiveLinks', 'NAME' => 'Заполнить привязки к активам'),
							                        )
							                    ),
							                    array(
							                        'ID'       => 'setInlotCnt',
							                        'TYPE'     => 'BUTTON',
							                        'TEXT'        => 'Заполнить количество в лотах',
							                        'CLASS'        => 'apply',
															'ONCLICK' => "setInlotCnt('".$list_id."',BX.adminUiList.SendSelected('".$list_id."','act=test'));"
/*							                        'ONCHANGE' => array(
				                                    array(
				                                       'ACTION' => \Bitrix\Main\Grid\Panel\Actions::CALLBACK,
				                                       //'ACTION' => 'setInlotCnt',
				                                       'DATA' => array(
				                                          array(
				                                             'JS' => "setInlotCnt('".$list_id."',BX.adminUiList.SendSelected('".$list_id."','act=test'));"
				                                          )
				                                       )
				                                    )
				                                 )*/
							                    ),
/*							                    array(
							                        'ID' => 'apply',
							                        'TYPE' => 'BUTTON',
							                        'TEXT' => 'Применить',
							                        'CLASS' => 'apply',
							                        'ONCHANGE' => array(
				                                    array(
				                                       //'ACTION' => \Bitrix\Main\Grid\Panel\Actions::CALLBACK,
				                                       //'ACTION' => 'setInlotCnt',
				                                       'DATA' => array(
				                                          array(
				                                             'JS' => "setInlotCnt('".$list_id."',BX.adminUiList.SendSelected('".$list_id."','act=test'));"
				                                          )
				                                       )
				                                    )
				                                 )
							                    ),*/
													 // $snippet->getRemoveButton(),
							                    $snippet->getApplyButton(array(
															'ID' => 'setInlotCnt',
				                                 'ONCHANGE' => array(
				                                    array(
				                                       'ACTION' => \Bitrix\Main\Grid\Panel\Actions::CALLBACK,
				                                       'DATA' => array(
				                                          array(
				                                             'JS' => "BX.adminUiList.SendSelected('".$list_id."');"
				                                          )
				                                       )
				                                    )
				                                 )
				                              )),
							                    array(
							                        'ID' => 'delete',
							                        'TYPE' => 'BUTTON',
							                        'TEXT' => 'Удалить',
							                        'CLASS' => 'icon remove',
							                        'ONCHANGE' => ''
							                    ),
							                ),
							            )
							        ),
							    ),
							));
							?>
							<script>
						     function setInlotCnt(list_id, arElementsId){
						     	 //arElementsId = BX.adminUiList.SendSelected(list_id);

								 console.log('list_id',list_id);
								 console.log('arElementsId',arElementsId);
						     }
							  function editPortfolioElement(elementId, url){
							(new BX.CAdminDialog({
										'content_url': '/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=27&ID='+elementId+'&lang=ru&form_element_52_active_tab=fpt_portfolio_actives_actives_table',
										'content_post': {'from_module':'hightloadblock','bxpublic':'Y','sessid':BX.bitrix_sessid(),'bxshowsettings':'N'},
										'draggable': true,
										'resizable': true,
										/*'buttons': [BX.CAdminDialog.btnSave,BX.CAdminDialog.btnCancel] */
									})).Show();
							  }
							</script>
					 </td>
			    </tr>
		 <?
	}

	public function check()
	{
		return true;
	}

	public function action()
	{
		return true;
	}
} ?>