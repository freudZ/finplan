<? use Bitrix\Main\Context,
	    Bitrix\Highloadblock as HL;
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");

class fptPortfolioTabset
{
	public function getTabList($elementInfo)
	{

		$request = Context::getCurrent()->getRequest();
		$addTabs = $elementInfo['ID'] > 0
			&& $elementInfo['IBLOCK']['ID'] == 52
			&& (!isset($request['action']) || $request['action'] != 'copy');

		return $addTabs ? [
			[
				"DIV"   => 'fpt_portfolio_actives',
				"SORT"  => PHP_INT_MAX,
				"TAB"   => 'Активы',
				"TITLE" => 'Активы в портфеле',
			],
		] : null;
	}

	public function showTabContent($div, $elementInfo, $formData)
	{

			$sTableID = "tbl_fpt_actives_list"; // ID таблицы
			$PoSort = new CAdminSorting($sTableID, "UF_ADD_DATE", "asc"); // объект сортировки
			$PlAdmin = new CAdminList($sTableID, $PoSort); // основной объект списка
/*			echo "<pre  style='color:black; font-size:11px;'>";
            print_r($PlAdmin);
            echo "</pre>";*/

			$hlblock_id = 27;
			$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
			$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$arFilter = array('UF_PORTFOLIO'=>$elementInfo["ID"]);
			$rsData = $entity_data_class::getList(array(
				'order' => array('UF_ADD_DATE' => 'ASC'),
				'select' => array('*'),
				'filter' => $arFilter,
			));

			// преобразуем список в экземпляр класса CAdminResult
			$rsData = new CAdminResult($rsData, $sTableID);

			// аналогично CDBResult инициализируем постраничную навигацию.
			$rsData->NavStart();

			// отправим вывод переключателя страниц в основной объект $lAdmin
			$PlAdmin->NavText($rsData->GetNavPrint(GetMessage("rub_nav")));


			$PlAdmin->AddHeaders(array(
			  array(  "id"    =>"ID",
			    "content"  =>"ID",
			    "sort"    =>"id",
			    "align"    =>"right",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_ACTIVE_TYPE",
			    "content"  =>"Тип",
			    "sort"    =>"UF_ACTIVE_TYPE",
			    "align"    =>"right",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"NAME",
			    "content"  =>"Актив",
			    "sort"    =>"name",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_SECID",
			    "content"  =>"Тикер",
			    "sort"    =>"UF_SECID",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_LOTCNT",
			    "content"  =>"Количество лотов",
			    "sort"    =>"UF_LOTCNT",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_LOTPRICE",
			    "content"  =>"Цена приобретения 1 лота",
			    "sort"    =>"UF_LOTPRICE",
			    "align"    =>"center",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_ACTIVE_LASTPRICE",
			    "content"  =>"Тек. стоимость актива",
			    "sort"    =>"UF_ACTIVE_LASTPRICE",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_ADD_DATE",
			    "content"  => "Дата добавления в портфель",
			    "sort"    =>"UF_ADD_DATE",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_CURRENCY",
			    "content"  =>"Валюта",
			    "sort"    =>"UF_CURRENCY",
			    "default"  =>true,
			  ),
			  array(  "id"    =>"UF_ACTIVE_CODE",
			    "content"  =>"Код актива",
			    "sort"    =>"UF_ACTIVE_CODE",
			    "default"  =>true,
			  ),
			));

			while($arRes = $rsData->NavNext(true, "f_")):

			  // создаем строку. результат - экземпляр класса CAdminListRow
			  $row =& $PlAdmin->AddRow($f_ID, $arRes);

			  // далее настроим отображение значений при просмотре и редаткировании списка
			  $row->AddViewField("UF_ACTIVE_TYPE",$arRes['UF_ACTIVE_TYPE']=='share'?'A':($arRes['UF_ACTIVE_TYPE']=='bond'?'О':'-'));

			  // параметр NAME будет редактироваться как текст, а отображаться ссылкой
			  $row->AddInputField("NAME", array("size"=>20));
			  $row->AddViewField("NAME", '<a href="'.$arRes['UF_ACTIVE_URL'].'" target="_blank">'.$arRes['UF_ACTIVE_NAME'].'</a><br><a href="'.$arRes['UF_EMITENT_URL'].'" target="_blank">'.$arRes['UF_EMITENT_NAME'].'</a>');
			  $row->AddViewField("UF_SECID", $arRes['UF_SECID']);

			  // параметр LID будет редактироваться в виде выпадающего списка языков
			  //$row->AddEditField("LID", CLang::SelectBox("LID", $f_LID));
			  $row->AddEditField("UF_LOTCNT", $arRes['UF_LOTCNT']);

			  // параметр SORT будет редактироваться текстом
			  $row->AddInputField("UF_LOTPRICE", $arRes['UF_LOTPRICE']);
			  $row->AddInputField("UF_ACTIVE_LASTPRICE", $arRes['UF_ACTIVE_LASTPRICE']);
			  $row->AddInputField("UF_ADD_DATE", $arRes['UF_ADD_DATE']);
			  $row->AddInputField("UF_CURRENCY", $arRes['UF_CURRENCY']);
			  $row->AddInputField("UF_ACTIVE_CODE", $arRes['UF_ACTIVE_CODE']);

 			  // сформируем контекстное меню
			  $arActions = Array();



			  // редактирование элемента
			  $arActions[] = array(
			    "ICON"=>"edit",
			    "DEFAULT"=>true,
			    "TEXT"=>"Редактировать",
			    "ACTION"=>$PlAdmin->ActionRedirect("/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=27&ID=".$arRes["ID"]."&lang=ru")
			  );

			  // удаление элемента
/*			  if ($POST_RIGHT>="W")
			    $arActions[] = array(
			      "ICON"=>"delete",
			      "TEXT"=>GetMessage("rub_del"),
			      "ACTION"=>"if(confirm('".GetMessage('rub_del_conf')."')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
			    );*/

			  // вставим разделитель
			  $arActions[] = array("SEPARATOR"=>true);


			  // если последний элемент - разделитель, почистим мусор.
			  if(is_set($arActions[count($arActions)-1], "SEPARATOR"))
			    unset($arActions[count($arActions)-1]);

			  // применим контекстное меню к строке
			  $row->AddActions($arActions);

			endwhile;

			// резюме таблицы
			$PlAdmin->AddFooter(
			  array(
			    array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // кол-во элементов
			    array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // счетчик выбранных элементов
			  )
			);

			// групповые действия
/*			$PlAdmin->AddGroupActionTable(Array(
			  "delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // удалить выбранные элементы
			  "activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // активировать выбранные элементы
			  "deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // деактивировать выбранные элементы
			  ));*/

				// альтернативный вывод
				$PlAdmin->CheckListMode();
			 // не забудем разделить подготовку данных и вывод
				require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
		 ?>
		 <tr>
        <td colspan="2">
			 <? // выведем таблицу списка элементов
					$PlAdmin->DisplayList(); ?>
		  </td>
       </tr>
		 <?
	}

	public function check($params)
	{

		return true;
	}

	public function action($params)
	{

		return true;
	}
} ?>