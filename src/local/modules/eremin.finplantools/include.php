<?
global $DB;
$db_type = strtolower($DB->type);

/*Bitrix\Main\Loader::registerAutoloadClasses(
	"eremin.finplantools",
	array(
		"Eremin\\Finplantools\\fptTools" => "classes/fpt_tools.php",
	)
);
*/
//include("classes/fpt_tools.php");
CModule::AddAutoloadClasses(
	"eremin.finplantools",
	array(
		"fptTools" => "classes/fpt_tools.php",
		"fptCrontools" => "classes/fpt_crontools.php",
		"fptPortfolioTabset" => "classes/fpt_portfolio_tabs.php",
	)
);

