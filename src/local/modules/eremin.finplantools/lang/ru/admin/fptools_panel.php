<?php
 $MESS["FPT_PANEL_TAB1"] = "Анкеты";
 $MESS["FPT_PANEL_TAB2"] = "Список анкет";
 $MESS["FPT_EXCEL_DOWLOAD"] = "Скачать в excel";
 $MESS["COPY_TO_CLIPBOARD"] = "Скопировать";
 $MESS["OPEN_ANK_IN_NEW_TAB"] = "Открыть в новой вкладке";
 $MESS["OPEN_ANK_TABLE_HEADING"] = "V - Важность; <br>N - Удовлетворенность";
 $MESS["OPEN_ANK_TABLE_HEADING_V"] = "V - Важность";
 $MESS["OPEN_ANK_TABLE_HEADING_N"] = "N - Удовлетворенность";
 $MESS["OPEN_ANK_TABLE_HEADING_BG_COLOR"] = "#FFFFFF";
 $MESS["OPEN_ANK_TABLE_HEADING2_BG_COLOR"] = "#FFFF00";
 $MESS["FPT_PANEL_ANKETS_LIST_HEADING"] = "Список анкет";
 $MESS["FPT_PANEL_ANK_TAB_COMMON"] = "Общая таблица";
 $MESS["FPT_PANEL_ANK_TAB_V"] = "Важность";
 $MESS["FPT_PANEL_ANK_TAB_N"] = "Удовлетворенность";
 $MESS["FPT_PANEL_ANK_DATE"] = "Период отображения результатов:";
 $MESS["FPT_PANEL_ANK_DATE_START"] = "c";
 $MESS["FPT_PANEL_ANK_DATE_END"] = "по";
 $MESS["FPT_PANEL_ANK_DATE_APPLY"] = "применить";
 $MESS["FPT_PANEL_ANK_TAB_COMMMON_HEADING"] = "Сводные данные анкет";
 $MESS["FPT_PANEL_ANK_TAB_COMMMON_PARAMS_LINK"] = "Ссылка на страницу анкеты:";

 $MESS["FPT_EXCEL_WORKSHEET1_TITLE"] = "Общая";
 $MESS["FPT_EXCEL_WORKSHEET2_TITLE"] = "Важность";
 $MESS["FPT_EXCEL_WORKSHEET3_TITLE"] = "Удовлетворенность";
 $MESS["FPT_EXCEL_TABLE_SUBTITLE_QUESTION"] = "Оцениваемая характеристика";
?>