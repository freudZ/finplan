<?

IncludeModuleLangFile(__FILE__);
$APPLICATION->SetAdditionalCSS("/bitrix/panel/eremin.finplantools/main/fpt_menu.css");


if ($APPLICATION->GetGroupRight("eremin.finplantools") != "D") {
    $aMenu = array(
        "parent_menu" => "global_menu_services",
        "section" => "eremin.finplantools",
        "sort" => 900,
        "icon" => "eank_menu_icon",
        "text" => GetMessage("FPT_MENU_ITEM_TEXT"),
        "title" => GetMessage("FPT_MENU_ITEM_TITLE"),
        "items_id" => "eremin_finplantools",
        "items" => array()

    );
    $aMenu["items"][] = array(

        "text" => GetMessage("FPT_PANEL"),
        "title" => GetMessage("FPT_PANEL_ALT"),
        "url" => "fpt_panel.php?lang=" . LANGUAGE_ID,
        "icon" => "fpt_panel_icon",
        "page_icon" => "",
        "items" => array()
    );
    $aMenu["items"][] = array(

        "text" => GetMessage("FPT_CAP_CALC"),
        "title" => GetMessage("FPT_CAP_CALC_ALT"),
        "url" => "fpt_cap_calc.php?lang=" . LANGUAGE_ID,
        "icon" => "fpt_panel_icon",
        "page_icon" => "",
        "items" => array()
    );
    return $aMenu;

}
return false;
											  p

?>