<?//загрузка файла на сервер с последущим вызовом его обработки из консоли
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/include.php"); // инициализация модуля
//$APPLICATION->SetAdditionalCSS("/bitrix/components/bitrix/desktop/templates/admin/style.css");


CJSCore::Init (
	array("clipboard","jquery","date")
);

//\Bitrix\Main\UI\Extension::load("ui.buttons");
//\Bitrix\Main\UI\Extension::load("ui.icons");
//\Bitrix\Main\UI\Extension::load("ui.buttons.icons");

// подключим языковой файл
IncludeModuleLangFile(__FILE__);


// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //


// установим заголовок страницы
$APPLICATION->SetTitle("Загрузка компаний");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // второй общий пролог
?>

<?$APPLICATION->IncludeComponent("finplan:main.file.input", "parsers_loader",
   array(
      "INPUT_NAME"=>"PERIOD_CORP_FILE",
      "MULTIPLE"=>"N",
      "MODULE_ID"=>"main",
      "MAX_FILE_SIZE"=>"",
      "ALLOW_UPLOAD"=>"A",
      "ALLOW_UPLOAD_EXT"=>""
   ),
   false
);?>

<button id="period_corp_run">Обработать</button>

<script>
$(document).ready(function(){
	$('#period_corp_run').on('click', function(){
	  var file_id = $('input[name=PERIOD_CORP_FILE]').val();
	  		$.ajax({
	  		type: "POST",
	  		url: "/parsers/console/period_corp_console.php",
	  		dataType: "html",
	  		data:{ajax: "y", file_id: file_id},
	  		cashe: false,
	  		async: false,
	  		success: function(data){
	  			getResponse(data);
	  		}
	  		});

	  	 function getResponse(data){
			console.log(data);
	  	 }
	});
});
</script>