<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/include.php"); // инициализация модуля
//$APPLICATION->SetAdditionalCSS("/bitrix/components/bitrix/desktop/templates/admin/style.css");


CJSCore::Init (
   array("clipboard","jquery","date")
);

//\Bitrix\Main\UI\Extension::load("ui.buttons");
//\Bitrix\Main\UI\Extension::load("ui.icons");
//\Bitrix\Main\UI\Extension::load("ui.buttons.icons");

// подключим языковой файл
IncludeModuleLangFile(__FILE__);
if (!CModule::IncludeModule("iblock")) return;
// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("eremin.finplantools");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
?>
<?
$aTabs = array(
  array("DIV" => "edit1", "TAB" => GetMessage("FPT_PANEL_TAB1"), "ICON"=>"main_user_edit", "TITLE"=>GetMessage("FPT_PANEL_TAB1_TITLE")),
);

$date_start = date('01.m.y');
$date_end = date("t.m.y");

// Применение фильтра по дате
if(
    $REQUEST_METHOD == "POST" // проверка метода вызова страницы
//    &&
//    ($save!="" || $apply!="") // проверка нажатия кнопок "Сохранить" и "Применить"
    &&
    $POST_RIGHT=="W"          // проверка наличия прав на запись для модуля
    &&
    check_bitrix_sessid()     // проверка идентификатора сессии
)
{
  if(!empty($_POST["date_start"])){
  	$date_start = $_POST["date_start"];
  } else {
  	$date_start = date('01.m.Y');
  }
  if(!empty($_POST["date_end"])){
  	$date_end = $_POST["date_end"];
  }    else {
	$date_end = date("01.m.Y", strtotime("+1 month"));
  }
}




$tabControl = new CAdminTabControl("tabControl", $aTabs);
// здесь будет вся серверная обработка и подготовка данных
$ANKETA_IBLOCK = COption::GetOptionInt("eremin.finplantools", "ANKETA_IBLOCK", 0);
$ANKETA_ANSWERS_IBLOCK = COption::GetOptionInt("eremin.finplantools", "ANKETA_ANSWERS_IBLOCK", 0);
$ANKETA_CRITERIA_IBLOCK = COption::GetOptionInt("eremin.finplantools", "ANKETA_CRITERIA_IBLOCK", 0);


 //require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/lib/anketatools.php"); // инициализация модуля
if(
    $REQUEST_METHOD == "POST" // проверка метода вызова страницы
    &&
    $POST_RIGHT=="W"          // проверка наличия прав на запись для модуля
    &&
    check_bitrix_sessid()     // проверка идентификатора сессии
)

{



}



?>
<?


// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //


// установим заголовок страницы
$APPLICATION->SetTitle(GetMessage("SIOKE_LIST_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // второй общий пролог
?>

<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" id="anketa_post_form" name="fpt_post_form">
<?// проверка идентификатора сессии ?>
<?echo bitrix_sessid_post();?>
<input type="hidden" name="lang" value="<?=LANG?>">
<?
// отобразим заголовки закладок
$tabControl->Begin();

$tabControl->BeginNextTab();     ?>

		<tr class="heading"><td colspan="3">Ссылки на служебные страницы для управления данными</td></tr>
 		<tr>
			<td><span style="color:#330099">Управление радаром:</span> </td>
			<td><span id="ToolLinkCopyText_0"><b>https://fin-plan.org/lk/obligations/system.php</b></span></td>
			<td>
				   <button class="ui-btn ui-btn-primary ui-btn-xs" id="ToolLinkCopyBtn_0">Копировать</button> <a class="ui-btn ui-btn-success ui-btn-xs" href="http://fin-plan.org/lk/obligations/system.php" target="_blank">Перейти</a>
			 <script>
			 BX.clipboard.bindCopyClick(
				    BX('ToolLinkCopyBtn_0'),
				    {
				        text: BX('ToolLinkCopyText_0')
				    }
				);
          </script>
			</td>
		</tr>
		<tr class="heading"><td colspan="3">Парсеры для облигаций</td></tr>
		<tr class=""><td colspan="3">(грузить без пробелов, даты в формате дд.мм.гггг)</td></tr>
 		<tr>
			<td><span style="color:#330099">показывает все листы в почтовой программе MailChimp<br>
mc_list b6050240b6<br>
mc_interes 0ac7b87356:</span> </td>
			<td><span id="ToolLinkCopyText_1"><b>https://fin-plan.org/api/test.php</b></span></td>
			<td>
				   <button class="ui-btn ui-btn-primary ui-btn-xs" id="ToolLinkCopyBtn_1">Копировать</button> <a class="ui-btn ui-btn-success ui-btn-xs" href="https://fin-plan.org/api/test.php" target="_blank">Перейти</a>
			 <script>
			 BX.clipboard.bindCopyClick(
				    BX('ToolLinkCopyBtn_1'),
				    {
				        text: BX('ToolLinkCopyText_1')
				    }
				);
          </script>
			</td>
		</tr>

 		<tr>
			<td><span style="color:#330099">США:</span> </td>
			<td><span id="ToolLinkCopyText_2"><b>https://fin-plan.org/parsers/period_corp_usa.php</b></span></td>
			<td>
				   <button class="ui-btn ui-btn-primary ui-btn-xs" id="ToolLinkCopyBtn_2">Копировать</button> <a class="ui-btn ui-btn-success ui-btn-xs" href="https://fin-plan.org/parsers/period_corp_usa.php" target="_blank">Перейти</a>
			 <script>
			 BX.clipboard.bindCopyClick(
				    BX('ToolLinkCopyBtn_2'),
				    {
				        text: BX('ToolLinkCopyText_2')
				    }
				);
          </script>
			</td>
		</tr>

 		<tr>
			<td><span style="color:#330099">Регионы:</span> </td>
			<td><span id="ToolLinkCopyText_3"><b>https://fin-plan.org/parsers/period_regions.php</b></span></td>
			<td>
				   <button class="ui-btn ui-btn-primary ui-btn-xs" id="ToolLinkCopyBtn_3">Копировать</button> <a class="ui-btn ui-btn-success ui-btn-xs" href="https://fin-plan.org/parsers/period_regions.php" target="_blank">Перейти</a>
			 <script>
			 BX.clipboard.bindCopyClick(
				    BX('ToolLinkCopyBtn_3'),
				    {
				        text: BX('ToolLinkCopyText_3')
				    }
				);
          </script>
			</td>
		</tr>

 		<tr>
			<td><span style="color:#330099">Парсер для вебинаров:</span> </td>
			<td><span id="ToolLinkCopyText_4"><b>https://crm.fin-plan.org/webinar_parser.php</b></span></td>
			<td>
				   <button class="ui-btn ui-btn-primary ui-btn-xs" id="ToolLinkCopyBtn_4">Копировать</button> <a class="ui-btn ui-btn-success ui-btn-xs" href="https://crm.fin-plan.org/webinar_parser.php" target="_blank">Перейти</a>
			 <script>
			 BX.clipboard.bindCopyClick(
				    BX('ToolLinkCopyBtn_4'),
				    {
				        text: BX('ToolLinkCopyText_4')
				    }
				);
          </script>
			</td>
		</tr>

<?// завершаем интерфейс закладки
$tabControl->End();
?>
</form>
<?
// дополнительное уведомление об ошибках - вывод иконки около поля, в котором возникла ошибка
$tabControl->ShowWarnings("post_form", $message);
?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>