<?
if (isset($_REQUEST['work_start']))
{
	define("NO_AGENT_STATISTIC", true);
	define("NO_KEEP_STATISTIC", true);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule("eremin.finplantools");
//require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/eremin.finplantools/include.php"); // инициализация модуля
CModule::IncludeModule("iblock");
IncludeModuleLangFile(__FILE__);

CJSCore::Init(array('ajax', 'json', 'ls', 'session', 'jquery', 'popup', 'pull'));


$fptTools = new fptTools();

//Выборка со страниц компаний
$arCompanyPages  = $fptTools->getCompanyList();

if($_REQUEST["ajax"]=="y"){

if($_REQUEST["save_company"]=="y" && intval($_REQUEST["company_id"])>0){
  $result = $fptTools->saveOneCompanyHistory(intval($_REQUEST["company_id"]));
  echo json_encode($result);
  die();
}


if($_REQUEST["save_cur_company"]=="y" && intval($_REQUEST["company_id"])>0){
  $result = $fptTools->saveCurrentCap(intval($_REQUEST["company_id"]),$_REQUEST['cur_capa']);
  echo json_encode($result);
  die();
}


}

//$arCompanyPages1  = $fptTools->getMonthEndWorkday('30.09.2017');


$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm("Доступ запрещен");







$aTabs = array(array("DIV" => "edit1", "TAB" => "Обработка"));
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle("Расчет капитализации по выбранной акции");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
<style type="text/css">
a.fpt_button{
background-color: #36DCFF;
display: inline-block;
border: 1px solid #36DCFF;
padding: 10px;
color: #000000;
text-decoration: none;
border-radius: 3px;
}
a.fpt_button:hover{
border: 1px solid #00B2D6;
box-shadow: 0 0 7px 0 #808080;
}
.result_text{
padding: 10px;
margin-top: 10px;
display: block;
background-color: #e7f2dc;
}
.cap_table{
	width:100%;
}
    .cap_table {width: 100%; text-align: center; border-bottom: 2px solid #dfdfdf; border-radius: 6px; border-collapse: separate; border-spacing: 0px;}
    .cap_table thead tr {color: #ffffff; font-weight: bold; background: #328bc8;}
    .cap_table tr td {border-right: 1px solid #dfdfdf;}
    .cap_table tr td:last-child {border-right: 0px;}
    .cap_table tbody tr:nth-child(1n) {background: #f6f6f6;}
    .cap_table tbody tr:nth-child(2n) {background: #e6e6e6;}
    .cap_table tbody tr:hover {background: #BDF2FF; transition-duration: 0.6s;}
.rawDataBlock{
border: 1px solid #91bae6;
padding: 5px;
background-color:#b7e2f2;
border-radius: 9px;
margin: 5px auto;
}
.editDataBlock{
border: 1px solid #B1D9A3;
padding: 5px;
background-color: #E7FFDE;
border-radius: 9px;
margin: 5px auto;
}
.editDataBlock>span{
border-bottom: 1px dashed #507942;
display: block;
background-color: #fff;
padding: 4px;
border-radius: 5px;
margin-bottom: 3px;
}
.rawDataBlock>span{
border-bottom: 1px dashed #003399;
display: block;
background-color: #fff;
padding: 4px;
border-radius: 5px;
margin-bottom: 3px;
}
.OblCompanyData tbody tr:nth-child(1n){ background: #f6f6f6;}
.OblCompanyData tbody tr:nth-child(2n){ background: #e6e6e6;}
.OblCompanyData tbody td{
	width:20%;
}


</style>
<div class="ajax-preloader-wrap" id="ajax-preloader-wrap" style="display:none;">
	<div class="ajax-preloader">
		<div class="ajax-preloader-h"></div>
		<div class="ajax-preloader-m"></div>
	</div>
	<div class="ajax-preloader-shadow"></div>
</div>
<form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form" id="post_form">
<?
echo bitrix_sessid_post();

$tabControl->Begin();
$tabControl->BeginNextTab();
?>
	<tr>
		<td colspan="2">
		<?
		$aTabsCompanies = array();
		 $compIndex = 1;

		 foreach($arCompanyPages as $cname=>$arCompany){

				$aTabsCompanies[] = array("DIV" => "editCompany".$compIndex, "TAB" => $cname);
				$compIndex++;
				}

		$tabControlCompanies = new CAdminTabControl("tabControl", $aTabsCompanies);
		 $tabControlCompanies->Begin();?>
		<?foreach($arCompanyPages as $cname=>$arCompany):?>
		 <?
				$tabControlCompanies->BeginNextTab(); ?>
		 <!--<table id="" border="1"> -->
		 	<? $SECID = $fptTools->arActions[$arCompany["COMPANY_ID"]];
				$arPrices = $fptTools->arActionsPrices[$SECID];
/*				echo "<pre  style='color:black; font-size:11px;'>";
				   print_r($arPrices);
				   echo "</pre>";*/
			?>
				<tr><td colspan="5">
				<a class="save_capa_company fpt_button" href="javascript:void(0);" data-company-id="<?= $arCompany["COMPANY_ID"] ?>" data-cname="<?=$cname?>">Записать исторические данные</a>
				&nbsp;<a href="javascript:void(0);" class="fpt_button save_cur_capa_company" data-company-id="<?= $arCompany["COMPANY_ID"] ?>">Записать текущую капитализацию</a>
				</td></tr>
				<tr><td colspan="5"><span style="display:none;" id="company_result_<?= $arCompany["COMPANY_ID"] ?>" class="result_text"></span></td></tr>
				<? $MoexActionsDataLink = "https://fin-plan.org/bitrix/admin/highloadblock_rows_list.php?PAGEN_1=1&SIZEN_1=20&ENTITY_ID=24&lang=ru&set_filter=Y&adm_filter_applied=0&find_UF_ITEM=".$SECID."&by=UF_DATE&order=asc";
					$OblCompanyDataLink = "/bitrix/admin/highloadblock_rows_list.php?PAGEN_1=1&SIZEN_1=20&ENTITY_ID=14&lang=ru&set_filter=Y&adm_filter_applied=0&find_UF_COMPANY=".$arCompany["COMPANY_ID"];
				?>
			<tr class="heading"><td colspan="5"><?=$cname?> [<a href="<?= $MoexActionsDataLink ?>" target="_blank"><?= $SECID ?></a>] [ID компании: <a href="<?= $OblCompanyDataLink ?>" target="_blank"><?=$arCompany["COMPANY_ID"]?></a>]</td></tr>



			 	<? ksort($arPrices); ?>
			<tr>
				<td colspan="5">
					<table class="cap_table" id="capa_block_<?= $arCompany["COMPANY_ID"] ?>">


			  <thead>
			 <tr style="border: 1px solid #ccc; text-align: center;">

		 	<th>Годы / кварталы</th><th>1</th><th>2</th><th>3</th><th>4</th>

			</tr>
			 </thead>


			<tbody>
			 <?foreach($arPrices as $year=>$kvartals):?>
			 <tr class="year_row" data-year="<?= $year ?>">
			   <td style="border: 1px solid #ccc;" align="center"><?= $year ?></td>
			  <?
				  for($kj=1; $kj<=4; $kj++){
				  	if(!array_key_exists($kj, $kvartals)){
					  $kvartals[$kj] = array(
										            'PRICE' => '',
										            'COMPANY_ID' => '',
										            'ACTIONS_CNT' => '',
														'CAPITAL_FORMULA_DIVIDER' => '',
										            'CAPITALIZATION' => '');
				  	}
				  }
				 ksort($kvartals);
			  ?>
			  <?foreach($kvartals as $kv=>$kvData):?>
			  <? $bd_elem =key($kvData);
/*				  echo "<pre  style='color:black; font-size:11px;'>";
                 print_r($kvData);
                 echo "</pre>";*/

			  ?>

				 <td id="y_<?=$year?>_kv_<?=$kv?>" style="border: 1px solid #ccc;" class="kv_capa <?=($fptTools->checkCurrentQuarter($year,$kv)==true?'kv_cur_capa_'.$arCompany["COMPANY_ID"]:'kv_hist_capa');?>" align="center" data-secid="<?=$SECID?>" data-currcap="<?=($fptTools->checkCurrentQuarter($year,$kv)==true?'Y':'N');?>" data-year="<?= $year ?>" data-kv="<?=$kv?>" data-capa="<?=$kvData[$bd_elem]["CAPITALIZATION"]?>">
				 <div style="text-align: left;">
				 	<strong>Дата цены:</strong> <?=$bd_elem?><br>
				 	<?if(!empty($kvData[$bd_elem]["PRICE"])):?>
				 	<strong>Цена:</strong> <?=$kvData[$bd_elem]["PRICE"]?><br>
				 	<strong>Кол-во:</strong> <?=$kvData[$bd_elem]["ACTIONS_CNT"]?><br>
				 	<strong>Капа:</strong> <?=$kvData[$bd_elem]["CAPITALIZATION"]?>
					<?endif;?>
				 </div>

				 </td>
			  <?endforeach;?>
			  </tr>
			  <?endforeach;?>
				</tbody>

				</table>
				</td>
			</tr>

			<tr class="heading"><td colspan="5">Исторические данные по компании</td></tr>
			<tr>
			  <td colspan="5">
			  	<!-- Истоические данные -->
				<table class="OblCompanyData">
				<thead>
					<th>Год</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
				</thead>
				<tbody>
					<? ksort($arCompany["OblCompanyData"]); ?>
					<?foreach($arCompany["OblCompanyData"] as $year=>&$arKvartals):?>
					<tr class="oblYears">
					 <td><?= $year ?></td>
					    <? for($i=1; $i<=4; $i++){
							 if(!array_key_exists($i,$arKvartals)){
							  $arKvartals[$i] = array();
							 }
					    } ?>
						 <? ksort($arKvartals); ?>
						 <?foreach($arKvartals as $kv=>$kvData):?>
						 <td>
						  <div class="rawDataBlock">
						  	<span><span class="span_kv_num"><?= $kv ?>-<?= $year ?></span>: Записано в БД:</span>
							<?foreach($kvData as $kvdkey=>$kvdval):?>
							  <?if(is_array($kvdval)):?>
								 <?foreach($kvdval as $kvdkey1=>$kvdval1):?>
								 <strong><?= $kvdkey1 ?>:</strong> <?=$kvdval1?><br>
								 <?endforeach;?>
							  <?else:?>
							  <strong><?= $kvdkey ?>:</strong> <?=$kvdval?><br>
							  <?endif;?>
							<?endforeach;?>
						  </div>
						  <div class="editDataBlock">
						  	<span><span class="span_kv_num"><?= $kv ?>-<?= $year ?> </span>: Рассчитано:</span>
						  	<? if(!$kvData["UF_DATA"]["Прошлая капитализация"]){
						  		      $priceKvData = $arPrices[$year][$kv];
										$bd_elem =key($priceKvData);

                            	$kvData["UF_DATA"]["Прошлая капитализация"] = round(($arPrices[$year][$kv][$bd_elem]["CAPITALIZATION"]/1000000), 2);
										}?>
							<?foreach($kvData as $kvdkey=>$kvdval):?>
							  <?if(is_array($kvdval)):?>
								 <?foreach($kvdval as $kvdkey1=>$kvdval1):?>
								 <strong><?= $kvdkey1 ?>:</strong> <?=$kvdval1?><br>
								 <?endforeach;?>
							  <?else:?>
							  <strong><?= $kvdkey ?>:</strong> <?=$kvdval?><br>
							  <?endif;?>
							<?endforeach;?>
						      <?//= print_r($kvData,true); ?>

						  </div>
						 </td>
						<?endforeach;?>
					  </tr>
					<?endforeach;?>
				</tbody>
				</table>
			  </td>
			</tr>

		<?endforeach;?>
		<? $tabControlCompanies->End(); ?>
		</td>
	</tr>
<?

$tabControl->End();
?>
</form>
<script>
  $(document).ready(function(){

	$('.save_capa_company').on('click', function(e){
	e.preventDefault;
	var comany_id=$(this).data('company-id');
	var cname=$(this).data('cname');

    var data = {
      ajax: 'y',
		save_company: 'y',
		company_id: comany_id,
		cname: cname
        }
		var result = null;
        $.ajax({
            //url:"/lk/market_map/getData.php",
            type:"POST",
            dataType:"json",
            data:data,
				async: false,
            success:function(data){
				  result = data;
            }
        });

	 $('#company_result_'+comany_id).text(result["status"]).show();



   });


	$('.save_cur_capa_company').on('click', function(e){
		e.preventDefault;
		var comany_id=$(this).data('company-id');

    var data = {
      ajax: 'y',
		save_cur_company: 'y',
		company_id: comany_id,
		cur_capa: $('.kv_cur_capa_'+comany_id).data('capa')
        }
		var result = null;
		BX.showWait();
        $.ajax({
            //url:"/lk/market_map/getData.php",
            type:"POST",
            dataType:"json",
            data:data,
				async: false,
            success:function(data){
				  result = data;
            }
        });

	 $('#company_result_'+comany_id).text(result["result"]).show();
	 if(result["status"]=='error'){
	 $('#company_result_'+comany_id).addClass('error');
	 } else {
	 $('#company_result_'+comany_id).removeClass('error');
	 }
	 BX.closeWait();

   });




  });


</script>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>